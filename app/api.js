/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { call, put } from 'redux-saga/effects';
import axios from 'axios';
import { ErrorCheck, GetHeaders, GetHeadersWithoutContentType } from './commonUtils';
import jwt_decode from "jwt-decode";

const apis = {
    'login': 'api/v1/login',
    'fetchEntityList': 'api/v3/policy/etl/attribute/',
    'savePage': 'api/v3/page',
    'getRoleList': 'api/v3/role',
    'saveRole': 'api/v3/role',
    'getRoleById': 'api/v3/role',
    'updateRole': 'api/v3/role',
    'fetchMenuDetails': 'api/v3/menu/mapping?id=',
    'saveMenuMapping': 'api/v3/menu/mapping',
    'getUserDetailsById': 'api/v3/users/',
    'saveUserDetails': 'api/v3/users',
    'getTicketList': 'api/v3/tickets/filter',
    'getTicketDetails': 'api/v1/tickets/',
    'getUsersWithoutPagination': 'api/v3/users/all',
    'getTicketComments': 'api/v3/tickets/',
    'saveTicketComment': 'api/v3/tickets/',
    'saveTicketDetails': 'api/v3/tickets',
    'getNavigationAttribute': 'api/v3/nav',
    'saveNavigationAttribute': 'api/v3/menu',
    'deleteUser': 'api/v3/users/',
    'deleteRoleById': 'api/v3/role/',
    'getDashboardList': 'api/v3/page',
    'getAllPages': 'api/v4/pages',
    'deleteDashboardById': 'api/v3/page/',
    'resetPassword': 'api/unauth/credentials/reset',
    'accessPermissionCheck': 'api/unauth/verify/token?tokenId=',
    'changePassword': 'api/v3/users/changePassword',
    'getAccountList': 'api/v3/accounts',
    'deleteAccountById': 'api/v3/accounts/',
    'saveAccountDetails': 'api/v3/payment/tenant/subscription',
    'getMenuAttribute': 'api/v3/menu',
    'saveMenuDetails': 'api/v3/menu',
    'deleteMenuById': 'api/v3/menu/',
    "fetchUsersWithPagination": "api/v3/users/search",
    "getAllUsers": "api/v3/users",
    "getDeviceType": "api/v1/device/types",
    "onSubmitAlarmsAndAlert": "api/v1/anomaly/definition",
    "getAlarmsAndAlert": "api/v1/anomaly/filteredInsights",
    "getAnomalyDefinitionList": "api/v1/anomaly/definition/",
    "fileUploadRequest": "api/v1/s3/upload",
    "getAccountStats": "api/v3/accounts/stats",
    "getTenantDetailsById": "api/v3/accounts",
    "etlActions": "api/v3/transformations/detail",
    "etlHandler": "api/v3/dynamicEtl",
    "etlGetInfo": "api/v3/transformations",
    "etlDeleteHandle": "api/v3/transformations",
    "getDataSourceList": "api/v3/connectors",
    "deleteDeviceType": "api/v3/deviceType",
    "deleteConnector": "api/v3/connectors",
    "saveDataSource": "api/v3/connectors",
    "updateDevice": "api/v3/deviceType",
    "createDevice": "api/v3/deviceType",
    "getDataSource": "api/v3/connectors/mqtt",
    "changeETLState": "api/v3/transformations/rpcInfo?action=",
    'getAllSparksMethod': 'api/v3/elasticSearch/getAllSparkMethods',
    'deleteRow': "api/v1/anomaly/definition/",
    "getSourceJson": "api/v1/kafka",
    "saveFaaS": "api/v4/actions",
    "getFaaSById": "api/v4/actions",
    "getFaaSList": "api/v4/actions",
    "deleteFaas": "api/v4/actions",
    "getInstanceAndEntities": "api/v1/device",
    'getApiGatewayUntouchedList': 'api/v4/actions?attached=false&limit=30',
    'createApiGateway': 'api/v4/gateway/api',
    "saveEtlFunction": "api/v3/dynamicTemplate/",
    "getEtlFunctionById": "api/v3/dynamicTemplate",
    "getApiList": "api/v4/gateway/api/module",
    "deleteApiById": "api/v4/gateway/api/",
    "getApiById": "api/v4/gateway/api",
    "getEtlFunctionsUrl": "api/v3/dynamicTemplate",
    "deleteJsFunction": "api/v3/dynamicTemplate",
    "debugETL": "api/v3/dynamicEtl/debug",
    "getConnectionString": "api/v3/connection/mongo",
    "getAllGatewayApis": "api/v3/gateway/api/all",
    "saveApplication": "api/v3/applications",
    "getApplications": "api/v3/applications",
    "deleteApplication": "api/v3/applications",
    "getExcelFileDataUrl": "api/v1/s3/upload",
    "clonePage": "api/v3/page/clone",
    "invokeApi": "",
    "invokeFaasFunction": "api/v4/actions",
    "getDataConnectors": "api/v3/connectors/default",
    "getFaasImages": "api/v3/faas/images",
    "getParentPages": "api/v3/page/parentPages",
    "cloneFaaSFunction": "api/v4/actions/cloneAction",
    "getTemplateCodeFaaS": "api/v4/actions/sample",
    "cloneApi": "api/v4/gateway/cloneApi",
    "getAuditLogs": "api/v3/audit",
    "getAuditOperations": "api/v3/audit/operations",
    "forgotPassword": "api/unauth/forgotPassword",
    "exportApplication": "api/v3/applications/export",
    "importApplication": "api/v3/applications/import",
    "fetchLicenseDetails": "api/v3/payment/license",
    "getMyActivities": "api/v3/audit/myActivities",
    "getAccountsActivityLineChartData": "api/v3/accounts/activity/graph/line",
    "createPage": "api/v4/pages",
    "deleteWidgetById": "api/v3/widgets",
    "getAllWidgets": "api/v3/widgets",
    "getPageById": "api/v4/pages/",
    "cloneWidget": "api/v3/widgets/clone",
    "getAccountsActivityBarChartData": "api/v3/accounts/activity/graph/bar",
    "getTenantActivityLineChartData": "api/v3/accounts",
    "getTenantActivityBarChartData": "api/v3/accounts",
    "saveSimulation": "api/v3/simulate",
    "deleteSimulation": "api/v3/simulate",
    "getAllSimulations": "api/v3/simulate",
    "getAllPolicyData": "api/v3/policy",
    "deletePolicyItem": "api/v3/policy",
    "updateLicenseData": "api/v2/licenses",
    "getAllLicenses": "api/v3/payment/license",
    "testBootstrapConnection": "api/v3/connectors/testConnection",
    "getUserDetails": "api/v2/users/detail",
    "getCategoryData": "api/v3/connectors/category/",
    "getConnectorsByCategory": "api/v3/connectors/category/",
    "getTopicsData": "api/v3/connectors/topics?id=",
    "getFunction": "api/",
    "generateSchema": "api/v3/connectors/",
    "getDataCollection": "api/v3/database/mongo/collections",
    "getDataCollectionsById": "api/v3/database/mongo/",
    "getTenantStats": "api/v3/accounts/stats",
    "startStopSimulation": "api/v3/simulate",
    "getConnectors": "api/v3/connectors",
    "getConnectorsForCategory": "api/v3/connectors",
    "doTransformation": "api/v3/transformations",
    "getBucket": "api/v3/connectors/topics",
    "getSimulationById": "api/v3/simulate",
    "hitSchema": "api/v3/transformations/schema",
    "hitTimeSeriesSchema": "api/v3/transformations/timeSeries",
    "getCategoryList": "",
    "logout": "api/v1/logout",
    "savePolicy": "api/v3/policy",
    "fetchPolicies": "api/v3/policy",
    "clearDataCollection": "api/v3/database/mongo",
    "addDataCollection": "api/v3/database/mongo/collections",
    "createTimeScaleDBTable": "api/v3/database/timescale/create",
    "deleteDataCollection": "api/v3/database/mongo/delete/",
    "deleteTimescaleDataCollection": "api/v3/database/timescale/drop/",
    "getMonitorData": "api/v3/transformations/monitoring",
    "etlListByCategory": "api/v3/policy/etl/",
    "changeApplicationStatus": "api/v3/applications/publish/",
    "changePolicyStatus": "api/v3/policy/active/",
    "getApplicationsByUser": "api/v3/users/applications",
    "exportWidget": "api/v3/widgets/export ",
    "importWidget": "api/v3/widgets/import",
    "cloneAppApi": "api/v3/applications/clone",
    "getPods": "api/v3/pods",
    "getPodLogs": "api/v3/pods/",
    "multipleDeleteApi": "api/v4/gateway",
    "getAllMlExperiments": "api/v3/ml/experiment",
    "deleteMlExperiment": "api/v3/ml/experiment/",
    "trainMlExperiment": 'api/v3/ml/experiment/process',
    "getAllSimulationTopics": "api/v3/simulate/topics",
    "getAllNavsForSelectedRole": "api/v3/roles/common/menus",
    "getAPIKey": "api/v3/connectors/apiKey",
    "saveExperiment": "api/v3/ml/experiment",
    "getColumnsByConnector": "api/v3/connectors/row",
    "fetchProjects": "api/v3/projects",
    "addOrEditProject": "api/v3/projects",
    "deleteProject": "api/v3/projects/",
    "getExperimentById": "api/v3/ml/experiment",
    "saveLambdaFunctionSimulation": "api/v3/simulate/action",
    "getGroupList": "api/v3/group",
    "saveGroup": "api/v3/group",
    "cloneSimulation": "api/v3/simulate/clone",
    "deleteGroup": "api/v3/group/",
    "oAuthValidity": "api/v3/oauth/validityCheck",
    "addOAuthProvider": "api/v3/oauth/config",
    "getOAuthConfigurationDetails": "api/v3/oauth/config",
    "getOAuthConfigurations": "api/v3/oauth/config",
    "deleteOAuthProvider": "api/v3/oauth/config",
    "getTenantDetails": "api/unauth/account/home/details",
    "oauthLogoutRequest": "api/v3/oauth/logout",
    "getOAuthProviders": "api/v2/oAuthprovider",
    "getCommitHistory": "api/v3/oauth/github/commits",
    "getOAuthProvidersForSuperAdmin": "api/v3/oAuthprovider/category",
    "getApplicationRoleList": "api/v3/role/app",
    "deleteLambdaFunctionSimulation": "api/v3/simulate/multi",
    "getAccountSettings": "api/unauth/account/settings",
    "saveAccountSettings": "api/v3/accounts/settings",
    "getAllSchedule": "api/v3/scheduler",
    "createUpdateSchedule": "api/v3/scheduler",
    "getScheduleById": "api/v3/scheduler",
    "deleteSchedule": "api/v3/scheduler",
    "getInvitationStatus": "api/v3/projects/",
    "getAllGroups": "api/v3/group/all",
    "getAllAttributes": "api/v3/database/mongo/collections/attribute",
    "getAllDatasets": "api/v3/database/mongo/collections/report",
    "saveReport": "api/v4/report",
    "getAllReports": "api/v4/report",
    "deleteReport": "api/v4/report",
    "getReportById": "api/v4/report",
    "generateReport": "api/v4/report/generate/",
    "getStatisticsMlExperiment": "api/v3/ml/experiment/statistics",
    "getContributers": "api/v3/oauth/github/stats/contributors",
    "getCodeFrequency": "api/v3/oauth/github/stats/codeFrequency",
    "getParticipation": "api/v3/oauth/github/stats/participation",
    "contributerList": "api/v3/oauth/github/contributors",
    "contributerActivities": "api/v3/oauth/github/stats/contributor/activities",
    "getQueryDataCollection": "api/v3/database/mongo/query",
    "timeScaleQuery": "api/v3/database/timescale/execute",
    "debugLogs": "api/v4/actions/logs",
    "saveTemplate": "api/v2/templates",
    "getAllTemplates": "api/v2/templates",
    "deleteTemplate": "api/v2/templates",
    "getTemplateById": "api/v2/templates",
    "changeSchedulerState": "api/v3/scheduler/state",
    "getExperimentHistory": "api/v3/ml/experiment/history",
    "getJiraOauthStatus": "api/v3/oauth/jira/status",
    "createJiraProject": "api/v3/oauth/jira/project",
    "getJiraUsers": "api/v3/oauth/jira/users",
    "createSimulationForDemo": "api/v2/demotour/simulation",
    "startSimulationForDemo": "api/v2/demotour/simulation/start",
    "createConnectorForDemo": "api/v2/demotour/connector",
    "createTransformationForDemo": "api/v2/demotour/transformation",
    "startTransformationForDemo": "api/v2/demotour/transformation/start",
    "createFaasForDemo": "api/v2/demotour/faas",
    "createApiForDemo": "api/v2/demotour/api",
    "createWidgetForDemo": "api/v2/demotour/widget",
    "createPageForDemo": "api/v2/demotour/page",
    "createAppForDemo": "api/v2/demotour/app",
    "publishAppForDemo": "api/v2/demotour/app/publish",
    "deleteDemoTour": "api/v2/demotour",
    "getReportHistory": "api/v4/report/{reportId}/history?duration=",
    "getDemoTourStatus": "api/v2/demotour/status",
    "getAllFaaSForScheduler": "api/v3/scheduler/actions",
    "submitRequestFaq": "api/v3/faq",
    "getAllFaq": "api/v3/faq",
    "getByIdFaq": "api/v3/faq",
    "deleteFaq": "api/v3/faq",
    "getAllAlarms": "api/v3/alarm/filterAlarms",
    "updateAlarmState": "api/v3/alarm/",
    "getAllProjectsForRegistration": "api/unauth/projects",
    "onUserRegistration": "api/v3/payment/user/subscription",
    "uploadFile": "api/v3/storage/upload",
    "getAllImages": "api/v3/applications/images",
    "deleteFile": "api/v3/storage/upload",
    'getNotificationList': 'api/v3/notification',
    'updateNotificationList': 'api/v3/notification',
    "getAllProperties": "api/v3/consul",
    "createOrUpdateConsul": "api/v3/consul",
    "deleteConsulProperty": "api/v3/consul",
    "generateClientIds": "api/v3/connectors/client/",
    "registerDevice": "api/v3/connectors/register/",
    "getDockerImage": "api/v3/docker/image",
    "createOrEditDockerImage": "api/v3/docker/image",
    "deleteDockerImage": "api/v3/docker/image",
    "getDependencyList": "api/v3/docker/image/dependencies",
    "getCollectionDetails": "api/v3/dataAttributes/keys",
    "getDurationList": "api/v3/platform/dropdown/filters",
    "getConnectorStatus": "api/v3/dataAttributes/connector/status",
    "createFastApp": "api/v3/dataAttributes",
    "getCdnList": "",
    "editDevice": "api/v3/deviceType/",
    "refreshSchema": "api/v3/dataAttributes/refresh",
    "verifySchema": "api/v3/dataAttributes/verifySchema",
    "resetDeviceAttributes": "api/v3/dataAttributes/loadSchema/",
    "deleteDevice": "api/v3/deviceType/",
    "getProfileDetails": "api/v3/user/dashboard/profile",
    "getSubscriptionDetails": "api/v3/user/dashboard/myplans",
    "getBillingDetails": "api/v3/user/dashboard/billing/details",
    "getProfileStatsAnalysis": "api/v3/user/dashboard/resources/stats",
    "getAllAvailablePlans": "api/v3/user/dashboard/available/plans",
    "cancelSubscription": "api/v3/user/dashboard/cancel/subscription",
    "cancelSubscriptionTenant": "api/v3/tenant/dashboard/cancel/subscription",
    "upgradeSubscription": "api/v3/user/dashboard",
    "renewSubscription": "api/v3/user/dashboard/renew/subscription",
    "emailCredentials": "api/v3/connectors/email",
    "getVMQCredentials": "api/v3/platform/mqtt",
    "shareResources": "api/v3/user/dashboard/resource/",
    "saveCommands": "api/v3/connectors/control",
    "deleteCommands": "api/v3/connectors/control/",
    "getDeviceGroups": "api/v3/devices/groups/",
    "addOrUpdateGroup": "api/v3/devices/groups",
    "deleteClusterGroup": "api/v3/devices/groups/",
    "getTagsForDevice": "api/v3/devices/groups/",
    "getETLMathFunctions": "api/v3/dataAttributes/functions",
    "fastAppToggleActions": "api/v3/dataAttributes/toggle",
    "getAllProducts": "api/v3/products",
    "getAllProductsUnAuth": "api/unauth/products",
    "saveLicense": "api/v3/license",
    "getAllDevices": "api/v3/devices/groups/filter",
    "getAllConnectorsAndGroups": "api/v3/devices/groups",
    "deviceTypeList": "api/v3/deviceType/filters",
    "getDeviceInfo": "api/v3/connectors/{connectorId}/device/{deviceId}",
    "saveTag": "api/v3/devices/groups/tag",
    "getAllTags": "api/v3/devices/",
    "getTagsByGroupId": "api/v3/devices/groups",
    "saveTagByGroupId": "api/v3/devices/groups",
    "debugDeviceAttributes": "api/v3/transformations/debug",
    "createOrUpdateEvent": "api/v3/event",
    "getAttributeList": "api/v3/dataAttributes/attributes",
    "getEventList": "api/v3/event",
    "getOtp": "api/unauth/otp",
    "otpStatus": "api/unauth/otpStatus",
    "deleteEvent": "api/v3/event",
    "deleteTag": "api/v3/devices/groups/tag/",
    "getEventById": "api/v3/event",
    "createorEditAction": "api/v3/event/trigger",
    "getActionsList": "api/v3/event/trigger",
    "getAddOns": "api/v3/payment/addOns",
    "deleteAction": "api/v3/event/trigger",
    "getTopicsFromConnectorID": "api/v3/connectors",
    "getDeviceTypeList": "api/v3/event/deviceType",
    "saveDeviceTopology": "api/v3/deviceTopology/",
    "getDeviceTopology": "api/v3/deviceTopology/",
    "diagnosticDeviceList": "api/v3/connectors/debug/deviceType",
    "isEvaluateEvent": "api/v3/event/state",
    "getDeviceGroupsForMap": "api/v3/deviceType/{connectorId}/dashboard/filters",
    "getAllDevicesForMap": "api/v4/dashboard/analytics/functions/map",
    "getDeveloperQuota": "api/v3/products/count",
    "getAllRpc": "api/v3/connectors",
    "getDeviceTypeById": "api/v3/deviceType/{deviceTypeId}/details",
    "getAllDeviceType": "api/v3/deviceType",
    "eventList": "api/v4/event/history",
    "getListOfConnectorsWithGroups": "api/v3/simulate/deviceType",
    "getDetailsPageData": "api/v3/dashboard/analytics/functions/",
    "getAndUpdateAlarmConfigData": "api/v3/alarm",
    "getCssThemes": "api/v3/user/css",
    "getSelectedTheme": "api/v3/user/css/theme.scss?id=",
    "dashboardConfig": "api/v3/custom/dashboard/config",
    "getMonitoringchartData": "api/v3/dashboard/analytics/functions",
    "saveConfig": "api/v3/custom/dashboard/config",
    "getAttributes": "api/v3/alarm",
    "getAlarmFilterData": "api/v4/alarm/filter",
    "getGroupDataByConnector": "api/v3/dashboard/analytics/functions/history",
    "resetAlarm": "api/v3/alarm",
    "updateAlarmStatus": "api/v3/alarm/status/",
    "getCommandHistory": "api/v3/dashboard/analytics/functions/commandHistory",
    "rpcCommandPublish": "api/v3/connectors/publish",
    "getDevicesFromGroupId": "api/v3/event/filter/devices",
    "searchByIdOrNameDeviceType": "api/v3/deviceType/",
    "getAlarmsCount": "api/v4/aggregate/dashboard/totalDistribution/alarm",
    "getAlarmsPieChartData": "api/v4/aggregate/dashboard/alarm/distribution/pieChart",
    "getDailyAlarms": "api/v4/aggregate/dashboard/daily/alarm",
    "getAlarmsDistribution": "api/v4/aggregate/dashboard/alarm/distribution/deviceGroup",
    "getMetricActions": "api/v3/forecast/actions",
    "forcastPredict": "api/v3/forecast/predict",
    "getForcastData": "api/v3/forecast",
    "getDataConfig": "api/v3/forecast/",
    "saveOrUpdateForcastConfig": "api/v3/forecast",
    "saveMapZoomLevel": "api/v3/deviceType/map/config",
    "getStatsData": "api/v3/deviceType",
    "getNotificationStatus": "api/v3/deviceType/notification",
    "changeNotificationStatus": "api/v3/deviceType/notification",
    "getAllActions": "api/v3/event/trigger/all",
    "validateTenantName": "api/unauth/check/tenantName?tenantName=",
    "getDeviceTypes": "api/v3/dataAttributes/deviceType/filters",
    "getAllDevicesByDeviceTypes": "api/v3/users/devices",
    "getAllNoteBooks": "api/v3/notebook",
    "createNoteBook": "api/v3/notebook",
    "deleteNoteBook": "api/v3/notebook",
    "updateNoteBook": "api/v3/notebook",
    "debugNoteBook": "api/v3/notebook/start",
    "stopNoteBookCodeBlock": "api/v3/notebook/stop",
    "getNoteBookById": "api/v3/notebook",
    "getTenantDashboardDetails": "api/v3/tenant/dashboard/plan/details",
    "getTenantTransactionDetails": "api/v3/tenant/dashboard/transaction/details",
    "paymentOfTenantAddOns": "api/v3/tenant/dashboard/tenant/addOns",
    "saveAddOns": "api/v3/account/addOns",
    "getAllAddOns": "api/v3/account/addOns",
    "deleteAddOns": "api/v3/account/addOns",
    "renewSubscriptionFlex": "api/v3/tenant/dashboard/renew/subscription",
    "purchaseSubscriptionFlex": "api/v3/tenant/dashboard/plan/purchase",
    "deletePage": "api/v4/pages",
    "getAllCustomMenus": "api/v4/custom/menu",
    "getCustomMenuById": "api/v4/custom/menu",
    "saveCustomMenu": "api/v4/custom/menu",
    "deleteCustomMenu": "api/v4/custom/menu",
    "deleteNavById": "api/v4/custom/menu/",
    "saveNavCustomMenu": "api/v4/custom/menu/",
    "updateNavCustomMenu": "api/v4/custom/menu/",
    "updateCustomMenu": "api/v4/custom/menu/",
    "getAllDeviceTypeWithTopics": "api/v3/deviceType/attributes",
    "getSparkFunctions": "api/v3/transformations/sparkFunctions",
    "saveETL": "api/v3/transformations/new",
    "getTimescaleConnectorTables": "api/v3/connectors/timescale/selectTables",
    "getAllAccounts": "api/v3/payment/tenant",
    "getLineMonitoringchartData": "api/v4/dashboard/analytics/functions/line",
    "timeScaleDataCollection": "api/v3/database/timescale/tables",
    "timeScaleDataByTableName": "api/v3/database/timescale/select",
    "executeFunctions": "api/v3/dynamicTemplate/error",
    "getLibrary": "api/v3/dynamicTemplate/dependency",
    "clearTSTableData": "api/v3/database/timescale/delete",
    "addTimescaleDocuments": "api/v3/database/timescale/insert",
    "getBucketImages": "api/v3/storage/images?fileType=",
    "resendInvite": "api/v3/users/resend/email?id=",
    "createTopicSchema": "api/v3/connectors/schema",
    "describeTimeScaleTable": "api/v3/database/timescale/describe",
    "alterTimeScaleTable": "api/v3/database/timescale/alter",
    "clearTopicSchema": "api/v3/connectors/{connectorId}/schema/{topicId}",
    "saveMiscTopic": "api/v3/connectors/miscellaneous/topic",
    "addMongoDocument": "api/v3/database/mongo/insert",
    "saveDefaultTheme": "api/v4/tenant/theme/",
    "getTopics": "api/v3/connectors/{deviceTypeId}/device/{id}",
    "uploadProfilePicture": "api/v3/users/upload/profilePicture",
    "promoteAsOwner": "api/v3/users/upgrade/",
    "getMQTTCredentials": "api/v3/deviceType/mqtt",
    "getFlowManager": "api/v4/flowmanager",
    "restartNoteBookCodeBlock": "api/v3/notebook/restart",
    "clearCheckpoint": "api/v3/transformations/checkpoint/",
    "createFlowManager": "api/v4/flowmanager",
    "startStopFlowManager": "api/v4/flowmanager/",
    "deleteFlowManager": "api/v4/flowmanager",
    "getFlowManagerHistory": "api/v4/flowmanager/history",
    "deleteControlImage": "api/v3/connectors/control/removeIcon/",
    "getGoogleMapKey": "api/v3/platform/googleMapsKey",
    "monitorNotebook": "api/v3/notebook",
    "createEtlScheduler": "api/v3/transformations/scheduler",
    "getAllEtl": "api/v3/transformations/scheduler/batch/etl",
    "updateEtlScheduler": "api/v3/transformations/scheduler/",
    "deleteEtlScheduler": "api/v3/transformations/scheduler/",
    "getAllEtlScheduler": "api/v3/transformations/scheduler",
    "getEtlSchedulerById": "api/v3/transformations/scheduler/",
    "etlSchedulerAction": "api/v3/transformations/scheduler/",
    "getAllDLSentiments": "api/v3/deeplearning",
    "createDLSentiment": "api/v3/deeplearning",
    "deleteDLSentiment": "api/v3/deeplearning",
    "getAllDLSentimentById": "api/v3/deeplearning",
    "updateDLSentiment": "api/v3/deeplearning"
}


export function* apiCallHandler(action, responseConst, errorConst, apiUrlConstant, isLoading = true) {
    try {
        yield [apiTryBlockHandler(action, responseConst, apiUrlConstant, isLoading)];
    } catch (error) {
        yield [ErrorCheck(action, error, errorConst)];
    } finally {
        isLoading ? yield put({ type: 'hide_loader' }) : null
    }
}

function* apiTryBlockHandler(action, responseConst, apiUrlConstant, isLoading) {
    const baseUrl = window.API_URL
    let url = baseUrl + apis[apiUrlConstant];
    switch (apiUrlConstant) {
        case 'login': {
            const headers = {
                'X-Requested-With': 'XMLHttpRequest',
            }
            const response = yield call(axios.post, url, action.data, { headers });
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        case 'savePolicy': {
            if (action.id) {
                url = url + `/${action.id}`;
            }
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(action.id ? axios.patch : axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.message });
            break;
        }
        case 'fetchPolicies': {
            url = url + `/${action.deviceId}`;
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        case 'etlDeleteHandle': {
            url = url + `/new?id=${action.id}`;
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.delete, url, GetHeaders());
            yield put({ type: responseConst, response: { id: action.id, message: response.data.message } });
            break;
        }
        case 'hitTimeSeriesSchema':
        case 'hitSchema': {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: action.topic });
            break;
        }

        case 'doTransformation': {
            if (action.id) {
                url = url + "?id=" + action.id
            } else url = url;
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(action.id ? axios.put : axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.message });
            break;
        }
        case 'getConnectorsForCategory':
        case 'getConnectors': {
            apiUrlConstant === "getConnectors" ? isLoading ? yield put({ type: 'show_loader' }) : null : '';
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        case 'getFunction': {
            url = url + action.functionType;
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        case 'getTopicsData': {
            url = url + action.id;
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        case 'getConnectorsByCategory':
        case 'getCategoryData': {
            url = url + action.category;
            apiUrlConstant === "getCategoryData" ? isLoading ? yield put({ type: 'show_loader' }) : null : ''
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data, category: action.category, isFromMount: action.fromMount });
            break;
        }
        case 'etlActions': {
            url = action.id ? url + `/${action.id}` : url;
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(action.id ? axios.delete : axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: action.id ? action.id : response.data.data });
            break;
        }
        case 'etlGetInfo': {
            url = url + `/${action.id}/new`
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        case 'getTimescaleConnectorTables': {
            url = url + `/${action.connectorId}`
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        case 'getTemplateCodeFaaS': {
            url = url + `?interpreter=${action.interpreter}` + `&database=${action.database}`
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        case 'etlHandler': {
            url = action.id ? url + `/${action.id}` : url;
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(action.id ? axios.put : axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.message });
            break;
        }
        case 'saveAccountSettings': {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.put, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: { message: response.data.message, isReset: action.isReset } });
            break;
        }
        case 'getDeviceType': {
            url = url + "?isConfigured=" + action.isConfigured
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        case 'getInstanceAndEntities': {
            url = url + "?deviceType=" + action.deviceType
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        case 'etlListByCategory': {
            url = url + action.category
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        case 'fetchEntityList': {
            url = url + action.id;
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        case 'savePage': {
            let id, method = axios.post
            if (action.id) {
                id = action.id
                url = url + "/" + id
                method = axios.put
            }
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(method, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.message });
            break;
        }
        case 'getPageById': {
            url = url + action.id
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data })
            break;
        }
        case 'getApplicationRoleList':
        case 'getRoleList': {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data })
            break;
        }
        case 'fetchMenuDetails': {
            url = url + action.id
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data })
            break;
        }
        case 'saveMenuMapping': {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.put, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data })
            break;
        }
        case 'getUserDetailsById': {
            url = url + action.id
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data })
            break;
        }
        case 'saveUserDetails': {
            let id,
                method = axios.post,
                successMessage = "User created successfully."
            if (action.payload.id) {
                id = action.payload.id
                url = url + '/' + id
                method = axios.put
                successMessage = "User updated successfully."
            }
            const response = yield call(method, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: successMessage });
            break;
        }
        case 'getTicketList': {
            url = url + "?jiraEnabled=" + localStorage.isJiraLoggedIn
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.data, currentPageNumber: action.currentPageNumber })
            break;
        }
        case 'getTicketListByDeviceByStatus': {
            url = url + action.id + "/" + action.status
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data })
            break;
        }
        case 'getTicketDetails': {
            url = url + action.id
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data })
            break;
        }
        case 'getUsersWithoutPagination': {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data })
            break;
        }
        case 'getTicketComments': {
            url = url + action.id + '/comments?jiraEnabled=' + localStorage.isJiraLoggedIn
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data })
            break;
        }
        case 'saveTicketComment': {
            let id, method = axios.post
            if (action.payload.commentId) {
                url = url + action.id + '/comments/' + action.payload.commentId + '?jiraEnabled=' + localStorage.isJiraLoggedIn
                method = axios.put
            } else {
                url = url + action.id + '/comments?jiraEnabled=' + localStorage.isJiraLoggedIn
            }
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(method, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        case 'saveTicketDetails': {
            let method = axios.post
            if (action.payload.id) {
                url = url + "/" + action.payload.id + "?jiraEnabled=" + localStorage.isJiraLoggedIn
                method = axios.put
            } else {
                url = url + "?jiraEnabled=" + localStorage.isJiraLoggedIn

            }
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(method, url, action.payload, GetHeaders());
            yield put({
                type: responseConst,
                response: action.payload.ticketId ? "Ticket updated successfully." : "Ticket added successfully.",
                noModal: action.noModal
            });
            break;
        }
        case 'getNavigationAttribute': {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data })
            break;
        }
        case 'saveNavigationAttribute': {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.put, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.message })
            break;
        }

        case 'deleteUser':
        case 'deleteTag': {
            url = url + action.id
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.delete, url, GetHeaders());
            yield put({ type: responseConst, response: { id: action.id, message: response.data.message } })
            break;
        }
        case 'deleteRoleById': {
            url = url + action.id
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.delete, url, GetHeaders());
            yield put({ type: responseConst, response: { id: action.id, message: response.data.message } })
            break;
        }
        case 'getConfigDetails': {
            url = url + action.id
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data })
            break;
        }
        case 'getAllPages':
        case 'getDashboardList': {
            if (action.type && action.model) {
                url = url + "?deviceType=" + action.deviceType + "&deviceModel=" + action.model
            }
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data })
            break;
        }
        case 'deleteDashboardById': {
            url = url + action.dashboardId
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.delete, url, GetHeaders());
            yield put({ type: responseConst, response: { id: action.dashboardId, message: response.data.message } })
            // yield put({ type: 'app/HomePage/FETCH_SIDE_NAV_REQUEST' })
            break;
        }
        case 'deleteMqttConnection': {
            url = url + action.data
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.delete, url, GetHeaders());
            yield put({ type: responseConst, response: action.data })
            break;
        }
        case 'resetPassword': {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.post, url, action.data);
            yield put({ type: responseConst })
            break;
        }
        case 'accessPermissionCheck': {
            url = `${url}${action.data.tokenId}`
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url);
            yield put({ type: responseConst, response: response.data.data })
            break;
        }

        case "onUserRegistration": {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.data })
            break;
        }
        case 'changePassword': {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.post, url, action.data, GetHeaders());
            yield put({ type: responseConst })
            break;
        }
        case 'getFaasDetails': {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            if (action.id)
                url = url + action.id
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        case 'getAccountList': {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data })
            break;
        }
        case "deleteCommands": {
            url = url + action.id
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.delete, url, GetHeaders());
            yield put({ type: responseConst, response: { id: action.id, message: response.data.message, actionType: "rpc" } })
            break;
        }
        case 'deleteAccountById': {
            url = url + action.data
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.delete, url, GetHeaders());
            yield put({ type: responseConst, response: { id: action.data, message: response.data.message } })
            break;
        }
        case 'updateNavCustomMenu': {
            url = url + `${action.id}/update_nav/${action.payload.menuId}`
            const response = yield call(axios.patch, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.message })
            break;
        }
        case 'saveDefaultTheme': {
            url = url + `${action.tenantName}/${action.id}/theme.css `
            const response = yield call(axios.patch, url, {}, GetHeaders());
            yield put({ type: responseConst, response: { message: response.data.message, id: action.id } })
            break;
        }
        case 'updateCustomMenu': {
            url = url + action.id
            const response = yield call(axios.patch, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.message })
            break;
        }
        case 'saveNavCustomMenu': {
            if (action.isParent)
                url = url + `${action.id}/add_new_nav`
            else
                url = url + `${action.id}/add_child_nav`

            const response = yield call(axios.patch, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.message })
            break;
        }
        case 'deleteNavById': {
            url = url + `${action.customMenuId}/delete_nav/${action.navId}`;
            const response = yield call(axios.patch, url, {}, GetHeaders());
            yield put({ type: responseConst, response: { navId: action.navId, message: response.data.message } })
            break;
        }
        case 'saveAccountDetails': {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        case 'getMenuAttribute': {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data })
            break;
        }
        case 'saveMenuDetails': {
            let id, method = axios.post, successMessage = "Navigation saved succesfully."
            if (action.payload.id) {
                id = action.payload.id
                url = url + id
                method = axios.put
                successMessage = "Navigation updated successfully."
            }
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(method, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: successMessage });
            break;
        }
        case 'deleteMenuById': {
            url = url + action.data
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.delete, url, GetHeaders());
            yield put({ type: responseConst, response: action.data })
            break;
        }
        case 'fetchUsersWithPagination': {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.data })
            break;
        }
        case 'getAllUsers': {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            url = `${url}?max=100&offset=0`;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data })
            break;

        }
        case 'getDashboardById': {
            url = `https://dashboard-studio.herokuapp.com/dashboard/${action.id}`
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data });
            break;
        }
        case "onSubmitAlarmsAndAlert": {
            let payload = action.payload, method = axios.post
            if (action.action != "add") {
                payload = action.payload
                url = url + "/" + action.payload.deviceType
                method = axios.put
            }
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(method, url, payload, GetHeaders());
            yield put({ type: responseConst, response: Math.random() });
            break;
        }
        case 'getAnomalyDefinitionList': {
            url = url + action.deviceType
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data })
            break;
        }
        case 'getAlarmsAndAlert': {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        case "fileUploadRequest": {
            if (action.imageType === "certificate") {
                url = `${url}?fileType=${action.imageType}`;
            }
            let formData = new FormData()
            formData.append('file', action.payload)
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.post, url, formData, GetHeaders());
            yield put({ type: responseConst, response: { secure_url: response.data.data.secure_url, imageType: action.imageType, eventId: action.eventId } });
            break;
        }
        case 'getAccountStats': {
            if (action.id) {
                url = `${url}/${action.id}`;
            }
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data })
            break;
        }
        case 'getDataSourceList': {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data })
            break;
        }
        case 'deleteSchedule':
        case 'deleteDeviceType':
        case 'deleteConnector': {
            url += `?id=${action.id}`
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.delete, url, GetHeaders());
            yield put({ type: responseConst, response: { message: response.data.message, id: action.id } })
            break;
        }
        case 'createUpdateSchedule': {
            action.payload.id ? url += `?id=${action.payload.id}` : null;
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(action.payload.id ? axios.put : axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: { message: response.data.message, id: action.payload.id } })
            break;
        }
        case 'updateDevice': {
            url += `/${action.payload.id}`;
            let payload;
            payload = {
                description: action.payload.description,
                reportInterval: action.payload.reportInterval,
                image: action.payload.image
            }
            if (!action.isDeviceImageChanged) {
                delete payload.image
            }
            const response = yield call(axios.patch, url, payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.message })
            break;
        }
        case 'saveDataSource': {
            action.payload.id ? url += `?id=${action.payload.id}` : null;
            let payload;
            if (action.payload.id && action.payload.category === "MQTT" && !action.payload.isExternal) {
                url = url.split('?')[0] + `/${action.payload.id}`
                payload = {
                    description: action.payload.description,
                    reportInterval: action.payload.reportInterval,
                    image: action.payload.image
                }
            }
            const response = yield call(action.payload.id ? (action.payload.category === "MQTT" && !action.payload.isExternal) ? axios.patch : axios.put : axios.post, url, (action.payload.id && action.payload.category === "MQTT" && !action.payload.isExternal) ? payload : action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.message })
            break;
        }
        case "submitRequestFaq": {
            action.payload.id ? url += `/${action.payload.id}` : null;
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(action.payload.id ? axios.put : axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.message })
            break;
        }
        case 'changeETLState': {
            url = url + action.state + "&name=" + action.etlName + "&type=" + action.etlType + "&tenantName=" + localStorage.tenant
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: { name: action.etlName, state: action.state, data: response.data.data, etlId: action.etlId } });
            break;
        }
        case 'getDataSource': {
            url += `/${action.id}`;
            let payload = {
                max: 10,
                offset: 0
            };
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.post, url, payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        case 'deleteRow': {
            url = url + action.deviceType + "/" + action.id
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.delete, url, GetHeaders());
            yield put({ type: responseConst, response: action.id })
            break;
        }
        case 'getAllSparksMethod': {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        case 'getSourceJson': {
            url = url + `/${action.id}`;
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        case 'saveFaaS': {
            if (action.id)
                url = url + "/" + action.id
            // if (action.commitMessage && !action.payload.developmentMode)
            //     url = url + `?pushToGit=true&commitMessage=${action.commitMessage}`
            // else url += "?pushToGit=false"
            // isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(action.id ? axios.put : axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: { message: response.data.message, data: response.data.data } })
            break;
        }
        case 'getFaaSById': {
            if (action.id) url = url + "/" + action.id
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data })
            break;
        }
        case 'getFaaSList': {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data })
            break;
        }
        case "deleteFaq": {
            url = url + `/${action.id}`;
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.delete, url, GetHeaders());
            yield put({ type: responseConst, response: { action: action, message: response.data.message } });
            break;
        }
        case 'deleteFaas': {
            url = url + `/${action.id}`;
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.delete, url, GetHeaders());
            yield put({ type: responseConst, response: { id: action.id, message: response.data.message } });
            break;
        }
        case 'getApiGatewayUntouchedList': {
            url = url;
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }

        case 'saveEtlFunction': {
            action.payload.id ? url += `${action.payload.id}` : null;
            if (action.commitMessage)
                url = url + `?pushToGit=true&commitMessage=${action.commitMessage}`
            else url += "?pushToGit=false"
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(action.payload.id ? axios.put : axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.message })
            break;
        }

        case 'saveExperiment': {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.put, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.message })
            break;
        }

        case 'createApiGateway': {
            if (action.id) url += `/${action.id}`;
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(action.id ? axios.put : axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.status === 400 ? response.data.data : response.data.message });
            break;
        }
        case 'getEtlFunctionsUrl': {
            url = url;
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data })
            break;
        }
        case 'deleteJsFunction':
        case 'deleteCustomMenu': {
            url = url + `/${action.id}`;
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.delete, url, GetHeaders());
            yield put({ type: responseConst, response: { id: action.id, message: response.data.message } });
            break;
        }
        case 'getApiList': {
            url = url;
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        case 'deleteApiById': {
            url = url + action.apiDetails.apiId //check
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.delete, url, GetHeaders());
            yield put({ type: responseConst, response: { response: action.apiDetails, message: response.data.message } })
            break;
        }
        case 'debugETL': {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            let payload = action.payload;
            const response = yield call(axios.post, url, payload, GetHeaders());
            payload.timestamp = new Date()
            yield put({ type: responseConst, response: payload });
            break;
        }

        case "invokeApi": {
            let url = !action.uri.startsWith("http") ? window.API_URL + action.uri : action.uri;
            let response
            if (action.method.toUpperCase() === "POST" || action.method.toUpperCase() === "PATCH" || action.method.toUpperCase() === "PUT")
                response = yield call(axios[action.method.toLowerCase()], url, action.payload, GetHeaders());
            else
                response = yield call(axios[action.method.toLowerCase()], url, GetHeaders());
            yield put({ type: responseConst, response: response.data, moduleName: action.moduleName, apiId: action.apiId, index: action.index });
            break;
        }

        case 'getConnectionString':
        case 'getAllGatewayApis': {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        case 'saveApplication': {
            if (action.id) {
                url = url + "/" + action.id
            }
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(action.id ? axios.put : axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.message });
            break;
        }

        case 'getApplications': {
            if (action.id) url = url + `/${action.id}`
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        case 'deleteApplication': {
            url = url + `/${action.id}`
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.delete, url, GetHeaders());
            yield put({ type: responseConst, response: action.id });
            break;
        }

        case 'getExcelFileDataUrl': {
            // url = url + "?fileType=" + action.fileType   --- code removed for csv pending 
            if (action.fileType === "EXCEL") {
                url = baseUrl + "api/v3/connectors/excel"
            }
            let formData = new FormData()
            formData.append('file', action.fileUpload)
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.post, url, formData, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }

        case 'getExcelFileData': {
            const response = yield call(axios.get, action.url, { "responseType": "arraybuffer" });
            yield put({ type: responseConst, response: response.data })
            break;
        }

        case 'clonePage': {
            url = `${url}/${action.id}?clone=true`
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.post, url, {}, GetHeaders());
            yield put({ type: responseConst, response: { message: response.data.message, id: action.id } });
            break;
        }

        case 'cloneAppApi':
        case 'cloneSimulation': {
            url = `${url}/${action.id}`
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.post, url, {}, GetHeaders());
            yield put({ type: responseConst, response: { message: response.data.message, id: action.id } });
            break;
        }
        case 'multipleDeleteApi': {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.delete, url, { headers: GetHeaders().headers, data: action.payload });
            yield put({ type: responseConst, response: { unDeletedApi: response.data.data, apiIndexList: action.payload.ids, message: response.data.message, moduleName: action.moduleName } });
            break;
        }

        case "invokeFaasFunction": {
            url = url + `/${action.actionId}`
            const response = yield call(axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.data ? response.data.data : "Request format and response format do not match." });
            break;
        }

        //  GetById call
        case "exportApplication":
        case "getRoleById":
        case 'getApiById':
        case "getTenantDetailsById":
        case 'getEtlFunctionById':
        case "getScheduleById":
        case "getSimulationById":
        case "getCustomMenuById":
        case "getByIdFaq":
        case "getExperimentById": {
            url = url + `/${action.id}`
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        //  GetById call

        //  Get call Without any params
        case "getAllProjectsForRegistration":
        case "getAllFaq":
        case "getFlowManager":
        case "getFlowManagerHistory":
        case "getParentPages":
        case "getFaasImages":
        case "getDataConnectors":
        case 'getBucket':
        case "fetchLicenseDetails":
        case 'getAllTemplates':
        case 'getJiraOauthStatus':
        case 'getJiraUsers':
        case "getAllPolicyData":
        case 'getAllDatasets':
        case 'getAllGroups':
        case 'getListOfConnectorsWithGroups':
        case 'getAllReports':
        case "getAccountSettings":
        case "getApplicationPublicStatus":
        case "getAllSimulationTopics":
        case "getAllSchedule":
        case "getExperimentHistory":
        case "getAllFaaSForScheduler":
        case "getAuditOperations":
        case "deviceTypeList":
        case "getAllConnectorsAndGroups":
        case "getAddOns":
        case "getAllCustomMenus":
        case "getAllDeviceType":
        case "getAllDeviceTypeWithTopics":
        case 'getAllSimulations': {
            if (action.interpreter) {
                url = `${url}?interpreter=${action.interpreter}`
            }
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        //  Get call Without any URL changes
        case "getAllTags": {
            url = `${url}${action.connectorId}/groups/tag`
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        case "getMonitorData": {
            url = url + `/${action.etlName}?timestamp=60`
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data })
            break;
        }

        case "cloneFaaSFunction": {
            url = url + `/${action.id}`
            const response = yield call(axios.post, url, {}, GetHeaders());
            yield put({ type: responseConst, response: response.data.message, id: action.id });
            break;
        }

        case "cloneApi": {
            url = url + `/${action.apiId}`
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.post, url, {}, GetHeaders());
            yield put({
                type: responseConst, response: {
                    response: response.data.data,
                    apiId: action.apiId,
                    moduleName: action.moduleName,
                    message: response.data.message
                }
            });
            break;
        }
        case "getAuditLogs":
        case "saveCustomMenu":
        case "createJiraProject":
        case "saveRole": {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data });
            break;
        }
        case "forgotPassword": {
            const response = yield call(axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.message });
            break;
        }
        case "getMyActivities":
        case 'getColumnsByConnector': {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        case 'createFlowManager': {
            const response = yield call(axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        case 'startStopFlowManager': {
            url += action.state
            const response = yield call(axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: { message: response.data.message, state: action.state } });
            break;
        }
        case "importWidget":
        case "importApplication": {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.post, url, action.payload, GetHeadersWithoutContentType());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        case "getAccountsActivityLineChartData":
        case "getAccountsActivityBarChartData": {
            url = `${url}?durationInDays=${action.durationInDays}`
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }

        case "getTenantActivityBarChartData": {
            url = `${url}/${action.id}/activity/graph/bar?durationInDays=${action.durationInDays}`
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        case "getTenantActivityLineChartData": {
            url = `${url}/${action.id}/activity/graph/line?durationInDays=${action.durationInDays}`
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        case 'createPage': {
            if (action.payload.id)
                url += `/${action.payload.id}`;
            const response = yield call(action.payload.id ? axios.put : axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.message });
            break;
        };
        case 'deleteWidgetById': {
            url = url + `/${action.id}`;
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.delete, url, GetHeaders());
            yield put({ type: responseConst, response: { id: action.id, message: response.data.message } });
            break;
        }
        case 'getAllWidgets': {
            if (action.widgetType) url += `?category=${action.widgetType}`;
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        case 'cloneWidget': {
            url = `${url}/${action.id}`
            const response = yield call(axios.post, url, {}, GetHeaders());
            yield put({ type: responseConst, response: { message: response.data.message, id: action.id } });
            break;
        }
        case 'updateRole': {
            url = url + `/${action.payload.id}`
            const response = yield call(axios.put, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data })
            break;
        }
        case 'startStopSimulation': {
            url = url + `/${action.id}/${action.state}`
            const response = yield call(axios.post, url, {}, GetHeaders());
            yield put({
                type: responseConst, response: {
                    response: response.data.message,
                    id: action.id,
                    state: action.state
                }
            })
            break;
        }
        case "saveSimulation": {
            if (action.payload.ID) {
                url = url + `/${action.payload.ID}`
            }
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(action.payload.ID ? axios.put : axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data });
            break;
        }
        case "saveLambdaFunctionSimulation": {
            if (action.payload.actionId) {
                url = url + `/${action.payload.actionId}`
            }
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(action.payload.actionId ? axios.put : axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data });
            break;
        }
        case 'deleteSimulation': {
            url = url + `/${action.id}`
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.delete, url, GetHeaders());
            yield put({ type: responseConst, response: { message: response.data.message, id: action.id } });
            break;
        }

        case 'updateLicenseData': {
            url = url + "/" + action.id
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.patch, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.message });
            break;
        }

        case 'deletePolicyItem': {
            url = url + `/${action.id}`;
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.delete, url, GetHeaders());
            yield put({ type: responseConst, response: { id: action.id, message: response.data.message } });
            break;
        }
        case 'getAllLicenses': {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }

        case 'testBootstrapConnection': {
            if (action.category) { url = url + action.category }
            const response = yield call(axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.data, index: action.index })
            break;
        }

        case 'generateSchema': {
            if (action.category) { url = url + action.category }
            const response = yield call(axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: { data: response.data, id: action.id } })
            break;
        }

        case 'getUserDetails': {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }

        case 'clearDataCollection': {
            url = url + `/${action.name}/documents`
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.delete, url, GetHeaders());
            yield put({ type: responseConst, name: action.name, message: response.data.message, collectionType: action.collectionType })
            break;
        }
        case 'addDataCollection': {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.post, url, action.payload, GetHeaders());
            yield put({
                type: responseConst,
                response: response.data
            });
            break;
        }
        case 'addMongoDocument': {
            const response = yield call(axios.post, url, action.payload, GetHeaders());
            yield put({
                type: responseConst,
                response: response.data
            });
            break;
        }
        case 'createTimeScaleDBTable': {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.post, url, action.payload, GetHeaders());
            yield put({
                type: responseConst,
                response: response.data
            });
            break;
        }
        case 'exportWidget': {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data });
            break;
        }
        case 'deleteDataCollection': {
            url = url + `${action.name}`;
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.delete, url, GetHeaders());
            yield put({ type: responseConst, response: { id: action.name, message: response.data.message, collectionType: action.collectionType } });
            break;
        }
        case 'deleteTimescaleDataCollection': {
            url = url + `${action.name}`;
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.delete, url, GetHeaders());
            yield put({ type: responseConst, response: { id: action.name, message: response.data.message } });
            break;
        }

        case 'getTenantStats': {
            if (action.id) {
                url = `${url}/${action.id}`;
            }
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data })
            break;
        }

        case 'getDataCollectionsById': {
            url = url + `${action.id}?max=${action.max}&offSet=${action.offset}`
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data, id: action.id })
            break;
        }
        case "getCategoryList": {
            // const response = yield call(axios.get, url, GetHeaders());
            yield put({
                type: responseConst, response: [
                    { displayName: "Line", value: "lineChart", isDisabled: false },
                    { displayName: "Gauge", value: "gaugeChart", isDisabled: false },
                    { displayName: "Bar", value: "barChart", isDisabled: false },
                    { displayName: "Pie", value: "pieChart", isDisabled: false },
                    { displayName: "Table", value: "table", isDisabled: false },
                    { displayName: "Maps", value: "map", isDisabled: false },
                    { displayName: "Counter", value: "counter", isDisabled: false },
                    { displayName: "Other", value: "other", isDisabled: false },
                    { displayName: "Custom Widgets", value: "customWidgets", isDisabled: false },
                    { displayName: "Utilities", value: "utilities", isDisabled: false },
                    { displayName: "Progress Bar", value: "progressBar", isDisabled: false },
                ]
            });
            break;
        }
        case "getDataCollection": {
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        case 'changeApplicationStatus': {
            let isPublished = action.isPublished ? false : true;
            url = `${url}${action.id}?isPublished=${isPublished}`;
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.put, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.message });
            break;
        }
        case "logout": {
            url = url;
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.delete, url, GetHeaders());
            yield put({ type: responseConst, response: response });
            break;
        }
        case 'changePolicyStatus': {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            url += `${action.id}?active=${!action.status}`
            const response = yield call(axios.patch, url, {}, GetHeaders());
            yield put({ type: responseConst, response: { id: action.id, message: response.data.message } });
            break;
        }
        case 'getPods': {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data });
            break;
        }
        case 'getPodLogs': {
            url = url + action.pod + "/logs?duration=" + action.duration + "&lines=" + action.lines
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        case "getApplicationsByUser": {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        case "getAllNavsForSelectedRole": {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.post, url, action.roleIds, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        case "getAPIKey": {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }

        case "getAllMlExperiments": {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        case "deleteMlExperiment": {
            url = url + action.id
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.delete, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.message });
            break;
        }
        case "trainMlExperiment":
        case "getStatisticsMlExperiment": {
            const response = yield call(axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.message, id: action.payload.experimentId });
            break;
        }
        case "getGroupList": {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        case "saveGroup": {
            if (action.id) {
                url = url + '/' + action.id
            }
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(action.id ? axios.patch : axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data });
            break;
        }
        case 'deleteGroup': {
            url = url + action.id
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.delete, url, GetHeaders());
            yield put({ type: responseConst, response: { id: action.id, message: response.data.message } })
            break;
        }
        case "fetchProjects": {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            let headers = GetHeaders();
            delete headers.headers["x-project-id"];
            const response = yield call(axios.get, url, headers);
            yield put({ type: responseConst, response: response.data.data });
            break;
        }

        case "deleteProject": {
            url = url + action.id
            action.deleteRepo ? url += `?deleteRepo=true` : '';
            isLoading ? yield put({ type: 'show_loader' }) : null;
            let headers = GetHeaders();
            delete headers.headers["x-project-id"];
            const response = yield call(axios.delete, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.message, id: action.id });
            yield put({ type: 'app/HomePage/DELETE_PROJECT_SUCCESS', id: action.id })
            break;
        }

        case "addOrEditProject": {
            if (action.id) {
                url = url + "/" + action.id
            }
            isLoading ? yield put({ type: 'show_loader' }) : null;
            let headers = GetHeaders();
            delete headers.headers["x-project-id"];
            const response = yield call(action.id ? axios.put : axios.post, url, action.payload, headers);
            yield put({ type: responseConst, response: response.data.message, id: action.id });
            break;
        }
        //for github connection check
        case "oAuthValidity": {
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        case "addOAuthProvider": {
            if (action.payload.id) {
                url = url + "/" + action.payload.id
            }
            const response = yield call(action.payload.id ? axios.put : axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.message, id: action.payload.id });
            break;
        }

        case "getOAuthConfigurations": {
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }

        case "getOAuthConfigurationDetails": {
            url = url + "/" + action.id
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }

        case "deleteOAuthProvider": {
            url = url + "/" + action.id
            const response = yield call(axios.delete, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.message, id: action.id });
            break;
        }
        case 'getTenantDetails': {
            const response = yield call(axios.get, url);
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        case 'oauthLogoutRequest': {
            url = url + "?name=" + action.name
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.delete, url, GetHeaders());
            yield put({ type: responseConst, response: { response: response.data.data, name: action.name } });
            break;
        }
        case "getOAuthProvidersForSuperAdmin":
        case 'getOAuthProviders': {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        case 'getCommitHistory': {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            if (action.usedFor === 'faas')
                url = `${url}?path=/${action.usedFor}/${action.category}/${action.fileName}.${action.fileExt}&offset=${action.offset}&max=10`;
            else if (action.usedFor === 'jsFunctions')
                url = `${url}?path=/${action.usedFor}/${action.fileName}.${action.fileExt}&offset=1&max=10`;
            else
                url = `${url}?path=/${action.usedFor}/${action.category}/${action.fileName}/index.${action.fileExt}&offset=${action.offset}&max=10`
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data })
            break;
        }
        case 'searchUsers': {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            url = `${url}?max=${action.max}&offset=${action.offset}`;
            const response = yield call(axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.data })
            break;
        }
        case 'deleteLambdaFunctionSimulation': {
            const response = yield call(axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.message });
            break;
        }
        case "getInvitationStatus": {
            const response = yield call(axios.get, url + action.id, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        case 'getAllAttributes': {
            url += `/?name=${action.selectedDataSet}`
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        case 'saveReport': {
            if (action.payload.id)
                url += `/${action.payload.id}`
            const response = yield call(action.payload.id ? axios.put : axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.message });
            break;
        }
        case 'searchByIdOrNameDeviceType': {
            url += `${action.deviceTypeId}/devices/search`
            const response = yield call(axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        case "deleteReport": {
            url = url + "/" + action.id
            const response = yield call(axios.delete, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.message, id: action.id });
            break;
        }
        case "getReportById": {
            url = url + "/" + action.id
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data, id: action.id });
            break;
        }
        case "generateReport": {
            url += action.id;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: { url: response.data.data.s3path, id: action.id } });
            break;
        }
        case 'getContributers': {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        case 'getCodeFrequency': {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        case 'fetchTicketLogs': {
            url = window.API_URL + apis.getTicketComments + action.id + '/activityLog?jiraEnabled=' + localStorage.isJiraLoggedIn
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data })
            break;
        }
        case 'getParticipation': {
            url = url + '?numOfWeeks=' + action.numOfWeeks;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data })
            break;
        }
        case 'contributerList': {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data })
            break;
        }
        case 'contributerActivities': {
            url = url + '?contributorName=' + action.contributorName + '&numOfWeeks=' + action.numOfWeeks;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data })
            break;
        }
        case 'getQueryDataCollection': {
            const response = yield call(axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data });
            break;
        }
        case "debugLogs": {
            url = url + "/" + action.activationId
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        case 'timeScaleQuery': {
            const response = yield call(axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data });
            break;
        }
        case 'saveTemplate': {
            if (action.payload.id)
                url += `/${action.payload.id}`
            const response = yield call(action.payload.id ? axios.patch : axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.message });
            break;
        }
        case "deleteTemplate": {
            url = url + "/" + action.id
            const response = yield call(axios.delete, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.message, id: action.id, templateType: action.templateType });
            break;
        }
        case "getTemplateById": {
            url = url + "/" + action.id
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data, id: action.id });
            break;
        }
        case "changeSchedulerState": {
            url = url + "/" + action.id + "?action=" + action.action
            const response = yield call(axios.post, url, action, GetHeaders());
            yield put({
                type: responseConst, response: {
                    response: response.data.message,
                    id: action.id,
                    state: action.action
                }
            });
            break;
        }
        case "createSimulationForDemo":
        case "startSimulationForDemo":
        case "createConnectorForDemo":
        case "createTransformationForDemo":
        case "startTransformationForDemo":
        case "createFaasForDemo":
        case "createApiForDemo":
        case "createWidgetForDemo":
        case "createPageForDemo":
        case "createAppForDemo":
        case "publishAppForDemo": {
            const response = yield call(axios.post, url, {}, GetHeaders());
            yield put({ type: responseConst, response: response.data.message });
            break;
        }
        case 'deleteDemoTour': {
            const response = yield call(axios.delete, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        case 'deleteFlowManager': {
            const response = yield call(axios.delete, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.message });
            break;
        }
        case 'getDemoTourStatus': {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        case 'getAllAlarms': {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        case 'updateAlarmState': {
            url = url + action.id
            const response = yield call(axios.put, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.data, id: action.id, newStatus: action.payload.state });
            break;
        }
        case 'uploadFile': {
            url += `?fileType=${action.payload.fileType}`
            let formData = new FormData()
            formData.append('file', action.payload.file)
            const response = yield call(axios.post, url, formData, GetHeaders("formData"));
            yield put({ type: responseConst, response: { message: response.data.message, secureUrl: response.data.data.secure_url, addOns: action.addOns } });
            break;
        }
        case 'getAllImages': {
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        case 'deleteFile': {
            url += `?file=${action.fileUrl}`
            const response = yield call(axios.delete, url, GetHeaders());
            yield put({ type: responseConst, response: { message: response.data.data, fileUrl: action.fileUrl } });
            break;
        }
        case 'getAllProperties': {
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        case 'createOrUpdateConsul': {
            const response = yield call(axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.data, key: action.payload.property.name });
            break;
        }
        case 'getNotificationList': {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        case 'updateNotificationList': {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.put, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.message });
            break;
        }
        case 'createDevice': {
            action.payload.id ? url += `?id=${action.payload.id}` : null;
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(action.payload.id ? axios.put : axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.data })
            break;
        }
        case 'generateClientIds': {
            url += `${action.connectorId}?devices=${action.noOfDevices}`
            const response = yield call(axios.post, url, { deviceIds: action.deviceIds }, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        case 'getCollectionDetails': {
            url += `?collectionName=${action.collectionName}`
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        case 'getDockerImage': {
            url = url + `?interpreter=${action.interpreter}`
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        case 'deleteDockerImage': {
            url += `/${action.id}`
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.delete, url, GetHeaders());
            yield put({ type: responseConst, response: response.data });
            break;
        }
        case 'createOrEditDockerImage': {
            url += action.id ? `/${action.id}` : '';
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(action.id ? axios.put : axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data });
            break;
        }

        case 'getDependencyList': {
            url += `?name=${action.term}&interpreter=${action.interpreter}`
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }

        case 'getDurationList': {
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data, attribute: action.attribute });
            break;
        }
        case 'getConnectorStatus': {
            url += `?id=${action.id}`;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data, attribute: action.attribute });
            break;
        }
        case 'createFastApp': {
            if (action.id) {
                url += `/${action.id}`;
            }
            const response = yield call(action.id ? axios.put : axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data });
            break;
        }

        case 'getCdnList': {
            let cdnUrl = `https://api.cdnjs.com/libraries?search=${action.term}`
            const response = yield call(axios.get, cdnUrl, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.results });
            break;
        }

        case 'editDevice': {
            url += action.deviceTypeId + "/device/" + action.deviceId
            const response = yield call(axios.put, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.message, deviceId: action.deviceId, deviceDetails: action.payload });
            break;
        }
        case 'refreshSchema': {
            url += `?connector=${action.id}`;
            let headers = GetHeaders();
            headers.headers["x-user-consent"] = action.userConsentToken;
            const response = yield call(axios.post, url, {}, headers);
            yield put({ type: responseConst, response: response.data });
            break;
        }
        case 'verifySchema': {
            url += `/${action.id}`
            const response = yield call(axios.post, url, {}, GetHeaders());
            yield put({ type: responseConst, response: response.data });
            break;
        }
        case 'resetDeviceAttributes': {
            url += `${action.id}`
            const response = yield call(axios.post, url, {}, GetHeaders());
            yield put({ type: responseConst, response: { message: response.data.message, timestamp: Date.now() } });
            break;
        }
        case 'deleteDevice': {
            url += `${action.connectorId}/device/${action.deviceId}`
            const response = yield call(axios.delete, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data, deviceId: action.deviceId });
            break;
        }
        case 'getProfileDetails': {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data })
            break;
        }
        case "getTenantTransactionDetails":
        case 'getTenantDashboardDetails': {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data })
            break;
        }
        case 'getSubscriptionDetails': {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data })
            break;
        }
        case 'getBillingDetails': {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data })
            break;
        }
        case 'getProfileStatsAnalysis': {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data })
            break;
        }
        case 'getAllAvailablePlans': {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data })
            break;
        }
        case "cancelSubscriptionTenant":
        case 'cancelSubscription': {
            url += `?reason=${action.reason}`
            const response = yield call(axios.delete, url, GetHeaders());
            yield put({ type: responseConst, response: response.data });
            break;
        }
        case 'upgradeSubscription': {
            if (action.isTrialPackage) {
                url += `/trial/upgrade/subscription?packageId=${action.packageId}`
            } else {
                url += `/upgrade/subscription?packageId=${action.packageId}`
            }
            const response = yield call(axios.post, url, {}, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        case 'renewSubscription': {
            url += `?packageId=${action.packageId}`;
            const response = yield call(axios.post, url, {}, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        case 'emailCredentials': {
            url += `/${action.connectorId}`
            const response = yield call(axios.post, url, {}, GetHeaders());
            yield put({ type: responseConst, response: response.data });
            break;
        }
        case 'getVMQCredentials': {
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data })
            break;
        }
        case 'saveCommands': {
            if (action.payload.id) {
                url = url + "/" + action.payload.id
            }
            const json = JSON.stringify(action.payload);
            const blob = new Blob([json], {
                type: 'application/json'
            });
            let formData = new FormData();
            formData.append('request', blob);
            if (action.commandImage) {
                formData.append('file', action.commandImage);
            }
            const response = yield call(action.payload.id ? axios.put : axios.post, url, formData, GetHeaders("formData"));
            yield put({ type: responseConst, response: { message: response.data.message, data: response.data.data, isAddMode: action.isAddMode } })
            break;
        }
        case 'shareResources': {
            url += `${action.resourceType}/${action.id}`;
            const response = yield call(axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.message })
            break;
        }
        case 'getDeviceGroups': {
            url += `${action.id}`
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data, connectorId: action.id, invokedFrom: action.invokedFrom })
            break;
        }
        case 'addOrUpdateGroup': {
            if (action.payload.deviceGroupId) {
                url += `/${action.payload.deviceGroupId}`;
            }
            const response = yield call(action.payload.deviceGroupId ? axios.put : axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.message });
            break;
        }
        case 'deleteClusterGroup': {
            url += `${action.id}`;
            const response = yield call(axios.delete, url, GetHeaders());
            yield put({ type: responseConst, response: response.data });
            break;
        }
        case "getTagsForDevice": {
            url += action.groupId + "/connectors/" + action.connectorId;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        case "getETLMathFunctions": {
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        case "getAllProducts": {
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        case "getAllProductsUnAuth": {
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        case "fastAppToggleActions": {
            let payload = {
                id: action.id,
                type: action.key,
                value: action.value,
            }
            const response = yield call(axios.post, url, payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.message })
            break;
        }
        case "saveLicense": {
            const response = yield call(axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data })
            break;
        }
        case 'getAllDevices': {
            const response = yield call(axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }

        case 'getDeviceInfo': {
            url = url.replace("{connectorId}", action.connectorId).replace("{deviceId}", action.deviceId)
            const response = yield call(axios.get, url, GetHeaders());

            yield put({ type: responseConst, response: response.data.data })
            break;
        }

        case 'saveTag':
            if (action.payload.id) {
                url += `/${action.payload.id}`
            }
            {
                const response = yield call(action.payload.id ? axios.put : axios.post, url, action.payload, GetHeaders());
                yield put({ type: responseConst, response: response.data.message });
                break;
            }
        case 'getTagsByGroupId': {
            url = `${url}/${action.groupId}/tags`
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        case 'saveTagByGroupId': {
            url += `/${action.groupId}/tags`
            const response = yield call(axios.put, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.message });
            break;
        }
        case 'debugDeviceAttributes': {
            const response = yield call(axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }

        case "createOrUpdateEvent": {
            url = action.id ? `${url}/${action.id}` : url
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(action.id ? axios.put : axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data })
            break;
        }
        case 'getAttributeList': {
            url = `${url}/${action.id}`
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        case 'getEventList': {
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        case 'getOtp': {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.post, url, action.payload);
            yield put({ type: responseConst, response: { message: "OTP generated successfully", otpCount: response.data.data.otpCount, tenantName: action.payload.tenantName } })
            break;
        }

        case 'otpStatus': {
            isLoading ? yield put({ type: 'show_loader' }) : null;

            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data })
            break;
        }


        case 'deleteEvent': {
            url = url + `/${action.id}`;
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.delete, url, GetHeaders());
            yield put({ type: responseConst, response: { id: action.id, message: response.data.message } });
            break;
        }

        case 'getEventById': {
            url = url + `/${action.id}`;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        case "createorEditAction": {
            url = action.payload.id ? `${url}/${action.payload.id}` : url
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(action.payload.id ? axios.put : axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data })
            break;
        }

        case 'getActionsList': {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data })
            break;
        }

        case 'deleteAction': {
            url = url + `/${action.id}`;
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.delete, url, GetHeaders());
            yield put({ type: responseConst, response: { id: action.id, message: response.data.message, actionType: action.actionType } });
            break;
        }

        case 'getTopicsFromConnectorID': {
            url = url + `/${action.id}/topic`;
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }

        case 'getDeviceTypeList': {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data })
            break;
        }

        case 'saveDeviceTopology': {
            url += action.connectorId
            const response = yield call(axios[action.isEdit ? "put" : "post"], url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.message })
            break;
        }

        case 'getDeviceTopology': {
            url += action.connectorId
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data })
            break;
        }

        case 'diagnosticDeviceList': {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data })
            break;
        }

        case 'isEvaluateEvent': {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: { data: response.data, id: action.payload.eventId } });
            break;
        }

        case 'getDeviceGroupsForMap': {
            const response = yield call(axios.get, url.replace("{connectorId}", action.connectorId), GetHeaders());
            yield put({ type: responseConst, response: response.data.data })
            break;
        }

        case 'getAllDevicesForMap': {
            const response = yield call(axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.data, deviceTypeIds: action.payload.deviceTypeIds })
            break;
        }

        case "paymentOfTenantAddOns":
        case 'getDeveloperQuota': {
            const response = yield call(axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }

        case 'getAllRpc': {
            url = `${url}/${action.id}/rpc`;
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }

        case 'getDeviceTypeById': {
            const response = yield call(axios.get, url.replace("{deviceTypeId}", action.id), GetHeaders());
            yield put({ type: responseConst, response: response.data.data })
            break;
        }

        case 'getCssThemes': {
            // if (localStorage.tenantType === "FLEX") {
            //     url = baseUrl + "api/unauth/themes"
            // }
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data })
            break;
        }

        case 'getAndUpdateAlarmConfigData': {
            url = `${url}/${action.id}/config`;
            isLoading ? yield put({ type: 'show_loader' }) : null;
            let response;
            if (action.payload) {
                response = yield call(axios.put, url, action.payload, GetHeaders());
            } else {
                response = yield call(axios.get, url, GetHeaders());
            }
            yield put({ type: responseConst, response: response.data });
            break;
        }


        case 'getDetailsPageData': {
            url += `${action.subKey}?connectorId=${action.connectorId}&uniqueId=${action.deviceId}`
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data })
            break;
        }

        case 'getSelectedTheme': {
            url = url + action.id
            // if (localStorage.tenantType === "FLEX") {
            //     url = `${baseUrl}api/unauth/${localStorage.tenant}/${action.id}/theme.css`
            // }
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.patch, url, {}, GetHeaders());
            yield put({ type: responseConst, response: action.id });
            break;
        }

        case 'eventList': {
            const response = yield call(axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.data })
            break;
        }

        case 'dashboardConfig': {
            url = url + `/${action.id}`;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data })
            break;
        }

        case 'getMonitoringchartData': {
            url = url + `/${action.chartType}`;
            let header = GetHeaders();
            header.params = action.payload
            const response = yield call(axios.get, url, header);
            yield put({ type: responseConst, response: { data: response.data.data, id: action.id, chartOf: action.chartType } })
            break;
        }

        case 'saveConfig': {
            url = url + `/${action.id}`;
            let payload = {
                widgets: action.payload
            }
            const response = yield call(axios.put, url, payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.data })
            break;
        }

        case 'getAttributes': {
            url = url + `/${action.id}/config`;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data })
            break;
        }

        case 'getAlarmFilterData': {
            const response = yield call(axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }

        case 'getGroupDataByConnector': {
            url = url + `?duration=${action.duration}`;
            const response = yield call(axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.data, payload: action.payload })
            break;
        }

        case 'resetAlarm': {
            url = url + `/${action.id}/config`;
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.delete, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }

        case "updateAlarmStatus": {
            url = url + `${action.payload.deviceTypeId}/${action.payload.deviceId}`;
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.put, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.message, alarmDetails: action.alarmDetails })
            break;
        }

        case 'getCommandHistory': {
            let payload = action.payload
            url = url + `?deviceTypeId=${payload.deviceTypeId}&duration=${payload.duration}&from=${payload.from}&to=${payload.to}&uniqueId=${payload.uniqueId}`
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }

        case "rpcCommandPublish": {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data })
            break;
        }

        case 'getDevicesFromGroupId': {
            const response = yield call(axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }

        case 'getMetricActions': {
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data })
            break;
        }

        case 'getAlarmsCount': {
            const response = yield call(axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }

        case 'getAlarmsPieChartData': {
            const response = yield call(axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }

        case 'getDailyAlarms': {
            const response = yield call(axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }

        case 'getAlarmsDistribution': {
            const response = yield call(axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }

        case 'forcastPredict': {
            const response = yield call(axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: { response: response.data.data, isHardRefresh: action.payload.isHardRefresh } });
            break;
        }

        case 'getForcastData': {
            url = `${url}/${action.payload.deviceTypeId}/attribute/${action.payload.attribute}/action/${action.payload.action}`
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: { data: response.data.data, attribute: action.payload.attribute, action: action.payload.action } })
            break;
        }

        case 'getDataConfig': {
            url = url + action.id
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data })
            break;
        }

        case 'saveOrUpdateForcastConfig': {
            const response = yield call(axios.put, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data });
            break;
        }

        case 'saveMapZoomLevel': {
            const response = yield call(axios.put, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.data, deviceTypeDetails: action.payload });
            break;
        }

        case 'getStatsData': {
            url = url + `/${action.id}/monitoring?duration=${action.duration}`
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }

        case 'getNotificationStatus': {
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }

        case 'changeNotificationStatus': {
            const response = yield call(axios.put, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data });
            break;
        }

        case 'getAllActions': {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.data })
            break;
        }

        case 'validateTenantName': {
            url = url + action.tenantName
            const response = yield call(axios.get, url);
            yield put({ type: responseConst, response: response.data.data, tenantName: action.tenantName });
            break;
        }
        case 'getDeviceTypes': {
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }

        case 'getAllDevicesByDeviceTypes': {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        case 'getAllNoteBooks': {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data })
            break;
        }

        case 'createNoteBook': {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data });
            break;
        }

        case 'deleteNoteBook': {
            url += `/${action.notebookId}`
            const response = yield call(axios.delete, url, GetHeaders());
            yield put({ type: responseConst, response: response.data });
            break;
        }

        case 'updateNoteBook': {
            url += `/${action.payload.id}`
            let payload = action.payload;
            delete payload.id;
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(payload.description ? axios.patch : axios.put, url, payload, GetHeaders());
            yield put({ type: responseConst, response: response.data });
            break;
        }

        case 'debugNoteBook': {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.put, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data });
            break;
        }

        case 'stopNoteBookCodeBlock': {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.put, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data });
            break;
        }

        case 'getNoteBookById': {
            url += `/${action.notebookId}`
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data })
            break;
        }

        case 'renewSubscriptionFlex': {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }

        case 'purchaseSubscriptionFlex': {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }


        case 'saveAddOns': {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            url = action.payload.id ? url + `/${action.payload.id}` : url;
            const response = yield call(action.payload.id ? axios.put : axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.message })
            break;
        }

        case 'getAllAddOns': {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data })
            break;
        }

        case 'deleteAddOns': {
            url += `/${action.id}`
            const response = yield call(axios.delete, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.message, deletedAddonId: action.id });
            break;
        }

        case 'deletePage': {
            url = url + `/${action.id}`;
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.delete, url, GetHeaders());
            yield put({ type: responseConst, response: { id: action.id, message: response.data.message } });
            break;
        }

        case 'getSparkFunctions': {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data })
            break;
        }

        case 'getAllAccounts': {
            url = `${url}?email=${jwt_decode(localStorage.token).sub}`;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data })
            break;
        }
        case 'saveETL': {
            url = action.payload.id ? url + `?id=${action.payload.id}` : url;
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(action.payload.id ? axios.put : axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.message });
            break;
        }
        case 'getLineMonitoringchartData': {
            const response = yield call(axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: { data: response.data.data, id: action.id, dataof: action.payload.operation ? "operationData" : "data" } })
            break;
        }

        case "executeFunctions": {
            const response = yield call(axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.data })
            break;
        }

        case "timeScaleDataCollection": {
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }

        case "timeScaleDataByTableName": {
            url += `/${action.table}?max=${action.max}&offset=${action.offset}`;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data, tableName: action.table });
            break;
        }

        case "getLibrary": {
            url = url + "/" + action.langType;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data })
            break;
        }

        case 'clearTSTableData': {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.delete, url, { ...GetHeaders(), data: action.payload });
            yield put({ type: responseConst, message: response.data.message, tableName: action.payload.tableName })
            break;
        }

        case "addTimescaleDocuments": {
            const response = yield call(axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.message, tableName: action.payload.tableName });
            break;
        }

        case 'getBucketImages': {
            url = url + action.bucketName;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        case 'resendInvite': {
            url = url + action.id;
            const response = yield call(axios.post, url, {}, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }

        case 'createTopicSchema': {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.put, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data });
            break;
        }

        case 'describeTimeScaleTable': {
            url += `/${action.payload}`;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }

        case 'alterTimeScaleTable': {
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.put, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data });
            break;
        }

        case 'clearTopicSchema': {
            url = url.replace("{connectorId}", action.connectorId).replace("{topicId}", action.topicId)
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.delete, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.message });
            break;
        }
        case "getReportHistory": {
            url = url.replace("{reportId}", action.id)
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data })
            break;
        }

        case 'saveMiscTopic': {
            const response = yield call(axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data });
            break;
        }

        case "getTopics": {
            url = url.replace("{deviceTypeId}", action.deviceTypeId).replace("{id}", action.id)
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data })
        }

        case 'uploadProfilePicture': {
            let formData = new FormData()
            formData.append('file', action.payload.file)
            const response = yield call(axios.post, url, formData, GetHeaders("formData"));
            yield put({ type: 'updateUserDisplayPicture', imageUrl: response.data.data })
            yield put({ type: responseConst, response: { message: response.data.message, secureUrl: response.data.data.secure_url, addOns: action.addOns } });
            break;
        }
        case 'promoteAsOwner': {
            url += `${action.id}`;
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.put, url, {}, GetHeaders());
            yield put({ type: responseConst, response: response.data });
            break;
        }
        case 'getMQTTCredentials': {
            url = `${url}?deviceTypeId=${action.deviceTypeId}`;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }

        case 'restartNoteBookCodeBlock': {
            const response = yield call(axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: { message: response.data.message, codeBlockId: action.payload.codeBlockId } })
            break;
        }

        case 'clearCheckpoint': {
            url = url + action.etlName;
            const response = yield call(axios.post, url, null, GetHeaders());
            yield put({ type: responseConst, response: { message: response.data.message, etlName: action.etlName } });
            break;
        }

        case 'deleteControlImage': {
            url += action.controlCommandId;
            const response = yield call(axios.delete, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.message });
            break;
        }

        case 'getGoogleMapKey': {
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.data });
            break;
        }
        
        case 'monitorNotebook': {
            url = url + `/${action.payload.notebookId}/monitoring/${action.payload.codeBlockId}?duration=${action.payload.duration}`
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data });
            break;
        }

        case 'createEtlScheduler': {
            const response = yield call(axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.message });
            break;
        }

        case 'getAllEtl': {
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data });
            break;
        }

        case 'updateEtlScheduler': {
            url += `${action.id}`;
            isLoading ? yield put({ type: 'show_loader' }) : null;
            const response = yield call(axios.put, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data });
            break;
        }

        case 'deleteEtlScheduler': {
            url += `${action.id}`;
            const response = yield call(axios.delete, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.message });
            break;
        }

        case 'getAllEtlScheduler': {
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data });
            break;
        }

        case 'getEtlSchedulerById': {
            url += `${action.id}`;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data });
            break;
        }

        case 'etlSchedulerAction': {
            url = url + `${action.name}?action=${action.status}`
            action.payload = {}
            const response = yield call(axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.message, name: action.name, status: action.status });
            break;
        }

        case 'getAllDLSentiments': {
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data });
            break;
        }

        case 'createDLSentiment': {
            const response = yield call(axios.post, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data.message });
            break;
        }

        case 'deleteDLSentiment': {
            url += `/${action.id}`;
            const response = yield call(axios.delete, url, GetHeaders());
            yield put({ type: responseConst, response: response.data.message });
            break;
        }

        case 'getAllDLSentimentById': {
            url += `/${action.id}`;
            const response = yield call(axios.get, url, GetHeaders());
            yield put({ type: responseConst, response: response.data });
            break;
        }
        case 'updateDLSentiment': {
            url += `/${action.id}`;
            const response = yield call(axios.put, url, action.payload, GetHeaders());
            yield put({ type: responseConst, response: response.data });
            break;
        }

        default:
            break;
    }
}