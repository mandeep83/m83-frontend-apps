import * as MQTT from "mqtt";
import { getVMQCredentials } from "./commonUtils";
export let client;
let subscribedTopics = []

export const getMqttConnection = (onClientConnect, credentials) => {
    let count = 0,
        options = {};

    if (credentials) {
        options = {
            host: `mqtt.${window.API_URL.split('.')[1]}.com`,
            protocol: 'wss',
            path: '/mqtt',
            port: 9090,
            clientId: credentials.clientId,
            username: credentials.userName,
            password: credentials.password,
        }
    } else {
        options = {
            host: window.location.origin === "http://localhost:3001" ? window.API_URL.split("//")[1].substring(0, window.API_URL.split("//")[1].length - 1) : window.location.host,
            protocol: 'wss',
            path: '/connectmqtt',
            username: getVMQCredentials().userName,
            password: getVMQCredentials().password,
        }
    }
    client = MQTT.connect(options);
    client.on('connect', function () {
        onClientConnect && onClientConnect()
    });

    client.on('close', function (response) {
        if (response && count > 4) {
            client.end();
        }
    });
    client.on('reconnect', function () {
        count++;
    });
    
}

export const subscribeTopic = (topic, mqttMessageHandler, onClientConnect, credentials) => {
    getMqttConnection(onClientConnect, credentials);
    client.subscribe(topic, function (err) {
        if (!err) {
            client.on('message', function (topic, message) {
                message = JSON.parse(message.toString());
                mqttMessageHandler(message, topic)
            })
        }
    })
    subscribedTopics.push(topic)
}

export const unsubscribeTopic = (topic, doNotCloseConnection) => {
    client && client.unsubscribe(topic);
    let unsubscribedTopicIndex = subscribedTopics.indexOf(topic)
    subscribedTopics.splice(unsubscribedTopicIndex, 1)
    if (!doNotCloseConnection && !subscribedTopics.length) {
        closeMqttConnection()
    }
}

export const closeMqttConnection = (topic) => {
    client && client.end()
    subscribedTopics = []
    client = null
}