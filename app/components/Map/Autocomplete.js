import React, { Component } from 'react';
import Autocomplete from 'react-google-autocomplete';

export default class Autocomplete1 extends Component {
    shouldComponentUpdate(){
        return false
    }

    render(){
        if(!this.render.count){
            this.render.count = 1
            return(
                <Autocomplete
                    className="form-control"
                    id="myAnchor"
                    onPlaceSelected={this.props.onPlaceSelected}
                    types={['(regions)']}
                    // onChange = {(e)=> {this.setState({tempAddress : e.target.value, isSelected: false})}}
                    // value={this.state.isSelected ? this.state.address : this.state.tempAddress}
                />
            )
        }
    }
}