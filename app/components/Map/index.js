/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import React, { Component } from 'react';
import { withGoogleMap, GoogleMap, withScriptjs, InfoWindow, Marker } from "react-google-maps";
import Autocomplete from 'react-google-autocomplete';
import isEqual from 'lodash/isEqual';
import { GOOGLE_MAP_KEY, GOOGLE_MAP_URL_PLACE } from "../../utils/constants";
import { getAddressFromLatLng } from "../../commonUtils"
import MapSearchBox from "../MapSearchBox"
class Map extends Component {
    state = {
        zoom: this.props.zoom || 5
    }

    shouldComponentUpdate(nextProps, nextState) {
        return !isEqual(nextProps.deviceDetails, this.props.deviceDetails)
    }
    mapClickHandler = ({ latLng }) => {
        const lat = latLng.lat();
        const lng = latLng.lng();
        getAddressFromLatLng(lat, lng, this.addressHandler)
    }

    mapRef = React.createRef();

    render() {
        let selectedLat = this.props.deviceDetails.lat || 53.3427371467871
        let selectedLng = this.props.deviceDetails.lng || -6.25911712396487
        const AsyncMap = withScriptjs(
            withGoogleMap(
                props => (
                    <GoogleMap google={this.props.google}
                        defaultZoom={this.state.zoom}
                        defaultCenter={{ lat: selectedLat, lng: selectedLng }}
                        onClick={this.mapClickHandler}
                        ref={this.mapRef}
                        onZoomChanged={() => this.setState({ zoom: this.mapRef.current.getZoom() })}
                    >
                        <div className="auto-complete-box form-group pd-l-10">
                            <label className="form-group-label">Location :</label>
                            <MapSearchBox onSelect={this.props.setLocation} value={this.props.deviceDetails.location} />
                        </div>

                        <Marker google={this.props.google}
                            name={'Dolores park'}
                            draggable={true}
                            onDragEnd={this.mapClickHandler}
                            position={{ lat: selectedLat, lng: selectedLng }}
                        />

                    </GoogleMap>
                )
            )
        );
        let map;
        if (this.props.deviceDetails.lat || 53.3427371467871 !== undefined) {
            map =
                <div>
                    <AsyncMap
                        googleMapURL={GOOGLE_MAP_URL_PLACE}
                        loadingElement={
                            <div style={{ height: 360 }} />
                        }
                        containerElement={
                            <div style={{ height: 360 }} />
                        }
                        mapElement={
                            <div style={{ height: 360 }} />
                        }
                    >
                    </AsyncMap>
                </div>
        } else {
            map = <div style={{ height: this.props.height }} />
        }
        return (map)
    }
}
export default Map
