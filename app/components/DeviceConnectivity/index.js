/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * DeviceConnectivity
 *
 */

import React from "react";
// import PropTypes from 'prop-types';
// import styled from 'styled-components';
import * as am4core from "@amcharts/amcharts4/core";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import * as am4charts from "@amcharts/amcharts4/charts";
import { cloneDeep, isEqual } from "lodash";

/* eslint-disable react/prefer-stateless-function */
class DeviceConnectivity extends React.Component {
    createConnectivityChart = (chartData, divId) => {
        am4core.ready(function () {
            am4core.addLicense("CH145187641");
            am4core.useTheme(am4themes_animated);
            var chart = am4core.create(divId, am4charts.XYChart);
            chart.colors.step = 2;
            chartData.forEach((temp) => {
                let total = temp.connected + temp.disconnected
                temp.connectedPercentage = ((temp.connected / total) * 100).toFixed(2)
                temp.disconnectedPercentage = ((temp.disconnected / total) * 100).toFixed(2)
                // temp.timestamp = new Date(temp.timestamp).toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' });
            })
            chart.data = chartData;
            
            var categoryAxis = chart.xAxes.push(new am4charts.DateAxis());
            categoryAxis.dataFields.category = "timestamp";
            categoryAxis.renderer.grid.template.location = 0;
            // categoryAxis.renderer.minGridDistance = 20;
            categoryAxis.startLocation = 0.5;
            categoryAxis.endLocation = 0.5;
            categoryAxis.paddingRight = 15;
            categoryAxis.fontSize = 12;
            categoryAxis.renderer.baseGrid.disabled = true;
            categoryAxis.renderer.grid.template.disabled = true;
            categoryAxis.renderer.line.strokeOpacity = 0.5;
            categoryAxis.renderer.line.strokeWidth = 0.6;
            categoryAxis.renderer.minGridDistance = 30;
            var label = categoryAxis.renderer.labels.template;
            label.wrap = true;
            label.maxWidth = 120;
            // categoryAxis.renderer.labels.template.adapter.add("dy", function(dy, target) {
            //   if (target.dataItem && target.dataItem.index & 2 == 2) {
            //     return dy + 25;
            //   }
            //   return dy;
            // });
            // categoryAxis.renderer.labels.template.margin = 90
            // categoryAxis.renderer.labels.template.wrap = true
            
            var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
            valueAxis.calculateTotals = true;
            valueAxis.min = 0;
            valueAxis.max = 100;
            valueAxis.fontSize = 12;
            valueAxis.renderer.baseGrid.disabled = true;
            valueAxis.renderer.grid.template.disabled = true;
            valueAxis.renderer.line.strokeOpacity = 0.5;
            valueAxis.renderer.line.strokeWidth = 0.6;
            valueAxis.renderer.labels.template.adapter.add("text", function (text) {
                return text+"%";
            });
            
            var series = chart.series.push(new am4charts.LineSeries());
            series.dataFields.valueY = "connectedPercentage";
            series.dataFields.dateX = "timestamp";
            series.name = "Connected";
            series.stacked = true;
            series.fill = am4core.color("#6BFB7C");
            series.stroke = am4core.color("#04BC1A");
            series.fillOpacity = 0.3;
            series.strokeWidth = 1;
            
            series.tooltipHTML = "<span' style=color:#666666; margin:0px; '>Connected Devices: <span style='color:#666666;'><b>{valueY.value}% ({connected})</b></span></span>";
            series.tooltip.fontSize = 13;
            series.tooltip.getFillFromObject = false;
            series.tooltip.background.fill = am4core.color("#fff");
            series.tooltip.getStrokeFromObject = true;
            series.tooltip.background.strokeWidth = 1;
            series.legendSettings.labelText = "Connected Devices";
            series.legendSettings.itemValueText = "[bold]{valueY}%[/bold] ({connected})";
            
            var series2 = chart.series.push(new am4charts.LineSeries());
            series2.dataFields.valueY = "disconnectedPercentage";
            series2.dataFields.dateX = "timestamp";
            series2.name = "Disconnected";
            series2.stacked = true;
            series2.fill = am4core.color("#3afcef");
            series2.stroke = am4core.color("#20b0a6");
            series2.fillOpacity = 0.3;
            series2.strokeWidth = 1;
            series2.fill = am4core.color("#F78C74");
            series2.stroke = am4core.color("#FB4922");
            
            series2.tooltipHTML = "<span' style=color:#666666; margin:0px;'>Disconnected Devices: <span style='color:#666666; '><b>{valueY.value}% ({disconnected})</b></span></span>";
            series2.tooltip.fontSize = 13;
            series2.tooltip.getFillFromObject = false;
            series2.tooltip.background.fill = am4core.color("#fff");
            series2.tooltip.getStrokeFromObject = true;
            series2.tooltip.background.strokeWidth = 1;
            series2.legendSettings.labelText = "Disconnected Devices";
            series2.legendSettings.itemValueText = "[bold]{valueY}%[/bold] ({disconnected})";
            chart.cursor = new am4charts.XYCursor();
            
            chart.legend = new am4charts.Legend();
            chart.legend.fontSize = 14;
            chart.legend.marginRight = 1400;
            chart.legend.labels.template.fill = am4core.color("#666666");
        });
    }
    
    shouldComponentUpdate(nextProps) {
        return !isEqual(nextProps.widgetData, this.props.widgetData)
    }
    
    componentDidMount() {
        this.createCharts()
    }
    
    componentDidUpdate() {
        this.createCharts()
    }
    
    createCharts = () => {
        this.props.widgetData.map((chart, index) => {
            this.createConnectivityChart(cloneDeep(chart.data), "connectivityChart" + index)
        })
    }
    
    render() {
        return (
            <React.Fragment>
                {this.props.widgetData.map((chart, index) =>
                    <div className="card">
                        <div className="card-header"><h6>{chart.header}</h6></div>
                        <div className="card-body">
                            <div id={"connectivityChart" + index} className="card-chart-box" />
                            {/*<div className="card-loader">*/}
                            {/*    <i className="fad fa-sync-alt fa-spin"></i>*/}
                            {/*</div>*/}
                            {/*<div className="card-message">*/}
                            {/*    <p>There is no data to display !</p>*/}
                            {/*</div>*/}
                        </div>
                    </div>
                )}
            </React.Fragment>
        );
    }
}

DeviceConnectivity.propTypes = {};

export default DeviceConnectivity;