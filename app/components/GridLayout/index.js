/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */


/// lookout for ...props
//make sure to pass ...props & state in <GridLayout {...props} state={state}/>
/**
 *
 * GridLayout
 *
 */

import React from "react";
import cloneDeep from 'lodash/cloneDeep';
import { WidthProvider, Responsive } from "react-grid-layout";
const ResponsiveReactGridLayout = WidthProvider(Responsive);
import ReactTooltip from "react-tooltip";
import Chart from "../../components/LineChart";
import MapWithMarkerCluster from "../../components/PduMapWithMarkerCluster";
import DeviceConnectivity from "../../components/DeviceConnectivity";
import EventHistory from "../../components/EventHistory";
import CountersWidget from '../../components/CountersWidget';
import AlarmsWidget from "../../components/AlarmWidget"
import ReactSelect from 'react-select';
import Loader from "../../components/Loader";
// import { setSelectedDay } from "../../commonUtils"
import WeatherWidget from "../../components/WeatherWidget/Loadable";
import GaugeChart from '../../containers/GaugeChart/Loadable';
import { getVMQCredentials, convertTimestampToDate } from "../../commonUtils";
import { getCategoryInResponse } from "../../containers/CreateOrEditPage/pageStudioUtils"
import * as MQTT from "mqtt";
import { startCase } from "lodash";
import DeviceMap from "../../containers/DeviceMap/Loadable";
let publishClient;
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

function GridLayout(props) {

    let { state, setState, onCloseHandler, onLayoutChange } = props

    let validateHeaderVisibility = (widgetType, subType, isNotForConfig) => {
        let widgetsWithoutHeader = ["images", "counter", "layout", "label", "header", "control", "insights"];
        // isNotForConfig && widgetsWithoutHeader.push("label", "header")
        let headerVisibility = widgetsWithoutHeader.every(widget => widget !== widgetType) && subType !== "custom";
        return headerVisibility;
    }


    let showDropdown = (widget) => {
        // let requiredWidget = state.widgetMetaData.find(widget => widget.widgetName === widget.widgetName)
        return (
            <div className="widget-button-group">
                <ul className="list-style-none d-flex">
                    {widget.showFullScreen && <li data-tip data-for="Expand" onClick={() => handleFullScreenWidgetView(widget.i)}><i className="far fa-expand text-indigo"></i></li>}
                    <ReactTooltip id="Expand" place="bottom" type="dark">
                        <div className="tooltipText"><p>Expand</p></div>
                    </ReactTooltip>
                    {(props.parent === "createOrEditPage" || widget.dataSource === "API") && <React.Fragment>
                        <li data-tip data-for="Config" onClick={() => configureClickHandler(widget)} ><i className="far fa-tools text-green"></i></li>
                        <ReactTooltip id="Config" place="bottom" type="dark">
                            <div className="tooltipText"><p>Config</p></div>
                        </ReactTooltip>
                    </React.Fragment>}
                    {props.parent === "createOrEditPage" && <React.Fragment>
                        <li data-tip data-for="Clone" onClick={() => props.addWidgetHandler && props.addWidgetHandler(null, widget.i)} ><i className="far fa-copy text-cyan"></i></li>
                        <ReactTooltip id="Clone" place="bottom" type="dark" globalEventOff="click">
                            <div className="tooltipText"><p>Clone</p></div>
                        </ReactTooltip>
                        {widget.widgetType === "customWidgets" && <li data-tip data-for="Edit" onClick={() => props.openEditWidgetTab && props.openEditWidgetTab(widget.widgetId)} ><i className="far fa-pen text-primary"></i></li>}
                        <ReactTooltip id="Edit" place="bottom" type="dark">
                            <div className="tooltipText"><p>Edit</p></div>
                        </ReactTooltip>
                        <li data-tip data-for="Delete" onClick={() => props.removeWidgetHandler && props.removeWidgetHandler(widget.i)}><i className="far fa-trash-alt text-red"></i></li>
                        <ReactTooltip id="Delete" place="bottom" type="dark" globalEventOff="click">
                            <div className="tooltipText"><p>Delete</p></div>
                        </ReactTooltip>
                    </React.Fragment>}
                </ul>
            </div>
        )
    }

    let getControlWidgetTitle = (widget) => {
        let isForConfiguration = widget.i.includes("preview"),
            pathParams = isForConfiguration ? state.api.pathParameters : widget.apiDetails.pathParameters,
            title = pathParams && pathParams.find(widget => widget.name === "command") ? pathParams.find(widget => widget.name === "command").value : "Reboot"
        return title || "N/A";
    }

    let publishMqttConnection = (topic, publishMessage, message2, widgetId) => {
        let brokerURL = window.location.origin === "http://localhost:3001" ? window.API_URL.split("//")[1].substring(0, window.API_URL.split("//")[1].length - 1) : window.location.host;
        let count = 0;
        let credentials = getVMQCredentials();

        const options = {
            host: brokerURL,
            protocol: 'wss',
            path: '/connectmqtt',
            username: credentials.userName,
            password: credentials.password,
        };
        if (typeof publishClient !== 'undefined') {
            publishClient.end();
        }
        publishClient = MQTT.connect(options);

        publishClient.on('connect', function () {
        });

        publishClient.on('close', function (response) {
            if (response && response.type == 'error' && count > 4) {
                publishClient.end();
            }
        });
        publishClient.on('reconnect', function () {
            count++;
        });
        publishClient.publish(topic, publishMessage, function (err) {
            let addedWidgets = cloneDeep(state.addedWidgets);
            let requiredWidget = addedWidgets.find(el => el.i === widgetId)
            if (requiredWidget) {
                requiredWidget.isLoadingData = false;
                setState({
                    addedWidgets,
                }, () => {
                    let message = err ? "Something went wrong!!" : message2
                    let modalType = err ? "error" : "success"
                    props.showNotification(modalType, message, onCloseHandler)
                })
            }
        })
    }

    let createCustomWidget = (widget, data) => {
        widget = cloneDeep(widget)
        data = widget.widgetData
        if (widget.error) {
            return <p className="text-danger dashboardGridLayoutLoader"> {widget.error} </p>
        }
        let html = decodeURIComponent(escape(atob(widget.customWidgetDetails.html)))
        let js = decodeURIComponent(escape(atob(widget.customWidgetDetails.js)))
        if (JSON.stringify(data) != "[]") {
            if (widget.customWidgetDetails.type === "custom") {
                Object.entries(data).map(([key, value]) =>
                    html = html.replace(new RegExp(`{{${key}}}`, 'g'), value)
                )
            } else {
                Object.entries(data).map(([key, value]) => js = `var ${key} = ${JSON.stringify(value)};` + js)
            }
        } else {
            if (widget.customWidgetDetails.responseFormat)
                Object.entries(widget.customWidgetDetails.responseFormat).map(([key, value]) => js = `var ${key} = ${JSON.stringify(value)};` + js)
        }
        return (<div className="dashboardGridCounterBox">
            <Iframe css={decodeURIComponent(escape(atob(widget.customWidgetDetails.css)))} js={js}
                html={html} id={widget.widgetName} setting={widget.customWidgetDetails.widgetSettings} />
        </div>)
    }

    let handleControlClick = (widget) => {
        if (widget.dataSource === "API") {
            let addedWidgets = cloneDeep(state.addedWidgets);
            addedWidgets.find(el => el.i === widget.i).isLoadingData = true;
            let connectorId = widget.apiDetails.pathParameters.find(param => param.name === "connectorId").value,
                connectorDetails = state.connectorsList.find(connector => connector.id === connectorId),
                deviceName = widget.apiDetails.pathParameters.find(param => param.name === "command").value,
                topicId = connectorDetails.properties.topics[0].name,
                deviceId = widget.apiDetails.pathParameters.find(param => param.name === "id").value,
                topic = `${topicId}/${deviceId}/control`,
                publishMessage = JSON.stringify(widget.widgetDetails.configCommand),
                message = `RPC Command - ${getControlWidgetTitle(widget)} invoked successfully for ${deviceName}`;
            setState({
                addedWidgets
            })
            publishMqttConnection(topic, publishMessage, message, widget.i)
        }
    }

    let setSelectedDay = (dayInfo, i) => {
        let addedWidgets = cloneDeep(state.addedWidgets)
        addedWidgets.find(temp => temp.i === i).widgetData.selectedDay = dayInfo;
        return setState({
            addedWidgets
        })
    }

    let createWidget = (widget) => {
        if (widget.error) {
            return <div className="widget-message">
                <p className="text-red">{widget.error}</p>
            </div>
        }
        let noDataMessage = <div className="widget-message"><p className="text-gray">There is no data to display.</p></div>
        if ((!widget.widgetData || (Array.isArray(widget.widgetData) && !widget.widgetData.length)) && widget.widgetType !== "control") {
            return noDataMessage
        }
        switch (widget.widgetType) {
            case "customWidgets":
                return createCustomWidget(widget)
            // case "deviceList":
            //     return (
            //         <TableWidget data={widget.widgetData}
            //             widgetProperties={widget.widgetDetails}
            //             actionClickHandler={(url, attr) => props.history.push(url + "/" + attr)}
            //             {...props} />
            //     );
            case "weather":
                let noData = (JSON.stringify(widget.widgetData) === "{}")
                return noData ? <div className="widget-message"><p className="text-gray">You didn't configure the location for this device.</p></div> : <WeatherWidget widget={widget} setSelectedDay={setSelectedDay} />

            case "alarms":
                return <AlarmsWidget data={widget.widgetData} />;
            case "deviceList":
            case "deviceMap":
                return (
                    <DeviceMap data={widget.widgetData} connectorId={widget.dataSource === "API" ? state.selectedConnector : null} widgetProperties={widget.widgetDetails} widgetType={widget.widgetType} {...props}/>
                );
            /*
                    <MapWithMarkerCluster onCloseHandler={(index) => mapInfoBoxHandler("CLOSE", widget)} openSelectedInfoBox={widget.openSelectedInfoBox} data={widget.widgetData ? widget.widgetData.tableData : ''} attributes={widget.widgetData ? widget.widgetData.availableAttributes : ''} pageUrl={widget.widgetDetails.detailPage} zoomLevel={widget.widgetDetails.customValues.zoomLevel.value} selectedAttribute={widget.widgetDetails.selectedAttribute} {...props} />
            );*/

            case "header":
            case "label":
                let style = {
                    backgroundColor: widget.widgetDetails.customValues.backgroundColor.value,
                    color: widget.widgetDetails.customValues.textColor.value,
                    fontSize: widget.widgetDetails.customValues.textFontSize.value,
                    fontWeight: widget.widgetDetails.customValues.fontWeight.value,
                }
                if (widget.widgetType === "label") {
                    return (
                        <div className="textAreaWidget position-relative" style={style}>
                            <h5 style={style}>{widget.widgetDetails.headerName}</h5>
                        </div>
                    )
                }
                return (
                    <div className="header-widget" style={style}>{widget.widgetDetails.headerName}</div>
                );

            case "counter":
                return (
                    <CountersWidget type={widget.widgetName} data={{ count: widget.widgetData, header: widget.widgetDetails.headerName }} customValues={widget.widgetDetails.customValues} icon={widget.widgetDetails.icon} />
                );

            case "control":
                return (
                    <div className="button-widget h-100">
                        <button className="btn btn-primary w-100" type="button" disabled={widget.dataSource == "DEFAULT"} onClick={() => handleControlClick(widget)}>
                            {widget.isLoadingData ? <i className="fad fa-sync-alt fa-spin"></i> : getControlWidgetTitle(widget)}
                        </button>
                    </div>
                );

            case "images":
                return (
                    widget.apiId ?
                        <div className="imageWidgetPreview">
                            <img src={(widget.apiId)} />
                        </div> :
                        <div className="imageWidgetNotFound">
                            <img src="https://content.iot83.com/m83/other/dummyImage.png" />
                        </div>
                );

            case "gaugeChart":
                if (!widget.widgetData[0])
                    return noDataMessage
                let chartData = [{ ...widget.widgetData[0], ...widget.widgetDetails.customValues }],
                    maxSmallerThanMin = chartData[0].max < chartData[0].min || chartData[0].max == chartData[0].min
                if (maxSmallerThanMin) {
                    return <div className="widget-message"><p className="text-red">Max should be greater than Min</p></div>
                }
                return <GaugeChart data={chartData} id={widget.i} type={widget.widgetName} />

            case "info":
                let { lastReported, deviceName, deviceLocation, tag, deviceGroupName, image, status } = widget.widgetData
                return (
                    <div className="device-details-widget">
                        <div className="device-image-box">
                            <div className="device-image">
                                <img src={image} />
                            </div>
                            <h6><strong>Last Reported : </strong>{lastReported ? convertTimestampToDate(lastReported) : "N/A"}</h6>
                            <span className="text-green"><i className="fas fa-circle"></i></span>
                        </div>
                        <div className="device-item">
                            <p>{deviceName}</p>
                            <h6><strong>Location : </strong>{deviceLocation || "N/A"}</h6>
                            <h6><strong>Tag : </strong>{tag || "N/A"}</h6>
                            <h6><strong>Cluster : </strong>{deviceGroupName || "N/A"}</h6>
                        </div>
                    </div>
                );

            case "topology":
                return (
                    <div className="topology-widget">
                        <ul className="topology-list">
                            {widget.widgetData.map((item, i) => <li>
                                <div className="topology-item">
                                    <div className="topology-image">
                                        <img src={item.img || "https://image.flaticon.com/icons/svg/485/485884.svg"} />
                                    </div>
                                    <p>{item.name}</p>
                                    {((i + 1) != widget.widgetData.length) && <div className="topology-connection">
                                        <p>{item.connectedVia}</p><div className="topology-bar"></div>
                                    </div>}
                                </div>
                            </li>)
                            }
                        </ul>
                    </div>
                );

            case "details":
                widget.widgetData = [
                    {
                        "name": "temp",
                        "displayName": "Temperature",
                        "value": 35,
                        "unit": "C",
                    },
                    {
                        "name": "press",
                        "displayName": "press",
                        "value": 3150,
                        "unit": "hPa",
                    },
                    {
                        "name": "switch",
                        "displayName": "switch",
                        "value": false,
                        "unit": "",
                    },
                    {
                        "name": "deviceName",
                        "displayName": "Display Name",
                        "value": "Living Room",
                        "unit": "",
                    },
                    {
                        "name": "deviceId",
                        "displayName": "Device Id",
                        "value": "oef6782adcd3850469c9dfb82cd411c0dc2",
                        "unit": "",
                    }
                ]
                return (
                    <div className="device-info">
                        <h1>Device Details</h1>
                        <table className="table table-bordered">
                            <tbody>
                                {widget.widgetData.map(({ displayName, value, unit }, i) => <tr key={i}>
                                    <td>{displayName}</td>
                                    <td>{value + " " + unit}</td>
                                </tr>)}
                            </tbody>
                        </table>
                    </div>
                );

            case "insights":
                widget.widgetData = {
                    "attrUnit": "C",
                    "trend": "inc",
                    "attrTrend": [
                        {
                            "temp": 35,
                            "timestamp": 1594510200000
                        },
                        {
                            "temp": 32,
                            "timestamp": 1594513800000
                        },
                        {
                            "temp": 36,
                            "timestamp": 1594517400000
                        },
                        {
                            "temp": 37,
                            "timestamp": 1594521000000
                        },
                        {
                            "temp": 38,
                            "timestamp": 1594524600000
                        }
                    ],
                    "attr": "temp",
                    "attrDisplayName": "Temperature",
                    "value": 3
                }
                let { attrDisplayName, trend, value, attrUnit } = widget.widgetData
                let iconClassName = "far fa-minus text-red"
                if (trend === "inc") {
                    trend = "increased"
                    // iconClassName = "far fa-plus text-green"
                }
                return (
                    <div className="insight-widget">
                        <h6>{attrDisplayName}</h6>
                        <p> <i className="far fa-minus text-green"></i> <span>{value}</span> {`${trend} ${value} ${attrUnit}`}</p>
                        <Chart data={widget} dimensions={(widget.w) * (widget.h) * (window.devicePixelRatio)} />
                    </div>
                );

            case "uptimeInfo":
                return <DeviceConnectivity widgetData={widget.widgetData} />;

            case "deviceAlarms":
                return (
                    <div className="widget-tab-wrapper">
                        <ul className="nav nav-tabs" id="myTab" role="tablist">
                            <li className="nav-item">
                                <a className="nav-link active" data-toggle="tab" href="#all" role="tab">All</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" data-toggle="tab" href="#high" role="tab">High Priority</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" data-toggle="tab" href="#low" role="tab">Low Priority</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" data-toggle="tab" href="#occurred" role="tab">Recently Occurred</a>
                            </li>
                        </ul>
                        <div className="tab-content" id="myTabContent">
                            <div className="tab-pane fade show active" id="all">
                                <div className="alarm-widget">
                                    <div className="card">
                                        <div className="card-header">
                                            <div className="alarm-widget-icon"><i className="fas fa-bell"></i>
                                                <span className="badge badge-danger">1</span>
                                            </div>
                                            <h1>
                                                Alarm On Device 2
                                            </h1>
                                            <ol className="alarm-action">
                                                <li>
                                                    <div className="widget-circular-image">
                                                        <img src="https://image.flaticon.com/icons/svg/281/281769.svg" />
                                                    </div>
                                                </li>
                                                <li>
                                                    <div className="widget-circular-image">
                                                        <img src="https://symphony.com/wp-content/uploads/2019/08/webhooks_1024.png" />
                                                    </div>
                                                </li>
                                            </ol>
                                        </div>
                                        <div className="card-body">
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting</p>
                                            <span>28 july 2020 <span>|</span> 08:00 PM</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                );

            case "eventHistory":
                return <EventHistory widgetData={widget.widgetData} />

            case "layout":
                return <div />

            default:
                let noDataCheck = (widget, data) => {
                    let pieOrSankeyCondition = (widget === "pieChart" && data && data.length === 0)
                    let othersCondition = ((["lineChart", "barChart", "lineChartMultiAxes"].includes(widget)) && data.chartData && data.chartData.length === 0)
                    return Boolean(pieOrSankeyCondition || othersCondition)
                }
                if ((widget.apiId && widget.widgetData) && noDataCheck(widget.widgetType, widget.widgetData)) {
                    return noDataMessage
                }
                if (widget.widgetName === "layeredColumnChart" && widget.widgetData.availableAttributes && widget.widgetData.availableAttributes.length < 2) {
                    return <div className="widget-message"><p className="text-gray">Please check the data provided for widget configuration.</p></div>;
                }
                return <Chart data={widget} dimensions={(widget.w) * (widget.h) * (window.devicePixelRatio)} />

        }
    }

    let createElement = (widget, index) => {
        const i = widget.i;
        let showHeader = validateHeaderVisibility(widget.widgetType, widget.customWidgetDetails.type, widget.widgetDetails.hideHeader, true);
        let dataGrid = {
            x: widget.x,
            y: widget.y,
            w: widget.w,
            h: widget.h,
            minH: widget.minH,
            minW: widget.minW,
            maxH: widget.maxH,
            maxW: widget.maxW,
        }
        return (
            <div key={i} data-grid={dataGrid}>
                <div className={"widget-wrapper" + (widget.customWidgetDetails.type == "custom" ? " dashboardGridLayoutTransparent" : "")}>
                    {showHeader &&
                        <div style={state.pageSettings.header} className={widget.widgetDetails.iconVisibility ? "widget-header widget-header-padding" : "widget-header "}>
                            {/*<span></span>*/}
                            {widget.widgetDetails.iconVisibility && <div className="widget-header-icon">
                                <i className={widget.widgetDetails.icon} style={{ color: widget.widgetDetails.iconColor }}></i>
                            </div>}
                            <h6 style={{ fontWeight: state.pageSettings.header.fontWeight, fontSize: state.pageSettings.header.fontSize }}>{widget.widgetDetails.headerName}</h6>
                            {widget.widgetDetails.subHeadingVisibility && <p style={state.pageSettings.subHeading}>{widget.widgetDetails.subHeading}</p>}
                        </div>
                    }
                    {widget.isLoadingData ?
                        <div className="widget-loader">
                            <i className="fad fa-sync-alt fa-spin"></i>
                        </div> :
                        <div className={!showHeader || widget.widgetType === "custom" ? "widget-body widget-body-full" : "widget-body"}>
                            {(typeof (state.configWidgetIndex) != "number") && showDropdown(widget)}
                            {widget.dataSource === "MQTT" && <div className="dashboardLayoutLive"></div>}
                            {createWidget(widget)}
                        </div>
                    }
                </div>
            </div>
        );
    }

    let clearMapFilter = ({ i }) => {
        let addedWidgets = cloneDeep(state.addedWidgets);
        let foundIndex = addedWidgets.findIndex(temp => temp.i == i)
        addedWidgets[foundIndex].searchTerm = "";
        addedWidgets[foundIndex].widgetData = cloneDeep(addedWidgets[foundIndex].originalData);
        setState({
            addedWidgets
        })
    }

    let mapInfoBoxHandler = (action, { i }, index) => {
        let addedWidgets = cloneDeep(state.addedWidgets);
        let foundIndex = addedWidgets.findIndex(temp => temp.i == i)
        addedWidgets[foundIndex].widgetData.tableData.map(temp => temp.openSelectedInfoBox = false)
        if (action === "OPEN")
            addedWidgets[foundIndex].widgetData.tableData[index].openSelectedInfoBox = true
        setState({
            addedWidgets
        })
    }

    let mapSearchHandler = ({ i }) => ({ currentTarget }) => {
        let addedWidgets = cloneDeep(state.addedWidgets);
        let requiredWidgetIndex = addedWidgets.findIndex(temp => temp.i == i);
        addedWidgets[requiredWidgetIndex].originalData = addedWidgets[requiredWidgetIndex].originalData ? addedWidgets[requiredWidgetIndex].originalData : addedWidgets[requiredWidgetIndex].widgetData;
        addedWidgets[requiredWidgetIndex].searchTerm = currentTarget.value;
        addedWidgets[requiredWidgetIndex].widgetData = cloneDeep(addedWidgets[requiredWidgetIndex].originalData);
        if (currentTarget.value)
            addedWidgets[requiredWidgetIndex].widgetData.tableData = addedWidgets[requiredWidgetIndex].widgetData.tableData.filter((temp) => (temp.name || temp._uniqueDeviceId).toLowerCase().includes(currentTarget.value.toLowerCase().trim()));
        setState({
            addedWidgets
        })
    }

    let handleFullScreenWidgetView = (i) => {
        let fullScreenWidgetIndex = state.addedWidgets.findIndex(el => el.i === i)
        setState({ fullScreenWidgetIndex });
    }

    let configureClickHandler = (param) => {
        let addedWidgets = cloneDeep(state.addedWidgets)
        let configWidgetIndex;
        let configWidget = addedWidgets.find((el, index) => {
            if (el.i === param.i) {
                configWidgetIndex = index
                return true
            }
        }),
            tempWidgetData = cloneDeep(configWidget),
            configWidgetType = configWidget.widgetType,
            controlPreviewValidation = configWidgetType === "control" && configWidget.dataSource === "API"
        if ((configWidget.apiId && configWidget.widgetName !== "image") || (controlPreviewValidation)) {
            configWidget.apiDetails = configWidgetType === "control" ? {} : {
                ...cloneDeep(state.allGatewayApis.find(match => match.id === configWidget.apiId)),
                pathParameters: configWidget.apiDetails.pathParameters
            }
        }
        let devices = state.devices
        if (configWidget.dataSource === "MQTT") {
            devices = state.connectorsList.find(temp => temp.id === configWidget.mqttDetails.topic.connector).properties.deviceIds.map(temp => ({ label: temp.name || temp.deviceId, value: temp.deviceId }))
        }
        setState({
            addedWidgets,
            tempWidgetData,
            configWidgetType,
            configWidgetIndex,
            widgetMenu: false,
            pageUrl: configWidget.widgetDetails.detailPage,
            selectedAttribute: "_uniqueDeviceId",
            customData: configWidget.dataSource === "CUSTOM" ? configWidget.widgetData : null,
            selectedTopic: configWidget.dataSource === "MQTT" ? configWidget.mqttDetails.topic.name : "",
            isLocalTimestamp: configWidget.mqttDetails.isLocalTimestamp ? configWidget.mqttDetails.isLocalTimestamp : false,
            subCategory: configWidget.customWidgetDetails.type,
            redirectUri: "",
            searchTerm: "",
            controlPreviewValidation,
            devices,
        })
    }

    return (
        <React.Fragment>
            <ResponsiveReactGridLayout
                id="div1"
                verticalCompact={true}
                onLayoutChange={onLayoutChange}
                rowHeight={50}
                isDraggable={props.parent === "createOrEditPage"}
                isResizable={props.parent === "createOrEditPage"}
                breakpoints={{ lg: 1200, md: 996, sm: 768, xs: 480, xxs: 0 }}
                cols={{ lg: 12, md: 12, sm: 12, xs: 12, xxs: 12 }}
            >
                {cloneDeep(state.addedWidgets).map((el, index) => createElement(el, index))}
            </ResponsiveReactGridLayout>

            {/*{typeof(state.fullScreenWidgetIndex) == 'number' &&*/}
            {/*    <div className="modal d-block" id="expandWidgetModal">*/}
            {/*        <div className="modal-dialog modal-xl" role="document">*/}
            {/*            <div className="modal-content">*/}
            {/*                <div className="modal-body full-widget-body">*/}
            {/*                    {state.addedWidgets[state.fullScreenWidgetIndex] &&*/}
            {/*                        <div className="widget-body">*/}
            {/*                            {state.addedWidgets[state.fullScreenWidgetIndex].dataSource === "MQTT" && <div className="dashboardLayoutLive"></div>}*/}
            {/*                            {createWidget(state.addedWidgets[state.fullScreenWidgetIndex])}*/}
            {/*                        </div>*/}
            {/*                    }*/}
            {/*                </div>*/}
            {/*                <div className="modal-footer">*/}
            {/*                    <button type="button" className="btn btn-light" onClick={()=>{setState({fullScreenWidgetIndex:null})}}>close</button>*/}
            {/*                </div>*/}
            {/*            </div>*/}
            {/*        </div>*/}
            {/*    </div>*/}
            {/*}*/}
        </React.Fragment >
    )
}

GridLayout.propTypes = {};

export default GridLayout;
