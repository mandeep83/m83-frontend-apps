/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * ListingTable
 *
 */

import React, { useState, useEffect, useRef } from "react";
import cx from 'classnames';
import debounce from 'lodash/debounce'
import {
    getFilteredData, getHtmlForCell, getBlankSkeletons,
    geticon, getStatusBar, getFlexClass, PAGE_SIZE_OPTIONS,
    rowClickHandler, getPageNumbers, getLastPageNumber
} from "./tableFunctionsRequired"
import range from 'lodash/range';
import cloneDeep from 'lodash/cloneDeep';
import Skeleton from "react-loading-skeleton";
function ListingTable(props) {
    var { columns, data, pageChangeHandler, pagination, statusBar, icon, pageSize, activePageNumber, isLoading, totalItemsCount, itemClickHandler, id, className } = props
    data = data || []
    pagination = typeof (pagination) == "boolean" ? pagination : true

    let [state, setState] = useState({
        pageSize: pageSize && pageChangeHandler ? pageSize : 10,
        activePageNumber: activePageNumber && pageChangeHandler ? activePageNumber : 1,
        totalItemsCount: totalItemsCount || data.length,
        completeData: data,
        id,
        columns,
        filtersAndSort: {
            sort: null,
            filters: []
        }
    })

    let { paginatedData, paginatedDataItemsCount } = pageChangeHandler ? { paginatedData: state.completeData, paginatedDataItemsCount: state.completeData.length } : getFilteredData(state.columns, state.completeData);
    paginatedDataItemsCount = totalItemsCount || paginatedDataItemsCount
    let totalPages = Math.ceil(paginatedDataItemsCount / state.pageSize)
    let endIndex = (state.pageSize * state.activePageNumber);
    let startIndex = endIndex - state.pageSize;

    const inBuiltPageChangeHandler = (pageSize, activePageNumber) => {
        let pageSizeChanged = pageSize !== state.pageSize
        if (pageSizeChanged) {
            let totalPages = Math.ceil(paginatedDataItemsCount / pageSize)
            if (totalPages < activePageNumber) {
                activePageNumber = totalPages
            }
        } else if (activePageNumber === "...") {
            activePageNumber = Math.ceil((getPageNumbers(totalPages, state.activePageNumber)[3] - 1) / 2)
        }
        setState({
            ...state,
            pageSize,
            activePageNumber
        })
    }

    pageChangeHandler = pageChangeHandler || inBuiltPageChangeHandler
    if (!props.pageChangeHandler) {
        paginatedData = paginatedData.slice(startIndex, endIndex)
    }

    useEffect(() => {
        let activePageNumber = state.activePageNumber;
        if (pagination && !props.pageChangeHandler) {
            if (data.length !== state.completeData.length) {
                activePageNumber = 1
            }
        }
        setState((prevState) => ({ ...prevState, totalItemsCount: totalItemsCount, completeData: data, activePageNumber }))
    }, [data])

    useEffect(() => {
        if (props.pageChangeHandler) {
            if (pageSize && pageSize !== state.pageSize)
                setState({ ...state, pageSize })
            if (activePageNumber && activePageNumber !== state.activePageNumber) {
                setState({ ...state, activePageNumber })
            }
        }
        if (id && id !== state.id) {
            setState({
                ...state,
                pageSize: pageSize && pageChangeHandler ? pageSize : 10,
                activePageNumber: activePageNumber && pageChangeHandler ? activePageNumber : 1,
                totalItemsCount: totalItemsCount || data.length,
                completeData: data,
                id,
                filtersAndSort: {
                    sort: null,
                    filters: []
                },
                columns
            })
        }
    }, [pageSize, activePageNumber, id])

    const onChangeHandler = debounce((value, accessor) => {
        let columns = cloneDeep(state.columns)
        columns.find(temp => temp.accessor == accessor).value = value;
        setState({
            ...state,
            activePageNumber: 1,
            columns,
        })
    }, 200)

    const sortList = (operation, accessor) => {
        let columns = cloneDeep(state.columns)
        columns.map(temp => {
            temp.sortValue = ""
            if (temp.accessor == accessor) {
                temp.sortValue = operation
            }
        })
        setState({
            ...state,
            activePageNumber: 1,
            columns
        })
    }

    useEffect(() => {
        if (props.pageChangeHandler && state.columns !== columns) {
            const filters = state.columns.filter(temp => temp.filterable && temp.value).map(temp => ({ accessor: temp.accessor, value: temp.value }))
            const findSortColumn = state.columns.find(temp => temp.sortValue)
            const filtersAndSort = {
                sort: findSortColumn ? { accessor: findSortColumn.accessor, value: findSortColumn.sortValue } : null,
                filters
            }
            props.pageChangeHandler(state.pageSize, state.activePageNumber, filtersAndSort)
            setState({
                ...state,
                filtersAndSort
            })
        }
    }, [state.columns])

    let initialRender = useRef(true);
    let columnsRef = useRef(null)
    useEffect(() => {
        if (initialRender.current) {
            columnsRef.current = JSON.stringify(columns);
            initialRender.current = false;
        } else {
            let newColumns = JSON.stringify(columns);
            if(columnsRef.current !== newColumns){
                columnsRef.current = newColumns;
                setState((prevState) => ({
                    ...prevState,
                    columns,
                }));
            }
        }
    }, [columns])

    return (
        <React.Fragment>
            <div className="list-view">
                <div className="list-view-header d-flex">
                    {state.columns.map(({ Header, width, sortable }, index) => (
                        <p key={index} className={getFlexClass(width)} >{Header}</p>
                    ))}
                </div>
                {state.columns.some(temp => (temp.filterable || temp.sortable)) && <div className="list-view-filter d-flex">
                    {state.columns.map(({ Header, width, sortable, filterable, filter, value, accessor, sortValue }, index) => {
                        if (filterable || sortable)
                            return <div className={`form-group dropdown ${!sortable ? "form-group-sort" : ""} ${getFlexClass(width)}`} key={index}>
                                {filterable && (filter ? filter({ value, onChangeHandler: (event, accessor) => onChangeHandler(event.target.value, accessor), accessor }) :
                                    <input type="text" className="form-control" placeholder="Search.." onChange={(event) => onChangeHandler(event.target.value, accessor)} />)
                                }
                                {sortable &&
                                    <React.Fragment>
                                        <button className="btn btn-light" data-toggle="dropdown"><i className="fas fa-sort"></i></button>
                                        <div className="dropdown-menu">
                                            <p>Sort by</p>
                                            <ul className="list-style-none">
                                                <li className={cx("", { 'active': sortValue === "ASC" })} onClick={() => sortList("ASC", accessor)}><i className="far fa-sort-amount-down-alt"></i>A - Z</li>
                                                <li className={cx("", { 'active': sortValue === "DESC" })} onClick={() => sortList("DESC", accessor)}><i className="far fa-sort-amount-up-alt"></i>Z - A</li>
                                            </ul>
                                        </div>
                                    </React.Fragment>
                                }
                            </div>
                        else
                            return <div className={`form-group ${getFlexClass(width)}`} key={index} />
                    })}
                </div>
                }
                {props.isLoading ?
                    <Skeleton count={6} /> :
                    <div className="list-view-body">
                        {paginatedData.length ?
                            paginatedData.map((dataItem, dataItemIndex) => (
                                <div className={"list-view-item " + className} key={dataItemIndex} id="dataItemDiv">
                                    {Boolean(statusBar) && getStatusBar(statusBar, dataItem, dataItemIndex)}
                                    <ul className="list-style-none d-flex" id="rowContainer" onClick={rowClickHandler(dataItem, itemClickHandler)}>
                                        {Boolean(icon) && geticon(dataItem, icon)}
                                        {columns.map((col, index) => (
                                            <li key={index} className={getFlexClass(col.width)}>
                                                {col.cell ? col.cell({ ...dataItem, index: dataItemIndex }) :
                                                    getHtmlForCell(dataItem, col)
                                                }
                                            </li>
                                        ))}
                                    </ul>
                                </div>
                            )) :
                            <div className="list-view-empty"><h6>There is no data to display.</h6></div>
                        }
                        {/* {Boolean(paginatedData.length > 0 && paginatedData.length < state.pageSize) && getBlankSkeletons(state.pageSize, paginatedData.length)} */}
                    </div>
                }
            </div>

            {(pagination && paginatedData.length > 0) &&
                <div className="d-flex align-items-center pagination-box">
                    <div className="flex-50">
                        <h6>Showing {(((state.activePageNumber * state.pageSize) - state.pageSize) + 1)} to {getLastPageNumber(state.activePageNumber, state.pageSize, paginatedDataItemsCount)} of
                            <strong className="text-theme"> {paginatedDataItemsCount} rows</strong>
                            <span className="mr-l-10 mr-r-10">|</span>
                            Records Per Page:
                            <button className="btn btn-light dropdown-toggle" type="button" data-toggle="dropdown">{state.pageSize} </button>
                            <ul className="dropdown-menu">
                                {PAGE_SIZE_OPTIONS.map((size, index) => (
                                    <li key={index} onClick={() => size !== state.pageSize && pageChangeHandler(size, state.activePageNumber, state.filtersAndSort)}>{size}</li>
                                ))}
                            </ul>
                        </h6>
                    </div>
                    <div className="flex-50">
                        <ul className="list-style-none d-flex justify-content-end">
                            <li className="page-item" onClick={() => state.activePageNumber > 1 && pageChangeHandler(state.pageSize, state.activePageNumber - 1, state.filtersAndSort)}>
                                <a className={`page-link ${state.activePageNumber > 1 ? "bg-theme text-white" : "text-light-gray"}`}>
                                    <i className="far fa-angle-double-left mr-r-5"></i>Previous
                            </a>
                            </li>
                            {getPageNumbers(totalPages, state.activePageNumber).map((pageNumber, index) => (
                                <li key={index} className="page-item" onClick={() => pageNumber !== state.activePageNumber && pageChangeHandler(state.pageSize, pageNumber, state.filtersAndSort)}>
                                    <a className={`page-link ${state.activePageNumber === pageNumber ? "bg-theme text-white" : "text-gray"}`}>{pageNumber}</a>
                                </li>
                            ))}
                            <li className="page-item"
                                onClick={() => state.activePageNumber < totalPages && pageChangeHandler(state.pageSize, state.activePageNumber + 1, state.filtersAndSort)}>
                                <a className={`page-link ${state.activePageNumber < totalPages ? "bg-theme text-white" : "text-light-gray"}`}>
                                    Next<i className="far fa-angle-double-right mr-l-5"></i>
                                </a>
                            </li>
                            <h6> Go to Page :
                            <button className="btn btn-light dropdown-toggle" type="button" data-toggle="dropdown"> {state.activePageNumber} </button>
                                <ul className="dropdown-menu">
                                    {range(1, totalPages + 1).map((pageNumber, index) => (
                                        <li key={index} onClick={() => pageNumber !== state.activePageNumber && pageChangeHandler(state.pageSize, pageNumber, state.filtersAndSort)}>{pageNumber}</li>
                                    ))}
                                </ul>
                            </h6>
                        </ul>
                    </div>
                </div>
            }
        </React.Fragment>
    );
}

ListingTable.propTypes = {};

export default ListingTable;