/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * ConnectionHistoryChart
 *
 */

import React from "react";
import * as am4core from "@amcharts/amcharts4/core";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import * as am4charts from "@amcharts/amcharts4/charts";
import { cloneDeep, isEqual } from "lodash";
am4core.addLicense("CH145187641");
am4core.useTheme(am4themes_animated)
am4core.options.queue = true;
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

/* eslint-disable react/prefer-stateless-function */
class ConnectionHistoryChart extends React.Component {

  componentDidMount() {
    this.createConnectionHistoryChart()
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.timeZone !== this.props.timeZone) {
      this.chart && this.chart.dispose()
      this.createConnectionHistoryChart()
    }
  }

  componentWillUnmount() {
    this.chart && this.chart.dispose();
  }

  createConnectionHistoryChart = () => {
    let { chartData, chartId } = this.props
    this.chart = am4core.create(chartId, am4charts.XYChart);
    this.chart.colors.step = 2;
    chartData.forEach((temp) => {
      let total = temp.connected + temp.disconnected
      temp.connectedPercentage = temp.connected ? ((temp.connected / total) * 100).toFixed(2) : 0
      temp.disconnectedPercentage = temp.disconnected ? ((temp.disconnected / total) * 100).toFixed(2) : 0
      // temp.timestamp = new Date(temp.timestamp).toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' });
    })
    this.chart.data = chartData;

    var dateAxis = this.chart.xAxes.push(new am4charts.DateAxis());
    dateAxis.dataFields.category = "timestamp";
    // dateAxis.renderer.grid.template.location = 0;
    // dateAxis.renderer.minGridDistance = 20;
    dateAxis.startLocation = 0.5;
    dateAxis.endLocation = 0.5;
    // dateAxis.paddingRight = 15;
    dateAxis.fontSize = 12;
    dateAxis.renderer.baseGrid.disabled = true;
    dateAxis.renderer.grid.template.disabled = true;
    dateAxis.renderer.line.strokeOpacity = 0.5;
    dateAxis.renderer.line.strokeWidth = 0.6;
    // dateAxis.renderer.minGridDistance = 50;
    dateAxis.dateFormats.setKey("day", "dd MMM");
    dateAxis.dateFormats.setKey("hour", "dd MMM HH:mm");
    dateAxis.dateFormats.setKey("minute", "dd MMM HH:mm");
    dateAxis.dateFormats.setKey("second", "HH:mm:ss");
    dateAxis.dateFormats.setKey("millisecond", "HH:mm:ss");
    dateAxis.dateFormatter.timezone = localStorage.timeZone

    var label = dateAxis.renderer.labels.template;
    label.wrap = true;
    label.maxWidth = 120;

    var valueAxis = this.chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.calculateTotals = true;
    valueAxis.min = 0;
    // valueAxis.max = 100;
    valueAxis.fontSize = 12;
    valueAxis.renderer.baseGrid.disabled = true;
    valueAxis.renderer.grid.template.disabled = true;
    valueAxis.renderer.line.strokeOpacity = 0.5;
    valueAxis.renderer.line.strokeWidth = 0.6;
    // valueAxis.renderer.labels.template.adapter.add("text", function (text) {
    //   return text + "%";
    // });

    var series = this.chart.series.push(new am4charts.LineSeries());
    series.dataFields.valueY = "connected";
    series.dataFields.discription = "connectedPercentage";
    series.dataFields.dateX = "timestamp";
    series.name = "Connected";
    series.stacked = true;
    series.fill = am4core.color("#6BFB7C");
    series.stroke = am4core.color("#04BC1A");
    series.fillOpacity = 0.3;
    series.strokeWidth = 1;

    series.tooltipHTML = "<span' style=color:#666666; margin:0px; '>Connected Devices: <span style='color:#666666;'><b>{connected} ({connectedPercentage}%)</b></span></span>";
    series.tooltip.fontSize = 13;
    series.tooltip.getFillFromObject = false;
    series.tooltip.background.fill = am4core.color("#fff");
    series.tooltip.getStrokeFromObject = true;
    series.tooltip.background.strokeWidth = 1;
    series.legendSettings.labelText = "Connected Devices";
    series.legendSettings.itemValueText = "[bold]{connected}[/bold] ({discription}%)";

    var series2 = this.chart.series.push(new am4charts.LineSeries());
    series2.dataFields.valueY = "disconnected";
    series2.dataFields.discription = "disconnectedPercentage";
    series2.dataFields.dateX = "timestamp";
    series2.name = "Disconnected";
    series2.stacked = true;
    series2.fill = am4core.color("#3afcef");
    series2.stroke = am4core.color("#20b0a6");
    series2.fillOpacity = 0.3;
    series2.strokeWidth = 1;
    series2.fill = am4core.color("#F78C74");
    series2.stroke = am4core.color("#FB4922");

    series2.tooltipHTML = "<span' style=color:#666666; margin:0px;'>Disconnected Devices: <span style='color:#666666; '><b>{disconnected} ({disconnectedPercentage}%)</b></span></span>";
    series2.tooltip.fontSize = 13;
    series2.tooltip.getFillFromObject = false;
    series2.tooltip.background.fill = am4core.color("#fff");
    series2.tooltip.getStrokeFromObject = true;
    series2.tooltip.background.strokeWidth = 1;
    series2.legendSettings.labelText = "Disconnected Devices";
    series2.legendSettings.itemValueText = "[bold]{disconnected}[/bold] ({discription}%)";

    this.chart.cursor = new am4charts.XYCursor();

    this.chart.legend = new am4charts.Legend();
    this.chart.legend.fontSize = 14;
    this.chart.legend.marginRight = 1400;
    this.chart.legend.labels.template.fill = am4core.color("#666666");
    // });
  }

  shouldComponentUpdate() {
    return false
  }

  render() {
    return <div className="card-chart-box" id={this.props.chartId} />;
  }
}

ConnectionHistoryChart.propTypes = {};

export default ConnectionHistoryChart;
