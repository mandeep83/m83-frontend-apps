/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * AlarmWidget
 *
 */

import React from "react";
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

import { FormattedMessage } from "react-intl";
import messages from "./messages";
import moment from 'moment'
import ReactTooltip from "react-tooltip";


function AlarmWidget(props) {
    return (
        <div className="alarm-widget-box">
            <div className="alarm-widget-header">
                Recent Alarms <span className="text-theme fw500">({props.data.length})</span>
            </div>
            <ul className="list-style-none">
                {props.data.length > 0 ? props.data.map((el, index) =>
                    <li key={index}>
                        <div className="collapsed" data-toggle="collapse" data-target={`#alarm_${index}`}>
                            <div className="alarm-list-count alert-danger">{el.details.length}</div>
                            <div className="alarm-list-icon"><i className="fad fa-sensor-alert"></i></div>
                            <h4><strong className="fw500 mr-r-5">Device:</strong>{el.deviceName}</h4>
                            <h6>
                                <span className="text-cyan" data-tip data-for={"time" + index}>{moment(el.timestamp).fromNow()}</span>
                                <ReactTooltip id={"time" + index} place="bottom" type="dark">
                                    <div className="tooltipText"><p>{new Date(el.timestamp).toLocaleString('en-US', { timeZone: localStorage.timeZone })}</p></div>
                                </ReactTooltip>
                                {(el.notifications.sms || el.notifications.email) &&
                                    <React.Fragment>
                                        <span className="mr-r-10 mr-l-10">|</span><strong className="fw500 mr-r-5">Notified Via:</strong>
                                        {el.notifications.sms && <span className="mr-r-10"><i className="fas fa-comment-alt-lines text-orange mr-r-5 f-11"></i>SMS</span>}
                                        {el.notifications.email && <span><i className="fas fa-envelope text-lightGreen mr-r-5 f-11"></i>Email</span>}
                                    </React.Fragment>
                                }
                            </h6>
                            <div className="alarm-list-arrow"><i className="far fa-angle-down"></i></div>
                        </div>
                        <div className="collapse" id={`alarm_${index}`}>
                            <div className="alarm-inner-list">
                                {el.details.map((temp, id) =>
                                    <h5 key={id}>{temp}</h5>
                                )}
                            </div>
                        </div>
                    </li>
                ) :
                    <p>There is no data to display.</p>
                }
            </ul>
        </div>
    );
}

AlarmWidget.propTypes = {};

export default AlarmWidget;
