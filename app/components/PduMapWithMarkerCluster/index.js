/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import React from "react";
import { GOOGLE_MAP_URL } from "../../utils/constants";
const { compose, withProps, withHandlers, withStateHandlers } = require("recompose");
import { withScriptjs, withGoogleMap, GoogleMap, Marker, InfoWindow, Circle } from "react-google-maps";
import { convertTimestampToDate } from "../../commonUtils";
const { MarkerClusterer } = require("react-google-maps/lib/components/addons/MarkerClusterer");

const PduMapWithMarkerCluster = compose(
    withProps({
        googleMapURL: GOOGLE_MAP_URL,
        loadingElement: <div style={{ height: `inherit` }} />,
        containerElement: <div style={{ height: `100%` }} />,
        mapElement: <div style={{ height: `100%` }} />,
    }),
    withHandlers({
        onMarkerClustererClick: () => (markerClusterer) => {
            const clickedMarkers = markerClusterer.getMarkers()
        },
    }),
    withStateHandlers(() => ({
        isOpen: false,
        showInfo: 0
    }), {
        onToggleOpen: ({ isOpen }) => () => ({
            isOpen: !isOpen,
        }),
        showInfo: ({ showInfo, isOpen }) => (a) => ({
            isOpen: !isOpen,
            showInfoIndex: a
        }),
    }),
    withScriptjs,
    withGoogleMap,
)(props => {
    let [state, setState] = React.useState({
        activeInfoBoxId: ""
    })

    let getLinkedPageUrl = (id) => {
        let url = (localStorage.applications ? "/page/" : "/previewPage/") + props.widgetProperties.detailPage + "/" + id
        // if (props.otherParams && props.otherParams.length > 0) {
        //     url = url + "/" + props.otherParams.join("/")
        // }
        return url
    }
    let getMapCenter = () => {
        let lat, lng;
        if (props.geofencing.status && props.geofencing.lat) {
            lat = props.geofencing.lat
            lng = props.geofencing.lng
        }
        else {
            lat = (props.data && props.data.length > 0) ? Number(props.data[0].lat) : 40.7035957
            lng = (props.data && props.data.length > 0) ? Number(props.data[0].lng) : -73.8854603
        }
        return { lat, lng }
    }
    return (
        <GoogleMap
            zoom={props.zoomLevel}
            center={getMapCenter()}
        >
            {props.geofencing.status && props.geofencing.lat && <Circle
                center={{
                    lat: props.geofencing.lat,
                    lng: props.geofencing.lng
                }}
                radius={props.geofencing.radius * 1000}
                options={{
                    strokeColor: '#08EE23',
                    strokeOpacity: 0.8,
                    strokeWeight: 2,
                    fillColor: '#d0fdd5',
                    fillOpacity: 0.35,
                }}
            />}
            <MarkerClusterer
                onClick={props.onMarkerClustererClick}
                averageCenter
                enableRetinaIcons
                gridSize={100}
                maxZoom={11}
            >
                {props.data && props.data.map((marker, index) => {
                    if (marker.location)
                        return (<Marker
                            key={index}
                            icon={marker.status === "online" ? 'https://content.iot83.com/m83/icons/greenMarker.png' : 'https://content.iot83.com/m83/icons/redMarker.png'}
                            position={{ lat: Number(marker.lat), lng: Number(marker.lng) }}
                            onClick={() => {
                                let activeInfoBoxId = state.activeInfoBoxId === marker.id ? "" : marker.id
                                setState({ ...state, activeInfoBoxId });
                            }}
                        >
                            {state.activeInfoBoxId === marker.id &&
                                <InfoWindow onCloseClick={() => {
                                    setState({ ...state, activeInfoBoxId: "" });
                                }} options={{ enableEventPropagation: true }}>
                                    <div className="map-marker">
                                        <h6>Device Details</h6>
                                        <div className="map-marker-detail">
                                            <div className="d-flex">
                                                <p className="flex-50 text-gray">Device Name :</p>
                                                <p className="flex-50 text-theme">{marker.name}</p>
                                            </div>
                                            <div className="d-flex">
                                                <p className="flex-50 text-gray">Device Id :</p>
                                                <p className="flex-50 text-theme">{marker.id.slice(0, 4) + "..." + marker.id.slice(marker.id.length - 5)}</p>
                                            </div>
                                            <div className="d-flex">
                                                <p className="flex-50 text-gray">Location :</p>
                                                <p className="flex-50 text-theme">{marker.location}</p>
                                            </div>
                                            <div className="d-flex">
                                                <p className="flex-50 text-gray">Last Reported At:</p>
                                                <p className="flex-50 text-theme">{convertTimestampToDate(marker.lastReportedAt)}</p>
                                            </div>
                                        </div>
                                        {props.widgetProperties && props.widgetProperties.detailPage &&
                                            <div className="text-right">
                                                <button className="btn-link" type="button" onClick={() => props.history.push(getLinkedPageUrl(marker.id))}>
                                                    See More<i className="fas fa-caret-right mr-l-5"></i>
                                                </button>
                                            </div>
                                        }
                                    </div>
                                </InfoWindow>}
                        </Marker>
                        )
                })}
            </MarkerClusterer>
        </GoogleMap >)
}
);


export default PduMapWithMarkerCluster;
