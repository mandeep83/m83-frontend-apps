/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * TagsWithSuggestions
 *
 */

import React, { useState, useEffect } from "react";
import PropTypes from 'prop-types';
// import styled from 'styled-components';
import SearchBarWithSuggestion from "../../components/SearchBarWithSuggestion";

function TagsWithSuggestions(props) {
    let { selectedTags, suggestionData, suggestionKeys, onAddition, disabled, placeholder, validationRegex, allowOnlyUnique, allowNewEntries, showNotification, autoFocus } = props
    const [state, setState] = useState({
        searchBarValue: "",
        suggestions: []
    })

    useEffect(() => {
        if (!props.isLoading && !props.suggestionData.length && state.searchBarValue) {
            setState({
                ...state,
                unmatchedKeyword: state.searchBarValue
            })
        }
    }, [props.isLoading])

    useEffect(() => {
        document.addEventListener('mousedown', handleInputBoxBlur);
        return () => {
            document.removeEventListener('mousedown', handleInputBoxBlur);
        }
    }, [])

    let handleInputBoxBlur = (event) => {
        if (event.target.className === "search-wrapper-multi") {
            document.getElementById("____suggestionSearchBox").focus()
        }
        else if (!event.target.id.startsWith("__suggestion")) {
            if (props.fetchSuggestionsFromServer) {
                setState((oldState) => ({
                    ...oldState,
                    blurred: true
                }))
                props.fetchSuggestions("")
            } else {
                setState((oldState) => ({
                    ...oldState,
                    suggestions: [],
                    blurred: true
                }))
            }
        }
    }

    let removeTag = (tag) => {
        inputBoxRef.current.focus()
        let tagToBeDeletedIndex = selectedTags.indexOf(tag)
        let tags = [...selectedTags]
        tags.splice(tagToBeDeletedIndex, 1)
        onAddition(tags)
    }

    let getSuggestions = (value) => {
        let suggestions = []
        suggestionData.forEach(item => {
            let itemValues = suggestionKeys.map(key => item[key])
            let matchingValue = itemValues.find(itemValue => itemValue && itemValue.toLowerCase().includes(value.toLowerCase()) && !suggestions.includes(itemValue))
            if (matchingValue) {
                suggestions.push(matchingValue)
            }
        })
        return suggestions
    }
    
    let onChangeHandler = (value) => {
        setState({
            searchBarValue: value,
            suggestions: value ? getSuggestions(value) : []
        })
    }
    
    let onTagSubmission = (tag) => {
        let isRegexTestFailed = validationRegex && !validationRegex.regex.test(tag)
        let isNotUniqueTag = allowOnlyUnique && selectedTags.some(tags => tags.toLowerCase() == tag.toLowerCase())
        if (isRegexTestFailed || isNotUniqueTag) {
            let errorMessage = isNotUniqueTag ? "Same entry already exists." : validationRegex.validationMessage || "Invalid entry"
            return showNotification("error", errorMessage)
        }
        setState({
            searchBarValue: "",
            suggestions: []
        })
        let tags = Array.isArray(selectedTags) ? [...selectedTags, tag] : [tag]
        inputBoxRef.current.focus()
        onAddition(tags)
    }
    
    let fetchSuggestions = (value) => {
        let unmatchedKeyword = state.unmatchedKeyword ? value.startsWith(state.unmatchedKeyword) ? state.unmatchedKeyword : "" : ""
        setState({
            searchBarValue: value,
            unmatchedKeyword
        })
        if (!unmatchedKeyword) props.fetchSuggestions(value)
    }
    
    let inputChangeHandler = ({ currentTarget }) => {
        props.fetchSuggestionsFromServer ? fetchSuggestions(currentTarget.value) : onChangeHandler(currentTarget.value)
    }
    
    let onKeyUpHandler = (event) => {
        allowNewEntries && event.currentTarget.value && event.key == "Enter" && onTagSubmission(event.currentTarget.value)
    }
    
    let suggestions = props.fetchSuggestionsFromServer ? props.allowOnlyUnique && props.selectedTags && props.selectedTags.length ? props.suggestionData.filter(suggestion =>  !props.selectedTags.includes(suggestion)) : props.suggestionData : state.suggestions
    
    let inputBoxRef = React.createRef()
    
    return (
        <React.Fragment>
            <div className="search-wrapper-multi">
                {Array.isArray(selectedTags) && selectedTags.map((tag, index) =>
                    <span className="badge badge-pill" key={index}>{tag}
                        <button type="button" className="btn" onClick={() => removeTag(tag)}><i className="far fa-times"></i></button>
                    </span>
                )}
                <input type="text"
                    id="____suggestionSearchBox"
                    className="form-control"
                    disabled={disabled}
                    placeholder={placeholder || ""}
                    autoFocus={autoFocus}
                    autoComplete="off"
                    ref={inputBoxRef}
                    value={state.searchBarValue}
                    onChange={inputChangeHandler}
                    onKeyUp={onKeyUpHandler}
                />
            </div>

            {(state.searchBarValue.length > 0 && !state.blurred) && <div id="stre" className="search-wrapper-suggestion">
                <ul className="list-style-none">
                    {props.isLoading ?
                        <li id="__suggestionFetching">Fetching..</li> : suggestions.length ?
                            suggestions.map((suggestion, index) =>
                                <li key={index} id={"__suggestion" + index} onClick={() => onTagSubmission(suggestion)}>{suggestion}</li>
                            ) : <li id="__suggestionNotFound">No Matches Found. {allowNewEntries ? "Press enter to add this tag" : ""}</li>}
                </ul>
            </div>}
        </React.Fragment>
    );
}

TagsWithSuggestions.propTypes = {
    selectedTags: PropTypes.array,
};

export default TagsWithSuggestions;
