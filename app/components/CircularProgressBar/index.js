/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import React from "react";
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

import { FormattedMessage } from "react-intl";
import messages from "./messages";
import { CircularProgressbar, buildStyles } from 'react-circular-progressbar';

function CircularProgressBar(props) {
  
  let count = (props.count && props.count.value) ? props.count.value.toString() : '0'
  
  return (
    <React.Fragment>
          <div className="progressBarWidget">
            <CircularProgressbar className="circleProgressBar" value={count } text={ count } />
          </div>
    </React.Fragment>
  );
}

CircularProgressBar.propTypes = {};

export default CircularProgressBar;
