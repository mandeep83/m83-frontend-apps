/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import React from 'react';

class MessageModal extends React.Component {
    state = {
        timeOutCounter: 0,
    }

    componentDidMount() {
        setTimeout(() => this.props.onClose(), 5000);
    }

    render() {
        return (
            <React.Fragment>
                <div className="outerMessageBox">
                    {Array.isArray(this.props.message2) ?
                        this.props.message2.map((val, index) => (
                            <div style={{ animation: `successMessage ${index + 1}s forwards` }} className="messageModal bg-red mr-b-10" key={index}>
                                <i className="fas fa-times-circle" />
                                <h6>{val}</h6>
                                <button className="btn btn-light" onClick={() => {this.props.onClose(index)}}><i className="far fa-times"></i></button>
                            </div>
                        ))
                        :
                        <div className={this.props.type == 'error' ? "messageModal bg-red" : "messageModal bg-lightGreen"}>
                            <i className={this.props.type == 'error' ? "fas fa-times-circle" : "fas fa-check-circle"} />
                            <h6>{this.props.message2}</h6>
                            <button className="btn btn-light" onClick={() => {this.props.onClose()}}><i className="far fa-times"></i></button>
                        </div>
                    }
                </div>
            </React.Fragment>
        );
    }
}

MessageModal.propTypes = {};

export default MessageModal;
