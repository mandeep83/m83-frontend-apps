/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * SearchBox
 *
 */

import React from "react";
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

function SearchBox({onChangeHandler, value, isDisabled}) {
  return <div className="search-box">
    <span className="search-icon"><i className="far fa-search"></i></span>
    <input type="text" className="form-control" placeholder="Search..." value={value} onChange={(e) => onChangeHandler(e.currentTarget.value)} disabled={isDisabled} />
    {value.length > 0 &&
      <button className="search-button" onClick={() => onChangeHandler("")}><i className="far fa-times"></i></button>
    }
  </div>;
}

SearchBox.propTypes = {};

export default SearchBox;
