/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import React from "react";

/* eslint-disable react/prefer-stateless-function */
class NotFoundPage extends React.Component {
    render() {
        let navUrl
        const sideNav = JSON.parse(localStorage.getItem('sideNav'))
        if (sideNav) {
            if (localStorage.getItem('role') !== "SYSTEM_ADMIN") {
                if (localStorage.getItem('selectedProjectId')) {
                    navUrl = sideNav.length > 0 ? sideNav[0].subMenus.length > 0 ? sideNav[0].subMenus[0].url : sideNav[0].url : '';
                }
                else {
                    navUrl = 'projects'
                }
            }
            else {
                navUrl = sideNav.length > 0 ? sideNav[0].subMenus.length > 0 ? sideNav[0].subMenus[0].url : sideNav[0].url : '';
            }
        }
        return (
            <div className="error-box">
                <div className="error-content">
                    <h5>Oops! page not found</h5>
                    <h1>
                        <span>4</span>
                        <span>0</span>
                        <span>4</span>
                    </h1>
                    <h6>We are sorry, but the page you requested <br /> was not found</h6>
                    {sideNav && <button className="btn btn-primary" onClick={() => this.props.history.push("/" + navUrl)}><i className="far fa-angle-double-left mr-r-5" />Return</button>}
                </div>
            </div>
        );
    }
}

NotFoundPage.propTypes = {};

export default NotFoundPage;
