/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * WidgetConfigWindow
 *
 */
import React from "react";
import cloneDeep from 'lodash/cloneDeep';
import startCase from 'lodash/startCase';
import range from 'lodash/range';
import ReactSelect from 'react-select';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';
import ColorPicker from "../../components/ColorPicker";
import { subscribeTopic, unsubscribeTopic } from "../../mqttConnection";
import { getVMQCredentials, getMinMax, getTopicName } from '../../commonUtils';
import {
    invokeApiHandler,
    getChartDataFormat,
    getMqttChartData,
    mqttDetailsValidation,
    getActiveClass,
    getCustomValues,
    getApiTableData,
    commonParamChangeHandler,
    validateVisibility,
    isParentFalse,
    getOptionsArr,
    getReactSelectValue
} from "../../containers/CreateOrEditPage/pageStudioUtils"
import FontAwesomeDisplayList from "../../components/FontAwesomeDisplayList";
import * as pageStudioConstants from "../../containers/CreateOrEditPage/commonConstants"

let { CUSTOMIZATION_WIDGETS, MQTT_ENABLED_WIDGETS, WIDGET_WITHOUT_DATASOURCE, WIDGETS_WITHOUT_HEADER, API_COLLECTION_WIDGETS } = pageStudioConstants
var flatten = require('flat');

function WidgetConfigWindow(props) {
    let { setState, state, invokeApi, mqttMessageHandler, setWrapperRef, toggleColorPicker } = props
    let configWidget = state.addedWidgets[state.configWidgetIndex]
    let configWidgetType = configWidget.widgetType
    let configWidgetIndex = state.configWidgetIndex

    let getApiObj = (apiId, configWidgetType) => {
        if (!apiId)
            return state.allGatewayApis.find((api) => (api.category === configWidgetType || (api.category === "lineChart" && configWidgetType === "lineChartMultiAxes")))
        return state.allGatewayApis.find((api) => api.id === apiId)
    }

    let getUpdatedCustomValues = (widgetData, customValues, configWidgetType) => {
        let isLineOrBarChart = configWidgetType === "barChart" || configWidgetType === "lineChart" || configWidgetType === "lineChartMultiAxes";
        if (isLineOrBarChart) {
            let dataIndependentKeys = configWidgetType === "barChart" ? ["barWidth", "legendPlacement", "chartAngle", "chartDepth", "columnTopRadius", "customColors"] : ["lineThickness", "legendPlacement", "bulletVisibility", "bulletSize", "customColors", "scrollBarVisibility", "lineVisibility", "valueAxisMax", "valueAxisMin", "valueAxisGridDistance", "customizedValueAxis"];
            Object.keys(customValues).map(key => {
                if (!dataIndependentKeys.includes(key)) {
                    delete customValues[key]
                }
            })
            widgetData && widgetData.availableAttributes && widgetData.availableAttributes.map(({ attr, attrDisplayName }) => {
                customValues[attr] = {
                    value: "#0ba6ef",
                    type: "color",
                    parent: "customColors",
                    displayName: attrDisplayName
                }
            })
        }
        return customValues
    }

    let dataSourceChangeHandler = (dataSource) => {
        let addedWidgets = cloneDeep(state.addedWidgets),
            configWidget = addedWidgets[state.configWidgetIndex],
            requiredWidget = state.allWidgets.find(variant => variant.category === configWidget.widgetType),
            configWidgetType = configWidget.widgetType,
            isControlWidget = configWidgetType === "control",
            controlCommands = cloneDeep(state.controlCommands),
            subscribedTopics = cloneDeep(state.subscribedTopics)
        configWidget.widgetData = dataSource === "DEFAULT" ? requiredWidget.data : null
        configWidget.error = dataSource === "DEFAULT" ? false : configWidget.error
        configWidget.widgetDetails.customValues = getUpdatedCustomValues(configWidget.widgetData, configWidget.widgetDetails.customValues, configWidgetType)
        if (dataSource === "API") {
            let isApiCollectionWidget = API_COLLECTION_WIDGETS.includes(configWidgetType)
            configWidget.apiDetails = isApiCollectionWidget ? getApiObj(configWidget.apiId, configWidgetType) : {}
            configWidget.apiId = isApiCollectionWidget ? configWidget.apiDetails.id : ""
            if (isApiCollectionWidget || isControlWidget) {
                configWidget.apiDetails.pathParameters = getApiTableData(configWidgetType, state.identifier, state.pageType === "aggregate", state.selectedConnector, state.dataSourceValue)
            }
            if (isControlWidget) {
                let requiredConnector = state.connectorsList.find(connector => connector.value === state.selectedConnector)
                controlCommands = requiredConnector.controlConfig ? requiredConnector.controlConfig.map(config => {
                    config.label = config.controlName;
                    config.value = config.controlName;
                    return config
                }) : controlCommands
            }
        } else
            configWidget.apiId = ""

        if (state.dataSource === "MQTT" && configWidget.mqttDetails.topic.deviceId) {
            let topicToUnsubscribe = configWidget.mqttDetails.topic.name
            unsubscribeTopic(topicToUnsubscribe)
            let unsubscribedTopicIndex = subscribedTopics.indexOf(topicToUnsubscribe)
            subscribedTopics.splice(unsubscribedTopicIndex, 1)
        }
        if (dataSource === "MQTT") {
            configWidget.mqttDetails = {
                topic: {
                    deviceId: "",
                    connector: state.selectedConnector,
                    selectedAttributes: configWidgetType === "lineChartMultiAxes" ? [] : {},
                },
                name: "",
                // isLocalTimestamp: !Object.keys(state.messages[this.state.selectedTopic][0]).includes("timestamp")
            }
        }
        configWidget.dataSource = dataSource
        setState({
            ...state,
            addedWidgets,
            selectedAttribute: "_uniqueDeviceId",
            configLoader: false,
            controlCommands,
            isSniffing: false,
            subscribedTopics,
        })
    }

    let configSaveDisablingValidator = () => {
        if (["header", "images"].includes(configWidgetType) || configWidget.dataSource === "DEFAULT") {
            return false
        }
        let val = false;

        const checkMqtt = () => {
            return mqttDetailsValidation(configWidget)
        }

        const checkApi = () => {
            return Boolean(configWidget.apiId && configWidget.widgetData && JSON.stringify(configWidget.widgetData) != "{}" && (!configWidget.apiDetails.pathParameters.some(param => param.required && (!param.value || JSON.stringify(param.value) === "[]"))))
        }

        if (configWidgetType === "control") {
            val = !(configWidget.apiDetails.pathParameters && configWidget.apiDetails.pathParameters.every(param => typeof (param.value) != "undefined"))
        } else {
            val = configWidget.dataSource === "API" ? !checkApi() : !checkMqtt()
        }
        return val;
    }

    let mqttDeviceChangeHandler = (value) => {
        let isSniffing = state.isSniffing
        let messages = cloneDeep(state.messages)
        let addedWidgets = cloneDeep(state.addedWidgets)
        let configWidget = addedWidgets[state.configWidgetIndex]
        let previouslySuscribed = Boolean(configWidget.mqttDetails.topic.deviceId);
        let subscribedTopics = cloneDeep(state.subscribedTopics)
        let notSubscribedByOtherWidget = !state.addedWidgets.some(widget => {
            return widget.dataSource === "MQTT" && widget.mqttDetails.topic.connector === configWidget.mqttDetails.topic.connector && widget.mqttDetails.topic.deviceId === configWidget.mqttDetails.topic.deviceId
        })
        if (previouslySuscribed && notSubscribedByOtherWidget) {
            let topicToUnsubscribe = configWidget.mqttDetails.topic.name
            unsubscribeTopic(topicToUnsubscribe)
            let unsubscribedTopicIndex = subscribedTopics.indexOf(topicToUnsubscribe)
            subscribedTopics.splice(unsubscribedTopicIndex, 1)
        }
        let selectedTopic = getTopicName(configWidget.mqttDetails.topic.connector, value.value, state.connectorsList)
        configWidget.mqttDetails.topic.deviceId = value.value
        configWidget.mqttDetails.topic.name = selectedTopic
        configWidget.mqttDetails.topic.selectedAttributes = configWidgetType === "lineChartMultiAxes" ? [] : {}
        isSniffing = !messages[selectedTopic] || !messages[selectedTopic].length ? selectedTopic : state.isSniffing
        if (!messages[selectedTopic]) {
            messages = { ...messages, [selectedTopic]: [] }
        }
        subscribedTopics.push(selectedTopic)
        let topicSubscription = !state.subscribedTopics.includes(selectedTopic)
        setState({
            selectedTopic,
            messages,
            isSniffing,
            subscribedTopics,
            addedWidgets,
        }, (state) => {
            if (topicSubscription)
                subscribeTopic(selectedTopic, (message, topic) => mqttMessageHandler(message, topic, state))
        })
    }

    let paramChangeHandler = (event, paramIndex, isReactSelect) => {
        let addedWidgets = commonParamChangeHandler(event, paramIndex, isReactSelect, state)
        setState({
            addedWidgets
        })
    }

    let closeConfigWindow = ({ value, name, optionsKey }) => {
        let addedWidgets = cloneDeep(state.addedWidgets)
        addedWidgets[state.configWidgetIndex] = state.tempWidgetData
        setState({
            ...state,
            widgetMenu: true,
            addedWidgets,
            tempWidgetData: null,
            configWidgetIndex: null,
        })
    }

    let saveWidgetConfiguration = ({ value, name, optionsKey }) => {
        setState({
            widgetMenu: true,
            configWidgetIndex: null,
            tempWidgetData: null,
        })
    }

    let widgetDetailsChangeHandler = (key, value) => {
        let addedWidgets = cloneDeep(state.addedWidgets)
        addedWidgets[state.configWidgetIndex].widgetDetails[key] = value
        setState({
            addedWidgets
        })
    }

    let getMqttAttributes = (topic) => {
        let options = [];
        if (state.messages[topic].length > 0) {
            Object.entries(flatten(state.messages[topic][state.messages[topic].length - 1])).filter(([key, value]) => {
                if ((typeof value != "string" || state.addedWidgets[state.configWidgetIndex].widgetType == "table") && key !== "timestamp") {
                    options.push({ label: key, value: key })
                }
            });
        }
        return options
    }

    let mqttAttributesChangeHandler = (value) => {
        let addedWidgets = cloneDeep(state.addedWidgets),
            configWidget = addedWidgets[state.configWidgetIndex],
            configWidgetType = configWidget.widgetType,
            customValues = configWidget.widgetDetails.customValues,
            chartData = getChartDataFormat(configWidgetType),
            isLineOrBarChart = ["lineChartMultiAxes", "lineChart", "barChart"].includes(configWidgetType)
        isLineOrBarChart && configWidget.widgetData && configWidget.widgetData.availableAttributes && configWidget.widgetData.availableAttributes.map(({ attr }) => {
            delete customValues[attr]
        })
        configWidget.isLoadingData = true
        configWidget.widgetData = chartData
        configWidget.mqttDetails.topic.selectedAttributes = value
        chartData = getMqttChartData(value, configWidgetType, chartData, state.messages[state.selectedTopic], state.isLocalTimestamp, configWidget.mqttDetails.topic, state.connectorsList)
        isLineOrBarChart && chartData && chartData.availableAttributes && chartData.availableAttributes.map(({ attr, attrDisplayName }) => {
            customValues[attr] = { value: "#0ba6ef", type: "color", parent: "customColors", displayName: attrDisplayName }
        })
        setState({ ...state, addedWidgets })
    }

    let validateApiCall = () => {
        let addedWidgets = cloneDeep(state.addedWidgets)
        let configWidget = addedWidgets[state.configWidgetIndex]
        let errorMessage = false
        let hasParameters = (configWidget.apiDetails.pathParameters && configWidget.apiDetails.pathParameters.length)
        if (hasParameters) {
            configWidget.apiDetails.pathParameters.map((param, index) => {
                if (!param.value && param.required) {
                    param.errorMessage = true
                    errorMessage = true
                }
            })
        }
        if (errorMessage) {
            setState({
                addedWidgets
            })
        } else {
            configWidget.isLoadingData = true
            invokeApiHandler(configWidget, true, invokeApi, state)
            setState({
                addedWidgets
            })
        }
    }

    let customValuesChangeHandler = ({ value, key }) => {
        let addedWidgets = cloneDeep(state.addedWidgets);
        addedWidgets[state.configWidgetIndex].widgetDetails.customValues[key].value = value
        setState({ addedWidgets })
    }

    let validateParentValue = (key, customValues) => {
        let keyObj = customValues[key]
        let parent = keyObj.parent
        if (!parent) {
            return true
        }
        let parentObj = customValues[keyObj.parent]
        return parentObj.value
    }

    let getHtmlForCustomValues = (key, index, customData, customChangeHandler) => {
        if (!validateParentValue(key, customData)) {
            return null
        }
        switch (key) {
            case "legendPlacement":
                return (
                    <div key={key} className="form-group">
                        <label className="form-group-label">Legend Placement :</label>
                        <div className="button-group">
                            <button type="button" onClick={() => customChangeHandler({ value: "top", key })} className={`btn btn-light ${getActiveClass(customData[key].value, "top")}`}>
                                <i className="fas fa-align-left fa-rotate-90"></i>
                            </button>
                            <button type="button" onClick={() => customChangeHandler({ value: "right", key })} className={`btn btn-light ${getActiveClass(customData[key].value, "right")}`}>
                                <i className="fas fa-align-left fa-rotate-180"></i>
                            </button>
                            <button type="button" onClick={() => customChangeHandler({ value: "bottom", key })} className={`btn btn-light ${getActiveClass(customData[key].value, "bottom")}`}>
                                <i className="fas fa-align-left fa-rotate-270"></i>
                            </button>
                            <button type="button" onClick={() => customChangeHandler({ value: "left", key })} className={`btn btn-light ${getActiveClass(customData[key].value, "left")}`}>
                                <i className="fas fa-align-left"></i>
                            </button>
                        </div>
                    </div>
                )

            case "chartAngle":
                return (
                    <div key={key} className="form-group">
                        <label className="form-group-label">Chart Angle :</label>
                        <div className="text-center">
                            <img src="https://image.flaticon.com/icons/svg/3089/3089808.svg" height="80" />
                        </div>
                    </div>
                )
            default:
                switch (customData[key].type) {
                    case "number":
                        return <div key={key} className={`flex-100 ${index % 2 != 0 ? "pd-l-0" : "pd-r-0"}`} key={index}>
                            <div className="form-group">
                                <label className="form-group-label">{startCase(key)} :</label>
                                <input type="number" className="form-control" id={key} value={customData[key].value}
                                    onChange={(e) => customChangeHandler({
                                        value: isNaN(Number(e.target.value)) || !e.target.value ? "" : Number(e.target.value), key
                                    })
                                    } placeholder={key} name={key}
                                />
                            </div>
                        </div>
                    case "boolean":
                        return <div key={key} className={`flex-100 ${index % 2 != 0 ? "pd-l-0" : "pd-r-0"}`} key={index}>
                            <div className="form-group">
                                <label className="form-group-label">{startCase(key)}
                                    <div className="toggle-switch-wrapper">
                                        <label className="toggle-switch">
                                            <span className="checkText"></span>
                                            <input type="checkbox" id={key} checked={customData[key].value}
                                                onChange={(e) => customChangeHandler({ value: e.target.checked, key })}
                                                placeholder={key} name={key}
                                            />
                                            <span className="toggle-switch-slider"></span>
                                        </label>
                                    </div>
                                </label>
                            </div>
                        </div>
                    case "select":
                        return <div key={key} className={`flex-100 ${index % 2 != 0 ? "pd-l-0" : "pd-r-0"}`} key={index}>
                            <div className="form-group">
                                <label className="form-group-label">{startCase(key)}: </label>
                                <select className="form-control mr-r-8" value={customData[key].value} id={key}
                                    onChange={(e) => customChangeHandler({ value: e.target.value, key })}>
                                    {customData[key].options.map((val, index) => {
                                        return <option key={index} value={val.displayName ? val.value : val} key={index}>{val.displayName || val}</option>
                                    })}
                                </select>
                            </div>
                        </div>
                    case "color":
                        return <div key={key} className={`flex-100 ${index % 2 != 0 ? "pd-l-0" : "pd-r-0"}`} key={index}>
                            <div className="form-group">
                                <label className="form-group-label">{startCase(key)} :</label>
                                <div className="pallet-color-selector">
                                    <ColorPicker
                                        setWrapperRef={(node) => setWrapperRef(node)}
                                        id={key}
                                        toggleColorPicker={() => toggleColorPicker(key)}
                                        isPickerVisible={state.selectedColorPicker === key}
                                        colorChangeHandler={(color, key) => customChangeHandler({
                                            value: color.hex,
                                            key
                                        })}
                                        selectedColor={customData[key].value}
                                    />
                                </div>
                            </div>
                        </div>
                }
        }
    }

    let changeWidgetType = ({ widgetName }) => {
        if (widgetName !== configWidget.widgetName) {
            let addedWidgets = cloneDeep(state.addedWidgets)
            let configWidget = addedWidgets[configWidgetIndex]
            configWidget.widgetName = widgetName
            if (["barChart", "pieChart"].includes(configWidgetType)) {
                configWidget.widgetDetails.customValues = getCustomValues(configWidget.widgetType, configWidget.widgetName, configWidget.widgetData)
            }
            setState({
                addedWidgets
            })
        }
    }

    let getSubCategories = () => {
        let requiredParent = state.allWidgets.find(widget => widget.category === configWidgetType)
        let subCategories = requiredParent.subcategories
        if (subCategories) {
            if (!subCategories.some(category => category.widgetName === requiredParent.widgetName)) {
                subCategories.unshift(cloneDeep(requiredParent))
            }
            return (
                <div className="form-group flex-100">
                    <label className="form-group-label">Chart Type :</label>
                    <ul className="list-style-none d-flex form-attribute-list">
                        {subCategories.map((subCategory, i) => (
                            <li key={i} onClick={() => changeWidgetType(subCategory)}>
                                <div className="form-attribute-item">
                                    <div className="form-attribute-image">
                                        {/*<img src={require('../../assets/images/columnChart.png')} />*/}
                                    </div>
                                    <p>{(subCategory.widgetDisplayName)}</p>
                                </div>
                            </li>
                        ))}
                    </ul>
                </div>)
        }
    }

    let getSelectionRange = (index) => {
        let rangeColors = configWidget.widgetDetails.customValues.rangeColors.colors
        let start = index === 0 ? 1 : rangeColors[index - 1].end + 1;
        let end = index + 1 === rangeColors.length ? 100 : rangeColors[index + 1].end - 1;
        return range(start, end + 1)
    }

    let removeColorRange = (index) => {
        let addedWidgets = cloneDeep(state.addedWidgets)
        addedWidgets[state.configWidgetIndex].widgetDetails.customValues.rangeColors.colors.splice(index, 1)
        setState({
            addedWidgets,
            errorCode: 0
        })
    }

    let getGaugeCustomValuesHTML = () => {
        return (
            <React.Fragment>
                <div className="form-group">
                    <label className="form-group-label">Color Pattern :</label>
                    <div className="d-flex">
                        <div className="flex-50 pd-r-10">
                            <label className="radio-button ">
                                <span className="radio-button-text">Two-Colored</span>
                                <input type="radio" value="twoColors" onChange={gaugeCustomValuesChangeHandler}
                                    name="colorPattern"
                                    checked={configWidget.widgetDetails.customValues.rangeColors.isTwoColored}
                                    id="Default" />
                                <span className="radio-button-mark"></span>
                            </label>
                        </div>
                        <div className="flex-50 pd-l-10">
                            <label className="radio-button">
                                <span className="radio-button-text">Multi-colored</span>
                                <input type="radio" value="multiColored" onChange={gaugeCustomValuesChangeHandler}
                                    name="colorPattern"
                                    checked={!configWidget.widgetDetails.customValues.rangeColors.isTwoColored} />
                                <span className="radio-button-mark"></span>
                            </label>
                        </div>
                    </div>
                </div>
                {state.errorCode === 1 &&
                    <p>The selected color is already used in one of the ranges.</p>
                }
                {configWidget.widgetDetails.customValues.rangeColors.isTwoColored ?
                    <React.Fragment>
                        <div className="flex-100">
                            <div className="form-group">
                                <label className="form-group-label">First Color :</label>
                                <div className="pallet-color-selector">
                                    <ColorPicker
                                        id={`hexCode_0`}
                                        setWrapperRef={setWrapperRef}
                                        toggleColorPicker={() => toggleColorPicker(`hexCode_0`)}
                                        isPickerVisible={state.selectedColorPicker === `hexCode_0`}
                                        colorChangeHandler={gaugeCustomValuesChangeHandler}
                                        selectedColor={configWidget.widgetDetails.customValues.rangeColors.colors[0].hexCode}
                                    />
                                </div>
                            </div>
                        </div>
                        <div className="flex-100">
                            <div className="form-group">
                                <label className="form-group-label">Second Color :</label>
                                <div className="pallet-color-selector">
                                    <ColorPicker
                                        id={`hexCode_1`}
                                        setWrapperRef={(node) => setWrapperRef(node)}
                                        toggleColorPicker={() => toggleColorPicker(`hexCode_1`)}
                                        isPickerVisible={state.selectedColorPicker === `hexCode_1`}
                                        colorChangeHandler={gaugeCustomValuesChangeHandler}
                                        selectedColor={configWidget.widgetDetails.customValues.rangeColors.colors[1].hexCode}
                                    />
                                </div>
                            </div>
                        </div>
                    </React.Fragment> :
                    <React.Fragment>
                        <div className="form-group">
                            <div className="d-flex">
                                <label className="flex-25 form-group-label pd-r-7">Start %</label>
                                <label className="flex-35 form-group-label pd-r-7 pd-l-10">End %</label>
                                <label className="flex-25 form-group-label pd-r-7 pd-l-10">Color</label>
                                <label className="flex-15 form-group-label pd-l-7 pd-r-7">Action</label>
                            </div>
                            {configWidget.widgetDetails.customValues.rangeColors.colors.map((color, i) => (
                                <div className="d-flex" key={color.hexCode}>
                                    <div className="flex-25 pd-r-7">
                                        <div className="form-group">
                                            <input type="number" readOnly className="form-control"
                                                value={i === 0 ? 0 : configWidget.widgetDetails.customValues.rangeColors.colors[i - 1].end}
                                            />
                                        </div>
                                    </div>
                                    <div className="flex-35 pd-r-7 pd-l-7">
                                        <div className="form-group">
                                            <select type="number" max="100" className="form-control" id={`end_${i}`} onChange={gaugeCustomValuesChangeHandler} value={color.end}>
                                                {getSelectionRange(i).map(option => (
                                                    <option key={option}>{option}</option>
                                                ))}
                                            </select>
                                        </div>
                                    </div>
                                    <div className="flex-25 pd-r-7 pd-l-7">
                                        <div className="form-group pallet-color-selector">
                                            <ColorPicker
                                                setWrapperRef={(node) => setWrapperRef(node)}
                                                id={`hexCode_${i}`}
                                                toggleColorPicker={() => toggleColorPicker(`hexCode_${i}`)}
                                                isPickerVisible={state.selectedColorPicker === `hexCode_${i}`}
                                                colorChangeHandler={gaugeCustomValuesChangeHandler}
                                                selectedColor={color.hexCode}
                                            />
                                        </div>
                                    </div>
                                    <div className="flex-15 pd-l-7 pd-r-7">
                                        {!configWidget.widgetDetails.customValues.rangeColors.isTwoColored &&
                                            <button type="button" className="btn-transparent btn-transparent-red mr-t-7"
                                                disabled={i + 1 === configWidget.widgetDetails.customValues.rangeColors.colors.length}
                                                onClick={() => removeColorRange(i)}>
                                                <i className="far fa-trash-alt"></i>
                                            </button>
                                        }
                                    </div>
                                </div>
                            ))}
                        </div>
                        {/*<table className="table table-bordered">*/}
                        {/*    <thead>*/}
                        {/*    <tr>*/}
                        {/*        <th width="30%">Start %</th>*/}
                        {/*        <th width="30%">End %</th>*/}
                        {/*        <th width="30%">Color</th>*/}
                        {/*        <th width="10%">Action</th>*/}
                        {/*    </tr>*/}
                        {/*    </thead>*/}
                        {/*    <tbody>*/}
                        {/*    {configWidget.widgetDetails.customValues.rangeColors.colors.map((color, i) => (*/}
                        {/*        <tr key={color.hexCode}>*/}
                        {/*            <td>*/}
                        {/*                <div className="form-group m-0">*/}
                        {/*                    <input type="number" readOnly className="form-control" value={i === 0 ? 0 : configWidget.widgetDetails.customValues.rangeColors.colors[i - 1].end} />*/}
                        {/*                </div>*/}
                        {/*            </td>*/}
                        {/*            <td>*/}
                        {/*                <div className="form-group m-0">*/}
                        {/*                    <select type="number" max="100" className="form-control" id={`end_${i}`} onChange={gaugeCustomValuesChangeHandler} value={color.end}>*/}
                        {/*                        {getSelectionRange(i).map(option => (*/}
                        {/*                            <option key={option}>{option}</option>*/}
                        {/*                        ))}*/}
                        {/*                    </select>*/}
                        {/*                </div>*/}
                        {/*            </td>*/}
                        {/*            <td className="position-relative">*/}
                        {/*                <ColorPicker*/}
                        {/*                    setWrapperRef={(node) => setWrapperRef(node)}*/}
                        {/*                    id={`hexCode_${i}`}*/}
                        {/*                    toggleColorPicker={() => toggleColorPicker(`hexCode_${i}`)}*/}
                        {/*                    isPickerVisible={state.selectedColorPicker === `hexCode_${i}`}*/}
                        {/*                    colorChangeHandler={gaugeCustomValuesChangeHandler}*/}
                        {/*                    selectedColor={color.hexCode}*/}
                        {/*                />*/}
                        {/*            </td>*/}
                        {/*            <td>*/}
                        {/*                {!configWidget.widgetDetails.customValues.rangeColors.isTwoColored &&*/}
                        {/*                <button type="button" disabled={i + 1 === configWidget.widgetDetails.customValues.rangeColors.colors.length} className="btn btn-transparent btn-transparent-danger mt-2" onClick={() => removeColorRange(i)}>*/}
                        {/*                    <i className="far fa-trash-alt"></i>*/}
                        {/*                </button>*/}
                        {/*                }*/}
                        {/*            </td>*/}
                        {/*        </tr>*/}
                        {/*    ))}*/}
                        {/*    </tbody>*/}
                        {/*</table>*/}
                    </React.Fragment>
                }
            </React.Fragment>
        )
    }

    let getCustomValuesHTML = () => {
        if (configWidgetType === "gaugeChart") {
            return getGaugeCustomValuesHTML()
        } else {
            return Object.keys(configWidget.widgetDetails.customValues).map((key, index) => {
                return getHtmlForCustomValues(key, index, configWidget.widgetDetails.customValues, customValuesChangeHandler)
            })
        }
    }

    let gaugeCustomValuesChangeHandler = (event, param) => {
        let addedWidgets = cloneDeep(state.addedWidgets)
        let customValues = addedWidgets[state.configWidgetIndex].widgetDetails.customValues
        let { currentTarget, hex } = event
        if (currentTarget && currentTarget.name === "colorPattern") {
            let isTwoColored = currentTarget.value === "twoColors"
            customValues.rangeColors.isTwoColored = isTwoColored
            customValues.rangeColors.colors = isTwoColored ? [{ hexCode: "#67b7dc" }, { hexCode: "#6771DC" }] : [{
                hexCode: "#FF0000",
                end: 25
            }, { hexCode: "#67b7dc", end: 50 }, { hexCode: "#32CD32", end: 75 }, { hexCode: "#FF4500", end: 100 }]
        } else {
            if (hex) {
                let colorAlreadyUsed = customValues.rangeColors.colors.some(color => color.hexCode === hex)
                if (colorAlreadyUsed) {
                    return setState({ errorCode: 1 })
                }
            }
            let id = currentTarget ? currentTarget.id : param
            let elementInfo = id.split("_")
            let key = elementInfo[0]
            let index = elementInfo[1]
            customValues.rangeColors.colors[index][key] = key === "end" ? Number(currentTarget.value) : currentTarget ? currentTarget.value : hex
            if (!customValues.rangeColors.isTwoColored) {
                let lastIndex = customValues.rangeColors.colors.length - 1,
                    isIncomplete = customValues.rangeColors.colors[lastIndex].end !== 100
                if (isIncomplete) {
                    customValues.rangeColors.colors.push({
                        hexCode: "",
                        end: 100
                    })
                }
            }
        }
        setState({
            addedWidgets,
            errorCode: 0
        })
    }

    return (
        <div className="form-info-wrapper form-info-wrapper-left ">
            <div className="form-info-header">
                <h5>
                    <button className="btn-close" type="button" onClick={closeConfigWindow}><i className="fas fa-angle-double-left"></i></button>Customize Widget
                </h5>
            </div>
            <div className="form-info-body form-info-body-adjust">
                <ul className="nav nav-tabs d-flex form-info-tab">
                    <li className="nav-item flex-33">
                        <a className="nav-link active" data-toggle="tab" href="#basicInfo"><i className="far fa-credit-card"></i></a>
                    </li>
                    {!WIDGET_WITHOUT_DATASOURCE.includes(configWidgetType) &&
                        <li className="nav-item flex-33">
                            <a className="nav-link" data-toggle="tab" href="#dataSource"><i className="far fa-database"></i></a>
                        </li>
                    }
                    {["deviceMap", "deviceList"].includes(configWidgetType) ?
                        <li className="nav-item flex-33">
                            <a className="nav-link" data-toggle="tab" data-target="#linking"><i className="fal fa-link"></i></a>
                        </li>
                        :
                        <li className="nav-item flex-33">
                            <a className="nav-link" data-toggle="tab" href="#visualization"><i className="far fa-cogs"></i></a>
                        </li>
                    }
                </ul>
                <div className="tab-content">
                    <div className="tab-pane fade show active" id="basicInfo">
                        <div className="page-setting-item">
                            <div className="form-group">
                                <label className="form-group-label">Heading :</label>
                                <input type="text" className="form-control" id="headerName"
                                    value={configWidget.widgetDetails.headerName}
                                    onChange={({ currentTarget }) => widgetDetailsChangeHandler("headerName", currentTarget.value)}
                                />
                            </div>
                            {!WIDGETS_WITHOUT_HEADER.includes(configWidgetType) &&
                                <div className="form-group">
                                    <label className="form-group-label">Sub Heading :
                                        {/*<div className="toggle-switch-wrapper">
                                            <label className="toggle-switch">
                                                <input type="checkbox" name="subHeadingVisibility"
                                                       checked={configWidget.widgetDetails.subHeadingVisibility}
                                                       onChange={({currentTarget}) => widgetDetailsChangeHandler("subHeadingVisibility", currentTarget.checked)}
                                                />
                                                <span className="toggle-switch-slider"></span>
                                            </label>
                                        </div>*/}
                                    </label>
                                    <input type="text" className="form-control" id="cardSubHeading"
                                        value={configWidget.widgetDetails.subHeading}
                                        disabled={!configWidget.widgetDetails.subHeadingVisibility}
                                        onChange={({ currentTarget }) => widgetDetailsChangeHandler("subHeading", currentTarget.value)}
                                    />
                                </div>
                            }
                            <div className="form-group">
                                <label className="form-group-label">Icon :
                                    {/*<div className="toggle-switch-wrapper">
                                        <label className="toggle-switch">
                                            <input type="checkbox" checked={configWidget.widgetDetails.iconVisibility}
                                                   onChange={({currentTarget}) => widgetDetailsChangeHandler("iconVisibility", currentTarget.checked)}
                                            />
                                            <span className="toggle-switch-slider"></span>
                                        </label>
                                    </div>*/}
                                </label>
                                <FontAwesomeDisplayList value={configWidget.widgetDetails.icon}
                                    disabled={!configWidget.widgetDetails.iconVisibility}
                                    changeHandlerForSelect={(icon) => widgetDetailsChangeHandler("icon", icon)}>
                                </FontAwesomeDisplayList>
                                {/*<div className="d-flex">
                                    <div className="flex-70 pd-r-10">
                                    
                                    </div>
                                    <div className="flex-30 pd-l-10">
                                        <div className="pallet-color-selector">
                                            <ColorPicker
                                                setWrapperRef={setWrapperRef}
                                                toggleColorPicker={() => configWidget.widgetDetails.iconVisibility && toggleColorPicker("icon_color")}
                                                isPickerVisible={state.selectedColorPicker === "icon_color"}
                                                colorChangeHandler={(color) => widgetDetailsChangeHandler("iconColor", color.hex)}
                                                selectedColor={configWidget.widgetDetails.iconVisibility ? configWidget.widgetDetails.iconColor : "#D3D3D3"}
                                            />
                                        </div>
                                    </div>
                                </div>*/}
                            </div>
                        </div>
                    </div>

                    <div className="tab-pane fade" id="dataSource">
                        <div className="page-setting-item">
                            <div className="form-group">
                                <label className="form-group-label">Data Source :</label>
                                <div className="d-flex">
                                    <div className="flex-50 pd-r-10">
                                        <label className={`radio-button ${!state.selectedConnector ? "cursor-notAllowed" : ""}`}>
                                            <span className="radio-button-text">API</span>
                                            <input type="radio" value="API" disabled={!state.selectedConnector}
                                                checked={configWidget.dataSource === "API"}
                                                onChange={() => state.selectedConnector && dataSourceChangeHandler("API")}
                                            />
                                            <span className="radio-button-mark"></span>
                                        </label>
                                    </div>
                                    {MQTT_ENABLED_WIDGETS.includes(configWidgetType) &&
                                        <div className="flex-50 pd-l-10">
                                            <label className="radio-button">
                                                <span className="radio-button-text">LIVE</span>
                                                <input type="radio" value="MQTT" checked={configWidget.dataSource === "MQTT"}
                                                    onChange={() => dataSourceChangeHandler("MQTT")} />
                                                <span className="radio-button-mark"></span>
                                            </label>
                                        </div>
                                    }
                                </div>
                            </div>
                            <button disabled={configWidget.dataSource === "DEFAULT"} className="btn btn-primary" onClick={() => dataSourceChangeHandler("DEFAULT")}>
                                Reset
                            </button>

                            {(API_COLLECTION_WIDGETS.includes(configWidgetType) || configWidgetType === "control") && configWidget.dataSource === "API" &&
                                <div className="configTable">
                                    {configWidget.apiDetails.pathParameters.map((parameter, paramIndex) => {
                                        if (validateVisibility(parameter, configWidget)) {
                                            return (
                                                <div className="form-group " key={paramIndex}>
                                                    <label className="form-group-label">{parameter.displayName || parameter.name}</label>
                                                    {/* {Boolean(parameter.required) && <i className="fa fa-asterisk f-8 ml-1 text-red"></i>} */}
                                                    {parameter.type === "Boolean" ?
                                                        <div className="flex">
                                                            <div className="fx-b50 mb-2">
                                                                <label className="radioLabel">True
                                                                <input type="radio" id="required" value="true"
                                                                        checked={parameter.value === "true"}
                                                                        onChange={(e) => paramChangeHandler(e, paramIndex)}
                                                                    />
                                                                    <span className="radioMark" />
                                                                </label>
                                                            </div>
                                                            <div className="fx-b50 mb-2">
                                                                <label className="radioLabel">False
                                                                <input type="radio" id="required" value="false"
                                                                        checked={parameter.value === "false"}
                                                                        onChange={(e) => paramChangeHandler(e, paramIndex)}
                                                                    />
                                                                    <span className="radioMark" />
                                                                </label>
                                                            </div>
                                                            {parameter.errorMessage &&
                                                                <div className="fx-b100"><p className="text-right">Please fill out this field.</p></div>
                                                            }
                                                        </div> :
                                                        parameter.type === "select" ? parameter.isLoading ?
                                                            <div className="text-center">
                                                                <i className="fad fa-sync-alt fa-spin text-primary f-14"></i>
                                                            </div> :
                                                            <React.Fragment>
                                                                <ReactSelect
                                                                    className="form-control-multi-select"
                                                                    isMulti={parameter.name.includes("Csv")}
                                                                    isDisabled={parameter.isDisabled || parameter.parent && isParentFalse(parameter, configWidget)}
                                                                    options={getOptionsArr(parameter, configWidget, state[parameter.optionsKey])}
                                                                    onChange={(value) => paramChangeHandler({
                                                                        value,
                                                                        id: parameter.name
                                                                    }, paramIndex, true)}
                                                                    value={getReactSelectValue(parameter, state[parameter.optionsKey])}
                                                                />
                                                                {parameter.errorMessage && <p className="text-right">Please fill out this field.</p>}
                                                            </React.Fragment> :
                                                            <React.Fragment>
                                                                <input
                                                                    disabled={parameter.isDisabled || (parameter.parent && isParentFalse(parameter, configWidget))}
                                                                    className="form-control"
                                                                    onChange={(e) => paramChangeHandler(e, paramIndex)}
                                                                    required={parameter.required} value={parameter.value} />
                                                                {parameter.errorMessage &&
                                                                    <p className="text-right">Please fill out this field.</p>}
                                                            </React.Fragment>
                                                    }
                                                </div>
                                            )
                                        }
                                    })
                                    }
                                </div>}

                            {configWidget.dataSource === "MQTT" &&
                                <React.Fragment>
                                    <div className="form-group flex align-items-center">
                                        <div className="fx-b25">
                                            <label className="form-label">Device Name/Id :</label> <i className="fa fa-asterisk form-group-required "></i>
                                        </div>
                                        <div className="fx-b75">
                                            <ReactSelect
                                                className="form-control-multi-select"
                                                options={state.devices}
                                                isDisabled={!configWidget.mqttDetails.topic.connector}
                                                onChange={mqttDeviceChangeHandler}
                                                value={state.devices.find(el => el.value === configWidget.mqttDetails.topic.deviceId) || {}}
                                            />
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        {state.isSniffing ?
                                            <div className="fileUploadLoader">
                                                <img src="https://content.iot83.com/m83/misc/uploading.gif" />
                                                <h6>Sniffing...</h6>
                                            </div> :
                                            (Boolean(configWidget.mqttDetails.topic.deviceId && state.messages[state.selectedTopic].length) &&
                                                <div className="flex align-items-center">
                                                    <div className="fx-b25">
                                                        <label className="form-label">Select Attribute :</label> <i className="fa fa-asterisk form-group-required"></i>
                                                    </div>
                                                    <div className="fx-b75">
                                                        <ReactSelect
                                                            className="form-control-multi-select"
                                                            isMulti={configWidgetType === "lineChartMultiAxes"}
                                                            value={configWidget.mqttDetails.topic.selectedAttributes}
                                                            onChange={mqttAttributesChangeHandler}
                                                            options={getMqttAttributes(state.selectedTopic)}>
                                                        </ReactSelect>
                                                    </div>
                                                </div>
                                            )
                                        }
                                    </div>
                                </React.Fragment>
                            }

                            {configWidget.dataSource === "API" &&
                                <div className="form-group">
                                    <label className="form-group-label">Enable auto refresh :
                                        <div className="toggle-switch-wrapper">
                                            <label className="toggle-switch">
                                                <input type="checkbox" checked={configWidget.widgetDetails.autoRefreshing}
                                                    onChange={({ currentTarget }) => widgetDetailsChangeHandler("autoRefreshing", currentTarget.checked)} />
                                                <span className="toggle-switch-slider"></span>
                                            </label>
                                        </div>
                                    </label>
                                </div>
                            }
                        </div>
                    </div>

                    <div className="tab-pane fade" id="visualization">
                        <div className="page-setting-item">
                            <div className="d-flex">
                                {getSubCategories()}
                                {CUSTOMIZATION_WIDGETS.includes(configWidgetType) && getCustomValuesHTML()}
                            </div>
                        </div>
                    </div>

                    <div className="tab-pane fade" id="linking">
                        <div className="config-outer-box">
                            <div className="form-group">
                                <label className="form-group-label">Page :<i
                                    className="fa fa-asterisk form-group-required" />
                                </label>
                                <select className="form-control" value={configWidget.widgetDetails.detailPage}
                                    onChange={(event) => widgetDetailsChangeHandler("detailPage", event.target.value)}>
                                    <option value="">Select</option>
                                    {state.pageList.filter(page => page.id !== props.match.params.id && page.connectorId === state.selectedConnector && page.pageType == "deviceDetail").map(page =>
                                        <option key={page.id} value={page.id}>{page.name}</option>)}
                                </select>
                            </div>
                        </div>
                    </div>

                </div>

                <div className="form-info-footer">
                    {configWidget.dataSource === "API" && configWidgetType !== "control" &&
                        <button className="btn btn-info" disabled={!configWidget.apiId} onClick={validateApiCall}>Preview</button>}
                    <button className="btn btn-primary" disabled={configSaveDisablingValidator()} onClick={saveWidgetConfiguration}>Save</button>
                </div>
            </div>
        </div>
    );
}

WidgetConfigWindow.propTypes = {};

export default WidgetConfigWindow;
