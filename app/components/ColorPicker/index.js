/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * ColorPicker
 *
 */

import React from "react";
import { SketchPicker } from 'react-color';

// import PropTypes from 'prop-types';
// import styled from 'styled-components';

class ColorPicker extends React.Component {
    render() {
        const { toggleColorPicker, isPickerVisible, id, colorChangeHandler, selectedColor, title, setWrapperRef } = this.props;
        return <React.Fragment>
            {title && <label className="form-label">{title} :</label>}
            <div className="pallet-color-wrapper">
                <div className="pallet-color-box" style={{background: selectedColor}} onClick={toggleColorPicker} />
                {isPickerVisible ?
                    <div className="pallet-picker" ref={setWrapperRef}>
                        <SketchPicker color={selectedColor} onChange={(color) => colorChangeHandler(color, id)} />
                    </div> : null
                }
            </div>
        </React.Fragment>;
    }
}

ColorPicker.propTypes = {};

export default ColorPicker;
