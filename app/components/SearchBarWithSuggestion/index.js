/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * SearchBarWithSuggestion
 *
 */

import React from "react";
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

/* eslint-disable react/prefer-stateless-function */
export default class SearchBarWithSuggestion extends React.Component {
    
    state = {
        searchBarValue: "",
        suggestions: []
    }
    
    getSuggestions = (value) => {
        let suggestions = []
        let suggestionKeys = this.props.suggestionKeys || [this.props.suggestionKey]
        this.props.data.forEach(item => {
            let itemValues = suggestionKeys.map(key => item[key])
            let matchingValue = itemValues.find(itemValue => itemValue && itemValue.toLowerCase().includes(value.toLowerCase()) && !suggestions.includes(itemValue))
            if (matchingValue) {
                suggestions.push(matchingValue)
            }
        })
        return suggestions
    }
    
    
    onChangeHandler = (value, isTemporary) => {
        let searchBarValue = value
        let isBeingSearched = searchBarValue && isTemporary
        this.setState({
            searchBarValue,
            suggestions: isBeingSearched ? this.getSuggestions(searchBarValue) : []
        }, () => !isBeingSearched && this.props.onChange(searchBarValue))
    }
    
    clearSearchBox = ()=> {
        this.setState({
            searchBarValue: "",
            suggestions: []
        })
    }
    
    render() {
        return <React.Fragment>
            <div className="search-wrapper-group">
                <input type="text" className="form-control" disabled={this.props.disabled} placeholder="Search..." value={this.state.searchBarValue} onChange={({ currentTarget }) => this.onChangeHandler(currentTarget.value, true)} />
                <span className="search-wrapper-icon"><i className="far fa-search"></i></span>
                <button type="button" disabled={!(this.props.value || this.state.searchBarValue)} className="search-wrapper-button" onClick={() => this.onChangeHandler("")}><i className="far fa-times"></i></button>
            </div>
            {this.state.suggestions.length > 0 &&
            <div className="search-wrapper-suggestion">
                <ul className="list-style-none">
                    {this.state.suggestions.map((suggestion, index) =>
                        <li key={index} onClick={() => this.onChangeHandler(suggestion)}>{suggestion}</li>
                    )}
                </ul>
            </div>
            }
        </React.Fragment>
    }
}

SearchBarWithSuggestion.propTypes = {};

export const clearSearchBox = new SearchBarWithSuggestion().clearSearchBox
