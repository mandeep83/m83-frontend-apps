/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/



import React from "react";
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import am4themes_spiritedaway from "@amcharts/amcharts4/themes/spiritedaway";
import am4themes_moonrisekingdom from "@amcharts/amcharts4/themes/moonrisekingdom";
import am4themes_frozen from "@amcharts/amcharts4/themes/frozen";
import am4themes_kelly from "@amcharts/amcharts4/themes/kelly";
import am4themes_material from "@amcharts/amcharts4/themes/material";
import am4themes_dataviz from "@amcharts/amcharts4/themes/dataviz";
import { createLineFromConfig, createLineFromConfigForMl } from "../../containers/WidgetStudio/lineConfigAndData";
import { createPieFromConfig } from "../../containers/WidgetStudio/pieConfigAndData";
import { createBarFromConfig, createBarFromConfigForMl } from "../../containers/WidgetStudio/barConfigAndData";
import createMiniLineChart from "../../containers/WidgetStudio/createMiniLineChart"
import cloneDeep from 'lodash/cloneDeep';
let chart = {};
/* eslint-disable react/prefer-stateless-function */

class LineChart extends React.Component {
    state = {
        chartCreationCount: 0
    }

    setTheme = (theme) => {
        let allThemes = {
            "spiritedAway": am4themes_spiritedaway,
            "moonriseKingdom": am4themes_moonrisekingdom,
            "frozen": am4themes_frozen,
            "kelly's": am4themes_kelly,
            "material": am4themes_material,
            "dataviz": am4themes_dataviz
        };
        am4core.addLicense("CH145187641");
        Object.entries(allThemes).map(([name, am4theme]) => {
            if (name === theme)
                am4core.useTheme(am4theme)
            else
                am4core.unuseTheme(am4theme)
        })
    }

    createChart = ({ theme, widgetType, widgetName, widgetData, i, widgetDetails }, deleteLegend) => {
        widgetData = cloneDeep(widgetData);
        if (chart[i]) {
            chart[i].dispose();
        }
        let customValues = widgetDetails.customValues;
        let config = this.getWidgetTypeConfig(widgetType, widgetName, widgetData, i, deleteLegend, customValues, theme);
        this.setTheme(theme);
        if (widgetType === "barChart") {
            config.series.map(el => {
                el.columns.template.adapter = {
                    "fill": (fill, target) => {
                        let color = customValues && customValues.customColors.value ? customValues[el.id].value : chart[i].colors.getIndex(target.dataItem.index)
                        return color;
                    }
                }
            })
        }
        am4core.options.queue = true;
        chart[i] = am4core.createFromConfig(config);
        if (widgetName === "multiAxisChart") {
            chart[i].yAxes.values.map((val, index) => {
                val.renderer.line.stroke = customValues.customColors && customValues.customColors.value ? chart[i].series.values[index].stroke : chart[i].colors.getIndex(index)
            })
        }
        if (customValues && customValues.legendPlacement && ((["right", "left"]).includes(customValues.legendPlacement.value))) {
            chart[i].legend.width = widgetType === "pieChart" ? 175 : 75;
        }
        this.setState(prevState => ({ chartCreationCount: prevState.chartCreationCount + 1 }))
    }

    getWidgetTypeConfig = (selectedWidget, widgetType, chartData, id, deleteLegend, customValues, theme) => {
        chartData = cloneDeep(chartData);
        switch (selectedWidget) {
            case "lineChart":
            case "lineChartMultiAxes":
                return createLineFromConfig(widgetType, chartData, customValues, id, deleteLegend);
            case "insights":
                return createMiniLineChart(chartData, id)
            case "lineChartForMl":
                return createLineFromConfigForMl(widgetType, chartData, customValues, id, deleteLegend);
            case "pieChart":
                return createPieFromConfig(widgetType, chartData, customValues, id);
            case "barChart":
                return createBarFromConfig(widgetType, chartData, customValues, id, deleteLegend, theme);
            case "barChartForMl":
                return createBarFromConfigForMl(widgetType, chartData, customValues, id, deleteLegend, theme);
            case "sankeyChart":
                return createSankeyFromConfig(widgetType, chartData, id);
            case "radarChart":
                return createRadarFromConfig(widgetType, chartData, id);
            case "xyBubbleChart":
                return createXYchartFromConfig(widgetType, chartData, id);
        }
    }

    componentDidMount() {
        this.createChart(this.props.data, this.props.deleteLegend);
    }

    alignLegend = (customValues, legendWidth) => {
        chart[this.props.data.i].legend.position = customValues ? customValues.legendPlacement.value : "bottom";
        if (customValues && (customValues.legendPlacement.value === "left" || customValues.legendPlacement.value === "right")) {
            chart[this.props.data.i].legend.width = legendWidth;
            chart[this.props.data.i].legend.align = customValues.legendPlacement.value
        }
    }

    setChartAngleAndDepth = (customValues) => {
        chart[this.props.data.i].depth = Number(customValues.chartDepth.value);
        chart[this.props.data.i].angle = Number(customValues.chartAngle.value)
    }

    linecolorChanged = (customValues, prevCustomValues) => {
        customValues = cloneDeep(customValues)
        prevCustomValues = cloneDeep(prevCustomValues)
        let propertiesToBedeleted = ["lineThickness", "legendPlacement", "bulletVisibility", "bulletSize", "customColors"]
        propertiesToBedeleted.map(property => {
            delete customValues[property]
            delete prevCustomValues[property]
        })
        return JSON.stringify(customValues) != JSON.stringify(prevCustomValues)
    }

    handleCustomPropsChange = (data, prevData) => {
        let { widgetDetails, widgetType, widgetName } = data
        let prevCustomValues = prevData.widgetDetails.customValues,
            customValues = widgetDetails.customValues
        switch (widgetType) {
            case "lineChartMultiAxes":
            case "lineChart":
                let bulletsChanged = (prevCustomValues.bulletVisibility.value !== customValues.bulletVisibility.value) || (prevCustomValues.bulletSize.value !== customValues.bulletSize.value)
                let lineColorChanged = (prevCustomValues.customColors.value !== customValues.customColors.value || this.linecolorChanged(customValues, prevCustomValues))
                let scrollBarVisibilityChanged = prevCustomValues.scrollBarVisibility && (prevCustomValues.scrollBarVisibility.value !== customValues.scrollBarVisibility.value)
                if (bulletsChanged || lineColorChanged || scrollBarVisibilityChanged) {
                    this.createChart(data)
                    return
                }
                chart[data.i].series.values.map(val => {
                    val.strokeWidth = customValues.lineVisibility.value ? Number(customValues.lineThickness.value) : 0;
                })
                this.alignLegend(customValues, 80)
                break;
            case "barChart":
                let colorCustomizationRemoved = (customValues.customColors.value !== prevCustomValues.customColors.value),
                    barWidthChanged = (customValues.barWidth.value !== prevCustomValues.barWidth.value),
                    isCylinderical = widgetName === "3DCylinderChart"
                if (colorCustomizationRemoved || barWidthChanged || isCylinderical) {
                    this.createChart(data)
                    return;
                }
                let columnRadius = customValues.columnTopRadius ? Number(customValues.columnTopRadius.value) : 10;
                chart[data.i].series.values.map(value => {
                    value.columns.values.map((column, index) => {
                        data.widgetName === "basicBarChart" && column.children.values[0].cornerRadius(columnRadius, columnRadius, 0, 0)
                        let color = customValues.customColors.value ? customValues[value.id].value : chart[data.i].colors.getIndex(index)
                        column.fill = color
                    })
                    if (customValues.customColors.value) {
                        value.columns.template.setFill(customValues[value.id].value)
                    }
                })
                this.alignLegend(customValues, 80)
                if (widgetName === "3DColumnChart") {
                    this.setChartAngleAndDepth(customValues, 30, 30)
                }
                break;
            case "pieChart":
                this.alignLegend(customValues, 175)
                if (widgetName === "3dPieChart") {
                    this.setChartAngleAndDepth(customValues)
                }
                break;
            default:
                null
        }

    }

    shouldComponentUpdate(nextProps, nextState) {
        let areDimensionsChanged = nextProps.dimensions && nextProps.dimensions !== this.props.dimensions;
        if ((nextState.chartCreationCount !== this.state.chartCreationCount) || areDimensionsChanged) {
            return true
        }
        let isThemeChanged = nextProps.data.theme !== this.props.data.theme,
            attributesChanged = (nextProps.data.widgetData.availableAttributes && JSON.stringify(nextProps.data.widgetData.availableAttributes) !== JSON.stringify(this.props.data.widgetData.availableAttributes)) || (nextProps.data.widgetData.seriesName !== this.props.data.widgetData.seriesName),
            unitsChanged = JSON.stringify(nextProps.data.widgetData.units) !== JSON.stringify(this.props.data.widgetData.units),
            dataChanged = JSON.stringify(nextProps.data.widgetData) !== JSON.stringify(this.props.data.widgetData),
            chartChanged = nextProps.data.widgetName !== this.props.data.widgetName,
            customPropsChanged = (JSON.stringify(nextProps.data.widgetDetails.customValues) !== JSON.stringify(this.props.data.widgetDetails.customValues));

        if (isThemeChanged || attributesChanged || chartChanged) {
            this.createChart(nextProps.data, nextProps.legend);
            return true
        }
        if (customPropsChanged) {
            this.handleCustomPropsChange(nextProps.data, this.props.data)
            return true
        }
        else {
            if (dataChanged) {
                chart[nextProps.data.i].data = nextProps.data.widgetType === "pieChart" ? nextProps.data.widgetData : nextProps.data.widgetData.chartData;
            }
            if (unitsChanged) {
                chart[nextProps.data.i].series.values.map((series, index) => {
                    let attr = nextProps.data.widgetData.availableAttributes[index],
                        chartData = nextProps.data.widgetData
                    series.tooltipText = (this.props.data.widgetType === "multiAxisChart" ? attr.attrDisplayName : chartData.seriesName) + " : {" + attr.attr + "} " + (chartData.units && chartData.units[attr.attr] ? chartData.units[attr.attr] : "")
                })
            }
        }

        return false
    }

    componentWillUnmount() {
        if (chart[this.props.data.i]) {
            chart[this.props.data.i].dispose();
            delete chart[this.props.data.i];
        }
    }

    render() {
        return (
            <div id={this.props.data.i} className="widget-chart-box"></div>
        )
    }
}

LineChart.propTypes = {};

export default LineChart;
