/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * WidgetsMenu
 *
 */

import React from "react";
import ColorPicker from "../ColorPicker"
import ConfirmModel from '../../components/ConfirmModel/Loadable'
import { getCustomValues, getActiveClass } from "../../containers/CreateOrEditPage/pageStudioUtils";
import * as pageStudioConstants from "../../containers/CreateOrEditPage/commonConstants";
import cloneDeep from "lodash/cloneDeep";
let { PAGE_TYPE_OPTIONS, THEMES } = pageStudioConstants
// import PropTypes from 'prop-types';
// import styled from 'styled-components';
function WidgetsMenu(props) {
    let { state, addWidgetHandler, setState, savePage, connectorChangeHandler, setWrapperRef, toggleColorPicker, pageTypeChangeHandler, resetWidgetToDefaultSettings } = props
    let savePageHandler = (event) => {
        event.preventDefault();
        let addedWidgets = JSON.parse(JSON.stringify(state.addedWidgets));
        let requiredJson = {
            name: state.pageName.trim(),
            pageSettings: state.pageSettings,
            description: state.description,
            connectorId: state.selectedConnector,
            duration: state.selectedDuration,
            pageType: state.pageType,
            widgets: addedWidgets.map(widget => {
                if (widget.dataSource !== "CUSTOM") {
                    widget.widgetData = {}
                }
                widget.customWidgetDetails = {};
                let pathParameters;
                if (widget.widgetType !== "gaugeChart") {
                    let customValues = {};
                    Object.entries(widget.widgetDetails.customValues).map(([key, val]) => {
                        customValues[key] = val.value
                    })
                    widget.widgetDetails.customValues = customValues
                }
                if (widget.apiDetails && widget.apiDetails.pathParameters && widget.apiDetails.pathParameters.length) {
                    let isDefaultChanged = widget.apiDetails.pathParameters.some(el => el.value && el.value != el.defaultValue);
                    pathParameters = isDefaultChanged ? widget.apiDetails.pathParameters : null
                }
                widget.apiDetails = {};
                if (pathParameters) widget.apiDetails.pathParameters = pathParameters
                delete widget.data;
                delete widget.error;
                return widget;
            })
        }
        setState({
            isLoading: 1,
        }, (state) => {
            savePage(requiredJson, props.match.params.id)
        })
    }

    const getActiveClassName = (name, key) => {
        return state.addedWidgets.some(item => item[key] === name) ? "active" : ""
    }

    let validatePageTypeChange = ({ currentTarget }) => {
        let tempPageType = currentTarget.value
        if (state.addedWidgets.length) {
            setState({
                tempPageType,
            }, (state) => {
                let confirmationMessage = "the previously set page type. All the added widgets would be deleted. Continue";
                props.showConfirmationModal(confirmationMessage, pageTypeChangeHandler)
            })
        }
        else
            pageTypeChangeHandler(tempPageType)
    }

    let validateConnectorChange = ({ currentTarget }) => {
        let connectorLinkedToWidget = state.addedWidgets.some(widget => widget.dataSource !== "DEFAULT")
        if (connectorLinkedToWidget) {
            setState({
                tempConnector: currentTarget.value,
            }, () => {
                let confirmationMessage = "the previously selected Device Type. All the widgets would be restored to the their default settings. Continue ?";
                props.showConfirmationModal(confirmationMessage, resetWidgetToDefaultSettings)
            })
        }
        connectorChangeHandler(currentTarget.value)
    }

    let pageDetailsChangeHandler = ({ currentTarget }) => {
        let key = currentTarget.id
        let value = currentTarget.value
        setState({
            [key]: value
        })
    }


    let pageSettingsChangeHandler = (value, details) => {
        let pageSettings = cloneDeep(state.pageSettings);
        let detailsArr = details.split("_");
        let settingskey = detailsArr[0];
        let subkey = detailsArr[1];
        pageSettings[settingskey][subkey] = value;
        setState({
            pageSettings
        })
    }

    let themeChangeHandler = (theme) => {
        let pageSettings = cloneDeep(state.pageSettings)
        pageSettings.theme = theme
        setState({
            pageSettings
        })
    }

    let html = <div className="form-info-wrapper form-info-wrapper-left">
        <div className="form-info-header">
            <h5>Basic Information</h5>
        </div>
        <div className="form-info-body form-info-body-adjust">
            <ul className="nav nav-tabs d-flex form-info-tab">
                <li className="nav-item flex-33">
                    <a className="nav-link active" data-toggle="tab" href="#info"><i className="far fa-window"></i></a>
                </li>
                <li className={"nav-item flex-33"}>
                    <a className={`nav-link ${state.selectedConnector ? "" : "cursor-not-allowed"}`} data-toggle="tab" href={state.selectedConnector ? "#widgets" : ""}><i className="far fa-grip-horizontal"></i></a>
                </li>
                <li className="nav-item flex-33">
                    <a className="nav-link" data-toggle="tab" href="#setting"><i className="far fa-cogs"></i></a>
                </li>
            </ul>

            <div className="tab-content">
                <div className="tab-pane fade show active" id="info">
                    <div className="form-group">
                        <label className="form-group-label">Device Type : <i className="fas fa-asterisk form-group-required"></i></label>
                        <select className="form-control" disabled={state.isDisabledTopic || state.isConnectorAttached}
                            value={state.selectedConnector} onChange={validateConnectorChange}>
                            <option value="">Select</option>
                            {state.connectorsList && state.connectorsList.map((connector, index) => {
                                return <option value={connector.value} key={index}>{connector.label}</option>
                            })}
                        </select>
                    </div>
                    <div className="form-group">
                        <label className="form-group-label">Dashboard Type : <i className="fas fa-asterisk form-group-required"></i></label>
                        <select className="form-control" value={state.pageType} onChange={validatePageTypeChange}
                            required>
                            {Object.entries(PAGE_TYPE_OPTIONS).map(([type, displayName], index) => {
                                return <option value={type} key={index}>{displayName}</option>
                            })}
                        </select>
                    </div>
                    <div className="form-group">
                        <label className="form-group-label">Name : <i className="fas fa-asterisk form-group-required"></i></label>
                        <input type="text" className="form-control" id="pageName" value={state.pageName} onChange={pageDetailsChangeHandler} />
                    </div>
                    <div className="form-group">
                        <label className="form-group-label">Description :</label>
                        <textarea rows="3" className="form-control" id="description" value={state.description} onChange={pageDetailsChangeHandler} />
                    </div>
                </div>

                <div className="tab-pane fade" id="widgets">
                    <label>{`Available ${state.allWidgets.length && state.allWidgets[0].isPage ? "Variants" : "Widgets"} for ${PAGE_TYPE_OPTIONS[state.pageType]} :`}</label>
                    <ul className="list-style-none d-flex form-attribute-list">
                        {state.allWidgets && state.allWidgets.length ? state.allWidgets.map((widget, index) =>
                            <li className={state.addedWidgets.some(item => item.widgetName === widget.widgetName) ? "active " : ""} onClick={() => !widget.disabled && addWidgetHandler(widget.widgetName)} key={index}>
                                <div className={`form-attribute-item ${widget.disabled ? "cursor-not-allowed" : "cursor-pointer"}`}>
                                    <div className="form-attribute-image">
                                        <img src={`https://content.iot83.com/m83/charts/${widget.imageName}`} />
                                    </div>
                                    <p>{widget.widgetDisplayName}</p>
                                </div>
                            </li>
                        ) :
                            <div className="form-attribute-list-empty">
                                <p>No widgets found.</p>
                            </div>
                        }
                    </ul>
                </div>

                <div className="tab-pane fade" id="setting">
                    {/*<div className="page-setting-item">
                        <h6>Widget Heading</h6>
                        <div className="form-group">
                            <label className="form-group-label">Size :</label>
                            <ol className="list-style-none d-flex size-list font-size-list">
                                <li onClick={() => pageSettingsChangeHandler(12, "header_fontSize")} className={getActiveClass(state.pageSettings.header.fontSize, 12)}><span></span>12</li>
                                <li onClick={() => pageSettingsChangeHandler(14, "header_fontSize")} className={getActiveClass(state.pageSettings.header.fontSize, 14)}><span></span>14</li>
                                <li onClick={() => pageSettingsChangeHandler(16, "header_fontSize")} className={getActiveClass(state.pageSettings.header.fontSize, 16)}><span></span>16</li>
                                <li onClick={() => pageSettingsChangeHandler(18, "header_fontSize")} className={getActiveClass(state.pageSettings.header.fontSize, 18)}><span></span>18</li>
                                <li onClick={() => pageSettingsChangeHandler(20, "header_fontSize")} className={getActiveClass(state.pageSettings.header.fontSize, 20)}><span></span>20</li>
                                <li onClick={() => pageSettingsChangeHandler(22, "header_fontSize")} className={getActiveClass(state.pageSettings.header.fontSize, 22)}><span></span>22</li>
                            </ol>
                        </div>
                        <div className="form-group d-flex">
                            <div className="flex-60 pd-r-10">
                                <label className="form-group-label">Weight :</label>
                                <select className="form-control" value={state.pageSettings.header.fontWeight} onChange={(e) => pageSettingsChangeHandler(e.currentTarget.value, "header_fontWeight")}>
                                    <option value={300}>300</option>
                                    <option value={400}>400</option>
                                    <option value={600}>600</option>
                                    <option value={700}>700</option>
                                    <option value={800}>800</option>
                                </select>
                            </div>
                            <div className="flex-40 pd-l-10">
                                <label className="form-group-label">Color :</label>
                                <div className="pallet-color-selector">
                                    <ColorPicker
                                        setWrapperRef={setWrapperRef}
                                        toggleColorPicker={() => toggleColorPicker("header_color")}
                                        isPickerVisible={state.selectedColorPicker === "header_color"}
                                        colorChangeHandler={(color) => pageSettingsChangeHandler(color.hex, "header_color")}
                                        selectedColor={state.pageSettings.header.color}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>*/}
                    {/*<div className="page-setting-item">
                        <h6>Widget Sub Heading</h6>
                        <div className="form-group">
                            <label className="form-group-label">Size :</label>
                            <ol className="list-style-none d-flex size-list font-size-list">
                                <li onClick={() => pageSettingsChangeHandler(12, "subHeading_fontSize")} className={getActiveClass(state.pageSettings.subHeading.fontSize, 12)}><span></span>12</li>
                                <li onClick={() => pageSettingsChangeHandler(14, "subHeading_fontSize")} className={getActiveClass(state.pageSettings.subHeading.fontSize, 14)}><span></span>14</li>
                                <li onClick={() => pageSettingsChangeHandler(16, "subHeading_fontSize")} className={getActiveClass(state.pageSettings.subHeading.fontSize, 16)}><span></span>16</li>
                                <li onClick={() => pageSettingsChangeHandler(18, "subHeading_fontSize")} className={getActiveClass(state.pageSettings.subHeading.fontSize, 18)}><span></span>18</li>
                                <li onClick={() => pageSettingsChangeHandler(20, "subHeading_fontSize")} className={getActiveClass(state.pageSettings.subHeading.fontSize, 20)}><span></span>20</li>
                                <li onClick={() => pageSettingsChangeHandler(22, "subHeading_fontSize")} className={getActiveClass(state.pageSettings.subHeading.fontSize, 22)}><span></span>22</li>
                            </ol>
                        </div>
                        <div className="form-group d-flex">
                            <div className="flex-60 pd-r-10">
                                <label className="form-group-label">Weight :</label>
                                <select className="form-control" value={state.pageSettings.subHeading.fontWeight} onChange={(e) => pageSettingsChangeHandler(e.currentTarget.value, "subHeading_fontWeight")}>
                                    <option value={300}>300</option>
                                    <option value={400}>400</option>
                                    <option value={600}>600</option>
                                    <option value={700}>700</option>
                                    <option value={800}>800</option>
                                </select>
                            </div>
                            <div className="flex-40 pd-l-10">
                                <label className="form-group-label">Color :</label>
                                <div className="pallet-color-selector">
                                    <ColorPicker
                                        setWrapperRef={setWrapperRef}
                                        toggleColorPicker={() => toggleColorPicker("subHeading_color")}
                                        isPickerVisible={state.selectedColorPicker === "subHeading_color"}
                                        colorChangeHandler={(color) => pageSettingsChangeHandler(color.hex, "subHeading_color")}
                                        selectedColor={state.pageSettings.subHeading.color}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>*/}
                    <div className="page-setting-item">
                        <div className="form-group">
                            <label className="form-group-label">Theme :</label>
                            <ul className="list-style-none d-flex page-theme-list">
                                {THEMES.map((theme, i) => <li key={i} onClick={() => themeChangeHandler(theme)} className={getActiveClass(state.pageSettings.theme, theme)} ></li>)}
                            </ul>
                        </div>
                    </div>
                    <div className="page-setting-item">
                        <div className="form-group">
                            <label className="form-group-label">View Port :</label>
                            <div className="button-group">
                                <button className="btn btn-light"><i className="far fa-desktop"></i></button>
                                <button className="btn btn-light"><i className="far fa-tablet-alt"></i></button>
                                <button className="btn btn-light"><i className="far fa-mobile"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div className="form-info-footer">
            <button className="btn btn-light" onClick={props.history.goBack}>Cancel</button>
            <button className="btn btn-primary" disabled={!state.pageName} onClick={state.pageName ? savePageHandler : null} >{props.match.params.id ? "Update" : "Save"}</button>
        </div>
    </div>
    return <React.Fragment>
        {html}
    </React.Fragment>
}

WidgetsMenu.propTypes = {};

export default WidgetsMenu;
