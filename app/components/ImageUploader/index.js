/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * ImageUploader
 *
 */

import React from "react";
import { uIdGenerator } from "../../commonUtils";
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

function ImageUploader({ loader, imagePath, uploadHandler, params, classNames , acceptFileType }) {
    params = params || []
    let { blankUploader, } = classNames || {}
    let id = uIdGenerator()
    if (loader) {
        return (
            <div className="file-upload-loader">
                <img src="https://content.iot83.com/m83/misc/uploading.gif" />
                <p>Uploading...</p>
            </div>
        )
    }
    else if (imagePath) {
        return (
            <div className="file-upload-preview">
                <img src={imagePath} />
                <input type="file" name="image" id={id} accept={acceptFileType ? acceptFileType : ""} onChange={({ currentTarget }) => { uploadHandler(currentTarget.files[0], ...params) }} />
                <button type="button" className="btn-transparent btn-transparent-gray" data-tooltip data-tooltip-text="Update" data-tooltip-place="bottom" onClick={() => document.getElementById(id).click()}>
                    <i className="far fa-pen" />
                </button>
            </div>
        )
    }
    return (
        <div className="file-upload-form">
            <input type="file" name="image" accept={acceptFileType ? acceptFileType : ""} onChange={({ currentTarget }) => uploadHandler(currentTarget.files[0], ...params)} />
            <p>Click to upload file...!</p>
        </div>
    )
}

ImageUploader.propTypes = {};

export default ImageUploader;
