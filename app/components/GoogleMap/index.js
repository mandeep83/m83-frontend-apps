/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * GoogleMap
 *
 */

import React from "react";
import { isEqual } from "lodash";
import { getAddressFromLatLng, convertTimestampToDate, getMiniLoader } from '../../commonUtils';
import { render } from 'react-dom';
import Script from 'react-load-script';
import axios from 'axios';
import { GetHeaders } from '../../commonUtils';
import {showToast} from '../../containers/HomePage/actions';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

let map, infoWindow, prevCenter = {
    lat: 40.7035957,
    lng: -73.8854603
};
let userLocation = {
    lat: null,
    lng: null
}

let isZoomChangedByUser;
/* eslint-disable react/prefer-stateless-function */
class GoogleMap extends React.Component {
    state = {
        zoom: this.props.zoom || 5,
        isLoading: !window.google
    }

    componentDidMount() {
        if (window.google) {
            this.loadMap()
        } else{
            axios.get(window.API_URL + 'api/v3/platform/googleMapsKey', GetHeaders()).then(res => {
                if (res.status == 200) {
                    this.setState({ mapKey: res.data.data.value , isLoading: false })
                }
                }).catch(error => {
                    this.setState({isLoading: false})
                    if (error.response) {
                        if (error.response.status === 401) {
                            delete localStorage.token;
                            delete localStorage.showThemePopUp;
                            delete localStorage.selectedProjectId;
                            this.props.history.push('/login');
                        } else if (error.response.status === 402) {
                            if (localStorage.tenantType === 'FLEX') {
                                this.props.showToast("error",error.response.data);
                            } else if (localStorage.role === 'DEVELOPER') {
                                this.props.showToast("error",error.response.data.message);
                            } else {
                                delete localStorage.showThemePopUp;
                                delete localStorage.token;
                                delete localStorage.selectedProjectId;
                                this.props.showToast("error",error.response.data.message);
                                
                            }
                        } else if (error.response.status === 400) {
                            this.props.showToast("error",error.response.data.data || error.response.data.error);
                        }
                        else {
                            this.props.showToast("error",error.response.data.message);
                        }
                    } else {
                        this.props.showToast("error",error.message);
                    }
                }) 
        }
    }

    componentDidUpdate() {
        if (map) {
            let centerObj = map.getCenter()
            prevCenter = {
                lat: centerObj.lat(),
                lng: centerObj.lng(),
            }
            let defaultZoom = this.getDefaultZoom()
            this.renderMap(defaultZoom);
        }
    }

    getDefaultZoom = () => {
        if (this.props.isFor === "deviceMap" || !isZoomChangedByUser) {
            return this.props.zoom
        }
        return null
    }

    getZoomLevel = (value) => {
        let zoomLevel;
        if (value <= 100) {
            zoomLevel = 8
        }
        else if (value > 100 && value <= 200) {
            zoomLevel = 7
        }
        else if (value > 200 && value <= 600) {
            zoomLevel = 6
        }
        else if (value > 600 && value <= 1200) {
            zoomLevel = 5
        }
        else if (value > 1200 && value <= 1500) {
            zoomLevel = 4
        }
        else if (value > 1300) {
            zoomLevel = 3
        }
        return zoomLevel
    }

    shouldComponentUpdate(nextProps, nextState) {
        return !isEqual(this.props, nextProps) || nextState != this.state
    }

    getMapCenter = () => {
        let lat = userLocation.lat || prevCenter.lat, lng = userLocation.lng || prevCenter.lng;
        if (this.props.geofencing && this.props.geofencing.status && this.props.geofencing.lat) {
            lat = this.props.geofencing.lat
            lng = this.props.geofencing.lng
        }
        else if (this.props.markers.length && this.props.markers[0].lat) {
            lat = this.props.markers[0].lat
            lng = this.props.markers[0].lng
        }
        return [lat, lng]
    }

    createInfoWindow(e, map, device) {
        if (infoWindow) {
            infoWindow.close();
        }
        infoWindow = new window.google.maps.InfoWindow({
            content: '<div id="infoWindow" />',
            position: { lat: e.latLng.lat(), lng: e.latLng.lng() },
            maxWidth: 350,
        })
        infoWindow.addListener('domready', e => {
            render(
                <div className="map-marker" ref={this.hideInfoRef}>
                    <div className="map-marker-header">
                        <span className={`map-marker-status ${device.status == "online" ? "bg-green" : device.status == "offline" ? "bg-red" : "bg-gray"}`}></span>
                        <h6>Device Type: {device.deviceTypeName}</h6>
                        <p><strong>Name: </strong>{device.name || "-"}</p>
                        <p><strong>Device Id :</strong>{device.id}</p>
                        <p><strong>Last Reported :</strong><span>{convertTimestampToDate(device.lastReportedAt)}</span></p>
                        <div className="map-marker-image">
                            <img src={device.deviceTypeImage || "https://content.iot83.com/m83/misc/device-type-fallback.png"} />
                            <span className={device.status == "online" ? "bg-green" : device.status == "offline" ? "bg-red" : "bg-gray"}></span>
                        </div>
                    </div>
                    <div className="map-marker-body">
                        <p><strong>Location :</strong>{device.location}</p>
                        <p><strong>Group :</strong>{device.groupName}</p>
                        <p><strong>Tag :</strong>{device.tagName}</p>
                        <ul className="d-flex list-style-none mt-2">
                            <li><span className="badge badge-pill bg-critical">{device.alarmsCount.critical}</span><p>Critical</p></li>
                            <li><span className="badge badge-pill bg-major">{device.alarmsCount.major}</span><p>Major</p></li>
                            <li><span className="badge badge-pill bg-minor">{device.alarmsCount.minor}</span><p>Minor</p></li>
                            <li><span className="badge badge-pill bg-alarm-warning">{device.alarmsCount.warning}</span><p>Warning</p></li>
                        </ul>
                        {this.props.movableDeviceTypes.includes(device.deviceTypeId) && <div className="text-left">
                            <button className="btn btn-link" type="button" id="tree" onClick={() => this.props.history.push(`locationTracking/${device.deviceTypeId}/${device.id}`)}>
                                Live Tracking<i className="fas fa-caret-right mr-l-5"></i>
                            </button>
                        </div>}
                        <div className="text-right">
                            <button className="btn btn-link" type="button" id="tree" onClick={() => this.props.history.push(`deviceInfo/${device.deviceTypeId}/${device.id}`)}>
                                See More<i className="fas fa-caret-right mr-l-5"></i>
                            </button>
                        </div>
                    </div>
                </div>, document.getElementById('infoWindow')
            )
        })
        infoWindow.open(map)
    }

    renderMap(defaultZoom) {
        let mapDiv, isClusterRemoved, markerClusterer, oldZoom = 0, timeout;
        let markerCluster = [];
        let mapClickHandler = ({ latLng }) => {
            const lat = latLng.lat();
            const lng = latLng.lng();
            getAddressFromLatLng(lat, lng, this.props.setLocation)
        }
        mapDiv = this.mapRef.current;
        map = new google.maps.Map(mapDiv, {
            center: new google.maps.LatLng(...this.getMapCenter()),
            zoom: typeof defaultZoom == "number" ? defaultZoom : this.state.zoom,
            minZoom: 1
        });
        map.addListener('zoom_changed', () => {
            let currentZoom = map.getZoom()
            if (this.props.isFor !== "deviceMap") {
                this.setState({ zoom: currentZoom })
            }
            else if (isClusterRemoved && oldZoom > currentZoom) {
                isClusterRemoved = false;
                markerClusterer.addMarkers(markerCluster)
            }
            oldZoom = currentZoom;
            isZoomChangedByUser = currentZoom !== this.props.zoom
        })

        const oms = new OverlappingMarkerSpiderfier(map, {
            circleFootSeparation: 70,
            keepSpiderfied: true,
            nearbyDistance: 0.0001
        })

        let markers = this.props.markers || [];
        if (this.props.geofencing && this.props.geofencing.status && this.props.geofencing.lat) {
            let circle = new google.maps.Circle({
                strokeColor: '#08EE23',
                strokeOpacity: 0.8,
                strokeWeight: 2,
                fillColor: '#d0fdd5',
                fillOpacity: 0.35,
                map,
                center: { lat: this.props.geofencing.lat, lng: this.props.geofencing.lng },
                radius: parseInt(this.props.geofencing.radius) * 1000
            });
            map.setZoom(this.getZoomLevel(this.props.geofencing.radius));
        }
        if (this.props.isFor === "deviceMap") {
            let bounds = new google.maps.LatLngBounds();
            markers.map((device, i) => {
                let latLng = { lat: device.lat, lng: device.lng }
                let image = {
                    size: new google.maps.Size(42, 42),
                    url: device.deviceTypeImage ? device.deviceTypeImage : device.status === "online" ? 'https://content.iot83.com/m83/misc/icons/greenMarker.png' : device.status === "offline" ? 'https://content.iot83.com/m83/misc/icons/redMarker.png' : 'https://content.iot83.com/m83/misc/icons/grayMarker.png',
                }
                let mapMarker = new google.maps.Marker({
                    position: { lat: device.lat, lng: device.lng },
                    map,
                    animation: google.maps.Animation.DROP,
                    icon: image,
                    originalIcon: image
                });
                mapMarker.setMap(map)
                google.maps.event.addListener(mapMarker, 'spider_click', (e) => {
                    timeout = setTimeout(() => this.createInfoWindow(e, map, device), 300)
                });
                mapMarker.addListener("dblclick", () => {
                    clearTimeout(timeout)
                    this.props.history.push(`deviceInfo/${device.deviceTypeId}/${device.id}`)
                })
                oms.addMarker(mapMarker)
                if (!this.props.zoom) {
                    bounds.extend(latLng);
                    map.fitBounds(bounds);
                    map.setCenter(bounds.getCenter());
                }
                markerCluster.push(mapMarker);
            })
            setTimeout(() => typeof this.props.zoom != "number" && map.getZoom() > 14 && map.setZoom(14), 30)
            markerClusterer = new MarkerClusterer(map, markerCluster, { imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m' });
            google.maps.event.addListener(markerClusterer, 'clusterclick', function (cluster) {
                var markers = markerClusterer.markers_;
                markerClusterer.clearMarkers();
                isClusterRemoved = true
                markers.map(marker => {
                    marker.setMap(map);
                })
                oms.addListener('spiderfy', function (markers) {
                    for (let i = 0; i < markers.length; i++) {
                        markers[i].setIcon(markers[i].originalIcon);
                    }
                });

                oms.addListener('unspiderfy', function (markers) {
                    for (let i = 0; i < markers.length; i++) {
                        markers[i].setIcon({ url: require("../../assets/images/other/user.png"), size: stuff[i].originalIcon.size });
                    }
                });

                let stuff = oms.markersNearAnyOtherMarker();
                for (let i = 0; i < stuff.length; i++) {
                    stuff[i].setIcon({ url: require('../../assets/images/other/user.png'), size: stuff[i].originalIcon.size });
                }
            });
        }
        else {
            map.addListener("click", mapClickHandler)
            if (this.props.markers.length && this.props.markers[0].lat) {
                let marker = new google.maps.Marker({
                    position: this.props.markers[0],
                    map,
                    draggable: true,
                });
                marker.addListener('dragend', mapClickHandler);
            }
        }
    }

    mapRef = React.createRef()
    hideInfoRef = React.createRef();

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClickOutside);
    }

    handleClickOutside = () => {
        if (this.hideInfoRef.current && !this.hideInfoRef.current.contains(event.target)) {
            if (infoWindow) {
                infoWindow.close();
            }
        }
    }

    loadMap = () => {
        this.renderMap(this.getDefaultZoom());
        document.addEventListener('mousedown', this.handleClickOutside);
        navigator.geolocation.getCurrentPosition((position) => {
            userLocation = {
                lat: position.coords.latitude,
                lng: position.coords.longitude,
            }
            let defaultZoom = this.getDefaultZoom()
            this.renderMap(defaultZoom);
        });
    }
    render() {
        return (
            this.state.isLoading ? getMiniLoader() :
            <React.Fragment>
                <div id="map" style={{ height: "100%" }} ref={this.mapRef} />
                {this.state.mapKey && <Script url= {`https://maps.googleapis.com/maps/api/js?key=${ this.state.mapKey }&libraries=places`} onLoad = {this.loadMap}/>}
            </React.Fragment>
        );
    }

}

GoogleMap.propTypes = {};

export const getMapZoom = () => {
    return map ? map.getZoom() : 0
}

function mapDispatchToProps(dispatch) {
    return {
        dispatch,
        showToast: (showToastType,showToastMessage) => dispatch(showToast(showToastType,showToastMessage)),
    };
}

const mapStateToProps = createStructuredSelector({
    
});

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
);

export default compose(
    withConnect,
)(GoogleMap);