/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * QuotaHeader
 *
 */

import React, { useEffect, useState } from "react";
import SearchBox from "../SearchBox";
// import PropTypes from 'prop-types';
// import styled from 'styled-components';
import { GetHeaders } from "../../commonUtils";
import axios from "axios";
function QuotaHeader(props) {
    const { componentName,isAddSuccess, showQuota, isAddButtonDisabled, callBack, onAddClick, refreshHandler, searchBoxDetails, productName, isDeletedSuccess, addButtonText, moudleChangeHandler, refetchQuota, refreshDisabled, addDisabled, toggleButton } = props
    const [state, setState] = useState({
        totalCount: 0,
        usedCount: 0,
        remainingCount: 0,
        isFetching: true,
    })
    
    let fetchQuota = () => {
        const payload = [
            {
                dependingProductId: null,
                productName,
            }
        ]
        axios.post(window.API_URL + "api/v3/products/count", payload, GetHeaders()).then(res => {
            const findObject = res.data.data.find(product => product.productName === productName)
            let remainingCount = findObject['remaining'],
                totalCount = findObject['total'],
                usedCount = findObject['used'];
            setState({
                remainingCount,
                totalCount,
                usedCount,
                isFetching: false
            });
        }).catch(error => {
            if (error.response) {
                if (error.response.status === 401) {
                    delete localStorage.token;
                    delete localStorage.showThemePopUp;
                    delete localStorage.selectedProjectId
                    props.history.push('/login');
                } else if (error.response.status === 402) {
                    if (localStorage.tenantType === 'FLEX') {
                        props.showNotification("error", error.response.data, callBack);
                    } else if (localStorage.role === 'DEVELOPER') {
                        props.showNotification("error", error.response.data.message, callBack);
                    } else {
                        delete localStorage.showThemePopUp;
                        delete localStorage.token;
                        delete localStorage.selectedProjectId
                        props.showNotification("error", error.response.data.message, callBack);
                    }
                } else if (error.response.status === 400) {
                    props.showNotification("error", error.response.data.data || error.response.data.error, callBack);
                }
                else {
                    props.showNotification("error", error.response.data.message, callBack);
                }
            } else {
                props.showNotification("error", error.message, callBack);
            }
        })
        
    }
    
    useEffect(() => {
        if (typeof refetchQuota !== "undefined") {
            setState({
                ...state,
                isFetching: true
            })
            fetchQuota()
        }
    }, [refetchQuota])
    
    useEffect(fetchQuota, [])
    
    
    useEffect(() => {
        if (typeof isDeletedSuccess !== "undefined")
            setState((previousState) => ({
                ...previousState,
                remainingCount: ++previousState.remainingCount,
                usedCount: --previousState.usedCount
            }))
    }, [isDeletedSuccess])
    
    useEffect(() => {
        if (typeof isAddSuccess !== "undefined")
            setState((previousState) => ({
                ...previousState,
                remainingCount: --previousState.remainingCount,
                usedCount: ++previousState.usedCount
            }))
    }, [isAddSuccess])
    
    useEffect(() => {
        !state.isFetching && callBack()
    }, [state.isFetching])
    
    return (
        <React.Fragment>
            <header className="content-header d-flex">
                <div className="flex-60">
                    {showQuota ?
                        <h6>{componentName} -
                            <span className="content-header-badge-group">
                                <span className="content-header-badge-item">Allocated <span className="badge badge-pill badge-primary">{state.totalCount}</span></span>
                                <span className="content-header-badge-item">Used <span className="badge badge-pill badge-success">{state.usedCount}</span></span>
                                <span className="content-header-badge-item">Remaining <span className="badge badge-pill badge-warning">{state.remainingCount}</span></span>
                            </span>
                        </h6> :
                        <h6>{componentName}</h6>
                    }
                </div>
                
                <div className="flex-40 text-right">
                    <div className="content-header-group">
                        {moudleChangeHandler &&
                            <div className="toggle-switch-wrapper mr-2">
                                <span>API</span>
                                <label className="toggle-switch">
                                    <input type="checkbox" id="isAuthenticated" onClick={moudleChangeHandler} />
                                    <span className="toggle-switch-slider"></span>
                                </label>
                                <span>Module</span>
                            </div>
                        }
                        {searchBoxDetails && <SearchBox value={searchBoxDetails.value} onChangeHandler={searchBoxDetails.onChangeHandler} isDisabled={searchBoxDetails.isDisabled} />}
                        <button className={toggleButton == true ? "btn btn-light active" : "btn btn-light"} disabled={state.isFetching} data-tooltip="true" data-tooltip-text="List View" data-tooltip-place="bottom" onClick={toggleButton}><i className="fad fa-list-ul"></i></button>
                        <button className={toggleButton == false ? "btn btn-light active" : "btn btn-light"} disabled={state.isFetching} data-tooltip="true" data-tooltip-text="Card View" data-tooltip-place="bottom" onClick={toggleButton}><i className="fad fa-table"></i></button>
                        <button className="btn btn-light" disabled={refreshDisabled} data-tooltip data-tooltip-text="Refresh" data-tooltip-place="bottom" onClick={refreshHandler}>
                            <i className="far fa-sync-alt"></i>
                        </button>
                        <button type="button" disabled={!state.remainingCount || addDisabled} className="btn btn-primary" data-tooltip data-tooltip-text={addButtonText} data-tooltip-place="bottom" onClick={onAddClick}>
                            <i className="far fa-plus"></i>
                        </button>
                    </div>
                </div>
            </header>
        </React.Fragment>
    )
};

QuotaHeader.propTypes = {};

export default QuotaHeader;
