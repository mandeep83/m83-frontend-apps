/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import React from "react";

function NoTenantExists() {
    return (
        <React.Fragment>
            <div className="outer-error">
                <div className="outer-error-content">
                    <div className="outer-error-icon">
                        <img src="https://content.iot83.com/m83/misc/noTenant.gif" alt="No Tenant Exists" />
                    </div>
                    <h1>No Such tenant exists</h1>
                    <h5>Please use another tenant url !</h5>
                </div>
            </div>
        </React.Fragment>
    );
}

NoTenantExists.propTypes = {};

export default NoTenantExists;
