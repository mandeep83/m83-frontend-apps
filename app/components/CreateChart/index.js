/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * CreateChart
 *
 */

import React from "react";
import * as am4charts from "@amcharts/amcharts4/charts";
import * as am4core from "@amcharts/amcharts4/core";
import { getMiniLoader } from "../../commonUtils";

let chart = {}
let chartsInQueue = []

/* eslint-disable react/prefer-stateless-function */
class CreateChart extends React.Component {
    state = {
        color: ["#00da00", "#11A8E7"],
        alarm: {
            critical: "#e2030f",
            major: "#fb7f37",
            minor: "#fbbd50",
            warning: "#ffe680"
        },
        loaderVisibility: true,
    }
    hand;
    label;

    componentDidMount() {
        this.createChart();
    }

    componentWillReceiveProps(props) {
        let requiredChart = chart[this.props.divId].chart;
        if (requiredChart) {
            if (props.data.category === "lineChart") {
                if (this.props.data && (JSON.stringify(props.data.data.chartData) != JSON.stringify(this.props.data.data.chartData))) {
                    if (props.operationAttribute && props.operationAttribute !== "NONE") {
                        let oldTime = requiredChart.series.getIndex(0).data.length ? requiredChart.series.getIndex(0).data[requiredChart.series.getIndex(0).data.length - 1].category : 0;
                        let operationOldTime = requiredChart.series.getIndex(1).data.length ? requiredChart.series.getIndex(1).data[requiredChart.series.getIndex(1).data.length - 1].category : 0;
                        let dataPoints = props.data.data.chartData.filter(val => val.category > oldTime)
                        let operationDataPoints = props.data.operationData.chartData.filter(val => val.category > operationOldTime)
                        if (dataPoints.length)
                            requiredChart.series.getIndex(0).addData(dataPoints, dataPoints.length);
                        if (operationDataPoints.length)
                            requiredChart.series.getIndex(1).addData(operationDataPoints, operationDataPoints.length);
                    } else {
                        let oldTime = requiredChart.series.getIndex(0).data[requiredChart.series.getIndex(0).data.length - 1].category;
                        let dataPoints = props.data.data.chartData.filter(val => val.category > oldTime)
                        requiredChart.series.getIndex(0).addData(dataPoints, dataPoints.length);
                    }
                }
                if (props.timeZone !== this.props.timeZone) {
                    requiredChart.dispose();
                    this.createChart();
                }
            }
            if (props.data.category === "gaugeChart") {
                if (this.props.data && JSON.stringify(props.data.data.value) != JSON.stringify(this.props.data.data.value)) {
                    this.hand.value = props.data.data.value;
                    this.label.text = `${props.data.data.value ? `${props.data.data.value.toFixed(2)} ${props.data.data.unit}` : ''}`;
                }
            }
        }
    }

    createChart() {
        // am4core.options.queue = true;
        let _this = this;
        chart[_this.props.divId] = {
            chart: null,
            config: _this.props.data,
            divId: _this.props.divId,
            type: "gaugeChart",
            _this,
            attributes: _this.props.attributes,
        };

        this.createGaugeChart(_this.props.divId);
    }

    createGaugeChart = (chartName) => {
        let requiredChartDetails = chart[chartName];
        let chartConfigData = requiredChartDetails.config;
        let _this = requiredChartDetails._this;
        requiredChartDetails.chart = am4core.create(chartName, am4charts.GaugeChart);
        requiredChartDetails.chart.innerRadius = am4core.percent(90);
        requiredChartDetails.chart.radius = am4core.percent(100);
        requiredChartDetails.chart.startAngle = -200;
        requiredChartDetails.chart.endAngle = 20;

        /**
         * Normal axis
         */

        let axis = requiredChartDetails.chart.xAxes.push(new am4charts.ValueAxis());
        axis.min = chartConfigData.data.min;
        axis.max = chartConfigData.data.max;
        // axis.strictMinMax = true;
        axis.renderer.radius = am4core.percent(85);
        axis.renderer.inside = true;
        axis.renderer.line.strokeOpacity = 0;
        axis.renderer.ticks.template.disabled = false
        axis.renderer.ticks.template.strokeOpacity = 1;
        axis.renderer.ticks.template.length = 5;
        axis.renderer.grid.template.disabled = true;
        axis.renderer.labels.template.radius = 30;
        axis.renderer.labels.template.fontSize = 10;
        axis.renderer.minGridDistance = 75;
        // axis.startLocation = 1;
        // axis.endLocation = 0;

        /**
         * Axis for ranges
         */

        let colorSet = "new am4core.ColorSet();"

        let axis2 = requiredChartDetails.chart.xAxes.push(new am4charts.ValueAxis());
        axis2.min = chartConfigData.data.min;
        axis2.max = chartConfigData.data.max;
        // axis2.strictMinMax = true;
        axis2.renderer.labels.template.disabled = true;
        axis2.renderer.ticks.template.disabled = true;
        axis2.renderer.grid.template.disabled = true;
        axis2.renderer.axisFills.template.disabled = false;
        axis2.renderer.axisFills.template.fillOpacity = 0.1;
        axis2.renderer.axisFills.template.fill = am4core.color("red");
        axis2.renderer.minGridDistance = 75;

        let checkAlarm = requiredChartDetails.attributes.length > 0 ? requiredChartDetails.attributes.find(val => val.attribute === chartConfigData.attributes[0] && val.isConfigured) : undefined
        if (checkAlarm) {
            const uniqueOperators = [...new Set(checkAlarm.alarmCriteria.map(item => item.operator))];
            if (uniqueOperators.length === 1) {
                if (uniqueOperators[0] === 'GREATER_THAN') {
                    let range = axis2.axisRanges.create();
                    range.value = getFirstRangeMin();
                    range.endValue = checkAlarm.alarmCriteria[0].low;
                    range.axisFill.fillOpacity = 1;
                    range.axisFill.fill = "#00da00";

                    checkAlarm.alarmCriteria.map((category, i) => {
                        let range = axis2.axisRanges.create();
                        range.value = category.low;
                        range.endValue = category.type === 'critical' ? getLastRangeMax() : category.high;
                        range.axisFill.fillOpacity = 1;
                        range.axisFill.fill = _this.state.alarm[category.type];
                    })
                } else {
                    checkAlarm.alarmCriteria.map((category, i) => {
                        let range = axis2.axisRanges.create();
                        range.value = category.type === 'critical' ? getFirstRangeMin() : category.low;
                        range.endValue = category.high;
                        range.axisFill.fillOpacity = 1;
                        range.axisFill.fill = _this.state.alarm[category.type];
                    })

                    let range = axis2.axisRanges.create();
                    range.value = checkAlarm.alarmCriteria[checkAlarm.alarmCriteria.length - 1].high;
                    range.endValue = getLastRangeMax();
                    range.axisFill.fillOpacity = 1;
                    range.axisFill.fill = "#00da00";
                }
            } else {
                checkAlarm.alarmCriteria.map((category, i) => {
                    let range = axis2.axisRanges.create();
                    range.value = category.type === 'critical' && category.operator === 'LESS_THAN' ? getFirstRangeMin() : category.low;
                    range.endValue = category.type === 'critical' && category.operator === 'GREATER_THAN' ? getLastRangeMax() : category.high;
                    range.axisFill.fillOpacity = 1;
                    range.axisFill.fill = _this.state.alarm[category.type];
                })
                let range = axis2.axisRanges.create();
                range.value = checkAlarm.alarmCriteria.filter(criteria => criteria.type === 'warning' && criteria.operator === 'LESS_THAN')[0].high;
                range.endValue = checkAlarm.alarmCriteria.filter(criteria => criteria.type === 'warning' && criteria.operator === 'GREATER_THAN')[0].low;
                range.axisFill.fillOpacity = 1;
                range.axisFill.fill = "#00da00";
            }
        } else {
            let range0 = axis2.axisRanges.create();
            range0.value = getFirstRangeMin();
            range0.endValue = getLastRangeMax();
            range0.axisFill.fillOpacity = 1;
            range0.axisFill.fill = "#00da00";
        }

        /**
         * Label
         */

        _this.label = requiredChartDetails.chart.radarContainer.createChild(am4core.Label);
        _this.label.isMeasured = false;
        _this.label.fontSize = 16;
        _this.label.fontWeight = 600;
        _this.label.fill = "#4a4a4a";
        _this.label.x = am4core.percent(50);
        _this.label.y = am4core.percent(100);
        _this.label.horizontalCenter = "middle";
        _this.label.verticalCenter = "bottom";
        _this.label.dy = 40;
        _this.label.text = `${chartConfigData.data.value ? `${chartConfigData.data.value.toFixed(2)} ${chartConfigData.data.unit}` : ''}`;

        let label = requiredChartDetails.chart.radarContainer.createChild(am4core.Label);
        label.isMeasured = false;
        label.fontSize = 13;
        label.fontWeight = 400;
        label.fill = "#4a4a4a";
        label.x = am4core.percent(50);
        label.y = am4core.percent(100);
        label.horizontalCenter = "middle";
        label.verticalCenter = "bottom";
        label.dy = 60;
        label.text = `${chartConfigData.data.attrDisplayName ? chartConfigData.data.attrDisplayName : chartConfigData.data.name}`;

        /**
         * Hand
         */

        _this.hand = requiredChartDetails.chart.hands.push(new am4charts.ClockHand());
        _this.hand.axis = axis;
        _this.hand.radius = am4core.percent(80);
        _this.hand.startWidth = 5;
        _this.hand.endWidth = 1;
        _this.hand.fill = am4core.color("#4a4a4a");
        _this.hand.stroke = am4core.color("#4a4a4a");
        _this.hand.pin.disabled = false;
        _this.hand.value = chartConfigData.data.value;

        let shadowHand = _this.hand.filters.push(new am4core.DropShadowFilter());
        shadowHand.dy = 2;
        shadowHand.color = "#b8b8b8";

        let shadowPin = _this.hand.pin.filters.push(new am4core.DropShadowFilter());
        shadowPin.dy = -2;
        shadowPin.dx = -2;
        shadowPin.color = "#b8b8b8";

        _this.hand.pin.radius = 6;
        _this.hand.pin.strokeWidth = 4;
        _this.hand.pin.stroke = "#fff";
        _this.hand.pin.fill = "#4a4a4a";
        _this.hand.pin.zIndex = 1;
        requiredChartDetails.chart.events.on('ready', () => {
            _this.setState({
                loaderVisibility: false,
            })
        });


        function getFirstRangeMin() {
            return chartConfigData.data.min - (chartConfigData.data.max - chartConfigData.data.min)
        }

        function getLastRangeMax() {
            return chartConfigData.data.max + (chartConfigData.data.max - chartConfigData.data.min)
        }
    }
    render() {
        return (
            <React.Fragment>
                <div id={this.props.divId} className="h-100" />
            </React.Fragment>
        )
    }

    componentWillUnmount() {
        chartsInQueue = []
        if (chart[this.props.divId].chart) {
            chart[this.props.divId].chart.dispose();
            chart[this.props.divId] = null;
        }
    }
}

CreateChart.propTypes = {};

export default CreateChart;
