/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * Tooltip
 *
 */

import React from "react";
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

import { FormattedMessage } from "react-intl";
import messages from "./messages";
import "../../assets/scss/components/_tooltip.scss"
import $ from 'jquery'

/* eslint-disable react/prefer-stateless-function */
class Tooltip extends React.PureComponent {
    componentDidMount() {
        $(document).mousemove((e) => {
            if (e.target.hasAttribute("data-tooltip") && e.target.dataset.tooltip === "true") {
                this.getTooltip(e.target);
            }
        });
    }

    getTooltip = (tooltipTag) => {
        let tooltipContainer = document.getElementById('tooltipContainer');
        let addToolTip = (e) => {
            let tooltipText = tooltipTag.dataset.tooltipText;
            tooltipContainer.innerHTML = tooltipText || "";
            let position = this.getTooltipPos(tooltipContainer, tooltipTag, e.pageX, e.pageY);
            let styleTooltip = {
                'left': position.tooltipX + 'px',
                'top': position.tooltipY + 'px',
                'visibility': 'visible'
            }
            Object.assign(tooltipContainer.style, styleTooltip);
            tooltipContainer.setAttribute("data-place", position.pointerY + "-" + position.pointerX);
        }
        if (tooltipContainer) {
            tooltipTag.addEventListener('mousemove', addToolTip);
            tooltipTag.addEventListener('mouseleave', () => {
                tooltipTag.removeEventListener('mousemove',addToolTip);
                tooltipContainer.style = {}
                tooltipContainer.innerHTML = ""
                tooltipContainer.setAttribute("data-place", "")
            });

            tooltipTag.addEventListener('click', () => {
                tooltipTag.removeEventListener('mousemove',addToolTip);
                tooltipContainer.style = {}
                tooltipContainer.innerHTML = ""
                tooltipContainer.setAttribute("data-place", "")
            });
        }
    }

    getTooltipPos = (tooltipContainer, tooltipTag, posX, posY) => {
        let tooltipWidth = tooltipContainer.offsetWidth,
            tooltipHeight = tooltipContainer.offsetHeight,
            body = document.getElementsByTagName('body')[0],
            maxPosX = body.offsetWidth - tooltipWidth,
            maxPosY = body.offsetHeight - tooltipHeight,
            tooltipY, tooltipX, pointerX, pointerY, minDistance = 20,
            placeRight = tooltipTag.dataset.tooltipPlace === "right" && posX < maxPosX,
            placeBottom = tooltipTag.dataset.tooltipPlace === "bottom" && posY < maxPosY,
            placeLeft = tooltipTag.dataset.tooltipPlace === "left" && posX > tooltipWidth

        if (posY < tooltipHeight || placeBottom && posY < maxPosY) {
            tooltipY = posY + minDistance
            pointerY = "bottom"
        } else if ((posX < tooltipWidth || posX > maxPosX) && !(posY > maxPosY) || placeRight || placeLeft) {
            tooltipY = posY - tooltipHeight / 2
            pointerY = "center"
        } else {
            tooltipY = posY - tooltipHeight - minDistance
            pointerY = "top"
        }

        if (posX < tooltipWidth || placeRight) {
            tooltipX = posX + minDistance
            pointerX = "right"
        }
        else if (posX > maxPosX || placeLeft) {
            tooltipX = posX - tooltipWidth - minDistance
            pointerX = "left"
        } else {
            tooltipX = posX - tooltipWidth / 2
            pointerX = "center"
        }

        return { tooltipX, tooltipY, pointerX, pointerY }
    }

    render() {
        return (
            <div className="tooltip-wrapper" data-place="top-center" id="tooltipContainer" />
        );
    }
}

Tooltip.propTypes = {};

export default Tooltip;
