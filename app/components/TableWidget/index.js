/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * TableWidget
 *
 */

import React from "react";
// import PropTypes from 'prop-types';
// import styled from 'styled-components';
import { convertTimestampToDate, getTimeDifference } from "../../commonUtils";

/* eslint-disable react/prefer-stateless-function */
const status = {
    "online": <span className="badge badge-pill alert-success list-view-pill"><i className="fad fa-circle mr-r-5"></i>Online</span>,
    "offline": <span className="badge badge-pill alert-danger list-view-pill"><i className="fad fa-circle mr-r-5"></i>Offline</span>,
    "not-connected": <span className="badge badge-pill alert-dark list-view-pill"><i className="fad fa-circle mr-r-5"></i>Not-connected</span>
}

const getTimeDifferenceClasses = {
    "online": "text-green",
    "offline": "text-red",
    "not-connected": "text-gray"
}

class TableWidget extends React.Component {

    render() {
        return (
            <div className="list-view">
                <div className="list-view-header d-flex">
                    <p className="flex-25">Device Detail</p>
                    <p className="flex-15">Device Type</p>
                    <p className="flex-10">Group</p>
                    <p className="flex-10">Tag</p>
                    <p className="flex-17_5">Last Reported</p>
                    <p className="flex-12_5">Status</p>
                    <p className="flex-10">All Alarms</p>
                </div>
                <div className="list-view-body">
                    {this.props.isFetchingDevices ? <div className="inner-loader-wrapper" style={{ height: 300 }}>
                        <div className="inner-loader-content">
                            <i className="fad fa-sync-alt fa-spin"></i>
                        </div>
                    </div> :
                        this.props.data.length > 0 ?
                            this.props.data.map(device => {
                                let totalAlarms = device.alarmsCount.critical + device.alarmsCount.major + device.alarmsCount.minor + device.alarmsCount.warning;
                                return <div className="list-view-item cursor-pointer" key={device.id} onClick={() => this.props.history.push(`deviceDetails/${device.deviceTypeId}/${device.id}`)}>
                                    <div className={`list-view-border list-view-border-${device.status === 'online' ? "green" : device.status === 'offline' ? 'red' : 'gray'}`}></div>
                                    <ul className="list-style-none d-flex">
                                        <li className="flex-25">
                                            <div className="list-view-icon">
                                                <img src={device.deviceTypeImage || "https://content.iot83.com/m83/misc/device-type-fallback.png"}></img>
                                            </div>
                                            <div className="list-view-icon-box">
                                                <h6 className="text-theme fw-600 text-underline-hover cursor-pointer">{device.name ? device.name : device.id}</h6>
                                                <p>{device.location ? device.location : "-"}</p>
                                            </div>
                                        </li>
                                        <li className="flex-15">
                                            <h6>{device.deviceTypeName}</h6>
                                        </li>
                                        <li className="flex-10">
                                            <h6>{device.groupName}</h6>
                                        </li>
                                        <li className="flex-10">
                                            <span className="badge badge-pill badge-light list-view-badge-pill" style={{ color: device.tagColor }}>{device.tagName}</span>
                                        </li>
                                        <li className="flex-17_5">
                                            <h6>{convertTimestampToDate(device.lastReportedAt)}</h6>
                                            <p className={getTimeDifferenceClasses[device.status]}>{getTimeDifference(device.lastReportedAt)}</p>
                                        </li>
                                        <li className="flex-12_5">
                                            {status[device.status]}
                                        </li>
                                        <li className="flex-10">
                                            <div className="button-group-link">
                                                {totalAlarms ?
                                                    <div className="list-view-alarm-count">
                                                        <i className="fad fa-bell text-orange"></i>
                                                        <span className="badge badge-danger">{totalAlarms}</span>
                                                    </div>
                                                    :
                                                    <div className="list-view-alarm-count">
                                                        <i className="fad fa-bell"></i>
                                                        <span className="badge badge-light">0</span>
                                                    </div>
                                                }
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            })
                            :
                            <div className="list-view-empty"><h6>There is no data to display.</h6></div>}
                </div>
            </div>
        )
    }
}

TableWidget.propTypes = {};

export default TableWidget;
