/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * EventHistory
 *
 */

import React from "react";
// import PropTypes from 'prop-types';
// import styled from 'styled-components';
import { convertTimestampToDate } from "../../commonUtils"
import { cloneDeep } from 'lodash'
/* eslint-disable react/prefer-stateless-function */
let ACTIONS_DETAILS = [
    {
        name: "SMS",
        miniTableDetails: [[
            { header: "Name", accessor: "name" },
            { header: "Phone No.", accessor: "phoneNumber" },
        ], []],
        className: "fas fa-comment-lines text-orange"
    },
    {
        name: "EMAIL",
        miniTableDetails: [[
            { header: "Name", accessor: "name" },
            { header: "Email", accessor: "email" },
        ], []],
        className: "fad fa-envelope text-green"
        
    },
    {
        name: "CONTROL",
        miniTableDetails: [[
            { header: "Name", accessor: "name" },
            { header: "Action", accessor: "rpcAction" },
        ], []],
        className: "fad fa-code-merge text-red"
        
    },
    {
        name: "WEBHOOK",
        miniTableDetails: [[
            { header: "Name", accessor: "name" },
            { header: "Webhook", accessor: "webhook" },
        ], []],
        className: "fad fa-terminal text-yellow"
    },
]
class EventHistory extends React.Component {
    
    state = {
        filters: {
            from: "",
            to: "",
            actionType: "",
        }
    }
    
    filterChangeHandler = ({ currentTarget }) => {
        let { id, value } = currentTarget
        let filters = cloneDeep(this.state.filters)
        filters[id] = value
        this.setState({ filters })
    }
    
    getMiniTable = (columns, data) => {
        return <React.Fragment>
            <div className="header d-flex">
                {columns.map(column => <h2 className="flex-40">{column.header}</h2>)}
            </div>
            <div className="body">
                {data.map(item => <div className="body-content d-flex">
                    {columns.map(column => <h2 className="flex-40">{item[column.accessor]}</h2>)}
                </div>)}
            </div>
        </React.Fragment>
    }
    
    clearAllFilters = () => {
        this.setState({
            filters: {
                from: "",
                to: "",
                actionType: "",
            }
        })
    }
    
    
    render() {
        return (
            <div className="event-history">
                {/*<div className="event-history-filter d-flex">
                        <div className="form-group flex-33 pd-r-10">
                            <div className="input-group">
                                <div className="input-group-prepend">
                                    <span className="input-group-text">From :</span>
                                </div>
                                <input type="date" className="form-control" id="form" value="" />
                            </div>
                        </div>
                        <div className="form-group flex-33 pd-l-5 pd-r-5">
                            <div className="input-group">
                                <div className="input-group-prepend">
                                    <span className="input-group-text">To :</span>
                                </div>
                                <input type="date" className="form-control" id="form" value="" />
                            </div>
                        </div>
                        <div className="form-group flex-33 pd-l-10">
                            <div className="input-group">
                                <div className="input-group-prepend">
                                    <span className="input-group-text">Action :</span>
                                </div>
                                <select className="form-control">
                                    <option>SMS</option>
                                    <option>Email</option>
                                    <option>RPC</option>
                                    <option>Webhook</option>
                                </select>
                            </div>
                        </div>
                        <div className="button-group">
                            <button type="button" className="btn btn-danger"><i className="far fa-times mr-r-5"></i>Clear</button>
                        </div>
                    </div>*/}
                
                {/* variant 1 */}
                <div className="event-list">
                    {this.props.widgetData.map(event =>
                        <div className="event-list-item d-flex">
                            <div className="event-time">
                                <div>
                                    <h5 className="fw-600 m-0 mb-1 text-dark-theme">10 Aug '20</h5>
                                    <p className="m-0 text-theme">8:00 pm</p>
                                </div>
                            </div>
                            <div className="flex-60 pd-r-5">
                                <div className="d-flex event-content">
                                    <div className="flex-50">
                                        <div className="event-content-item">
                                            <p>Event Name</p>
                                            <h6>{event.name}</h6>
                                        </div>
                                        <div className="event-content-item">
                                            <p>Description</p>
                                            <h6>{event.description}</h6>
                                        </div>
                                    </div>
                                    <div className="flex-25">
                                        <div className="event-content-item">
                                            <p>Occurrence</p>
                                            <h6>{event.occurrence} {event.occurrence > 1 ? "times" : "time"}</h6>
                                        </div>
                                        <div className="event-content-item">
                                            <p>Duration</p>
                                            <h6>{event.duration} {event.duration > 1 ? "minutes" : "minute"}</h6>
                                        </div>
                                    </div>
                                    <div className="flex-25">
                                        <div className="event-content-item">
                                            <p>Actions</p>
                                            <div className="badge-group">
                                                <span className="badge badge-pill badge-light active">SMS</span>
                                                <span className="badge badge-pill badge-light">Email</span>
                                                <span className="badge badge-pill badge-light">RPC</span>
                                                <span className="badge badge-pill badge-light">Webhook</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="flex-40 pd-l-5">
                                <div className="event-condition">
                                    <div className="event-content-item">
                                        <p>Conditions</p>
                                        <ul className="list-style-none">
                                            {event.conditions.map(condition => <li>{condition}</li>)}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    )}
                </div>
                {/* end variant 1 */}
    
                {/* variant 2 */}
                <div className="widget-tab-wrapper" id="eventHistoryVareint_1">
                    <ul className="nav nav-tabs" id="myTab" role="tablist">
                        <li className="nav-item">
                            <a className="nav-link active" data-toggle="tab" href="#action" role="tab">All Action</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" data-toggle="tab" href="#email" role="tab">Email</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" data-toggle="tab" href="#webhook" role="tab">Webhook</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" data-toggle="tab" href="#sms" role="tab">SMS</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" data-toggle="tab" href="#rpc" role="tab">RPC</a>
                        </li>
                        <div className="form-group mr-b-0">
                            <p>Filter By date</p>
                            <input type="date" className="form-control" placeholder="Filter By Date"/>
                            <span className="calender-icon"><i className="fal fa-calendar-alt"></i></span>
                        </div>
                    </ul>
                    <div className="tab-content" id="myTabContent">
                        <div className="tab-pane fade show active" id="home">
                            <div className="widget-list-wrapper">
                                <div className="widget-list-header d-flex">
                                    <p className="flex-15">Event Name</p>
                                    <p className="flex-20">Description</p>
                                    <p className="flex-20">Action</p>
                                    <p className="flex-10">Device Count</p>
                                    <p className="flex-15">Last Reported At</p>
                                    <p className="flex-10">Occurrence</p>
                                    <p className="flex-10">Action</p>
                                </div>
                                <div className="widget-list-body">
                                    <ul className="widget-list list-style-none d-flex">
                                        <li className="flex-15">
                                            <p className="text-theme">Event Name</p>
                                        </li>
                                        <li className="flex-20">
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting</p>
                                        </li>
                                        <li className="flex-20">
                                            <ol className="list-style-none">
                                                <li>
                                                    <div className="widget-circular-image">
                                                        <img src="https://image.flaticon.com/icons/svg/281/281769.svg" />
                                                    </div>
                                                </li>
                                                <li>
                                                    <div className="widget-circular-image">
                                                        <img src="https://image.flaticon.com/icons/svg/736/736117.svg" />
                                                    </div>
                                                </li>
                                                <li>
                                                    <div className="widget-circular-image">
                                                        <img src="https://symphony.com/wp-content/uploads/2019/08/webhooks_1024.png" />
                                                    </div>
                                                </li>
                                                <li>
                                                    <div className="widget-circular-image">
                                                        <img src="https://upload.wikimedia.org/wikipedia/commons/7/77/Logo_de_RPC_2016.png" />
                                                    </div>
                                                </li>
                                            </ol>
                                        </li>
                                        <li className="flex-10">
                                            <div className="alert">
                                                10
                                            </div>
                                        </li>
                                        <li className="flex-15">
                                            <p>7/10/2020 <span>|</span> 02:00 PM</p>
                                            <h6>2 days Ago</h6>
                                        </li>
                                        <li className="flex-10">
                                            <div className="alert">
                                                10
                                            </div>
                                        </li>
                                        <li className="flex-10">
                                            <div className="button-group">
                                                <button className="btn-transparent btn-transparent-blue"><i
                                                    className="fas fa-broom"></i></button>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/* end variant 2 */}
            </div>
        );
    }
}

EventHistory.propTypes = {};

export default EventHistory;
