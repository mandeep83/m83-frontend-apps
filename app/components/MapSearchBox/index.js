/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * MapSearchBox
 *
 */

import React from "react";
import Script from 'react-load-script';
import axios from 'axios';
import { GetHeaders } from '../../commonUtils';
import {showToast} from '../../containers/HomePage/actions';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

/* eslint-disable react/prefer-stateless-function */
class MapSearchBox extends React.PureComponent {
  state={}
  componentDidMount() {
    if (window.google) {
      this.loadMap()
    } else {
        axios.get(window.API_URL + 'api/v3/platform/googleMapsKey', GetHeaders()).then(res => {
        if (res.status == 200) {
            this.setState({ mapKey: res.data.data.value })
          }
        }).catch(error => {
            if (error.response) {
              if (error.response.status === 401) {
                  delete localStorage.token;
                  delete localStorage.showThemePopUp;
                  delete localStorage.selectedProjectId;
                  this.props.history.push('/login');
              } else if (error.response.status === 402) {
                  if (localStorage.tenantType === 'FLEX') {
                      this.props.showToast("error",error.response.data);
                  } else if (localStorage.role === 'DEVELOPER') {
                      this.props.showToast("error",error.response.data.message);
                  } else {
                      delete localStorage.showThemePopUp;
                      delete localStorage.token;
                      delete localStorage.selectedProjectId;
                      this.props.showToast("error",error.response.data.message);
                      
                  }
              } else if (error.response.status === 400) {
                  this.props.showToast("error",error.response.data.data || error.response.data.error);
              }
              else {
                  this.props.showToast("error",error.response.data.message);
              }
            } else {
              this.props.showToast("error",error.message);
            }
        }) 
    }
  }

  inputRef = React.createRef()

  loadMap = () => {
    let autocomplete = new google.maps.places.Autocomplete(this.inputRef.current);
    autocomplete.setFields(['address_components', 'geometry', 'icon', 'name', "formatted_address"]);
    autocomplete.addListener('place_changed', () => {
      let place = autocomplete.getPlace(),
        address = `${place.formatted_address.startsWith(place.name + ",") ? "" : (place.name + ", ")}${place.formatted_address}`,
        lat = place.geometry.location.lat(),
        lng = place.geometry.location.lng()
      this.props.onSelect(address, lat, lng)
    });
  }

  render() {
    return(
      <React.Fragment>
        <input type="text"  onFocus={this.props.onFocus} className={this.props.className || "form-control"} id="mapSearch" disabled={this.props.disabled} placeholder="Search Location.." ref={this.inputRef} value={this.props.value} onChange={this.props.onChange} />
        {this.state.mapKey && <Script url= {`https://maps.googleapis.com/maps/api/js?key=${this.state.mapKey}&libraries=places`}
            onLoad = {this.loadMap}
        />}
      </React.Fragment>
    )
  }
}

MapSearchBox.propTypes = {};

function mapDispatchToProps(dispatch) {
  return {
      dispatch,
      showToast: (showToastType,showToastMessage) => dispatch(showToast(showToastType,showToastMessage)),
  };
}

const mapStateToProps = createStructuredSelector({
  
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
)(MapSearchBox);
