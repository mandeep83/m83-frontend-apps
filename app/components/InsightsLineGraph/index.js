/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * InsightsLineGraph
 *
 */

import React, { useState, useEffect, useRef } from "react";
import { getMiniLoader } from "../../commonUtils";
import * as am4charts from "@amcharts/amcharts4/charts";
import * as am4core from "@amcharts/amcharts4/core";


let createChart = ({ chartData, availableAttributes }) => {
  var chart = am4core.create("deviceDetailsInsightsChart", am4charts.XYChart);

  chart.colors.step = 2;

  chart.data = chartData;
  chart.height = "100%";
  chart.width = "100%";

  // Create axes
  var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
  dateAxis.renderer.minGridDistance = 90;
  dateAxis.cursorTooltipEnabled = true;
  dateAxis.tooltipDateFormat = "dd MMM yyyy HH:mm";
  dateAxis.dateFormats.setKey("day", "dd MMM");
  dateAxis.dateFormats.setKey("hour", "dd MMM HH:mm");
  dateAxis.dateFormats.setKey("minute", "dd MMM HH:mm");
  dateAxis.dateFormats.setKey("second", "HH:mm:ss");
  dateAxis.dateFormats.setKey("millisecond", "HH:mm:ss");
  dateAxis.dateFormatter.timezone = localStorage.timeZone;
  // Create series
  function createAxisAndSeries(field, name, opposite, unit) {
    var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    if (chart.yAxes.indexOf(valueAxis) != 0) {
      valueAxis.syncWithAxis = chart.yAxes.getIndex(0);
    }

    valueAxis.title.text = `${name} ${unit ? `(${unit})` : ""}`;

    valueAxis.cursorTooltipEnabled = false;

    var series = chart.series.push(new am4charts.LineSeries());
    series.dataFields.valueY = field;
    series.dataFields.dateX = "category";
    series.strokeWidth = 2;
    series.yAxis = valueAxis;
    series.name = name;
    series.tooltipText = "{name}: [bold]{valueY}[/]";
    series.tensionX = 0.8;
    series.showOnInit = true;

    var interfaceColors = new am4core.InterfaceColorSet();
    if (chartData.length < 4) {
      var bullet = series.bullets.push(new am4charts.CircleBullet());
      bullet.circle.stroke = interfaceColors.getFor("background");
      bullet.circle.strokeWidth = 2;
    }

    valueAxis.renderer.line.strokeOpacity = 1;
    valueAxis.renderer.line.strokeWidth = 1.5;
    valueAxis.renderer.line.stroke = series.stroke;
    valueAxis.renderer.labels.template.fill = series.stroke;
    valueAxis.renderer.opposite = opposite;
  }

  availableAttributes.map((attr, index) => {
    let isOppositeAxis = index % 2 !== 0;
    createAxisAndSeries(attr.attr, attr.attrDisplayName, isOppositeAxis, attr.unit);
  })


  // Add legend
  chart.legend = new am4charts.Legend();
  chart.legend.useDefaultMarker = true;

  // Add cursor
  chart.cursor = new am4charts.XYCursor();

  return chart;
}

function InsightsLineGraph(props) {
  let firstRender = useRef(true)

  useEffect(() => {
    let chart = createChart(props.data);
    return () => {
      chart.dispose()
    }
  }, []);

  useEffect(() => {
    if (firstRender.current) {
      firstRender.current = false;
    } else {
      let chart = createChart(props.data);
      return () => {
        chart.dispose()
      }
    }
  }, [props.timeZone]);

  return <React.Fragment>
    <div id="deviceDetailsInsightsChart" className="card-chart-box" />
  </React.Fragment>;;
}

InsightsLineGraph.propTypes = {};

export default InsightsLineGraph;
