/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import React from "react";
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

function CountersWidget(props) {
    let {id, type, data, icon, customValues} = props
    let textStyle = customValues ? {fontSize: customValues.textFontSize.value, color: customValues.textFontColor.value, fontWeight: customValues.textFontWeight.value} : null
    let valueStyle = customValues ? {fontSize: customValues.valueFontSize.value, color: customValues.valueFontColor.value, fontWeight: customValues.valueFontWeight.value} : null
    let backgroundColor = customValues ? {backgroundColor: customValues.backgroundColor.value} : null
    let iconStyle = customValues && customValues.iconColor ? {color: customValues.iconColor.value} : null
    let patchColor = customValues && customValues.patchColor ? {backgroundColor: customValues.patchColor.value} : null
    let header = data.header ? data.header : "Sample Header"
    let value = data.count ? data.count.value : "526"
    switch (type) {
        case "counter1":
            return <div id={id} className="counter-widget-outer-box">
                <div style={backgroundColor} className="counterWidgetBox counterWidgetOne text-center">
                    <div className="counterWidgetOneContent">
                        <h5 style={textStyle}>{header}</h5>
                        <h1 style={valueStyle} >{value}</h1>
                    </div>
                </div>
            </div>
        case "counter3":
            return <div id={id} className="counter-widget-outer-box">
                <div style={backgroundColor} className="counterWidgetBox counterWidgetOne">
                    <div className="counterWidgetOneContentLeft">
                        <h5 style={textStyle}>{header}</h5>
                        <h1 style={valueStyle} >{value}</h1>
                    </div>
                </div>
            </div>
        case "counter5":
            return <div id={id} className="counter-widget-outer-box">
                <div style={backgroundColor} className="counterWidgetBox counterWidgetTwo">
                    <h5 style={textStyle}>{header}</h5>
                    <div style={patchColor} className="counterWidgetCountBox">
                        <h1 style={valueStyle} >{value}</h1>
                    </div>
                </div>
            </div>
        case "counter7":
            return <div id={id} className="counter-widget-outer-box">
                <div style={backgroundColor} className="counterWidgetBox counterWidgetFour">
                    <h5 style={textStyle}>{header}</h5>
                    <div style={patchColor}  className="counterWidgetCircleBox">
                        <h1 style={valueStyle} >{value}</h1>
                    </div>
                </div>
            </div>
        case "counter9":
            return <div id={id} className="counter-widget-outer-box">
                <div style={backgroundColor} className="counter-widget-box counter-widget-six">
                    <div className="counter-widget-six-content">
                        <h5 style={textStyle}>{header}</h5>
                        <h1 style={valueStyle} >{value}</h1>
                    </div>
                    <div style={iconStyle} style={patchColor} className="counter-widget-count-box">
                        <i className={icon ? icon : "fal fa-home"}></i>
                    </div>
                </div>
            </div>
        case "counter11":
            return <div id={id} className="counter-widget-outer-box">
                <div style={backgroundColor} className="counterWidgetBox counterWidgetEight">
                    <div className="counterWidgetEightContent">
                        <h1 style={valueStyle} >{value}</h1>
                    </div>
                    <h5 style={{...textStyle, ...patchColor}}>{header}</h5>
                </div>
            </div>
    }
    // return (
    //     <React.Fragment>

    //         <div id="counter11" className="counterWidgetOuterBox">
    //             <div className="counterWidgetBox counterWidgetEight">
    //                 <h1>999</h1>
    //                 <h5>Sample Header</h5>
    //             </div>
    //         </div>

    //         <div id="counter12" className="counterWidgetOuterBox">
    //             <div className="counterWidgetBox counterWidgetNine">
    //                 <h5>Sample Header</h5>
    //                 <h1>999</h1>
    //             </div>
    //         </div>

    //         {/*<div className="counterWidgetOuterBox">*/}
    //             {/*<div className="counterWidgetBox counter-eight">*/}
    //                 {/*<h5>Counter_Text</h5>*/}
    //                 {/*<span>999</span>*/}
    //             {/*</div>*/}
    //         {/*</div>*/}

    //         {/*<div className="counterWidgetOuterBox">*/}
    //             {/*<div className="counterWidgetBox counter-nine">*/}
    //                 {/*<h5>Counter_Text</h5>*/}
    //                 {/*<div>*/}
    //                     {/*<span>999</span>*/}
    //                 {/*</div>*/}
    //             {/*</div>*/}
    //         {/*</div>*/}

    //     </React.Fragment>
    // );
}

CountersWidget.propTypes = {};

export default CountersWidget;
