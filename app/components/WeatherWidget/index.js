/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * WeatherWidget
 *
 */

import React from "react";
import { DAYS_IN_WEEK } from "../../utils/constants";
import ReactTooltip from "react-tooltip";
import cloneDeep from "lodash/cloneDeep";

// import PropTypes from 'prop-types';
// import styled from 'styled-components';

function WeatherWidget({ data, setSelectedDay }) {
    const { humidity, pressure, wind_speed, sunrise, sunset, timestamp, min, max, temp, night, feels_like } = data.selectedDay;
    const d = new Date(timestamp * 1000)
    const ye = new Intl.DateTimeFormat('en', { year: 'numeric' }).format(timestamp * 1000)
    const mo = new Intl.DateTimeFormat('en', { month: 'short' }).format(timestamp * 1000)
    const da = new Intl.DateTimeFormat('en', { day: '2-digit' }).format(timestamp * 1000)
    return <div className="weather-widget">
        <div className="weather-header" style={{ backgroundImage: `url(https://content.iot83.com/m83/widgets/weather/${data.selectedDay.condition}.jpg)` }}>
            <div className="weather-header-bg"></div>
            <div className="weather-header-content">
                <h5>{temp || night}<sup>&deg;</sup><small>C</small></h5>
                <p>{feels_like ? <span>Feels like {feels_like}&deg; </span> : "-"}</p>
                <p>{min}&deg; min <span className="mr-r-5 mr-l-5">|</span> {max}&deg; max</p>
            </div>
            <div className="weather-header-detail">
                <h4>{data.currentDate.location}</h4>
                <h6>{DAYS_IN_WEEK[d.getDay()] + " " + `${da}th ${mo}, ${ye}`}</h6>
                <h5>{d.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true, timeZone: localStorage.timeZone })}</h5>
            </div>
            <ul className="list-style-none d-flex justify-content-center">
                <li>
                    <h6>Humidity</h6>
                    <p>{humidity}<small>%</small></p>
                </li>
                <li>
                    <h6>Pressure</h6>
                    <p>{pressure}<small>hPa</small></p>
                </li>
                <li>
                    <h6>Wind</h6>
                    <p>{wind_speed}<small>km/h</small></p>
                </li>
                <li>
                    <h6>Sunrise</h6>
                    <p>{new Date(sunrise * 1000).toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true, timeZone: localStorage.timeZone })}</p>
                </li>
                <li>
                    <h6>Sunset</h6>
                    <p>{new Date(sunset * 1000).toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true, timeZone: localStorage.timeZone })}</p>
                </li>
            </ul>
        </div>

        <ul className="list-style-none d-flex weather-body-list">
            {data.daily7DayForecast.map((temp, index) => {
                let date = new Date(temp.timestamp * 1000);
                if (index < data.daily7DayForecast.length - 1)
                    return (
                        <li className={data.selectedDay.timestamp == temp.timestamp ? "active" : ""} key={index} onClick={() => setSelectedDay(temp)} data-tooltip data-tooltip-text={temp.condition} data-tooltip-place="bottom">
                            <p>{DAYS_IN_WEEK[date.getDay()]}</p>
                            <div className="weather-body-image">
                                <img src={`https://content.iot83.com/m83/other/weather${temp.condition}.png`} />
                            </div>
                            <h6>{Math.round(temp.max)}&deg; <small className="f-11">/</small> {Math.round(temp.min)}&deg;</h6>
                        </li>
                    )
            })}
        </ul>
    </div>
}

WeatherWidget.propTypes = {};

export default WeatherWidget;
