/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import React from "react";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

/* eslint-disable react/prefer-stateless-function */
class NotificationModal extends React.Component {
    componentDidMount() {
        if (Array.isArray(this.props.message2)) {
            this.props.message2.map(messageObj => {
                let msg = <p>{this.props.type === 'success' ? <i className="fad fa-check-circle"></i> : <i className="fad fa-exclamation-circle"></i>}{messageObj}</p>
                this.props.type === 'success' ? this.showToast(msg, true) : this.showToast(msg, false)
            });
        } else {
            let msg = <p>{this.props.type === 'success' ? <i className="fad fa-check-circle"></i> : <i className="fad fa-exclamation-circle"></i>}{this.props.message2}</p>
            this.props.type === 'success' ? this.showToast(msg, true) : this.showToast(msg, false)
        }
    }
    
    showToast = (message, isSuccess) => {
        let config = {
            position: "bottom-right",
            autoClose: 2500,
            hideProgressBar: false,
            closeOnClick: true,
            draggable: true,
            onClose: this.toastClosed,
        };
        isSuccess ? toast.success(message, config) : toast.error(message, config);

    }
    
    toastClosed = () => {
        this.props.onCloseHandler();
    }

    render() {
        return (
            <React.Fragment>
                <ToastContainer
                    position="bottom-right"
                    autoClose={2500}
                    newestOnTop={false}
                    closeOnClick
                    rtl={false}
                    pauseOnFocusLoss
                    draggable
                    pauseOnHover
                />
                <ToastContainer />
            </React.Fragment>
        );
    }
}

NotificationModal.propTypes = {};

export default NotificationModal;
