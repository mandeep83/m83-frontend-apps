/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * AddNewButton
 *
 */

import React, { Fragment } from "react";
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

import { FormattedMessage } from "react-intl";
import messages from "./messages";
/* eslint-disable react/prefer-stateless-function */
const  AddNewButton = (props) => {
 
    const { text1, text2, createItemOnAddButtonClick, imageIcon, addButtonEnable } = props
    return (
      <Fragment>
        <div className="content-add-wrapper">
        <div className="content-add-bg"><img src="https://content.iot83.com/m83/misc/m83-logo.png" /></div>
          <div className="content-add-detail">
            <div className="content-add-image-outer">
              <div className="content-add-image">
                <img src={`https://content.iot83.com/m83/misc/${imageIcon}`} />
              </div>
            </div>
            <h5>{text1}</h5>
            <h6>{text2}</h6>
            {(addButtonEnable == undefined || addButtonEnable) &&
              <button className="btn btn-primary" type="button" onClick={() => createItemOnAddButtonClick()}>
                <i className="far fa-plus" />Add new
            </button>
            }
          </div>
        </div>
      </Fragment>
    );
  
}

AddNewButton.propTypes = {};

export default AddNewButton;
