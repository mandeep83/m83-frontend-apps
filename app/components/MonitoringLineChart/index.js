/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * MonitoringLineChart
 *
 */

import React, { useEffect, useState, useRef } from "react";
import * as am4charts from "@amcharts/amcharts4/charts";
import * as am4core from "@amcharts/amcharts4/core";
import { getMiniLoader } from "../../commonUtils";
import { cloneDeep } from "lodash";


let ALARM_COLORS = {
  critical: "#e2030f",
  major: "#fb7f37",
  minor: "#fbbd50",
  warning: "#ffe680"
}

let getxAxes = (availableAttributes) => {
  let xAxes = availableAttributes.map((attr, index) => ({
    //"cursorTooltipEnabled": false,
    id: `xAxis${attr.attr}`,
    "startLocation": 0.5,
    "endLocation": 0.5,
    "type": "DateAxis",
    "dataFields": {
      "category": "category",
    },
    "renderer": {
      "minLabelPosition": 0.05,
      "maxLabelPosition": 0.95,
      "line": {
        "strokeOpacity": 0.5,
        "strokeWidth": 1,
      },
      "grid": {
        "strokeOpacity": 0.2,
        // "stroke": "#A0CA92",
        "strokeWidth": 0.3
      },
      "minGridDistance": 90,
      "labels": {
        "disabled": false,
        "fill": "#808080"
      },
      "opposite": index ? true : false,
    },
    "cursorTooltipEnabled": true,
    "tooltipDateFormat": "dd MMM yyyy HH:mm",
    "dateFormats": {
      "day": "dd MMM",
      "hour": "dd MMM HH:mm",
      "minute": "dd MMM HH:mm",
      "second": "HH:mm:ss",
      "millisecond": "HH:mm:ss"
    },
    "dateFormatter": { timezone: localStorage.timeZone },
  }))
  return xAxes
}

let getyAxes = (availableAttributes, alarmAttributes) => {
  let primaryAttribute = availableAttributes[0].attr;
  let yAxes = availableAttributes.map((attr, index) => ({
    "cursorTooltipEnabled": false,
    "type": "ValueAxis",
    "id": `yAxis${attr.attr}`,
    "title": {
      text: attr.attrDisplayName ? `${attr.attrDisplayName} ${attr.unit ? `(${attr.unit})` : ""}` : `${attr.attr} ${attr.unit ? `(${attr.unit})` : ""}`,
      fill: "#4a4a4a",
      strokeWidth: 1,
      fontSize: 12
    },
    minGridDistance: 75,
    renderer: {
      line: {
        strokeOpacity: 0.2,
        strokeWidth: 1,
      },
      labels: {
        fill: "#808080",
        adapter: {
          "text": function (text, target, key) {
            if (target._dataItem.value) {
              if (target._dataItem.value.toString().length > 9) {
                return target._dataItem.value.toExponential(3);
              }
              return target._dataItem.value.toString();
            }
          },
        }
      },
      opposite: index ? true : false,
    },
    baseValue: -1000,
    maxPrecision: 0,
    fontSize: 10,
  }))

  //** For providing color to y-Aaxis ranges according to Alarms */
  alarmAttributes.map(val => {
    if (val.attribute === primaryAttribute && val.isConfigured) {
      yAxes[0].axisRanges = [];
      const uniqueOperators = [...new Set(val.alarmCriteria.map(item => item.operator))];
      if (uniqueOperators.length === 1) {
        val.alarmCriteria.map(criteria => {
          yAxes[0].axisRanges.push({
            "series": "series0",
            "value": criteria.type === "critical" && criteria.operator === "LESS_THAN" ? -1000 : criteria.low,
            "endValue": criteria.type === "critical" && criteria.operator === "GREATER_THAN" ? 10000000 : criteria.high,
            "contents": {
              "stroke": ALARM_COLORS[criteria.type],
              "fill": ALARM_COLORS[criteria.type],
              "fillOpacity": 0.5
            }
          })
        })
      } else {
        val.alarmCriteria.map(criteria => {
          yAxes[0].axisRanges.push({
            "series": "series0",
            "value": criteria.type === "critical" && criteria.operator === "LESS_THAN" ? -1000 : criteria.low,
            "endValue": criteria.type === "critical" && criteria.operator === "GREATER_THAN" ? 10000000 : criteria.high,
            "contents": {
              "stroke": ALARM_COLORS[criteria.type],
              "fill": ALARM_COLORS[criteria.type],
              "fillOpacity": 0.5
            }
          })
        })
      }
    }
  })
  return yAxes;
}

let getSeries = (availableAttributes, operationAttribute, alarmAttributes, bulletsVisibility) => {
  let primaryAttribute = availableAttributes[0].attr;
  let alarms = alarmAttributes.find(val => val.attribute === primaryAttribute && val.isConfigured);

  //** For providing color to Bullets according to Alarms */
  function findColor(value) {
    if (alarms) {
      for (var i = 0; i < alarms.alarmCriteria.length; i++) {
        if (value < (alarms.alarmCriteria[i].type === "critical" && alarms.alarmCriteria[i].operator === "GREATER_THAN" ? 10000000 : alarms.alarmCriteria[i].high) && value >= (alarms.alarmCriteria[i].type === "critical" && alarms.alarmCriteria[i].operator === "LESS_THAN" ? -1000 : alarms.alarmCriteria[i].low)) {
          return ALARM_COLORS[alarms.alarmCriteria[i].type];
        }
      }
    }
  }

  let series = availableAttributes.map((attr, index) => {
    let strokeFill = index ? "#11A8E7" : "#00da00";
    let seriesObj = {
      "type": "LineSeries",
      "strokeWidth": 2,
      "id": "series" + index,
      "name": attr.attrDisplayName,
      "adapter": {
        tooltipText: function (text, target) {
          if (target.tooltipDataItem._dataContext) {
            return `${attr.attrDisplayName ? attr.attrDisplayName : attr.attr} : [bold]${target.tooltipDataItem._dataContext[attr.attr].toString().length > 9 ? target.tooltipDataItem._dataContext[attr.attr].toFixed(2) : target.tooltipDataItem._dataContext[attr.attr]} ${" " + attr.unit}`;
          }
        }
      },
      "tooltip": {
        getFillFromObject: false,
        background: {
          fill: "#211D21",
          fillOpacity: 1,
        },
        label: {
          fill: "#fff",
        }
      },
      "segments": {
        "fillModifier": {
          "type": "LinearGradientModifier",
          "opacities": [0.5, 0.2, 0.1, , 0, 0],
          "gradient": {
            "rotation": 90
          }
        }
      },
      "dataFields": {
        "valueY": attr.attr,
        "dateX": index && operationAttribute.value !== "rateOfChange" ? "timestamp" : "category",
      },
      "stroke": strokeFill,
      "fill": strokeFill,
      "fillOpacity": index ? 0 : 0.2,
      "filters": [{
        "type": "DropShadowFilter",
        "color": "#e6e6e6",
        "dx": 6,
        "dy": 6
      }],
      // "data": data.chartData
    };
    if (bulletsVisibility["series" + index]) {
      seriesObj.bullets = [{
        "isDynamic": true,
        "children": [{
          "type": "Circle",
          "fill": am4core.color("#fff"),
          "cursorOverStyle": am4core.MouseCursorStyle.pointer,
          "resizable": true,
          "width": 6,
          "height": 6,
          "states": {
            "hover": {
              "properties": {
                "scale": 1.7
              }
            }
          },
          "strokeWidth": 3,
          "adapter": {
            "stroke": function (stroke, target) {
              if (target.dataItem && !index) {
                return findColor(target.dataItem.valueY);
              }
            }
          },
          "filters": [{
            "type": "DropShadowFilter",
            "color": "#B8B8B8",
            "dx": 3,
            "dy": 3
          }]
        }],
      }]
    }
    if (index) {
      seriesObj.xAxis = `xAxis${attr.attr}`;
      seriesObj.yAxis = `yAxis${attr.attr}`;
    }
    return seriesObj;
  });
  return series;
}

let getFinalChartData = (chartData, operationData, operationAttributeValue, primaryAttribute) => {
  let finalChartData = chartData;
  if (operationAttributeValue) {
    if (operationAttributeValue === "rateOfChange") {
      finalChartData = operationData;
    } else {
      finalChartData = [...chartData, ...(operationData.map(item => (
        {
          [operationAttributeValue]: item[primaryAttribute],
          timestamp: item.category,
        }
      )))]
    }
  }
  return finalChartData;
}

let getFinalAvailableAttributes = (availableAttributes, operationAttribute) => {
  let operationAttributeValue = operationAttribute.value;
  if (operationAttributeValue) {
    availableAttributes = cloneDeep(availableAttributes);
    let primaryAttribute = availableAttributes[0].attr;
    availableAttributes.push({
      "attr": operationAttributeValue === "rateOfChange" ? primaryAttribute + "Rate" : operationAttributeValue,
      "attrDisplayName": operationAttribute.displayName,
      "unit": availableAttributes[0].unit,
    });
  }
  return availableAttributes;
}

let getLegend = (operationAttributeValue) => {
  if (operationAttributeValue) {
    return {
      useDefaultMarker: true,
      markers: {
        width: 15,
        height: 15
      },
      labels: {
        textDecoration: "none",
      }
    };
  }
}

let getChartConfig = (props) => {
  let { chartId, operationAttribute = {}, data: { data: { chartData, availableAttributes }, operationData }, alarmAttributes } = props;
  let operationAttributeValue = operationAttribute.value;
  let primaryAttribute = availableAttributes[0].attr;
  let finalChartData = getFinalChartData(chartData, operationData, operationAttributeValue, primaryAttribute);
  availableAttributes = getFinalAvailableAttributes(availableAttributes, operationAttribute);
  let bulletsVisibility = getBulletsVisibilityStatus(chartData, operationData, operationAttributeValue);
  let config = {
    "type": "XYChart",
    "container": chartId,
    "maskBullets": false,
    "data": finalChartData,
    "width": "100%",
    "height": "100%",
    "xAxes": getxAxes(availableAttributes),
    "yAxes": getyAxes(availableAttributes, alarmAttributes),
    "series": getSeries(availableAttributes, operationAttribute, alarmAttributes, bulletsVisibility),
    "legend": getLegend(operationAttributeValue),
    "cursor": {
      "behavior": "zoomXY"
    },
    "responsive": {
      "enabled": true,
    },
    "preloader": {
      "enabled": true
    }
  };

  return config;
}

let getBulletsVisibilityStatus = (chartData, operationdata, operation) => {
  let visibilityStatus = {
    series0: false,
    series1: false,
  }
  if (operation) {
    if (operation === "rateOfChange") {
      if (operationdata.length < 4) {
        visibilityStatus = {
          series0: true,
          series1: true,
        }
      }
    } else {
      visibilityStatus = {
        series0: chartData.length < 4,
        series1: operationdata.length < 4,
      }
    }
  } else if (chartData.length < 4) {
    visibilityStatus = {
      series0: true,
      series1: true,
    }
  }
  return visibilityStatus
}

function MonitoringLineChart(props) {
  let { data: { data: { chartData, availableAttributes }, operationData }, timeZone, operationAttribute } = props;
  let chartRef = useRef(null);
  let primaryDataRef = useRef(null);
  let operationDataRef = useRef(null);

  let createChart = () => {
    primaryDataRef.current = chartData;
    operationDataRef.current = operationData;
    let chartConfig = getChartConfig(props);
    let chart = am4core.createFromConfig(chartConfig, props.chartId, am4charts.XYChart);
    chart.events.on('ready', () => {
      chartRef.current = chart
    });
    return () => {
      chart.dispose()
    }
  }

  useEffect(createChart, []);

  useEffect(() => {
    let requiredChart = chartRef.current;
    if (requiredChart && !operationAttribute.value) {
      // requiredChart.addData(chartData, chartData.length);
      let oldTime = requiredChart.data[requiredChart.data.length - 1].category;
      let dataPoints = props.data.data.chartData.filter(val => val.category > oldTime)
      requiredChart.addData(dataPoints, dataPoints.length);
    }
  }, [chartData]);

  useEffect(() => {
    let requiredChart = chartRef.current;
    if (requiredChart && operationAttribute.value) {
      let previousPrimaryData = primaryDataRef.current;
      let oldTime = previousPrimaryData[previousPrimaryData.length - 1].category;
      let newPrimaryData = chartData.filter(val => val.category > oldTime);
      primaryDataRef.current = chartData;

      let previousOperationData = operationDataRef.current;
      let oldOperationDataTime = previousOperationData[previousOperationData.length - 1].category;
      let newOperationData = operationData.filter(val => val.category > oldOperationDataTime);
      operationDataRef.current = chartData;

      let allNewItems = [...newPrimaryData, ...newOperationData];

      requiredChart.addData(allNewItems, allNewItems.length);
    }
  }, [operationData]);

  useEffect(() => {
    let requiredChart = chartRef.current;
    if (requiredChart) {
      requiredChart.dispose();
      createChart();
    }
  }, [timeZone]);

  return <React.Fragment>
    <div id={props.chartId} className="h-100" />
  </React.Fragment>;
}

MonitoringLineChart.propTypes = {};

export default React.memo(MonitoringLineChart);
