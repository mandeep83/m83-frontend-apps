/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import React from "react";
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

import { FormattedMessage } from "react-intl";
import messages from "./messages";
import json from "../FontAwesomeDisplayList/fontAwesome.json"
import Select from 'react-select';
import cloneDeep from 'lodash/cloneDeep';


/* eslint-disable react/prefer-stateless-function */
class FontAwesomeDisplayList extends React.Component {

  state = {
    selectedIcon: this.props.value ? {
      value: this.props.value,
      label: <span><i className={`mr-r-10 ${this.props.value}`}></i>{this.props.value}</span>
    } : ""
  }


  createOptionsForSelect = () => {
    let options = cloneDeep(json.icons)
    options = this.props.filterOptions ? options.filter(el => el.split(" ")[0] === this.props.filterOptions) : options
    options = options.map((option) => {
      return {
        value: option,
        label: <span><i className={`mr-r-10 ${option}`}></i>{option}</span>
      }
    })
    return options;
  }

  shouldComponentUpdate(nextProps, nextState) {
    return nextProps.value !== this.props.value || this.props.disabled !== nextProps.disabled
  }

  handleChangeForReactSelect = (value) => {
    let selectedIcon = "", selectedIconSend = ""
    if (value) {
      selectedIcon = value
      selectedIconSend = value.value
    }
    this.props.changeHandlerForSelect(selectedIconSend)
    this.setState({ selectedIcon, selectedIconSend })
  }

  render() {
    return (
      <Select
        isDisabled={this.props.disabled}
        value={this.state.selectedIcon}
        isClearable={true}
        onChange={this.handleChangeForReactSelect}
        options={this.createOptionsForSelect()}
        className="form-control-multi-select"
      />
    );
  }
}

FontAwesomeDisplayList.propTypes = {};

export default FontAwesomeDisplayList;
