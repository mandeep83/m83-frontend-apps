/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * CollapseOrShowAllFilters
 *
 */

import React from "react";
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

/* eslint-disable react/prefer-stateless-function */
let ALL_FILTERS = [
    "deviceType__filter__",
    "groups__filter__",
    "tags__filter__",
    "alarms__filter__",
    "geolocation__filter__",
    "status__filter__",
    "duration__filter__",
    "from__filter__",
    "to__filter__",
]

class CollapseOrShowAllFilters extends React.Component {
    state = {
        activeClassName: "show"
    }
    
    toggleFiltersVisibility = () => {
        let activeClassName = this.state.activeClassName === "show" ? "collapse" : "show"
        ALL_FILTERS.forEach(filter => {
            let element = document.getElementById(filter)
            if (element) {
                element.className = activeClassName
            }
        })
        this.setState({
            activeClassName
        })
    }
    
    render() {
        return <React.Fragment>
            <button className="btn" onClick={() => this.toggleFiltersVisibility()}>
                {this.state.activeClassName === "collapse" ? <i className="fas fa-caret-down"></i> : <i className="fas fa-caret-up"></i>}
            </button>
        </React.Fragment>;
    }
}

CollapseOrShowAllFilters.propTypes = {};

export default CollapseOrShowAllFilters;
