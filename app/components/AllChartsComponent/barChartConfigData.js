import * as am4core from "@amcharts/amcharts4/core";

export function createBarFromConfig(widgetType, chartData, id) {


    let series = chartData.availableAttributes.map((attr, index) => {
        return {
            "type": "ColumnSeries",
            "name": attr,
            "dataFields": {
                "valueY": attr,
                "categoryX": "category"
            },
            "columns": {
                "tooltipText": "Category: {categoryX}\nValue: {valueY}",
            }
        }
    })

    let config = {
        "type": "XYChart",
        "container": id,
        "data": chartData.chartData,
        "legend": {
            "position": "bottom",
            "marginBottom": 10,
            "marginTop": 10,
        },
        "width": "100%",
        "height": "100%",
        "xAxes": [{
            "type": "CategoryAxis",
            "dataFields": {
                "category": "category"
            }
        }],

        "yAxes": [{
            "type": "ValueAxis"
        }],

        "series": series,
        "cursor": {
            "behavior": "zoomXY"
        },
    };
    if (chartData.chartData.length && chartData.chartData[0].timestamp) {
        config.xAxes = [{
            "type": "DateAxis",
            id: "a1",
            "cursorTooltipEnabled": true,
            "tooltipDateFormat": "dd MMM yyyy HH:mm",
            // "dateFormats": {
            //     "day": "dd MMM",
            //     "hour": "dd MMM HH:mm",
            //     "minute": "dd MMM HH:mm",
            //     "second": "HH:mm:ss",
            //     "millisecond": "HH:mm:ss"
            // },
            baseInterval: {
                "timeUnit": "second",
                "count": 1
            },
            groupData: false,
            dateFormatter: { "timezone": localStorage.timeZone },
            "renderer": {
                "labels": {
                    "wrap": true,
                    "maxWidth": 90
                }
            },
            "fontSize": "12px",
        }]
        config.series = config.series.map(el => {
            el.dataFields = {
                "valueY": el.dataFields.valueY,
                "dateX": "timestamp",
            }
            return el
        })
    }
    return config;
}