export function createLineFromConfig(chartSubType, chartData, id) {
    let series = chartData.availableAttributes && chartData.availableAttributes.map((attr, index) => {
        let seriesObj = {
            "type": "LineSeries",
            "strokeWidth": 2,
            "id": "series" + index,
            "name": attr,
            "yAxis": chartSubType === "multiAxisChart" ? attr : false,
            "tooltip": {
                "getFillFromObject": false,
                "background": {
                    "fill": "#000",
                    "filters": [{
                        "opacity": 2
                    }]
                },
                "label": {
                    "fill": "#fff"
                }
            },
            "tooltipText": attr + " : {" + attr + "} " + (chartData.units && chartData.units[attr] ? chartData.units[attr] : ""),
            "propertyFields": {
                "stroke": ""
            },
            "fillOpacity": "",
            "dataFields": {
                "valueY": attr,
                "categoryX": "category",
            },
        }
        return seriesObj
    })

    let config = {
        "type": "XYChart",
        "container": id,
        "data": chartData.chartData,
        "legend": {
            "position": "bottom",
            "marginBottom": 10,
            "marginTop": 10,
        },
        "width": "100%",
        "height": "100%",
        "xAxes": [{
            id: "a1",
            "type": "CategoryAxis",
            "dataFields": {
                "category": "category",
            },
            // dateFormatter: { timezone: localStorage.timeZone },
            "renderer": {
                "labels": {
                    "wrap": true,
                    "maxWidth": 90
                }
            },
            "fontSize": "12px",
        }],
        "cursor": {
            "behavior": "zoomXY"
        },
        "yAxes": [{
            "type": "ValueAxis",
            renderer: {
                // opposite: true,
            },
            // min: 0,
            // max: 100,
            axisRanges: {
                // "type": "range",
                // "value": 50,
                // "endValue": 1200,
                "contents": {
                    "stroke": "#FF0000",
                    "fill": "#FF0000"
                },
            },
            // baseValue: 50
        }],
        "responsive": {
            "enabled": true,
        },
        "series": series,
    };

    switch (chartSubType) {
        case "simpleLineChartWithScrollAndZoom":
            config.scrollbarX = {
                "type": "XYChartScrollbar",
                "series": chartData.availableAttributes.map((attr, index) => "series" + index), //make it dynamic with above series
            }
            break;
        case "simpleLineChart":
            break;
        case "simpleLineChartWithArea":
            series.map(series => {
                series.fillOpacity = "0.5"
            })
            break;
        case "simpleLineChartWithArea&Scroll":
            series.map(series => {
                series.fillOpacity = "0.5"
            })
            config.scrollbarX = {
                "type": "XYChartScrollbar",
                "series": chartData.availableAttributes.map((attr, index) => "series" + index), //make it dynamic with above series
            }
            break;
        case "smoothedLineChart":
            series.map(series => {
                series.tensionX = 0.8
                series.tensionY = 1
            })
            break;
        case "stepLineChart":
            series.map(series => {
                series.type = "StepLineSeries"
                delete series.bullets
            })
            config.cursor = {
                xAxis: "a1",
                "fullWidthLineX": true,
                "lineX": {
                    "strokeWidth": 0,
                    "fill": "#8F3985",
                    "fillOpacity": 0.1
                },
                "behavior": "zoomXY"
            }
            break;
        case "multiAxisChart":
            series.map(series => {
                series.tensionX = 0.8
                series.tensionY = 1
            })
            config.yAxes = chartData.availableAttributes.map((attr, index) => {
                return {
                    "type": "ValueAxis",
                    renderer: {
                        opposite: index % 2 === 0 ? false : true,
                        line: {
                            strokeOpacity: 1,
                            strokeWidth: 2,
                            stroke: series.stroke
                        },
                        labels: { template: { fill: series.stroke } }
                    },
                    id: attr,
                    axisRanges: {
                        "contents": {
                            "stroke": "#FF0000",
                            "fill": "#FF0000"
                        },
                    },
                    title: {
                        text: attr
                    }
                }
            })
            break;
        default:
    }
    if (chartData.chartData.length && chartData.chartData[0].timestamp) {
        config.xAxes = [{
            "type": "DateAxis",
            id: "a1",
            "cursorTooltipEnabled": true,
            "tooltipDateFormat": "dd MMM yyyy HH:mm",
            "dateFormats": {
                "day": "dd MMM",
                "hour": "dd MMM HH:mm",
                "minute": "dd MMM HH:mm",
                "second": "HH:mm:ss",
                "millisecond": "HH:mm:ss"
            },
            groupData: false,
            dateFormatter: { "timezone": localStorage.timeZone },
            "renderer": {
                "labels": {
                    "wrap": true,
                    "maxWidth": 90
                }
            },
            "fontSize": "12px",
        }]
        config.series = config.series.map(el => {
            el.dataFields = {
                "valueY": el.dataFields.valueY,
                "dateX": "timestamp",
            }
            return el
        })
        // config.scrollbarX = {
        //     "type": "XYChartScrollbar",
        //     "series": "series0",
        // }
    }
    return config;

}
