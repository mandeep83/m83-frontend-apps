/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * AllChartsComponent
 *
 */

import React from "react";
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import am4themes_spiritedaway from "@amcharts/amcharts4/themes/spiritedaway";
import am4themes_moonrisekingdom from "@amcharts/amcharts4/themes/moonrisekingdom";
import am4themes_frozen from "@amcharts/amcharts4/themes/frozen";
import am4themes_kelly from "@amcharts/amcharts4/themes/kelly";
import am4themes_material from "@amcharts/amcharts4/themes/material";
import am4themes_dataviz from "@amcharts/amcharts4/themes/dataviz";
import { createLineFromConfig } from '../AllChartsComponent/lineConfig';
import { createBarFromConfig } from '../AllChartsComponent/barChartConfigData';

let chart = {}

class AllChartsComponent extends React.Component {

  setTheme = (theme) => {
    let allThemes = {
      "spiritedAway": am4themes_spiritedaway,
      "moonriseKingdom": am4themes_moonrisekingdom,
      "frozen": am4themes_frozen,
      "kelly's": am4themes_kelly,
      "material": am4themes_material,
      "dataviz": am4themes_dataviz
    };
    am4core.addLicense("CH145187641");
    Object.entries(allThemes).map(([name, am4theme]) => {
      if (name === theme) {
        am4core.useTheme(am4theme)
      }
      else
        am4core.unuseTheme(am4theme)
    })
  }
  componentDidMount() {
    this.getChartConfig(this.props.config)
  }

  shouldComponentUpdate(nextProps) {
    let dataChanged = JSON.stringify(nextProps.config.chartData) !== JSON.stringify(this.props.config.chartData);
    if (dataChanged) {
      chart[nextProps.config.id].data = nextProps.config.chartData.chartData
    }
    if (nextProps.timeZone !== this.props.timeZone) {
      chart[this.props.config.id].dispose();
      delete chart[this.props.config.id];
      this.getChartConfig(this.props.config)
    }
    return false
  }


  getChartConfig = ({ chartType, chartSubType, chartData, id, theme }) => {
    let chartConfig = this.getWidgetTypeConfig(chartType, chartSubType, chartData, id);
    chart[id] = am4core.createFromConfig(chartConfig);
    this.setTheme(theme);
  }

  getWidgetTypeConfig = (chartType, chartSubType, chartData, id) => {
    chartData = JSON.parse(JSON.stringify(chartData));
    switch (chartType) {
      case "lineChart":
        return createLineFromConfig(chartSubType, chartData, id);

      case "barChart":
        return createBarFromConfig(chartSubType, chartData, id);

    }
  }

  componentWillUnmount() {
    if (chart[this.props.config.id]) {
      chart[this.props.config.id].dispose();
      delete chart[this.props.config.id];
    }
  }

  render() {
    return (
      <div id={this.props.config.id} className="h-100"></div>
    );
  }
}

AllChartsComponent.propTypes = {};

export default AllChartsComponent;
