/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * DeleteModel
 *
 */

import React from "react";
import ClientCaptcha from "react-client-captcha";

class DeleteModel extends React.Component {
    state = {
        givenCaptchaCode: '',
        userInput: '',
        deleteRepo: false,
        isConfirm: false,
    }

    inputHandler = (event) => {
        this.setState({
            userInput: event.target.value,
            isConfirm: this.state.givenCaptchaCode === event.target.value ? true : false,
        })
    }

    verificationHandler = () => {
        this.props.confirmClicked(this.state.deleteRepo);
    }

    render() {
        return (
            <div className="modal show d-block animated slideInDown" id="deleteModal">
                <div className="modal-dialog modal-dialog-centered">
                    <div className="modal-content">
                        <div className="modal-body">
                            <div className="delete-content">
                                <div className="delete-icon">
                                    <i className="fad fa-trash-alt text-red"></i>
                                </div>
                                <h4>{this.props.title ? this.props.title : "Delete!"}</h4>
                                <h6>Are you sure you want to {this.props.status ? this.props.status : "delete"} <strong className="text-gray">{this.props.projectName ? this.props.projectName : ""}</strong> ?</h6>
                                <h6>It will clear all the data associated with this device type !</h6>
                                <h6 className="text-red">This action can't be undone !</h6>
                                {this.props.isSourceControlEnabled &&
                                    <label className="checkboxLabel">
                                        <span className="checkText">Delete associated git repository ?</span>
                                        <input type="CheckBox" disabled={!(localStorage.isGitHubLogin == "true")} onChange={() => this.setState({ deleteRepo: !this.state.deleteRepo })} />
                                        <span className="checkmark"></span>
                                    </label>
                                }
                                <div className="form-group captcha-box">
                                    <ClientCaptcha captchaClassName="captcha-text" captchaCode={code => this.setState({ givenCaptchaCode: code, isConfirm:false, userInput:''})} />
                                    <input type="text" className="form-control" placeholder="Enter captcha" value={this.state.userInput} onChange={() => this.inputHandler(event)}></input>
                                </div>
                            </div>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-dark" onClick={() => {this.props.cancelClicked()}}>Cancel</button>
                            <button type="button" className="btn btn-success" disabled={!this.state.isConfirm} onClick={() => this.verificationHandler()}>Delete</button>
                        </div>
                    </div>
                </div>
            </div >
        )
    }
}

DeleteModel.propTypes = {};

export default DeleteModel;
