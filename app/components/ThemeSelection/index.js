/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * ThemeSelection
 *
 */

import React from "react";
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

import { FormattedMessage } from "react-intl";
import messages from "./messages";

/* eslint-disable react/prefer-stateless-function */
class ThemeSelection extends React.Component {
    state = {
        isShow: false,
    }
    
    componentWillReceiveProps(nextProps) {
        if(nextProps.show !== this.props.isShow){
            this.setState({ isShow : nextProps.show })
        }
    }
    
    shouldComponentUpdate(nextProps){
        if(nextProps.show !== this.props.isShow){
            return true
        }
        return false
    }
    
    closeThemePopUp = () => {
        localStorage.removeItem("showThemePopUp");
        this.setState({isShow : undefined})
    }
    
    render() {
        return (
            this.state.isShow ?
                <div className="theme-wrapper animated slideInUp">
                    <button type="button" className="btn-transparent btn-transparent-blue" onClick={() => this.closeThemePopUp()} ><i className="far fa-angle-double-down" /></button>
                    <ul className="theme-list">
                        {this.props.themes.map((val, index) =>
                        <li key={index} onClick={() => this.props.applyTheme(val.id)}>
                            <div className={`theme-list-box ${val.isUserSelectedTheme ? "active" : ""}`} style={{backgroundColor: val.darkThemeColor}}>
                                <p>Flex83</p>
                                <h6>{val.displayName}</h6>
                                <div className={`theme-list-border ${val.isSelecting ? "blink-animation" : ""}`} style={{backgroundColor: val.themeColor}}></div>
                            </div>
                        </li>
                        )}
                    </ul>
                </div>
                :
                null
        );
    }
}

ThemeSelection.propTypes = {};

export default ThemeSelection;
