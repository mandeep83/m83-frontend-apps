/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * PageStudioErrorBoundry
 *
 */

import React from "react";
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

/* eslint-disable react/prefer-stateless-function */
class PageStudioErrorBoundry extends React.Component {
  state = { 
    error: false 
  };

  componentDidUpdate() {
      if (this.state.error) {
          this.setState({
              error: false,
          })
      }
  }

  componentDidCatch(error, errorInfo) {
      this.setState({
          error: error,
      }, () => this.props.errorHandler());
  }
  render() {
      if (this.state.error) {
          return (<React.Fragment />)
      }
      return this.props.children || <p> Invalid Data </p>;
  }
}

PageStudioErrorBoundry.propTypes = {};

export default PageStudioErrorBoundry;
