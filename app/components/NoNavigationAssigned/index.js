/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import React from "react";
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

import { FormattedMessage } from "react-intl";
import messages from "./messages";

/* eslint-disable react/prefer-stateless-function */
class NoNavigationAssigned extends React.Component {
  render() {
    return (
      <div className="error-box">
        <div className="error-content">
          <h5>Oops! No Navigation Assigned</h5>
          <h1>
            <span>4</span>
            <span>0</span>
            <span>4</span>
          </h1>
          <h6>
            No Navigation Assigned <br />
          </h6>
        </div>
      </div>
    );
  }
}

NoNavigationAssigned.propTypes = {};

export default NoNavigationAssigned;
