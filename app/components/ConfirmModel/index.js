/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import React from "react";

function ConfirmModel(props) {
    return (
        <div className="modal show d-block animated slideInDown" id="deleteModal">
            <div className="modal-dialog modal-dialog-centered">
                <div className="modal-content">
                    <div className="modal-body">
                        <div className="delete-content">
                            <div className="delete-icon">
                                <i className="fad fa-trash-alt text-red"></i>
                            </div>
                            <h4>{props.title ? props.title : "Delete!"}</h4>
                            <h6>Are you sure you want to {props.status ? props.status : "delete"} {props.message1 ? props.message1 : ""} <strong className="text-gray">{props.deleteName ? props.deleteName : ""}</strong> ?</h6>
                        </div>
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-dark" onClick={() => { props.cancelClicked() }}>No</button>
                        <button type="button" className="btn btn-success" onClick={() => { props.confirmClicked() }}>Yes</button>
                    </div>
                </div>
            </div>
        </div >
    )
}
ConfirmModel.propTypes = {};

export default ConfirmModel;


