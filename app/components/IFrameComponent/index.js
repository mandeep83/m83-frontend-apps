/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * IFrameComponent
 *
 */

import React from "react";
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

function IFrameComponent() {
  let url = window.API_URL.slice(0, -1)

  switch (window.location.pathname) {
    case "/grafana":
      url = url + ":3000"
      break;
    case "/node-red":
      url = url + ":1880"
      break;
    case "/jupyter":
      url = url + ":8000"
      break;
  }


  return <div className="helpOuterBox pd-2">

    <iframe src={url} height="100%" width="100%" name="iframe_a" />
  </div>
}

IFrameComponent.propTypes = {};

export default IFrameComponent;
