/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import React from "react";
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

class Iframe extends React.Component {
  componentDidMount() {
    this.createIframe(this.props.html, this.props.css, this.props.js, this.props.setting);
  };
  shouldComponentUpdate(nextProps) {
    if (nextProps.html != this.props.html || nextProps.css != this.props.css || nextProps.js != this.props.js || JSON.stringify(nextProps.setting) != JSON.stringify(this.props.setting)) {
      this.createIframe(nextProps.html, nextProps.css, nextProps.js, nextProps.setting);
    }
    return false;
  }
  createIframe(html, css, js, setting) {
    const iframe = this.refs.iframe;
    const document = iframe.contentDocument;
    let cdnList = '';

    Object.entries(setting).map(([key, value]) =>
    value.cdnList && value.cdnList.map(cdn => {
        if (key === "css") {
          cdnList += `<link href=${cdn.value} rel="stylesheet"></link>`
        }
        else {
          cdnList += `<script src=${cdn.value}></script>`
        }
      })
    )

    const documentContents = `
        <!DOCTYPE html>
        <html lang="en">
          <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta http-equiv="X-UA-Compatible" content="ie=edge">
            <title>Document</title>
            ${cdnList}    
            <style type="text/css">
              body {
                margin: 0;
                padding: 0;
                border: 0;
                width: 100%;
              }
              html {
                height: 100%;
              }
              ${css}
            </style>
          </head>
          <body>
              ${html}
            <script type=${setting.js.preProcessor === "BABEL" ? 'text/babel' : 'text/javascript'}>
              ${js}
              </script>
              </body>
              </html>
              `;

    document.open();
    document.write(documentContents);
    document.close();
  }
  render() {
    return <iframe title="result" className="iframe" id={this.props.id} ref="iframe" />;
  }
}

Iframe.propTypes = {};

export default Iframe;