/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * RpcImageUploader
 *
 */

import React from "react";
import ImageUploader from "../ImageUploader";
import { GetHeaders } from "../../commonUtils";
import axios from "axios";
import { withRouter } from 'react-router-dom';
function RpcImageUploader(props) {
    
    let deleteRPCImageAsync = () => {
        let isNewCommand = !props.controlCommandId;
        if (isNewCommand) {
            props.setControlImageDetails({
                controlImageUrl: "",
                imageFile: "",
            })
            return;
        }
        
        props.deleteRpcImageHandler();
        axios.delete(window.API_URL + `api/v3/connectors/control/removeIcon/${props.controlCommandId}`, GetHeaders()).then(res => {
            props.onDeleteImage()
        }).catch(error => {
            if (error.response) {
                
                if (error.response.status === 401) {
                    delete localStorage.token;
                    delete localStorage.showThemePopUp;
                    delete localStorage.selectedProjectId
                    props.history.push('/login');
                } else if (error.response.status === 402) {
                    if (localStorage.tenantType === 'FLEX') {
                        props.deletionErrorCallBack(error.response.data)
                        
                    } else if (localStorage.role === 'DEVELOPER') {
                        props.deletionErrorCallBack(error.response.data.message)
                    } else {
                        delete localStorage.showThemePopUp;
                        delete localStorage.token;
                        delete localStorage.selectedProjectId
                        props.deletionErrorCallBack(rror.response.data.message);
                        
                    }
                } else if (error.response.status === 400) {
                    props.deletionErrorCallBack(error.response.data.data || error.response.data.error)
                }
                else {
                    props.deletionErrorCallBack(error.response.data)
                    
                }
            } else {
                props.deletionErrorCallBack(error.message);
            }
        })
    }
    
    let connectorImageUploadHandler = React.useCallback((file) => {
        const reader = new FileReader();
        reader.addEventListener("load", () => {
            props.setControlImageDetails({
                controlImageUrl: reader.result,
                imageFile: file
            })
        }, false);
        
        if (file) {
            reader.readAsDataURL(file);
        }
    })
    
    return (
        <div className="form-group">
            <label className="form-group-label">Control Image :
                <button type="button" className="btn btn-link" disabled={!props.controlImageUrl} onClick={deleteRPCImageAsync}>
                    <i className="far fa-redo-alt"></i>Reset
                </button>
            </label>
            <div className="file-upload-box">
                <ImageUploader
                    imagePath={props.controlImageUrl}
                    uploadHandler={connectorImageUploadHandler}
                    classNames={{ blankUploader: "file-upload-form" }}
                    acceptFileType="image/x-png,image/gif,image/jpeg"
                />
            </div>
        </div>
    )
}

RpcImageUploader.propTypes = {};

export default withRouter(RpcImageUploader);
