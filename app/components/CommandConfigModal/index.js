/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * CommandConfigModal
 *
 */

import React from "react";
// import PropTypes from 'prop-types';
// import styled from 'styled-components';
import JSONInput from "react-json-editor-ajrm/dist";
import isPlainObject from 'lodash/isPlainObject'
import cloneDeep from 'lodash/cloneDeep'
import ImageUploader from "../ImageUploader";
import RpcImageUploader from "../RpcImageUploader";

/* eslint-disable react/prefer-stateless-function */
class CommandConfigModal extends React.Component {
    state = {
        COMMAND_OBJ: this.props.COMMAND_OBJ,
        deviceTypeName: this.props.deviceTypeName,
        controlImageUrl: this.props.commandImage,
    }

    commandChangeHandler = (value, keyName) => {
        let COMMAND_OBJ = cloneDeep(this.state.COMMAND_OBJ)
        if (keyName == "controlCommand") {
            COMMAND_OBJ[keyName] = value
        } else {
            COMMAND_OBJ[keyName] = value
        }
        this.setState({
            COMMAND_OBJ
        })
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.commandModalLoader !== this.props.commandModalLoader) {
            this.setState({
                commandModalLoader: nextProps.commandModalLoader
            })
        }
    }

    uploadConnectorImage = (file) => {
        const reader = new FileReader();
        reader.addEventListener("load", () => {
            this.setState({
                controlImageUrl: reader.result,
                imageFile: file
            })
        }, false);

        if (file) {
            reader.readAsDataURL(file);
        }
    }

    setControlImageDetails = (controlImageDetails) => {
        this.setState(controlImageDetails);
    }

    onDeleteRPCImage = () => {
        this.setState({
            commandModalLoader: false,
        }, this.props.onDeleteRPCImage)
    }

    RPCImagedeletionErrorCallBack = (errorMessage)=> {
        this.setState({
            commandModalLoader: false,
        }, () => this.props.showNotification("error", errorMessage))
    }

    deleteRpcImageHandler = () => {
        this.setState({
            commandModalLoader: true,
        })
    }

    render() {
        return (
            <div className="modal d-block animated slideInDown" id="commandModal" role="dialog">
                <div className="modal-dialog modal-lg modal-dialog-centered">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h6 className="modal-title">{this.state.COMMAND_OBJ.isEdit ? this.state.COMMAND_OBJ.controlName : "Add"} Control for - <span>{this.state.deviceTypeName}</span>
                                <button type="button" className="close" data-tooltip data-tooltip-text="Close" data-tooltip-place="bottom" onClick={this.props.closeCommandModal} data-dismiss="modal"><i className="far fa-times"></i></button>
                            </h6>
                        </div>

                        {this.state.commandModalLoader ?
                            <div className="inner-loader-wrapper" style={{ height: 506 }}>
                                <div className="inner-loader-content">
                                    <i className="fad fa-sync-alt fa-spin"></i>
                                </div>
                            </div>
                            :
                            <React.Fragment>
                                <div className="modal-body">
                                    <div className="d-flex">
                                        <div className="flex-35 pd-r-10">
                                            <div className="form-group">
                                                <label className="form-group-label">Control Name :  <i className="fas fa-asterisk form-group-required"></i></label>
                                                <input placeholder="e.g. Reboot or Reset or Power Off" className="form-control" onChange={(e) => this.commandChangeHandler(e.currentTarget.value, "controlName")} value={this.state.COMMAND_OBJ.controlName} />
                                            </div>
                                            <RpcImageUploader
                                                controlImageUrl={this.state.controlImageUrl}
                                                setControlImageDetails={this.setControlImageDetails}
                                                controlCommandId={this.state.COMMAND_OBJ.id}
                                                onDeleteImage={this.onDeleteRPCImage}
                                                deleteRpcImageHandler={this.deleteRpcImageHandler}
                                                deletionErrorCallBack={this.RPCImagedeletionErrorCallBack}
                                            />
                                        </div>
                                        <div className="flex-65 pd-l-10">
                                            <div className="json-input-box">
                                                <JSONInput
                                                    theme="light_mitsuketa_tribute"
                                                    value={this.state.COMMAND_OBJ.controlCommand}
                                                    onChange={(data) => this.commandChangeHandler(data.jsObject, "controlCommand")}
                                                    placeholder={this.state.COMMAND_OBJ.controlCommand}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-dark" onClick={this.props.closeCommandModal} data-dismiss="modal">Cancel</button>
                                    <button type="button" className="btn btn-success" onClick={() => this.props.saveCommandsHandler(this.state.COMMAND_OBJ, this.state.imageFile)}
                                        disabled={(!this.state.COMMAND_OBJ.controlName.trim() || !isPlainObject(this.state.COMMAND_OBJ.controlCommand))}>{this.state.COMMAND_OBJ.isEdit ? "Update" : "Save"}
                                    </button>
                                </div>
                            </React.Fragment>
                        }
                    </div>
                </div>
            </div>
        );
    }
}

CommandConfigModal.propTypes = {};

export default CommandConfigModal;
