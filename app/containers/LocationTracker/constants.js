/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/*
 *
 * LocationTracker constants
 *
 */
export const RESET_TO_INITIAL_STATE = "app/LocationTracker/RESET_TO_INITIAL_STATE";

export const GET_MQTT_CREDENTIALS = {
	action: 'app/LocationTracker/GET_MQTT_CREDENTIALS',
	success: "app/LocationTracker/GET_MQTT_CREDENTIALS_SUCCESS",
	failure: "app/LocationTracker/GET_MQTT_CREDENTIALS_FAILURE",
	urlKey: "getMQTTCredentials",
	successKey: "mqttCredentialsSuccess",
	failureKey: "mqttCredentialsFailure",
	actionName: "getMQTTCredentials",
	actionArguments: ["deviceTypeId"]
}

export const GET_DEVICE_TYPE_BY_ID = {
	action: 'app/LocationTracker/GET_DEVICE_TYPE_BY_ID',
	success: "app/LocationTracker/GET_DEVICE_TYPE_BY_ID_SUCCESS",
	failure: "app/LocationTracker/GET_DEVICE_TYPE_BY_ID_FAILURE",
	urlKey: "getDeviceTypeById",
	successKey: "getDeviceTypeByIdSuccess",
	failureKey: "getDeviceTypeByIdFailure",
	actionName: "getDeviceTypeById",
	actionArguments: ["id"]
}

export const GET_DEVICE_INFO = {
    action : 'app/LocationTracker/GET_DEVICE_INFO',
    success: "app/LocationTracker/GET_DEVICE_INFO_SUCCESS",
    failure: "app/LocationTracker/GET_DEVICE_INFO_FAILURE",
    urlKey: "getDetailsPageData",
    subKey: "deviceInfo",
    successKey: "getDeviceInfoSuccess",
    failureKey: "getDeviceInfoFailure",
    actionName: "getDeviceInfo",
    actionArguments : ["connectorId", "deviceId"]
}

export const GET_GOOGLE_MAP_KEY = {
	action : 'app/LocationTracker/ GET_GOOGLE_MAP_KEY',
    success: "app/LocationTracker/ GET_GOOGLE_MAP_KEY_SUCCESS",
    failure: "app/LocationTracker/ GET_GOOGLE_MAP_KEY_FAILURE",
    urlKey: "getGoogleMapKey",
    successKey: "getGoogleMapKeySuccess",
    failureKey: "getGoogleMapKeyFailure",
    actionName: "getGoogleMapKey",
}


