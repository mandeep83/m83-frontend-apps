/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * LocationTracker
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import { convertTimestampToDate, getTimeDifference, uIdGenerator, isNavAssigned, getBucketSizeOptions, DURATION_OPTIONS } from "../../commonUtils";
import Loader from "../../components/Loader/Loadable";
import { closeMqttConnection, subscribeTopic } from "../../mqttConnection";
import reducer from "./reducer";
import saga from "./saga";
import { allActions as ACTIONS } from './actions'
import { allSelectors as SELECTORS } from "./selectors";
import { Helmet } from "react-helmet";
import Script from 'react-load-script';
import axios from 'axios';
import { GetHeaders } from '../../commonUtils';


/* eslint-disable react/prefer-stateless-function */

class LocationTracker extends React.PureComponent {

    state = {
        deviceTypeId: this.props.match.params.deviceTypeId,
        deviceId: this.props.match.params.deviceId,
        coordinates: [],
        selectedTopic: null,
        deviceInfo: {},
        credentials: null,
        showDeviceDetails: false,
        deviceDetailsLoader: true,
    }

    componentDidMount() {
            this.props.getGoogleMapKey();
    }

    componentWillUnmount() {
        closeMqttConnection();
    }

    handleNewCoordinates() {
        let isMapRenderingNeeded = this.state.coordinates.length === 1;
        if (isMapRenderingNeeded) {
            this.setState({isLoadingMap: true})
        }
        else if(this.map){
            if (window.google) {
                let lastCoordinates = this.state.coordinates[this.state.coordinates.length - 1];
                let newCoordinates = new google.maps.LatLng(lastCoordinates.lat, lastCoordinates.lng);
                this.map.panTo(newCoordinates)
                this.deviceMarker.setPosition(newCoordinates);
                var path = this.polyline.getPath();
                // add new point
                path.push(newCoordinates);
                // update the polyline with the updated path
                this.polyline.setPath(path);
            }
        }
    }

    showLocationData = () => {
        subscribeTopic(this.state.selectedTopic, (message, topic) => {
            try {
                let coordinates = JSON.parse(JSON.stringify(this.state.coordinates));
                coordinates.push({ lat: message.geoLocation.lat, lng: message.geoLocation.long });
                this.setState({
                    coordinates
                }, this.handleNewCoordinates)
            } catch (e) {
                this.setState({
                    modalType: "error",
                    isOpen: true,
                    message2: 'The debug data is not a valid JSON data format.',
                })
            }
        }, null, this.state.credentials)
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.mqttCredentialsSuccess && nextProps.mqttCredentialsSuccess !== this.props.mqttCredentialsSuccess) {
            let password = window.atob(nextProps.mqttCredentialsSuccess.response.password);
            password = password.substring(8, password.length - 6);
            let credentials = {
                userName: this.state.deviceTypeId,
                password: password,
                clientId: `${this.state.deviceId}-read`,
            },
                selectedTopic = `${nextProps.mqttCredentialsSuccess.response.topicPrefix}/${this.state.deviceId}/report`;
            this.setState({
                selectedTopic,
                credentials
            }, this.showLocationData)
        }
        if (nextProps.mqttCredentialsFailure && nextProps.mqttCredentialsFailure !== this.props.mqttCredentialsFailure) {
            this.props.showNotification("error", nextProps.mqttCredentialsFailure.error);
        }
        if (nextProps.getDeviceTypeByIdSuccess && nextProps.getDeviceTypeByIdSuccess !== this.props.getDeviceTypeByIdSuccess) {
            this.props.getMQTTCredentials(this.state.deviceTypeId);
        }
        if (nextProps.getDeviceTypeByIdFailure && nextProps.getDeviceTypeByIdFailure !== this.props.getDeviceTypeByIdFailure) {
            this.props.showNotification("error", nextProps.getDeviceTypeByIdFailure.error);
            this.props.getMQTTCredentials(this.state.deviceTypeId);
        }

        if (nextProps.getDeviceInfoSuccess && nextProps.getDeviceInfoSuccess !== this.props.getDeviceInfoSuccess) {
            this.setState({
                deviceDetailsLoader: false,
                deviceInfo: nextProps.getDeviceInfoSuccess.response
            })
        }
        if (nextProps.getDeviceInfoFailure && nextProps.getDeviceInfoFailure !== this.props.getDeviceInfoFailure) {
            this.props.showNotification("error", nextProps.getDeviceInfoFailure.error);
        }

        if (nextProps.getGoogleMapKeySuccess && nextProps.getGoogleMapKeySuccess !== this.props.getGoogleMapKeySuccess) {
            this.setState({ mapKey: nextProps.getGoogleMapKeySuccess.response.value },() => {
                this.props.getDeviceTypeById(this.props.match.params.deviceTypeId)
                this.props.getDeviceInfo(this.props.match.params.deviceTypeId, this.props.match.params.deviceId);
            })
        }

        if (nextProps.getGoogleMapKeyFailure && nextProps.getGoogleMapKeyFailure !== this.props.getGoogleMapKeyFailure) {
            this.props.showNotification("error",nextProps.getGoogleMapKeyFailure.error)
        }
    }

    renderMap() {
        let mapDiv = this.mapRef.current;
        this.map = new google.maps.Map(mapDiv, {
            center: this.state.coordinates[0],
            zoom: 6,
            minZoom: 1
        });

        let initialPointMarker = new google.maps.Marker({
            position: this.state.coordinates[this.state.coordinates.length - 1],
            map: this.map,
            icon: {
                url: require("../../assets/images/other/circleWithCenter.png"),
                size: new google.maps.Size(25, 25),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(14, 15),
            }
        });

        let markerImageUrl = this.props.getDeviceTypeByIdSuccess && this.props.getDeviceTypeByIdSuccess.response.image;
        this.deviceMarker = new google.maps.Marker({
            position: this.state.coordinates[this.state.coordinates.length - 1],
            map: this.map,
            icon: markerImageUrl ? {
                url: markerImageUrl,
                size: new google.maps.Size(32, 40),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(15, 15),
            } : null
        });

        this.polyline = new google.maps.Polyline({
            path: [this.state.coordinates[this.state.coordinates.length - 1]],
            geodesic: true,
            strokeColor: "#0591F5",
            strokeOpacity: 1.0,
            strokeWeight: 3,
            map: this.map
        });
    }
    mapRef = React.createRef();

    loadMap = () => {
        this.setState({isLoadingMap: false},this.renderMap)
    }
    getMiniLoader = () => {
        return <div className="card-loader">
            <i className="fad fa-sync-alt fa-spin"></i>
        </div>
    }

    copyToClipboard = (id) => {
        navigator.clipboard.writeText(id)
        this.setState({
            type: "success",
            isOpen: true,
            message2: 'Copied to Clipboard successfully !',
        })
    }

    render() {
        let { deviceInfo } = this.state;
        return (
            <React.Fragment>
                <Helmet>
                    <title>Live Tracking</title>
                    <meta name="description" content="Live Tracking" />
                </Helmet>
                <header className="content-header d-flex">
                    <div className="flex-60">
                        <h6>Live Tracking</h6>
                    </div>
                </header>
                <div className='content-body'>
                    {
                        this.state.isLoadingMap && this.state.mapKey &&
                        <Script url= {`https://maps.googleapis.com/maps/api/js?key=${ this.state.mapKey }&libraries=places`}
                            onLoad = {this.loadMap}
                        />
                    }
                    {this.state.coordinates.length > 0 && (this.state.showDeviceDetails ?
                        <button className="btn-transparent btn-transparent-close collapse-button"
                            onClick={() => this.setState({ showDeviceDetails: false })}
                        >
                            <i class="far fa-times"></i>
                        </button>
                        :
                        <button className="btn-transparent btn-success  collapse-button"
                            onClick={() => this.setState({ showDeviceDetails: true })}
                        >
                            <i class="fas fa-info"></i>
                        </button>)
                    }
                    {this.state.showDeviceDetails ?
                        <div className="device-details-box">
                            <div className="card">
                                {this.state.deviceDetailsLoader ?
                                    this.getMiniLoader()
                                    :
                                    <div className="card-body">
                                        <div className="device-info-box">
                                            <div className="device-info-status">
                                                {deviceInfo.status == "not-connected" ?
                                                    <h6 className="mr-r-22"><i className="far fa-unlink mr-2"></i>Not connected</h6> :
                                                    <h6>Last Reported :
                                                <span className={`ml-2 ${deviceInfo.status == "online" ? "text-green" : "text-red"}`}>{getTimeDifference(deviceInfo.lastReported)}</span>
                                                    </h6>
                                                }
                                            </div>
                                            <div className="device-detail-image">
                                                <img src={deviceInfo.image ? deviceInfo.image : "https://content.iot83.com/m83/misc/device-type-fallback.png"} />
                                                <span className={`device-detail-status ${deviceInfo.status == "online" ? "bg-green" : deviceInfo.status == "offline" ? "bg-red" : "bg-gray"}`}></span>
                                            </div>
                                            {deviceInfo.deviceName ?
                                                <h5>{deviceInfo.deviceName}
                                                    <button className="btn-transparent btn-transparent-green copy-button pt-1" data-tooltip data-tooltip-text="Copy" data-tooltip-place="bottom" onClick={() => this.copyToClipboard(`${deviceInfo.id}`)}>
                                                        <i className="far fa-copy f-14"></i>
                                                    </button>
                                            ({deviceInfo.id})
                                        </h5> :
                                                <h5>
                                                    <button className="btn-transparent btn-transparent-green copy-button" data-tooltip data-tooltip-text="Copy" data-tooltip-place="bottom" onClick={() => this.copyToClipboard(`${deviceInfo.id}`)}>
                                                        <i className="far fa-copy"></i>
                                                    </button>
                                                    {deviceInfo.id}
                                                </h5>
                                            }
                                            <p><strong><i className="fad fa-map-marker-alt"></i>Location : </strong><a href="#" onClick={() => { deviceInfo.deviceLocation && window.open(`https://www.google.com/maps/place/${deviceInfo.lat},${deviceInfo.lng}`) }}>{deviceInfo.deviceLocation || "-"}</a></p>
                                            <p><strong><i className="fad fa-desktop"></i>Device Type : </strong><span onClick={() => isNavAssigned("deviceTypes") && this.props.history.push(`/addOrEditDeviceType/${this.props.match.params.deviceTypeId}`)}>{deviceInfo.deviceTypeName}</span></p>
                                            <p><strong><i className="fad fa-layer-group"></i>Group : </strong><span onClick={() => isNavAssigned('deviceGroups') && this.props.history.push({ pathname: `/deviceGroups/${this.props.match.params.deviceTypeId}/${deviceInfo.deviceGroupId}`, state: { childViewType: 'GROUP_DETAILS' } })}>{deviceInfo.deviceGroupName}</span></p>
                                            <p><strong><i className="fad fa-tags"></i>Tag : </strong><span style={{ color: deviceInfo.tagColor }} onClick={() => isNavAssigned('deviceGroups') && this.props.history.push({ pathname: `/deviceGroups/${this.props.match.params.deviceTypeId}/${deviceInfo.deviceGroupId}`, state: { childViewType: 'GROUP_TAGVIEW' } })}>{deviceInfo.tag || "-"}</span></p>
                                            <p><strong><i className="fad fa-hourglass-half"></i>Reporting interval : </strong>{deviceInfo.reportInterval} secs</p>
                                            <p><strong><i className="fad fa-clock"></i>Last Reported : </strong>{convertTimestampToDate(deviceInfo.lastReported) || "-"}</p>
                                        </div>
                                    </div>
                                }
                            </div>

                        </div>
                        :
                        <div className="device-details-box collapsed"></div>
                    }
                    {this.state.coordinates.length > 0 ?
                        <div id="map" style={{ height: "100%" }} ref={this.mapRef} /> :
                        <div className="diagnose-box">
                            <div className="inner-loader-wrapper h-100">
                                <div className="inner-loader-content">
                                    <h6>Tracking Device...</h6>
                                    <img src="https://content.iot83.com/m83/misc/uploading.gif" />
                                    <p className="text-gray">Please be patient. This may take a few seconds.</p>
                                </div>
                            </div>
                        </div>
                    }

                </div>
                {this.state.isOpen &&
                    <NotificationModal
                        type={this.state.type}
                        message2={this.state.message2}
                        onCloseHandler={this.onCloseHandler}
                    />
                }
            </React.Fragment>

        );
    }
}
LocationTracker.propTypes = {
    dispatch: PropTypes.func.isRequired
};

let allSelectors = {}
Object.entries(SELECTORS).map(([key, value]) => {
    allSelectors[key] = value()
})

const mapStateToProps = createStructuredSelector(allSelectors);


function mapDispatchToProps(dispatch) {
    let allActions = { dispatch }
    Object.entries(ACTIONS).map(([key, value]) => {
        allActions[key] = (...args) => dispatch(value(...args))
    })
    return allActions;
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "locationTracker", reducer });
const withSaga = injectSaga({ key: "locationTracker", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(LocationTracker);
