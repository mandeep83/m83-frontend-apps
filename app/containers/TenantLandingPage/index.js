/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { FormattedMessage } from "react-intl";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import { getTenantDetails, getTenantDetailsFailure, fetchTenantDetails, fetchTenantDetailsError, getTenantSummarySuccess, getTenantSummaryError, lineChartData, lineChartDataError, barChartData, barChartDataError, fetchLicenseCount, fetchLicenseCountError } from "./selectors";
import { getUserDetails, fetchAllTenantDetails, fetchTenantSummary, getActivityLineChartData, getActivityBarChartData, getTotalLicenseCount } from './actions';
import reducer from "./reducer";
import saga from "./saga";
import ReactTooltip from "react-tooltip";
import { buildStyles, CircularProgressbar } from "react-circular-progressbar";
import Loader from '../../components/Loader/Loadable';
import NotificationModal from '../../components/NotificationModal/Loadable'
import Chart from "../../components/LineChart";
import AllChartsComponent from "../../components/AllChartsComponent";




/* eslint-disable react/prefer-stateless-function */
export class TenantLandingPage extends React.Component {
    state = {
        userDetails: [],
        fetchUserDetails: {},
        fetchTenantStats: [],
        durationInDays: 7,
        lineChartData: [],
        barChartData: [],
        isFetchingLineChartData: true,
        isFetchingBarChartData: true,
        isFetching: true,
        isFetchingMessageModal: true,
        durationActivityLineChartData: 7,
        durationActivityBarChartData: 7,
        clusterInfo: []
    }

    componentWillMount() {
        this.props.getUserDetails();
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.getTenantDetails && nextProps.getTenantDetails !== this.props.getTenantDetails) {
            this.setState({
                userDetails: nextProps.getTenantDetails
            })
            this.props.fetchAllTenantDetails(nextProps.getTenantDetails.tenantId);
            this.props.fetchTenantSummary(nextProps.getTenantDetails.tenantId);
            this.props.getActivityLineChartData(nextProps.getTenantDetails.tenantId, this.state.durationInDays);
            this.props.getActivityBarChartData(nextProps.getTenantDetails.tenantId, this.state.durationInDays);
        }

        if (nextProps.getTenantDetailsFailure && nextProps.getTenantDetailsFailure !== this.props.getTenantDetailsFailure) {
            this.setState({
                type: 'error',
                isOpen: true,
                message2: nextProps.getTenantDetailsFailure,
                isFetchingMessageModal: false
            })
        }

        if (nextProps.fetchTenantDetails && nextProps.fetchTenantDetails !== this.props.fetchTenantDetails) {
            this.setState({ fetchUserDetails: nextProps.fetchTenantDetails })
        }

        if (nextProps.fetchTenantDetailsError && nextProps.fetchTenantDetailsError !== this.props.fetchTenantDetailsError) {
            this.setState({
                type: 'error',
                isOpen: true,
                message2: nextProps.fetchTenantDetailsError,
                isFetchingMessageModal: false
            })
        }


        if (nextProps.tenantSummary && nextProps.tenantSummary !== this.props.tenantSummary) {
            let clusterInfo = [
                {
                    name: "CPU Usage",
                    count: nextProps.tenantSummary.clusterInfo.cpuUsgae,
                    pathColor: '#ffffff',
                },
                {
                    name: "Memory Usage",
                    count: nextProps.tenantSummary.clusterInfo.memoryUsage,
                    pathColor: '#ffffff',
                },
                {
                    name: "Pods Usage",
                    count: nextProps.tenantSummary.clusterInfo.noOfPods,
                    pathColor: '#ffffff',
                },
            ]

            this.setState({
                fetchTenantStats: nextProps.tenantSummary.features,
                clusterInfo,
                isFetching: false
            })
        }

        if (nextProps.tenantSummaryError && nextProps.tenantSummaryError !== this.props.tenantSummaryError) {
            this.setState({
                type: 'error',
                isOpen: true,
                message2: nextProps.tenantSummaryError,
                isFetchingMessageModal: false
            })
        }

        if (nextProps.lineChartData && nextProps.lineChartData !== this.props.lineChartData) {
            this.setState({
                lineChartData: nextProps.lineChartData,
                isFetchingLineChartData: false,
            })
        }

        if (nextProps.lineChartDataError && nextProps.lineChartDataError !== this.props.lineChartDataError) {
            this.setState({
                type: 'error',
                isOpen: true,
                message2: nextProps.lineChartDataError,
                isFetchingMessageModal: false
            })
        }

        if (nextProps.barChartData && nextProps.barChartData !== this.props.barChartData) {
            this.setState({
                barChartData: nextProps.barChartData,
                isFetchingBarChartData: false,
            })
        }

        if (nextProps.barChartDataError && nextProps.barChartDataError !== this.props.barChartDataError) {
            this.setState({
                type: 'error',
                isOpen: true,
                message2: nextProps.barChartDataError,
                isFetchingMessageModal: false
            })
        }
    }

    onCloseHandler = () => {
        this.setState({
            message2: '',
            isOpen: false,
            type: ''
        })
    }

    filterHandler = (event) => {
        const self = this;
        if (event.target.id === 'activities') {
            self.setState({
                isFetchingLineChartData: true,
                durationActivityLineChartData: event.target.value
            }, () => { this.props.getActivityLineChartData(self.state.userDetails.tenantId, self.state.durationActivityLineChartData) })
        } else {
            self.setState({
                isFetchingBarChartData: true,
                durationActivityBarChartData: event.target.value
            }, () => { this.props.getActivityBarChartData(self.state.userDetails.tenantId, self.state.durationActivityBarChartData) });
        }
    }


    createChart = (data, id, chartType, chartSubType) => {
        let dataForChart = {
            availableAttributes: [],
            chartData: chartType === "barChartForMl" ? data : data.map(chartDataObj => {
                chartDataObj.category = new Date(chartDataObj.timestamp).toLocaleDateString()
                return chartDataObj;
            })
        }
        Object.keys(data[0]).map((key, index) => {
            if (key !== 'category' && key !== 'timestamp') {
                dataForChart.availableAttributes.push(key)
            }
        })
        let chartData = {
            theme: "kelly's",
            widgetType: chartType,
            widgetName: chartSubType,
            i: id,
            widgetData: dataForChart,
            widgetDetails: {
                "customValues": chartType === "lineChartForMl" ?
                    {
                        "lineThickness": { "value": 2, "type": "number" },
                        "legendPlacement": {
                            "value": "bottom", "type": "select",
                            "options": ["bottom", "top", "left", "right"]
                        },
                        "bulletVisibility": { "value": true, "type": "boolean" }
                        , "bulletSize": { "value": 5, "type": "number", "parent": "bulletVisibility" },
                        "customColors": { "value": false, "type": "boolean" },
                        "aqi": { "value": "#FF0000", "type": "color", "parent": "customColors" }
                    } : {
                        "barWidth": { "value": 80, "type": "number" },
                        "legendPlacement": { "value": "bottom", "type": "select", "options": ["bottom", "top", "left", "right"] },
                        "chartAngle": { "value": 30, "type": "number" }, "chartDepth": { "value": 30, "type": "number" },
                        "customColors": { "value": false, "type": "boolean" }, "year2005": { "value": "#0ba6ef", "type": "color", "parent": "customColors" },
                        "columnTopRadius": { "value": 10 }
                    }
            }
        };
        return chartData;
    }

    createChart = (data, id, chartType, chartSubType) => {
        let dataForChart = {
            availableAttributes: [],
            chartData: chartType == "lineChart" ? data.map(chartDataObj => {
                chartDataObj.category = new Date(chartDataObj.timestamp).toLocaleDateString()
                return chartDataObj;
            }) : data
        }

        Object.keys(data[0]).map((key, index) => {
            if (key !== 'category' && key !== 'timestamp') {
                dataForChart.availableAttributes.push(key)
            }
        })
        let config = {
            chartType: chartType,
            chartSubType: chartSubType,
            id: id,
            chartData: dataForChart
        };
        return config;
    }

    render() {
        return (
            <div>
                <Helmet>
                    <title>Admin - Home</title>
                    <meta name="description" content="M83-TenantLandingPage" />
                </Helmet>

                <div className="pageBreadcrumb">
                    <div className="row">
                        <div className="col-8">
                            <p><span>Home</span></p>
                        </div>
                    </div>
                </div>

                {this.state.isFetching ? <Loader /> :
                    <div className="outerBox position-relative">
                        <div className="flex">
                            <div className="flex-item fx-b25">
                                <div className="card panel">
                                    <div className="card-body panelBody">
                                        <div className="tenantDetailImage">
                                            <img src={this.state.fetchUserDetails.imageUrl ? this.state.fetchUserDetails.imageUrl : "https://content.iot83.com/m83/other/noImageFound.png"} />
                                            <button type="button" className="btn btn-transparent btn-transparent-danger"><i className="fas fa-lock"></i></button>
                                        </div>
                                        <h5 className="text-center mr-b-2">{this.state.fetchUserDetails.companyName}</h5>
                                        <p className="text-center mr-b-2"><strong className={this.state.fetchUserDetails.licenseType == 'TRIAL' ? 'text-primary' : this.state.fetchUserDetails.licenseType == 'POC' ? 'text-orange' : 'text-lightGreen'}>{this.state.fetchUserDetails.licenseType}</strong></p>

                                        {this.state.fetchUserDetails.licenseType == 'TRIAL' ?
                                            <div className="packageStar">
                                                <i className="fas fa-star"></i>
                                            </div> :
                                            this.state.fetchUserDetails.licenseType == 'POC' ?
                                                <div className="packageStar">
                                                    <i className="fas fa-star mr-r-4"></i>
                                                    <i className="fas fa-star"></i>
                                                </div> :
                                                <div className="packageStar">
                                                    <i className="fas fa-star mr-r-4"></i>
                                                    <i className="fas fa-star mr-r-4"></i>
                                                    <i className="fas fa-star"></i>
                                                </div>
                                        }
                                        <div className="headerLine"></div>
                                        <div className="tenantDetailContent">
                                            <p className="position-relative"><i className="fad fa-phone-alt"></i>Phone No :<span>{this.state.fetchUserDetails.mobile}</span></p>
                                            <p className="position-relative"><i className="fad fa-envelope"></i> Email : <span>{this.state.fetchUserDetails.email}</span></p>
                                            <p className="position-relative"><i className="fad fa-bookmark"></i>Description :<span>{this.state.fetchUserDetails.description ? this.state.fetchUserDetails.description : "N/A"}</span></p>
                                        </div>
                                    </div>
                                    <div className="card-footer flex">
                                        <div className="footerIcon flex-item  fx-b60">
                                            <i className="far fa-calendar-alt"></i>
                                        </div>
                                        <div className="footerContent flex-item fx-b40">
                                            <p>Renewal Date</p>
                                            <h5>{new Date(this.state.fetchUserDetails.licenseExpire).toLocaleDateString()}</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="flex-item fx-b75 pd-l-7">
                                <div className="flex tenantDetailUsage">
                                    {this.state.clusterInfo.map((info, index) =>
                                        <div key={index} className="flex-item fx-b33 usageList">
                                            <div className={index === 0 ? "card panel usageListContent flex position-relative backgroundFirstUsageList" : index === 1 ? "card panel usageListContent  flex position-relative backgroundSecondUsageList" : "card panel usageListContent  flex position-relative backgroundLastUsageList"}>
                                                <div className="flex-item fx-b60 ">
                                                    <CircularProgressbar
                                                        value={info.count}
                                                        circleRatio={0.75}
                                                        styles={buildStyles({
                                                            rotation: 1 / 2 + 1 / 8,
                                                            pathColor: `${info.pathColor}`,
                                                            trailColor: "rgba(255,255,255,0.4)"
                                                        })}
                                                        className="circleProgressBar"
                                                    />
                                                    <h1>{info.count}<span>%</span></h1>
                                                </div>
                                                <h5 className="flex-item">{info.name}</h5>
                                            </div>
                                        </div>
                                    )}
                                </div>

                                <div className="card panel progressBarBox mr-l-5 mr-r-5 pd-16">
                                    <ul className="listStyleNone progressBarList">
                                        <li>
                                            <div className="flex">
                                                <span className="flex-item fx-b15 ">Connectors</span>
                                                <span className="flex-item fx-b65">
                                                    <div className="progress">
                                                        <div className="progress-bar" role="progressbar">
                                                        </div>
                                                    </div>
                                                </span>
                                                <span className="flex-item fx-b5 mr-l-15">10%</span>
                                                <span className="flex-item fx-b12_5 text-right"><strong>9</strong> Out of <strong>90</strong></span>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="flex">
                                                <span className="flex-item fx-b15 ">Transformation</span>
                                                <span className="flex-item fx-b65">
                                                    <div className="progress">
                                                        <div className="progress-bar" role="progressbar">
                                                        </div>
                                                    </div>
                                                </span>
                                                <span className="flex-item fx-b5 mr-l-15">80%</span>
                                                <span className="flex-item fx-b12_5 text-right"><strong>72</strong> Out of <strong>90</strong></span>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="flex">
                                                <span className="flex-item fx-b15 ">APIS</span>
                                                <span className="flex-item fx-b65">
                                                    <div className="progress">
                                                        <div className="progress-bar" role="progressbar">
                                                        </div>
                                                    </div>
                                                </span>
                                                <span className="flex-item fx-b5 mr-l-15">100%</span>
                                                <span className="flex-item fx-b12_5 text-right"><strong>72</strong> Out of <strong>180</strong></span>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="flex">
                                                <span className="flex-item fx-b15 ">FaaS</span>
                                                <span className="flex-item fx-b65">
                                                    <div className="progress">
                                                        <div className="progress-bar" role="progressbar">
                                                        </div>
                                                    </div>
                                                </span>
                                                <span className="flex-item fx-b5 mr-l-15">10%</span>
                                                <span className="flex-item fx-b12_5 text-right"><strong>72</strong> Out of <strong>180</strong></span>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="flex">
                                                <span className="flex-item fx-b15 ">Application</span>
                                                <span className="flex-item fx-b65">
                                                    <div className="progress">
                                                        <div className="progress-bar" role="progressbar">
                                                        </div>
                                                    </div>
                                                </span>
                                                <span className="flex-item fx-b5 mr-l-15">10%</span>
                                                <span className="flex-item fx-b12_5 text-right"><strong>9</strong> Out of <strong>90</strong></span>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="flex">
                                                <span className="flex-item fx-b15 ">Pages</span>
                                                <span className="flex-item fx-b65">
                                                    <div className="progress">
                                                        <div className="progress-bar" role="progressbar">
                                                        </div>
                                                    </div>
                                                </span>
                                                <span className="flex-item fx-b5 mr-l-15">10%</span>
                                                <span className="flex-item fx-b12_5 text-right"><strong>9</strong> Out of <strong>90</strong></span>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="flex">
                                                <span className="flex-item fx-b15 ">JS Function</span>
                                                <span className="flex-item fx-b65">
                                                    <div className="progress">
                                                        <div className="progress-bar" role="progressbar">
                                                        </div>
                                                    </div>
                                                </span>
                                                <span className="flex-item fx-b5 mr-l-15">10%</span>
                                                <span className="flex-item fx-b12_5 text-right"><strong>9</strong> Out of <strong>90</strong></span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div className="flex">
                            <div className="flex-item fx-b50 pd-r-7">
                                <div className="card panel">
                                    <div className="card-header panelHeader"><h5>Activities</h5>
                                        <select id="activities" value={this.state.durationActivityLineChartData} className="form-control" onChange={this.filterHandler}>
                                            <option value={1}>Last 1 Day</option>
                                            <option value={2}>Last 2 Days</option>
                                            <option value={3}>Last 3 Days</option>
                                            <option value={5}>Last 5 Days</option>
                                            <option value={7}>Last 7 Days</option>
                                        </select>
                                    </div>
                                    <div className="card-body panelBody" style={{ height: "400px" }}>
                                        {
                                            this.state.isFetchingLineChartData ?
                                                <div className="panelLoader">
                                                    <i className="fal fa-sync-alt fa-spin"></i>
                                                </div> :
                                                this.state.lineChartData.length > 0 ?
                                                <AllChartsComponent config={this.createChart(this.state.lineChartData, "lineChartDiv", "lineChart", "simpleLineChart")} /> :
                                                <div className="panelMessage">
                                                        <p><i className="far fa-exclamation-triangle"></i>No Activity yet ! </p>
                                                    </div>
                                        }
                                    </div>
                                </div>
                            </div>
                            <div className="flex-item fx-b50 pd-l-7">
                                <div className="card panel">
                                    <div className="card-header panelHeader"><h5>Top Tenant Activities</h5>
                                        <select id="topTenantActivities" value={this.state.durationActivityBarChartData} className="form-control" onChange={this.filterHandler}>
                                            <option value={1}>Last 1 Day</option>
                                            <option value={2}>Last 2 Days</option>
                                            <option value={3}>Last 3 Days</option>
                                            <option value={5}>Last 5 Days</option>
                                            <option value={7}>Last 7 Days</option>
                                        </select>
                                    </div>
                                    <div className="card-body panelBody">
                                        {
                                            this.state.isFetchingBarChartData ?
                                                <div className="panelLoader">
                                                    <i className="fal fa-sync-alt fa-spin"></i>
                                                </div> :
                                                this.state.barChartData.length > 0 ?
                                                <AllChartsComponent config={this.createChart(this.state.barChartData, "barChartDiv", "barChart", "simpleBarChart")} /> :
                                                <div className="panelMessage">
                                                        <p><i className="far fa-exclamation-triangle"></i>No Activity yet ! </p>
                                                    </div>
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                        {/* <div className="card panel mr-l-5 mr-r-5">
                                    <div className="flex usageList tenantDetailUsage">
                                        {this.state.clusterInfo.map((info, index) =>
                                            <div key={index} className="flex-item fx-b33">
                                                <div className="usageListContent  flex">
                                                    <div className="flex-item fx-b60 position-relative">
                                                        <CircularProgressbar
                                                            value={info.count}
                                                            circleRatio={0.75}
                                                            styles={buildStyles({
                                                                rotation: 1 / 2 + 1 / 8,
                                                                pathColor: `${info.pathColor}`,
                                                                trailColor: "#eee"
                                                            })}
                                                            className="circleProgressBar"
                                                        />
                                                        <h1>{info.count}<span>%</span></h1>
                                                    </div>
                                                    <div className="flex-item fx-b40"><h5>{info.name}</h5></div>
                                                </div>
                                            </div>
                                        )}
                                    </div>
                                </div> */}

                        {/* <ul className="counterList">
                                    {this.state.fetchTenantStats.map((data, index) => {
                                        return (
                                            <li key={index}>
                                                <div className="counterCard landingCard">
                                                    <div className="counterContent">
                                                        <p>{data.displayName}</p>
                                                        <h4>{data.count}<span>Out of</span>{data.totalCount}</h4>
                                                    </div>
                                                    <div className="counterProgress">
                                                        <CircularProgressbar
                                                            value={parseInt((data.count / data.totalCount) * 100)}
                                                            text={`${parseInt((data.count / data.totalCount) * 100)}%`}
                                                            styles={buildStyles({
                                                                trailColor: "#eee",
                                                                pathColor: "#0099ff"
                                                            })}
                                                            className="circleProgressBar"
                                                        />
                                                    </div>
                                                </div>
                                            </li>
                                        )
                                    })
                                    }
                                    <li>
                                        <div className="counterCard calenderIconCard">
                                            <div className="counterContent">
                                                <p>Renewal Date</p>
                                                <h5>{new Date(this.state.fetchUserDetails.licenseExpire).toLocaleDateString()}</h5>
                                            </div>
                                            <div className="counterIcon">
                                                <i className="far fa-calendar-alt"></i>
                                            </div>
                                        </div>
                                    </li>
                                </ul> */}

                        {/* <div className="notificationBox">
                            <div className="triangleUp"></div>
                            <div className="notificationHeader flex">
                                <p className="flex-item text-primary fx-b90 mr-0">Clear All</p>
                                <p className="text-right fx-b10 mr-0"><i className="fa fa-cog"></i></p>
                            </div>
                            <div className="notificationBody">
                                <ul className="listStyleNone notificationList">
                                    <li>
                                        <div className="flex pd-l-20">
                                            <div className="flex-item fx-b85 pd-5">
                                                <p><span className="text-dark">Lorem Ipsum is simply dummy</span> text of the printing and typesetting industry.</p>
                                                <p className="mr-t-5 mr-b-0">0 minutes ago</p>
                                            </div>
                                            <div className="flex-item fx-b15 deleteBtn">
                                                <button className="btn btn-transparent "><i className="fas fa-trash-alt"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex pd-l-20">
                                            <div className="flex-item fx-b85 pd-5">
                                                <p><span className="text-dark">Lorem Ipsum is simply dummy</span> text of the printing and typesetting industry.</p>
                                                <p className="mr-t-5 mr-b-0">0 minutes ago</p>
                                            </div>
                                            <div className="flex-item fx-b15 deleteBtn">
                                                <button className="btn btn-transparent "><i className="fas fa-trash-alt"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="unreadNotification flex pd-l-20">
                                            <div className="flex-item fx-b85 pd-5">
                                                <p><span className="text-dark">Lorem Ipsum is simply dummy</span> text of the printing and typesetting industry.</p>
                                                <p className="mr-t-5 mr-b-0">0 minutes ago</p>
                                            </div>
                                            <div className="flex-item fx-b15 deleteBtn">
                                                <button className="btn btn-transparent "><i className="fas fa-trash-alt"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="unreadNotification flex pd-l-20">
                                            <div className="flex-item fx-b85 pd-5">
                                                <p><span className="text-dark">Lorem Ipsum is simply dummy</span> text of the printing and typesetting industry.</p>
                                                <p className="mr-t-5 mr-b-0">0 minutes ago</p>
                                            </div>
                                            <div className="flex-item fx-b15 deleteBtn">
                                                <button className="btn btn-transparent "><i className="fas fa-trash-alt"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex pd-l-20">
                                            <div className="flex-item fx-b85 pd-5">
                                                <p><span className="text-dark">Lorem Ipsum is simply dummy</span> text of the printing and typesetting industry.</p>
                                                <p className="mr-t-5 mr-b-0">0 minutes ago</p>
                                            </div>
                                            <div className="flex-item fx-b15 deleteBtn">
                                                <button className="btn btn-transparent "><i className="fas fa-trash-alt"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div className="notificationFooter flex">
                                <p className="flex-item text-primary fx-b70 mr-0">Mark as all read</p>
                                <p className="text-right fx-b30 mr-0"> See all<span className="mr-l-10"><i className="fas fa-caret-right"></i></span></p>
                            </div>
                        </div>
                     */}

                    </div>

                }


                {this.state.isOpen &&
                    <NotificationModal
                        type={this.state.type}
                        message2={this.state.message2}
                        onCloseHandler={this.onCloseHandler}
                    />
                }
            </div>
        );
    }
}

TenantLandingPage.propTypes = {
    dispatch: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
    getTenantDetails: getTenantDetails(),
    getTenantDetailsFailure: getTenantDetailsFailure(),
    fetchTenantDetails: fetchTenantDetails(),
    fetchTenantDetailsError: fetchTenantDetailsError(),
    tenantSummary: getTenantSummarySuccess(),
    tenantSummaryError: getTenantSummaryError(),
    lineChartData: lineChartData(),
    lineChartDataError: lineChartDataError(),
    barChartData: barChartData(),
    barChartDataError: barChartDataError(),
    fetchLicenseCount: fetchLicenseCount(),
    fetchLicenseCountError: fetchLicenseCountError()

});

function mapDispatchToProps(dispatch) {
    return {
        dispatch,
        getUserDetails: () => dispatch(getUserDetails()),
        fetchAllTenantDetails: (id) => dispatch(fetchAllTenantDetails(id)),
        fetchTenantSummary: (id) => dispatch(fetchTenantSummary(id)),
        getTotalLicenseCount: (id) => dispatch(getTotalLicenseCount(id)),
        getActivityLineChartData: (id, durationInDays) => dispatch(getActivityLineChartData(id, durationInDays)),
        getActivityBarChartData: (id, durationInDays) => dispatch(getActivityBarChartData(id, durationInDays))
    };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "tenantLandingPage", reducer });
const withSaga = injectSaga({ key: "tenantLandingPage", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(TenantLandingPage);
