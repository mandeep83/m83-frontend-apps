/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * AddOrEditClient
 *
 */

import React from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {Helmet} from "react-helmet";
import {FormattedMessage} from "react-intl";
import {createStructuredSelector} from "reselect";
import {compose} from "redux";

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import makeSelectAddOrEditClient from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import messages from "./messages";

/* eslint-disable react/prefer-stateless-function */
export class AddOrEditClient extends React.Component {
    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>AddOrEditClient</title>
                    <meta name="description" content="Description of AddOrEditClient"/>
                </Helmet>
                <header className="content-header d-flex">
                    <div className="flex-60">
                        <h6 className="previous" onClick={() => this.props.history.push('/clientList')}>Client</h6>
                        <h6 className="active">Add New</h6>
                    </div>
                    <div className="flex-40 text-right">
                        <div className="content-header-group">
                            <button className="btn btn-light" onClick={() => this.props.history.push('/clientList')}><i className="fas fa-angle-left"></i></button>
                        </div>
                    </div>
                </header>

                <div className="content-body">
                    <div className="form-content-wrapper form-content-wrapper-left">
                        <div className="form-content-box">
                            <div className="form-content-header">
                                <p>Add User(s)</p>
                            </div>
                            <div className="form-content-body">
                                <div className="form-group add-tenant-box">
                                    <button className="btn-transparent btn-transparent-red"><i className="far fa-trash-alt"></i></button>
                                    <div className="d-flex">
                                        <div className="flex-50 pd-r-10">
                                            <div className="form-group">
                                                <label className="form-group-label">First Name :</label>
                                                <input type="text" id="firstName" className="form-control"/>
                                            </div>
                                        </div>
                                        <div className="flex-50 pd-l-10">
                                            <div className="form-group">
                                                <label className="form-group-label">Last Name :</label>
                                                <input type="text" id="lastName" className="form-control" required/>
                                            </div>
                                        </div>
                                        <div className="flex-50 pd-r-10">
                                            <div className="form-group">
                                                <label className="form-group-label">Email :</label>
                                                <input type="email" id="email" className="form-control" required/>
                                            </div>
                                        </div>
                                        <div className="flex-50 pd-l-10">
                                            <div className="form-group">
                                                <label className="form-group-label">Phone Number :</label>
                                                <input type="text" maxLength="10" id="mobile" className="form-control" required  />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="text-center">
                                    <button className="btn-link"><i className="far fa-plus mr-r-5"></i>Add new</button>
                                </div>
                            </div>
                        </div>

                        <div className="form-content-box">
                            <div className="form-content-header">
                                <p>Application(s)</p>
                            </div>
                            <div className="form-content-body">
                                <div className="form-group">
                                    <label className="form-group-label">Select Application :</label>
                                    <div className="input-group">
                                        <select className="form-control">
                                            <option>Application One</option>
                                            <option>Application Two</option>
                                        </select>
                                    </div>
                                </div>
                                <div className="d-flex">
                                    <div className="flex-50 pd-r-10">
                                        <div className="form-group">
                                            <label className="form-group-label">Application One (No. of devices):</label>
                                            <input type="number" id="number" className="form-control" />
                                        </div>
                                    </div>
                                    <div className="flex-50 pd-r-10">
                                        <div className="form-group">
                                            <label className="form-group-label">Application Two (No. of devices):</label>
                                            <input type="number" id="number" className="form-control" />
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div className="form-content-box">
                            <div className="form-content-header">
                                <p>Other Quota</p>
                            </div>
                            <div className="form-content-body">
                                <ul className="list-style-none d-flex custom-attribute-list">
                                    <li>
                                        <div className="form-group">
                                            <label className="check-box">
                                                <span className="check-text">SMS</span>
                                                <input type="checkbox"/>
                                                <span className="check-mark"></span>
                                            </label>
                                            <input type="text" id="path" name="categoryTopic" className="form-control"/>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="form-group">
                                            <label className="check-box">
                                                <span className="check-text">Emails</span>
                                                <input type="checkbox"/>
                                                <span className="check-mark"></span>
                                            </label>
                                            <input type="text" id="path" name="categoryTopic" className="form-control"/>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="form-group">
                                            <label className="check-box">
                                                <span className="check-text">User</span>
                                                <input type="checkbox"/>
                                                <span className="check-mark"></span>
                                            </label>
                                            <input type="text" id="path" name="categoryTopic" className="form-control"/>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="form-group">
                                            <label className="check-box">
                                                <span className="check-text">Roles</span>
                                                <input type="checkbox"/>
                                                <span className="check-mark"></span>
                                            </label>
                                            <input type="text" id="path" name="categoryTopic" className="form-control"/>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="form-group">
                                            <label className="check-box">
                                                <span className="check-text">Groups</span>
                                                <input type="checkbox"/>
                                                <span className="check-mark"></span>
                                            </label>
                                            <input type="text" id="path" name="categoryTopic"
                                                   className="form-control"/>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="form-group">
                                            <label className="check-box">
                                                <span className="check-box-label">Device Groups</span>
                                                <input type="checkbox"/>
                                                <span className="check-mark"></span>
                                            </label>
                                            <input type="text" id="path" name="categoryTopic" className="form-control"/>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="form-group">
                                            <label className="check-box">
                                                <span className="check-text">Events</span>
                                                <input type="checkbox"/>
                                                <span className="check-mark"></span>
                                            </label>
                                            <input type="text" id="path" name="categoryTopic" className="form-control"/>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="form-group">
                                            <label className="check-box">
                                                <span className="check-text">RPC</span>
                                                <input type="checkbox"/>
                                                <span className="check-mark"></span>
                                            </label>
                                            <input type="text" id="path" name="categoryTopic" className="form-control"/>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="form-group">
                                            <label className="check-box">
                                                <span className="check-text">Webhooks</span>
                                                <input type="checkbox"/>
                                                <span className="check-mark"></span>
                                            </label>
                                            <input type="text" id="path" name="categoryTopic" className="form-control"/>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div className="form-info-wrapper form-info-wrapper-left">
                        <div className="form-info-header">
                            <h5>Basic Information</h5>
                        </div>
                        <div className="form-info-body form-info-body-adjust">
                            <div className="form-group">
                                <label className="form-group-label">Name : <i className="fas fa-asterisk form-group-required"></i></label>
                                <input type="text" id="path" name="categoryTopic" className="form-control"/>
                            </div>
                            <div className="form-group">
                                <label className="form-group-label">Description: </label>
                                <textarea rows="3" id="description" className="form-control"/>
                            </div>
                            <div className="form-group">
                                <label className="form-group-label">Image :</label>
                                <div className="file-upload-box">
                                    <div className="file-upload-form">
                                        <input type="file" name="image"/>
                                        <p>Click to upload file...!</p>
                                    </div>
                                    {/*<div className="file-upload-loader">*/}
                                    {/*    <img src="https://content.iot83.com/m83/misc/uploading.gif" />*/}
                                    {/*    <p>Uploading...</p>*/}
                                    {/*</div>*/}
                                    {/*<div className="file-upload-preview">*/}
                                    {/*    <img src="" />*/}
                                    {/*    <input type="file" name="image" />*/}
                                    {/*    <h6><i className="far fa-pen" />Update</h6>*/}
                                    {/*</div>*/}
                                </div>
                            </div>
                        </div>
                        <div className="form-info-footer">
                            <button className="btn btn-light">cancel</button>
                            <button className="btn btn-primary" onClick={() => this.props.history.push('/clientList')}>Save</button>
                        </div>

                    </div>
                </div>

            </React.Fragment>
        );
    }
}

AddOrEditClient.propTypes = {
    dispatch: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
    addoreditclient: makeSelectAddOrEditClient()
});

function mapDispatchToProps(dispatch) {
    return {
        dispatch
    };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({key: "addOrEditClient", reducer});
const withSaga = injectSaga({key: "addOrEditClient", saga});

export default compose(
    withReducer,
    withSaga,
    withConnect
)(AddOrEditClient);
