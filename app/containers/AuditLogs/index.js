/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { FormattedMessage } from "react-intl";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import { getAuditLogsSuccess, getAuditLogsFailure, getOperationListSuccess, getOperationListFailure, getUsersListSuccess, getUsersListError } from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import { auditLogs, getOperationList, getAllUsers } from './actions';
import ReactTable from 'react-table';
//import Datetime from 'datetime-react-83incs';
import ReactSelect from 'react-select';
// import 'brace/theme/github';
import ReactJson from "react-json-view";
/* eslint-disable react/prefer-stateless-function */
const defaultButton = props => (
    <button type="button" {...props} className="-btn">
        {props.children}
    </button>
)

export class AuditLogs extends React.Component {

    state = {
        auditLogs: [],
        loading: true,
        page: 0,
        size: 10,
        // count: 50,
        to: new Date().getTime(),
        from: new Date().getTime() - 86400000,
        auditOperationList: [],
        operations: [],
        userId: "",
        errorMessage: '',
        showFunctionModal: false
    }

    componentWillMount() {
        this.props.auditLogs({ max: 10, offset: 0, order: 'ASC', sortBy: 'dateCreated' });
        this.props.getOperationList();
        this.props.getAllUsers();
    }

    toTitleCase(str) {
        return str.toLowerCase().split(' ')
            .map((s) => s.charAt(0).toUpperCase() + s.substring(1))
            .join(' ');
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.getAuditLogsSuccess && nextProps.getAuditLogsSuccess !== this.props.getAuditLogsSuccess) {
            this.setState({
                auditLogs: nextProps.getAuditLogsSuccess.object,
                count: nextProps.getAuditLogsSuccess.count,
                loading: false
            })

        }

        if (nextProps.getAuditLogsFailure && nextProps.getAuditLogsFailure !== this.props.getAuditLogsFailure) {
            this.setState({
                message: nextProps.getAuditLogsFailure,
                loading: false
            })
        }

        if (nextProps.getOperationListSuccess && nextProps.getOperationListSuccess !== this.props.getOperationListSuccess) {
            this.setState({
                auditOperationList: nextProps.getOperationListSuccess,
                loading: false
            })
        }

        if (nextProps.getOperationListFailure && nextProps.getOperationListFailure !== this.props.getOperationListFailure) {
            this.setState({
                message: nextProps.getOperationListFailure,
            })
        }

        if (nextProps.userList && nextProps.userList !== this.props.userList) {
            let allUsers = nextProps.userList.map(user => {
                return {
                    value: user.id,
                    label: this.toTitleCase(user.email)
                }
            })
            this.setState({
                isFetching: false,
                allUsers
            })
        }

        if (nextProps.userListError && nextProps.userListError !== this.props.userListError) {
            this.setState(prevState => ({
                isFetching: false,
                message2: nextProps.userListError.error,
                isOpen: true,
                type: 'error'
            }))
        }
    }

    fetchUsers = (state, instance) => {
        const sorted = state.sorted;
        const offset =
            typeof instance.state.page === 'undefined' || instance.state.page === 0
                ? 0
                : instance.state.page * instance.state.pageSize;
        let payload = this.state.payload
        payload = {
            offset,
            // max: this.state.count > 25 ? instance.state.pageSize : 10,
            max: instance.state.pageSize
        };
        this.setState({
            page: instance.state.page,
            size: instance.state.pageSize,
            loading: true
        });
        if (this.state.page != instance.state.page || this.state.size != instance.state.pageSize || sorted.length > 0) {
            if (sorted.length > 0) {
                payload.sortBy = sorted[0].id;
                payload.order = sorted[0].desc ? 'DESC' : 'ASC';
            }
            payload.order = 'ASC';
            payload.sortBy = 'dateCreated';
            payload.from = this.state.from;
            payload.to = this.state.to;
            payload.operations = this.state.operations.map((operation) => operation.value)
            payload.userId = this.state.userId.value
            this.props.auditLogs(payload);
        }
    };

    operationChangeHandler = (value) => {
        this.setState({ operations: value, clear: false })
    }

    showHandler = () => {
        let payload = {
            max: 10,
            offset: 0,
            order: 'ASC',
            sortBy: 'dateCreated',
            userId: this.state.userId.value,
            from: this.state.from,
            to: this.state.to,
            operations: this.state.operations.map((operation) => operation.value)
        }
        this.setState({ loading: true })
        this.props.auditLogs(payload)
    }

    auditOperationList = () => {
        let options = JSON.parse(JSON.stringify(this.state.auditOperationList)).map(operation => {
            return {
                value: operation,
                label: operation
            }
        })
        return options;
    }

    disabledFutureDate = (current) => {
        let today = Datetime.moment().subtract(0, 'day');
        return current.isBefore(today);
    }

    betweenFromToDate = (current) => {
        let fromDate = this.state.from
        let today = Datetime.moment().subtract(0, 'day');
        return current.isAfter(fromDate) && current.isBefore(today);
    }

    colorClass = (apiType) => {
        switch (apiType.toUpperCase()) {
            case "PUT":
                return "text-yellow"
            case "POST":
                return "text-lightGreen"
            case "GET":
                return "text-primary"
            case "PATCH":
                return "text-info"
            default:
                return "text-red"
        }
    }

    reactSelectChangeHandler = (userId) => {
        this.setState({ userId });
    }

    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>Audit Logs</title>
                    <meta name="description" content="Description of M83-AuditLogs" />
                </Helmet>

                <div className="pageBreadcrumb">
                    <div className="row">
                        <div className="col-8">
                            <p><span>Audit Logs</span></p>
                        </div>
                        <div className="col-4 text-right">
                            <div className="flex h-100 justify-content-end align-items-center"></div>
                        </div>
                    </div>
                </div>

                <div className="outerBox pb-0">
                    <div className="customMenuBox">
                        <div className="customLeftMenu">
                            <form className="contentForm">
                                <div className="form-group">
                                    <label className="form-label">From :</label>
                                    <Datetime isValidDate={this.disabledFutureDate} value={this.state.from} onChange={(from) => { this.setState({ from: Math.round(new Date(from).getTime()) }) }} />
                                </div>

                                <div className="form-group">
                                    <label className="form-label">To :</label>
                                    <Datetime isValidDate={this.betweenFromToDate} value={this.state.to} onChange={(to) => { this.setState({ to: Math.round(new Date(to).getTime()) }) }} />
                                </div>

                                <div className="form-group">
                                    <label className="form-label">User : </label>
                                    <ReactSelect
                                        options={this.state.allUsers}
                                        value={this.state.userId}
                                        onChange={this.reactSelectChangeHandler}
                                        isMulti={false}
                                        className="form-control-multi-select" >
                                    </ReactSelect>
                                </div>

                                <div className="form-group">
                                    <label className="form-label">Operations :</label>
                                    <ReactSelect
                                        className="form-control-multi-select"
                                        isMulti={true}
                                        value={this.state.clear ? "" : this.state.operations.value}
                                        onChange={this.operationChangeHandler}
                                        options={this.auditOperationList()}
                                    ></ReactSelect>
                                </div>

                                <div className="bottomButton">
                                    <button type="button" className="btn btn-success" onClick={this.showHandler}>Show</button>
                                    <button type="button" className="btn btn-dark" onClick={() => this.setState({
                                        clear: true,
                                        userId: "",
                                        operations: [],
                                        from: new Date().getTime(),
                                        to: new Date().getTime()
                                    })}>Clear
                                    </button>
                                </div>
                            </form>
                        </div>

                        <div className="customReactTableBox animated slideInUp mb-4">
                            <ReactTable
                                className="customReactTable"
                                data={this.state.auditLogs}
                                PreviousComponent={(props) => <button type="button" {...props}><i className="fal fa-angle-left"></i></button>}
                                NextComponent={(props) => <button type="button" {...props}><i className="fal fa-angle-right"></i></button>}
                                noDataText={this.state.loading ? '' : 'There is no data to display.'}
                                loading={this.state.loading}
                                manual
                                defaultPageSize={this.state.size}
                                pages={Math.ceil(this.state.count / this.state.size)}
                                columns={[
                                    {
                                        Header: 'Name',
                                        accessor: 'firstName',
                                        Cell: (row) => <div >{this.toTitleCase(`${row.original.firstName} ${row.original.lastName}`)}</div>
                                    },
                                    {
                                        Header: 'Email',
                                        accessor: 'email',
                                    },
                                    {
                                        Header: 'Method',
                                        accessor: 'method',
                                        Cell: (row) => <div className={this.colorClass(row.original.method)}>{row.original.method}</div>,
                                    },
                                    {
                                        Header: 'Date',
                                        Cell: row => (<span>{new Date(row.original.dateCreated).toLocaleString('en-US', {timeZone: localStorage.timeZone})}</span>)
                                    },
                                    {
                                        Header: 'Operation',
                                        accessor: 'operation',
                                    },
                                    // {
                                    //     Header: 'View',
                                    //     accessor: 'operation',
                                    //     Cell: row => (<i className="fas fa-eye"  onClick={() => this.setState({ viewData: JSON.parse(row.original.payload), showFunctionModal: true })} data-target="#functionModal"></i>)
                                    // },
                                ]}
                                onFetchData={this.fetchUsers}
                            />
                        </div>
                    </div>

                </div>
                {this.state.showFunctionModal &&
                    <div className="modal fade show d-block" role="dialog" id="functionModal" >
                        <div className="modal-dialog modal-lg modal-dialog-centered">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h6 className="modal-title"><span>Payload</span>
                                        <button className="close" data-dismiss="modal" onClick={() => this.setState({ showFunctionModal: false, viewData: '' })}><i className="far fa-times"></i></button>
                                    </h6>
                                </div>
                                <div className="modal-body">
                                    <div className="functionDetailBox">
                                        <ReactJson src={this.state.viewData} />
                                    </div>
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-success" data-dismiss="modal" onClick={() => this.setState({ showFunctionModal: false, viewData: '' })}>OK</button>
                                </div>
                            </div>
                        </div>
                    </div>
                }
            </React.Fragment>
        );
    }
}

AuditLogs.propTypes = {
    dispatch: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
    getAuditLogsSuccess: getAuditLogsSuccess(),
    getAuditLogsFailure: getAuditLogsFailure(),
    getOperationListSuccess: getOperationListSuccess(),
    getOperationListFailure: getOperationListFailure(),
    userList: getUsersListSuccess(),
    userListError: getUsersListError(),
});

function mapDispatchToProps(dispatch) {
    return {
        dispatch,
        auditLogs: (payload) => dispatch(auditLogs(payload)),
        getOperationList: () => dispatch(getOperationList()),
        getAllUsers: () => dispatch(getAllUsers()),
    };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "auditLogs", reducer });
const withSaga = injectSaga({ key: "auditLogs", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(AuditLogs);
