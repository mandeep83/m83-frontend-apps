/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { apiCallHandler } from '../../api';
import { takeEvery } from 'redux-saga';
import * as CONSTANTS from "./constants";

export function* getAuditLogs(action) {
  yield [apiCallHandler(action, CONSTANTS.AUDIT_LOGS_SUCCESS, CONSTANTS.AUDIT_LOGS_FAILURE, 'getAuditLogs')];
}

export function* watcherGetAuditLogs() {
  yield takeEvery(CONSTANTS.AUDIT_LOGS, getAuditLogs);
}

export function* getOperationList(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_OPERATION_LIST_SUCCESS, CONSTANTS.GET_OPERATION_LIST_FAILURE, 'getAuditOperations')];
}

export function* watcherGetOperationList() {
  yield takeEvery(CONSTANTS.GET_OPERATION_LIST, getOperationList);
}

export function* getUsersListApiHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_USERS_SUCCESS, CONSTANTS.GET_USERS_FAILURE, 'getUsersWithoutPagination')];
}

export function* watcherGetUsersListRequest() {
  yield takeEvery(CONSTANTS.GET_USERS, getUsersListApiHandlerAsync);
}

export default function* rootSaga() {
  yield [
    watcherGetAuditLogs(),
    watcherGetOperationList(),
    watcherGetUsersListRequest(),
  ];
}
