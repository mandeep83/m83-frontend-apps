/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { fromJS } from "immutable";
import * as CONSTANTS from "./constants";

export const initialState = fromJS({});

function auditLogsReducer(state = initialState, action) {
  switch (action.type) {
    case CONSTANTS.DEFAULT_ACTION:
      return initialState;
    case CONSTANTS.AUDIT_LOGS_SUCCESS:
      return Object.assign({}, state, {
        'getAuditLogsSuccess': action.response.data,
      })
    case CONSTANTS.AUDIT_LOGS_FAILURE:
      return Object.assign({}, state, {
        'getAuditLogsFailure': action.error,
      })
      case CONSTANTS.GET_OPERATION_LIST_SUCCESS:
      return Object.assign({}, state, {
        'getOperationListSuccess': action.response,
      })
    case CONSTANTS.GET_OPERATION_LIST_FAILURE:
      return Object.assign({}, state, {
        'getOperationListFailure': action.error,
      })
    case CONSTANTS.GET_USERS_SUCCESS:
      return Object.assign({}, state, {
          usersList: action.response
      });
    case CONSTANTS.GET_USERS_FAILURE:
        return Object.assign({}, state, {
            usersListError: {error: action.error, timestamp : Date.now()}
        });
    default:
      return state;
  }
}

export default auditLogsReducer;
