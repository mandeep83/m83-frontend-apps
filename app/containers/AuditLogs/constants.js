/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

export const DEFAULT_ACTION = "app/AuditLogs/DEFAULT_ACTION";

export const AUDIT_LOGS = "app/AuditLogs/AUDIT_LOGS";
export const AUDIT_LOGS_SUCCESS = "app/AuditLogs/AUDIT_LOGS_SUCCESS ";
export const AUDIT_LOGS_FAILURE = "app/AuditLogs/AUDIT_LOGS_FAILURE";

export const GET_OPERATION_LIST = "app/AuditLogs/GET_OPERATION_LIST";
export const GET_OPERATION_LIST_SUCCESS = "app/AuditLogs/GET_OPERATION_LIST_SUCCESS ";
export const GET_OPERATION_LIST_FAILURE = "app/AuditLogs/GET_OPERATION_LIST_FAILURE";

export const GET_USERS = "app/AuditLogs/GET_USERS";
export const GET_USERS_SUCCESS = "app/AuditLogs/GET_USERS_SUCCESS ";
export const GET_USERS_FAILURE = "app/AuditLogs/GET_USERS_FAILURE";
