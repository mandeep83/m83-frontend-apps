/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/*
 *
 * MyAccount actions
 *
 */

import * as CONSTANTS from "./constants";

export function getProfileData() {
  return {
    type: CONSTANTS.GET_PROFILE_DETAILS
  };
}

export function getSubscriptionPlans() {
  return {
    type: CONSTANTS.GET_SUBSCRIPTIONS_PLANS_DETAILS
  };
}

export function getBillingDetails() {
  return {
    type: CONSTANTS.GET_BILLING_DETAILS
  };
}

export function getProfileAnalysis() {
  return {
    type: CONSTANTS.GET_PROFILE_ANALYSIS
  };
}

export function getAvailablePlans() {
  return {
    type: CONSTANTS.GET_ALL_AVAILABLE_PLANS
  };
}

export function cancelSubscription(reason) {
  return {
    type: CONSTANTS.CANCEL_SUBSCRIPTION,
    reason,
  };
}

export function upgradeSubscription(packageId, isTrialPackage) {
  return {
    type: CONSTANTS.UPGRADE_SUBSCRIPTION,
    packageId,
    isTrialPackage,
  };
}

export function renewSubscription(packageId) {
  return {
    type: CONSTANTS.RENEW_SUBSCRIPTION,
    packageId,
  };
}
