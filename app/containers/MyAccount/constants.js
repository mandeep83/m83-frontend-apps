/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/*
 *
 * MyAccount constants
 *
 */

export const GET_PROFILE_DETAILS = "app/MyAccount/GET_PROFILE_DETAILS";
export const GET_PROFILE_DETAILS_SUCCESS = "app/MyAccount/GET_PROFILE_DETAILS_SUCCESS";
export const GET_PROFILE_DETAILS_FAILURE = "app/MyAccount/GET_PROFILE_DETAILS_FAILURE";

export const GET_SUBSCRIPTIONS_PLANS_DETAILS = "app/MyAccount/GET_SUBSCRIPTIONS_PLANS_DETAILS";
export const GET_SUBSCRIPTIONS_PLANS_DETAILS_SUCCESS = "app/MyAccount/GET_SUBSCRIPTIONS_PLANS_DETAILS_SUCCESS";
export const GET_SUBSCRIPTIONS_PLANS_DETAILS_FAILURE = "app/MyAccount/GET_SUBSCRIPTIONS_PLANS_DETAILS_FAILURE";

export const GET_BILLING_DETAILS = "app/MyAccount/GET_BILLING_DETAILS";
export const GET_BILLING_DETAILS_SUCCESS = "app/MyAccount/GET_BILLING_DETAILS_SUCCESS";
export const GET_BILLING_DETAILS_FAILURE = "app/MyAccount/GET_BILLING_DETAILS_FAILURE";

export const GET_PROFILE_ANALYSIS = "app/MyAccount/GET_PROFILE_ANALYSIS";
export const GET_PROFILE_ANALYSIS_SUCCESS = "app/MyAccount/GET_PROFILE_ANALYSIS_SUCCESS";
export const GET_PROFILE_ANALYSIS_FAILURE = "app/MyAccount/GET_PROFILE_ANALYSIS_FAILURE";

export const GET_ALL_AVAILABLE_PLANS = "app/MyAccount/GET_ALL_AVAILABLE_PLANS";
export const GET_ALL_AVAILABLE_PLANS_SUCCESS = "app/MyAccount/GET_ALL_AVAILABLE_PLANS_SUCCESS";
export const GET_ALL_AVAILABLE_PLANS_FAILURE = "app/MyAccount/GET_ALL_AVAILABLE_PLANS_FAILURE";

export const CANCEL_SUBSCRIPTION = "app/MyAccount/CANCEL_SUBSCRIPTION";
export const CANCEL_SUBSCRIPTION_SUCCESS = "app/MyAccount/CANCEL_SUBSCRIPTION_SUCCESS";
export const CANCEL_SUBSCRIPTION_FAILURE = "app/MyAccount/CANCEL_SUBSCRIPTION_FAILURE";

export const UPGRADE_SUBSCRIPTION = "app/MyAccount/UPGRADE_SUBSCRIPTION";
export const UPGRADE_SUBSCRIPTION_SUCCESS = "app/MyAccount/UPGRADE_SUBSCRIPTION_SUCCESS";
export const UPGRADE_SUBSCRIPTION_FAILURE = "app/MyAccount/UPGRADE_SUBSCRIPTION_FAILURE";

export const RENEW_SUBSCRIPTION = "app/MyAccount/RENEW_SUBSCRIPTION";
export const RENEW_SUBSCRIPTION_SUCCESS = "app/MyAccount/RENEW_SUBSCRIPTION_SUCCESS";
export const RENEW_SUBSCRIPTION_FAILURE = "app/MyAccount/RENEW_SUBSCRIPTION_FAILURE";


