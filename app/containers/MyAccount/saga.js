/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

// import { take, call, put, select } from 'redux-saga/effects';

import { apiCallHandler } from '../../api';
import { takeEvery } from 'redux-saga';
import * as CONSTANTS from './constants';
// Individual exports for testing

export function* getProfileDetails(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_PROFILE_DETAILS_SUCCESS, CONSTANTS.GET_PROFILE_DETAILS_FAILURE, 'getProfileDetails')]
}

export function* getSubscriptionDetails(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_SUBSCRIPTIONS_PLANS_DETAILS_SUCCESS, CONSTANTS.GET_SUBSCRIPTIONS_PLANS_DETAILS_FAILURE, 'getSubscriptionDetails')]
}

export function* getBillingDetails(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_BILLING_DETAILS_SUCCESS, CONSTANTS.GET_BILLING_DETAILS_FAILURE, 'getBillingDetails')]
}

export function* getProfileStatsAnalysis(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_PROFILE_ANALYSIS_SUCCESS, CONSTANTS.GET_PROFILE_ANALYSIS_FAILURE, 'getProfileStatsAnalysis')]
}

export function* getAllAvailablePlan(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_ALL_AVAILABLE_PLANS_SUCCESS, CONSTANTS.GET_ALL_AVAILABLE_PLANS_FAILURE, 'getAllAvailablePlans')]
}

export function* cancelSubscriptionPlanHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.CANCEL_SUBSCRIPTION_SUCCESS, CONSTANTS.CANCEL_SUBSCRIPTION_FAILURE, 'cancelSubscription')]
}

export function* upgradeSubscriptionPlanHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.UPGRADE_SUBSCRIPTION_SUCCESS, CONSTANTS.UPGRADE_SUBSCRIPTION_FAILURE, 'upgradeSubscription')]
}

export function* renewSubscriptionPlanHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.RENEW_SUBSCRIPTION_SUCCESS, CONSTANTS.RENEW_SUBSCRIPTION_FAILURE, 'renewSubscription')]
}

export function* watcherGetProfileDetails() {
  yield takeEvery(CONSTANTS.GET_PROFILE_DETAILS, getProfileDetails)
}

export function* watcherGetSubscriptionsDetails() {
  yield takeEvery(CONSTANTS.GET_SUBSCRIPTIONS_PLANS_DETAILS, getSubscriptionDetails)
}

export function* watcherGetBillingDetails() {
  yield takeEvery(CONSTANTS.GET_BILLING_DETAILS, getBillingDetails)
}

export function* watcherProfileStatsAnalysis() {
  yield takeEvery(CONSTANTS.GET_PROFILE_ANALYSIS, getProfileStatsAnalysis)
}

export function* watcherGetAllAvailablePlans() {
  yield takeEvery(CONSTANTS.GET_ALL_AVAILABLE_PLANS, getAllAvailablePlan)
}

export function* watcherCancelSubscriptionPlan() {
  yield takeEvery(CONSTANTS.CANCEL_SUBSCRIPTION, cancelSubscriptionPlanHandlerAsync)
}

export function* watcherUpgradeSubscriptionPlan() {
  yield takeEvery(CONSTANTS.UPGRADE_SUBSCRIPTION, upgradeSubscriptionPlanHandlerAsync)
}

export function* watcherRenewSubscriptionPlan() {
  yield takeEvery(CONSTANTS.RENEW_SUBSCRIPTION, renewSubscriptionPlanHandlerAsync)
}

export default function* rootSaga() {
  yield [
    watcherGetProfileDetails(),
    watcherGetSubscriptionsDetails(),
    watcherGetBillingDetails(),
    watcherProfileStatsAnalysis(),
    watcherGetAllAvailablePlans(),
    watcherCancelSubscriptionPlan(),
    watcherUpgradeSubscriptionPlan(),
    watcherRenewSubscriptionPlan(),
  ]
}