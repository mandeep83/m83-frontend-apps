/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
 * Direct selector to the myProfile state domain
 */

const selectMyProfileDomain = state => state.get("myProfile", initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by MyAccount
 */

export const getProfileDetailsSuccess = () => createSelector(selectMyProfileDomain, subState => subState.getProfileDetailsSuccess);
export const getProfileDetailsFailure = () => createSelector(selectMyProfileDomain, subState => subState.getProfileDetailsFailure);

export const getSubscriptionDetailsSuccess = () => createSelector(selectMyProfileDomain, subState => subState.getSubscriptionDetailsSuccess);
export const getSubscriptionDetailsFailure = () => createSelector(selectMyProfileDomain, subState => subState.getSubscriptionDetailsFailure);

export const getBillingDetailsSuccess = () => createSelector(selectMyProfileDomain, subState => subState.getBillingDetailsSuccess);
export const getBillingDetailsFailure = () => createSelector(selectMyProfileDomain, subState => subState.getBillingDetailsFailure);

export const getProfileAnalysisSuccess = () => createSelector(selectMyProfileDomain, subState => subState.getProfileAnalysisSuccess);
export const getProfileAnalysisFailure = () => createSelector(selectMyProfileDomain, subState => subState.getProfileAnalysisFailure);

export const getAllAvailablePlansSuccess = () => createSelector(selectMyProfileDomain, subState => subState.getAllAvailablePlansSuccess);
export const getAllAvailablePlansFailure = () => createSelector(selectMyProfileDomain, subState => subState.getAllAvailablePlansFailure);

export const cancelSubscriptionSuccess = () => createSelector(selectMyProfileDomain, subState => subState.cancelSubscriptionSuccess);
export const cancelSubscriptionFailure = () => createSelector(selectMyProfileDomain, subState => subState.cancelSubscriptionFailure);

export const upgradeSubscriptionSuccess = () => createSelector(selectMyProfileDomain, subState => subState.upgradeSubscriptionSuccess);
export const upgradeSubscriptionFailure = () => createSelector(selectMyProfileDomain, subState => subState.upgradeSubscriptionFailure);

export const renewSubscriptionSuccess = () => createSelector(selectMyProfileDomain, subState => subState.renewSubscriptionSuccess);
export const renewSubscriptionFailure = () => createSelector(selectMyProfileDomain, subState => subState.renewSubscriptionFailure);

export { selectMyProfileDomain };
