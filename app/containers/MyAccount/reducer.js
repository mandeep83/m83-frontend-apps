/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/*
 *
 * MyAccount reducer
 *
 */

import { fromJS } from "immutable";
import * as CONSTANTS from "./constants";

export const initialState = fromJS({});

function myProfileReducer(state = initialState, action) {
  switch (action.type) {
    case CONSTANTS.GET_PROFILE_DETAILS_SUCCESS:
      return Object.assign({}, state, {
        getProfileDetailsSuccess: action.response
      });
    case CONSTANTS.GET_PROFILE_DETAILS_FAILURE:
      return Object.assign({}, state, {
        getProfileDetailsFailure: { message: action.error, timestamp: Date.now() }
      });
    case CONSTANTS.GET_SUBSCRIPTIONS_PLANS_DETAILS_SUCCESS:
      return Object.assign({}, state, {
        getSubscriptionDetailsSuccess: action.response
      });
    case CONSTANTS.GET_SUBSCRIPTIONS_PLANS_DETAILS_FAILURE:
      return Object.assign({}, state, {
        getSubscriptionDetailsFailure: { message: action.error, timestamp: Date.now() }
      });
    case CONSTANTS.GET_BILLING_DETAILS_SUCCESS:
      return Object.assign({}, state, {
        getBillingDetailsSuccess: action.response
      });
    case CONSTANTS.GET_BILLING_DETAILS_FAILURE:
      return Object.assign({}, state, {
        getBillingDetailsFailure: { message: action.error, timestamp: Date.now() }
      });
    case CONSTANTS.GET_PROFILE_ANALYSIS_SUCCESS:
      return Object.assign({}, state, {
        getProfileAnalysisSuccess: action.response
      });
    case CONSTANTS.GET_PROFILE_ANALYSIS_FAILURE:
      return Object.assign({}, state, {
        getProfileAnalysisFailure: { message: action.error, timestamp: Date.now() }
      });
    case CONSTANTS.GET_ALL_AVAILABLE_PLANS_SUCCESS:
      return Object.assign({}, state, {
        getAllAvailablePlansSuccess: action.response
      });
    case CONSTANTS.GET_ALL_AVAILABLE_PLANS_FAILURE:
      return Object.assign({}, state, {
        getAllAvailablePlansFailure: { message: action.error, timestamp: Date.now() }
      });
    case CONSTANTS.CANCEL_SUBSCRIPTION_SUCCESS:
      return Object.assign({}, state, {
        cancelSubscriptionSuccess: action.response
      });
    case CONSTANTS.CANCEL_SUBSCRIPTION_FAILURE:
      return Object.assign({}, state, {
        cancelSubscriptionFailure: { message: action.error, timestamp: Date.now() }
      });
    case CONSTANTS.UPGRADE_SUBSCRIPTION_SUCCESS:
      return Object.assign({}, state, {
        upgradeSubscriptionSuccess: action.response
      });
    case CONSTANTS.UPGRADE_SUBSCRIPTION_FAILURE:
      return Object.assign({}, state, {
        upgradeSubscriptionFailure: { message: action.error, timestamp: Date.now() }
      });
    case CONSTANTS.RENEW_SUBSCRIPTION_SUCCESS:
      return Object.assign({}, state, {
        renewSubscriptionSuccess: action.response
      });
    case CONSTANTS.RENEW_SUBSCRIPTION_FAILURE:
      return Object.assign({}, state, {
        renewSubscriptionFailure: { message: action.error, timestamp: Date.now() }
      });
    default:
      return state;
  }
}

export default myProfileReducer;
