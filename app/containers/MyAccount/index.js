/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * MyAccount
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import * as ACTIONS from './actions';
import * as SELECTORS from './selectors'
import reducer from "./reducer";
import saga from "./saga";
import NotificationModal from "../../components/NotificationModal/Loadable";
import { getBillingTime,getTimeDifference } from '../../commonUtils';
import PhoneInput from "react-phone-input-2";
import 'react-phone-input-2/lib/style.css'
import ReactTooltip from "react-tooltip";
import ListingTable from "../../components/ListingTable/Loadable";
import { CircularProgressbar, buildStyles } from 'react-circular-progressbar';


/* eslint-disable react/prefer-stateless-function */
export class MyAccount extends React.Component {

    state = {
        profileData: {},
        activeTab: "subscriptionPlans",
        allAvailablePlans: [],
        activePlan: {},
        isPlanExpired: false,
        isFetching: true,
        isFetchingMyPlanDetails: true,
        isFetchingAllPlanDetails: true,
        isFetchingBillingDetails: false,
        confirmCancelSubscription: false,
        upgradePlanConfirmation: false,
        buyPlanConfirmation: false,
        upgradeSuccessModal: false,
        purchaseSuccessModal: false,
        cancelSubscriptionSuccess: false,
        disableCancel: false,
        selectedPackageId: null,
        features: [{
                name: "device_types",
                displayName: "Device Type(s)",
                showDescription: false,
            }, {
                name: "no_of_devices",
                displayName: "Device(s)",
                showDescription: false,
            }, {
                name: "device_groups",
                displayName: "Device Group(s)",
                showDescription: false,
            }, {
                name: "attributeKeys",
                displayName: "Device Attribute(s)",
                showDescription: false,
            }, {
                name: "events",
                displayName: "Event(s)",
                showDescription: true,
            }, {
                name: "rules",
                displayName: "Rule(s)",
                showDescription: true,
            }, {
                name: "sms",
                displayName: "SMS",
                showDescription: false,
            }, {
                name: "email",
                displayName: "Email(s)",
                showDescription: false,
            }, {
                name: "webhooks",
                displayName: "Webhook(s)",
                showDescription: true,
            }, {
                name: "rpc",
                displayName: "Control(s)",
                showDescription: true,
            }, {
                name: "retention",
                displayName: "Retention",
                showDescription: true,
            }
        ]
    }

    componentWillMount() {
        const queryString = window.location.search,
            urlParams = new URLSearchParams(queryString);
        if (urlParams.get("upgrade") === 'true') {
            this.setState({
                upgradeSuccessModal: true,
            })
        } else if (urlParams.get("renew") === 'true') {
            this.setState({
                purchaseSuccessModal: true,
            })
        } else {
            this.props.getProfileData();
            this.props.getSubscriptionPlans();
            this.props.getAvailablePlans();
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.getProfileDetailsSuccess && this.props.getProfileDetailsSuccess != nextProps.getProfileDetailsSuccess) {
            this.setState({
                isFetching: false,
                profileData: nextProps.getProfileDetailsSuccess,
            })
        }
        if (nextProps.getProfileDetailsFailure && this.props.getProfileDetailsFailure != nextProps.getProfileDetailsFailure) {
            this.setState({
                isFetching: false,
                isOpen: true,
                message2: nextProps.getProfileDetailsFailure.message,
                modalType: "error",
            })
        }
        if (nextProps.getSubscriptionDetailsSuccess && this.props.getSubscriptionDetailsSuccess != nextProps.getSubscriptionDetailsSuccess) {
            let activePlan = nextProps.getSubscriptionDetailsSuccess.find(el => el.projectId == localStorage.selectedProjectId).packages[0],
                isPlanExpired = new Date().getTime() > activePlan.packageExpiry ? true : false;

            this.setState({
                isFetchingMyPlanDetails: false,
                isPlanExpired,
                activePlan,
            })
        }
        if (nextProps.getSubscriptionDetailsFailure && this.props.getSubscriptionDetailsFailure != nextProps.getSubscriptionDetailsFailure) {
            this.setState({
                isOpen: true,
                isFetchingMyPlanDetails: false,
                message2: nextProps.getSubscriptionDetailsFailure.message,
                modalType: "error",
            })
        }
        if (nextProps.getAllAvailablePlansSuccess && this.props.getAllAvailablePlansSuccess != nextProps.getAllAvailablePlansSuccess) {
            let allAvailablePlans = nextProps.getAllAvailablePlansSuccess.plans;
                allAvailablePlans.map(plan => {
                    let featureList = [];
                    this.state.features.map(feature => {
                        let featureObj = plan.productList.filter(product => product.name === feature.name)[0];
                            featureObj.displayName = feature.displayName;
                            featureObj.showDescription = feature.showDescription;
                            if (feature.name === 'retention') {
                                featureObj.count = featureObj.count/30 >= 12 ? `${featureObj.count/(30*12)} Year` : `${featureObj.count/(30)} Month(s)`
                            }
                        featureList.push(featureObj);
                    })
                    plan.featureList = featureList;
                })
            this.setState({
                isFetchingAllPlanDetails: false,
                allAvailablePlans,
            })
        }
        if (nextProps.getAllAvailablePlansFailure && this.props.getAllAvailablePlansFailure != nextProps.getAllAvailablePlansFailure) {
            this.setState({
                isOpen: true,
                isFetchingAllPlanDetails: false,
                message2: nextProps.getAllAvailablePlansFailure.message,
                modalType: "error",
            })
        }
        if (nextProps.getBillingDetailsSuccess && this.props.getBillingDetailsSuccess != nextProps.getBillingDetailsSuccess) {
            this.setState({
                isFetchingBillingDetails: false,
                billingDetails: nextProps.getBillingDetailsSuccess
            })
        }
        if (nextProps.getBillingDetailsFailure && this.props.getBillingDetailsFailure != nextProps.getBillingDetailsFailure) {
            this.setState({
                isOpen: true,
                isFetchingBillingDetails: false,
                message2: nextProps.getBillingDetailsFailure.message,
                modalType: "error",
            })
        }
        if (nextProps.getProfileAnalysisSuccess && this.props.getProfileAnalysisSuccess != nextProps.getProfileAnalysisSuccess) {
            this.setState({
                profileAnalysisDetails: nextProps.getProfileAnalysisSuccess
            })
        }
        if (nextProps.getProfileAnalysisFailure && this.props.getProfileAnalysisFailure != nextProps.getProfileAnalysisFailure) {
            this.setState({
                isOpen: true,
                message2: nextProps.getProfileAnalysisFailure.message,
                modalType: "error",
            })
        }
        if (nextProps.cancelSubscriptionSuccess && this.props.cancelSubscriptionSuccess != nextProps.cancelSubscriptionSuccess) {
            this.setState({
                disableCancel: true,
                cancelSubscriptionSuccess: true,
            })
        }
        if (nextProps.cancelSubscriptionFailure && this.props.cancelSubscriptionFailure != nextProps.cancelSubscriptionFailure) {
            this.setState({
                isOpen: true,
                isFetching: false,
                message2: nextProps.cancelSubscriptionFailure.message,
                modalType: "error",
            })
        }
        if (nextProps.upgradeSubscriptionSuccess && this.props.upgradeSubscriptionSuccess != nextProps.upgradeSubscriptionSuccess) {
            window.open(nextProps.upgradeSubscriptionSuccess, '_self');
        }
        if (nextProps.upgradeSubscriptionFailure && this.props.upgradeSubscriptionFailure != nextProps.upgradeSubscriptionFailure) {
            this.setState({
                isOpen: true,
                isFetching: false,
                message2: nextProps.upgradeSubscriptionFailure.message,
                modalType: "error",
            })
        }
        if (nextProps.renewSubscriptionSuccess && this.props.renewSubscriptionSuccess != nextProps.renewSubscriptionSuccess) {
            window.open(nextProps.renewSubscriptionSuccess, '_self');
        }
        if (nextProps.renewSubscriptionFailure && this.props.renewSubscriptionFailure != nextProps.renewSubscriptionFailure) {
            this.setState({
                isOpen: true,
                isFetching: false,
                message2: nextProps.renewSubscriptionFailure.message,
                modalType: "error",
            })
        }
    }

    changeTabHandler = (tabName) => {
        if (tabName == "billingDetails") {
            if (!this.state.billingDetails) {
                this.setState({
                    isFetchingBillingDetails: true,
                }, () => { this.props.getBillingDetails() })
            }

        }
        else if (tabName == "profileAnalysis") {
            if (!this.state.profileAnalysisDetails) {
                this.props.getProfileAnalysis()
            }
        }
        this.setState({
            activeTab: tabName,
        });
    }

    closeHandler = () => {
        this.setState({
            isOpen: false,
            type: '',
            message1: '',
            message2: ''
        })
    }

    handleUpgrade = () => {
        let isTrialPackage = this.state.activePlan.subscriptionPrice === 0 ? true : false;
        this.setState({
            isFetching: true,
            upgradePlanConfirmation: false,
        }, () => {
            this.props.upgradeSubscription(this.state.selectedPackageId, isTrialPackage)
        })
    }

    handlePurchaseSubscription = () => {
        this.setState({
            isFetching: true,
            buyPlanConfirmation: false,
        }, () => {
            this.props.renewSubscription(this.state.selectedPackageId)
        })
    }

    handlePaymentSuccessModalClose = () => {
        this.setState({
            purchaseSuccessModal: false,
            upgradeSuccessModal: false
        }, () => {
            this.props.history.push('/myAccount');
            this.props.getProfileData();
            this.props.getSubscriptionPlans();
            this.props.getAvailablePlans();
        });
    }

    handleCancelSuccessModalClose = () => {
        this.setState({
            isFetching: false,
            cancelSubscriptionSuccess: false,
            isFetchingMyPlanDetails: true,
            isFetchingAllPlanDetails: true,
        }, () => {
            this.props.getSubscriptionPlans();
            this.props.getAvailablePlans();
        });
    }
    getDisplayText =(feature) => {
        if (feature.name === 'device_types') {
            return feature.displayName;
        }else if (feature.name === 'no_of_devices') {
            return feature.displayName + " (Total)";
        } else if (feature.name === 'device_groups') {
            return feature.displayName + " / Device Type";
        } else if (feature.name === 'attributeKeys') {
            return feature.displayName + " / Device Type";;
        } else if (feature.name === 'events') {
            return feature.displayName + " / Device Type";;
        } else if (feature.name === 'rules') {
            return feature.displayName + " / Event";;
        } else if (feature.name === 'sms') {
            return feature.displayName + " / mo";
        } else if (feature.name === 'email') {
            return feature.displayName + " / mo";;
        } else if (feature.name === 'webhooks') {
            return feature.displayName + " / mo";;
        } else if (feature.name === 'rpc') {
            return feature.displayName + " / mo";;
        } else if (feature.name === 'retention') {
            return feature.displayName;
        }
    }
    getColumns = () => {
        return [
            {
                Header: "Transaction Type", width: 50, cell: row => (
                    <React.Fragment>
                        <div className="list-view-icon">
                            <img src={require('../../assets/images/other/paypal-logo.png')} />
                        </div>
                        <div className="list-view-icon-box">
                            <h6 className="text-theme fw-600">{row.type}</h6>
                            <p>Transaction Id : <strong className="text-gray">{row.txnId ? row.txnId : 'N/A'}</strong></p>
                        </div>
                    </React.Fragment>
                )
            },
            {
                Header: "Amount", width: 20, cell: row => (
                    <React.Fragment>
                        <h6 className="text-dark-theme fw-700">$ {(row.amount).toFixed(2)}</h6>
                        <p>{getTimeDifference(row.txnDate)}</p>
                    </React.Fragment>
                )
            },
            {
                Header: "Status", width: 15, cell: row => (
                    <div className="">
                        {row.status === 'SUCCESS' || row.status === 'PAID' ?
                            <span className="badge badge-pill alert-success list-view-pill"><i className="fad fa-check-circle mr-r-5"></i>{row.status}</span> :
                            row.status === 'CANCELLED' ?
                                <span className="badge badge-pill alert-danger list-view-pill"><i className="fad fa-times-circle mr-r-5"></i>{row.status}</span> :
                                <span className="badge badge-pill alert-warning list-view-pill"><i className="fad fa-exclamation-triangle mr-r-5"></i>{row.status}</span>
                        }
                    </div>
                )
            },
            {
                Header: "Invoice", width: 10, cell: row => (
                    <div className="button-group">
                        <button className="btn-transparent btn-transparent-blue" onClick={() => { window.open(row.invoiceUrl, '_blank') }} disabled={!row.invoiceUrl}>
                            <i className="far fa-file-invoice-dollar"></i>
                        </button>
                    </div>
                )
            },
        ]
    }

    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>Account Details</title>
                    <meta name="description" content="M83-MyAccount" />
                </Helmet>

                <header className="content-header d-flex">
                    <div className="flex-60">
                        <h6>Account Details</h6>
                    </div>
                    <div className="flex-40 text-right"></div>
                </header>
    
                {this.state.isFetching ? (!this.state.upgradeSuccessModal && !this.state.purchaseSuccessModal && <Loader />) :
                    <div className="content-body" id="makerProfileTrial">
                        <div className="d-flex">
                            <div className="flex-35 pd-r-7">
                                <div className="card">
                                    <div className="profile-image-box">
                                        <div className="profile-image-content">
                                            <h6>Welcome,</h6>
                                            <h4>{this.state.profileData.name}</h4>
                                        </div>
                                        <div className="profile-image-bg">
                                            <img src="https://content.iot83.com/m83/misc/icons/userProfileBg.jpg" />
                                        </div>
                                        <div className="profile-image">
                                            <img src="https://content.iot83.com/m83/misc/icons/userProfile.png" />
                                        </div>
                                    </div>
                                    <div className="profile-body mb-3">
                                        <p><i className="fad fa-envelope"></i>{this.state.profileData.email}</p>
                                        <div className="list-phone-input"><i className="fad fa-phone-alt f-12 text-green mr-r-10"></i>
                                            <PhoneInput
                                                inputProps={{
                                                    name: 'phone',
                                                }}
                                                disabled={true}
                                                disableDropdown={true}
                                                value={this.state.profileData.phone}
                                                onChange={(value) => null}
                                            />
                                        </div>
                                    </div>
                                </div>
    
                                <div className="card current-plan-wrapper">
                                    <div className="current-plan-header border-none">
                                        <h6>Current Subscription : <span className="text-pink fw-600">{this.state.activePlan.name}</span></h6>
                                        {this.state.activePlan.isPaymentPending ?
                                            <p className="text-content f-11 m-0">Validity details will be updated once the payment is processed.</p> :
                                            this.state.isPlanExpired ?
                                                <p className="text-content f-11 m-0">Expired On: {new Date(this.state.activePlan.packageExpiry).toLocaleString('en-US', { hour12: false, timeZone: localStorage.timeZone })}</p> :
                                                <p className="text-content f-11 m-0">Expiring On: {this.state.activePlan.packageExpiry && new Date(this.state.activePlan.packageExpiry).toLocaleString('en-US', { hour12: false, timeZone: localStorage.timeZone })}</p>
                                        }
                                        <h5>${this.state.activePlan.subscriptionPrice}<small className="f-10 ml-1">USD</small></h5>
                                    </div>
                                </div>
    
                                <div className="card current-plan-wrapper mb-0">
                                    <div className="current-plan-header">
                                        <h6>Enterprise Subscription <a href="https://platform.flex83.com/signUp" target="_blank" className="float-right f-12">Try it out <i className="far fa-share ml-1"></i></a></h6>
                                    </div>
                                    <div className="current-plan-body pl-3">
                                        <p className="text-content f-11"><i className="fas fa-circle mr-2 f-6"></i>Add more Device Types, Groups, Attributes.</p>
                                        <p className="text-content f-11"><i className="fas fa-circle mr-2 f-6"></i>Add more Emails, SMS, Webhooks and Controls.</p>
                                        <p className="text-content f-11"><i className="fas fa-circle mr-2 f-6"></i>Add Users, User Device Groups and Role.</p>
                                        <p className="text-content f-11"><i className="fas fa-circle mr-2 f-6"></i>Add API and FaaS.</p>
                                        <p className="text-content f-11"><i className="fas fa-circle mr-2 f-6"></i>Add ETL, ETL Notebooks and ETL UDF.</p>
                                        <p className="text-content f-11"><i className="fas fa-circle mr-2 f-6"></i>Add ML Experiments.</p>
                                        <p className="text-content f-11 mb-0"><i className="fas fa-circle mr-2 f-6"></i>Add Pages and Menus.</p>
                                    </div>
                                </div>
                            </div>
                            
                            <div className="flex-65 pd-l-7">
                                <div className="action-tabs mr-b-15">
                                    <ul className="nav nav-tabs">
                                        <li className={this.state.activeTab == "subscriptionPlans" ? "nav-item active" : "nav-item"}  onClick={() => this.changeTabHandler("subscriptionPlans")}>
                                            <a className="nav-link">Subscriptions</a>
                                        </li>
                                        <li className={this.state.activeTab == "billingDetails" ? "nav-item active" : "nav-item"}  onClick={() => this.changeTabHandler("billingDetails")}>
                                            <a className="nav-link">Billing</a>
                                        </li>
                                    </ul>
                                </div>
    
                                <div className="tab-content" id="makerProfile">
                                    {this.state.activeTab == "subscriptionPlans" ?
                                        <div className="tab-pane fade show active">
                                            <div className="plan-outer-wrapper">
                                                {!this.state.isFetchingMyPlanDetails && !this.state.isFetchingAllPlanDetails ?
                                                    <ul className="list-style-none d-flex plan-view-list">
                                                        {this.state.allAvailablePlans.map((el, index) =>
                                                            <li className="flex-40" key={index}>
                                                                <div className="plan-view">
                                                                    <div className="plan-view-header">
                                                                        <div className="plan-view-icon">
                                                                            <img src={el.name == "TRIAL" ? "https://content.iot83.com/m83/account/free.png" : `https://content.iot83.com/m83/account/${el.name.toLowerCase()}.png`} />
                                                                        </div>
                                                                        <h6 className={`fw-600 text-${el.name == "SILVER" ? "theme" : el.name == "BRONZE" ? "indigo" : el.name == "GOLD" ? "orange" : el.name == "PLATINUM" ? "green" : "theme"}`}>{el.name}</h6>
                                                                        <h5>${el.subscriptionPrice} USD<small> / {el.numberOfDays === 30 ? 'Mo' : `${el.numberOfDays} Days`}</small></h5>
                                                                    </div>
                                                                    <div className="plan-view-body">
                                                                        <h6 className="text-gray mb-3 text-center f-12 fw-600">What do you get ?</h6>
                                                                        {el.featureList.map(feature =>
                                                                            <p key={feature.id}>
                                                                                <i className={feature.count === 0 ? "fal fa-times text-red" : "far fa-check"}></i>
                                                                                <strong>{feature.count}</strong>
                                                                                {this.getDisplayText(feature)}
                                                                                {feature.showDescription &&
                                                                                <span className="plan-view-body-info" data-tooltip data-tooltip-text={feature.description} data-tooltip-place="right" >
                                                                                <i className="fad fa-info-circle text-theme"></i>
                                                                            </span>
                                                                                }
                                                                            </p>
                            
                                                                        )}
                                                                    </div>
                                                                    {el.renewPlan ?
                                                                        <button className="btn btn-success" onClick={() => { this.setState({ buyPlanConfirmation: true, selectedPackageId: el.id }) }}>Buy</button>
                                                                        :
                                                                        el.enable && <button className={`btn btn-${el.name == "SILVER" ? "primary" : el.name == "BRONZE" ? "info" : el.name == "GOLD" ? "warning" : el.name == "PLATINUM" ? "success" : ""}`} onClick={() => { this.setState({ upgradePlanConfirmation: true, selectedPackageId: el.id }) }} disabled={!el.upgradePlan}>Upgrade</button>
                                                                    }
                                                                    {/*<div className="plan-view-overlay"></div>*/}
                                                                </div>
                                                            </li>
                                                        )}
                                                    </ul>
                                                    :
                                                    <div className="inner-loader-wrapper h-100">
                                                        <div className="inner-loader-content">
                                                            <i className="fad fa-sync-alt fa-spin"></i>
                                                        </div>
                                                    </div>
                                                }
                                            </div>
                                        </div> :
                                        <div className="tab-pane fade show active">
                                            <div className="plan-outer-wrapper">
                                                {this.state.isFetchingBillingDetails ?
                                                    <div className="inner-loader-wrapper h-100">
                                                        <div className="inner-loader-content">
                                                            <i className="fad fa-sync-alt fa-spin"></i>
                                                        </div>
                                                    </div> :
                                                    <ListingTable
                                                        columns={this.getColumns()}
                                                        data={this.state.billingDetails}
                                                    />
                                                }
                                            </div>
                                        </div>
                                    }
                                </div>


                                {/*<h6 className="package-attribute-header">Package Attributes</h6>*/}
                                
                                {/*{!this.state.isFetchingMyPlanDetails && !this.state.isFetchingAllPlanDetails ?
                                    <ul className="list-style-none d-flex package-attribute-list">
                                        {this.state.allAvailablePlans.map((el, index) =>
                                            <React.Fragment key={index}>
                                                {el.featureList.map(feature =>
                                                    <li className="flex-25" key={feature.id}>
                                                        <div className="package-attribute-box">
                                                            <span><i className={feature.count === 0 ? "fad fa-times-circle text-red" : "fad fa-check-circle text-green"}></i></span>
                                                            <div className="package-attribute-count">{feature.count == "3 Month(s)" ? <React.Fragment>3<small>months</small></React.Fragment>: feature.count}</div>
                                                            <h6 className="text-gray fw-600 f-12">{this.getDisplayText(feature)}</h6>
                                                            {feature.showDescription ?
                                                                <p className="text-content f-11 m-0">{feature.description}</p>:
                                                                <p className="text-content f-11 m-0">N/A</p>
                                                            }
                                                        </div>
                                                    </li>

                                                )}
                                            </React.Fragment>
                                        )}
                                    </ul>
                                    :
                                    <div className="profile-loader">
                                        <i className="fad fa-sync-alt fa-spin"></i>
                                    </div>
                                }*/}
                            </div>
                        </div>
                    </div>
                }
                
                {/*{this.state.isFetching ? (!this.state.upgradeSuccessModal && !this.state.purchaseSuccessModal && <Loader />) :
                    <div className="content-body" id="makerProfile">
                        <div className="d-flex">
                            <div className="flex-80 pd-r-7">
                                <div className="profile-box">
                                    <div className="profile-info">
                                        <button className="btn-transparent btn-transparent-blue"><i className="far fa-pencil"></i></button>
                                        <div className="profile-image">
                                            <img src="https://content.iot83.com/m83/misc/icons/userProfile.png" />
                                        </div>
                                        <div className="profile-details">
                                            <h6>Welcome, {this.state.profileData.name}</h6>
                                            <p><i className="fad fa-envelope mr-r-5"></i> {this.state.profileData.email}</p>
                                            <p><i className="fad fa-phone-alt mr-r-5"></i> {this.state.profileData.phone}</p>
                                        </div>
                                    </div>
                                    <div className="profile-tabs">
                                        <ul className="nav nav-tabs list-style-none d-flex">
                                            <li className={`nav-item flex-30  ${this.state.activeTab == "subscriptionPlans" ? "active" : null}`} onClick={() => this.changeTabHandler("subscriptionPlans")}>
                                                <a>Subscriptions and Plans Renewal <img src="https://content.iot83.com/m83/account/subscription.png" /></a>
                                            </li>
                                            <li className={`nav-item flex-30 ${this.state.activeTab == "billingDetails" ? "active" : null}`} onClick={() => this.changeTabHandler("billingDetails")}>
                                                <a>Payments and Billing Information <img src="https://content.iot83.com/m83/account/bill.png" /></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div className="flex-20 pd-l-7">
                                <div className="current-plan">
                                    <div className="current-plan-icon">
                                        <img src="https://content.iot83.com/m83/account/currentPlan.svg" />
                                    </div>
                                    <div className="current-plan-detail">
                                        <h5>{this.state.activePlan.name}</h5>
                                        <h3><strong>${this.state.activePlan.subscriptionPrice}</strong><small>USD</small></h3>
                                        { this.state.activePlan.isPaymentPending ?
                                            <p>Validity details will be updated once the payment is processed.</p> :
                                                this.state.isPlanExpired ? <p>Expired On: {new Date(this.state.activePlan.packageExpiry).toLocaleString('en-US', { hour12: false })}</p> :
                                                <p> Expiring On: {this.state.activePlan.packageExpiry && new Date(this.state.activePlan.packageExpiry).toLocaleString('en-US', { hour12: false })}</p>
                                        }
                                    </div>
                                    <button className="btn btn-danger" disabled={!this.state.activePlan.cancelEnable} onClick={() => { this.setState({ confirmCancelSubscription: true }) }}>Cancel Subscription</button>
                                </div>
                            </div>
                        </div>

                        <div className="tab-content">
                            {this.state.activeTab == "subscriptionPlans" ?
                                <div className="tab-pane fade show active" id="tab1">
                                    {!this.state.isFetchingMyPlanDetails && !this.state.isFetchingAllPlanDetails ?
                                        <ul className="list-style-none d-flex plan-outer-wrapper">
                                            {this.state.allAvailablePlans.map((el, index) =>
                                                <li className="flex-20" key={index}>
                                                    <div className="plan-view">
                                                        <div className="plan-view-header">
                                                            <div className="plan-view-icon">
                                                                <img src={el.name == "TRIAL" ? "https://content.iot83.com/m83/account/free.png" : `https://content.iot83.com/m83/account/${el.name.toLowerCase()}.png`} />
                                                            </div>
                                                            <h6 className={`text-${el.name == "SILVER" ? "primary" : el.name == "BRONZE" ? "indigo" : el.name == "GOLD" ? "orange" : el.name == "PLATINUM" ? "green" : ""}`}>{el.name}</h6>
                                                            <h5>${el.subscriptionPrice} USD<small> / {el.numberOfDays === 30 ? 'Mo' : `${el.numberOfDays} Days`}</small></h5>
                                                        </div>
                                                        <div className="plan-view-body">
                                                            <h6 className="text-gray mb-4 text-center f-12 fw-600">What do you get ?</h6>
                                                            {el.featureList.map(feature =>
                                                                <p key={feature.id}>
                                                                    <i className={feature.count === 0 ? "fal fa-times text-red" : "far fa-check"}></i>
                                                                    <strong>{feature.count}</strong>
                                                                    {this.getDisplayText(feature)}
                                                                    {feature.showDescription &&
                                                                    <span className="plan-view-body-info" data-tooltip data-tooltip-text={feature.description} data-tooltip-place="right" >
                                                                                <i className="fad fa-info-circle text-theme"></i>
                                                                            </span>
                                                                    }
                                                                </p>
                        
                                                            )}
                                                        </div>
                                                        {el.renewPlan ?
                                                            <button className="btn btn-success" onClick={() => { this.setState({ buyPlanConfirmation: true, selectedPackageId: el.id }) }}>Buy</button>
                                                            :
                                                            el.enable && <button className={`btn btn-${el.name == "SILVER" ? "primary" : el.name == "BRONZE" ? "info" : el.name == "GOLD" ? "warning" : el.name == "PLATINUM" ? "success" : ""}`} onClick={() => { this.setState({ upgradePlanConfirmation: true, selectedPackageId: el.id }) }} disabled={!el.upgradePlan}>Upgrade</button>
                                                        }
                                                        <div className="plan-view-overlay"></div>
                                                    </div>
                                                </li>
                                            )}
                                        </ul>
                                        :
                                        <div className="profile-loader">
                                            <i className="fad fa-sync-alt fa-spin"></i>
                                        </div>
                                    }
                                </div>
                                :
                                this.state.activeTab == "billingDetails" ?
                                    <div className="tab-pane fade show active" id="tab2">
                                        {!this.state.isFetchingBillingDetails ?
                                            <ul className="list-style-none payment-list">
                                                {this.state.billingDetails && this.state.billingDetails.map((el, index) =>
                                                    <li key={index}>
                                                        <div className="payment-list-icon">
                                                            <img src={el.planName == "TRIAL" ? "https://content.iot83.com/m83/account/free.png" : `https://content.iot83.com/m83/account/${el.planName.toLowerCase()}.png`} />
                                                        </div>
                                                        <h6 className="text-gray f-13"><span className="text-green">{el.type}</span></h6>
                                                        <p>{getBillingTime(el.txnDate)} <span className="mr-r-10 mr-l-10">|</span>Transaction Id : {el.txnId ? el.txnId : 'N/A'}  </p>
                                                        <div className="payment-list-amount">
                                                            <h3>${el.amount}</h3>
                                                            {el.status == "PENDING" && <h5 className="m-0 text-orange f-15">Payment Pending..</h5>}
                                                            <button className="btn btn-primary" disabled={el.invoiceUrl === null} onClick={() => { window.open(el.invoiceUrl, '_blank') }}>Invoice</button>
                                                        </div>
                                                    </li>
                                                )}
                                            </ul>
                                            :
                                            <div className="profile-loader">
                                                <i className="fad fa-sync-alt fa-spin"></i>
                                            </div>
                                        }
                                    </div>
                                    :

                                    <div className="tab-pane fade show active" id="tab3">
                                    
                                    </div>
                            }
                        </div>
                    </div>
                }*/}

                {/* edit info modal */}
                <div className="modal fade animated slideInDown" id="editInfo" role="dialog">
                    <div className="modal-dialog modal-dialog-centered">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h6 className="modal-title">Edit Profile Info
                                    <button type="button" className="close" data-dismiss="modal"><i className="far fa-times"></i></button>
                                </h6>
                            </div>
                            <form>
                                <div className="modal-body">
                                    <div className="form-group">
                                        <div className="fileUploadBox">
                                            {/*<div className="fileUploadLoader">*/}
                                            {/*    <img src="https://content.iot83.com/m83/misc/uploading.gif" />*/}
                                            {/*    <h6>Uploading...</h6>*/}
                                            {/*</div>*/}
                                            {/*<div className="fileUploadPreview">*/}
                                            {/*    <img src="" />*/}
                                            {/*    <div className="fileUploadOverlay">*/}
                                            {/*        <input type="file" name="logoImageUrl" />*/}
                                            {/*        <h6><i className="far fa-pen" />Update</h6>*/}
                                            {/*    </div>*/}
                                            {/*</div>*/}
                                            <div className="fileUploadForm">
                                                <input type="file" name="profileImageUrl" />
                                                <p>Click to upload file...!</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <input type="text" id="FirstName" className="form-control" placeholder="First Name" />
                                        <label className="form-group-label">First Name :<span className="requiredMark"><i className="fa fa-asterisk" /></span></label>
                                    </div>
                                    <div className="form-group">
                                        <input type="text" id="LastName" className="form-control" placeholder="Last Name" />
                                        <label className="form-group-label">Last Name :<span className="requiredMark"><i className="fa fa-asterisk" /></span></label>
                                    </div>
                                    <div className="form-group">
                                        <input type="text" name="mobile" className="form-control" placeholder="Mobile" maxLength="10" />
                                        <label className="form-group-label">Mobile No :<span className="requiredMark"><i className="fa fa-asterisk" /></span></label>
                                    </div>
                                    <div className="form-group m-0">
                                        <textarea type="text" id="address" className="form-control" placeholder="Address" />
                                        <label className="form-group-label">Address :</label>
                                    </div>
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-success" data-dismiss="modal">Update</button>
                                    <button type="button" className="btn btn-dark">Cancel</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                {/* end edit info modal */}

                {/* cancel subscription modal */}
                {this.state.confirmCancelSubscription &&
                    <div className="modal animated slideInDown show d-block">
                        <div className="modal-dialog modal-dialog-centered">
                            <div className="modal-content">
                                <div className="modal-body">
                                    <div className="delete-content">
                                        <div className="delete-icon">
                                            <i className="fad fa-tags text-orange"></i>
                                        </div>
                                        <h4>Cancel Subscription!</h4>
                                        <h6>Are you sure you want to cancel this subscription ?</h6>
                                    </div>
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-dark" onClick={() => { this.setState({ confirmCancelSubscription: false }) }}>No</button>
                                    <button type="button" className="btn btn-success" onClick={() => { this.setState({ confirmCancelSubscription: false, isFetching: true }, () => { this.props.cancelSubscription("Please cancel my subscription") }) }}>Yes</button>
                                </div>
                            </div>
                        </div>
                    </div>
                }
                {this.state.cancelSubscriptionSuccess &&
                    <div className="modal animated slideInDown show d-block">
                        <div className="modal-dialog modal-dialog-centered">
                            <div className="modal-content">
                                <div className="modal-body">
                                    <div className="delete-content">
                                        <div className="delete-icon">
                                            <i className="fad fa-tags text-orange"></i>
                                        </div>
                                        <h4>Cancel Subscription!</h4>
                                        <p>Your cancel subscription request has been completed successfully !</p>
                                    </div>
                                </div>
                                <div className="modal-footer justify-content-center">
                                    <button type="button" className="btn btn-success" onClick={() => { this.handleCancelSuccessModalClose() }}>OK</button>
                                </div>
                            </div>
                        </div>
                    </div>
                }
                {/* end cancel subscription modal */}

                {/* upgrade subscription modal */}
                {this.state.upgradePlanConfirmation &&
                    <div className="modal animated slideInDown show d-block">
                        <div className="modal-dialog modal-dialog-centered">
                            <div className="modal-content">
                                <div className="modal-body">
                                    <div className="delete-content">
                                        <div className="delete-icon">
                                            <i className="fad fa-tags text-green"></i>
                                        </div>
                                        <h4 className="text-green">Upgrade Subscription!</h4>
                                        <h6>Are you sure you want to upgrade your subscription ?</h6>
                                    </div>
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-dark" onClick={() => { this.setState({ upgradePlanConfirmation: false }) }}>No</button>
                                    <button type="button" className="btn btn-success" onClick={() => { this.handleUpgrade() }}>Yes</button>
                                </div>
                            </div>
                        </div>
                    </div>
                }
                {/* end upgrade subscription modal */}

                {/* upgrade subscription modal */}
                {this.state.buyPlanConfirmation &&
                    <div className="modal animated slideInDown show d-block">
                        <div className="modal-dialog modal-dialog-centered">
                            <div className="modal-content">
                                <div className="modal-body">
                                    <div className="delete-content">
                                        <div className="delete-icon">
                                            <i className="fad fa-tags text-green"></i>
                                        </div>
                                        <h4 className="text-green">Purchase Subscription!</h4>
                                        <h6>Are you sure you want to purchase this subscription ?</h6>
                                    </div>
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-dark" onClick={() => { this.setState({ buyPlanConfirmation: false }) }}>No</button>
                                    <button type="button" className="btn btn-success" onClick={() => { this.handlePurchaseSubscription() }}>Yes</button>
                                </div>
                            </div>
                        </div>
                    </div>
                }
                {/* end upgrade subscription modal */}

                {/* upgraded plan modal */}
                {this.state.upgradeSuccessModal &&
                    <div className="modal animated slideInDown show d-block">
                        <div className="modal-dialog">
                            <div className="modal-content">
                                <div className="modal-body">
                                    <div className="delete-content">
                                        <div className="delete-icon">
                                            <i className="fad fa-thumbs-up text-green"></i>
                                        </div>
                                        <h4 className="text-green">Congratulations !</h4>
                                        <h6>Your subscription plan has been upgraded successfully !</h6>
                                    </div>
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-success" onClick={() => { this.handlePaymentSuccessModalClose() }}>OK</button>
                                </div>
                            </div>
                        </div>
                    </div>
                }
                {/* upgraded plan modal */}

                {/* Renew plan modal */}
                {this.state.purchaseSuccessModal &&
                    <div className="modal animated slideInDown show d-block">
                        <div className="modal-dialog">
                            <div className="modal-content">
                                <div className="modal-body">
                                    <div className="delete-content">
                                        <div className="delete-icon">
                                            <i className="fad fa-thumbs-up text-green"></i>
                                        </div>
                                        <h4 className="text-green">Congratulations !</h4>
                                        <h6>Your purchase is successful !</h6>
                                    </div>
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-success" onClick={() => { this.handlePaymentSuccessModalClose() }}>OK</button>
                                </div>
                            </div>
                        </div>
                    </div>
                }
                {/* upgraded plan modal */}

                {this.state.isOpen &&
                    <NotificationModal
                        type={this.state.type}
                        message2={this.state.message2}
                        onCloseHandler={this.closeHandler}
                    />
                }
            </React.Fragment >
        );
    }
}

MyAccount.propTypes = {
    dispatch: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
    getProfileDetailsSuccess: SELECTORS.getProfileDetailsSuccess(),
    getProfileDetailsFailure: SELECTORS.getProfileDetailsFailure(),
    getSubscriptionDetailsSuccess: SELECTORS.getSubscriptionDetailsSuccess(),
    getSubscriptionDetailsFailure: SELECTORS.getSubscriptionDetailsFailure(),
    getBillingDetailsSuccess: SELECTORS.getBillingDetailsSuccess(),
    getBillingDetailsFailure: SELECTORS.getBillingDetailsFailure(),
    getProfileAnalysisSuccess: SELECTORS.getProfileAnalysisSuccess(),
    getProfileAnalysisFailure: SELECTORS.getProfileAnalysisFailure(),
    getAllAvailablePlansSuccess: SELECTORS.getAllAvailablePlansSuccess(),
    getAllAvailablePlansFailure: SELECTORS.getAllAvailablePlansFailure(),
    cancelSubscriptionSuccess: SELECTORS.cancelSubscriptionSuccess(),
    cancelSubscriptionFailure: SELECTORS.cancelSubscriptionFailure(),
    upgradeSubscriptionSuccess: SELECTORS.upgradeSubscriptionSuccess(),
    upgradeSubscriptionFailure: SELECTORS.upgradeSubscriptionFailure(),
    renewSubscriptionSuccess: SELECTORS.renewSubscriptionSuccess(),
    renewSubscriptionFailure: SELECTORS.renewSubscriptionFailure(),
});

function mapDispatchToProps(dispatch) {
    return {
        dispatch,
        getProfileData: () => dispatch(ACTIONS.getProfileData()),
        getSubscriptionPlans: () => dispatch(ACTIONS.getSubscriptionPlans()),
        getBillingDetails: () => dispatch(ACTIONS.getBillingDetails()),
        getProfileAnalysis: () => dispatch(ACTIONS.getProfileAnalysis()),
        getAvailablePlans: () => dispatch(ACTIONS.getAvailablePlans()),
        cancelSubscription: (reason) => dispatch(ACTIONS.cancelSubscription(reason)),
        upgradeSubscription: (packageId, isTrialPackage) => dispatch(ACTIONS.upgradeSubscription(packageId, isTrialPackage)),
        renewSubscription: (packageId) => dispatch(ACTIONS.renewSubscription(packageId)),
    };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "myProfile", reducer });
const withSaga = injectSaga({ key: "myProfile", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(MyAccount);
