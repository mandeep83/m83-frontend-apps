/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import * as SELECTORS from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import * as ACTIONS from './actions'
import { Helmet } from "react-helmet";
import * as MQTT from "mqtt";
import ReactSelect from "react-select";
import Loader from "../../components/Loader";

let client;
/* eslint-disable react/prefer-stateless-function */
export class ManagePodLogs extends React.Component {
    state = {
        podsList: [],
        pod: '',
        selectedTopic: null,
        duration: 100,
        lines: 1000,
        messages: [],
        isFetching: true
    }


    componentDidMount() {
        this.props.getPods();
    }

    listRef = React.createRef()


    static getDerivedStateFromProps(nextProps, prevState){
        if(nextProps.podsList){
            return ({
                podsList: nextProps.podsList.pods,
                isFetching: false,
                pod: nextProps.podsList.pods[0]
            })
        }
        return null
    }

    componentDidUpdate(preProps, preState, snapshot) {
        if(this.props.podsList || this.props.podMetaData){
            if(this.props.podsList){
                this.props.getPodLogs(this.props.podsList.pods[0], this.state.duration, this.state.lines)
            }
            else if(this.props.podMetaData){
                this.getMqttConnection(this.props.podMetaData.topic);
            }
            this.props.resetToInitialState()
        }
        if (snapshot !== null) {
            const list = this.listRef.current;
            if(list)
                list.scrollTop = list.scrollHeight - snapshot;
        }
    }

    getMqttConnection = (topic) => {
        let brokerURL = window.location.origin === "http://localhost:3001" ? window.API_URL.split("//")[1].substring(0, window.API_URL.split("//")[1].length - 1) : window.location.host;
        let count = 0;
        let self = this;
        const options = {
            host: brokerURL,
            protocol: 'wss',
            path: '/connectmqtt',
            username: '83incs',
            password: 'Hello@123',
        };

        if (topic !== this.state.selectedTopic) {
            if (typeof client !== 'undefined') {
                client.end();
            }
            client = MQTT.connect(options);
            client.on('connect', function () {
            });

            client.on('close', function (response) {
                if (response && count > 4) {
                    client.end();
                }
            });
            client.on('reconnect', function () {
                count++;
            });
            client.subscribe(topic, function (err) {
                if (!err) {
                    client.on('message', function (topic, message) {
                        let messages = [...self.state.messages]
                        messages.push(message.toString())
                        self.setState({
                            messages
                        })
                    })
                }
            })
        }
        this.setState({
            selectedTopic: topic,
        });
    };

    getSnapshotBeforeUpdate(prevProps, preState) {
        if(preState.messages.length < this.state.messages.length) {
            const list = this.listRef.current;
            return list.scrollHeight - list.scrollTop;
        }
        return null;
    }

    optionsForServicesMenu = () => {
        let podsList = JSON.parse(JSON.stringify(this.state.podsList)),
            selectedPod;
        podsList = podsList.map(pod => {
            return {
                value: pod,
                label: pod
            }
        })
        selectedPod = podsList[0]
        this.state.selectedPod ?
            null : this.setState({
                selectedPod
            })
        return podsList;
    }

    handleChangeForSelect = (value) => {
        let pod = value.value
        this.setState({
            pod,
            messages: [],
            selectedPod: value
        }, () => this.props.getPodLogs(pod, this.state.duration, this.state.lines))
    }

    optionsForLogCount = () => {
        let count = ["100", "1000", "10000"],
            selectedLinesCount;
        count = count.map(count => {
            return {
                value: count,
                label: `${count} lines`
            }
        })
        selectedLinesCount = count[1]
        this.state.selectedLinesCount ?
            null : this.setState({
                selectedLinesCount
            })
        return count
    }

    handleChangeForLogCount = (value) => {
        let lines = value.value
        this.setState({
            lines,
            messages: [],
            selectedLinesCount: value
        }, () => this.props.getPodLogs(this.state.pod, this.state.duration, lines))
    }

    render() {
        return (
            <div>
                <Helmet>
                    <title>Logs Manager</title>
                    <meta name="description" content="Description of Logging" />
                </Helmet>

                {this.state.isFetching ? <Loader /> :
                    <React.Fragment>
                        <div className="pageBreadcrumb">
                            <div className="row">
                                <div className="col-8">
                                    <p>
                                        <span>Logs Manager</span>
                                    </p>
                                </div>
                                <div className="col-4 text-right">
                                    <div className="flex h-100 justify-content-end align-items-center"></div>
                                </div>
                            </div>
                        </div>

                        <div className="outerBox pb-0">
                            <div className="flex">
                                <div className="flex-item fx-b75 pd-5">
                                    <div className="form-group mr-b-5">
                                        <ReactSelect
                                            className="form-control-multi-select"
                                            isMulti={false}
                                            options={this.optionsForServicesMenu()}
                                            onChange={this.handleChangeForSelect}
                                            value={this.state.selectedPod}
                                        />
                                    </div>
                                </div>
                                <div className="flex-item fx-b25 pd-5">
                                    <div className="form-group mr-b-5">
                                        <ReactSelect
                                            className="form-control-multi-select"
                                            isMulti={false}
                                            options={this.optionsForLogCount()}
                                            onChange={this.handleChangeForLogCount}
                                            value={this.state.selectedLinesCount}
                                        />
                                    </div>
                                </div>
                            </div>

                            <div className="logBox" id="list" ref={this.listRef}>
                                {this.state.messages.length > 0 ?
                                    this.state.messages.map((temp, index) =>
                                        <p key={index}>{temp.toString()}</p>
                                    ) :
                                    <div className="noLogBox">
                                        <h5>There is no data to display !</h5>
                                    </div>
                                }
                            </div>

                        </div>
                    </React.Fragment>
                }

            </div>
        );
    }
}

ManagePodLogs.propTypes = {
    dispatch: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
    podsList: SELECTORS.getPodsList(),
    podMetaData: SELECTORS.getPodsLogs()
});

function mapDispatchToProps(dispatch) {
    return {
        dispatch,
        getPods: () => dispatch(ACTIONS.getPods()),
        getPodLogs: (pod, duration, lines) => dispatch(ACTIONS.getPodLogs(pod, duration, lines)),
        resetToInitialState: () => dispatch(ACTIONS.resetToInitialState()),
    };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "managePodLogs", reducer });
const withSaga = injectSaga({ key: "managePodLogs", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(ManagePodLogs);
