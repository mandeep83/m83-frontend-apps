/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

export const RESET_TO_INITIAL_STATE = "app/ManagePodLogs/RESET_TO_INITIAL_STATE";


export const GET_PODS = "app/ManagePodLogs/GET_PODS";
export const GET_PODS_SUCCESS = "app/ManagePodLogs/GET_PODS_SUCCESS";
export const GET_PODS_FAILURE = "app/ManagePodLogs/GET_PODS_FAILURE";
export const GET_LOGS = "app/ManagePodLogs/GET_LOGS";
export const GET_LOGS_SUCCESS = "app/ManagePodLogs/GET_LOGS_SUCCESS";
export const GET_LOGS_FAILURE = "app/ManagePodLogs/GET_LOGS_FAILURE";
