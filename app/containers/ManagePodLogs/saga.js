/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { take,takeEvery, call, put, select } from 'redux-saga/effects';
import axios from 'axios';
import * as CONSTANTS from './constants';
 import {apiCallHandler} from "../../api";

export function* getPodsApiAsyncHandler(action) {
    yield [apiCallHandler(action,CONSTANTS.GET_PODS_SUCCESS,CONSTANTS.GET_PODS_FAILURE,'getPods')]
}
export function* getLogsApiAsyncHandler(action) {
    yield [apiCallHandler(action,CONSTANTS.GET_LOGS_SUCCESS,CONSTANTS.GET_LOGS_FAILURE,"getPodLogs")]
}
export function* watchGetPodsRequest() {
    yield takeEvery(CONSTANTS.GET_PODS,getPodsApiAsyncHandler)
}
export function* watchGetLogsRequest() {
    yield takeEvery(CONSTANTS.GET_LOGS,getLogsApiAsyncHandler)
}
export default function* defaultSaga() {
    yield [watchGetPodsRequest(),watchGetLogsRequest()]
}
