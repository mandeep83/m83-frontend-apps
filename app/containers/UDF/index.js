/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { FormattedMessage } from "react-intl";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import NotificationModal from '../../components/NotificationModal/Loadable'
import ConfirmModel from "../../components/ConfirmModel";
import Loader from '../../components/Loader/Loadable';
import ReactTooltip from "react-tooltip";
import ReactTable from 'react-table';
import InfiniteScroll from 'react-infinite-scroll-component'
import {
    getEtlFunctionsSuccess,
    getEtlFunctionsFailure,
    handle_delete_success,
    handle_delete_failure,
    getIsFetching,
    getCommitHistorySuccess,
    getCommitHistoryFailure,
    getInvitationStatusSuccess,
    getInvitationStatusFailure
} from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
// import messages from "./messages";
import { getEtlFunctions, handleDelete, getCommitHistory, getInvitationStatus } from "./actions";
import { getTimeDifference, isUsedForEnabled, getInitials } from '../../commonUtils';
import ListingTable from "../../components/ListingTable/Loadable";
import NoDataFoundMessage from "../../components/NoDataFoundMessage/Loadable"
import AddNewButton from "../../components/AddNewButton/Loadable"
import QuotaHeader from "../../components/QuotaHeader/Loadable";
import Skeleton from "react-loading-skeleton";

/* eslint-disable react/prefer-stateless-function */
export class UDFs extends React.Component {
    state = {
        tableState: {
            page: this.props.location.state && this.props.location.state.page ? this.props.location.state.page : 0,
            pageSize: this.props.location.state && this.props.location.state.pageSize ? this.props.location.state.pageSize : 10,
            functionNameFilter: this.props.location.state && this.props.location.state.functionNameFilter ? this.props.location.state.functionNameFilter : "",
            createdByFilter: this.props.location.state && this.props.location.state.createdByFilter ? this.props.location.state.createdByFilter : "",
            updatedByFilter: this.props.location.state && this.props.location.state.updatedByFilter ? this.props.location.state.updatedByFilter : "",
        },
        valueToBeDeleted: '',
        isOpen: false,
        toggleView:false,
        status: '',
        deleteName: '',
        toBeDeletedId: '',
        isFetching: true,
        commitHistory: [],
        hasMoreCommitHistory: true,
        commitOffset: 1,
        isFetchingCommitHistory: false,
        isStatusChecked: false,
        searchTerm: "",
        filterEtlFunctions: [],
        etlFunctions: [],
        interpreterOptions: [
            {
                label: 'Python',
                value: 'python',
                isDisabled: false,
            },
            {
                label: 'Node.js',
                value: 'node.js',
                isDisabled: false,
            },
            {
                label: 'Javascript',
                value: 'javascript',
                isDisabled: false,
            }
        ],
    }

    qoutaCallback = () => {
        window.history.pushState(null, '');
        this.props.getEtlFunctions();
        if (this.isProjectSourceControlled) {
            this.props.getInvitationStatus(localStorage.getItem("selectedProjectId"))
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.getEtlFunctionsSuccess && nextProps.getEtlFunctionsSuccess !== this.props.getEtlFunctionsSuccess) {
            this.setState({
                etlFunctions: nextProps.getEtlFunctionsSuccess,
                filterEtlFunctions: nextProps.getEtlFunctionsSuccess,
                isFetching: false
            })
        }
        if (nextProps.getEtlFunctionsFailure && nextProps.getEtlFunctionsFailure !== this.props.getEtlFunctionsFailure) {
            this.setState({
                type: "error",
                isOpen: true,
                message2: nextProps.allEltListFailure,
            })
        }

        if (nextProps.handle_delete_success != this.props.handle_delete_success) {
            this.setState((previousState) => ({
                message2: nextProps.handle_delete_success.message,
                type: 'success',
                isOpen: true,
                isFetching: false,
                isDeletedSuccess: !previousState.isDeletedSuccess
            }))
        }

        if (nextProps.handle_delete_failure && nextProps.handle_delete_failure !== this.props.handle_delete_failure) {
            this.setState({
                type: "error",
                isOpen: true,
                message2: nextProps.handle_delete_failure,
                isFetching: false
            })
        }
        if (nextProps.commitHistorySuccess && nextProps.commitHistorySuccess !== this.props.commitHistorySuccess) {
            let commitHistory = JSON.parse(JSON.stringify(this.state.commitHistory));
            nextProps.commitHistorySuccess.map((item) => {
                let itemDate = (new Date(item.commitedOn)).toLocaleDateString()
                let index = commitHistory.find((obj) => obj.date === itemDate);
                if (!index) {
                    let newObj = {
                        date: new Date(item.commitedOn).toLocaleString('en-US', { timeZone: localStorage.timeZone }),
                        subCommits: [item]
                    }
                    commitHistory.push(newObj)
                }
                else {
                    index.subCommits.push(item);
                }

            })
            this.setState({
                commitHistory,
                isFetchingCommitHistory: false,
                hasMoreCommitHistory: !(nextProps.commitHistorySuccess.length < 10)
            })
        }

        if (nextProps.commitHistoryFailure && nextProps.commitHistoryFailure !== this.props.commitHistoryFailure) {
            this.setState({
                commitHistory: []
            })
        }

        if (nextProps.invitationStatusSuccess && nextProps.invitationStatusSuccess !== this.props.invitationStatusSuccess) {
            this.setState({
                invitationStatus: nextProps.invitationStatusSuccess.verifiedUser,
                isStatusChecked: true
            })
        }

        if (nextProps.invitationStatusFailure && nextProps.invitationStatusFailure !== this.props.invitationStatusFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.invitationStatusFailure,
            })
        }
    }

    onCloseHandler = () => {
        this.setState({ isOpen: false })
    }


    deleteMethod = (id) => {
        this.setState({ confirmState: true, toBeDeletedId: id });
    }

    confirmBox = (collectionNameForAction, action) => {
        let status = action ? action : ""
        this.setState({
            collectionNameForAction,
            status,
            confirmState: true,
        })
    }

    cancelClicked = () => {
        this.setState({
            deleteName: "",
            confirmState: false
        })
    }

    filterCaseInsensitive =
        (filter, row) => {
            const id = filter.pivotId || filter.id;
            return (
                row[id] !== undefined ?
                    String(row[id].toLowerCase()).startsWith(filter.value.toLowerCase())
                    :
                    true
            );
        }

    isProjectSourceControlled = () => {
        let selectedProjectId = localStorage.getItem("selectedProjectId");
        let projectList = JSON.parse(localStorage.getItem("myProjects"));
        return projectList.some((project) => project.id === selectedProjectId && project.repoName)
    }

    isUserPermissionSourceControlled = () => {
        let selectedProjectId = localStorage.getItem("selectedProjectId");
        let projectList = JSON.parse(localStorage.getItem("myProjects"));
        return projectList.some((project) => project.id === selectedProjectId && project.sourceControlEnabled)
    }

    hasEditOrDeleteAccess = (isAuthor, action) => {
        let isAccess = !(this.isProjectSourceControlled() && isUsedForEnabled("js")) ? isAuthor : isAuthor && (this.isUserPermissionSourceControlled() && localStorage.isGitHubLogin == "true");
        let selectedProjectId = localStorage.getItem("selectedProjectId");
        let projectList = JSON.parse(localStorage.getItem("myProjects"));
        let isOwner = action === "delete" ? projectList.some((project) => project.id === selectedProjectId && project.isOwner) : true;
        return ((isOwner && ((this.isProjectSourceControlled() && localStorage.isGitHubLogin == "true") || !this.isProjectSourceControlled())) || isAccess);
    }

    showCommitHistory = (fileName, selectedDescription) => {
        const category = window.location.pathname.split('/')[2];
        this.setState({
            selectedDescription,
            isFetchingCommitHistory: true,
            commitHistory: [],
            commitFileName: fileName,
            commitOffset: 1
        }, () => this.props.getCommitHistory("jsFunctions", category, fileName, "js", this.state.commitOffset))

    }

    nextFetchHistory = () => {
        const category = window.location.pathname.split('/')[2];
        this.setState({ commitOffset: this.state.commitOffset + 1 },
            () => this.props.getCommitHistory("jsFunctions", category, this.state.commitFileName, "js", this.state.commitOffset))
    }

    navigateToAddOrEditJsFunction = (id) => {
        let pathname = '/addOrEditUdf',
            state = JSON.parse(JSON.stringify(this.state.tableState))
        if (typeof (id) == "string")
            pathname += `/${id}`;
        this.props.history.push({ pathname, state })
    }

    handlePageOrSizeChange = (key, value) => {
        let tableState = JSON.parse(JSON.stringify(this.state.tableState));
        tableState[key] = value;
        this.setState({ tableState })
    }

    searchEtlFunctionByName = (value) => {
        let filterEtlFunctions = JSON.parse(JSON.stringify(this.state.etlFunctions))
        filterEtlFunctions = filterEtlFunctions.filter(item => item.functionName.toLowerCase().includes(value.toLowerCase()))
        this.setState({ searchTerm: value, filterEtlFunctions })
    }

    emptySearchFieldOnCrossButton = () => {
        if (this.state.searchTerm.length > 0) {
            let filterEtlFunctions = JSON.parse(JSON.stringify(this.state.etlFunctions))
            this.setState({ filterEtlFunctions, searchTerm: "" })
        }
    }

    getColumns = () => {
        return [
            { Header: "Name", width: 35, accessor: "functionName", filterable: true, sortable: true,
                cell: (row) => (
                    <React.Fragment>
                        <div className="list-view-icon">
                            <i className="fad fa-function text-dark-theme"></i>
                        </div>
                        <div className="list-view-icon-box">
                            <h6 className="text-theme fw-600">{row.functionName}</h6>
                            <p>{`${row.description || "-"}`}</p>
                        </div>
                    </React.Fragment>
                ),
            },
            {
                Header: "Interpreter", width: 20, accessor: "type", filterable: true, sortable: true,
                cell: (row) => (
                    <span className="badge badge-pill alert-success list-view-pill w-50">{this.state.interpreterOptions.filter(interpreter => interpreter.value === row.type)[0].label}</span>
                ),
                filter: ({ value, onChangeHandler, accessor }) =>
                    <select
                        className="form-control"
                        onChange={event => onChangeHandler(event, accessor)}
                        value={value ? value : 'all'}
                    >
                        <option value="">ALL</option>
                        {this.state.interpreterOptions.map((list, index) => <option key={index} value={list.value}>{list.label}</option>)}
                    </select>
            },
            { Header: "Created", width: 15, accessor: "createdAt", sortable: true, filterable: true },
            { Header: "Updated", width: 15, accessor: "updatedAt", sortable: true, filterable: true },
            {
                Header: "Actions", width: 15, cell: (row) => (
                    <div className="button-group">
                        <button className="btn-transparent btn-transparent-blue" data-tooltip data-tooltip-text="Edit" data-tooltip-place="bottom"
                            onClick={() => this.navigateToAddOrEditJsFunction(row.id)}>
                            <i className="far fa-pencil"></i>
                        </button>
                        <button className="btn-transparent btn-transparent-red" data-tooltip data-tooltip-text="Delete" data-tooltip-place="bottom"
                            onClick={() => { this.deleteMethod(row.id) }}>
                            <i className="far fa-trash-alt"></i>
                        </button>
                    </div>
                )
            },
        ]
    }

    refreshComponent = () => {
        this.setState({
            isFetching: true
        }, () => this.props.getEtlFunctions())
    }

    toggleView = () => {
        this.setState((prevState) => ({ toggleView: !prevState.toggleView }))
    }

    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>UDFs</title>
                    <meta name="description" content="M83-UDFFunctions" />
                </Helmet>

                <QuotaHeader
                    showQuota
                    addButtonText="Add UDF"
                    componentName="UDFs"
                    productName="udf"
                    onAddClick={this.navigateToAddOrEditJsFunction}
                    // searchBoxDetails={{
                    //     value: this.state.searchTerm,
                    //     onChangeHandler: this.searchEtlFunctionByName,
                    //     isDisabled: !this.state.etlFunctions.length || this.state.isFetching
                    // }}
                    refreshDisabled={this.state.isFetching}
                    addDisabled={this.state.isFetching}
                    refreshHandler={this.refreshComponent}
                    callBack={this.qoutaCallback}
                    toggleButton={this.toggleView}
                    isDeletedSuccess={this.state.isDeletedSuccess}
                    {...this.props}
                />

                <div className="content-body">
                    {this.state.isFetching ?
                        <Skeleton count={9} className="skeleton-list-loader" /> :
                        <React.Fragment>
                            {this.state.etlFunctions.length > 0 ?
                                !this.state.toggleView ?
                                    <ListingTable
                                        data={this.state.filterEtlFunctions}
                                        columns={this.getColumns()}
                                        isLoading={this.state.isFetching}
                                    />
                                    :
                                    <ul className="card-view-list">
                                        {this.state.etlFunctions.map((item, index) =>
                                            <li key={index}>
                                                <div className="card-view-box">
                                                    <div className="card-view-header">
                                                    <span className="alert alert-success">Interpreter - {this.state.interpreterOptions.filter(interpreter => interpreter.value === item.type)[0].label}</span>
                                                        <div className="dropdown">
                                                            <button className="btn-transparent btn-transparent-gray" type="button" data-toggle="dropdown">
                                                                <i className="fas fa-ellipsis-v"></i>
                                                            </button>
                                                            <div className="dropdown-menu">
                                                                <button className="dropdown-item"
                                                                    onClick={() => this.navigateToAddOrEditJsFunction(item.id)}>
                                                                    <i className="far fa-pencil"></i>Edit
                                                                </button>
                                                                <button className="dropdown-item"
                                                                    onClick={() => { this.deleteMethod(item.id) }}>
                                                                    <i className="far fa-trash-alt"></i>Delete
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="card-view-body">
                                                        <div className="card-view-icon">
                                                            <i className="fad fa-function"></i>
                                                        </div>
                                                        <h6><i className="far fa-address-book"></i>{item.name ? item.name : "-"}</h6>
                                                        <p><i className="far fa-file-alt"></i>{item.description ? item.description : "-"}</p>
                                                    </div>
                                                    <div className="card-view-footer d-flex">
                                                        <div className="flex-50 p-3">
                                                            <h6>Created</h6>
                                                            <p><strong>By - </strong>{item.createdBy}</p>
                                                            <p><strong>At - </strong>{new Date(item.createdAt).toLocaleString('en-US', { createdAt: localStorage.createdAt })}</p>
                                                        </div>
                                                        <div className="flex-50 p-3">
                                                            <h6>Updated</h6>
                                                            <p><strong>By - </strong>{item.updatedBy}</p>
                                                            <p><strong>At - </strong>{new Date(item.updatedAt).toLocaleString('en-US', { createdAt: localStorage.createdAt })}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        )}
                                    </ul>
                                :
                                <AddNewButton
                                    text1="No user defined function(s) available"
                                    text2="You haven't created any UDFs yet. Please create a UDF first."
                                    createItemOnAddButtonClick={this.navigateToAddOrEditJsFunction}
                                    imageIcon="addUDF.png"
                                />
                            }
                        </React.Fragment>
                    }
                </div>

                {/* validity modal */}
                {(this.state.isStatusChecked && this.isProjectSourceControlled() && isUsedForEnabled("js") && localStorage.getItem('isGitHubLogin') == "true" && this.isUserPermissionSourceControlled() && !this.state.invitationStatus) &&
                    <div className="modal d-block animated slideInDown">
                        <div className="modal-dialog modal-dialog-centered">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h6 className="modal-title">Permission Denied </h6>
                                </div>
                                {this.props.isFetching ?
                                    <div className="validityBoxLoader">
                                        <div className="formLoaderBox">
                                            <i className="fad fa-sync-alt fa-spin"></i>
                                        </div>
                                    </div> :
                                    <React.Fragment>
                                        <div className="modal-body">
                                            <div className="validityBox">
                                                <div className="validityBoxImage">
                                                    <img src='https://image.flaticon.com/icons/svg/1167/1167224.svg' />
                                                </div>
                                                <h5>Kindly accept the invitation from Github before making any changes</h5>
                                            </div>
                                        </div>
                                        <div className="modal-footer">
                                            <button type="button" className="btn btn-success" onClick={() => { this.props.history.push('/projects') }}>OK</button>
                                        </div>
                                    </React.Fragment>
                                }
                            </div>
                        </div>
                    </div>
                }
                {/* end validity modal */}

                {/* git history modal start */}
                <div className="modal fade animated slideInDown" role="dialog" id="gitHistoryModal">
                    <div className="modal-dialog modal-lg modal-dialog-centered">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h6 className="modal-title">Function - {this.state.commitFileName}
                                    <button type="button" className="close" data-dismiss="modal"><i className="far fa-times"></i></button>
                                </h6>
                            </div>
                            <div className="modal-body">
                                <div className="timelineBox">
                                    {this.state.isFetchingCommitHistory ?
                                        <div className="formLoaderBox">
                                            <div className="loader"></div>
                                        </div>
                                        :
                                        <InfiniteScroll
                                            dataLength={this.state.commitHistory.length}
                                            next={this.nextFetchHistory}
                                            hasMore={this.state.hasMoreCommitHistory}
                                            loader={<h4>Loading...</h4>}
                                            endMessage={
                                                <p className="text-center">You have seen all the Commit History</p>
                                            }
                                            height={450}
                                        >
                                            <ul className="listStyleNone">
                                                {this.state.commitHistory.map((item, index) => {
                                                    return (
                                                        <li key={index}>
                                                            <span className="timelineIcon"><i className="fad fa-code-commit"></i></span>
                                                            <p>Commit on {item.date}</p>

                                                            {item.subCommits.map((commits, i) =>
                                                                <div key={index + " i " + i} className="timelineList">
                                                                    <div className="alert alert-primary"><a href={commits.commitUrl} target="blank"><i className="fad fa-code mr-r-5"></i>{commits.sha.substring(0, 7)}</a></div>
                                                                    <h5>{commits.commitMessage}</h5>
                                                                    <h6>
                                                                        <div className="initialsBox d-inline-block"><span className="initialsImage">{getInitials(commits.committedBy)}</span><p className="text-lowercase">{commits.committedBy}</p></div>
                                                                        <span className="mr-r-10 mr-l-10">|</span>
                                                                        <span className="text-cyan"> committed {getTimeDifference(commits.commitedOn)}</span>
                                                                    </h6>
                                                                </div>
                                                            )}

                                                        </li>
                                                    )
                                                })
                                                }
                                            </ul>

                                        </InfiniteScroll>
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/* end git history modal */}

                {
                    this.state.isOpen &&
                    <NotificationModal
                        type={this.state.type}
                        message2={this.state.message2}
                        onCloseHandler={this.onCloseHandler}
                    />
                }

                {
                    this.state.confirmState &&
                    <ConfirmModel
                        status={"delete"}
                        deleteName={this.state.functionName}
                        confirmClicked={() => {
                            this.props.handleDelete(this.state.toBeDeletedId)
                            this.setState({
                                isFetching: true,
                                confirmState: false,
                                isOpen: false
                            })
                        }}
                        cancelClicked={() => { this.cancelClicked() }}
                    />
                }
            </React.Fragment >
        );
    }
}

UDFs.propTypes = {
    dispatch: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
    getEtlFunctionsSuccess: getEtlFunctionsSuccess(),
    getEtlFunctionsFailure: getEtlFunctionsFailure(),
    handle_delete_success: handle_delete_success(),
    handle_delete_failure: handle_delete_failure(),
    isFetching: getIsFetching(),
    commitHistorySuccess: getCommitHistorySuccess(),
    commitHistoryFailure: getCommitHistoryFailure(),
    invitationStatusSuccess: getInvitationStatusSuccess(),
    invitationStatusFailure: getInvitationStatusFailure()
});

function mapDispatchToProps(dispatch) {
    return {
        dispatch,
        getEtlFunctions: () => dispatch(getEtlFunctions()),
        handleDelete: (id) => dispatch(handleDelete(id)),
        getCommitHistory: (usedFor, category, fileName, fileExt, offset) => dispatch(getCommitHistory(usedFor, category, fileName, fileExt, offset)),
        getInvitationStatus: (id) => dispatch(getInvitationStatus(id))
    };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "udfs", reducer });
const withSaga = injectSaga({ key: "udfs", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(UDFs);
