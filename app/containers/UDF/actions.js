/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { DEFAULT_ACTION,GET_ETL_FUNCTION ,HANDLE_DELETE, GET_COMMIT_HISTORY, GET_INVITATION_STATUS} from "./constants";

export function defaultAction() {
  return {
    type: DEFAULT_ACTION
  };
}

export function getEtlFunctions() {
  
  return {
    type: GET_ETL_FUNCTION
  };
}

export function handleDelete(id) {
  return {
    type: HANDLE_DELETE,
    id:id
  };
}

export function getCommitHistory(usedFor,category,fileName,fileExt,offset) {
  return {
    type: GET_COMMIT_HISTORY,
    usedFor,
    category,
    fileName,
    fileExt,
    offset
  }
}
export function getInvitationStatus(id){
  return {
    type: GET_INVITATION_STATUS,
    id,
  }
}

