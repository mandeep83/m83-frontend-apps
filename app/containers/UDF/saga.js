/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { takeLatest } from 'redux-saga/effects';
import { apiCallHandler } from '../../api';

import {
  GET_ETL_FUNCTION,
  GET_ETL_FUNCTION_SUCCESS,
  GET_ETL_FUNCTION_FAILURE,
  HANDLE_DELETE_SUCCESS,
  HANDLE_DELETE_FAILURE,
  HANDLE_DELETE,
  GET_COMMIT_HISTORY,
  GET_COMMIT_HISTORY_SUCCESS,
  GET_COMMIT_HISTORY_FAILURE, 
  GET_INVITATION_STATUS, GET_INVITATION_STATUS_SUCCESS, GET_INVITATION_STATUS_FAILURE 
} from './constants';


export function* apigetEtlFunctionsHandler(action) {
  yield [apiCallHandler(action, GET_ETL_FUNCTION_SUCCESS, GET_ETL_FUNCTION_FAILURE, 'getEtlFunctionsUrl')];
}

export function* apiDeleteApiHandler(action) {
  yield [apiCallHandler(action, HANDLE_DELETE_SUCCESS, HANDLE_DELETE_FAILURE, 'deleteJsFunction')];
}

export function* getCommitHistoryAsyncHandler(action) {
  yield [apiCallHandler(action, GET_COMMIT_HISTORY_SUCCESS, GET_COMMIT_HISTORY_FAILURE, 'getCommitHistory')];
}

export function* getInvitationStatus(action) {
  yield [apiCallHandler(action, GET_INVITATION_STATUS_SUCCESS, GET_INVITATION_STATUS_FAILURE, 'getInvitationStatus')]
}

export function* watchergetEtlFunctions() {
 
  yield takeLatest(GET_ETL_FUNCTION, apigetEtlFunctionsHandler);
}

export function* watcherDeleteApiList() {
  yield takeLatest(HANDLE_DELETE, apiDeleteApiHandler);
}

export function* watcherGetCommitHistory() {
  yield takeLatest(GET_COMMIT_HISTORY, getCommitHistoryAsyncHandler);
}

export function* watcherGetInvitationStatus() {
  yield takeLatest(GET_INVITATION_STATUS,  getInvitationStatus);
}

export default function* rootSaga() {
  yield [
    watchergetEtlFunctions(), 
    watcherDeleteApiList(), 
    watcherGetCommitHistory(),
    watcherGetInvitationStatus()
  ];
}

