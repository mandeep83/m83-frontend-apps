/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

export const DEFAULT_ACTION = "app/UDFs/DEFAULT_ACTION";

export const GET_ETL_FUNCTION = "app/UDFs/GET_ETL_FUNCTION"
export const GET_ETL_FUNCTION_SUCCESS = "app/UDFs/GET_ETL_FUNCTION_SUCCESS"
export const GET_ETL_FUNCTION_FAILURE = "app/UDFs/GET_ETL_FUNCTION_FAILURE"

export const HANDLE_DELETE = "app/UDFs/HANDLE_DELETE"
export const HANDLE_DELETE_SUCCESS = "app/UDFs/HANDLE_DELETE_SUCCESS"
export const HANDLE_DELETE_FAILURE = "app/UDFs/HANDLE_DELETE_FAILURE"

export const GET_COMMIT_HISTORY = "app/UDFs/GET_COMMIT_HISTORY";
export const GET_COMMIT_HISTORY_SUCCESS = "app/UDFs/GET_COMMIT_HISTORY_SUCCESS";
export const GET_COMMIT_HISTORY_FAILURE = "app/UDFs/GET_COMMIT_HISTORY_FAILURE";

export const GET_INVITATION_STATUS = "app/UDFs/GET_INVITATION_STATUS";
export const GET_INVITATION_STATUS_SUCCESS = "app/UDFs/GET_INVITATION_STATUS_SUCCESS";
export const GET_INVITATION_STATUS_FAILURE = "app/UDFs/GET_INVITATION_STATUS_FAILURE";
