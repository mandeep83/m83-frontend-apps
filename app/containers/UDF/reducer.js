/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { fromJS } from "immutable";
import {
  GET_ETL_FUNCTION_SUCCESS, GET_ETL_FUNCTION_FAILURE, 
  HANDLE_DELETE_SUCCESS, HANDLE_DELETE_FAILURE, 
  GET_COMMIT_HISTORY_SUCCESS, GET_COMMIT_HISTORY_FAILURE,
  GET_INVITATION_STATUS_SUCCESS, GET_INVITATION_STATUS_FAILURE   
} from "./constants";

export const initialState = fromJS({});

function etlJsFunctionsReducer(state = initialState, action) {
  switch (action.type) {
    case GET_ETL_FUNCTION_SUCCESS:
      return Object.assign({}, state, {
        'getEtlSuccess': action.response
      });
      
    case GET_ETL_FUNCTION_FAILURE:
      return Object.assign({}, state, {
        'getEtlFailure': action.response
      });
       
    case HANDLE_DELETE_SUCCESS:
        let temp = Object.assign({}, state, {
          'handle_delete_success': action.response
        })
        temp.getEtlSuccess = temp.getEtlSuccess.filter((listObject) =>{
          if(listObject.id != action.response.id){
            return listObject;
          }})
        return temp;
    case HANDLE_DELETE_FAILURE:
          return Object.assign({}, state, {
            'handle_delete_failure': true
          });
    case GET_COMMIT_HISTORY_SUCCESS:
            return Object.assign({}, state, {
              'commitHistorySuccess': action.response,
            })
    case GET_COMMIT_HISTORY_FAILURE:
            return Object.assign({}, state, {
              'commitHistoryFailure': action.error,
            })   
            case GET_INVITATION_STATUS_SUCCESS:
              return Object.assign({}, state, {
                  'InvitationStatusSuccess': action.response
              });
      
            case GET_INVITATION_STATUS_FAILURE:
              return Object.assign({}, state, {
                  'InvitationStatusFailure': action.error
              });  
    default:
      return state;
  }
}

export default etlJsFunctionsReducer;
