/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { createSelector } from "reselect";
import { initialState } from "./reducer";

const selectEtlJsFunctionsDomain = state =>
  state.get("udfs", initialState);


  export const getEtlFunctionsSuccess = () =>
  createSelector(selectEtlJsFunctionsDomain, substate => substate.getEtlSuccess);
  
  export const getEtlFunctionsFailure = () =>
  createSelector(selectEtlJsFunctionsDomain, substate => substate.getEtlFailure);

  export const handle_delete_success = () => 
  createSelector(selectEtlJsFunctionsDomain, handleDeleteState => handleDeleteState.handle_delete_success);

  export const handle_delete_failure = () => 
  createSelector(selectEtlJsFunctionsDomain, handleDeleteState => handleDeleteState.handle_delete_failure);

  export const getCommitHistorySuccess = () => createSelector(selectEtlJsFunctionsDomain, subState => subState.commitHistorySuccess);
  export const getCommitHistoryFailure = () => createSelector(selectEtlJsFunctionsDomain, subState => subState.commitHistoryFailure);


  export const isFetchingState = state => state.get('loader');

  export const getIsFetching = () =>
  createSelector(isFetchingState, substate => substate.get('isFetching'));
export { selectEtlJsFunctionsDomain };


export const getInvitationStatusSuccess = () => createSelector(selectEtlJsFunctionsDomain, substate => substate.InvitationStatusSuccess);
export const getInvitationStatusFailure = () => createSelector(selectEtlJsFunctionsDomain, substate => substate.InvitationStatusFailure);