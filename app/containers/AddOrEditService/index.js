/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * AddOrEditService
 *
 */

import React from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {Helmet} from "react-helmet";
import {FormattedMessage} from "react-intl";
import {createStructuredSelector} from "reselect";
import {compose} from "redux";

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import makeSelectAddOrEditService from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import messages from "./messages";
import ImageUploader from "../../components/ImageUploader";

/* eslint-disable react/prefer-stateless-function */
export class AddOrEditService extends React.Component {
    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>AddOrEditService</title>
                    <meta name="description" content="Description of AddOrEditService"/>
                </Helmet>
    
                <header className="content-header d-flex">
                    <div className="flex-60">
                        <div className="d-flex">
                            <h6 className="previous" onClick={()=> {this.props.history.push('/manageServices')}}>Manage Services</h6>
                            <h6 className="active">Add New</h6>
                        </div>
                    </div>
                    <div className="flex-40 text-right">
                        <div className="content-header-group">
                            <button className="btn btn-light" data-tooltip data-tooltip-text="Refresh" data-tooltip-place="bottom" onClick={()=> {this.props.history.push('/manageServices')}}>
                                <i className="fas fa-angle-left"></i>
                            </button>
                        </div>
                    </div>
                </header>
    
                <div className="content-body">
                    <div className="form-content-wrapper">
                        <div className="form-content-box">
                            <div className="form-content-header">
                                <p>Basic Information</p>
                            </div>
                            <div className="form-content-body">
                                <div className="d-flex">
                                    <div className="flex-50 pd-r-10">
                                        <div className="form-group">
                                            <label className="form-group-label">Service Name : <i className="fas fa-asterisk form-group-required"></i></label>
                                            <input type="text" className="form-control" id="name" />
                                        </div>
                                    </div>
                                    <div className="flex-50 pd-l-10">
                                        <div className="form-group">
                                            <label className="form-group-label">Service URL : <i className="fas fa-asterisk form-group-required"></i></label>
                                            <input type="text" className="form-control" id="url" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
    
                        <div className="form-content-box">
                            <div className="form-content-header">
                                <p>Service Runtime Information</p>
                            </div>
                            <div className="form-content-body">
                                <div className="d-flex">
                                    <div className="flex-50 pd-r-10">
                                        <div className="form-group">
                                            <label className="form-group-label">Replica Count : <i className="fas fa-asterisk form-group-required"></i></label>
                                            <input type="text" className="form-control" id="name" />
                                        </div>
                                    </div>
                                    <div className="flex-50 pd-l-10">
                                        <div className="form-group">
                                            <label className="form-group-label">Port : <i className="fas fa-asterisk form-group-required"></i></label>
                                            <input type="text" className="form-control" id="port" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
    
                        <div className="form-content-box">
                            <div className="form-content-header">
                                <p>Disk Allocation Information</p>
                            </div>
                            <div className="form-content-body">
                                <div className="d-flex">
                                    <div className="flex-33 pd-r-10">
                                        <div className="form-group">
                                            <label className="form-group-label">Mount Path : <i className="fas fa-asterisk form-group-required"></i></label>
                                            <input type="text" className="form-control" id="url" />
                                        </div>
                                    </div>
                                    <div className="flex-33 pd-l-10 pd-r-10">
                                        <div className="form-group">
                                            <label className="form-group-label">Size : <i className="fas fa-asterisk form-group-required"></i></label>
                                            <div className="input-group">
                                                <input type="text" className="form-control" id="name" />
                                                <div className="input-group-append">
                                                    <span className="input-group-text">GB</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="flex-33 pd-l-10">
                                        <div className="form-group">
                                            <label className="form-group-label">Persistent ? : <i className="fas fa-asterisk form-group-required"></i></label>
                                            <div className="d-flex pt-1">
                                                <div className="flex-40 pd-r-10">
                                                    <label className="radio-button">
                                                        <span className="radio-button-text">Yes</span>
                                                        <input type="radio" name="format" value="yes" />
                                                        <span className="radio-button-mark"></span>
                                                    </label>
                                                </div>
                                                <div className="flex-40 pd-l-10">
                                                    <label className="radio-button">
                                                        <span className="radio-button-text">No</span>
                                                        <input type="radio" name="format" value="no" />
                                                        <span className="radio-button-mark"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
    
                        <div className="form-content-box">
                            <div className="form-content-header">
                                <p>Source Information</p>
                            </div>
                            <div className="form-content-body">
                                <div className="d-flex align-items-center">
                                    <div className="flex-50 pd-r-10">
                                        <div className="form-group">
                                            <label className="form-group-label">Repo : <i className="fas fa-asterisk form-group-required"></i></label>
                                            <select className="form-control">
                                                <option>Repo 1</option>
                                                <option>Repo 2</option>
                                                <option>Repo 3</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div className="flex-50 pd-l-10">
                                        <div className="form-group">
                                            <label className="form-group-label">Image Tag : <i className="fas fa-asterisk form-group-required"></i></label>
                                            <select className="form-control">
                                                <option>Tag 1</option>
                                                <option>Tag 2</option>
                                                <option>Tag 3</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    
                    <div className="form-info-wrapper">
                        <div className="form-info-body">
                            <div className="form-info-icon"><img src={require('../../assets/images/other/notebook.png')} /></div>
                            <h5>What are Services ?</h5>
                            <ul className="list-style-none form-info-list mb-5">
                                <li><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p></li>
                                <li><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p></li>
                                <li><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p></li>
                                <li><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p></li>
                                <li><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p></li>
                                <li><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p></li>
                            </ul>
                            <div className="form-info-footer">
                                <button type='button' className="btn btn-light" onClick={()=> {this.props.history.push('/manageServices')}}>Cancel</button>
                                <button type='submit' className="btn btn-primary" onClick={()=> {this.props.history.push('/manageServices')}}>Save</button>
                            </div>
                        </div>
                    </div>
                    
                </div>
                
            </React.Fragment>
        );
    }
}

AddOrEditService.propTypes = {
    dispatch: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
    addoreditservice: makeSelectAddOrEditService()
});

function mapDispatchToProps(dispatch) {
    return {
        dispatch
    };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({key: "addOrEditService", reducer});
const withSaga = injectSaga({key: "addOrEditService", saga});

export default compose(
    withReducer,
    withSaga,
    withConnect
)(AddOrEditService);
