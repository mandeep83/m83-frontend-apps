/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/*
 *
 * AdminConfig constants
 *
 */

export const RESET_TO_INITIAL_STATE = "app/AdminConfig/RESET_TO_INITIAL_STATE";

export const GET_ALL_PROPERTIES = {
    action : 'app/AdminConfig/GET_ALL_PROPERTIES',
    success: "app/AdminConfig/GET_ALL_PROPERTIES_SUCCESS",
    failure: "app/AdminConfig/GET_ALL_PROPERTIES_FAILURE",
    urlKey: "getAllProperties",
    successKey: "getAllPropertiesSuccess",
    failureKey: "getAllPropertiesFailure",
    actionName: "getAllProperties",
}

export const CREATE_UPDATE_CONSUL = {
    action : 'app/AdminConfig/CREATE_UPDATE_CONSUL',
    success: "app/AdminConfig/CREATE_UPDATE_CONSUL_SUCCESS",
    failure: "app/AdminConfig/CREATE_UPDATE_CONSUL_FAILURE",
    urlKey: "createOrUpdateConsul",
    successKey: "createOrUpdateConsulSuccess",
    failureKey: "createOrUpdateConsulFailure",
    actionName: "createOrUpdateConsul",
    actionArguments : ["payload", "key"]
}
