/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * AdminConfig
 * 
 */

import React from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {Helmet} from "react-helmet";
import {FormattedMessage} from "react-intl";
import {createStructuredSelector} from "reselect";
import {compose} from "redux";

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import reducer from "./reducer";
import saga from "./saga";
import ReactTooltip from "react-tooltip";
import { getNotificationObj } from "../../commonUtils";
import {allSelectors as SELECTORS} from "./selectors";
import {allActions as ACTIONS} from './actions'
import {cloneDeep, startCase} from "lodash"
import NotificationModal from '../../components/NotificationModal/Loadable'
import Loader from "../../components/Loader";

/* eslint-disable react/prefer-stateless-function */
export class AdminConfig extends React.Component {
    state = {
        allProperties: [],
        activeKey: null,
        activeKeyValue: "",
        isFetching: true
    }

    componentDidMount = ()=> {
        this.props.getAllProperties()
    }

    static getDerivedStateFromProps(nextProps, state) {
        if (nextProps.getAllPropertiesSuccess) {
            return {
                isFetching: false,
                allProperties : nextProps.getAllPropertiesSuccess.response
            } 
        }
        if (nextProps.createOrUpdateConsulSuccess) {
            let { key } = nextProps.createOrUpdateConsulSuccess;
            let propertyType = key.split(".")[0];
            let allProperties = cloneDeep(state.allProperties)
            let propertyIndex = allProperties.findIndex(property => property.categoryName === propertyType);
            let propertyConfigIndex = allProperties[propertyIndex].configs.findIndex(configs => configs.key === key)
            allProperties[propertyIndex].configs[propertyConfigIndex].value = btoa(state.activeKeyValue)
            return {
                allProperties,
                isSavingKey: null,
                activeKey: null,
                activeKeyValue: "",
            } 
        }
        if (nextProps.createOrUpdateConsulFailure) {
            return {
                isSavingKey: null,
                ...getNotificationObj(nextProps.createOrUpdateConsulFailure.error, "error")
            } 
        }
        if (Object.keys(SELECTORS).filter(prop => prop.toLowerCase().includes("failure")).some(prop => nextProps[prop])) {
            let propName = Object.keys(SELECTORS).find(props => nextProps[props]);
            return getNotificationObj(nextProps[propName].error, "error")
        }
        return null
    }

    componentDidUpdate(prevProps, prevState) {
        if (Object.keys(SELECTORS).some(prop => this.props[prop])) {
            this.props.resetToInitialState()
        }
    }

    toggleEditing = (activeKey, activeKeyValue)=> {
        this.setState({
            activeKey,
            activeKeyValue
        })
    }

    createOrUpdateConsul = ()=> {
        let payload = { 
            property : {
                name: this.state.activeKey,
                value: btoa(this.state.activeKeyValue)
            }
        }
        this.setState(prevState => ({
            isSavingKey: prevState.activeKey
        }), () => this.props.createOrUpdateConsul(payload))
    }

    handleChange = ({ currentTarget })=> {
        this.setState({
            activeKeyValue: currentTarget.value
        })
    }

    onCloseHandler = () => {
        this.setState({
            isOpen: false,
            type: '',
            message2: ''
        })
    }

    render() {
        let { allProperties, activeKey, isSavingKey } = this.state
        return (
            <React.Fragment>
                <Helmet>
                    <title>System Properties</title>
                    <meta name="description" content="Description of System Properties"/>
                </Helmet>

                <header className="content-header d-flex">
                    <div className="flex-60">
                        <h6>System Properties
                            <span className="content-header-badge-group">
                                <span className="content-header-badge-item"><span className="badge badge-pill badge-primary">{allProperties.length}</span></span>
                            </span>
                        </h6>
                    </div>
                    <div className="flex-40 text-right">
                        <div className="content-header-group"></div>
                    </div>
                </header>

                 {this.state.isFetching ?
                    <Loader/> :
                     <div className="content-body">
                        {allProperties.map((property, index) =>
                            <div className="collapse-wrapper" key={property.categoryName}>
                                <div className="collapse-header" data-toggle="collapse" data-target={`#item${index}`}>
                                    <h6>{property.categoryDisplayName} <strong className="text-theme">({property.configs.length})</strong>
                                        <i className="fad fa-chevron-double-up collapse-header-toggle collapsed" data-toggle="collapse" data-target={`#item${index}`}></i>
                                    </h6>
                                </div>
                                <div className="collapse" id={`item${index}`}>
                                    <div className="collapse-body">
                                        <div className="content-table">
                                            <table className="table table-bordered mb-0">
                                                <thead>
                                                <tr>
                                                    <th width="25%">Display Name</th>
                                                    <th width="25%">Key</th>
                                                    <th width="35%">Value</th>
                                                    <th width="15%">Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                {property.configs.map( ({name, value, key}) =>  <tr key={key}>
                                                    <td className="text-theme">{name}</td>
                                                    <td>{key}</td>
                                                    <td>
                                                        {activeKey === key ?
                                                            <div className="form-group m-0">
                                                                <input type="text" className="form-control" value={this.state.activeKeyValue} onChange={this.handleChange} />
                                                            </div> :
                                                            <span>{atob(value)}</span>
                                                        }
                                                    </td>
                                                    <td>
                                                        {isSavingKey === key ?
                                                            <div className="">
                                                                <i className="fal fa-sync-alt fa-spin"></i>
                                                            </div> :
                                                            <div className="button-group">
                                                                {activeKey === key ?
                                                                    <React.Fragment>
                                                                        <button className="btn-transparent btn-transparent-green" data-tooltip data-tooltip-text="Save" data-tooltip-place="bottom"  onClick={this.createOrUpdateConsul} data-tip data-for="save">
                                                                            <i className="far fa-check"></i>
                                                                        </button>
                                                                    </React.Fragment>:
                                                                    <React.Fragment>
                                                                        <button className="btn-transparent btn-transparent-blue" data-tooltip data-tooltip-text="Edit" data-tooltip-place="bottom" disabled={isSavingKey} onClick={()=>this.toggleEditing(key, atob(value))} data-tip data-for="edit">
                                                                            <i className="far fa-pencil"></i>
                                                                        </button>
                                                                    </React.Fragment>
                                                                }
                                                                {activeKey === key &&
                                                                    <React.Fragment>
                                                                        <button className="btn-transparent btn-transparent-gray" data-tooltip data-tooltip-text="Cancel" data-tooltip-place="bottom" onClick={()=>this.toggleEditing()} data-tip data-for="cancel">
                                                                            <i className="far fa-times"></i>
                                                                        </button>
                                                                    </React.Fragment>
                                                                }
                                                            </div>
                                                        }
                                                    </td>
                                                </tr>)}
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        )}
                    </div>
                }

                {this.state.isOpen &&
                    <NotificationModal
                        type={this.state.modalType}
                        message2={this.state.message2}
                        onCloseHandler={this.onCloseHandler}
                    />
                }
            </React.Fragment>
        );
    }
}

AdminConfig.propTypes = {
    dispatch: PropTypes.func.isRequired
};

let allSelectors = {}
Object.entries(SELECTORS).map(([key,value]) => {
    allSelectors[key] = value()
})

const mapStateToProps = createStructuredSelector(allSelectors)

function mapDispatchToProps(dispatch) {
    let allActions = {dispatch}
    Object.entries(ACTIONS).map(([key,value]) => {
        allActions[key] = (...args)=> dispatch(value(...args))
    })
    return allActions
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({key: "adminConfig", reducer});
const withSaga = injectSaga({key: "adminConfig", saga});

export default compose(
    withReducer,
    withSaga,
    withConnect
)(AdminConfig);
