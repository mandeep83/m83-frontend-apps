/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import {DEFAULT_ACTION, DELETE_MENU, NAV_LIST, NAV_LIST_SAVE, RESET_TO_INTIAL, SUBMIT_FORM} from './constants';

export function getNavigationList() {
  return {
    type: NAV_LIST,
  };
}

export function navlistSaveHandler(payload) {
  return {
    type: NAV_LIST_SAVE,
    payload
  };
}

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function resetToIntial() {
  return {
    type: RESET_TO_INTIAL,
  };
}

export function updateNav() {
  return {
    type: 'app/HomePage/FETCH_SIDE_NAV_REQUEST'
  };
}


export function onSubmitHandler(payload) {
    return {
        type: SUBMIT_FORM,
        payload : payload
    };
}

export function menuDeleteHandler(id) {
    return {
        type: DELETE_MENU,
        data: id
    };
}

