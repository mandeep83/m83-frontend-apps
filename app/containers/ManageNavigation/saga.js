/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import {
    NAV_LIST, NAV_LIST_ERROR, NAV_LIST_SUCCESS, NAV_LIST_SAVE, NAV_LIST_SAVE_ERROR, NAV_LIST_SAVE_SUCCESS,
    SUBMIT_FORM, SUBMIT_FORM_SUCCESS, SUBMIT_FORM_FAILURE, DELETE_MENU, DELETE_MENU_SUCCESS, DELETE_MENU_FAILURE
} from './constants'
import { takeEvery } from 'redux-saga';
import {apiCallHandler} from '../../api';

export function* getNavigationAttributeData(action) {
  yield[apiCallHandler(action, NAV_LIST_SUCCESS, NAV_LIST_ERROR, 'getMenuAttribute')];
}

export function* saveNavListApi(action) {
  yield[apiCallHandler(action, NAV_LIST_SAVE_SUCCESS, NAV_LIST_SAVE_ERROR, 'saveNavigationAttribute')];
}

export function* watcherGetListRequest() {
  yield takeEvery(NAV_LIST, getNavigationAttributeData);
}

export function* watcherSaveListRequest() {
  yield takeEvery(NAV_LIST_SAVE, saveNavListApi);
}
export function* addOrEditMenuApiHandler(action) {
    yield[apiCallHandler(action, SUBMIT_FORM_SUCCESS, SUBMIT_FORM_FAILURE, 'saveMenuDetails')];
}

export function* watcherAddOrEditMenuRequest() {
    yield takeEvery(SUBMIT_FORM, addOrEditMenuApiHandler);
}
export function* deleteMenuApiHandlerAsync(action) {
    yield[apiCallHandler(action, DELETE_MENU_SUCCESS, DELETE_MENU_FAILURE, 'deleteMenuById')];
}

export function* watcherDeleteMenuRequest() {
    yield takeEvery(DELETE_MENU, deleteMenuApiHandlerAsync);
}

export default function* rootSaga() {
  yield [watcherGetListRequest(), watcherSaveListRequest(), watcherAddOrEditMenuRequest(),watcherDeleteMenuRequest()];
}
