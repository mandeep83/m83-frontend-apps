/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

export const DEFAULT_ACTION = 'app/NavigationAttributes/DEFAULT_ACTION';

export const NAV_LIST = 'app/NavigationAttributes/NAV_LIST';
export const NAV_LIST_ERROR = 'app/NavigationAttributes/NAV_LIST_ERROR';
export const NAV_LIST_SUCCESS = 'app/NavigationAttributes/NAV_LIST_SUCCESS';

export const NAV_LIST_SAVE = 'app/NavigationAttributes/NAV_LIST_SAVE';
export const NAV_LIST_SAVE_ERROR = 'app/NavigationAttributes/NAV_LIST_SAVE_ERROR';
export const NAV_LIST_SAVE_SUCCESS = 'app/NavigationAttributes/NAV_LIST_SAVE_SUCCESS';

export const SUBMIT_FORM = "app/NavigationAttributes/SUBMIT_FORM";
export const SUBMIT_FORM_FAILURE = "app/NavigationAttributes/SUBMIT_FORM_FAILURE";
export const SUBMIT_FORM_SUCCESS = "app/NavigationAttributes/SUBMIT_FORM_SUCCESS";

export const DELETE_MENU = 'app/NavigationAttributes/DELETE_MENU';
export const DELETE_MENU_SUCCESS = 'app/NavigationAttributes/DELETE_MENU_SUCCESS';
export const DELETE_MENU_FAILURE = 'app/NavigationAttributes/DELETE_MENU_FAILURE';

export const RESET_TO_INTIAL = 'app/NavigationAttributes/RESET_TO_INTIAL';
