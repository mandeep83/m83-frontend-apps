/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import {fromJS} from 'immutable';
import {
    DEFAULT_ACTION,
    NAV_LIST_SUCCESS,
    NAV_LIST_ERROR,
    NAV_LIST_SAVE_SUCCESS,
    NAV_LIST_SAVE_ERROR,
    RESET_TO_INTIAL, SUBMIT_FORM_SUCCESS, SUBMIT_FORM_FAILURE, DELETE_MENU_SUCCESS, DELETE_MENU_FAILURE
} from './constants';

export const initialState = fromJS({});

function navigationAttributesReducer(state = initialState, action) {
    switch (action.type) {
        case NAV_LIST_SUCCESS:
        
            return Object.assign({}, state, {
                list: action.response,
                isFetching: false,
            });

        case NAV_LIST_ERROR:
            return Object.assign({}, state, {
                error: action.error,
            });

        case NAV_LIST_SAVE_SUCCESS:
            return Object.assign({}, state, {
                navListSaveSuccess: {time : Math.random(), message: action.response }
            });

        case NAV_LIST_SAVE_ERROR:
            return Object.assign({}, state, {
                navListSaveError: action.error,
            });

        case DEFAULT_ACTION:
            return initialState;

        case RESET_TO_INTIAL:
            return Object.assign({}, state, {
                navListSaveSuccess: '',
            });
        case SUBMIT_FORM_SUCCESS: 
            let temp = Object.assign({}, state)
            temp.submitFormSuccess = Math.random()
            temp.submitFormSuccessMessage = action.response
            return temp;
        case SUBMIT_FORM_FAILURE:
            return Object.assign({}, state, {
                'submitFormFailure': action.error
            });
            
        case DELETE_MENU_SUCCESS: {
            let temp = Object.assign({}, state)
            temp.deleteMenuSuccess = !temp.deleteMenuSuccess
            temp.deleteSuccess = "Menu Item Deleted Successfully."
            temp.list = temp.list.filter((li)=> li.id !== action.response)
            if(Object.keys(temp.list).length === Object.keys(temp.list).length){
                temp.list = temp.list.filter((li)=>{
                    if(li.subMenus.length !== 0){
                        li.subMenus = li.subMenus.filter((childLi) =>childLi.id !== action.response)
                    }
                        return li;                    
                })
            }
            return temp;
        }

        case DELETE_MENU_FAILURE:
            return Object.assign({}, state, {
                deleteError: action.error,
            });

        default:
            return state;
    }
}

export default navigationAttributesReducer;
