/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import {
    getNavigationList, menuDeleteHandler, navlistSaveHandler, onSubmitHandler, resetToIntial,
    updateNav
} from './actions';
import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import {
    getNavList,
    getNavListError,
    getSaveNavListSuccess,
    getSaveNavListError,
    getIsFetching,
    getSubmitFormFailure,
    getSubmitFormSuccess,
    getDeleteSuccess,
    getDeleteError,
    getDeleteMenuSuccess,
    getSubmitFormSuccessMessage
} from './selectors';
import reducer from './reducer';
import saga from './saga';
import Loader from '../../components/Loader/Loadable'
import NotificationModal from '../../components/NotificationModal/Loadable'
import ReactTooltip from 'react-tooltip'
import TagsInput from 'react-tagsinput';
import SlidingPane from 'react-sliding-pane';
import Modal from 'react-modal';

/* eslint-disable react/prefer-stateless-function */
export class ManageNavigation extends React.Component {
    state = {
        menuToBeDeleted: '',
        navList: [],
        openModal: false,
        menuItems: [],
        addNavigationViewFlag: false,
        menuPayload: {
            entities: "",
            fontAwesomeIconClass: "",
            navName: "",
            newTab: false,
            parentId: null,
            position: 0,
            url: "",
            connectedRoutes: []
        }
    }

    componentDidMount() {
        this.props.getNavigationList();
        Modal.setAppElement('body');
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.navList !== this.props.navList) {
            this.setState({
                menuItems: nextProps.navList
            })
        }
        if (nextProps.navListError !== this.props.navListError) {
            this.setState({
                type: "error",
                openModal: true,
                message2: nextProps.navListError
            })
        }

        if (nextProps.navListSaveSuccess !== this.props.navListSaveSuccess) {
            this.setState({
                type: "success",
                openModal: true,
                message2: nextProps.navListSaveSuccess.message
            })
        }

        if (nextProps.navListSaveError !== this.props.navListSaveError) {
            this.setState({
                type: "error",
                openModal: true,
                message2: nextProps.navListSaveError
            })
        }

        if (nextProps.submitFormSuccess !== this.props.submitFormSuccess) {
            this.props.getNavigationList();
            this.setState({
                type: "success",
                message2: nextProps.submitFormSuccessMessage,
                openModal: true,
                addNavigationViewFlag: false
            })
        }
        if (nextProps.submitFormFailure && nextProps.submitFormFailure != this.props.submitFormFailure) {
            this.setState({
                type: 'error',
                openModal: true,
                message2: nextProps.submitFormFailure
            })
        }
        if (nextProps.deleteErrorMessage && nextProps.deleteErrorMessage != this.props.deleteErrorMessage) {
            this.setState({
                type: "error",
                message2: nextProps.deleteErrorMessage,
                openModal: true,
            })
        }
        if (nextProps.deleteMenuSuccess != this.props.deleteMenuSuccess) {
            this.setState({
                type: "success",
                message2: nextProps.deleteMenuSuccess,
                openModal: true,
            })
        }
    }

    parentChildInputChangeHandler = (id) => (event) => {
        let tempMenuList = JSON.parse(JSON.stringify(this.state.menuItems))
        tempMenuList = tempMenuList.map((list) => {
            if (list.id == id) {
                list[event.target.id] = event.target.type === "checkbox" ? event.target.checked : event.target.value
            }
            if (list.subMenus && list.subMenus.length > 0) {
                list.subMenus.map((subItem) => {
                    if (subItem.id == id) {
                        subItem[event.target.id] = event.target.type === "checkbox" ? event.target.checked : event.target.value
                    }
                })
            }
            return (list);
        })
        this.setState({ menuItems: tempMenuList })
    }

    childListDisplay = (menuId) => {
        let menus = JSON.parse(JSON.stringify(this.state.menuItems))
        menus = menus.map((temp) => {
            if (temp.id == menuId) {
                temp.childVisibility = !temp.childVisibility
            }
            return temp;
        })
        this.setState({ menuItems: menus });
    }

    handleUpdateNavigationAttributes() {
        let menuData = JSON.parse(JSON.stringify(this.state.menuItems));
        let finalResult = [];
        menuData = menuData.map((items) => {
            finalResult.push({
                id: items.id,
                navName: items.navName,
                enabled: items.enabled,
                fontAwesomeIconClass: items.fontAwesomeIconClass,
                entities: items.entities,
                newTab: items.newTab,
                position: items.position,
                url: items.url,
                connectedRoutes: items.connectedRoutes
            })
            if (items.subMenus && items.subMenus.length > 0) {
                items.subMenus.map((submen) => {
                    finalResult.push({
                        id: submen.id,
                        navName: submen.navName,
                        entities: submen.entities,
                        fontAwesomeIconClass: submen.fontAwesomeIconClass,
                        enabled: submen.enabled,
                        newTab: submen.newTab,
                        position: submen.position,
                        url: submen.url
                    })
                })
            }
        })
        this.props.navlistSaveHandler(finalResult);
    }

    inputChangeHandler = (event) => {
        let menuPayload = JSON.parse(JSON.stringify(this.state.menuPayload))
        menuPayload[event.target.id] = event.target.type === "checkbox" ? event.target.checked : event.target.value;
        this.setState({
            menuPayload
        })
    }

    onSubmitHandler = () => {
        $('#addChildModal').modal('hide');
        this.props.onSubmit(this.state.menuPayload)
    }

    onCloseHandler = () => {
        this.setState({
            openModal: false,
            message2: ''
        })
    }

    confirmModalHandler = (id) => {
        this.setState({
            menuToBeDeleted: id
        })
        $('#deleteModal').modal('show')
    }

    addChildMenuHandler = (id, url, entities) => {
        if ((url === "" || url == null) && (entities == "" || entities == null)) {
            let menuPayload = JSON.parse(JSON.stringify(this.state.menuPayload))
            menuPayload.parentId = id
            menuPayload.fontAwesomeIconClass = ""
            menuPayload.navName = ""
            menuPayload.newTab = false
            menuPayload.url = ""
            menuPayload.entities = ""
            menuPayload.connectedRoutes = []
            this.setState({
                menuPayload
            })
            $('#addChildModal').modal('show')
        }
    }

    addEditSwitch = () => {
        this.setState({
            addNavigationViewFlag: !this.state.addNavigationViewFlag,
            menuPayload: {
                entities: "",
                fontAwesomeIconClass: "",
                navName: "",
                newTab: false,
                parentId: null,
                position: 0,
                url: "",
                connectedRoutes: []
            }
        })
    }

    getChildHtml(subMenus) {
        let childTemp = subMenus.map((childItem) =>
            <div className="flex navGridBody" key={childItem.id}>
                <div className="fx-b14 navGridBox form-group">
                    <p className="text-info">Child</p>
                </div>
                <div className="fx-b15 navGridBox form-group">
                    <input type="text" className="form-control" id="navName" value={childItem.navName} onChange={this.parentChildInputChangeHandler(childItem.id)} />
                </div>
                <div className="fx-b15 navGridBox form-group">
                    <input type="text" className="form-control" id="entities" value={childItem.entities} onChange={this.parentChildInputChangeHandler(childItem.id)} />
                </div>
                <div className="fx-b15 navGridBox form-group">
                    <input type="text" className="form-control" id="url" value={childItem.url} onChange={this.parentChildInputChangeHandler(childItem.id)}
                    />
                </div>
                <div className="fx-b15 navGridBox form-group">
                    <input type="text" className="form-control" id="fontAwesomeIconClass" value={childItem.fontAwesomeIconClass} onChange={this.parentChildInputChangeHandler(childItem.id)} />
                </div>
                <div className="fx-b8 navGridBox form-group">
                    <label className="checkboxLabel">
                        <input checked={childItem.newTab} type="checkbox" id="newTab" onChange={this.parentChildInputChangeHandler(childItem.id)} />
                        <span className="checkmark"></span>
                    </label>
                </div>
                <div className="fx-b8 navGridBox form-group">
                    <label className="checkboxLabel">
                        <input checked={childItem.enabled} type="checkbox" id="enabled" onChange={this.parentChildInputChangeHandler(childItem.id)} />
                        <span className="checkmark"></span>
                    </label>
                </div>
                <div className="fx-b10 navGridBox form-group">
                    <div className="button-group">
                        <button className="btn btn-transparent btn-transparent-danger" onClick={() => { this.confirmModalHandler(childItem.id) }}>
                            <i className="far fa-trash-alt" />
                        </button>
                    </div>
                </div>
            </div>
        )
        childTemp.unshift(
            <div key="1">
                <div className="flex navGridBody">
                    <div className="fx-b14 navGridBox form-group">Menu Type</div>
                    <div className="fx-b15 navGridBox form-group">Name</div>
                    <div className="fx-b15 navGridBox form-group">Entity</div>
                    <div className="fx-b15 navGridBox form-group">URL</div>
                    <div className="fx-b15 navGridBox form-group">FontAwesome Class</div>
                    <div className="fx-b8 navGridBox form-group">New Tab</div>
                    <div className="fx-b8 navGridBox form-group">Is Enabled</div>
                    <div className="fx-b10 navGridBox form-group">Actions</div>
                </div>
            </div>
        )
        return childTemp;
    }

    render() {
        let temp = [];
        if (this.state.menuItems) {
            temp = this.state.menuItems.map((parentItem) => {
                let parentDropDownHtml = <p className="text-primary noDrag">Parent</p>;
                let parentEntitiesDisabled = false;
                let deleteNavButton = <button disabled={parentItem.adminMenu} data-tip data-for={"delete" + parentItem.id} className={parentItem.adminMenu ? "btn btn-transparent btn-transparent-danger btn-disabled" : "btn btn-transparent btn-transparent-danger"} onClick={() => { this.confirmModalHandler(parentItem.id) }}>
                    <i className="far fa-trash-alt" />
                    <ReactTooltip id={"delete" + parentItem.id} place="bottom" type="dark">
                        <div className="tooltipText"><p>Delete</p></div>
                    </ReactTooltip>
                </button>;
                let childrenListHtml = null;
                if (parentItem.subMenus && parentItem.subMenus.length > 0) {
                    parentDropDownHtml = <p className="text-primary cursor-pointer noDrag" onClick={() => { this.childListDisplay(parentItem.id) }}>Parent
                        <i className={parentItem.childVisibility ? "far fa-angle-up" : "far fa-angle-down"}></i>
                    </p>;
                    parentEntitiesDisabled = true;
                    deleteNavButton = null
                    childrenListHtml = <div className={parentItem.childVisibility ? "navChildGrid collapse show childLayout_" + parentItem.id : "d-none childLayout_" + parentItem.id}>
                        {this.getChildHtml(parentItem.subMenus)}
                    </div>
                }
                return (
                    <div id={"id" + parentItem.id} key={parentItem.id}>
                        <div className="flex navGridBody">
                            <div className="fx-b10 navGridBox form-group">
                                {parentDropDownHtml}
                            </div>
                            <div className="fx-b10 navGridBox form-group">
                                <input
                                    type="text" className="form-control text-ellipsis noDrag" id="navName"
                                    value={parentItem.navName} onChange={this.parentChildInputChangeHandler(parentItem.id)}
                                />
                            </div>
                            <div className="fx-b10 navGridBox form-group">
                                <input type="text" className="form-control text-ellipsis noDrag" id="entities"
                                    value={parentItem.entities ? parentItem.entities : ''} disabled={parentEntitiesDisabled}
                                    onChange={this.parentChildInputChangeHandler(parentItem.id)}
                                />
                            </div>
                            <div className="fx-b15 navGridBox form-group">
                                <input
                                    type="text" className="form-control text-ellipsis noDrag" id="url"
                                    value={parentItem.url ? parentItem.url : ''} disabled={parentEntitiesDisabled}
                                    onChange={this.parentChildInputChangeHandler(parentItem.id)}
                                />
                            </div>

                            <div className="fx-b15 navGridBox form-group">
                                <TagsInput value={parentItem.connectedRoutes} onChange={(value) => {
                                    let menuItems = JSON.parse(JSON.stringify(this.state.menuItems))
                                    menuItems = menuItems.map((list) => {
                                        if (list.id == parentItem.id) {
                                            list.connectedRoutes = value
                                        }
                                        return (list);
                                    })
                                    this.setState({ menuItems })
                                }} placeholder="Connected Routes :" />
                            </div>
                            <div className="fx-b12 navGridBox form-group">
                                <input
                                    type="text" className="form-control text-ellipsis noDrag" id="fontAwesomeIconClass"
                                    value={parentItem.fontAwesomeIconClass ? parentItem.fontAwesomeIconClass : ''} disabled={parentEntitiesDisabled}
                                    onChange={this.parentChildInputChangeHandler(parentItem.id)}
                                />
                            </div>
                            <div className="fx-b8 navGridBox form-group">
                                <label className="checkboxLabel">
                                    <input
                                        type="checkbox" id="newTab"
                                        onChange={this.parentChildInputChangeHandler(parentItem.id)} checked={parentItem.newTab}
                                    />
                                    <span className="checkmark"></span>
                                </label>
                            </div>
                            <div className="fx-b8 navGridBox form-group">
                                <label className="checkboxLabel">
                                    <input
                                        type="checkbox" id="enabled"
                                        onChange={this.parentChildInputChangeHandler(parentItem.id)} checked={parentItem.enabled}
                                    />
                                    <span className="checkmark"></span>
                                </label>
                            </div>
                            <div className="fx-b10 navGridBox form-group">
                                <div className="button-group">
                                    <button
                                        disabled={parentItem.adminMenu}
                                        className={parentItem.adminMenu ? "btn btn-transparent btn-transparent-success btn-disabled" : "btn btn-transparent btn-transparent-success"}
                                        onClick={() => this.addChildMenuHandler(parentItem.id, parentItem.url, parentItem.entities)}
                                        data-tip data-for={parentItem.id + "navTooltip"}
                                    >
                                        <i className="far fa-plus" />
                                    </button>
                                    {!((parentItem.url == null || parentItem.url == "") && (parentItem.entities == null || parentItem.entities == "")) &&
                                        <ReactTooltip id={parentItem.id + "navTooltip"} place="bottom" type="dark">
                                            <div className="tooltipText">
                                                <p>Before creating child elements, please remove the parent URL and Entity.</p>
                                            </div>
                                        </ReactTooltip>
                                    }
                                    {deleteNavButton}
                                </div>
                            </div>
                        </div>
                        {childrenListHtml}
                    </div>
                )
            })
            temp.unshift(
                <div id={1} key={1}>
                    <div className="flex navGridBody">
                        <div className="fx-b10 navGridBox form-group">Menu Type</div>
                        <div className="fx-b10 navGridBox form-group">Name</div>
                        <div className="fx-b10 navGridBox form-group">Entity</div>
                        <div className="fx-b15 navGridBox form-group">URL</div>
                        <div className="fx-b15 navGridBox form-group">Connected Routes</div>
                        <div className="fx-b12 navGridBox form-group">FontAwesome Class</div>
                        <div className="fx-b8 navGridBox form-group">New Tab</div>
                        <div className="fx-b8 navGridBox form-group">Is Enabled</div>
                        <div className="fx-b10 navGridBox form-group">Actions</div>
                    </div>
                </div>
            )
        }

        return (
            <React.Fragment>
                <Helmet>
                    <title>Manage Navigation</title>
                    <meta name="description" content="M83-ManageNavigation" />
                </Helmet>

                <div className="pageBreadcrumb">
                    <div className="row">
                        <div className="col-8">
                            <p><span>Manage Navigation</span></p>
                        </div>

                        <div className="col-4 text-right">
                            <div className="flex h-100 justify-content-end align-items-center">
                                {!this.state.addNavigationViewFlag &&
                                    <button className="btn btn-primary" onClick={this.addEditSwitch}>
                                        <span className="btn-primary-icon"><i className="far fa-plus" /></span>
                                    </button>
                                }
                            </div>
                        </div>
                    </div>
                </div>

                {this.props.isFetching ?
                    <Loader /> :
                    <div className="outerBox">
                        <div className="contentTableBox">
                            {temp}
                            <div className="bottomButton text-right">
                                <button type="button" name="button" className="btn btn-success" onClick={this.handleUpdateNavigationAttributes.bind(this)}>Update</button>
                            </div>
                        </div>

                        <SlidingPane
                            className=''
                            overlayClassName='slidingFormOverlay'
                            closeIcon={<div></div>}
                            isOpen={this.state.addNavigationViewFlag}
                            from='right'
                            width='550px'
                        >
                            {this.state.addNavigationViewFlag &&
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <h6 className="modal-title">Add New Navigation
                                        <button className="close" onClick={() => { this.setState({ addNavigationViewFlag: false }) }}><i className="far fa-angle-right"></i></button>
                                        </h6>
                                    </div>

                                    <div className="modal-body">
                                        <div className="form-group">
                                            <input type="text" id="navName" placeholder="Name" autoFocus
                                                className="form-control" value={this.state.menuPayload.navName}
                                                onChange={this.inputChangeHandler}
                                                required
                                            />
                                            <label className="form-group-label">Name :<span className="requiredMark"><i className="fa fa-asterisk" /></span></label>
                                        </div>

                                        <div className="form-group">
                                            <input type="text" id="entities" placeholder="Entity"
                                                className="form-control" value={this.state.menuPayload.entities}
                                                onChange={this.inputChangeHandler}
                                            />
                                            <label className="form-group-label">Entity :<span className="requiredMark"><i className="fa fa-asterisk" /></span></label>
                                        </div>

                                        <div className="form-group">
                                            <input type="text" id="url" placeholder="URL"
                                                className="form-control" value={this.state.menuPayload.url}
                                                onChange={this.inputChangeHandler}
                                            />
                                            <label className="form-group-label">URL :<span className="requiredMark"><i className="fa fa-asterisk" /></span></label>
                                        </div>

                                        <div className="form-group">
                                            <TagsInput value={this.state.menuPayload.connectedRoutes} onChange={(value) => {
                                                let menuPayload = JSON.parse(JSON.stringify(this.state.menuPayload))
                                                menuPayload.connectedRoutes = value;
                                                this.setState({
                                                    menuPayload
                                                })
                                            }} placeholder="Connected Routes :" />
                                        </div>

                                        <div className="form-group">
                                            <input type="text" id="fontAwesomeIconClass" placeholder="Font Class"
                                                className="form-control" value={this.state.menuPayload.fontAwesomeIconClass}
                                                onChange={this.inputChangeHandler}
                                            />
                                            <label className="form-group-label">Font Awesome Icon Class :<span className="requiredMark"><i className="fa fa-asterisk" /></span></label>
                                        </div>

                                        <div className="form-group">
                                            <label className="checkboxLabel">
                                                <span className="checkText">Do you want to open the navigation in new tab ?</span>
                                                <input type="checkbox" id="newTab"
                                                    onChange={this.inputChangeHandler}
                                                    checked={this.state.menuPayload.newTab}
                                                />
                                                <span className="checkmark"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div className="modal-footer">
                                        <button className="btn btn-success" disabled={this.state.menuPayload.fontAwesomeIconClass.length === 0 || this.state.menuPayload.entities.length === 0 || this.state.menuPayload.url.length === 0 || this.state.menuPayload.navName.length === 0} className={(this.state.menuPayload.fontAwesomeIconClass.length === 0 || this.state.menuPayload.entities.length === 0 || this.state.menuPayload.url.length === 0 || this.state.menuPayload.navName.length === 0 ? "btn-disabled " : "") + "btn btn-success"} onClick={() => this.onSubmitHandler()}>Save</button>
                                        <button type="button" className="btn btn-dark"  onClick={() => { this.setState({ addNavigationViewFlag: false }) }}>Cancel</button>
                                    </div>
                                </div>
                            }
                        </SlidingPane>
                    </div>
                }

                {/* delete modal */}
                <div className="modal show animated slideInDown deleteModal" id="deleteModal">
                    <div className="modal-dialog modal-dialog-centered">
                        <div className="modal-content">
                            <div className="modal-body">
                                <div className="deleteModalContent">
                                    <div className="deleteModalImage">
                                        <i className="fad fa-trash-alt text-red"></i>
                                    </div>
                                    <h4>Delete!</h4>
                                    <p>Are you sure you want to delete ?</p>
                                </div>
                            </div>
                            <div className="modal-footer justify-content-center">
                                <button type="button" className="btn btn-success" data-dismiss="modal" onClick={() => { this.props.menuDeleteHandler(this.state.menuToBeDeleted) }}>Yes</button>
                                <button type="button" className="btn btn-light" data-dismiss="modal" onClick={() => { this.setState({ menuToBeDeleted: '' }) }}>No</button>
                            </div>
                        </div>
                    </div>
                </div>
                {/* end delete modal */}

                {/* add child modal */}
                <div className="modal animated slideInRight" role="dialog" id="addChildModal">
                    <div className="modal-dialog modal-form">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title">Add Child Navigation
                                    <button type="button" className="close" data-dismiss="modal"><i className="far fa-times"></i></button>
                                </h5>
                            </div>
                            <div className="modal-body">
                                <div className="contentForm">
                                    <div className="form-group">
                                        <input type="text" id="navName" className="form-control"
                                            value={this.state.menuPayload.navName} onChange={this.inputChangeHandler}
                                        />
                                        <label className="form-group-label">Name :<span className="requiredMark"><i className="fa fa-asterisk" /></span></label>
                                    </div>

                                    <div className="form-group">
                                        <input type="text" id="entities" className="form-control"
                                            value={this.state.menuPayload.entities} onChange={this.inputChangeHandler}
                                        />
                                        <label className="form-group-label">Entity :<span className="requiredMark"><i className="fa fa-asterisk" /></span></label>
                                    </div>

                                    <div className="form-group">
                                        <input type="text" id="url" className="form-control"
                                            value={this.state.menuPayload.url}
                                            onChange={this.inputChangeHandler}
                                        />
                                        <label className="form-group-label">URL :<span className="requiredMark"><i className="fa fa-asterisk" /></span></label>
                                    </div>
                                    <div className="form-group">
                                        <input type="text" id="fontAwesomeIconClass" placeholder="Font Class"
                                            className="form-control" value={this.state.menuPayload.fontAwesomeIconClass}
                                            onChange={this.inputChangeHandler}
                                        />
                                        <label className="form-group-label">Font Awesome Icon Class :<span className="requiredMark"><i className="fa fa-asterisk" /></span></label>
                                    </div>

                                    <div className="form-group">
                                        <label className="checkboxLabel">
                                            <span className="checkText">Do you want to open the navigation in new tab ?</span>
                                            <input type="checkbox" id="newTab" onChange={this.inputChangeHandler} checked={this.state.menuPayload.newTab} />
                                            <span className="checkmark"></span>
                                        </label>
                                    </div>
                                </div>

                                <div className="bottomButton text-right">
                                    <button type="button" disabled={this.state.menuPayload.fontAwesomeIconClass.length === 0 || this.state.menuPayload.entities.length === 0 || this.state.menuPayload.url.length === 0 || this.state.menuPayload.navName.length === 0} className={(this.state.menuPayload.fontAwesomeIconClass.length === 0 || this.state.menuPayload.entities.length === 0 || this.state.menuPayload.url.length === 0 || this.state.menuPayload.navName.length === 0 ? "btn-disabled " : "") + "btn btn-success"} onClick={() => this.onSubmitHandler()}>Save</button>
                                    <button type="button" className="btn btn-dark" data-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/* end add child modal */}

                {this.state.openModal &&
                    <NotificationModal
                        type={this.state.type}
                        message2={this.state.message2}
                        onCloseHandler={this.onCloseHandler}
                    />
                }
            </React.Fragment>
        );
    }
}

ManageNavigation.propTypes = {
    dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
    navList: getNavList(),
    navListError: getNavListError(),
    navListSaveSuccess: getSaveNavListSuccess(),
    submitFormSuccessMessage: getSubmitFormSuccessMessage(),
    navListSaveError: getSaveNavListError(),
    submitFormSuccess: getSubmitFormSuccess(),
    submitFormFailure: getSubmitFormFailure(),
    deleteSuccessmessage: getDeleteSuccess(),
    deleteMenuSuccess: getDeleteMenuSuccess(),
    deleteErrormessage: getDeleteError(),
    isFetching: getIsFetching()
});

function mapDispatchToProps(dispatch) {
    return {
        dispatch,
        getNavigationList: () => dispatch(getNavigationList()),
        updateNav: () => dispatch(updateNav()),
        navlistSaveHandler: (payload) => dispatch(navlistSaveHandler(payload)),
        resetToIntial: () => dispatch(resetToIntial()),
        onSubmit: (payload) => dispatch(onSubmitHandler(payload)),
        menuDeleteHandler: (id) => dispatch(menuDeleteHandler(id))
    };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'ManageNavigation', reducer });
const withSaga = injectSaga({ key: 'ManageNavigation', saga });

export default compose(
    withReducer,
    withSaga,
    withConnect,
)(ManageNavigation);
