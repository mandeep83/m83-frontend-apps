/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectNavigationAttributesDomain = state =>
  state.get('ManageNavigation', initialState);


export const getNavList = () =>
  createSelector(selectNavigationAttributesDomain, substate => substate.list);

export const getNavListError = () =>
  createSelector(selectNavigationAttributesDomain, substate => substate.error);

export const getSaveNavListSuccess = () =>
  createSelector(selectNavigationAttributesDomain, substate => substate.navListSaveSuccess);

export const getSubmitFormSuccess = () =>
createSelector(selectNavigationAttributesDomain, substate => substate.submitFormSuccess);

export const getSaveNavListError = () =>
  createSelector(selectNavigationAttributesDomain, substate => substate.navListSaveError);

export const getSubmitFormSuccessMessage = () => createSelector(selectNavigationAttributesDomain, subState => subState.submitFormSuccessMessage);
export const getSubmitFormFailure = () => createSelector(selectNavigationAttributesDomain, subState => subState.submitFormFailure);

export const getDeleteSuccess = () => createSelector(selectNavigationAttributesDomain, substate => substate.deleteSuccess);
export const getDeleteMenuSuccess = () => createSelector(selectNavigationAttributesDomain, substate => substate.deleteMenuSuccess);
export const getDeleteError = () => createSelector(selectNavigationAttributesDomain, substate => substate.deleteError);

export const isFetchingState = state => state.get('loader');

export const getIsFetching = () =>
    createSelector(isFetchingState, substate => substate.get('isFetching'));

