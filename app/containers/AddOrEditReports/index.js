/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import makeSelectAddOrEditReports from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import ReactSelect from "react-select";
import SlidingPane from 'react-sliding-pane';
import Modal from 'react-modal';

/* eslint-disable react/prefer-stateless-function */
export class AddOrEditReports extends React.Component {
  render() {
    return (
      <React.Fragment>
        <Helmet>
          <title>AddOrEditReports</title>
          <meta name="description" content="Description of AddOrEditReports" />
        </Helmet>
        <div className="pageBreadcrumb">
          <div className="row">
            <div className="col-8">
              <p>
                <span className="previousPage" onClick={() => { this.props.history.push('/reports') }}>Reports</span>
                <span className="active">Add New</span>
              </p>
            </div>
            <div className="col-4 flex justify-content-end align-items-center">
            </div>
          </div>
        </div>
        <div className="outerBox pb-0">
          {/* <SlidingPane
            className='some-custom-class reactSlidingPane'
            overlayClassName='some-custom-overlay-class slidingFormOverlay'
            closeIcon={<div></div>}
            isOpen='false'
            from='right'
            width='550px'
          >
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">Add New</h5>
                <button className="close" >&times;</button>
              </div>

              <form className="modal-body">
                <div className="form-group">
                  <input type="text" className="form-control" required autoFocus />
                  <label className="form-group-label">Name :<span className="requiredMark"><i className="fa fa-asterisk" /></span></label>
                </div>
                <div className="form-group">
                  <textarea rows="3" type="text" id="description" placeholder="Description" className="form-control" />
                  <label className="form-group-label">Description :</label>
                </div>
                <div className="form-group">
                  <input type="text" className="form-control" required />
                  <label className="form-group-label">Report title :<span className="requiredMark"><i className="fa fa-asterisk" /></span></label>
                </div>
                <div className="form-group inheritedForm">
                  <label className="form-label">Data Providers :</label>
                  <ReactSelect
                    isMulti={true}
                    className="form-control-multi-select" >
                  </ReactSelect>
                </div>
                <div className="form-group inheritedForm">
                  <label className="form-label">Data Attributes :</label>
                  <ReactSelect
                    isMulti={true}
                    className="form-control-multi-select">
                  </ReactSelect>
                </div>
                <div className="form-group flex">
                  <div className="form-group-left-label fx-b20">
                    <label className="form-group-label">Is Schedule  :</label>
                  </div>
                  <div className="form-group-right-label fx-b80">
                    <div className="flex">
                      <div className="flex-item fx-b40">
                        <label className="radioLabel">Yes
                          <input type="radio" />
                          <span className="radioMark" />
                        </label>
                      </div>
                      <div className="flex-item fx-b40">
                        <label className="radioLabel">No
                          <input type="radio" />
                          <span className="radioMark" />
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="form-group inheritedForm">
                  <label className="form-label">Scheduled:</label>
                  <ReactSelect
                    isMulti={true}
                    className="form-control-multi-select">
                  </ReactSelect>
                </div>
                <div className="form-group flex">
                  <div className="form-group-left-label fx-b20">
                    <label className="form-group-label">Format  :</label>
                  </div>
                  <div className="form-group-right-label fx-b80">
                    <div className="flex">
                      <div className="flex-item fx-b20">
                        <label className="radioLabel">PDF
                          <input type="radio" />
                          <span className="radioMark" />
                        </label>
                      </div>
                      <div className="flex-item fx-b20">
                        <label className="radioLabel">CSV
                          <input type="radio" />
                          <span className="radioMark" />
                        </label>
                      </div>
                      <div className="flex-item fx-b20">
                        <label className="radioLabel">Excel
                          <input type="radio" />
                          <span className="radioMark" />
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="border-bottom" />
                <div className="bottomButton mt-2 mb-0">
                  <button className="btn btn-primary mr-r-10">Generate</button>
                  <button className="btn btn-success">Save</button>
                  <button type="button" className="btn btn-secondary">Cancel</button>
                </div>
              </form>
            </div>
          </SlidingPane> */}

        </div>
      </React.Fragment>
    );
  }
}

AddOrEditReports.propTypes = {
  dispatch: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
  addoreditreports: makeSelectAddOrEditReports()
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

const withReducer = injectReducer({ key: "addOrEditReports", reducer });
const withSaga = injectSaga({ key: "addOrEditReports", saga });

export default compose(
  withReducer,
  withSaga,
  withConnect
)(AddOrEditReports);
