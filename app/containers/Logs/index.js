/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * Logs
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { FormattedMessage } from "react-intl";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import makeSelectLogs from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import messages from "./messages";

/* eslint-disable react/prefer-stateless-function */
export class Logs extends React.Component {
  
  state = {
    payload: {
      "duration":60,
      "containerName":"platform"
    }      
  }

  componentDidMount(){
    this.handlingEventSources();
  }

  handlingEventSources = () => {

    fetch("https://platform.dev-iot83.com/api/v1/logs", { 
      method: "POST",  
      body: JSON.stringify(this.state.payload),
      headers: { 
          "Content-type": "application/stream+json;charset=UTF-8",
      }
    }).then(response => {
      const reader = response.body.getReader();
      return new ReadableStream({
        start(controller) {
          return pump();
          function pump() {
            return reader.read().then(({ done, value }) => {
              // When no more data needs to be consumed, close the stream
              if (done) {
                  controller.close();
                  return;
              }
              // Enqueue the next data chunk into our target stream
                controller.enqueue(value);
              return pump();
            });
          }
        }  
      })
    }).then(stream => {
      // new Response(stream)
    })

    // let socket = new WebSocket("https://platform.dev-iot83.com/api/v1/logs");
    // socket.onopen = function(e) {
    //   socket.send(JSON.stringify(this.state.payload));
    // };
    
    // socket.onmessage = function(event) {
    // };

    // EventSource = SSE;
    // var source = new EventSource("https://platform.dev-iot83.com/api/v1/logs", this.state.payload);
    // source.onmessage=function(event)
    // {
    //   // document.getElementById("message-window").innerHTML+=event.data + "<br>";
    // };
  }

  render() {
    return (
      <div>
        <Helmet>
          <title>Logs</title>
          <meta name="description" content="Description of Logs" />
        </Helmet>
        <FormattedMessage {...messages.header} />
      </div>
    );
  }
}

Logs.propTypes = {
  dispatch: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
  logs: makeSelectLogs()
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

const withReducer = injectReducer({ key: "logs", reducer });
const withSaga = injectSaga({ key: "logs", saga });

export default compose(
  withReducer,
  withSaga,
  withConnect
)(Logs);
