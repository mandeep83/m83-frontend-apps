/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * AddOrEditConnector
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { FormattedMessage } from "react-intl";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import { allSelectors as SELECTORS } from "./selectors";
import { allActions as ACTIONS } from './actions'
import reducer from "./reducer";
import saga from "./saga";
import NotificationModal from "../../components/NotificationModal/Loadable";
import Loader from "../../components/Loader";
import produce from "immer"
import cloneDeep from 'lodash/cloneDeep';
/* eslint-disable react/prefer-stateless-function */

let s3Regions = [
    { name: "US East (Ohio)", value: "us-east-2" },
    { name: "US East (N. Virginia)", value: "us-east-1" },
    { name: "US West (N. California)", value: "us-west-1" },
    { name: "US West (Oregon)", value: "us-west-2" },
    { name: "Asia Pacific (Hong Kong)", value: "ap-east-1" },
    { name: "Asia Pacific (Mumbai)", value: "ap-south-1" },
    { name: "Asia Pacific (Osaka-Local)", value: "ap-northeast-3" },
    { name: "Asia Pacific (Seoul)", value: "ap-northeast-2" },
    { name: "Asia Pacific (Singapore)", value: "ap-southeast-1" },
    { name: "Asia Pacific (Sydney)", value: "ap-southeast-2" },
    { name: "Asia Pacific (Tokyo)", value: "ap-northeast-1" },
    { name: "Canada (Central)", value: "ca-central-1" },
    { name: "China (Beijing)", value: "cn-north-1" },
    { name: "China (Ningxia)", value: "cn-northwest-1" },
    { name: "Europe (Frankfurt)", value: "eu-central-1" },
    { name: "Europe (Ireland)", value: "eu-west-1" },
    { name: "Europe ", value: "eu-west-2" },
    { name: "Europe (Paris)", value: "eu-west-3" },
    { name: "Europe (Stockholm)", value: "eu-north-1" },
    { name: "Middle East (Bahrain)", value: "me-south-1" },
    { name: "South America (Sao Paulo)", value: "sa-east-1" },
    { name: "AWS GovCloud (US-East)", value: "us-gov-east-1" },
    { name: "AWS GovCloud (US-West)", value: "us-gov-west-1" }
]

export class AddOrEditConnector extends React.Component {

    getPayloadPropertiesByConnector = () => {
        switch (this.props.match.params.connector) {
            case "KAFKA": {
                return {
                    bootstrapServer: '',
                    isAuthenticated: true,
                    username: '',
                    password: '',
                    topics: [{
                        name: '',
                        schema: ''
                    }],
                }
            }
            case "MQTT": {
                return {
                    topics: [{
                        name: '',
                        schema: ''
                    }],
                    deviceIds: []
                }
            }
            case "HTTP": {
                return {
                    basePath: 'api/v1/connector/',
                    sinkType: '',
                    category: "",
                    collectionName: '',
                }
            }
            case 'MONGODB': {
                return {
                    url: '*******',
                    childType: 'mongo',
                    list: [{
                        database: '*******',
                        isAuthenticated: true,
                        username: '*******',
                        password: '*******',
                    }]

                }
            }
            case "S3": {
                return {
                    childType: "s3",
                    accessKey: '******',
                    secretKey: '******',
                    list: [{
                        bucketName: 'dev-m83-datacenter',
                        region: 'us-east-1',
                        basePath: '',
                        format: ''
                    }],
                }
            }
            case "HDFS": {
                return {
                    isAuthenticated: true,
                    url: '',
                    basePath: '',
                    dataDirs: [{
                        name: '',
                        basePath: '',
                        format: '',
                        partitionBy: []
                    }]
                }
            }
            case "EXCEL": {
                return {
                    fileList: [],
                }
            }
            case "CSV": {
                return {
                    fileList: [],
                }
            }
            case "HIVE": {
                return {
                    database: "",
                    tableList: [{
                        name: "",
                        partitionBy: [],
                    }]
                }
            }
            case "EMAIL": {
                return {
                    accessKey: "",
                    secretKey: "",
                    region: ""
                }
            }
            case "TIMESCALEDB": {
                return {
                    "childType": "timescale",              // change value according with category
                    "url": "******",
                    "list": [{
                        "username": "******",
                        "password": "******",
                        "database": "******",
                        "isAuthenticated": true,
                        "newTables": []
                    }
                    ]
                }
            }
        }
    }

    state = {
        isFetching: false,
        payload: {
            name: '',
            description: '',
            category: this.props.match.params.connector,
            isExternal: false,
            reportInterval: 10,
            sink: true,
            source: false,
            properties: this.getPayloadPropertiesByConnector(),
        },
        connectorCreatedSuccess: false,
    }

    componentWillMount() {
        if (this.props.match.params.id) {
            this.props.getConnectorById(this.props.match.params.id)
            this.setState({
                isFetching: true
            })
        }
    }
    
    componentWillReceiveProps(nextProps) {
        if (nextProps.getConnectorByIdSuccess) {
            let payload = nextProps.getConnectorByIdSuccess.response
            if (this.props.match.params.connector == "TIMESCALEDB" || this.props.match.params.connector == "MONGODB") {
                payload.properties.list.map(el => el.password = "******")
                delete payload.deviceTopology
                delete payload.image
            }
            if (this.props.match.params.connector == "S3") {
                delete payload.deviceTopology
                delete payload.image
                payload.properties.accessKey = "******"
                payload.properties.secretKey = "******"
            }
            this.setState({
                payload,
                isFetching: false
            })
            return;
        }

        if (nextProps.getConnectorByIdFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.getConnectorByIdFailure.error,
                isFetching: false
            })
            return;
        }
        
        if (nextProps.saveConnectorSuccess) {
            this.setState({
                modalType: "success",
                isOpen: true,
                message2: nextProps.saveConnectorSuccess.response,
                connectorCreatedSuccess: true,
            })
            return;
        }

        if (nextProps.saveConnectorFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                isFetching: false,
                message2: nextProps.saveConnectorFailure.error,
            })
            return;
        }
        
        if (nextProps.testConnectionSuccess) {
            if (nextProps.testConnectionSuccess.response) {

                this.setState({
                    modalType: "success",
                    isOpen: true,
                    message2: "Connection Tested Successfully",
                    testConnectionSuccess: true,
                    testConnectionInProgress: false
                })
            }
            else if (nextProps.testConnectionSuccess.response === false) {
                this.setState({
                    modalType: "error",
                    isOpen: true,
                    message2: "Connection Test Failed",
                    testConnectionFailed: true,
                    testConnectionInProgress: false
                })
            }
        }
        
        if (nextProps.testConnectionFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                testConnectionInProgress: false,
                message2: nextProps.testConnectionFailure.error,
            })
            return;
        }
    }

    componentDidUpdate(prevProps, prevState) {
        if (Object.keys(SELECTORS).some(prop => this.props[prop])) {
            this.props.resetToInitialState()
        }
    }

    onCloseHandler = () => {
        this.setState({
            isOpen: false,
            message2: '',
            modalType: '',
        }, () => this.state.connectorCreatedSuccess && this.props.history.push(`/connectors/${this.props.match.params.connector}`));
    }

    onChangeHandler = (event) => {
        let payload = JSON.parse(JSON.stringify(this.state.payload));
        if (event.target.name === 'sourceSinkType') {
            if (event.target.value === 'source') {
                payload.source = true;
                payload.sink = false;
            } else if (event.target.value === 'sink') {
                payload.sink = true;
                payload.source = false;
            } else {
                payload.source = true;
                payload.sink = true;
            }
        } else if (event.target.id) {
            if (event.target.id !== "description" && (event.target.value.trim() || event.target.value === "")) {
                payload[event.target.id] = event.target.value.replace(/\s+/g, '');
            } else {
                payload[event.target.id] = event.target.value;
            }
        }
        this.setState({
            payload
        })
    }

    definitionChangeHandlerMongoDb = (event, index) => {
        let payload = JSON.parse(JSON.stringify(this.state.payload));
        if (event.target.id === "database" || event.target.id === "username" || event.target.id === "password") {
            payload.properties.list[index][event.target.id] = event.target.value;
        } else {
            payload.properties[event.target.id] = event.target.value;
        }
        this.setState({
            payload
        })
    }

    definitionChangeHandlerTimeScale = (event, index) => {
        let payload = JSON.parse(JSON.stringify(this.state.payload));
        if (event.target.id === "url") {
            payload.properties[event.target.id] = event.target.value;
        } else {
            payload.properties.list[index][event.target.id] = event.target.value;
        }
        this.setState({
            payload
        })
    }

    definitionChangeHandlerS3 = (event, index) => {
        let payload = JSON.parse(JSON.stringify(this.state.payload));
        if (event.target.name === "basePath" || event.target.name === "bucketName" || event.target.name === "format"
            || event.target.name === "region") {
            payload.properties.list[index][event.target.name] = event.target.value;
        } else {
            payload.properties[event.target.id] = event.target.value;
        }
        this.setState({
            payload
        })
    }

    s3BasePathChangeHandler = (value, index) => {
        if (!value.startsWith("/")) {
            value = `/${value}`
        }
        let doubleSlashesArr = value.match(/(\/{2,3})/g);
        if (doubleSlashesArr) {
            doubleSlashesArr.map(multipleSlashes => {
                value = value.replace(multipleSlashes, "/");
            })
            this.props.showNotification("error", "Consecutive forward slashes are not allowed in Directory Name.")
        }
        let payload = cloneDeep(this.state.payload);
        payload.properties.list[index].basePath = value;
        this.setState({
            payload
        })
    }

    testConnection = () => {
        // if (this.props.match.params.connector === 'MONGODB') {
        let testConnectionPayload = {
            category: this.props.match.params.connector,
            isExternal: false,
        }
        this.setState({
            testConnectionInProgress: true,
        }, () => { this.props.testConnection(testConnectionPayload) });
        // }
    }

    saveUpdateValidator = () => {
        let connector = this.props.match.params.connector;
        switch (connector) {
            case "MONGODB":
                return !(this.state.testConnectionSuccess && this.state.payload.name);
            case "S3":
                return !(this.state.testConnectionSuccess && this.state.payload.name && this.state.payload && this.state.payload.properties.list.every(bucket => bucket.basePath && bucket.basePath.length > 1));
        }
    }

    saveUpdateHandler = () => {
        let payload = JSON.parse(JSON.stringify(this.state.payload));
        this.setState({
            isFetching: true,
        }, () => { this.props.saveConnector(payload) });

    };

    addDataBaseObjTimeScale = () => {
        let payload = produce(this.state.payload, draftState => {
            draftState.properties.list.push({
                "username": "",
                "password": "",
                "database": "",
                "isAuthenticated": true,
                "newTables": []
            })
        })
        this.setState({
            payload
        })
    }

    deleteTimeScaleDbDataBase = (index) => {
        let payload = JSON.parse(JSON.stringify(this.state.payload));
        payload.properties.list.splice(index, 1)
        this.setState({
            payload
        })
    }

    deleteS3Bucket = (index) => {
        let payload = JSON.parse(JSON.stringify(this.state.payload));
        payload.properties.list.splice(index, 1)
        this.setState({
            payload
        })
    }

    addS3Bucket = () => {
        let payload = produce(this.state.payload, draftState => {
            draftState.properties.list.push({
                bucketName: '',
                region: 'us-east-1',
                basePath: '',
            })
        })
        this.setState({
            payload
        })
    }

    disableConnectorTypeButtons = (connectorType) => {
        let connector = this.props.match.params.connector,
            id = this.props.match.params.id;
        switch (connectorType) {
            case "source": {
                return connector === "EMAIL" || connector === "TIMESCALEDB" || id
            }
            case "sink": {
                return connector === "EXCEL" || connector === "TIMESCALEDB" || connector === "HTTP" || connector === "MQTT" || id
            }
            case "sourceSink": {
                return connector === "MONGODB" || connector === "EXCEL" || connector === "TIMESCALEDB" || connector === "HTTP" || connector === "EMAIL" || connector === "MQTT" || id || connector === "S3"
            }
        }
    }

    disableConnectionTypeButtons = (connectionType) => {
        let connector = this.props.match.params.connector,
            id = this.props.match.params.id;
        switch (connectionType) {
            case "external": {
                return id || connector === "TIMESCALEDB" || connector === "HTTP" || connector === "MQTT" || connector === "MONGODB" || connector === "S3"
            }
            case "internal": {
                return (!(connector === "MQTT" || connector === "TIMESCALEDB" || connector === "MONGODB" || connector === "KAFKA" || connector === "S3")) || id
            }
        }
    }

    s3ConnectorBucketsClass = () => {
        if (this.state.payload.source && this.state.payload.isExternal) {
            return "25"
        }
        else if ((!this.state.payload.source) && !this.state.payload.isExternal) {
            return "50"
        }
        else {
            return "33"
        }
    }

    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>Add Or Edit Connector</title>
                    <meta name="description" content="Description of Add Or Edit Connector" />
                </Helmet>

                {this.state.isFetching ? <Loader /> :
                    <React.Fragment>
                        <header className="content-header d-flex">
                            <div className="flex-60">
                                <div className="d-flex">
                                    <h6 className="previous" onClick={() => this.props.history.push('/connectors')}>Connector</h6>
                                    <h6 className="previous" onClick={() => this.props.history.push(`/connectors/${this.props.match.params.connector}`)}>{this.props.match.params.connector}</h6>
                                    <h6 className="active">{this.props.match.params.id ? this.state.payload.name : 'Add Connector'}</h6>
                                </div>
                            </div>
                            <div className="flex-40 text-right">
                                <div className="content-header-group">
                                    <button className="btn btn-light" data-tooltip data-tooltip-text="Back" data-tooltip-place="bottom" onClick={() => this.props.history.push(`/connectors/${this.props.match.params.connector}`)}><i className="far fa-angle-left"></i></button>
                                </div>
                            </div>
                        </header>
                        
                        <div className="content-body">
                            <form onSubmit={this.saveUpdateHandler}>
                                <div className="form-content-wrapper">
                                    <div className="form-content-box">
                                        <div className="form-content-header">
                                            <p>Configure</p>
                                        </div>
                                        <div className="form-content-body">
                                            <div className="d-flex">
                                                <div className="flex-50 pd-r-10">
                                                    <div className="form-group">
                                                        <label className="form-group-label">Name :<i className="fas fa-asterisk form-group-required"></i></label>
                                                        <input
                                                            type="text"
                                                            id="name"
                                                            className="form-control"
                                                            placeholder="Name"
                                                            value={this.state.payload.name}
                                                            onChange={this.onChangeHandler}
                                                            readOnly={this.props.match.params.id}
                                                            required
                                                            pattern="^[a-zA-z0-9]{1,}$"
                                                        />
                                                    </div>
                                                </div>
                                                <div className="flex-50 pd-l-10">
                                                    <div className="form-group">
                                                        <label className="form-group-label">Description :</label>
                                                        <textarea
                                                            rows="1"
                                                            type="text"
                                                            id="description"
                                                            placeholder="Description"
                                                            value={this.state.payload.description}
                                                            onChange={this.onChangeHandler}
                                                            className="form-control"
                                                        />
                                                    </div>
                                                </div>
                                                <div className="flex-50 pd-r-10">
                                                    <div className="form-group">
                                                        <label className="form-group-label">Connector Type :</label>
                                                        <div className="d-flex">
                                                            <div className="flex-30 pd-r-10">
                                                                <label className={`radio-button ${this.disableConnectorTypeButtons('source') ? "radio-button-disabled" : ""}`}>
                                                                    <span className="radio-button-text">Source</span>
                                                                    <input
                                                                        type="radio"
                                                                        value="source"
                                                                        name="sourceSinkType"
                                                                        checked={this.state.payload.source && !this.state.payload.sink}
                                                                        onChange={this.onChangeHandler}
                                                                        disabled={this.disableConnectorTypeButtons('source')}
                                                                    />
                                                                    <span className="radio-button-mark"></span>
                                                                </label>
                                                            </div>
                                                            <div className="flex-30 pd-l-5 pd-r-5">
                                                                <label className={`radio-button ${this.disableConnectorTypeButtons('sink') ? "radio-button-disabled" : ""}`}>
                                                                    <span className="radio-button-text">Sink</span>
                                                                    <input
                                                                        type="radio"
                                                                        value="sink"
                                                                        name="sourceSinkType"
                                                                        disabled={this.disableConnectorTypeButtons('sink')}
                                                                        checked={this.state.payload.sink && !this.state.payload.source}
                                                                        onChange={this.onChangeHandler}
                                                                    />
                                                                    <span className="radio-button-mark"></span>
                                                                </label>
                                                            </div>
                                                            <div className="flex-40 pd-l-10">
                                                                <label className={`radio-button ${this.disableConnectorTypeButtons('sourceSink') ? "radio-button-disabled" : ""}`}>
                                                                    <span className="radio-button-text">Source / Sink</span>
                                                                    <input
                                                                        type="radio"
                                                                        value="source/sink"
                                                                        name="sourceSinkType"
                                                                        disabled={this.disableConnectorTypeButtons('sourceSink')}
                                                                        checked={this.state.payload.source && this.state.payload.sink}
                                                                        onChange={this.onChangeHandler}
                                                                    />
                                                                    <span className="radio-button-mark"></span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="flex-50 pd-l-10">
                                                    <div className="form-group">
                                                        <label className="form-group-label">Connection Type :</label>
                                                        <div className="d-flex">
                                                            <div className="flex-30 pd-r-10">
                                                                <label className={`radio-button ${this.disableConnectionTypeButtons('external') ? "radio-button-disabled" : ""}`}>
                                                                    <span className="radio-button-text">External</span>
                                                                    <input
                                                                        type="radio"
                                                                        value="external"
                                                                        name="connectionType"
                                                                        checked={this.state.payload.isExternal}
                                                                        onChange={this.onChangeHandler}
                                                                        disabled={this.disableConnectionTypeButtons('external')}
                                                                    />
                                                                    <span className="radio-button-mark"></span>
                                                                </label>
                                                            </div>
                                                            <div className="flex-30 pd-l-10">
                                                                <label className={`radio-button ${this.disableConnectionTypeButtons('internal') ? "radio-button-disabled" : ""}`}>
                                                                    <span className="radio-button-text">Internal</span>
                                                                    <input
                                                                        type="radio"
                                                                        value="internal"
                                                                        name="connectionType"
                                                                        checked={!this.state.payload.isExternal}
                                                                        onChange={this.onChangeHandler}
                                                                        disabled={this.disableConnectionTypeButtons('internal')}
                                                                    />
                                                                    <span className="radio-button-mark"></span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    {/* Mongo DB */}
                                    {this.props.match.params.connector === 'MONGODB' &&
                                        <div className="form-content-box">
                                            <div className="form-content-header">
                                                <p>Definition</p>
                                            </div>
                                            <div className="form-content-body">
                                                <div className="d-flex">
                                                    <div className="flex-90 pd-r-10">
                                                        <div className="form-group">
                                                            <label className="form-group-label">URL : </label>
                                                            <input
                                                                type="text"
                                                                className="form-control"
                                                                id="url"
                                                                name="url"
                                                                placeholder="URL"
                                                                disabled={!this.state.payload.isExternal}
                                                                value={this.state.payload.properties.url}
                                                                onChange={this.definitionChangeHandlerMongoDb}
                                                                required />
                                                        </div>
                                                    </div>
                                                    <div className="flex-10 pd-l-5">
                                                        <div className="form-group">
                                                            {this.state.testConnectionInProgress ?
                                                                <button type="button" className="btn btn-transparent-green mr-t-25 w-100">
                                                                    <i className="far fa-cog mr-r-5"></i>Testing
                                                                </button> :
                                                                this.state.testConnectionSuccess ?
                                                                    <button type="button" className="btn btn-transparent-green mr-t-25 w-100">
                                                                        <i className="far fa-cog mr-r-5"></i>Tested
                                                                    </button> :
                                                                    this.state.testConnectionFailed ?
                                                                        <button type="button" className="btn btn-transparent-red mr-t-25 w-100" onClick={this.testConnection}>
                                                                            <i className="far fa-cog mr-r-5"></i>Retry
                                                                        </button> :
                                                                        <button type="button" className="btn btn-transparent-blue mr-t-25 w-100" onClick={this.testConnection}>
                                                                            <i className="far fa-cog mr-r-5"></i>Test
                                                                        </button>
                                                            }

                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="connector-form-list form-group">
                                                    <label className="form-group-label">
                                                        <button className="btn btn-primary" disabled={!this.state.payload.isExternal}><i className="far fa-plus"></i></button>
                                                        Add Database(s) :
                                                    </label>
                                                    <div className="connector-form-body">
                                                        <div className="d-flex pd-r-30">
                                                            <label className="form-group-label flex-30">Name :</label>
                                                            <label className="form-group-label flex-20">Authentication :</label>
                                                            <label className="form-group-label flex-25">Username :</label>
                                                            <label className="form-group-label flex-25">Password :</label>
                                                        </div>
                                                        {this.state.payload.properties.list.map((dataBaseObj, index) =>
                                                            <ul className="list-style-none d-flex">
                                                                <li className="flex-30">
                                                                    <input
                                                                        type="text"
                                                                        className="form-control"
                                                                        id="database"
                                                                        name="Data Base"
                                                                        placeholder="Data Base"
                                                                        value={dataBaseObj.database}
                                                                        disabled={!this.state.payload.isExternal}
                                                                        onChange={(e) => this.definitionChangeHandlerMongoDb(e, index)}
                                                                        required
                                                                    />
                                                                </li>
                                                                <li className="flex-20">
                                                                    <div className={`toggle-switch-wrapper ${!this.state.payload.isExternal ? "toggle-switch-wrapper-disabled" : ''} pt-2`}>
                                                                        <span>Disable</span>
                                                                        <label className="toggle-switch">
                                                                            <input
                                                                                type="checkbox"
                                                                                id="isAuthenticated"
                                                                                disabled={!this.state.payload.isExternal}
                                                                                checked={dataBaseObj.isAuthenticated}
                                                                                onChange={(e) => this.definitionChangeHandlerMongoDb(e, index)}
                                                                            />
                                                                            <span className="toggle-switch-slider"></span>
                                                                        </label>
                                                                        <span>Enable</span>
                                                                    </div>
                                                                </li>
                                                                <li className="flex-25">
                                                                    <input
                                                                        type="text"
                                                                        className="form-control"
                                                                        id="username"
                                                                        name="username"
                                                                        placeholder="User Name"
                                                                        value={dataBaseObj.username}
                                                                        disabled={!this.state.payload.isExternal}
                                                                        onChange={(e) => this.definitionChangeHandlerMongoDb(e, index)}
                                                                        required
                                                                    />

                                                                </li>
                                                                <li className="flex-25">
                                                                    <input
                                                                        type="password"
                                                                        className="form-control"
                                                                        id="password"
                                                                        name="password"
                                                                        placeholder="Password"
                                                                        value={dataBaseObj.password}
                                                                        disabled={!this.state.payload.isExternal}
                                                                        onChange={(e) => this.definitionChangeHandlerMongoDb(e, index)}
                                                                    />

                                                                </li>
                                                                <div className="button-group">
                                                                    <button className="btn-transparent btn-transparent-red" data-tooltip data-tooltip-text="Delete" data-tooltip-place="bottom" disabled={!this.state.payload.isExternal}><i className="far fa-trash-alt"></i></button>
                                                                </div>
                                                            </ul>
                                                        )}

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    }

                                    {/* S3 */}
                                    {this.props.match.params.connector === 'S3' &&
                                        <div className="form-content-box">
                                            <div className="form-content-header">
                                                <p>Definition</p>
                                            </div>
                                            <div className="form-content-body">
                                                <div className="d-flex">
                                                    <div className="flex-45 pd-r-10">
                                                        <div className="form-group">
                                                            <label className="form-group-label">Access Key :</label>
                                                            <input type="password" id="accessKey" name="accessKey"
                                                                placeholder="Access Key"
                                                                value={this.state.payload.properties.accessKey}
                                                                // disabled={!this.state.payload.isExternal}
                                                                onChange={this.definitionChangeHandlerS3}
                                                                required
                                                                className="form-control" />
                                                        </div>
                                                    </div>
                                                    <div className="flex-45 pd-l-5 pd-r-5">
                                                        <div className="form-group">
                                                            <label className="form-group-label">Secret Key :</label>
                                                            <input type="password" id="secretKey" name="secretKey" placeholder="Secret Key"
                                                                value={this.state.payload.properties.accessKey}
                                                                // disabled={!this.state.payload.isExternal}
                                                                onChange={this.definitionChangeHandlerS3}
                                                                required
                                                                className="form-control" />
                                                        </div>
                                                    </div>
                                                    <div className="flex-10 pd-l-10">
                                                        <div className="form-group">
                                                            {this.state.testConnectionInProgress ?
                                                                <button type="button" className="btn btn-transparent-green mr-t-25 w-100">
                                                                    <i className="far fa-cog mr-r-5"></i>Testing
                                                                </button> :
                                                                this.state.testConnectionSuccess ?
                                                                    <button type="button" className="btn btn-transparent-green mr-t-25 w-100">
                                                                        <i className="far fa-cog mr-r-5"></i>Tested
                                                                    </button> :
                                                                    this.state.testConnectionFailed ?
                                                                        <button type="button" className="btn btn-transparent-red mr-t-25 w-100" onClick={this.testConnection}>
                                                                            <i className="far fa-cog mr-r-5"></i>Retry
                                                                        </button> :
                                                                        <button type="button" className="btn btn-transparent-blue mr-t-25 w-100" onClick={this.testConnection}>
                                                                            <i className="far fa-cog mr-r-5"></i>Test
                                                                        </button>
                                                            }
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="connector-form-list form-group">
                                                    <label className="form-group-label">
                                                        <button className="btn btn-primary" disabled={!this.state.payload.isExternal} onClick={this.addS3Bucket}><i className="far fa-plus"></i></button>
                                                        Add Bucket(s) :
                                                    </label>
                                                    <div className="connector-form-body">
                                                        <div className="d-flex pd-r-30">
                                                            {this.state.payload.isExternal && <label className="form-group-label flex-25">Bucket Name :</label>}
                                                            <label className={`form-group-label flex-${this.s3ConnectorBucketsClass()}`}>Base Path :<i className="fas fa-asterisk form-group-required"></i></label>
                                                            {this.state.payload.source && <label className={`form-group-label flex-${this.s3ConnectorBucketsClass()}`}>Format :<i className="fas fa-asterisk form-group-required"></i></label>}
                                                            <label className={`form-group-label flex-${this.s3ConnectorBucketsClass()}`}>Region :</label>
                                                        </div>
                                                        {this.state.payload.properties.list.map((obj, index) => {
                                                            return (
                                                                <ul className="list-style-none d-flex" key={index}>
                                                                    {this.state.payload.isExternal &&
                                                                        <li className="flex-25"><input type="text" name="bucketName" placeholder="Bucket Name"
                                                                            value={obj.bucketName}
                                                                            // disabled={!this.state.payload.isExternal}
                                                                            onChange={(e) => this.definitionChangeHandlerS3(e, index)}
                                                                            required
                                                                            className="form-control" />
                                                                        </li>
                                                                    }
                                                                    <li className={`flex-${this.s3ConnectorBucketsClass()}`}>
                                                                        <input 
                                                                            type="text" 
                                                                            name="basePath"
                                                                            placeholder="Base Path"
                                                                            value={obj.basePath || "/"}
                                                                            onChange={({currentTarget: {value}}) => this.s3BasePathChangeHandler(value, index)}
                                                                            required
                                                                            className="form-control"
                                                                        />
                                                                    </li>
                                                                    {this.state.payload.source &&
                                                                        <li className={`flex-${this.s3ConnectorBucketsClass()}`}>
                                                                            <select className="form-control" name="format"
                                                                                value={obj.format}
                                                                                onChange={(e) => this.definitionChangeHandlerS3(e, index)}
                                                                                required>
                                                                                <option value="">Select Format</option>
                                                                                <option value="excel">EXCEL</option>
                                                                                <option value="json">JSON</option>
                                                                                <option value="csv">CSV</option>
                                                                                <option value="parquet">Parquet</option>
                                                                            </select>
                                                                        </li>
                                                                    }
                                                                    <li className={`flex-${this.s3ConnectorBucketsClass()}`}>
                                                                        <select className="form-control" name="region"
                                                                            value={obj.region}
                                                                            disabled={!this.state.payload.isExternal}
                                                                            onChange={(e) => this.definitionChangeHandlerS3(e, index)}
                                                                            required>
                                                                            <option>Select</option>
                                                                            {s3Regions.map((region, index) => {
                                                                                return (<option key={index} value={region.value}>{region.name}</option>)
                                                                            })}
                                                                        </select></li>
                                                                    <div className="button-group">
                                                                        <button className="btn-transparent btn-transparent-red" data-tooltip data-tooltip-text="Delete" data-tooltip-place="bottom" disabled={!this.state.payload.isExternal || this.state.payload.properties.list < 2} onClick={() => this.deleteS3Bucket(index)}>
                                                                            <i className="far fa-trash-alt"></i>
                                                                        </button>
                                                                    </div>
                                                                </ul>
                                                            )
                                                        })}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    }

                                    {/* HDFS */}
                                    {this.props.match.params.connector === 'HDFS' &&
                                        <div className="form-content-box">
                                            <div className="form-content-header">
                                                <p>Definition</p>
                                            </div>
                                            <div className="form-content-body">
                                                <div className="flex-20 pd-l-10">
                                                    <div className="form-group">
                                                        <label className="form-group-label">Authentication :</label>
                                                        <div className="toggle-switch-wrapper pt-2">
                                                            <span>Disable</span>
                                                            <label className="toggle-switch">
                                                                <input type="checkbox" id="isAuthenticated" />
                                                                <span className="toggle-switch-slider"></span>
                                                            </label>
                                                            <span>Enable</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="d-flex">
                                                    <div className="flex-33 pd-r-10">
                                                        <div className="form-group">
                                                            <label className="form-group-label">URL : </label>
                                                            <input type="text" id="url" name="url" className="form-control" />
                                                        </div>
                                                    </div>
                                                    <div className="flex-33 pd-r-10">
                                                        <div className="form-group">
                                                            <label className="form-group-label">User Name :</label>
                                                            <input type="text" id="userName" name="userName" className="form-control" />
                                                        </div>
                                                    </div>
                                                    <div className="flex-33 pd-r-10">
                                                        <div className="form-group">
                                                            <label className="form-group-label">Password :</label>
                                                            <input type="text" id="password" name="password" className="form-control" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    }

                                    {/* Redshift */}
                                    {this.props.match.params.connector === 'REDSHIFT' &&
                                        <div className="form-content-box">
                                            <div className="form-content-header">
                                                <p>Definition</p>
                                            </div>
                                            <div className="form-content-body">
                                                <div className="d-flex">
                                                    <div className="flex-50 pd-r-10">
                                                        <div className="form-group">
                                                            <label className="form-group-label">URL : </label>
                                                            <input type="text" id="url" name="url" className="form-control" />
                                                        </div>
                                                    </div>
                                                    <div className="flex-25 pd-l-5 pd-r-5">
                                                        <div className="form-group">
                                                            <label className="form-group-label">Port : </label>
                                                            <input type="text" id="port" name="port" className="form-control" />
                                                        </div>
                                                    </div>
                                                    <div className="flex-25 pd-l-10">
                                                        <div className="form-group">
                                                            <label className="form-group-label">Region :</label>
                                                            <select className="form-control" id="region">
                                                                <option>Select</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="d-flex">
                                                    <div className="flex-45 pd-r-10">
                                                        <div className="form-group">
                                                            <label className="form-group-label">Access Key :</label>
                                                            <input type="password" id="accessKey" name="accessKey" className="form-control" />
                                                        </div>
                                                    </div>
                                                    <div className="flex-45 pd-l-5 pd-r-5">
                                                        <div className="form-group">
                                                            <label className="form-group-label">Secret Key :</label>
                                                            <input type="password" id="secretKey" name="secretKey" className="form-control" />
                                                        </div>
                                                    </div>
                                                    <div className="flex-10 pd-l-10">
                                                        <div className="form-group">
                                                            <button type="button" className="btn btn-transparent-blue mr-t-25 w-100">
                                                                <i className="far fa-cog mr-r-5"></i>Test
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="connector-form-list form-group">
                                                    <label className="form-group-label">
                                                        <button className="btn btn-primary"><i className="far fa-plus"></i></button>Add Database(s) :
                                                    </label>
                                                    <div className="connector-form-body">
                                                        <div className="d-flex pd-r-30">
                                                            <label className="form-group-label flex-30">Name:</label>
                                                            <label className="form-group-label flex-20">Authentication :</label>
                                                            <label className="form-group-label flex-25">Username :</label>
                                                            <label className="form-group-label flex-25">Password :</label>
                                                        </div>
                                                        <ul className="list-style-none d-flex">
                                                            <li className="flex-30"><input type="text" id="name" name="name" className="form-control" /></li>
                                                            <li className="flex-20">
                                                                <div className="toggle-switch-wrapper pt-2">
                                                                    <span>Disable</span>
                                                                    <label className="toggle-switch">
                                                                        <input type="checkbox" id="isAuthenticated" />
                                                                        <span className="toggle-switch-slider"></span>
                                                                    </label>
                                                                    <span>Enable</span>
                                                                </div>
                                                            </li>
                                                            <li className="flex-25"><input type="text" id="userName" name="userName" className="form-control" /></li>
                                                            <li className="flex-25"><input type="password" id="password" name="Password" className="form-control" /></li>
                                                            <div className="button-group">
                                                                <button className="btn-transparent btn-transparent-red" data-tooltip data-tooltip-text="Delete" data-tooltip-place="bottom">
                                                                    <i className="far fa-trash-alt"></i>
                                                                </button>
                                                            </div>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    }

                                    {/* ELASTIC Search */}
                                    {this.props.match.params.connector === 'ELASTIC' &&
                                        <div className="form-content-box">
                                            <div className="form-content-header">
                                                <p>Definition</p>
                                            </div>
                                            <div className="form-content-body">
                                                <div className="d-flex">
                                                    <div className="flex-90 pd-r-10">
                                                        <div className="form-group">
                                                            <label className="form-group-label">Bootstrap Server : </label>
                                                            <input type="text" id="bootstrapServer" name="bootstrapServer" className="form-control" />
                                                        </div>
                                                    </div>
                                                    <div className="flex-10 pd-l-5">
                                                        <div className="form-group">
                                                            <button type="button" className="btn btn-transparent-blue mr-t-25 w-100">
                                                                <i className="far fa-cog mr-r-5"></i>Test
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <div className="flex-20 pd-r-10">
                                                        <div className="form-group">
                                                            <label className="form-group-label">Authentication :</label>
                                                            <div className="toggle-switch-wrapper pt-2">
                                                                <span>Disable</span>
                                                                <label className="toggle-switch">
                                                                    <input type="checkbox" id="isAuthenticated" />
                                                                    <span className="toggle-switch-slider"></span>
                                                                </label>
                                                                <span>Enable</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="flex-40 pd-l-5 pd-r-5">
                                                        <div className="form-group">
                                                            <label className="form-group-label">Username :</label>
                                                            <input type="text" id="userName" name="userName" className="form-control" />
                                                        </div>
                                                    </div>
                                                    <div className="flex-40 pd-l-10">
                                                        <div className="form-group">
                                                            <label className="form-group-label">Password :</label>
                                                            <input type="text" id="password" name="password" className="form-control" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    }

                                    {/* TIMESCALEDB */}
                                    {this.props.match.params.connector === 'TIMESCALEDB' &&
                                        <div className="form-content-box">
                                            <div className="form-content-header">
                                                <p>Definition</p>
                                            </div>
                                            <div className="form-content-body">
                                                <div className="d-flex">
                                                    <div className="flex-90 pd-r-10">
                                                        <div className="form-group">
                                                            <label className="form-group-label">URL : </label>
                                                            <input type="text" id="url" name="url" className="form-control" placeholder="URL"
                                                                disabled={!this.state.payload.isExternal} value={this.state.payload.properties.url}
                                                                onChange={this.definitionChangeHandlerTimeScale} required
                                                            />
                                                        </div>
                                                    </div>
                                                    {/* <div className="flex-10 pd-l-5">
                                                    <div className="form-group">
                                                        <button type="button" className="btn btn-transparent-blue mr-t-25 w-100"><i
                                                            className="far fa-cog mr-r-5"></i>Test
                                                    </button>
                                                    </div>
                                                </div> */}
                                                </div>
                                                <div className="connector-form-list form-group">
                                                    <label className="form-group-label">
                                                        <button className="btn btn-primary" disabled={!this.state.payload.isExternal} onClick={this.addDataBaseObjTimeScale}><i className="far fa-plus"></i></button>
                                                        Add Database(s) :
                                                    </label>
                                                    <div className="connector-form-body">
                                                        <div className="d-flex pd-r-30">
                                                            <label className="form-group-label flex-30">Name :</label>
                                                            <label className="form-group-label flex-20">Authentication :</label>
                                                            <label className="form-group-label flex-25">Username :</label>
                                                            <label className="form-group-label flex-25">Password :</label>
                                                        </div>
                                                        {this.state.payload.properties.list.map((databaseObj, databaseIndex) => {
                                                            return (
                                                                <ul key={databaseIndex} className="list-style-none d-flex">
                                                                    <li className="flex-30">
                                                                        <input type="text" id="database" name="database" placeholder="database"
                                                                            disabled={!this.state.payload.isExternal} value={databaseObj.database}
                                                                            onChange={(e) => this.definitionChangeHandlerTimeScale(e, databaseIndex)}
                                                                            required className="form-control"
                                                                        />
                                                                    </li>
                                                                    <li className="flex-20">
                                                                        <div className="toggle-switch-wrapper pt-2">
                                                                            <span>Disable</span>
                                                                            <label className="toggle-switch">
                                                                                <input type="checkbox" id="isAuthenticated"
                                                                                    disabled={!this.state.payload.isExternal}
                                                                                    checked={databaseObj.isAuthenticated}
                                                                                    onChange={(e) => this.definitionChangeHandlerTimeScale(e, databaseIndex)}
                                                                                    className="form-control"
                                                                                />
                                                                                <span className="toggle-switch-slider"></span>
                                                                            </label>
                                                                            <span>Enable</span>
                                                                        </div>
                                                                    </li>
                                                                    <li className="flex-25">
                                                                        <input type="text" id="username"
                                                                            value={databaseObj.username}
                                                                            disabled={!this.state.payload.isExternal}
                                                                            onChange={(e) => this.definitionChangeHandlerTimeScale(e, databaseIndex)}
                                                                            name="username"
                                                                            className="form-control"
                                                                        />
                                                                    </li>
                                                                    <li className="flex-25">
                                                                        <input type="password" id="password"
                                                                            value={databaseObj.password}
                                                                            disabled={!this.state.payload.isExternal}
                                                                            onChange={(e) => this.definitionChangeHandlerTimeScale(e, databaseIndex)}
                                                                            name="Password"
                                                                            className="form-control"
                                                                        />
                                                                    </li>
                                                                    <div className="button-group">
                                                                        <button className="btn-transparent btn-transparent-red" data-tooltip data-tooltip-text="Delete" data-tooltip-place="bottom"
                                                                            disabled={!this.state.payload.isExternal || this.state.payload.properties.list.length < 2}
                                                                            onClick={() => this.deleteTimeScaleDbDataBase(databaseIndex)}
                                                                        ><i className="far fa-trash-alt"></i></button>
                                                                    </div>
                                                                </ul>
                                                            )
                                                        })}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    }

                                    {/* My SQL */}
                                    {this.props.match.params.connector === 'MYSQL' &&
                                        <div className="form-content-box">
                                            <div className="form-content-header">
                                                <p>Definition</p>
                                            </div>
                                            <div className="form-content-body">
                                                <div className="d-flex">
                                                    <div className="flex-90 pd-r-10">
                                                        <div className="form-group">
                                                            <label className="form-group-label">URL : </label>
                                                            <input type="text" id="url" name="url" className="form-control" />
                                                        </div>
                                                    </div>
                                                    <div className="flex-10 pd-l-5">
                                                        <div className="form-group">
                                                            <button type="button" className="btn btn-transparent-blue mr-t-25 w-100"><i
                                                                className="far fa-cog mr-r-5"></i>Test
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="connector-form-list form-group">
                                                    <label className="form-group-label">
                                                        <button className="btn btn-primary"><i className="far fa-plus"></i></button>Add Database(s) :
                                                    </label>
                                                    <div className="connector-form-body">
                                                        <div className="d-flex pd-r-30">
                                                            <label className="form-group-label flex-30">Name :</label>
                                                            <label className="form-group-label flex-20">Authentication :</label>
                                                            <label className="form-group-label flex-25">Username :</label>
                                                            <label className="form-group-label flex-25">Password :</label>
                                                        </div>
                                                        <ul className="list-style-none d-flex">
                                                            <li className="flex-30"><input type="text" id="name" name="name" className="form-control" /></li>
                                                            <li className="flex-20">
                                                                <div className="toggle-switch-wrapper pt-2">
                                                                    <span>Disable</span>
                                                                    <label className="toggle-switch">
                                                                        <input type="checkbox" id="isAuthenticated" />
                                                                        <span className="toggle-switch-slider"></span>
                                                                    </label>
                                                                    <span>Enable</span>
                                                                </div>
                                                            </li>
                                                            <li className="flex-25"><input type="text" id="userName" name="userName" className="form-control" /></li>
                                                            <li className="flex-25"><input type="password" id="password" name="Password" className="form-control" /></li>
                                                            <div className="button-group">
                                                                <button className="btn-transparent btn-transparent-red" data-tooltip data-tooltip-text="Delete" data-tooltip-place="bottom">
                                                                    <i className="far fa-trash-alt"></i>
                                                                </button>
                                                            </div>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    }

                                    {/* Postgres SQL */}
                                    {this.props.match.params.connector === 'POSTGRESSQL' &&
                                        <div className="form-content-box">
                                            <div className="form-content-header">
                                                <p>Definition</p>
                                            </div>
                                            <div className="form-content-body">
                                                <div className="d-flex">
                                                    <div className="flex-90 pd-r-10">
                                                        <div className="form-group">
                                                            <label className="form-group-label">URL : </label>
                                                            <input type="text" id="url" name="url" className="form-control" />
                                                        </div>
                                                    </div>
                                                    <div className="flex-10 pd-l-5">
                                                        <div className="form-group">
                                                            <button type="button" className="btn btn-transparent-blue mr-t-25 w-100">
                                                                <i className="far fa-cog mr-r-5"></i>Test
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="connector-form-list form-group">
                                                    <label className="form-group-label">
                                                        <button className="btn btn-primary"><i className="far fa-plus"></i></button>
                                                        Add Database(s) :
                                                    </label>
                                                    <div className="connector-form-body">
                                                        <div className="d-flex pd-r-30">
                                                            <label className="form-group-label flex-30">Name :</label>
                                                            <label className="form-group-label flex-20">Authentication :</label>
                                                            <label className="form-group-label flex-25">Username :</label>
                                                            <label className="form-group-label flex-25">Password :</label>
                                                        </div>
                                                        <ul className="list-style-none d-flex">
                                                            <li className="flex-30"><input type="text" id="name" name="name" className="form-control" /></li>
                                                            <li className="flex-20">
                                                                <div className="toggle-switch-wrapper pt-2">
                                                                    <span>Disable</span>
                                                                    <label className="toggle-switch">
                                                                        <input type="checkbox" id="isAuthenticated" />
                                                                        <span className="toggle-switch-slider"></span>
                                                                    </label>
                                                                    <span>Enable</span>
                                                                </div>
                                                            </li>
                                                            <li className="flex-25"><input type="text" id="userName" name="userName" className="form-control" /></li>
                                                            <li className="flex-25"><input type="password" id="password" name="Password" className="form-control" /></li>
                                                            <div className="button-group">
                                                                <button className="btn-transparent btn-transparent-red" data-tooltip data-tooltip-text="delete" data-tooltip-place="bottom">
                                                                    <i className="far fa-trash-alt"></i>
                                                                </button>
                                                            </div>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    }

                                    {/* Influx DB */}
                                    {this.props.match.params.connector === 'INFLUXDB' &&
                                        <div className="form-content-box">
                                            <div className="form-content-header">
                                                <p>Definition</p>
                                            </div>
                                            <div className="form-content-body">
                                                <div className="d-flex">
                                                    <div className="flex-90 pd-r-10">
                                                        <div className="form-group">
                                                            <label className="form-group-label">URL : </label>
                                                            <input type="text" id="url" name="url" className="form-control" />
                                                        </div>
                                                    </div>
                                                    <div className="flex-10 pd-l-5">
                                                        <div className="form-group">
                                                            <button type="button" className="btn btn-transparent-blue mr-t-25 w-100">
                                                                <i className="far fa-cog mr-r-5"></i>Test
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="connector-form-list form-group">
                                                    <label className="form-group-label">
                                                        <button className="btn btn-primary"><i className="far fa-plus"></i></button>
                                                        Add Database(s) :
                                                    </label>
                                                    <div className="connector-form-body">
                                                        <div className="d-flex pd-r-30">
                                                            <label className="form-group-label flex-30">Name :</label>
                                                            <label className="form-group-label flex-20">Authentication :</label>
                                                            <label className="form-group-label flex-25">Username :</label>
                                                            <label className="form-group-label flex-25">Password :</label>
                                                        </div>
                                                        <ul className="list-style-none d-flex">
                                                            <li className="flex-30"><input type="text" id="name" name="name" className="form-control" /></li>
                                                            <li className="flex-20">
                                                                <div className="toggle-switch-wrapper pt-2">
                                                                    <span>Disable</span>
                                                                    <label className="toggle-switch">
                                                                        <input type="checkbox" id="isAuthenticated" />
                                                                        <span className="toggle-switch-slider"></span>
                                                                    </label>
                                                                    <span>Enable</span>
                                                                </div>
                                                            </li>
                                                            <li className="flex-25"><input type="text" id="userName" name="userName" className="form-control" /></li>
                                                            <li className="flex-25"><input type="password" id="password" name="Password" className="form-control" /></li>
                                                            <div className="button-group">
                                                                <button className="btn-transparent btn-transparent-red" data-tooltip data-tooltip-text="Delete" data-tooltip-place="bottom">
                                                                    <i className="far fa-trash-alt"></i>
                                                                </button>
                                                            </div>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    }

                                    {/* Dynamo DB */}
                                    {this.props.match.params.connector === 'DYNAMODB' &&
                                        <div className="form-content-box">
                                            <div className="form-content-header">
                                                <p>Definition</p>
                                            </div>
                                            <div className="form-content-body">
                                                <div className="d-flex">
                                                    <div className="flex-50 pd-r-10">
                                                        <div className="form-group">
                                                            <label className="form-group-label">Access Key :</label>
                                                            <input type="password" id="accessKey" name="accessKey" className="form-control" />
                                                        </div>
                                                    </div>
                                                    <div className="flex-50 pd-l-10">
                                                        <div className="form-group">
                                                            <label className="form-group-label">Secret Key :</label>
                                                            <input type="password" id="secretKey" name="secretKey" className="form-control" />
                                                        </div>
                                                    </div>
                                                    <div className="flex-90 pd-r-10">
                                                        <div className="form-group">
                                                            <label className="form-group-label">Region :</label>
                                                            <select className="form-control" id="region">
                                                                <option>Select</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div className="flex-10 pd-l-5">
                                                        <div className="form-group">
                                                            <button type="button" className="btn btn-transparent-blue mr-t-25 w-100">
                                                                <i className="far fa-cog mr-r-5"></i>Test
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    }

                                    {/* RabbitMQ */}
                                    {this.props.match.params.connector === 'RABBITMQ' &&
                                        <div className="form-content-box">
                                            <div className="form-content-header">
                                                <p>Definition</p>
                                            </div>
                                            <div className="form-content-body">
                                                <div className="d-flex">
                                                    <div className="flex-90 pd-r-10">
                                                        <div className="form-group">
                                                            <label className="form-group-label">URL : </label>
                                                            <input type="text" id="url" name="url" className="form-control" />
                                                        </div>
                                                    </div>
                                                    <div className="flex-10 pd-l-5">
                                                        <div className="form-group">
                                                            <button type="button" className="btn btn-transparent-blue mr-t-25 w-100">
                                                                <i className="far fa-cog mr-r-5"></i>Test
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="d-flex">
                                                    <div className="flex-20 pd-r-10">
                                                        <div className="form-group">
                                                            <label className="form-group-label">Authentication :</label>
                                                            <div className="toggle-switch-wrapper pt-2">
                                                                <span>Disable</span>
                                                                <label className="toggle-switch">
                                                                    <input type="checkbox" id="isAuthenticated" />
                                                                    <span className="toggle-switch-slider"></span>
                                                                </label>
                                                                <span>Enable</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="flex-40 pd-r-5 pd-l-5">
                                                        <div className="form-group">
                                                            <label className="form-group-label">Username :</label>
                                                            <input type="text" id="userName" name="userName" className="form-control" />
                                                        </div>
                                                    </div>
                                                    <div className="flex-40 pd-l-10">
                                                        <div className="form-group">
                                                            <label className="form-group-label">Password :</label>
                                                            <input type="password" id="password" name="Password" className="form-control" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="connector-form-list form-group">
                                                    <label className="form-group-label">
                                                        <button className="btn btn-primary"><i className="far fa-plus"></i></button>
                                                        Add Queue(s) :
                                                    </label>
                                                    <div className="connector-form-body">
                                                        <div className="d-flex pd-r-30">
                                                            <label className="form-group-label flex-100">Name :</label>
                                                        </div>
                                                        <ul className="list-style-none d-flex">
                                                            <li className="flex-80"><input type="text" id="name" name="name" className="form-control" /></li>
                                                            <li className="flex-20">
                                                                <button type="button" className="btn btn-transparent-green w-100">
                                                                    <i className="far fa-cog mr-r-5"></i>Generate Schema
                                                                </button>
                                                            </li>
                                                            <div className="button-group">
                                                                <button className="btn-transparent btn-transparent-red" data-tooltip data-tooltip-text="Delete" data-tooltip-place="bottom">
                                                                    <i className="far fa-trash-alt"></i>
                                                                </button>
                                                            </div>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    }

                                    {/* Cassandra */}
                                    {this.props.match.params.connector === 'CASSANDRA' &&
                                        <div className="form-content-box">
                                            <div className="form-content-header">
                                                <p>Definition</p>
                                            </div>
                                            <div className="form-content-body">
                                                <div className="d-flex">
                                                    <div className="flex-90 pd-r-10">
                                                        <div className="form-group">
                                                            <label className="form-group-label">URL : </label>
                                                            <input type="text" id="url" name="url" className="form-control" />
                                                        </div>
                                                    </div>
                                                    <div className="flex-10 pd-l-5">
                                                        <div className="form-group">
                                                            <button type="button" className="btn btn-transparent-blue mr-t-25 w-100">
                                                                <i className="far fa-cog mr-r-5"></i>Test
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="connector-form-list form-group">
                                                    <label className="form-group-label">
                                                        <button className="btn btn-primary"><i className="far fa-plus"></i></button>
                                                        Add Database(s) :
                                                    </label>
                                                    <div className="connector-form-body">
                                                        <div className="d-flex pd-r-30">
                                                            <label className="form-group-label flex-30">Name :</label>
                                                            <label className="form-group-label flex-20">Authentication :</label>
                                                            <label className="form-group-label flex-25">Username :</label>
                                                            <label className="form-group-label flex-25">Password :</label>
                                                        </div>
                                                        <ul className="list-style-none d-flex">
                                                            <li className="flex-30"><input type="text" id="name" name="name" className="form-control" /></li>
                                                            <li className="flex-20">
                                                                <div className="toggle-switch-wrapper pt-2">
                                                                    <span>Disable</span>
                                                                    <label className="toggle-switch">
                                                                        <input type="checkbox" id="isAuthenticated" />
                                                                        <span className="toggle-switch-slider"></span>
                                                                    </label>
                                                                    <span>Enable</span>
                                                                </div>
                                                            </li>
                                                            <li className="flex-25"><input type="text" id="userName" name="userName" className="form-control" /></li>
                                                            <li className="flex-25"><input type="password" id="password" name="Password" className="form-control" /></li>
                                                            <div className="button-group">
                                                                <button className="btn-transparent btn-transparent-red" data-tooltip data-tooltip-text="Delete" data-tooltip-place="bottom">
                                                                    <i className="far fa-trash-alt"></i>
                                                                </button>
                                                            </div>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    }

                                    {/* Kinesis */}
                                    {this.props.match.params.connector === 'KINESIS' &&
                                        <div className="form-content-box">
                                            <div className="form-content-header">
                                                <p>Definition</p>
                                            </div>
                                            <div className="form-content-body">
                                                <div className="d-flex">
                                                    <div className="flex-45 pd-r-10">
                                                        <div className="form-group">
                                                            <label className="form-group-label">Access Key :</label>
                                                            <input type="password" id="accessKey" name="accessKey" className="form-control" />
                                                        </div>
                                                    </div>
                                                    <div className="flex-45 pd-l-5 pd-r-5">
                                                        <div className="form-group">
                                                            <label className="form-group-label">Secret Key :</label>
                                                            <input type="password" id="secretKey" name="secretKey" className="form-control" />
                                                        </div>
                                                    </div>
                                                    <div className="flex-10 pd-l-10">
                                                        <div className="form-group">
                                                            <button type="button" className="btn btn-transparent-blue mr-t-25 w-100">
                                                                <i className="far fa-cog mr-r-5"></i>Test
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="connector-form-list form-group">
                                                    <label className="form-group-label">
                                                        <button className="btn btn-primary"><i className="far fa-plus"></i></button>
                                                        Add Stream(s) :
                                                    </label>
                                                    <div className="connector-form-body">
                                                        <div className="d-flex pd-r-30">
                                                            <label className="form-group-label flex-100">Name :</label>
                                                        </div>
                                                        <ul className="list-style-none d-flex">
                                                            <li className="flex-80"><input type="text" id="name" name="name" className="form-control" /></li>
                                                            <li className="flex-20">
                                                                <button type="button" className="btn btn-transparent-green w-100">
                                                                    <i className="far fa-cog mr-r-5"></i>Generate Schema
                                                                </button>
                                                            </li>
                                                            <div className="button-group">
                                                                <button className="btn-transparent btn-transparent-red" data-tooltip data-tooltip-text="Delete" data-tooltip-place="bottom">
                                                                    <i className="far fa-trash-alt"></i>
                                                                </button>
                                                            </div>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    }

                                    {/* HIVE */}
                                    {this.props.match.params.connector === 'HIVE' &&
                                        <div className="form-content-box">
                                            <div className="form-content-header">
                                                <p>Definition</p>
                                            </div>
                                            <div className="form-content-body">
                                                <div className="connector-form-list form-group">
                                                    <label className="form-group-label">
                                                        <button className="btn btn-primary"><i className="far fa-plus"></i></button>
                                                        Add Database(s) :
                                                    </label>
                                                    <div className="connector-form-body">
                                                        <div className="d-flex pd-r-30">
                                                            <label className="form-group-label flex-75">Name :</label>
                                                            <label className="form-group-label flex-25">Authentication :</label>
                                                        </div>
                                                        <ul className="list-style-none d-flex">
                                                            <li className="flex-75"><input type="text" id="name" name="name" className="form-control" /></li>
                                                            <li className="flex-25">
                                                                <div className="toggle-switch-wrapper pt-2">
                                                                    <span>Disable</span>
                                                                    <label className="toggle-switch">
                                                                        <input type="checkbox" id="isAuthenticated" />
                                                                        <span className="toggle-switch-slider"></span>
                                                                    </label>
                                                                    <span>Enable</span>
                                                                </div>
                                                            </li>
                                                            <div className="button-group">
                                                                <button className="btn-transparent btn-transparent-red" data-tooltip data-tooltip-text="Delete" data-tooltip-place="bottom">
                                                                    <i className="far fa-trash-alt"></i>
                                                                </button>
                                                            </div>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    }

                                    {/* KAFKA */}
                                    {this.props.match.params.connector === 'KAFKA' &&
                                        <div className="form-content-box">
                                            <div className="form-content-header">
                                                <p>Definition</p>
                                            </div>
                                            <div className="form-content-body">
                                                <div className="d-flex">
                                                    <div className="flex-90 pd-r-10">
                                                        <div className="form-group">
                                                            <label className="form-group-label">Bootstrap Server : </label>
                                                            <input type="text" id="url" name="url" className="form-control" />
                                                        </div>
                                                    </div>
                                                    <div className="flex-10 pd-l-5">
                                                        <div className="form-group">
                                                            <button type="button" className="btn btn-transparent-blue mr-t-25 w-100"><i
                                                                className="far fa-cog mr-r-5"></i>Test
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="d-flex">
                                                    <div className="flex-20 pd-l-10">
                                                        <div className="form-group">
                                                            <label className="form-group-label">Authentication :</label>
                                                            <div className="toggle-switch-wrapper pt-2">
                                                                <span>Disable</span>
                                                                <label className="toggle-switch">
                                                                    <input type="checkbox" id="isAuthenticated" />
                                                                    <span className="toggle-switch-slider"></span>
                                                                </label>
                                                                <span>Enable</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="flex-40 pd-r-5 pd-l-5">
                                                        <div className="form-group">
                                                            <label className="form-group-label">Username :</label>
                                                            <input type="text" id="userName" name="userName" className="form-control" />
                                                        </div>
                                                    </div>
                                                    <div className="flex-40 pd-l-10">
                                                        <div className="form-group">
                                                            <label className="form-group-label">Password :</label>
                                                            <input type="text" id="password" name="password" className="form-control" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="alert alert-info note-text">
                                                    <p>1. Please click generate schema to decipher the data attributes</p>
                                                    <p>2. Add <strong className="text-dark">'#'</strong> after the topic for wildcard subscription</p>
                                                </div>
                                                <div className="connector-form-list form-group">
                                                    <label className="form-group-label">
                                                        <button className="btn btn-primary"><i className="far fa-plus"></i></button>
                                                        Add Topic(s) :
                                                    </label>
                                                    <div className="connector-form-body">
                                                        <div className="d-flex pd-r-30">
                                                            <label className="form-group-label flex-100">Name :</label>
                                                        </div>
                                                        <ul className="list-style-none d-flex">
                                                            <li className="flex-80"><input type="text" id="name" name="name" className="form-control" /></li>
                                                            <li className="flex-20">
                                                                <button type="button" className="btn btn-transparent-green w-100"><i
                                                                    className="far fa-cog mr-r-5"></i>Generate Schema
                                                                </button>
                                                            </li>
                                                            <div className="button-group">
                                                                <button className="btn-transparent btn-transparent-red" data-tooltip data-tooltip-text="Delete" data-tooltip-place="bottom">
                                                                    <i className="far fa-trash-alt"></i>
                                                                </button>
                                                            </div>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    }

                                    {/* MQTT */}
                                    {this.props.match.params.connector === 'MQTT' &&
                                        <div className="form-content-box">
                                            <div className="form-content-header">
                                                <p>Definition</p>
                                            </div>
                                            <div className="form-content-body">
                                                <div className="d-flex">
                                                    <div className="flex-90 pd-r-10">
                                                        <div className="form-group">
                                                            <label className="form-group-label">Bootstrap Server : </label>
                                                            <input type="text" id="url" name="url" className="form-control" />
                                                        </div>
                                                    </div>
                                                    <div className="flex-10 pd-l-5">
                                                        <div className="form-group">
                                                            <button type="button" className="btn btn-transparent-blue mr-t-25 w-100"><i
                                                                className="far fa-cog mr-r-5"></i>Test
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="d-flex">
                                                    <div className="flex-20 pd-l-10">
                                                        <div className="form-group">
                                                            <label className="form-group-label">Authentication :</label>
                                                            <div className="toggle-switch-wrapper pt-2">
                                                                <span>Disable</span>
                                                                <label className="toggle-switch">
                                                                    <input type="checkbox" id="isAuthenticated" />
                                                                    <span className="toggle-switch-slider"></span>
                                                                </label>
                                                                <span>Enable</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="flex-40 pd-r-5 pd-l-5">
                                                        <div className="form-group">
                                                            <label className="form-group-label">Username :</label>
                                                            <input type="text" id="userName" name="userName" className="form-control" />
                                                        </div>
                                                    </div>
                                                    <div className="flex-40 pd-l-10">
                                                        <div className="form-group">
                                                            <label className="form-group-label">Password :</label>
                                                            <input type="text" id="password" name="password" className="form-control" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="alert alert-info note-text">
                                                    <p>1. Please click generate schema to decipher the data attributes</p>
                                                    <p>2. Add <strong className="text-dark">'#'</strong> after the topic for wildcard subscription</p>
                                                </div>
                                                <div className="connector-form-list form-group">
                                                    <label className="form-group-label">
                                                        <button className="btn btn-primary"><i className="far fa-plus"></i></button>
                                                        Add Topic(s) :
                                                    </label>
                                                    <div className="connector-form-body">
                                                        <div className="d-flex pd-r-30">
                                                            <label className="form-group-label flex-100">Name :</label>
                                                        </div>
                                                        <ul className="list-style-none d-flex">
                                                            <li className="flex-80"><input type="text" id="name" name="name" className="form-control" /></li>
                                                            <li className="flex-20">
                                                                <button type="button" className="btn btn-transparent-green w-100">
                                                                    <i className="far fa-cog mr-r-5"></i>Generate Schema
                                                                </button>
                                                            </li>
                                                            <div className="button-group">
                                                                <button className="btn-transparent btn-transparent-red" data-tooltip data-tooltip-text="Delete" data-tooltip-place="bottom">
                                                                    <i className="far fa-trash-alt"></i>
                                                                </button>
                                                            </div>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    }

                                    {/* HTTP */}
                                    {this.props.match.params.connector === 'HTTP' &&
                                        <div className="form-content-box">
                                            <div className="form-content-header">
                                                <p>Definition</p>
                                            </div>
                                            <div className="form-content-body">
                                                <div className="d-flex">
                                                    <div className="flex-50 pd-r-10">
                                                        <div className="form-group">
                                                            <label className="form-group-label">Base Path :</label>
                                                            <input type="text" id="basePath" name="basePath" className="form-control" />
                                                        </div>
                                                    </div>
                                                    <div className="flex-50 pd-l-10">
                                                        <div className="form-group">
                                                            <label className="form-group-label">Path :</label>
                                                            <input type="text" id="path" name="path" className="form-control" />
                                                        </div>
                                                    </div>
                                                    <div className="flex-33 pd-r-10">
                                                        <div className="form-group">
                                                            <label className="form-group-label">Sink Type :</label>
                                                            <select className="form-control">
                                                                <option>Select</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div className="flex-33 pd-l-5 pd-r-5">
                                                        <div className="form-group">
                                                            <label className="form-group-label">Category :</label>
                                                            <select className="form-control">
                                                                <option>Select</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div className="flex-33 pd-r-10">
                                                        <div className="form-group">
                                                            <label className="form-group-label">Topic :</label>
                                                            <input type="text" id="path" name="categoryTopic" className="form-control" />
                                                        </div>
                                                    </div>
                                                    <div className="flex-100">
                                                        <div className="alert alert-info note-text">
                                                            <p>1. This is a HTTP POST. Please pass this API Key as an HTTP Header X-API-KEY</p>
                                                        </div>
                                                    </div>
                                                    <div className="flex-100">
                                                        <div className="form-group">
                                                            <label className="form-group-label">API Key :</label>
                                                            <div className="input-group">
                                                                <input type="text" id="apiKey" name="apiKey" className="form-control" />
                                                                <div className="input-group-append pt-2">
                                                                    <button className="btn-transparent btn-transparent-green" data-tooltip data-tooltip-text="Refresh" data-tooltip-place="bottom">
                                                                        <i className="far fa-redo"></i>
                                                                    </button>
                                                                    <button className="btn-transparent btn-transparent-red" data-tooltip data-tooltip-text="Delete" data-tooltip-place="bottom">
                                                                        <i className="far fa-trash-alt"></i>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    }

                                    {/* EXCEl */}
                                    {this.props.match.params.connector === 'EXCEl' &&
                                        <div className="form-content-box">
                                            <div className="form-content-header">
                                                <p>Definition</p>
                                            </div>
                                            <div className="form-content-body">
                                                <div className="connector-form-list form-group">
                                                    <label className="form-group-label">
                                                        <button className="btn btn-primary"><i className="far fa-plus"></i></button>Add File(s) :
                                                    </label>
                                                    <div className="connector-form-body">
                                                        <div className="d-flex pd-r-30">
                                                            <label className="form-group-label flex-50">URL :</label>
                                                            <label className="form-group-label flex-50">Name :</label>
                                                        </div>
                                                        <ul className="list-style-none d-flex">
                                                            <li className="flex-50"><input type="text" id="url" name="url" className="form-control" /></li>
                                                            <li className="flex-50"><input type="text" id="name" name="name" className="form-control" /></li>
                                                            <div className="button-group">
                                                                <button className="btn-transparent btn-transparent-red" data-tooltip data-tooltip-text="Delete" data-tooltip-place="bottom">
                                                                    <i className="far fa-trash-alt"></i>
                                                                </button>
                                                            </div>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    }

                                    {/* JSON */}
                                    {this.props.match.params.connector === 'JSON' &&
                                        <div className="form-content-box">
                                            <div className="form-content-header">
                                                <p>Definition</p>
                                            </div>
                                            <div className="form-content-body">
                                                <div className="connector-form-list form-group">
                                                    <label className="form-group-label">
                                                        <button className="btn btn-primary"><i className="far fa-plus"></i></button>
                                                        Add File(s) :
                                                    </label>
                                                    <div className="connector-form-body">
                                                        <div className="d-flex pd-r-30">
                                                            <label className="form-group-label flex-50">URL :</label>
                                                            <label className="form-group-label flex-50">Name :</label>
                                                        </div>
                                                        <ul className="list-style-none d-flex">
                                                            <li className="flex-50"><input type="text" id="url" name="url" className="form-control" /></li>
                                                            <li className="flex-50"><input type="text" id="name" name="name" className="form-control" /></li>
                                                            <div className="button-group">
                                                                <button className="btn-transparent btn-transparent-red" data-tooltip data-tooltip-text="Delete" data-tooltip-place="bottom">
                                                                    <i className="far fa-trash-alt"></i>
                                                                </button>
                                                            </div>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    }

                                    {/* CSV */}
                                    {this.props.match.params.connector === 'CSV' &&
                                        <div className="form-content-box">
                                            <div className="form-content-header">
                                                <p>Definition</p>
                                            </div>
                                            <div className="form-content-body">
                                                <div className="connector-form-list form-group">
                                                    <label className="form-group-label">
                                                        <button className="btn btn-primary"><i className="far fa-plus"></i></button>Add File(s) :
                                                    </label>
                                                    <div className="connector-form-body">
                                                        <div className="d-flex pd-r-30">
                                                            <label className="form-group-label flex-50">URL :</label>
                                                            <label className="form-group-label flex-50">Name :</label>
                                                        </div>
                                                        <ul className="list-style-none d-flex">
                                                            <li className="flex-50"><input type="text" id="url" name="url" className="form-control" /></li>
                                                            <li className="flex-50"><input type="text" id="name" name="name" className="form-control" /></li>
                                                            <div className="button-group">
                                                                <button className="btn-transparent btn-transparent-red" data-tooltip data-tooltip-text="Delete" data-tooltip-place="bottom">
                                                                    <i className="far fa-trash-alt"></i>
                                                                </button>
                                                            </div>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    }

                                    {/* ActiveMQ */}
                                    {this.props.match.params.connector === 'ACTIVEMQ' &&
                                        <div className="form-content-box">
                                            <div className="form-content-header">
                                                <p>Definition</p>
                                            </div>
                                            <div className="form-content-body">
                                                <div className="d-flex">
                                                    <div className="flex-90 pd-r-10">
                                                        <div className="form-group">
                                                            <label className="form-group-label">URL : </label>
                                                            <input type="text" id="url" name="url" className="form-control" />
                                                        </div>
                                                    </div>
                                                    <div className="flex-10 pd-l-5">
                                                        <div className="form-group">
                                                            <button type="button" className="btn btn-transparent-blue mr-t-25 w-100">
                                                                <i className="far fa-cog mr-r-5"></i>Test
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="d-flex">
                                                    <div className="flex-20 pd-l-10">
                                                        <div className="form-group">
                                                            <label className="form-group-label">Authentication :</label>
                                                            <div className="toggle-switch-wrapper pt-2">
                                                                <span>Disable</span>
                                                                <label className="toggle-switch">
                                                                    <input type="checkbox" id="isAuthenticated" />
                                                                    <span className="toggle-switch-slider"></span>
                                                                </label>
                                                                <span>Enable</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="flex-40 pd-r-5 pd-l-5">
                                                        <div className="form-group">
                                                            <label className="form-group-label">Username :</label>
                                                            <input type="text" id="userName" name="userName" className="form-control" />
                                                        </div>
                                                    </div>
                                                    <div className="flex-40 pd-l-10">
                                                        <div className="form-group">
                                                            <label className="form-group-label">Password :</label>
                                                            <input type="text" id="password" name="password" className="form-control" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="connector-form-list form-group">
                                                    <label className="form-group-label">
                                                        <button className="btn btn-primary"><i className="far fa-plus"></i></button>
                                                        Add Destination(s) :
                                                    </label>
                                                    <div className="connector-form-body">
                                                        <div className="d-flex pd-r-30">
                                                            <label className="form-group-label flex-50">Name :</label>
                                                            <label className="form-group-label flex-50">Type :</label>
                                                        </div>
                                                        <ul className="list-style-none d-flex">
                                                            <li className="flex-50"><input type="text" id="name" name="name" className="form-control" /></li>
                                                            <li className="flex-50"><input type="text" id="type" name="type" className="form-control" /></li>
                                                            <div className="button-group">
                                                                <button className="btn-transparent btn-transparent-red" data-tooltip data-tooltip-text="Delete" data-tooltip-place="bottom">
                                                                    <i className="far fa-trash-alt"></i>
                                                                </button>
                                                            </div>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div className="connector-form-list form-group">
                                                    <label className="form-group-label">
                                                        <button className="btn btn-primary"><i className="far fa-plus"></i></button>Add Destination(s) :
                                                    </label>
                                                    <div className="connector-form-body">
                                                        <div className="d-flex pd-r-30">
                                                            <label className="form-group-label flex-40">Name :</label>
                                                            <label className="form-group-label flex-40">Type :</label>
                                                            <label className="form-group-label flex-20">TTL TIme :</label>
                                                        </div>
                                                        <ul className="list-style-none d-flex">
                                                            <li className="flex-40"><input type="text" id="name" name="name" className="form-control" /></li>
                                                            <li className="flex-40"><input type="text" id="type" name="type" className="form-control" /></li>
                                                            <li className="flex-20"><input type="number" id="second" name="second" className="form-control" /></li>
                                                            <div className="button-group">
                                                                <button className="btn-transparent btn-transparent-red" data-tooltip data-tooltip-text="Delete" data-tooltip-place="bottom">
                                                                    <i className="far fa-trash-alt"></i>
                                                                </button>
                                                            </div>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    }
                                </div>

                                <div className="form-info-wrapper">
                                    <div className="form-info-body">
                                        <div className="form-info-icon">
                                            <img src={`https://content.iot83.com/m83/dataConnectors/${this.props.history.location.state.connectorImage}`} />
                                        </div>
                                        <h5>{this.props.match.params.connector}</h5>
                                        <ul className="list-style-none form-info-list">
                                            <li><p>{this.props.history.location.state.connectorDescription}</p></li>
                                            <li><p className="text-gray font-weight-bold">For more detailed information, please refer the set-up manual that you would have received as a part of the registration email.</p></li>
                                        </ul>
                                    </div>
                                    <div className="form-info-footer">
                                        <button type='button' className="btn btn-light" onClick={() => this.props.history.push(`/connectors/${this.props.match.params.connector}`)}>Cancel</button>
                                        <button type='submit' className="btn btn-primary" disabled={this.saveUpdateValidator()}>{this.props.match.params.id ? "Update" : "Save"}</button>
                                    </div>
                                </div>
                            </form></div>
                    </React.Fragment>
                }

                {this.state.isOpen &&
                    <NotificationModal
                        type={this.state.modalType}
                        message2={this.state.message2}
                        onCloseHandler={this.onCloseHandler}
                    />
                }

            </React.Fragment >
        );
    }
}

AddOrEditConnector.propTypes = {
    dispatch: PropTypes.func.isRequired
};

let allSelectors = {}
Object.entries(SELECTORS).map(([key, value]) => {
    allSelectors[key] = value()
})

const mapStateToProps = createStructuredSelector(allSelectors);


function mapDispatchToProps(dispatch) {
    let allActions = { dispatch }
    Object.entries(ACTIONS).map(([key, value]) => {
        allActions[key] = (...args) => dispatch(value(...args))
    })
    return allActions;
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "newAddOrEditConnector", reducer });
const withSaga = injectSaga({ key: "newAddOrEditConnector", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(AddOrEditConnector);
