/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/*
 *
 * DeviceDashboardsAlarms constants
 *
 */

export const RESET_TO_INITIAL_STATE = "app/DeviceDashboardsAlarms/RESET_TO_INITIAL_STATE";
export const RESET_TO_INITIAL_STATE_PROPS = "app/DeviceDashboardsAlarms/RESET_TO_INITIAL_STATE_PROPS";


export const GET_CONNECTORS_LIST = {
    action : 'app/DeviceDashboardsAlarms/GET_CONNECTORS_LIST',
    success: "app/DeviceDashboardsAlarms/GET_CONNECTORS_LIST_SUCCESS",
    failure: "app/DeviceDashboardsAlarms/GET_CONNECTORS_LIST_FAILURE",
    urlKey: "deviceTypeList",
    successKey: "deviceTypeListSuccess",
    failureKey: "deviceTypeListFailure",
    actionName: "deviceTypeList",
    actionArguments : []
}

export const GET_ALARM_FILTER_DATA = {
    action : 'app/DeviceDashboardsAlarms/GET_ALARM_FILTER_DATA',
    success: "app/DeviceDashboardsAlarms/GET_ALARM_FILTER_DATA_SUCCESS",
    failure: "app/DeviceDashboardsAlarms/GET_ALARM_FILTER_DATA_FAILURE",
    urlKey: "getAlarmFilterData",
    successKey: "getAlarmFilterDataSuccess",
    failureKey: "getAlarmFilterDataFailure",
    actionName: "getAlarmFilterData",
    actionArguments : ["payload"]
}

export const GET_DEVICE_TAGS = {
    action : 'app/DeviceDashboardsAlarms/GET_DEVICE_TAGS',
    success: "app/DeviceDashboardsAlarms/GET_DEVICE_TAGS_SUCCESS",
    failure: "app/DeviceDashboardsAlarms/GET_DEVICE_TAGS_FAILURE",
    urlKey: "getAllTags",
    successKey: "getDeviceTagsSuccess",
    failureKey: "getDeviceTagsFailure",
    actionName: "getDeviceTags",
    actionArguments : ["connectorId"]
}

export const UPDATE_ALARM_STATUS = {
    action : 'app/DeviceDashboardsAlarms/UPDATE_ALARM_STATUS',
    success: "app/DeviceDashboardsAlarms/UPDATE_ALARM_STATUS_SUCCESS",
    failure: "app/DeviceDashboardsAlarms/UPDATE_ALARM_STATUS_FAILURE",
    urlKey: "updateAlarmStatus",
    successKey: "updateAlarmStatusSuccess",
    failureKey: "updateAlarmStatusFailure",
    actionName: "updateAlarmStatus",
    actionArguments : ["payload", "alarmDetails"]
}
