/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * DeviceDashboardsAlarms
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import ReactSelect from "react-select";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import { allSelectors as SELECTORS } from "./selectors";
import { allActions as ACTIONS } from './actions'
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import reducer from "./reducer";
import saga from "./saga";
import { cloneDeep, startCase } from "lodash";
import AddNewButton from "../../components/AddNewButton/Loadable"
import {convertTimestampToDate, getTimeDifference, isNavAssigned } from "../../commonUtils";
import SearchBarWithSuggestion from "../../components/SearchBarWithSuggestion";
import CollapseOrShowAllFilters from "../../components/CollapseOrShowAllFilters";

let DEFAULT_FILTERS = {
    keyword: "",
    groupId: [],
    tagId: [],
    alarmType: [],
    alarmStatus: [],
}

/* eslint-disable react/prefer-stateless-function */
export class DeviceDashboardsAlarms extends React.Component {

    state = {
        filterExpand: true,
        deviceTypeList: [],
        deviceGroups: [],
        filters: cloneDeep(DEFAULT_FILTERS),
        deviceTags: [],
        alarmList: [],
        //duration: 4320,
        isFetching: true,
        isFetchingAlarmList: false,
        selectedDeviceTypes: [],
    }


    componentDidMount() {
        this.props.deviceTypeList()
    }

    static getDerivedStateFromProps(nextProps, state) {
        let newProp = Object.keys(SELECTORS).find(prop => nextProps[prop])
        if (newProp) {
            let propData = nextProps[newProp].error ? nextProps[newProp].error : nextProps[newProp].response;
            if (nextProps.deviceTypeListSuccess) {
                let selectedDeviceTypes = state.selectedDeviceTypes;
                let deviceTypeList = propData
                if (deviceTypeList.length) {
                    let objFilterList = []
                    deviceTypeList.forEach(i => {
                        let objFilter = {}
                        objFilter.label = i.name
                        objFilter.value = i.id
                        objFilterList.push(objFilter);
                    })
                    selectedDeviceTypes = objFilterList
                    //selectedDeviceTypes = [{label: deviceTypeList[0].name, value:  deviceTypeList[0].id}]
                }
                return {
                    deviceTypeList,
                    isFetching: false,
                    selectedDeviceTypes
                }
            }
            else if (nextProps.deviceTypeListFailure) {
                return { isFetching: false }
            }
            else if (nextProps.getDeviceTagsSuccess) {
                return {
                    isFetchingTags: false,
                    deviceTags: propData,
                }
            }
            else if (nextProps.getDeviceTagsFailure) {
                return {
                    isFetchingTags: false,
                }
            }
            else if (nextProps.getAlarmFilterDataSuccess) {
                let alarmList = propData
                let alarmFilterList = propData
                alarmList.map(item => {
                    let deviceTypeDetails = state.deviceTypeList.find(deviceType => deviceType.id === item.deviceTypeId);
                    item.deviceTypeName = deviceTypeDetails.name;
                    item.deviceGroupName = deviceTypeDetails.deviceGroups.find(deviceGroup => deviceGroup.deviceGroupId === item.deviceGroupId).name;
                    if (item.tagId) {
                        item.tagName = deviceTypeDetails.tags.find(tag => tag.id === item.tagId).tag;
                    }
                })
                return {
                    alarmList,
                    alarmFilterList,
                    isFetching: false,
                    isFetchingAlarmList: false
                }
            }
            else if (nextProps.updateAlarmStatusSuccess || nextProps.updateAlarmStatusFailure) {
                let { _uniqueDeviceId, alarmType, alarmAttribute, actionType } = nextProps.updateAlarmStatusSuccess ? nextProps.updateAlarmStatusSuccess.alarmDetails : nextProps.updateAlarmStatusFailure.addOns.alarmDetails
                let alarmList = cloneDeep(state.alarmList)
                let requiredDevice = alarmList.find(device => device._uniqueDeviceId === _uniqueDeviceId)
                let requiredAlarmsArr = requiredDevice.alarms[alarmType]
                let requiredAlarmIndex = requiredAlarmsArr.findIndex(alarm => alarm.attr === alarmAttribute)
                requiredAlarmsArr[requiredAlarmIndex].loaderType = null
                if (actionType === "acknowledge") {
                    requiredAlarmsArr[requiredAlarmIndex].acknowledged = Boolean(nextProps.updateAlarmStatusSuccess);
                    requiredAlarmsArr[requiredAlarmIndex].timeAcknowledged = new Date().getTime();
                }
                else if (nextProps.updateAlarmStatusSuccess) {
                    requiredAlarmsArr.splice(requiredAlarmIndex, 1);
                    if (requiredAlarmsArr.length === 0)
                        delete requiredDevice.alarms[alarmType];
                    alarmList = alarmList.filter(alarmObject => {
                        let count = 0, alarmsObj = alarmObject.alarms;
                        Object.values(alarmsObj).map(alarmsArr => {
                            count += alarmsArr.length
                        })
                        return Boolean(count)
                    })
                }
                return {
                    alarmList
                }
            }
        }
        return null
    }


    componentDidUpdate(prevProps, prevState) {
        if (this.state.selectedDeviceTypes !== prevState.selectedDeviceTypes) {
            this.deviceTypeChangeHandler()
        }
        let newProp = Object.keys(SELECTORS).find(prop => this.props[prop])
        if (newProp) {
            if (["updateAlarmStatusSuccess", "updateAlarmStatusFailure"].includes(newProp)) {
                let propData = this.props[newProp].error || this.props[newProp].response
                let type = newProp.includes("Failure") ? "error" : "success"
                this.props.showNotification(type, propData)
            }
            this.props.resetToInitialStateProps(newProp)
        }
    }

    getActiveClass = (key, value) => {
        return this.state.filters[key].includes(value) ? "active" : ""
    }

    filterChangeHandler(key, value) {
        let filters = cloneDeep(this.state.filters)
        let previouslyCheckedIndex = filters[key].indexOf(value)
        if (Array.isArray(value)) {
            filters[key] = []
            if (key === "alarmType") {
                filters.alarmStatus = []
            }
        } else {
            if (previouslyCheckedIndex != -1) {
                filters[key].splice(previouslyCheckedIndex, 1)
            } else {
                filters[key].push(value)
            }
        }
        this.setState({
            filters,
        })
    }


    deviceTypeChangeHandler = (isForRefreshing) => {
        let deviceGroups = [],
            deviceTags = [],
            payload = {
                deviceTypeIds: [],
            };

        if (isForRefreshing && this.state.selectedDeviceTypes.length == 0) {
            this.setState({
                isFetchingAlarmList: true,
                isFetching: true,
            }, () => {
                this.props.getAlarmFilterData(payload)
            })
        }

        if (this.state.selectedDeviceTypes.length > 0) {
            this.state.selectedDeviceTypes.map(deviceType => {
                let selectedDeviceType = this.state.deviceTypeList.filter(connector => connector.id === deviceType.value)[0];
                Array.prototype.push.apply(deviceGroups, selectedDeviceType.deviceGroups);
                Array.prototype.push.apply(deviceTags, selectedDeviceType.tags);
            })
            let stateObj = isForRefreshing ? {
                isFetchingAlarmList: true,
                isFetching: true,
            } : {
                    isFetchingAlarmList: true,
                    alarmList: [],
                    deviceGroups,
                    deviceTags,
                    filters: cloneDeep(DEFAULT_FILTERS)
                }
            this.state.selectedDeviceTypes.map(deviceType => {
                payload.deviceTypeIds.push(deviceType.value);
            })

            this.setState(stateObj, () => {
                this.props.getAlarmFilterData(payload)
            })
        } else {
            this.setState({
                alarmList: [],
                deviceGroups,
                deviceTags,
                filters: cloneDeep(DEFAULT_FILTERS)
            })
        }

    }

    getFilteredItems = () => {
        let allItems = this.state.alarmList
        if (Object.values(this.state.filters).some(filter => filter.length)) {
            allItems = cloneDeep(allItems)
            return allItems.filter(device => {
                let groupIdValidation = !this.state.filters.groupId.length || this.state.filters.groupId.includes(device.deviceGroupId)
                let tagIdValidation = !this.state.filters.tagId.length || this.state.filters.tagId.includes(device.tagId)
                let keywordValidation = !this.state.filters.keyword || (([(device.name || "").toLowerCase(), device._uniqueDeviceId.toLowerCase()].includes(this.state.filters.keyword.toLowerCase())))
                let alarmValidation = true
                if (this.state.filters.alarmType.length || this.state.filters.alarmStatus.length) {
                    let filterAlarmsByStatus = (alarms) => {
                        return alarms.filter(alarm => this.state.filters.alarmStatus[0] === "acknowledged" ? alarm.acknowledged : !alarm.acknowledged)
                    }
                    if (this.state.filters.alarmType.length) {
                        let alarms = {}
                        this.state.filters.alarmType.map(type => {
                            if (device.alarms[type]) {
                                if (this.state.filters.alarmStatus.length == 1) {
                                    let statusValidatedAlarms = filterAlarmsByStatus(device.alarms[type])
                                    if (statusValidatedAlarms.length) {
                                        alarms[type] = statusValidatedAlarms
                                    }
                                }
                                else {
                                    alarms[type] = device.alarms[type]
                                }
                            }
                        })
                        device.alarms = alarms
                    }
                    else if (this.state.filters.alarmStatus.length == 1) {
                        Object.keys(device.alarms).map(alarmType => {
                            device.alarms[alarmType] = filterAlarmsByStatus(device.alarms[alarmType])
                            if (!device.alarms[alarmType].length) delete device.alarms[alarmType]
                        })
                    }
                    alarmValidation = this.getAlarmsCount(device.alarms)
                }
                return groupIdValidation && tagIdValidation && keywordValidation && alarmValidation
            })
        }
        return allItems
    }

    searchHandler = (value) => {
        let filters = cloneDeep(this.state.filters)
        filters.keyword = value
        this.setState({ filters })
    }

    getAlarmsCount = (alarmsObj) => {
        let count = 0
        Object.values(alarmsObj).map(alarmsArr => {
            count += alarmsArr.length
        })
        return count
    }

    handleSelectedDevice = (newSelectedDevice) => {
        let selectedDevice = this.state.selectedDevice;
        selectedDevice = selectedDevice === newSelectedDevice ? null : newSelectedDevice;
        this.setState({
            selectedDevice
        })
    }


    updateAlarmStatus = (alarmAttribute, alarmIndex, alarmType, { deviceTypeId, _uniqueDeviceId }, actionType) => {
        let alarmList = cloneDeep(this.state.alarmList)
        let requiredAlarmObj = alarmList.find(device => device._uniqueDeviceId === _uniqueDeviceId)
        requiredAlarmObj.alarms[alarmType][alarmIndex].loaderType = actionType
        let payload = {
            "deviceTypeId": deviceTypeId,
            "deviceId": _uniqueDeviceId,
            alarmAttribute,
            alarmType,
        }
        if (actionType === "acknowledge") {
            payload.ackDuration = Date.now() + 600000
            payload.acknowledged = true
        }
        else {
            payload.addressed = true
        }
        let alarmDetails = {
            _uniqueDeviceId,
            alarmType,
            alarmAttribute,
            actionType
        }
        this.setState({
            alarmList
        }, () => this.props.updateAlarmStatus(payload, alarmDetails))
    }

    getPercentAgeChange = (changedVal, origVal) => {
        return Math.abs(((Number(changedVal.toFixed(2)) - Number(origVal.toFixed(2))) / Number(origVal.toFixed(2)) * 100).toFixed(2));
    }

    getStatementForAlarm = (alarmValue, alarmLow, alarmHigh, unit, operator) => {
        let rateOfChange;
        let thresholdText;
        if (typeof operator != 'undefined' && operator === 'GREATER_THAN') {
            rateOfChange = alarmLow === 0 ? alarmValue : this.getPercentAgeChange(alarmValue, alarmLow);
            thresholdText = ' % ' + ' more than ' + alarmLow.toFixed(2) + ' ' + unit;
        } else {
            rateOfChange = alarmHigh === 0 ? alarmValue : this.getPercentAgeChange(alarmValue, alarmHigh);
            thresholdText = ' % ' + ' less than ' + alarmHigh.toFixed(2) + ' ' + unit;
        }
        return rateOfChange + thresholdText;
    }

    getDeviceTypes = () => {
        let deviceTypeList = this.state.deviceTypeList,
            list = [];

        deviceTypeList.map(deviceType => {
            list.push({
                label: deviceType.name,
                value: deviceType.id,
            })
        })
        return list;
    }

    deviceTypeChange = (selectedDeviceTypes) => {
        this.setState({
            selectedDeviceTypes,
        })
    }

    filterExpandCollapseHandler = () => {
        this.setState({
            filterExpand: this.state.filterExpand ? false : true
        })
    }

    render() {
        let filteredAlarms = this.getFilteredItems();
        return (
            <React.Fragment>
                <Helmet>
                    <title>Device Alarms</title>
                    <meta name="description" content="Description of M83-DeviceAlarms"
                    />
                </Helmet>

                <header className="content-header d-flex">
                    <div className="flex-60">
                        <h6>Device Alarms</h6>
                    </div>
                    <div className="flex-40 text-right">
                        <div className="content-header-group">
                            <button className="btn btn-light" data-tooltip data-tooltip-text="Refresh" data-tooltip-place="bottom" disabled={this.state.isFetchingAlarmList || this.state.isFetching} onClick={() => this.deviceTypeChangeHandler(true)}><i className="far fa-sync-alt"></i></button>
                            <button className={`btn btn-light ${this.state.filterExpand && "active"}`} data-tooltip data-tooltip-text="Filters" data-tooltip-place="bottom" disabled={this.state.isFetching || !Boolean(this.state.deviceTypeList.length) } onClick={() => this.filterExpandCollapseHandler()} ><i className="far fa-filter"></i></button>
                        </div>
                    </div>
                </header>

                {this.state.isFetching ?
                    <Loader /> :
                    this.state.deviceTypeList.length > 0 ?
                        <React.Fragment>
                            {this.state.filterExpand &&
                                <div className="device-filter-wrapper">
                                    <div className="device-filter-box">
                                        <h5>Filters <CollapseOrShowAllFilters /></h5>
                                        <h6>Specify Your Search Criteria</h6>
                                        <div className="form-group mb-0">
                                            <div className="search-wrapper">
                                                <SearchBarWithSuggestion value={this.state.filters.keyword} suggestionKeys={["name", "_uniqueDeviceId"]} onChange={this.searchHandler} data={this.state.alarmList} />
                                            </div>
                                        </div>
                                    </div>

                                    <div className="device-filter-box">
                                        <p>Device Type <i className="fas fa-angle-down device-filter-collapse" data-toggle="collapse" data-target="#deviceType__filter__"></i></p>
                                        <div className="collapse show" id="deviceType__filter__">
                                            <div className="device-filter-body">
                                                <div className="form-group mb-0">
                                                    <ReactSelect
                                                        id="deviceTypes"
                                                        options={this.getDeviceTypes()}
                                                        value={this.state.selectedDeviceTypes}
                                                        onChange={this.deviceTypeChange}
                                                        isMulti={true}
                                                        isDisabled={this.state.isFetchingAlarmList}
                                                        className="form-control-multi-select">
                                                    </ReactSelect>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="device-filter-box">
                                        <p>Groups <i className="fas fa-angle-down device-filter-collapse" data-toggle="collapse" data-target="#groups__filter__"></i>
                                            {Boolean(this.state.filters.groupId.length) && <button className="btn btn-link" onClick={() => this.state.filters.groupId && this.filterChangeHandler("groupId", [])}>Reset</button>}
                                        </p>
                                        <div className="collapse show" id="groups__filter__">
                                            <div className="device-filter-body">
                                                <ul className="d-flex list-style-none device-filter-group">
                                                    {this.state.deviceGroups.length ? this.state.deviceGroups.map(deviceGroup =>
                                                        <li key={deviceGroup.deviceGroupId} className={this.getActiveClass("groupId", deviceGroup.deviceGroupId)} onClick={() => this.filterChangeHandler("groupId", deviceGroup.deviceGroupId)}>{deviceGroup.name}</li>) :
                                                        <li><p>No Device Groups Found !</p></li>}
                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="device-filter-box">
                                        <p>Tags <i className="fas fa-angle-down device-filter-collapse" data-toggle="collapse" data-target="#tags__filter__"></i>
                                            {Boolean(this.state.filters.tagId.length) && <button className="btn btn-link" onClick={() => this.state.filters.tagId.length && this.filterChangeHandler("tagId", [])}>Reset</button>}
                                        </p>
                                        <div className="collapse show" id="tags__filter__">
                                            <div className="device-filter-body">
                                                <ul className="d-flex list-style-none device-filter-group">
                                                    {this.state.isFetchingTags ? <div className="text-center flex-100"><i className="fad fa-sync-alt fa-spin f-13 text-theme"></i></div> :
                                                        this.state.deviceTags.length ? this.state.deviceTags.map(deviceTag =>
                                                            <li key={deviceTag.id}
                                                                className={this.getActiveClass("tagId", deviceTag.id)}
                                                                onClick={() => this.filterChangeHandler("tagId", deviceTag.id)}>{deviceTag.tag}</li>
                                                        ) :
                                                            <li> <p>No Tags found.</p></li>
                                                    }
                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                    {/*<div className="device-filter-box">
                                    <p>Duration <i className="fas fa-angle-down device-filter-collapse" data-toggle="collapse" data-target="#duration__filter__"></i></p>
                                    <div className="collapse show" id="duration__filter__">
                                        <div className="device-filter-body">
                                            <div className="form-group mb-0">
                                                <select id="duration" className="form-control" value={this.state.duration} onChange={this.durationChangeHandler}>
                                                    <option value={60}>Last 1 hour</option>
                                                    <option value={180}>Last 3 hours</option>
                                                    <option value={360}>Last 6 hours</option>
                                                    <option value={540}>Last 9 hours</option>
                                                    <option value={720}>Last 12 hours</option>
                                                    <option value={1440}>Last 1 day</option>
                                                    <option value={4320}>Last 3 days</option>
                                                    <option value={10080}>Last 7 days</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>*/}

                                    <div className="device-filter-box">
                                        <p>Alarms Type <i className="fas fa-angle-down device-filter-collapse" data-toggle="collapse" data-target="#alarms__type__filter__"></i>
                                            {Boolean(this.state.filters.alarmType.length) && <button className="btn btn-link" onClick={() => this.state.filters.alarmType.length && this.filterChangeHandler("alarmType", [])}>Reset</button>}
                                        </p>
                                        <div className="collapse show" id="alarms__type__filter__">
                                            <div className="device-filter-body">
                                                <ul className="d-flex list-style-none device-filter-alarms">
                                                    <li className={this.getActiveClass("alarmType", 'critical')} onClick={() => this.filterChangeHandler("alarmType", 'critical')}>
                                                        <span className="bg-critical">
                                                            <i className="far fa-exclamation-triangle"></i>
                                                        </span>
                                                        <p>Critical</p>
                                                    </li>
                                                    <li className={this.getActiveClass("alarmType", 'major')} onClick={() => this.filterChangeHandler("alarmType", 'major')}>
                                                        <span className="bg-major">
                                                            <i className="far fa-exclamation-square"></i>
                                                        </span>
                                                        <p>Major</p>
                                                    </li>
                                                    <li className={this.getActiveClass("alarmType", 'minor')} onClick={() => this.filterChangeHandler("alarmType", 'minor')}>
                                                        <span className="bg-minor">
                                                            <i className="far fa-exclamation-circle"></i>
                                                        </span>
                                                        <p>Minor</p>
                                                    </li>
                                                    <li className={this.getActiveClass("alarmType", 'warning')} onClick={() => this.filterChangeHandler("alarmType", 'warning')}>
                                                        <span className="bg-alarm-warning">
                                                            <i className="far fa-exclamation"></i>
                                                        </span>
                                                        <p>Warning</p>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="device-filter-box">
                                        <p>Alarms State <i className="fas fa-angle-down device-filter-collapse" data-toggle="collapse" data-target="#alarms_state__filter__"></i>
                                            {Boolean(this.state.filters.alarmStatus.length) && <button className="btn btn-link" onClick={() => (this.state.filters.alarmStatus.length) && this.filterChangeHandler("alarmStatus", [])}>Reset</button>}
                                        </p>
                                        <div className="collapse show" id="alarms_state__filter__">
                                            <div className="device-filter-body">
                                                <ul className="d-flex list-style-none device-filter-alarms">
                                                    <li className={this.getActiveClass("alarmStatus", "acknowledged")} onClick={() => this.filterChangeHandler("alarmStatus", "acknowledged")}>
                                                        <span className="bg-green">
                                                            <i className="far fa-alarm-snooze"></i>
                                                        </span>
                                                        <p>Snoozed</p>
                                                    </li>
                                                    <li className={this.getActiveClass("alarmStatus", "unAcknowledged")} onClick={() => this.filterChangeHandler("alarmStatus", "unAcknowledged")}>
                                                        <span className="bg-yellow">
                                                            <i className="far fa-bell-on"></i>
                                                        </span>
                                                        <p>Active</p>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>}

                            <div className="content-body device-filter-content" style={{ paddingRight: this.state.filterExpand ? 270 : 12 }}>
                                {this.state.isFetchingAlarmList ?
                                    <div className="inner-loader-wrapper h-100">
                                        <div className="inner-loader-content">
                                            <i className="fad fa-sync-alt fa-spin"></i>
                                        </div>
                                    </div> :
                                    filteredAlarms.length ?
                                        filteredAlarms.map((device, index) => (
                                            <div className="card device-alarm-wrapper" key={device._uniqueDeviceId}>
                                                <div className="card-header pd-l-15">
                                                    <h6>
                                                        Device Type : <strong onClick={() => {isNavAssigned("deviceTypes") && this.props.history.push(`addOrEditDeviceType/${device.deviceTypeId}`)}}>{device.deviceTypeName}</strong>
                                                        <span className="mr-3 ml-3 f-11 text-content">|</span>
                                                        {device.name ? 'Name' : 'Device Id'} : <strong onClick={() => this.props.history.push(`deviceDetails/${device.deviceTypeId}/${device._uniqueDeviceId}`)}>{device.name || device._uniqueDeviceId}</strong>
                                                        <span className="mr-3 ml-3 f-11 text-content">|</span>
                                                        Group : <strong onClick={() => {isNavAssigned("deviceGroups") && this.props.history.push(`deviceGroups/${device.deviceTypeId}/${device.deviceGroupId}`)}}>{device.deviceGroupName}</strong>
                                                        <span className="mr-3 ml-3 f-11 text-content">|</span>
                                                        Tag : <strong onClick={() => { device.tagId && isNavAssigned("deviceGroups") && this.props.history.push({ pathname: `/deviceGroups/${device.deviceTypeId}/${device.deviceGroupId}`, state: { childViewType: 'GROUP_TAGVIEW' } }) }}>{device.tagId ? device.tagName : '-'}</strong>
                                                    </h6>
                                                    <div className="alert alert-primary">{this.getAlarmsCount(device.alarms)}</div>
                                                </div>
                                                <div className="card-body">
                                                    {Object.entries(device.alarms).map(([alarmType, alarms], i) =>
                                                        <div className="device-alarm-inner-wrapper" key={i}>
                                                            <div className="card-header device-alarm-inner-header">
                                                                <span className={`device-alarm-icon text-${alarmType === "warning" ? "alarm-warning" : alarmType}`}>
                                                                    <i className={`fas ${alarmType === "critical" ? "fa-exclamation-triangle" : alarmType === "major" ? "fa-exclamation-square" : alarmType === "minor" ? "fa-exclamation-circle" : "fa-exclamation"}`}></i>
                                                                </span>
                                                                <h6 className="text-capitalize">{alarmType}</h6>
                                                                <div className="alert alert-primary">{alarms.length}</div>
                                                            </div>
                                                            <div className="card-body">
                                                                <ul className="device-alarm-list list-style-none">
                                                                    {Array.isArray(alarms) && alarms.map((alarm, alarmIndex) => {
                                                                        let alarmCount = alarm.count
                                                                        let tooltipVisibility = false
                                                                        if (Math.floor((alarm.count) / 1000) > 0) {
                                                                            if (alarm.count % 1000 == 0) {
                                                                                alarmCount = Math.floor(alarm.count / 1000) + "K"
                                                                            } else {
                                                                                alarmCount = Math.floor(alarm.count / 1000) + "K+"
                                                                                tooltipVisibility = true
                                                                            }
                                                                        }
                                                                        return (<li key={alarmIndex}>
                                                                            {alarm.count ?
                                                                                <div className="device-alarm-count" >
                                                                                    <i className="fad fa-bell text-orange"></i>
                                                                                    <span data-tooltip={tooltipVisibility} data-tooltip-text={alarm.count} data-tooltip-place="bottom" className="badge badge-danger">{alarmCount}</span>
                                                                                </div> :
                                                                                <div className="device-alarm-count">
                                                                                    <i className="fad fa-bell"></i>
                                                                                    <span className="badge badge-light">0</span>
                                                                                </div>
                                                                            }
                                                                            <h6>Last Recorded value of <strong>{alarm.displayName !== "" ? alarm.displayName : alarm.attr}</strong> was <strong> {alarm.value % 1 ? alarm.value.toFixed(2) : alarm.value} {alarm.unit}.</strong> <span className="ml-1 mr-1">|</span> This was <strong>{this.getStatementForAlarm(alarm.value, alarm.low, alarm.high, alarm.unit, alarm.operator)}</strong> (Configured Threshold)</h6>
                                                                            <p>
                                                                                <strong>Last Reported : </strong>{convertTimestampToDate(alarm.timeRaised)} <span className="text-cyan">({getTimeDifference(alarm.timeRaised)})</span>
                                                                                {alarm.acknowledged && <React.Fragment><span className="mr-1 ml-1">|</span><strong>Snoozed At : </strong> {convertTimestampToDate(alarm.timeAcknowledged)} <span className="text-cyan">({getTimeDifference(alarm.timeAcknowledged)})</span></React.Fragment>}
                                                                            </p>
                                                                            <div className="button-group">
                                                                                <button type="button" disabled={alarm.loaderType || alarm.acknowledged} className="btn btn-outline-success" onClick={() => this.updateAlarmStatus(alarm.attr, alarmIndex, alarmType, device, "acknowledge")}>{alarm.acknowledged ? "Snoozed" : alarm.loaderType === "acknowledge" ? "Snoozing.." : "Snooze 10 mins"}</button>
                                                                                <button type="button" disabled={alarm.loaderType} className="btn btn-outline-primary" onClick={() => this.updateAlarmStatus(alarm.attr, alarmIndex, alarmType, device, "clear")} >{alarm.loaderType === "clear" ? "Clearing.." : "Clear"}</button>
                                                                            </div>
                                                                        </li>)
                                                                    })}
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    )}
                                                </div>
                                            </div>
                                        )) :
                                        <AddNewButton
                                            text1="No alarm(s) available"
                                            text2="Please try with different search criteria."
                                            imageIcon="noSearchResult.png"
                                            addButtonEnable={false}
                                        />
                                }
                            </div>
                        </React.Fragment>
                        :
                        <div className="content-body">
                            <AddNewButton
                                text1="No alarm(s) available"
                                text2="You haven't created any device type(s) yet. Please create a device type first."
                                addButtonEnable={isNavAssigned("deviceTypes")}
                                createItemOnAddButtonClick={() => this.props.history.push(`/addOrEditDeviceType`)}
                                imageIcon="addDeviceAlarms.png"
                            />
                        </div>
                }
            </React.Fragment>
        );
    }
}

DeviceDashboardsAlarms.propTypes = {
    dispatch: PropTypes.func.isRequired
};

let allSelectors = {}
Object.entries(SELECTORS).map(([key, value]) => {
    allSelectors[key] = value()
})

const mapStateToProps = createStructuredSelector(allSelectors)

function mapDispatchToProps(dispatch) {
    let allActions = { dispatch }
    Object.entries(ACTIONS).map(([key, value]) => {
        allActions[key] = (...args) => dispatch(value(...args))
    })
    return allActions
}


const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "deviceDashboardsAlarms", reducer });
const withSaga = injectSaga({ key: "deviceDashboardsAlarms", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(DeviceDashboardsAlarms);
