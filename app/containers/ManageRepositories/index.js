/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * ManageRepositories
 *
 */

import React from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {Helmet} from "react-helmet";
import {FormattedMessage} from "react-intl";
import {createStructuredSelector} from "reselect";
import {compose} from "redux";

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import makeSelectManageRepositories from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import messages from "./messages";
import ListingTable from "../../components/ListingTable/Loadable";

/* eslint-disable react/prefer-stateless-function */
export class ManageRepositories extends React.Component {
    state = {
        filteredList: [{
            name: "iot83/pymongo:pymongo",
            id: "4ee7e3c63dcc458999adec632f87077b",
            tagName: "pymongo image",
            owner: "Nisha Tanwar",
            createdBy: "Nisha Tanwar",
            createdAt: "12/29/2020, 10:45:18 AM",
            updatedBy: "Nisha Tanwar",
            updatedAt: "12/29/2020, 10:45:18 AM",
            state: "start"
        },{
            name: "iot83/test:asdfg",
            id: "4ee7e3c63dcc458999adec632f87077b",
            tagName: "test image",
            owner: "Nisha Tanwar",
            createdBy: "Nisha Tanwar",
            createdAt: "12/29/2020, 10:45:18 AM",
            updatedBy: "Nisha Tanwar",
            updatedAt: "12/29/2020, 10:45:18 AM",
            state: "stopped"
        }]
    }
    
    getColumns = () => {
        return [
            {
                Header: "Name", width: 30,
                cell: (row) => (
                    <React.Fragment>
                        <div className="list-view-icon"><i className="fab fa-docker text-dark-theme"></i></div>
                        <div className="list-view-icon-box">
                            <h6 className="text-theme fw-600">{row.name}</h6>
                            <p>{`${row.id || "-"}`}</p>
                        </div>
                    </React.Fragment>
                )
            },
            {
                Header: "Tag", width: 20,
                cell: (row) => (
                    <span className="alert alert-success list-view-badge">{row.tagName}</span>
                )
            },
            { Header: "Owner", width: 20, accessor: "owner" },
            { Header: "Created", width: 15, accessor: "createdAt" },
            { Header: "Updated", width: 15, accessor: "updatedAt" },
        ]
    }
    
    getStatusBarClass = (row) => {
        if (row.state === "start")
            return { className: "green", tooltipText: "Running" }
        return { className: "red", tooltipText: "Stopped" }
    }
    
    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>ManageRepositories</title>
                    <meta name="description" content="Description of ManageRepositories"/>
                </Helmet>
                
                <header className="content-header d-flex">
                    <div className="flex-60">
                        <h6>Manage Repositories -
                            <span className="content-header-badge-group">
                                <span className="content-header-badge-item">Allocated <span className="badge badge-pill badge-primary">5</span></span>
                                <span className="content-header-badge-item">Used <span className="badge badge-pill badge-success">5</span></span>
                                <span className="content-header-badge-item">Remaining <span className="badge badge-pill badge-warning">5</span></span>
                            </span>
                        </h6>
                    </div>
                    <div className="flex-40 text-right">
                        <div className="content-header-group">
                            <button className="btn btn-light" data-tooltip data-tooltip-text="Refresh" data-tooltip-place="bottom">
                                <i className="far fa-sync-alt"></i>
                            </button>
                        </div>
                    </div>
                </header>
                
                <div className="content-body">
                    <ListingTable
                        columns={this.getColumns()}
                        data={this.state.filteredList}
                        statusBar={this.getStatusBarClass}
                    />
                </div>
            </React.Fragment>
        );
    }
}

ManageRepositories.propTypes = {
    dispatch: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
    managerepositories: makeSelectManageRepositories()
});

function mapDispatchToProps(dispatch) {
    return {
        dispatch
    };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({key: "manageRepositories", reducer});
const withSaga = injectSaga({key: "manageRepositories", saga});

export default compose(
    withReducer,
    withSaga,
    withConnect
)(ManageRepositories);
