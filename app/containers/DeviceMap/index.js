/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * DeviceMap
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import * as SELECTORS from "./selectors";
import * as ACTIONS from "./actions";
import reducer from "./reducer";
import saga from "./saga";
import { isEqual, cloneDeep } from 'lodash'
import PduMapWithMarkerCluster from "../../components/PduMapWithMarkerCluster";
import TableWidget from "../../components/TableWidget";
let STATUS_TYPES = [
  {
    type: "online",
    displayName: "Online",
  },
  {
    type: "offline",
    displayName: "Offline",
  },
  {
    type: "disconnected",
    displayName: "Disconnected",
  },
]
/* eslint-disable react/prefer-stateless-function */
class DeviceMap extends React.Component {
  state = {
    deviceGroups: this.props.connectorId ? [] : this.props.data.deviceGroups,
    isFetchingGroups: Boolean(this.props.connectorId),
    filters: {
      groupId: "",
      tagId: [],
    },
    deviceTags: []
  }

  shouldComponentUpdate(nextProps, nextState) {
    return !isEqual(this.props.data, nextProps.data) || !isEqual(nextState, this.state) || !isEqual(nextProps.widgetProperties, this.props.widgetProperties)
  }

  componentDidMount() {
    this.props.connectorId && this.props.getDeviceGroups(this.props.connectorId)
  }

  static getDerivedStateFromProps(nextProps, state) {
    if (nextProps.getDeviceGroupsSuccess) {
      return {
        isFetchingGroups: false,
        deviceGroups: nextProps.getDeviceGroupsSuccess
      }
    }
    if (nextProps.getDeviceGroupsFailure) {
      return {
        isFetchingGroups: false,
        deviceGroupsError: nextProps.getDeviceGroupsFailure
      }
    }
    return null
  }

  filterChangeHandler(key, value) {
    let filters = cloneDeep(this.state.filters)
    let deviceTags = cloneDeep(this.state.deviceTags)
    let previouslyCheckedIndex = filters[key].indexOf(value)
    if (key === "groupId") {
      filters.groupId = previouslyCheckedIndex != -1 ? "" : value
      deviceTags = previouslyCheckedIndex != -1 ? [] : this.state.deviceGroups.find(group => group.id === value).tags
      filters.tagId = []
    }
    if (key === "tagId") {
      if (Array.isArray(value)) {
        filters.tagId = []
      }
      else {
        if (previouslyCheckedIndex != -1) {
          filters.tagId.splice(previouslyCheckedIndex, 1)
        }
        else {
          filters.tagId.push(value)
        }
      }
    }
    this.setState({
      filters,
      deviceTags
    })
  }

  statusFilterChangeHandler = (statusType) => {
    this.setState(prevState => ({
      status: statusType === prevState.status ? "" : statusType
    }))
  }

  getFilteredItems = () => {
    let allItems = this.props.data.devices
    if (this.state.filters.groupId || this.state.filters.tagId.length) {
      return allItems.filter(dashboard => {
        let groupIdValidation = !this.state.filters.groupId || dashboard.groupId === this.state.filters.groupId
        let tagIdValidation = !this.state.filters.tagId.length || this.state.filters.tagId.includes(dashboard.tagId)
        return groupIdValidation && tagIdValidation
      })
    }
    return allItems
  }

  render() {
    let filteredDevices = this.getFilteredItems()
    let onlineDevices = 0
    let offlineDevices = 0
    let disconnectedDevices = 0
    filteredDevices.map(device => {
      if (device.status === "online") onlineDevices++;
      else if (device.status === "offline") offlineDevices++;
      else disconnectedDevices++
    })
    return <div className="device-page-widget" id="deviceMapVarient_1">
      <ul className="list-style-none device-counter-list d-flex">
        <li className="flex-25">
          <div className="device-counter-card">
            <h1>{filteredDevices.length}</h1>
            <p>All devices</p>
            <h6>Total Count of the interior</h6>
            <div className="device-counter-icon">
              <img src={this.props.data.deviceType.image} />
            </div>
          </div>
        </li>
        <li className="flex-25">
          <div className="device-counter-card">
            <h1>{onlineDevices}</h1>
            <p>Online devices</p>
            <h6>Total Count of the interior</h6>
            <div className="device-counter-icon">
              <img src={this.props.data.deviceType.image} />
              <span className="text-green"><i className="fas fa-circle"></i></span>
            </div>
          </div>
        </li>
        <li className="flex-25">
          <div className="device-counter-card">
            <h1>{offlineDevices}</h1>
            <p>Offline devices</p>
            <h6>Total Count of the interior</h6>
            <div className="device-counter-icon">
              <img src={this.props.data.deviceType.image} />
              <span className="text-red"><i className="fas fa-circle"></i></span>
            </div>
          </div>
        </li>
        <li className="flex-25">
          <div className="device-counter-card">
            <h1>{disconnectedDevices}</h1>
            <p>Disconnected devices</p>
            <h6>Total Count of the interior</h6>
            <div className="device-counter-icon">
              <img src={this.props.data.deviceType.image} />
              <span className="text-primary"><i className="fas fa-wifi-slash"></i></span>
            </div>
          </div>
        </li>
      </ul>
      <div className="d-flex">
        <div className="flex-70 pd-r-10">
          <div className="card">
              <div className="card-header">
                  {this.props.widgetType === "deviceMap" ? `Device Map` : `Device List`}
                  <div className="device-status-button">
                      {STATUS_TYPES.map((status, i) => <span key={i} className={status.type === this.state.status ? "tag active" : "tag"} onClick={() => this.statusFilterChangeHandler(status.type)}>{status.displayName}</span>)}
                      <button className="btn" onClick={() => this.state.status && this.statusFilterChangeHandler(this.state.status)}><i className={`fas fa-filter ${this.state.status ? "text-primary" : ""}`}></i></button>
                  </div>
              </div>
            <div className="card-body">
                {this.props.widgetType === "deviceMap" ?
                    <div className="device-map-wrapper">
                        <PduMapWithMarkerCluster {...this.props} data={filteredDevices.filter(device => device.lat && (!this.state.status || device.status === this.state.status))} widgetProperties={this.props.widgetProperties} zoomLevel={3.5} />
                    </div>:
                    <TableWidget {...this.props} data={filteredDevices.filter(device => device.lat && (!this.state.status || device.status === this.state.status))} linkedPage={this.props.widgetProperties.detailPage} />
              }
            </div>
          </div>
        </div>

        <div className="flex-30 pd-l-10">
          <div className="card">
            <div className="card-header">
              Filters Device
            </div>
            <div className="card-body">
              <div className="device-tags-wrapper">
                <div className="device-tags-header">
                  <p>Device Groups</p>
                  <button className="btn" onClick={() => this.state.filters.groupId && this.filterChangeHandler("groupId", this.state.filters.groupId)}><i className={`fas fa-filter ${this.state.filters.groupId ? "text-primary" : ""}`}></i></button>
                </div>
                <div className="device-tags-body">
                  {this.state.isFetchingGroups ? <div className="inner-loader-wrapper" style={{ height: 59.13 }}>
                    <div className="inner-loader-content">
                      <i className="fad fa-sync-alt fa-spin"></i>
                    </div>
                  </div> : this.state.deviceGroups.length ?
                      this.state.deviceGroups.map(deviceGroup => <span key={deviceGroup.id} className={deviceGroup.id === this.state.filters.groupId ? "tag active" : "tag"} onClick={() => this.filterChangeHandler("groupId", deviceGroup.id)}>{deviceGroup.name}</span>) :
                      <p>No Device Groups</p>}
                </div>
              </div>
              <div className="device-tags-wrapper">
                <div className="device-tags-header">
                  <p>Device Tags</p>
                  <button className="btn" onClick={() => this.state.filters.tagId.length && this.filterChangeHandler("tagId", [])}><i className={`fas fa-filter ${this.state.filters.tagId.length ? "text-primary" : ""}`}></i></button>
                </div>
                <div className="device-tags-body">
                  {this.state.filters.groupId ?
                    this.state.deviceTags.length ? this.state.deviceTags.map(deviceTag =>
                      <span key={deviceTag.id} className={this.state.filters.tagId.includes(deviceTag.id) ? "tag active" : "tag"} onClick={() => this.filterChangeHandler("tagId", deviceTag.id)}>{deviceTag.name}</span>
                    ) : <p>No Tags found for the selected Device Group</p> :
                    <p>Select a Device Group to view Tags.</p>}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>;
  }
}

DeviceMap.propTypes = {
  dispatch: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
  getDeviceGroupsSuccess: SELECTORS.getDeviceGroupsSuccess(),
  getDeviceGroupsFailure: SELECTORS.getDeviceGroupsFailure(),
})

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    getDeviceGroups: (connectorId) => dispatch(ACTIONS.getDeviceGroups(connectorId)),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

const withReducer = injectReducer({ key: "deviceMap", reducer });
const withSaga = injectSaga({ key: "deviceMap", saga });

export default compose(
  withReducer,
  withSaga,
  withConnect
)(DeviceMap);
