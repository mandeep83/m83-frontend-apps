/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/*
 *
 * DeviceMap reducer
 *
 */

import { fromJS } from "immutable";
import { GET_DEVICE_GROUPS_SUCCESS, GET_DEVICE_GROUPS_FAILURE } from "./constants";

export const initialState = fromJS({});

function deviceMapReducer(state = initialState, action) {
  switch (action.type) {
    case GET_DEVICE_GROUPS_SUCCESS:
      return Object.assign({}, state, {
        getDeviceGroupsSuccess: action.response
      })
    case GET_DEVICE_GROUPS_FAILURE:
      return Object.assign({}, state, {
        getDeviceGroupsFailure: action.error
      })
    default:
      return state;
  }
}

export default deviceMapReducer;
