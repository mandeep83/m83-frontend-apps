/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

export const ADD_OR_EDIT_O_AUTH_PROVIDER = "app/AddOrEditOauthProvider/ADD_OR_EDIT_O_AUTH_PROVIDER";
export const ADD_OR_EDIT_O_AUTH_PROVIDER_SUCCESS = "app/AddOrEditOauthProvider/ADD_OR_EDIT_O_AUTH_PROVIDER_SUCCESS";
export const ADD_OR_EDIT_O_AUTH_PROVIDER_FAILURE = "app/AddOrEditOauthProvider/ADD_OR_EDIT_O_AUTH_PROVIDER_FAILURE";

export const GET_OAUTH_CONFIGURATION_DETAILS = "app/AddOrEditOauthProvider/GET_OAUTH_CONFIGURATION_DETAILS";
export const GET_OAUTH_CONFIGURATION_DETAILS_SUCCESS = "app/AddOrEditOauthProvider/GET_OAUTH_CONFIGURATION_DETAILS_SUCCESS";
export const GET_OAUTH_CONFIGURATION_DETAILS_FAILURE = "app/AddOrEditOauthProvider/GET_OAUTH_CONFIGURATION_DETAILS_FAILURE";

export const GET_OAUTH_CONFIGURATION_LIST = "app/AddOrEditOauthProvider/GET_OAUTH_CONFIGURATION_LIST";
export const GET_OAUTH_CONFIGURATION_LIST_SUCCESS = "app/AddOrEditOauthProvider/GET_OAUTH_CONFIGURATION_LIST_SUCCESS";
export const GET_OAUTH_CONFIGURATION_LIST_FAILURE = "app/AddOrEditOauthProvider/GET_OAUTH_CONFIGURATION_LIST_FAILURE";

export const DELETE_O_AUTH_PROVIDER = "app/AddOrEditOauthProvider/DELETE_O_AUTH_PROVIDER";
export const DELETE_O_AUTH_PROVIDER_SUCCESS = "app/AddOrEditOauthProvider/DELETE_O_AUTH_PROVIDER_SUCCESS";
export const DELETE_O_AUTH_PROVIDER_FAILURE = "app/AddOrEditOauthProvider/DELETE_O_AUTH_PROVIDER_FAILURE";

export const GET_OAUTH_PROVIDER_OPTIONS = "app/AddOrEditOauthProvider/GET_OAUTH_PROVIDER_OPTIONS";
export const GET_OAUTH_PROVIDER_OPTIONS_SUCCESS = "app/AddOrEditOauthProvider/GET_OAUTH_PROVIDER_OPTIONS_SUCCESS";
export const GET_OAUTH_PROVIDER_OPTIONS_FAILURE = "app/AddOrEditOauthProvider/GET_OAUTH_PROVIDER_OPTIONS_FAILURE";
