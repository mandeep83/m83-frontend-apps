/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { fromJS } from "immutable";
import * as CONSTANTS from "./constants";


export const initialState = fromJS({});

function addOrEditOauthProviderReducer(state = initialState, action) {
  switch (action.type) {
    case CONSTANTS.ADD_OR_EDIT_O_AUTH_PROVIDER_SUCCESS:
      return Object.assign({}, state, {
        'addOrEditOAuthProviderSuccess': { message: action.response, timestamp: Date.now() },
      })
    case CONSTANTS.ADD_OR_EDIT_O_AUTH_PROVIDER_FAILURE:
      return Object.assign({}, state, {
        'addOrEditOAuthProviderFailure': { error: action.error, timestamp: Date.now() },
      })
    case CONSTANTS.GET_OAUTH_CONFIGURATION_DETAILS_SUCCESS:
      return Object.assign({}, state, {
        'oAuthConfigurationDetailsSuccess': action.response,
      })
    case CONSTANTS.GET_OAUTH_CONFIGURATION_DETAILS_FAILURE:
      return Object.assign({}, state, {
        'oAuthConfigurationDetailsFailure': { error: action.error, timestamp: Date.now() },
      })
    case CONSTANTS.GET_OAUTH_CONFIGURATION_LIST_SUCCESS:
      return Object.assign({}, state, {
        'getOAuthConfigurationsSuccess': action.response,
      })
    case CONSTANTS.GET_OAUTH_CONFIGURATION_LIST_FAILURE:
      return Object.assign({}, state, {
        'getOAuthConfigurationsFailure': { error: action.error, timestamp: Date.now() },
      })
    case CONSTANTS.DELETE_O_AUTH_PROVIDER_SUCCESS:
      return Object.assign({}, state, {
        'deleteOAuthProviderSuccess': { message: action.response, timestamp: Date.now(), id: action.id },
      })
    case CONSTANTS.DELETE_O_AUTH_PROVIDER_FAILURE:
      return Object.assign({}, state, {
        'deleteOAuthProviderFailure': { error: action.error, timestamp: Date.now() },
      })
    case CONSTANTS.GET_OAUTH_PROVIDER_OPTIONS_SUCCESS:
      return Object.assign({}, state, {
        'oAuthProviderOptions': { message: action.response, timestamp: Date.now(), id: action.id },
      })
    case CONSTANTS.GET_OAUTH_PROVIDER_OPTIONS_FAILURE:
      return Object.assign({}, state, {
        'oAuthProviderOptionsFailure': { error: action.error, timestamp: Date.now() },
      })
    default:
      return state;
  }
}

export default addOrEditOauthProviderReducer;
