/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
 * Direct selector to the addOrEditOauthProvider state domain
 */

const selectAddOrEditOauthProviderDomain = state =>
  state.get("addOrEditOauthProvider", initialState);

// const makeSelectAddOrEditOauthProvider = () =>
//   createSelector(selectAddOrEditOauthProviderDomain, subState =>
//     subState.toJS()
//   );

export const addOrEditOAuthProviderSuccess = () => createSelector(selectAddOrEditOauthProviderDomain, subState => subState.addOrEditOAuthProviderSuccess);
export const addOrEditOAuthProviderFailure = () => createSelector(selectAddOrEditOauthProviderDomain, subState => subState.addOrEditOAuthProviderFailure);

export const oAuthConfigurationDetailsSuccess = () => createSelector(selectAddOrEditOauthProviderDomain, subState => subState.oAuthConfigurationDetailsSuccess);
export const oAuthConfigurationDetailsFailure = () => createSelector(selectAddOrEditOauthProviderDomain, subState => subState.oAuthConfigurationDetailsFailure);

export const getOAuthConfigurationsSuccess = () => createSelector(selectAddOrEditOauthProviderDomain, subState => subState.getOAuthConfigurationsSuccess);
export const getOAuthConfigurationsFailure = () => createSelector(selectAddOrEditOauthProviderDomain, subState => subState.getOAuthConfigurationsFailure);

export const deleteOAuthProviderSuccess = () => createSelector(selectAddOrEditOauthProviderDomain, subState => subState.deleteOAuthProviderSuccess);
export const deleteOAuthProviderFailure = () => createSelector(selectAddOrEditOauthProviderDomain, subState => subState.deleteOAuthProviderFailure);

export const oAuthProviderOptions = () => createSelector(selectAddOrEditOauthProviderDomain, subState => subState.oAuthProviderOptions);
export const oAuthProviderOptionsFailure = () => createSelector(selectAddOrEditOauthProviderDomain, subState => subState.oAuthProviderOptionsFailure);

// export default makeSelectAddOrEditOauthProvider;
export { selectAddOrEditOauthProviderDomain };
