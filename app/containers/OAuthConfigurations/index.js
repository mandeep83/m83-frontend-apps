/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import * as SELECTORS from "./selectors";
import * as ACTIONS from "./actions";
import reducer from "./reducer";
import saga from "./saga";
import ReactSelect from "react-select";
import Loader from "../../components/Loader/index";
import NotificationModal from '../../components/NotificationModal/Loadable';
import SlidingPane from "react-sliding-pane";
import cloneDeep from "lodash/cloneDeep"
const usedForList = [
    { value: 'login', label: "Login", category: "login" },
    { value: 'faas', label: "FaaS", category: "sourceControl" },
    { value: 'widget', label: "Widget Studio", category: "sourceControl" },
    { value: 'js', label: "JS Function", category: "sourceControl" },
    { value: 'ticketManagement', label: "Ticket Management", category: "tickets" },
]
/* eslint-disable react/prefer-stateless-function */
export class AddOrEditOauthProvider extends React.Component {
    state = {
        isFetching: true,
        oauthModal: false,
        showModalLoader: false,
        usedFor: usedForList,
        oAuthConfigurations: [],
        oAuthProviders: [],
        payload: {
            id: null,
            name: '',
            clientId: '',
            clientSecret: '',
            redirectUri: "",
            authorizationUri: '',
            tokenUri: '',
            userInfoUri: '',
            transactionUri: '',
            scope: '',
            usedFor: '',
        }
    };

    componentDidMount() {
        this.props.getOAuthProviderOptions();
        this.props.getOAuthConfigurationList()
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.addOrEditOAuthProviderSuccess && nextProps.addOrEditOAuthProviderSuccess !== this.props.addOrEditOAuthProviderSuccess) {
            this.setState({
                modalType: "success",
                isOpen: true,
                message2: nextProps.addOrEditOAuthProviderSuccess.message,
                oauthModal: false,
                showModalLoader: false,
            }, () => this.props.getOAuthConfigurationList())

        }

        if (nextProps.addOrEditOAuthProviderFailure && nextProps.addOrEditOAuthProviderFailure !== this.props.addOrEditOAuthProviderFailure) {
            this.setState({
                showModalLoader: false,
                modalType: "error",
                isOpen: true,
                message2: nextProps.addOrEditOAuthProviderFailure.error,
            })
        }

        if (nextProps.getOAuthConfigurationsSuccess && nextProps.getOAuthConfigurationsSuccess !== this.props.getOAuthConfigurationsSuccess) {
            this.setState({
                oAuthConfigurations: nextProps.getOAuthConfigurationsSuccess.filter(temp => temp.name !== "pubnub"),
                isFetching: false
            }, () => this.props.oAuthStateChange(nextProps.getOAuthConfigurationsSuccess))
        }

        if (nextProps.getOAuthConfigurationsFailure && nextProps.getOAuthConfigurationsFailure !== this.props.getOAuthConfigurationsFailure) {
            this.setState({
                isFetching: false,
                modalType: "error",
                isOpen: true,
                message2: nextProps.getOAuthConfigurationsFailure.error
            })

        }

        if (nextProps.oAuthConfigurationDetailsSuccess && nextProps.oAuthConfigurationDetailsSuccess !== this.props.oAuthConfigurationDetailsSuccess) {
            let payload = nextProps.oAuthConfigurationDetailsSuccess;
            payload.clientSecret = "";
            let category = cloneDeep(this.state.oAuthProviders).find(temp => temp.name === payload.name).category;
            this.setState({
                payload,
                showModalLoader: false,
                usedFor: usedForList.filter((temp) => temp.category === category)
            })
        }

        if (nextProps.oAuthConfigurationDetailsFailure && nextProps.oAuthConfigurationDetailsFailure !== this.props.oAuthConfigurationDetailsFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.oAuthConfigurationDetailsFailure.error,
                showModalLoader: false,
                oauthModal: false
            })
        }

        if (nextProps.deleteOAuthProviderSuccess && nextProps.deleteOAuthProviderSuccess !== this.props.deleteOAuthProviderSuccess) {
            let oAuthConfigurations = JSON.parse(JSON.stringify(this.state.oAuthConfigurations));
            oAuthConfigurations = oAuthConfigurations.filter(el => el.id !== nextProps.deleteOAuthProviderSuccess.id)
            this.setState({
                oAuthConfigurations,
                isFetching: false,
                modalType: "success",
                isOpen: true,
                message2: nextProps.deleteOAuthProviderSuccess.message,
            }, () => this.props.oAuthStateChange(oAuthConfigurations))
        }

        if (nextProps.deleteOAuthProviderFailure && nextProps.deleteOAuthProviderFailure !== this.props.deleteOAuthProviderFailure) {
            this.setState({
                isFetching: false,
                modalType: "error",
                isOpen: true,
                message2: nextProps.deleteOAuthProviderFailure.error || "Something went wrong.",
            })
        }

        if (nextProps.oAuthProviderOptions && nextProps.oAuthProviderOptions !== this.props.oAuthProviderOptions) {
            this.setState({
                oAuthProviders: nextProps.oAuthProviderOptions.message.filter(temp => temp.name !== "pubnub"),
            })
        }

        if (nextProps.oAuthProviderOptionsFailure && nextProps.oAuthProviderOptionsFailure !== this.props.oAuthProviderOptionsFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.oAuthProviderOptionsFailure.error,
            })
        }
    }

    onChangeHandler = (event) => {
        let payload = JSON.parse(JSON.stringify(this.state.payload));
        payload[event.target.id] = event.target.value.trim();
        this.setState({ payload })
    }

    onCloseHandler = () => {
        this.setState({
            isOpen: false,
            message2: '',
            modalType: "",
        })
    }

    submitFormHandler = (event) => {
        event.preventDefault();
        let payload = JSON.parse(JSON.stringify(this.state.payload));
        this.setState({ showModalLoader: true }, () => this.props.addOrEditOAuthProvider(payload));
    }

    handleProviderChange = (provider) => {
        let payload = JSON.parse(JSON.stringify(this.state.payload));
        let usedFor = usedForList.filter(temp => temp.category === provider.category)
        payload.name = provider.value;
        payload.usedFor = ""
        this.setState({
            payload,
            usedFor
        });
    }

    oAuthEditHandler = (e, id) => {
        this.setState({
            oauthModal: true,
            showModalLoader: true,
        }, () => this.props.getOAuthConfigurationById(id))
    }

    oAuthDeleteHandler = (e, id) => {
        this.setState({
            isFetching: true,
        }, () => this.props.deleteOAuthProvider(id))
    }

    closeModalHandler = () => {
        this.setState({
            oauthModal: false,
        })
    }

    openOAuthModal = () => {
        let oAuthProviders = this.state.oAuthProviders,
            oAuthConfigurations = this.state.oAuthConfigurations,
            assignedProviderNames = [...new Set(oAuthConfigurations.map(configuration => configuration.name))],
            unassignedProviders = [];
        oAuthProviders.map(provider => {
            if (!assignedProviderNames.includes(provider.name)) {
                unassignedProviders.push(provider);
            }
        });
        let payload = {
            id: null,
            name: unassignedProviders[0].name,
            clientId: '',
            clientSecret: '',
            redirectUri: "",
            authorizationUri: '',
            tokenUri: '',
            userInfoUri: '',
            transactionUri: '',
            scope: '',
            usedFor: '',
        };
        this.setState({
            payload,
            oauthModal: true,
            usedFor: usedForList.filter(temp => temp.category === unassignedProviders[0].category)
        })
    }

    getOAuthProviderMenuOptions = () => {
        let oAuthProviders = this.state.oAuthProviders,
            oAuthConfigurations = this.state.oAuthConfigurations,
            options = [];
        let assignedProviderNames = [...new Set(oAuthConfigurations.map(configuration => configuration.name))],
            unassignedProviders = [];
        oAuthProviders.map(provider => {
            if (!assignedProviderNames.includes(provider.name)) {
                unassignedProviders.push(provider);
            }
        })
        oAuthProviders.map(provider => {
            if (this.state.payload.id !== null) {
                options.push({
                    label: provider.displayName,
                    value: provider.name,
                    category: provider.category,
                    isDisabled: true,
                })
            } else {
                if (!assignedProviderNames.includes(provider.name)) {
                    options.push({
                        label: provider.displayName,
                        value: provider.name,
                        category: provider.category,
                    })
                }
            }
        })
        return options;
    }

    getSelectedProvider = () => {
        let oAuthProviders = this.state.oAuthProviders,
            oAuthConfigurations = this.state.oAuthConfigurations,
            obj = {
                label: '',
                value: ''
            };

        if (this.state.payload.id !== null) {
            obj.label = this.state.oAuthProviders.filter(provider => provider.name === this.state.payload.name)[0].displayName;
            obj.value = this.state.payload.name;
        } else {
            let assignedProviderNames = [...new Set(oAuthConfigurations.map(configuration => configuration.name))],
                unassignedProviders = [];
            oAuthProviders.map(provider => {
                if (!assignedProviderNames.includes(provider.name)) {
                    unassignedProviders.push(provider);
                }
            })
            if (this.state.payload.name && unassignedProviders.length > 0) {
                if (unassignedProviders.filter(provider => provider.name === this.state.payload.name).length > 0) {
                    obj.label = unassignedProviders.filter(provider => provider.name === this.state.payload.name)[0].displayName;
                    obj.value = unassignedProviders.filter(provider => provider.name === this.state.payload.name)[0].name;
                } else {
                    obj.label = unassignedProviders[0].displayName;
                    obj.value = unassignedProviders[0].name;
                }
            }
        }
        return obj;
    }
    getUsedForSelectedData = () => {
        let data = [],
            usedFor = this.state.usedFor,
            assignedFeatures = this.state.payload.usedFor.split(',');
        usedFor.map(feature => {
            if (assignedFeatures.includes(feature.value)) {
                data.push(feature);
            }
        })
        return data;
    }

    handleUsedForChange = (feature) => {
        let payload = JSON.parse(JSON.stringify(this.state.payload)),
            assignedFeatures = [...new Set(feature.map(obj => obj.value))].join(',');
        payload.usedFor = assignedFeatures;
        this.setState({
            payload,
        })
    }

    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>OauthProvider</title>
                    <meta name="description" content="Description of OauthProvider"/>
                </Helmet>

                {this.state.isFetching ? <Loader /> :
                    <React.Fragment>
                        <div className="pageBreadcrumb">
                            <div className="row">
                                <div className="col-8">
                                    <p><span>OAuth Configurations {this.state.oAuthConfigurations.length > 0 && '(' + this.state.oAuthConfigurations.length + ')'}</span></p>
                                </div>
                                <div className="col-4 text-right">
                                    <div className="flex h-100 justify-content-end align-items-center" >
                                        {this.state.oAuthConfigurations.length > 0 && this.state.oAuthConfigurations.length !== this.state.oAuthProviders.length &&
                                        <button type="button" name="button" className="btn btn-primary" onClick={this.openOAuthModal}>
                                            <span className="btn-primary-icon"><i className="far fa-plus" /></span>
                                        </button>
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>

                        {this.state.oAuthConfigurations.length ?
                            <div className="outerBox pb-0">
                                <ul className="pageList projectList">
                                    {this.state.oAuthConfigurations.map((el, index) => {
                                            let usedForArray = el.usedFor.split(',')
                                            usedForArray = usedForArray.map(el => usedForList.find(option => option.value === el).label)
                                            let usedFor = usedForArray.join(" | ")
                                            return (
                                                <li key={index}>
                                                    <div className="pageListBox">
                                                        <div className="pageListBoxImage">
                                                            <img src={`https://content.iot83.com/m83/tenant/${el.name}.png`} />
                                                        </div>
                                                        <div className="pageListBoxContent">
                                                            <h5>{el.name}</h5>
                                                            <h6>Used For: <strong>{usedFor}</strong></h6>
                                                        </div>
                                                        <div className="pageListBoxFooter">
                                                            <p><strong>Created At:</strong>{new Date(el.createdAt).toLocaleString('en-US', {timeZone: localStorage.timeZone})}</p>
                                                            <p><strong>Updated At:</strong>{new Date(el.updatedAt).toLocaleString('en-US', {timeZone: localStorage.timeZone})}</p>
                                                            <div className="dropdown pageListDropdown">
                                                                <button type="button" className="btn dropdown-toggle" data-toggle="dropdown">
                                                                    <i className="far fa-ellipsis-v"></i>
                                                                </button>
                                                                <ul className="dropdown-menu pageListDropdownMenu">
                                                                    <li onClick={(e) => this.oAuthEditHandler(e, el.id)}>
                                                                        <i className="far fa-pen"></i>
                                                                        Edit
                                                                    </li>
                                                                    {/* <li className="pageListDropdownMenu" onClick={(e) => this.oAuthDeleteHandler(e, el.id)}><i
                                    className="far fa-trash-alt"></i>Delete
                                    </li> */}
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>)
                                        }
                                    )}
                                </ul>
                            </div>
                            : <div className="contentAddBox">
                                <div className="contentAddBoxDetail">
                                    <div className="contentAddBoxImage">
                                        <img src="https://content.iot83.com/m83/other/addOAuthConfig.png" /></div>
                                    {this.state.oAuthProviders.length > 0 ? <React.Fragment>
                                            <h6>You have not created any OAuth Configuration yet.</h6>
                                            <button className="btn btn-primary" type="button" onClick={this.openOAuthModal}><i className="far fa-plus"></i>Add new</button>
                                        </React.Fragment>
                                        : <h6>You have not assigned any OAuth Providers yet.</h6>
                                    }
                                </div>
                            </div>
                        }
                    </React.Fragment>
                }

                {/* add new oauth */}
                <SlidingPane
                    className=''
                    overlayClassName='slidingFormOverlay'
                    closeIcon={<div></div>}
                    isOpen={this.state.oauthModal || false}
                    from='right'
                    width='550px'
                >
                    <div className="modal-content">
                        <div className="modal-header">
                            <h6 className="modal-title">{this.state.payload.id ? "Update" :"Add New"} OAuth Provider
                                <button className="close" onClick={this.closeModalHandler}><i className="far fa-angle-right"></i></button>
                            </h6>
                        </div>

                        {this.state.showModalLoader ?
                            <div className="modal-loader">
                                <i className="fad fa-sync-alt fa-spin text-primary"></i>
                            </div> :
                            <form onSubmit={this.submitFormHandler}>
                                <div className="modal-body">
                                    <div className="form-group">
                                        <label className="form-label">Name: <span className="requiredMark"><i className="fa fa-asterisk"></i></span></label>
                                        {this.state.oAuthProviders.length > 0 &&
                                        <ReactSelect
                                            name="name"
                                            isMulti={false}
                                            value={this.getSelectedProvider()}
                                            options={this.getOAuthProviderMenuOptions()}
                                            onChange={this.handleProviderChange}
                                        />}
                                    </div>

                                    <div className="flex">
                                        <div className="flex-item fx-b50 pd-r-7">
                                            <div className="form-group">
                                                <input type="text" className="form-control" id="clientId" placeholder="Client Id" value={this.state.payload.clientId} required onChange={this.onChangeHandler} />
                                                <label className="form-group-label">Client Id: <span className="requiredMark"><i className="fa fa-asterisk"></i></span></label>
                                            </div>
                                        </div>
                                        <div className="flex-item fx-b50 pd-l-7">
                                            <div className="form-group">
                                                <input type="text" className="form-control" id="clientSecret" placeholder="Client Secret" value={this.state.payload.clientSecret} required onChange={this.onChangeHandler} />
                                                <label className="form-group-label">Client Secret: <span className="requiredMark"><i className="fa fa-asterisk"></i></span></label>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="form-group">
                                        <input type="text" className="form-control" id="redirectUri" placeholder="Redirection Url" value={this.state.payload.redirectUri} required onChange={this.onChangeHandler} />
                                        <label className="form-group-label">Redirection Url: <span className="requiredMark"><i className="fa fa-asterisk"></i></span></label>
                                    </div>

                                    <div className="form-group">
                                        <input type="text" className="form-control" id="authorizationUri" placeholder="Authorization Url" value={this.state.payload.authorizationUri} required onChange={this.onChangeHandler} />
                                        <label className="form-group-label">Authorization Url : <span className="requiredMark"><i className="fa fa-asterisk"></i></span></label>
                                    </div>

                                    <div className="form-group">
                                        <input type="text" className="form-control" id="tokenUri" placeholder="Token Url" value={this.state.payload.tokenUri} required onChange={this.onChangeHandler} />
                                        <label className="form-group-label">Token Url : <span className="requiredMark"><i className="fa fa-asterisk"></i></span></label>
                                    </div>

                                    <div className="form-group">
                                        <input type="text" className="form-control" id="scope" placeholder="Scope" value={this.state.payload.scope} required onChange={this.onChangeHandler} />
                                        <label className="form-group-label">Scope : <span className="requiredMark"><i className="fa fa-asterisk"></i></span></label>
                                    </div>

                                    <div className="form-group">
                                        <input type="text" className="form-control" id="userInfoUri" placeholder="User Info Uri" value={this.state.payload.userInfoUri} onChange={this.onChangeHandler} />
                                        <label className="form-group-label">User Info Uri: </label>
                                    </div>

                                    <div className="form-group">
                                        <input type="text" className="form-control" id="transactionUri" placeholder="Transaction Uri" value={this.state.payload.transactionUri} onChange={this.onChangeHandler} />
                                        <label className="form-group-label">Transaction Uri: </label>
                                    </div>

                                    <div className="form-group">
                                        <label className="form-label">Used For: <span className="requiredMark"><i className="fa fa-asterisk"></i></span></label>
                                        <ReactSelect
                                            name="usedFor"
                                            isMulti={true}
                                            onChange={this.handleUsedForChange}
                                            value={this.getUsedForSelectedData()}
                                            options={this.state.usedFor}
                                        />
                                    </div>
                                </div>

                                <div className="modal-footer">
                                    <button className="btn btn-success">Save</button>
                                    <button type="button" className="btn btn-dark" onClick={this.closeModalHandler}>Cancel</button>
                                </div>
                            </form>
                        }
                    </div>
                </SlidingPane>
                {/* end add new oauth */}

                {this.state.isOpen &&
                <NotificationModal
                    type={this.state.modalType}
                    message2={this.state.message2}
                    onCloseHandler={this.onCloseHandler}
                />
                }
            </React.Fragment>
        );
    }
}



AddOrEditOauthProvider.propTypes = {
    dispatch: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
    addOrEditOAuthProviderSuccess: SELECTORS.addOrEditOAuthProviderSuccess(),
    addOrEditOAuthProviderFailure: SELECTORS.addOrEditOAuthProviderFailure(),
    oAuthConfigurationDetailsSuccess: SELECTORS.oAuthConfigurationDetailsSuccess(),
    oAuthConfigurationDetailsFailure: SELECTORS.oAuthConfigurationDetailsFailure(),
    getOAuthConfigurationsSuccess: SELECTORS.getOAuthConfigurationsSuccess(),
    getOAuthConfigurationsFailure: SELECTORS.getOAuthConfigurationsFailure(),
    deleteOAuthProviderSuccess: SELECTORS.deleteOAuthProviderSuccess(),
    deleteOAuthProviderFailure: SELECTORS.deleteOAuthProviderFailure(),
    oAuthProviderOptions: SELECTORS.oAuthProviderOptions(),
    oAuthProviderOptionsFailure: SELECTORS.oAuthProviderOptionsFailure(),
});

function
mapDispatchToProps(dispatch) {
    return {
        dispatch,
        addOrEditOAuthProvider: (payload) => dispatch(ACTIONS.addOrEditOAuthProvider(payload)),
        getOAuthConfigurationById: (id) => dispatch(ACTIONS.getOAuthConfigurationById(id)),
        getOAuthConfigurationList: () => dispatch(ACTIONS.getOAuthConfigurationList()),
        deleteOAuthProvider: (id) => dispatch(ACTIONS.deleteOAuthProvider(id)),
        getOAuthProviderOptions: (id) => dispatch(ACTIONS.getOAuthProviderOptions(id)),
    };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "addOrEditOauthProvider", reducer });
const withSaga = injectSaga({ key: "addOrEditOauthProvider", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(AddOrEditOauthProvider);
