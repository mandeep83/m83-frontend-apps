/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import * as CONSTANTS from "./constants";

export function addOrEditOAuthProvider(payload) {
  return {
    type: CONSTANTS.ADD_OR_EDIT_O_AUTH_PROVIDER,
    payload,
  };
}

export function getOAuthConfigurationById(id) {
  return {
    type: CONSTANTS.GET_OAUTH_CONFIGURATION_DETAILS,
    id,
  };
}

export function deleteOAuthProvider(id) {
  return {
    type: CONSTANTS.DELETE_O_AUTH_PROVIDER,
    id,
  };
}
export function getOAuthConfigurationList() {
  return {
    type: CONSTANTS.GET_OAUTH_CONFIGURATION_LIST,
  };
}
export function getOAuthProviderOptions() {
  return {
    type: CONSTANTS.GET_OAUTH_PROVIDER_OPTIONS,
  };
}