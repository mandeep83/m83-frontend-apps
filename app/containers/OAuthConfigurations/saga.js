/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { apiCallHandler } from '../../api';
import { takeEvery } from 'redux-saga';
import * as CONSTANTS from "./constants";

export function* getMyActivities(action) {
  yield [apiCallHandler(action, CONSTANTS.ADD_OR_EDIT_O_AUTH_PROVIDER_SUCCESS, CONSTANTS.ADD_OR_EDIT_O_AUTH_PROVIDER_FAILURE, 'addOAuthProvider')];
}

export function* getOAuthConfigurationDetails(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_OAUTH_CONFIGURATION_DETAILS_SUCCESS, CONSTANTS.GET_OAUTH_CONFIGURATION_DETAILS_FAILURE, 'getOAuthConfigurationDetails')];
}

export function* getOAuthConfigurationList(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_OAUTH_CONFIGURATION_LIST_SUCCESS, CONSTANTS.GET_OAUTH_CONFIGURATION_LIST_FAILURE, 'getOAuthConfigurations')];
}

export function* deleteOAuthProvider(action) {
  yield [apiCallHandler(action, CONSTANTS.DELETE_O_AUTH_PROVIDER_SUCCESS, CONSTANTS.DELETE_O_AUTH_PROVIDER_FAILURE, 'deleteOAuthProvider')];
}

export function* getOAuthProviderOptions(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_OAUTH_PROVIDER_OPTIONS_SUCCESS, CONSTANTS.GET_OAUTH_PROVIDER_OPTIONS_FAILURE, 'getOAuthProviders')];
}

export function* watcherAddOAuthProvider() {
  yield takeEvery(CONSTANTS.ADD_OR_EDIT_O_AUTH_PROVIDER, getMyActivities);
}

export function* watcherGetOAuthConfigurationDetails() {
  yield takeEvery(CONSTANTS.GET_OAUTH_CONFIGURATION_DETAILS, getOAuthConfigurationDetails);
}

export function* watcherGetOAuthConfigurationList() {
  yield takeEvery(CONSTANTS.GET_OAUTH_CONFIGURATION_LIST, getOAuthConfigurationList);
}

export function* watcherDeleteOAuthProvider() {
  yield takeEvery(CONSTANTS.DELETE_O_AUTH_PROVIDER, deleteOAuthProvider);
}

export function* watcherGetOAuthProviderOptions() {
  yield takeEvery(CONSTANTS.GET_OAUTH_PROVIDER_OPTIONS, getOAuthProviderOptions);
}


export default function* rootSaga() {
  yield [
    watcherAddOAuthProvider(),
    watcherGetOAuthConfigurationDetails(),
    watcherGetOAuthConfigurationList(),
    watcherDeleteOAuthProvider(),
    watcherGetOAuthProviderOptions(),
  ];
}