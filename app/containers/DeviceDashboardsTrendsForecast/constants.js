/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/*
 *
 * DeviceDashboardsTrendsForecast constants
 *
 */

export const RESET_TO_INITIAL_STATE = "app/DeviceDashboardsTrendsForecast/RESET_TO_INITIAL_STATE";
export const RESET_TO_INITIAL_STATE_PROPS = "app/DeviceDashboardsTrendsForecast/RESET_TO_INITIAL_STATE_PROPS";


export const GET_CONNECTORS_LIST = {
  action: 'app/DeviceDashboardsTrendsForecast/GET_CONNECTORS_LIST',
  success: "app/DeviceDashboardsTrendsForecast/GET_CONNECTORS_LIST_SUCCESS",
  failure: "app/DeviceDashboardsTrendsForecast/GET_CONNECTORS_LIST_FAILURE",
  urlKey: "deviceTypeList",
  successKey: "deviceTypeListSuccess",
  failureKey: "deviceTypeListFailure",
  actionName: "deviceTypeList",
  actionArguments: []
}

export const GET_ATTRIBUTES_INFO = {
  action : 'app/DeviceDashboardsTrendsForecast/GET_ATTRIBUTES_INFO',
  success: "app/DeviceDashboardsTrendsForecast/GET_ATTRIBUTES_SUCCESS",
  failure: "app/DeviceDashboardsTrendsForecast/GET_ATTRIBUTES_FAILURE",
  urlKey: "getAttributes",
  successKey: "getAttributesSuccess",
  failureKey: "getAttributesFailure",
  actionName: "getAttributes",
  actionArguments : ["id"]
}

export const GET_METRIC_ACTIONS = {
  action : 'app/DeviceDashboardsTrendsForecast/GET_METRIC_ACTIONS',
  success: "app/DeviceDashboardsTrendsForecast/GET_METRIC_ACTIONS_SUCCESS",
  failure: "app/DeviceDashboardsTrendsForecast/GET_METRIC_ACTIONS_FAILURE",
  urlKey: "getMetricActions",
  successKey: "getMetricActionsSuccess",
  failureKey: "getMetricActionsFailure",
  actionName: "getMetricActions",
  actionArguments : []
}

export const FORCAST_PREDICT = {
  action : 'app/DeviceDashboardsTrendsForecast/FORCAST_PREDICT',
  success: "app/DeviceDashboardsTrendsForecast/FORCAST_PREDICT_SUCCESS",
  failure: "app/DeviceDashboardsTrendsForecast/FORCAST_PREDICT_FAILURE",
  urlKey: "forcastPredict",
  successKey: "forcastPredictSuccess",
  failureKey: "forcastPredictFailure",
  actionName: "forcastPredict",
  actionArguments : ["payload"]
}

export const GET_FORCAST_DATA = {
  action : 'app/DeviceDashboardsTrendsForecast/GET_FORCAST_DATA',
  success: "app/DeviceDashboardsTrendsForecast/GET_FORCAST_DATA_SUCCESS",
  failure: "app/DeviceDashboardsTrendsForecast/GET_FORCAST_DATA_FAILURE",
  urlKey: "getForcastData",
  successKey: "getForcastDataSuccess",
  failureKey: "getForcastDataFailure",
  actionName: "getForcastData",
  actionArguments : ["payload"]
}

export const GET_GET_CONFIG_ACTIONS = {
  action : 'app/DeviceDashboardsTrendsForecast/GET_GET_CONFIG_ACTIONS',
  success: "app/DeviceDashboardsTrendsForecast/GET_GET_CONFIG_ACTIONS_SUCCESS",
  failure: "app/DeviceDashboardsTrendsForecast/GET_GET_CONFIG_ACTIONS_FAILURE",
  urlKey: "getDataConfig",
  successKey: "getDataConfigSuccess",
  failureKey: "getDataConfigFailure",
  actionName: "getDataConfig",
  actionArguments : ["id"]
}

export const SAVE_CONFIG = {
  action : 'app/DeviceDashboardsTrendsForecast/SAVE_CONFIG',
  success: "app/DeviceDashboardsTrendsForecast/SAVE_CONFIG_SUCCESS",
  failure: "app/DeviceDashboardsTrendsForecast/SAVE_CONFIG_FAILURE",
  urlKey: "saveOrUpdateForcastConfig",
  successKey: "saveConfigSuccess",
  failureKey: "saveConfigFailure",
  actionName: "saveForcastConfig",
  actionArguments : ["payload"]
}

export const UPDATE_CONFIG = {
  action : 'app/DeviceDashboardsTrendsForecast/UPDATE_CONFIG',
  success: "app/DeviceDashboardsTrendsForecast/UPDATE_CONFIG_SUCCESS",
  failure: "app/DeviceDashboardsTrendsForecast/UPDATE_CONFIG_FAILURE",
  urlKey: "saveOrUpdateForcastConfig",
  successKey: "updateConfigSuccess",
  failureKey: "updateConfigFailure",
  actionName: "updateConfig",
  actionArguments : ["payload", "id"]
}