/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

/**
 *
 * DeviceDashboardsTrendsForecast
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import { allSelectors as SELECTORS } from "./selectors";
import { allActions as ACTIONS } from './actions'
import reducer from './reducer';
import { getTimeDifference, uIdGenerator, isNavAssigned } from "../../commonUtils";
import saga from './saga';
import AddNewButton from "../../components/AddNewButton/Loadable"
import { cloneDeep, startCase, isEqual } from 'lodash';
import ConfirmModel from "../../components/ConfirmModel";
import LineChart from './charts/LineChart';

/* eslint-disable react/prefer-stateless-function */
export class DeviceDashboardsTrendsForecast extends React.Component {

    state = {
        filterExpand: true,
        chartLoader: {},
        intervals: undefined,
        deviceTypeList: [],
        isFetching: true,
        attributeList: [],
        isAddAttributeTab: false,
        metricActions: [],
        attributeSelected: "",
        configData: {
            "deviceTypeId": "",
            "config": []
        },
        tempConfig: {
            attribute: "",
            actions: []
        },
        durationOptions: {
            filter1: [{ displayName: "Last 1 Day", value: 1 },
            { displayName: "Last 7 Days", value: 7 },
            { displayName: "Last 15 Days", value: 15 },
            { displayName: "Last 1 Month", value: 30 },
            { displayName: "Last 3 Months", value: 90 },
            { displayName: "Last 6 Months", value: 180 },
            { displayName: "Last 9 Months", value: 270 },
            { displayName: "Last 1 Year", value: 365 }],
            filter2: [{ displayName: "Next 3 Days", value: 3 },
            { displayName: "Next 7 Days", value: 7 },
            { displayName: "Next 15 Days", value: 15 },
            { displayName: "Next 1 Month", value: 30 },
            { displayName: "Next 3 Months", value: 90 },
            { displayName: "Next 6 Months", value: 180 },
            { displayName: "Next 9 Months", value: 270 },
            { displayName: "Next 1 Year", value: 365 }]
        },
        dataStore: {},
    }

    componentDidMount() {
        this.props.deviceTypeList();
        this.props.getMetricActions()
    }

    static getDerivedStateFromProps(nextProps, state) {

        if (nextProps.getDataConfigSuccess) {
            let intervals = cloneDeep(state.intervals), inProgress = false, chartLoader = cloneDeep(state.chartLoader);
            let configData = nextProps.getDataConfigSuccess.response
            let attributeSelected = state.attributeSelected === "" ? configData.config && configData.config.length ? configData.config[0].attribute : "" : state.attributeSelected
            configData.config.map(config => {
                config.actions.map(action => {
                    if (action.status === "success" && !state.dataStore[config.attribute + action.name]) {
                        let payload = {
                            deviceTypeId: state.configData.deviceTypeId,
                            attribute: config.attribute,
                            action: action.name
                        }
                        nextProps.getForcastData(payload)
                    }
                    if (action.status == "" || action.status == "inProgress") {
                        inProgress = true
                        if (chartLoader[config.attribute + action.name]) {
                            delete chartLoader[config.attribute + action.name]
                        }
                    }
                })
            })
            if (inProgress) {
                if (!intervals) {
                    intervals = setInterval(() => nextProps.getDataConfig(state.configData.deviceTypeId), 10000)
                }
            } else {
                clearInterval(intervals)
                intervals = undefined
            }
            return { configData, attributeSelected, intervals, chartLoader }
        }

        else if (nextProps.getDataConfigFailure) {
            return {
                isOpen: true,
                type: "error",
                message2: nextProps.getDataConfigFailure.error,
                isFetching: false,
                deviceTypeChangeLoader: false
            }
        }

        if (nextProps.deviceTypeListSuccess) {
            let deviceTypeList = nextProps.deviceTypeListSuccess.response
            let configData = {
                deviceTypeId: deviceTypeList.length ? deviceTypeList[0].id : "",
                config: []
            }
            if (deviceTypeList.length) {
                nextProps.getDataConfig(configData.deviceTypeId);
                nextProps.getAttributes(configData.deviceTypeId)
            }
            return { deviceTypeList, configData, isFetching: deviceTypeList.length > 0 }
        }

        else if (nextProps.deviceTypeListFailure) {
            return {
                isOpen: true,
                type: "error",
                message2: nextProps.deviceTypeListFailure.error,
                isFetching: false
            }
        }

        else if (nextProps.getAttributesSuccess) {
            let attributeList = cloneDeep(state.attributeList)
            attributeList = nextProps.getAttributesSuccess.response.metaInfo
            return { attributeList, deviceTypeChangeLoader: false, isFetching: false }
        }
        else if (nextProps.getMetricActionsSuccess) {
            return { metricActions: nextProps.getMetricActionsSuccess.response }
        }

        else if (nextProps.forcastPredictSuccess) {
            let intervals = cloneDeep(state.intervals)
            if (nextProps.forcastPredictSuccess.response.isHardRefresh) {
                if (!intervals) {
                    intervals = setInterval(() => nextProps.getDataConfig(state.configData.deviceTypeId), 10000)
                }
            }
            return { intervals }
        }
        else if (nextProps.getForcastDataSuccess) {
            let configData = cloneDeep(state.configData),
                dataStore = cloneDeep(state.dataStore)
            configData.config.map(attr => {
                if (attr.attribute === nextProps.getForcastDataSuccess.response.attribute) {
                    attr.actions.map(action => {
                        if (action.name === nextProps.getForcastDataSuccess.response.action) {
                            dataStore[attr.attribute + action.name] = { data: nextProps.getForcastDataSuccess.response.data, id: uIdGenerator() }
                            action.id = uIdGenerator()
                        }
                    })
                }
            })
            return { configData, dataStore }
        }

        else if (nextProps.saveConfigSuccess) {
            state.configData.config.map(val => {
                val.actions.map(subVal => {
                    if (subVal.status == "" && !state.dataStore[val.attribute + subVal.name]) {
                        let predictionPayload = {
                            action: {
                                type: "timeSeries"
                            },
                            deviceTypeId: state.configData.deviceTypeId,
                            attribute: val.attribute,
                            duration: parseInt(subVal.duration),
                            task: subVal.name,
                            isHardRefresh: true
                        }
                        nextProps.forcastPredict(predictionPayload)
                    }
                })
            })
            nextProps.getDataConfig(state.configData.deviceTypeId)
            return { deviceTypeChangeLoader: false }
        }
        else if (nextProps.saveConfigFailure) {
            return {
                deviceTypeChangeLoader: false,
                configData: state.previousConfig,
                attributeSelected: state.previouslySelectedAttribute,
                editConfig: false,
                dataStore: state.previousDataStore,
                previousConfig: {},
                previouslySelectedAttribute: ''
            }
        }

        else if (nextProps.updateConfigFailure) {
            return { isFetching: false }
        }
        return null
    }

    componentDidUpdate(prevProps, prevState) {
        let newProp = Object.keys(SELECTORS).find(prop => this.props[prop])
        if (newProp) {
            if (newProp.includes("Failure")) {
                let propData = this.props[newProp].error ? this.props[newProp].error : cloneDeep(this.props[newProp].response)
                let type = newProp.includes("Failure") ? "error" : "success"
                if (newProp == "getForcastDataFailure") {
                    let intervals = cloneDeep(prevState.intervals),
                        configData = cloneDeep(prevState.configData),
                        dataStore = cloneDeep(prevState.dataStore),
                        chartLoader = cloneDeep(prevState.chartLoader)
                    if (intervals) {
                        clearInterval(intervals)
                        intervals = undefined
                    }
                    configData.config.map(val => {
                        val.actions.map(subVal => {
                            if (!dataStore[val.attribute + subVal.name])
                                dataStore[val.attribute + subVal.name] = { data: {} }
                            if (chartLoader[config.attribute + action.name]) {
                                delete chartLoader[config.attribute + action.name]
                            }
                        })
                    })
                    this.setState({
                        intervals,
                        configData,
                        dataStore,
                        chartLoader
                    })
                }
                this.props.showNotification(type, propData, this.onCloseHandler)
            }
            this.props.resetToInitialStateProps(newProp)
        }
    }

    componentWillUnmount() {
        let intervals = cloneDeep(this.state.intervals)
        clearInterval(intervals)
    }

    onCloseHandler = () => {
        this.setState({
            isOpen: false,
            type: '',
            message2: ''
        })
    }

    sendRpcHandler = (event, index) => {
        let tempConfig = cloneDeep(this.state.tempConfig)
        if (event.target.name === "action") {
            if (event.target.checked) {
                tempConfig.actions.push({
                    name: event.target.id,
                    duration: "",
                    status: ""
                })
            } else {
                tempConfig.actions.splice(tempConfig.actions.findIndex(act => act.name === event.target.id), 1)
            }
        } else {
            tempConfig.actions[tempConfig.actions.findIndex(act => act.name === this.state.metricActions[index])].duration = event.target.value
        }

        this.setState({
            tempConfig
        })
    }

    attributeChangeHandler = (attributeSelected) => {
        let tempConfig = {
            attribute: attributeSelected,
            actions: []
        }
        this.setState({ tempConfig })
    }

    saveChangeHandler = () => {
        let tempConfig = cloneDeep(this.state.tempConfig),
            intervals = cloneDeep(this.state.intervals),
            configData = cloneDeep(this.state.configData),
            dataStore = cloneDeep(this.state.dataStore),
            previousDataStore = cloneDeep(this.state.dataStore),
            previousConfig = cloneDeep(this.state.configData),
            previouslySelectedAttribute = this.state.attributeSelected;
        if (this.state.editConfig) {
            configData.config.map((attr, index) => {
                if (attr.attribute === tempConfig.attribute) {
                    if (!isEqual(attr.actions, tempConfig.actions)) {
                        tempConfig.actions.map((ack, subindex) => {
                            if (attr.actions.some(suback => suback.name === ack.name)) {
                                if (attr.actions.find(suback => suback.name === ack.name).duration != ack.duration) {
                                    tempConfig.actions[subindex].status = ""
                                    if (dataStore[tempConfig.attribute + tempConfig.actions[subindex].name])
                                        delete dataStore[tempConfig.attribute + tempConfig.actions[subindex].name]
                                }
                            }
                        })
                    }
                    configData.config[index] = cloneDeep(tempConfig)
                }
            })
        } else {
            configData.config.push(tempConfig);
        }
        if (intervals) {
            clearInterval(intervals)
            intervals = undefined
        }
        this.props.saveForcastConfig(configData)
        this.setState({
            editConfig: false,
            configData,
            intervals,
            dataStore,
            isAddAttributeTab: false,
            attributeSelected: tempConfig.attribute,
            previouslySelectedAttribute,
            previousConfig,
            previousDataStore
        })
    }

    addNewConfigHandler = () => {
        this.setState({
            isAddAttributeTab: true,
            editConfig: false,
            tempConfig: {
                attribute: "",
                actions: [],
            }
        })
    }

    cancelClicked = () => {
        this.setState({
            confirmState: false,
            configActionIndexToBeDeleted: ""
        })
    }

    changeDurationHandler = (index, actionIndex, actionName) => {
        let configData = cloneDeep(this.state.configData),
            intervals = cloneDeep(this.state.intervals),
            dataStore = cloneDeep(this.state.dataStore)

        let payload = {}
        configData.config[index].actions[actionIndex].status = ""
        configData.config[index].actions[actionIndex].duration = event.target.value
        if (dataStore[configData.config[index].attribute + configData.config[index].actions[actionIndex].name])
            delete dataStore[configData.config[index].attribute + configData.config[index].actions[actionIndex].name]
        payload = {
            action: {
                type: "timeSeries"
            },
            deviceTypeId: this.state.configData.deviceTypeId,
            attribute: configData.config[index].attribute,
            duration: event.target.value,
            task: configData.config[index].actions[actionIndex].name
        }
        if (intervals) {
            clearInterval(intervals)
            intervals = undefined
        }
        this.setState({ configData, intervals, dataStore }, () => {
            this.props.saveForcastConfig(configData)
        })
    }

    editConfigHandler = (index) => {
        this.setState({
            editConfig: true,
            isAddAttributeTab: true,
            tempConfig: this.state.configData.config[index]
        })
    }

    deviceTypeChangeHandler = (event) => {
        let intervals = cloneDeep(this.state.intervals)
        let configData = {
            deviceTypeId: "",
            config: []
        }
        configData.deviceTypeId = event.target.value
        if (intervals) {
            clearInterval(intervals)
            intervals = undefined
        }
        this.setState({ configData, deviceTypeChangeLoader: true, intervals, attributeSelected: "" }, () => {
            if (configData.deviceTypeId != "")
                this.props.getDataConfig(configData.deviceTypeId)
            this.props.getAttributes(configData.deviceTypeId)
        })
    }

    refreshPage = () => {
        let intervals = cloneDeep(this.state.intervals)
        clearInterval(intervals)
        intervals = undefined
        this.setState({
            intervals,
            deviceTypeList: [],
            isFetching: true,
            attributeList: [],
            isAddAttributeTab: false,
            metricActions: [],
            attributeSelected: "",
            configData: {
                "deviceTypeId": "",
                "config": []
            },
            tempConfig: {
                attribute: "",
                actions: []
            },
        }, () => {
            this.props.deviceTypeList();
            this.props.getMetricActions()
        })
    }

    deleteAttribute = () => {
        let configData = cloneDeep(this.state.configData),
            intervals = cloneDeep(this.state.intervals),
            dataStore = cloneDeep(this.state.dataStore),
            previousConfig = cloneDeep(this.state.configData),
            previousDataStore = cloneDeep(this.state.dataStore),
            previouslySelectedAttribute = this.state.attributeSelected

        if (intervals) {
            clearInterval(intervals)
            intervals = undefined
        }
        Object.keys(dataStore).map(val => {
            if (val.includes(configData.config[this.state.attributeToBeDeleted].attribute)) {
                delete dataStore[val];
            }
        })
        configData.config.splice(this.state.attributeToBeDeleted, 1)
        let attributeSelected = configData.config.length ? configData.config[0].attribute : ""
        this.setState({
            deviceTypeChangeLoader: true,
            dataStore,
            configData,
            intervals,
            attributeToBeDeleted: "",
            confirmState: false,
            attributeSelected,
            previouslySelectedAttribute,
            previousConfig,
            previousDataStore
        }, () => {
            this.props.saveForcastConfig(configData)
        })
    }

    getAttributeName = (attribute) => {
        if (this.state.attributeList.length) {
            if (this.state.attributeList.find(val => val.attribute === attribute)) {
                if (this.state.attributeList.find(val => val.attribute === attribute).displayName)
                    return this.state.attributeList.find(val => val.attribute === attribute).displayName
            }
        }
        return attribute
    }

    paginatedRefresh = (attributeIndex, actionIndex) => {
        let configData = cloneDeep(this.state.configData),
            chartLoader = cloneDeep(this.state.chartLoader)
        let predictionPayload = {
            action: {
                type: "timeSeries"
            },
            deviceTypeId: configData.deviceTypeId,
            attribute: configData.config[attributeIndex].attribute,
            duration: parseInt(configData.config[attributeIndex].actions[actionIndex].duration),
            task: configData.config[attributeIndex].actions[actionIndex].name,
            isHardRefresh: true
        }
        chartLoader[configData.config[attributeIndex].attribute + configData.config[attributeIndex].actions[actionIndex].name] = true
        this.setState({
            chartLoader
        }, () => this.props.forcastPredict(predictionPayload))
    }

    filterExpandCollapseHandler = () => {
        this.setState({
            filterExpand: this.state.filterExpand ? false : true
        })
    }

    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>DeviceTrendsForecast</title>
                    <meta name="description" content="Description of M83-DeviceTrendsForecast" />
                </Helmet>

                <header className="content-header d-flex">
                    <div className="flex-60">
                        <h6>Trends & Forecast</h6>
                    </div>
                    <div className="flex-40 text-right">
                        <div className="content-header-group">
                            {this.state.configData.config.length > 0 &&
                                <button className="btn btn-light" onClick={() => this.refreshPage()} data-tooltip data-tooltip-text="Refresh" data-tooltip-place="bottom"><i className="far fa-sync-alt"></i></button>
                            }
                            {Boolean(this.state.deviceTypeList.length) && <button className={`btn btn-light ${this.state.filterExpand && "active"}`} data-tooltip data-tooltip-text="Filters" data-tooltip-place="bottom" disabled={this.state.isFetchingDevices} onClick={() => this.filterExpandCollapseHandler()} ><i className="far fa-filter"></i></button>}
                            {(this.state.configData.config.length < this.state.attributeList.length) &&
                                <button className="btn btn-primary" data-tooltip data-tooltip-text="Add Data Point" data-tooltip-place="bottom" onClick={() => this.addNewConfigHandler()} disabled={this.state.attributeList.length == 0}><i className="far fa-plus"></i></button>
                            }
                        </div>
                    </div>
                </header>

                {this.state.isFetching ?
                    <Loader /> :
                    <React.Fragment>
                        {this.state.deviceTypeList.length ?
                            <React.Fragment>
                                {this.state.filterExpand && <div className="device-filter-wrapper">
                                    <div className="device-filter-box">
                                        <h5>Filters</h5>
                                        <h6>Specify Your Search Criteria</h6>
                                    </div>
                                    <div className="device-filter-box">
                                        <p>Device Type <i className="fas fa-angle-down device-filter-collapse" data-toggle="collapse" data-target="#deviceType__filter__"></i></p>
                                        <div className="collapse show" id="deviceType__filter__">
                                            <div className="device-filter-body">
                                                <div className="form-group mb-0">
                                                    <select id="deviceType" disabled={this.state.intervals != undefined} className="form-control" value={this.state.configData.deviceTypeId} onChange={this.deviceTypeChangeHandler}>
                                                        <option value="" disabled={true}>Select</option>
                                                        {this.state.deviceTypeList.map(val => (
                                                            <option key={val.id} value={val.id}>{val.name}</option>
                                                        ))}
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>}

                                <div className="content-body device-filter-content" style={{ paddingRight: this.state.filterExpand ? 270 : 12 }}>
                                    {this.state.configData.deviceTypeId != "" ?
                                        this.state.deviceTypeChangeLoader ?
                                            <div className="inner-loader-wrapper h-100">
                                                <div className="inner-loader-content">
                                                    <i className="fad fa-sync-alt fa-spin"></i>
                                                </div>
                                            </div>
                                            :
                                            this.state.configData.config.length > 0 ?
                                                <React.Fragment>
                                                    <ul className="nav nav-tabs device-detail-tabs">
                                                        {this.state.configData.config && this.state.configData.config.map((attribute, index) =>
                                                            <li className="nav-item" key={index} onClick={() => this.setState({ attributeSelected: attribute.attribute })}>
                                                                <a className={`nav-link ${attribute.attribute == this.state.attributeSelected ? "active" : ""}`}>{attribute.attribute}</a>
                                                            </li>
                                                        )}
                                                    </ul>
                                                    <div className="tab-content device-detail-tab-content">
                                                        {this.state.configData.config && this.state.configData.config.map((attribute, index) => (
                                                            <div className={`tab-pane fade show ${attribute.attribute === this.state.attributeSelected ? "active" : ""}`} key={"attribute" + index}>
                                                                <div className="device-chart-group">
                                                                    <div className="device-chart-group-title">
                                                                        <h6>Available Metric(s)</h6>
                                                                        <div className="button-group">
                                                                            <button type="button" className="btn-transparent btn-transparent-blue" data-tooltip data-tooltip-text="Edit" data-tooltip-place="bottom" onClick={() => this.editConfigHandler(index)} disabled={this.state.attributeSelected != attribute.attribute}>
                                                                                <i className="far fa-pencil"></i>
                                                                            </button>
                                                                            <button type="button" className="btn-transparent btn-transparent-red" data-tooltip data-tooltip-text="Delete" data-tooltip-place="bottom"
                                                                                onClick={() => this.setState({ attributeToBeDeleted: index, confirmState: true })}
                                                                            >
                                                                                <i className="far fa-trash-alt"></i>
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                    {attribute.actions.map((actions, actionIndex) => (<div className="card" key={actions.id}>
                                                                        <div className="card-header">
                                                                            <h6>{`${startCase(actions.name)} Data`}
                                                                                <small className="text-green f-11 ml-2">(<strong>Last calculated:</strong>{actions.lastCalculated ? " " + getTimeDifference(actions.lastCalculated) : " NA"})</small>
                                                                            </h6>
                                                                            <div className="form-group card-header-filter mr-r-50">
                                                                                <select id="duration" className="form-control" value={actions.duration} onChange={() => this.changeDurationHandler(index, actionIndex, actions.name)}>
                                                                                    {this.state.durationOptions[actions.name === "forecast" ? "filter2" : "filter1"].map((duration, id) =>
                                                                                        <option value={duration.value} key={id}>{duration.displayName}</option>
                                                                                    )}
                                                                                </select>
                                                                            </div>
                                                                            <div className="button-group">
                                                                                <button disabled={actions.status === "inProgress"} className="btn-transparent btn-transparent-blue"><i disabled={actions.status === "inProgress"} onClick={() => this.paginatedRefresh(index, actionIndex)} className="far fa-sync-alt"></i></button>
                                                                            </div>
                                                                        </div>
                                                                        <div className="card-body">
                                                                            {this.state.chartLoader[attribute.attribute + actions.name] || actions.status == "" ?
                                                                                <div className="card-chart-box text-center">
                                                                                    <div className="inner-loader-wrapper h-100">
                                                                                        <div className="inner-loader-content">
                                                                                            <h6>Analyzing Data...</h6>
                                                                                            <img src="https://content.iot83.com/m83/misc/uploading.gif" />
                                                                                            <p className="text-gray">Please be patient. This may take some time as per dataset size.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                :
                                                                                actions.status == "failed" || actions.status == "fail" ?
                                                                                    <div className="card-message">
                                                                                        <p>Something went wrong, failed to fetch data !</p>
                                                                                    </div> :
                                                                                    this.state.dataStore[attribute.attribute + actions.name] == undefined || actions.status == "inProgress" ?
                                                                                        <div className="card-chart-box text-center">
                                                                                            <div className="inner-loader-wrapper h-100">
                                                                                                <div className="inner-loader-content">
                                                                                                    <h6>Fetching Data...</h6>
                                                                                                    <img src="https://content.iot83.com/m83/misc/uploading.gif" />
                                                                                                    <p className="text-gray">Please be patient. This may take some time as per dataset size.</p>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        : this.state.dataStore[attribute.attribute + actions.name].data.data ?
                                                                                            <div className="card-chart-box text-center">
                                                                                                <LineChart divId={this.state.dataStore[attribute.attribute + actions.name].id} chartData={this.state.dataStore[attribute.attribute + actions.name].data.data} actionType={this.getAttributeName(attribute.attribute)} />
                                                                                            </div> :
                                                                                            <div className="card-message">
                                                                                                <p>There is no data to display.</p>
                                                                                            </div>
                                                                            }
                                                                        </div>
                                                                    </div>))
                                                                    }
                                                                </div>
                                                            </div>))}
                                                    </div>
                                                </React.Fragment>
                                                :
                                                this.state.attributeList.length == 0 ?
                                                    <AddNewButton
                                                        text1="No Attribute(s) available"
                                                        text2="You haven't created any Attribute(s) yet"
                                                        createItemOnAddButtonClick={() => this.props.history.push('/deviceAttributes')}
                                                        imageIcon="addTrend&Forecast.png"
                                                    />
                                                    :
                                                    <AddNewButton
                                                        text1="No Metric(s) available"
                                                        text2="You haven't created any Metric(s) yet"
                                                        createItemOnAddButtonClick={() => this.addNewConfigHandler()}
                                                        imageIcon="addTrend&Forecast.png"
                                                    />
                                        :
                                        <AddNewButton
                                            text1="No Trends & Forecast configuration available"
                                            text2="You haven't created any device type(s) yet. Please create a device type first."
                                            addButtonEnable={isNavAssigned("deviceTypes")}
                                            createItemOnAddButtonClick={() => this.props.history.push(`/addOrEditDeviceType`)}
                                            imageIcon="addTrend&Forecast.png"
                                        />
                                    }
                                </div>
                            </React.Fragment>
                            :
                            <div className="content-body">
                                <AddNewButton
                                    text1="No trends available"
                                    text2="You haven't created any device type(s) yet. Please create a device type first."
                                    addButtonEnable={isNavAssigned("deviceTypes")}
                                    createItemOnAddButtonClick={() => this.props.history.push(`/addOrEditDeviceType`)}
                                    imageIcon="addTrend&Forecast.png"
                                />
                            </div>
                        }
                    </React.Fragment>
                }


                {/* configure modal */}
                {this.state.isAddAttributeTab &&
                    <div className="modal d-block animated slideInDown" >
                        <div className="modal-dialog modal-dialog-centered">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h6 className="modal-title">Select Data Points
                                        <button
                                            type="button"
                                            className="close"
                                            data-tooltip data-tooltip-text="Close"
                                            data-tooltip-place="bottom"
                                            onClick={() => this.setState({
                                                isAddAttributeTab: false,
                                                editConfig: false
                                            })}
                                        >
                                            <i className="far fa-times"></i>
                                        </button>
                                    </h6>
                                </div>
                                <div className="modal-body">
                                    {!this.state.editConfig &&
                                        <React.Fragment>
                                            <div className="form-group">
                                                <label className="form-group-label">Header :</label>
                                                <input type="text" className="form-control" name="header" id="headerName"
                                                    defaultValue={this.state.attributeList.length > 0 && this.state.attributeList.find(val => val.attribute === this.state.tempConfig.attribute) ?
                                                        this.state.attributeList.find(val => val.attribute === this.state.tempConfig.attribute).displayName &&
                                                        this.state.attributeList.find(val => val.attribute === this.state.tempConfig.attribute).displayName :
                                                        this.state.tempConfig.attribute}
                                                    placeholder="Sample Header" />
                                            </div>
                                            <div className="form-group mb-2">
                                                <label className="form-group-label">Select Attribute (showing only numeric attributes) :</label>
                                                <div className="d-flex">
                                                    {this.state.attributeList.filter(attr => !this.state.configData.config.some(temp => temp.attribute === attr.attribute)).map((attribute, index) =>
                                                        <div className="flex-33 pd-r-10" key={index}>
                                                            <label className="radio-button mb-2">
                                                                <span className="radio-button-text-monitoring">{attribute.displayName ? attribute.displayName : attribute.attribute}</span>
                                                                <input type="radio" checked={attribute.attribute == this.state.tempConfig.attribute} name="attribute" id="attribute" onChange={() => this.attributeChangeHandler(attribute.attribute)} />
                                                                <span className="radio-button-mark"></span>
                                                            </label>
                                                        </div>
                                                    )}
                                                </div>
                                            </div>
                                        </React.Fragment>
                                    }
                                    <div className="form-group mb-0">
                                        <div className="attribute-list">
                                            <div className="attribute-list-header d-flex">
                                                <p className="flex-60">Metric</p>
                                                <p className="flex-40">Duration</p>
                                            </div>
                                            <div className="attribute-list-body">
                                                {this.state.metricActions.map((action, index) =>
                                                    <ul className="list-style-none d-flex align-items-center" key={index}>
                                                        <li className="flex-60">
                                                            <label className="check-box">
                                                                <span className="check-text text-capitalize">{action}</span>
                                                                <input type="checkbox" disabled={this.state.tempConfig.attribute == ""} checked={this.state.tempConfig.actions.some(act => act.name === action)} name="action" id={action} onChange={() => this.sendRpcHandler(event, index)} />
                                                                <span className="check-mark"></span>
                                                            </label>
                                                        </li>
                                                        <li className="flex-40">
                                                            <select className="form-control" id="duration" value={this.state.tempConfig.actions.some(act => act.name === action) ? this.state.tempConfig.actions.find(act => act.name === action).duration : ""} onChange={() => this.sendRpcHandler(event, index)} disabled={!(this.state.tempConfig.actions.some(act => act.name === action))}>
                                                                <option value="">Select</option>
                                                                {this.state.durationOptions[action === "forecast" ? "filter2" : "filter1"].map((duration, index) =>
                                                                    <option value={duration.value} key={index}>{duration.displayName}</option>
                                                                )}
                                                            </select>
                                                        </li>
                                                    </ul>
                                                )}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-light" data-dismiss="modal"
                                        onClick={() => this.setState({
                                            isAddAttributeTab: false,
                                            editConfig: false
                                        })}
                                    >Cancel</button>
                                    <button
                                        type="button"
                                        className="btn btn-success"
                                        disabled={this.state.tempConfig.attribute === "" || this.state.tempConfig.actions.length === 0 || (this.state.tempConfig.actions.length && this.state.tempConfig.actions.some(ack => ack.duration === ""))}
                                        onClick={() => this.saveChangeHandler()}
                                    >
                                        {this.state.editConfig ? "Update" : "Save"}
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                }
                {/* end configure modal */}

                {this.state.isOpen &&
                    <NotificationModal
                        type={this.state.type}
                        message2={this.state.message2}
                        onCloseHandler={this.onCloseHandler}
                    />
                }

                {this.state.confirmState &&
                    <ConfirmModel
                        status={"delete"}
                        delete={""}
                        confirmClicked={() => this.deleteAttribute()}
                        cancelClicked={() => {
                            this.cancelClicked()
                        }}
                    />
                }

            </React.Fragment>
        );
    }
}

DeviceDashboardsTrendsForecast.propTypes = {
    dispatch: PropTypes.func.isRequired
};

let allSelectors = {}
Object.entries(SELECTORS).map(([key, value]) => {
    allSelectors[key] = value()
})

const mapStateToProps = createStructuredSelector(allSelectors)

function mapDispatchToProps(dispatch) {
    let allActions = { dispatch }
    Object.entries(ACTIONS).map(([key, value]) => {
        allActions[key] = (...args) => dispatch(value(...args))
    })
    return allActions
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: 'deviceDashboardsTrendsForecast', reducer });
const withSaga = injectSaga({ key: 'deviceDashboardsTrendsForecast', saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(DeviceDashboardsTrendsForecast);

