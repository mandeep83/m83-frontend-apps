import React from "react";
import * as am4charts from "@amcharts/amcharts4/charts";
import * as am4core from "@amcharts/amcharts4/core";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";

let chart = {}

class lineChart extends React.Component {
  componentDidMount() {
    this.createChart();
  }

  createChart() {
    let _this = this;
    let config = {}
    config.data = _this.props.chartData
    config.cursor = {}
    config.series = [{
      id: "Series0",
      name: "Series0",
      stroke: "#00da00",
      strokeWidth: 2,
      type: "LineSeries",
      minBulletDistance: 50,
      bullets: [{
        children: [{
          "type": "Circle",
          "fill": am4core.color("#fff"),
          "cursorOverStyle": am4core.MouseCursorStyle.pointer,
          "resizable": true,
          "width": 8,
          "height": 8,
          "states": {
            "hover": {
              "properties": {
                "scale": 1.7
              }
            }
          },
          "strokeWidth": 3,
          "filters": [{
            "type": "DropShadowFilter",
            "color": "#B8B8B8",
            "dx": 3,
            "dy": 3
          }]
        }],
      }],
      fill: "#00da00",
      filters: [{
        "type": "DropShadowFilter",
        "color": "#B8B8B8",
        "dx": 8,
        "dy": 8
      }],
      fillOpacity: 0.4,
      dataFields: {
        valueY: "value",
        dateX: "timestamp",
      },
      tooltipText: `${_this.props.actionType} : [bold]{valueY}`,
      tooltip: {
        getFillFromObject: false,
        background: {
          fill: "#211D21",
          fillOpacity: 1,
        },
        label: {
          fill: "#fff",
        }
      },
      segments: {
        fillModifier: {
          type: "LinearGradientModifier",
          opacities: [1, 0],
          gradient: {
            rotation: 90
          }
        }
      },
      propertyFields: {
        strokeDasharray: "dashLength",
        fillOpacity: "alpha",
        stroke: "lineColor",
        fill: "lineColor"
      }
    }]
    config.xAxes = [{
      "startLocation": 0.5,
      "endLocation": 0.7,
      "type": "DateAxis",
      "cursorTooltipEnabled": true,
      "tooltipDateFormat": "dd MMM yyyy HH:mm",
      "dateFormats": {
        "day": "dd MMM",
        "hour": "dd MMM HH:mm",
        "minute": "dd MMM HH:mm",
        "second": "HH:mm:ss",
        "millisecond": "HH:mm:ss"
      },
      "renderer": {
        "grid": {
          "disabled": true
        },
        "labels": {
          "disabled": false
        },
      },
      "fontSize": "12px",
    }]
    config.yAxes = [{
      type: "ValueAxis",
      cursorTooltipEnabled: false,
      renderer: {
        line: {
          strokeOpacity: 1,
          strokeWidth: 1,
          // stroke: "#68fbea"
        },
        labels: {
          fill: "#757575"
        }
      },
      baseValue: -1000,
      title: {
        text: `${_this.props.actionType}`,
        fill: "#757575",
        strokeWidth: 1,
        fontWeight: "bold"
      },
      maxPrecision: 0,
    }]
    /*config.legend = {
    useDefaultMarker: true,
    markers: {
    width: 15,
    height: 15
    }
    }*/
    config.scrollbarX = {
      "type": "XYChartScrollbar",
      series: ["Series0"],
      minHeight: 30,
      dy: -10
    }
    config.paddingTop = 10
    config.paddingRight = 30
    config.paddingLeft = 0
    config.paddingBottom = 0
    chart[_this.props.divId] = am4core.createFromConfig(config, _this.props.divId, am4charts.XYChart);
  }

  componentWillUnmount() {
    chart[this.props.divId].dispose()
  }

  render() {
    return (<React.Fragment>
      <div id={this.props.divId} className="h-100"></div>
    </React.Fragment>);
  }
}

export default lineChart;