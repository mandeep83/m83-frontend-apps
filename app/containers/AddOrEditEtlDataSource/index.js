/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import * as Actions from "./actions";
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import * as Selectors from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import Loader from "../../components/Loader";
import NotificationModal from '../../components/NotificationModal/Loadable'
import JSONInput from "react-json-editor-ajrm/dist";
import ReactTooltip from 'react-tooltip';
import MQTT from 'mqtt';
import XLSX from 'xlsx';
import TagsInput from 'react-tagsinput';
import { getVMQCredentials, uIdGenerator } from "../../commonUtils";
import ReactJson from "react-json-view";
import ReactTable from 'react-table';
import cloneDeep from 'lodash/cloneDeep'
import isPlainObject from 'lodash/isPlainObject'
import Map from "../../components/Map"
import ConfirmModel from "../../components/ConfirmModel";

const EventEmitter = require('events');
const emitter = new EventEmitter()
emitter.setMaxListeners(50)
let COMMAND_OBJ = {
    controlName: "",
    controlCommand: {},
}
let client;
let s3Regions = [
    { name: "US East (Ohio)", value: "us-east-2" },
    { name: "US East (N. Virginia)", value: "us-east-1" },
    { name: "US West (N. California)", value: "us-west-1" },
    { name: "US West (Oregon)", value: "us-west-2" },
    { name: "Asia Pacific (Hong Kong)", value: "ap-east-1" },
    { name: "Asia Pacific (Mumbai)", value: "ap-south-1" },
    { name: "Asia Pacific (Osaka-Local)", value: "ap-northeast-3" },
    { name: "Asia Pacific (Seoul)", value: "ap-northeast-2" },
    { name: "Asia Pacific (Singapore)", value: "ap-southeast-1" },
    { name: "Asia Pacific (Sydney)", value: "ap-southeast-2" },
    { name: "Asia Pacific (Tokyo)", value: "ap-northeast-1" },
    { name: "Canada (Central)", value: "ca-central-1" },
    { name: "China (Beijing)", value: "cn-north-1" },
    { name: "China (Ningxia)", value: "cn-northwest-1" },
    { name: "Europe (Frankfurt)", value: "eu-central-1" },
    { name: "Europe (Ireland)", value: "eu-west-1" },
    { name: "Europe ", value: "eu-west-2" },
    { name: "Europe (Paris)", value: "eu-west-3" },
    { name: "Europe (Stockholm)", value: "eu-north-1" },
    { name: "Middle East (Bahrain)", value: "me-south-1" },
    { name: "South America (Sao Paulo)", value: "sa-east-1" },
    { name: "AWS GovCloud (US-East)", value: "us-gov-east-1" },
    { name: "AWS GovCloud (US-West)", value: "us-gov-west-1" }
]

/* eslint-disable react/prefer-stateless-function */
export class AddOrEditEtlDataSource extends React.Component {

    getPayloadPropertiesByConnector = () => {
        switch (this.props.match.params.connector) {
            case "KAFKA": {
                return {
                    bootstrapServer: '',
                    isAuthenticated: true,
                    username: '',
                    password: '',
                    topics: [{
                        name: '',
                        schema: ''
                    }],
                }
            }
            case "MQTT": {
                return {
                    topics: [{
                        name: '',
                        schema: ''
                    }],
                    deviceIds: []
                }
            }
            case "HTTP": {
                return {
                    basePath: 'api/v1/connector/',
                    sinkType: '',
                    category: "",
                    collectionName: '',
                }
            }
            case "MONGODB": {
                return {
                    url: "*******",
                    database: "*******",
                    isAuthenticated: true,
                    username: "*******",
                    password: "*******",
                }
            }
            case "S3": {
                return {
                    isAuthenticated: true,
                    accessKey: '',
                    secretKey: '',
                    region: '',
                    buckets: [{
                        name: '',
                        dataDirs: [{
                            name: '',
                            basePath: '',
                            format: '',
                            partitionBy: []
                        }]
                    }],
                }
            }
            case "HDFS": {
                return {
                    isAuthenticated: true,
                    url: '',
                    basePath: '',
                    dataDirs: [{
                        name: '',
                        basePath: '',
                        format: '',
                        partitionBy: []
                    }]
                }
            }
            case "EXCEL": {
                return {
                    fileList: [],
                }
            }
            case "CSV": {
                return {
                    fileList: [],
                }
            }
            case "HIVE": {
                return {
                    "database": "",
                    "tableList": [{
                        "name": "",
                        "partitionBy": [],
                    }]
                }
            }
            case "EMAIL": {
                return {
                    "accessKey": "",
                    "secretKey": "",
                    "region": ""
                }
            }
        }
    }

    state = {
        payload: {
            name: "",
            description: "",
            category: this.props.match.params.connector,
            source: true,
            sink: false,
            isExternal: this.props.match.params.connector === 'HTTP' || this.props.match.params.connector === 'MQTT' || this.props.match.params.connector === 'MONGODB' ? false : true,
            properties: this.getPayloadPropertiesByConnector()
        },
        scheme: "ssl://",
        isFetching: true,
        url: '',
        showPassword: false,
        fileUploaded: '',
        selectedTopic: {
            name: '',
            schema: {}
        },
        messages: [],
        sniffingTopic: '',
        data: [],
        cols: [],
        dataView: 'FILE',
        isFetchingAPIKey: false,
        dataConnectors: [],
        userJson: {},
        noOfClientIds: "1",
        topicSuffix: '',
        generatingClientIds: false,
        availableClientIds: 0,
        isDescriptionChanged: false,
        isLoader: false,
        emailCredentialsInitiated: false,
        activeCommandIndex: -1,
        controlConfig: [cloneDeep(COMMAND_OBJ)]
    };

    componentDidMount() {
        if (this.props.match.params.id) {
            this.props.getDataSource(this.props.match.params.id)
        }
        else {
            this.props.getConnectorsList();
        }
        if (this.props.match.params.connector === 'MQTT' && !this.state.payload.isExternal && localStorage.tenantType == "SAAS") {
            this.props.fetchAvailableDeviceIdCount();
        } else if (this.props.match.params.connector === 'HTTP') {
            this.props.getAllConnectorsCategory();
        } else if (this.props.match.params.connector === 'EMAIL') {
            let payload = cloneDeep(this.state.payload);
            payload.sink = true
            payload.source = false
            this.setState({
                payload
            })
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.getExcelFileDataSuccess && nextProps.getExcelFileDataSuccess !== this.props.getExcelFileDataSuccess) {
            let payload = JSON.parse(JSON.stringify(this.state.payload));
            if (this.props.match.params.connector === "EXCEL") {
                payload.properties.fileList = nextProps.getExcelFileDataSuccess.map(url => {
                    return ({
                        filePath: url.secure_url.split('.com')[1],
                        sheetName: url.sheetName,
                        fileName: payload.properties.fileName
                    })
                })
                this.displayExcelData(payload.properties.fileList[0].filePath, 0)
            }
            else {
                payload.properties.fileList.push({
                    filePath: nextProps.getExcelFileDataSuccess.secure_url.split('.com')[1],
                    fileName: payload.properties.fileName
                })
            }
            payload.properties = {
                fileList: payload.properties.fileList,
            }
            this.setState({
                payload,
                excelUploadLoader: false
            })
        }

        if (nextProps.getExcelFileDataFailure && nextProps.getExcelFileDataFailure !== this.props.getExcelFileDataFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.getExcelFileDataFailure,
                excelUploadLoader: false
            })
        }

        if (nextProps.getDisplayCodeContentSuccess && nextProps.getDisplayCodeContentSuccess !== this.props.getDisplayCodeContentSuccess) {
            let arrayBuffer = nextProps.getDisplayCodeContentSuccess;
            /* convert data to binary string */
            let binaryData = new Uint8Array(arrayBuffer);
            let arr = new Array();
            for (let i = 0; i != binaryData.length; ++i) arr[i] = String.fromCharCode(binaryData[i]);
            let bstr = arr.join("");
            /* Call XLSX */
            let workbook = XLSX.read(bstr, {
                type: "binary"
            });
            /* DO SOMETHING WITH workbook HERE */
            /* Get worksheet */
            let worksheet = workbook.Sheets[workbook.SheetNames[0]];
            let data = XLSX.utils.sheet_to_json(worksheet, { header: 1 });
            const make_cols = refstr => {
                let o = [], C = XLSX.utils.decode_range(refstr).e.c + 1;
                for (var i = 0; i < C; ++i) o[i] = { name: XLSX.utils.encode_col(i), key: i }
                return o;
            };
            let cols = make_cols(worksheet['!ref'])
            this.setState({ data, cols });
        }

        if (nextProps.getDisplayCodeContentFailure && nextProps.getDisplayCodeContentFailure !== this.props.getDisplayCodeContentFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.getDisplayCodeContentFailure,
            })
        }

        if (nextProps.fileUploadSuccess && nextProps.fileUploadSuccess !== this.props.fileUploadSuccess) {
            let payload = JSON.parse(JSON.stringify(this.state.payload))
            payload.properties[`${nextProps.fileUploadSuccess.eventId}Url`] = nextProps.fileUploadSuccess.url.split('.com/')[1]
            this.setState({
                payload,
                isUploadingFile: false
            })
        }

        if (nextProps.fileUploadFailure && nextProps.fileUploadFailure !== this.props.fileUploadFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.fileUploadFailure,
                isUploadingFile: false
            })
        }

        if (nextProps.generateSchemaSuccess && nextProps.generateSchemaSuccess !== this.props.generateSchemaSuccess) {
            let payload = JSON.parse(JSON.stringify(this.state.payload))
            payload.properties.topics.map(topic => {
                if (topic.id === nextProps.generateSchemaSuccess.id) {
                    let isBackendGenerated = !topic.id.includes("_userJson")
                    topic.isGeneratingSchema = false;
                    topic.schema = JSON.parse(nextProps.generateSchemaSuccess.data.data);
                    topic.isBackendGenerated = isBackendGenerated
                    if (!isBackendGenerated)
                        topic.userJson = this.state.userJson;
                }
            })
            this.setState({
                selectedTopicIndex: null,
                showJsonModal: false,
                jsonModalLoader: false,
                userJson: {},
                payload,
                modalType: "success",
                isOpen: true,
                message2: nextProps.generateSchemaSuccess.data.message,
            })
        }

        if (nextProps.generateSchemaFailure && nextProps.generateSchemaFailure !== this.props.generateSchemaFailure) {
            let payload = JSON.parse(JSON.stringify(this.state.payload))
            payload.properties.topics.map(topic => {
                if (topic.id === nextProps.generateSchemaFailure.id) {
                    topic.isGeneratingSchema = false
                }
            })
            this.setState({
                jsonModalLoader: false,
                modalType: "error",
                isOpen: true,
                message2: nextProps.generateSchemaFailure.error,
                payload
            })
        }

        if (nextProps.testConnectionSuccess !== undefined && nextProps.testConnectionSuccess !== this.props.testConnectionSuccess) {
            if (nextProps.testConnectionSuccess) {
                this.setState({
                    modalType: "success",
                    isOpen: true,
                    message2: "Connection Tested Successfully",
                    connectionTestedSuccessFully: true,
                    isTestingConnection: false
                })
            }
            else if (nextProps.testConnectionSuccess === false) {
                this.setState({
                    modalType: "error",
                    isOpen: true,
                    message2: "Connection Test Failed",
                    connectionTestFailed: true,
                    isTestingConnection: false
                })
            }
        }

        if (nextProps.testConnectionFailure && nextProps.testConnectionFailure !== this.props.testConnectionFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: "Connection Tested Failed",
                connectionTestFailed: true,
                isTestingConnection: false

            })
        }

        if (nextProps.getDataSourceSuccess && nextProps.getDataSourceSuccess !== this.props.getDataSourceSuccess) {
            let controlConfig = nextProps.getDataSourceSuccess.controlConfig
            delete nextProps.getDataSourceSuccess.controlConfig
            this.props.getConnectorsList();
            let payload = cloneDeep(nextProps.getDataSourceSuccess),
                scheme,
                noOfClientIds = this.state.noOfClientIds;
            payload.properties.topics && payload.properties.topics.map(topic => {
                if (topic.schema)
                    topic.schema !== '' ? topic.schema = JSON.parse(topic.schema) : ''
            })

            if (this.props.match.params.connector === "MQTT") {
                if (payload.isExternal) {
                    let link = payload.properties.bootstrapServer.split('://')
                    scheme = link[0] + '://'
                    payload.properties.bootstrapServer = link[1]
                }
            }
            else if (this.props.match.params.connector === "MONGODB") {
                if (!payload.isExternal) {
                    payload.properties.isAuthenticated = true
                    payload.properties.username = '*******'
                    payload.properties.password = '*******'
                    payload.properties.url = "*******"
                    payload.properties.database = "*******"
                }
            }
            else if (this.props.match.params.connector === "KAFKA") {
                if (!payload.isExternal) {
                    payload.properties.isAuthenticated = true
                    payload.properties.username = '*******'
                    payload.properties.password = '*******'
                    payload.properties.bootstrapServer = "*******"
                }
            }
            else if (this.props.match.params.connector === "EXCEL" || this.props.match.params.connector === "CSV") {
                this.displayExcelData(payload.properties.fileList[0].filePath, 0)
            }

            this.setState({
                payload,
                scheme,
                noOfClientIds,
                controlConfig
            })
        }

        if (nextProps.getDataSourceFailure && nextProps.getDataSourceFailure !== this.props.getDataSourceFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.getDataSourceFailure,
                isFetching: false
            })
        }

        if (nextProps.getSaveDataSourceSuccess && nextProps.getSaveDataSourceSuccess !== this.props.getSaveDataSourceSuccess) {
            this.setState({
                modalType: "success",
                isOpen: true,
                message2: nextProps.getSaveDataSourceSuccess,
                saveSuccess: true,
                isFetching: false
            })
        }

        if (nextProps.getSaveDataSourceFailure && nextProps.getSaveDataSourceFailure !== this.props.getSaveDataSourceFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.getSaveDataSourceFailure,
                isFetching: false,
            })
        }

        if (nextProps.connectionListSuccess && nextProps.connectionListSuccess !== this.props.connectionListSuccess) {
            let connectorCount = nextProps.connectionListSuccess.filter(connectors => connectors.category === this.props.match.params.connector).length;
            this.setState({
                connectorCount,
                isFetching: false
            })
        }

        if (nextProps.connectionListFailure && nextProps.connectionListFailure !== this.props.connectionListFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.connectionListFailure,
                isFetching: false
            })
        }

        if (nextProps.apiKeySuccess && nextProps.apiKeySuccess !== this.props.apiKeySuccess) {
            let payload = JSON.parse(JSON.stringify(this.state.payload));
            payload.properties.apiKey = nextProps.apiKeySuccess;
            this.setState({
                isFetchingAPIKey: false,
                payload
            });
        }

        if (nextProps.apiKeyFailure && nextProps.apiKeyFailure !== this.props.apiKeyFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.apiKeyFailure,
                isFetchingAPIKey: false,
            })
        }

        if (nextProps.dataConnectorsSuccess && nextProps.dataConnectorsSuccess !== this.props.dataConnectorsSuccess) {
            this.setState({
                dataConnectors: nextProps.dataConnectorsSuccess
            });
        }

        if (nextProps.dataConnectorsFailure && nextProps.dataConnectorsFailure !== this.props.dataConnectorsFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.dataConnectorsFailure,
                isFetchingAPIKey: false,
            })
        }

        if (nextProps.createDeviceSuccess && nextProps.createDeviceSuccess !== this.props.createDeviceSuccess) {
            let payload = cloneDeep(this.state.payload);
            payload.id = nextProps.createDeviceSuccess.id
            payload.mailRequest = payload.mailRequest ? payload.mailRequest : 0
            this.setState({
                isFetching: false,
                payload
            }, () => { this.props.history.push(`/addOrEditConnector/MQTT/${nextProps.createDeviceSuccess.id}`) });
        }

        if (nextProps.createDeviceFailure && nextProps.createDeviceFailure !== this.props.createDeviceFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.createDeviceFailure,
                isFetchingAPIKey: false,
                isFetching: false
            })
        }

        if (nextProps.clientIdsSuccess && nextProps.clientIdsSuccess !== this.props.clientIdsSuccess) {
            let payload = cloneDeep(this.state.payload),
                deviceIds = payload.properties.deviceIds ? payload.properties.deviceIds : [];
            nextProps.clientIdsSuccess.deviceIds.map(el => {
                deviceIds.push({
                    deviceId: el,
                    lastReportedAt: null,
                    deviceStatus: ''
                });
            });
            payload.properties = {
                deviceIds,
                credentials: payload.properties.credentials ? payload.properties.credentials : nextProps.clientIdsSuccess.url,
                password: payload.properties.password ? payload.properties.password : nextProps.clientIdsSuccess.password,
                topics: [{
                    name: payload.properties.topics && payload.properties.topics[0].name ? payload.properties.topics[0].name : nextProps.clientIdsSuccess.topic,
                    schema: '',
                    topicId: ''
                }]
            }
            this.setState({
                payload,
                generatingClientIds: localStorage.tenantType === "SAAS"
            }, () => {
                localStorage.tenantType === "SAAS" && this.props.fetchAvailableDeviceIdCount()
            });
        }

        if (nextProps.clientIdsFailure && nextProps.clientIdsFailure !== this.props.clientIdsFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.clientIdsFailure,
                isFetchingAPIKey: false,
                generatingClientIds: false,
            })
        }

        if (nextProps.editDeviceSuccess && nextProps.editDeviceSuccess !== this.props.editDeviceSuccess) {
            let { deviceId, deviceDetails } = nextProps.editDeviceSuccess
            let payload = cloneDeep(this.state.payload)
            let objIndex = payload.properties.deviceIds.findIndex((obj => obj.deviceId == deviceId));
            payload.properties.deviceIds[objIndex].originalLat = deviceDetails.lat;
            payload.properties.deviceIds[objIndex].originalLng = deviceDetails.originalLng;
            payload.properties.deviceIds[objIndex].originalLocation = deviceDetails.location;
            payload.properties.deviceIds[objIndex].originalName = deviceDetails.name;
            payload.properties.deviceIds[objIndex].isLoading = false
            this.setState({
                payload
            })
        }

        if (nextProps.editDeviceFailure && nextProps.editDeviceFailure !== this.props.editDeviceFailure) {
            let payload = cloneDeep(this.state.payload)
            let objIndex = payload.properties.deviceIds.findIndex((obj => obj.deviceId == nextProps.editDeviceFailure.deviceId));
            payload.properties.deviceIds[objIndex].isLoading = false
            payload.properties.deviceIds[objIndex].isEdit = true
            this.setState({
                payload,
                modalType: "error",
                isOpen: true,
                message2: nextProps.editDeviceFailure.error,
                isFetchingAPIKey: false,
            })
        }

        if (nextProps.availableDeviceIds && nextProps.availableDeviceIds !== this.props.availableDeviceIds) {
            this.setState({
                generatingClientIds: false,
                availableClientIds: nextProps.availableDeviceIds.count,
            });
        }

        if (nextProps.availableDeviceIdsFailure && nextProps.availableDeviceIdsFailure !== this.props.availableDeviceIdsFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.availableDeviceIdsFailure,
                isFetchingAPIKey: false,
            })
        }

        if (nextProps.deleteDeviceSuccess && nextProps.deleteDeviceSuccess !== this.props.deleteDeviceSuccess) {
            let payload = cloneDeep(this.state.payload)
            payload.properties.deviceIds = payload.properties.deviceIds.filter((obj => obj.deviceId !== nextProps.deleteDeviceSuccess.deviceId));
            let message2 = JSON.stringify(nextProps.deleteDeviceSuccess.response) === "{}" ? "Device deleted Successfully." : nextProps.deleteDeviceSuccess.response,
                availableClientIds = this.state.availableClientIds + 1;
            this.setState({
                modalType: "success",
                isOpen: true,
                message2,
                isLoader: false,
                payload,
                availableClientIds
            })
        }

        if (nextProps.deleteDeviceFailure && nextProps.deleteDeviceFailure !== this.props.deleteDeviceFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.deleteDeviceFailure,
                isLoader: false,
                isFetching: false,
            })
        }
        if (nextProps.emailCredentialsSuccess && nextProps.emailCredentialsSuccess !== this.props.emailCredentialsSuccess) {
            let payload = cloneDeep(this.state.payload);
            payload.mailRequest += 1
            this.setState({
                modalType: "success",
                isOpen: true,
                message2: nextProps.emailCredentialsSuccess.response.message,
                emailCredentialsInitiated: false,
                payload
            })
        }

        if (nextProps.emailCredentialsFailure && nextProps.emailCredentialsFailure !== this.props.emailCredentialsFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.emailCredentialsFailure,
                emailCredentialsInitiated: false,
            })
        }
        if (nextProps.saveCommandsSuccess && nextProps.saveCommandsSuccess !== this.props.saveCommandsSuccess) {
            this.setState({
                modalType: "success",
                isOpen: true,
                message2: nextProps.saveCommandsSuccess,
                activeCommandIndex: -1,
                commandModalLoader: false,

            })
        }

        if (nextProps.saveCommandsFailure && nextProps.saveCommandsFailure !== this.props.saveCommandsFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.saveCommandsFailure,
                commandModalLoader: false,
            })
        }
    }

    onChangeHandler = (event) => {
        let payload = JSON.parse(JSON.stringify(this.state.payload)),
            url = this.state.url,
            connectionTestedSuccessFully = this.state.connectionTestedSuccessFully,
            connectionTestFailed = this.state.connectionTestFailed,
            isTestingConnection = this.state.isTestingConnection,
            isDescriptionChanged = this.state.isDescriptionChanged;

        if (event.target.name === 'sourceSinkType') {
            if (event.target.value === 'source') {
                payload.source = true;
                payload.sink = false;
            } else if (event.target.value === 'sink') {
                payload.sink = true;
                payload.source = false;
            } else {
                payload.source = true;
                payload.sink = true;
            }
        } else if (event.target.name === 'connectionType') {
            url = '';
            if (payload.category === "MQTT" || payload.category === "KAFKA") {
                payload.properties.topics = [{
                    name: '',
                    schema: ''
                }]
            }
            payload.properties.username = ''
            payload.properties.password = ''
            payload.properties.bootstrapServer = ''
            payload.properties.url = ''
            payload.properties.database = ''
            connectionTestFailed = false
            connectionTestedSuccessFully = false
            isTestingConnection = false
            payload.properties.isAuthenticated = false
            payload.isExternal = event.target.value === 'external' ? true : false
            if (event.target.value === 'internal' && payload.category === "MQTT") {
                payload.properties = {
                    topics: [{
                        name: '',
                        schema: ''
                    }],
                    deviceIds: []
                }
            }
            else if (event.target.value === 'external' && payload.category === "MQTT") {
                payload.properties = {
                    bootstrapServer: "",
                    isAuthenticated: false,
                    username: '',
                    password: '',
                    certificate: '',
                    certificateUrl: '',
                    privateKey: '',
                    privateKeyUrl: '',
                    caCertificate: '',
                    caCertificateUrl: '',
                    topics: [{
                        name: '',
                        schema: ''
                    }]
                }
            }
            else if (event.target.value === 'internal' && payload.category === "MONGODB") {
                payload.properties.isAuthenticated = true
                payload.properties.username = '*******'
                payload.properties.password = '*******'
                payload.properties.url = "*******"
                payload.properties.database = "*******"
            }
            else if (event.target.value === 'internal' && payload.category === "S3") {
                payload.properties.isAuthenticated = true
                payload.properties.accessKey = '*******'
                payload.properties.secretKey = "*******"
                payload.properties.region = "us-west-2"
            }
            else if (event.target.value === 'external' && payload.category === "S3") {
                payload.properties.isAuthenticated = true
                payload.properties.accessKey = ''
                payload.properties.secretKey = ''
                payload.properties.region = ""
            }
            else if (event.target.value === 'internal' && payload.category === "KAFKA") {
                payload.properties.isAuthenticated = true
                payload.properties.username = '*******'
                payload.properties.password = '*******'
                payload.properties.bootstrapServer = "*******"
            }
        } else if (event.target.id) {
            if (event.target.id !== "description" && (event.target.value.trim() || event.target.value === "")) {
                payload[event.target.id] = event.target.value.replace(/\s+/g, '');
            } else {
                payload[event.target.id] = event.target.value;
            }
        }


        if (payload.category === "MQTT" && (!payload.isExternal) && event.target.id === "description") {
            isDescriptionChanged = true
        }

        this.setState({
            payload,
            url,
            connectionTestFailed,
            connectionTestedSuccessFully,
            isTestingConnection,
            isDescriptionChanged
        })
    }

    definitionChangeHandler = (event, index) => {
        let payload = JSON.parse(JSON.stringify(this.state.payload));
        let connectionTestedSuccessFully = this.state.connectionTestedSuccessFully,
            connectionTestFailed = this.state.connectionTestFailed,
            isTestingConnection = this.state.isTestingConnection,
            scheme = this.state.scheme;
        if (event.target.id === "isAuthenticated") {
            payload.properties.isAuthenticated = event.target.checked;
            payload.properties.username = '';
            payload.properties.password = '';
            connectionTestFailed = false
            connectionTestedSuccessFully = false
            isTestingConnection = false
        }
        else if (event.target.id === "topicName") {
            payload.properties.topics[index].name = event.target.value
            payload.properties.topics[index].schema = ''
            payload.properties.topics[index].isBackendGenerated = false
        }
        else if (event.target.id === "username" || event.target.id === "password" || event.target.id === "bootstrapServer"
            || event.target.id === "accessKey" || event.target.id === "secretKey" || event.target.id === "url" || event.target.id === "database") {
            payload.properties[event.target.id] = event.target.value;
            connectionTestFailed = false
            connectionTestedSuccessFully = false
            isTestingConnection = false
        }
        else if (event.target.id === 'scheme') {
            scheme = event.target.value
        }
        else {
            payload.properties[event.target.id] = event.target.value;
            if (event.target.id === "sinkType") {
                payload.properties.category = "";
                payload.properties.collectionName = "";
                payload.properties.topic = ""
            }
        }
        this.setState({
            payload,
            connectionTestFailed,
            connectionTestedSuccessFully,
            isTestingConnection,
            scheme
        })
    }

    onCloseHandler = () => {
        this.setState({
            isOpen: false,
            message2: '',
            modalType: '',
        }, () => {
            if (this.state.saveSuccess) {
                this.props.history.push(`/connectors/${this.props.match.params.connector}`)
            }
        });
    }

    submitHandler = event => {
        event.preventDefault();
        let payload = JSON.parse(JSON.stringify(this.state.payload));
        if (payload.category === "KAFKA" || payload.category === "MQTT") {
            if (payload.category === "MQTT" && payload.isExternal) {
                payload.properties.bootstrapServer = this.state.scheme + payload.properties.bootstrapServer
                payload.properties.sslEnabled = payload.properties.bootstrapServer.split('://')[0] === "ssl" ? true : false
            }
            payload.properties.topics && payload.properties.topics.map(topic => {
                topic.schema = JSON.stringify(topic.schema)
                topic.isGeneratingSchema = false
                if (!this.props.match.params.id) {
                    delete topic.id
                }
            })
        }
        this.setState({
            isFetching: true,
        }, () => { this.props.saveDataSource(payload) });

    };

    fileUploadHandler = event => {
        let payload = JSON.parse(JSON.stringify(this.state.payload)),
            uploadedFile = event.target.files[0],
            excelUploadLoader = this.state.excelUploadLoader,
            connector = this.props.match.params.connector;
        if (connector === "EXCEL") {
            payload.properties.fileName = uploadedFile.name
            this.props.getExcelFileData(uploadedFile, connector)
            excelUploadLoader = true
        }
        else if (connector === "CSV" || connector === "JSON") {
            payload.properties.fileName = uploadedFile.name
            this.props.getExcelFileData(uploadedFile, connector)
            excelUploadLoader = true
        }
        else {
            payload.properties[event.target.name] = event.target.files[0].name
            this.props.uploadFileHandler(uploadedFile, "certificate", event.target.name);
        }
        this.setState({
            payload,
            excelUploadLoader
        });
    }

    openSchemaModal = (selectedTopic, index) => {
        selectedTopic = JSON.parse(JSON.stringify(selectedTopic))
        let userJson = (index >= 0 && selectedTopic.userJson) ? selectedTopic.userJson : JSON.parse(JSON.stringify(this.state.userJson))
        this.setState({
            userJson,
            selectedTopic,
            selectedTopicIndex: index,
            showJsonModal: true
        })
    }

    closeHandler = () => {
        this.setState((previousState, props) => ({
            isOpen: false,
            isFetching: previousState.modalType === "success",
            modalType: '',
            message2: ''
        }))
    }

    addDeleteTopics = (mode, index) => {
        let payload = JSON.parse(JSON.stringify(this.state.payload))
        if (mode === "add") {
            payload.properties.topics.push({
                name: '',
                schema: ''
            })
        } else {
            payload.properties.topics.splice(index, 1)
        }
        this.setState({
            payload
        })
    }

    addDeleteBuckets = (mode, index, type, childIndex) => {
        let payload = JSON.parse(JSON.stringify(this.state.payload))
        if (type === "parent") {
            if (mode === "add") {
                payload.properties.buckets.push({
                    name: '',
                    dataDirs: [{
                        name: '',
                        basePath: '',
                        format: '',
                        partitionBy: []
                    }]
                })
            } else {
                payload.properties.buckets.splice(index, 1)
            }
        } else {
            if (mode === "add") {
                payload.properties.buckets[index].dataDirs.push({
                    name: '',
                    basePath: '',
                    format: '',
                    partitionBy: []
                })
            } else {
                payload.properties.buckets[index].dataDirs.splice(childIndex, 1)
                if (payload.properties.buckets[index].dataDirs.length === 0) {
                    payload.properties.buckets.splice(index, 1)
                }
            }
        }
        this.setState({
            payload
        })
    }

    testConnection = () => {
        let payload = JSON.parse(JSON.stringify(this.state.payload))
        let payloadForTestConnection = payload.properties
        this.setState({
            isTestingConnection: true,
            isOpen: false
        })
        delete payloadForTestConnection.dataDirs;
        delete payloadForTestConnection.certificate;
        delete payloadForTestConnection.caCertificate;
        delete payloadForTestConnection.region;
        delete payloadForTestConnection.privateKey;
        payloadForTestConnection.isExternal = payload.isExternal
        payloadForTestConnection.category = this.state.payload.category;
        if (payloadForTestConnection.category === "KAFKA" || payloadForTestConnection.category === "MQTT") {
            delete payloadForTestConnection.topics;
            if (payloadForTestConnection.category == "MQTT") {
                payloadForTestConnection.bootstrapServer = this.state.scheme + payloadForTestConnection.bootstrapServer
                payload.properties.sslEnabled = payload.properties.bootstrapServer.split('://')[0] === "ssl" ? true : false
            }
            if (payloadForTestConnection.bootstrapServer !== '' && !payloadForTestConnection.isAuthenticated) {
                delete payloadForTestConnection.username;
                delete payloadForTestConnection.password;
                this.props.testConnection(payloadForTestConnection)
            }
            else if (payloadForTestConnection.bootstrapServer !== '' && payloadForTestConnection.isAuthenticated
                && payloadForTestConnection.username !== '' && payloadForTestConnection.password !== '') {
                this.props.testConnection(payloadForTestConnection)
            }
        }
        else if (payloadForTestConnection.category === "S3") {
            delete payloadForTestConnection.buckets;
            this.props.testConnection(payloadForTestConnection)
        }
        else if (payloadForTestConnection.category === "MONGODB") {
            this.props.testConnection(payloadForTestConnection)
        }
    }

    generateSchema = (index, isUserJson) => {
        let payload = JSON.parse(JSON.stringify(this.state.payload))
        let connectors = {
            KAFKA: "kafkaSchema",
            MQTT: "mqttSchema"
        }
        if (!payload.properties.topics[index].id)
            payload.properties.topics[index].id = uIdGenerator();

        if (isUserJson) {
            payload.properties.topics[index].id += "_userJson"
            let payloadForSchema = { json: this.state.userJson }
            this.setState({
                jsonModalLoader: true,
                payload
            }, () => this.props.generateSchema(payloadForSchema, payload.properties.topics[index].id, "json"))
            return;
        }

        let payloadForSchema = JSON.parse(JSON.stringify(payload.properties));
        payloadForSchema.isExternal = payload.isExternal
        payloadForSchema.topic = payload.properties.topics[index].name
        delete payloadForSchema.certificate;
        delete payloadForSchema.caCertificate;
        delete payloadForSchema.privateKey;
        delete payloadForSchema.topics;

        if (this.state.payload.category == "MQTT") {
            payloadForSchema.bootstrapServer = this.state.scheme + payloadForSchema.bootstrapServer
            payloadForSchema.sslEnabled = payloadForSchema.bootstrapServer.split('://')[0] === "ssl" ? true : false
        }
        if (payload.properties.isAuthenticated && payload.properties.username !== '' && payload.properties.password !== '') {
            payload.properties.topics[index].isGeneratingSchema = true
        } else {
            delete payloadForSchema.username
            delete payloadForSchema.password
            payload.properties.topics[index].isGeneratingSchema = true
        }
        payload.properties.topics[index].schema = ''
        this.setState({
            payload,
            isOpen: false
        }, () => this.props.generateSchema(payloadForSchema, payload.properties.topics[index].id, connectors[this.props.match.params.connector]))
    }

    bucketChangeHandler = (event, index, childIndex) => {
        let payload = JSON.parse(JSON.stringify(this.state.payload))
        if (event.target.id === "bucketName") {
            payload.properties.buckets[index].name = event.target.value;
        }
        else if (event.target.id === "basePath") {
            let regex = /^\S+$/;
            if (regex.test(event.target.value)) {
                payload.properties.buckets[index].dataDirs[childIndex][event.target.id] = event.target.value
            }
        }
        else {
            payload.properties.buckets[index].dataDirs[childIndex][event.target.id] = event.target.value;
        }
        this.setState({
            payload
        })
    }

    addDeleteDirectories = (event, mode, index) => {
        let payload = JSON.parse(JSON.stringify(this.state.payload))
        if (mode === "add") {
            payload.properties.dataDirs.push({
                name: '',
                basePath: '',
                format: '',
                partitionBy: []
            })
        } else {
            payload.properties.dataDirs.splice(index, 1)
        }
        this.setState({
            payload
        })
    }

    dataDirectoryChangeHandler = (event, index) => {
        let payload = JSON.parse(JSON.stringify(this.state.payload))
        let id = event.target.id
        if (id === "dataDirsBasePath")
            id = "basePath"
        else if (id === "dataDirsName")
            id = "name"
        payload.properties.dataDirs[index][id] = event.target.value
        this.setState({
            payload
        })
    }

    inputTagsChangeHandler = (index, childIndex, category) => (tags) => {
        let payload = JSON.parse(JSON.stringify(this.state.payload));
        if (category === "S3") {
            payload.properties.buckets[index].dataDirs[childIndex].partitionBy = tags
        } else {
            payload.properties.dataDirs[index].partitionBy = tags
        }
        this.setState({
            payload
        })
    }

    validateData = () => {
        let payload = JSON.parse(JSON.stringify(this.state.payload)),
            checker = payload.properties;
        if (this.state.payload.isExternal) {
            if (this.state.payload.category === "KAFKA") {
                if (checker.bootstrapServer && !checker.isAuthenticated) {
                    return true;
                }
                else if (checker.bootstrapServer !== '' && checker.isAuthenticated && checker.username !== '' && checker.password !== '') {
                    return true;
                }
            }
            else if (this.state.payload.category === "MQTT") {
                if (this.state.scheme === "ssl://") {
                    if (checker.bootstrapServer && !checker.isAuthenticated && checker.caCertificateUrl && checker.certificateUrl && checker.privateKeyUrl) {
                        return true;
                    }
                    else if (checker.bootstrapServer !== '' && checker.isAuthenticated && checker.username !== ''
                        && checker.password !== '' && checker.caCertificateUrl && checker.certificateUrl && checker.privateKeyUrl) {
                        return true;
                    }
                }
                else {
                    if (checker.bootstrapServer && !checker.isAuthenticated) {
                        return true;
                    }
                    else if (checker.bootstrapServer !== '' && checker.isAuthenticated && checker.username !== '' && checker.password !== '') {
                        return true;
                    }
                }
            }
            else if (this.state.payload.category === "S3") {
                if (checker.accessKey !== '' && checker.secretKey !== '') {
                    return true;
                }
            }
            else if (this.state.payload.category === "MONGODB") {
                if (checker.url && !checker.isAuthenticated && checker.database) {
                    return true;
                }
                else if (checker.url && checker.isAuthenticated && checker.database && checker.username !== '' && checker.password !== '') {
                    return true;
                }

            }
            return false;
        }
        else {
            return true;
        }
    }

    downLoadCertificate = () => {
        let url = ''
        if (window.API_URL.includes('internal-')) {
            url = "https://incs83.s3-us-west-2.amazonaws.com/cdn/certs/RootCA-InternalIoT83.pem"
        }
        else {
            url = "https://incs83.s3-us-west-2.amazonaws.com/cdn/certs/RootCA-IoT83.pem"
        }
        this.setState({
            url
        })
    }

    toggleClass() {
        this.setState(prevState => ({
            showPassword: !prevState.showPassword
        }));
    }

    showSniffingModal = (topic, selectedDeviceId) => {
        let allTopics = [],
            sniffingTopic;

        let reportTopic = {
            topicName: `${topic}/${selectedDeviceId}/report`,
            messages: []
        },
            controlTopic = {
                topicName: `${topic}/${selectedDeviceId}/control`,
                messages: []
            };
        allTopics.push(reportTopic);
        allTopics.push(controlTopic);
        sniffingTopic = reportTopic.topicName

        this.setState({
            allTopics,
        }, () => {
            $('#sniffingModal').modal('show')
            this.subscribeMqttTopic(sniffingTopic)
        });
    }

    createMqttConnection = () => {
        let brokerURL = window.location.origin === "http://localhost:3001" ? window.API_URL.split("//")[1].substring(0, window.API_URL.split("//")[1].length - 1) : window.location.host,
            count = 0,
            credentials = getVMQCredentials();
        const options = {
            host: brokerURL,
            protocol: 'wss',
            path: '/connectmqtt',
            username: credentials.userName,
            password: credentials.password,
        };
        client = MQTT.connect(options);
        client.on('connect', function () {
        });
        client.on('close', function (response) {
            if (response && count > 4) {
                client.end();
            }
        });
        client.on('reconnect', function () {
            count++;
        });
    }

    subscribeMqttTopic = (sniffingTopic) => {
        if (!(client) || (!client.connected)) {
            this.createMqttConnection();
        }
        let _this = this,
            allTopics = JSON.parse(JSON.stringify(this.state.allTopics)),
            previousSniffingTopic = this.state.previousSniffingTopic;
        if (previousSniffingTopic) {
            client.unsubscribe(previousSniffingTopic);
        }
        client.subscribe(sniffingTopic, function (err) {
            if (!err) {
                client.on('message', function (topic, message) {
                    allTopics.map(topic => {
                        if (topic.topicName === sniffingTopic) {
                            topic.messages.unshift({
                                name: sniffingTopic,
                                time: new Date().toLocaleString('en-US', { hour12: false, timeZone: localStorage.timeZone }),
                                message: JSON.stringify(JSON.parse(message.toString()), undefined, 2),
                            });
                            topic.messages.length == 60 ? topic.messages.splice(59) : ''
                        }
                    })
                    if (!_this.state.isHoldData) {
                        _this.setState({
                            allTopics,
                        })
                    }
                });
            }
        })
        this.setState({
            sniffingTopic,
            previousSniffingTopic: sniffingTopic
        })
    }

    copyToClipboard = (id) => {
        var copyText = document.getElementById(id);
        copyText.select();
        document.execCommand("copy");
        let element = $(`#Copied${id[id.length - 1]}`)[0]
        ReactTooltip.show(element)
        setTimeout(() => ReactTooltip.hide(element), 2500)
    }

    closeSniffingModal = () => {
        client.unsubscribe(this.state.previousSniffingTopic);
        $('#sniffingModal').modal('hide')
        this.setState({
            previousSniffingTopic: '',
            sniffingTopic: '',
            allTopics: []
        })
    }

    clearSniffingModal = () => {
        let allTopics = cloneDeep(this.state.allTopics)
        allTopics.map(el => el.messages = [])
        this.setState({
            allTopics
        }, () => this.subscribeMqttTopic(this.state.previousSniffingTopic))
    }

    saveUpdateValidator = () => {
        let connector = this.props.match.params.connector;
        switch (connector) {
            case "MQTT":
                if (this.state.payload.isExternal) {
                    return (!this.state.connectionTestedSuccessFully)
                }
                else {
                    if (this.props.match.params.id)
                        return (!this.state.isDescriptionChanged)
                    else
                        return true
                }
            case "HDFS":
            case "EMAIL":
                return false
            case "HTTP":
                let hasSinkType = Boolean(this.state.payload.properties.sinkType),
                    hasCategory = Boolean(this.state.payload.properties.category),
                    hasPath = Boolean(this.state.payload.properties.path),
                    hasTopicOrCollection = Boolean(this.state.payload.properties.sinkType === "nosql" ? this.state.payload.properties.collectionName : this.state.payload.properties.topic),
                    hasApiKey = this.state.payload.properties.apiKey;
                return !(hasSinkType && hasCategory && hasPath && hasTopicOrCollection && hasApiKey);
            case "EXCEL":
            case "CSV":
            case "JSON":
                return (!this.state.payload.properties.fileList.length)
            case "HIVE":
                let tableListValidation = this.state.payload.properties.tableList.every(el => el.partitionBy.length && el.name)
                return !(tableListValidation && this.state.payload.properties.database)
            default:
                return (!this.state.connectionTestedSuccessFully)
        }
    }

    displayExcelData = (filePath, index) => {
        let data = [],
            cols = [],
            url = "https://incs83.s3.us-west-2.amazonaws.com" + filePath;
        this.setState({
            sheetIndex: index,
            data,
            cols
        }, () => this.props.getDisplayCodeContent(url))
    }

    removeAPIKey = () => {
        let payload = JSON.parse(JSON.stringify(this.state.payload));
        payload.properties.apiKey = '';
        this.setState({
            payload
        })
    }

    schemaEditHandler = (schemaJSON) => {
        this.setState({
            userJson: schemaJSON,
        })
    }

    IsNotValidJsonString = () => {
        let previousjson = JSON.stringify(this.state.payload.properties.topics[this.state.selectedTopicIndex].userJson || null),
            currentJson = JSON.stringify(this.state.userJson || null);
        if ((currentJson.trim() === "null") || (previousjson == currentJson || currentJson === "{}")) {
            return true
        }
        try {
            JSON.parse(currentJson);
        } catch (e) {
            return true;
        }
        return false
    }

    closeJsonModal = () => {
        this.setState({ selectedTopicIndex: null, userJson: {}, showJsonModal: false })
    }

    getValue = (isFor) => {
        switch (isFor) {
            case "id":
                return this.state.payload.properties.sinkType === "nosql" ? "collectionName" : "topic"
            case "value":
                return this.state.payload.properties.sinkType === "nosql" ? this.state.payload.properties.collectionName : this.state.payload.properties.topic
            case "disabled":
                return Boolean(this.props.match.params.id || !this.state.payload.properties.category)
            default:
                return this.state.payload.properties.category ? this.state.payload.properties.sinkType === "nosql" ? "Mongo Collection:" : "Topic:" : ""
        }
    }

    hivePropsChangeHandler = ({ currentTarget }) => {
        let payload = cloneDeep(this.state.payload)
        if (currentTarget.id === "database") {
            payload.properties.database = currentTarget.value
        }
        else {
            let details = currentTarget.id.split("_"),
                key = details[0],
                index = details[1];
            payload.properties.tableList[index][key] = currentTarget.value
        }
        this.setState({ payload })
    }

    addOrDeleteTable = (operation, index) => {
        let payload = cloneDeep(this.state.payload)
        if (operation === "ADD")
            payload.properties.tableList.push({
                name: "",
                partitionBy: []
            })
        else
            payload.properties.tableList.splice(index, 1)
        this.setState({
            payload
        })
    }

    createInternalMQTTConnector = () => {
        let payload = cloneDeep(this.state.payload);
        payload.properties = {};
        this.setState({
            isFetching: true,
            isDescriptionChanged: false
        }, () => { this.props.createDevice(payload) })

    }

    deviceDetailsChangeHandler = ({ deviceId }) => ({ currentTarget }) => {
        let payload = cloneDeep(this.state.payload)
        let objIndex = payload.properties.deviceIds.findIndex((obj => obj.deviceId == deviceId));
        payload.properties.deviceIds[objIndex][currentTarget.id] = currentTarget.value;
        this.setState({
            payload
        })
    }

    toggleDeviceEditing = ({ deviceId }, action) => {
        let payload = cloneDeep(this.state.payload)
        let objIndex = payload.properties.deviceIds.findIndex((obj => obj.deviceId == deviceId));
        let apiPayload = {
            lat: null,
            lng: null,
            location: null,
            name: null
        };

        if (action == "cancel") {
            payload.properties.deviceIds[objIndex].lat = payload.properties.deviceIds[objIndex].originalLat;
            payload.properties.deviceIds[objIndex].lng = payload.properties.deviceIds[objIndex].originalLng;
            payload.properties.deviceIds[objIndex].location = payload.properties.deviceIds[objIndex].originalLocation;
            payload.properties.deviceIds[objIndex].name = payload.properties.deviceIds[objIndex].originalName;
            payload.properties.deviceIds[objIndex].isEdit = false;
        }

        else if (action == "edit") {
            payload.properties.deviceIds[objIndex].isEdit = true
        }

        else if (action == "save") {
            apiPayload.lat = payload.properties.deviceIds[objIndex].lat;
            apiPayload.lng = payload.properties.deviceIds[objIndex].lng;
            apiPayload.location = payload.properties.deviceIds[objIndex].location;
            apiPayload.name = payload.properties.deviceIds[objIndex].name;
            payload.properties.deviceIds[objIndex].isLoading = true
            payload.properties.deviceIds[objIndex].isEdit = false;
            this.props.editDevice(apiPayload, deviceId)
        }

        payload.properties.deviceIds[objIndex].originalLat = payload.properties.deviceIds[objIndex].lat;
        payload.properties.deviceIds[objIndex].originalLng = payload.properties.deviceIds[objIndex].lng;
        payload.properties.deviceIds[objIndex].originalLocation = payload.properties.deviceIds[objIndex].location;
        payload.properties.deviceIds[objIndex].originalName = payload.properties.deviceIds[objIndex].name;

        this.setState({
            payload
        })
    }

    setLocation = (lat, lng, location) => {
        let payload = cloneDeep(this.state.payload)
        let objIndex = payload.properties.deviceIds.findIndex((obj => obj.deviceId == this.state.selectedDeviceDetails.deviceId));
        payload.properties.deviceIds[objIndex].lat = lat;
        payload.properties.deviceIds[objIndex].lng = lng;
        payload.properties.deviceIds[objIndex].location = location;
        this.setState({
            payload
        })
    }

    openLocationModal = ({ location, deviceId, lat, lng }) => {
        this.setState({
            openLocationSelector: true,
            selectedDeviceDetails: {
                location,
                deviceId,
                lat,
                lng
            }
        })
    }

    deleteDeviceHandler = () => {
        let payload = cloneDeep(this.state.payload),
            deviceId = payload.properties.deviceIds[this.state.deviceToBeDeletedIndex].deviceId,
            connectorId = this.props.match.params.id
        this.setState({
            // isFetching: true,
            confirmState: false,
            isLoader: true,
            deviceToBeDeletedIndex: null,
            deviceToBeDeletedName: null,
        }, () => this.props.deleteDevice(deviceId, connectorId))
    }

    toggleConfirmModal = (deviceDetails) => {
        let { deviceId, name } = deviceDetails || {},
            payload = cloneDeep(this.state.payload),
            deviceToBeDeletedIndex = deviceId ? payload.properties.deviceIds.findIndex((obj => obj.deviceId == deviceId)) : null,
            confirmState = Boolean(deviceId),
            deviceToBeDeletedName = deviceId ? name || deviceId : null
        this.setState({
            deviceToBeDeletedIndex,
            confirmState,
            deviceToBeDeletedName
        })
    }

    defaultPageSize = () => {
        let pageSizeOptions = [5, 10, 20, 25, 50, 100];
        let defaultPageSizeIndex = pageSizeOptions.findIndex(e => e > this.state.payload.properties.deviceIds.length);
        return pageSizeOptions[defaultPageSizeIndex];
    }

    resetLocation = (id) => {
        let payload = cloneDeep(this.state.payload);
        let currentIndex = payload.properties.deviceIds.findIndex(el => el.deviceId == id);
        payload.properties.deviceIds[currentIndex].location = null
        payload.properties.deviceIds[currentIndex].lat = null
        payload.properties.deviceIds[currentIndex].lng = null
        this.setState({ payload })
        ReactTooltip.hide();
    }

    downLoadDeviceIdsExcel = () => {
        let payload = cloneDeep(this.state.payload);
        let ws = XLSX.utils.json_to_sheet(payload.properties.deviceIds);
        let wb = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, `${payload.name}_DeviceIds_Sheet`);
        XLSX.writeFile(wb, `${payload.name}_Deviceids.xlsx`);
    }

    showCommandModal = () => {
        this.setState((prevState, props) => ({
            activeCommandIndex: 0,
            controlConfig: prevState.controlConfig.length === 0 ? [cloneDeep(COMMAND_OBJ)] : prevState.controlConfig
        }))
    }

    addCommandHandler = () => {
        let controlConfig = cloneDeep(this.state.controlConfig)
        controlConfig.push(cloneDeep(COMMAND_OBJ))
        this.setState({
            controlConfig
        })
    }

    handleActiveCommand = (index) => {
        this.setState({
            activeCommandIndex: index
        })
    }

    commandChangeHandler = (commandIndex, value, keyName) => {
        let controlConfig = cloneDeep(this.state.controlConfig)
        controlConfig[commandIndex][keyName] = value
        this.setState({
            controlConfig
        })
    }

    deleteCommandHandler = (commandIndex) => {
        let controlConfig = cloneDeep(this.state.controlConfig)
        controlConfig.splice(commandIndex, 1)
        let activeCommandIndex = this.state.activeCommandIndex + 1 > controlConfig.length ? this.state.activeCommandIndex - 1 : this.state.activeCommandIndex
        this.setState({
            controlConfig,
            activeCommandIndex
        })
    }

    saveCommandsHandler = () => {
        let payload = {
            controlConfig: this.state.controlConfig,
            connectorId: this.props.match.params.id
        }
        this.setState({
            commandModalLoader: true
        }, () => this.props.saveCommands(payload))

    }

    closeCommandModal = () => {
        this.setState({
            activeCommandIndex: -1,
            controlConfig: [cloneDeep(COMMAND_OBJ)]
        })
    }

    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>Add/Edit Connectors</title>
                    <meta name="description" content="M83-AddOrEditConnectors" />
                </Helmet>

                {this.state.isFetching ? <Loader /> :
                    <React.Fragment>
                        <div className="pageBreadcrumb">
                            <div className="row">
                                <div className="col-8">
                                    {localStorage.tenantType === "SAAS" ?
                                        <p>

                                            <span className="previousPage" onClick={() => { this.props.history.push("/connectors"); }}>Connectors {this.state.connectorCount ? ' (' + this.state.connectorCount + ')' : ''}</span>
                                            <span className="active">
                                                {this.props.match.params.id ? this.state.payload.name : 'Add New'}
                                            </span>
                                        </p>
                                        :
                                        <p>

                                            <span className="previousPage" onClick={() => { this.props.history.push("/connectors"); }}>Connectors</span>
                                            <span className="previousPage" onClick={() => { this.props.history.push(`/connectors/${this.props.match.params.connector}`) }} > {this.props.match.params.connector}{this.state.connectorCount ? ' (' + this.state.connectorCount + ')' : ''}</span>
                                            <span className="active">
                                                {this.props.match.params.id ? this.state.payload.name : 'Add New'}
                                            </span>
                                        </p>
                                    }
                                </div>
                                <div className="col-4 text-right">
                                    <div className="flex h-100 justify-content-end align-items-center" />
                                </div>
                            </div>
                        </div>

                        <div className="outerBox pb-0">
                            <form className="contentForm" onSubmit={this.submitHandler}>
                                <div className="flex">
                                    <div className="flex-item fx-b35 contentFormDetail pd-r-10">
                                        <div className="contentFormHeading"><p>Configure:</p></div>

                                        <div className="contentFormBody">
                                            <div className="form-group">
                                                <input required type="text" name="DataSource-name" id="name" placeholder="Name :" className="form-control" autoFocus
                                                    value={(this.state.payload.name)} onChange={this.onChangeHandler} readOnly={this.props.match.params.id} pattern="^[a-zA-z0-9]{1,}$"
                                                />
                                                <label className="form-group-label">Name :
                                                    <span className="requiredMark"><i className="fa fa-asterisk" /></span>
                                                </label>
                                            </div>

                                            <div className="form-group">
                                                <textarea type="text" name="description" id="description" placeholder="description" value={this.state.payload.description} onChange={this.onChangeHandler} className="form-control" />
                                                <label className="form-group-label">Description :</label>
                                            </div>

                                            {this.props.match.params.connector === "MQTT" ?
                                                <div className="contentFormNote position-relative">
                                                    <div className="contentFormNoteImage">
                                                        <img src="https://content.iot83.com/m83/dataConnectors/mqtt.png" />
                                                    </div>
                                                    <ul className="listStyleNone">
                                                        <li><p>Give a Name to your device and Click Next</p></li>
                                                        <li><p>To be able to Connect your Device to M83 cloud, you need to a pair of credentials (available via <strong>Download</strong> option)</p></li>
                                                        <li><p>You also need to generate Device Ids as per allocated to you (based on your plan)</p></li>
                                                        <li>
                                                            <p>Once the above steps are done, you are also provided a pair of topics (Report and Control);</p>
                                                            <p><strong>report</strong> is used for publishing sensor data</p>
                                                            <p><strong>control</strong> for sending commands to the device</p>
                                                        </li>
                                                        <li><p>You can assign a name to your devices and specify their geographic location, also delete a device and recreate a new Id</p></li>
                                                        <li><p className="fw500 text-dark">For more detailed information, please refer the set-up manual that you would have received as a part of the registration email</p></li>
                                                    </ul>
                                                </div> :
                                                <React.Fragment>
                                                    <div className="form-group">
                                                        <label className="form-label">Connector Type:</label>
                                                        <ul className="versionControlList">
                                                            <li>
                                                                <div className="versionControlItem">
                                                                    <div className="versionControlItemImage">
                                                                        <i className="fad fa-cloud-upload text-orange"></i>
                                                                    </div>Source
                                                                    <label className={this.props.match.params.connector === "EMAIL" || this.props.match.params.id ? "radioLabel radioDisabled" : "radioLabel"}>
                                                                        <input
                                                                            type="radio" value="source" name="sourceSinkType"
                                                                            checked={this.state.payload.source && !this.state.payload.sink}
                                                                            onChange={this.onChangeHandler} disabled={this.props.match.params.connector === "EMAIL" || this.props.match.params.id}
                                                                        />
                                                                        <span className="radioMark" />
                                                                    </label>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div className="versionControlItem">
                                                                    <div className="versionControlItemImage">
                                                                        <i className="fad fa-cloud-download text-lightGreen"></i>
                                                                    </div>Sink
                                                                    <label className={this.state.payload.category === "EXCEL" || this.props.match.params.connector === "HTTP" || (this.props.match.params.connector === "MQTT" && localStorage.tenantType === 'SAAS') || this.props.match.params.id ? "radioLabel radioDisabled" : "radioLabel"}>
                                                                        <input
                                                                            type="radio" value="sink" name="sourceSinkType"
                                                                            disabled={this.props.match.params.connector === "EXCEL" || this.props.match.params.connector === "HTTP" || (this.props.match.params.connector === "MQTT" && localStorage.tenantType === 'SAAS') || this.props.match.params.id}
                                                                            checked={this.state.payload.sink && !this.state.payload.source} onChange={this.onChangeHandler}
                                                                        />
                                                                        <span className="radioMark" />
                                                                    </label>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div className="versionControlItem">
                                                                    <div className="versionControlItemImage">
                                                                        <i className="fab fa-mixcloud text-primary"></i>
                                                                    </div>Both
                                                                    <label className={this.state.payload.category === "EXCEL" || this.props.match.params.connector === "HTTP" || this.props.match.params.connector === "EMAIL" || (this.props.match.params.connector === "MQTT" && localStorage.tenantType === 'SAAS') || this.props.match.params.id ? "radioLabel radioDisabled" : "radioLabel"}>
                                                                        <input
                                                                            type="radio" value="source/sink" name="sourceSinkType"
                                                                            disabled={this.props.match.params.connector === "EXCEL" || this.props.match.params.connector === "HTTP" || this.props.match.params.connector === "EMAIL" || (this.props.match.params.connector === "MQTT" && localStorage.tenantType === 'SAAS') || this.props.match.params.id}
                                                                            checked={this.state.payload.source && this.state.payload.sink} onChange={this.onChangeHandler}
                                                                        />
                                                                        <span className="radioMark" />
                                                                    </label>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>

                                                    <div className="form-group">
                                                        <label className="form-label">Connection Type:</label>
                                                        <ul className="versionControlList">
                                                            <li>
                                                                <div className="versionControlItem">
                                                                    <div className="versionControlItemImage">
                                                                        <i className="fad fa-arrow-to-top text-primary"></i>
                                                                    </div>External
                                                                    <label className={(this.props.match.params.id || this.state.payload.category === "HTTP" || (this.props.match.params.connector === "MQTT" && localStorage.tenantType === 'SAAS')) ? "radioLabel radioDisabled" : "radioLabel"}>
                                                                        <input
                                                                            type="radio" value="external" name="connectionType"
                                                                            checked={this.state.payload.isExternal} onChange={this.onChangeHandler}
                                                                            disabled={this.props.match.params.id || this.state.payload.category === "HTTP" || (this.props.match.params.connector === "MQTT" && localStorage.tenantType === 'SAAS')}
                                                                        />
                                                                        <span className="radioMark" />
                                                                    </label>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div className="versionControlItem">
                                                                    <div className="versionControlItemImage">
                                                                        <i className="fad fa-arrow-to-bottom text-lightGreen"></i>
                                                                    </div>Internal
                                                                    <label className={(!(this.state.payload.category === "MQTT" || this.state.payload.category === "MONGODB" || this.state.payload.category === "KAFKA" || this.props.match.params.connector === "S3")) || this.props.match.params.id ? "radioLabel radioDisabled" : "radioLabel"}>
                                                                        <input
                                                                            type="radio" value="internal" name="connectionType"
                                                                            checked={!this.state.payload.isExternal} onChange={this.onChangeHandler}
                                                                            disabled={(!(this.state.payload.category === "MQTT" || this.state.payload.category === "MONGODB" || this.state.payload.category === "KAFKA" || this.props.match.params.connector === "S3")) || this.props.match.params.id}
                                                                        />
                                                                        <span className="radioMark" />
                                                                    </label>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </React.Fragment>
                                            }

                                            {this.props.match.params.connector === "EXCEL" || this.props.match.params.connector === "CSV" ?
                                                <div className="form-group">
                                                    <label className="form-label">File Upload :</label>
                                                    <div className="fileUploadBox">
                                                        {this.state.excelUploadLoader ?
                                                            <div className="fileUploadLoader">
                                                                <img
                                                                    src="https://content.iot83.com/m83/misc/uploading.gif" />
                                                                <h6>Uploading...</h6>
                                                            </div> :
                                                            this.state.payload.properties.fileList.length ?
                                                                <div className="fileUploadPreview">
                                                                    <div className="fileUploadOverlay">
                                                                        <input type="file" name="excelUpload"
                                                                            onChange={this.fileUploadHandler}
                                                                            accept={this.props.match.params.connector === "EXCEL" ? ".xlsx" : ".csv"} />
                                                                        <p>{this.state.payload.properties.fileList[0].fileName}</p>
                                                                        <h6><i className="far fa-pen" />Update</h6>
                                                                    </div>
                                                                </div> :
                                                                <div className="fileUploadForm">
                                                                    <input type="file" name="excelUpload"
                                                                        onChange={this.fileUploadHandler}
                                                                        accept={this.props.match.params.connector === "EXCEL" ? ".xlsx" : ".csv"} />
                                                                    <p>Click to upload file...!</p>
                                                                </div>
                                                        }
                                                    </div>
                                                </div>
                                                :
                                                " "
                                            }
                                        </div>
                                    </div>

                                    <div className="flex-item fx-b65 contentFormDetail pd-l-10">
                                        {this.props.match.params.connector === "EXCEL" || this.props.match.params.connector === "CSV" ? " " :
                                            <div className="contentFormHeading"><p>Definition:</p></div>
                                        }

                                        {this.props.match.params.connector === "EXCEL" || this.props.match.params.connector === "CSV" ?
                                            <div className="connectorExcelView h-100">
                                                {this.state.payload.properties.fileList.length ?
                                                    <div className="connectorExcelInnerView h-100">
                                                        <ul className="nav nav-tabs modalNavTabs">
                                                            {this.state.payload.properties.fileList.map((sheet, index) => {
                                                                return (
                                                                    <li className="nav-item fx-b20">
                                                                        <a className={this.state.sheetIndex === index ? "nav-link text-truncate active" : "nav-link text-truncate"} key={index} onClick={() => this.displayExcelData(sheet.filePath, index)}>
                                                                            <i className="far fa-file-excel mr-r-7"></i>{this.props.match.params.connector === "EXCEL" ? sheet.sheetName : sheet.fileName}
                                                                        </a>
                                                                    </li>
                                                                );
                                                            })
                                                            }
                                                        </ul>
                                                        <div className="card panel">
                                                            <div className="card-body panelBody">
                                                                {this.state.data.length ?
                                                                    <table className="table table-bordered m-0">
                                                                        <thead>
                                                                            <tr>{this.state.cols.map((c) => <th key={c.key}>{c.name}</th>)}</tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            {this.state.data.map((r, i) => <tr key={i}>{this.state.cols.map(c => <td key={c.key}>{r[c.key]}</td>)}</tr>)}
                                                                        </tbody>
                                                                    </table> :
                                                                    <div className="formLoaderBox">
                                                                        <i className="fad fa-sync-alt fa-spin"></i>
                                                                    </div>
                                                                }
                                                            </div>
                                                        </div>
                                                    </div> :
                                                    <div className="contentAddBox card panel">
                                                        <div className="contentAddBoxDetail">
                                                            <div className="contentAddBoxImage">
                                                                <img src="https://content.iot83.com/m83/icons/excel.png" />
                                                            </div>
                                                            <h6>Please upload a file to preview</h6>
                                                        </div>
                                                    </div>
                                                }
                                            </div> :

                                            <div className="contentFormBody">
                                                {this.props.match.params.connector === "KAFKA" &&
                                                    <div className="verticalStepBox">
                                                        <ul className="listStyleNone">
                                                            <li>
                                                                <h6><strong>Step 1:</strong>Add server and test your connection before proceeding further</h6>
                                                                <div className="card panel">
                                                                    <div className="card-body panelBody">
                                                                        <div className="flex mb-4">
                                                                            <div className="fx-b65 pd-r-15">
                                                                                <div className="form-group input-group">
                                                                                    <input type="text" className="form-control" id="bootstrapServer" name="bootstrapServer" placeholder="Bootstrap Server" readOnly={!this.state.payload.isExternal} value={this.state.payload.properties.bootstrapServer} onChange={this.definitionChangeHandler} required />
                                                                                    <label className="form-group-label" htmlFor="bootstrapServer">Bootstrap Server :
                                                                                    <span className="requiredMark"><i className="fa fa-asterisk" /></span>
                                                                                    </label>
                                                                                    <div className="input-group-append">
                                                                                        {this.state.isTestingConnection ?
                                                                                            <button type="button" className="btn btn-info cursor-default">
                                                                                                <i className="fas fa-cog fa-spin mr-r-7"></i>Testing...
                                                                                        </button> :
                                                                                            this.state.connectionTestedSuccessFully ?
                                                                                                <button type="button" className="btn btn-success cursor-default">
                                                                                                    <i className="fas fa-check mr-r-7"></i>Tested
                                                                                            </button> :
                                                                                                this.state.connectionTestFailed ?
                                                                                                    <button type="button" className="btn btn-danger" disabled={!this.validateData()} onClick={this.testConnection}>
                                                                                                        <i className="fas fa-undo-alt mr-r-7"></i>Retry
                                                                                                </button> :
                                                                                                    <button type="button" className="btn btn-primary" disabled={!this.validateData()} onClick={this.testConnection}>
                                                                                                        <i className="fas fa-cog mr-r-7"></i>Test
                                                                                                </button>
                                                                                        }
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div className="fx-b35 pd-l-15 border-left">
                                                                                <div className="form-control h-100">
                                                                                    <p className="d-inline-block mb-0 mt-1">Enable Authentication :</p>
                                                                                    <label className={this.state.payload.isExternal ? "toggleSwitch mr-l-15" : "toggleSwitch mr-l-15 toggleSwitchDisabled"}>
                                                                                        <input type="checkbox" id="isAuthenticated" disabled={!this.state.payload.isExternal} checked={this.state.payload.properties.isAuthenticated} onChange={this.definitionChangeHandler} />
                                                                                        <span className="toggleSwitchSlider"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        {this.state.payload.properties.isAuthenticated ?
                                                                            <div className="flex">
                                                                                <div className="form-group fx-b50 pd-r-10">
                                                                                    <input type="text" className="form-control" id="username" name="username" readOnly={!this.state.payload.isExternal} value={this.state.payload.properties.username} onChange={this.definitionChangeHandler} placeholder="User Name" required />
                                                                                    <label className="form-group-label" htmlFor="User Name">User Name :
                                                                                    <span className="requiredMark"><i className="fa fa-asterisk" /></span></label>
                                                                                </div>
                                                                                <div className="form-group fx-b50 pd-l-10">
                                                                                    <input type="password" className="form-control" id="password" name="password" readOnly={!this.state.payload.isExternal} value={this.state.payload.properties.password} onChange={this.definitionChangeHandler} placeholder="Password" required />
                                                                                    <label className="form-group-label" htmlFor="Password Name">Password :
                                                                                    <span className="requiredMark"><i className="fa fa-asterisk" /></span></label>
                                                                                </div>
                                                                            </div> : ''
                                                                        }
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <h6><strong>Step 2:</strong>Add topics</h6>
                                                                <div className="card panel">
                                                                    <div className="card-body panelBody">
                                                                        <div>
                                                                            <p className="noteText mb-1"><strong>Note:</strong></p>
                                                                            <p className="noteText mb-1"><strong>1.</strong>Please click generate schema to decipher the data attributes</p>
                                                                            <p className="noteText mb-1"><strong>2.</strong>Add <span>'#'</span> after the topic for wildcard subscription</p>
                                                                        </div>

                                                                        {this.state.payload.properties.topics.map((topic, index) => {
                                                                            return (
                                                                                <div className="topicListBox" key={index}>
                                                                                    <div className="input-group form-group">
                                                                                        <input type="text" id="topicName" placeholder="topicName/#" className="form-control" required value={topic.name} onChange={(e) => this.definitionChangeHandler(e, index)} />

                                                                                        <div className="input-group-append">
                                                                                            <div className="button-group">
                                                                                                <button type="button" data-tip data-for={`generate${index}`} className="btn btn-transparent btn-transparent-success" disabled={(topic.name === '')} onClick={() => this.generateSchema(index)}>
                                                                                                    {topic.isGeneratingSchema ? <i className="fas fa-cog fa-spin"></i> : <i className="fas fa-cog"></i>}
                                                                                                </button>
                                                                                                <ReactTooltip id={`generate${index}`} place="bottom" type="dark">
                                                                                                    <div className="tooltipText"><p>Generate Schema</p></div>
                                                                                                </ReactTooltip>
                                                                                                <button type="button" className="btn btn btn-transparent btn-transparent-info" data-tip data-for={`view${index}`} disabled={(!topic.schema)} onClick={() => this.openSchemaModal(topic)} >
                                                                                                    <i className="fas fa-eye"></i>
                                                                                                </button>
                                                                                                <ReactTooltip id={`view${index}`} place="bottom" type="dark">
                                                                                                    <div className="tooltipText"><p>View Schema</p></div>
                                                                                                </ReactTooltip>
                                                                                                <button type="button" data-tip data-for={`delete${index}`} className="btn btn-transparent btn-transparent-danger" disabled={this.state.payload.properties.topics.length === 1} onClick={() => this.addDeleteTopics("delete", index)}>
                                                                                                    <i className="far fa-trash-alt"></i>
                                                                                                </button>
                                                                                                <ReactTooltip id={`delete${index}`} place="bottom" type="dark">
                                                                                                    <div className="tooltipText"><p>Delete</p></div>
                                                                                                </ReactTooltip>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            )
                                                                        })}

                                                                        <div className="text-right">
                                                                            <button type="button" className="btn-link" onClick={() => this.addDeleteTopics("add", null)}>
                                                                                <i className="far fa-plus"></i>Add New Topic
                                                                        </button>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                }

                                                {this.props.match.params.connector === "MQTT" &&
                                                    <React.Fragment>
                                                        {this.state.payload.isExternal ?
                                                            <div className="verticalStepBox">
                                                                <ul className="listStyleNone">
                                                                    <li>
                                                                        <h6><strong>Step 1:</strong>Select your scheme type</h6>
                                                                        <div className="card panel">
                                                                            <div className="card-body panelBody">
                                                                                <div className="topicListBox m-0">
                                                                                    <div className="form-group input-group">
                                                                                        <div className="input-group-prepend">
                                                                                            <span className="input-group-text">{this.state.scheme} </span>
                                                                                        </div>
                                                                                        <select className="form-control" id="scheme" value={this.state.scheme} onChange={this.definitionChangeHandler}>
                                                                                            <option value="ssl://">TLS</option>
                                                                                            <option value="tcp://">TCP</option>
                                                                                        </select>
                                                                                        <label className="form-group-label mr-l-80">Scheme :<span className="requiredMark"><i className="fa fa-asterisk" /></span></label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <h6><strong>Step 2:</strong>Add broker URL and test your connection before proceeding further</h6>
                                                                        <div className="card panel">
                                                                            <div className="card-body panelBody">
                                                                                <div className="flex">
                                                                                    <div className="fx-b65 pd-r-15">
                                                                                        <div className="form-group input-group">
                                                                                            <input type="text" className="form-control" id="bootstrapServer" name="bootstrapServer" placeholder={`example.brokerurl.com:${this.state.scheme === "ssl://" ? "8883" : "1883"}`} value={this.state.payload.properties.bootstrapServer} onChange={this.definitionChangeHandler} />
                                                                                            <div className="input-group-append">
                                                                                                {this.state.isTestingConnection ?
                                                                                                    <button type="button" className="btn btn-info cursor-default">
                                                                                                        <i className="fas fa-cog fa-spin mr-r-7"></i>Testing...
                                                                                                </button> :
                                                                                                    this.state.connectionTestedSuccessFully ?
                                                                                                        <button type="button" className="btn btn-success cursor-default">
                                                                                                            <i className="fas fa-check mr-r-7"></i>Tested
                                                                                                    </button> :
                                                                                                        this.state.connectionTestFailed ?
                                                                                                            <button type="button" className="btn btn-danger" disabled={!this.validateData()} onClick={this.testConnection}>
                                                                                                                <i className="fas fa-undo-alt mr-r-7"></i>Retry
                                                                                                        </button> :
                                                                                                            <button type="button" className="btn btn-primary" disabled={!this.validateData()} onClick={this.testConnection}>
                                                                                                                <i className="fas fa-cog mr-r-7"></i>Test
                                                                                                        </button>
                                                                                                }
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div className="fx-b35 pd-l-15 border-left">
                                                                                        <div className="form-control h-100">
                                                                                            <p className="d-inline-block mb-0 mt-1">Enable Authentication :</p>
                                                                                            <label className="toggleSwitch mr-l-15">
                                                                                                <input type="checkbox" id="isAuthenticated" checked={this.state.payload.properties.isAuthenticated} onChange={this.definitionChangeHandler} />
                                                                                                <span className="toggleSwitchSlider"></span>
                                                                                            </label>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                                {this.state.payload.properties.isAuthenticated &&
                                                                                    <div className="flex mr-t-20">
                                                                                        <div className="form-group fx-b50 pd-r-10">
                                                                                            <input type="text" className="form-control" id="username" name="username" value={this.state.payload.properties.username} onChange={this.definitionChangeHandler} placeholder="User Name" required />
                                                                                            <label className="form-group-label">User Name :<span className="requiredMark"><i className="fa fa-asterisk" /></span></label>
                                                                                        </div>
                                                                                        <div className="form-group input-group fx-b50 pd-l-10">
                                                                                            <input type={this.state.showPassword ? "text" : "password"} className="form-control" id="password" name="password" value={this.state.payload.properties.password} onChange={this.definitionChangeHandler} placeholder="Password" />
                                                                                            <label className="form-group-label">Password :<span className="requiredMark"><i className="fa fa-asterisk" /></span></label>
                                                                                            <div className="input-group-append">
                                                                                                <button type="button" className="btn btn-transparent-primary" onMouseDown={() => this.toggleClass()} onMouseUp={() => this.toggleClass()}>
                                                                                                    <i className="fas fa-eye"></i>
                                                                                                </button>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                }

                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                    {this.state.scheme === "ssl://" ?
                                                                        <li>
                                                                            <h6><strong>Step 3:</strong>Add Certificate</h6>
                                                                            <div className="card panel">
                                                                                <div className="card-body panelBody">
                                                                                    <div className="flex form-group">
                                                                                        <div className="fx-b33 pd-r-10">
                                                                                            <label className="form-label">Certificate :</label>
                                                                                            <div className="fileUploadBox connectorFileBox">
                                                                                                {this.state.isUploadingFile ?
                                                                                                    <div className="fileUploadLoader">
                                                                                                        <img src="https://content.iot83.com/m83/misc/uploading.gif" />
                                                                                                        <h6>Uploading...</h6>
                                                                                                    </div> :
                                                                                                    this.state.payload.properties.certificate ?
                                                                                                        <div className="fileUploadPreview">
                                                                                                            <div className="fileUploadOverlay">
                                                                                                                <input type="file" name='certificate' onChange={this.fileUploadHandler} />
                                                                                                                <p>{this.state.payload.properties.certificate}</p>
                                                                                                                <h6><i className="far fa-pen" /></h6>
                                                                                                            </div>
                                                                                                        </div> :
                                                                                                        <div className="fileUploadForm">
                                                                                                            <input type="file" name='certificate' onChange={this.fileUploadHandler} />
                                                                                                            <p>{"Click to upload certificate...!"}</p>
                                                                                                        </div>
                                                                                                }
                                                                                            </div>
                                                                                        </div>

                                                                                        <div className="fx-b33 pd-l-5 pd-r-5">
                                                                                            <label className="form-label">Private Key :</label>
                                                                                            <div className="fileUploadBox connectorFileBox">
                                                                                                {this.state.isUploadingFile ?
                                                                                                    <div className="fileUploadLoader">
                                                                                                        <img src="https://content.iot83.com/m83/misc/uploading.gif" />
                                                                                                        <h6>Uploading...</h6>
                                                                                                    </div> :
                                                                                                    this.state.payload.properties.privateKey ?
                                                                                                        <div className="fileUploadPreview">
                                                                                                            <div className="fileUploadOverlay">
                                                                                                                <input type="file" name='privateKey' onChange={this.fileUploadHandler} />
                                                                                                                <p>{this.state.payload.properties.privateKey}</p>
                                                                                                                <h6><i className="far fa-pen" /></h6>
                                                                                                            </div>
                                                                                                        </div> :
                                                                                                        <div className="fileUploadForm">
                                                                                                            <input type="file" name='privateKey' onChange={this.fileUploadHandler} />
                                                                                                            <p>{"Click to upload certificate...!"}</p>
                                                                                                        </div>
                                                                                                }
                                                                                            </div>
                                                                                        </div>

                                                                                        <div className="fx-b33 pd-l-10">
                                                                                            <label className="form-label">CA Certificate :</label>
                                                                                            <div className="fileUploadBox connectorFileBox">
                                                                                                {this.state.isUploadingFile ?
                                                                                                    <div className="fileUploadLoader">
                                                                                                        <img src="https://content.iot83.com/m83/misc/uploading.gif" />
                                                                                                        <h6>Uploading...</h6>
                                                                                                    </div> :
                                                                                                    this.state.payload.properties.caCertificate ?
                                                                                                        <div className="fileUploadPreview">
                                                                                                            <div className="fileUploadOverlay">
                                                                                                                <input type="file" name='caCertificate' onChange={this.fileUploadHandler} />
                                                                                                                <p>{this.state.payload.properties.caCertificate}</p>
                                                                                                                <h6><i className="far fa-pen" /></h6>
                                                                                                            </div>
                                                                                                        </div> :
                                                                                                        <div className="fileUploadForm">
                                                                                                            <input type="file" name='caCertificate' onChange={this.fileUploadHandler} />
                                                                                                            <p>{"Click to upload certificate...!"}</p>
                                                                                                        </div>}
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </li> : ''
                                                                    }
                                                                    <li>
                                                                        <h6><strong>{this.state.scheme === "ssl://" ? 'Step 4 :' : 'Step 3 :'}</strong>Add Topics</h6>
                                                                        <div className="card panel">
                                                                            <div className="card-body panelBody">
                                                                                <div>
                                                                                    <p className="noteText mb-1"><strong>Note:</strong></p>
                                                                                    <p className="noteText mb-1"><strong>1.</strong>Please click generate schema to decipher the data attributes</p>
                                                                                    <p className="noteText mb-1"><strong>2.</strong>Add <span>'#'</span> after the topic for wildcard subscription</p>
                                                                                </div>

                                                                                {this.state.payload.properties.topics.map((topic, index) => (
                                                                                    <div className="topicListBox" key={index}>
                                                                                        <div className="input-group">
                                                                                            <input type="text" id="topicName" placeholder="topicName/#" className="form-control" required value={topic.name} onChange={(e) => this.definitionChangeHandler(e, index)} />
                                                                                            <div className="input-group-append">
                                                                                                <div className="button-group">
                                                                                                    <button type="button" data-tip data-for={`generate${index}`} className="btn btn-transparent btn-transparent-success" disabled={topic.name === ''} onClick={() => this.generateSchema(index)}>
                                                                                                        {topic.isGeneratingSchema ? <i className="fas fa-cog fa-spin"></i> : <i className="fas fa-cog"></i>}
                                                                                                    </button>
                                                                                                    <ReactTooltip id={`generate${index}`} place="bottom" type="dark">
                                                                                                        <div className="tooltipText"><p>Generate Schema</p></div>
                                                                                                    </ReactTooltip>
                                                                                                    {this.props.match.params.connector === "MQTT" &&
                                                                                                        <React.Fragment>
                                                                                                            <button type="button" className="btn btn-transparent btn-transparent-primary" data-tip data-for={`add${index}`} disabled={topic.name === '' || topic.isBackendGenerated || topic.isGeneratingSchema} onClick={() => this.openSchemaModal(topic, index)}>
                                                                                                                <i className={`fas fa-${topic.schema ? "pencil" : "plus"}`}></i>
                                                                                                            </button>
                                                                                                            <ReactTooltip id={`add${index}`} place="bottom" type="dark">
                                                                                                                <div className="tooltipText"><p>{topic.schema ? "Edit" : "Add"} Data JSON for Schema</p></div>
                                                                                                            </ReactTooltip>
                                                                                                        </React.Fragment>
                                                                                                    }
                                                                                                    <button type="button" className="btn btn-transparent btn-transparent-info" data-tip data-for={`view${index}`} disabled={topic.name === '' || topic.isGeneratingSchema || !topic.schema} onClick={() => this.openSchemaModal(topic)}>
                                                                                                        <i className="fas fa-eye"></i>
                                                                                                    </button>
                                                                                                    <ReactTooltip id={`view${index}`} place="bottom" type="dark">
                                                                                                        <div className="tooltipText"><p>View Schema</p></div>
                                                                                                    </ReactTooltip>
                                                                                                    <button type="button" className="btn btn-transparent btn-transparent-warning" data-tip data-for={`sniff${index}`} disabled={topic.name === '' || this.state.payload.isExternal || topic.isGeneratingSchema} onClick={() => this.subscribeMqttTopic(topic.name)}>
                                                                                                        <i className="fas fa-brackets-curly"></i>
                                                                                                    </button>
                                                                                                    <ReactTooltip id={`sniff${index}`} place="bottom" type="dark">
                                                                                                        <div className="tooltipText"><p>Sniff</p></div>
                                                                                                    </ReactTooltip>
                                                                                                    <button type="button" className="btn btn-transparent btn-transparent-danger" data-tip data-for={`delete${index}`} disabled={this.state.payload.properties.topics.length === 1 || topic.isGeneratingSchema} onClick={() => this.addDeleteTopics("delete", index)}>
                                                                                                        <i className="far fa-trash-alt"></i>
                                                                                                    </button>
                                                                                                    <ReactTooltip id={`delete${index}`} place="bottom" type="dark">
                                                                                                        <div className="tooltipText"><p>Delete</p></div>
                                                                                                    </ReactTooltip>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>)
                                                                                )}

                                                                                <div className="text-right">
                                                                                    <button type="button" className="btn-link" onClick={() => this.addDeleteTopics("add")}><i className="far fa-plus mr-r-5"></i>Add New Topic</button>
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                            :
                                                            <React.Fragment>
                                                                <div className="verticalStepBox">
                                                                    <ul className="listStyleNone">
                                                                        <li>
                                                                            <h6><strong>Step 1:</strong>Generate Device Ids & Request Credentials by Email (allowed 3 times)</h6>
                                                                            <div className="card panel">
                                                                                <div className="card-body panelBody">
                                                                                    <div className="flex align-items-center">
                                                                                        <div className="fx-b75 pd-r-20">
                                                                                            <div className="form-group input-group">
                                                                                                {localStorage.tenantType === "SAAS" ? <select className="form-control" value={this.state.noOfClientIds} onChange={(e) => { this.setState({ noOfClientIds: e.currentTarget.value }) }}>
                                                                                                    {this.state.availableClientIds > 0 ? [...Array(this.state.availableClientIds)].map((x, i) =>
                                                                                                        <option key={i + 1} value={i + 1}>{i + 1}</option>
                                                                                                    ) : <option value="0">0</option>}
                                                                                                </select> :
                                                                                                    <input className="form-control" type="number" value={this.state.noOfClientIds} min="0" onChange={({ currentTarget }) => { currentTarget.value && this.setState({ noOfClientIds: parseInt(currentTarget.value) }) }} />
                                                                                                }
                                                                                                <label className="form-group-label">No. of Device IDs</label>
                                                                                                <div className="input-group-append">
                                                                                                    <button type="button" className="btn btn-primary" disabled={(typeof this.props.match.params.id === 'undefined' || this.state.generatingClientIds || this.state.noOfClientIds === 0) || (this.state.availableClientIds === 0 && localStorage.tenantType === "SAAS")} onClick={() => { this.setState({ generatingClientIds: true }, () => { this.props.generateClientIdsForDevice(this.state.noOfClientIds, this.props.match.params.id) }) }}>Generate</button>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div className="fx-b25 border-left pd-l-20">
                                                                                            {this.state.emailCredentialsInitiated ?
                                                                                                <button type="button" className="btn btn-info cursor-default w-100">
                                                                                                    <i className="fas fa-cog fa-spin mr-r-7"></i>Sending Email...
                                                                                                </button> :
                                                                                                <button type="button" className="btn btn-primary w-100" disabled={this.state.payload.properties.deviceIds && this.state.payload.properties.deviceIds.length === 0 || this.state.payload.mailRequest && this.state.payload.mailRequest >= 3} onClick={() => { this.setState({ emailCredentialsInitiated: true }, () => { this.props.emailCredentials(this.props.match.params.id) }) }}>
                                                                                                    <i className="fas fa-envelope mr-r-7"></i>Email Credentials
                                                                                                </button>
                                                                                            }
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                        <li>
                                                                            <h6><strong>Step 2:</strong>Topic Information</h6>
                                                                            <div className="card panel">
                                                                                <div className="card-body panelBody">
                                                                                    <table className="table table-bordered">
                                                                                        <thead>
                                                                                            <tr>
                                                                                                <th width="20%">Topic</th>
                                                                                                <th width="80%">Topic Id</th>
                                                                                            </tr>
                                                                                        </thead>
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td className="text-orange">Report (Publish)</td>
                                                                                                <td>{this.state.payload.properties.topics && this.state.payload.properties.topics[0].name ? `${this.state.payload.properties.topics[0].name}/<deviceId>/report` : '<topicId>/<deviceId>/report'}</td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td className="text-lightGreen">Control (Subscribe)</td>
                                                                                                <td>{this.state.payload.properties.topics && this.state.payload.properties.topics[0].name ? `${this.state.payload.properties.topics[0].name}/<deviceId>/control` : '<topicId>/<deviceId>/control'}
                                                                                                    <button type="button" className="btn btn-link float-right p-0" data-toggle="modal" onClick={this.showCommandModal}><i className="far fa-plus"></i>Add Device Command<span className="text-lowercase fw500">(s)</span></button>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                        <li>
                                                                            <h6><strong>Step 3:</strong>Device List
                                                                                {this.state.payload.properties.deviceIds && this.state.payload.properties.deviceIds.length > 0 &&
                                                                                    <button type='button' className="btn btn-link float-right" data-tip data-for="exportDevice" onClick={this.downLoadDeviceIdsExcel}><i className="far fa-file-export"></i>Export</button>
                                                                                }
                                                                                <ReactTooltip id="exportDevice" place="bottom" type="dark">
                                                                                    <div className="tooltipText"><p>Export Devices</p></div>
                                                                                </ReactTooltip>
                                                                            </h6>
                                                                            {this.state.payload.properties.deviceIds && this.state.payload.properties.deviceIds.length ?
                                                                                <ReactTable
                                                                                    columns={[
                                                                                        {
                                                                                            Header: 'Device Id',
                                                                                            accessor: "deviceId",
                                                                                            filterable: true,
                                                                                            filterMethod: (filter, row) => {
                                                                                                return (row._original.deviceId.toUpperCase().includes(filter.value.toUpperCase()))
                                                                                            },
                                                                                        }, {
                                                                                            Header: 'Device Name',
                                                                                            filterable: true,
                                                                                            Cell: row => (row.original.isEdit ?
                                                                                                <input type="text" className="form-control" id="name" value={row.original.name || ""} onChange={this.deviceDetailsChangeHandler(row.original)}></input>
                                                                                                : row.original.name || ""),
                                                                                            filterMethod: (filter, row) => {
                                                                                                return (row._original.name && row._original.name.toUpperCase().includes(filter.value.toUpperCase()))
                                                                                            },
                                                                                        }, {
                                                                                            Header: 'Location',
                                                                                            accessor: "location",
                                                                                            filterable: true,
                                                                                            Cell: row => (row.original.isEdit ?
                                                                                                <div className="button-group">
                                                                                                    <button type="button" className="btn btn-transparent btn-transparent-primary" onClick={() => this.openLocationModal(row.original)} data-tip data-for={"location" + row.original.deviceId}>
                                                                                                        <i className="far fa-map-marker-alt"></i>
                                                                                                    </button>
                                                                                                    <ReactTooltip id={"location" + row.original.deviceId} place="bottom" type="dark">
                                                                                                        <div className="tooltipText"><p>Add Location</p></div>
                                                                                                    </ReactTooltip>
                                                                                                    <button type="button" className="btn btn-transparent btn-transparent-warning" onClick={() => this.resetLocation(row.original.deviceId)} data-tip data-for={"reset" + row.original.deviceId} disabled={!row.original.location}>
                                                                                                        <i className="fas fa-redo-alt"></i>
                                                                                                    </button>
                                                                                                    <ReactTooltip id={"reset" + row.original.deviceId} place="bottom" type="dark">
                                                                                                        <div className="tooltipText"><p id={`Reset${row.original.deviceId}`}>Reset Location</p></div>
                                                                                                    </ReactTooltip>
                                                                                                </div>
                                                                                                : row.original.location || ""
                                                                                            ),
                                                                                            filterMethod: (filter, row) => {
                                                                                                return (row._original.location && row._original.location.toUpperCase().includes(filter.value.toUpperCase()))
                                                                                            },
                                                                                        }, {
                                                                                            Header: 'Status',
                                                                                            filterable: false,
                                                                                            Cell: row => {
                                                                                                return row.original.isConnected === "online" ? <span className="text-lightGreen"><i className="fad fa-webcam mr-r-5"></i>Online</span> : row.original.isConnected === "offline" ? <span className="text-red"><i className="fad fa-webcam-slash mr-r-5"></i>Offline</span> : <span className="text-gray"><i className="fad fa-webcam-slash mr-r-5"></i>Pending</span>
                                                                                            }
                                                                                        }, {
                                                                                            Header: 'Actions',
                                                                                            accessor: "name",
                                                                                            filterable: false,
                                                                                            Cell: row => (
                                                                                                <div className="button-group">
                                                                                                    {row.original.isLoading ? "Loading..." : row.original.isEdit ?
                                                                                                        <React.Fragment>
                                                                                                            <button type="button" className="btn btn-transparent btn-transparent-success" data-tip data-for={"saveDevice" + row.original.deviceId} onClick={() => this.toggleDeviceEditing(row.original, "save")}>
                                                                                                                <i className="fas fa-check"></i>
                                                                                                            </button>
                                                                                                            <ReactTooltip id={"saveDevice" + row.original.deviceId} place="bottom" type="dark">
                                                                                                                <div className="tooltipText"><p>Save</p></div>
                                                                                                            </ReactTooltip>
                                                                                                            <button type="button" className="btn btn-transparent btn-transparent-dark" data-tip data-for={"cancelDevice" + row.original.deviceId} onClick={() => this.toggleDeviceEditing(row.original, "cancel")}>
                                                                                                                <i className="fas fa-times"></i>
                                                                                                            </button>
                                                                                                            <ReactTooltip id={"cancelDevice" + row.original.deviceId} place="bottom" type="dark" >
                                                                                                                <div className="tooltipText"><p>Cancel</p></div>
                                                                                                            </ReactTooltip>
                                                                                                        </React.Fragment> :
                                                                                                        <React.Fragment>
                                                                                                            <button type="button" className="btn btn-transparent btn-transparent-warning" disabled={row.original.isConnected !== "online"} data-tip data-for={"sniffDevice" + row.original.deviceId} onClick={() => { row.original.isConnected && this.showSniffingModal(this.state.payload.properties.topics[0].name, row.original.deviceId) }}>
                                                                                                                <i className="fas fa-brackets-curly"></i>
                                                                                                            </button>
                                                                                                            <ReactTooltip id={"sniffDevice" + row.original.deviceId} place="bottom" type="dark" >
                                                                                                                <div className="tooltipText"><p>Sniff</p></div>
                                                                                                            </ReactTooltip>
                                                                                                            <button type="button" className="btn btn-transparent btn-transparent-primary" data-tip data-for={"editDevice" + row.original.deviceId} onClick={() => this.toggleDeviceEditing(row.original, "edit")}>
                                                                                                                <i className="fas fa-pencil"></i>
                                                                                                            </button>
                                                                                                            <ReactTooltip id={"editDevice" + row.original.deviceId} place="bottom" type="dark">
                                                                                                                <div className="tooltipText"><p>Edit</p></div>
                                                                                                            </ReactTooltip>
                                                                                                            <button type="button" className="btn btn-transparent btn-transparent-danger" data-tip data-for={"deleteDevice" + row.original.deviceId} onClick={() => this.toggleConfirmModal(row.original)}>
                                                                                                                <i className="fas fa-trash-alt"></i>
                                                                                                            </button>
                                                                                                            <ReactTooltip id={"deleteDevice" + row.original.deviceId} place="bottom" type="dark" >
                                                                                                                <div className="tooltipText"><p>Delete</p></div>
                                                                                                            </ReactTooltip>
                                                                                                        </React.Fragment>
                                                                                                    }
                                                                                                </div>
                                                                                            ),
                                                                                            filterMethod: (filter, row) => {
                                                                                                return (row.original.name.toUpperCase().includes(filter.value.toUpperCase()))
                                                                                            },
                                                                                        },]}
                                                                                    data={this.state.payload.properties.deviceIds}
                                                                                    noDataText="There is no data to display."
                                                                                    showPagination={true}
                                                                                    defaultPageSize={this.state.payload.properties.deviceIds.length > 0 ? this.defaultPageSize() : 5}
                                                                                    PreviousComponent={(props) => <button type="button"{...props}><i className="fal fa-angle-left"></i></button>}
                                                                                    NextComponent={(props) => <button type="button" {...props}><i className="fal fa-angle-right"></i></button>}
                                                                                    className="customReactTable"
                                                                                    loading={this.state.isLoader}
                                                                                />
                                                                                :
                                                                                <div className="card panel">
                                                                                    <div className="card-body panelBody">
                                                                                        <div className="verticalStepEmpty">
                                                                                            <h5>No device Id(s) generated !</h5>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            }
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                {!this.props.match.params.id &&
                                                                    <div className="verticalStepOverlay">
                                                                        <div className="verticalStepOverlayText text-center">
                                                                            <h4>Please proceed next to create your device</h4>
                                                                            <button type="button" className="btn btn-primary" disabled={this.state.payload.name.trim() === ''} onClick={() => { this.createInternalMQTTConnector() }}><i className="far fa-angle-double-right mr-r-5"></i>Next</button>
                                                                        </div>
                                                                    </div>
                                                                }
                                                                {this.state.generatingClientIds &&
                                                                    <div className="verticalStepOverlay">
                                                                        <div className="formLoaderBox">
                                                                            <i className="fad fa-sync-alt fa-spin"></i>
                                                                        </div>
                                                                    </div>
                                                                }
                                                            </React.Fragment>
                                                        }
                                                    </React.Fragment>
                                                }

                                                {this.props.match.params.connector === "HTTP" &&
                                                    <div className="verticalStepBox">
                                                        <ul className="listStyleNone">
                                                            <li>
                                                                <h6><strong>Step 1:</strong>Add path</h6>
                                                                <div className="card panel">
                                                                    <div className="card-body panelBody">
                                                                        <div className="flex">
                                                                            <div className="fx-b50 pd-r-10">
                                                                                <div className="form-group">
                                                                                    <input type="text" className="form-control" id="basePath" name="basePath" value={this.state.payload.properties.basePath} placeholder="API Base Path" readOnly />
                                                                                    <label className="form-group-label" htmlFor="basePath">Base Path:
                                                                                    <span className="requiredMark"><i className="fa fa-asterisk" /></span>
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                            <div className="fx-b50 pd-l-10">
                                                                                <div className="form-group">
                                                                                    <input type="text" className="form-control" id="path" name="path" placeholder="API Path" value={this.state.payload.properties.path} onChange={this.definitionChangeHandler} />
                                                                                    <label className="form-group-label">Path:
                                                                                    <span className="requiredMark"><i className="fa fa-asterisk" /></span>
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <h6><strong>Step 2:</strong>Select sink point</h6>
                                                                <div className="card panel">
                                                                    <div className="card-body panelBody">
                                                                        <div className="flex">
                                                                            <div className="fx-b33 pd-r-10">
                                                                                <div className="form-group">
                                                                                    <select className="form-control" id="sinkType" required onChange={this.definitionChangeHandler} value={this.state.payload.properties.sinkType}>
                                                                                        <option value="">Select</option>
                                                                                        {this.state.dataConnectors.map((connector, index) =>
                                                                                            <option key={index} value={connector.type} disabled={connector.type !== 'nosql' && connector.type !== "stream"}>{connector.displayName}</option>

                                                                                        )}
                                                                                    </select>
                                                                                    <label className="form-group-label">Sink Type :</label>
                                                                                </div>
                                                                            </div>
                                                                            <div className="fx-b33 pd-r-5 pd-l-5">
                                                                                <div className="form-group">
                                                                                    <select className="form-control" disabled={!this.state.payload.properties.sinkType} id="category" onChange={this.definitionChangeHandler} value={this.state.payload.properties.category} required >
                                                                                        <option value="">Select</option>
                                                                                        {this.state.payload.properties.sinkType && this.state.dataConnectors.filter(connector => connector.type === this.state.payload.properties.sinkType)[0].connectors.map((connector, index) =>
                                                                                            <option key={index} value={connector.connector} disabled={(this.state.payload.properties.sinkType === "nosql" && connector.connector !== 'MONGODB') || (this.state.payload.properties.sinkType === "stream" && connector.connector === 'HTTP')}>{connector.displayName}</option>

                                                                                        )}
                                                                                    </select>
                                                                                    <label className="form-group-label">Category :</label>
                                                                                </div>
                                                                            </div>
                                                                            <div className="fx-b33 pd-l-10">
                                                                                <div className="form-group">
                                                                                    <input type="text" className="form-control" disabled={this.getValue("disabled")}
                                                                                        id={this.getValue("id")}
                                                                                        value={this.getValue("value")}
                                                                                        onKeyDown={() => event.keyCode === 32 && event.preventDefault()}
                                                                                        onChange={this.definitionChangeHandler} />
                                                                                    <label className="form-group-label">{this.getValue("label")}
                                                                                        {this.state.payload.properties.category && <span className="requiredMark"><i className="fa fa-asterisk" /></span>}
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <h6><strong>Step 3:</strong>Add api key</h6>
                                                                <div className="card panel">
                                                                    <div className="card-body panelBody">
                                                                        <div>
                                                                            <p className="noteText mb-1"><strong>Note :</strong></p>
                                                                            <p className="noteText mb-1"><strong>1.</strong>This is a HTTP POST. Please pass this API Key as an HTTP Header X-API-KEY</p>
                                                                        </div>
                                                                        <div className="topicListBox">
                                                                            <div className="form-group input-group">
                                                                                <input className="form-control" id="apiKey" placeholder="API Key" name="apiKey" readOnly value={this.state.payload.properties.apiKey} />
                                                                                <div className="input-group-append">
                                                                                    <div className="button-group">
                                                                                        {this.state.payload.properties.apiKey === '' ?
                                                                                            <React.Fragment>
                                                                                                <button type="button" className="btn btn-transparent btn-transparent-success" data-tip data-for="generateApi"
                                                                                                    onClick={() => {
                                                                                                        this.setState({ isFetchingAPIKey: true }, () => {
                                                                                                            this.props.getAPIKey()
                                                                                                        })
                                                                                                    }}>
                                                                                                    {this.state.isFetchingAPIKey ? <i className="fas fa-cog fa-spin"></i> : <i className="fas fa-key"></i>}
                                                                                                </button>
                                                                                                <ReactTooltip id="generateApi" place="bottom" type="dark">
                                                                                                    <div className="tooltipText"><p>Generate API key</p></div>
                                                                                                </ReactTooltip>
                                                                                            </React.Fragment> :
                                                                                            <React.Fragment>
                                                                                                <button type="button" className="btn btn-transparent btn-transparent-primary" data-tip data-for="refreshApi"
                                                                                                    onClick={() => {
                                                                                                        this.setState({ isFetchingAPIKey: true }, () => {
                                                                                                            this.props.getAPIKey()
                                                                                                        })
                                                                                                    }}>
                                                                                                    <i className="fas fa-undo-alt"></i>
                                                                                                </button>
                                                                                                <ReactTooltip id="refreshApi" place="bottom" type="dark">
                                                                                                    <div className="tooltipText"><p>Refresh</p></div>
                                                                                                </ReactTooltip>
                                                                                            </React.Fragment>
                                                                                        }

                                                                                        <button type="button" className="btn btn-transparent btn-transparent-danger" data-tip data-for="deleteApi" onClick={() => { this.removeAPIKey() }}><i className="fas fa-trash-alt"></i></button>
                                                                                        <ReactTooltip id="deleteApi" place="bottom" type="dark">
                                                                                            <div className="tooltipText"><p>Delete</p></div>
                                                                                        </ReactTooltip>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                }

                                                {this.props.match.params.connector === "S3" &&
                                                    <div className="verticalStepBox">
                                                        <ul className="listStyleNone">
                                                            <li>
                                                                <h6><strong>Step 1:</strong>Add keys and test your connection before proceeding further</h6>
                                                                <div className="card panel">
                                                                    <div className="card-body panelBody">
                                                                        <div className="flex">
                                                                            <div className="fx-b40 pd-r-10">
                                                                                <div className="form-group">
                                                                                    <input type="password" className="form-control" id="accessKey" readOnly={!this.state.payload.isExternal} name="accessKey" value={this.state.payload.properties.accessKey} onChange={this.definitionChangeHandler} placeholder="Access Key" required />
                                                                                    <label className="form-group-label">Access Key:
                                                                                        <span className="requiredMark"><i className="fa fa-asterisk" /></span>
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                            <div className="fx-b40 pd-l-5 pd-r-5">
                                                                                <div className="form-group">
                                                                                    <input type="password" className="form-control" id="secretKey" readOnly={!this.state.payload.isExternal} name="secretKey" value={this.state.payload.properties.secretKey} onChange={this.definitionChangeHandler} placeholder="Secret Key" required />
                                                                                    <label className="form-group-label" htmlFor="secretKey">Secret Key:
                                                                                        <span className="requiredMark"><i className="fa fa-asterisk" /></span>
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                            <div className="fx-b20 pd-l-10">
                                                                                {this.state.isTestingConnection ?
                                                                                    <button type="button" className="btn btn-info cursor-default w-100">
                                                                                        <i className="fas fa-cog fa-spin mr-r-7"></i>Testing...
                                                                                    </button> :
                                                                                    this.state.connectionTestedSuccessFully ?
                                                                                        <button type="button" className="btn btn-success cursor-default w-100">
                                                                                            <i className="fas fa-check mr-r-7"></i>Tested
                                                                                        </button> :
                                                                                        this.state.connectionTestFailed ?
                                                                                            <button type="button" className="btn btn-danger w-100" disabled={!this.validateData()} onClick={this.testConnection}>
                                                                                                <i className="fas fa-undo-alt mr-r-7"></i>Retry
                                                                                            </button> :
                                                                                            <button type="button" className="btn btn-primary w-100" disabled={!this.validateData()} onClick={this.testConnection}>
                                                                                                <i className="fas fa-cog mr-r-7"></i>Test
                                                                                            </button>
                                                                                }
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <h6><strong>Step 2:</strong>Select your region, add bucket(s) and specify path</h6>
                                                                <div className="card panel">
                                                                    <div className="card-body panelBody">
                                                                        <div className="form-group mb-3">
                                                                            <select className="form-control" id="region" value={this.state.payload.properties.region} onChange={this.definitionChangeHandler} required>
                                                                                <option value=''>Select</option>
                                                                                {s3Regions.map((region, index) => {
                                                                                    return (<option key={index} value={region.value}>{region.name}</option>)
                                                                                })}
                                                                            </select>
                                                                            <label className="form-group-label">Region
                                                                                <span className="requiredMark"><i className="fa fa-asterisk" /></span>
                                                                            </label>
                                                                        </div>

                                                                        <p className="noteText"><strong>Note:</strong>Name(Description), Path, Format, Region field should not be blank</p>

                                                                        {this.state.payload.properties.buckets.map((bucket, index) => {
                                                                            return (
                                                                                <div className="bucketBox position-relative mb-5" key={index}>
                                                                                    <div className="bucketBoxHeader position-relative">
                                                                                        <div className="form-group">
                                                                                            <input required type="text" id="bucketName" className="form-control" placeholder="Bucket Name" value={bucket.name} onChange={(e) => this.bucketChangeHandler(e, index)} />
                                                                                            <label className="form-group-label">Bucket Name</label>
                                                                                        </div>
                                                                                        <div className="button-group">
                                                                                            <button type="button" className="btn btn-transparent btn-transparent-primary" data-tip data-for={'addPath' + index} onClick={() => this.addDeleteBuckets("add", index, "child")}>
                                                                                                <i className="far fa-plus"></i>
                                                                                            </button>
                                                                                            <ReactTooltip id={'addPath' + index} place="bottom" type="dark">
                                                                                                <div className="tooltipText"><p>Add Path</p></div>
                                                                                            </ReactTooltip>
                                                                                            <button type="button" className="btn btn-transparent btn-transparent-danger" data-tip data-for={'deleteBucket' + index} disabled={this.state.payload.properties.buckets.length === 1} onClick={() => this.addDeleteBuckets("delete", index, "parent")}><i className="far fa-trash-alt" ></i></button>
                                                                                            <ReactTooltip id={'deleteBucket' + index} place="bottom" type="dark">
                                                                                                <div className="tooltipText"><p>Delete Bucket</p></div>
                                                                                            </ReactTooltip>
                                                                                        </div>
                                                                                    </div>

                                                                                    {bucket.dataDirs.map((dataDir, childIndex) => {
                                                                                        return (
                                                                                            <ul className="bucketBoxContent listStyleNone flex" key={childIndex}>
                                                                                                <li className="fx-b25">
                                                                                                    <div className="form-group">
                                                                                                        <input type="text" className="form-control" id="name" name="name" placeholder="Name(Description)" value={dataDir.name} onChange={(e) => this.bucketChangeHandler(e, index, childIndex)} required />
                                                                                                        <label className="form-group-label" htmlFor="name">Name(Description) :
                                                                                                            <span className="requiredMark"><i className="fa fa-asterisk" /></span></label>
                                                                                                    </div>
                                                                                                </li>
                                                                                                <li className="fx-b25">
                                                                                                    <div className="form-group">
                                                                                                        <input required type="text" id="basePath" placeholder="Path" className="form-control" value={dataDir.basePath} onChange={(e) => this.bucketChangeHandler(e, index, childIndex)} />
                                                                                                        <label className="form-group-label">Path :
                                                                                                            <span className="requiredMark"><i className="fa fa-asterisk" /></span>
                                                                                                        </label>
                                                                                                    </div>
                                                                                                </li>
                                                                                                <li className="fx-b25">
                                                                                                    <div className="form-group">
                                                                                                        <select className="form-control" id="format" value={dataDir.format} onChange={(e) => this.bucketChangeHandler(e, index, childIndex)} required>
                                                                                                            <option value=''>Select</option>
                                                                                                            <option value="json">json</option>
                                                                                                            <option value="csv">csv</option>
                                                                                                            <option value="parquet">parquet</option>
                                                                                                        </select>
                                                                                                        <label className="form-group-label" htmlFor="topic">Format :
                                                                                                            <span className="requiredMark"><i className="fa fa-asterisk" /></span>
                                                                                                        </label>
                                                                                                    </div>
                                                                                                </li>
                                                                                                <li className="fx-b25">
                                                                                                    <div className="form-group">
                                                                                                        <TagsInput value={dataDir.partitionBy} inputProps={{ "placeholder": 'Partition By' }} onChange={this.inputTagsChangeHandler(index, childIndex, "S3")} />
                                                                                                    </div>
                                                                                                </li>
                                                                                                <button type="button" className="btn btn-transparent btn-transparent-danger" data-tip data-for={'deletePath' + childIndex} disabled={this.state.payload.properties.buckets[0].dataDirs.length === 1} onClick={() => this.addDeleteBuckets("delete", index, "child", childIndex)} >
                                                                                                    <i className="far fa-trash-alt"></i>
                                                                                                </button>
                                                                                                <ReactTooltip id={'deletePath' + childIndex} place="bottom" type="dark">
                                                                                                    <div className="tooltipText"><p>Delete</p></div>
                                                                                                </ReactTooltip>
                                                                                            </ul>
                                                                                        )
                                                                                    })}
                                                                                </div>
                                                                            )
                                                                        })}

                                                                        <div className="text-right">
                                                                            <button type='button' className="btn-link" onClick={() => this.addDeleteBuckets("add", null, "parent")}><i className="far fa-plus"></i>Add New Bucket</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                }

                                                {this.props.match.params.connector === "HDFS" &&
                                                    <div className="verticalStepBox">
                                                        <ul className="listStyleNone">
                                                            <li>
                                                                <h6><strong>Step 1:</strong>Add URL and path</h6>
                                                                <div className="card panel">
                                                                    <div className="card-body panelBody">
                                                                        <p className="noteText"><strong>Note:</strong>Name(Description), Path, Format, URL field should not be blank</p>
                                                                        {this.state.payload.isExternal ?
                                                                            <React.Fragment>
                                                                                <div className="flex mb-4">
                                                                                    <div className="fx-b35 pd-r-15">
                                                                                        <div className="form-group">
                                                                                            <input type="text" className="form-control" id="url" name="url" value={this.state.payload.properties.url} onChange={this.definitionChangeHandler} placeholder="Url" required />
                                                                                            <label className="form-group-label">URL :
                                                                                                <span className="requiredMark"><i className="fa fa-asterisk" /></span>
                                                                                            </label>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div className="fx-b35 pd-l-5 pd-r-10">
                                                                                        <div className="form-group">
                                                                                            <input type="text" className="form-control" id="basePath" name="basePath" value={this.state.payload.properties.basePath} onChange={this.definitionChangeHandler} placeholder="basePath" />
                                                                                            <label className="form-group-label">Base Path :
                                                                                                <span className="requiredMark"><i className="fa fa-asterisk" /></span>
                                                                                            </label>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div className="fx-b30 pd-l-10 border-left">
                                                                                        <div className="form-control h-100">
                                                                                            <p className="d-inline-block mb-0 mt-1 f-13">Enable Authentication :</p>
                                                                                            <label className="toggleSwitch mr-l-10">
                                                                                                <input type="checkbox" id="isAuthenticated" checked={this.state.payload.properties.isAuthenticated} onChange={this.definitionChangeHandler} />
                                                                                                <span className="toggleSwitchSlider"></span>
                                                                                            </label>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                {this.state.payload.properties.isAuthenticated ?
                                                                                    <div className="flex">
                                                                                        <div className="form-group fx-b50 pd-r-10">
                                                                                            <input type="text" className="form-control" id="username" name="username" value={this.state.payload.properties.username} onChange={this.definitionChangeHandler} placeholder="User Name" required />
                                                                                            <label className="form-group-label">User Name :
                                                                                                <span className="requiredMark"><i className="fa fa-asterisk" /></span></label>
                                                                                        </div>
                                                                                        <div className="form-group fx-b50 pd-l-10">
                                                                                            <input type="password" className="form-control" id="password" name="password" value={this.state.payload.properties.password} onChange={this.definitionChangeHandler} placeholder="Password" />
                                                                                            <label className="form-group-label">Password :
                                                                                                <span className="requiredMark"><i className="fa fa-asterisk" /></span></label>
                                                                                        </div>
                                                                                    </div> : ''
                                                                                }
                                                                            </React.Fragment> : ''
                                                                        }
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <h6><strong>Step 2:</strong>Specify your path</h6>
                                                                <div className="card panel">
                                                                    <div className="card-body panelBody">
                                                                        <div className="hdfsListBox position-relative">
                                                                            {this.state.payload.properties.dataDirs.map((dir, index) => {
                                                                                return (
                                                                                    <ul className="flex listStyleNone" key={index}>
                                                                                        <li className="fx-b25">
                                                                                            <div className="form-group">
                                                                                                <input type="text" className="form-control" id="dataDirsName" name="name" placeholder="Name(Description)" value={dir.name} onChange={(e) => this.dataDirectoryChangeHandler(e, index)} required />
                                                                                                <label className="form-group-label" htmlFor="name">Name(Description) :
                                                                                                    <span className="requiredMark"><i className="fa fa-asterisk" /></span>
                                                                                                </label>
                                                                                            </div>
                                                                                        </li>
                                                                                        <li className="fx-b25">
                                                                                            <div className="form-group">
                                                                                                <input required type="text" id="dataDirsBasePath" placeholder="Path" className="form-control" value={dir.basePath} onChange={(e) => this.dataDirectoryChangeHandler(e, index)} required />
                                                                                                <label className="form-group-label">Path :
                                                                                                    <span className="requiredMark"><i className="fa fa-asterisk" /></span>
                                                                                                </label>
                                                                                            </div>
                                                                                        </li>
                                                                                        <li className="fx-b25">
                                                                                            <div className="form-group">
                                                                                                <select className="form-control" id="format" value={dir.format} onChange={(e) => this.dataDirectoryChangeHandler(e, index)} required>
                                                                                                    <option value=''>Select</option>
                                                                                                    <option value="json">json</option>
                                                                                                    <option value="csv">csv</option>
                                                                                                    <option value="parquet">parquet</option>
                                                                                                </select>
                                                                                                <label className="form-group-label" htmlFor="topic">Format :
                                                                                                    <span className="requiredMark"><i className="fa fa-asterisk" /></span>
                                                                                                </label>
                                                                                            </div>
                                                                                        </li>
                                                                                        <li className="fx-b25">
                                                                                            <div className="form-group">
                                                                                                <TagsInput value={dir.partitionBy} inputProps={{ "placeholder": 'Partition By' }} onChange={this.inputTagsChangeHandler(index, null, "HDFS")} />
                                                                                            </div>
                                                                                        </li>
                                                                                        <button type="button" className="btn btn-transparent btn-transparent-danger" data-tip data-for={'deleteDir' + index} disabled={this.state.payload.properties.dataDirs.length === 1} onClick={(e) => this.addDeleteDirectories(e, "delete", index)}>
                                                                                            <i className="far fa-trash-alt"></i>
                                                                                        </button>
                                                                                        <ReactTooltip id={'deleteDir' + index} place="bottom" type="dark">
                                                                                            <div className="tooltipText"><p>Delete</p></div>
                                                                                        </ReactTooltip>
                                                                                    </ul>
                                                                                )
                                                                            })}
                                                                            <div className="text-right">
                                                                                <button type="button" className="btn-link" onClick={(e) => this.addDeleteDirectories(e, "add", null)} ><i className="far fa-plus"></i>Add New</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                }

                                                {this.props.match.params.connector === "MONGODB" &&
                                                    <div className="verticalStepBox">
                                                        <ul className="listStyleNone">
                                                            <li>
                                                                <h6><strong>Step:</strong>Add URL, database and test your connection before proceeding further</h6>
                                                                <div className="card panel">
                                                                    <div className="card-body panelBody">
                                                                        <div className="flex mb-4">
                                                                            <div className="fx-b40 pd-r-10">
                                                                                <div className="form-group">
                                                                                    <div className="form-group">
                                                                                        <input type="text" className="form-control" readOnly={!this.state.payload.isExternal} id="url" name="url" placeholder="Url" value={this.state.payload.properties.url} onChange={this.definitionChangeHandler} required />
                                                                                        <label className="form-group-label" htmlFor="url">URL :
                                                                                            <span className="requiredMark"><i className="fa fa-asterisk" /></span></label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div className="fx-b40 pd-l-5 pd-r-5">
                                                                                <div className="form-group">
                                                                                    <input type="text" className="form-control" readOnly={!this.state.payload.isExternal} id="database" name="Data Base" placeholder=" Data Base" value={this.state.payload.properties.database} onChange={this.definitionChangeHandler} required />
                                                                                    <label className="form-group-label"> Data Base :
                                                                                        <span className="requiredMark"><i className="fa fa-asterisk" /></span>
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                            <div className="fx-b20 pd-l-10">
                                                                                {this.state.isTestingConnection ?
                                                                                    <button type="button" className="btn btn-info cursor-default w-100">
                                                                                        <i className="fas fa-cog fa-spin mr-r-7"></i>Testing...
                                                                                    </button> :
                                                                                    this.state.connectionTestedSuccessFully ?
                                                                                        <button type="button" className="btn btn-success cursor-default w-100">
                                                                                            <i className="fas fa-check mr-r-7"></i>Tested
                                                                                        </button> :
                                                                                        this.state.connectionTestFailed ?
                                                                                            <button type="button" className="btn btn-danger w-100" disabled={!this.validateData()} onClick={this.testConnection}>
                                                                                                <i className="fas fa-undo-alt mr-r-7"></i>Retry
                                                                                            </button> :
                                                                                            <button type="button" className="btn btn-primary w-100" disabled={!this.validateData()} onClick={this.testConnection}>
                                                                                                <i className="fas fa-cog mr-r-7"></i>Test
                                                                                            </button>
                                                                                }
                                                                            </div>
                                                                        </div>
                                                                        <div className="form-group mb-4">
                                                                            <label className="form-label mr-r-15">Do you want to enable Authentication ?</label>
                                                                            <label className={this.state.payload.isExternal ? "toggleSwitch" : "toggleSwitch toggleSwitchDisabled"}>
                                                                                <input type="checkbox" id="isAuthenticated" disabled={!this.state.payload.isExternal} checked={this.state.payload.properties.isAuthenticated} onChange={this.definitionChangeHandler} />
                                                                                <span className="toggleSwitchSlider"></span>
                                                                            </label>
                                                                        </div>
                                                                        {this.state.payload.properties.isAuthenticated &&
                                                                            <div className="flex">
                                                                                <div className="fx-b50 pd-r-10">
                                                                                    <div className="form-group">
                                                                                        <input type="text" className="form-control" id="username" name="username" readOnly={!this.state.payload.isExternal} placeholder="User Name" value={this.state.payload.properties.username} onChange={this.definitionChangeHandler} required />
                                                                                        <label className="form-group-label">User Name
                                                                                        <span className="requiredMark"><i className="fa fa-asterisk" /></span>
                                                                                        </label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="fx-b50 pd-l-10">
                                                                                    <div className="form-group">
                                                                                        <input type="password" className="form-control" id="password" name="password" readOnly={!this.state.payload.isExternal} placeholder="Password" value={this.state.payload.properties.password} onChange={this.definitionChangeHandler} />
                                                                                        <label className="form-group-label">Password
                                                                                        <span className="requiredMark"><i className="fa fa-asterisk" /></span>
                                                                                        </label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        }
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                }

                                                {this.props.match.params.connector === "HIVE" &&
                                                    <div className="verticalStepBox">
                                                        <ul className="listStyleNone">
                                                            <li>
                                                                <h6><strong>Step:</strong>Add your database and keys</h6>
                                                                <div className="card panel">
                                                                    <div className="card-body panelBody">
                                                                        <div className="bucketBox position-relative mb-3">
                                                                            <div className="bucketBoxHeader bucketBoxHeaderBorderNone position-relative">
                                                                                <div className="form-group">
                                                                                    <input required type="text" id="database" placeholder="Data Base" value={this.state.payload.properties.database} onChange={this.hivePropsChangeHandler} className="form-control" />
                                                                                    <label className="form-group-label">Database</label>
                                                                                </div>
                                                                            </div>
                                                                            {this.state.payload.properties.tableList.map((table, index) =>
                                                                                <ul key={index} className="bucketBoxContent flex listStyleNone">
                                                                                    <li className="fx-b50">
                                                                                        <div className="form-group">
                                                                                            <input type="text" className="form-control" id={`name_${index}`} name="name" value={table.name} onChange={this.hivePropsChangeHandler} placeholder="Name" required />
                                                                                            <label className="form-group-label" htmlFor="name">Name:
                                                                                            <span className="requiredMark"><i className="fa fa-asterisk" /></span>
                                                                                            </label>
                                                                                        </div>
                                                                                    </li>
                                                                                    <li className="fx-b50">
                                                                                        <div className="form-group">
                                                                                            <TagsInput value={table.partitionBy} onChange={(tags) => this.hivePropsChangeHandler({ currentTarget: { value: tags, id: `partitionBy_${index}` } })} inputProps={{ "placeholder": 'Partition By' }} />
                                                                                        </div>
                                                                                    </li>
                                                                                    <button type="button" className="btn btn-transparent btn-transparent-danger" data-tip data-for={'deleteKey' + index} disabled={this.state.payload.properties.tableList.length === 1} onClick={() => this.addOrDeleteTable("REMOVE", index)}>
                                                                                        <i className="far fa-trash-alt"></i>
                                                                                    </button>
                                                                                    <ReactTooltip id={'deleteKey' + index} place="bottom" type="dark">
                                                                                        <div className="tooltipText"><p>Delete</p></div>
                                                                                    </ReactTooltip>
                                                                                </ul>
                                                                            )}
                                                                        </div>
                                                                        <div className="text-right">
                                                                            <button type="button" className="btn btn-link" onClick={() => this.addOrDeleteTable("ADD")} ><i className="far fa-plus"></i>Add Key</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                }

                                                {this.props.match.params.connector === "EMAIL" &&
                                                    <div className="verticalStepBox">
                                                        <ul className="listStyleNone">
                                                            <li>
                                                                <h6><strong>Step:</strong>Select you region and add access and secret key</h6>
                                                                <div className="card panel">
                                                                    <div className="card-body panelBody">
                                                                        <div className="flex">
                                                                            <div className="fx-b30 pd-r-10">
                                                                                <div className="form-group">
                                                                                    <select className="form-control" id="region" value={this.state.payload.properties.region} onChange={this.definitionChangeHandler} required>
                                                                                        <option value=''>Select</option>
                                                                                        {s3Regions.map((region, index) => {
                                                                                            return (<option key={index} value={region.value}>{region.name}</option>)
                                                                                        })}
                                                                                    </select>
                                                                                    <label className="form-group-label">Region
                                                                                    <span className="requiredMark"><i className="fa fa-asterisk" /></span>
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                            <div className="fx-b35 pd-l-5 pd-r-5">
                                                                                <div className="form-group">
                                                                                    <input type="password" className="form-control" id="accessKey" name="accessKey" value={this.state.payload.properties.accessKey} onChange={this.definitionChangeHandler} placeholder="Access Key" required />
                                                                                    <label className="form-group-label">Access Key:
                                                                                    <span className="requiredMark"><i className="fa fa-asterisk" /></span></label>
                                                                                </div>
                                                                            </div>
                                                                            <div className="fx-b35 pd-l-10">
                                                                                <div className="form-group">
                                                                                    <input type="password" className="form-control" id="secretKey" name="secretKey" value={this.state.payload.properties.secretKey} onChange={this.definitionChangeHandler} placeholder="Secret Key" required />
                                                                                    <label className="form-group-label">Secret Key:
                                                                                    <span className="requiredMark"><i className="fa fa-asterisk" /></span>
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                }

                                                {this.props.match.params.connector === "ELASTIC" &&
                                                    <div className="verticalStepBox">
                                                        <ul className="listStyleNone">
                                                            <li>
                                                                <h6><strong>Step 1:</strong>Select your scheme type</h6>
                                                                <div className="card panel">
                                                                    <div className="card-body panelBody">
                                                                        <p className="noteText"><strong>Note:</strong>Name(Description), Path, Format, URL field should not be blank</p>
                                                                        <div className="flex mb-4">
                                                                            <div className="fx-b35 pd-r-15">
                                                                                <div className="form-group">
                                                                                    <input type="text" className="form-control" id="esNode" name="esNode" placeholder="Node" />
                                                                                    <label className="form-group-label">Node :
                                                                                    <span className="requiredMark"><i className="fa fa-asterisk" /></span>
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                            <div className="fx-b35 pd-l-5 pd-r-10">
                                                                                <div className="form-group">
                                                                                    <input type="text" className="form-control" id="esPort" name="esPort" placeholder="Port" />
                                                                                    <label className="form-group-label">Port :
                                                                                    <span className="requiredMark"><i className="fa fa-asterisk" /></span>
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                            <div className="fx-b30 pd-l-10 border-left">
                                                                                <div className="form-control h-100">
                                                                                    <p className="d-inline-block mb-0 mt-1">Enable Authentication :</p>
                                                                                    <label className="toggleSwitch mr-l-15">
                                                                                        <input type="checkbox" id="isAuthenticated" />
                                                                                        <span className="toggleSwitchSlider"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div className="flex">
                                                                            <div className="fx-b33 pd-r-10">
                                                                                <div className="form-group">
                                                                                    <input type="text" className="form-control" id="" name="username" placeholder="User Name" required />
                                                                                    <label className="form-group-label">User Name :
                                                                                    <span className="requiredMark"><i className="fa fa-asterisk" /></span>
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                            <div className="fx-b33 pd-l-5 pd-r-5">
                                                                                <div className="form-group ">
                                                                                    <input type="password" className="form-control" id="" name="password" placeholder="Password" />
                                                                                    <label className="form-group-label">Password :
                                                                                    <span className="requiredMark"><i className="fa fa-asterisk" /></span>
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                            <div className="fx-b33 pd-l-10">
                                                                                <div className="form-group ">
                                                                                    <select className="form-control">
                                                                                        <option>Select</option>
                                                                                        <option value="idx_temperature">Temperature</option>
                                                                                        <option value="idx_humidity">humidity</option>
                                                                                    </select>
                                                                                    <label className="form-group-label" htmlFor="Index">Index:
                                                                                    <span className="requiredMark"><i className="fa fa-asterisk" /></span>
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                }
                                            </div>
                                        }
                                    </div>
                                </div>

                                <div className="bottomButton">
                                    <button type="submit" className="btn btn-success" disabled={this.saveUpdateValidator()}>
                                        {this.props.match.params.id ? "Update" : "Save"}
                                    </button>
                                    <button type="button" className="btn btn-dark" onClick={() => { localStorage.tenantType === 'SAAS' ? this.props.history.push(`/connectors`) : this.props.history.push(`/connectors/${this.props.match.params.connector}`) }}>Cancel</button>
                                </div>
                            </form>
                        </div>
                    </React.Fragment >
                }

                {/* map modal */}
                {this.state.openLocationSelector &&
                    <div className="modal d-block animated slideInDown" id="locationModal">
                        <div className="modal-dialog modal-lg modal-dialog-centered">
                            <div className="modal-content">
                                <div className="modal-header position-relative">
                                    <h6 className="modal-title">Add Location for Device Id - {this.state.selectedDeviceDetails.deviceId}<span></span>
                                        <button type="button" className="close" onClick={() => this.setState({ openLocationSelector: false })} data-dismiss="modal">
                                            <i className="far fa-times"></i>
                                        </button>
                                    </h6>
                                </div>
                                <div className="modal-body pd-15 pd-t-60">
                                    <Map
                                        deviceDetails={this.state.selectedDeviceDetails}
                                        setLocation={this.setLocation}
                                        zoom={5}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                }
                {/* end map modal */}

                {/* schema modal */}
                {this.state.showJsonModal &&
                    <div className="modal d-block animated slideInDown" id="schema">
                        <div className="modal-dialog modal-lg modal-dialog-centered">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h6 className="modal-title">Connector Name - <span>{this.state.payload.name}</span>
                                        <button type="button" className="close" data-dismiss="modal" onClick={this.closeJsonModal}><i className="far fa-times"></i></button>
                                    </h6>
                                </div>
                                {this.state.jsonModalLoader ?
                                    <div className="schemaBoxLoader">
                                        <div className="formLoaderBox">
                                            <div className="loader"></div>
                                        </div>
                                    </div> :
                                    <React.Fragment>
                                        <div className="modal-body">
                                            <div className="jsonInputBox">
                                                <JSONInput
                                                    theme="light_mitsuketa_tribute"
                                                    viewOnly={typeof (this.state.selectedTopicIndex) != "number"}
                                                    onChange={(data) => { this.schemaEditHandler(data.jsObject) }}
                                                    placeholder={typeof (this.state.selectedTopicIndex) === "number" ? this.state.userJson : this.state.selectedTopic.schema}
                                                />
                                            </div>
                                        </div>
                                        {typeof (this.state.selectedTopicIndex) === "number" ?
                                            <div className="modal-footer">
                                                <button name="button" className="btn btn-success" disabled={this.IsNotValidJsonString()} onClick={() => this.generateSchema(this.state.selectedTopicIndex, "userJson")}>Save</button>
                                                <button type="button" name="button" className="btn btn-dark" onClick={this.closeJsonModal}>Cancel</button>
                                            </div> :
                                            <div className="modal-footer">
                                                <button type="button" name="button" className="btn btn-success" onClick={this.closeJsonModal}>OK</button>
                                            </div>
                                        }
                                    </React.Fragment>
                                }
                            </div>
                        </div>
                    </div>
                }
                {/* end schema modal */}

                {/* sniffing modal */}
                <div className="modal animated slideInDown" id="sniffingModal" role="dialog">
                    {this.state.sniffingTopic ?
                        <div className="modal-dialog modal-lg modal-dialog-centered">
                            <div className="modal-content">
                                <div className="modal-header position-relative">
                                    <h6 className="modal-title">Connector: <span>{this.state.payload.name}</span>
                                        <button type="button" className="close" onClick={this.closeSniffingModal}><i className="far fa-times"></i></button>
                                    </h6>
                                    <ul className="modalHeaderList listStyleNone">
                                        <li className={this.state.dataView === 'FILE' ? 'active' : ''} data-tip data-for="jsonView" onClick={() => this.setState({ dataView: 'FILE' })}>
                                            <i className="fas fa-file-alt"></i>
                                            <ReactTooltip id="jsonView" place="bottom" type="dark">
                                                <div className="tooltipText"><p>JSON View</p></div>
                                            </ReactTooltip>
                                        </li>
                                        <li className={this.state.dataView === 'TREE' ? 'active' : ''} data-tip data-for="treeView" onClick={() => this.setState({ dataView: 'TREE' })}>
                                            <i className="fas fa-folder-tree"></i>
                                            <ReactTooltip id="treeView" place="bottom" type="dark">
                                                <div className="tooltipText"><p>Tree View</p></div>
                                            </ReactTooltip>
                                        </li>
                                        <li data-tip data-for="clear" onClick={this.clearSniffingModal} >
                                            <i className="far fa-trash-alt"></i>
                                            <ReactTooltip id="clear" place="bottom" type="dark">
                                                <div className="tooltipText"><p>Clear All</p></div>
                                            </ReactTooltip>
                                        </li>
                                    </ul>
                                </div>

                                <div className="modal-body">
                                    <ul className="nav nav-tabs modalNavTabs">
                                        {this.state.allTopics.map(topic => {
                                            return (
                                                <li key={topic.topicName} className="nav-item">
                                                    <a className={this.state.sniffingTopic === topic.topicName ? "nav-link active" : "nav-link"} onClick={() => { this.subscribeMqttTopic(topic.topicName) }}>{`<topic>/${topic.topicName.split('/')[1]}/${topic.topicName.split('/')[2]}`}</a>
                                                </li>
                                            )
                                        })}
                                    </ul>
                                    <div className="streamingBox">
                                        <ul className="liveStreamingData">
                                            {this.state.allTopics.map((topic, index) => {
                                                let html
                                                if (this.state.sniffingTopic === topic.topicName) {
                                                    topic.messages.length ?
                                                        this.state.dataView === 'FILE' ?
                                                            html = topic.messages.map((messageObj, index) => {
                                                                this.state.dataView !== 'FILE' ? delete messageObj.time : ''
                                                                delete messageObj.name
                                                                return (
                                                                    <li key={index} onMouseEnter={() => this.setState({ isHoldData: true })} onMouseLeave={() => this.setState({ isHoldData: false })}>
                                                                        <input type="text" id={`copyText${index}`} value={messageObj.message} readOnly />
                                                                        {messageObj.message}
                                                                        <button className="btn btn-transparent btn-transparent-primary" onClick={() => this.copyToClipboard(`copyText${index}`)}><i className="far fa-copy"></i>
                                                                            <p className="d-inline-block" id={`Copied${index}`} data-tip="Copied"></p>
                                                                            <ReactTooltip className="copyMessage" place="left" />
                                                                        </button>
                                                                        <span>{messageObj.time}</span>
                                                                    </li>)
                                                            }) :
                                                            html = topic.messages.map((messageObj, index) => <ReactJson key={index} src={messageObj} />)
                                                        :
                                                        html = (
                                                            <div key={index} className="fileUploadLoader">
                                                                <img src="https://content.iot83.com/m83/misc/uploading.gif" />
                                                                <h6>Sniffing...</h6>
                                                            </div>
                                                        )
                                                }
                                                return html;
                                            })}
                                        </ul>
                                    </div>
                                </div>

                                <div className="modal-footer">
                                    <button type="button" className="btn btn-success" onClick={this.closeSniffingModal}>OK</button>
                                </div>
                            </div>
                        </div> : ''
                    }
                </div>
                {/* end sniffing modal */}

                {/* device command modal */}
                {this.state.activeCommandIndex >= 0 &&
                    <div className="modal d-block animated slideInDown" id="commandModal">
                        <div className="modal-dialog modal-lg modal-dialog-centered">
                            <div className="modal-content">
                                <div className="modal-header position-relative">
                                    <h6 className="modal-title">Add Commands
                                    <button type="button" className="close" onClick={this.closeCommandModal}>
                                            <i className="far fa-times"></i>
                                        </button>
                                    </h6>
                                </div>
                                {this.state.commandModalLoader ?
                                    <div className="schemaBoxLoader">
                                        <div className="formLoaderBox">
                                            <i className="fad fa-sync-alt fa-spin text-primary"></i>
                                        </div>
                                    </div>
                                    :
                                    <React.Fragment>
                                        <div className="modal-body">
                                            <div className="flex">
                                                <div className="fx-b50 pd-r-10">
                                                    <ul className="listStyleNone form-group">
                                                        {this.state.controlConfig.map((command, i) =>
                                                            <li key={i} className="input-group mb-2">
                                                                <input className={this.state.activeCommandIndex == i ? "form-control form-control-active" : "form-control"} onChange={(e) => this.commandChangeHandler(i, e.currentTarget.value, "controlName")} onFocus={() => this.handleActiveCommand(i)} value={command.controlName} />
                                                                <div className="input-group-append">
                                                                    <button type="button" className="btn btn-transparent btn-transparent-danger" disabled={this.state.controlConfig.length === 1} onClick={() => this.deleteCommandHandler(i)}>
                                                                        <i className="fas fa-trash-alt"></i>
                                                                    </button>
                                                                </div>
                                                            </li>
                                                        )}
                                                    </ul>
                                                    <div className="text-center">
                                                        <button className="btn btn-link p-0" onClick={this.addCommandHandler}><i className="fas fa-plus"></i>Add New</button>
                                                    </div>
                                                </div>
                                                <div className="fx-b50 pd-l-10">
                                                    <div className="jsonInputBox">
                                                        <JSONInput
                                                            value={this.state.controlConfig[this.state.activeCommandIndex].controlCommand}
                                                            theme="light_mitsuketa_tribute"
                                                            onChange={(data) => this.commandChangeHandler(this.state.activeCommandIndex, data.jsObject, "controlCommand")}
                                                            placeholder={this.state.controlConfig[this.state.activeCommandIndex].controlCommand}
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="modal-footer">
                                            <button className="btn btn-success" onClick={this.saveCommandsHandler}
                                                disabled={this.state.controlConfig.some(config => !config.controlName || !isPlainObject(config.controlCommand))}>Save</button>
                                            <button className="btn btn-dark" onClick={this.closeCommandModal}>Cancel</button>
                                        </div>
                                    </React.Fragment>
                                }
                            </div>
                        </div>
                    </div>
                }
                {/* end device command modal */}

                {this.state.confirmState &&
                    <ConfirmModel
                        status={"delete"}
                        deleteName={this.state.deviceToBeDeletedName}
                        confirmClicked={this.deleteDeviceHandler}
                        cancelClicked={this.toggleConfirmModal}
                    />
                }

                {this.state.isOpen &&
                    <NotificationModal
                        type={this.state.modalType}
                        message2={this.state.message2}
                        onCloseHandler={this.onCloseHandler}
                    />
                }
            </React.Fragment >
        );
    }
}

AddOrEditEtlDataSource.propTypes = {
    dispatch: PropTypes.func
};

const mapStateToProps = createStructuredSelector({
    getSaveDataSourceSuccess: Selectors.getSaveDataSourceSuccess(),
    getSaveDataSourceFailure: Selectors.getSaveDataSourceFailure(),
    getDataSourceSuccess: Selectors.getDataSourceSuccess(),
    getDataSourceFailure: Selectors.getDataSourceFailure(),
    isFetching: Selectors.getIsFetching(),
    getSourceJsonSuccess: Selectors.getSourceJsonSuccess(),
    getSourceJsonFailure: Selectors.getSourceJsonFailure(),
    fileUploadSuccess: Selectors.getFileUploadSuccess(),
    fileUploadFailure: Selectors.getFileUploadFailure(),
    getExcelFileDataSuccess: Selectors.getExcelFileDataSuccess(),
    getExcelFileDataFailure: Selectors.getExcelFileDataFailure(),
    generateSchemaSuccess: Selectors.generateSchemaSuccess(),
    generateSchemaFailure: Selectors.generateSchemaFailure(),
    testConnectionSuccess: Selectors.testConnectionSuccess(),
    testConnectionFailure: Selectors.testConnectionFailure(),
    connectionListSuccess: Selectors.connectionListSuccess(),
    connectionListFailure: Selectors.connectionListFailure(),
    getDisplayCodeContentSuccess: Selectors.getDisplayCodeContentSuccess(),
    getDisplayCodeContentFailure: Selectors.getDisplayCodeContentFailure(),
    apiKeySuccess: Selectors.apiKeySuccess(),
    apiKeyFailure: Selectors.apiKeyFailure(),
    dataConnectorsSuccess: Selectors.dataConnectorsSuccess(),
    dataConnectorsFailure: Selectors.dataConnectorsFailure(),
    createDeviceSuccess: Selectors.createDeviceSuccess(),
    createDeviceFailure: Selectors.createDeviceFailure(),
    clientIdsSuccess: Selectors.clientIdsSuccess(),
    clientIdsFailure: Selectors.clientIdsFailure(),
    availableDeviceIds: Selectors.remainingDeviceIdsCount(),
    availableDeviceIdsFailure: Selectors.remainingDeviceIdsFailure(),
    editDeviceSuccess: Selectors.editDeviceSuccess(),
    editDeviceFailure: Selectors.editDeviceFailure(),
    deleteDeviceSuccess: Selectors.deleteDeviceSuccess(),
    deleteDeviceFailure: Selectors.deleteDeviceFailure(),
    emailCredentialsSuccess: Selectors.emailCredentialsSuccess(),
    emailCredentialsFailure: Selectors.emailCredentialsFailure(),
    saveCommandsSuccess: Selectors.saveCommandsSuccess(),
    saveCommandsFailure: Selectors.saveCommandsFailure(),
});

function mapDispatchToProps(dispatch) {
    return {
        dispatch,
        saveDataSource: payload => dispatch(Actions.saveDataSource(payload)),
        uploadFileHandler: (payload, imageType, eventId) => dispatch(Actions.uploadFileHandler(payload, imageType, eventId)),
        getDataSource: id => dispatch(Actions.getDataSource(id)),
        getSourceJson: id => dispatch(Actions.getSourceJson(id)),
        getExcelFileData: (fileUpload, fileType) => dispatch(Actions.getExcelFileData(fileUpload, fileType)),
        generateSchema: (payload, index, category) => dispatch(Actions.generateSchema(payload, index, category)),
        testConnection: (payload) => dispatch(Actions.testConnection(payload)),
        getDisplayCodeContent: (url) => dispatch(Actions.getDisplayCodeContent(url)),
        getConnectorsList: () => dispatch(Actions.getConnectorsList()),
        getAPIKey: () => dispatch(Actions.getAPIKey()),
        getAllConnectorsCategory: () => dispatch(Actions.getAllConnectorsCategory()),
        createDevice: (payload) => dispatch(Actions.createDevice(payload)),
        fetchAvailableDeviceIdCount: () => dispatch(Actions.fetchAvailableDeviceIdCount()),
        editDevice: (payload, deviceId) => dispatch(Actions.editDevice(payload, deviceId)),
        generateClientIdsForDevice: (noOfDevices, connectorId) => dispatch(Actions.generateClientIdsForDevice(noOfDevices, connectorId)),
        deleteDevice: (deviceId, connectorId) => dispatch(Actions.deleteDevice(deviceId, connectorId)),
        emailCredentials: (connectorId) => dispatch(Actions.emailCredentials(connectorId)),
        saveCommands: (payload) => dispatch(Actions.saveCommands(payload)),
    }
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "addOrEditEtlDataSource", reducer });
const withSaga = injectSaga({ key: "addOrEditEtlDataSource", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(AddOrEditEtlDataSource);
