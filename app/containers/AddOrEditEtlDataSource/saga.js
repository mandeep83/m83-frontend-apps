/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { apiCallHandler } from '../../api';
import { takeEvery, takeLatest } from 'redux-saga';
import * as CONSTANTS from './constants'


export function* saveDataSource(action) {
  yield [apiCallHandler(action, CONSTANTS.SAVE_DATASOURCE_SUCCESS, CONSTANTS.SAVE_DATASOURCE_FAILURE, 'saveDataSource')]
}

export function* getDataSource(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_DATASOURCE_SUCCESS, CONSTANTS.GET_DATASOURCE_ERROR, 'getDataSource')]
}
export function* getSourceJson(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_SOURCE_JSON_SUCCESS, CONSTANTS.GET_SOURCE_JSON_ERROR, 'getSourceJson')]
}

export function* fileUploadApiHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.FILE_UPLOAD_SUCCESS, CONSTANTS.FILE_UPLOAD_FAILURE, 'fileUploadRequest', false)];
}

export function* excelFileDataHandler(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_EXCEL_FILE_DATA_SUCCESS, CONSTANTS.GET_EXCEL_FILE_DATA_FAILURE, 'getExcelFileDataUrl')]
}

export function* generateSchemaHandler(action) {
  yield [apiCallHandler(action, CONSTANTS.GENERATE_SCHEMA_SUCCESS, CONSTANTS.GENERATE_SCHEMA_FAILURE, 'generateSchema')]
}

export function* testConnectionHandler(action) {
  yield [apiCallHandler(action, CONSTANTS.TEST_CONNECTION_SUCCESS, CONSTANTS.TEST_CONNECTION_FAILURE, 'testBootstrapConnection')]
}

export function* getConnectorListHandler(action) {
  yield [apiCallHandler(action, CONSTANTS.CONNECTOR_LIST_REQUEST_SUCCESS, CONSTANTS.CONNECTOR_LIST_REQUEST_FAILURE, 'getDataSourceList')]
}

export function* displayCodeContentHandler(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_DISPLAY_CODE_CONTENT_SUCCESS, CONSTANTS.GET_DISPLAY_CODE_CONTENT_FAILURE, 'getExcelFileData')]
}

export function* getAPIKeyHandler(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_API_KEY_SUCCESS, CONSTANTS.GET_API_KEY_FAILURE, 'getAPIKey')]
}

export function* getAllConnectorsCategoryHandler(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_ALL_CONNECTORS_CATEGORY_SUCCESS, CONSTANTS.GET_ALL_CONNECTORS_CATEGORY, 'getDataConnectors')]
}

export function* createDeviceHandler(action) {
  yield [apiCallHandler(action, CONSTANTS.CREATE_DEVICE_SUCCESS, CONSTANTS.CREATE_DEVICE_FAILURE, 'createDevice')]
}

export function* getClientIdsGenerationHandler(action) {
  yield [apiCallHandler(action, CONSTANTS.GENERATE_CLIENT_IDS_SUCCESS, CONSTANTS.GENERATE_CLIENT_IDS_FAILURE, 'generateClientIds')]
}

export function* getRemainingClientIdsHandler(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_REMAINING_DEVICE_ID_COUNT_SUCCESS, CONSTANTS.GET_REMAINING_DEVICE_ID_COUNT_FAILURE, 'getRemainingDeviceIdCount')]
}

export function* deleteDeviceHandler(action) {
  yield [apiCallHandler(action, CONSTANTS.DELETE_DEVICE_SUCCESS, CONSTANTS.DELETE_DEVICE_FAILURE, 'deleteDevice')]
}

export function* editDeviceHandler(action) {
  yield [apiCallHandler(action, CONSTANTS.EDIT_DEVICE_SUCCESS, CONSTANTS.EDIT_DEVICE_FAILURE, 'editDevice')]
}

export function* emailCredentialsHandler(action) {
  yield [apiCallHandler(action, CONSTANTS.EMAIL_CREDENTIALS_SUCCESS, CONSTANTS.EMAIL_CREDENTIALS_FAILURE, 'emailCredentials')]
}

export function* saveCommandsHandler(action) {
  yield [apiCallHandler(action, CONSTANTS.SAVE_COMMANDS_SUCCESS, CONSTANTS.SAVE_COMMANDS_FAILURE, 'saveCommands')]
}

export function* watcherSaveDataSource() {
  yield takeEvery(CONSTANTS.SAVE_DATASOURCE, saveDataSource);
}

export function* watcherGetDataSource() {
  yield takeEvery(CONSTANTS.GET_DATASOURCE, getDataSource);
}

export function* watchergetSourceJson() {
  yield takeEvery(CONSTANTS.GET_SOURCE_JSON, getSourceJson);
}

export function* watcherFileUploadRequest() {
  yield takeEvery(CONSTANTS.FILE_UPLOAD_REQUEST, fileUploadApiHandlerAsync);
}

export function* watcherExcelFileData() {
  yield takeEvery(CONSTANTS.GET_EXCEL_FILE_DATA, excelFileDataHandler);
}

export function* watcherGenerateSchema() {
  yield takeEvery(CONSTANTS.GENERATE_SCHEMA, generateSchemaHandler);
}

export function* watcherTestConnection() {
  yield takeEvery(CONSTANTS.TEST_CONNECTION, testConnectionHandler);
}

export function* watcherGetConnectorList() {
  yield takeEvery(CONSTANTS.CONNECTOR_LIST_REQUEST, getConnectorListHandler);
}

export function* watcherDisplayCodeContent() {
  yield takeEvery(CONSTANTS.GET_DISPLAY_CODE_CONTENT, displayCodeContentHandler);
}

export function* watcherGetAPIKey() {
  yield takeEvery(CONSTANTS.GET_API_KEY, getAPIKeyHandler);
}

export function* watcherGetAllConnectorsCategory() {
  yield takeEvery(CONSTANTS.GET_ALL_CONNECTORS_CATEGORY, getAllConnectorsCategoryHandler);
}

export function* watcherCreateDevice() {
  yield takeEvery(CONSTANTS.CREATE_DEVICE, createDeviceHandler);
}

export function* watcherGenerateClientIds() {
  yield takeEvery(CONSTANTS.GENERATE_CLIENT_IDS, getClientIdsGenerationHandler);
}

export function* watcherEditDevice() {
  yield takeEvery(CONSTANTS.EDIT_DEVICE, editDeviceHandler);
}

export function* watcherGetRemainingClientIds() {
  yield takeEvery(CONSTANTS.GET_REMAINING_DEVICE_ID_COUNT, getRemainingClientIdsHandler);
}

export function* watcherDeleteDevice() {
  yield takeEvery(CONSTANTS.DELETE_DEVICE, deleteDeviceHandler);
}

export function* watcherEmailCredentials() {
  yield takeEvery(CONSTANTS.EMAIL_CREDENTIALS, emailCredentialsHandler);
}

export function* watcherSaveCommands() {
  yield takeEvery(CONSTANTS.SAVE_COMMANDS, saveCommandsHandler);
}

export default function* rootSaga() {
  yield [
    watcherSaveDataSource(),
    watcherGetDataSource(),
    watchergetSourceJson(),
    watcherFileUploadRequest(),
    watcherExcelFileData(),
    watcherGenerateSchema(),
    watcherTestConnection(),
    watcherDisplayCodeContent(),
    watcherGetConnectorList(),
    watcherGetAPIKey(),
    watcherGetAllConnectorsCategory(),
    watcherCreateDevice(),
    watcherGenerateClientIds(),
    watcherGetRemainingClientIds(),
    watcherEditDevice(),
    watcherDeleteDevice(),
    watcherEmailCredentials(),
    watcherSaveCommands(),
  ]
}