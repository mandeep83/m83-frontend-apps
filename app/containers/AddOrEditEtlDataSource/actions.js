/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import * as CONSTANTS from "./constants";

export function defaultAction() {
  return {
    type: CONSTANTS.DEFAULT_ACTION
  };
}


export function saveDataSource(payload) {
  return {
    type: CONSTANTS.SAVE_DATASOURCE,
    payload,
  }
}

export function getDataSource(id) {
  return {
    type: CONSTANTS.GET_DATASOURCE,
    id
  }
}
export function uploadFileHandler(payload, imageType, eventId) {
  return {
    type: CONSTANTS.FILE_UPLOAD_REQUEST,
    payload,
    imageType,
    eventId
  };
}

export function getSourceJson(id) {
  return {
    type: CONSTANTS.GET_SOURCE_JSON,
    id
  }
}

export function getExcelFileData(fileUpload, fileType) {
  return {
    type: CONSTANTS.GET_EXCEL_FILE_DATA,
    fileUpload,
    fileType
  }
}

export function generateSchema(payload, id, category) {
  return {
    type: CONSTANTS.GENERATE_SCHEMA,
    payload,
    id,
    category
  }
}
export function testConnection(payload) {
  return {
    type: CONSTANTS.TEST_CONNECTION,
    payload
  }
}

export function getConnectorsList() {
  return {
    type: CONSTANTS.CONNECTOR_LIST_REQUEST,
  };
}

export function getDisplayCodeContent(url) {
  return {
    type: CONSTANTS.GET_DISPLAY_CODE_CONTENT,
    url
  }
}
export function getAPIKey() {
  return {
    type: CONSTANTS.GET_API_KEY,
  }
}
export function getAllConnectorsCategory() {
  return {
    type: CONSTANTS.GET_ALL_CONNECTORS_CATEGORY,
  }
}
export function createDevice(payload) {
  return {
    type: CONSTANTS.CREATE_DEVICE,
    payload,
  }
}
export function editDevice(payload, deviceId) {
  return {
    type: CONSTANTS.EDIT_DEVICE,
    payload,
    deviceId
  }
}

export function generateClientIdsForDevice(noOfDevices, connectorId) {
  return {
    type: CONSTANTS.GENERATE_CLIENT_IDS,
    noOfDevices,
    connectorId,
  }
}

export function fetchAvailableDeviceIdCount() {
  return {
    type: CONSTANTS.GET_REMAINING_DEVICE_ID_COUNT,
  }
}

export function deleteDevice(deviceId, connectorId) {
  return {
    type: CONSTANTS.DELETE_DEVICE,
    deviceId,
    connectorId,
  }
}
export function emailCredentials(connectorId) {
  return {
    type: CONSTANTS.EMAIL_CREDENTIALS,
    connectorId,
  }
}

export function saveCommands(payload) {
  return {
    type: CONSTANTS.SAVE_COMMANDS,
    payload,
  }
}