/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
 * Direct selector to the addOrEditEtlDataSource state domain
 */

const selectAddOrEditEtlDataSourceDomain = state =>
  state.get("addOrEditEtlDataSource", initialState);

export const getSaveDataSourceSuccess = () => createSelector(selectAddOrEditEtlDataSourceDomain, subState => subState.getSaveDataSourceSuccess);
export const getSaveDataSourceFailure = () => createSelector(selectAddOrEditEtlDataSourceDomain, subState => subState.getSaveDataSourceFailure);

export const getFileUploadSuccess = () => createSelector(selectAddOrEditEtlDataSourceDomain, subState => subState.fileUploadSuccess);

export const getFileUploadFailure = () => createSelector(selectAddOrEditEtlDataSourceDomain, subState => subState.fileUploadFailure);

const isFetchingState = state => state.get('loader');
export const getIsFetching = () => createSelector(isFetchingState, fetchingState => fetchingState.get('isFetching'));

export const getDataSourceSuccess = () => createSelector(selectAddOrEditEtlDataSourceDomain, subState => subState.getDataSourceSuccess);
export const getDataSourceFailure = () => createSelector(selectAddOrEditEtlDataSourceDomain, subState => subState.getDataSourceFailure);

export const getSourceJsonSuccess = () => createSelector(selectAddOrEditEtlDataSourceDomain, subState => subState.getSourceJsonSuccess);
export const getSourceJsonFailure = () => createSelector(selectAddOrEditEtlDataSourceDomain, subState => subState.getSourceJsonFailure);

export const getExcelFileDataSuccess = () => createSelector(selectAddOrEditEtlDataSourceDomain, subState => subState.getExcelFileDataSuccess);
export const getExcelFileDataFailure = () => createSelector(selectAddOrEditEtlDataSourceDomain, subState => subState.getExcelFileDataFailure);

export const generateSchemaSuccess = () => createSelector(selectAddOrEditEtlDataSourceDomain, subState => subState.generateSchemaSuccess);
export const generateSchemaFailure = () => createSelector(selectAddOrEditEtlDataSourceDomain, subState => subState.generateSchemaFailure);

export const testConnectionSuccess = () => createSelector(selectAddOrEditEtlDataSourceDomain, subState => subState.testConnectionSuccess);
export const testConnectionFailure = () => createSelector(selectAddOrEditEtlDataSourceDomain, subState => subState.testConnectionFailure);

export const connectionListSuccess = () => createSelector(selectAddOrEditEtlDataSourceDomain, subState => subState.connectionListSuccess);
export const connectionListFailure = () => createSelector(selectAddOrEditEtlDataSourceDomain, subState => subState.connectionListFailure);

export const getDisplayCodeContentSuccess =()=> createSelector(selectAddOrEditEtlDataSourceDomain, subState => subState.getDisplayCodeContentSuccess);
export const getDisplayCodeContentFailure =()=> createSelector(selectAddOrEditEtlDataSourceDomain, subState => subState.getDisplayCodeContentFailure);

export const apiKeySuccess =()=> createSelector(selectAddOrEditEtlDataSourceDomain, subState => subState.apiKeySuccess);
export const apiKeyFailure =()=> createSelector(selectAddOrEditEtlDataSourceDomain, subState => subState.apiKeyFailure);

export const dataConnectorsSuccess =()=> createSelector(selectAddOrEditEtlDataSourceDomain, subState => subState.dataConnectorsSuccess);
export const dataConnectorsFailure =()=> createSelector(selectAddOrEditEtlDataSourceDomain, subState => subState.dataConnectorsFailure);

export const clientIdsSuccess =()=> createSelector(selectAddOrEditEtlDataSourceDomain, subState => subState.clientIdsSuccess);
export const clientIdsFailure =()=> createSelector(selectAddOrEditEtlDataSourceDomain, subState => subState.clientIdsFailure);

export const createDeviceSuccess =()=> createSelector(selectAddOrEditEtlDataSourceDomain, subState => subState.createDeviceSuccess);
export const createDeviceFailure =()=> createSelector(selectAddOrEditEtlDataSourceDomain, subState => subState.createDeviceFailure);

export const editDeviceSuccess =()=> createSelector(selectAddOrEditEtlDataSourceDomain, subState => subState.editDeviceSuccess);
export const editDeviceFailure =()=> createSelector(selectAddOrEditEtlDataSourceDomain, subState => subState.editDeviceFailure);


export const remainingDeviceIdsCount =()=> createSelector(selectAddOrEditEtlDataSourceDomain, subState => subState.remainingDeviceIdsCount);
export const remainingDeviceIdsFailure =()=> createSelector(selectAddOrEditEtlDataSourceDomain, subState => subState.remainingDeviceIdsFailure);

export const deleteDeviceSuccess =()=> createSelector(selectAddOrEditEtlDataSourceDomain, subState => subState.deleteDeviceSuccess);
export const deleteDeviceFailure =()=> createSelector(selectAddOrEditEtlDataSourceDomain, subState => subState.deleteDeviceFailure);

export const emailCredentialsSuccess =()=> createSelector(selectAddOrEditEtlDataSourceDomain, subState => subState.emailCredentialsSuccess);
export const emailCredentialsFailure =()=> createSelector(selectAddOrEditEtlDataSourceDomain, subState => subState.emailCredentialsFailure);

export const saveCommandsSuccess =()=> createSelector(selectAddOrEditEtlDataSourceDomain, subState => subState.saveCommandsSuccess);
export const saveCommandsFailure =()=> createSelector(selectAddOrEditEtlDataSourceDomain, subState => subState.saveCommandsFailure);