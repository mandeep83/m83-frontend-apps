/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

export const DEFAULT_ACTION = "app/AddOrEditEtlDataSource/DEFAULT_ACTION";

export const SAVE_DATASOURCE = "app/AddOrEditEtlDataSource/SAVE_DATASOURCE";
export const SAVE_DATASOURCE_SUCCESS = "app/AddOrEditEtlDataSource/SAVE_DATASOURCE_SUCCESS";
export const SAVE_DATASOURCE_FAILURE = "app/AddOrEditEtlDataSource/SAVE_DATASOURCE_FAILURE";

export const GET_DATASOURCE = "app/AddOrEditEtlDataSource/GET_DATASOURCE";
export const GET_DATASOURCE_SUCCESS = "app/AddOrEditEtlDataSource/GET_DATASOURCE_SUCCESS";
export const GET_DATASOURCE_ERROR = "app/AddOrEditEtlDataSource/GET_DATASOURCE_ERROR";

export const FILE_UPLOAD_REQUEST = "app/AddOrEditEtlDataSource/FILE_UPLOAD_REQUEST";
export const FILE_UPLOAD_SUCCESS = "app/AddOrEditEtlDataSource/FILE_UPLOAD_SUCCESS";
export const FILE_UPLOAD_FAILURE = "app/AddOrEditEtlDataSource/FILE_UPLOAD_FAILURE";

export const GET_SOURCE_JSON = "app/AddOrEditEtlDataSource/GET_SOURCE_JSON";
export const GET_SOURCE_JSON_SUCCESS = "app/AddOrEditEtlDataSource/GET_SOURCE_JSON_SUCCESS";
export const GET_SOURCE_JSON_ERROR = "app/AddOrEditEtlDataSource/GET_SOURCE_JSON_ERROR";

export const GET_EXCEL_FILE_DATA = "app/AddOrEditEtlDataSource/GET_EXCEL_FILE_DATA";
export const GET_EXCEL_FILE_DATA_SUCCESS = "app/AddOrEditEtlDataSource/GET_EXCEL_FILE_DATA_SUCCESS";
export const GET_EXCEL_FILE_DATA_FAILURE = "app/AddOrEditEtlDataSource/GET_EXCEL_FILE_DATA_FAILURE";

export const GENERATE_SCHEMA = "app/AddOrEditEtlDataSource/GENERATE_SCHEMA";
export const GENERATE_SCHEMA_SUCCESS = "app/AddOrEditEtlDataSource/GENERATE_SCHEMA_SUCCESS";
export const GENERATE_SCHEMA_FAILURE = "app/AddOrEditEtlDataSource/GENERATE_SCHEMA_FAILURE";

export const TEST_CONNECTION = "app/AddOrEditEtlDataSource/TEST_CONNECTION";
export const TEST_CONNECTION_SUCCESS = "app/AddOrEditEtlDataSource/TEST_CONNECTION_SUCCESS";
export const TEST_CONNECTION_FAILURE = "app/AddOrEditEtlDataSource/TEST_CONNECTION_FAILURE";

export const CONNECTOR_LIST_REQUEST = "app/AddOrEditEtlDataSource/CONNECTOR_LIST_REQUEST";
export const CONNECTOR_LIST_REQUEST_SUCCESS = "app/AddOrEditEtlDataSource/CONNECTOR_LIST_REQUEST_SUCCESS";
export const CONNECTOR_LIST_REQUEST_FAILURE = "app/AddOrEditEtlDataSource/CONNECTOR_LIST_REQUEST_FAILURE";

export const GET_DISPLAY_CODE_CONTENT = "app/AddOrEditEtlDataSource/GET_DISPLAY_CODE_CONTENT";
export const GET_DISPLAY_CODE_CONTENT_SUCCESS = "app/AddOrEditEtlDataSource/GET_DISPLAY_CODE_CONTENT_SUCCESS";
export const GET_DISPLAY_CODE_CONTENT_FAILURE = "app/AddOrEditEtlDataSource/GET_DISPLAY_CODE_CONTENT_FAILURE";

export const GET_API_KEY = "app/AddOrEditEtlDataSource/GET_API_KEY";
export const GET_API_KEY_SUCCESS = "app/AddOrEditEtlDataSource/GET_API_KEY_SUCCESS";
export const GET_API_KEY_FAILURE = "app/AddOrEditEtlDataSource/GET_API_KEY_FAILURE";

export const GET_ALL_CONNECTORS_CATEGORY = "app/AddOrEditEtlDataSource/GET_ALL_CONNECTORS_CATEGORY";
export const GET_ALL_CONNECTORS_CATEGORY_SUCCESS = "app/AddOrEditEtlDataSource/GET_ALL_CONNECTORS_CATEGORY_SUCCESS";
export const GET_ALL_CONNECTORS_CATEGORY_FAILURE = "app/AddOrEditEtlDataSource/GET_ALL_CONNECTORS_CATEGORY_FAILURE";

export const CREATE_DEVICE = "app/AddOrEditEtlDataSource/CREATE_DEVICE";
export const CREATE_DEVICE_SUCCESS = "app/AddOrEditEtlDataSource/CREATE_DEVICE_SUCCESS";
export const CREATE_DEVICE_FAILURE = "app/AddOrEditEtlDataSource/CREATE_DEVICE_FAILURE";

export const GENERATE_CLIENT_IDS = "app/AddOrEditEtlDataSource/GENERATE_CLIENT_IDS";
export const GENERATE_CLIENT_IDS_SUCCESS = "app/AddOrEditEtlDataSource/GENERATE_CLIENT_IDS_SUCCESS";
export const GENERATE_CLIENT_IDS_FAILURE = "app/AddOrEditEtlDataSource/GENERATE_CLIENT_IDS_FAILURE";

export const GET_REMAINING_DEVICE_ID_COUNT = "app/AddOrEditEtlDataSource/GET_REMAINING_DEVICE_ID_COUNT";
export const GET_REMAINING_DEVICE_ID_COUNT_SUCCESS = "app/AddOrEditEtlDataSource/GET_REMAINING_DEVICE_ID_COUNT_SUCCESS";
export const GET_REMAINING_DEVICE_ID_COUNT_FAILURE = "app/AddOrEditEtlDataSource/GET_REMAINING_DEVICE_ID_COUNT_FAILURE";

export const EDIT_DEVICE = "app/AddOrEditEtlDataSource/EDIT_DEVICE";
export const EDIT_DEVICE_SUCCESS = "app/AddOrEditEtlDataSource/EDIT_DEVICE_SUCCESS";
export const EDIT_DEVICE_FAILURE = "app/AddOrEditEtlDataSource/EDIT_DEVICE_FAILURE";

export const DELETE_DEVICE = "app/AddOrEditEtlDataSource/DELETE_DEVICE";
export const DELETE_DEVICE_SUCCESS = "app/AddOrEditEtlDataSource/DELETE_DEVICE_SUCCESS";
export const DELETE_DEVICE_FAILURE = "app/AddOrEditEtlDataSource/DELETE_DEVICE_FAILURE";

export const EMAIL_CREDENTIALS = "app/AddOrEditEtlDataSource/EMAIL_CREDENTIALS";
export const EMAIL_CREDENTIALS_SUCCESS = "app/AddOrEditEtlDataSource/EMAIL_CREDENTIALS_SUCCESS";
export const EMAIL_CREDENTIALS_FAILURE = "app/AddOrEditEtlDataSource/EMAIL_CREDENTIALS_FAILURE";

export const SAVE_COMMANDS = "app/AddOrEditEtlDataSource/SAVE_COMMANDS";
export const SAVE_COMMANDS_SUCCESS = "app/AddOrEditEtlDataSource/SAVE_COMMANDS_SUCCESS";
export const SAVE_COMMANDS_FAILURE = "app/AddOrEditEtlDataSource/SAVE_COMMANDS_FAILURE";