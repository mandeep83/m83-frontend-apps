/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { fromJS } from "immutable";
import * as CONSTANTS from "./constants";

export const initialState = fromJS({});

function addOrEditEtlDataSourceReducer(state = initialState, action) {
  switch (action.type) {
    case CONSTANTS.DEFAULT_ACTION:
      return state;
    case CONSTANTS.SAVE_DATASOURCE_SUCCESS:
      return Object.assign({}, state, {
        'getSaveDataSourceSuccess': action.response
      });
    case CONSTANTS.SAVE_DATASOURCE_FAILURE:
      return Object.assign({}, state, {
        'getSaveDataSourceFailure': action.error
      });
    case CONSTANTS.GET_DATASOURCE_SUCCESS:
      return Object.assign({}, state, {
        'getDataSourceSuccess': action.response
      });
    case CONSTANTS.GET_DATASOURCE_ERROR:
      return Object.assign({}, state, {
        'getDataSourceFailure': action.error
      });
    case CONSTANTS.GET_SOURCE_JSON_SUCCESS:
      return Object.assign({}, state, {
        'getSourceJsonSuccess': action.response
      });
    case CONSTANTS.GET_SOURCE_JSON_ERROR:
      return Object.assign({}, state, {
        'getSourceJsonFailure': action.error
      });
    case CONSTANTS.GET_EXCEL_FILE_DATA_SUCCESS:
      return Object.assign({}, state, {
        'getExcelFileDataSuccess': action.response
      });
    case CONSTANTS.GET_EXCEL_FILE_DATA_FAILURE:
      return Object.assign({}, state, {
        'getExcelFileDataFailure': action.error
      });
    case CONSTANTS.FILE_UPLOAD_SUCCESS:
      return Object.assign({}, state, {
        fileUploadSuccess: {
          url: action.response.secure_url,
          eventId: action.response.eventId
        }
      });
    case CONSTANTS.FILE_UPLOAD_FAILURE:
      return Object.assign({}, state, {
        fileUploadFailure: action.error
      });
    case CONSTANTS.GENERATE_SCHEMA:
      return Object.assign({}, state, {
        generateSchemaSuccess: undefined,
        generateSchemaFailure: undefined
      });
    case CONSTANTS.GENERATE_SCHEMA_SUCCESS:
      return Object.assign({}, state, {
        generateSchemaSuccess: action.response
      });
    case CONSTANTS.GENERATE_SCHEMA_FAILURE:
      return Object.assign({}, state, {
        generateSchemaFailure: { error: action.error, id: action.addOns.id }
      });
    case CONSTANTS.TEST_CONNECTION:
      return Object.assign({}, state, {
        testConnectionSuccess: undefined,
        testConnectionFailure: undefined
      });
    case CONSTANTS.TEST_CONNECTION_SUCCESS:
      return Object.assign({}, state, {
        testConnectionSuccess: action.response
      });
    case CONSTANTS.TEST_CONNECTION_FAILURE:
      return Object.assign({}, state, {
        testConnectionFailure: action.error
      });
    case CONSTANTS.CONNECTOR_LIST_REQUEST_SUCCESS:
      return Object.assign({}, state, {
        connectionListSuccess: action.response
      });
    case CONSTANTS.CONNECTOR_LIST_REQUEST_FAILURE:
      return Object.assign({}, state, {
        connectionListFailure: action.error
      });
    case CONSTANTS.GET_DISPLAY_CODE_CONTENT_SUCCESS:
      return Object.assign({}, state, {
        'getDisplayCodeContentSuccess': action.response
      });
    case CONSTANTS.GET_DISPLAY_CODE_CONTENT_FAILURE:
      return Object.assign({}, state, {
        'getDisplayCodeContentFailure': action.error
      });
    case CONSTANTS.GET_API_KEY_SUCCESS:
      return Object.assign({}, state, {
        'apiKeySuccess': action.response
      });
    case CONSTANTS.GET_API_KEY_FAILURE:
      return Object.assign({}, state, {
        'apiKeyFailure': action.error
      });
    case CONSTANTS.GET_ALL_CONNECTORS_CATEGORY_SUCCESS:
      return Object.assign({}, state, {
        'dataConnectorsSuccess': action.response
      });
    case CONSTANTS.GET_ALL_CONNECTORS_CATEGORY_FAILURE:
      return Object.assign({}, state, {
        'dataConnectorsFailure': action.error
      });
    case CONSTANTS.CREATE_DEVICE_SUCCESS:
      return Object.assign({}, state, {
        'createDeviceSuccess': action.response
      });
    case CONSTANTS.CREATE_DEVICE_FAILURE:
      return Object.assign({}, state, {
        'createDeviceFailure': action.error
      });
    case CONSTANTS.GENERATE_CLIENT_IDS_SUCCESS:
      return Object.assign({}, state, {
        'clientIdsSuccess': action.response
      });
    case CONSTANTS.GENERATE_CLIENT_IDS_FAILURE:
      return Object.assign({}, state, {
        'clientIdsFailure': action.error
      });
    case CONSTANTS.GET_REMAINING_DEVICE_ID_COUNT_SUCCESS:
      return Object.assign({}, state, {
        'remainingDeviceIdsCount': {
          count:action.response,
          timestamp:new Date()
        }
      });
    case CONSTANTS.GET_REMAINING_DEVICE_ID_COUNT_FAILURE:
      return Object.assign({}, state, {
        'remainingDeviceIdsFailure': action.error
      });
    case CONSTANTS.EDIT_DEVICE_SUCCESS:
      return Object.assign({}, state, {
        'editDeviceSuccess': {response : action.response, deviceId: action.deviceId, deviceDetails: action.deviceDetails}
      });
    case CONSTANTS.EDIT_DEVICE_FAILURE:
      return Object.assign({}, state, {
        'editDeviceFailure': { error: action.error, deviceId: action.addOns.deviceId }
      });
    case CONSTANTS.DELETE_DEVICE_SUCCESS:
      return Object.assign({}, state, {
        'deleteDeviceSuccess': {response : action.response, deviceId: action.deviceId}
      });
    case CONSTANTS.DELETE_DEVICE_FAILURE:
      return Object.assign({}, state, {
        'deleteDeviceFailure': action.error
      });
    case CONSTANTS.EMAIL_CREDENTIALS_SUCCESS:
      return Object.assign({}, state, {
        'emailCredentialsSuccess': {response : action.response}
      });
    case CONSTANTS.EMAIL_CREDENTIALS_FAILURE:
      return Object.assign({}, state, {
        'emailCredentialsFailure': action.error
      });
    case CONSTANTS.SAVE_COMMANDS_SUCCESS:
      return Object.assign({}, state, {
        'saveCommandsSuccess': action.response
      });
    case CONSTANTS.SAVE_COMMANDS_FAILURE:
      return Object.assign({}, state, {
        'saveCommandsFailure': action.error
      });
    default:
      return initialState;
  }
}

export default addOrEditEtlDataSourceReducer;
