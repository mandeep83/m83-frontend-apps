/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * Events
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import { allSelectors as SELECTORS } from "./selectors";
import { allActions as ACTIONS } from './actions'
import reducer from "./reducer";
import saga from "./saga";
import ListingTable from "../../components/ListingTable/Loadable";
import NoDataFoundMessage from "../../components/NoDataFoundMessage/Loadable"
import AddNewButton from "../../components/AddNewButton/Loadable"
import Loader from "../../components/Loader/Loadable";
import ConfirmModel from "../../components/ConfirmModel";
import NotificationModal from '../../components/NotificationModal/Loadable';
import { cloneDeep } from "lodash";
import { isNavAssigned } from "../../commonUtils";

export const ACTION_TYPES = [
    {
        name: "sms",
        displayName: "SMS",
    },
    {
        name: "email",
        displayName: "Email",
    },
    {
        name: "rpc",
        displayName: "Controls",
    },
    {
        name: "webhook",
        displayName: "Webhook",
    },
]
/* eslint-disable react/prefer-stateless-function */
export class Events extends React.Component {
    state = {
        searchTerm: '',
        eventList: [],
        toggleView: true,
        filteredDataList: [],
        isFetching: true,
        deviceTypeList: [],
        selectedConnector: "",
        selectedGroup: '',
        totalEventCount: 0,
        remainingEventCount: 0,
        usedEventCount: 0,
        isEvaluating: false,
    }

    refreshComponent = () => {
        this.setState({
            searchTerm: '',
            eventList: [],
            filteredDataList: [],
            isFetching: true,
            deviceTypeList: [],
            selectedConnector: "",
            selectedGroup: '',
            totalEventCount: 0,
            remainingEventCount: 0,
            usedEventCount: 0,
            isEvaluating: false,
        }, () => {
            this.props.deviceTypeList()
        })
    }

    componentDidMount() {
        this.props.deviceTypeList()
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.getAllEventListSuccess) {
            let eventList = nextProps.getAllEventListSuccess.response.map((event) => {
                let requiredeviceType = this.state.deviceTypeList.find(device => device.id === event.deviceTypeId)
                return {
                    ...event,
                    deviceTypeDisplayName: requiredeviceType ? requiredeviceType.name : "-"
                }
            })
            this.setState({ eventList, filteredDataList: eventList, isFetching: false })
        }

        if (nextProps.getAllEventListFailure) {
            this.setState({
                isOpen: true,
                type: "error",
                message2: nextProps.getAllEventListFailure.error,
                isFetching: false
            })
        }

        if (nextProps.deleteEventSuccess) {
            let eventList = cloneDeep(this.state.eventList)
            eventList = eventList.filter(el => el.id !== nextProps.deleteEventSuccess.response.id)
            this.setState({
                eventList,
                filteredDataList: eventList,
                isFetching: false,
                isOpen: true,
                message2: nextProps.deleteEventSuccess.response.message,
                remainingEventCount: this.state.remainingEventCount + 1,
                usedEventCount: this.state.usedEventCount - 1,
                type: "success"
            })
        }

        if (nextProps.deleteEventFailure) {
            this.setState({
                isOpen: true,
                type: "error",
                message2: nextProps.deleteEventFailure.error,
                isFetching: false
            })
        }

        if (nextProps.deviceTypeListSuccess) {
            let deviceTypeList = nextProps.deviceTypeListSuccess.response,
                selectedConnector = this.props.match.params.id ? this.props.match.params.id : deviceTypeList.length > 0 ? deviceTypeList[0].id : '',
                selectedGroup = deviceTypeList.length > 0 ? deviceTypeList[0].deviceGroups.length > 0 ? 'ALL' : '' : '';
            this.setState({
                deviceTypeList,
                selectedGroup,
                selectedConnector,
                isFetching: deviceTypeList.length ? true : false
            }, () => {
                if (deviceTypeList.length > 0) {
                    let payload = [
                        {
                            dependingProductId: this.state.selectedConnector,
                            productName: "events"
                        }
                    ];
                    this.props.getDeveloperQuota(payload);
                    this.props.getAllEventList();
                } else {
                    let payload = [
                        {
                            dependingProductId: null,
                            productName: "events"
                        }
                    ];
                    this.props.getDeveloperQuota(payload);
                }

            })
        }

        if (nextProps.deviceTypeListFailure) {
            this.setState({
                isOpen: true,
                type: "error",
                message2: nextProps.deviceTypeListFailure.error,
            })
        }

        if (nextProps.evaluateEventSuccess) {
            let eventList = cloneDeep(this.state.eventList);
            let evaluteEventIndex = eventList.findIndex(e => e.id == nextProps.evaluateEventSuccess.response.id)
            eventList[evaluteEventIndex].evaluateEvent = !this.state.eventList[evaluteEventIndex].evaluateEvent

            eventList = eventList.map(e => {
                e.isEvaluateEventLoader = false
                return e
            })

            this.setState({
                isEvaluating: false,
                eventList,
                filteredDataList: eventList,
                evaluateEventIndex: '',
                isOpen: true,
                type: "success",
                message2: nextProps.evaluateEventSuccess.response.data.message,
            })
        }

        if (nextProps.evaluateEventFailure) {
            let eventList = cloneDeep(this.state.eventList);
            eventList = eventList.map(e => {
                e.isEvaluateEventLoader = false
                return e
            })

            this.setState({
                isOpen: true,
                type: "error",
                message2: nextProps.evaluateEventFailure.error,
                evaluateEventIndex: '',
                eventList,
                isEvaluating: false,
            })
        }
        if (nextProps.developerQuotaSuccess) {
            let totalEventCount = nextProps.developerQuotaSuccess.response.filter(product => product.productName === 'events')[0]['total'],
                remainingEventCount = nextProps.developerQuotaSuccess.response.filter(product => product.productName === 'events')[0]['remaining'],
                usedEventCount = nextProps.developerQuotaSuccess.response.filter(product => product.productName === 'events')[0]['used'];
            this.setState({
                totalEventCount,
                remainingEventCount,
                usedEventCount
            })
        }
        if (nextProps.developerQuotaFailure) {
            this.setState({
                type: "error",
                isOpen: true,
                message2: nextProps.developerQuotaFailure.error,
                isFetching: false
            })
        }

    }

    componentDidUpdate(prevProps, prevState) {
        if (Object.keys(SELECTORS).some(prop => this.props[prop])) {
            this.props.resetToInitialState()
        }
    }

    deleteEventHandler = ({ id, name }) => {
        this.setState({
            idToBeDeleted: id,
            selectedEventToBeDeleted: name,
            confirmState: true
        })
    }

    onCloseHandler = () => {
        this.setState({
            isOpen: false,
            type: '',
            message2: ''
        })
    }

    getColumns = () => {
        return [
            {
                Header: "Name", width: 15,
                cell: (row) => (
                    <React.Fragment>
                        <div className="list-view-icon"><img src="https://content.iot83.com/m83/misc/event-fallback.png" /></div>
                        <div className="list-view-icon-box">
                            <h6 className="text-theme fw-600">{row.name}</h6>
                            <p>{`${row.description || "-"}`}</p>
                        </div>
                    </React.Fragment>
                ),
                accessor: "name",
                filterable: true,
                sortable: true
            },
            {
                Header: "Criteria", width: 15,
                cell: (row) => (
                    <React.Fragment>
                        <h6>Action Every <strong className="text-orange">{row.duration}</strong> min(s)</h6>
                        <p><strong className="text-green">{row.occurrence}</strong> Consecutive Occurrence</p>
                    </React.Fragment>
                )
            },
            {
                Header: "Device Type", width: 10,
                cell: (row) => (
                    <div className="button-group-link">
                        <button className="btn btn-link text-truncate" onClick={() => { isNavAssigned('deviceTypes') && this.props.history.push(`/addOrEditDeviceType/${row.deviceTypeId}`) }}>{row.deviceTypeDisplayName}</button>
                    </div>
                ),
            },
            /*{
                Header: "Group", width: 10,
                cell: (row) => (
                    <div className="button-group-link">
                        {row.deviceGroupId.map((group, index) => {
                            let findDeviceObj = this.state.deviceTypeList.find(device => device.id == row.deviceTypeId)
                            let findGroupObj = findDeviceObj.deviceGroups.find(el => el.deviceGroupId == group)
                            return <button key={index} className="btn btn-link text-truncate" onClick={() => { this.props.history.push({ pathname: `/deviceGroups/${row.deviceTypeId}/${group}`, state: { childViewType: 'GROUP_DETAILS' } }) }}>{findGroupObj.name}</button>
                        })}
                    </div>
                )
            },*/
            {
                Header: "Event Actions", width: 15,
                cell: (row) => (
                    <div className="list-view-initial list-style-none d-flex">
                        <span className={row.actionTypes.includes("sms") ? "active" : null} data-tooltip data-tooltip-text="SMS" data-tooltip-place="bottom"><i className="fad fa-comment-lines"></i></span>
                        <span className={row.actionTypes.includes("email") ? "active" : null} data-tooltip data-tooltip-text="Email" data-tooltip-place="bottom"><i className="fad fa-envelope"></i></span>
                        <span className={row.actionTypes.includes("rpc") ? "active" : null} data-tooltip data-tooltip-text="Control" data-tooltip-place="bottom"><i className="fad fa-terminal"></i></span>
                        <span className={row.actionTypes.includes("webhook") ? "active" : null} data-tooltip data-tooltip-text="Webhook" data-tooltip-place="bottom"><i className="fad fa-code-merge"></i></span>
                    </div>
                ),
                accessor: "actionTypes",
                filterable: true,
                sortable: true,
                filter: ({ value, onChangeHandler, accessor }) =>
                    <select className="form-control" onChange={(event) => onChangeHandler(event, accessor)}>
                        <option value="">All</option>
                        {ACTION_TYPES.map(actionType =>
                            <option key={actionType.name} value={actionType.name}>{actionType.displayName}</option>
                        )}
                    </select>
            },
            { Header: "Created", width: 15, accessor: "createdAt", filterable: true, sortable: true },
            { Header: "Updated", width: 15, accessor: "updatedAt", filterable: true, sortable: true },
            {
                Header: "Actions", width: 15,
                cell: (row) => (
                    <div className="button-group">
                        {row.isEvaluateEventLoader ?
                            <button type="button" className="btn-transparent btn-transparent-green"><i className="far fa-cog fa-spin"></i></button>
                            :
                            <div className="toggle-switch-wrapper d-inline-block">
                                <label className="toggle-switch ml-0" data-tooltip data-tooltip-text={row.evaluateEvent ? "Disable Event" : "Enable Event"} data-tooltip-place="bottom">
                                    <input type="checkbox" checked={row.evaluateEvent} disabled={this.state.isEvaluating} onChange={() => this.evaluateEventChangeHandler(event, row.id)} />
                                    <span className="toggle-switch-slider"></span>
                                </label>
                            </div>
                        }
                        <button className="btn-transparent btn-transparent-cyan" data-tooltip data-tooltip-text="Preview" data-tooltip-place="bottom"
                            onClick={() => this.props.history.push(`/addOrEditEvent/${row.id}`, { isViewMode: true })}>
                            <i className="far fa-info"></i>
                        </button>
                        <button className="btn-transparent btn-transparent-blue" data-tooltip data-tooltip-text="Edit" data-tooltip-place="bottom" onClick={() => { this.props.history.push('/addOrEditEvent/' + row.id, { isViewMode: false }) }} disabled={row.evaluateEvent}><i className="far fa-pencil"></i></button>
                        <button className="btn-transparent btn-transparent-red" data-tooltip data-tooltip-text="Delete" data-tooltip-place="bottom" onClick={() => this.deleteEventHandler(row)} disabled={row.evaluateEvent}><i className="far fa-trash-alt"></i></button>
                    </div>
                )
            },
        ]
    }

    evaluateEventChangeHandler = (event, eventId) => {
        let eventList = cloneDeep(this.state.eventList)
        let eventStatePayload = {
            evaluateEvent: event.target.checked,
            eventId: eventId
        }

        let findEvaluateIndex = eventList.findIndex(el => el.id == eventId)

        eventList[findEvaluateIndex].isEvaluateEventLoader = true

        this.setState({
            isEvaluating: true,
            evaluateEventId: eventId,
            eventList,
            evaluateEventIndex: findEvaluateIndex
        }, () => this.props.evaluateEvent(eventStatePayload))
    }

    cancelClicked = () => {
        this.setState({
            confirmState: false
        })
    }

    filterChangeHandler = () => {
        let selectedGroup = this.state.selectedGroup,
            selectedConnector = this.state.selectedConnector;
        if (event.target.id === 'selectedConnector') {
            selectedConnector = event.target.value;
            selectedGroup = selectedConnector && this.state.deviceTypeList.filter(connector => connector.id === selectedConnector)[0]['deviceGroups'].length > 0 ? 'ALL' : '';
            this.setState({
                remainingEventCount: 0,
                usedEventCount: 0,
                totalEventCount: 0,
            });
            let payload = [
                {
                    dependingProductId: selectedConnector,
                    productName: "events"
                }
            ];
            this.props.getDeveloperQuota(payload);
        }
        else if (event.target.id === 'selectedGroup') {
            selectedGroup = event.target.value;
        }
        this.setState({
            selectedConnector,
            selectedGroup,
            isFilterLoader: true
        }, () => setTimeout(() => this.setState({ isFilterLoader: false }), 2000));
    }

    getFilteredEvents = () => {
        let eventList = this.state.eventList,
            selectedConnector = this.state.selectedConnector,
            selectedGroup = this.state.selectedGroup,
            filteredList = [];
        filteredList = eventList.filter(event => event.deviceTypeId === selectedConnector);
        if (selectedGroup !== 'ALL') {
            filteredList = filteredList.filter(event => event.deviceGroupId === selectedGroup);
        }

        if (!selectedConnector) {
            filteredList = this.state.eventList
        }
        return filteredList;
    }

    render() {
        let filteredDataList = this.getFilteredEvents();
        return (
            <React.Fragment>
                <Helmet>
                    <title>Events</title>
                    <meta name="description" content="Description of Events" />
                </Helmet>

                <header className="content-header d-flex">
                    <div className="flex-60">
                        <h6>Events -
                            <span className="content-header-badge-group">
                                <span className="content-header-badge-item">Allocated <span className="badge badge-pill badge-primary">{this.state.totalEventCount}</span></span>
                                <span className="content-header-badge-item">Used <span className="badge badge-pill badge-success">{this.state.usedEventCount}</span></span>
                                <span className="content-header-badge-item">Remaining <span className="badge badge-pill badge-warning">{this.state.remainingEventCount}</span></span>
                            </span>
                        </h6>
                    </div>

                    <div className="flex-40 text-right">
                        <div className="content-header-group">
                            {/*<div className="search-box">
                                <span className="search-icon"><i className="far fa-search"></i></span>
                                <input type="text" className="form-control" id="name" placeholder="Search..." value={this.state.filters.name} onChange={this.filterChangeHandler} disabled={this.state.eventList.length == 0} />
                                {Boolean(this.state.filters.name) && <button className="search-button" onClick={() => this.filterChangeHandler({ currentTarget: { id: "name", value: "" } })}><i className="far fa-times"></i></button>}
                            </div>*/}
                            <button className={this.state.toggleView == true ? "btn btn-light active" : "btn btn-light"} disabled={this.state.isFetching} data-tooltip="true" data-tooltip-text="List View" data-tooltip-place="bottom" onClick={() => this.setState({ toggleView: true })}><i className="fad fa-list-ul"></i></button>
                            <button className={this.state.toggleView == false ? "btn btn-light active" : "btn btn-light"} disabled={this.state.isFetching} data-tooltip="true" data-tooltip-text="Card View" data-tooltip-place="bottom" onClick={() => this.setState({ toggleView: false })}><i className="fad fa-table"></i></button>
                            <button className="btn btn-light" disabled={this.state.isFetching} data-tooltip data-tooltip-text="Refresh" data-tooltip-place="bottom" onClick={() => this.refreshComponent()}><i className="far fa-sync-alt"></i></button>
                            <button type="button" className="btn btn-primary" data-tooltip data-tooltip-text="Add Event" data-tooltip-place="bottom" disabled={this.state.remainingEventCount === 0 || this.state.isFetching} onClick={() => {
                                let url = this.props.match.params.type && this.props.match.params.model ? '/addOrEditEvent/' + this.props.match.params.type + "/" + this.props.match.params.model : '/addOrEditEvent';
                                this.props.history.push(url, { deviceTypeId: this.state.selectedConnector, isViewMode: false })
                            }}>
                                <i className="far fa-plus"></i>
                            </button>
                        </div>
                    </div>
                </header>

                {this.state.isFetching ?
                    <Loader /> :
                    <React.Fragment>
                        {(this.state.deviceTypeList.length > 0 && this.state.eventList.length > 0) &&
                            <div className="d-flex content-filter">
                                <div className="flex-50 form-group pd-r-10">
                                    <div className="input-group">
                                        <div className="input-group-prepend">
                                            <span className="input-group-text">Device Type :</span>
                                        </div>
                                        <select className="form-control" id="selectedConnector" value={this.state.selectedConnector} onChange={this.filterChangeHandler}>
                                            {this.state.deviceTypeList.map(connector =>
                                                <option key={connector.id} value={connector.id}>{connector.name}</option>
                                            )}
                                        </select>
                                    </div>
                                </div>
                                {/*<div className="flex-50 form-group pd-l-10">
                                    <div className="input-group">
                                        <div className="input-group-prepend">
                                            <span className="input-group-text">Device Group :</span>
                                        </div>
                                        <select className="form-control" id="selectedGroup" value={this.state.selectedGroup} onChange={this.filterChangeHandler}>
                                            {this.state.deviceTypeList.filter(connector => connector.id === this.state.deviceTypeList[0].id)[0]['deviceGroups'].length > 0 ?
                                                <option value='ALL'>All</option> :
                                                <option value=''>No Groups Available</option>
                                            }
                                            {this.state.selectedConnector && this.state.deviceTypeList.filter(connector => connector.id === this.state.selectedConnector)[0]['deviceGroups'].map(group =>
                                                <option key={group.deviceGroupId} value={group.deviceGroupId}>{group.name}</option>
                                            )}
                                        </select>
                                    </div>
                                </div>*/}
                            </div>
                        }
                        <div className={`content-body ${(this.state.deviceTypeList.length > 0 && this.state.eventList.length > 0) && "content-filter-body"}`}>
                            {this.state.deviceTypeList.length > 0 ?
                                this.state.eventList.length > 0 ?
                                    filteredDataList.length > 0 ?
                                        this.state.isFilterLoader ?
                                            <div className="inner-loader-wrapper h-100 mr-t-25">
                                                <div className="inner-loader-content">
                                                    <i className="fad fa-sync-alt fa-spin f-20"></i>
                                                </div>
                                            </div>
                                            :
                                            this.state.toggleView ?
                                                <ListingTable
                                                    columns={this.getColumns()}
                                                    data={filteredDataList}
                                                />
                                                :
                                                <ul className="card-view-list">
                                                    {filteredDataList.map((item, index) =>
                                                        <li key={index}>
                                                            <div className="card-view-box">
                                                                <div className="card-view-header">
                                                                    <div className="card-view-initial list-style-none d-flex">
                                                                        <span className={item.actionTypes.includes("sms") ? "active" : null} data-tooltip data-tooltip-text="SMS" data-tooltip-place="bottom"><i className="fad fa-comment-lines"></i></span>
                                                                        <span className={item.actionTypes.includes("email") ? "active" : null} data-tooltip data-tooltip-text="Email" data-tooltip-place="bottom"><i className="fad fa-envelope"></i></span>
                                                                        <span className={item.actionTypes.includes("rpc") ? "active" : null} data-tooltip data-tooltip-text="Control" data-tooltip-place="bottom"><i className="fad fa-terminal"></i></span>
                                                                        <span className={item.actionTypes.includes("webhook") ? "active" : null} data-tooltip data-tooltip-text="Webhook" data-tooltip-place="bottom"><i className="fad fa-code-merge"></i></span>
                                                                    </div>
                                                                    <div className="dropdown">
                                                                        <button className="btn-transparent btn-transparent-gray" type="button" data-toggle="dropdown">
                                                                            <i className="fas fa-ellipsis-v"></i>
                                                                        </button>
                                                                        <div className="dropdown-menu">
                                                                            {item.isEvaluateEventLoader ?
                                                                                <button type="button" className="dropdown-item"><i className="far fa-cog fa-spin"></i></button>
                                                                                :
                                                                                <div className="toggle-switch-wrapper d-inline-block">
                                                                                    <label className="toggle-switch ml-0" >
                                                                                        <input type="checkbox" checked={item.evaluateEvent} disabled={this.state.isEvaluating} onChange={() => this.evaluateEventChangeHandler(event, item.id)} />
                                                                                        <span className="toggle-switch-slider"></span>
                                                                                    </label>
                                                                                    <span className="f-12">{item.evaluateEvent ? "Disable" : "Enable"}</span>
                                                                                </div>
                                                                            }
                                                                            <button className="dropdown-item" 
                                                                                onClick={() => this.props.history.push(`/addOrEditEvent/${item.id}`, { isViewMode: true })}>
                                                                                <i className="far fa-info"></i>Preview
                                                                            </button>
                                                                            <button className="dropdown-item" onClick={() => { this.props.history.push('/addOrEditEvent/' + item.id, { isViewMode: false }) }} disabled={item.evaluateEvent}><i className="far fa-pencil"></i>Edit</button>
                                                                            <button className="dropdown-item" onClick={() => this.deleteEventHandler(item)} disabled={item.evaluateEvent}><i className="far fa-trash-alt"></i>Delete</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div className="card-view-body">
                                                                    <div className="card-view-icon">
                                                                        <img src="https://content.iot83.com/m83/misc/event-fallback.png" />
                                                                    </div>
                                                                    <h6><i className="far fa-address-book"></i>{item.name ? item.name : "N/A"}</h6>
                                                                    <p><i className="far fa-file-alt"></i>{item.description ? item.description : "N/A"}</p>
                                                                    <p><i className="far fa-history"></i>Action Every <strong className="text-orange fw-600">{item.duration}</strong> min(s)</p>
                                                                    <p><i className="far fa-repeat-alt"></i><strong className="text-green fw-600">{item.occurrence}</strong> Consecutive Occurrence</p>
                                                                    <p className="text-primary text-underline-hover cursor-pointer" onClick={() => { isNavAssigned('deviceTypes') && this.props.history.push(`/addOrEditDeviceType/${item.deviceTypeId}`) }}>
                                                                        <i className="far fa-microchip"></i>{item.deviceTypeDisplayName}
                                                                    </p>
                                                                </div>
                                                                <div className="card-view-footer d-flex">
                                                                    <div className="flex-50 p-3">
                                                                        <h6>Created</h6>
                                                                        <p><strong>By - </strong>{item.createdBy}</p>
                                                                        <p><strong>At - </strong>{new Date(item.createdAt).toLocaleString('en-US', { createdAt: localStorage.createdAt })}</p>
                                                                    </div>
                                                                    <div className="flex-50 p-3">
                                                                        <h6>Updated</h6>
                                                                        <p><strong>By - </strong>{item.updatedBy}</p>
                                                                        <p><strong>At - </strong>{new Date(item.updatedAt).toLocaleString('en-US', { createdAt: localStorage.createdAt })}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    )}
                                                </ul>
                                        :
                                        <NoDataFoundMessage />
                                    :
                                    <AddNewButton
                                        text1="No event(s) available"
                                        text2="You haven't created any event(s) yet"
                                        imageIcon="addEvent.png"
                                        createItemOnAddButtonClick={() => {
                                            let url = this.props.match.params.type && this.props.match.params.model ? '/addOrEditEvent/' + this.props.match.params.type + "/" + this.props.match.params.model : '/addOrEditEvent';
                                            this.props.history.push(url, { isViewMode: false })
                                        }}
                                    />
                                :
                                <AddNewButton
                                    text1="No event(s) available"
                                    text2="You haven't created any device type(s) yet. Please create a device type first."
                                    addButtonEnable={isNavAssigned("deviceTypes")}
                                    createItemOnAddButtonClick={() => this.props.history.push(`/addOrEditDeviceType`)}
                                    imageIcon="addEvent.png"
                                />
                            }
                        </div>
                    </React.Fragment>
                }

                {
                    this.state.confirmState &&
                    <ConfirmModel
                        status={"delete"}
                        delete={""}
                        deleteName={this.state.selectedEventToBeDeleted}
                        confirmClicked={() => {
                            this.props.deleteEvent(this.state.idToBeDeleted)
                            this.setState({
                                isFetching: true,
                                confirmState: false,
                                isOpen: false
                            })
                        }}
                        cancelClicked={() => {
                            this.cancelClicked()
                        }}
                    />
                }

                {
                    this.state.isOpen &&
                    <NotificationModal
                        type={this.state.type}
                        message2={this.state.message2}
                        onCloseHandler={this.onCloseHandler}
                    />
                }
            </React.Fragment >
        );
    }
}

Events.propTypes = {
    dispatch: PropTypes.func.isRequired
};

let allSelectors = {}
Object.entries(SELECTORS).map(([key, value]) => {
    allSelectors[key] = value()
})

const mapStateToProps = createStructuredSelector(allSelectors);


function mapDispatchToProps(dispatch) {
    let allActions = { dispatch }
    Object.entries(ACTIONS).map(([key, value]) => {
        allActions[key] = (...args) => dispatch(value(...args))
    })
    return allActions;
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "events", reducer });
const withSaga = injectSaga({ key: "events", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(Events);
