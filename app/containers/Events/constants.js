/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/*
 *
 * Events constants
 *
 */

export const RESET_TO_INITIAL_STATE = "app/Events/RESET_TO_INITIAL_STATE";


export const GET_ALL_EVENT_LIST = {
	action: 'app/Events/GET_ALL_EVENT_LIST',
	success: "app/Events/GET_ALL_EVENT_LIST_SUCCESS",
	failure: "app/Events/GET_ALL_EVENT_LIST_FAILURE",
	urlKey: "getEventList",
	successKey: "getAllEventListSuccess",
	failureKey: "getAllEventListFailure",
	actionName: "getAllEventList",
	actionArguments: []
}

export const DELETE_EVENT = {
	action: 'app/Events/DELETE_EVENT',
	success: "app/Events/DELETE_EVENT_SUCCESS",
	failure: "app/Events/DELETE_EVENT_FAILURE",
	urlKey: "deleteEvent",
	successKey: "deleteEventSuccess",
	failureKey: "deleteEventFailure",
	actionName: "deleteEvent",
	actionArguments: ["id"]
}

export const GET_DEVICE_TYPE_LIST = {
	action: 'app/Events/GET_DEVICE_TYPE_LIST',
	success: "app/Events/GET_DEVICE_TYPE_LIST_SUCCESS",
	failure: "app/Events/GET_DEVICE_TYPE_LIST_FAILURE",
	urlKey: "deviceTypeList",
	successKey: "deviceTypeListSuccess",
	failureKey: "deviceTypeListFailure",
	actionName: "deviceTypeList",
	actionArguments: []
}

export const EVALUATE_EVENT = {
	action: 'app/Events/EVALUATE_EVENT',
	success: "app/Events/EVALUATE_EVENT_SUCCESS",
	failure: "app/Events/EVALUATE_EVENT_FAILURE",
	urlKey: "isEvaluateEvent",
	successKey: "evaluateEventSuccess",
	failureKey: "evaluateEventFailure",
	actionName: "evaluateEvent",
	actionArguments: ["payload"]
}


export const DEVELOPER_QUOTA = {
	action: 'app/Events/GET_DEVELOPER_QUOTA',
	success: "app/Events/GET_DEVELOPER_QUOTA_SUCCESS",
	failure: "app/Events/GET_DEVELOPER_QUOTA_FAILURE",
	urlKey: "getDeveloperQuota",
	successKey: "developerQuotaSuccess",
	failureKey: "developerQuotaFailure",
	actionName: "getDeveloperQuota",
	actionArguments: ["payload"]
}


