/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { fromJS } from "immutable";
import {DEFAULT_ACTION, 
    GOT_LIST, NOT_GOT_LIST,
    GET_INSTANCES_FAILURE,GET_INSTANCES_SUCCESS,
    GOT_DEVICES, NOT_GOT_DEVICES} from "./constants";

export const initialState = fromJS({});

function viewAlarmsAndAlertsReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
        return state;
    case GOT_LIST:
        return Object.assign({}, state, {
            anomalyList: action.response
        });
    case NOT_GOT_LIST:
        return Object.assign({}, state, {
            anomalyListError: action.error
        });
    case GET_INSTANCES_SUCCESS:
      return Object.assign({}, state, {
        instances: action.response
      });
    case GET_INSTANCES_FAILURE:
      return Object.assign({}, state, {
        instancesError: action.error
      });
    case GOT_DEVICES:
        return Object.assign({}, state, {
            deviceTypes: action.response
        });
    case NOT_GOT_DEVICES:
        return Object.assign({}, state, {
            deviceTypesError: action.error
        });
    default:
        return state;
  }
}

export default viewAlarmsAndAlertsReducer;
