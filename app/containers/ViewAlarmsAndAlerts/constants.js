/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

export const DEFAULT_ACTION = "app/ViewAlarmsAndAlerts/DEFAULT_ACTION";

export const GET_LIST = 'app/ViewAlarmsAndAlerts/GET_TICKET_LIST';
export const GOT_LIST = 'app/ViewAlarmsAndAlerts/GOT_TICKET_LIST';
export const NOT_GOT_LIST = 'app/ViewAlarmsAndAlerts/NOT_GOT_TICKET_LIST';

export const GET_DEVICES = 'app/ViewAlarmsAndAlerts/GET_DEVICES_LIST';
export const GOT_DEVICES = 'app/ViewAlarmsAndAlerts/GOT_DEVICES_LIST';
export const NOT_GOT_DEVICES = 'app/ViewAlarmsAndAlerts/NOT_GOT_DEVICES_LIST';

export const GET_INSTANCES_REQUEST = "app/ViewAlarmsAndAlerts/GET_INSTANCES_REQUEST";
export const GET_INSTANCES_SUCCESS = "app/ViewAlarmsAndAlerts/GET_INSTANCES_SUCCESS";
export const GET_INSTANCES_FAILURE = "app/ViewAlarmsAndAlerts/GET_INSTANCES_FAILURE";
