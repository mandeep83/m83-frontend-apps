/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import {
    getIsFetching,
    getAnomalyListSuccess, getAnomalyListError,
    getDevicesTypesSuccess, getdeviceTypeListError, getInstancesSuccess,
    getInstancesError
} from "./selectors";

import {
    getAnomalyList, fetchDeviceTypes, getinstances
} from "./actions";
import reducer from "./reducer";
import saga from "./saga";
import Loader from '../../components/Loader/Loadable'
import NotificationModal from '../../components/NotificationModal/Loadable'

/* eslint-disable react/prefer-stateless-function */
export class ViewAlarmsAndAlerts extends React.Component {
    state = {
        filteredList: [],
        deleteItemId: '',
        confirmState: false,
        deviceTypes: [],
        instances: [],
        showAlarmList: true,
        selectedFilters: {
            deviceType: "",
            instance: "All",
            entities: "All",
            type: "All",
            severity: "All",
        },
        isOpen: false,
        anomalyList: ""
    }

    componentWillMount() {
        this.props.fetchDeviceTypes(true);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.anomalyList !== this.props.anomalyList) {
            this.setState({
                anomalyList: nextProps.anomalyList
            })
        }

        if (nextProps.anomalyListError !== this.props.anomalyListError) {
            this.setState({
                message2: nextProps.anomalyListError,
                isOpen: true,
                type: 'error'
            })
        }

        if (nextProps.deviceTypes !== this.props.deviceTypes) {

            let selectedFilters = JSON.parse(JSON.stringify(this.state.selectedFilters))
            selectedFilters.deviceType = nextProps.deviceTypes[0].type
            this.props.getinstances(nextProps.deviceTypes[0].type)

            this.setState({
                deviceTypes: nextProps.deviceTypes,
                selectedFilters
            }, () => this.props.getAnomalyList(this.state.selectedFilters))
        }

        if (nextProps.deviceTypesError !== this.props.deviceTypesError) {
            this.setState({
                message2: nextProps.deviceTypesError,
                isOpen: true,
                type: 'error'
            })
        }

        if (nextProps.instances !== this.props.instances) {
            this.setState({
                instances: nextProps.instances,
            })
        }

        if (nextProps.instancesError != this.props.instancesError) {
            this.setState({
                modaltype: "error",
                openModal: true,
                message2: nextProps.instancesError
            })
        }
    }

    filterHandler = (event) => {
        let selectedFilters = JSON.parse(JSON.stringify(this.state.selectedFilters))

        if (event.target.id === "deviceType") {
            this.props.getinstances(event.target.value);
            selectedFilters = {
                deviceType: event.target.value,
                instance: "All",
                entities: "All",
                type: "All",
                severity: "All",
            }
        }
        else {
            selectedFilters[event.target.id] = event.target.value
        }

        this.props.getAnomalyList(selectedFilters);
        this.setState({ selectedFilters })
    }

    onCloseHandler = () => {
        this.setState({
            message2: '',
            isOpen: false,
            type: ''
        })
    }

    getClassName(anomaly) {
        let className = ""
        if (anomaly.type === "alert") {
            className = className + "alert";
            if (anomaly.severity === "major") {
                className = className + " highAlertBorder";
            } else {
                className = className + " lowAlertBorder";
            }
        } else {
            className = className + "alarm";
            if (anomaly.severity === "major") {
                className = className + " highAlarmBorder";
            } else {
                className = className + " lowAlarmBorder";
            }
        }
        return className;
    }
    getIconClassName(anomaly) {
        let className = ""
        if (anomaly.type === "alert") {
            if (anomaly.severity === "major") {
                className = "far fa-exclamation-triangle";
            } else {
                className = "far fa-exclamation-triangle";
            }
        } else {
            if (anomaly.severity === "major") {
                className = "far fa-exclamation-triangle";
            } else {
                className = "far fa-bell";
            }
        }
        return className;
    }

    render() {
        return (
            <div>
                <Helmet>
                    <title>Alarms & Alerts</title>
                    <meta name="description" content="M83-ViewAlarmsAndAlerts" />
                </Helmet>

                <div className="pageBreadcrumb">
                    <div className="row">
                        <div className="col-8">
                            <p><span>All Available Alarms & Alerts</span></p>
                        </div>

                        <div className="col-4 text-right">
                            <div className="flex h-100 justify-content-end align-items-center">
                            </div>
                        </div>
                    </div>
                </div>

                <div className="outerBox">
                    <div className="filterBox">
                        <ul>
                            <li>
                                <div className="form-group">
                                    <select name="deviceType" id="deviceType" className="form-control" required value={this.state.selectedFilters.deviceType}
                                        onChange={this.filterHandler}>
                                        {this.state.deviceTypes.map((type, index) => {
                                            return (
                                                <option key={index} value={type.type}>{type.name}</option>)
                                        })
                                        }
                                    </select>
                                    <label className="form-group-label">Device Type :</label>
                                </div>
                            </li>
                            <li>
                                <div className="form-group">
                                    <select name="instance" id="instance" className="form-control" required value={this.state.selectedFilters.instance}
                                        onChange={this.filterHandler} disabled={this.state.selectedFilters.deviceType ? false : true}>
                                        <option key="ALL" value="ALL">All</option>
                                        {this.state.instances.map(el => el.instancesData.map((arr, index) => {
                                            return (
                                                <option key={index} value={arr.id}>{arr.id}</option>)
                                        }))
                                        }
                                    </select>
                                    <label className="form-group-label">Instances :</label>
                                </div>
                            </li>
                            <li>
                                <div className="form-group">
                                    <select name="entities" id="entities" className="form-control" required value={this.state.selectedFilters.entities}
                                        onChange={this.filterHandler} disabled={this.state.selectedFilters.deviceType ? false : true}>
                                        <option value="ALL">All</option>
                                        {this.state.instances.map(el => el.entities.map((arr) => {
                                            if (arr.displayName != "id") return (
                                                <option key={arr.path} value={arr.path}>{arr.displayName}</option>)
                                        }))
                                        }
                                    </select>
                                    <label className="form-group-label">Entities :</label>
                                </div>
                            </li>
                            <li>
                                <div className="form-group">
                                    <select name="alarmalert" id="type" className="form-control" required value={this.state.selectedFilters.type}
                                        onChange={this.filterHandler} disabled={this.state.selectedFilters.deviceType ? false : true}>
                                        <option value="ALL">All</option>
                                        <option value="Alarm">Alarm</option>
                                        <option value="Alert">Alert</option>
                                    </select>
                                    <label className="form-group-label">Alarm/Alert :</label>
                                </div>
                            </li>
                            <li>
                                <div className="form-group">
                                    <select name="severity" id="severity" className="form-control" required value={this.state.selectedFilters.severity}
                                        onChange={this.filterHandler} disabled={this.state.selectedFilters.deviceType ? false : true}>
                                        <option value="ALL">All</option>
                                        <option value="High">High</option>
                                        <option value="Low">Low</option>
                                    </select>
                                    <label className="form-group-label">Severity :</label>
                                </div>
                            </li>
                        </ul>
                    </div>


                    {this.props.isFetching ? <Loader /> :
                        <div className="card-body graphBoxBody alertListBox">
                            <ul className="alertList">
                                {this.state.anomalyList.length > 0 && this.state.anomalyList.map(anomaly => <li key={anomaly.id} className={this.getClassName(anomaly)}>
                                    <div className="alertListIcon">
                                        <i className={this.getIconClassName(anomaly)}></i>
                                    </div>
                                    <h4>
                                        <strong>{anomaly.title}</strong>
                                        <strong className="text-info float-right f-11">{new Date(anomaly.timestamp).toLocaleDateString()}</strong>
                                    </h4>
                                    <h5>
                                        <strong className="text-dark">Entity :</strong>
                                        <span className="ml-2">{anomaly.entity}</span>
                                        <strong className="text-dark">Low Alert :</strong>
                                        <span className="ml-2">{anomaly.minAlert}</span>
                                        <strong className="text-dark">High Alert :</strong>
                                        <span className="ml-2">{anomaly.maxAlert}</span>
                                        <strong className="text-dark">Low Alarm :</strong>
                                        <span className="ml-2">{anomaly.minAlarm}</span>
                                        <strong className="text-dark">High Alarm :</strong>
                                        <span className="ml-2">{anomaly.maxAlarm}</span>
                                        <strong className="text-dark">Min Range :</strong>
                                        <span className="ml-2">{anomaly.minRange}</span>
                                        <strong className="text-dark">Max Range :</strong>
                                        <span className="ml-2">{anomaly.maxRange}</span>
                                    </h5>
                                </li>)}
                            </ul>
                        </div>
                    }

                </div>
                {this.state.isOpen &&
                    <NotificationModal
                        type={this.state.type}
                        message2={this.state.message2}
                        onCloseHandler={this.onCloseHandler}
                    />
                }
            </div>
        );
    }
}

ViewAlarmsAndAlerts.propTypes = {
    dispatch: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
    isFetching: getIsFetching(),
    anomalyList: getAnomalyListSuccess(),
    anomalyListError: getAnomalyListError(),
    deviceTypes: getDevicesTypesSuccess(),
    deviceTypesError: getdeviceTypeListError(),
    instances: getInstancesSuccess(),
    instancesError: getInstancesError(),
});

function mapDispatchToProps(dispatch) {
    return {
        dispatch,
        fetchDeviceTypes: (isDeployed) => dispatch(fetchDeviceTypes(isDeployed)),
        getinstances: (deviceType) => dispatch(getinstances(deviceType)),
        getAnomalyList: (payload) => dispatch(getAnomalyList(payload)),
    };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "viewAlarmsAndAlerts", reducer });
const withSaga = injectSaga({ key: "viewAlarmsAndAlerts", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(ViewAlarmsAndAlerts);
