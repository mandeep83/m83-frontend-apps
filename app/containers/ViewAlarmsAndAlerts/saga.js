/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { takeEvery } from 'redux-saga';
import {
    GET_LIST, GOT_LIST, NOT_GOT_LIST,
    GOT_DEVICES, GET_DEVICES, NOT_GOT_DEVICES,GET_INSTANCES_SUCCESS,GET_INSTANCES_FAILURE,GET_INSTANCES_REQUEST
} from './constants';
import {apiCallHandler} from '../../api';

export function* getListApiHandlerAsync(action) {
    yield[apiCallHandler(action, GOT_LIST, NOT_GOT_LIST, 'getAlarmsAndAlert')];
}

export function* fetchDevicesApiHandlerAsync(action) {
    yield[apiCallHandler(action, GOT_DEVICES, NOT_GOT_DEVICES, 'getDeviceType', false)];
}

export function* getInstancesApiHandlerAsync(action) {
    yield [apiCallHandler(action, GET_INSTANCES_SUCCESS, GET_INSTANCES_FAILURE, 'getInstanceAndEntities')];
}

export function* watcherGetListRequest() {
    yield takeEvery(GET_LIST, getListApiHandlerAsync);
}

export function* watcherFetchAllDevices() {
    yield takeEvery(GET_DEVICES, fetchDevicesApiHandlerAsync);
}

export function* watcherGetInstancesRequest() {
    yield takeEvery(GET_INSTANCES_REQUEST, getInstancesApiHandlerAsync);
}


export default function* rootSaga() {
    yield [
        watcherGetListRequest(),
        watcherFetchAllDevices(),
        watcherGetInstancesRequest(),
    ];
}
