/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
 * Direct selector to the viewAlarmsAndAlerts state domain
 */

const selectViewAlarmsAndAlertsDomain = state =>
  state.get("viewAlarmsAndAlerts", initialState);

  export const getAnomalyListSuccess = () =>
      createSelector(selectViewAlarmsAndAlertsDomain, subState => subState.anomalyList);
  
  export const getAnomalyListError = () =>
      createSelector(selectViewAlarmsAndAlertsDomain, subState => subState.anomalyListError);
  
  export const getDevicesTypesSuccess = () =>
      createSelector(selectViewAlarmsAndAlertsDomain, subState => subState.deviceTypes);
  
  export const getdeviceTypeListError = () =>
      createSelector(selectViewAlarmsAndAlertsDomain, subState => subState.deviceTypesError);
  
  export   const getInstancesSuccess = () =>
      createSelector(selectViewAlarmsAndAlertsDomain, substate => substate.instances);
    
  export   const getInstancesError = () =>
      createSelector( selectViewAlarmsAndAlertsDomain, substate => substate.instancesError);
  
  export const isFetchingState = state => state.get('loader');
  
  export const getIsFetching = () =>
      createSelector(isFetchingState, substate => substate.get('isFetching'));
