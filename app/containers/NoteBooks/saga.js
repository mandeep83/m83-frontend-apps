/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

// import { take, call, put, select } from 'redux-saga/effects';

// Individual exports for testing
import { takeEvery } from 'redux-saga';
import * as CONSTANTS from './constants';
import { apiCallHandler } from '../../api';

export function* getAllNoteBooksHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_ALL_NOTEBOOK_LIST_SUCCESS, CONSTANTS.GET_ALL_NOTEBOOK_LIST_FAILURE, 'getAllNoteBooks')];
}
export function* deleteNoteBookHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.DELETE_NOTEBOOK_SUCCESS, CONSTANTS.DELETE_NOTEBOOK_FAILURE, 'deleteNoteBook')];
}
export function*  watcherGetAllNoteBooks() {
  yield takeEvery(CONSTANTS.GET_ALL_NOTEBOOK_LIST, getAllNoteBooksHandlerAsync);
}

export function*  watcherdeleteNoteBook() {
  yield takeEvery(CONSTANTS.DELETE_NOTEBOOK, deleteNoteBookHandlerAsync);
}


export default function* rootSaga() {
  // See example in containers/HomePage/saga.js
  yield[
    watcherGetAllNoteBooks(),
    watcherdeleteNoteBook(),
  ]
}
