/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/*
 *
 * NoteBooks constants
 *
 */

export const DEFAULT_ACTION = "app/NoteBooks/DEFAULT_ACTION";

export const GET_ALL_NOTEBOOK_LIST = 'app/NoteBooks/GET_ALL_NOTEBOOK_LIST';
export const  GET_ALL_NOTEBOOK_LIST_SUCCESS = 'app/NoteBooks/GET_ALL_NOTEBOOK_LIST_SUCCESS';
export const  GET_ALL_NOTEBOOK_LIST_FAILURE = 'app/NoteBooks/GET_ALL_NOTEBOOK_LIST_FAILURE';

export const DELETE_NOTEBOOK = 'app/NoteBooks/DELETE_NOTEBOOK';
export const  DELETE_NOTEBOOK_SUCCESS = 'app/NoteBooks/DELETE_NOTEBOOK_SUCCESS';
export const  DELETE_NOTEBOOK_FAILURE = 'app/NoteBooks/DELETE_NOTEBOOK_FAILURE';
