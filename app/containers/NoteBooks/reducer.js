/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/*
 *
 * NoteBooks reducer
 *
 */

import { fromJS } from "immutable";
import * as CONSTANTS from './constants';

export const initialState = fromJS({});

function noteBooksReducer(state = initialState, action) {
  switch (action.type) {
    case CONSTANTS.DEFAULT_ACTION:
      return state;
    
    case CONSTANTS.GET_ALL_NOTEBOOK_LIST_SUCCESS:
      return Object.assign({}, state, {
        notebookslistSuccess: action.response
      });

    case CONSTANTS.GET_ALL_NOTEBOOK_LIST_FAILURE:
      return Object.assign({}, state, {
        notebookslistFailure: { message: action.error, timestamp: Date.now() }
      });

    case CONSTANTS.DELETE_NOTEBOOK_SUCCESS:
      return Object.assign({}, state, {
        notebookDeleteSuccess: action.response
      });

    case CONSTANTS.DELETE_NOTEBOOK_FAILURE:
      return Object.assign({}, state, {
        notebookDeleteFailure: { message: action.error, timestamp: Date.now() }
      });  
    
    default:
      return state;
  }
}

export default noteBooksReducer;
