/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * NoteBooks
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { FormattedMessage } from "react-intl";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import { cloneDeep } from "lodash";
import { convertTimestampToDate } from '../../commonUtils';
import * as ACTIONS from './actions';
import * as SELECTORS from './selectors';
import Loader from "../../components/Loader";
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
//import makeSelectNoteBooks from "./selectors";
import NotificationModal from '../../components/NotificationModal/Loadable';
import ListingTable from "../../components/ListingTable/Loadable";
import ConfirmModel from "../../components/ConfirmModel";
import reducer from "./reducer";
import saga from "./saga";
import messages from "./messages";
import QuotaHeader from "../../components/QuotaHeader";
import AddNewButton from "../../components/AddNewButton/Loadable";
import Skeleton from "react-loading-skeleton";

/* eslint-disable react/prefer-stateless-function */
export class NoteBooks extends React.Component {

    state = {
        isFetching: true,
        allnotebooksList: [],
        filteredDataList: [],
        searchTerm: '',
        toggleView: false,
    }

    refreshComponent = () => {
        this.setState({
            isFetching: true,
            theme: true,
            filteredDataList: [],
            searchTerm: '',
        }, () => {
            this.props.getAllNoteBooks()
        })
    }

    deleteNoteBookHandler = (noteBookId) => {
        this.props.deleteNoteBook(noteBookId)
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.notebooksListSuccess && nextProps.notebooksListSuccess !== this.props.notebooksListSuccess) {
            this.setState({
                allnotebooksList: nextProps.notebooksListSuccess,
                filteredDataList: nextProps.notebooksListSuccess,
                isFetching: false
            })
        }
        if (nextProps.notebooksListFailure && nextProps.notebooksListFailure !== this.props.notebooksListFailure) {
            this.setState({
                isFetching: false,
                modalType: "error",
                isOpen: true,
                message2: nextProps.notebooksListFailure.message,
            })
        }
        if (nextProps.notebookDeleteSuccess && nextProps.notebookDeleteSuccess !== this.props.notebookDeleteSuccess) {
            this.setState((previousState) => ({
                isFetching: true,
                modalType: "success",
                isOpen: true,
                message2: nextProps.notebookDeleteSuccess.message,
                isDeletedSuccess: !previousState.isDeletedSuccess
            }), () => {
                this.props.getAllNoteBooks()
            })
        }
        if (nextProps.notebookDeleteFailure && nextProps.notebookDeleteFailure !== this.props.notebookDeleteFailure) {
            this.setState({
                isFetching: false,
                modalType: "error",
                isOpen: true,
                message2: nextProps.notebookDeleteFailure.message,
            })
        }
    }

    onCloseHandler = () => {
        this.setState({
            isOpen: false,
            message2: '',
            modalType: '',
        })
    }

    toggleView = () => {
        this.setState((prevState) => ({ toggleView: !prevState.toggleView }))
    }

    cancelClicked = () => {
        this.setState({
            notebooktobeDeleted: null,
            confirmState: false
        })
    }

    onSearchHandler = (value) => {
        let filterList = cloneDeep(this.state.allnotebooksList);
        if (value) {
            filterList = filterList.filter(item => item.notebookName.toLowerCase().includes(value.toLowerCase()))
        }
        this.setState({
            filteredDataList: filterList,
            searchTerm: value
        })
    }

    getColumns = () => {
        return [
            {
                Header: "Name", width: 30, accessor: "notebookName",
                cell: row => (
                    <React.Fragment>
                        <div className="list-view-icon"><i className="fad fa-book text-dark-theme"></i></div>
                        <div className="list-view-icon-box">
                            <h6 className="text-theme fw-600">{row.notebookName}</h6>
                            <p>{`${row.description || "-"}`}</p>
                        </div>
                    </React.Fragment>
                ),
                filterable: true,
                sortable: true
            },
            {
                Header: "Type", width: 20, cell: row => (
                    <span className="badge badge-pill alert-success list-view-pill">ETL Notebook</span>
                )
            },
            {
                Header: "Code Blocks", width: 10, cell: row => (
                    <span className="alert alert-primary list-view-badge">{row.codeBlock}</span>
                )
            },
            {
                Header: "Created", width: 15, accessor: "createdAt", sortable: true, filterable: true, cell: row => (
                    <React.Fragment>
                        <h6>{row.createdBy}</h6><p>{convertTimestampToDate(row.createdAt)}</p>
                    </React.Fragment>
                )
            },
            {
                Header: "Updated", width: 15, accessor: "updatedAt", sortable: true, filterable: true, cell: row => (
                    <React.Fragment>
                        <h6>{row.updatedBy}</h6><p>{convertTimestampToDate(row.updatedAt)}</p>
                    </React.Fragment>
                )
            },
            {
                Header: "Actions", width: 10, cell: row => (
                    <div className="button-group">
                        <button className="btn-transparent btn-transparent-blue" data-tooltip data-tooltip-text="Edit" data-tooltip-place="bottom" onClick={() => this.props.history.push('/addOrEditNoteBook/' + row.notebookId)}>
                            <i className="far fa-pencil"></i>
                        </button>
                        <button className="btn-transparent btn-transparent-red" data-tooltip data-tooltip-text="Delete" data-tooltip-place="bottom" onClick={() => this.setState({ confirmState: true, selectedNoteBookToBeDeleted: row.notebookName, notebooktobeDeleted: row.notebookId })}>
                            <i className="far fa-trash-alt"></i>
                        </button>
                    </div>
                )
            },
        ]
    }

    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>NoteBooks</title>
                    <meta name="description" content="Description of NoteBooks" />
                </Helmet>

                <QuotaHeader
                    showQuota
                    addButtonText="Add Notebook"
                    componentName="Notebooks"
                    productName="notebook"
                    onAddClick={() => this.props.history.push(`/addOrEditNoteBook`)}
                    searchBoxDetails={{
                        value: this.state.searchTerm,
                        onChangeHandler: this.onSearchHandler,
                        isDisabled: !this.state.allnotebooksList.length || this.state.isFetching
                    }}
                    refreshDisabled={this.state.isFetching}
                    addDisabled={this.state.isFetching}
                    refreshHandler={this.refreshComponent}
                    toggleButton={this.toggleView}
                    callBack={() => this.props.getAllNoteBooks()}
                    isDeletedSuccess={this.state.isDeletedSuccess}
                    {...this.props}
                />

                <div className="content-body">
                    {this.state.isFetching ?
                        <Skeleton count={9} className="skeleton-list-loader" /> :
                        <React.Fragment>
                            {this.state.allnotebooksList && this.state.allnotebooksList.length > 0 ?
                                !this.state.toggleView ?
                                    <ListingTable
                                        columns={this.getColumns()}
                                        data={this.state.filteredDataList}
                                    />
                                    :
                                    <ul className="card-view-list">
                                        {this.state.allnotebooksList.map((item, index) =>
                                            <li key={index}>
                                                <div className="card-view-box">
                                                    <div className="card-view-header">
                                                        <span className="alert alert-primary">Code Blocks - {item.codeBlock}</span>
                                                        <div className="dropdown">
                                                            <button className="btn-transparent btn-transparent-gray" type="button" data-toggle="dropdown">
                                                                <i className="fas fa-ellipsis-v"></i>
                                                            </button>
                                                            <div className="dropdown-menu">
                                                                <button className="dropdown-item" onClick={() => this.props.history.push('/addOrEditNoteBook/' + item.notebookId)}>
                                                                    <i className="far fa-pencil"></i>Edit
                                                                </button>
                                                                <button className="dropdown-item" onClick={() => this.setState({ confirmState: true, selectedNoteBookToBeDeleted: item.notebookName, notebooktobeDeleted: item.notebookId })}>
                                                                    <i className="far fa-trash-alt"></i>Delete
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="card-view-body">
                                                        <div className="card-view-icon">
                                                            <i className="fad fa-book text-dark-theme"></i>
                                                        </div>
                                                        <h6><i className="far fa-address-book"></i>{item.notebookName ? item.notebookName : "-"}</h6>
                                                        <p><i className="far fa-file-alt"></i>{item.description ? item.description : "-"}</p>
                                                        <p><span className="badge badge-pill alert-success">ETL Notebook</span></p>
                                                    </div>
                                                    <div className="card-view-footer d-flex">
                                                        <div className="flex-50 p-3">
                                                            <h6>Created</h6>
                                                            <p><strong>By - </strong>{item.createdBy}<small></small></p>
                                                            <p><strong>At - </strong>{new Date(item.createdAt).toLocaleString('en-US', { createdAt: localStorage.createdAt })}</p>
                                                        </div>
                                                        <div className="flex-50 p-3">
                                                            <h6>Updated</h6>
                                                            <p><strong>By - </strong>{item.updatedBy}<small></small></p>
                                                            <p><strong>At - </strong>{new Date(item.updatedAt).toLocaleString('en-US', { createdAt: localStorage.createdAt })}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        )}
                                    </ul>
                                :
                                <AddNewButton
                                    text1="No notebook(s) available"
                                    text2="You haven't created any notebooks yet. Please create a notebook first."
                                    createItemOnAddButtonClick={() => this.props.history.push(`/addOrEditNoteBook`)}
                                    imageIcon="addNotebook.png"
                                />
                            }
                        </React.Fragment>
                    }
                </div>

                {this.state.confirmState &&
                    <ConfirmModel
                        status={"delete"}
                        deleteName={this.state.selectedNoteBookToBeDeleted}
                        confirmClicked={() => {
                            this.setState({
                                isFetching: true,
                                confirmState: false,
                                isOpen: false
                            }, () => { this.deleteNoteBookHandler(this.state.notebooktobeDeleted) })
                        }}
                        cancelClicked={() => {
                            this.cancelClicked()
                        }}
                    />
                }

                {this.state.isOpen &&
                    <NotificationModal
                        type={this.state.modalType}
                        message2={this.state.message2}
                        onCloseHandler={this.onCloseHandler}
                    />
                }
            </React.Fragment>
        );
    }
}

NoteBooks.propTypes = {
    dispatch: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
    //notebooks: SELECTORS.makeSelectNoteBooks(),
    notebooksListSuccess: SELECTORS.notebooksListSuccess(),
    notebooksListFailure: SELECTORS.notebooksListFailure(),
    notebookDeleteSuccess: SELECTORS.deleteNoteBookSuccess(),
    notebookDeleteFailure: SELECTORS.deleteNoteBookFailure(),
});

function mapDispatchToProps(dispatch) {
    return {
        dispatch,
        getAllNoteBooks: () => dispatch(ACTIONS.getAllNoteBooks()),
        deleteNoteBook: (notebookId) => dispatch(ACTIONS.deleteNotebook(notebookId)),
    };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "noteBooks", reducer });
const withSaga = injectSaga({ key: "noteBooks", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(NoteBooks);
