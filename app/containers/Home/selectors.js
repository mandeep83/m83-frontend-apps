/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
 * Direct selector to the landingPage state domain
 */

const selectLandingPageDomain = state => state.get("home", initialState);

/**
 * Other specific selectors
 */
export const allDetails = () => createSelector(selectLandingPageDomain, subState => subState.allDetails);

export const allDetailsError = () => createSelector(selectLandingPageDomain, subState => subState.allDetailsError);

export const lineChartData = () => createSelector(selectLandingPageDomain, subState => subState.lineChartData);

export const lineChartDataError = () => createSelector(selectLandingPageDomain, subState => subState.lineChartDataError);

export const barChartData = () => createSelector(selectLandingPageDomain, subState => subState.barChartData);

export const barChartDataError = () => createSelector(selectLandingPageDomain, subState => subState.barChartDataError);
/**
 * Default selector used by LandingPage
 */

const makeSelectLandingPage = () =>
  createSelector(selectLandingPageDomain, substate => substate.toJS());

export default makeSelectLandingPage;
export { selectLandingPageDomain };
