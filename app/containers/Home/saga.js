/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { takeEvery } from 'redux-saga';
import {
    GET_ALL_DETAILS,
    GET_ALL_DETAILS_SUCCESS,
    GET_ALL_DETAILS_FAILURE,
    GET_ACTIVITY_LINE_CHART_DATA,
    GET_ACTIVITY_LINE_CHART_DATA_SUCCESS,
    GET_ACTIVITY_LINE_CHART_DATA_FAILURE,
    GET_ACTIVITY_BAR_CHART_DATA,
    GET_ACTIVITY_BAR_CHART_DATA_SUCCESS,
    GET_ACTIVITY_BAR_CHART_DATA_FAILURE
} from './constants';

import { apiCallHandler } from "../../api";


export function* getALlDetailsApiHandlerAsync(action) {
    yield [apiCallHandler(action, GET_ALL_DETAILS_SUCCESS, GET_ALL_DETAILS_FAILURE, 'getAccountStats')];
}

export function* getActivityLineChartDataHandlerAsync(action) {
    yield [apiCallHandler(action, GET_ACTIVITY_LINE_CHART_DATA_SUCCESS, GET_ACTIVITY_LINE_CHART_DATA_FAILURE, 'getAccountsActivityLineChartData')];
}

export function* getActivityBarChartDataHandlerAsync(action) {
    yield [apiCallHandler(action, GET_ACTIVITY_BAR_CHART_DATA_SUCCESS, GET_ACTIVITY_BAR_CHART_DATA_FAILURE, 'getAccountsActivityBarChartData')];
}

export function* watcherGetAllDetailsRequest() {
    yield takeEvery(GET_ALL_DETAILS, getALlDetailsApiHandlerAsync);
}

export function* watcherGetActivityLineChartDataRequest() {
    yield takeEvery(GET_ACTIVITY_LINE_CHART_DATA, getActivityLineChartDataHandlerAsync);
}

export function* watcherGetActivityBarChartDataRequest() {
    yield takeEvery(GET_ACTIVITY_BAR_CHART_DATA, getActivityBarChartDataHandlerAsync);
}

export default function* rootSaga() {
    yield [watcherGetAllDetailsRequest(),
    watcherGetActivityLineChartDataRequest(),
    watcherGetActivityBarChartDataRequest(),
    ];
}
