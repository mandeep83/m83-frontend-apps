/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

export const DEFAULT_ACTION = "app/Home/DEFAULT_ACTION";

export const GET_ALL_DETAILS = "app/Home/GET_ALL_DETAILS";
export const GET_ALL_DETAILS_SUCCESS = "app/Home/GET_ALL_DETAILS_SUCCESS";
export const GET_ALL_DETAILS_FAILURE = "app/Home/GET_ALL_DETAILS_FAILURE";

export const GET_ACTIVITY_LINE_CHART_DATA = "app/Home/GET_ACTIVITY_LINE_CHART_DATA";
export const GET_ACTIVITY_LINE_CHART_DATA_SUCCESS = "app/Home/GET_ACTIVITY_LINE_CHART_DATA_SUCCESS";
export const GET_ACTIVITY_LINE_CHART_DATA_FAILURE = "app/Home/GET_ACTIVITY_LINE_CHART_DATA_FAILURE";

export const GET_ACTIVITY_BAR_CHART_DATA = "app/Home/GET_ACTIVITY_BAR_CHART_DATA";
export const GET_ACTIVITY_BAR_CHART_DATA_SUCCESS = "app/Home/GET_ACTIVITY_BAR_CHART_DATA_SUCCESS";
export const GET_ACTIVITY_BAR_CHART_DATA_FAILURE = "app/Home/GET_ACTIVITY_BAR_CHART_DATA_FAILURE";
