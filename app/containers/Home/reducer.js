/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { fromJS } from "immutable";
import {
  DEFAULT_ACTION,
  GET_ALL_DETAILS_SUCCESS,
  GET_ALL_DETAILS_FAILURE,
  GET_ACTIVITY_LINE_CHART_DATA_SUCCESS,
  GET_ACTIVITY_LINE_CHART_DATA_FAILURE,
  GET_ACTIVITY_BAR_CHART_DATA_SUCCESS,
  GET_ACTIVITY_BAR_CHART_DATA_FAILURE
} from "./constants";

export const initialState = fromJS({});

function landingPageReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case GET_ALL_DETAILS_SUCCESS:
      return Object.assign({}, state, {
        allDetails: action.response,
      });
    case GET_ALL_DETAILS_FAILURE:
      return Object.assign({}, state, {
        allDetailsError: action.error,
      });
    case GET_ACTIVITY_LINE_CHART_DATA_SUCCESS:
      return Object.assign({}, state, {
        lineChartData: action.response,
      });
    case GET_ACTIVITY_LINE_CHART_DATA_FAILURE:
      return Object.assign({}, state, {
        lineChartDataError: action.error,
      });
    case GET_ACTIVITY_BAR_CHART_DATA_SUCCESS:
      return Object.assign({}, state, {
        barChartData: action.response,
      });
    case GET_ACTIVITY_BAR_CHART_DATA_FAILURE:
      return Object.assign({}, state, {
        barChartDataError: action.error,
      });
    default:
      return state;
  }
}

export default landingPageReducer;
