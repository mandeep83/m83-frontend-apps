/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import { CircularProgressbar, buildStyles } from 'react-circular-progressbar';
import reducer from "./reducer";
import saga from "./saga";
import { getAllTenantDetails, getActivityLineChartData, getActivityBarChartData } from "./actions";
import { allDetails, allDetailsError, lineChartData, lineChartDataError, barChartData, barChartDataError } from "./selectors";
import Loader from "../../components/Loader";
import AllChartsComponent from "../../components/AllChartsComponent";

/* eslint-disable react/prefer-stateless-function */
export class Home extends React.Component {

    state = {
        isFetching: true,
        isFetchingLineChartData: true,
        isFetchingBarChartData: true,
        allDetails: {},
        counters: [],
        lineChartData: [],
        barChartData: [],
        durationActivityLineChartData: 7,
        durationActivityBarChartData: 7,
    }
    componentDidMount() {
        this.props.getAllTenantDetails();
        this.props.getActivityLineChartData(this.state.durationActivityLineChartData);
        this.props.getActivityBarChartData(this.state.durationActivityBarChartData);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.allDetails && nextProps.allDetails !== this.props.allDetails) {
            let allDetails = nextProps.allDetails;
            let counters = [
                {
                    displayName: "Tenants",
                    icon: "fal fa-building",
                    count: nextProps.allDetails.stats.tenants,
                },
                {
                    displayName: "Connectors",
                    icon: "fal fa-chart-network",
                    count: nextProps.allDetails.stats.connectors,
                },
                {
                    displayName: "Transformations",
                    icon: "fal fa-exchange",
                    count: nextProps.allDetails.stats.transformations,
                },
                {
                    displayName: "Code Engines",
                    icon: "fal fa-lambda",
                    count: nextProps.allDetails.stats.codeEngines,
                },
                {
                    displayName: "Js Functions",
                    icon: "fal fa-function",
                    count: nextProps.allDetails.stats.jsFunctions,
                },
                {
                    displayName: "APIs",
                    icon: "fal fa-network-wired",
                    count: nextProps.allDetails.stats.apis,
                },
                {
                    displayName: "Pages",
                    icon: "fal fa-window",
                    count: nextProps.allDetails.stats.pages,
                },
                {
                    displayName: "Applications",
                    icon: "fal fa-desktop-alt",
                    count: nextProps.allDetails.stats.applications,
                }
            ];
            this.setState({
                allDetails,
                counters,
                isFetching: false,
            })
        }

        if (nextProps.lineChartData && nextProps.lineChartData !== this.props.lineChartData) {
            this.setState({
                isFetchingLineChartData: false,
                lineChartData: nextProps.lineChartData,
            })
        }

        if (nextProps.barChartData && nextProps.barChartData !== this.props.barChartData) {
            this.setState({
                isFetchingBarChartData: false,
                barChartData: nextProps.barChartData,
            })
        }
    }

    filterHandler = (event) => {
        const self = this;
        if (event.target.id === 'activities') {
            self.setState({
                isFetchingLineChartData: true,
                durationActivityLineChartData: event.target.value
            }, () => { this.props.getActivityLineChartData(self.state.durationActivityLineChartData) })
        } else {
            self.setState({
                isFetchingBarChartData: true,
                durationActivityBarChartData: event.target.value
            }, () => { this.props.getActivityBarChartData(self.state.durationActivityBarChartData) });
        }
    }

    createChart = (data, id, chartType, chartSubType) => {
        let dataForChart = {
            availableAttributes: [],
            chartData: chartType === "lineChart" ? data.map(chartDataObj => {
                chartDataObj.category = new Date(chartDataObj.timestamp).toLocaleDateString()
                return chartDataObj;
            }) : data
        }

        for (let i = 0; i < data.length; i++) {
            Object.keys(data[i]).map((key) => {
                if (key !== 'category' && key !== 'timestamp' && (!dataForChart.availableAttributes.includes(key))) {
                    dataForChart.availableAttributes.push(key)
                }
            })
        }

        let config = {
            chartType: chartType,
            chartSubType: chartSubType,
            id: id,
            chartData: dataForChart
        };
        return config;
    }

    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>Home</title>
                    <meta name="description" content="M83-Home" />
                </Helmet>

                <header className="content-header d-flex">
                    <div className="flex-60">
                        <h6>Home</h6>
                    </div>
                    <div className="flex-40 text-right">
                        <div className="content-header-group"></div>
                    </div>
                </header>


                {this.state.isFetching ? <Loader /> :
                    <div className="content-body">
                        <ul className="list-style-none d-flex tenant-counter-list">
                            {this.state.counters.map((data, index) => (
                                <li key={index} onClick={() => { data.displayName === 'Tenants' && this.props.history.push('/tenantList') }}>
                                    <div className="tenant-counter-box">
                                        <h4>{data.count}</h4>
                                        <p>{data.displayName}</p>
                                        <div className="tenant-counter-icon">
                                            <i className={data.icon}></i>
                                        </div>
                                    </div>
                                </li>
                            ))}
                        </ul>

                        <div className="d-flex tenant-usage-list">
                            <div className="flex-33 pd-r-10">
                                <div className="tenant-usage-content">
                                    {this.state.allDetails.clusterInfo &&
                                    <CircularProgressbar
                                        value={this.state.allDetails.clusterInfo.cpuUsgae}
                                        circleRatio={0.75}
                                        styles={buildStyles({
                                            rotation: 1 / 2 + 1 / 8,
                                            pathColor: 'rgba(0, 230, 115, 0.7)',
                                            trailColor: "#eee"
                                        })}
                                        className="tenant-usage-progress"
                                    />
                                    }
                                    <h1>{this.state.allDetails.clusterInfo.cpuUsgae}<span>%</span></h1>
                                    <h6>CPU Usage</h6>
                                </div>
                            </div>
                            <div className="flex-33 pd-r-5 pd-l-5">
                                <div className="tenant-usage-content">
                                    {this.state.allDetails.clusterInfo &&
                                    <CircularProgressbar
                                        value={this.state.allDetails.clusterInfo.memoryUsage}
                                        circleRatio={0.75}
                                        styles={buildStyles({
                                            rotation: 1 / 2 + 1 / 8,
                                            pathColor: 'rgba(255, 163, 26, 0.7)',
                                            trailColor: "#eee"
                                        })}
                                        className="tenant-usage-progress"
                                    />
                                    }
                                    <h1>{this.state.allDetails.clusterInfo.memoryUsage}<span>%</span></h1>
                                    <h6>Memory Usage</h6>
                                </div>
                            </div>
                            <div className="flex-33 pd-l-10">
                                <div className="tenant-usage-content">
                                    {this.state.allDetails.clusterInfo &&
                                    <CircularProgressbar
                                        value={this.state.allDetails.clusterInfo.noOfPods}
                                        circleRatio={0.75}
                                        styles={buildStyles({
                                            rotation: 1 / 2 + 1 / 8,
                                            pathColor: 'rgba(51, 102, 255, 0.5)',
                                            trailColor: "#eee"
                                        })}
                                        className="tenant-usage-progress"
                                    />
                                    }
                                    <h1>{this.state.allDetails.clusterInfo.noOfPods}<span>%</span></h1>
                                    <h6>Pods Usage</h6>
                                </div>
                            </div>
                        </div>

                        <div className="d-flex pd-l-5 pd-r-5">
                            <div className="flex-50 pd-r-10">
                                <div className="card">
                                    <div className="card-header"><h6>Activities</h6>
                                        <div className="form-group card-header-filter">
                                            <select id="activities" value={this.state.durationActivityLineChartData} className="form-control" onChange={this.filterHandler}>
                                                <option value={1}>Last 1 Day</option>
                                                <option value={2}>Last 2 Days</option>
                                                <option value={3}>Last 3 Days</option>
                                                <option value={5}>Last 5 Days</option>
                                                <option value={7}>Last 7 Days</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div className="card-body">
                                        {this.state.isFetchingLineChartData ?
                                            <div className="card-loader">
                                                <i className="fad fa-sync-alt fa-spin"></i>
                                            </div> :
                                            this.state.lineChartData.length > 0 ?
                                                <AllChartsComponent className="card-chart-box" config={this.createChart(this.state.lineChartData, "lineChartDiv", "lineChart", "simpleLineChart")} />
                                                :
                                                <div className="card-message">
                                                    <p>There is no data to display !</p>
                                                </div>
                                        }
                                    </div>
                                </div>
                            </div>
                            <div className="flex-50 pd-l-10">
                                <div className="card">
                                    <div className="card-header"><h6>Top Tenant Activities</h6>
                                        <div className="form-group card-header-filter">
                                            <select id="topTenantActivities" value={this.state.durationActivityBarChartData} className="form-control" onChange={this.filterHandler}>
                                                <option value={1}>Last 1 Day</option>
                                                <option value={2}>Last 2 Days</option>
                                                <option value={3}>Last 3 Days</option>
                                                <option value={5}>Last 5 Days</option>
                                                <option value={7}>Last 7 Days</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div className="card-body">
                                        {this.state.isFetchingBarChartData ?
                                            <div className="card-loader">
                                                <i className="fad fa-sync-alt fa-spin"></i>
                                            </div> :
                                            this.state.barChartData.length > 0 ?
                                                <AllChartsComponent className="card-chart-box" config={this.createChart(this.state.barChartData, "barChartDiv", "barChart", "simpleBarChart")} />
                                                :
                                                <div className="card-message">
                                                    <p>There is no data to display !</p>
                                                </div>
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                }
            </React.Fragment>
        );
    }
}

Home.propTypes = {
    dispatch: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
    allDetails: allDetails(),
    allDetailsError: allDetailsError(),
    lineChartDataError: lineChartDataError(),
    lineChartData: lineChartData(),
    barChartDataError: barChartDataError(),
    barChartData: barChartData(),
});

function mapDispatchToProps(dispatch) {
    return {
        dispatch,
        getAllTenantDetails: () => dispatch(getAllTenantDetails()),
        getActivityLineChartData: (durationInDays) => dispatch(getActivityLineChartData(durationInDays)),
        getActivityBarChartData: (durationInDays) => dispatch(getActivityBarChartData(durationInDays))
    };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "home", reducer });
const withSaga = injectSaga({ key: "home", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(Home);
