/************************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

/**
*
* NewAddOrEditMl
*
*/

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { FormattedMessage } from "react-intl";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import reducer from "./reducer";
import saga from "./saga";
import { allSelectors as SELECTORS } from "./selectors";
import { allActions as ACTIONS } from './actions';
import { getNotificationObj } from "../../commonUtils";
import produce from "immer";
import cloneDeep from 'lodash/cloneDeep';
import Slider from 'react-input-slider';
import ReactSelect from 'react-select';
import ReactTooltip from "react-tooltip";
import { getMqttConnection, closeMqttConnection, subscribeTopic, unsubscribeTopic } from "../../mqttConnection";
import Chart from "../../components/LineChart";
import Loader from "../../components/Loader";

const MACHINE_LEARNING_PAYLOAD = {
	name: '',
	description: '',
	type: "machineLearning",
	dataSource: {
		//no dirId anymore
		connectorCategory: "S3"
	},
	cleaningAndTransformConfig: {
		operations: [
			{
				colName: "",
				actions: []
			}]
	},
	featureSelectionConfig: {
		techniques: [

		]
	},
	algorithm: {
		trainingJobName: ''
	},
	"model": "customModel" // autoML , platformML
}, featureList = [{
	displayName: "Constant Removal",
	value: "constantRemoval",
	imageName: "vectorSlicing"
}, {
	displayName: "Correlation Removal",
	value: "correlationRemoval",
	imageName: "rFormula"
}, {
	displayName: "Information Gain",
	value: "informationGain",
	imageName: "vectorSlicing"
}, {
	displayName: "Chi-Sq Test",
	value: "chiSqTest",
	imageName: "rFormula"
}], optionsForEncoder = [
	{ value: 'countVectorize', type: ['Numeric'], label: 'Count Vectorize' },
	{ value: 'oneHotEncoder', type: ['Numeric'], label: 'One Hot Encoder' },
	{ value: 'binaries', type: ['Numeric'], label: 'Binaries' },
	{ value: 'stopWordsRemover', type: ['String'], label: 'Stop Words Remover' },
	{ value: 'vectorAssemblers', type: ['Numeric'], label: 'Vector Assemblers' },
	{ value: 'stringIndexer', type: ['String'], label: 'String Indexer' }
], algorithmsList = [
	{
		name: 'Regression',
		children: [
			{ name: 'XgBoost', value: "XgBoost", url: 'https://content.iot83.com/m83/mlStudio/regression.png' },
			{ name: 'Linear-Learner', value: "linear-learner", url: 'https://content.iot83.com/m83/mlStudio/regression.png' },
		],
		toolTipText: "Technique for prediction of continous variables"
	}, {
		name: ' Classification',
		children: [
			{ name: 'XgBoost', value: "XgBoost", url: 'https://content.iot83.com/m83/mlStudio/regression.png' },
			{ name: 'Linear-Learner', value: "linear-learner", url: 'https://content.iot83.com/m83/mlStudio/regression.png' }
		],
		toolTipText: "Technique for prediction of discrette variables"
	}, {
		name: 'Anomaly',
		children: [
			{ name: 'RandomCutForest', value: "RandomCutForest", url: 'https://content.iot83.com/m83/mlStudio/regression.png' }
		],
		toolTipText: "Technique for predicting unusual behaviour in data points"
	}, {
		name: 'Clustering',
		children: [
			{ name: 'K-Means', value: "K-Means", url: 'https://content.iot83.com/m83/mlStudio/regression.png' }
		],
		toolTipText: "Quantization technique to identify data points in the same subgroup (clusters)"
	}
], recognizeWithOptions = [
	{
		label: "Google",
		value: "google"
	},
	{
		label: "IBM",
		value: "ibm"
	},
	{
		label: "Sphinix",
		value: "sphinix"
	},
	{
		label: "Wit",
		value: "wit"
	}
], optionsForAudio = [
	{
		label: "True",
		value: true
	},
	{
		label: "False",
		value: false
	}
], tools = [
	{
		displayName: 'Trimming',
		name: 'trimming',
		url: 'https://content.iot83.com/m83/mlStudio/trimming.png',
		type: ["String"]
	}, {
		displayName: 'Padding',
		name: 'padding',
		url: 'https://content.iot83.com/m83/mlStudio/padding.png',
		type: ["String", "Numeric"]
	}, {
		displayName: 'Fill Null Value',
		name: 'fillNullValues',
		url: 'https://content.iot83.com/m83/mlStudio/nullValue.png',
		type: ["String", "Numeric"]
	}, {
		displayName: 'Unique Column',
		name: 'uniqueColumn',
		url: 'https://content.iot83.com/m83/mlStudio/unique.png',
		type: ["String", "Numeric"]
	}, {
		displayName: 'Handling All N/A',
		name: 'treatAllMissingValues',
		url: 'https://content.iot83.com/m83/mlStudio/nullValue.png',
		type: ["String", "Numeric"]
	}, {
		displayName: 'Drop Column',
		name: 'dropColumn',
		url: 'https://content.iot83.com/m83/mlStudio/dropDuplicate.png',
		type: ["String", "Numeric"]
	}, {
		displayName: 'Drop Duplicate',
		name: 'dropDuplicates',
		url: 'https://content.iot83.com/m83/mlStudio/dropDuplicate.png',
		type: ["String", "Numeric"]
	}, {
		displayName: 'Regrex Replace',
		name: 'regrexReplace',
		url: 'https://content.iot83.com/m83/mlStudio/replace.png',
		type: ["String", "Numeric"]
	}, {
		displayName: 'Quassi Constant',
		name: 'quasiConstant',
		url: 'https://content.iot83.com/m83/mlStudio/constantcolumn.png',
		type: ["String", "Numeric"]
	}, {
		displayName: 'Encoding',
		name: 'encoding',
		url: 'https://content.iot83.com/m83/mlStudio/encode.png',
		type: ["String", "Numeric"]
	}, {
		displayName: 'Transformation',
		name: 'transformation',
		url: 'https://content.iot83.com/m83/mlStudio/encode.png',
		type: ["String", "Numeric"]
	}, {
		displayName: 'Select Multiple Columns',
		name: 'selectColumn',
		url: 'https://content.iot83.com/m83/mlStudio/encode.png',
		type: ["String", "Numeric"]
	},];

export class NewAddOrEditMl extends React.Component {
	state = {
		isFetching: true,
		listOfSourceConnectors: { "S3": [{ properties: { list: [] } }] },
		selectedSourceConnector: "S3",
		payload: { ...MACHINE_LEARNING_PAYLOAD },
		activeTab: "dataConfiguration",
		columnsList: [],
		informationGainData: {},
		dataCorrelationData: {},
		dataCorrelationDataWithCleaningAndTransform: {},
		statisticalReportData: {},
		statisticalReportDataWithCleaningAndTransform: {},
		showMissingValuesData: {},
		showMissingValuesDataWithCleaningAndTransform: {},
		showSampleData: {},
		showSampleDataWithCleaningAndTransform: {},
		featureImportanceData: {},
	}

	componentDidMount() {
		this.props.getConnectorsByCategory(this.state.selectedSourceConnector);
		this.props.match.params.id && this.props.getExperimentById(this.props.match.params.id)
		subscribeTopic([`MLStudio/${window.API_URL.split("//")[1].split(".")[0]}`, `MLStudio/${window.API_URL.split("//")[1].split(".")[0]}/informationgain`], this.mqttMessageHandler);
	}

	static getDerivedStateFromProps(nextProps, state) {
		if (nextProps.getConnectorsByCategorySuccess) {
			let listOfSourceConnectors = { ...state.listOfSourceConnectors },
				{ response, category } = nextProps.getConnectorsByCategorySuccess;
			listOfSourceConnectors[category] = response.filter(el => el.source);
			return {
				isFetching: false,
				listOfSourceConnectors
			}
		}

		if (nextProps.getColumnsByConnectorSuccess) {
			return {
				columnsList: nextProps.getColumnsByConnectorSuccess.response,
				isFetchingColumns: false
			}
		}

		if (nextProps.getColumnsByConnectorFailure) {
			return { isFetchingColumns: false }
		}

		if (nextProps.getStatisticsSuccess) {
			nextProps.showNotification("success", "Preparing Data");
			nextProps.resetToInitialState();
		}

		if (nextProps.getStatisticsFailure) {
			nextProps.showNotification("error", nextProps.getStatisticsFailure.error);
			nextProps.resetToInitialState();
		}

		if (nextProps.saveExperimentSuccess) {
			nextProps.history.push('/mlExperiments');
			return { isFetching: false }
		}

		if (nextProps.saveExperimentFailure) {
			return { isFetching: false }
		}

		if (nextProps.getExperimentByIdSuccess) {
			nextProps.getColumnsByConnector(nextProps.getExperimentByIdSuccess.response.dataSource)
			return { payload: nextProps.getExperimentByIdSuccess.response, isFetchingColumns: true }
		}

		if (nextProps.getExperimentByIdFailure) {
			nextProps.showNotification("error", nextProps.getExperimentByIdFailure)
			return { isFetchingColumns: false }
		}

		if (Object.keys(SELECTORS).filter(prop => prop.toLowerCase().includes("success")).some(prop => nextProps[prop])) {
			let propName = Object.keys(SELECTORS).find(props => nextProps[props]);
			// return { ...getNotificationObj(nextProps[propName].response, "success") }
		}

		if (Object.keys(SELECTORS).filter(prop => prop.toLowerCase().includes("failure")).some(prop => nextProps[prop])) {
			let propName = Object.keys(SELECTORS).find(props => nextProps[props]);
			// return { ...getNotificationObj(nextProps[propName].error, "error"), isFetching: false }
		}
		return null
	}

	componentDidUpdate(prevProps, prevState) {
		let newProp = Object.keys(SELECTORS).find(prop => this.props[prop])
		if (newProp) {
			if (newProp.includes("Failure")) {
				this.props.showNotification("error", this.props[newProp].error)
			}
			this.props.resetToInitialState(newProp)
		}
	}

	mqttMessageHandler = (message, topic) => {
		let dataMessage = JSON.parse(message.toString()),
			statisticalReportData = cloneDeep(this.state.statisticalReportData),
			dataCorrelationData = cloneDeep(this.state.dataCorrelationData),
			showMissingValuesData = cloneDeep(this.state.showMissingValuesData),
			showSampleData = cloneDeep(this.state.showSampleData),
			statisticalReportDataWithCleaningAndTransform = cloneDeep(this.state.statisticalReportDataWithCleaningAndTransform),
			dataCorrelationDataWithCleaningAndTransform = cloneDeep(this.state.dataCorrelationDataWithCleaningAndTransform),
			showMissingValuesDataWithCleaningAndTransform = cloneDeep(this.state.showMissingValuesDataWithCleaningAndTransform),
			showSampleDataWithCleaningAndTransform = cloneDeep(this.state.showSampleDataWithCleaningAndTransform),
			featureImportanceData = cloneDeep(this.state.featureImportanceData),
			message2;
		if (topic === `MLStudio/${window.API_URL.split("//")[1].split(".")[0]}/informationgain`) {
			let informationGainData = JSON.parse(message.toString()),
				featurePayload = cloneDeep(this.state.featurePayload);
			informationGainData.dataKeys = informationGainData.data && Object.keys(informationGainData.data).length ? Object.keys(informationGainData.data) : []
			let masterList = [];
			informationGainData.dataKeys.map(el => {
				masterList.push({
					key: el, val: informationGainData.data[el]
				})
			})
			masterList = masterList.sort((a, b) => b.val - a.val);
			informationGainData.data = {}
			masterList.map(el => informationGainData.data[el.key] = el.val)
			informationGainData.dataKeys = Object.keys(informationGainData.data)
			featurePayload.k = 1;
			this.setState({
				informationGainData,
				informationGainLoader: false,
				message2: "Data Prepared , click on the Show Information Gain Data button to view the data.",
				isOpen: true,
				modalType: "success",
				featurePayload
			})
			return;
		}
		if (jwt_decode(localStorage["token"]).jti === dataMessage.userId && this.state.payload.dataSource.dirId === dataMessage.dataSource.dirId) {
			if (dataMessage.cleaningAndTransformConfig) {
				if (dataMessage.type === "dataCorrelation") {
					dataCorrelationDataWithCleaningAndTransform = dataMessage
					dataCorrelationDataWithCleaningAndTransform.dataKeys = dataMessage.data.length ? Object.keys(dataMessage.data[0]) : []
					message2 = "Data Prepared , click on the Data Correlation button to view the data.";
				}
				else if (dataMessage.type === "showStatisticalReport") {
					statisticalReportDataWithCleaningAndTransform = dataMessage
					statisticalReportDataWithCleaningAndTransform.dataKeys = dataMessage.variables ? Object.keys(dataMessage.variables) : []
					message2 = "Data Prepared , click on the Show Statistical Report button to view the data.";
				}
				else if (dataMessage.type === "showMissingValues") {
					showMissingValuesDataWithCleaningAndTransform = dataMessage
					showMissingValuesDataWithCleaningAndTransform.dataKeys = dataMessage.data ? Object.keys(dataMessage.data[0]) : []
					message2 = "Data Prepared , click on the Show All N/A button to view the data.";
				}
				else if (dataMessage.type === "showSampleData") {
					showSampleDataWithCleaningAndTransform = dataMessage
					showSampleDataWithCleaningAndTransform.dataKeys = dataMessage.sample ? Object.keys(dataMessage.sample[0]) : []
					message2 = "Data Prepared , click on the Show Sample Data button to view the data.";
				}
				this.setState({
					statisticalReportDataWithCleaningAndTransform,
					dataCorrelationDataWithCleaningAndTransform,
					showMissingValuesDataWithCleaningAndTransform,
					showSampleDataWithCleaningAndTransform,
					isOpen: true,
					modalType: "success",
					message2,
					[`${dataMessage.type}WithCleaningAndTransformLoader`]: false
				})
			}
			else {
				if (dataMessage.type === "dataCorrelation") {
					dataCorrelationData = dataMessage
					dataCorrelationData.dataKeys = dataMessage.data.length ? Object.keys(dataMessage.data[0]) : []
					message2 = "Data Prepared , click on the Data Correlation button to view the data.";
				}
				else if (dataMessage.type === "showStatisticalReport") {
					statisticalReportData = dataMessage
					statisticalReportData.dataKeys = dataMessage.variables ? Object.keys(dataMessage.variables) : []
					message2 = "Data Prepared , click on the Show Statistical Report button to view the data.";
				}
				else if (dataMessage.type === "showMissingValues") {
					showMissingValuesData = dataMessage
					showMissingValuesData.dataKeys = dataMessage.data ? Object.keys(dataMessage.data[0]) : []
					message2 = "Data Prepared , click on the Show All N/A button to view the data.";
				}
				else if (dataMessage.type === "showSampleData") {
					showSampleData = dataMessage
					showSampleData.dataKeys = dataMessage.sample ? Object.keys(dataMessage.sample[0]) : []
					message2 = "Data Prepared , click on the Show Sample Data button to view the data.";
				}
				else if (dataMessage.type === "featureImportance") {
					featureImportanceData = dataMessage
					featureImportanceData.dataKeys = dataMessage.data ? Object.keys(dataMessage.data).sort() : []
					let masterList = [];
					featureImportanceData.dataKeys.map(el => {
						masterList.push({
							key: el, val: featureImportanceData.data[el]
						})
					})
					masterList = masterList.sort((a, b) => b.val - a.val);
					featureImportanceData.data = {}
					masterList.map(el => featureImportanceData.data[el.key] = el.val)
					featureImportanceData.dataKeys = Object.keys(featureImportanceData.data)
					message2 = "Data Prepared , click on the Feature Importance button to view the data.";
				}
				this.setState({
					statisticalReportData,
					dataCorrelationData,
					showMissingValuesData,
					showSampleData,
					featureImportanceData,
					isOpen: true,
					modalType: "success",
					message2,
					[`${dataMessage.type}Loader`]: false
				})
			}
		}

	}

	getStatistics = ({ currentTarget: { name } }, isCleaningAndTransform) => {
		// event.stopPropagation();
		let payload = {
			action: {
				type: "showStatistics",
				actionName: name
			},
			dataSource: this.state.payload.dataSource
		},
			showMissingValuesData = cloneDeep(this.state.showMissingValuesData),
			statisticalReportData = cloneDeep(this.state.statisticalReportData),
			showSampleData = cloneDeep(this.state.showSampleData),
			dataCorrelationData = cloneDeep(this.state.dataCorrelationData),
			statisticalReportDataWithCleaningAndTransform = cloneDeep(this.state.statisticalReportDataWithCleaningAndTransform),
			dataCorrelationDataWithCleaningAndTransform = cloneDeep(this.state.dataCorrelationDataWithCleaningAndTransform),
			showMissingValuesDataWithCleaningAndTransform = cloneDeep(this.state.showMissingValuesDataWithCleaningAndTransform),
			showSampleDataWithCleaningAndTransform = cloneDeep(this.state.showSampleDataWithCleaningAndTransform);
		if (isCleaningAndTransform) {
			if ((name === "dataCorrelationWithCleaningAndTransform" && dataCorrelationDataWithCleaningAndTransform.userId)
				|| (name === "showStatisticalReportWithCleaningAndTransform" && statisticalReportDataWithCleaningAndTransform.userId)
				|| (name === "showMissingValuesWithCleaningAndTransform" && showMissingValuesDataWithCleaningAndTransform.userId)
				|| (name === "showSampleDataWithCleaningAndTransform" && showSampleDataWithCleaningAndTransform.userId)) {
				$(`#${name}Modal`).modal()
			}
			else {
				payload.cleaningAndTransformConfig = this.state.payload.cleaningAndTransformConfig
				if (name === "dataCorrelationWithCleaningAndTransform") {
					dataCorrelationDataWithCleaningAndTransform = {}
					payload.action.actionName = "dataCorrelation"
				} else if (name === "showStatisticalReportWithCleaningAndTransform") {
					statisticalReportDataWithCleaningAndTransform = {}
					payload.action.actionName = "showStatisticalReport"
				} else if (name === "showMissingValuesWithCleaningAndTransform") {
					showMissingValuesDataWithCleaningAndTransform = {}
					payload.action.actionName = "showMissingValues"
				}
				else if (name === "showSampleDataWithCleaningAndTransform") {
					showSampleDataWithCleaningAndTransform = {}
					payload.action.actionName = "showSampleData"
				}
				this.props.getStatistics(payload, `${name}Loader`);

				this.setState({
					statisticalReportDataWithCleaningAndTransform,
					dataCorrelationDataWithCleaningAndTransform,
					showMissingValuesDataWithCleaningAndTransform,
					showSampleDataWithCleaningAndTransform,
					[`${name}Loader`]: true
				})
			}
		}
		else {
			if ((name === "dataCorrelation" && dataCorrelationData.userId)
				|| (name === "showStatisticalReport" && statisticalReportData.userId)
				|| (name === "showMissingValues" && showMissingValuesData.userId)
				|| (name === "showSampleData" && showSampleData.userId)) {
				$(`#${name}Modal`).modal()
			}
			else {
				if (name === "dataCorrelation") {
					dataCorrelationData = {}
				} else if (name === "showStatisticalReport") {
					statisticalReportData = {}
				} else if (name === "showMissingValues") {
					showMissingValuesData = {}
				}
				else if (name === "showSampleData") {
					showSampleData = {}
				}
				this.props.getStatistics(payload, `${name}Loader`);

				this.setState({
					statisticalReportData,
					dataCorrelationData,
					showMissingValuesData,
					showSampleData,
					[`${name}Loader`]: true
				})
			}
		}
		let id = name
		setTimeout(() => this.resetLoaders(id), 300000);
	}

	resetLoaders = (id,) => {
		this.setState({
			[`${id}Loader`]: false
		})
	}

	handleChange = (value, id) => {
		let payload = cloneDeep(this.state.payload);
		if (id === "type") {
			if (value === "audioSentiment") {
				payload = { ...AUDIO_SENTIMENT_ANALYTICS_PAYLOAD }
			} else {
				payload = { ...MACHINE_LEARNING_PAYLOAD }
			}
		} else {
			payload[id] = value;
		}
		this.setState({
			payload
		})
	}

	changeActiveTab = (activeTab) => {
		this.setState({
			activeTab
		})
	}


	dataSourceChangeHandler = (type, id) => {
		let payload = produce(this.state.payload, payload => {
			if (type === "connector") {
				payload.dataSource.connectorId = id
				payload.dataSource.bucketId = ""
			}
			else if (type === "bucket") {
				payload.dataSource.bucketId = id
				let requiredConnector = this.state.listOfSourceConnectors["S3"].find(el => el.id == payload.dataSource.connectorId).properties.list.find(el => el.bucketId == id)
				payload.dataSource.basePath = requiredConnector.basePath
				payload.dataSource.format = requiredConnector.format
			}
		})
		// this.emptyModalPayload();
		// this.emptyModalPayloadWithCleaningAndTransform();
		this.setState({
			payload,
			isFetchingColumns: type === "bucket"
		}, () => {
			type === "bucket" && this.props.getColumnsByConnector(this.state.payload.dataSource)
		})
	}

	onToolDrag = (selectedToolName) => {
		this.setState({ selectedToolName })
	}

	addRemoveColumns = (type, operationIndex) => {
		let payload = produce(this.state.payload, payload => {
			if (type === "add") {
				payload.cleaningAndTransformConfig.operations.push({
					colName: "",
					actions: []
				})
			}
			else {
				payload.cleaningAndTransformConfig.operations.splice(operationIndex, 1);
				if (this.state.operationIndex === operationIndex) {
					this.setState({
						operationIndex: '',
						actionIndex: '',
						selectedAction: ''
					})
				}
			}
		});
		this.emptyModalPayloadWithCleaningAndTransform();
		this.setState({ payload })

	}

	allowDrop = (event) => {
		event.stopPropagation();
		event.preventDefault();
	}

	onToolDrop = (event, operationIndex) => {
		let payload = produce(this.state.payload, payload => {
			let column = payload.cleaningAndTransformConfig.operations[operationIndex];
			let selectedToolName = this.state.selectedToolName;
			if (column.colName) {
				if (tools.find(tool => selectedToolName !== "selectColumn" && selectedToolName === tool.name && tool.type.includes(column.colType))) {
					if (column.actions.find(action => action.actionName === this.state.selectedToolName)) {
						this.props.showNotification("error", "Error ! Error Action is being repeated");
					} else {
						column.actions.push({
							actionName: selectedToolName,
							isCompleted: ['treatAllMissingValues', 'dropColumn', 'dropDuplicates'].includes(selectedToolName)
						})
						this.emptyModalPayloadWithCleaningAndTransform();
					}
				} else if (column.colName === "ForAll" && selectedToolName === "selectColumn") {
					if (!column.actions.find(action => action.actionName === selectedToolName)) {
						column.actions.push({
							actionName: selectedToolName,
							isCompleted: false
						})
						this.emptyModalPayloadWithCleaningAndTransform();
					} else {
						this.props.showNotification("error", "Error ! Error Action is being repeated")
					}
				} else {
					this.props.showNotification("error", "Error! This column doesn't Support the selected tool")
				}
			} else {
				this.props.showNotification("error", "Error Please Select Column First");
			}
		});
		this.setState({
			payload,
			selectedToolName: ''
		})
	}

	onFeatureDrag = (draggedFeature) => {
		this.setState({
			draggedFeature
		})
	}

	onFeatureDrop = () => {
		let payload = cloneDeep(this.state.payload);
		if (payload.featureSelectionConfig.techniques.find(action => action.actionName === this.state.draggedFeature)) {
			this.props.showNotification("error", "Error! Feature is being repeated!")
			return;
		}
		else {
			payload.featureSelectionConfig.techniques.push({
				actionName: this.state.draggedFeature,
				isCompleted: this.state.draggedFeature === "constantRemoval" ? true : false
			})
			this.setState({
				payload,
				draggedFeature: ''
			})
		}
	}

	deleteFeature = (e, featureIndex) => {
		let payload = cloneDeep(this.state.payload);
		payload.featureSelectionConfig.techniques.splice(featureIndex, 1)
		this.setState({
			payload,
			featureIndex: '',
			selectedFeature: ''
		})
	}

	onFeatureSelect = (e, featureIndex, selectedFeature) => {
		let payload = cloneDeep(this.state.payload),
			featurePayload = {},
			selectedFeaturePayload = payload.featureSelectionConfig.techniques[featureIndex];
		featurePayload = Object.keys(selectedFeaturePayload).length > 1 ? selectedFeaturePayload : {}
		switch (selectedFeature) {
			case "correlationRemoval":
				featurePayload.threshold = selectedFeaturePayload.threshold ? selectedFeaturePayload.threshold : 0.1;
				featurePayload.targetColumn = selectedFeaturePayload.targetColumn ? selectedFeaturePayload.targetColumn : '';
				break;
			case "chiSqTest":
				featurePayload.k = selectedFeaturePayload.k ? selectedFeaturePayload.k : 1;
				break;
			case "informationGain":
				featurePayload.targetColumn = selectedFeaturePayload.targetColumn ? selectedFeaturePayload.targetColumn : '';
				featurePayload.targetColumnType = selectedFeaturePayload.targetColumnType ? selectedFeaturePayload.targetColumnType : 'regressionType';
				break;

		}
		this.setState({
			payload,
			featureIndex,
			selectedFeature,
			featurePayload
		})
	}

	emptyModalPayloadWithCleaningAndTransform = () => {
		this.setState({
			statisticalReportDataWithCleaningAndTransform: {},
			dataCorrelationDataWithCleaningAndTransform: {},
			showMissingValuesDataWithCleaningAndTransform: {},
			showSampleDataWithCleaningAndTransform: {},
		})
	}

	columnChangeHandler = (event, operationIndex) => {
		let selectedColumn;
		let payload = produce(this.state.payload, payload => {
			payload.cleaningAndTransformConfig.operations[operationIndex][event.target.id] = event.target.value;
			if (event.target.value === "ForAll") {
				payload.cleaningAndTransformConfig.operations[operationIndex].colType = "String Numeric";
			} else {
				selectedColumn = this.state.columnsList.find(col => col.keys === event.target.value);
				payload.cleaningAndTransformConfig.operations[operationIndex].colType = selectedColumn ? selectedColumn.type : '';
			}
			payload.cleaningAndTransformConfig.operations[operationIndex].actions = [];
		})
		this.emptyModalPayloadWithCleaningAndTransform();
		this.setState({
			payload,
			selectedAction: '',
			actionIndex: '',
			operationIndex: ''
		})
	}

	onActionSelect = (e, operationIndex, actionIndex, selectedAction) => {
		let formPayload = {},
			payload = cloneDeep(this.state.payload),
			selectedPayload = payload.cleaningAndTransformConfig.operations[operationIndex].actions[actionIndex],
			selectedEncoder, selectedInputCols;
		formPayload = Object.keys(selectedPayload).length > 1 ? selectedPayload : {}
		switch (selectedAction) {
			case "trimming":
				formPayload.type = selectedPayload.type ? selectedPayload.type : "all";
				break;
			case "quasiConstant":
			case "uniqueColumn":
				formPayload.tolerance = selectedPayload.tolerance ? selectedPayload.tolerance : "0";
				break;
			case "encoding":
				selectedEncoder = selectedPayload.encoder ? optionsForEncoder.find(encoder => encoder.value === selectedPayload.encoder) : {};
				if (selectedPayload.inputCols || selectedPayload.inputCol) {
					if (selectedPayload.encoder === "vectorAssemblers") {
						selectedInputCols = selectedPayload.inputCols.map(col => {
							return ({
								value: col,
								label: col
							})
						})
					}
					else {
						selectedInputCols = {
							value: selectedPayload.inputCol,
							label: selectedPayload.inputCol
						}
					}
				}
				break;
			case "selectColumn":
				if (selectedPayload.columnList) {
					selectedInputCols = selectedPayload.columnList.map(col => {
						return ({
							value: col,
							label: col
						})
					})
				}
				break;
		}
		this.setState({
			operationIndex,
			actionIndex,
			selectedAction,
			formPayload,
			selectedEncoder,
			selectedInputCols
		})
	}

	transformationFormChangeHandler = (event) => {
		let formPayload = cloneDeep(this.state.formPayload);
		if (event.target.name === "type") {
			formPayload.type = event.target.value
		}
		else if (event.target.name === "dropColumn") {
			formPayload.dropColumn = event.target.value
		}
		else if (event.target.id === "encoder") {
			formPayload = {}
			formPayload[event.target.id] = event.target.value
		}
		else if (event.target.id === "length" || event.target.id === "padWith" || event.target.id === "vocabSize" || event.target.id === "minDf") {
			if (/[0-9]+/.test(event.target.value) || /^$/.test(event.target.value)) {
				formPayload[event.target.id] = event.target.value === '' ? '' : parseInt(event.target.value)
			}
		}
		else if (event.target.id === "transform") {
			if (event.target.value === "splitData") {
				formPayload.percent = 0
			}
			formPayload[event.target.id] = event.target.value
		}
		else {
			formPayload[event.target.id] = (event.target.value)
		}
		this.setState({
			formPayload
		})
	}

	actionSaveHandler = (event) => {
		event.preventDefault();
		let formPayload = cloneDeep(this.state.formPayload),
			payload = cloneDeep(this.state.payload)
		let keysArray = Object.keys(formPayload),
			changeIndex = payload.cleaningAndTransformConfig.operations[this.state.operationIndex].actions[this.state.actionIndex]
		for (let i = 0; i < keysArray.length; i++) {
			keysArray[i] !== "actionName" ? changeIndex[keysArray[i]] = formPayload[keysArray[i]] : ''
		}
		changeIndex.isCompleted = true
		this.emptyModalPayloadWithCleaningAndTransform();
		this.setState({
			formPayload: {},
			payload,
			operationIndex: '',
			actionIndex: '',
			selectedAction: ''
		})
	}

	SliderChangeHandler = (x, key) => {
		let formPayload = cloneDeep(this.state.formPayload);
		if (key === "percent") {
			formPayload.percent = x.toFixed(2);
		}
		else if (key === "threshold") {
			formPayload.threshold = x.toFixed(2);
		}
		else {
			formPayload.tolerance = x.toFixed(2);
		}
		this.setState({
			formPayload
		})
	}

	createOptionsForReactSelect = () => {
		let columnsList = cloneDeep(this.state.columnsList),
			options = [];
		for (let i = 0; i < columnsList.length; i++) {
			if (columnsList[i].type === (this.state.payload.cleaningAndTransformConfig && this.state.payload.cleaningAndTransformConfig.operations[this.state.operationIndex].colType)) {
				options.push({
					value: columnsList[i].keys,
					label: columnsList[i].keys
				})
			}
		}
		return options;
	}

	handleInputColumnChange = (value, type, isMulti) => {
		let formPayload = cloneDeep(this.state.formPayload),
			selectedInputCols;
		selectedInputCols = value
		if (isMulti) {
			formPayload.inputCols = []
			formPayload.inputCols = value.map(val => { return val.value })
		}
		else {
			formPayload.inputCol = ''
			formPayload.inputCol = value.value
		}
		this.setState({
			formPayload,
			selectedInputCols
		})
	}

	handleChangeForReactSelect = (value) => {
		let formPayload = cloneDeep(this.state.formPayload),
			selectedEncoder;
		formPayload.encoder = value.value;
		selectedEncoder = value
		this.setState({
			formPayload,
			selectedEncoder
		})
	}

	handleMultipleColumnsToolChange = value => {
		let formPayload = cloneDeep(this.state.formPayload),
			selectedInputCols;
		selectedInputCols = value
		formPayload.columnList = []
		formPayload.columnList = value.map(val => { return val.value })
		this.setState({
			formPayload,
			selectedInputCols
		})
	}

	createOptionsForMultiColumns = () => {
		let columnsList = cloneDeep(this.state.columnsList),
			options = [];
		for (let i = 0; i < columnsList.length; i++) {
			options.push({
				value: columnsList[i].keys,
				label: columnsList[i].keys
			})
		}
		return options;
	}

	selectAlgorithm = (property, value) => {
		let payload = produce(this.state.payload, payload => {
			if (property === "name") {
				payload.algorithm.algorithmName = value
				payload.algorithm.algorithmType = '';
				payload.algorithm.hyperparams = {}
			}
			else {
				payload.algorithm.algorithmType = value
				switch (value) {
					case "XgBoost": {
						payload.algorithm.hyperparams = {
							objective: payload.algorithm.algorithmName === "Regression" ? 'reg:linear' : 'binary:logistics',
							eta: 0.1,
							subsample: 1,
							num_round: 100,
							trainSplitValue: 0.7,
							testSplitValue: 0.3
						}
						break;
					}
					case "linear-learner": {
						payload.algorithm.hyperparams = {
							feature_dim: 1,
							predictor_type: payload.algorithm.algorithmName === "Regression" ? 'regressor' : 'binary_classifier',
							trainSplitValue: 0.7,
							testSplitValue: 0.3
						}
						break;
					}
					case "RandomCutForest": {
						payload.algorithm.hyperparams = {
							feature_dim: 1,
							num_samples_per_tree: 256,
							num_trees: 50,
							train_instance_count: 1
						}
						break;
					}
					case "K-Means": {
						payload.algorithm.hyperparams = {
							feature_dim: 1,
							k: 5
						}
						break;
					}
				}
			}
		})

		this.setState({
			payload
		})
	}

	algorithmFormHandler = (event) => {
		let payload = produce(this.state.payload, payload => {
			if (event.target.id === 'trainingJobName') {
				if (/^[A-Za-z0-9]+$/.test(event.target.value) || /^$/.test(event.target.value)) {
					payload.algorithm.trainingJobName = event.target.value
				}
			}
			else if (event.target.id === 'max_depth') {
				if ((/^[0-9]+$/.test(event.target.value) && (parseInt(event.target.value) >= 0 && parseInt(event.target.value) <= 1000)) || /^$/.test(event.target.value)) {
					payload.algorithm.hyperparams.max_depth = event.target.value
				}
			}
			else if (event.target.id === 'subsample') {
				if (((parseInt(event.target.value) >= 0 && parseInt(event.target.value) <= 1)) || /^$/.test(event.target.value)) {
					payload.algorithm.hyperparams.subsample = event.target.value
				}
			}
			else if (event.target.id === 'feature_dim') {
				if (/^[0-9]\d*$/.test(event.target.value) || /^$/.test(event.target.value)) {
					payload.algorithm.hyperparams.feature_dim = event.target.value
				}
			}
			else {
				payload.algorithm.hyperparams[event.target.id] = event.target.value
			}
		});
		this.setState({
			payload
		})
	}

	SliderChangeHandlerForAlgorithm = (x, key) => {
		let payload = produce(this.state.payload, payload => {
			let value = parseFloat(x.toFixed(1)),
				adjustedValue = parseFloat((1 - value).toFixed(1));
			if (key === "trainSplitValue") {
				payload.algorithm.hyperparams.trainSplitValue = value
				payload.algorithm.hyperparams.testSplitValue = adjustedValue
			} else {
				payload.algorithm.hyperparams.testSplitValue = value
				payload.algorithm.hyperparams.trainSplitValue = adjustedValue
			}
		})
		this.setState({
			payload
		})
	}

	SliderChangeHandlerForFeatureSelection = (x, key) => {
		let featurePayload = produce(this.state.featurePayload, featurePayload => {
			if (key === "threshold") {
				featurePayload.threshold = x.toFixed(2);
			} else {
				featurePayload[key] = x;
			}
		});
		this.setState({
			featurePayload
		})
	}

	featureFormChangeHandler = (event) => {
		let featurePayload = cloneDeep(this.state.featurePayload),
			informationGainData = cloneDeep(this.state.informationGainData);
		if (event.target.id === "targetColumn" || event.target.id === "targetColumnType") {
			informationGainData = {}
		}
		featurePayload[event.target.id] = event.target.value
		this.setState({
			featurePayload,
			informationGainData
		})
	}

	featureSaveHandler = (event) => {
		event.preventDefault();
		let featurePayload = cloneDeep(this.state.featurePayload),
			payload = cloneDeep(this.state.payload)
		let keysArray = Object.keys(featurePayload),
			changeIndex = payload.featureSelectionConfig.techniques[this.state.featureIndex]
		for (let i = 0; i < keysArray.length; i++) {
			keysArray[i] !== "actionName" ? changeIndex[keysArray[i]] = featurePayload[keysArray[i]] : ''
		}
		changeIndex.isCompleted = true
		this.setState({
			featurePayload: {},
			payload,
			featureIndex: '',
			selectedFeature: '',
		})
	}

	deleteAction = (e, deleteOperationIndex, deleteActionIndex) => {
		let payload = produce(this.state.payload, payload => {
			(payload.cleaningAndTransformConfig.operations[deleteOperationIndex].actions).splice(deleteActionIndex, 1);
		})
		this.emptyModalPayloadWithCleaningAndTransform();
		this.setState({
			payload,
			operationIndex: '',
			actionIndex: '',
			selectedAction: ''
		})
	}

	createChart = (data, el, chartType, chartSubType) => {
		let dataForChartJson = data.variables[el].histogram_data.__Series__ ? JSON.parse(data.variables[el].histogram_data.__Series__) : null,
			dataForChart = [];
		if (dataForChartJson) {
			for (let i = 0; i < 100; i++) {
				dataForChart.push({
					category: i,
					[el]: dataForChartJson[i]
				})
			}
			dataForChart = {
				availableAttributes: [el],
				chartData: dataForChart
			}
			let chartData = {
				theme: "kelly's",
				widgetType: chartType,
				widgetName: chartSubType,
				i: data.cleaningAndTransformConfig ? `chartData_${chartType}_${el}_WithCleaningAndTransform` : `chartData_${chartType}_${el}`,
				widgetData: dataForChart,
				widgetDetails: {
					"customValues": chartType === "lineChartForMl" ?
						{
							"lineThickness": { "value": 2, "type": "number" },
							"legendPlacement": {
								"value": "bottom", "type": "select",
								"options": ["bottom", "top", "left", "right"]
							},
							"bulletVisibility": { "value": true, "type": "boolean" }
							, "bulletSize": { "value": 5, "type": "number", "parent": "bulletVisibility" },
							"customColors": { "value": false, "type": "boolean" },
							"aqi": { "value": "#FF0000", "type": "color", "parent": "customColors" }
						} : {
							"barWidth": { "value": 80, "type": "number" },
							"legendPlacement": { "value": "bottom", "type": "select", "options": ["bottom", "top", "left", "right"] },
							"chartAngle": { "value": 30, "type": "number" }, "chartDepth": { "value": 30, "type": "number" },
							"customColors": { "value": false, "type": "boolean" }, "year2005": { "value": "#0ba6ef", "type": "color", "parent": "customColors" },
							"columnTopRadius": { "value": 10 }
						}
				}
			};
			return (<Chart data={chartData} deleteLegend={chartType === "lineChartForMl" ? false : true} />);
		}
		else {
			return ("Error creating Charts");
		}
	}

	createChartForFeatureImportanceData = (data, chartType, chartSubType) => {
		let dataForChart = [];
		if (data.dataKeys.length) {
			for (let i = 0; i < data.dataKeys.length; i++) {
				dataForChart.push({
					category: data.dataKeys[i],
					columnValue: data.data[data.dataKeys[i]]
				})
			}
			dataForChart = {
				availableAttributes: ["columnValue"],
				chartData: dataForChart
			}
			let chartData = {
				widgetType: chartType,
				widgetName: chartSubType,
				i: `chartData_${chartType}`,
				widgetData: dataForChart,
				widgetDetails: {
					"customValues": {
						"barWidth": { "value": 80, "type": "number" },
						"legendPlacement": { "value": "bottom", "type": "select", "options": ["bottom", "top", "left", "right"] },
						"chartAngle": { "value": 30, "type": "number" }, "chartDepth": { "value": 30, "type": "number" },
						"customColors": { "value": false, "type": "boolean" }, "year2005": { "value": "#0ba6ef", "type": "color", "parent": "customColors" }
					}
				}
			};
			return (<Chart data={chartData} deleteLegend={true} />);
		}
		else {
			return ("Error creating Charts");
		}
	}

	saveExperimentHandler = () => {
		let payload = cloneDeep(this.state.payload);
		if (!payload.name || !payload.dataSource.bucketId) {
			this.props.showNotification("error", "Experiment Name or DataSource cannot be empty")
		} else {
			payload.isDraft = false;
			this.setState({
				isFetching: true,
			}, () => this.props.saveExperiment(payload));
		}
	}

	handleAudioAnalyticsTransformation = ({ value }, id) => {
		let payload = produce(this.state.payload, payload => {
			payload.speechSentiment[id] = value;
		})
		this.setState({ payload })
	}

	render() {
		var directoryList = [];
		this.state.listOfSourceConnectors[this.state.selectedSourceConnector].map(connector => {
			if (connector.id === this.state.payload.dataSource.connectorId) {
				connector.properties.list.map(bucket => {
					if (bucket.bucketId === this.state.payload.dataSource.bucketId) {
						directoryList.push({
							connector: connector.name,
							bucket: bucket.bucketName
						})
					}
				})
			}
		})
		return (
			<React.Fragment>
				<Helmet>
					<title>NewAddOrEditMl</title>
					<meta name="description" content="Description of NewAddOrEditMl" />
				</Helmet>
				{this.state.isFetching ?
					<Loader /> :
					<React.Fragment>
						<header className="content-header d-flex">
							<div className="flex-60">
								<div className="d-flex">
									<h6 className="previous" onClick={() => this.props.history.push('/mlExperiments')}>ML Experiments</h6>
									<h6 className="active">Add New</h6>
								</div>
							</div>

							<div className="flex-40 text-right">
								<div className="content-header-group">
									<button className="btn btn-light" data-tooltip="true" data-tooltip-text="Back" data-tooltip-place="bottom">
										<i className="fas fa-angle-left"></i>
									</button>
								</div>
							</div>
						</header>


						<div className="content-body">
							<form>
								<div className="form-content-wrapper form-content-wrapper-both">

									<div id="tabs" className="action-tabs mb-3 position-sticky">
										<ul className="nav nav-tabs list-style-none d-flex border-0">
											<li className={`nav-item ${this.state.activeTab == "dataConfiguration" ? "active" : ""}`} data-toggle="tab" onClick={() => this.changeActiveTab("dataConfiguration")} href="#dataConfiguration">
												<a className="nav-link">Data Configuration</a>
											</li>
											<li className={`nav-item ${this.state.activeTab == "cleaningTransformation" ? "active" : ""}`} data-toggle="tab" onClick={() => this.changeActiveTab("cleaningTransformation")} href="#cleaningTransformation">
												<a className="nav-link">{"Cleaning & Transformation"}</a>
											</li>
											<li className={`nav-item ${this.state.activeTab == "featureSelection" ? "active" : ""}`} data-toggle="tab" onClick={() => this.changeActiveTab("featureSelection")} href="#featureSelection">
												<a className="nav-link">Feature Selection</a>
											</li>
											<li className={`nav-item ${this.state.activeTab == "modelBuilding" ? "active" : ""}`} data-toggle="tab" onClick={() => this.changeActiveTab("modelBuilding")} href="#modelBuilding">
												<a className="nav-link">Model Building</a>
											</li>
										</ul>
									</div>

									<div className="tab-content">
										<div className={`tab-pane ${this.state.activeTab == "dataConfiguration" ? "active" : ""}`} id="dataConfiguration">

											<div className="d-flex">
												<div className="flex-50 pd-r-7">
													<div className="card">
														<div className="card-header">
															<h6>Select Source File</h6>
														</div>

														<div className="card-body">
															<div className="card-body-height">
																<div className="collapse-box">
																	<p className="accordion-link" data-toggle="collapse" data-target="#collapseOne1">
																		<img src={`https://content.iot83.com/m83/dataConnectors/amazons3.png`} /> Amazon S3
																		</p>
																	<div id="collapseOne1" className={this.props.match.params.id ? "collapse show" : "collapse"}>
																		<ul className="list-style-none d-flex flex-column pl-3">
																			{this.state.listOfSourceConnectors[this.state.selectedSourceConnector].map((connector, index) => {
																				return (
																					<li key={index}>
																						<p className="level-two collapsed" data-toggle="collapse" onClick={() => this.dataSourceChangeHandler("connector", connector.id)}>{connector.name}</p>
																						<ul className={this.state.payload.dataSource.connectorId === connector.id ? "collapse show pl-3" : "collapse"} id="child2" >
																							{connector.properties.list.map((bucket, bucketIndex) => {
																								return (
																									// <li key={bucketIndex}>
																									// <p key={bucketIndex} className="level-three pd-l-80" onClick={() => this.dataSourceChangeHandler("bucket", bucket.bucketId)}><i className="fas fa-circle"></i>{bucket.bucketName}</p>
																									<p key={bucketIndex} className="level-three pd-l-80" data-toggle="collapse" onClick={() => this.dataSourceChangeHandler("bucket", bucket.bucketId)}>{bucket.bucketName}</p>
																									// </li>)
																								)
																							})}
																						</ul>
																					</li>
																				)
																			})}
																		</ul>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>

												<div className="flex-50 pd-l-7">
													{!this.state.payload.dataSource.bucketId ?
														<div className="drop-zone-box card full-card-height p-0">
															<div className="drop-zone">
																<h5>
																	<div className="drop-zone-icon">
																		<img src="https://content.iot83.com/m83/mlStudio/feature.png" />
																	</div>
																No Source File Selected
															</h5>
															</div>
														</div> :
														<div className="selected-item-box p-0 full-img-wrapper">
															<div className="selected-item ">
																<i className="fas fa-times-circle cancel text-content"></i>
																<div className="d-flex selected-info-box">
																	<img src="https://content.iot83.com/m83/mlStudio/algo.png" />
																	<div className="d-flex selected-info-name">
																		<div className="flex-50 selected-info-image">
																			<img src="https://content.iot83.com/m83/mlStudio/amazon.png" />
																		</div>
																		{directoryList.map((dir, index) => <div className="flex-50" key={index}>
																			<p className="m-0"><strong>{this.state.payload.dataSource.connectorCategory}</strong></p>
																			<p className="m-0">{dir.connector}</p>
																			<p className="m-0">{dir.bucket}</p>
																		</div>
																		)}
																	</div>
																</div>
															</div>
														</div>}
												</div>
											</div> :


										</div>

										<div className="tab-pane fade" id="cleaningTransformation">
											<div className="d-flex">
												<React.Fragment>
													<div className="flex-100">
														<div className="card">
															<div className="card-header">
																<h6>Select Tools</h6>
															</div>
															<div className="card-body">
																<ul className="list-style-none d-flex feature-list draggable-list">
																	{tools.map(({ name, url, displayName }, index) => {
																		return <li key={index} className="flex-33" draggable onDragStart={() => this.onToolDrag(name)}><img src={url} />{displayName}</li>
																	})
																	}
																</ul>
															</div>
														</div>
													</div>

													<div className="flex-50 pd-r-7">
														<div className="card mb-0">
															<div className="card-header card-header-btn-link">
																<h6>Columns</h6>
																<button type="button" className="btn btn-link" onClick={() => this.addRemoveColumns("add", null)}><i className="far fa-plus"></i>Add Columns</button>
															</div>

															<div className="card-body px-0">
																<div className="card-body-height card-overflow-scroll px-3">
																	{this.state.payload.cleaningAndTransformConfig && this.state.payload.cleaningAndTransformConfig.operations.map((operation, operationIndex) => {
																		return (
																			<div className="condition-box py-3 custom-condition-column" key={operationIndex}>
																				<div className="form-group">
																					<label className="form-group-label">Select Column : <i className="fas fa-asterisk form-group-required"></i> </label>
																					<select className="form-control" id="colName" disabled={this.state.isFetchingColumns} required value={operation.colName} onChange={(e) => this.columnChangeHandler(e, operationIndex)}>
																						<option>Select</option>
																						{this.state.columnsList.map((column, columnIndex) => <option key={columnIndex} value={column.keys}>{column.keys}</option>)}
																						{this.state.columnsList.length && <option value='ForAll'>Select Multi Columns</option>}
																					</select>
																				</div>

																				<div className="selection-outerBox">
																					{operation.actions.map((action, actionIndex) => {
																						let url = tools.find(tool => tool.name === action.actionName).url
																						let displayName = tools.find(tool => tool.name === action.actionName).displayName
																						return (
																							<div className="" key={actionIndex} className={`selection-text ${action.isCompleted ? this.state.actionIndex === actionIndex && this.state.operationIndex === operationIndex ? "active completed" : "unactive completed" : this.state.actionIndex === actionIndex && this.state.operationIndex === operationIndex ? "active" : "unactive"}`}>
																								<p onClick={(e) => { this.onActionSelect(e, operationIndex, actionIndex, action.actionName) }}><img src={url} height="25px" width="25px" />{displayName}
																									<button type="button" className="btn-transparent btn-transparent-blue" data-tooltip="true" data-tooltip-text={action.isCompleted ? "Completed" : "InComplete"} data-tooltip-place="bottom">
																										<i className={action.isCompleted ? "fas fa-check" : "fad fa-question"}></i>
																									</button>
																								</p>
																								<button type="button" className="btn-transparent btn-transparent-red" data-tooltip="true" data-tooltip-text="Delete" data-tooltip-place="bottom" onClick={(e) => this.deleteAction(e, operationIndex, actionIndex)}>
																									<i className="far fa-trash-alt"></i>
																								</button>
																							</div>
																						)
																					})}
																				</div>

																				<div className="file-upload-box" onDrop={(e) => this.onToolDrop(e, operationIndex)} onDragOver={(event) => this.allowDrop(event)}>
																					<div className="file-upload-form">
																						<p>Drag & Drop Feature Here..!</p>
																					</div>
																				</div>

																				<button type="button" className={this.state.payload.cleaningAndTransformConfig && this.state.payload.cleaningAndTransformConfig.operations.length === 1 ? "btn-transparent btn-transparent-red btn-disabled" : "btn-transparent btn-transparent-red"} disabled={this.state.payload.cleaningAndTransformConfig && this.state.payload.cleaningAndTransformConfig.operations.length === 1} data-tooltip="true" data-tooltip-text="Delete" data-tooltip-place="bottom" onClick={() => this.addRemoveColumns("delete", operationIndex)}>
																					<i className="far fa-trash-alt"></i>
																				</button>

																			</div>
																		)
																	})}
																</div>
															</div>
														</div>
													</div>

													<div className="flex-50 pd-l-7">
														<div className="card mb-0">
															<div className="card-header">
																{this.state.selectedAction ?
																	<h6>Properties <i className="fas fa-greater-than mr-r-10 mr-l-10 f-12"></i> Transformation <i className="fas fa-greater-than mr-r-10 mr-l-10 f-12"></i> {tools.find(tool => tool.name === this.state.selectedAction).displayName}</h6>
																	: <h6>{"Properties > Transformation"}</h6>
																}
															</div>
															<div className="card-body">
																<div className="card-body-height card-overflow-scroll features-list-form-wrapper">
																	{
																		this.state.selectedAction === '' &&
																		<div className="card-message">
																			<h5 className="f-13">Select Atleast One Tool</h5>
																		</div>
																	}
																	{/* ________________ Trimming ________________ */}
																	{this.state.selectedAction === "trimming" &&
																		<form className="contentForm" onSubmit={this.actionSaveHandler}>
																			<div className="form-group">
																				<div className="d-flex">
																					<label className="flex-100 pd-l-10 form-group-label">Column Name:</label>
																					<div className="flex-33">
																						<label className="radioLabel form-group-label">Left
																				<input name="type" type="radio" value="left" checked={this.state.formPayload.type === "left"} onChange={this.transformationFormChangeHandler} />
																							<span className="radioMark" />
																						</label>
																					</div>
																					<div className="flex-33">
																						<label className="radioLabel form-group-label">Right
																				<input name="type" type="radio" value="right" checked={this.state.formPayload.type === "right"} onChange={this.transformationFormChangeHandler} />
																							<span className="radioMark" />
																						</label>
																					</div>
																					<div className="flex-33">
																						<label className="radioLabel form-group-label">All
																				<input name="type" type="radio" value="all" checked={this.state.formPayload.type === "all"} onChange={this.transformationFormChangeHandler} />
																							<span className="radioMark" />
																						</label>
																					</div>
																				</div>
																			</div>
																			<div className="bottom-button text-right">
																				<button type="submit" className="btn btn-primary m-0">Save</button>
																			</div>
																		</form>
																	}

																	{/* ________________ Padding ________________ */}
																	{this.state.selectedAction === "padding" &&
																		<form className="contentForm" onSubmit={this.actionSaveHandler}  >
																			<div className="form-group">
																				<label className="form-group-label">Length :<i className="fas fa-asterisk form-group-required"></i> </label>
																				<input className="form-control" id="length" value={this.state.formPayload.length} type="number" onChange={this.transformationFormChangeHandler} placeholder="indexes" />

																			</div>
																			<div className="form-group">
																				<div className="d-flex">
																					<label className="flex-100  form-group-label">Type:</label>
																					<div className="flex-33">
																						<label className="radioLabel  form-group-label">Left
																					<input name="type" type="radio" value="left" checked={this.state.formPayload.type === "left"} onChange={this.transformationFormChangeHandler} />
																							<span className="radioMark" />
																						</label>
																					</div>
																					<div className="flex-33">
																						<label className="radioLabel form-group-label">Right
																					<input name="type" type="radio" value="right" checked={this.state.formPayload.type === "right"} onChange={this.transformationFormChangeHandler} />
																							<span className="radioMark" />
																						</label>
																					</div>
																				</div>
																			</div>
																			<div className="form-group">
																				<label className="form-group-label">Bandwidth :<i className="fas fa-asterisk form-group-required"></i>
																				</label>
																				<input className="form-control" id="padWith" value={this.state.formPayload.padWith} type="number" onChange={this.transformationFormChangeHandler} placeholder="indexes" />

																			</div>
																			<div className="bottom-button text-right">
																				<button type="submit" className="btn btn-primary m-0">Save</button>
																			</div>
																		</form>
																	}

																	{/* ________________ Fill Null value ________________ */}
																	{this.state.selectedAction === "fillNullValues" &&
																		<form className="contentForm" onSubmit={this.actionSaveHandler}>
																			<div className="d-flex">
																				<label className="flex-100 form-group-label">Type:</label>
																				<div className="flex-33 form-group-label">
																					<label className={this.state.payload.cleaningAndTransformConfig && this.state.payload.cleaningAndTransformConfig.operations[this.state.operationIndex].colType === "String" ? "radioLabel radioDisabled" : "radioLabel"}>Mean
																			<input name="type" type="radio" value="mean" disabled={this.state.payload.cleaningAndTransformConfig && this.state.payload.cleaningAndTransformConfig.operations[this.state.operationIndex].colType === "String"} checked={this.state.formPayload.type === "mean"} onChange={this.transformationFormChangeHandler} />
																						<span className="radioMark" />
																					</label>
																				</div>
																				<div className="flex-33 form-group-label">
																					<label className="radioLabel">Mode
																			<input name="type" type="radio" value="mode" checked={this.state.formPayload.type === "mode"} onChange={this.transformationFormChangeHandler} />
																						<span className="radioMark" />
																					</label>
																				</div>
																			</div>
																			<div className="bottom-button text-right">
																				<button type="submit" className="btn btn-primary m-0">Save</button>
																			</div>
																		</form>
																	}

																	{/* ________________ Handling All N/A ________________ */}
																	{this.state.selectedAction === "treatAllMissingValues" &&
																		<div className="card-message">
																			<p className='handlingAll'>Handling All doesn't require any parameter</p>
																		</div>
																	}

																	{/* ________________ Unique Column ________________   ________________ Quassi Constant  ________________ */}
																	{(this.state.selectedAction === "uniqueColumn" || this.state.selectedAction === "quasiConstant") &&
																		<form className="contentForm" onSubmit={this.actionSaveHandler}>
																			<div className="form-group">
																				<div className="d-flex">
																					<label className="flex-25 pl-3 form-group-label">Tolerance:</label>
																					<p className="flex-20 fw-700 pd-t-2 text-theme">{this.state.formPayload.tolerance}</p>
																					<div className="flex-100 d-flex px-3">
																						<label className="radioLabel form-group-label pr-3" >0</label>
																						<Slider
																							axis="x"
																							xmin={0}
																							xmax={1}
																							xstep={0.01}
																							x={this.state.formPayload.tolerance}
																							onChange={({ x }) => { this.SliderChangeHandler(x) }}
																						/>
																						<label className="radioLabel pl-3 form-group-label">1</label>
																					</div>
																				</div>
																			</div>
																			<div className="bottom-button text-right">
																				<button type="submit" className="btn btn-primary m-0">Save</button>
																			</div>
																		</form>
																	}

																	{/* ________________ Drop Column  ________________ */}
																	{this.state.selectedAction === "dropColumn" &&
																		<div className="card-message">
																			<p className='handlingAll'>Drop Column doesn't require any parameter</p>
																		</div>
																	}

																	{/* ________________ Drop Duplicate  ________________ */}
																	{this.state.selectedAction === "dropDuplicates" &&
																		<div className="card-message">
																			<p className='handlingAll'>Drop Duplicate doesn't require any parameter</p>
																		</div>
																	}

																	{/* ________________ Regrex Replace  ________________ */}
																	{this.state.selectedAction === "regrexReplace" &&
																		<form className="contentForm" onSubmit={this.actionSaveHandler}>
																			<div className="form-group">
																				<label className="form-group-label">Replace Except :<i className="fas fa-asterisk form-group-required"></i></label>
																				<input className="form-control" type="text" required id="replaceExcept" value={this.state.formPayload.replaceExcept} onChange={this.transformationFormChangeHandler} placeholder="indexes" />

																			</div>
																			<div className="form-group">
																				<label className="form-group-label">Replace With :<i className="fas fa-asterisk form-group-required"></i></label>
																				<input className="form-control" type="text" required id="replaceWith" value={this.state.formPayload.replaceWith} onChange={this.transformationFormChangeHandler} placeholder="indexes" />

																			</div>
																			<div className="bottom-button text-right">
																				<button type="submit" className="btn btn-primary m-0">Save</button>
																			</div>
																		</form>
																	}

																	{/* ________________ Select Multiple Columns  ________________ */}
																	{this.state.selectedAction === "selectColumn" &&
																		<form className="contentForm" onSubmit={this.actionSaveHandler}>
																			<div className="form-group inheritedForm">
																				<label className="form-group-label">Select Column :</label>
																				<ReactSelect
																					value={this.state.selectedInputCols}
																					onChange={this.handleMultipleColumnsToolChange}
																					options={this.createOptionsForMultiColumns()}
																					isMulti={true}
																				/>
																			</div>
																			<div className="bottom-button text-right">
																				<button type="submit" className="btn btn-primary m-0">Save</button>
																			</div>
																		</form>
																	}

																	{/* ________________ Encoding  ________________ */}
																	{this.state.selectedAction === "encoding" &&
																		<form className="contentForm" onSubmit={this.actionSaveHandler}>
																			<div className="inheritedForm">
																				<label className="form-group-label">Select Encoding :</label>
																				<ReactSelect
																					value={this.state.selectedEncoder}
																					onChange={this.handleChangeForReactSelect}
																					options={optionsForEncoder}
																					isMulti={false}
																					isOptionDisabled={(option) => { return !option.type.includes(this.state.payload.cleaningAndTransformConfig && this.state.payload.cleaningAndTransformConfig.operations[this.state.operationIndex].colType) }}
																				/>
																			</div>


																			{/* (1)_______________Count vectorize________________ */}
																			{this.state.formPayload.encoder === "countVectorize" &&
																				<React.Fragment>
																					<div className="form-group">
																						<label className="form-group-label">Min DF :<i className="fas fa-asterisk form-group-required"></i></label>
																						<input className="form-control" type="number" id="minDf" value={this.state.formPayload.minDf} onChange={this.transformationFormChangeHandler} placeholder="indexes" />

																					</div>
																					<div className="form-group">
																						<label className="form-group-label">Vocab Size :<i className="fas fa-asterisk form-group-required"></i> </label>
																						<input className="form-control" type="number" id="vocabSize" value={this.state.formPayload.vocabSize} onChange={this.transformationFormChangeHandler} placeholder="indexes" />

																					</div>
																					<div className="form-group">
																						<label className="form-group-label">Output Column :<i className="fas fa-asterisk form-group-required"></i> </label>
																						<input className="form-control" type="text" id="outputCol" value={this.state.formPayload.outputCol} onChange={this.transformationFormChangeHandler} placeholder="indexes" />

																					</div>
																					<div className="form-group">
																						<div className="d-flex">
																							<label className="flex-100">Drop Original Column:</label>
																							<div className="flex-30">
																								<label className="radioLabel form-group-label">True
																						<input name="dropColumn" type="radio" value={"true"} checked={this.state.formPayload.dropColumn === "true"} onChange={this.transformationFormChangeHandler} />
																									<span className="radioMark" />
																								</label>
																							</div>
																							<div className="flex-30">
																								<label className="radioLabel form-group-label">False
																						<input name="dropColumn" type="radio" value={"false"} checked={this.state.formPayload.dropColumn === "false"} onChange={this.transformationFormChangeHandler} />
																									<span className="radioMark" />
																								</label>
																							</div>
																						</div>
																					</div>
																					<div className="bottom-button text-right">
																						<button type="submit" className="btn btn-primary m-0">Save</button>
																					</div>
																				</React.Fragment>}


																			{/* (2)_________________Onehotencoder________________ */}
																			{this.state.formPayload.encoder === "oneHotEncoder" &&
																				<React.Fragment>
																					<div className="form-group inheritedForm">
																						<label className="form-group-label">Select Column :</label>
																						<ReactSelect
																							value={this.state.selectedInputCols}
																							onChange={(value, type) => this.handleInputColumnChange(value, type, false)}
																							options={this.createOptionsForReactSelect()}
																							isMulti={false}
																						/>
																					</div>
																					<div className="form-group">
																						<label className="form-group-label">Output column :<i className="fas fa-asterisk form-group-required"></i> </label>
																						<input className="form-control" type="text" id="outputCols" value={this.state.formPayload.outputCols} onChange={this.transformationFormChangeHandler} placeholder="indexes" />
																					</div>
																					<div className="form-group">
																						<div className="d-flex">
																							<label className="flex-100 form-group-label">Drop Original Column :</label>
																							<div className="flex-30">
																								<label className="radioLabel form-group-label">True
																							<input name="dropColumn" type="radio" value={"true"} checked={this.state.formPayload.dropColumn === "true"} onChange={this.transformationFormChangeHandler} />
																									<span className="radioMark" />
																								</label>
																							</div>
																							<div className="flex-30">
																								<label className="radioLabel form-group-label">False
																							<input name="dropColumn" type="radio" value={"false"} checked={this.state.formPayload.dropColumn === "false"} onChange={this.transformationFormChangeHandler} />
																									<span className="radioMark" />
																								</label>
																							</div>
																						</div>
																					</div>
																					<div className="bottom-button text-right">
																						<button type="submit" className="btn btn-primary m-0">Save</button>
																					</div>
																				</React.Fragment>}


																			{/* (3)_________________Vector Assemblers________________ */}
																			{this.state.formPayload.encoder === "vectorAssemblers" &&
																				<React.Fragment>
																					<div className="form-group inheritedForm">
																						<label className="form-label">Select Column :</label>
																						<ReactSelect
																							value={this.state.selectedInputCols}
																							onChange={(value, type) => this.handleInputColumnChange(value, type, true)}
																							options={this.createOptionsForReactSelect()}
																							isMulti={true}
																						/>
																					</div>
																					<div className="form-group">
																						<label className="form-group-label">Output column :<i className="fas fa-asterisk form-group-required"></i> </label>
																						<input className="form-control" type="text" id="outputCol" value={this.state.formPayload.outputCol} onChange={this.transformationFormChangeHandler} placeholder="indexes" />

																					</div>
																					<div className="form-group">
																						<div className="d-flex">
																							<label className="flex-100 form-group-label">Drop Original Column:</label>
																							<div className="flex-30">
																								<label className="radioLabel form-group-label">True
																							<input name="dropColumn" type="radio" value={"true"} checked={this.state.formPayload.dropColumn === "true"} onChange={this.transformationFormChangeHandler} />
																									<span className="radioMark" />
																								</label>
																							</div>
																							<div className="flex-30">
																								<label className="radioLabel form-group-label">False
																							<input name="dropColumn" type="radio" value={"false"} checked={this.state.formPayload.dropColumn === "false"} onChange={this.transformationFormChangeHandler} />
																									<span className="radioMark" />
																								</label>
																							</div>
																						</div>
																					</div>
																					<div className="bottom-button text-right">
																						<button type="submit" className="btn btn-primary m-0">Save</button>
																					</div>
																				</React.Fragment>}


																			{/* (4)_________________ Binaries ________________ */}
																			{this.state.formPayload.encoder === "binaries" &&
																				<React.Fragment>
																					<div className="form-group inheritedForm">
																						<label className="form-group-label">Select Column :</label>
																						<ReactSelect
																							value={this.state.selectedInputCols}
																							onChange={(value, type) => this.handleInputColumnChange(value, type, false)}
																							options={this.createOptionsForReactSelect()}
																							isMulti={true}
																						/>
																					</div>
																					<div className="form-group">
																						<div className="d-flex">
																							<label className="flex-30">Threshold:</label>
																							<p className="flex-20 fw-700 pd-t-2 text-theme">{this.state.formPayload.threshold}</p>
																							<div className="flex-100 d-flex">
																								<label className="radioLabel pd-0 mr-r-16 form-group-label">0</label>
																								<Slider
																									axis="x"
																									xmin={0}
																									xmax={1}
																									xstep={0.01}
																									x={this.state.formPayload.threshold}
																									onChange={({ x }) => { this.SliderChangeHandler(x, "threshold") }}
																								/>
																								<label className="radioLabel pd-0 mr-l-16 form-group-label">1</label>
																							</div>
																						</div>
																					</div>
																					<div className="form-group">
																						<div className="d-flex">
																							<label className="flex-100 form-group-label">Drop Original Column:</label>
																							<div className="flex-30">
																								<label className="radioLabel form-group-label">True
																							<input name="dropColumn" type="radio" value={"true"} checked={this.state.formPayload.dropColumn === "true"} onChange={this.transformationFormChangeHandler} />
																									<span className="radioMark" />
																								</label>
																							</div>
																							<div className="flex-30">
																								<label className="radioLabel form-group-label">False
																							<input name="dropColumn" type="radio" value={"false"} checked={this.state.formPayload.dropColumn === "false"} onChange={this.transformationFormChangeHandler} />
																									<span className="radioMark" />
																								</label>
																							</div>
																						</div>
																					</div>
																					<div className="bottom-button text-right">
																						<button type="submit" className="btn btn-primary m-0">Save</button>
																					</div>
																				</React.Fragment>
																			}

																			{/* (5)_________________Stop Words Remover________________ */}
																			{this.state.formPayload.encoder === "stopWordsRemover" &&
																				<React.Fragment>
																					<div className="form-group inheritedForm">
																						<label className="form-group-label">Select Column :</label>
																						<ReactSelect
																							value={this.state.selectedInputCols}
																							onChange={(value, type) => this.handleInputColumnChange(value, type, false)}
																							options={this.createOptionsForReactSelect()}
																							isMulti={false}
																						/>
																					</div>
																					<div className="form-group">
																						<label className="form-group-label">Output Column : <i className="fas fa-asterisk form-group-required"></i> </label>
																						<input className="form-control" type="text" id="outputCol" value={this.state.formPayload.outputCol} onChange={this.transformationFormChangeHandler} placeholder="indexes" />

																					</div>
																					<div className="form-group">
																						<div className="d-flex">
																							<label className="flex-100 form-group-label">Drop Original Column:</label>
																							<div className="flex-30">
																								<label className="radioLabel form-group-label">True
																						<input name="dropColumn" type="radio" value={"true"} checked={this.state.formPayload.dropColumn === "true"} onChange={this.transformationFormChangeHandler} />
																									<span className="radioMark" />
																								</label>
																							</div>
																							<div className="flex-30">
																								<label className="radioLabel form-group-label">False
																						<input name="dropColumn" type="radio" value={"false"} checked={this.state.formPayload.dropColumn === "false"} onChange={this.transformationFormChangeHandler} />
																									<span className="radioMark" />
																								</label>
																							</div>
																						</div>
																					</div>
																					<div className="bottom-button text-right">
																						<button type="submit" className="btn btn-primary m-0">Save</button>
																					</div>
																				</React.Fragment>
																			}

																			{/* (6)_________________String Indexer________________ */}
																			{this.state.formPayload.encoder === "stringIndexer" &&
																				<React.Fragment>
																					<div className="form-group inheritedForm">
																						<label className="form-group-label">Select Column :</label>
																						<ReactSelect
																							value={this.state.selectedInputCols}
																							onChange={(value, type) => this.handleInputColumnChange(value, type, false)}
																							options={this.createOptionsForReactSelect()}
																							isMulti={false}
																						/>
																					</div>
																					<div className="form-group">
																						<label className="form-group-label">Output Column :<i className="fas fa-asterisk form-group-required"></i> </label>
																						<input className="form-control" type="text" id="outputCol" value={this.state.formPayload.outputCol} onChange={this.transformationFormChangeHandler} placeholder="indexes" />

																					</div>
																					<div className="form-group">
																						<div className="d-flex">
																							<label className="flex-100 form-group-label">Drop Original Column:</label>
																							<div className="flex-30">
																								<label className="radioLabel form-group-label">True
																							<input name="dropColumn" type="radio" value={"true"} checked={this.state.formPayload.dropColumn === "true"} onChange={this.transformationFormChangeHandler} />
																									<span className="radioMark" />
																								</label>
																							</div>
																							<div className="flex-30">
																								<label className="radioLabel form-group-label">False
																							<input name="dropColumn" type="radio" value={"false"} checked={this.state.formPayload.dropColumn === "false"} onChange={this.transformationFormChangeHandler} />
																									<span className="radioMark" />
																								</label>
																							</div>
																						</div>
																					</div>
																					<div className="bottom-button text-right">
																						<button type="submit" className="btn btn-primary m-0">Save</button>
																					</div>
																				</React.Fragment>
																			}
																		</form>
																	}

																	{this.state.selectedAction === "transformation" &&
																		<form className="contentForm" onSubmit={this.actionSaveHandler}>
																			<div className="form-group">
																				<label className="form-group-label">Column :<i className="fas fa-asterisk form-group-required"></i> </label>
																				<select className="form-control" id="transform" required value={this.state.formPayload.transform} onChange={this.transformationFormChangeHandler}>
																					<option value=''>Select</option>
																					<option value='renameColumn'>Rename Column</option>
																					<option value='targetColumn'>Target Column</option>
																					<option value='splitData'>Split Data</option>
																				</select>

																			</div>
																			{this.state.formPayload.transform === 'renameColumn' &&
																				<React.Fragment>
																					<div className="form-group">
																						<label className="form-group-label">New Column Name :<i className="fas fa-asterisk form-group-required"></i> </label>
																						<input className="form-control" type="text" id="newColumnName" value={this.state.formPayload.newColumnName} onChange={this.transformationFormChangeHandler} placeholder="indexes" />

																					</div>
																					<div className="bottom-button text-right">
																						<button type="submit" className="btn btn-primary m-0">Save</button>
																					</div>
																				</React.Fragment>
																			}
																			{this.state.formPayload.transform === 'targetColumn' &&
																				<React.Fragment>
																					<div className="form-group">
																						<p className='handlingAll'>Target Column doesn't require any parameter</p>
																					</div>
																					<div className="bottom-button text-right">
																						<button type="submit" className="btn btn-primary m-0">Save</button>
																					</div>
																				</React.Fragment>
																			}
																			{this.state.formPayload.transform === 'splitData' &&
																				<React.Fragment>
																					<div className="d-flex">
																						<label className="flex-20 form-group-label">Percent:</label>
																						<p className="flex-20 fw-700 pd-t-2 text-theme">{this.state.formPayload.percent}</p>
																						<div className="flex-100 d-flex">
																							<label className="radioLabel pd-0 mr-r-16 form-group-label">0</label>
																							<Slider
																								axis="x"
																								xmin={0}
																								xmax={1}
																								xstep={0.01}
																								x={this.state.formPayload.percent}
																								onChange={({ x }) => { this.SliderChangeHandler(x, "percent") }}
																							/>
																							<label className="radioLabel p-0 mr-l-16 form-group-label">1</label>
																						</div>
																					</div>
																					<div className="bottom-button text-right">
																						<button type="submit" className="btn btn-primary m-0">Save</button>
																					</div>
																				</React.Fragment>
																			}
																		</form>
																	}
																</div>
															</div>
														</div>
													</div>
												</React.Fragment>
											</div>
										</div>

										<div className="tab-pane fade" id="featureSelection">

											<div className="d-flex">

												<div className="flex-100">
													<div className="card">
														<div className="card-header">
															<h6>Select Feature</h6>
														</div>

														<div className="card-body">
															<ul className="list-style-none d-flex feature-list draggable-list">
																{featureList.map((feature, index) => {
																	return (
																		<li key={index} className="flex-25" draggable onDragStart={() => this.onFeatureDrag(feature.value)}> <img src={`https://content.iot83.com/m83/mlStudio/${feature.imageName}.png`} />{feature.displayName}</li>
																	)
																})}
															</ul>
														</div>
													</div>
												</div>

												<div className="flex-50 pd-r-7">
													<div className="card mb-0">
														<div className="card-header">
															<h6>Selected Feature</h6>
														</div>

														<div className="card-body">
															<div className="card-body-height">
																<div className="condition-box py-3 mt-0">

																	<div className="selection-outerBox">
																		{this.state.payload.featureSelectionConfig && this.state.payload.featureSelectionConfig.techniques.map((feature, featureIndex) => {
																			let displayName = featureList.find(el => el.value === feature.actionName).displayName
																			return (
																				<div key={featureIndex} className="selection-text" >
																					<p onClick={(e) => { this.onFeatureSelect(e, featureIndex, feature.actionName) }}><i className="fad fa-book"></i>{displayName}
																						<button type="button" className="btn-transparent btn-transparent-blue" data-tooltip="true" data-tooltip-text={feature.isCompleted ? 'Completed' : "InComplete"} data-tooltip-place="bottom">
																							<i className={feature.isCompleted ? 'fas fa-check' : 'fad fa-question'}></i>
																						</button>
																					</p>
																					<button type="button" className="btn-transparent btn-transparent-red" data-tooltip="true" data-tooltip-text="Delete" data-tooltip-place="bottom" onClick={(e) => this.deleteFeature(e, featureIndex)}>
																						<i className="far fa-trash-alt"></i>
																					</button>
																				</div>)
																		})}
																	</div>

																	<div className="file-upload-box" onDrop={(e) => this.onFeatureDrop(e)} onDragOver={(event) => this.allowDrop(event)}>
																		<div className="file-upload-form">
																			<p>Drag & Drop Feature Here..!</p>
																		</div>
																	</div>


																</div>
															</div>
														</div>
													</div>
												</div>

												<div className="flex-50 pd-l-7">
													<div className="card mb-0">
														<div className="card-header">
															{this.state.selectedFeature ?
																<div className="form-section-header">
																	<h6>Properties<i className="fas fa-greater-than mr-r-10 mr-l-10 f-12"></i> {featureList.find(el => el.value === this.state.selectedFeature).displayName}</h6>
																</div> :
																<div className="form-section-header">
																	<h6 className="fw-600">Properties</h6>
																</div>
															}
														</div>

														<div className="card-body">
															<div className="card-body-height">
																{!this.state.selectedFeature &&
																	<div className="card-message">
																		<h5 className="f-13">Select Atleast One Feature</h5>
																	</div>
																}
																{this.state.selectedFeature === "correlationRemoval" &&
																	<form className="contentForm" onSubmit={this.featureSaveHandler}>
																		<div className="d-flex form-group">
																			<label className="flex-20 form-group-label">Percent:</label>
																			<p className="flex-20 pd-t-2 fw-700 text-theme">{this.state.featurePayload.threshold}</p>
																			<div className="flex-100 d-flex">
																				<label className="radioLabel pr-3 form-group-label">0.1</label>
																				<Slider
																					axis="x"
																					xmin={0.1}
																					xmax={0.7}
																					xstep={0.01}
																					x={this.state.featurePayload.threshold}
																					onChange={({ x }) => { this.SliderChangeHandlerForFeatureSelection(x, "threshold") }}
																				/>
																				<label className="radioLabel pl-3 form-group-label">0.7</label>
																			</div>
																		</div>
																		<div className="form-group">
																			<label className="form-group-label">Target Column: <i className="fas fa-asterisk form-group-required"></i></label>
																			<select className="form-control" id="targetColumn" required disabled={this.state.isFetchingColumns} value={this.state.featurePayload.targetColumn} onChange={this.featureFormChangeHandler}>
																				<option value=''>Select</option>
																				{this.state.columnsList.map((column, columnIndex) => {
																					return (<option key={columnIndex} value={column.keys}>{column.keys}</option>)
																				}
																				)}
																			</select>
																		</div>
																		<div className="bottom-button selection-save-button text-right">
																			<button type="submit" className="btn btn-primary m-0">Save</button>
																		</div>
																	</form>
																}
																{this.state.selectedFeature === "informationGain" &&
																	<form className="contentForm" onSubmit={this.featureSaveHandler}>
																		<div className="form-group">
																			<label className="form-group-label">Target Column: <i className="fas fa-asterisk form-group-required"></i></label>
																			<select className="form-control" id="targetColumn" disabled={this.state.informationGainLoader || this.state.isFetchingColumns} required value={this.state.featurePayload.targetColumn} onChange={this.featureFormChangeHandler}>
																				<option value=''>Select</option>
																				{this.state.columnsList.map((column, columnIndex) => {
																					return (<option key={columnIndex} value={column.keys}>{column.keys}</option>)
																				}
																				)}
																			</select>
																		</div>
																		<div className="form-group">
																			<label className="form-group-label">Target Column Type :<i className="fas fa-asterisk form-group-required"></i></label>
																			<select className="form-control" id="targetColumnType" disabled={this.state.informationGainLoader} required value={this.state.featurePayload.targetColumnType} onChange={this.featureFormChangeHandler}>
																				<option value='categoricalType'>Categorical Type</option>
																				<option value='regressionType'>Regression Type</option>
																			</select>
																		</div>
																		{Object.keys(this.state.informationGainData).length ?
																			<div className="d-flex">
																				<label className="flex-20">Percent:</label>
																				<p className="flex-20 pd-t-2 fw-700 text-theme">{this.state.featurePayload.k}</p>
																				<div className="flex-60">
																					<label className="radioLabel pd-0 mr-r-16">1</label>
																					<Slider
																						axis="x"
																						xmin={1}
																						xmax={this.state.informationGainData.dataKeys.length}
																						xstep={1}
																						x={this.state.featurePayload.k}
																						onChange={({ x }) => { this.SliderChangeHandlerForFeatureSelection(x, "k") }}
																					/>
																					<label className="radioLabel pd-0 mr-l-16">{this.state.columnsList.length}</label>
																				</div>
																			</div> : null}
																		{Object.keys(this.state.informationGainData).length ? <button type="button" className="btn btn-link" data-toggle="modal" data-target="#informationGainModal" ><i className="far fa-plus"></i>Show Information Gain Data</button> : null}
																		<div className="text-right">
																			<button type="button" disabled={(!this.state.featurePayload.targetColumn || this.state.informationGainLoader)} onClick={() => {
																				let payload = {
																					dataSource: cloneDeep(this.state.payload.dataSource),
																					action: {
																						type: "showStatistics",
																						actionName: "informationGain",
																						targetColumn: this.state.featurePayload.targetColumn,
																						targetColumnType: this.state.featurePayload.targetColumnType
																					}
																				};
																				this.props.getStatistics(payload, "informationGainLoader");
																				this.setState({
																					informationGainLoader: true
																				})
																			}} className="btn btn-success m-0">Get Dependant Columns Number</button>
																		</div>
																		<div className="bottom-button selection-save-button text-right">
																			<button type="submit" disabled={!this.state.featurePayload.k || this.state.informationGainLoader} className="btn btn-primary m-0">Save</button>
																		</div>
																	</form>
																}
																{this.state.selectedFeature === "chiSqTest" &&
																	<form className="contentForm" onSubmit={this.featureSaveHandler}>
																		<div className="d-flex">
																			<label className="flex-25 form-group-label">Percent:</label>
																			<p className="flex-20 fw-700 pd-t-2 text-theme">{this.state.featurePayload.k}</p>
																			<div className="flex-100 d-flex">
																				<label className="radioLabel form-group-label pr-3">1</label>
																				<Slider
																					axis="x"
																					xmin={1}
																					xmax={this.state.columnsList.length}
																					xstep={1}
																					x={this.state.featurePayload.k}
																					onChange={({ x }) => { this.SliderChangeHandlerForFeatureSelection(x, "k") }}
																				/>
																				<label className="radioLabel form-group-label pl-3">{this.state.columnsList.length}</label>
																			</div>
																		</div>
																		<div className="bottom-button selection-save-button text-right">
																			<button type="submit" className="btn btn-primary m-0">Save</button>
																		</div>
																	</form>
																}
															</div>
														</div>
													</div>
												</div>
											</div>

										</div>

										<div className="tab-pane fade" id="modelBuilding">
											<div className="d-flex">

												<div className="flex-50 pd-r-7">
													<div className="card">
														<div className="card-header">
															<h6>Select Algorithm</h6>
														</div>

														<div className="card-body px-0">
															<div className="card-body-height card-overflow-scroll collapse-box px-3">
																<div className="collapse-box">
																	{algorithmsList.map((algorithm, index) => {
																		return (
																			<React.Fragment key={index}>
																				<p data-tip data-for={`alogo${index}`} className="accordion-link" data-toggle="collapse" data-target={`#collapse${index}`} onClick={() => this.selectAlgorithm("name", algorithm.name)}>
																					{algorithm.name}
																				</p>
																				<ReactTooltip id={`alogo${index}`} place="right" type="dark">
																					<div className="tooltipText">
																						<p>{algorithm.toolTipText}</p>
																					</div>
																				</ReactTooltip>
																				<div id={`collapse${index}`} className={this.state.payload.algorithm && this.state.payload.algorithm.algorithmName === algorithm.name ? "collapse show" : "collapse"}>
																					<ul className="list-style-none d-flex flex-column pl-3">
																						{algorithm.children.map((children, childIndex) => <li key={childIndex}> <i className="fad fa-chart-area"></i> <span onClick={() => this.selectAlgorithm("type", children.value)}>{children.name}</span></li>)}
																					</ul>
																				</div>
																			</React.Fragment>
																		)
																	})
																	}
																</div>
															</div>
														</div>
													</div>
												</div>

												<div className="flex-50 pd-l-7">
													<div className="selected-item-box p-0 full-img-wrapper">
														<div className="selected-item ">
															<i className="fas fa-times-circle cancel text-content"></i>
															<div className="d-flex selected-info-box">
																<img src="https://content.iot83.com/m83/mlStudio/algo.png" />

																<div className="d-flex selected-info-name">
																	<div className="flex-50 selected-info-image">
																		<img src="https://content.iot83.com/m83/mlStudio/XGBoostLogo.png" />
																	</div>
																	<div className="flex-50">
																		<p className="m-0 pd-l-10"><strong className="fw-700">{this.state.payload.algorithm && this.state.payload.algorithm.algorithmName}</strong></p>
																		<p className="m-0 pd-l-10 fw-600">{this.state.payload.algorithm && this.state.payload.algorithm.algorithmType}</p>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>

												<div className="flex-100">
													<div className="card mb-0">
														<div className="card-header">
															{this.state.payload.algorithm && this.state.payload.algorithm.algorithmType ?
																<h6>Parameters <i className="fas fa-greater-than mr-r-10 mr-l-10 f-12"></i> {this.state.payload.algorithm && this.state.payload.algorithm.algorithmName} <i className="fas fa-greater-than mr-r-10 mr-l-10 f-12"></i> {this.state.payload.algorithm.algorithmType === "linear-learner" ? "Linear-Learner" : this.state.payload.algorithm.algorithmType}</h6> :
																<h6>Parameters</h6>
															}
														</div>

														<div className="card-body">
															{this.state.payload.algorithm && !this.state.payload.algorithm.algorithmType &&
																<div className="card-message">
																	<h5 className="text-center f-13">Select Atleast One Algorithm</h5>
																</div>
															}
															{this.state.payload.algorithm && this.state.payload.algorithm.algorithmType === "XgBoost" &&
																<form className="contentForm">
																	<div className="form-group">
																		<label className="form-group-label">Training Job Name : <i className="fas fa-asterisk form-group-required"></i> </label>
																		<input type="text" className="form-control" id="trainingJobName" value={this.state.payload.algorithm && this.state.payload.algorithm.trainingJobName} onChange={this.algorithmFormHandler} placeholder="indexes" />
																	</div>
																	<div className="form-group form-group-headings">
																		<h6>HyperParameters</h6>
																	</div>
																	<div className="d-flex">
																		<div className="form-group flex-50 pd-r-6">
																			<label className="form-group-label">Objective : <i className="fas fa-asterisk form-group-required"></i> </label>
																			<input type="text" className="form-control" id="objective" defaultValue={this.state.payload.algorithm && this.state.payload.algorithm.hyperparams.objective} readOnly={true} placeholder="indexes" />
																		</div>

																		<div className="form-group flex-50 pd-l-6">
																			<label className="form-group-label">ETA : <i className="fas fa-asterisk form-group-required"></i> </label>
																			<input type="text" id="eta" className="form-control" value={this.state.payload.algorithm && this.state.payload.algorithm.hyperparams.eta} onChange={this.algorithmFormHandler} placeholder="indexes" />
																		</div>
																	</div>
																	<div className="form-group">
																		<label className="form-group-label">Sub Sample : <i className="fas fa-asterisk form-group-required"></i> </label>
																		<input type="text" id="subsample" className="form-control" value={this.state.payload.algorithm && this.state.payload.algorithm.hyperparams.subsample} onChange={this.algorithmFormHandler} placeholder="indexes" />
																	</div>
																	<div className="form-group">
																		<label className="form-group-label">num_round : <i className="fas fa-asterisk form-group-required"></i></label>
																		<input type="text" id="num_round" className="form-control" value={this.state.payload.algorithm && this.state.payload.algorithm.hyperparams.num_round} onChange={this.algorithmFormHandler} placeholder="indexes" />
																	</div>

																	<div className="d-flex">
																		<label className="flex-20 pd-l-10">Train Value:</label>
																		<p className="flex-20 fw-700 pd-t-2 text-theme">{this.state.payload.algorithm && this.state.payload.algorithm.hyperparams.trainSplitValue}</p>
																		<div className="flex-60">
																			<label className="radioLabel pd-0 mr-r-16">0</label>
																			<Slider
																				axis="x"
																				xmin={0}
																				xmax={1}
																				xstep={0.1}
																				x={this.state.payload.algorithm && this.state.payload.algorithm.hyperparams.trainSplitValue}
																				onChange={({ x }) => { this.SliderChangeHandlerForAlgorithm(x, "trainSplitValue") }}
																			/>
																			<label className="radioLabel pd-0 mr-l-16">1</label>
																		</div>
																	</div>
																	<div className="d-flex">
																		<label className="flex-20 pd-l-10">Test Value:</label>
																		<p className="flex-20 fw-700 pd-t-2 text-theme">{this.state.payload.algorithm && this.state.payload.algorithm.hyperparams.testSplitValue}</p>
																		<div className="flex-60">
																			<label className="radioLabel pd-0 mr-r-16">0</label>
																			<Slider
																				axis="x"
																				xmin={0}
																				xmax={1}
																				xstep={0.1}
																				x={this.state.payload.algorithm && this.state.payload.algorithm.hyperparams.testSplitValue}
																				onChange={({ x }) => { this.SliderChangeHandlerForAlgorithm(x, "testSplitValue") }}
																			/>
																			<label className="radioLabel pd-0 mr-l-16">1</label>
																		</div>
																	</div>

																	{/* <div className="modal-footer text-right px-0">
																<button type="submit" className="btn btn-primary">Save</button>
																<button type="button" className="btn btn-dark">Cancel</button>
															</div> */}
																</form>
															}
															{this.state.payload.algorithm && this.state.payload.algorithm.algorithmType === "linear-learner" &&
																<form className="contentForm">
																	<div className="form-group">
																		<label className="form-group-label">Training Job Name : <i className="fas fa-asterisk form-group-required"></i></label>
																		<input type="text" id="trainingJobName" className="form-control" value={this.state.payload.algorithm && this.state.payload.algorithm.trainingJobName} onChange={this.algorithmFormHandler} placeholder="indexes" />
																	</div>
																	<div className="form-group form-group-headings">
																		<h6>HyperParameters</h6>
																	</div>
																	<div className="form-group">
																		<label className="form-group-label">Feature_Dim : <i className="fas fa-asterisk form-group-required"></i></label>
																		<input type="text" id="feature_dim" className="form-control" value={this.state.payload.algorithm && this.state.payload.algorithm.hyperparams.feature_dim} onChange={this.algorithmFormHandler} placeholder="indexes" />
																	</div>
																	<div className="form-group">
																		<label className="form-group-label">predictor_type : <i className="fas fa-asterisk form-group-required"></i></label>
																		<select className="form-control" id="predictor_type" value={this.state.payload.algorithm && this.state.payload.algorithm.hyperparams.predictor_type} onChange={this.algorithmFormHandler}>
																			<option value='binary_classifier'>Binary Classifier</option>
																			<option value='multiclass_classifier'>Multi Class Classifier</option>
																		</select>
																	</div>
																	<div className="form-group">
																		<label className="form-group-label">Target Column : <i className="fas fa-asterisk form-group-required"></i></label>
																		<input className="form-control" type="text" id="target" value={this.state.payload.algorithm && this.state.payload.algorithm.hyperparams.target} onChange={this.algorithmFormHandler} placeholder="indexes" />
																	</div>
																	<div className="d-flex">
																		<label className="flex-20 pd-l-10">Train Value:</label>
																		<p className="flex-20 fw-700 pd-t-2 text-theme">{this.state.payload.algorithm && this.state.payload.algorithm.hyperparams.trainSplitValue}</p>
																		<div className="flex-60">
																			<label className="radioLabel pd-0 mr-r-16">0</label>
																			<Slider
																				axis="x"
																				xmin={0}
																				xmax={1}
																				xstep={0.1}
																				x={this.state.payload.algorithm && this.state.payload.algorithm.hyperparams.trainSplitValue}
																				onChange={({ x }) => { this.SliderChangeHandlerForAlgorithm(x, "trainSplitValue") }}
																			/>
																			<label className="radioLabel pd-0 mr-l-16">1</label>
																		</div>
																	</div>
																	<div className="d-flex">
																		<label className="flex-20 pd-l-10">Test Value:</label>
																		<p className="flex-20 fw-700 pd-t-2 text-theme">{this.state.payload.algorithm && this.state.payload.algorithm.hyperparams.testSplitValue}</p>
																		<div className="flex-60">
																			<label className="radioLabel pd-0 mr-r-16">0</label>
																			<Slider
																				axis="x"
																				xmin={0}
																				xmax={1}
																				xstep={0.1}
																				x={this.state.payload.algorithm && this.state.payload.algorithm.hyperparams.testSplitValue}
																				onChange={({ x }) => { this.SliderChangeHandlerForAlgorithm(x, "testSplitValue") }}
																			/>
																			<label className="radioLabel pd-0 mr-l-16">1</label>
																		</div>
																	</div>
																</form>
															}
															{this.state.payload.algorithm && this.state.payload.algorithm.algorithmType === "RandomCutForest" &&
																<form className="contentForm">
																	<div className="form-group">
																		<label className="form-group-label">Training Job Name : <i className="fas fa-asterisk form-group-required"></i></label>
																		<input type="text" id="trainingJobName" className="form-control" value={this.state.payload.algorithm && this.state.payload.algorithm.trainingJobName} onChange={this.algorithmFormHandler} placeholder="indexes" />
																	</div>
																	<div className="form-group form-group-headings">
																		<h6>HyperParameters</h6>
																	</div>
																	<div className="form-group">
																		<label className="form-group-label">Feature_Dim :</label>
																		<input type="text" id="feature_dim" className="form-control" value={this.state.payload.algorithm && this.state.payload.algorithm.hyperparams.feature_dim} onChange={this.algorithmFormHandler} placeholder="indexes" />
																	</div>
																	<div className="form-group">
																		<label className="form-group-label">NUM_SAMPLES_PER_TREE :</label>
																		<input type="text" id="num_samples_per_tree" className="form-control" value={this.state.payload.algorithm && this.state.payload.algorithm.hyperparams.num_samples_per_tree} onChange={this.algorithmFormHandler} placeholder="indexes" />
																	</div>
																	<div className="form-group">
																		<label className="form-group-label">NUM_TREE : <i className="fas fa-asterisk form-group-required"></i></label>
																		<input type="text" id="num_trees" className="form-control" value={this.state.payload.algorithm && this.state.payload.algorithm.hyperparams.num_trees} onChange={this.algorithmFormHandler} placeholder="indexes" />
																	</div>
																	<div className="form-group">
																		<label className="form-group-label">TRAIN_INSTANCE_COUNT : <i className="fas fa-asterisk form-group-required"></i></label>
																		<input type="text" id="train_instance_count" className="form-control" defaultValue={this.state.payload.algorithm && this.state.payload.algorithm.hyperparams.train_instance_count} placeholder="indexes" />
																	</div>
																</form>
															}
															{this.state.payload.algorithm && this.state.payload.algorithm.algorithmType === "K-Means" &&
																<form className="contentForm">
																	<div className="form-group">
																		<label className="form-group-label">Training Job Name : <i className="fas fa-asterisk form-group-required"></i></label>
																		<input type="text" id="trainingJobName" className="form-control" value={this.state.payload.algorithm && this.state.payload.algorithm.trainingJobName} onChange={this.algorithmFormHandler} placeholder="indexes" />
																	</div>
																	<div className="form-group form-group-headings">
																		<h6>HyperParameters</h6>
																	</div>
																	<div className="form-group">
																		<label className="form-group-label">Feature_Dim :</label>
																		<input type="text" id="feature_dim" className="form-control" value={this.state.payload.algorithm && this.state.payload.algorithm.hyperparams.feature_dim} onChange={this.algorithmFormHandler} placeholder="indexes" />
																	</div>
																	<div className="form-group">
																		<label className="form-group-label">K : <i className="fas fa-asterisk form-group-required"></i></label>
																		<input type="text" id="k" className="form-control" value={this.state.payload.algorithm && this.state.payload.algorithm.hyperparams.k} onChange={this.algorithmFormHandler} placeholder="indexes" />
																	</div>
																</form>
															}
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>

									{this.state.payload.type !== "machineLearning" &&
										<React.Fragment>
											{/*===== Pipeline View ====*/}
											<div className="card">
												<div className="card-header">
													<h6>Pipeline View</h6>
												</div>

												<div className="card-body">
													<ul className="pipeline-view d-flex flex-column list-style-none">
														<li>
															<div className="pipeline-text">
																<p>File Conversion and File Uploadation to S3</p>
															</div>
														</li>

														<li>
															<div className="pipeline-text">
																<p>Recognization and Uploadation of CSV file to S3</p>
															</div>
														</li>

														<li>
															<div className="pipeline-text">
																<p>Sentiment Analysis</p>
															</div>
														</li>

														<li>
															<div className="pipeline-text">
																<p>Key Words / Faulty Words Detection</p>
															</div>
														</li>

														<li>
															<div className="pipeline-text">
																<p>Word Cloud</p>
															</div>
														</li>

														<p className="text-center">
															<button type="button" className="btn btn-light">Stop</button>
														</p>
													</ul>
												</div>
											</div>

											<div className="card">
												<div className="card-header">
													<h6>Response / Result <span className="text-theme">(Step 3 Result)</span></h6>
												</div>

												<div className="card-body">
													<div className="content-table">
														<table className="table table-bordered m-0">
															<thead>
																<tr>
																	<th>Serial No.</th>
																	<th>From</th>
																	<th>To</th>
																	<th>Word</th>
																	<th>Count</th>
																</tr>
															</thead>
															<tbody>
																<tr>
																	<td>1</td>
																	<td>1.5</td>
																	<td>2.5</td>
																	<td>alpha</td>
																	<td>1</td>
																</tr>

																<tr>
																	<td>2</td>
																	<td>1.5</td>
																	<td>2.5</td>
																	<td>beta</td>
																	<td>1</td>
																</tr>
															</tbody>
														</table>
													</div>
												</div>
											</div>
											{/*===== End Pipeline View ====*/}


											{/*------------End When User Select Audio Sentiments -------------*/}




											{/*--------------- Start Auto ML Pipeline View ----------------*/}
											<div className="card">
												<div className="card-header">
													<h6>Pipeline View</h6>
												</div>

												<div className="card-body">
													<ul className="pipeline-view d-flex flex-column list-style-none">
														<li className="no-user-click">
															<div className="pipeline-text">
																<p>Data Cleaning</p>
															</div>
														</li>

														<li className="no-user-click">
															<div className="pipeline-text">
																<p>Feature Entration</p>
															</div>
														</li>

														<li>
															<div className="pipeline-text">
																<p>Model Training</p>
															</div>
														</li>

														<li>
															<div className="pipeline-text">
																<p>Model Deployment</p>
															</div>
														</li>

													</ul>
												</div>
											</div>

											{/*--------------- End Auto ML Pipeline View ----------------*/}


											{/*--------------- Start Result View Of Auto ML ----------------*/}
											<div className="card">
												<div className="card-header">
													<h6>Response / Result <span className="text-theme">(Model Training : Result)</span></h6>
												</div>

												<div className="card-body">
													<div className="content-table">
														<table className="table table-bordered m-0">
															<thead>
																<tr>
																	<th>Sample Prediction</th>
																	<th>Metrics</th>
																</tr>
															</thead>
															<tbody>
																<tr>
																	<td></td>
																	<td></td>
																</tr>

																<tr>
																	<td></td>
																	<td></td>
																</tr>
															</tbody>
														</table>
													</div>
												</div>
											</div>
											{/*--------------- End Result View Of Auto ML ----------------*/}
										</React.Fragment>
									}


								</div>


								<div className="form-info-wrapper pb-0 mb-0 ">
									<div className="form-info-header">
										<h5>{this.state.activeTab == "dataConfiguration" ? "Data Insight" : "Data Insight With Cleaning & Transformation"}</h5>
									</div>

									{this.state.activeTab == "dataConfiguration" ?
										<div className="form-info-body form-info-body-adjust data-insight-btns-group">
											<h5>Data Correlation</h5>
											<ul className="list-style-none form-info-list mb-4">
												<li>
													<p>Statiscal technique to show how strongly pairs of variables related.
											<button type="button" className="btn btn-link" name="dataCorrelation" disabled={!this.state.payload.dataSource.bucketId} onClick={this.getStatistics}>
															Apply
											</button>
													</p>
												</li>
											</ul>

											<h5>Show Statiscal Report</h5>
											<ul className="list-style-none form-info-list mb-4">
												<li>
													<p>Generates report for Exploratory data analysis.
											<button type="button" className="btn btn-link" name="showStatisticalReport" disabled={!this.state.payload.dataSource.bucketId} onClick={this.getStatistics}>
															Apply
											</button>
													</p>
												</li>
											</ul>

											<h5>Show All N/A</h5>
											<ul className="list-style-none form-info-list mb-4">
												<li>
													<p>Missing values in entire dataset.
											<button type="button" className="btn btn-link" name="showMissingValues" disabled={!this.state.payload.dataSource.bucketId} onClick={this.getStatistics}>
															Apply
											</button>
													</p>
												</li>
											</ul>

											<h5>Show Sample Data</h5>
											<ul className="list-style-none form-info-list mb-4">
												<li>
													<p>Display Sample Data.
											<button type="button" className="btn btn-link" name="showSampleData" disabled={!this.state.payload.dataSource.bucketId} onClick={this.getStatistics}>
															Apply
											</button>
													</p>
												</li>
											</ul>

											<h5>Feature Importance</h5>
											<ul className="list-style-none form-info-list mb-4">
												<li>
													<p>Feature importance refers to techniques that assign a score to input features based on how useful they are at predicting a target variable.
												<button type="button" className="btn btn-link" name="featureImportance" disabled={!this.state.payload.dataSource.bucketId} onClick={(event) => {
															event.stopPropagation();
															this.setState({
																featureImportanceLoader: false
															})
															$(`#${event.target.name}Modal`).modal()
														}}>
															Apply
												</button>
													</p>
												</li>
											</ul>

										</div> : <div className="form-info-body form-info-body-adjust data-insight-btns-group">
											<h5>Data Correlation</h5>
											<ul className="list-style-none form-info-list mb-4">
												<li>
													<p>Statiscal technique to show how strongly pairs of variables related.
											<button type="button" className="btn btn-link" name="dataCorrelationWithCleaningAndTransform" disabled={this.state.payload.cleaningAndTransformConfig && !this.state.payload.cleaningAndTransformConfig.operations[0].actions.length} onClick={(e) => this.getStatistics(e, true)}>
															Apply
											</button>
													</p>
												</li>
											</ul>

											<h5>Show Statiscal Report</h5>
											<ul className="list-style-none form-info-list mb-4">
												<li>
													<p>Generates report for Exploratory data analysis.
											<button type="button" className="btn btn-link" name="showStatisticalReportWithCleaningAndTransform" disabled={this.state.payload.cleaningAndTransformConfig && !this.state.payload.cleaningAndTransformConfig.operations[0].actions.length} onClick={(e) => this.getStatistics(e, true)}>
															Apply
											</button>
													</p>
												</li>
											</ul>

											<h5>Show All N/A</h5>
											<ul className="list-style-none form-info-list mb-4">
												<li>
													<p>Missing values in entire dataset.
											<button type="button" className="btn btn-link" name="showMissingValuesWithCleaningAndTransform" disabled={this.state.payload.cleaningAndTransformConfig && !this.state.payload.cleaningAndTransformConfig.operations[0].actions.length} onClick={(e) => this.getStatistics(e, true)}>
															Apply
											</button>
													</p>
												</li>
											</ul>

											<h5>Show Sample Data</h5>
											<ul className="list-style-none form-info-list mb-4">
												<li>
													<p>Display Sample Data.
											<button type="button" className="btn btn-link" name="showSampleDataWithCleaningAndTransform" disabled={this.state.payload.cleaningAndTransformConfig && !this.state.payload.cleaningAndTransformConfig.operations[0].actions.length} onClick={(e) => this.getStatistics(e, true)}>
															Apply
											</button>
													</p>
												</li>
											</ul>
										</div>}

								</div>


								<div className="form-info-wrapper form-info-wrapper-left">
									<div className="form-info-header"><h5>Basic Information</h5></div>
									<div className="form-info-body form-info-body-adjust">
										<div className="form-group">
											<label className="form-group-label">Experiment Name : <i className="fas fa-asterisk form-group-required"></i> </label>
											<input type="text" className="form-control" id="name" onChange={({ currentTarget }) => this.handleChange(currentTarget.value, "name")} value={this.state.payload.name} />
										</div>

										<div className="form-group">
											<label className="form-group-label">Description :</label>
											<textarea rows="3" type="text" id="description" className="form-control" onChange={({ currentTarget }) => this.handleChange(currentTarget.value, "description")} value={this.state.payload.description} />
										</div>
									</div>

									<div className="form-info-footer">
										<button type="button" className="btn btn-light" onClick={() => this.props.history.push('/mlExperiments')}>Cancel</button>
										<button type="button" className="btn btn-primary" onClick={this.saveExperimentHandler}>Save</button>
									</div>
								</div>
							</form>
						</div>

						<div className="modal animated slideInDown" id="showStatisticalReportModal">
							<div className="modal-dialog themeModal modal-xl modal-dialog-scrollable">
								{this.state.statisticalReportData.userId &&
									<div className="modal-content">
										<div className="modal-header">
											<h6 className="modal-title text-center">Transformation
                                    <button type="button" className="close" data-dismiss="modal"><i className="far fa-times"></i></button>
											</h6>
										</div>
										<div className="modal-body">
											<div className="reportBox">
												<ul className="nav nav-tabs">
													<li className="nav-item">
														<a className="nav-link active" data-toggle="tab" href="#home">OverView</a>
													</li>
													<li className="nav-item">
														<a className="nav-link" data-toggle="tab" href="#menu1">Variable</a>
													</li>
												</ul>

												<div className="tab-content">
													<div className="tab-pane active" id="home">
														<div className="reportBoxContent">
															<div className="reportBoxItem">
																<p>Overview</p>
															</div>
															<div className="flex">
																<div className="flex-item fx-b33">
																	<div className="contentTableBox">
																		<table className="table">
																			<thead>
																				<tr>
																					<th colSpan="2">
																						Dataset info
                                                                            </th>
																				</tr>
																			</thead>
																			<tbody>
																				<tr>
																					<td><strong>Number of variables</strong></td>
																					<td>{this.state.statisticalReportData.table.n_var}</td>
																				</tr>
																				<tr>
																					<td><strong>Number of observations</strong></td>
																					<td>{this.state.statisticalReportData.table.n}</td>
																				</tr>
																				<tr>
																					<td><strong>Total Missing (%)</strong></td>
																					<td>{this.state.statisticalReportData.table.p_cells_missing}</td>
																				</tr>
																				<tr>
																					<td><strong>Total size in memory</strong></td>
																					<td>{this.state.statisticalReportData.table.memory_size ? `${this.state.statisticalReportData.table.memory_size.__int64__} B` : ''}</td>
																				</tr>
																				<tr>
																					<td><strong>Average record size in memory</strong></td>
																					<td>{this.state.statisticalReportData.table.record_size ? `${this.state.statisticalReportData.table.record_size} B` : ''}</td>
																				</tr>
																			</tbody>
																		</table>
																	</div>
																</div>
																<div className="flex-item fx-b33">
																	<div className="contentTableBox">
																		<table className="table">
																			<thead>
																				<tr>
																					<th colSpan="2">
																						Variables types
                                                                            </th>
																				</tr>
																			</thead>
																			<tbody>
																				<tr>
																					<td><strong>Numeric</strong></td>
																					<td>{this.state.statisticalReportData.table.types && this.state.statisticalReportData.table.types.NUM ? `${this.state.statisticalReportData.table.types.NUM.__int64__} %` : ''}</td>
																				</tr>
																				<tr>
																					<td><strong>Categorical</strong></td>
																					<td>{this.state.statisticalReportData.table.types && this.state.statisticalReportData.table.types.NUM ? `${this.state.statisticalReportData.table.types.NUM.__int64__} %` : ''}</td>
																				</tr>
																				<tr>
																					<td><strong>Date</strong></td>
																					<td>{this.state.statisticalReportData.table.types && this.state.statisticalReportData.table.types.NUM ? `${this.state.statisticalReportData.table.types.NUM.__int64__} %` : ''}</td>
																				</tr>
																				<tr>
																					<td><strong>Text (Unique)</strong></td>
																					<td>{this.state.statisticalReportData.table.types && this.state.statisticalReportData.table.types.NUM ? `${this.state.statisticalReportData.table.types.NUM.__int64__} %` : ''}</td>
																				</tr>
																				<tr>
																					<td><strong>Rejected</strong></td>
																					<td>{this.state.statisticalReportData.table.types && this.state.statisticalReportData.table.types.NUM ? `${this.state.statisticalReportData.table.types.NUM.__int64__} %` : ''}</td>
																				</tr>
																			</tbody>
																		</table>
																	</div>
																</div>
																<div className="flex-item fx-b33">
																	<div className="contentTableBox">
																		<table className="table">
																			<thead>
																				<tr>
																					<th>
																						Variables types
                                                                    </th>
																				</tr>
																			</thead>
																			<tbody>
																				{this.state.statisticalReportData.dataKeys.map((el, index) => {
																					return (
																						<React.Fragment key={index}>
																							<tr>
																								<td><span className="text-red">{el}</span>has {this.state.statisticalReportData.variables[el].n_missing.__int64__} / {this.state.statisticalReportData.variables[el].p_missing} % missing values <span className="badge  badge-secondary">missing</span></td>
																							</tr>
																							<tr>
																								<td><span className="text-red">{el}</span>has {this.state.statisticalReportData.variables[el].n_zeros} / {this.state.statisticalReportData.variables[el].p_zeros} % zeros</td>
																							</tr>
																						</React.Fragment>
																					)
																				})}
																			</tbody>
																		</table>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div className="tab-pane fade" id="menu1">
														{this.state.statisticalReportData.dataKeys.map((el, index) => {
															return (
																<div className="reportBoxContent" key={index}>
																	<div className="reportBoxItem">
																		<p>{`${el}(${(this.state.statisticalReportData.variables[el].type.__Variable__).split("_")[1]})`}</p>
																	</div>
																	<div className="flex">
																		<div className="flex-item fx-b35">
																			<div className="contentTableBox">
																				<table className="table">
																					<tbody>
																						<tr>
																							<td><strong>Distinct count</strong></td>
																							<td>{this.state.statisticalReportData.variables[el].distinct_count.__int64__}</td>
																						</tr>
																						<tr>
																							<td><strong>Unique (%)</strong></td>
																							<td>{this.state.statisticalReportData.variables[el].p_unique} %</td>
																						</tr>
																						<tr>
																							<td><strong>Total Missing (%)</strong></td>
																							<td>{this.state.statisticalReportData.table.p_cells_missing} %</td>
																						</tr>
																						<tr>
																							<td><strong>Missing (%)	</strong></td>
																							<td>{this.state.statisticalReportData.variables[el].p_missing} %</td>
																						</tr>
																						<tr>
																							<td><strong> Missing (n)</strong></td>
																							<td>{this.state.statisticalReportData.variables[el].n_missing.__int64__}</td>
																						</tr>
																						<tr>
																							<td><strong>Infinite (%)</strong></td>
																							<td>{this.state.statisticalReportData.variables[el].p_infinite} %</td>
																						</tr>
																						<tr>
																							<td><strong>Infinite (n)</strong></td>
																							<td>{this.state.statisticalReportData.variables[el].n_infinite.__int64__}</td>
																						</tr>
																					</tbody>
																				</table>
																			</div>
																		</div>
																		<div className="flex-item fx-b35">
																			<div className="contentTableBox">
																				<table className="table">
																					<tbody>
																						<tr>
																							<td><strong> Mean</strong></td>
																							<td>{this.checkIfKeyIsValid(this.state.statisticalReportData.variables[el].mean)}</td>
																						</tr>
																						<tr>
																							<td><strong> Minimum</strong></td>
																							<td>{this.checkIfKeyIsValid(this.state.statisticalReportData.variables[el].min)}</td>
																						</tr>
																						<tr>
																							<td><strong>Maximum</strong></td>
																							<td>{this.checkIfKeyIsValid(this.state.statisticalReportData.variables[el].max)}</td>
																						</tr>
																						<tr>
																							<td><strong>Zeros (%)</strong></td>
																							<td>{this.checkIfKeyIsValid(this.state.statisticalReportData.variables[el].p_zeros)} %</td>
																						</tr>
																					</tbody>
																				</table>
																			</div>
																		</div>
																		<div className="flex-item fx-b30">
																			<div className="staticBox position-relative">
																				<div className="chatImage">
																					{this.createChart(this.state.statisticalReportData, el, "barChartForMl", "basicBarChart")}
																				</div>
																				<button className="btn btn-link" data-target={`#details_${index}`} data-toggle="collapse">Details</button>
																			</div>
																		</div>

																	</div>
																	<div className="flex-item fx-b100">
																		<div className="collapse mr-t-10" id={`details_${index}`}>
																			<div className="flex">
																				<div className="flex-item fx-b20">
																					<label className="form-label font-weight-bold f-18 mr-l-10">Quantile statistics :</label>
																					<ul className="staticList">
																						<li><strong>Minimum</strong>{this.checkIfKeyIsValid(this.state.statisticalReportData.variables[el].min)}</li>
																						<li><strong>5-th percentile</strong>{this.state.statisticalReportData.variables[el]["5%"]}</li>
																						<li><strong>Q1</strong>{this.state.statisticalReportData.variables[el]["25%"]}</li>
																						<li><strong>Mode</strong>{this.checkIfKeyIsValid(this.state.statisticalReportData.variables[el].mode)}</li>
																						<li><strong>Q3</strong>{this.state.statisticalReportData.variables[el]["75%"]}</li>
																						<li><strong>95-th percentile</strong>{this.state.statisticalReportData.variables[el]["95%"]}</li>
																						<li><strong>Maximum</strong>{this.checkIfKeyIsValid(this.state.statisticalReportData.variables[el].max)}</li>
																						<li><strong>Range</strong>{this.checkIfKeyIsValid(this.state.statisticalReportData.variables[el].range)}</li>
																						<li><strong>Interquartile range</strong>{this.state.statisticalReportData.variables[el].iqr}</li>
																					</ul>
																				</div>
																				<div className="flex-item fx-b20">
																					<label className="form-label font-weight-bold f-18 mr-l-10">Descriptive statistics :</label>
																					<ul className="staticList">
																						<li><strong>Standard deviation</strong>{this.state.statisticalReportData.variables[el].std}</li>
																						<li><strong>Coef of variation</strong>{this.state.statisticalReportData.variables[el].cv}</li>
																						<li><strong>Kurtosis</strong>{this.state.statisticalReportData.variables[el].kurtosis}</li>
																						<li><strong>Mean</strong>{this.checkIfKeyIsValid(this.state.statisticalReportData.variables[el].mean)}</li>
																						<li><strong>MAD</strong>{this.state.statisticalReportData.variables[el].mad}</li>
																						<li><strong>Skewness</strong>{this.state.statisticalReportData.variables[el].skewness}</li>
																						<li><strong>Sum</strong>{this.checkIfKeyIsValid(this.state.statisticalReportData.variables[el].sum)}</li>
																						<li><strong>Variance</strong>{this.state.statisticalReportData.variables[el].variance}</li>
																						<li><strong>Memory size</strong>{this.state.statisticalReportData.variables[el].memory_size} B</li>
																					</ul>
																				</div>
																				<div className="flex-item fx-b60">
																					<div className="chatImage">
																						{this.createChart(this.state.statisticalReportData, el, "lineChartForMl", "simpleLineChart")}
																					</div>

																				</div>
																			</div>
																		</div>
																	</div>
																</div>)
														})}
													</div>
												</div>
											</div>
										</div>
									</div>}
							</div>
						</div>

						<div className="modal animated slideInDown" id="showStatisticalReportWithCleaningAndTransformModal">
							<div className="modal-dialog themeModal modal-xl modal-dialog-scrollable">
								{this.state.statisticalReportDataWithCleaningAndTransform.userId &&
									<div className="modal-content">
										<div className="modal-header">
											<h6 className="modal-title text-center">Transformation
                                    <button type="button" className="close" data-dismiss="modal"><i className="far fa-times"></i></button>
											</h6>
										</div>
										<div className="modal-body">
											<div className="reportBox">
												<ul className="nav nav-tabs">
													<li className="nav-item">
														<a className="nav-link active" data-toggle="tab" href="#home">OverView</a>
													</li>
													<li className="nav-item">
														<a className="nav-link" data-toggle="tab" href="#menu1">Variable</a>
													</li>
												</ul>

												<div className="tab-content">
													<div className="tab-pane active" id="home">
														<div className="reportBoxContent">
															<div className="reportBoxItem">
																<p>Overview</p>
															</div>
															<div className="flex">
																<div className="flex-item fx-b33">
																	<div className="contentTableBox">
																		<table className="table">
																			<thead>
																				<tr>
																					<th colSpan="2">
																						Dataset info
                                                                            </th>
																				</tr>
																			</thead>
																			<tbody>
																				<tr>
																					<td><strong>Number of variables</strong></td>
																					<td>{this.state.statisticalReportDataWithCleaningAndTransform.table.n_var}</td>
																				</tr>
																				<tr>
																					<td><strong>Number of observations</strong></td>
																					<td>{this.state.statisticalReportDataWithCleaningAndTransform.table.n}</td>
																				</tr>
																				<tr>
																					<td><strong>Total Missing (%)</strong></td>
																					<td>{this.state.statisticalReportDataWithCleaningAndTransform.table.p_cells_missing}</td>
																				</tr>
																				<tr>
																					<td><strong>Total size in memory</strong></td>
																					<td>{this.state.statisticalReportDataWithCleaningAndTransform.table.memory_size ? `${this.state.statisticalReportDataWithCleaningAndTransform.table.memory_size.__int64__} B` : ''}</td>
																				</tr>
																				<tr>
																					<td><strong>Average record size in memory</strong></td>
																					<td>{this.state.statisticalReportDataWithCleaningAndTransform.table.record_size ? `${this.state.statisticalReportDataWithCleaningAndTransform.table.record_size} B` : ''}</td>
																				</tr>
																			</tbody>
																		</table>
																	</div>
																</div>
																<div className="flex-item fx-b33">
																	<div className="contentTableBox">
																		<table className="table">
																			<thead>
																				<tr>
																					<th colSpan="2">
																						Variables types
                                                                            </th>
																				</tr>
																			</thead>
																			<tbody>
																				<tr>
																					<td><strong>Numeric</strong></td>
																					<td>{this.state.statisticalReportDataWithCleaningAndTransform.table.types && this.state.statisticalReportDataWithCleaningAndTransform.table.types.NUM ? `${this.state.statisticalReportDataWithCleaningAndTransform.table.types.NUM.__int64__} %` : ''}</td>
																				</tr>
																				<tr>
																					<td><strong>Categorical</strong></td>
																					<td>{this.state.statisticalReportDataWithCleaningAndTransform.table.types && this.state.statisticalReportDataWithCleaningAndTransform.table.types.NUM ? `${this.state.statisticalReportDataWithCleaningAndTransform.table.types.NUM.__int64__} %` : ''}</td>
																				</tr>
																				<tr>
																					<td><strong>Date</strong></td>
																					<td>{this.state.statisticalReportDataWithCleaningAndTransform.table.types && this.state.statisticalReportDataWithCleaningAndTransform.table.types.NUM ? `${this.state.statisticalReportDataWithCleaningAndTransform.table.types.NUM.__int64__} %` : ''}</td>
																				</tr>
																				<tr>
																					<td><strong>Text (Unique)</strong></td>
																					<td>{this.state.statisticalReportDataWithCleaningAndTransform.table.types && this.state.statisticalReportDataWithCleaningAndTransform.table.types.NUM ? `${this.state.statisticalReportDataWithCleaningAndTransform.table.types.NUM.__int64__} %` : ''}</td>
																				</tr>
																				<tr>
																					<td><strong>Rejected</strong></td>
																					<td>{this.state.statisticalReportDataWithCleaningAndTransform.table.types && this.state.statisticalReportDataWithCleaningAndTransform.table.types.NUM ? `${this.state.statisticalReportDataWithCleaningAndTransform.table.types.NUM.__int64__} %` : ''}</td>
																				</tr>
																			</tbody>
																		</table>
																	</div>
																</div>
																<div className="flex-item fx-b33">
																	<div className="contentTableBox">
																		<table className="table">
																			<thead>
																				<tr>
																					<th>
																						Variables types
                                                                    </th>
																				</tr>
																			</thead>
																			<tbody>
																				{this.state.statisticalReportDataWithCleaningAndTransform.dataKeys.map((el, index) => {
																					return (
																						<React.Fragment key={index}>
																							<tr>
																								<td><span className="text-red">{el}</span>has {this.state.statisticalReportDataWithCleaningAndTransform.variables[el].n_missing.__int64__} / {this.state.statisticalReportDataWithCleaningAndTransform.variables[el].p_missing} % missing values <span className="badge  badge-secondary">missing</span></td>
																							</tr>
																							<tr>
																								<td><span className="text-red">{el}</span>has {this.state.statisticalReportDataWithCleaningAndTransform.variables[el].n_zeros} / {this.state.statisticalReportDataWithCleaningAndTransform.variables[el].p_zeros} % zeros</td>
																							</tr>
																						</React.Fragment>
																					)
																				})}
																			</tbody>
																		</table>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div className="tab-pane fade" id="menu1">
														{this.state.statisticalReportDataWithCleaningAndTransform.dataKeys.map((el, index) => {
															return (
																<div className="reportBoxContent" key={index}>
																	<div className="reportBoxItem">
																		<p>{`${el}(${(this.state.statisticalReportDataWithCleaningAndTransform.variables[el].type.__Variable__).split("_")[1]})`}</p>
																	</div>
																	<div className="flex">
																		<div className="flex-item fx-b35">
																			<div className="contentTableBox">
																				<table className="table">
																					<tbody>
																						<tr>
																							<td><strong>Distinct count</strong></td>
																							<td>{this.state.statisticalReportDataWithCleaningAndTransform.variables[el].distinct_count.__int64__}</td>
																						</tr>
																						<tr>
																							<td><strong>Unique (%)</strong></td>
																							<td>{this.state.statisticalReportDataWithCleaningAndTransform.variables[el].p_unique} %</td>
																						</tr>
																						<tr>
																							<td><strong>Total Missing (%)</strong></td>
																							<td>{this.state.statisticalReportDataWithCleaningAndTransform.table.p_cells_missing} %</td>
																						</tr>
																						<tr>
																							<td><strong>Missing (%)	</strong></td>
																							<td>{this.state.statisticalReportDataWithCleaningAndTransform.variables[el].p_missing} %</td>
																						</tr>
																						<tr>
																							<td><strong> Missing (n)</strong></td>
																							<td>{this.state.statisticalReportDataWithCleaningAndTransform.variables[el].n_missing.__int64__}</td>
																						</tr>
																						<tr>
																							<td><strong>Infinite (%)</strong></td>
																							<td>{this.state.statisticalReportDataWithCleaningAndTransform.variables[el].p_infinite} %</td>
																						</tr>
																						<tr>
																							<td><strong>Infinite (n)</strong></td>
																							<td>{this.state.statisticalReportDataWithCleaningAndTransform.variables[el].n_infinite.__int64__}</td>
																						</tr>
																					</tbody>
																				</table>
																			</div>
																		</div>
																		<div className="flex-item fx-b35">
																			<div className="contentTableBox">
																				<table className="table">
																					<tbody>
																						<tr>
																							<td><strong> Mean</strong></td>
																							<td>{this.checkIfKeyIsValid(this.state.statisticalReportDataWithCleaningAndTransform.variables[el].mean)}</td>
																						</tr>
																						<tr>
																							<td><strong> Minimum</strong></td>
																							<td>{this.checkIfKeyIsValid(this.state.statisticalReportDataWithCleaningAndTransform.variables[el].min)}</td>
																						</tr>
																						<tr>
																							<td><strong>Maximum</strong></td>
																							<td>{this.checkIfKeyIsValid(this.state.statisticalReportDataWithCleaningAndTransform.variables[el].max)}</td>
																						</tr>
																						<tr>
																							<td><strong>Zeros (%)</strong></td>
																							<td>{this.checkIfKeyIsValid(this.state.statisticalReportDataWithCleaningAndTransform.variables[el].p_zeros)} %</td>
																						</tr>
																					</tbody>
																				</table>
																			</div>
																		</div>
																		<div className="flex-item fx-b30">
																			<div className="staticBox position-relative">
																				<div className="chatImage">
																					{this.createChart(this.state.statisticalReportDataWithCleaningAndTransform, el, "barChartForMl", "basicBarChart")}
																				</div>
																				<button className="btn btn-link" data-target={`#details_${index}`} data-toggle="collapse">Details</button>
																			</div>
																		</div>

																	</div>
																	<div className="flex-item fx-b100">
																		<div className="collapse mr-t-10" id={`details_${index}`}>
																			<div className="flex">
																				<div className="flex-item fx-b20">
																					<label className="form-label font-weight-bold f-18 mr-l-10">Quantile statistics :</label>
																					<ul className="staticList">
																						<li><strong>Minimum</strong>{this.checkIfKeyIsValid(this.state.statisticalReportDataWithCleaningAndTransform.variables[el].min)}</li>
																						<li><strong>5-th percentile</strong>{this.state.statisticalReportDataWithCleaningAndTransform.variables[el]["5%"]}</li>
																						<li><strong>Q1</strong>{this.state.statisticalReportDataWithCleaningAndTransform.variables[el]["25%"]}</li>
																						<li><strong>Mode</strong>{this.checkIfKeyIsValid(this.state.statisticalReportDataWithCleaningAndTransform.variables[el].mode)}</li>
																						<li><strong>Q3</strong>{this.state.statisticalReportDataWithCleaningAndTransform.variables[el]["75%"]}</li>
																						<li><strong>95-th percentile</strong>{this.state.statisticalReportDataWithCleaningAndTransform.variables[el]["95%"]}</li>
																						<li><strong>Maximum</strong>{this.checkIfKeyIsValid(this.state.statisticalReportDataWithCleaningAndTransform.variables[el].max)}</li>
																						<li><strong>Range</strong>{this.checkIfKeyIsValid(this.state.statisticalReportDataWithCleaningAndTransform.variables[el].range)}</li>
																						<li><strong>Interquartile range</strong>{this.state.statisticalReportDataWithCleaningAndTransform.variables[el].iqr}</li>
																					</ul>
																				</div>
																				<div className="flex-item fx-b20">
																					<label className="form-label font-weight-bold f-18 mr-l-10">Descriptive statistics :</label>
																					<ul className="staticList">
																						<li><strong>Standard deviation</strong>{this.state.statisticalReportDataWithCleaningAndTransform.variables[el].std}</li>
																						<li><strong>Coef of variation</strong>{this.state.statisticalReportDataWithCleaningAndTransform.variables[el].cv}</li>
																						<li><strong>Kurtosis</strong>{this.state.statisticalReportDataWithCleaningAndTransform.variables[el].kurtosis}</li>
																						<li><strong>Mean</strong>{this.checkIfKeyIsValid(this.state.statisticalReportDataWithCleaningAndTransform.variables[el].mean)}</li>
																						<li><strong>MAD</strong>{this.state.statisticalReportDataWithCleaningAndTransform.variables[el].mad}</li>
																						<li><strong>Skewness</strong>{this.state.statisticalReportDataWithCleaningAndTransform.variables[el].skewness}</li>
																						<li><strong>Sum</strong>{this.checkIfKeyIsValid(this.state.statisticalReportDataWithCleaningAndTransform.variables[el].sum)}</li>
																						<li><strong>Variance</strong>{this.state.statisticalReportDataWithCleaningAndTransform.variables[el].variance}</li>
																						<li><strong>Memory size</strong>{this.state.statisticalReportDataWithCleaningAndTransform.variables[el].memory_size} B</li>
																					</ul>
																				</div>
																				<div className="flex-item fx-b60">
																					<div className="chatImage">
																						{this.createChart(this.state.statisticalReportDataWithCleaningAndTransform, el, "lineChartForMl", "simpleLineChart")}
																					</div>

																				</div>
																			</div>
																		</div>
																	</div>
																</div>)
														})}
													</div>
												</div>
											</div>
										</div>
									</div>}
							</div>
						</div>

						<div className="modal animated slideInDown" id="featureImportanceModal">
							<div className="modal-dialog modal-xl modal-dialog-scrollable">
								<div className="modal-content">
									<div className="modal-header">
										<h4 className="modal-title text-center">Feature Importance</h4>
										<button type="button" className="close" data-dismiss="modal"><i className="far fa-times"></i></button>
									</div>
									<div className="modal-body">
										<div className="form-group input-group">
											<select className="form-control" disabled={this.state.featureImportanceLoader} id="featureImportanceColName" required value={this.state.featureImportanceColName} onChange={(e) => {
												let featureImportanceColName = e.currentTarget.value
												this.setState({
													featureImportanceColName
												})
											}}>
												<option value=''>Select</option>
												{this.state.columnsList.map((column, columnIndex) => {
													return (<option key={columnIndex} value={column.keys}>{column.keys}</option>)
												}
												)}
											</select>
											<div className="input-group-append">
												<button className="btn btn-transparent-success input-group-text" disabled={this.state.featureImportanceLoader} onClick={(e) => {
													let payload = {
														action: {
															type: "showStatistics",
															actionName: "featureImportance",
															target: this.state.featureImportanceColName
														},
														dataSource: this.state.payload.dataSource
													}
													this.setState({
														featureImportanceLoader: true
													})
													this.props.getStatistics(payload, `featureImportanceLoader`);
												}} >Get Importance</button>
											</div>
											<label className="form-group-label">Target Column :<i className="fas fa-asterisk form-group-required"></i></label>
										</div>
										{/*<button type="button"  className="btn btn-success"></button>*/}
										<div className="featureModalBox">
											{this.state.featureImportanceLoader ?
												<div className="fileUploadLoader">
													<img src="https://content.iot83.com/m83/misc/uploading.gif" />
													<h6>Loading...</h6>
												</div> : Object.keys(this.state.featureImportanceData).length ?
													<div className="reportBox">
														<ul className="nav nav-tabs">
															<li className="nav-item">
																<a className="nav-link active" data-toggle="tab" href="#home">Chart Data</a>
															</li>
															<li className="nav-item">
																<a className="nav-link" data-toggle="tab" href="#menu1">Table</a>
															</li>
														</ul>

														<div className="tab-content">
															<div className="tab-pane active" id="home">
																<div className="chartBox">
																	{this.createChartForFeatureImportanceData(this.state.featureImportanceData, "barChartForMl", "basicBarChart")}
																</div>
															</div>
															<div className="tab-pane fade" id="menu1">
																<div className="contentTableBox mr-0 mr-t-10 table-responsive">
																	<table className="table">
																		<thead>
																			<tr>
																				{this.state.featureImportanceData.dataKeys.map((el, index) => {
																					return (<th className="bg-transparent text-dark" key={index}>{el}</th>)
																				})}
																			</tr>
																		</thead>
																		<tbody>
																			<tr>
																				{this.state.featureImportanceData.dataKeys.map((el, elIndex) => {
																					return (
																						<td key={elIndex}>{this.state.featureImportanceData.data[el]}</td>
																					)
																				})}
																			</tr>
																		</tbody>
																	</table>
																</div>
															</div>
														</div>
													</div> : <p className="noteText">'Click Get Importance button to get the data'
                                            </p>}
										</div>
									</div>
								</div>
							</div>
						</div>

						<div className="modal animated slideInDown" id="informationGainModal">
							<div className="modal-dialog modal-xl modal-dialog-scrollable">
								<div className="modal-content">
									<div className="modal-header">
										<h4 className="modal-title text-center">Information Gain</h4>
										<button type="button" className="close" data-dismiss="modal"><i className="far fa-times"></i></button>
									</div>
									<div className="modal-body">
										<div className="featureModalBox">
											{Object.keys(this.state.informationGainData).length ?
												<div className="reportBox">
													<div className="tab-content">
														<div className="contentTableBox mr-0 mr-t-10 table-responsive">
															<table className="table">
																<thead>
																	<tr>
																		{this.state.informationGainData.dataKeys.map((el, index) => {
																			return (<th className="bg-transparent text-dark" key={index}>{el}</th>)
																		})}
																	</tr>
																</thead>
																<tbody>
																	<tr>
																		{this.state.informationGainData.dataKeys.map((el, elIndex) => {
																			return (
																				<td key={elIndex}>{this.state.informationGainData.data[el]}</td>
																			)
																		})}
																	</tr>
																</tbody>
															</table>
														</div>
													</div>
												</div> : <p className="noteText">'Click Get Importance button to get the data'
                                            </p>}
										</div>
									</div>
								</div>
							</div>
						</div>

						<div className="modal animated slideInDown " id="dataCorrelationModal">
							<div className="modal-dialog modal-xl modal-dialog-scrollable">
								{this.state.dataCorrelationData.userId &&
									<div className="modal-content">
										<div className="modal-header">
											<h6 className="modal-title text-center">Data Correlation (Method='Pearson')
                                    <button type="button" className="close" data-dismiss="modal"><i className="far fa-times"></i></button>
											</h6>
										</div>
										<div className="modal-body">
											<div className="reportBox">
												<div className="flex">
													<div className="flex-item fx-b20">
														<div className="contentTableBox mr-0">
															<table className="table">
																<thead>
																	<tr>
																		<th className="border-r-0"><span className="visibilityHidden">Header</span></th>
																	</tr>
																</thead>
																<tbody>
																	{this.state.dataCorrelationData.data.map((data, index) => {
																		let html = <tr key={index}>
																			{index < this.state.dataCorrelationData.dataKeys.length ?
																				<td className="font-weight-bold text-dark border-r-0">{this.state.dataCorrelationData.dataKeys[index]}</td>
																				: ''}
																		</tr>
																		return html;
																	}
																	)}
																</tbody>
															</table>
														</div>
													</div>
													<div className="flex-item fx-b80">
														<div className="contentTableBox mr-0 table-responsive">
															<table className="table">
																<thead>
																	<tr>
																		{/*<th className="bg-transparent text-dark"></th>*/}
																		{this.state.dataCorrelationData.dataKeys.map((el, index) => {
																			return (<th className="bg-transparent text-dark" key={index}>{el}</th>)
																		})}
																	</tr>
																</thead>
																<tbody>
																	{this.state.dataCorrelationData.data.map((data, index) => {
																		let html = <tr key={index}>
																			{this.state.dataCorrelationData.dataKeys.map((el, elIndex) => {
																				return (
																					<React.Fragment key={elIndex}>
																						<td>{data[el]}</td>
																					</React.Fragment>
																				)
																			})
																			}
																		</tr>
																		return html;
																	})}
																</tbody>
															</table>
														</div>
													</div>
												</div>
												<p className="noteText pd-l-7 mr-b-20"><strong>Note: </strong>String type and object type column have been dropped, which are - {this.state.dataCorrelationData.droppedColumnList.map((el, index) => { return (`${el}${this.state.dataCorrelationData.droppedColumnList.length - 1 === index ? '.' : ', '}`) })}</p>
											</div>
										</div>
									</div>}
							</div>
						</div>

						<div className="modal animated slideInDown" id="dataCorrelationWithCleaningAndTransformModal">
							<div className="modal-dialog modal-xl modal-dialog-scrollable">
								{this.state.dataCorrelationDataWithCleaningAndTransform.userId &&
									<div className="modal-content">
										<div className="modal-header">
											<h6 className="modal-title text-center">Data Correlation (Method='Pearson')
                                    <button type="button" className="close" data-dismiss="modal"><i className="far fa-times"></i></button>
											</h6>
										</div>
										<div className="modal-body">
											<div className="reportBox">
												<div className="flex">
													<div className="flex-item fx-b20">
														<div className="contentTableBox mr-0">
															<table className="table">
																<thead>
																	<tr>
																		<th className="border-r-0"><span className="visibilityHidden">Header</span></th>
																	</tr>
																</thead>
																<tbody>
																	{this.state.dataCorrelationDataWithCleaningAndTransform.data.map((data, index) => {
																		let html = <tr key={index}>
																			{index < this.state.dataCorrelationDataWithCleaningAndTransform.dataKeys.length ?
																				<td className="font-weight-bold text-dark">{this.state.dataCorrelationDataWithCleaningAndTransform.dataKeys[index]}</td>
																				: ''}
																		</tr>
																		return html;
																	}
																	)}
																</tbody>
															</table>
														</div>
													</div>
													<div className="flex-item fx-b80">
														<div className="contentTableBox mr-0 table-responsive">
															<table className="table">
																<thead>
																	<tr>
																		{/*<th className="bg-transparent text-dark"></th>*/}
																		{this.state.dataCorrelationDataWithCleaningAndTransform.dataKeys.map((el, index) => {
																			return (<th className="bg-transparent text-dark" key={index}>{el}</th>)
																		})}
																	</tr>
																</thead>
																<tbody>
																	{this.state.dataCorrelationDataWithCleaningAndTransform.data.map((data, index) => {
																		let html = <tr key={index}>
																			{this.state.dataCorrelationDataWithCleaningAndTransform.dataKeys.map((el, elIndex) => {
																				return (
																					<React.Fragment key={elIndex}>
																						<td>{data[el]}</td>
																					</React.Fragment>
																				)
																			})
																			}
																		</tr>
																		return html;
																	})}
																</tbody>
															</table>
														</div>
													</div>
												</div>
												<p className="noteText pd-l-7 mr-b-20"><strong>Note: </strong>String type and object type column have been dropped, which are - {this.state.dataCorrelationData.droppedColumnList.map((el, index) => { return (`${el}${this.state.dataCorrelationData.droppedColumnList.length - 1 === index ? '.' : ', '}`) })}</p>
											</div>
										</div>
									</div>}
							</div>
						</div>

						<div className="modal animated slideInDown" id="showMissingValuesModal">
							<div className="modal-dialog modal-xl modal-dialog-scrollable">
								{this.state.showMissingValuesData.userId &&
									<div className="modal-content">
										<div className="modal-header">
											<h6 className="modal-title text-center">Show Missing Values
                                    <button type="button" className="close" data-dismiss="modal"><i className="far fa-times"></i></button>
											</h6>
										</div>
										<div className="modal-body">
											<div className="reportBox">
												<div className="contentTableBox mr-0 table-responsive">
													<table className="table">
														<thead>
															<tr>
																{this.state.showMissingValuesData.dataKeys.map((el, index) => {
																	return (<th className="bg-transparent text-dark" key={index}>{el}</th>)
																})}
															</tr>
														</thead>
														<tbody>
															{this.state.showMissingValuesData.data.map((data, index) => {
																let html = <tr key={index}>
																	{this.state.showMissingValuesData.dataKeys.map((el, elIndex) => {
																		return (
																			<td key={elIndex}>{data[el]}</td>
																		)
																	})}
																</tr>
																return html;
															})}
														</tbody>
													</table>
												</div>
											</div>
										</div>
									</div>}
							</div>
						</div>

						<div className="modal animated slideInDown" id="showMissingValuesWithCleaningAndTransformModal">
							<div className="modal-dialog modal-xl modal-dialog-scrollable">
								{this.state.showMissingValuesDataWithCleaningAndTransform.userId &&
									<div className="modal-content">
										<div className="modal-header">
											<h6 className="modal-title text-center">Show Missing Values
                                    <button type="button" className="close" data-dismiss="modal"><i className="far fa-times"></i></button>
											</h6>
										</div>
										<div className="modal-body">
											<div className="reportBox">
												<div className="contentTableBox mr-0 table-responsive">
													<table className="table">
														<thead>
															<tr>
																{this.state.showMissingValuesDataWithCleaningAndTransform.dataKeys.map((el, index) => {
																	return (<th className="bg-transparent text-dark" key={index}>{el}</th>)
																})}
															</tr>
														</thead>
														<tbody>
															{this.state.showMissingValuesDataWithCleaningAndTransform.data.map((data, index) => {
																let html = <tr key={index}>
																	{this.state.showMissingValuesDataWithCleaningAndTransform.dataKeys.map((el, elIndex) => {
																		return (
																			<td key={elIndex}>{data[el]}</td>
																		)
																	})}
																</tr>
																return html;
															})}
														</tbody>
													</table>
												</div>
											</div>
										</div>
									</div>}
							</div>
						</div>

						<div className="modal animated slideInDown" id="showSampleDataModal">
							<div className="modal-dialog modal-xl modal-dialog-scrollable">
								{this.state.showSampleData.userId &&
									<div className="modal-content">
										<div className="modal-header">
											<h6 className="modal-title text-center">Sample Data
                                    <button type="button" className="close" data-dismiss="modal"><i className="far fa-times"></i></button>
											</h6>
										</div>
										<div className="modal-body">
											<div className="reportBox">
												<div className="contentTableBox mr-0 table-responsive">
													<table className="table">
														<thead>
															<tr>
																{this.state.showSampleData.dataKeys.map((el, index) => {
																	return (<th className="bg-transparent text-dark" key={index}>{el}</th>)
																})}
															</tr>
														</thead>
														<tbody>
															{this.state.showSampleData.sample.map((data, index) => {
																let html = <tr key={index}>
																	{this.state.showSampleData.dataKeys.map((el, elIndex) => {
																		return (
																			<td key={elIndex}>{data[el]}</td>
																		)
																	})}
																</tr>
																return html;
															})}
														</tbody>
													</table>
												</div>
											</div>
										</div>
									</div>}
							</div>
						</div>

						<div className="modal animated slideInDown" id="showSampleDataWithCleaningAndTransformModal">
							<div className="modal-dialog modal-xl modal-dialog-scrollable">
								{this.state.showSampleDataWithCleaningAndTransform.userId &&
									<div className="modal-content">
										<div className="modal-header">
											<h6 className="modal-title text-center">Sample Data
                                    <button type="button" className="close" data-dismiss="modal"><i className="far fa-times"></i></button>
											</h6>
										</div>
										<div className="modal-body">
											<div className="reportBox">
												<div className="contentTableBox mr-0 table-responsive">
													<table className="table">
														<thead>
															<tr>
																{this.state.showSampleDataWithCleaningAndTransform.dataKeys.map((el, index) => {
																	return (<th className="bg-transparent text-dark" key={index}>{el}</th>)
																})}
															</tr>
														</thead>
														<tbody>
															{this.state.showSampleDataWithCleaningAndTransform.sample.map((data, index) => {
																let html = <tr key={index}>
																	{this.state.showSampleDataWithCleaningAndTransform.dataKeys.map((el, elIndex) => {
																		return (
																			<td key={elIndex}>{data[el]}</td>
																		)
																	})}
																</tr>
																return html;
															})}
														</tbody>
													</table>
												</div>
											</div>
										</div>
									</div>}
							</div>
						</div>
					</React.Fragment>
				}
			</React.Fragment >
		);
	}
}

NewAddOrEditMl.propTypes = {
	dispatch: PropTypes.func.isRequired
};
let allSelectors = {}
Object.entries(SELECTORS).map(([key, value]) => {
	allSelectors[key] = value()
})

const mapStateToProps = createStructuredSelector(allSelectors);

function mapDispatchToProps(dispatch) {
	let allActions = { dispatch }
	Object.entries(ACTIONS).map(([key, value]) => {
		allActions[key] = (...args) => dispatch(value(...args))
	})
	return allActions;
}

const withConnect = connect(
	mapStateToProps,
	mapDispatchToProps
);

const withReducer = injectReducer({ key: "newAddOrEditMl", reducer });
const withSaga = injectSaga({ key: "newAddOrEditMl", saga });

export default compose(
	withReducer,
	withSaga,
	withConnect
)(NewAddOrEditMl);
