/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

import { createSelector } from "reselect";
import { initialState } from "./reducer";
import * as CONSTANTS from "./constants"

/**
 * Direct selector to the newAddOrEditMl state domain
 */

const selectNewAddOrEditMlDomain = state =>
  state.get("newAddOrEditMl", initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by NewAddOrEditMl
 */
let allCases = Object.values(CONSTANTS).filter(constant => typeof (constant) == "object");
export const allSelectors = {}
allCases.map(constant => {
  allSelectors[constant.successKey] = () => createSelector(selectNewAddOrEditMlDomain, subState => subState[constant.successKey]);
  allSelectors[constant.failureKey] = () => createSelector(selectNewAddOrEditMlDomain, subState => subState[constant.failureKey]);
})

