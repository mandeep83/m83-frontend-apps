/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/*
 *
 * NewAddOrEditMl constants
 *
 */

export const RESET_TO_INITIAL_STATE = "app/NewAddOrEditMl/RESET_TO_INITIAL_STATE";

export const GET_CONNECTORS_BY_CATEGORY = {
	action: 'app/NewAddOrEditMl/GET_CONNECTORS_BY_CATEGORY',
	success: "app/NewAddOrEditMl/GET_CONNECTORS_BY_CATEGORY_SUCCESS",
	failure: "app/NewAddOrEditMl/GET_CONNECTORS_BY_CATEGORY_FAILURE",
	urlKey: "getConnectorsByCategory",
	successKey: "getConnectorsByCategorySuccess",
	failureKey: "getConnectorsByCategoryFailure",
	actionName: "getConnectorsByCategory",
	actionArguments: ["category"],
}

export const GET_COLUMNS_BY_CONNECTOR = {
	action: 'app/NewAddOrEditMl/GET_COLUMNS_BY_CONNECTOR',
	success: "app/NewAddOrEditMl/GET_COLUMNS_BY_CONNECTOR_SUCCESS",
	failure: "app/NewAddOrEditMl/GET_COLUMNS_BY_CONNECTOR_FAILURE",
	urlKey: "getColumnsByConnector",
	successKey: "getColumnsByConnectorSuccess",
	failureKey: "getColumnsByConnectorFailure",
	actionName: "getColumnsByConnector",
	actionArguments: ["payload"],
}

export const GET_STATISTICS = {
	action: 'app/NewAddOrEditMl/GET_STATISTICS',
	success: 'app/NewAddOrEditMl/GET_STATISTICS_SUCCESS',
	failure: 'app/NewAddOrEditMl/GET_STATISTICS_FAILURE',
	urlKey: 'getStatisticsMlExperiment',
	successKey: 'getStatisticsSuccess',
	failureKey: 'getStatisticsFailure',
	actionName: 'getStatistics',
	actionArguments: ["payload"],
}

export const SAVE_EXPERIMENT = {
	action: 'app/NewAddOrEditMl/SAVE_EXPERIMENT',
	success: 'app/NewAddOrEditMl/SAVE_EXPERIMENT_SUCCESS',
	failure: 'app/NewAddOrEditMl/SAVE_EXPERIMENT_FAILURE',
	urlKey: 'saveExperiment',
	successKey: 'saveExperimentSuccess',
	failureKey: 'saveExperimentFailure',
	actionName: 'saveExperiment',
	actionArguments: ["payload"],
}

export const GET_EXPERIMENT_BY_ID = {
	action: 'app/NewAddOrEditMl/GET_EXPERIMENT_BY_ID',
	success: 'app/NewAddOrEditMl/GET_EXPERIMENT_BY_ID_SUCCESS',
	failure: 'app/NewAddOrEditMl/GET_EXPERIMENT_BY_ID_FAILURE',
	urlKey: 'getExperimentById',
	successKey: 'getExperimentByIdSuccess',
	failureKey: 'getExperimentByIdFailure',
	actionName: 'getExperimentById',
	actionArguments: ["id"],
}
