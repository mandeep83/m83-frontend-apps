/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/*
 *
 * DeviceTypes constants
 *
 */
export const RESET_TO_INITIAL_STATE = "app/DeviceTypes/RESET_TO_INITIAL_STATE";

export const GET_ALL_DEVICE_TYPES = {
	action: 'app/DeviceTypes/GET_ALL_DEVICE_TYPES',
	success: "app/DeviceTypes/GET_ALL_DEVICE_TYPES_SUCCESS",
	failure: "app/DeviceTypes/GET_ALL_DEVICE_TYPES_FAILURE",
	urlKey: "getAllDeviceType",
	successKey: "getAllDeviceTypesSuccess",
	failureKey: "getAllDeviceTypesFailure",
	actionName: "getAllDeviceType",
	actionArguments: []
}

export const DELETE_DEVICE_TYPE = {
	action: 'app/DeviceTypes/DELETE_DEVICE_TYPE',
	success: "app/DeviceTypes/DELETE_DEVICE_TYPE_SUCCESS",
	failure: "app/DeviceTypes/DELETE_DEVICE_TYPE_FAILURE",
	urlKey: "deleteDeviceType",
	successKey: "deleteDeviceTypeByIdSuccess",
	failureKey: "deleteDeviceTypeByIdFailure",
	actionName: "deleteDeviceTypeById",
	actionArguments: ["id"]
}

export const DEVELOPER_QUOTA = {
	action: 'app/DeviceTypes/GET_DEVELOPER_QUOTA',
	success: "app/DeviceTypes/GET_DEVELOPER_QUOTA_SUCCESS",
	failure: "app/DeviceTypes/GET_DEVELOPER_QUOTA_FAILURE",
	urlKey: "getDeveloperQuota",
	successKey: "developerQuotaSuccess",
	failureKey: "developerQuotaFailure",
	actionName: "getDeveloperQuota",
	actionArguments: ["payload"]
}
