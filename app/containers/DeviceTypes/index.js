/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information containedhomherein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * DeviceTypes
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import makeSelectDeviceTypeList from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import { allSelectors as SELECTORS } from "./selectors";
import { allActions as ACTIONS } from './actions'
import { isNavAssigned } from "../../commonUtils";
import Loader from "../../components/Loader";
import NotificationModal from '../../components/NotificationModal/Loadable';
import DeleteModel from "../../components/DeleteModel";
import ListingTable from "../../components/ListingTable/Loadable";
import AddNewButton from "../../components/AddNewButton/Loadable";
import NoDataFoundMessage from "../../components/NoDataFoundMessage/Loadable"
import cloneDeep from "lodash/cloneDeep";
import Skeleton from "react-loading-skeleton";

/* eslint-disable react/prefer-stateless-function */
export class DeviceTypes extends React.Component {
    state = {
        isFetching: true,
        toggleView: true,
        remainingDeviceTypesCount: 0,
        totalDeviceTypesCount: 0,
        usedDeviceTypesCount: 0,
        connectorsList: [],
        filteredDataList: [],
        searchTerm: '',
        theme: true,
        connectorsCategory: []
    }

    refreshComponent = () => {
        this.setState({
            isFetching: true,
            remainingDeviceTypesCount: 0,
            totalDeviceTypesCount: 0,
            usedDeviceTypesCount: 0,
            connectorsList: [],
            filteredDataList: [],
            searchTerm: '',
            theme: true
        }, () => {
            let payload = [
                {
                    dependingProductId: null,
                    productName: "device_types",
                }
            ];
            this.props.getDeveloperQuota(payload)
        })
    }

    componentDidMount() {
        let payload = [
            {
                dependingProductId: null,
                productName: "device_types",
            }
        ];
        this.props.getDeveloperQuota(payload);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.developerQuotaSuccess) {
            let remainingDeviceTypesCount = nextProps.developerQuotaSuccess.response.filter(product => product.productName === 'device_types')[0]['remaining'],
                totalDeviceTypesCount = nextProps.developerQuotaSuccess.response.filter(product => product.productName === 'device_types')[0]['total'],
                usedDeviceTypesCount = nextProps.developerQuotaSuccess.response.filter(product => product.productName === 'device_types')[0]['used'];
            this.setState({
                remainingDeviceTypesCount,
                totalDeviceTypesCount,
                usedDeviceTypesCount
            }, () => this.props.getAllDeviceType());

        }
        if (nextProps.developerQuotaFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.developerQuotaFailure.error,
            }, () => this.props.getAllDeviceType())
        }
        if (nextProps.getAllDeviceTypesSuccess) {
            this.setState({
                connectorsList: nextProps.getAllDeviceTypesSuccess.response,
                filteredDataList: nextProps.getAllDeviceTypesSuccess.response,
                isFetching: false,
            })
        }
        if (nextProps.getAllDeviceTypesFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.getAllDeviceTypesFailure.error,
                isFetching: false,
            })
        }

        if (nextProps.deleteDeviceTypeByIdSuccess) {
            let connectorsList = cloneDeep(this.state.connectorsList),
                remainingDeviceTypesCount = this.state.remainingDeviceTypesCount,
                usedDeviceTypesCount = this.state.usedDeviceTypesCount;

            connectorsList = connectorsList.filter(val => val.id !== nextProps.deleteDeviceTypeByIdSuccess.response.id);
            remainingDeviceTypesCount = remainingDeviceTypesCount + 1,
                usedDeviceTypesCount = usedDeviceTypesCount - 1;

            this.setState({
                connectorsList,
                filteredDataList: connectorsList,
                remainingDeviceTypesCount,
                usedDeviceTypesCount,
                modalType: "success",
                isOpen: true,
                message2: nextProps.deleteDeviceTypeByIdSuccess.response.message,
                isFetching: false,
            })
        }
        if (nextProps.deleteDeviceTypeByIdFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.deleteDeviceTypeByIdFailure.error,
                isFetching: false,
            })
        }
    }

    componentDidUpdate(prevProps, prevState) {
        if (Object.keys(SELECTORS).some(prop => this.props[prop])) {
            this.props.resetToInitialState()
        }
    }

    onCloseHandler = () => {
        this.setState({
            isOpen: false,
            message2: '',
            modalType: "",
        });
    }

    cancelClicked = () => {
        this.setState({
            deleteName: "",
            connectorToBeDeleted: null,
            confirmState: false
        })
    }

    onSearchHandler = (e) => {
        let filterList = cloneDeep(this.state.connectorsList);
        if (e.target.value.length > 0) {
            filterList = filterList.filter(item => item.name.toLowerCase().includes(e.target.value.toLowerCase()))
        }
        this.setState({
            filteredDataList: filterList,
            searchTerm: e.target.value
        })
    }

    emptySearchBox = () => {
        let filteredDataList = cloneDeep(this.state.connectorsList);
        this.setState({
            searchTerm: '',
            filteredDataList
        })
    }

    goToCreateDevicePage = () => {
        this.props.history.push(`/addOrEditDeviceType`);
    }
    getColumns = () => {
        return [
            {
                Header: "Name", width: 20,
                cell: (row) => (
                    <React.Fragment>
                        <div className="list-view-icon"><img src={row.image ? row.image + `?lastmod=${(new Date()).getTime()}` : "https://content.iot83.com/m83/misc/device-type-fallback.png"}></img></div>
                        <div className="list-view-icon-box">
                            <h6 className="text-theme fw-600">{row.name}</h6>
                            <p>{`${row.description || "-"}`}</p>
                        </div>
                    </React.Fragment>
                )
            },
            {
                Header: "Devices / Groups / Tags", width: 15,
                cell: (row) => (
                    <div className="button-group-link">
                        <span className="alert alert-primary list-view-badge list-view-badge-link" onClick={() => { isNavAssigned('devices') && this.props.history.push(`/devices/${row.id}`) }}>{row.deviceCount}</span>
                        <span className="mr-l-10 mr-r-10">/</span>
                        <span className="alert alert-success list-view-badge list-view-badge-link" onClick={() => { isNavAssigned('deviceGroups') && this.props.history.push(`/deviceGroups/${row.id}`) }}>{row.groupCount}</span>
                        <span className="mr-l-10 mr-r-10">/</span>
                        <span className="alert alert-warning list-view-badge">{row.tagCount}</span>
                    </div>
                )
            },
            {
                Header: "Manage", width: 25,
                cell: (row) => (
                    <div className="button-group button-group-link">
                        <button className="btn btn-link" /*disabled={row.deviceCount === 0}*/ onClick={() => { isNavAssigned('deviceAttributes') && this.props.history.push(`/deviceAttributes/${row.id}`) }}>Attributes</button>
                        <span className="text-light-gray">|</span>
                        <button className="btn btn-link" /*disabled={row.deviceCount === 0}*/ onClick={() => { isNavAssigned('events') && this.props.history.push(`/events/${row.id}`) }}>Events</button>
                        <span className="text-light-gray">|</span>
                        <button className="btn btn-link" /*disabled={row.deviceCount === 0}*/ onClick={() => { isNavAssigned('deviceList') && this.props.history.push({ pathname: "/deviceList", state: { selectedConnectorId: row.id } }) }}>Dashboards</button>
                        <span className="text-light-gray">|</span>
                        <button className="btn btn-link" /*disabled={row.deviceCount === 0}*/ onClick={() => { isNavAssigned('simulation') && this.props.history.push(`/simulation`) }}>Simulation</button>
                    </div>
                )
            },
            { Header: "Created", width: 15, accessor: "createdAt" },
            { Header: "Updated", width: 15, accessor: "updatedAt" },
            {
                Header: "Actions", width: 10,
                cell: (row) => (
                    <div className="button-group">
                        <button className="btn-transparent btn-transparent-blue" data-tooltip data-tooltip-text="Edit" data-tooltip-place="bottom" onClick={() => this.props.history.push(`/addOrEditDeviceType/${row.id}`)}>
                            <i className="far fa-pencil"></i>
                        </button>
                        <button className="btn-transparent btn-transparent-red" data-tooltip data-tooltip-text="Delete" data-tooltip-place="bottom" onClick={() => this.setState({
                            deleteName: row.name,
                            connectorToBeDeleted: row.id,
                            confirmState: true,
                            connectorName: row.name
                        })}><i className="far fa-trash-alt"></i>
                        </button>
                    </div>
                )
            },
        ]
    }
    
    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>Device Types</title>
                    <meta name="description" content="Description of Device Types" />
                </Helmet>

                <header className="content-header d-flex">
                    <div className="flex-60">
                        <h6>Device Types -
                            <span className="content-header-badge-group">
                                <span className="content-header-badge-item">Allocated <span className="badge badge-pill badge-primary">{this.state.totalDeviceTypesCount}</span></span>
                                <span className="content-header-badge-item">Used <span className="badge badge-pill badge-success">{this.state.usedDeviceTypesCount}</span></span>
                                <span className="content-header-badge-item">Remaining <span className="badge badge-pill badge-warning">{this.state.remainingDeviceTypesCount}</span></span>
                            </span>
                        </h6>
                        {this.props.match.params.connector &&
                            <h6 className="active">
                                {this.props.match.params.connector}{this.state.connectorsList.length ? ' (' + this.state.connectorsList.length + ')' : ''}
                            </h6>
                        }
                    </div>

                    <div className="flex-40 text-right">
                        <div className="content-header-group">
                            <div className="search-box">
                                <span className="search-icon"><i className="far fa-search"></i></span>
                                <input type="text" disabled={this.state.connectorsList.length > 0 || this.state.isFetching} className="form-control" placeholder="Search..." value={this.state.searchTerm} onChange={this.onSearchHandler} disabled={this.state.connectorsList.length === 0 || this.state.isFetching} />
                                {this.state.searchTerm.length > 0 &&
                                    <button className="search-button" onClick={() => this.emptySearchBox()}><i className="far fa-times"></i></button>
                                }
                            </div>
                            <button className={this.state.toggleView == true ? "btn btn-light active" : "btn btn-light"} disabled={this.state.isFetching} data-tooltip="true" data-tooltip-text="List View" data-tooltip-place="bottom" onClick={() => this.setState({ toggleView: true })}><i className="fad fa-list-ul"></i></button>
                            <button className={this.state.toggleView == false ? "btn btn-light active" : "btn btn-light"} disabled={this.state.isFetching} data-tooltip="true" data-tooltip-text="Card View" data-tooltip-place="bottom" onClick={() => this.setState({ toggleView: false })}><i className="fad fa-table"></i></button>
                            <button className="btn btn-light" disabled={this.state.isFetching} data-tooltip data-tooltip-text="Refresh" data-tooltip-place="bottom" onClick={() => this.refreshComponent()}>
                                <i className="far fa-sync-alt"></i>
                            </button>
                            <button type="button" disabled={this.state.isFetching} className="btn btn-primary" data-tooltip data-tooltip-text="Add Device Type" data-tooltip-place="bottom" disabled={this.state.remainingDeviceTypesCount === 0} onClick={() => this.props.history.push(`/addOrEditDeviceType`)}>
                                <i className="far fa-plus"></i>
                            </button>
                        </div>
                    </div>
                </header>

                <div className="content-body">
                    {this.state.isFetching ?
                        <Skeleton count={9} className="skeleton-list-loader" /> :
                        <React.Fragment>
                            {this.state.connectorsList.length > 0 ?
                                this.state.filteredDataList.length > 0 ?
                                    this.state.toggleView ?
                                        <ListingTable
                                            columns={this.getColumns()}
                                            data={this.state.filteredDataList}
                                        />
                                        :
                                        <ul className="card-view-list">
                                            {this.state.connectorsList.map((item, index) =>
                                                <li key={index}>
                                                    <div className="card-view-box">         
                                                        <div className="card-view-header">
                                                            <span className="alert alert-primary mr-r-7 cursor-pointer text-underline-hover"  onClick={() => { isNavAssigned('devices') && this.props.history.push(`/devices/${item.id}`) }}>Devices - {item.deviceCount}</span>
                                                            <span className="alert alert-success mr-r-7 cursor-pointer text-underline-hover" onClick={() => { isNavAssigned('deviceGroups') && this.props.history.push(`/deviceGroups/${item.id}`) }}>Groups - {item.groupCount}</span>
                                                            <span className="alert alert-warning">Tags - {item.tagCount}</span>
                                                            <div className="dropdown">
                                                                <button className="btn-transparent btn-transparent-gray" type="button" data-toggle="dropdown">
                                                                    <i className="fas fa-ellipsis-v"></i>
                                                                </button>
                                                                <div className="dropdown-menu">
                                                                    <button className="dropdown-item" onClick={() => this.props.history.push(`/addOrEditDeviceType/${item.id}`)}>
                                                                        <i className="far fa-pencil"></i> Edit
                                                                    </button>
                                                                    <button className="dropdown-item" onClick={() => this.setState({
                                                                        deleteName: item.name,
                                                                        connectorToBeDeleted: item.id,
                                                                        confirmState: true,
                                                                        connectorName: item.name
                                                                    })}><i className="far fa-trash-alt"></i> Delete
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="card-view-body">
                                                            <div className="card-view-icon">
                                                                <img src={item.image ? item.image + `?lastmod=${(new Date()).getTime()}` : "https://content.iot83.com/m83/misc/device-type-fallback.png"} />
                                                            </div>
                                                            <h6><i className="far fa-address-book"></i>{item.name}</h6>
                                                            <p><i className="far fa-file-alt"></i>{item.description ? item.description : "-"}</p>
                                                            <div className="button-group-link">
                                                                <button className="btn btn-link f-11" onClick={() => { isNavAssigned('deviceAttributes') && this.props.history.push(`/deviceAttributes/${item.id}`) }}>Attributes</button>
                                                                <span className="text-light-gray">|</span>
                                                                <button className="btn btn-link f-11" onClick={() => { isNavAssigned('events') && this.props.history.push(`/events/${item.id}`) }}>Events</button>
                                                                <span className="text-light-gray">|</span>
                                                                <button className="btn btn-link f-11" onClick={() => { isNavAssigned('deviceList') && this.props.history.push({ pathname: "/deviceList", state: { selectedConnectorId: item.id } }) }}>Dashboards</button>
                                                                <span className="text-light-gray">|</span>
                                                                <button className="btn btn-link f-11" onClick={() => { isNavAssigned('simulation') && this.props.history.push(`/simulation`) }}>Simulation</button>
                                                            </div>
                                                        </div>
                                                        <div className="card-view-footer d-flex">
                                                            <div className="flex-50 p-3">
                                                                <h6>Created</h6>
                                                                <p><strong>By - </strong>{item.createdBy}</p>
                                                                <p><strong>At - </strong>{new Date(item.createdAt).toLocaleString('en-US', { createdAt: localStorage.createdAt })}</p>
                                                            </div>
                                                            <div className="flex-50 p-3">
                                                                <h6>Updated</h6>
                                                                <p><strong>By - </strong>{item.updatedBy}</p>
                                                                <p><strong>At - </strong>{new Date(item.updatedAt).toLocaleString('en-US', { createdAt: localStorage.createdAt })}` `</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            )}
                                        </ul>
                                    :
                                    <NoDataFoundMessage />
                                :
                                <AddNewButton
                                    text1="No device type(s) available"
                                    text2="You haven't created any device type(s) yet. Please create a device type first."
                                    createItemOnAddButtonClick={this.goToCreateDevicePage}
                                    imageIcon="addDeviceType.png"
                                />
                            }
                        </React.Fragment>
                    }
                </div>

                {/* delete modal */}
                {this.state.confirmState &&
                    <DeleteModel
                        projectName={this.state.connectorName}
                        cancelClicked={() => { this.cancelClicked() }}
                        confirmClicked={() => {
                            this.setState({
                                isFetching: true,
                                confirmState: false,
                                isOpen: false
                            }, () => { this.props.deleteDeviceTypeById(this.state.connectorToBeDeleted) })
                        }}
                    />
                }
                {/* end delete modal */}

                {this.state.isOpen &&
                    <NotificationModal
                        type={this.state.modalType}
                        message2={this.state.message2}
                        onCloseHandler={this.onCloseHandler}
                    />
                }
            </React.Fragment>
        );
    }
}

DeviceTypes.propTypes = {
    dispatch: PropTypes.func.isRequired
};

let allSelectors = {}
Object.entries(SELECTORS).map(([key, value]) => {
    allSelectors[key] = value()
})

const mapStateToProps = createStructuredSelector(allSelectors);

function mapDispatchToProps(dispatch) {
    let allActions = { dispatch }
    Object.entries(ACTIONS).map(([key, value]) => {
        allActions[key] = (...args) => dispatch(value(...args))
    })
    return allActions;
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "deviceTypes", reducer });
const withSaga = injectSaga({ key: "deviceTypes", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(DeviceTypes);
