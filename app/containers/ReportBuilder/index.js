/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { FormattedMessage } from "react-intl";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import makeSelectReportBuilder from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import messages from "./messages";

/* eslint-disable react/prefer-stateless-function */
export class ReportBuilder extends React.Component {
  render() {
    return (
      <div>
        <Helmet>
          <title>Report Builder</title>
          <meta name="description" content="M83-ReportBuilder" />
        </Helmet>

        <div className="pageBreadcrumb">
          <div className="row">
            <div className="col-8">
              <p><span>Report Builder</span></p>
            </div>
            <div className="col-4 text-right">
              <div className="flex h-100 justify-content-end align-items-center">
                <button type="button" name="button" className="btn btn-primary">
                  <span className="btn-primary-icon"><i className="far fa-plus"></i></span>
                </button>
              </div>
            </div>
          </div>
        </div>


      </div>
    );
  }
}

ReportBuilder.propTypes = {
  dispatch: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
  reportbuilder: makeSelectReportBuilder()
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

const withReducer = injectReducer({ key: "reportBuilder", reducer });
const withSaga = injectSaga({ key: "reportBuilder", saga });

export default compose(
  withReducer,
  withSaga,
  withConnect
)(ReportBuilder);
