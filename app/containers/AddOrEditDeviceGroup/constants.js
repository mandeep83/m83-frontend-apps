/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/*
 *
 * AddOrEditDeviceGroup constants
 *
 */

export const RESET_TO_INITIAL_STATE = "app/AddOrEditDeviceGroup/RESET_TO_INITIAL_STATE";

export const ADD_UPDATE_GROUP = {
	action: 'app/AddOrEditDeviceGroup/ADD_UPDATE_GROUP',
	success: "app/AddOrEditDeviceGroup/ADD_UPDATE_GROUP_SUCCESS",
	failure: "app/AddOrEditDeviceGroup/ADD_UPDATE_GROUP_FAILURE",
	urlKey: "addOrUpdateGroup",
	successKey: "addUpdateGroupSuccess",
	failureKey: "addUpdateGroupFailure",
	actionName: "addOrUpdateGroup",
	actionArguments: ["payload"]
}

export const SAVE_TAG = {
	action: 'app/AddOrEditDeviceGroup/SAVE_TAG',
	success: "app/AddOrEditDeviceGroup/SAVE_TAG_SUCCESS",
	failure: "app/AddOrEditDeviceGroup/SAVE_TAG_FAILURE",
	urlKey: "saveTag",
	successKey: "saveTagSuccess",
	failureKey: "saveTagFailure",
	actionName: "saveTag",
	actionArguments: ["payload"]
}

export const GET_ALL_TAGS = {
	action: 'app/AddOrEditDeviceGroup/GET_ALL_TAGS',
	success: "app/AddOrEditDeviceGroup/GET_ALL_TAGS_SUCCESS",
	failure: "app/AddOrEditDeviceGroup/GET_ALL_TAGS_FAILURE",
	urlKey: "getAllTags",
	successKey: "getAllTagsSuccess",
	failureKey: "getAllTagsFailure",
	actionName: "getAllTags",
	actionArguments: ["connectorId"]
}

export const GET_TAGS_BY_GROUPID = {
	action: 'app/AddOrEditDeviceGroup/GET_TAGS_BY_GROUPID',
	success: "app/AddOrEditDeviceGroup/GET_TAGS_BY_GROUPID_SUCCESS",
	failure: "app/AddOrEditDeviceGroup/GET_TAGS_BY_GROUPID_FAILURE",
	urlKey: "getTagsByGroupId",
	successKey: "getTagsByGroupIdSuccess",
	failureKey: "getTagsByGroupIdFailure",
	actionName: "getTagsByGroupId",
	actionArguments: ["groupId"]
}

export const SAVE_TAG_BY_GROUPID = {
	action: 'app/AddOrEditDeviceGroup/SAVE_TAG_BY_GROUPID',
	success: "app/AddOrEditDeviceGroup/SAVE_TAG_BY_GROUPID_SUCCESS",
	failure: "app/AddOrEditDeviceGroup/SAVE_TAG_BY_GROUPID_FAILURE",
	urlKey: "saveTagByGroupId",
	successKey: "saveTagByGroupIdSuccess",
	failureKey: "saveTagByGroupIdFailure",
	actionName: "saveTagByGroupId",
	actionArguments: ["payload","groupId"]
}

export const DELETE_TAG = {
	action: 'app/AddOrEditDeviceGroup/DELETE_TAG',
	success: "app/AddOrEditDeviceGroup/DELETE_TAG_SUCCESS",
	failure: "app/AddOrEditDeviceGroup/DELETE_TAG_FAILURE",
	urlKey: "deleteTag",
	successKey: "deleteTagSuccess",
	failureKey: "deleteTagFailure",
	actionName: "deleteTag",
	actionArguments: ["id"]
}
