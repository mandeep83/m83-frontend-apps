/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * AddOrEditDeviceGroup
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { FormattedMessage } from "react-intl";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import reducer from "./reducer";
import saga from "./saga";
import { allSelectors as SELECTORS } from "./selectors";
import { allActions as ACTIONS } from './actions'
import { getNotificationObj, stringAppend } from "../../commonUtils";
import { cloneDeep, isEqual } from "lodash";
import NotificationModal from '../../components/NotificationModal/Loadable'
import ConfirmModel from "../../components/ConfirmModel";
import Loader from "../../components/Loader";
import TagsInput from 'react-tagsinput';
import ColorPicker from "../../components/ColorPicker";

/* eslint-disable react/prefer-stateless-function */
export class AddOrEditDeviceGroup extends React.Component {

    state = {
        isFetching: false,
        groupPayload: this.props.data.groupPayload,
        allDevices: this.props.data.groupPayload.default ?
            [] : this.props.data.allDevices.map(device => {
                device.added = false
                return device;
            }),
        filteredDevices: this.props.data.groupPayload.default ?
            [] : this.props.data.allDevices.map(device => {
                device.added = false
                return device;
            }),
        viewType: this.props.childViewType ? this.props.childViewType : 'GROUP_DETAILS',
        tagPayload: {
            tag: '',
            color: '',
            connectorId: this.props.data.selectedConnector.id
        },
        listOfAllTags: [],
        listOfTagsByGroupId: [],
        assignTags: [],
        searchDeviceTerm: '',
        availableDevicesForTag: this.props.data.groupPayload.attachedDevices.filter(device => !device.tag),
        selectedConnector: this.props.data.selectedConnector,
        groupDevices: this.props.data.groupPayload.attachedDevices,
        listOfConnectors: this.props.data.listOfConnectors,
        selectedTag: {}
    }

    componentDidMount() {
        // document.addEventListener('mousedown', this.handleClickOutside);
        if (this.state.viewType === 'GROUP_TAGVIEW') {
            if (this.props.match.params.groupId) {
                this.props.getTagsByGroupId(this.props.match.params.groupId);
            }
            this.setState({ isFetching: true })
        }
    }


    static getDerivedStateFromProps(nextProps, state) {
        if (nextProps.saveTagSuccess) {
            nextProps.getAllTags(nextProps.match.params.connectorId);
            return {
                tagPayload: {
                    tag: '',
                    color: '',
                    connectorId: nextProps.data.selectedConnector.id
                },
                isAddEditTag: false,
                modalType: "success",
                isOpen: true,
                message2: nextProps.saveTagSuccess.response,
            }
        }

        if (nextProps.getAllTagsSuccess) {
            let assignTags = cloneDeep(state.assignTags),
                listOfAllTags = nextProps.getAllTagsSuccess.response;
            if (assignTags.length === 0) {
                assignTags = [{
                    devices: [],
                    tagId: "",
                    operation: "CREATE",
                    tagName: '',
                    tagColor: ''
                }]
            }
            listOfAllTags.map(tag => {
                if (assignTags.find(assign => assign.tagId === tag.id) && assignTags.find(assign => assign.tagId === tag.id).operation !== "DELETE") {
                    tag.added = true
                }
                else {
                    tag.added = false
                }
            })
            return {
                listOfAllTags,
                assignTags,
                isFetchingTags: false,
                isFetching: false,
                isLoadingTab: false
            }
        }

        if (nextProps.getTagsByGroupIdSuccess) {
            nextProps.getAllTags(nextProps.match.params.connectorId);
            return {
                assignTags: nextProps.getTagsByGroupIdSuccess.response
            }
        }

        if (nextProps.saveTagByGroupIdSuccess) {
            return {
                modalType: "success",
                isOpen: true,
                message2: nextProps.saveTagByGroupIdSuccess.response,
                saveSuccess: "saveTagByGroupIdSuccess"
            }
        }

        if (nextProps.addUpdateGroupSuccess) {
            return {
                modalType: "success",
                isOpen: true,
                message2: nextProps.addUpdateGroupSuccess.response,
                saveSuccess: "saveGroupSuccess"
            }
        }

        if (nextProps.deleteTagSuccess) {
            let listOfAllTags = cloneDeep(state.listOfAllTags)
            listOfAllTags = listOfAllTags.filter(tag => tag.id !== nextProps.deleteTagSuccess.response.id)
            return {
                ...getNotificationObj(nextProps.deleteTagSuccess.response.message, "success"),
                listOfAllTags,
                isFetchingTags: false
            }
        }

        if (nextProps.deleteTagFailure) {
            return {
                ...getNotificationObj(nextProps.deleteTagFailure.error, "error"),
                isFetchingTags: false
            }
        }


        if (nextProps.getAllDeviceGroupsByConnectorIdSuccess && nextProps.getAllDeviceGroupsByConnectorIdSuccess.invokedFrom === "CHILD") {
            let allDevices = [],
                groupPayload = cloneDeep(state.groupPayload);
            nextProps.getAllDeviceGroupsByConnectorIdSuccess.response.map(el => {
                if (el.default) {
                    el.attachedDevices.map(device => allDevices.push(device))
                }
            })
            allDevices.map(device => {
                device.added = false
                return device;
            });
            groupPayload = {
                deviceGroupName: groupPayload.deviceGroupName,
                description: groupPayload.description,
                connectorId: state.selectedConnector.id,
                attachedDevices: [],
            }
            return {
                isFetching: false,
                allDevices,
                filteredDevices: allDevices,
                groupPayload,
                isFetchingGroups: false
            }
        }

        if (Object.keys(SELECTORS).filter(prop => prop.toLowerCase().includes("success")).some(prop => nextProps[prop])) {
            let propName = Object.keys(SELECTORS).find(props => nextProps[props]);
            return { ...getNotificationObj(nextProps[propName].response, "success") }
        }
        if (Object.keys(SELECTORS).filter(prop => prop.toLowerCase().includes("failure")).some(prop => nextProps[prop])) {
            let propName = Object.keys(SELECTORS).find(props => nextProps[props]);
            return {
                ...getNotificationObj(nextProps[propName].error, "error"),
                isFetchingTags: false,
                isFetching: false,
                isLoadingTab: false
            }
        }
        return null
    }

    componentDidUpdate(prevProps, prevState) {
        if (Object.keys(SELECTORS).some(prop => this.props[prop])) {
            this.props.resetToInitialState()
        }
    }

    groupDetailsChangeHandler = ({ currentTarget }) => {
        let groupPayload = cloneDeep(this.state.groupPayload);
        groupPayload[currentTarget.id] = currentTarget.value;
        this.setState({
            groupPayload
        });
    }

    goToDeviceGroupPage = () => {
        this.props.changeViewToList(false);
    }

    onDeviceDrag = (device) => {
        let selectedDevice = device
        delete device.added
        this.setState({
            selectedDevice
        })
    }

    allowDrop = (event) => {
        event.stopPropagation();
        event.preventDefault();
    }

    onDeviceDrop = (event) => {
        let allDevices = cloneDeep(this.state.allDevices),
            filteredDevices = cloneDeep(this.state.filteredDevices),
            groupPayload = cloneDeep(this.state.groupPayload);
        groupPayload.attachedDevices.push(this.state.selectedDevice)
        allDevices.map(device => {
            if (device._uniqueDeviceId === this.state.selectedDevice._uniqueDeviceId) {
                device.added = true
            }
            return device
        })
        filteredDevices.map(device => {
            if (device._uniqueDeviceId === this.state.selectedDevice._uniqueDeviceId) {
                device.added = true
            }
            return device
        })
        this.setState({
            allDevices,
            filteredDevices,
            groupPayload
        })
    }

    onCloseHandler = () => {
        this.setState({
            isOpen: false,
            message2: '',
            modalType: '',
        }, () => {
            if (this.state.saveSuccess === "saveGroupSuccess") {
                let increaseCount;
                increaseCount = !Boolean(this.props.match.params.groupId)
                this.props.changeViewToList(true, increaseCount)
            }
            else if (this.state.saveSuccess === "saveTagByGroupIdSuccess") {
                this.props.changeViewToList(true)
            }
        })
    }

    saveGroup = () => {
        let groupPayload = cloneDeep(this.state.groupPayload);
        groupPayload.deviceIds = groupPayload.attachedDevices.map(el => { return el._uniqueDeviceId })
        delete groupPayload.attachedDevices
        this.setState({
            isFetching: true
        }, () => this.props.addOrUpdateGroup(groupPayload))
    }

    removeDevice = (id) => {
        let allDevices = cloneDeep(this.state.allDevices),
            groupPayload = cloneDeep(this.state.groupPayload);
        !allDevices.some(device => device._uniqueDeviceId === id) ? allDevices.push(groupPayload.attachedDevices.find(device => device._uniqueDeviceId === id)) : null
        allDevices.map(device => {
            if (device._uniqueDeviceId === id) {
                device.added = false
            }
            return device
        })
        groupPayload.attachedDevices = groupPayload.attachedDevices.filter(device => device._uniqueDeviceId !== id)
        this.setState({
            allDevices,
            filteredDevices: allDevices,
            groupPayload,
            searchDeviceTerm: ''
        })
    }

    onTagChangeHandler = ({ currentTarget }) => {
        let tagPayload = cloneDeep(this.state.tagPayload)
        if (currentTarget.id === 'tag') {
            tagPayload.tag = (currentTarget.value).replace(/\s+/g, '');
        }
        else
            tagPayload[currentTarget.id] = currentTarget.value
        this.setState({
            tagPayload
        })
    }

    saveTag = () => {
        this.setState({
            isFetchingTags: true,
            showColorPicker: null
        }, () => this.props.saveTag(this.state.tagPayload))
    }

    onTagDrag = (tag) => {
        this.setState({
            selectedTag: tag
        })
    }

    onTagDrop = (event, index) => {
        let assignTags = cloneDeep(this.state.assignTags),
            listOfAllTags = cloneDeep(this.state.listOfAllTags),
            selectedTag = cloneDeep(this.state.selectedTag);
        assignTags[index].tagId = selectedTag.id;
        assignTags[index].tagName = selectedTag.tag;
        assignTags[index].tagColor = selectedTag.color;
        listOfAllTags.map(tag => {
            // tag.added = false
            // if (  assignTags.find(el => el.tagId === tag.id) && assignTags.find(el => el.tagId === tag.id).operation !== "DELETE") {
            //     tag.added = true
            // }
            selectedTag.id === tag.id ? tag.added = true : null
        })

        this.setState({
            assignTags,
            listOfAllTags,
            selectedTag: {}
        })
    }

    addDeviceToTag = (id, index) => {
        let assignTags = cloneDeep(this.state.assignTags),
            availableDevicesForTag = cloneDeep(this.state.availableDevicesForTag);

        assignTags[index].devices.push(id)
        availableDevicesForTag = availableDevicesForTag.filter(device => device._uniqueDeviceId !== id)
        assignTags[index].operation = assignTags[index].operation === "" ? "UPDATE" : assignTags[index].operation
        this.setState({
            assignTags,
            availableDevicesForTag
        })
    }

    saveTagByGroupId = () => {
        let payload = { tags: cloneDeep(this.state.assignTags) }
        if (payload.tags.some(el => ((!el.tagId) || (el.operation !== "DELETE" && !el.devices.length)))) {
            this.setState({
                isOpen: true,
                message2: 'TagId or devices cannot be empty ',
                modalType: 'error',
            })
            return
        }
        this.setState({
            isFetching: true
        }, () => {
            this.props.saveTagByGroupId(payload, this.props.match.params.groupId)
        })
    }

    onSearchHandler = ({ currentTarget }) => {
        let filteredDevices = JSON.parse(JSON.stringify(this.state.allDevices));
        if (currentTarget.value.length > 0) {
            filteredDevices = filteredDevices.filter(item => item.name ? item.name.toLowerCase().includes(currentTarget.value.toLowerCase()) : item._uniqueDeviceId.toLowerCase().includes(currentTarget.value.toLowerCase()))
        }
        this.setState({
            filteredDevices,
            searchDeviceTerm: currentTarget.value
        })
    }

    emptySearchBox = () => {
        this.setState({
            searchDeviceTerm: '',
            filteredDevices: cloneDeep(this.state.allDevices)
        })
    }

    toggleConfirmModal = (tagDetails) => {
        let { id, tag } = tagDetails || {},
            tagIdToBeDeleted = id,
            confirmState = Boolean(id),
            tagToBeDeletedName = tag ? tag || id : null
        this.setState({
            tagIdToBeDeleted,
            confirmState,
            tagToBeDeletedName
        })
    }

    deleteTagHandler = () => {
        this.setState({
            confirmState: false,
            isFetchingTags: true,
        }, () => this.props.deleteTag(this.state.tagIdToBeDeleted))
    }

    deletedAssignedTag = (tag, index) => {
        let assignTags = cloneDeep(this.state.assignTags),
            listOfAllTags = cloneDeep(this.state.listOfAllTags),
            availableDevicesForTag = cloneDeep(this.state.availableDevicesForTag);

        assignTags[index].devices.map(id => { // add devices into available devices list
            let removedDevice = this.state.groupDevices.find(el => el._uniqueDeviceId === id)
            availableDevicesForTag.push(removedDevice)
        })

        if (tag.operation === "CREATE") {
            assignTags.splice(index, 1)
        }
        else {
            assignTags[index].operation = "DELETE"
        }
        listOfAllTags.map(tags => {
            if (tags.id === tag.tagId) {
                tags.added = false
            }
        })
        this.setState({
            assignTags,
            listOfAllTags,
            availableDevicesForTag
        })
    }

    resetTag = (tag, index) => {
        // Logic here to reset the tag by index
        let assignTags = cloneDeep(this.state.assignTags),
            listOfAllTags = cloneDeep(this.state.listOfAllTags);

        listOfAllTags.map(tags => {
            if (tags.id === assignTags[index].tagId) {
                tags.added = false
            }
        })

        if (tag.operation === "CREATE") {
            assignTags.splice(index, 1, {
                devices: assignTags[index].devices,
                tagId: "",
                operation: "CREATE",
                tagColor: ""
            })
        }
        else {
            assignTags[index].operation = "DELETE"
            assignTags.splice(index + 1, 0, {
                devices: assignTags[index].devices,
                tagId: "",
                operation: "CREATE",
                tagColor: ""
            });
            assignTags[index].devices = []
        }

        this.setState({
            assignTags,
            listOfAllTags
        })
    }


    inputTagsChangeHandler = (tags, changedId, index) => {
        let assignTags = cloneDeep(this.state.assignTags),
            availableDevicesForTag = cloneDeep(this.state.availableDevicesForTag);
        if (tags.length > assignTags[index].devices)
            return

        let removedDevice = this.state.groupDevices.find(el => el._uniqueDeviceId === changedId[0] || el.name === changedId[0])
        availableDevicesForTag.push(removedDevice)
        let updatedDevices = this.state.groupDevices.filter(el => tags.some(tag => el._uniqueDeviceId === tag || el.name === tag))
        assignTags[index].devices = updatedDevices.map(el => el._uniqueDeviceId)
        assignTags[index].operation = assignTags[index].operation === "" ? "UPDATE" : assignTags[index].operation
        this.setState({
            assignTags,
            availableDevicesForTag
        })
    }

    clearDevices = (index) => {
        let assignTags = cloneDeep(this.state.assignTags),
            availableDevicesForTag = cloneDeep(this.state.availableDevicesForTag);

        assignTags[index].devices.map(id => {
            let removedDevice = this.state.groupDevices.find(el => el._uniqueDeviceId === id)
            availableDevicesForTag.push(removedDevice)
        })

        assignTags[index].operation = assignTags[index].operation === "" ? "UPDATE" : assignTags[index].operation
        assignTags[index].devices = []
        this.setState({
            assignTags,
            availableDevicesForTag
        })
    }

    editTag = (tagDetails) => {
        this.setState({
            tagPayload: {
                tag: tagDetails.tag,
                connectorId: tagDetails.connectorId,
                id: tagDetails.id,
                color: tagDetails.color
            },
            isAddEditTag: true
        })
    }

    changeSelectedConnector = ({ currentTarget }) => {
        let selectedConnector = this.state.listOfConnectors.find(el => el.id === currentTarget.value);
        this.props.changeSelectedConnector(selectedConnector)
        this.setState({
            selectedConnector,
            isFetchingGroups: true
        }, () => this.props.getAllDeviceGroupsByConnectorId(selectedConnector.id, "CHILD"))
    }

    setWrapperRef = (node) => {
        this.wrapperRef = node;
    }

    // handleClickOutside = (event) => {
    //     if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
    //         this.setState({
    //             showColorPicker: null
    //         })
    //     }
    // }

    componentWillUnmount() {
        // document.removeEventListener('mousedown', this.handleClickOutside);
    }

    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>AddOrEditDeviceGroup</title>
                    <meta name="description" content="Description of AddOrEditDeviceGroup" />
                </Helmet>

                <header className="content-header d-flex">
                    <div className="flex-60">
                        <div className="d-flex">
                            <h6 className="previous" onClick={this.goToDeviceGroupPage}>Device Groups</h6>
                            <h6 className="active">{this.props.match.params.groupId ? this.state.groupPayload.deviceGroupName : "Add New"}</h6>
                        </div>
                    </div>
                    <div className="flex-40 text-right">
                        <div className="content-header-group">
                            <button type='button' className="btn btn-light" data-tooltip data-tooltip-text="Back" data-tooltip-place="bottom" onClick={this.goToDeviceGroupPage}><i className="fas fa-angle-left"></i></button>
                        </div>
                    </div>
                </header>

                {this.state.isFetching ?
                    <Loader /> :
                    <React.Fragment>
                        <div className="content-body">
                            <div className="content-body-tab">
                                <ul className="nav nav-tabs list-style-none">
                                    <li className={`nav-item ${this.state.viewType === 'GROUP_DETAILS' ? 'active' : ''}`} onClick={() => !this.state.isFetchingGroups && this.setState({//reset to inital state
                                        viewType: 'GROUP_DETAILS',
                                        groupPayload: this.props.data.groupPayload,
                                        searchDeviceTerm: '',
                                        allDevices: this.props.data.groupPayload.default ?
                                            [] : this.props.data.allDevices.map(device => {
                                                device.added = false
                                                return device;
                                            }),
                                        filteredDevices: this.props.data.groupPayload.default ?
                                            [] : this.props.data.allDevices.map(device => {
                                                device.added = false
                                                return device;
                                            }),
                                    })}>
                                        <a className="nav-link"><i className="fad fa-users"></i>Group</a>
                                    </li>
                                    <li className={`nav-item ${this.state.viewType === 'GROUP_TAGVIEW' ? 'active' : ''}`} style={{ "cursor": this.props.match.params.groupId && !this.state.isFetchingGroups ? "auto" : "not-allowed" }} onClick={() => this.props.match.params.groupId && this.state.viewType !== 'GROUP_TAGVIEW' && !this.state.isFetchingGroups ? this.setState({//reset to inital state
                                        viewType: 'GROUP_TAGVIEW',
                                        isLoadingTab: true,
                                        availableDevicesForTag: this.state.groupDevices.filter(device => !device.tag)
                                    }, () => this.props.getTagsByGroupId(this.props.match.params.groupId)) : false}>
                                        <a style={{ "pointerEvents": this.props.match.params.groupId && !this.state.isFetchingGroups ? "auto" : "none" }} className="nav-link"><i className="fad fa-tag"></i>Tags</a>
                                    </li>
                                </ul>
                            </div>

                            <div className="tab-content">
                                {this.state.viewType === 'GROUP_DETAILS' ?
                                    <div className={`tab-pane fade show ${this.state.viewType === 'GROUP_DETAILS' ? 'active' : ''}`} id="tab1">
                                        <form onSubmit={this.saveGroup}>
                                            <div className="form-content-wrapper">
                                                <div className="form-content-box mb-0">
                                                    <div className="form-content-header">
                                                        <p>Basic Information</p>
                                                    </div>
                                                    <div className="form-content-body">
                                                        {this.state.isFetchingGroups ?
                                                            <div className="inner-loader-wrapper content-body-tab-loader">
                                                                <div className="inner-loader-content">
                                                                    <i className="fad fa-sync-alt fa-spin"></i>
                                                                </div>
                                                            </div> :
                                                            <React.Fragment>
                                                                <div className="d-flex">
                                                                    <div className="flex-33 pd-r-10">
                                                                        <div className="form-group">
                                                                            <label className="form-group-label">Device Type : <i className="fas fa-asterisk form-group-required"></i></label>
                                                                            <select id='connectorSelect' className="form-control" value={this.state.selectedConnector.id} onChange={this.changeSelectedConnector} disabled={this.props.match.params.groupId} >
                                                                                {this.state.listOfConnectors.map(connector => {
                                                                                    return (
                                                                                        <option key={connector.id} value={connector.id}>{connector.name}</option>
                                                                                    )
                                                                                })}
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div className="flex-33 pd-r-5 pd-l-5">
                                                                        <div className="form-group">
                                                                            <label className="form-group-label">Name : <i className="fas fa-asterisk form-group-required"></i></label>
                                                                            <input placeholder="e.g. East Group or Test Device Group" type="text" id="deviceGroupName" className="form-control" pattern="^[a-zA-Z0-9_-]+( [a-zA-Z0-9_-]+)*$" value={this.state.groupPayload.deviceGroupName} onChange={this.groupDetailsChangeHandler} maxLength="30" required />
                                                                        </div>
                                                                    </div>
                                                                    <div className="flex-33 pd-l-10">
                                                                        <div className="form-group">
                                                                            <label className="form-group-label">Description :</label>
                                                                            <textarea rows="1" id="description" className="form-control" value={this.state.groupPayload.description} onChange={this.groupDetailsChangeHandler} />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div className="form-group mb-0">
                                                                    <h6 className="form-group-label">Add Devices to Group :</h6>
                                                                    <div className="drop-box" onDrop={(e) => this.onDeviceDrop(e)} onDragOver={this.allowDrop}>
                                                                        {this.state.groupPayload.attachedDevices.length > 0 ?
                                                                            <ul className="list-style-none d-flex">
                                                                                {this.state.groupPayload.attachedDevices.map(device => {
                                                                                    return (
                                                                                        <li key={device._uniqueDeviceId}>
                                                                                            <div className="drop-item-box">{device.name ? device.name : device._uniqueDeviceId}
                                                                                                <button type='button' className="btn-transparent btn-transparent-red" disabled={this.state.groupPayload.default} onClick={() => this.removeDevice(device._uniqueDeviceId)}>
                                                                                                    <i className="far fa-times"></i>
                                                                                                </button>
                                                                                            </div>
                                                                                        </li>
                                                                                    )
                                                                                })}
                                                                            </ul> :
                                                                            <div className="drop-box-overlay">
                                                                                <div className="text-center">
                                                                                    <i className="fad fa-draw-square text-theme f-24"></i>
                                                                                    <h6>Drag And Drop Device Here</h6>
                                                                                </div>
                                                                            </div>
                                                                        }
                                                                    </div>
                                                                </div>
                                                            </React.Fragment>
                                                        }
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="form-info-wrapper">
                                                <div className="form-info-header">
                                                    <h5>Available Devices</h5>
                                                </div>
                                                <div className="form-info-body form-info-body-adjust">
                                                    {this.state.isFetchingGroups ?
                                                        <div className="inner-loader-wrapper h-100">
                                                            <div className="inner-loader-content">
                                                                <i className="fad fa-sync-alt fa-spin"></i>
                                                            </div>
                                                        </div> :
                                                        <React.Fragment>
                                                            <ul className="list-style-none drag-list">
                                                                {(this.state.allDevices.length > 0 && this.state.filteredDevices.length ? (this.state.filteredDevices.some(device => !device.added) || this.state.searchDeviceTerm.length > 0) : true) &&
                                                                    <div className="form-group search-wrapper">
                                                                        <div className="search-wrapper-group">
                                                                            <input type="text" className="form-control" placeholder="Search..." value={this.state.searchDeviceTerm} onChange={this.onSearchHandler} />
                                                                            <span className="search-wrapper-icon"><i className="far fa-search"></i></span>
                                                                            {this.state.searchDeviceTerm.length > 0 &&
                                                                                <button type='button' className="search-wrapper-button" onClick={() => this.emptySearchBox()}><i className="far fa-times"></i></button>
                                                                            }
                                                                        </div>
                                                                    </div>
                                                                }
                                                                {this.state.filteredDevices.length ?
                                                                    this.state.filteredDevices.some(device => !device.added) ?
                                                                        this.state.filteredDevices.map(device => {
                                                                            if (!device.added)
                                                                                return (
                                                                                    <li className='active' key={device._uniqueDeviceId} draggable onDragStart={() => this.onDeviceDrag(device)}>
                                                                                        <div className="drag-list-item">{device.name ? device.name : device._uniqueDeviceId}</div>
                                                                                    </li>
                                                                                )
                                                                        }) :
                                                                        <div className="drag-list-empty">
                                                                            <div>
                                                                                <img src="https://content.iot83.com/m83/misc/addDevice.png" />
                                                                                <h6>No device(s) available</h6>
                                                                                <p>You have used all device(s). Please create a new device first.</p>
                                                                            </div>
                                                                        </div>
                                                                    : this.state.groupPayload.default ?
                                                                        <div className="drag-list-empty">
                                                                            <div>
                                                                                <img src="https://content.iot83.com/m83/misc/addDevice.png" />
                                                                                <h6>No device(s) available</h6>
                                                                                <p>You cannot add device(s) to default group.Delete device(s) from other group(s).</p>
                                                                            </div>
                                                                        </div> :
                                                                        <div className="drag-list-empty">
                                                                            <div>
                                                                                <img src="https://content.iot83.com/m83/misc/addDevice.png" />
                                                                                <h6>No device(s) available</h6>
                                                                                <p>{this.state.searchDeviceTerm.length ? "Requested search paramters doesn't match any device" : "You havent created any devices(s) yet. Please create a new device."}</p>
                                                                            </div>
                                                                        </div>
                                                                }
                                                            </ul>
                                                            <div className="form-info-footer">
                                                                <button type='button' className="btn btn-light" onClick={this.goToDeviceGroupPage}>Cancel</button>
                                                                <button type='submit' className="btn btn-primary">Save</button>
                                                            </div>
                                                        </React.Fragment>
                                                    }
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    :
                                    <div className={`tab-pane fade show ${this.state.viewType === 'GROUP_TAGVIEW' ? 'active' : ''}`} id="tab2">
                                        <div className="form-content-wrapper">
                                            <div className="form-content-box mb-0">
                                                <div className="form-content-header">
                                                    <p>Device - Tag Mapping
                                                        <button type="button" className="btn btn-link" disabled={this.state.availableDevicesForTag.length === 0 || !this.state.listOfAllTags.some(tag => !tag.added)} onClick={() => {
                                                            let assignTags = cloneDeep(this.state.assignTags);
                                                            assignTags.push({
                                                                devices: [],
                                                                tagId: "",
                                                                operation: "CREATE"
                                                            })
                                                            this.setState({ assignTags })
                                                        }}>
                                                            <i className="far fa-plus"></i>Add Mapping
                                                        </button>
                                                    </p>
                                                </div>

                                                <div className="form-content-body">
                                                    {this.state.isLoadingTab ?
                                                        <div className="inner-loader-wrapper content-body-tab-loader">
                                                            <div className="inner-loader-content">
                                                                <i className="fad fa-sync-alt fa-spin"></i>
                                                            </div>
                                                        </div> :
                                                        <div className="tag-drop-box">
                                                            {this.state.assignTags.map((tag, index) => {
                                                                if (tag.operation !== "DELETE")
                                                                    return (
                                                                        <div className="d-flex tag-drop-box-content" key={index}>
                                                                            <div className="flex-65 position-relative">
                                                                                <div className="form-group mb-0">
                                                                                    <div className="input-group">
                                                                                        <div className="input-group-prepend">
                                                                                            <div className="input-group-text" onClick={(e) => {
                                                                                                e.stopPropagation();
                                                                                                this.setState((state) => ({ selectedTabIndex: state.selectedTabIndex == index ? -1 : index }))
                                                                                            }}><i className={`far fa-angle-${this.state.selectedTabIndex === index ? 'up' : 'down'}`}></i></div>
                                                                                        </div>
                                                                                        <TagsInput value={tag.devices.map(dev => { return this.state.groupDevices.find(el => el._uniqueDeviceId === dev) && this.state.groupDevices.find(el => el._uniqueDeviceId === dev).name ? this.state.groupDevices.find(el => el._uniqueDeviceId === dev).name : dev })} inputProps={{ placeholder: 'Select Devices' }} onChange={(tags, changedId) => { this.inputTagsChangeHandler(tags, changedId, index) }} renderInput={() => { return false }} />
                                                                                    </div>
                                                                                </div>
                                                                                {this.state.availableDevicesForTag.length > 0 ?
                                                                                    <div className={`collapse ${this.state.selectedTabIndex === index ? 'show' : ''}`}>
                                                                                        <ul className="list-style-none d-flex custom-menu-list mt-2">
                                                                                            {this.state.availableDevicesForTag.map(device => {
                                                                                                return (
                                                                                                    <li key={device._uniqueDeviceId}>
                                                                                                        <div className="custom-menu-list-item" onClick={() => this.addDeviceToTag(device._uniqueDeviceId, index)}>
                                                                                                            <button type='button' className="btn text-green">
                                                                                                                <i className="far fa-plus"></i>
                                                                                                            </button>
                                                                                                            <h6>{device.name ? device.name : device._uniqueDeviceId}</h6>
                                                                                                        </div>
                                                                                                    </li>
                                                                                                )
                                                                                            })}
                                                                                        </ul>
                                                                                    </div> : ""
                                                                                }
                                                                            </div>
                                                                            <div className="flex-35">
                                                                                <ul className="list-style-none tag-list">
                                                                                    {tag.tagId ?
                                                                                        <li className="mr-0">
                                                                                            <i className="fad fa-triangle tag-item-icon"></i>
                                                                                            <div className="tag-item" style={{ background: tag.tagColor }}>{tag.tagName}</div>
                                                                                        </li> :
                                                                                        <div className="tag-list-overlay" onDrop={(e) => { this.state.selectedTag.id && this.onTagDrop(e, index) }} onDragOver={this.allowDrop}>Drag & drop tag here</div>
                                                                                    }
                                                                                </ul>
                                                                            </div>
                                                                            <div className="button-group">
                                                                                <button className="btn-transparent btn-transparent-yellow" data-tooltip data-tooltip-text="Clear Devices" data-tooltip-place="bottom" onClick={() => this.clearDevices(index)}><i className="far fa-broom"></i></button>
                                                                                <button className="btn-transparent btn-transparent-blue" data-tooltip data-tooltip-text="Reset Tag" data-tooltip-place="bottom" onClick={() => { this.resetTag(tag, index) }}><i className="far fa-redo"></i></button>
                                                                                <button className="btn-transparent btn-transparent-red" data-tooltip data-tooltip-text="Delete" data-tooltip-place="bottom" onClick={() => { this.deletedAssignedTag(tag, index) }}><i className="far fa-trash-alt"></i></button>
                                                                            </div>
                                                                        </div>
                                                                    )
                                                            })}
                                                        </div>
                                                    }
                                                </div>
                                            </div>
                                        </div>

                                        <div className="form-info-wrapper">
                                            <div className="form-info-header">
                                                <h5>Available Tags
                                                        <button type='button' className="btn btn-link p-0 float-right" disabled={this.state.isAddEditTag} onClick={() => this.setState({
                                                    isAddEditTag: true,
                                                    tagPayload: {
                                                        tag: '',
                                                        color: '',
                                                        connectorId: this.props.data.selectedConnector.id
                                                    }
                                                })}><i className="far fa-plus"></i>Add Tag</button>
                                                </h5>
                                            </div>
                                            <div className="form-info-body form-info-body-adjust">
                                                {this.state.isFetchingTags || this.state.isLoadingTab ?
                                                    <div className="inner-loader-wrapper h-100">
                                                        <div className="inner-loader-content">
                                                            <i className="fad fa-sync-alt fa-spin"></i>
                                                        </div>
                                                    </div> :
                                                    <ul className="list-style-none tag-list position-relative">
                                                        {this.state.isAddEditTag &&
                                                            <React.Fragment>
                                                                <div className="form-group">
                                                                    <label className="form-group-label">Name :</label>
                                                                    <input type="text" className="form-control" id="tag" value={this.state.tagPayload.tag} onChange={this.onTagChangeHandler} />
                                                                </div>
                                                                <div className="form-group">
                                                                    <label className="form-group-label">Color :</label>
                                                                    <ColorPicker
                                                                        setWrapperRef={(node) => this.setWrapperRef(node)}
                                                                        toggleColorPicker={() => { this.setState((prevState) => ({ showColorPicker: !prevState.showColorPicker })) }}
                                                                        isPickerVisible={this.state.showColorPicker}
                                                                        colorChangeHandler={(color) => {
                                                                            let tagPayload = cloneDeep(this.state.tagPayload);
                                                                            tagPayload.color = color.hex;
                                                                            this.setState({
                                                                                tagPayload
                                                                            })
                                                                        }}
                                                                        selectedColor={this.state.tagPayload.color}
                                                                    />
                                                                </div>
                                                                <div className="button-group text-right">
                                                                    <button type='button' className="btn-transparent btn-transparent-green" data-tooltip data-tooltip-text="Save Tag" data-tooltip-place="bottom" disabled={!this.state.tagPayload.tag} onClick={this.saveTag}><i className="far fa-check"></i></button>
                                                                    <button type='button' className="btn-transparent btn-transparent-gray" data-tooltip data-tooltip-text="Cancel" data-tooltip-place="bottom" onClick={() => this.setState({
                                                                        isAddEditTag: false,
                                                                        tagPayload: {},
                                                                        showColorPicker: null
                                                                    })}><i className="far fa-times"></i></button>
                                                                </div>
                                                            </React.Fragment>
                                                        }
                                                        {(!this.state.isAddEditTag) ?
                                                            this.state.listOfAllTags.length ?
                                                                this.state.listOfAllTags.some(tag => !tag.added) ?
                                                                    this.state.listOfAllTags.map(tag => {
                                                                        if (!tag.added)
                                                                            return (
                                                                                <li key={tag.id}>
                                                                                    <div className="input-group">
                                                                                        <i className="fad fa-triangle tag-item-icon"></i>
                                                                                        <div className="tag-item" style={{ background: tag.color }} draggable={!(this.state.tagPayload.id === tag.id)} onDragStart={() => { this.state.tagPayload.id === tag.id ? false : this.onTagDrag(tag) }}>{tag.tag}</div>
                                                                                        <div className="input-group-append">
                                                                                            <button className="btn btn-transparent-blue" data-tooltip data-tooltip-text="Edit" data-tooltip-place="bottom" disabled={this.state.assignTags.some(assign => assign.tagId === tag.id) || (this.state.tagPayload.id === tag.id)} onClick={() => this.editTag(tag)}>
                                                                                                <i className="far fa-pen"></i>
                                                                                            </button>
                                                                                            <button className="btn btn-transparent-red" data-tooltip data-tooltip-text="Delete" data-tooltip-place="bottom" disabled={this.state.assignTags.some(assign => assign.tagId === tag.id) || (this.state.tagPayload.id === tag.id)} onClick={() => this.toggleConfirmModal(tag)}>
                                                                                                <i className="far fa-trash-alt"></i>
                                                                                            </button>
                                                                                        </div>
                                                                                    </div>
                                                                                </li>
                                                                            )
                                                                    }) :
                                                                    <div className="tag-list-empty">
                                                                        <div>
                                                                            <img src="https://content.iot83.com/m83/misc/addDevice.png" />
                                                                            <h6>No tags(s) available</h6>
                                                                            <p>You have used all tags(s). Please create a new tag first.</p>
                                                                        </div>
                                                                    </div> :
                                                                <div className="tag-list-empty">
                                                                    <div>
                                                                        <img src="https://content.iot83.com/m83/misc/addDevice.png" />
                                                                        <h6>No tags(s) available</h6>
                                                                        <p>You havent created any tag(s) yet. Please create a new tag.</p>
                                                                    </div>
                                                                </div> : null
                                                        }
                                                    </ul>
                                                }
                                            </div>
                                            <div className="form-info-footer">
                                                <button type='button' className="btn btn-light" onClick={this.goToDeviceGroupPage}>Cancel</button>
                                                <button type='button' className="btn btn-primary" disabled={this.state.assignTags.length === 0 || this.state.isAddEditTag} onClick={() => this.saveTagByGroupId()}>Save</button>
                                            </div>
                                        </div>
                                    </div>
                                }
                            </div>

                        </div>
                    </React.Fragment>
                }

                {this.state.confirmState &&
                    <ConfirmModel
                        status={"delete"}
                        deleteName={this.state.tagToBeDeletedName}
                        confirmClicked={this.deleteTagHandler}
                        cancelClicked={this.toggleConfirmModal}
                    />
                }

                {this.state.isOpen &&
                    <NotificationModal
                        type={this.state.modalType}
                        message2={this.state.message2}
                        onCloseHandler={this.onCloseHandler}
                    />
                }
            </React.Fragment>

        );
    }
}

AddOrEditDeviceGroup.propTypes = {
    dispatch: PropTypes.func.isRequired
};


let allSelectors = {}
Object.entries(SELECTORS).map(([key, value]) => {
    allSelectors[key] = value()
})

const mapStateToProps = createStructuredSelector(allSelectors);

function mapDispatchToProps(dispatch) {
    let allActions = { dispatch }
    Object.entries(ACTIONS).map(([key, value]) => {
        allActions[key] = (...args) => dispatch(value(...args))
    })
    return allActions;
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "addOrEditDeviceGroup", reducer });
const withSaga = injectSaga({ key: "addOrEditDeviceGroup", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(AddOrEditDeviceGroup);
