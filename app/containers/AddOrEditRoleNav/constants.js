/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

export const DEFAULT_ACTION = 'app/AddOrEditRoleNav/DEFAULT_ACTION';
export const FETCH_DETAILS = 'app/AddOrEditRoleNav/FETCH_DETAILS';
export const FETCH_DETAILS_SUCCESS = 'app/AddOrEditRoleNav/FETCH_DETAILS_SUCCESS';
export const FETCH_DETAILS_FAILURE = 'app/AddOrEditRoleNav/FETCH_MAPPING_FAILURE';
export const FETCH_NAV = 'app/AddOrEditRoleNav/FETCH_NAV';
export const FETCH_NAV_SUCCESS = 'app/AddOrEditRoleNav/FETCH_NAV_SUCCESS';
export const FETCH_NAV_FAILURE = 'app/AddOrEditRoleNav/FETCH_NAV_FAILURE';
export const SAVE_NAVIGATION ='app/AddOrEditRoleNav/SAVE_NAVIGATION';
export const SAVE_NAVIGATION_SUCCESS ='app/AddOrEditRoleNav/SAVE_NAVIGATION_SUCCESS';

