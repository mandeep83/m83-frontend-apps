/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { fromJS } from 'immutable';
import {DEFAULT_ACTION,FETCH_DETAILS_SUCCESS,FETCH_DETAILS_FAILURE,SAVE_NAVIGATION_SUCCESS} from './constants';

export const initialState = fromJS({});

function addOrEditRoleNavReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case FETCH_DETAILS_SUCCESS:
      return Object.assign({}, state,{
      detailsList: action.response 
      })
    case FETCH_DETAILS_FAILURE:
      return Object.assign({},state,{
        detailsListFailure: action.error
      })
    case SAVE_NAVIGATION_SUCCESS:
    return Object.assign({}, state,{
      saveNavigation: action.response 
      })
    default:
      return state;
  }
}

export default addOrEditRoleNavReducer;
