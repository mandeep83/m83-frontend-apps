/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { takeEvery } from 'redux-saga';
import {
  FETCH_DETAILS, FETCH_DETAILS_SUCCESS, FETCH_DETAILS_FAILURE,
  SAVE_NAVIGATION, SAVE_NAVIGATION_SUCCESS,} from './constants';
import {apiCallHandler} from '../../api';

export function* getDetailsApiHandlerAsync(action) {
  yield[apiCallHandler(action, FETCH_DETAILS_SUCCESS, FETCH_DETAILS_FAILURE, 'fetchMenuDetails')];
}

export function* apiForSaveNavigationHandlerAsync(action) {
  yield[apiCallHandler(action, SAVE_NAVIGATION_SUCCESS, '', 'saveMenuMapping')];
}


export function* detailsWatcherGetListRequest() {
  yield takeEvery(FETCH_DETAILS,getDetailsApiHandlerAsync);
}

export function* saveNavigationApiRequest() {
  yield takeEvery(SAVE_NAVIGATION,apiForSaveNavigationHandlerAsync);
}

export default function* rootSaga() {
  yield [detailsWatcherGetListRequest(),saveNavigationApiRequest()];
}

