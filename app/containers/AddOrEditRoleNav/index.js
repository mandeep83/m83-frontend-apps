/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {Helmet} from 'react-helmet';
import {createStructuredSelector} from 'reselect';
import {compose} from 'redux';
import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import RGL, { WidthProvider } from "react-grid-layout";
import NotificationModal from '../../components/NotificationModal/Loadable'
import Loader from '../../components/Loader/Loadable';
import { getDetails, saveNavigation} from './actions';
import {getIsFetching, detailsList, getdetailsListFailure, getSavedNavigation} from './selectors';
import reducer from './reducer';
import saga from './saga';

const ReactGridLayout = WidthProvider(RGL);

/* eslint-disable react/prefer-stateless-function */
export class AddOrEditRoleNav extends React.Component {
    state = {
        payload :{
          "description" : '',
          "reportTo" : '',
          "type" : '',
          mappedNav: []
        },
        name: '',
        updatemenu: [],
    }

    submit = () => {
        confirmAlert({
            title: 'Confirm to submit',
            message: 'Are you sure to do this.',
            buttons: [
                {
                    label: 'Yes',
                    onClick: () => alert('Click Yes')
                },
                {
                    label: 'No',
                    onClick: () => alert('Click No')
                }
            ]
        })
    };

    componentDidMount() {
        this.props.getDetails(this.props.match.params.id)
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.payload && nextProps.payload != this.props.payload) {
            let innerLayout = [];
            let layout = nextProps.payload.mappedNav.map((menuitem,index) => {
                if(menuitem.subMenus.length > 0){
                    menuitem.subMenus.map((inner,innerindex) => {
                        innerLayout.push({
                            i: inner.id,
                            x: 0,
                            y: 2 + (innerindex * 2),
                            w: 12,
                            h: 2,
                            static: false,
                            position: innerindex+1,
                            isResizable: false,
                        })
                    })
                }
                return {
                    i: menuitem.id,
                    x: 0,
                    y: 2 + (index * 2),
                    w: 12,
                    h: 2,
                    static: false,
                    position: index+1,
                    isResizable: false,
                }
            })
            this.setState({
                nav : nextProps.payload.mappedNav,
                innerLayout,
                layout,
                payload: nextProps.payload
            })
        }

        if (nextProps.savedNavigation != this.props.savedNavigation) {
            this.setState({
                open: true
            })
        }

    }

    nameChangeHandler = (event) => {
        this.setState({
            name: event.target.value
        })
    }

    subMenuCheckBox = (index, subMenuIndex) => {
        let menuItems = JSON.parse(JSON.stringify(this.state.nav));
        menuItems[index].subMenus[subMenuIndex].assigned = !menuItems[index].subMenus[subMenuIndex].assigned;
        if (menuItems[index].subMenus.some(check => check.assigned)) {
            menuItems[index].assigned = true;
        } else {
            menuItems[index].assigned = false;
        }
        this.setState({
            nav:menuItems,
        })
    }

    checkBoxOnChange = (index) => {
        let navposition = JSON.parse(JSON.stringify(this.state.nav));
        navposition[index].assigned = !navposition[index].assigned;
        if (navposition[index].subMenus.length > 0) {
            navposition[index].subMenus.map((submenuObjects) => {
                submenuObjects.assigned = navposition[index].assigned;
            })
        }
        this.setState({
            nav: navposition
        })
    }

    saveNavigation = (event) => {
        const updatemenu = this.state.nav.filter((checkSelected) => checkSelected.assigned === true);
        const menuIdCapture = [];
        updatemenu.map((parentselect) => {
            if (parentselect.assigned) {
                menuIdCapture.push(parentselect.id);
            }
            if (parentselect.subMenus.length > 0) {
                parentselect.subMenus.map((selectedmenu) => {
                    if (selectedmenu.assigned) {
                        menuIdCapture.push(selectedmenu.id);
                    }
                })
            }
        })
        event.preventDefault()
        let payload = {};
        payload.menuId = menuIdCapture
        payload.roleId = this.props.match.params.id
        this.props.saveNavigation(payload);
    }

    onCloseHandler = () => {
        this.props.history.push(`/manageRoles`);
    }

    childListDisplay = (menuId) => {
        let menus = JSON.parse(JSON.stringify(this.state.nav))
        menus = menus.map((temp) => {
            if (temp.id == menuId) {
                temp.childVisibility = !temp.childVisibility
                if (temp.childVisibility) {
                    let layout = JSON.parse(JSON.stringify(this.state.layout));
                    layout = layout.map((layout) => {
                        if (layout.i == temp.id) {
                            if (temp.subMenus)
                                layout.h = (2 * temp.subMenus.length) + 3;
                            else
                                layout.h = (2 * 0) + 1;
                            layout.isDraggable = false;
                        }
                        return (layout);
                    })
                    this.setState({ layout: layout })
                } else {
                    let layout = JSON.parse(JSON.stringify(this.state.layout));
                    layout = layout.map((layout) => {
                        if (layout.i == temp.id) {
                            layout.h = 2;
                            layout.isDraggable = true;
                        }
                        return (layout);
                    })
                    this.setState({ layout: layout })
                }
            }
            return temp;
        })
        this.setState({ nav: menus });
    }

    childlayoutHandler = (layout) => {
        let innerLayout = JSON.parse(JSON.stringify(this.state.innerLayout));
        let nav = JSON.parse(JSON.stringify(this.state.nav));
        innerLayout = innerLayout.map((child, index) => {
            layout.map((layout) => {
                if (child.i == layout.i) {
                    child.y = layout.y;
                    child.h = child.h;
                }
            })
            return (child);
        })
        nav.map(menu => {
            if (menu.subMenus.length > 0 && menu.subMenus[0].id === layout[0].i) {
                menu.subMenus.map((item, index) => {
                    item.position = (layout[index].y / 2) + 1
                })
            }
        })
        this.setState({
            nav,
            innerLayout,
        })
    }

    layoutChangeHandler = (layout) => {

        let nav = [...this.state.nav];
        let menulayout = [...this.state.layout], position = 1;
        menulayout = menulayout.map((layoutitem, index) => {
            layoutitem.y = layout[index].y
            layoutitem.h = layout[index].h
            return layoutitem;
        })
        for (let check = 0; menulayout.some(val => val.y >= check); check++) {
            if (menulayout.find(lay => lay.y === check)) {
                nav.map((item) => {
                    if (item.id === menulayout.find(lay => lay.y === check).i)
                        item.position = position++
                    return item;
                })
            }
        }
        this.setState({
            layout: menulayout,
            nav,
        })
    }

    render() {
        let listHtml;
        let temp;
        if (this.state.layout) {
            temp = this.state.nav.map((menuObject, index) =>{
                return (
                    <li className="nav-item" key={menuObject.id}>
                        <label className="checkboxLabel">
                            <input type="checkbox" checked={menuObject.assigned} onChange={() => this.checkBoxOnChange(index)}/>
                            <span className="checkmark"></span>
                        </label>
                        <a className="nav-link">
                            <span>{menuObject.navName}</span>
                            {menuObject.subMenus.length > 0 ?
                                <i className={menuObject.childVisibility ?
                                    "fal fa-angle-up collapseIcon cursor-pointer noDrag" : "fal fa-angle-down collapseIcon cursor-pointer noDrag"}
                                    onClick={() => { this.childListDisplay(menuObject.id) }}
                                /> : ""}
                        </a>
                        {menuObject.childVisibility &&
                            <ul>
                                <ReactGridLayout
                                    layout={this.state.innerLayout}
                                    rowHeight={11}
                                    onLayoutChange={this.childlayoutHandler}
                                    {...this.props}
                                >
                                    {menuObject.subMenus.map((subMenusObjects, i) => (
                                        <li className="nav-item" key={subMenusObjects.id}>
                                            <label className="checkboxLabel noDrag">
                                                <input type="checkbox"
                                                    checked={subMenusObjects.assigned}
                                                    onChange={() => this.subMenuCheckBox(index, i)} />
                                                <span className="checkmark"></span>
                                            </label>
                                            <a className="nav-link">
                                                <span>{subMenusObjects.navName}</span>
                                            </a>
                                        </li>
                                    ))}
                                </ReactGridLayout>
                            </ul>
                        }
                    </li>
                )
            })
            listHtml = (
                <div className="form-group">
                    <div className="flex">
                        <label className="form-group-left-label" htmlFor="">Assign Navigations :</label>
                        <div className="form-group-right-label">
                            <ul className="navbar-nav navigationList">
                            <ReactGridLayout
                                id="outerGrid"
                                rowHeight={11}
                                layout={this.state.layout}
                                draggableCancel=".noDrag"
                                onLayoutChange={this.layoutChangeHandler}
                                {...this.props}
                            >
                                {temp}
                            </ReactGridLayout>
                            </ul>
                        </div>
                    </div>
                </div>
            )
        } else {
            listHtml =
                <div className="emptyMessage">
                    <p><i className="fal fa-file-exclamation"/>Sorry! no navigations found.</p>
                </div>
        }
        return (
            <React.Fragment>
                <Helmet>
                    <title>Add/Edit Role Navigation</title>
                    <meta name="description" content="M83-AddOrEditRoleNavigation"/>
                </Helmet>

                <div className="pageBreadcrumb">
                    <div className="row">
                        <div className="col-8">
                            <p>
                                <span className="previousPage" onClick={() => {this.props.history.push('/manageRoles');}}>Roles</span>
                                <span className="active">Assign Menu</span>
                            </p>

                        </div>
                        <div className="col-4"></div>
                    </div>
                </div>
                {this.props.isFetching ? <Loader/> :
                <div className="outerBox">
                    <form className="contentForm">
                        <div className="flex">
                            <div className="flex-item fx-b50 pd-r-7">
                                <div className="form-group">
                                    <input
                                        type="text" name="roleName" placeholder="Role Name" required disabled
                                        className="form-control" value={this.props.match.params.name} onChange={this.nameChangeHandler}
                                    />
                                    <label className="form-group-label" htmlFor="roleName">Role name :</label>
                                </div>
                            </div>
                            <div className="flex-item fx-b50 pd-l-7">
                                <div className="form-group">
                                    <input
                                        type="text" name="roleType" placeholder="Role Type" required disabled
                                        className="form-control" value={this.state.payload.type}
                                    />
                                    <label className="form-group-label" htmlFor="roleType">Type :</label>
                                </div>
                            </div>
                            <div className="flex-item fx-b50 pd-r-7">
                                <div className="form-group">
                                    <input
                                        type="text" name="reportsTo" placeholder="Reports To" required disabled
                                        className="form-control" value={this.state.payload.reportTo}
                                    />
                                    <label className="form-group-label" htmlFor="reportsTo">Reports To :</label>
                                </div>
                            </div>
                            <div className="flex-item fx-b50 pd-l-7">
                                <div className="form-group">
                                    <input
                                        type="text" name="desc" placeholder="Description" disabled
                                        className="form-control" value={this.state.payload.description}
                                    />
                                    <label className="form-group-label" htmlFor="desc">Description :</label>
                                </div>
                            </div>
                        </div>

                         {listHtml}

                        <div className="bottomButton text-right">
                            <button type="submit" name="button" className="btn btn-success mr-r-10" onClick={this.saveNavigation}>
                                <span className="btn-success-icon text-success"><i className="far fa-check text-white mr-r-5"/></span>Save
                            </button>
                            <button type="button" name="button" className="btn btn-secondary" onClick={() => {this.props.history.push('/manageRoles');}}>
                                Cancel
                            </button>
                        </div>
                    </form>
                </div>}

             
                 {   this.state.open &&
                    <NotificationModal 
                        type= "success"
                        message2 = "Role based Navigation is updated successfully !"
                        onCloseHandler = {this.onCloseHandler}
                    />
                }
            </React.Fragment>
        );
    }
}

AddOrEditRoleNav.propTypes = {
    dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
    isFetching: getIsFetching(),
    payload: detailsList(),
    getpayloadFailure: getdetailsListFailure(),
    savedNavigation: getSavedNavigation()

});

function mapDispatchToProps(dispatch) {
    return {
        dispatch,
        saveNavigation: (payload) => dispatch(saveNavigation(payload)),
        getDetails: (id) => dispatch(getDetails(id)),
    };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
);

const withReducer = injectReducer({key: 'addOrEditRoleNav', reducer});
const withSaga = injectSaga({key: 'addOrEditRoleNav', saga});

export default compose(
    withReducer,
    withSaga,
    withConnect,
)(AddOrEditRoleNav);
