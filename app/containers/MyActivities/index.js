/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from 'react-helmet';
import { createStructuredSelector } from "reselect";
import { compose } from "redux";

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import { getMyActivitiesSuccess, getMyActivitiesFailure, getIsFetching } from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import { getMyActivities } from './actions';
import InfiniteScroll from "react-infinite-scroll-component";
import Loader from "../../components/Loader";
import NotificationModal from '../../components/NotificationModal/Loadable'

/* eslint-disable react/prefer-stateless-function */
const monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
export class MyActivities extends React.Component {

    state = {
        activityList: [],
        isFetching: true,
        duration: 1,
        message2: "",
        isOpen: false,
        modaltype: "",
        timeSlots: [
            {
                startTime: "12 AM",
                endTime: "06 AM",
                start: 0,
                end: 6,
            },
            {
                startTime: "06 AM",
                endTime: "12 PM",
                start: 6,
                end: 12,
            },
            {
                startTime: "12 PM",
                endTime: "06 PM",
                start: 12,
                end: 18,
            },
            {
                startTime: "06 PM",
                endTime: "12 PM",
                start: 18,
                end: 24,
            }
        ]
    }

    componentWillMount() {
        this.props.getMyActivities({ max: 100, offset: 0, order: 'ASC', sortBy: 'dateCreated', duration: 1 });

    }
    componentDidMount() {
        let element = $('#activityBox')[0]
        this.props.getActivityBox(element)
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.getMyActivitiesSuccess && nextProps.getMyActivitiesSuccess !== this.props.getMyActivitiesSuccess) {
            let dates = []
            for (let i = 0; i < this.state.duration; i++) {
                let obj,
                    date = new Date(),
                    nextDate = date;

                date.setDate(date.getDate() - i);
                date.setHours(0, 0, 0, 0);
                date = date.getTime();
                nextDate.setHours(23, 59, 59, 999)
                nextDate = nextDate.getTime();
                obj = {
                    dateCreated: date,
                    subIncludes: []
                }
                obj.subIncludes = nextProps.getMyActivitiesSuccess.object ? nextProps.getMyActivitiesSuccess.object.filter(val => new Date(val.dateCreated) >= new Date(obj.dateCreated) && new Date(val.dateCreated) < new Date(nextDate)) : [];
                dates.push(obj)
            }
            if (this.state.duration === 1 && (dates[0].subIncludes && dates[0].subIncludes.length === 0)) {
                dates = [];
            }

            this.setState({
                activityList: dates,
                totalCount: nextProps.getMyActivitiesSuccess.count,
                isFetching: false
            })

        }

        if (nextProps.getMyActivitiesFailure && nextProps.getMyActivitiesFailure !== this.props.getMyActivitiesFailure) {
            this.setState({
                isFetching: false,
                modaltype: "error",
                isOpen: true,
                message2: nextProps.getMyActivitiesFailure
            })
        }
    }

    fetchActivities = (duration) => {
        let payload = {
            max: 100,
            offset: this.state.count,
            order: 'ASC',
            sortBy: 'dateCreated',
            duration: duration
        };
        this.setState({
            isFetching: true,
            duration
        })
        this.props.getMyActivities(payload);
    }

    onCloseHandler = (index) => {
        if (index !== undefined) {
            let message2 = [...this.state.message2]
            message2.splice(index, 1);
            this.setState({
                message2
            })
        } else {
            this.setState({
                isOpen: false,
                message2: '',
                modalType: "",
            })
        }
    }
    filterSubs = (arr, start, end) => {
        let newArr = arr.filter(el => {
            let hours = new Date(el.dateCreated).getHours();
            return hours > start && hours < end
        })
        return newArr
    }

    render() {
        return (
            <React.Fragment>
                <div className="activityBox" id="activityBox">
                    <div className="activityBoxHeader">
                        <button type="button" className="btn activityCloseButton" onClick={this.props.onClose}><i className="fas fa-arrow-right"></i></button>
                        <p>My Activities</p>
                        <ul className="actionBoxTimeList">
                            <li className={this.state.duration === 1 ? "active" : ""} onClick={() => this.fetchActivities(1)}>Today</li>
                            <li className={this.state.duration === 3 ? "active" : ""} onClick={() => this.fetchActivities(3)}>Last 3 Days</li>
                            <li className={this.state.duration === 7 ? "active" : ""} onClick={() => this.fetchActivities(7)}>Last 7 days</li>
                        </ul>
                    </div>

                    <div className="activityBoxBody">
                        {this.state.isFetching ?
                            <div className="formLoaderBox">
                                <div className="loader"></div>
                            </div> : <React.Fragment>
                                {this.state.activityList.length ? this.state.activityList.map((activity, index) =>
                                    <React.Fragment key={index}>
                                        <h6 className="collapsed" data-toggle="collapse" data-target={"#activityBox" + index}>
                                            {new Date(activity.dateCreated).toDateString() === new Date().toDateString() ? "Today" : new Date(activity.dateCreated).toLocaleDateString()}
                                            <i className="fas fa-chevron-down"></i>
                                        </h6>
                                        <div className="collapse show" id={"activityBox" + index}>
                                            <div className="activityInnerBox">
                                                <ul className="activitySlotList listStyleNone">
                                                    {this.state.timeSlots.map((slot, index) => {
                                                        return (
                                                            <li id={index} key={index}>
                                                                <h4>{slot.startTime}<span> - </span>{slot.endTime}</h4>
                                                                <ul className="activitySlotSubList listStyleNone">
                                                                    {this.filterSubs(activity.subIncludes, slot.start, slot.end).length ? this.filterSubs(activity.subIncludes, slot.start, slot.end).map((task, index) =>
                                                                        <li key={index}>
                                                                            <span>{new Date(task.dateCreated).toLocaleTimeString('en-GB')}</span>
                                                                            <p>{task.operation} ( {task.url} )</p>
                                                                        </li>) :
                                                                        <li><p>No activity performed.</p></li>
                                                                    }
                                                                </ul>
                                                            </li>
                                                        )
                                                    }
                                                    )}
                                                </ul>
                                            </div>
                                        </div>
                                    </React.Fragment>
                                ) :
                                    <div className="contentAddBox h-100">
                                        <div className="contentAddBoxDetail">
                                            <div className="contentAddBoxImage">
                                            </div>
                                            <h6 className="bg-transparent">There is no activity to display.</h6>
                                        </div>
                                    </div>
                                }
                            </React.Fragment>}
                    </div>
                </div>
                {this.state.isOpen &&
                    <NotificationModal
                        type={this.state.modaltype}
                        message2={this.state.message2}
                        onCloseHandler={this.onCloseHandler}
                    />
                }
            </React.Fragment>
        );
    }
}

MyActivities.propTypes = {
    dispatch: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
    getMyActivitiesSuccess: getMyActivitiesSuccess(),
    getMyActivitiesFailure: getMyActivitiesFailure(),
});

function mapDispatchToProps(dispatch) {
    return {
        dispatch,
        getMyActivities: (payload) => dispatch(getMyActivities(payload)),
        isFetching: getIsFetching(),
    };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "myActivities", reducer });
const withSaga = injectSaga({ key: "myActivities", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(MyActivities);
