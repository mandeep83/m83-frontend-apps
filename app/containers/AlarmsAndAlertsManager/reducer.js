/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { fromJS } from "immutable";
import * as CONSTANTS from "./constants";

export const initialState = fromJS({});

function demoAlarmsAndAlertsManagerReducer(state = initialState, action) {
  switch (action.type) {
    case CONSTANTS.RESET_TO_INITIAL_STATE:
      return initialState;
    case CONSTANTS.GET_INSTANCES_SUCCESS:
      return Object.assign({}, state, {
        instances: action.response
      });
    case CONSTANTS.GET_INSTANCES_FAILURE:
      return Object.assign({}, state, {
        instancesError: action.error
      });
    case CONSTANTS.GOT_ANOMALY_DEFINITION_LIST:
      return Object.assign({}, state, {
        anomalyDefinitionList: action.response
      });
    case CONSTANTS.NOT_GOT_ANOMALY_DEFINITION_LIST:
      return Object.assign({}, state, {
        anomalyDefinitionListError: action.error
      });
    case CONSTANTS.ON_SUBMIT_REQUEST:
      return Object.assign({}, state, {
        submitSuccess: undefined,
        submitFailure:undefined
      });
    case CONSTANTS.SUBMIT_SUCCESS:
      return Object.assign({}, state, {
        submitSuccess: action.response
      });
    case CONSTANTS.SUBMIT_FAILURE:
      return Object.assign({}, state, {
        submitFailure: action.error
      });
    case CONSTANTS.GOT_DEVICES:
      return Object.assign({}, state, {
        deviceTypes: action.response
      });
    case CONSTANTS.NOT_GOT_DEVICES:
      return Object.assign({}, state, {
        deviceTypesError: action.error
      });
    case CONSTANTS.DELETE_ROW_SUCCESS:
        return Object.assign({}, state, {
          deleteSuccess: action.response
        });

    case CONSTANTS.DELETE_ROW_FAILURE:
        return Object.assign({}, state, {
          deleteError: action.error
        });
    case CONSTANTS.GET_ALL_ALARMS_SUCCESS:
        return Object.assign({}, state, {
          getAllAlarmsSuccess: action.response
        });
    case CONSTANTS.GET_ALL_ALARMS_FAILURE:
        return Object.assign({}, state, {
          getAllAlarmsFailure: action.error
        });
    case CONSTANTS.GET_ALL_TEMPLATES_SUCCESS:
        return Object.assign({}, state, {
          getAllTemplatesSuccess: action.response
        });

    case CONSTANTS.GET_ALL_TEMPLATES_FAILURE:
        return Object.assign({}, state, {
          getAllTemplatesFailure: action.error
        });
    case CONSTANTS.UPDATE_ALARM_STATE_SUCCESS:
        return Object.assign({}, state, {
          updateAlarmStateSuccess: action
        });

    case CONSTANTS.UPDATE_ALARM_STATE_FAILURE:
        return Object.assign({}, state, {
          updateAlarmStateFailure: action.error
        });
    default:
      return state;
  }
}

export default demoAlarmsAndAlertsManagerReducer;