/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

export const DEFAULT_ACTION = "app/DemoAlarmsAndAlertsManager/DEFAULT_ACTION";

export const RESET_TO_INITIAL_STATE = "app/DemoAlarmsAndAlertsManager/RESET_TO_INITIAL_STATE";

export const GET_INSTANCES_REQUEST = "app/DemoAlarmsAndAlertsManager/GET_INSTANCES_REQUEST";
export const GET_INSTANCES_SUCCESS = "app/DemoAlarmsAndAlertsManager/GET_INSTANCES_SUCCESS";
export const GET_INSTANCES_FAILURE = "app/DemoAlarmsAndAlertsManager/GET_INSTANCES_FAILURE";

export const ON_SUBMIT_REQUEST = "app/DemoAlarmsAndAlertsManager/ON_SUBMIT_REQUEST";
export const SUBMIT_SUCCESS = "app/DemoAlarmsAndAlertsManager/SUBMIT_SUCCESS";
export const SUBMIT_FAILURE = "app/DemoAlarmsAndAlertsManager/SUBMIT_FAILURE";

export const DELETE_ROW = "app/DemoAlarmsAndAlertsManager/DELETE_ROW";
export const DELETE_ROW_SUCCESS = "app/DemoAlarmsAndAlertsManager/DELETE_ROW_SUCCESS";
export const DELETE_ROW_FAILURE = "app/DemoAlarmsAndAlertsManager/DELETE_ROW_FAILURE";


export const GET_DEVICES = 'app/DemoAlarmsAndAlertsManager/GET_DEVICES_LIST';
export const GOT_DEVICES = 'app/DemoAlarmsAndAlertsManager/GOT_DEVICES_LIST';
export const NOT_GOT_DEVICES = 'app/DemoAlarmsAndAlertsManager/NOT_GOT_DEVICES_LIST';  

export const GET_ANOMALY_DEFINITION_LIST = 'app/DemoAlarmsAndAlertsManager/GET_ANOMALY_DEFINITION_LIST';
export const GOT_ANOMALY_DEFINITION_LIST = 'app/DemoAlarmsAndAlertsManager/GOT_ANOMALY_DEFINITION_LIST';
export const NOT_GOT_ANOMALY_DEFINITION_LIST = 'app/DemoAlarmsAndAlertsManager/NOT_GOT_ANOMALY_DEFINITION_LIST';

export const GET_ALL_ALARMS = 'app/DemoAlarmsAndAlertsManager/GET_ALL_ALARMS';
export const GET_ALL_ALARMS_FAILURE = 'app/DemoAlarmsAndAlertsManager/GET_ALL_ALARMS_FAILURE';
export const GET_ALL_ALARMS_SUCCESS = 'app/DemoAlarmsAndAlertsManager/GET_ALL_ALARMS_SUCCESS';

export const GET_ALL_TEMPLATES = 'app/DemoAlarmsAndAlertsManager/GET_ALL_TEMPLATES';
export const GET_ALL_TEMPLATES_FAILURE = 'app/DemoAlarmsAndAlertsManager/GET_ALL_TEMPLATES_FAILURE';
export const GET_ALL_TEMPLATES_SUCCESS = 'app/DemoAlarmsAndAlertsManager/GET_ALL_TEMPLATES_SUCCESS';

export const UPDATE_ALARM_STATE = 'app/DemoAlarmsAndAlertsManager/UPDATE_ALARM_STATE';
export const UPDATE_ALARM_STATE_FAILURE = 'app/DemoAlarmsAndAlertsManager/UPDATE_ALARM_STATE_FAILURE';
export const UPDATE_ALARM_STATE_SUCCESS = 'app/DemoAlarmsAndAlertsManager/UPDATE_ALARM_STATE_SUCCESS';