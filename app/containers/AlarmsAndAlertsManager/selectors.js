/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { createSelector } from "reselect";
import { initialState } from "./reducer";

const selectDemoAlarmsAndAlertsManagerDomain = state => state.get("demoAlarmsAndAlertsManager", initialState);

export const getInstancesSuccess = () =>createSelector(selectDemoAlarmsAndAlertsManagerDomain,substate => substate.instances);
export const getInstancesError = () => createSelector(selectDemoAlarmsAndAlertsManagerDomain,substate => substate.instancesError);

export const getAllAlarmsSuccess = () =>createSelector(selectDemoAlarmsAndAlertsManagerDomain, subState => subState.getAllAlarmsSuccess);
export const getAllAlarmsFailure = () => createSelector(selectDemoAlarmsAndAlertsManagerDomain, subState => subState.getAllAlarmsFailure);

export const getAllTemplatesSuccess = () =>createSelector(selectDemoAlarmsAndAlertsManagerDomain, subState => subState.getAllTemplatesSuccess);
export const getAllTemplatesFailure = () => createSelector(selectDemoAlarmsAndAlertsManagerDomain, subState => subState.getAllTemplatesFailure);

export const updateAlarmStateSuccess = () =>createSelector(selectDemoAlarmsAndAlertsManagerDomain, subState => subState.updateAlarmStateSuccess);
export const updateAlarmStateFailure = () => createSelector(selectDemoAlarmsAndAlertsManagerDomain, subState => subState.updateAlarmStateFailure);
