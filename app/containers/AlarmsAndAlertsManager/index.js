/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { createStructuredSelector } from "reselect";
import ConfirmModel from '../../components/ConfirmModel';
import { compose } from "redux";
import ReactSelect from "react-select";

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import * as ACTIONS from './actions'
import NotificationModal from '../../components/NotificationModal/Loadable'
import * as SELECTORS from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import Loader from '../../components/Loader/Loadable'
import { getTimeDifference, toTitleCase } from '../../commonUtils';
import cloneDeep from 'lodash/cloneDeep';
import InfiniteScroll from "react-infinite-scroll-component";

/* eslint-disable react/prefer-stateless-function */
export class DemoAlarmsAndAlertsManager extends React.Component {
	state = {
		arr: [], //TEMP CHANGE
		view: 'list',
		anomalyList: [],
		filteredList: [],
		deleteItemId: '',
		confirmState: false,
		deviceTypes: [],
		deviceType: "",
		instances: [],
		anamolyDefinitionList: [],
		showAlarmList: true,
		payload: {
			deviceType: "",
			instances: [],
			entity: "",
			title: "",
			minAlert: 0,
			maxAlert: 0,
			minAlarm: 0,
			maxAlarm: 0,
			alarmSetPoint: "",
			isBoolean: "false",
			maxRange: 0,
			minRange: 0,
		},
		errors: {
			instances: '',
		},
		isFetching: true,
		alarmList: [],
		alarmTemplates: [],
		filters: {
			alarmCategory: "",
			currentStatus: "", //RAISED|ACKNOWLEDGED|ADDRESSED
			sortingOrder: 1         // DESC --> -1 or ASC--> 1
		}
	}

	componentDidMount() {
		// this.props.fetchDeviceTypes(true);
		this.props.getAllAlarms({});
		this.props.getAllTemplates();
	}

	componentWillReceiveProps(nextProps) {

		if (nextProps.deleteSuccess) {
			let anamolyDefinitionList = JSON.parse(JSON.stringify(this.state.anamolyDefinitionList)).filter(anomaly => anomaly.id != nextProps.deleteSuccess)
			this.setState({
				modalType: 'success',
				openModal: true,
				message2: 'Anomaly Definition Deleted Successfully',
				isFetching: false,
				anamolyDefinitionList
			});
		}

		if (nextProps.deleteError) {
			this.setState({
				modalType: 'error',
				openModal: true,
				message2: nextProps.deleteError,
				isFetching: false,
			});
		}


		if (nextProps.deviceTypes) {
			if (nextProps.deviceTypes.length > 0) {
				let payload = JSON.parse(JSON.stringify(this.state.payload))
				let deviceType = nextProps.deviceTypes[0].type
				payload.deviceType = nextProps.deviceTypes[0].type

				this.setState({
					deviceType,
					payload,
				})
				this.props.getinstances(nextProps.deviceTypes[0].type)
				this.props.getAnomalyDefinitionList(nextProps.deviceTypes[0].type)
			}
			this.setState({
				deviceTypes: nextProps.deviceTypes
			})
		}

		if (nextProps.deviceTypesError) {
			this.setState({
				modaltype: 'error',
				openModal: true,
				message2: nextProps.deviceTypesError
			})
		}

		if (nextProps.instances) {
			this.setState({
				instances: nextProps.instances,
			})
		}

		if (nextProps.instancesError) {
			this.setState({
				modaltype: "error",
				openModal: true,
				message2: nextProps.instancesError
			})
		}

		if (nextProps.anamolyDefinitionListSuccess) {
			this.setState({
				anamolyDefinitionList: nextProps.anamolyDefinitionListSuccess,
			})
		}

		if (nextProps.anamolyDefinitionListError) {
			this.setState({
				modaltype: "error",
				openModal: true,
				message2: nextProps.anamolyDefinitionListError
			})
		}

		if (nextProps.submitSuccess) {
			this.props.getAnomalyDefinitionList(this.state.payload.deviceType);
			this.setState({
				isEnableForListView: true,
				modaltype: "success",
				openModal: true,
				message2: "Request complete successfully."
			})
		}

		if (nextProps.submitError) {
			this.props.getAnomalyDefinitionList(this.state.payload.deviceType);
			this.setState({
				modaltype: "error",
				openModal: true,
				message2: nextProps.submitError
			})
		}

		if (nextProps.getAllAlarmsSuccess) {
			this.setState({
				alarmList: nextProps.getAllAlarmsSuccess,
				isFetching: false,
			})
		}
		if (nextProps.getAllAlarmsFailure) {
			this.setState({
				isFetching: false,
				isOpen: true,
				modaltype: "error",
				openModal: true,
				message2: nextProps.getAllAlarmsFailure
			})
		}

		if (nextProps.getAllTemplatesSuccess) {
			let alarmTemplates = nextProps.getAllTemplatesSuccess.filter(template => template.type === "ALARM").map(alarm => alarm.metaInfo)
			this.setState({
				alarmTemplates,
				isFetching: false,
			})
		}
		if (nextProps.getAllTemplatesFailure) {
			this.setState({
				isFetching: false,
				isOpen: true,
				modaltype: "error",
				openModal: true,
				message2: nextProps.getAllTemplatesFailure
			})
		}
		if (nextProps.updateAlarmStateSuccess) {
			let alarmList = cloneDeep(this.state.alarmList),
				alarmIndex = alarmList.findIndex(alarm => alarm.alarmId === nextProps.updateAlarmStateSuccess.id);
			alarmList[alarmIndex].currentStatus = nextProps.updateAlarmStateSuccess.newStatus;
			this.setState({
				alarmList,
				isChangingState: null,
			})
		}
		if (nextProps.updateAlarmStateFailure) {
			this.setState({
				isChangingState: null,
				isOpen: true,
				modaltype: "error",
				openModal: true,
				message2: nextProps.updateAlarmStateFailure
			})
		}
	}

	componentDidUpdate() {
		if (Object.keys(SELECTORS).some(prop => this.props[prop])) {
			this.props.resetToInitialState()
		}
	}

	onChangeHandler = (event) => {

		if (event.target.id === "deviceTypeDefinition") {
			let payload = JSON.parse(JSON.stringify(this.state.payload))
			payload.deviceType = event.target.value
			this.props.getinstances(event.target.value)
			this.setState({ payload }, () => this.props.getAnomalyDefinitionList(this.state.payload.deviceType))
			return
		}
	}

	confirmBox = (id, deleteName) => {
		this.setState({
			deleteItemId: id,
			confirmState: true,
			deleteName
		});
	};

	cancelClicked = () => {
		this.setState({
			deleteItemId: '',
			confirmState: false,
		});
	}

	payloadChangeHandler = (index) => (event) => {
		let payload = JSON.parse(JSON.stringify(this.state.payload))
		if (event.target.id === "deviceType") {
			payload = {
				instances: [],
				entity: "",
				minAlert: 0,
				maxAlert: 0,
				minAlarm: 0,
				maxAlarm: 0,
				alarmSetPoint: "",
				isBoolean: "false",
				maxRange: 0,
				minRange: 0,
			}, this.props.getinstances(event.target.value);
		}
		if (event.target.id === "entity") {
			this.state.instances.map(el => el.entities.map((arr) => {
				if (arr.path === event.target.value) {
					payload.maxRange = (arr.max).toString()
					payload.minRange = (arr.min).toString()
				}
			}))

		}
		payload[event.target.id] = event.target.value;
		this.setState({
			payload
		})
	}

	validateForReactSelect = () => {
		let errors = JSON.parse(JSON.stringify(this.state.errors));
		if (this.state.payload.instances.length < 1) {
			errors.instances = 'Please Select Instances !';
		} else {
			errors.instances = '';
		}

		return errors;
	}

	onSubmitHandler = (event) => {
		event.preventDefault();

		if (this.state.payload.isBoolean === "true") {
			delete this.state.payload.maxAlarm
			delete this.state.payload.maxAlert
			delete this.state.payload.minAlarm
			delete this.state.payload.minAlert
		}
		else {
			delete this.state.payload.alarmSetPoint;
			delete this.state.payload.entityDisplayName
		}
		let payload = JSON.parse(JSON.stringify(this.state.payload))
		payload.instances = payload.instances.length != 0 ? payload.instances.map(instance => instance.label) : []
		if (payload.instances[0] === "ALL") {
			payload.instances = this.state.instances[0].instancesData.map(el => { return (el.id) })
		}
		let errors = this.validateForReactSelect();

		if (errors.instances) {
			this.setState({ errors });
		} else {
			this.props.onSubmitHandler(payload, this.state.view);
		}

	}


	addNewDefinitionHandler = (type, id = null) => {
		let payload = JSON.parse(JSON.stringify(this.state.payload))
		if (type == "add") {
			payload = {
				deviceType: this.state.deviceType,
				instances: [],
				entity: '',
				title: "",
				minAlert: 0,
				maxAlert: 0,
				minAlarm: 0,
				maxAlarm: 0,
				minRange: 0,
				maxRange: 0,
				alarmSetPoint: "",
				isBoolean: "false"
			}
		} else {
			let obj = JSON.parse(JSON.stringify(this.state.anamolyDefinitionList)).find(anomaly => anomaly.id == id)
			obj.deviceType = payload.deviceType
			payload = obj
			payload.instances = payload.instances.map(el => {
				return {
					value: el,
					label: el
				}
			})

		}
		this.setState({ isAddNewTicket: true, payload, view: type })
	}

	createOptionsForReactSelectInstances = () => {
		let options = [];
		if (this.state.instances.length > 0 && this.state.instances[0].instancesData.length > 0) {
			options = this.state.instances[0].instancesData.map(arr => {
				return {
					value: arr.id,
					label: arr.id
				}
			})
			this.state.instances.length ? options.unshift({
				value: "ALL",
				label: "ALL"
			}) : ''
			if (this.state.payload.instances && this.state.payload.instances.some(val => val.value === "ALL")) {
				options = [];
			}
		}
		return options;
	}

	handleChangeForReactSelect = () => value => {
		let payload = JSON.parse(JSON.stringify(this.state.payload))
		if (value.some(val => val.value === "ALL")) {
			payload.instances = [{
				value: "ALL",
				label: "ALL"
			}]
		}
		else {
			payload.instances = value;
		}
		if (this.state.errors.instances && this.state.payload.instances) {
			let errors = JSON.parse(JSON.stringify(this.state.errors))
			errors.instances = ''
			this.setState({
				errors
			})
		}
		this.setState({ payload })
	}

	onCloseHandler = () => {
		this.setState({
			isOpen: false,
			modalType: "",
			message2: "",
		})
	}
	filterChangeHandler = ({ target }) => {
		let filters = cloneDeep(this.state.filters);
		filters[target.id] = target.value;
		Object.entries(filters).map(([key, value]) => {
			if (!value)
				delete filters[key]
		})
		this.setState({
			isFetching: true,
			filters
		}, () => this.props.getAllAlarms(filters))
	}

	viewHistoryModal = (alarmHistory) => {
		this.setState({
			alarmHistory,
			historyModal: true,
		})
	}

	getHigherStatus = (currentStatus, isForKey) => {
		if (currentStatus === "RAISED") {
			return isForKey ? "ACKNOWLEDGED" : "Acknowledge"
		}
		if (currentStatus === "ACKNOWLEDGED") {
			return isForKey ? "ADDRESSED" : "Address"
		}
		return "Addressed"
	}

	changeAlarmStatus = (currentStatus, alarmId) => {
		if (currentStatus !== "ADDRESSED") {
			let newStatus = this.getHigherStatus(currentStatus, "key"),
				payload = { state: newStatus }
			if (newStatus === "ACKNOWLEDGED")
				payload.ackDuration = 10;
			this.setState({
				isChangingState: alarmId
			}, () => this.props.updateAlarmState(payload, alarmId))
		}
	}
	getClassName = (alarmStatus) => {
		if (alarmStatus === "RAISED")
			return "btn btn-transparent btn-transparent-danger"
		if (alarmStatus === "ACKNOWLEDGED")
			return "btn btn-transparent btn-transparent-warning"
		return "btn btn-transparent btn-transparent-success"

	}

	render() {
		return (
			<React.Fragment>
				<Helmet>
					<title>Alarms</title>
					<meta name="description" content="M83-ManageAlarmsAndAlerts" />
				</Helmet>

				<div className="pageBreadcrumb">
					<div className="row">
						<div className="col-8">
							<p> <span onClick={() => { this.setState({ view: "list" }) }}>Alarms</span>
								{this.state.view !== "list" && <span className="active">{this.state.view === "add" ? "Create Definition" : "Edit Definition"}</span>}
							</p>
						</div>
					</div>
				</div>
				<div className="outerBox alarmBox">
					<div className="flex filterBox">
						<div className="fx-b25 pd-r-10">
							<div className="form-group">
								<select className="form-control" id="alarmCategory" disabled={this.state.isFetching} value={this.state.filters.alarmCategory} onChange={this.filterChangeHandler}>
									<option value="">All</option>
									{this.state.alarmTemplates.map(template =>
										<option value={template.alarmCategory} key={template.alarmCategory}>{template.alarmCategory}</option>
									)}
								</select>
								<label className="form-group-label">Type :</label>
							</div>
						</div>
						<div className="fx-b25 pd-r-10 pd-l-10">
							<div className="form-group">
								<select className="form-control" id="currentStatus" disabled={this.state.isFetching} value={this.state.filters.currentStatus} onChange={this.filterChangeHandler}>
									<option value="">All</option>
									<option value="RAISED">Raised</option>
									<option value="ACKNOWLEDGED">Acknowledged</option>
									<option value="ADDRESSED">Addressed</option>
								</select>
								<label className="form-group-label">Status :</label>
							</div>
						</div>
						<div className="fx-b25 pd-r-10 pd-l-10">
							<div className="form-group">
								<select className="form-control" disabled={this.state.isFetching} id="connectorType" >
									<option>Generated At</option>
								</select>
								<label className="form-group-label">Sort By :</label>
							</div>
						</div>
						<div className="fx-b25">
							<div className="form-group filterOrder">
								<select className="form-control" id="sortingOrder" disabled={this.state.isFetching} value={this.state.filters.sortingOrder} onChange={this.filterChangeHandler}>
									<option value="1">Ascending</option>
									<option value="-1">Descending</option>
								</select>
								<label className="form-group-label">Order :</label>
							</div>
						</div>
					</div>
					{this.state.isFetching ? <Loader /> :
						this.state.alarmList.length ? <ul className="alarmsAndAlertsList">
							{this.state.alarmList.map(alarm => (
								<li key={alarm.alarmId}>
									<span className="alertIcon " style={{ backgroundColor: alarm.color }}>
										<i className="far fa-bell"></i>
										<span className="badge badge-secondary">{alarm.alarmHistory.filter(event => event.raised).length}</span>
									</span>
									<div className="flex">
										<p><strong>Type :</strong>{alarm.alarmCategory}</p>
										<p><strong>Last Renewed :</strong>{getTimeDifference(alarm.lastRenewedAt)}</p>
										<p><strong>Generated :</strong>{getTimeDifference(alarm.timestamp)}</p>
										{/*<span className="mx-2">|</span>
													<p><strong>Priority :</strong><span className="badge-pill badge-success">Low</span></p>*/}
									</div>
									<p className="alarmDescription"><strong>Description :</strong>{alarm.description}</p>
									<div className="btnAlertGroup">
										{this.state.isChangingState === alarm.alarmId ?
											<div className="panelLoader alert">
												<i className="fal fa-sync-alt fa-spin"></i>
											</div> :
											<button className={this.getClassName(alarm.currentStatus)}>{toTitleCase(alarm.currentStatus)}</button>

										}
										<button className="btn btn-transparent btn-transparent-success" disabled={alarm.currentStatus === "ADDRESSED"} type="button" onClick={() => this.changeAlarmStatus(alarm.currentStatus, alarm.alarmId)}><i className="far fa-thumbs-up mr-r-5"></i>{this.getHigherStatus(alarm.currentStatus)}</button>
										<button className="btn btn-transparent btn-transparent-primary" type="button" onClick={() => this.viewHistoryModal(alarm.alarmHistory)}><i className='far fa-clock mr-r-5'></i>History</button>
									</div>
								</li>
							))}
						</ul> :
							<div className="contentAddBox alarmAddBox">
								<div className="contentAddBoxDetail">
									<div className="contentAddBoxImage">
										<img src="https://content.iot83.com/m83/other/addAlarm.png" />
									</div>
									<h6>No Alarms found.</h6>
								</div>
							</div>
					}
				</div>

				{this.state.historyModal &&
					<div className="modal d-block animated slideInDown" role="dialog" id="">
						<div className="modal-dialog modal-lg">
							<div className="modal-content">
								<div className="modal-header">
									<h6 className="modal-title">
										<span className="modal-title-info"><strong>Alarm History</strong></span>
										{/*<React.Fragment>*/}
										{/*  <span className="mr-l-10 mr-r-10">|</span>*/}
										{/*  <span className="modal-title-info"><strong>Description :</strong>test</span>*/}
										{/*</React.Fragment>*/}
										<button type="button" className="close" data-dismiss="modal" onClick={() => this.setState({ historyModal: false, alarmHistory: [] })}><i className="far fa-times"></i></button>
									</h6>
								</div>
								<div className="modal-body">
									<div className="timelineBox">
										{/*<div className="formLoaderBox">*/}
										{/*  <div className="loader"></div>*/}
										{/*</div>*/}
										<InfiniteScroll
											dataLength=""
											// next={this.nextFetchHistory}
											// hasMore={this.state.hasMoreCommitHistory}
											// loader={<h4>Loading...</h4>}
											endMessage={
												<p className="text-center">You have seen all the Alarm History.</p>
											}
											height={450}
										>
											<ul className="listStyleNone">
												{this.state.alarmHistory.map((event, index) => (
													<li key={index}>
														<span className="timelineIcon">
															<i className="fad fa-code-commit"></i>
														</span>
														<div className="timelineList">
															<div className={event.addressed ? "alert alert-success" : event.acknowledged ? "alert alert-warning" : "alert alert-danger"}>{event.addressed ? "Addressed" : event.acknowledged ? "Acknowledged" : "Raised"}</div>
															<h6><span className="text-cyan">{new Date(event.timestamp).toLocaleString('en-US', { hour12: false, timeZone: localStorage.timeZone })}</span></h6>
															<h6><span>{getTimeDifference(event.timestamp)}</span></h6>
														</div>
													</li>
												))}
											</ul>
										</InfiniteScroll>
									</div>
								</div>
							</div>
						</div>
						}
					</div>
				}

				{this.state.isOpen &&
					<NotificationModal
						type={this.state.modalType}
						message2={this.state.message2}
						onCloseHandler={this.onCloseHandler}
					/>
				}
			</React.Fragment>
		);
	}
}

DemoAlarmsAndAlertsManager.propTypes = {
	dispatch: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
	instances: SELECTORS.getInstancesSuccess(),
	instancesError: SELECTORS.getInstancesError(),
	getAllAlarmsSuccess: SELECTORS.getAllAlarmsSuccess(),
	getAllAlarmsFailure: SELECTORS.getAllAlarmsFailure(),
	getAllTemplatesSuccess: SELECTORS.getAllTemplatesSuccess(),
	getAllTemplatesFailure: SELECTORS.getAllTemplatesFailure(),
	updateAlarmStateSuccess: SELECTORS.updateAlarmStateSuccess(),
	updateAlarmStateFailure: SELECTORS.updateAlarmStateFailure(),
});

function mapDispatchToProps(dispatch) {
	return {
		dispatch,
		resetToInitialState: () => dispatch(ACTIONS.resetToInitialState()),
		fetchDeviceTypes: (isDeployed) => dispatch(ACTIONS.fetchDeviceTypes(isDeployed)),
		getinstances: (deviceType) => dispatch(ACTIONS.getinstances(deviceType)),
		onSubmitHandler: (payload, type) => dispatch(ACTIONS.onSubmitHandler(payload, type)),
		deleteRow: (id, deviceType) => dispatch(ACTIONS.deleteRow(id, deviceType)),
		getAnomalyDefinitionList: (deviceType) => dispatch(ACTIONS.getAnomalyDefinitionList(deviceType)),
		getAllAlarms: (payload) => dispatch(ACTIONS.getAllAlarms(payload)),
		getAllTemplates: () => dispatch(ACTIONS.getAllTemplates()),
		updateAlarmState: (payload, id) => dispatch(ACTIONS.updateAlarmState(payload, id)),
	};
}

const withConnect = connect(
	mapStateToProps,
	mapDispatchToProps
);

const withReducer = injectReducer({
	key: "demoAlarmsAndAlertsManager",
	reducer
});
const withSaga = injectSaga({ key: "demoAlarmsAndAlertsManager", saga });

export default compose(
	withReducer,
	withSaga,
	withConnect
)(DemoAlarmsAndAlertsManager);
