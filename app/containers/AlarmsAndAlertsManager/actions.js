/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import * as CONSTANTS from "./constants";


export function resetToInitialState() {
  return {
    type: CONSTANTS.RESET_TO_INITIAL_STATE,
  };
}

export function defaultAction() {
  return {
    type: CONSTANTS.DEFAULT_ACTION
  };
}

export function getinstances(deviceType) {
  return {
    type: CONSTANTS.GET_INSTANCES_REQUEST,
    deviceType
  };
}

export function onSubmitHandler(payload, action) {
  return {
    type: CONSTANTS.ON_SUBMIT_REQUEST,
    payload, action
  };
}

export function fetchDeviceTypes(isConfigured) {
  return {
      type: CONSTANTS.GET_DEVICES,
      isConfigured
  };
}

export function getAnomalyDefinitionList(deviceType) {
  return {
    type: CONSTANTS.GET_ANOMALY_DEFINITION_LIST,
    deviceType
  };
}

export function deleteRow(id, deviceType) {
  return {
      type: CONSTANTS.DELETE_ROW,
      id, deviceType
  };
}

export function getAllAlarms(payload) {
  return {
      type: CONSTANTS.GET_ALL_ALARMS,
      payload
  };
}

export function updateAlarmState(payload, id) {
  return {
      type: CONSTANTS.UPDATE_ALARM_STATE,
      payload,
      id
  };
}

export function getAllTemplates() {
  return {
      type: CONSTANTS.GET_ALL_TEMPLATES,
  };
}