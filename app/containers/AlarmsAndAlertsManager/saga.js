/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { takeEvery } from 'redux-saga';
import * as CONSTANTS from './constants';

import { apiCallHandler } from '../../api';

export function* getInstancesApiHandlerAsync(action) {
  yield [apiCallHandler(action,CONSTANTS. GET_INSTANCES_SUCCESS, CONSTANTS.GET_INSTANCES_FAILURE, 'getInstanceAndEntities')];
}
export function* getOnSubmitApiHandlerAsyc(action) {
  yield [apiCallHandler(action,CONSTANTS. SUBMIT_SUCCESS, CONSTANTS.SUBMIT_FAILURE, 'onSubmitAlarmsAndAlert')];
}

export function* fetchDevicesApiHandlerAsync(action) {
    yield[apiCallHandler(action,CONSTANTS. GOT_DEVICES, CONSTANTS.NOT_GOT_DEVICES, 'getDeviceType', false)];
}

export function* deleteRowApiHandlerAsync(action) {
  yield[apiCallHandler(action,CONSTANTS.DELETE_ROW_SUCCESS, CONSTANTS.DELETE_ROW_FAILURE,'deleteRow',true)];
}

export function* getAnomalyDefinitionListApiHandlerAsync(action) {
  yield[apiCallHandler(action,CONSTANTS.GOT_ANOMALY_DEFINITION_LIST, CONSTANTS.NOT_GOT_ANOMALY_DEFINITION_LIST,'getAnomalyDefinitionList')];
}

export function* getAllAlarmsApiHandlerAsync(action) {
  yield[apiCallHandler(action,CONSTANTS.GET_ALL_ALARMS_SUCCESS, CONSTANTS.GET_ALL_ALARMS_FAILURE, 'getAllAlarms')];
}

export function* getAllTemplatesApiHandlerAsync(action) {
  yield[apiCallHandler(action,CONSTANTS.GET_ALL_TEMPLATES_SUCCESS, CONSTANTS.GET_ALL_TEMPLATES_FAILURE, 'getAllTemplates')];
}

export function* updateAlarmStateApiHandlerAsync(action) {
  yield[apiCallHandler(action,CONSTANTS.UPDATE_ALARM_STATE_SUCCESS, CONSTANTS.UPDATE_ALARM_STATE_FAILURE, 'updateAlarmState')];
}

export function* watcherGetInstancesRequest() {
  yield takeEvery(CONSTANTS.GET_INSTANCES_REQUEST, getInstancesApiHandlerAsync);
}


export function* watcherOnSubmitRequest() {
  yield takeEvery(CONSTANTS.ON_SUBMIT_REQUEST, getOnSubmitApiHandlerAsyc);
}

export function* watcherFetchAllDevices() {
    yield takeEvery(CONSTANTS.GET_DEVICES, fetchDevicesApiHandlerAsync);
}

export function* watcherDeleteRow() {
  yield takeEvery(CONSTANTS.DELETE_ROW, deleteRowApiHandlerAsync);
}

export function* watcherGetAnomalyDefinitionList() {
  yield takeEvery(CONSTANTS.GET_ANOMALY_DEFINITION_LIST, getAnomalyDefinitionListApiHandlerAsync);
}

export function* watcherGetAllAlarms() {
  yield takeEvery(CONSTANTS.GET_ALL_ALARMS, getAllAlarmsApiHandlerAsync);
}

export function* watcherGetAllTemplates() {
  yield takeEvery(CONSTANTS.GET_ALL_TEMPLATES, getAllTemplatesApiHandlerAsync);
}

export function* watcherUpdateAlarmState() {
  yield takeEvery(CONSTANTS.UPDATE_ALARM_STATE, updateAlarmStateApiHandlerAsync);
}

export default function* rootSaga() {
  yield [
    watcherGetInstancesRequest(),
    watcherOnSubmitRequest(),
    watcherFetchAllDevices(),
    watcherDeleteRow(),
    watcherGetAnomalyDefinitionList(),
    watcherGetAllAlarms(),
    watcherGetAllTemplates(),
    watcherUpdateAlarmState(),
  ];
}
