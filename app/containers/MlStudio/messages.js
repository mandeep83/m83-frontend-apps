/*
 * MlStudio Messages
 *
 * This contains all the text for the MlStudio component.
 */

import { defineMessages } from "react-intl";

export default defineMessages({
  header: {
    id: "app.containers.MlStudio.header",
    defaultMessage: "This is MlStudio container !"
  }
});
