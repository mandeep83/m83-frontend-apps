import { createSelector } from "reselect";
import { initialState } from "./reducer";
import * as CONSTANTS from "./constants"
/**
 * Direct selector to the mlStudio state domain
 */

const selectMlStudioDomain = state => state.get("mlStudio", initialState);

let allCases = Object.values(CONSTANTS).filter(constant => typeof (constant) == "object")
export const allSelectors = {}
allCases.map(constant => {
	allSelectors[constant.successKey] = () => createSelector(selectMlStudioDomain, subState => subState[constant.successKey]);
	allSelectors[constant.failureKey] = () => createSelector(selectMlStudioDomain, subState => subState[constant.failureKey]);
})



