/**
 *
 * MlStudio
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { FormattedMessage } from "react-intl";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import reducer from "./reducer";
import saga from "./saga";
import { allActions as ACTIONS } from './actions';
import { allSelectors as SELECTORS } from "./selectors";
import NotificationModal from '../../components/NotificationModal/Loadable'
import Loader from "../../components/Loader/index";
import ConfirmModel from "../../components/ConfirmModel";
import ReactTooltip from "react-tooltip";
import MQTT from 'mqtt';
import SlidingPane from "react-sliding-pane";
import cloneDeep from "lodash/cloneDeep";
import ReactJson from "react-json-view";
import TagsInput from 'react-tagsinput';
import InfiniteScroll from "react-infinite-scroll-component";
import { getTimeDifference, getInitials } from '../../commonUtils';
import ListingTable from "../../components/ListingTable/Loadable";
import QuotaHeader from "../../components/QuotaHeader/Loadable";
import AddNewButton from "../../components/AddNewButton/Loadable"
import ImageUploader from "../../components/ImageUploader";
import produce from "immer"
import ReactSelect from 'react-select';
import { getMqttConnection, closeMqttConnection, subscribeTopic, unsubscribeTopic } from "../../mqttConnection";



const IMPORT_EXPERIMENT_PAYLOAD = {
    name: '',
    description: '',
    endpointname: '',
    s3Path: '',
    isDraft: false,
    fileType: '',
    customModel: true,
    customModelType: '',
    algorithm: {
        algorithmType: ''
    },
    model: "customModel", //autoML , platformML, customModel
},
    AUTOML_EXPERIMENT_PAYLOAD = {
        name: '',
        description: '',
        dataSource: {
            connectorCategory: "S3"
        },
        algorithm: {
            id: "",
            algorithmType: "sparkmljob", //for automl fixed
            trainingJobName: "",
            hyperparams: {
                target: "",
                trainingJobType: "regression", //drop down can be either classification or regression
                algorithmName: "linearRegression"
            }
        },
        model: "autoML", // autoML , platformML,customModel
        isDraft: false
    },
    optionsForTrainingJobType = [{ value: "regression", label: "Regression" }, { value: "classification", label: "Classification" }],
    optionsForRegressionAlgorithm = [{ value: "linearRegression", label: "Linear Regression" }, { value: "decisionTreeRegressor", label: "Decision Tree Regressor" }, { value: "gradientBoostedRegressor", label: "Gradient Boosted Regressor" }],
    optionsForClassificationAlgorithm = [{ value: "decisionTree", label: "Decision Tree" }, { value: "logisticRegression", label: "Logistic Regression" }, { value: "gradientBoostedClassifier", label: "Gradient Boosted Classifier" }, { value: "randomForestClassifier", label: "Random Forest Classifier" }],
    mlTopics = [`MLStudio/${window.API_URL.split("//")[1].split(".")[0]}/inference`, `MLStudio/${window.API_URL.split("//")[1].split(".")[0]}/states`],
    EXPERIMENT_STATES = {
        "stop": { name: "Stop", className: "text-red" },
        "transformInProgress": { name: "Transform in Progress", className: "text-gray" },
        "transformSuccess": { name: "Transform Success", className: "text-cyan" },
        "trainInProgress": { name: "Train in Progress", className: "text-gray" },
        "trainSuccess": { name: "Train Success", className: "text-green" }
    },
    MODEL_TYPES = {
        "autoML": { name: "Auto ML" },
        "platformML": { name: "Auto ML" },
        "customModel": { name: "Custom Model" },
    }
/* eslint-disable react/prefer-stateless-function */
export class MlStudio extends React.Component {
    state = {
        mlExperimentList: [],
        isOpen: false,
        confirmState: false,
        isFetching: true,
        openCustomForm: false,
        openAutoMLForm: false,
        trainPayload: {
            experimentId: '',
            action: {
                type: ''
            }
        },
        searchTerm: '',
        predictExperiment: {},
        selectedExperiment: {},
        predictResponse: {},
        predictionPayload: [],
        modalPayload: { ...AUTOML_EXPERIMENT_PAYLOAD },
        experimentUploadLoader: false,
        isAddOrEditExperiment: false,
        listOfSourceConnectors: {
            "S3": [{ properties: { list: [] } }]
        },
        listOfColumns: []
    }

    qoutaCallback = () => {
        this.props.getAllMlExperiments();
        // this.props.getExperimentHistory();
        this.props.getConnectorsByCategory("S3");
        subscribeTopic(mlTopics, this.mqttMessageHandler);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.getConnectorsByCategorySuccess && nextProps.getConnectorsByCategorySuccess !== this.props.getConnectorsByCategorySuccess) {
            let listOfSourceConnectors = { ...this.state.listOfSourceConnectors },
                { response, category } = nextProps.getConnectorsByCategorySuccess;
            listOfSourceConnectors[category] = response.filter(el => el.source);
            this.setState({
                listOfSourceConnectors
            })
        }

        if (nextProps.getColumnsByConnectorSuccess && nextProps.getColumnsByConnectorSuccess !== this.props.getColumnsByConnectorSuccess) {
            let { response } = nextProps.getColumnsByConnectorSuccess;
            this.setState({
                isFetchingColumns: false,
                listOfColumns: response.map(column => {
                    return {
                        value: column.keys,
                        keys: column.keys,
                        type: column.type,
                        label: column.keys
                    }
                })
            })
        }

        if (nextProps.getColumnsByConnectorFailure && nextProps.getColumnsByConnectorFailure !== this.props.getColumnsByConnectorFailure) {
            this.setState({
                isFetchingColumns: false,
            })
        }

        if (nextProps.getAllMlExperimentsSuccess && nextProps.getAllMlExperimentsSuccess !== this.props.getAllMlExperimentsSuccess) {
            let { response } = nextProps.getAllMlExperimentsSuccess;
            this.setState({
                mlExperimentList: response,
                isFetching: false
            })
        }

        if (nextProps.getAllMlExperimentsFailure && nextProps.getAllMlExperimentsFailure !== this.props.getAllMlExperimentsFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.getAllMlExperimentsFailure.error,
                isFetching: false
            })
        }

        if (nextProps.deleteMlExperimentSuccess && nextProps.deleteMlExperimentSuccess !== this.props.deleteMlExperimentSuccess) {
            this.setState({
                modalType: "success",
                isOpen: true,
                message2: nextProps.deleteMlExperimentSuccess.response,
            }, () => this.props.getAllMlExperiments())
        }

        if (nextProps.deleteMlExperimentFailure && nextProps.deleteMlExperimentFailure !== this.props.deleteMlExperimentFailure) {
            this.setState({
                isOpen: true,
                modalType: "error",
                message2: nextProps.deleteMlExperimentFailure.error,
                isFetching: false
            })
        }

        if (nextProps.getExperimentHistorySuccess && nextProps.getExperimentHistorySuccess !== this.props.getExperimentHistorySuccess) {
            this.setState({
                allExperimentHistory: nextProps.getExperimentHistorySuccess.response
            })
        }

        if (nextProps.getExperimentHistoryFailure && nextProps.getExperimentHistoryFailure !== this.props.getExperimentHistoryFailure) {
            this.setState({
                isOpen: true,
                modalType: "error",
                message2: nextProps.getExperimentHistoryFailure.error,
                isFetching: false
            })
        }

        if (nextProps.uploadFileSuccess && this.props.uploadFileSuccess !== nextProps.uploadFileSuccess) {
            let modalPayload = JSON.parse(JSON.stringify(this.state.modalPayload));
            modalPayload.s3Path = nextProps.uploadFileSuccess.secureUrl
            modalPayload.fileType = "tar"
            this.setState({
                modalPayload,
                experimentUploadLoader: false
            })
        }

        if (nextProps.uploadFileFailure && nextProps.uploadFileFailure !== this.props.uploadFileFailure) {
            this.setState({
                isOpen: true,
                modalType: "error",
                message2: nextProps.uploadFileFailure,
                experimentUploadLoader: false
            })
        }

        if (nextProps.createExperimentSuccess && nextProps.createExperimentSuccess !== this.props.createExperimentSuccess) {
            this.setState({
                isModalLoader: false,
                openCustomForm: false,
                openAutoMLForm: false,
                isFetching: true
            }, () => this.props.getAllMlExperiments())
        }

        if (nextProps.createExperimentFailure && nextProps.createExperimentFailure !== this.props.createExperimentFailure) {
            this.setState({
                isOpen: true,
                modalType: "error",
                message2: nextProps.createExperimentFailure.error,
                isFetching: false,
                isModalLoader: false
            })
        }

        if (nextProps.trainMlExperimentSuccess && this.props.trainMlExperimentSuccess != nextProps.trainMlExperimentSuccess) {
            let mlExperimentList = cloneDeep(this.state.mlExperimentList);
            mlExperimentList.map((temp) => {
                if (temp.id === nextProps.trainMlExperimentSuccess.id) {
                    temp.experimentLoader = false
                }
            })
            this.setState({
                isOpen: true,
                modalType: "success",
                message2: nextProps.trainMlExperimentSuccess.response,
                mlExperimentList,
                predictExperiment: {},
                //isFetching: true,
                predictionPayload: []
            }, () => {
                //this.props.getAllMlExperiments();
                //this.props.getExperimentHistory();
            })
        }

        if (nextProps.trainMlExperimentFailure && this.props.trainMlExperimentFailure != nextProps.trainMlExperimentFailure) {
            let mlExperimentList = cloneDeep(this.state.mlExperimentList)
            mlExperimentList.map((temp) => {
                if (temp.id === nextProps.trainMlExperimentFailure.id) {
                    temp.experimentLoader = false
                }
            })
            this.setState({
                isOpen: true,
                modalType: "error",
                message2: nextProps.trainMlExperimentFailure.error,
                mlExperimentList
            })
        }
    }


    componentDidUpdate(prevProps, prevState) {
        let newProp = Object.keys(SELECTORS).find(prop => this.props[prop])
        if (newProp) {
            if (newProp.includes("Failure")) {
                this.props.showNotification("error", this.props[newProp].error)
            }
            this.props.resetToInitialState(newProp)
        }
    }

    componentWillUnmount() {
        closeMqttConnection()
    }

    mqttMessageHandler = (message, topic) => {
        let dataMessage = { ...message }
        if (topic === mlTopics[0]) {
            let predictResponse = message;
            let isJson = this.IsJsonString(predictResponse);
            if (isJson) {
                predictResponse = { ...message }
            }
            else {
                predictResponse = {}
            }
            this.setState({
                predictResponse,
                modalType: "success",
                isOpen: true,
                message2: "Click on view predict response to see output ",
            })
        }
        else if (topic === mlTopics[1]) {
            let mlExperimentList = cloneDeep(this.state.mlExperimentList),
                selectedExperiment = cloneDeep(this.state.selectedExperiment);
            mlExperimentList.map(exp => {
                if (exp.id === dataMessage.experimentId) {
                    exp.state = dataMessage.status
                    exp.metricConfiguration = exp.metricConfiguration || {}
                    if (dataMessage.type == "metricConfiguration") {
                        exp.metricConfiguration = { ...exp.metricConfiguration, ...dataMessage.algorithmConfiguration }
                    }
                    if (dataMessage.status == "transformSuccess") {
                        exp.metricConfiguration = { ...exp.metricConfiguration, trainSetSize: dataMessage.trainSetSize, testSetSize: dataMessage.testSetSize }
                    }
                }
            })
            if (selectedExperiment.id === dataMessage.experimentId) {
                selectedExperiment.state = dataMessage.status
            }
            this.setState({
                mlExperimentList,
                selectedExperiment,
            })
        }
    }

    onCloseHandler = () => {
        this.setState({
            isOpen: false,
            modalType: '',
            message2: ''
        })
    };

    deleteMlExperiment = (id, deleteExperimentName) => {
        this.setState({
            confirmState: true,
            expToBeDeleted: id,
            deleteExperimentName
        })
    }

    emptySearchBox = () => {
        this.setState({
            searchTerm: ''
        })
    }

    onSearchHandler = (value) => {
        this.setState({
            searchTerm: value
        })
    }

    trainMlExperiment = (state, id) => {
        let mlExperimentList = cloneDeep(this.state.mlExperimentList);
        mlExperimentList.map((temp) => {
            if (temp.id === id) {
                temp.experimentLoader = true
            }
        })
        let payload = { action: { type: state }, experimentId: id }
        if (state === "deleteEndpoint") {
            payload.experimentId = id
        }
        if (state === "predict") {
            payload.experimentId = this.state.predictExperiment.id
            payload.action.data = this.state.predictionPayload
        }
        this.setState({
            mlExperimentList,
            isOpen: false
        }, () => this.props.trainMlExperiment(payload))
    }

    IsJsonString = (str) => {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }

    closeModalHandler = () => {
        this.setState({ predictExperiment: {}, predictionPayload: [] })
    }

    viewExperiment = (id) => {
        let mlExperimentList = cloneDeep(this.state.mlExperimentList),
            selectedExperiment = mlExperimentList.find(exp => exp.id === id);
        this.setState({
            selectedExperiment
        }, () => $("#viewExperimentModal").modal())
    }

    showExperimentHistory = (id, name) => {
        let allExperimentHistory = cloneDeep(this.state.allExperimentHistory),
            selectedExperiment;
        selectedExperiment = allExperimentHistory.find(exp => exp.experimentId === id);
        if (selectedExperiment) {
            selectedExperiment.name = name;
            selectedExperiment.history = selectedExperiment.history.reverse();
        }
        else {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: "No History Found",
            })
            return;
        }
        this.setState({
            selectedExperiment
        }, () => $("#experimentHistoryModal").modal())
    }

    openAccuracyModal = (id) => {
        let allExperimentHistory = cloneDeep(this.state.allExperimentHistory),
            accuracyMatrix,
            selectedExperiment = allExperimentHistory.find(exp => exp.experimentId === id),
            lastTrainingObject = selectedExperiment.history.reverse();
        lastTrainingObject = lastTrainingObject.find(history => history.rpcRequestType === "modelTrain")
        accuracyMatrix = {
            keys: Object.keys(lastTrainingObject.errorMetrics[0]).filter(el => el !== "Timestamp"),
            data: lastTrainingObject.errorMetrics
        }
        this.setState({
            accuracyMatrix
        }, () => $("#showAccuracyModal").modal())
    }

    importExperimentChangeHandler = ({ currentTarget }) => {
        let modalPayload = cloneDeep(this.state.modalPayload);
        if (currentTarget.id === "algorithmType") {
            modalPayload.algorithm[currentTarget.id] = currentTarget.value
        }
        else {
            modalPayload[currentTarget.id] = currentTarget.value
        }
        if (currentTarget.id === "customModelType") {
            modalPayload.algorithm.algorithmType = ''
        }
        this.setState({
            modalPayload
        })
    }

    fileUploadHandler = ({ currentTarget }) => {
        let uploadedFile = currentTarget.files[0],
            payload = {};
        payload.fileType = "ML_EXPERIMENT"
        payload.file = uploadedFile
        this.props.uploadFileHandler(payload);
        this.setState({
            experimentUploadLoader: true,
            experimentFileName: uploadedFile.name
        });
    }

    createExperiment = (e) => {
        e.preventDefault();
        this.setState({
            isModalLoader: true
        }, () => this.props.createExperiment(this.state.modalPayload))
    }

    getColumns = () => {
        let column = [
            {
                Header: "Name", width: "15",
                cell: (row) => (
                    <React.Fragment>
                        <div className="list-view-icon">
                            <img src={require('../../assets/images/other/card-5.png')} />
                        </div>
                        <div className="list-view-icon-box">
                            <h6 className="text-theme fw-600">{row.name}</h6>
                            <p>{`${row.description || "-"}`}</p>
                        </div>
                    </React.Fragment>
                )
            },
            {
                Header: "Connector Name", width: "15",
                cell: (row) => (
                    <h6 style={{ textTransform: "uppercase" }}>{row.dataSource.connectorName}</h6>
                )
            },
            {
                Header: "Status", width: "15",
                cell: (row) => (
                    <h6 style={{ textTransform: "uppercase" }} className={EXPERIMENT_STATES[row.state].className}>{EXPERIMENT_STATES[row.state].name}</h6>
                )
            }, {
                Header: "Model Type", width: "7_5",
                cell: (row) => (
                    <h6>{MODEL_TYPES[row.model].name}</h6>
                )
            },
            {
                Header: "Draft", width: "7_5",
                cell: (row) => (
                    <h6 className={row.isDraft ? "text-green" : "text-red"}>{row.isDraft ? "Yes" : "No"}</h6>
                )
            },
            { Header: "Created", width: 10, accessor: "createdAt" },
            { Header: "Updated", width: 10, accessor: "updatedAt" },
            {
                Header: "Actions", width: "20", cell: (row) => (
                    <div className="button-group" >
                        <button className="btn-transparent btn-transparent-cyan" data-tooltip data-tooltip-text="View Experiment Status" data-tooltip-place="bottom"
                            onClick={() => this.viewExperiment(row.id)}>
                            <i className="far fa-info"></i>
                        </button>
                        {(row.model == "autoML" && row.state == "stop") && <button className="btn-transparent btn-transparent-cyan" data-tooltip data-tooltip-text="Start Auto ML Experiment" data-tooltip-place="bottom"
                            onClick={() => this.trainMlExperiment("sparkautoml", row.id)}>
                            <i className={row.experimentLoader ? "far fa-cog fa-spin" : "far fa-play"}></i>
                        </button>}
                        {/* <button className="btn-transparent btn-transparent-yellow" data-tooltip data-tooltip-text="History" data-tooltip-place="bottom"
                            disabled={(row.state === "stop" || (row.customModel))} onClick={() => this.showExperimentHistory(row.id, row.name)}>
                            <i className="far fa-history"></i>
                        </button> */}
                        {/* {row.state === "deploySuccess" ?
                            <button className="btn-transparent btn-transparent-green" data-tooltip data-tooltip-text="Predict" data-tooltip-place="bottom"
                                onClick={() => { this.setState({ predictExperiment: row, predictionPayload: [] }) }}>
                                <i class="far fa-lightbulb-on"></i>
                            </button> : ''
                        } */}
                        {/* {((row.state === "deploySuccess" || row.state === "trainSuccess") && (!row.customModel)) ?
                            <button className="btn-transparent btn-transparent-gray" data-tooltip data-tooltip-text="Error Metric" data-tooltip-place="bottom"
                                onClick={() => { this.openAccuracyModal(row.id) }}>
                                <i className="far fa-bug"></i>
                            </button> : ''
                        } */}
                        {((row.state === "trainSuccess") && (row.model == "autoML")) ?
                            <button className="btn-transparent btn-transparent-gray" data-tooltip data-tooltip-text="Metric Configuration" data-tooltip-place="bottom"
                                onClick={() => { this.openMetricConfiguration(row) }}>
                                <i className="far fa-bug"></i>
                            </button> : ''
                        }
                        {((row.state === "trainSuccess") && (row.model == "autoML")) ?
                            <button className="btn-transparent btn-transparent-green" disabled={!row.model_uri} data-tooltip data-tooltip-text={row.model_uri ? "Download Model" : "Model file has validity for 24 hour only"} data-tooltip-place="bottom"
                                onClick={() => { window.open(row.model_uri) }}>
                                <i className="fad fa-download"></i>
                            </button> : ''
                        }
                        {/* <button className="btn-transparent btn-transparent-red" data-tooltip data-tooltip-text="Remove Endpoint" data-tooltip-place="bottom"
                            disabled={row.state !== "deploySuccess"} onClick={() => { this.trainMlExperiment("deleteEndpoint", row.id) }}>
                            <i className="far fa-minus-hexagon"></i>
                        </button> */}
                        {!row.customModel ?
                            <button className="btn-transparent btn-transparent-blue" data-tooltip data-tooltip-text="Edit" data-tooltip-place="bottom"
                                onClick={() => row.model == "autoML" ? this.setState({ openAutoMLForm: true, isAddOrEditExperiment: false, modalPayload: { ...row }, isFetchingColumns: true }, () => this.props.getColumnsByConnector(this.state.modalPayload.dataSource)) : this.props.history.push(`/addOrEditMlExperiments/${row.id}`)}>
                                <i className="far fa-pencil"></i>
                            </button> : ''
                        }
                        <button className="btn-transparent btn-transparent-red" data-tooltip data-tooltip-text="Delete" data-tooltip-place="bottom"
                            disabled={row.state === "deploySuccess"}
                            onClick={() => this.deleteMlExperiment(row.id, row.name)}>
                            <i className="far fa-trash-alt"></i>
                        </button>
                    </div>
                )
            },
        ];
        return column;
    }

    getStatusBarClass = (row) => {
        if (row.state === "deploySuccess")
            return { className: "green", tooltipText: "Running" }
        else if (row.state === "stop")
            return { className: "red", tooltipText: "Stopped" }
        return { className: "gray", tooltipText: "In Progress" }
    }

    addNewExperimentHandler = () => {
        this.setState({
            isAddOrEditExperiment: true,
        })
    }

    dataSourceChangeHandler = (type, obj) => {
        let isFetchingColumns = this.state.isFetchingColumns;
        let modalPayload = cloneDeep(this.state.modalPayload)
        if (type === "connector") {
            modalPayload.dataSource.connectorId = obj.id
            modalPayload.dataSource.connectorName = obj.name
            modalPayload.dataSource.bucketId = ""
        }
        else if (type === "bucket") {
            modalPayload.dataSource.bucketId = obj.bucketId
            let requiredConnector = this.state.listOfSourceConnectors["S3"].find(el => el.id == modalPayload.dataSource.connectorId).properties.list.find(el => el.bucketId == obj.bucketId)
            modalPayload.dataSource.basePath = requiredConnector.basePath
            modalPayload.dataSource.format = requiredConnector.format
            isFetchingColumns = true
        }
        modalPayload.algorithm.hyperparams.target = ""
        this.setState({
            modalPayload,
            isFetchingColumns,
            listOfColumns: []
        }, () => {
            type === "bucket" &&
                this.props.getColumnsByConnector(this.state.modalPayload.dataSource)
        })
    }

    autoMLExperimentChangeHandler = ({ currentTarget: { id, value, name } }) => {
        let modalPayload = produce(this.state.modalPayload, payload => {
            if (name == "algorithm")
                payload.algorithm[id] = value
            else
                payload[id] = value
        })
        this.setState({
            modalPayload
        })
    }

    autoMLSelectChangeHandler = (valueObj, id) => {
        let modalPayload = produce(this.state.modalPayload, payload => {
            payload.algorithm.hyperparams[id] = valueObj.value
            if (id == "trainingJobType") {
                payload.algorithm.hyperparams.algorithmName = payload.algorithm.hyperparams.trainingJobType == "regression" ?
                    optionsForRegressionAlgorithm[0].value
                    : optionsForClassificationAlgorithm[0].value
            }
        })
        this.setState({
            modalPayload
        })
    }

    refreshComponent = () => {
        this.setState({
            isFetching: true
        }, () => this.props.getAllMlExperiments())
    }

    openMetricConfiguration = (experiment) => {
        this.setState({ metricConfigurationData: experiment }, () => $("#metricConfigurationModal").modal())
    }

    render() {
        let filteredExperimentList = (this.state.searchTerm ? this.state.mlExperimentList.filter(el => (el.name.toLowerCase()).includes(this.state.searchTerm.toLowerCase())) : this.state.mlExperimentList);
        return (
            <React.Fragment>
                <Helmet>
                    <title>ML Experiments</title>
                    <meta name="description" content="Description of MlExperiments" />
                </Helmet>

                <QuotaHeader
                    showQuota
                    addButtonText="Add ML Experiment"
                    componentName="Ml Experiments"
                    productName="experiments"
                    onAddClick={this.addNewExperimentHandler}
                    searchBoxDetails={{
                        value: this.state.searchTerm,
                        onChangeHandler: this.onSearchHandler,
                        isDisabled: !this.state.mlExperimentList.length
                    }}
                    refreshHandler={this.refreshComponent}
                    callBack={this.qoutaCallback}
                    isDeletedSuccess={this.state.isDeletedSuccess}
                    {...this.props}
                />

                {this.state.isFetching ?
                    <Loader /> :
                    <div className="content-body" id="ml-studio">
                        {this.state.mlExperimentList.length ?
                            filteredExperimentList.length ?
                                <ListingTable
                                    columns={this.getColumns()}
                                    data={filteredExperimentList}
                                    statusBar={this.getStatusBarClass}
                                />
                                : <div className="application-search-content">
                                    <div>
                                        <div className="application-search-image">
                                            <img src="https://content.iot83.com/m83/misc/noSearchResult.png" />
                                        </div>
                                        <p>No data matched your search. Try using other search options.</p>
                                    </div>
                                </div> :
                            <AddNewButton
                                text1="No ML Experiment(s) available"
                                text2="You haven't created any ML Experiments yet"
                                imageIcon="addMlExperiment.png"
                                addButtonEnable={true}
                                createItemOnAddButtonClick={this.addNewExperimentHandler}
                            />
                        }
                    </div>
                }

                {/* add experiment modal */}
                {this.state.isAddOrEditExperiment ?
                    <div className="modal d-block animated slideInDown">
                        <div className="modal-dialog modal-dialog-centered">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h6 className="modal-title">Add New Experiment
                                        <button type="button" className="close" data-tooltip data-tooltip-text="Close" data-tooltip-place="bottom" onClick={() => { this.setState({ isAddOrEditExperiment: false }) }}>
                                            <i className="far fa-times"></i>
                                        </button>
                                    </h6>
                                </div>
                                <div className="modal-body">
                                    <div className="button-group add-exp-button-group">
                                        <button type="button" className="btn btn-primary" data-dismiss="modal" disabled onClick={() => { this.props.history.push('/addOrEditMlExperiments') }}>Create New</button>
                                        <button type="button" className="btn btn-primary" data-dismiss="modal" disabled onClick={() => { this.setState({ openCustomForm: true, isAddOrEditExperiment: false, modalPayload: { ...IMPORT_EXPERIMENT_PAYLOAD } }) }}>Custom Model</button>
                                        <button type="button" className="btn btn-primary" data-dismiss="modal" onClick={() => { this.setState({ openAutoMLForm: true, isAddOrEditExperiment: false, modalPayload: { ...AUTOML_EXPERIMENT_PAYLOAD } }) }}>Auto ML</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> :
                    ""
                }
                {/* end add experiment modal */}

                {/* add custom / auto ml form */}
                <SlidingPane
                    className=''
                    overlayClassName='sliding-form'
                    closeIcon={<div></div>}
                    isOpen={this.state.openCustomForm || this.state.openAutoMLForm || false}
                    from='right'
                    width='500px'
                >
                    <div className="modal-content">
                        <div className="modal-header">
                            <h6 className="modal-title">{this.state.openAutoMLForm ? this.state.modalPayload.id ? "Update Auto ML" : "Add Auto ML" : "Add New Custom Model"}
                                <button className="btn btn-light close" onClick={() => { this.setState({ openCustomForm: false, openAutoMLForm: false }) }}><i className="fas fa-angle-right"></i></button>
                            </h6>
                        </div>

                        {this.state.openAutoMLForm ?
                            this.state.isModalLoader ?
                                <div className="modal-loader">
                                    <i className="fad fa-sync-alt fa-spin"></i>
                                </div>
                                :
                                <form onSubmit={this.createExperiment}>
                                    <div className="modal-body">
                                        <div className="form-group">
                                            <label className="form-group-label">Experiment Name : <i className="fas fa-asterisk form-group-required"></i></label>
                                            <input type="text" id="name" className="form-control" value={this.state.modalPayload.name} required
                                                onChange={this.autoMLExperimentChangeHandler}
                                            />
                                        </div>

                                        <div className="form-group">
                                            <label className="form-group-label">Description :</label>
                                            <textarea id="description" rows="3" className="form-control" value={this.state.modalPayload.description}
                                                onChange={this.autoMLExperimentChangeHandler}
                                            />
                                        </div>

                                        <div className="form-group">
                                            <label className="form-group-label">Select Source : <i className="fas fa-asterisk form-group-required"></i></label>
                                            <div className="collapse-wrapper shadow-none border border-radius-4">
                                                <div className="collapse-header" data-toggle="collapse" data-target="#amazonS3">
                                                    <h6 className="fw-400">Amazon S3 <strong className="text-theme">({this.state.listOfSourceConnectors["S3"].length})</strong>
                                                        <i className="fad fa-chevron-double-up collapse-header-toggle collapsed" data-toggle="collapse" data-target="#amazonS3"></i>
                                                    </h6>
                                                </div>
                                                <div className={this.state.modalPayload.id ? 'collapse show' : "collapse"} id="amazonS3">
                                                    <div className="collapse-body">
                                                        {this.state.listOfSourceConnectors["S3"].length > 0 ?
                                                            this.state.listOfSourceConnectors["S3"].map((connector, index) => {
                                                                return (
                                                                    <div className="collapse-wrapper shadow-none border border-radius-4" key={index}>
                                                                        <div className="collapse-header" onClick={() => this.dataSourceChangeHandler("connector", connector)} data-toggle="collapse" data-target={"#child" + index}>
                                                                            <h6 className="fw-400" >{connector.name} <strong className="text-theme">({connector.properties.list.length})</strong>
                                                                                <i className="fad fa-chevron-double-up collapse-header-toggle collapsed" data-toggle="collapse" data-target={"#child" + index}></i>
                                                                            </h6>
                                                                        </div>
                                                                        <div className={this.state.modalPayload.dataSource.connectorId === connector.id ? "collapse show" : "collapse"} id={"child" + index}>
                                                                            <div className="collapse-body d-flex">
                                                                                {connector.properties.list.map((bucket, bucketIndex) => {
                                                                                    return (
                                                                                        <label className="check-box flex-50 mb-2" key={bucketIndex}>
                                                                                            <span className="check-text text-capitalize">{bucket.bucketName}</span>
                                                                                            <input type="checkbox" checked={this.state.modalPayload.dataSource.bucketId == bucket.bucketId} onChange={() => this.state.isFetchingColumns ? false : this.dataSourceChangeHandler("bucket", bucket)} />
                                                                                            <span className="check-mark"></span>
                                                                                        </label>
                                                                                    )
                                                                                })}
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                )
                                                            }) :
                                                            <div className="inner-loader-wrapper" style={{ height: '200px' }}>
                                                                <div className="inner-loader-content">
                                                                    <i className="fad fa-sync-alt fa-spin"></i>
                                                                </div>
                                                            </div>
                                                        }
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="form-group">
                                            <label className="form-group-label">Target Column :  <i className="fas fa-asterisk form-group-required"></i></label>
                                            <ReactSelect
                                                className="form-control-multi-select"
                                                value={this.state.listOfColumns.find(el => el.value == this.state.modalPayload.algorithm.hyperparams.target)}
                                                onChange={(value) => this.autoMLSelectChangeHandler(value, "target")}
                                                options={this.state.listOfColumns}
                                                isDisabled={this.state.isFetchingColumns}
                                            />
                                            <input
                                                tabIndex={-1} style={{ opacity: 0, width: "100%", height: 0, position: "absolute" }}
                                                value={this.state.modalPayload.algorithm.hyperparams.target}
                                                onChange={() => { return false }}
                                                required={true}
                                            />
                                        </div>

                                        <div className="form-group">
                                            <label className="form-group-label fw-600 text-dark-theme text-uppercase">Hyper Parameters</label>
                                        </div>

                                        <div className="form-group">
                                            <label className="form-group-label">Training Job Name : <i className="fas fa-asterisk form-group-required"></i> </label>
                                            <input type="text" className="form-control" id="trainingJobName" name="algorithm" value={this.state.modalPayload.algorithm.trainingJobName} onChange={this.autoMLExperimentChangeHandler} pattern="[A-Za-z]{1,128}" required />
                                        </div>

                                        <div className="form-group">
                                            <label className="form-group-label">Training Job Type : <i className="fas fa-asterisk form-group-required"></i></label>
                                            <ReactSelect
                                                className="form-control-multi-select"
                                                value={optionsForTrainingJobType.find(el => el.value == this.state.modalPayload.algorithm.hyperparams.trainingJobType)}
                                                onChange={(value) => this.autoMLSelectChangeHandler(value, "trainingJobType")}
                                                options={optionsForTrainingJobType}
                                            />
                                        </div>

                                        <div className="form-group">
                                            <label className="form-group-label">Training Algorithm Name : <i className="fas fa-asterisk form-group-required"></i></label>
                                            <ReactSelect
                                                className="form-control-multi-select"
                                                value={this.state.modalPayload.algorithm.hyperparams.trainingJobType == "regression" ?
                                                    optionsForRegressionAlgorithm.find(el => el.value == this.state.modalPayload.algorithm.hyperparams.algorithmName)
                                                    : optionsForClassificationAlgorithm.find(el => el.value == this.state.modalPayload.algorithm.hyperparams.algorithmName)}
                                                onChange={(value) => this.autoMLSelectChangeHandler(value, "algorithmName")}
                                                options={this.state.modalPayload.algorithm.hyperparams.trainingJobType == "regression" ?
                                                    optionsForRegressionAlgorithm : optionsForClassificationAlgorithm}
                                            />
                                        </div>
                                    </div>

                                    <div className="modal-footer justify-content-start">
                                        <button type="submit" className="btn btn-primary" disabled={this.state.experimentUploadLoader}>{this.state.modalPayload.id ? 'Update' : 'Save'}</button>
                                        <button type="button" className="btn btn-dark" onClick={() => { this.setState({ openAutoMLForm: false }) }}>Cancel</button>
                                    </div>
                                </form>
                            :
                            <form onSubmit={this.createExperiment}>
                                <div className="modal-body">
                                    <div className="form-group">
                                        <label className="form-group-label">Experiment Name : <i className="fas fa-asterisk form-group-required"></i></label>
                                        <input type="text" id="name" className="form-control" value={this.state.modalPayload.name}
                                            onChange={this.importExperimentChangeHandler}
                                        />
                                    </div>

                                    <div className="form-group">
                                        <label className="form-group-label">Description :</label>
                                        <textarea id="description" rows="3" className="form-control" value={this.state.modalPayload.description}
                                            onChange={this.importExperimentChangeHandler}
                                        />
                                    </div>

                                    <div className="form-group">
                                        <label className="form-group-label">Endpoint Name : <i className="fas fa-asterisk form-group-required"></i></label>
                                        <input type="text" id="endpointname" className="form-control" value={this.state.modalPayload.endpointname}
                                            onChange={this.importExperimentChangeHandler}
                                        />
                                    </div>

                                    <div className="form-group">
                                        <label className="form-group-label">Custom Model Type : <i className="fas fa-asterisk form-group-required"></i></label>
                                        <select id="customModelType" className="form-control" required value={this.state.modalPayload.customModelType}
                                            onChange={this.importExperimentChangeHandler}>
                                            <option value="sageMakerTrainedModel">Sagemaker Trained Model </option>
                                            <option value="skLearnTrainedModel">SK Learn Trained Model </option>
                                            <option value="tensorflowTrainedModel">Tensorflow Trained Model </option>
                                        </select>
                                    </div>

                                    {this.state.modalPayload.customModelType === "sageMakerTrainedModel" ?
                                        <div className="form-group">
                                            <label className="form-group-label">Algorithm Type : <i className="fas fa-asterisk form-group-required"></i></label>
                                            <select id="algorithmType" className="form-control" required value={this.state.modalPayload.algorithmType}
                                                onChange={this.importExperimentChangeHandler}>
                                                <option value="linear-learner">Linear Learner</option>
                                                <option value="XgBoost">XGBoost</option>
                                                <option value="RandomCutForest">RCF</option>
                                            </select>
                                        </div> : ''
                                    }

                                    <div className="form-group">
                                        <label className="form-group-label">Upload File : <i className="fas fa-asterisk form-group-required"></i></label>
                                        <div className="file-upload-box">
                                            {this.state.experimentUploadLoader ?
                                                <div className="file-upload-loader">
                                                    <img src="https://content.iot83.com/m83/misc/uploading.gif" />
                                                    <p>Uploading...</p>
                                                </div> :
                                                this.state.modalPayload.s3Path ?
                                                    <div className="file-upload-preview">
                                                        <input type="file" name="excelUpload" onChange={this.fileUploadHandler} />
                                                        <button type="button" className="btn-transparent btn-transparent-gray" data-tooltip data-tooltip-text="Update" data-tooltip-place="bottom">
                                                            <i className="far fa-pen" />
                                                        </button>
                                                        <p>{this.state.experimentFileName}</p>
                                                    </div> :
                                                    <div className="file-upload-form">
                                                        <input required type="file" name="excelUpload" onChange={this.fileUploadHandler} />
                                                        <p>Click to upload file...!</p>
                                                    </div>
                                            }
                                        </div>
                                    </div>
                                </div>

                                <div className="modal-footer justify-content-start">
                                    <button type="submit" className="btn btn-primary" disabled={this.state.experimentUploadLoader}>Save</button>
                                    <button type="button" className="btn btn-dark" onClick={() => { this.setState({ openCustomForm: false }) }}>Cancel</button>
                                </div>
                            </form>
                        }
                    </div>
                </SlidingPane>
                {/* end add custom / auto ml form */}

                {/* prediction payload modal */}
                <SlidingPane
                    className=''
                    overlayClassName='sliding-form'
                    closeIcon={<div></div>}
                    isOpen={(Object.keys(this.state.predictExperiment).length) ? true : false}
                    from='right'
                    width='500px'
                >
                    <div className="modal-content">
                        <div className="modal-header">
                            <h6 className="modal-title">Prediction payload
                                <button className="btn btn-light close" onClick={this.closeModalHandler}><i className="fas fa-angle-right"></i></button>
                            </h6>
                        </div>

                        <form onSubmit={(event) => { event.preventDefault(); this.trainMlExperiment("predict", this.state.predictExperiment.id) }}>
                            <div className="modal-body">
                                <div className="form-group">
                                    {this.state.predictExperiment.sampleData && this.state.predictExperiment.sampleData.length ?
                                        <div className="content-table">
                                            <table className="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        {this.state.predictExperiment.sequenceKeys.map((el, index) => {
                                                            return (
                                                                <th key={index}>{index === 0 && (this.state.predictExperiment.algorithm && (this.state.predictExperiment.algorithm.algorithmName === "Classification" || this.state.predictExperiment.algorithm.algorithmName === "Regression")) ? `${el}(TargetCol)` : el}</th>
                                                            )
                                                        })}
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {this.state.predictExperiment.sampleData.map((obj, index) => {
                                                        let html
                                                        html = (
                                                            <tr key={index}>
                                                                {this.state.predictExperiment.sequenceKeys.map((el, keyIndex) => {
                                                                    return (
                                                                        <td key={keyIndex}>
                                                                            {obj[el]}
                                                                        </td>)
                                                                })}
                                                            </tr>
                                                        )
                                                        return html;
                                                    })}
                                                </tbody>
                                            </table>
                                        </div> : ''
                                    }
                                </div>

                                <div className="alert alert-info note-text">
                                    <p><strong>Note :</strong></p>
                                    <p><strong>1.</strong> Add data in comma separated format</p>
                                    {this.state.predictExperiment.algorithm && (this.state.predictExperiment.algorithm.algorithmName === "Classification" || this.state.predictExperiment.algorithm.algorithmName === "Regression") ?
                                        <p><strong>2.</strong> The first column is the target column,indicated by TargetCol that needs to be predicted </p> : ''
                                    }
                                </div>

                                <div className="form-group">
                                    <label className="form-group-label">Add Data : <i className="fas fa-asterisk form-group-required"></i></label>
                                    <TagsInput value={this.state.predictionPayload} onChange={(tags) => { this.setState({ predictionPayload: tags }) }} />
                                </div>
                            </div>

                            <div className="modal-footer justify-content-start">
                                <button type="submit" className="btn btn-primary" disabled={!this.state.predictionPayload.length}>Predict</button>
                                <button type="button" className="btn btn-dark" data-dismiss="modal" onClick={this.closeModalHandler}>Cancel</button>
                            </div>
                        </form>
                    </div>
                </SlidingPane>
                {/* end prediction payload modal */}

                {/* history modal */}
                <div className="modal fade animated slideInDown" role="dialog" id="experimentHistoryModal">
                    {this.state.selectedExperiment.experimentId &&
                        <div className="modal-dialog modal-lg modal-dialog-centered">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h6 className="modal-title">Experiment - <span>{this.state.selectedExperiment.name}</span>
                                        <button type="button" className="close" data-tooltip data-tooltip-text="Close" data-dismiss="modal" data-tooltip-place="bottom" onClick={() => this.setState({ selectedExperiment: {} })}><i className="far fa-times"></i></button>
                                    </h6>
                                </div>

                                <div className="modal-body">
                                    <div className="history-timeline-wrapper">
                                        <InfiniteScroll
                                            dataLength=""
                                            // loader={<h4>Loading...</h4>}
                                            // endMessage={<p className="text-center text-content">You have seen all the History.</p>}
                                            height="100%"
                                        >
                                            <ul className="list-style-none">
                                                {this.state.selectedExperiment.history.map((obj, index) => {
                                                    return (
                                                        <li key={index}>
                                                            <div className="history-timeline-box">
                                                                <div className={`history-timeline-state ${obj.state === "in_progress" ? "bg-yellow" : obj.state === "success" ? "bg-green" : "bg-red"}`}></div>
                                                                <span className={`badge badge-pill ${obj.state === "in_progress" ? "badge-warning" : obj.state === "success" ? "badge-success" : "badge-danger"}`}>{obj.state === "in_progress" ? "In Progress" : obj.state === "success" ? "Success" : "Error"}</span>
                                                                <h6><strong>Type : </strong>{obj.rpcRequestType === "transformation" ? "Transform Model" : obj.rpcRequestType === "modelTrain" ? "Train Model" : obj.rpcRequestType === "createEndPoint" ? "Deploy Model" : "Delete Model"}</h6>
                                                                <p>{getTimeDifference(obj.requestedAt)}</p>
                                                            </div>
                                                        </li>
                                                    )
                                                })}
                                            </ul>
                                        </InfiniteScroll>
                                    </div>
                                </div>

                                <div className="modal-footer">
                                    <button type="button" className="btn btn-success" onClick={() => this.setState({ selectedExperiment: {} })}>OK</button>
                                </div>
                            </div>
                        </div>
                    }
                </div>
                {/* history modal */}

                {/* view experiment modal */}
                <div className="modal fade animated slideInDown" id="viewExperimentModal">
                    {this.state.selectedExperiment.id &&
                        <div className="modal-dialog modal-xl modal-dialog-centered">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h6 className="modal-title">Experiment - <span>{this.state.selectedExperiment.name}</span>
                                        <button type="button" className="close" data-tooltip data-tooltip-text="Close" onClick={() => this.setState({ selectedExperiment: {} }, () => $("#viewExperimentModal").modal('hide'))}>
                                            <i className="far fa-times"></i>
                                        </button>
                                    </h6>
                                </div>

                                <div className="modal-body">
                                    <ul className="train-model-list">
                                        <li className="active flex-33">
                                            <div className="train-model-box">
                                                <div className="train-model-image">
                                                    <img src="https://content.iot83.com/m83/mlStudio/mlExperiment.png" />
                                                    <i className="fas fa-check-circle"></i>
                                                </div>
                                                <h6 className="fw-600 text-gray f-13">Experiment Creation</h6>
                                                <p className="f-11 text-content">{new Date(this.state.selectedExperiment.updatedAt).toLocaleDateString()} | {new Date(this.state.selectedExperiment.updatedAt).toLocaleTimeString()}</p>
                                                <span className="badge badge-pill badge-success"><i className="far fa-check mr-2"></i>Successful</span>
                                            </div>
                                        </li>
                                        <li className={this.state.selectedExperiment.state === "stop" || this.state.selectedExperiment.state === "transformInProgress" ? "flex-33" : "flex-33 active"}>
                                            <div className="train-model-box">
                                                <div className="train-model-image">
                                                    <img src="https://content.iot83.com/m83/mlStudio/transform.png" />
                                                    {(this.state.selectedExperiment.state === "transformSuccess" || this.state.selectedExperiment.state.includes("train") || this.state.selectedExperiment.state.includes("deploy")) &&
                                                        <i className="fas fa-check-circle"></i>
                                                    }
                                                </div>
                                                <h6 className="fw-600 text-gray f-13">Transformation</h6>
                                                <p className="f-11 text-content">{new Date(this.state.selectedExperiment.updatedAt).toLocaleDateString()} | {new Date(this.state.selectedExperiment.updatedAt).toLocaleTimeString()}</p>
                                                {(this.state.selectedExperiment.state === "transformSuccess" || this.state.selectedExperiment.state.includes("train") || this.state.selectedExperiment.state.includes("deploy")) ?
                                                    <span className="badge badge-pill badge-success"><i className="far fa-check mr-2"></i>Successful</span> :
                                                    <button type="button" disabled className="btn btn-link p-0" onClick={() => { this.trainMlExperiment("preProcess", this.state.selectedExperiment.id) }}><i className="far fa-link"></i>Transform</button>
                                                }
                                                {this.state.selectedExperiment.state === "transformInProgress" &&
                                                    <div className="train-model-loader">
                                                        <img src="https://content.iot83.com/m83/misc/uploading.gif" />
                                                    </div>
                                                }
                                            </div>
                                        </li>
                                        <li className={this.state.selectedExperiment.state === "stop" || this.state.selectedExperiment.state.includes("transform") || this.state.selectedExperiment.state === "trainInProgress" ? "flex-33" : "flex-33 active"}>
                                            <div className="train-model-box">
                                                <div className="train-model-image">
                                                    <img src="https://content.iot83.com/m83/mlStudio/trainModel.png" />
                                                    {(this.state.selectedExperiment.state !== "stop" && (!this.state.selectedExperiment.state.includes("transform")) && this.state.selectedExperiment.state !== "trainInProgress") &&
                                                        <i className="fas fa-check-circle"></i>
                                                    }
                                                </div>
                                                <h6 className="fw-600 text-gray f-13">Train Model</h6>
                                                <p className="f-11 text-content">{new Date(this.state.selectedExperiment.updatedAt).toLocaleDateString()} | {new Date(this.state.selectedExperiment.updatedAt).toLocaleTimeString()}</p>
                                                {(this.state.selectedExperiment.state === "trainSuccess" || this.state.selectedExperiment.state.includes("deploy")) ?
                                                    <span className="badge badge-pill badge-success"><i className="far fa-check mr-2"></i>Successful</span> :
                                                    <button type="button" disabled className="btn btn-link p-0" onClick={() => { if (this.state.selectedExperiment.state === "transformSuccess") { this.trainMlExperiment("modelTrain", this.state.selectedExperiment.id) } }}><i className="far fa-link"></i>Train</button>
                                                }
                                                {this.state.selectedExperiment.state === "trainInProgress" &&
                                                    <div className="train-model-loader">
                                                        <img src="https://content.iot83.com/m83/misc/uploading.gif" />
                                                    </div>
                                                }
                                            </div>
                                        </li>
                                        {/* <li className={this.state.selectedExperiment.state === "deploySuccess" ? "" : "active"}>
                                            <div className="train-model-box">
                                                <div className="train-model-image">
                                                    <img src="https://content.iot83.com/m83/mlStudio/endpoint.png" />
                                                    {this.state.selectedExperiment.state === "deploySuccess" &&
                                                        <i className="fas fa-check-circle"></i>
                                                    }
                                                </div>
                                                <h6 className="fw-600 text-gray f-13">Deployment</h6>
                                                <p className="f-11 text-content">{new Date(this.state.selectedExperiment.updatedAt).toLocaleDateString()} | {new Date(this.state.selectedExperiment.updatedAt).toLocaleTimeString()}</p>
                                                {this.state.selectedExperiment.state === "deploySuccess" ?
                                                    <span className="badge badge-pill badge-success"><i className="far fa-check mr-2"></i>Successful</span> :
                                                    <button type="button" className="btn btn-link p-0" onClick={() => { if (this.state.selectedExperiment.state === "trainSuccess") { this.trainMlExperiment("createEndPoint", this.state.selectedExperiment.id) } }}><i className="far fa-link"></i>Deploy</button>
                                                }
                                                {this.state.selectedExperiment.state === "deployInProgress" &&
                                                    <div className="train-model-loader">
                                                        <img src="https://content.iot83.com/m83/misc/uploading.gif" />
                                                    </div>
                                                }
                                            </div>
                                        </li> */}
                                    </ul>
                                </div>
                            </div>
                        </div>
                    }
                </div>
                {/* end view experiment modal */}

                {/* prediction output modal */}
                <div className="modal fade animated slideInDown" role="dialog">
                    <div className="modal-dialog modal-lg modal-dialog-centered">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h6 className="modal-title">Prediction Output
                                    <button type="button" className="close" data-tooltip data-tooltip-text="Close" data-dismiss="modal"><i className="far fa-times"></i></button>
                                </h6>
                            </div>
                            <div className="modal-body">
                                <div className="overflow-y-auto" style={{ height: 300 }}>
                                    <ReactJson src={this.state.predictResponse} />
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-success">OK</button>
                            </div>
                        </div>
                    </div>
                </div>
                {/* end prediction output modal */}

                {/* prediction error modal */}
                <div className="modal fade animated slideInDown" role="dialog" id="showAccuracyModal">
                    {this.state.accuracyMatrix &&
                        <div className="modal-dialog modal-lg modal-dialog-centered">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h6 className="modal-title">Prediction Errors
                                        <button type="button" className="close" data-tooltip data-tooltip-text="Close" data-dismiss="modal"><i className="far fa-times"></i></button>
                                    </h6>
                                </div>

                                <div className="modal-body">
                                    {this.state.accuracyMatrix.data.length ?
                                        <div className="content-table overflow-y-auto" style={{ height: 400 }}>
                                            <table className="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        {this.state.accuracyMatrix.keys.map((el, index) => {
                                                            return (<th key={index}>{el}</th>)
                                                        })}
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {this.state.accuracyMatrix.data.map((data, index) => {
                                                        let html = <tr key={index}>
                                                            {this.state.accuracyMatrix.keys.map((el, elIndex) => {
                                                                return (
                                                                    <td key={elIndex}>{data[el]}</td>
                                                                )
                                                            })}
                                                        </tr>
                                                        return html;
                                                    })}
                                                </tbody>
                                            </table>
                                        </div> :
                                        <div className="inner-message-wrapper" style={{ height: 400 }}>
                                            <div className="inner-message-content">
                                                <i className="fad fa-exclamation-triangle text-red"></i>
                                                <h6>No error matrix found.</h6>
                                            </div>
                                        </div>
                                    }
                                </div>

                                <div className="modal-footer">
                                    <button type="button" className="btn btn-success">OK</button>
                                </div>
                            </div>
                        </div>
                    }
                </div>
                {/* end prediction error modal */}

                {/* metric configuration modal */}
                <div className="modal fade animated slideInDown" role="dialog" id="metricConfigurationModal">
                    {this.state.metricConfigurationData &&
                        <div className="modal-dialog modal-lg modal-dialog-centered">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h6 className="modal-title">Metric Configuration : {this.state.metricConfigurationData.name}
                                        <button type="button" className="close" data-tooltip data-tooltip-text="Close" data-dismiss="modal"><i className="far fa-times"></i></button>
                                    </h6>
                                </div>

                                <div className="modal-body">
                                    {this.state.metricConfigurationData.metricConfiguration && Object.keys(this.state.metricConfigurationData.metricConfiguration).length ?
                                        <div className="content-table overflow-y-auto" style={{ height: 400 }}>
                                            <ReactJson src={this.state.metricConfigurationData.metricConfiguration} />
                                        </div> :
                                        <div className="inner-message-wrapper" style={{ height: 400 }}>
                                            <div className="inner-message-content">
                                                <i className="fad fa-exclamation-triangle text-red"></i>
                                                <h6>No Metric Configuration found.</h6>
                                            </div>
                                        </div>
                                    }
                                </div>

                                <div className="modal-footer">
                                    <button type="button" className="btn btn-success" data-dismiss="modal">OK</button>
                                </div>
                            </div>
                        </div>
                    }
                </div>
                {/* metric configuration modal */}

                {this.state.isOpen &&
                    <NotificationModal
                        type={this.state.modalType}
                        message2={this.state.message2}
                        onCloseHandler={this.onCloseHandler}
                    />
                }

                {this.state.confirmState &&
                    <ConfirmModel
                        status={"delete"}
                        deleteName={this.state.deleteExperimentName}
                        confirmClicked={() => {
                            this.props.deleteMlExperiment(this.state.expToBeDeleted);
                            this.setState({
                                isFetching: true,
                                confirmState: false,
                            })
                        }}
                        cancelClicked={() => {
                            this.setState({
                                confirmState: false,
                            })
                        }}
                    />
                }

            </React.Fragment>
        );
    }

}


MlStudio.propTypes = {
    dispatch: PropTypes.func.isRequired
};
let allSelectors = {}
Object.entries(SELECTORS).map(([key, value]) => {
    allSelectors[key] = value()
})
const mapStateToProps = createStructuredSelector(allSelectors);

function mapDispatchToProps(dispatch) {
    let allActions = { dispatch }
    Object.entries(ACTIONS).map(([key, value]) => {
        allActions[key] = (...args) => dispatch(value(...args))
    })
    return allActions;
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "mlStudio", reducer });
const withSaga = injectSaga({ key: "mlStudio", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(MlStudio);
