/*
 *
 * MlStudio actions
 *
 */

import * as CONSTANTS from './constants';


let allCases = Object.values(CONSTANTS).filter(constant => typeof (constant) == "object")

export const allActions = {
  resetToInitialState: (prop) => {
    return {
      type: CONSTANTS.RESET_TO_INITIAL_STATE,
      prop
    };
  }
}
allCases.map(constant => {
  allActions[constant.actionName] = (...args) => {
    let actionObj = { type: constant.action }
    constant.actionArguments && constant.actionArguments.map((arg, index) => {
      actionObj[arg] = args[index]
    })
    return actionObj
  }
})
