/*
 *
 * MlStudio constants
 *
 */
export const RESET_TO_INITIAL_STATE = "app/MlStudio/RESET_TO_INITIAL_STATE";
export const GET_ALL_ML_EXPERIMENTS = {
	action: 'app/MlStudio/GET_ALL_ML_EXPERIMENTS',
	success: "app/MlStudio/GET_ALL_ML_EXPERIMENTS_SUCCESS",
	failure: "app/MlStudio/GET_ALL_ML_EXPERIMENTS_FAILURE",
	urlKey: "getAllMlExperiments",
	successKey: "getAllMlExperimentsSuccess",
	failureKey: "getAllMlExperimentsFailure",
	actionName: "getAllMlExperiments",
}
export const DELETE_ML_EXPERIMENT = {
	action: 'app/MlStudio/DELETE_ML_EXPERIMENT',
	success: "app/MlStudio/DELETE_ML_EXPERIMENT_SUCCESS",
	failure: "app/MlStudio/DELETE_ML_EXPERIMENT_FAILURE",
	urlKey: "deleteMlExperiment",
	successKey: "deleteMlExperimentSuccess",
	failureKey: "deleteMlExperimentFailure",
	actionName: "deleteMlExperiment",
	actionArguments: ["id"],
}
export const TRAIN_ML_EXPERIMENT = {
	action: 'app/MlStudio/TRAIN_ML_EXPERIMENT',
	success: "app/MlStudio/TRAIN_ML_EXPERIMENT_SUCCESS",
	failure: "app/MlStudio/TRAIN_ML_EXPERIMENT_FAILURE",
	urlKey: "trainMlExperiment",
	successKey: "trainMlExperimentSuccess",
	failureKey: "trainMlExperimentFailure",
	actionName: "trainMlExperiment",
	actionArguments: ["payload"],
}
export const GET_EXPERIMENT_HISTORY = {
	action: 'app/MlStudio/GET_EXPERIMENT_HISTORY',
	success: "app/MlStudio/GET_EXPERIMENT_HISTORY_SUCCESS",
	failure: "app/MlStudio/GET_EXPERIMENT_HISTORY_FAILURE",
	urlKey: "getExperimentHistory",
	successKey: "getExperimentHistorySuccess",
	failureKey: "getExperimentHistoryFailure",
	actionName: "getExperimentHistory",
}
export const UPLOAD_FILE = {
	action: 'app/MlStudio/UPLOAD_FILE',
	success: "app/MlStudio/UPLOAD_FILE_SUCCESS",
	failure: "app/MlStudio/UPLOAD_FILE_SUCCESS",
	urlKey: "uploadFile",
	successKey: "uploadFileSuccess",
	failureKey: "uploadFileFailure",
	actionName: "uploadFileHandler",
	actionArguments: ["payload"],
}
export const CREATE_EXPERIMENT = {
	action: 'app/MlStudio/CREATE_EXPERIMENT',
	success: "app/MlStudio/CREATE_EXPERIMENT_SUCCESS",
	failure: "app/MlStudio/CREATE_EXPERIMENT_FAILURE",
	urlKey: "saveExperiment",
	successKey: "createExperimentSuccess",
	failureKey: "createExperimentFailure",
	actionName: "createExperiment",
	actionArguments: ["payload"],
}


export const GET_CONNECTORS_BY_CATEGORY = {
	action: 'app/MlStudio/GET_CONNECTORS_BY_CATEGORY',
	success: "app/MlStudio/GET_CONNECTORS_BY_CATEGORY_SUCCESS",
	failure: "app/MlStudio/GET_CONNECTORS_BY_CATEGORY_FAILURE",
	urlKey: "getConnectorsByCategory",
	successKey: "getConnectorsByCategorySuccess",
	failureKey: "getConnectorsByCategoryFailure",
	actionName: "getConnectorsByCategory",
	actionArguments: ["category"],
}


export const GET_COLUMNS_BY_CONNECTOR = {
	action: 'app/NewAddOrEditMl/GET_COLUMNS_BY_CONNECTOR',
	success: "app/NewAddOrEditMl/GET_COLUMNS_BY_CONNECTOR_SUCCESS",
	failure: "app/NewAddOrEditMl/GET_COLUMNS_BY_CONNECTOR_FAILURE",
	urlKey: "getColumnsByConnector",
	successKey: "getColumnsByConnectorSuccess",
	failureKey: "getColumnsByConnectorFailure",
	actionName: "getColumnsByConnector",
	actionArguments: ["payload"],
}


