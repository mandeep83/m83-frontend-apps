/*
 *
 * MlStudio reducer
 *
 */
import { fromJS } from "immutable";
import * as CONSTANTS from "./constants";

export const initialState = fromJS({});

let allCases = Object.values(CONSTANTS).filter(constant => typeof(constant) == "object")

let getValueObj = (action, index)=>{
  let isSuccessCase = action.type.includes("SUCCESS")
  let key =  isSuccessCase ? allCases[index].successKey : allCases[index].failureKey
  return {
    [key]: action
  }
}

function mlStudioReducer(state = initialState, action) {
  if(action.type.includes("RESET_TO_INITIAL_STATE"))
        return initialState
  for(let i=0; i < allCases.length; i++){
    if(action.type === allCases[i].success || action.type === allCases[i].failure){
      return Object.assign({}, state, getValueObj(action, i));
    }
  }
  return state
}

export default mlStudioReducer;
