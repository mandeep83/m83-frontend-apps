/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/*
 *
 * AddOrEditCustomMenu constants
 *
 */

export const RESET_TO_INITIAL_STATE = "app/AddOrEditCustomMenu/RESET_TO_INITIAL_STATE";

export const GET_PAGES = {
	action: 'app/AddOrEditCustomMenu/GET_PAGES',
	success: "app/AddOrEditCustomMenu/GET_PAGES_SUCCESS",
	failure: "app/AddOrEditCustomMenu/GET_PAGES_FAILURE",
	urlKey: "getAllPages",
	successKey: "getPagesSuccess",
	failureKey: "getPagesFailure",
	actionName: "getPages",
	actionArguments: []
}

export const GET_CUSTOM_MENU_BY_ID = {
	action: 'app/AddOrEditCustomMenu/GET_CUSTOM_MENU_BY_ID',
	success: "app/AddOrEditCustomMenu/GET_CUSTOM_MENU_BY_ID_SUCCESS",
	failure: "app/AddOrEditCustomMenu/GET_CUSTOM_MENU_BY_ID_FAILURE",
	urlKey: "getCustomMenuById",
	successKey: "getCustomMenuByIdSuccess",
	failureKey: "getCustomMenuByIdFailure",
	actionName: "getCustomMenuById",
	actionArguments: ["id"]
}

export const SAVE_CUSTOM_MENU = {
	action: 'app/AddOrEditCustomMenu/SAVE_CUSTOM_MENU',
	success: "app/AddOrEditCustomMenu/SAVE_CUSTOM_MENU_SUCCESS",
	failure: "app/AddOrEditCustomMenu/SAVE_CUSTOM_MENU_FAILURE",
	urlKey: "saveCustomMenu",
	successKey: "saveCustomMenuSuccess",
	failureKey: "saveCustomMenuFailure",
	actionName: "saveCustomMenu",
	actionArguments: ["payload"]
}

export const DELETE_NAV_BY_ID = {
	action: 'app/AddOrEditCustomMenu/DELETE_NAV_BY_ID',
	success: "app/AddOrEditCustomMenu/DELETE_NAV_BY_ID_SUCCESS",
	failure: "app/AddOrEditCustomMenu/DELETE_NAV_BY_ID_FAILURE",
	urlKey: "deleteNavById",
	successKey: "deleteNavByIdSuccess",
	failureKey: "deleteNavByIdFailure",
	actionName: "deleteNavById",
	actionArguments: ["customMenuId", "navId"]
}

export const SAVE_NAV = {
	action: 'app/AddOrEditCustomMenu/SAVE_NAV',
	success: "app/AddOrEditCustomMenu/SAVE_NAV_SUCCESS",
	failure: "app/AddOrEditCustomMenu/SAVE_NAV_FAILURE",
	urlKey: "saveNavCustomMenu",
	successKey: "saveNavCustomMenuSuccess",
	failureKey: "saveNavCustomMenuFailure",
	actionName: "saveNavCustomMenu",
	actionArguments: ["payload", "id", "isParent"]
}

export const UPDATE_NAV = {
	action: 'app/AddOrEditCustomMenu/UPDATE_NAV',
	success: "app/AddOrEditCustomMenu/UPDATE_NAV_SUCCESS",
	failure: "app/AddOrEditCustomMenu/UPDATE_NAV_FAILURE",
	urlKey: "updateNavCustomMenu",
	successKey: "updateNavCustomMenuSuccess",
	failureKey: "updateNavCustomMenuFailure",
	actionName: "updateNavCustomMenu",
	actionArguments: ["payload", "id", "isParent"]
}

export const UPDATE_CUSTOM_MENU = {
	action: 'app/AddOrEditCustomMenu/UPDATE_CUSTOM_MENU',
	success: "app/AddOrEditCustomMenu/UPDATE_CUSTOM_MENU_SUCCESS",
	failure: "app/AddOrEditCustomMenu/UPDATE_CUSTOM_MENU_FAILURE",
	urlKey: "updateCustomMenu",
	successKey: "updateCustomMenuSuccess",
	failureKey: "updateCustomMenuFailure",
	actionName: "updateCustomMenu",
	actionArguments: ["payload", "id"]
}