/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * AddOrEditCustomMenu
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { FormattedMessage } from "react-intl";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import reducer from "./reducer";
import saga from "./saga";
import { allSelectors as SELECTORS } from "./selectors";
import { allActions as ACTIONS } from './actions'
import { getNotificationObj, isNavAssigned } from "../../commonUtils";
import produce from "immer"
import Loader from '../../components/Loader/Loadable';
import cloneDeep from "lodash/cloneDeep";
import NotificationModal from '../../components/NotificationModal/Loadable';
import ConfirmModel from '../../components/ConfirmModel/Loadable'
import FontAwesomeDisplayList from "../../components/FontAwesomeDisplayList";

/* eslint-disable react/prefer-stateless-function */
export class AddOrEditCustomMenu extends React.Component {
    state = {
        isFetching: true,
        payload: {
            name: '',
            version: '',
            description: '',
            customMenus: []
        },
        assignMenu: [],
        totalPages: [],
        searchPageName: '',
        deleteNavDetails: {},
        errorMessageCode: 0
    }

    componentDidMount() {
        this.props.getPages()
    }

    static getDerivedStateFromProps(nextProps, state) {
        if (nextProps.getPagesSuccess) {
            if (nextProps.match.params.id) {
                nextProps.getCustomMenuById(nextProps.match.params.id)
            }
            return {
                totalPages: nextProps.getPagesSuccess.response,
                isFetching: nextProps.match.params.id
            }
        }

        if (nextProps.getCustomMenuByIdSuccess) {
            let payload = nextProps.getCustomMenuByIdSuccess.response,
                assignMenu = cloneDeep(payload.customMenus),
                totalPages = cloneDeep(state.totalPages);
            totalPages.map(page => {
                page.added = false
                assignMenu.map(menu => {
                    if (menu.pageId === page.id) {
                        menu.pageName = page.name
                        page.added = true
                    }
                    menu.children.map(child => {
                        if (child.pageId === page.id) {
                            child.pageName = page.name
                            page.added = true
                        }
                    })
                })
            });
            payload.customMenus = [];
            return {
                payload,
                assignMenu,
                isFetching: false,
                totalPages
            }
        }

        if (nextProps.deleteNavByIdSuccess) {
            let assignMenu = cloneDeep(state.assignMenu),
                totalPages = cloneDeep(state.totalPages);
            nextProps.fetchSideNav()
            assignMenu = assignMenu.filter(menu => {
                if (menu.menuId === nextProps.deleteNavByIdSuccess.response.navId) {
                    return false
                }
                menu.children = menu.children.filter(child => child.menuId !== nextProps.deleteNavByIdSuccess.response.navId)
                return true
            })
            totalPages.map(page => {
                page.added = false
                assignMenu.map(menu => {
                    if (menu.pageId === page.id) {
                        menu.pageName = page.name
                        page.added = true
                    }
                    menu.children.map(child => {
                        if (child.pageId === page.id) {
                            child.pageName = page.name
                            page.added = true
                        }
                    })
                })
            });
            return {
                isOpen: true,
                message2: nextProps.deleteNavByIdSuccess.response.message,
                modalType: "success",
                assignMenu,
                isFetching: false,
                totalPages
            }
        }

        if (nextProps.saveCustomMenuSuccess) {
            nextProps.fetchSideNav()
            return {
                isOpen: true,
                message2: nextProps.saveCustomMenuSuccess.response.message,
                modalType: "success",
                saveSuccess: true
            }
        }

        if (nextProps.updateCustomMenuSuccess) {
            nextProps.fetchSideNav()
            return {
                isOpen: true,
                message2: nextProps.updateCustomMenuSuccess.response,
                modalType: "success",
                saveSuccess: true
            }
        }

        if (nextProps.saveNavCustomMenuSuccess) {
            nextProps.fetchSideNav()
            nextProps.getCustomMenuById(nextProps.match.params.id)
            return {
                isOpen: true,
                message2: nextProps.saveNavCustomMenuSuccess.response,
                modalType: "success",
                // saveSuccess: true,
                // isFetching: false,
            }
        }

        if (nextProps.updateNavCustomMenuSuccess) {
            nextProps.getCustomMenuById(nextProps.match.params.id)
            nextProps.fetchSideNav()
            return {
                isOpen: true,
                message2: nextProps.updateNavCustomMenuSuccess.response,
                modalType: "success",
                // saveSuccess: true,
                // isFetching: false,
            }
        }

        if (Object.keys(SELECTORS).filter(prop => prop.toLowerCase().includes("success")).some(prop => nextProps[prop])) {
            let propName = Object.keys(SELECTORS).find(props => nextProps[props]);
            return { ...getNotificationObj(nextProps[propName].response.message, "success") }
        }
        if (Object.keys(SELECTORS).filter(prop => prop.toLowerCase().includes("failure")).some(prop => nextProps[prop])) {
            let propName = Object.keys(SELECTORS).find(props => nextProps[props]);
            return { ...getNotificationObj(nextProps[propName].error, "error"), isFetching: false }
        }
        return null
    }

    componentDidUpdate(prevProps, prevState) {
        if (Object.keys(SELECTORS).some(prop => this.props[prop])) {
            this.props.resetToInitialState()
        }
    }

    basicInfoChangeHandler = ({ currentTarget }) => {
        let payload = produce(this.state.payload, draftState => {
            if (currentTarget.id == "name") {
                if (/^[a-zA-Z0-9-_() ]+$/.test(currentTarget.value) || /^$/.test(currentTarget.value))
                    draftState[currentTarget.id] = currentTarget.value
            } else if (currentTarget.id == "version") {
                if (/^[A-Za-z0-9_.#-]+$/.test(currentTarget.value) || /^$/.test(currentTarget.value))
                    draftState[currentTarget.id] = currentTarget.value
            } else {
                draftState[currentTarget.id] = currentTarget.value
            }
        })
        this.setState({
            payload,
            isBasicInfoChanged: Boolean(this.props.match.params.id)
        })
    }

    menuChangeHandler = ({ currentTarget }, parentIndex, childIndex, isParent) => {
        let errorMessageCode = this.state.errorMessageCode,
            assignMenu = produce(this.state.assignMenu, draftState => {
                if (currentTarget.name == "menuName") {
                    if (currentTarget.value) {
                        errorMessageCode = 0
                    }
                    if (isParent) {
                        draftState[parentIndex][currentTarget.name] = currentTarget.value
                        draftState[parentIndex].isEdit = Boolean(draftState[parentIndex].menuId)
                    } else {
                        draftState[parentIndex].children[childIndex][currentTarget.name] = currentTarget.value
                        draftState[parentIndex].children[childIndex].isEdit = Boolean(draftState[parentIndex].children[childIndex].menuId)
                    }
                }
                else {
                    if (isParent) {
                        draftState[parentIndex].enable = currentTarget.checked
                        draftState[parentIndex].isEdit = Boolean(draftState[parentIndex].menuId)
                    } else {
                        draftState[parentIndex].children[childIndex].enable = currentTarget.checked
                        draftState[parentIndex].children[childIndex].isEdit = Boolean(draftState[parentIndex].children[childIndex].menuId)
                    }
                }
            })
        this.setState({
            assignMenu,
            errorMessageCode
        })
    }

    cancelClicked = () => {
        this.setState({
            deleteNavDetails: {},
            confirmState: false
        })
    }

    onCloseHandler = () => {
        this.setState({
            isOpen: false,
            message2: '',
            modalType: "",
        }, () => { this.state.saveSuccess && this.props.history.push(`/applications`) })
    }

    allowDrop = ev => {
        ev.preventDefault();
    }

    drag = (event, selectedPage) => {
        this.setState({
            selectedPage
        })
    }

    drop = (ev, parentIndex, childIndex, isParent) => {
        let assignMenu,
            totalPages = cloneDeep(this.state.totalPages);
        assignMenu = produce(this.state.assignMenu, draftState => {
            if (isParent) {
                draftState[parentIndex].pageId = this.state.selectedPage.id
                draftState[parentIndex].pageName = this.state.selectedPage.name
                draftState[parentIndex].url = "dashboard/" + this.state.selectedPage.id
                draftState[parentIndex].isEdit = Boolean(draftState[parentIndex].menuId)
            } else {
                draftState[parentIndex].children[childIndex].pageId = this.state.selectedPage.id
                draftState[parentIndex].children[childIndex].pageName = this.state.selectedPage.name
                draftState[parentIndex].children[childIndex].url = "dashboard/" + this.state.selectedPage.id
                draftState[parentIndex].children[childIndex].isEdit = Boolean(draftState[parentIndex].children[childIndex].menuId)
            }
        })

        totalPages.map(page => {
            page.added = false
            assignMenu.map(menu => {
                if (menu.pageId === page.id) {
                    page.added = true
                }
                menu.children.map(child => {
                    if (child.pageId === page.id) {
                        page.added = true
                    }
                })
            })
        })
        this.setState({
            assignMenu,
            totalPages,
            selectedPage: {},
            errorMessageCode: 0
        })
    }

    addChildMenu = (parentIndex) => {
        let assignMenu,
            totalPages = cloneDeep(this.state.totalPages);
        assignMenu = produce(this.state.assignMenu, draftState => {
            draftState[parentIndex].url = ""
            draftState[parentIndex].pageId = ""
            draftState[parentIndex].pageName = ""
            draftState[parentIndex].children.push({
                "menuName": "",
                "url": "dashboard/",
                "fontAwesomeIconClass": "window",
                "newTab": false,
                "enable": true,
                "pageId": "",
                "pageName": "",
                "children": [],
                "newMenuAdded": Boolean(this.props.match.params.id)
            })
        })
        totalPages.map(page => {
            page.added = false
            assignMenu.map(menu => {
                if (menu.pageId === page.id) {
                    page.added = true
                }
                menu.children.map(child => {
                    if (child.pageId === page.id) {
                        page.added = true
                    }
                })
            })
        })
        this.setState({
            assignMenu,
            totalPages,
            errorMessageCode: 0
        })
    }

    addParentMenu = () => {
        let assignMenu = produce(this.state.assignMenu, draftState => {
            draftState.push({
                "menuName": "",
                "url": "dashboard/",
                "fontAwesomeIconClass": "window",
                "newTab": false,
                "enable": true,
                "pageId": "",
                "pageName": "",
                "children": [],
                "newMenuAdded": Boolean(this.props.match.params.id)
            })
        })
        this.setState({
            assignMenu,
            errorMessageCode: 0
        })
    }

    saveCustomMenu = (e) => {
        e.preventDefault();
        let payload = { ...this.state.payload }
        if (this.menuValidation()) {
            return
        }
        payload.name = payload.name.trim()
        payload.customMenus = cloneDeep(this.state.assignMenu).map(nav => {
            nav.menuName = nav.menuName.trim()
            nav.children = nav.children.map(child => {
                child.menuName = child.menuName.trim()
                return child
            })
            return nav
        })
        this.setState({
            isFetching: true
        }, () => this.props.saveCustomMenu(payload))
    }

    updateCustomMenu = (e) => {
        e.preventDefault();
        let payload = {
            name: this.state.payload.name.trim(),
            version: this.state.payload.version,
            description: this.state.payload.description
        }
        this.setState({
            isFetching: true
        }, () => this.props.updateCustomMenu(payload, this.props.match.params.id))
    }

    resetPage = (parentIndex, childIndex, isParent) => {
        let assignMenu,
            totalPages = cloneDeep(this.state.totalPages);
        assignMenu = produce(this.state.assignMenu, draftState => {
            if (isParent) {
                draftState[parentIndex].pageId = ""
                draftState[parentIndex].pageName = ""
                draftState[parentIndex].url = "dashboard/"
                draftState[parentIndex].newMenuAdded = Boolean(this.props.match.params.id)
            } else {
                draftState[parentIndex].children[childIndex].pageId = ""
                draftState[parentIndex].children[childIndex].pageName = ""
                draftState[parentIndex].children[childIndex].url = "dashboard/"
                draftState[parentIndex].children[childIndex].newMenuAdded = Boolean(this.props.match.params.id)
            }
        })

        totalPages.map(page => {
            page.added = false
            assignMenu.map(menu => {
                if (menu.pageId === page.id) {
                    page.added = true
                }
                menu.children.map(child => {
                    if (child.pageId === page.id) {
                        page.added = true
                    }
                })
            })
        })
        this.setState({
            assignMenu,
            totalPages,
            errorMessageCode: 0
        })
    }

    deleteMenuItem = (parentIndex, childIndex, isParent) => {
        let assignMenu,
            totalPages = cloneDeep(this.state.totalPages);
        assignMenu = produce(this.state.assignMenu, draftState => {
            if (isParent) {
                draftState.splice(parentIndex, 1)
            } else {
                draftState[parentIndex].children.splice(childIndex, 1)
            }
        })
        if (assignMenu.length == 0) {
            assignMenu = [{
                "menuName": "",
                "url": "",
                "fontAwesomeIconClass": "window",
                "newTab": false,
                "pageId": "",
                "pageName": "",
                "children": []
            }]
        }
        totalPages.map(page => {
            page.added = false
            assignMenu.map(menu => {
                if (menu.pageId === page.id) {
                    page.added = true
                }
                menu.children.map(child => {
                    if (child.pageId === page.id) {
                        page.added = true
                    }
                })
            })
        })
        this.setState({
            assignMenu,
            totalPages,
            errorMessageCode: 0
        })
    }

    deleteMenuItemApiCall = (navMenu) => {
        this.setState({
            confirmState: true,
            deleteNavDetails: navMenu
        })
    }

    saveNav = (navPayload, isParent, parentId) => {
        let payload = cloneDeep(navPayload);
        if (!isParent) {
            payload.parentId = parentId
        }
        payload.menuName = payload.menuName.trim()
        payload.children = payload.children.map(nav => {
            nav.menuName = nav.menuName.trim()
            return nav
        })
        if (this.menuValidation()) {
            return
        }
        this.setState({
            isFetching: true
        }, () => this.props.saveNavCustomMenu(payload, this.props.match.params.id, isParent))
    }

    editMenu = (navPayload, isParent, parentId) => {
        let payload = cloneDeep(navPayload);
        if (!isParent) {
            payload.parentId = parentId
        }
        payload.menuName = payload.menuName.trim()
        if (this.menuValidation()) {
            return
        }
        this.setState({
            isFetching: true
        }, () => this.props.updateNavCustomMenu(payload, this.props.match.params.id, isParent))
    }

    handleMenuIconBox = (parentIndex, childIndex, isParent) => {
        let assignMenu = cloneDeep(this.state.assignMenu);
        assignMenu.map((menuObj, menuParentIndex) => {
            if (isParent) {
                if (menuParentIndex === parentIndex) {
                    menuObj.showIconBox = menuObj.showIconBox ? !menuObj.showIconBox : true;
                } else {
                    menuObj.showIconBox = false;
                }
                menuObj.children.map((menuChildObj) => {
                    menuChildObj.showIconBox = false;
                });
            } else {
                menuObj.showIconBox = false;
                if (parentIndex === menuParentIndex) {
                    menuObj.children.map((menuChildObj, menuChildIndex) => {
                        if (childIndex === menuChildIndex) {
                            menuChildObj.showIconBox = menuChildObj.showIconBox ? !menuChildObj.showIconBox : true;
                        } else {
                            menuChildObj.showIconBox = false;
                        }
                    });
                }

            }
        })

        this.setState({
            assignMenu,
        })
    }

    handleChange = (icon, isParent) => {
        let value = icon.split('fa-')[1];

        let assignMenu = cloneDeep(this.state.assignMenu);
        assignMenu.map((menuObj, menuIndex) => {
            if (isParent) {
                if (menuObj.showIconBox) {
                    menuObj.fontAwesomeIconClass = value;
                    menuObj.showIconBox = false;
                    menuObj.isEdit = Boolean(this.props.match.params.id);
                }
            } else {
                menuObj.children.map((menuChildObj, menuChildIndex) => {
                    if (menuChildObj.showIconBox) {
                        menuChildObj.fontAwesomeIconClass = value;
                        menuChildObj.showIconBox = false;
                        menuChildObj.isEdit = Boolean(this.props.match.params.id);
                    }
                });
            }
        })

        this.setState({
            assignMenu,
        })
    }

    menuValidation = () => {
        let blankNameMenu = this.findBlankNameMenu(this.state.assignMenu);

        if (blankNameMenu != -1) {
            this.setState({
                errorMessageCode: 1 //page assigned to menu but no menu name
            })
            return true;
        }
        return false
    }

    findBlankNameMenu = (menu) => {
        let index = menu.findIndex(nav => {
            if (nav.children.length) {
                let x;
                if (!nav.menuName.trim()) {
                    return true
                }
                nav.children.map(el => {
                    if (!el.menuName.trim() && el.pageId) {
                        x = true
                    }
                    else if (el.menuName.trim() && !el.pageId) {
                        x = true
                    }
                    else if (!el.menuName.trim() && !el.pageId) {
                        x = true
                    }
                })
                return x
            }
            else {
                if (!nav.menuName.trim() && nav.pageId) {
                    return true
                }
                else if (nav.menuName.trim() && !nav.pageId) {
                    return true
                }
                else if (!nav.menuName.trim() && !nav.pageId) {
                    return true
                }
            }
        })
        return index
    }

    render() {
        let availablePageList = this.state.totalPages.filter(page => {
            return !page.added
        }),
            availablePageCount = availablePageList.length,
            filteredPageList = (this.state.searchPageName ? availablePageList.filter(page => (page.name.toLowerCase()).includes(this.state.searchPageName.toLowerCase())) : availablePageList);
        let assignMenuPagesCount = 0;
        this.state.assignMenu.map(menu => {
            if (menu.children.length) {
                menu.children.map(child => {
                    if (!child.pageId) {
                        assignMenuPagesCount++
                    }
                })
            }
            else {
                if (!menu.pageId) {
                    assignMenuPagesCount++
                }
            }
        });

        return (
            <React.Fragment>
                <Helmet>
                    <title>AddOrEditCustomMenu</title>
                    <meta name="description" content="Description of AddOrEditCustomMenu" />
                </Helmet>

                <header className="content-header d-flex">
                    <div className="flex-60">
                        <div className="d-flex">
                            <h6 className="previous" onClick={() => this.props.history.push(`/applications`)}>Applications</h6>
                            <h6 className="active">{this.props.match.params.id ? this.state.payload.name : "Add New"}</h6>
                        </div>
                    </div>
                    <div className="flex-40 text-right">
                        <div className="content-header-group">
                            <button className="btn btn-light" data-tooltip data-tooltip-text="Back" data-tooltip-place="bottom" onClick={() => this.props.history.push(`/applications`)}><i className="fas fa-angle-left"></i></button>
                        </div>
                    </div>
                </header>

                {this.state.isFetching ?
                    <Loader /> :
                    <div className="content-body">
                        <div className="form-content-wrapper form-content-wrapper-left">
                            <div className="d-flex">
                                <div className="flex-70 pd-r-7">
                                    <div className="form-content-box mb-0">
                                        <div className="form-content-header">
                                            <p>Add Menu
                                                {this.state.errorMessageCode ? <span className="form-content-error">Please enter a valid application menu.</span> : null}
                                            </p>
                                        </div>

                                        <div className="form-content-body p-3">
                                            <div className="application-wrapper">
                                                <div className="application-menu-wrapper">
                                                    {this.state.assignMenu.length > 0 ?
                                                        <React.Fragment>
                                                            <ul className="list-style-none mb-4">
                                                                {this.state.assignMenu.map((menu, parentIndex) => {
                                                                    return (
                                                                        <li key={parentIndex}>
                                                                            <div className="d-flex application-menu">
                                                                                <div className="application-menu-icon" onClick={() => { this.handleMenuIconBox(parentIndex, null, true) }}>
                                                                                    <i className={menu.fontAwesomeIconClass ? `fad fa-${menu.fontAwesomeIconClass}` : 'fad fa-window'}></i>
                                                                                </div>
                                                                                {menu.showIconBox &&
                                                                                    <div className="application-menu-icon-select">
                                                                                        <FontAwesomeDisplayList filterOptions={"fas"} changeHandlerForSelect={(value) => this.handleChange(value, true)} />
                                                                                    </div>
                                                                                }

                                                                                <div className="flex-45 form-group pd-r-5">
                                                                                    <input type="text" name="menuName" className="form-control" value={menu.menuName} onChange={({ currentTarget }) => this.menuChangeHandler({ currentTarget }, parentIndex, null, true)} placeholder="Menu Name" />
                                                                                </div>
                                                                                {menu.children && menu.children.length === 0 &&
                                                                                    <div className="flex-45 form-group pd-l-5">
                                                                                        <input type="text" name="dragdrop"
                                                                                            className={menu.pageName ? "form-control active" : "form-control"}
                                                                                            value={menu.pageName}
                                                                                            onDrop={(event) => { this.drop(event, parentIndex, null, true) }}
                                                                                            onDragOver={(event) => { this.allowDrop(event) }}
                                                                                            onChange={() => { return false }}
                                                                                            placeholder="Drag and drop pages here !" />
                                                                                    </div>
                                                                                }
                                                                                <div className="form-group button-group">
                                                                                    {!(menu.newMenuAdded || menu.isEdit) ?
                                                                                        <div className="toggle-switch-wrapper d-inline-block">
                                                                                            <label className="toggle-switch ml-0 mr-2" data-tooltip data-tooltip-text="Enable Menu" data-tooltip-place="bottom">
                                                                                                <input type="checkbox" name="enable" checked={menu.enable} onChange={({ currentTarget }) => this.menuChangeHandler({ currentTarget }, parentIndex, null, true)} />
                                                                                                <span className="toggle-switch-slider"></span>
                                                                                            </label>
                                                                                        </div> : null}
                                                                                    {(menu.newMenuAdded || menu.isEdit) &&
                                                                                        <button type='button' className="btn-transparent btn-transparent-green" disabled={(!menu.menuName) || (menu.children.length == 0 && !menu.pageId)} onClick={() => menu.menuId ? this.editMenu(menu, true) : this.saveNav(menu, true)} data-tooltip data-tooltip-text="Save Menu" data-tooltip-place="bottom">
                                                                                            <i className="far fa-check"></i>
                                                                                        </button>
                                                                                    }
                                                                                    {menu.children && menu.children.length === 0 &&
                                                                                        <button type='button' className="btn-transparent btn-transparent-cyan" onClick={() => this.resetPage(parentIndex, null, true)} data-tooltip data-tooltip-text="Reset" data-tooltip-place="bottom">
                                                                                            <i className="far fa-redo-alt"></i>
                                                                                        </button>
                                                                                    }
                                                                                    <button type='button' className="btn-transparent btn-transparent-blue" disabled={(menu.menuId && menu.children.length == 0) || assignMenuPagesCount >= availablePageCount} onClick={() => this.addChildMenu(parentIndex)} data-tooltip data-tooltip-text="Add Child" data-tooltip-place="bottom">
                                                                                        <i className="far fa-plus"></i>
                                                                                    </button>
                                                                                    <button type='button' className="btn-transparent btn-transparent-red" onClick={() => menu.menuId ? this.deleteMenuItemApiCall(menu) : this.deleteMenuItem(parentIndex, null, true)} data-tooltip data-tooltip-text="Delete" data-tooltip-place="bottom">
                                                                                        <i className="far fa-trash-alt"></i>
                                                                                    </button>
                                                                                </div>
                                                                            </div>

                                                                            {menu.children.length ?
                                                                                <ul className="list-style-none">
                                                                                    {menu.children.map((child, childIndex) => {
                                                                                        return (
                                                                                            <li key={childIndex}>
                                                                                                <div className="d-flex application-menu application-sub-menu">
                                                                                                    <div className="application-menu-icon" onClick={() => { this.handleMenuIconBox(parentIndex, childIndex, false) }}>
                                                                                                        <i className={child.fontAwesomeIconClass ? `fad fa-${child.fontAwesomeIconClass}` : 'fad fa-window'}></i>
                                                                                                    </div>
                                                                                                    {child.showIconBox &&
                                                                                                        <div className="application-menu-icon-select">
                                                                                                            <FontAwesomeDisplayList filterOptions={"fas"} changeHandlerForSelect={(value) => this.handleChange(value, false)} />
                                                                                                        </div>
                                                                                                    }
                                                                                                    <div className="flex-45 form-group pd-r-5">
                                                                                                        <input type="text" name="menuName" className="form-control" value={child.menuName} onChange={({ currentTarget }) => this.menuChangeHandler({ currentTarget }, parentIndex, childIndex, false)} placeholder="Menu Name" />
                                                                                                    </div>
                                                                                                    <div className="flex-45 form-group pd-l-5">
                                                                                                        <input type="text"
                                                                                                            className={child.pageName ? "form-control active" : "form-control"}
                                                                                                            value={child.pageName}
                                                                                                            onDrop={(event) => { this.drop(event, parentIndex, childIndex, false) }}
                                                                                                            onDragOver={(event) => { this.allowDrop(event) }}
                                                                                                            onChange={() => { return false }}
                                                                                                            placeholder="Drag and drop pages here !" />
                                                                                                    </div>
                                                                                                    <div className="form-group button-group">
                                                                                                        {!((child.newMenuAdded || child.isEdit) && (!menu.newMenuAdded)) &&
                                                                                                            <div className="toggle-switch-wrapper d-inline-block">
                                                                                                                <label className="toggle-switch ml-0 mr-2" data-tooltip data-tooltip-text="Enable Menu" data-tooltip-place="bottom">
                                                                                                                    <input type="checkbox" name="enable" checked={child.enable} onChange={({ currentTarget }) => this.menuChangeHandler({ currentTarget }, parentIndex, childIndex, false)} />
                                                                                                                    <span className="toggle-switch-slider"></span>
                                                                                                                </label>
                                                                                                            </div>}
                                                                                                        {((child.newMenuAdded || child.isEdit) && (!menu.newMenuAdded)) &&
                                                                                                            <button type='button' className="btn-transparent btn-transparent-green" disabled={(!child.menuName) || (!child.pageId)} onClick={() => child.menuId ? this.editMenu(child, false, menu.menuId) : this.saveNav(child, false, menu.menuId)} data-tooltip data-tooltip-text="Save Nav" data-tooltip-place="bottom">
                                                                                                                <i className="far fa-check"></i>
                                                                                                            </button>}
                                                                                                        <button type='button' className="btn-transparent btn-transparent-cyan" data-tooltip data-tooltip-text="Reset" onClick={() => this.resetPage(parentIndex, childIndex, false)} data-tooltip-place="bottom">
                                                                                                            <i className="far fa-redo-alt"></i>
                                                                                                        </button>
                                                                                                        <button type='button' className="btn-transparent btn-transparent-red" data-tooltip data-tooltip-text="Delete" onClick={() => child.menuId ? this.deleteMenuItemApiCall(child) : this.deleteMenuItem(parentIndex, childIndex, false)} data-tooltip-place="bottom">
                                                                                                            <i className="far fa-trash-alt"></i>
                                                                                                        </button>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </li>)
                                                                                    })}
                                                                                </ul> : null
                                                                            }
                                                                        </li>
                                                                    )
                                                                })
                                                                }
                                                            </ul>
                                                            <div className="application-menu-button">
                                                                <button type="button" className="btn btn-link" disabled={this.state.assignMenu.some(nav => (nav.newMenuAdded || nav.isEdit) || (nav.children.length && nav.children.some(child => child.newMenuAdded || child.isEdit))) || assignMenuPagesCount >= availablePageCount} onClick={this.addParentMenu}>
                                                                    <i className="far fa-plus"></i>Add New
                                                                </button>
                                                            </div>
                                                        </React.Fragment>
                                                        :
                                                        <div className="inner-message-wrapper h-100">
                                                            <div className="inner-message-content">
                                                                <img src="https://content.iot83.com/m83/misc/addCustomMenu.png" />
                                                                <h6>You have not created any menus yet.</h6>
                                                                <button type="button" className="btn btn-link" onClick={this.addParentMenu}>
                                                                    <i className="far fa-plus f-12 text-primary"></i>Add New
                                                                </button>
                                                            </div>
                                                        </div>
                                                    }
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="flex-30 pd-l-7">
                                    <div className="form-content-box mb-0">
                                        <div className="form-content-header">
                                            <p>Select Pages</p>
                                        </div>

                                        <div className="form-content-body p-3">
                                            <div className="application-wrapper">
                                                <div className="application-search-wrapper">
                                                    <div className="form-group">
                                                        <input type="text" className="form-control" value={this.state.searchPageName} onChange={({ currentTarget }) => {
                                                            this.setState({
                                                                searchPageName: currentTarget.value
                                                            })
                                                        }} placeholder="Search Pages..." />
                                                        <i className="far fa-search application-search-icon"></i>
                                                        <button className="btn" onClick={() => { this.setState({ searchPageName: '' }) }}><i className="far fa-times"></i></button>
                                                    </div>
                                                    {this.state.totalPages.length ?
                                                        availablePageList.length ?
                                                            filteredPageList.length ?
                                                                <ul className="list-style-none">
                                                                    {filteredPageList.map(page => {
                                                                        return (
                                                                            <li key={page.id} draggable={true} onDragStart={() => { this.drag(event, page) }}>
                                                                                <p className="m-0">{page.name}</p>
                                                                            </li>
                                                                        )
                                                                    })}
                                                                </ul> :
                                                                <div className="application-search-content">
                                                                    <div>
                                                                        <div className="application-search-image">
                                                                            <img src="https://content.iot83.com/m83/misc/noSearchResult.png" />
                                                                        </div>
                                                                        <p>No data matched your search. Try using other search options.</p>
                                                                    </div>
                                                                </div> :
                                                            <div className="application-search-content">
                                                                <div>
                                                                    <div className="application-search-image">
                                                                        <img src="https://content.iot83.com/m83/misc/addPage.png" />
                                                                    </div>
                                                                    <p>You have used all pages.</p>
                                                                    {isNavAssigned('pages') &&
                                                                        <button className="btn btn-link" type="button" onClick={() => { this.props.history.push('/createOrEditPage') }}>
                                                                            <i className="far fa-plus"></i>Add new
                                                                        </button>
                                                                    }
                                                                </div>
                                                            </div> :
                                                        <div className="application-search-content">
                                                            <div>
                                                                <div className="application-search-image">
                                                                    <img src="https://content.iot83.com/m83/misc/addPage.png" />
                                                                </div>
                                                                <p>You have not created any pages yet.</p>
                                                                <button className="btn btn-link" type="button" onClick={() => { this.props.history.push('/createOrEditPage') }}>
                                                                    <i className="far fa-plus"></i>Add new
                                                                </button>
                                                            </div>
                                                        </div>
                                                    }
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div className="form-info-wrapper form-info-wrapper-left">
                            <form onSubmit={this.state.isBasicInfoChanged ? this.updateCustomMenu : this.saveCustomMenu}>
                                <div className="form-info-header">
                                    <h5>Basic Information</h5>
                                </div>
                                <div className="form-info-body form-info-body-adjust">
                                    <div className="alert alert-info note-text">
                                        <p>Application name must contain only letters, numbers, spaces and symbols such as  _ , - , ()</p>
                                    </div>
                                    <div className="form-group">
                                        <label className="form-group-label">Name : <i className="fas fa-asterisk form-group-required"></i></label>
                                        <input type="text" id="name" className="form-control" autoFocus required value={this.state.payload.name} onChange={this.basicInfoChangeHandler} />
                                    </div>
                                    <div className="form-group">
                                        <label className="form-group-label">Description :</label>
                                        <textarea type="text" id="description" rows="3" className="form-control" value={this.state.payload.description} onChange={this.basicInfoChangeHandler} />
                                    </div>
                                    <div className="form-group">
                                        <label className="form-group-label">Version : <i className="fas fa-asterisk form-group-required"></i></label>
                                        <input type="text" id="version" className="form-control" required value={this.state.payload.version} onChange={this.basicInfoChangeHandler} />
                                    </div>

                                </div>
                                <div className="form-info-footer">
                                    <button type='button' className="btn btn-light" onClick={() => { this.props.history.push('/applications') }}>Cancel</button>
                                    <button type='submit' className="btn btn-primary" disabled={this.props.match.params.id ? !this.state.isBasicInfoChanged : false}>{this.props.match.params.id ? "Update" : "Save"}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                }

                {this.state.confirmState &&
                    <ConfirmModel
                        status={"delete"}
                        deleteName={this.state.deleteNavDetails.menuName}
                        confirmClicked={() => {
                            this.setState({
                                isFetching: true,
                                confirmState: false,
                            }, () => this.props.deleteNavById(this.props.match.params.id, this.state.deleteNavDetails.menuId))
                        }}
                        cancelClicked={() => {
                            this.cancelClicked()
                        }}
                    />
                }


                {this.state.isOpen &&
                    <NotificationModal
                        type={this.state.modalType}
                        message2={this.state.message2}
                        onCloseHandler={this.onCloseHandler}
                    />
                }

            </React.Fragment>
        );
    }
}

AddOrEditCustomMenu.propTypes = {
    dispatch: PropTypes.func.isRequired
};

let allSelectors = {}
Object.entries(SELECTORS).map(([key, value]) => {
    allSelectors[key] = value()
})

const mapStateToProps = createStructuredSelector(allSelectors);

function mapDispatchToProps(dispatch) {
    let allActions = { dispatch }
    Object.entries(ACTIONS).map(([key, value]) => {
        allActions[key] = (...args) => dispatch(value(...args))
    })
    return allActions;
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "addOrEditCustomMenu", reducer });
const withSaga = injectSaga({ key: "addOrEditCustomMenu", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(AddOrEditCustomMenu);
