/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/*
 *
 * DeviceGroups constants
 *
 */

export const RESET_TO_INITIAL_STATE = "app/DeviceGroups/RESET_TO_INITIAL_STATE";

export const GET_ALL_DEVICE_GROUPS_BY_CONNECTOR_ID = {
	action: 'app/DeviceGroups/GET_ALL_DEVICE_GROUPS_BY_CONNECTOR_ID',
	success: "app/DeviceGroups/GET_ALL_DEVICE_GROUPS_BY_CONNECTOR_ID_SUCCESS",
	failure: "app/DeviceGroups/GET_ALL_DEVICE_GROUPS_BY_CONNECTOR_ID_FAILURE",
	urlKey: "getDeviceGroups",
	successKey: "getAllDeviceGroupsByConnectorIdSuccess",
	failureKey: "getAllDeviceGroupsByConnectorIdFailure",
	actionName: "getAllDeviceGroupsByConnectorId",
	actionArguments: ["id","invokedFrom"]
}

export const DELETE_GROUP_BY_ID = {
	action: 'app/DeviceGroups/DELETE_GROUP_BY_ID',
	success: "app/DeviceGroups/DELETE_GROUP_BY_ID_SUCCESS",
	failure: "app/DeviceGroups/DELETE_GROUP_BY_ID_FAILURE",
	urlKey: "deleteClusterGroup",
	successKey: "deleteGroupSuccess",
	failureKey: "deleteGroupFailure",
	actionName: "deleteGroup",
	actionArguments: ["id"]
}

export const GET_ALL_CONNECTORS = {
	action: 'app/DeviceGroups/GET_ALL_CONNECTORS',
	success: "app/DeviceGroups/GET_ALL_CONNECTORS_SUCCESS",
	failure: "app/DeviceGroups/GET_ALL_CONNECTORS_FAILURE",
	urlKey: "deviceTypeList",
	successKey: "getAllConnectorsSuccess",
	failureKey: "getAllConnectorsFailure",
	actionName: "getAllConnectors",
	actionArguments: []
}

export const DEVELOPER_QUOTA = {
	action: 'app/DeviceGroups/GET_DEVELOPER_QUOTA',
	success: "app/DeviceGroups/GET_DEVELOPER_QUOTA_SUCCESS",
	failure: "app/DeviceGroups/GET_DEVELOPER_QUOTA_FAILURE",
	urlKey: "getDeveloperQuota",
	successKey: "developerQuotaSuccess",
	failureKey: "developerQuotaFailure",
	actionName: "getDeveloperQuota",
	actionArguments: ["payload"]
}

