/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * DeviceGroups
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { FormattedMessage } from "react-intl";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import reducer from "./reducer";
import saga from "./saga";
import { allSelectors as SELECTORS } from "./selectors";
import { allActions as ACTIONS } from './actions'
import { getNotificationObj, isNavAssigned } from "../../commonUtils";
import ListingTable from "../../components/ListingTable/Loadable";
import AddNewButton from "../../components/AddNewButton/Loadable";
import NoDataFoundMessage from "../../components/NoDataFoundMessage/Loadable"
import Loader from "../../components/Loader";
import ConfirmModel from "../../components/ConfirmModel";
import cloneDeep from "lodash/cloneDeep";
import NotificationModal from '../../components/NotificationModal/Loadable'
import AddOrEditDeviceGroup from "../AddOrEditDeviceGroup/Loadable";

/* eslint-disable react/prefer-stateless-function */
export class DeviceGroups extends React.Component {

    state = {
        listOfGroups: [],
        toggleView:true,
        listOfFilteredGroups: [],
        isFetching: true,
        deleteGroupConfirmation: false,
        viewType: "TABLE_VIEW",
        allDevices: [],
        selectedGroup: {},
        searchTerm: '',
        listOfConnectors: [],
        selectedConnector: {},
        childViewType: this.props.location.state && this.props.location.state.childViewType ? this.props.location.state.childViewType : 'GROUP_DETAILS',
        isFetchingGroups: false,
        totalDeviceGroupsCount: 0,
        remainingDeviceGroupsCount: 0,
        usedDeviceGroupsCount: 0,
    }

    refreshComponent = () => {
        this.setState({
            listOfGroups: [],
            listOfFilteredGroups: [],
            isFetching: true,
            deleteGroupConfirmation: false,
            viewType: "TABLE_VIEW",
            allDevices: [],
            selectedGroup: {},
            searchTerm: '',
            listOfConnectors: [],
            selectedConnector: {},
            childViewType: this.props.location.state && this.props.location.state.childViewType ? this.props.location.state.childViewType : 'GROUP_DETAILS',
            isFetchingGroups: false,
            totalDeviceGroupsCount: 0,
            remainingDeviceGroupsCount: 0,
            usedDeviceGroupsCount: 0,
        }, () => {
            this.props.getAllConnectors()
        })
    }

    componentDidMount() {
        this.props.getAllConnectors();
    }

    static getDerivedStateFromProps(nextProps, state) {
        if (nextProps.developerQuotaSuccess) {
            let totalDeviceGroupsCount = nextProps.developerQuotaSuccess.response.filter(product => product.productName === 'device_groups')[0]['total'],
                remainingDeviceGroupsCount = nextProps.developerQuotaSuccess.response.filter(product => product.productName === 'device_groups')[0]['remaining'],
                usedDeviceGroupsCount = nextProps.developerQuotaSuccess.response.filter(product => product.productName === 'device_groups')[0]['used'];
            return {
                totalDeviceGroupsCount,
                remainingDeviceGroupsCount,
                usedDeviceGroupsCount,
            }

        }
        if (nextProps.developerQuotaFailure) {
            return {
                modalType: "error",
                isOpen: true,
                message2: nextProps.developerQuotaFailure.error,
            }
        }

        if (nextProps.getAllDeviceGroupsByConnectorIdSuccess) {
            let allDevices = [],
                selectedGroup;
            nextProps.getAllDeviceGroupsByConnectorIdSuccess.response.map(el => {
                if (el.default) {
                    el.attachedDevices.map(device => allDevices.push(device))
                }
            })
            selectedGroup = nextProps.match.params.groupId ?
                nextProps.getAllDeviceGroupsByConnectorIdSuccess.response.find(el => el.deviceGroupId === nextProps.match.params.groupId)
                : {
                    deviceGroupName: '',
                    description: '',
                    connectorId: state.selectedConnector.id,
                    attachedDevices: [],
                }
            if (selectedGroup)
                return {
                    listOfGroups: nextProps.getAllDeviceGroupsByConnectorIdSuccess.response,
                    selectedGroup,
                    listOfFilteredGroups: nextProps.getAllDeviceGroupsByConnectorIdSuccess.response,
                    viewType: state.viewType === "ADD_EDIT_VIEW" ?
                        "ADD_EDIT_VIEW" : nextProps.match.params.groupId ?
                            "ADD_EDIT_VIEW" : "TABLE_VIEW",
                    isFetching: false,
                    allDevices,
                    isFetchingGroups: false
                }
            else {
                return {
                    selectedConnector: {},
                    modalType: "error",
                    isOpen: true,
                    message2: "Device details error",
                    isConnectorIdError: true
                }
            }
        }

        if (nextProps.getAllDeviceGroupsByConnectorIdFailure) {
            return {
                selectedConnector: {},
                modalType: "error",
                isOpen: true,
                message2: nextProps.getAllDeviceGroupsByConnectorIdFailure.error,
                isConnectorIdError: true,
                listOfGroups: [],
                listOfFilteredGroups: [],
                // isFetching: nextProps.getAllDeviceGroupsByConnectorIdFailure.error == "DeviceType does not exist With This Id",
                isFetching: false,
                isFetchingGroups: false

            }
        }

        if (nextProps.deleteGroupSuccess) {
            nextProps.getAllDeviceGroupsByConnectorId(state.selectedConnector.id, "PARENT")
            return {
                remainingDeviceGroupsCount: state.remainingDeviceGroupsCount + 1,
                usedDeviceGroupsCount: state.usedDeviceGroupsCount - 1,
                modalType: "success",
                isOpen: true,
                message2: nextProps.deleteGroupSuccess.response.message,
            }
        }

        if (nextProps.getAllConnectorsSuccess) {
            let isFetching = false,
                selectedConnector = {};
            if (nextProps.match.params.connectorId) {
                nextProps.getAllDeviceGroupsByConnectorId(nextProps.match.params.connectorId, "PARENT")
                selectedConnector = nextProps.getAllConnectorsSuccess.response.find(el => el.id === nextProps.match.params.connectorId)
                isFetching = true
            }
            else {
                if (nextProps.getAllConnectorsSuccess.response.length) {
                    selectedConnector = nextProps.getAllConnectorsSuccess.response[0]
                    nextProps.getAllDeviceGroupsByConnectorId(selectedConnector.id, "PARENT")
                    isFetching = true
                }
            }

            if (selectedConnector && selectedConnector.id) {
                let payload = [
                    {
                        dependingProductId: selectedConnector.id,
                        productName: "device_groups"
                    }
                ]
                nextProps.getDeveloperQuota(payload);
            } else {
                let payload = [
                    {
                        dependingProductId: null,
                        productName: "device_groups"
                    }
                ]
                nextProps.getDeveloperQuota(payload);
            }
            return {
                listOfConnectors: nextProps.getAllConnectorsSuccess.response,
                isFetching,
                selectedConnector
            }
        }

        if (Object.keys(SELECTORS).filter(prop => prop.toLowerCase().includes("success")).some(prop => nextProps[prop])) {
            let propName = Object.keys(SELECTORS).find(props => nextProps[props]);
            return { ...getNotificationObj(nextProps[propName].response.message, "success") }
        }
        if (Object.keys(SELECTORS).filter(prop => prop.toLowerCase().includes("failure")).some(prop => nextProps[prop])) {
            let propName = Object.keys(SELECTORS).find(props => nextProps[props]);
            return { ...getNotificationObj(nextProps[propName].error, "error"), isFetching: false }
        }
        return null
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevProps.match.params.groupId !== this.props.match.params.groupId) {
            if (this.props.match.params.groupId) {
                let selectedGroup = this.state.listOfGroups.find(el => el.deviceGroupId === this.props.match.params.groupId)
                if (selectedGroup)
                    this.setState({
                        viewType: 'ADD_EDIT_VIEW',
                        selectedGroup
                    })
                else
                    this.setState({
                        selectedConnector: {},
                        modalType: "error",
                        isOpen: true,
                        message2: "Device details error",
                        isConnectorIdError: true,
                        isFetching: true
                    })
            }
            else
                this.setState({ viewType: 'TABLE_VIEW' })
        }

        if (Object.keys(SELECTORS).some(prop => this.props[prop])) {
            this.props.resetToInitialState()
        }

    }

    onSearchHandler = (e) => {
        let listOfFilteredGroups = JSON.parse(JSON.stringify(this.state.listOfGroups));
        if (e.target.value.length > 0) {
            listOfFilteredGroups = listOfFilteredGroups.filter(item => item.deviceGroupName.toLowerCase().includes(e.target.value.toLowerCase()))
        }
        this.setState({
            listOfFilteredGroups,
            searchTerm: e.target.value
        })
    }

    emptySearchBox = () => {
        this.setState({
            searchTerm: '',
            listOfFilteredGroups: cloneDeep(this.state.listOfGroups)
        })
    }

    getColumns = () => {
        return [
            {
                Header: "Name", width: 20,
                cell: (row) => (
                    <React.Fragment>
                        <div className="list-view-icon"><img src="https://content.iot83.com/m83/misc/device-group-fallback.png"></img></div>
                        <div className="list-view-icon-box">
                            <h6 className="text-theme fw-600">{row.deviceGroupName}</h6>
                            <p>{`${row.description || "-"}`}</p>
                        </div>
                    </React.Fragment>
                )
            },
            {
                Header: "Default ?",
                width: 10,
                cell: row => (
                    <h6 className={row.default ? "text-green" : "text-red"}>{
                        row.default ?
                            <h6 className="text-green"><i className="fad fa-check-circle mr-2"></i>Yes</h6> :
                            <h6 className="text-red"><i className="fad fa-times-circle mr-2"></i>No</h6>}</h6>
                )
            },
            {
                Header: "Device Type",
                width: 10,
                cell: row => (
                    <div className="button-group-link">
                        <button className="btn btn-link text-truncate w-100" onClick={() => { isNavAssigned('deviceTypes') && this.props.history.push(`/addOrEditDeviceType/${row.connectorId}`) }}>{this.state.selectedConnector.name}</button>
                    </div>
                )
            },
            {
                Header: "Devices / Tags", width: 15, accessor: "deviceCount",
                cell: (row) => (
                    <React.Fragment>
                        <span className="alert alert-primary list-view-badge list-view-badge-link" onClick={() => isNavAssigned('devices') && this.props.history.push(`/devices/${this.state.selectedConnector.id}/${row.deviceGroupId}`)}>{row.attachedDevices.length}</span>
                        <span className="mr-l-10 mr-r-10">/</span>
                        <span className="alert alert-success list-view-badge">{row.tagCount}</span>
                    </React.Fragment>
                )
            },
            { Header: "Created", width: 15, accessor: "createdAt" },
            { Header: "Updated", width: 15, accessor: "updatedAt" },
            {
                Header: "Actions", width: 15,
                cell: (row) => (
                    <div className="button-group">
                        <button className="btn-transparent btn-transparent-green" /*disabled={!row.attachedDevices.length}*/ data-tooltip data-tooltip-text="Tags" data-tooltip-place="bottom" onClick={() => {
                            let selectedGroup = row
                            this.props.history.push(`/deviceGroups/${this.state.selectedConnector.id}/${row.deviceGroupId}`)
                            this.setState({
                                viewType: "ADD_EDIT_VIEW",
                                selectedGroup,
                                childViewType: 'GROUP_TAGVIEW'
                            })
                        }}
                        >
                            <i className="far fa-tag"></i>
                        </button>
                        <button className="btn-transparent btn-transparent-blue" data-tooltip data-tooltip-text="Edit" data-tooltip-place="bottom" onClick={() => {
                            let selectedGroup = row
                            this.props.history.push(`/deviceGroups/${this.state.selectedConnector.id}/${row.deviceGroupId}`)
                            this.setState({
                                viewType: "ADD_EDIT_VIEW",
                                selectedGroup,
                                childViewType: 'GROUP_DETAILS'
                            })
                        }}
                        >
                            <i className="far fa-pencil"></i>
                        </button>
                        <button className="btn-transparent btn-transparent-red" data-tooltip data-tooltip-text="Delete" data-tooltip-place="bottom" disabled={row.default} onClick={() => {
                            this.setState({
                                deleteGroupConfirmation: true,
                                selectedDeviceGroupId: row.deviceGroupId,
                                deviceGroupToBeDeletedName: row.deviceGroupName
                            })
                        }}
                            disabled={row.default}>
                            <i className="far fa-trash-alt"></i>
                        </button>
                    </div>
                )
            },
        ]
    }

    onCloseHandler = () => {
        this.setState({
            isOpen: false,
            message2: '',
            modalType: '',
        }, () => {
            if (this.state.isConnectorIdError) {
                if (this.props.match.params.connectorId) {
                    this.props.getAllConnectors();
                    this.props.history.push("/deviceGroups");
                }
            }
        });
    }

    connectorChangeHandler = ({ currentTarget }) => {
        let selectedConnector = this.state.listOfConnectors.find(el => el.id === currentTarget.value),
            payload = [
                {
                    dependingProductId: selectedConnector.id,
                    productName: "device_groups"
                }
            ];
        this.setState({
            isFetchingGroups: true,
            selectedConnector,
            totalDeviceGroupsCount: 0,
            remainingDeviceGroupsCount: 0,
            usedDeviceGroupsCount: 0,
        }, () => {
            this.props.getDeveloperQuota(payload);
            this.props.getAllDeviceGroupsByConnectorId(selectedConnector.id, "PARENT")
            this.props.history.push(`/deviceGroups`)
        })
    }

    render() {
        if (this.state.viewType === "ADD_EDIT_VIEW")
            return <AddOrEditDeviceGroup {...this.props}
                data={{
                    groupPayload: cloneDeep(this.state.selectedGroup),
                    allDevices: this.state.allDevices,
                    selectedConnector: this.state.selectedConnector,
                    listOfConnectors: this.state.listOfConnectors
                }}
                changeViewToList={(isReload, increaseCount) => {
                    this.setState((state) => ({
                        viewType: "TABLE_VIEW",
                        isFetching: isReload,
                        remainingDeviceGroupsCount: increaseCount ?
                            this.state.remainingDeviceGroupsCount - 1 : state.remainingDeviceGroupsCount,
                        usedDeviceGroupsCount: increaseCount ?
                            this.state.usedDeviceGroupsCount + 1 : state.usedDeviceGroupsCount,
                    }), () => {
                        if (isReload) {
                            this.props.getAllDeviceGroupsByConnectorId(this.state.selectedConnector.id, "PARENT");
                        }
                        this.props.history.push(`/deviceGroups`)
                    })
                }}
                childViewType={this.state.childViewType}
                changeSelectedConnector={(selectedConnector) => this.setState({ selectedConnector })}
            />
        else
            return (
                <React.Fragment>
                    <Helmet>
                        <title>Device Groups</title>
                        <meta name="description" content="M83-Device Groups" />
                    </Helmet>

                    <header className="content-header d-flex">
                        <div className="flex-40">
                            <h6>Device Groups -
                                <span className="content-header-badge-group">
                                    <span className="content-header-badge-item">Allocated <span className="badge badge-pill badge-primary">{this.state.totalDeviceGroupsCount}</span></span>
                                    <span className="content-header-badge-item">Used <span className="badge badge-pill badge-success">{this.state.usedDeviceGroupsCount}</span></span>
                                    <span className="content-header-badge-item">Remaining <span className="badge badge-pill badge-warning">{this.state.remainingDeviceGroupsCount}</span></span>
                                </span>
                            </h6>
                        </div>
                        <div className="flex-60 text-right">
                            <div className="content-header-group">

                                {/*<div className="search-box">
                                        <span className="search-icon"><i className="far fa-search"></i></span>
                                        <input type="text" className="form-control" placeholder="Search..." value={this.state.searchTerm} onChange={this.onSearchHandler} />
                                        {this.state.searchTerm.length > 0 &&
                                            <button className="search-button" onClick={() => this.emptySearchBox()}><i className="far fa-times"></i></button>
                                        }
                                    </div>*/}
                                <button className={this.state.toggleView == true ? "btn btn-light active" : "btn btn-light"} disabled={this.state.isFetching} data-tooltip="true" data-tooltip-text="List View" data-tooltip-place="bottom" onClick={() => this.setState({ toggleView: true })}><i className="fad fa-list-ul"></i></button>
                                <button className={this.state.toggleView == false ? "btn btn-light active" : "btn btn-light"} disabled={this.state.isFetching} data-tooltip="true" data-tooltip-text="Card View" data-tooltip-place="bottom" onClick={() => this.setState({ toggleView: false })}><i className="fad fa-table"></i></button>
                                <button className="btn btn-light" disabled={this.state.isFetching} data-tooltip data-tooltip-text="Refresh" data-tooltip-place="bottom" onClick={() => this.refreshComponent()}><i className="far fa-sync-alt"></i></button>
                                <button className="btn btn-primary" data-tooltip data-tooltip-text="Add group" data-tooltip-place="bottom" disabled={this.state.remainingDeviceGroupsCount === 0 || this.state.isFetching} onClick={() => this.setState({
                                    viewType: "ADD_EDIT_VIEW",
                                    selectedGroup: {
                                        deviceGroupName: '',
                                        description: '',
                                        connectorId: this.state.selectedConnector.id,
                                        attachedDevices: [],
                                    },
                                    childViewType: 'GROUP_DETAILS'
                                })}><i className="far fa-plus"></i></button>
                            </div>
                        </div>
                    </header>

                    {this.state.isFetching ?
                        <Loader />
                        :
                        <React.Fragment>
                            {this.state.listOfConnectors.length > 0 &&
                                <div className="d-flex content-filter">
                                    <div className="flex-50 form-group pd-r-10">
                                        <div className="input-group">
                                            <div className="input-group-prepend">
                                                <span className="input-group-text">Device Type :</span>
                                            </div>
                                            <select id='connectorSelect' className="form-control" value={this.state.selectedConnector.id} onChange={this.connectorChangeHandler}>
                                                {this.state.listOfConnectors.map(connector => {
                                                    return (
                                                        <option key={connector.id} value={connector.id}>{connector.name}</option>
                                                    )
                                                })}
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            }

                            <div className={`content-body ${this.state.listOfConnectors.length > 0 && "content-filter-body"}`}>
                                {this.state.isFetchingGroups ?
                                    <div className="inner-loader-wrapper h-100">
                                        <div className="inner-loader-content">
                                            <i className="fad fa-sync-alt fa-spin"></i>
                                        </div>
                                    </div> :
                                    this.state.listOfConnectors.length > 0 ?
                                        this.state.listOfGroups.length > 0 ?
                                            this.state.listOfFilteredGroups.length > 0 ?
                                                this.state.toggleView ?
                                                    <ListingTable
                                                        columns={this.getColumns()}
                                                        data={this.state.listOfFilteredGroups}
                                                    /> :
                                                    <ul className="card-view-list">
                                                        {this.state.listOfFilteredGroups.map((item, index) =>
                                                            <li key={index}>
                                                                <div className="card-view-box">
                                                                    <div className="card-view-header">
                                                                        <span className={item.default ? "alert alert-success mr-r-7" : "alert alert-danger text-red mr-r-7"} >Default - {item.default ? "Yes" : "No" }</span>
                                                                        <span className="alert alert-primary cursor-pointer mr-r-7" onClick={() => isNavAssigned('devices') && this.props.history.push(`/devices/${this.state.selectedConnector.id}/${item.deviceGroupId}`)}>Devices - {item.attachedDevices.length}</span>
                                                                        <span className="alert alert-success">Tags - {item.tagCount ? item.tagCount : "0"}</span>
                                                                        <div className="dropdown">
                                                                            <button className="btn-transparent btn-transparent-gray" type="button" data-toggle="dropdown">
                                                                                <i className="fas fa-ellipsis-v"></i>
                                                                            </button>
                                                                            <div className="dropdown-menu">
                                                                                <button className="dropdown-item" /*disabled={!row.attachedDevices.length}*/ onClick={() => {
                                                                                    let selectedGroup = item
                                                                                    this.props.history.push(`/deviceGroups/${this.state.selectedConnector.id}/${item.deviceGroupId}`)
                                                                                    this.setState({
                                                                                        viewType: "ADD_EDIT_VIEW",
                                                                                        selectedGroup,
                                                                                        childViewType: 'GROUP_TAGVIEW'
                                                                                    })
                                                                                }}
                                                                                >
                                                                                    <i className="far fa-tag"></i>Tags
                                                                                </button>
                                                                                <button className="dropdown-item" onClick={() => {
                                                                                    let selectedGroup = item
                                                                                    this.props.history.push(`/deviceGroups/${this.state.selectedConnector.id}/${item.deviceGroupId}`)
                                                                                    this.setState({
                                                                                        viewType: "ADD_EDIT_VIEW",
                                                                                        selectedGroup,
                                                                                        childViewType: 'GROUP_DETAILS'
                                                                                    })
                                                                                }}
                                                                                >
                                                                                    <i className="far fa-pencil"></i>Edit
                                                                                </button>
                                                                                <button className="dropdown-item" disabled={item.default} onClick={() => {
                                                                                    this.setState({
                                                                                        deleteGroupConfirmation: true,
                                                                                        selectedDeviceGroupId: item.deviceGroupId,
                                                                                        deviceGroupToBeDeletedName: item.deviceGroupName
                                                                                    })
                                                                                }}
                                                                                    disabled={item.default}>
                                                                                    <i className="far fa-trash-alt"></i>Delete
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div className="card-view-body">
                                                                        <div className="card-view-icon">
                                                                            <img src="https://content.iot83.com/m83/misc/device-group-fallback.png" />
                                                                        </div>
                                                                        <h6><i className="far fa-address-book"></i>{item.deviceGroupName}</h6>
                                                                        <p><i className="far fa-file-alt"></i>{item.description ? item.description : "N/A"}</p>
                                                                        <p className="text-primary text-underline-hover" onClick={() => { isNavAssigned('deviceTypes') && this.props.history.push(`/addOrEditDeviceType/${item.connectorId}`) }}>
                                                                            <i className="far fa-microchip"></i>
                                                                            <button className="btn btn-link">{this.state.selectedConnector.name}</button>
                                                                        </p>
                                                                    </div>
                                                                    <div className="card-view-footer d-flex">
                                                                        <div className="flex-50 p-3">
                                                                            <h6>Created</h6>
                                                                            <p><strong>By - </strong>{item.createdBy}</p>
                                                                            <p><strong>At - </strong>{new Date(item.createdAt).toLocaleString('en-US', { createdAt: localStorage.createdAt })}</p>
                                                                        </div>
                                                                        <div className="flex-50 p-3">
                                                                            <h6>Updated</h6>
                                                                            <p><strong>By - </strong>{item.updatedBy}</p>
                                                                            <p><strong>At - </strong>{new Date(item.updatedAt).toLocaleString('en-US', { createdAt: localStorage.createdAt })}` `</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        )}
                                                    </ul>
                                                : <NoDataFoundMessage /> :
                                            <AddNewButton
                                                text1="No group(s) available"
                                                text2="You haven't created any group(s) yet"
                                                createItemOnAddButtonClick={() => {
                                                    this.setState({
                                                        viewType: 'ADD_EDIT_VIEW',
                                                        selectedGroup: {
                                                            deviceGroupName: '',
                                                            description: '',
                                                            connectorId: this.state.selectedConnector.id,
                                                            attachedDevices: [],
                                                        }
                                                    })
                                                }}
                                                imageIcon="addDeviceGroup.png"
                                            /> :
                                        <AddNewButton
                                            text1="No groups(s) available"
                                            text2="You haven't created any device type(s) yet. Please create a device type first."
                                            addButtonEnable={isNavAssigned("deviceTypes")}
                                            createItemOnAddButtonClick={() => this.props.history.push(`/addOrEditDeviceType`)}
                                            imageIcon="addDeviceGroup.png"
                                        />
                                }
                            </div>
                        </React.Fragment>
                    }

                    {/*NotificationModal*/}
                    {this.state.isOpen &&
                        <NotificationModal
                            type={this.state.modalType}
                            message2={this.state.message2}
                            onCloseHandler={this.onCloseHandler}
                        />
                    }

                    {/* Delete Group Modal */}

                    {this.state.deleteGroupConfirmation &&
                        <ConfirmModel
                            status={"delete"}
                            deleteName={this.state.deviceGroupToBeDeletedName}
                            confirmClicked={() => {
                                this.setState({
                                    isFetching: true,
                                    deleteGroupConfirmation: false
                                }, () => {
                                    this.props.deleteGroup(this.state.selectedDeviceGroupId)
                                })
                            }}
                            cancelClicked={() => {
                                this.setState({
                                    deleteGroupConfirmation: false
                                })
                            }}
                        />
                    }
                </React.Fragment>
            );
    }
}

DeviceGroups.propTypes = {
    dispatch: PropTypes.func.isRequired
};

let allSelectors = {}
Object.entries(SELECTORS).map(([key, value]) => {
    allSelectors[key] = value()
})

const mapStateToProps = createStructuredSelector(allSelectors);

function mapDispatchToProps(dispatch) {
    let allActions = { dispatch }
    Object.entries(ACTIONS).map(([key, value]) => {
        allActions[key] = (...args) => dispatch(value(...args))
    })
    return allActions;
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "deviceGroups", reducer });
const withSaga = injectSaga({ key: "deviceGroups", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(DeviceGroups);
