/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * CreateOrEditPage
 *
 */

import React, {useState, useEffect}  from "react";
import { Helmet } from "react-helmet";
import GridLayout from "../../components/GridLayout";

/* eslint-disable react/prefer-stateless-function */
function CreateOrEditPage(props) {
    const [state, setState] = useState({ addedWidgets: [], val: true})

let wrapperRef;
// let setWrapperRef = (node) => { //find where it was used
//   wrapperRef = node;
// }

// let handleClickOutside = (event) => {
//     if (wrapperRef && !wrapperRef.contains(event.target)) {
//         setState({
//             selectedColorPicker: null
//         })
//     }
// }

  useEffect(() => { //didMount
    // document.addEventListener('mousedown', handleClickOutside);
    // setState({
    //     isLoading: 4,
    // })
    // props.getAllWidgets();
    // // props.getAllPages();
    props.getAllGatewayApis();
    props.getConnectorsByCategory("MQTT");
    // props.getDurationList();
  }, [])

  // if(Object.keys(SELECTORS).some(prop => props[prop])){
  //   // props.resetToInitialState()
  // }
  
  let setSelectedDayForWeather = (currentDate, id) => {
    // setSelectedDay(currentDate, id, state.addedWidgets, this)
  }

  let clearMapFilter = ({ i }) => {
    if (typeof (state.configWidgetIndex) === "number") {
        setState({
            ...state,
            searchTerm: "",
            chartData: state.originalData,
        })
    } else {
        let addedWidgets = cloneDeep(state.addedWidgets);
        let foundIndex = addedWidgets.findIndex(temp => temp.i == i)
        addedWidgets[foundIndex].searchTerm = "";
        addedWidgets[foundIndex].widgetData = cloneDeep(addedWidgets[foundIndex].originalData);
        setState({
            ...state,
            addedWidgets
        })
    }
  }

  let mapInfoBoxHandler = (action, { i }, index) => {
    if (typeof (state.configWidgetIndex) === "number") {
        let chartData = cloneDeep(state.chartData);
        chartData.tableData.map(temp => temp.openSelectedInfoBox = false)
        if (action === "OPEN")
            chartData.tableData[index].openSelectedInfoBox = true
        setState({
            ...state,
            chartData
        })

    } else {
        let addedWidgets = cloneDeep(state.addedWidgets);
        let foundIndex = addedWidgets.findIndex(temp => temp.i == i)
        addedWidgets[foundIndex].widgetData.tableData.map(temp => temp.openSelectedInfoBox = false)
        if (action === "OPEN")
            addedWidgets[foundIndex].widgetData.tableData[index].openSelectedInfoBox = true
        setState({
            ...state,
            addedWidgets
        })
    }
  }

  let mapSearchHandler = ({ i }) => ({ currentTarget }) => {
    if (typeof (state.configWidgetIndex) === "number") {
        let searchTerm = currentTarget.value, chartData = cloneDeep(state.chartData), originalData = cloneDeep(state.originalData);
        originalData = originalData ? originalData : chartData;
        chartData = cloneDeep(originalData)
        if (searchTerm)
            chartData.tableData = originalData.tableData.filter((temp) => (temp.name || temp._uniqueDeviceId).toLowerCase().includes(searchTerm.toLowerCase().trim()));
        setState({
            ...state,
            searchTerm,
            chartData,
            originalData
        })
    } else {
        let addedWidgets = cloneDeep(state.addedWidgets);
        let foundIndex = addedWidgets.findIndex(temp => temp.i == i)
        addedWidgets[foundIndex].originalData = addedWidgets[foundIndex].originalData ? addedWidgets[foundIndex].originalData : addedWidgets[foundIndex].widgetData;
        addedWidgets[foundIndex].searchTerm = currentTarget.value;
        addedWidgets[foundIndex].widgetData = cloneDeep(addedWidgets[foundIndex].originalData);
        if (currentTarget.value)
            addedWidgets[foundIndex].widgetData.tableData = addedWidgets[foundIndex].widgetData.tableData.filter((temp) => (temp.name || temp._uniqueDeviceId).toLowerCase().includes(currentTarget.value.toLowerCase().trim()));
        setState({
          ...state,
            addedWidgets
        })
    }
  }

  let handleFullScreenWidgetView = (i) => {
    let fullScreenWidgetIndex = state.addedWidgets.findIndex(el => el.i === i)
    setState({ ...state, fullScreenWidgetIndex });
  }

  let openEditWidgetTab = (widgetId) => {
    let url = `${window.location.origin}/createOrEditPage/${widgetId}`;
    window.open(url)
  }

  let addWidgetHandler = (i, isDuplicate) => {
    let addedWidgets = JSON.parse(JSON.stringify(state.addedWidgets));
    let requiredWidget
    let lastRowY = 0;
    let obj = {};
    if (addedWidgets.length === 1) {
        lastRowY = addedWidgets[0].y
    } else {
        addedWidgets.map((widget, index) => {  // to find the Y of widget at bottom
            lastRowY = (widget.y > lastRowY) ? widget.y : lastRowY
        })
    }
    let lastRowWidgets = addedWidgets.filter(widget => widget.y === lastRowY);
    let lastWidget = { x: 0 };
    if (lastRowWidgets.length === 1) {
        lastWidget = lastRowWidgets[0]
    } else {
        lastRowWidgets.map((widget, index) => {
            lastWidget = widget.x >= lastWidget.x ? widget : lastWidget;
        })
    }
    if (isDuplicate) {
        obj = JSON.parse(JSON.stringify(addedWidgets.find(el => el.i === i)))
        obj.i = uIdGenerator()
        // delete obj.widgetDetails
    }
    else {
        requiredWidget = state.widgetMetaData.find(variant => variant.widgetName === state.selectedChart)
        let isCustomWidget = state.selectedWidget === "customWidgets";
        obj = {
            headerName: requiredWidget.widgetDisplayName,
            i: uIdGenerator(),
            theme: "noTheme",
            widgetType: requiredWidget.category,
            widgetId: requiredWidget.id,
            widgetName: state.selectedChart,
            "dataSource": isCustomWidget ? "CUSTOM" : "DEFAULT",
            apiId: "",
            widgetData: isCustomWidget ? requiredWidget.data.responseFormat : requiredWidget.data,
            "mqttDetails": {},
            "widgetDetails": {
                "imageUrl": "",
                "icon": "",
                "detailPage": "",
                "selectedAttribute": "_uniqueDeviceId",
                "customValues": isCustomWidget ? {} : getCustomValues(requiredWidget.category, state.selectedChart, requiredWidget.data),
                "hideHeader": !["header", "counter"].includes(requiredWidget.category),
                "autoRefreshing": false
            },
            "apiDetails": {},
            "customWidgetDetails": isCustomWidget ? {
                ...requiredWidget.data,
                widgetSettings: requiredWidget.widgetSettings,
                type: requiredWidget.subCategory
            } : {},
            preview: true,
            showFullScreen: requiredWidget.showFullScreen,
        }
        switch (obj.widgetType) {
            case "gaugeChart":
                obj.w = 4;
                obj.h = 6;
                break;
            case "header":
                obj.w = 12;
                obj.h = 1;
                break;
            case "label":
                if (obj.widgetName === "textArea") {
                    obj.w = 3;
                    obj.h = 2;
                    break;
                }
                obj.w = 6;
                obj.h = 1;
                break;
            case "counter":
                obj.w = 3;
                obj.h = 1.6;
                obj.minW = 2;
                obj.minH = 1.4;
                obj.maxH = 1.6;
                break;
            case "table":
                obj.w = 6;
                obj.h = 8.6;
                break;
            case "map":
                obj.w = 12;
                obj.h = 7;
                break;
            case "control":
                obj.w = 2;
                obj.h = 1;
                obj.minH = 1;
                obj.maxH = 1.6;
                break;
            case "weather":
                obj.w = 7;
                obj.h = 6;
                obj.minW = 7;
                obj.minH = 6;
                obj.maxH = 6;
                break;
            default:
                obj.w = 6;
                obj.h = 6;
        }
    }
    if (addedWidgets.length > 0) {
        if ((12 - (lastWidget.x + lastWidget.w)) >= obj.w) {
            obj.x = (lastWidget.x + lastWidget.w);
            obj.y = lastWidget.y
        }
        else {
            obj.x = 0;
            if (lastRowWidgets.length > 1) {
                let maxHeight;
                lastRowWidgets.map((widget, index) => {
                    if ((index + 1) != lastRowWidgets.length) {
                        maxHeight = widget.h >= lastRowWidgets[index + 1].h ? widget.h : lastRowWidgets[index + 1].h
                    }
                })
                obj.y = lastRowWidgets[0].y + maxHeight;
            } else {
                obj.y = lastRowWidgets[0].y + lastRowWidgets[0].h
            }
        }
    }
    else {
        obj.x = 0;
        obj.y = 0;
    }
    addedWidgets.push(obj)
    setState({
        ...state,
        addedWidgets,
        selectedChart: "",
        updatedWidget: ""
    }, () => document.getElementById("reactGridBox").scroll({ top: parseInt(document.getElementById("reactGridBox").firstChild.style.height), left: 0, behavior: 'smooth' }))
  }

  let onLayoutChange = (layout) => {
    let addedWidgets = JSON.parse(JSON.stringify(state.addedWidgets));
    addedWidgets = addedWidgets.map((widget) => {
        layout.map((lay) => {
            if (lay.i === widget.i) {
                lay.h = lay.h
                widget.h = lay.h
                widget.w = lay.w
                widget.x = lay.x
                widget.y = lay.y
            }
            return lay
        })
        return widget
    })
    setState({ layout: layout, addedWidgets });
}

  let configureClickHandler = (param) => {
    let requiredWidget = state.addedWidgets.find(el => el.i === param.i);
    // let selectedTopic = state.addedWidgets.find(temp => temp.dataSource === "MQTT");
    if (requiredWidget.widgetType === "layout") {
        let index = state.addedWidgets.findIndex(el => el.i === param.i)
        setState({
            showLayoutConfig: true,
            configWidgetIndex: index,
        })
        return;
    }
    let api = {},
        controlPreviewValidation = requiredWidget.widgetType === "control" && requiredWidget.dataSource === "API"
    // controlPreviewValidation = state.controlPreviewValidation
    if ((requiredWidget.apiId && requiredWidget.widgetName !== "image") || (controlPreviewValidation)) {
        api = requiredWidget.widgetType === "control" ? {} : state.allGatewayApis.find(match => match.id === requiredWidget.apiId)
        api.pathParameters = requiredWidget.apiDetails.pathParameters
    }

    if ((API_COLLECTION_WIDGETS.includes(requiredWidget.widgetType) || (requiredWidget.widgetType === "control")) && param.dataSource === "API") {
        let collection = api.pathParameters.find(param => param.name === "datasource").value
        api.pathParameters.map(param => {
            param.isLoading = param.parent === "datasource"
        })
        !state.attributes.length && props.getCollectionDetails(collection)
    }
    let devices = state.devices
    if (requiredWidget.dataSource === "MQTT") {
        devices = state.connectorsList.find(temp => temp.id === requiredWidget.mqttDetails.topic.connector).properties.deviceIds.map(temp => ({ label: temp.name || temp.deviceId, value: temp.deviceId }))
    }
    setState({
        ...state,
        configWidgetName: requiredWidget.widgetName,
        configWidgetType: requiredWidget.widgetType,
        configWidgetIndex: state.addedWidgets.findIndex(el => el.i === param.i),
        headerName: requiredWidget.headerName,
        // isWidgetView: true,
        updatedWidget: requiredWidget.i,
        selectedTheme: requiredWidget.theme,
        pageUrl: requiredWidget.widgetDetails.detailPage,
        autoRefreshing: requiredWidget.widgetDetails.autoRefreshing,
        // selectedAttribute: requiredWidget.widgetDetails.selectedAttribute,
        selectedAttribute: "_uniqueDeviceId",
        // widgetData: requiredWidget.widgetData,
        chartData: requiredWidget.widgetData,
        apiId: requiredWidget.apiId,
        widgetName: requiredWidget.widgetName,
        dataSource: requiredWidget.widgetType === "customWidgets" ? "API" : param.dataSource,
        customData: requiredWidget.dataSource === "CUSTOM" ? requiredWidget.widgetData : null,
        api,
        icon: requiredWidget.widgetDetails.icon,
        keys: requiredWidget.mqttDetails.keys ? requiredWidget.mqttDetails.keys : requiredWidget.widgetType === "lineChartMultiAxes" ? [] : {},
        selectedConnectorDetails: requiredWidget.dataSource === "MQTT" ? requiredWidget.mqttDetails.topic : {
            connector: state.selectedConnector,
            deviceId: ""
        },
        selectedTopic: requiredWidget.dataSource === "MQTT" ? getTopicName(requiredWidget.mqttDetails.topic.connector, requiredWidget.mqttDetails.topic.deviceId, state.connectorsList) : "",
        // isDisabledTopic: selectedTopic ? true : false,
        // topicType: selectedTopic ? selectedTopic.mqttDetails.topicType : "SIMULATION",
        units: requiredWidget.mqttDetails.units ? JSON.parse(requiredWidget.mqttDetails.units) : {},
        isLocalTimestamp: requiredWidget.mqttDetails.isLocalTimestamp ? requiredWidget.mqttDetails.isLocalTimestamp : false,
        customValues: cloneDeep(requiredWidget.widgetDetails.customValues),
        subCategory: requiredWidget.customWidgetDetails.type,
        hideHeader: requiredWidget.widgetDetails.hideHeader,
        redirectUri: "",
        originalData: null,
        searchTerm: "",
        controlPreviewValidation,
        devices,
    })
  }

  let removeWidgetHandler = (i) => {
    let addedWidgets = JSON.parse(JSON.stringify(state.addedWidgets)),
        requiredIndex = addedWidgets.findIndex(widget => widget.i === i),
        requiredWidget = addedWidgets[requiredIndex]
    addedWidgets = addedWidgets.filter(widget => widget.i != i);
    // if (requiredWidget.dataSource === "MQTT" && client) {
    //     if (addedWidgets.every(widget => widget.dataSource !== "MQTT"))
    //         this.endMqttConnection()
    //     else if (!addedWidgets.find(widget => widget.dataSource === "MQTT" && widget.mqttDetails.topic.deviceId === requiredWidget.mqttDetails.topic.deviceId && widget.mqttDetails.topic.connector === requiredWidget.mqttDetails.topic.connector)) {
    //         this.unsubscribeTopic(this.getTopicName(widget.mqttDetails.topic.connector, widget.mqttDetails.topic.deviceId))
    //     }
    // }
    setState({ ...state, addedWidgets });
}

let setStateFromChild= () => {
    setState({...state, val: false})
}

  return (
    <React.Fragment>
      <Helmet>
        <title>CreateOrEditPage</title>
        <meta name="description" content="Description of CreateOrEditPage" />
      </Helmet>
      <header className="content-header d-flex">
        <div className="flex-60">
            <h6>Pages</h6>
            <h6 className="active">Add Page</h6>
        </div>
        <div className="flex-40 text-right">
            <div className="content-header-group">
                <button className="btn btn-light" onClick={()=>props.history.push('/dashboards')}><i className="fas fa-angle-left"></i></button>
            </div>
        </div>
    </header>
    <div className="content-body" id="pageStudioBody">
        {state.val && <GridLayout 
            functions = {{
                history: props.history, 
                clearMapFilter,
                mapInfoBoxHandler,
                mapSearchHandler,
                handleFullScreenWidgetView,
                openEditWidgetTab,
                configureClickHandler,
                addWidgetHandler,
                removeWidgetHandler,
                onLayoutChange,
                setSelectedDay: setSelectedDayForWeather,
                setState: setStateFromChild
            }}
            state={state}
        />}
    </div>
    </React.Fragment>
  );
}
CreateOrEditPage.propTypes = {};

export default CreateOrEditPage;
