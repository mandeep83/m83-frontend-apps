/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * CreateOrEditPage
 *
 */

import React, { useState, useEffect, useRef } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import { allSelectors as SELECTORS } from "./selectors";
import { allActions as ACTIONS } from './actions'
import reducer from "./reducer";
import saga from "./saga";
import WidgetConfigWindow from '../../components/WidgetConfigWindow';
import WidgetsMenu from '../../components/PageConfigure';
import GridLayout from '../../components/GridLayout';
import PageStudioErrorBoundry from "../../components/PageStudioErrorBoundry/Loadable"
import { uIdGenerator } from "../../commonUtils";
import cloneDeep from 'lodash/cloneDeep';
import ConfirmModel from '../../components/ConfirmModel/Loadable'
import { handleApiData, invokeAllApis, invokeApiHandler, getChartDataFormat, mqttDetailsValidation, updateWidgetData, getCustomValues, unsubscribeAllTopics, flattenTopologyData } from "./pageStudioUtils";
import { subscribeTopic, unsubscribeTopic } from "../../mqttConnection";
import Loader from "../../components/Loader/Loadable"
import * as pageStudioConstants from "./commonConstants"
let { CUSTOMIZATION_WIDGETS, LAST_AVAILABLE_LIST, COUNTER_TYPES } = pageStudioConstants
/* eslint-disable react/prefer-stateless-function */
class CreateOrEditPage extends React.Component {
    state = {
        pageName: "Device Map",
        description: "",
        addedWidgets: [],
        pageType: (this.props.location.state && this.props.location.state.pageType) || "deviceMap",
        selectedConnector: (this.props.location.state && this.props.location.state.selectedConnector) || "",
        selectedConnectorDetails: {
            connector: "",
            deviceId: ""
        },
        widgetMenu: true,
        widgetMetaData: [],
        controlCommands: [],
        connectorsList: [],
        devices: [],
        selectedDuration: 60,
        pageSettings: {
            header: {
                fontSize: 14,
                color: "#4a4a4a",
                backgroundColor: "#fff",
                fontWeight: 600,
            },
            subHeading: {
                fontSize: 11,
                color: "#808080",
                fontWeight: 400,
            },
        },
        apiCall: true,
        isLoading: 2,
        subscribedTopics: [],
        messages: [],
        lastAvailableList: LAST_AVAILABLE_LIST,
        counterTypes: COUNTER_TYPES,
        allDeviceGroups: {},
        deviceGroups: [],
        attributes: [],
    }

    // let wrapperRef;
    // let setWrapperRef = (node) => { //find where it was used
    //   wrapperRef = node;
    // }

    // let handleClickOutside = (event) => {
    //     if (wrapperRef && !wrapperRef.contains(event.target)) {
    //         setState({
    //             selectedColorPicker: null
    //         })
    //     }
    // }

    setWrapperRef = (node) => {
        this.wrapperRef = node;
    }

    componentDidMount() {
        this.props.getAllWidgets();
        this.props.getAllPages();
        this.props.getAllGatewayApis();
        this.props.getConnectorsByCategory("MQTT");
        this.props.getDurationList();
    }

    componentWillUnmount() {
        unsubscribeAllTopics(this.state.subscribedTopics)
    }

    static getDerivedStateFromProps(nextProps, state) {
        let newProp = Object.keys(SELECTORS).find(prop => nextProps[prop])
        if (newProp) {
            let propData = nextProps[newProp].error ? nextProps[newProp].error : cloneDeep(nextProps[newProp].response)
            switch (newProp) {
                case "getAllGatewayApisSuccess":
                    return { allGatewayApis: propData }
                case "getAllPagesSuccess":
                    return { pageList: propData.filter(page => page.resourceOwner) }
                case "getAllWidgetsSuccess":
                    propData.map(widget => {
                        if (widget.isPage && widget.subcategories && widget.subcategories.length) {
                            widget.subcategories.map(subCat => {
                                let newWidget = Object.create(widget)
                                newWidget.widgetDisplayName = "Device Listing 2"
                                newWidget.widgetName = subCat.widgetName
                                newWidget.widgetDisplayName = subCat.widgetDisplayName
                                newWidget.imageName = subCat.imageName
                                newWidget.disabled = subCat.disabled
                                propData.push(newWidget)
                            })
                        }
                    })
                    let topologyWidget = propData.find(widget => widget.category === "topology")
                    topologyWidget.data = flattenTopologyData(topologyWidget.data)
                    return {
                        allWidgets: propData.filter(widget => widget.pageTypes.includes(state.pageType)),
                        consistentWidgetData: propData,
                        widgetMetaData: propData,
                        isLoading: state.isLoading - 1,
                    };
                case "getConnectorsListSuccess":
                    let connectorsList = propData.map(connector => {
                        connector.label = connector.name;
                        connector.value = connector.id
                        return connector
                    })
                    return {
                        connectorsList,
                        isLoading: state.isLoading - 1,
                    }
                case "getDurationListSuccess":
                    let durationList = {
                        hourly: propData.filter(duration => duration.label.toLowerCase().includes("hour")),
                        daily: propData.filter(duration => duration.label.toLowerCase().includes("day")),
                        weekly: propData.filter(duration => duration.label.toLowerCase().includes("week")),
                        monthly: propData.filter(duration => duration.label.toLowerCase().includes("month")),
                        yearly: propData.filter(duration => duration.label.toLowerCase().includes("year")),
                    }
                    Object.entries(durationList).map(([key, value]) => {
                        !value.length && delete durationList[key]
                    })
                    return {
                        durationList,
                        rawDurationList: propData,
                    }
                case "invokeApiSuccess":
                    let addedWidgets = handleApiData(state.addedWidgets, nextProps[newProp])
                    return {
                        addedWidgets
                    }
                case "getCollectionDetailsSuccess":
                    let attributes = propData.map(el => {
                        el.label = el.displayName;
                        el.value = el.attribute || el.name;
                        return el
                    }),
                        uniqueAttr = attributes.find(attr => attr.isUnique),
                        devices = uniqueAttr ? uniqueAttr.uniqueValues.map(val => ({ label: val.name || val.deviceId, value: val.deviceId })) : state.devices,
                        identifier = uniqueAttr ? uniqueAttr.name : ""
                    attributes = attributes.filter(attr => !attr.isUnique)
                    return {
                        attributes,
                        devices,
                        identifier
                    }
                case "savePageSuccess":
                    return {
                        isSubmitSuccess: true,
                        addedWidgets: [],
                    }
                case "getPageByidSuccess":
                    // propData.widgets = addwidgetSettings(propData.widgets, true) //doubt
                    propData.widgets.forEach(widget => {
                        widget.preview = true;
                        widget.isLoadingData = Boolean(widget.apiId || (widget.mqttDetails.topic && widget.mqttDetails.topic.name))
                        let needsCustomValues = CUSTOMIZATION_WIDGETS.includes(widget.widgetType)
                        if (needsCustomValues && (widget.dataSource === "DEFAULT" || !["barChart", "lineChart", "lineChartMultiAxes"].includes(widget.widgetType)))
                            widget.widgetDetails.customValues = getCustomValues(widget.widgetType, widget.widgetName, widget.widgetData, widget.widgetDetails.customValues)
                        if (widget.widgetType === "topology" && widget.dataSource === "DEFAULT")
                            widget.widgetData = flattenTopologyData(widget.widgetData)
                    })
                    let allWidgets = state.widgetMetaData.filter(widget => widget.pageTypes.includes(propData.pageType))
                    return {
                        pageName: propData.name,
                        description: propData.description,
                        selectedConnector: propData.connectorId,
                        pageSettings: propData.pageSettings || state.pageSettings,
                        isConnectorAttached: propData.isConnectorAttached,
                        isLoading: state.isLoading - 1,
                        pageType: propData.pageType,
                        selectedDuration: propData.duration || 60,
                        allWidgets,
                        apiCall: false,
                        addedWidgets: propData.widgets,
                        pageId: nextProps.match.params.id
                    }
                case "getDeviceGroupsSuccess": {
                    let allDeviceGroups = cloneDeep(state.allDeviceGroups)
                    propData.map(group => {
                        group.value = group.deviceGroupId
                        group.label = group.deviceGroupName
                    })
                    allDeviceGroups[nextProps[newProp].connectorId] = propData
                    return {
                        allDeviceGroups,
                        deviceGroups: allDeviceGroups[state.selectedConnector] || []
                    }
                }
                case "invokeApiFailure":
                    addedWidgets = cloneDeep(state.addedWidgets);
                    let requiredWidget = addedWidgets.find(widget => widget.i === nextProps.invokeApiFailure.addOns.index)
                    requiredWidget.error = nextProps.invokeApiFailure.error
                    requiredWidget.isLoadingData = false
                    return {
                        addedWidgets,
                    }
                    break;
                case "getAllWidgetsFailure":
                case "getConnectorsListFailure":
                case "savePageFailure":
                    return {
                        isLoading: state.isLoading - 1
                    }
            }
        }
        return null
    }

    componentDidUpdate(prevProps, prevState) {
        if ((prevState.widgetMetaData !== this.state.widgetMetaData) || (prevState.connectorsList !== this.state.connectorsList)) {
            if (!this.state.isLoading && this.props.match.params.id) {
                this.setState({
                    isLoading: 1
                }, () => this.props.getPageById(this.props.match.params.id))
            }
        }
        if (prevState.pageId !== this.state.pageId) {
            if (this.state.selectedConnector)
                this.connectorChangeHandler(this.state.selectedConnector, true)
            else {
                this.getAllWidgetsData()
            }
        }
        let newProp = Object.keys(SELECTORS).find(prop => this.props[prop])
        if (newProp) {
            if (["savePageSuccess"].includes(newProp) || newProp.includes("Failure")) {
                let propData = this.props[newProp].error ? this.props[newProp].error : cloneDeep(this.props[newProp].response)
                let type = newProp.includes("Failure") ? "error" : "success"
                this.props.showNotification(type, propData, this.onCloseHandler)
            }
            this.props.resetToInitialStateProps(newProp)
        }
    }

    getAllWidgetsData = () => {
        let addedWidgets = cloneDeep(this.state.addedWidgets)
        let subscribedTopics = [...this.state.subscribedTopics]
        addedWidgets.map((widget, index) => {
            let isApiConfigured = widget.dataSource === "API" && widget.apiId
            let ismqttConfigured = mqttDetailsValidation(widget)
            widget.isLoadingData = Boolean(isApiConfigured || ismqttConfigured)
            if (isApiConfigured)
                invokeApiHandler(widget, false, this.props.invokeApi, this.state)
            else if (ismqttConfigured) {
                let topic = widget.mqttDetails.topic.name
                if (!subscribedTopics.includes(topic)) {
                    subscribedTopics.push(topic)
                    subscribeTopic(topic, this.mqttMessageHandler)
                }
            }
        })
        this.setState({
            subscribedTopics,
            addedWidgets
        })
    }

    setSelectedDayForWeather = (currentDate, id) => {
        // setSelectedDay(currentDate, id, state.addedWidgets, this)
    }

    openEditWidgetTab = (widgetId) => {
        let url = `${window.location.origin}/createOrEditPage/${widgetId}`;
        window.open(url)
    }

    mqttMessageHandler = (message, topic) => {
        let messages = cloneDeep(this.state.messages)
        let addedWidgets = cloneDeep(this.state.addedWidgets);
        if (!messages[topic] || messages[topic].every(msg => JSON.stringify(msg) !== JSON.stringify(message))) {
            if (messages[topic])
                messages[topic].push(message);
            else
                messages = { ...messages, [topic]: [message] }
            addedWidgets.map((widget) => {
                if (mqttDetailsValidation(widget) && widget.mqttDetails.topic.name === topic) {
                    widget = updateWidgetData(widget, messages, this.state.connectorsList)
                }
                return widget
            })
            let isSniffing = this.state.isSniffing === topic ? false : this.state.isSniffing
            this.setState({
                // chartData,
                messages,
                addedWidgets,
                isSniffing
            })
        }
    }

    getDimensionsForWidget = (widget) => {
        switch (widget.widgetType) {
            case "gaugeChart":
                widget.w = 6;
                widget.h = 6.5;
                break;
            case "header":
                widget.w = 12;
                widget.h = 1;
                break;
            case "label":
                if (widget.widgetName === "textArea") {
                    widget.w = 3;
                    widget.h = 2;
                    break;
                }
                widget.w = 6;
                widget.h = 1;
                break;
            case "counter":
                widget.w = 3;
                widget.h = 1.6;
                widget.minW = 2;
                widget.minH = 1.4;
                widget.maxH = 1.6;
                break;
            case "deviceList":
            case "deviceMap":
                widget.w = 12;
                widget.h = 20;
                break;
            case "deviceMap":
                widget.w = 12;
                widget.h = 7;
                break;
            case "deviceAlarms":
                widget.w = 12;
                widget.h = 7;
                break;
            case "control":
                widget.w = 2;
                widget.h = 1;
                widget.minH = 1;
                widget.maxH = 1.6;
                break;
            case "weather":
                widget.w = 7;
                widget.h = 6;
                widget.minW = 7;
                widget.minH = 6;
                widget.maxH = 6.6;
                break;
            case "insights":
                widget.w = 3.8;
                widget.h = 4.3;
                break;
            case "details":
                widget.w = 5;
                widget.h = 7;
                widget.minW = 4;
                widget.minH = 1;
                break;
            case "topology":
                widget.w = 12;
                widget.h = 4;
                widget.minW = 12;
                widget.minH = 1;
                break;
            case "info":
                widget.w = 5;
                widget.h = 4;
                break;
            case "eventHistory":
                widget.w = 12;
                widget.h = 10.2;
                break;
            case "uptimeInfo":
                widget.w = 12;
                widget.h = 23.3;
                break;
            default:
                widget.w = 6;
                widget.h = 6;
        }
        return widget;
    }

    getCoordinatesForWidget = (widget) => {
        let { addedWidgets } = this.state
        if (!addedWidgets.length) {
            widget.x = 0;
            widget.y = 0;
        }
        else {
            let lastRowY = 0;
            if (addedWidgets.length === 1) {
                lastRowY = addedWidgets[0].y
            } else {
                addedWidgets.map((widget, index) => {  // to find the Y of widget at bottom
                    lastRowY = (widget.y > lastRowY) ? widget.y : lastRowY
                })
            }
            let lastRowWidgets = addedWidgets.filter(widget => widget.y === lastRowY);
            let lastWidget = { x: 0 };
            if (lastRowWidgets.length === 1) {
                lastWidget = lastRowWidgets[0]
            } else {
                lastRowWidgets.map((widget, index) => {
                    lastWidget = widget.x >= lastWidget.x ? widget : lastWidget;
                })
            }
            if ((12 - (lastWidget.x + lastWidget.w)) >= widget.w) {
                widget.x = (lastWidget.x + lastWidget.w);
                widget.y = lastWidget.y
            }
            else {
                widget.x = 0;
                if (lastRowWidgets.length > 1) {
                    let maxHeight;
                    lastRowWidgets.map((widget, index) => {
                        if ((index + 1) != lastRowWidgets.length) {
                            maxHeight = widget.h >= lastRowWidgets[index + 1].h ? widget.h : lastRowWidgets[index + 1].h
                        }
                    })
                    widget.y = lastRowWidgets[0].y + maxHeight;
                } else {
                    widget.y = lastRowWidgets[0].y + lastRowWidgets[0].h
                }
            }
        }
        return widget;
    }

    getWidgetProperties = (widgetName) => {
        let requiredWidget = this.state.allWidgets.find(variant => variant.widgetName === widgetName)
        let isCustomWidget = requiredWidget.category === "customWidgets";
        let widget = {
            headerName: requiredWidget.widgetDisplayName,
            i: uIdGenerator(),
            theme: "noTheme",
            widgetType: requiredWidget.category,
            widgetId: requiredWidget.id,
            widgetName,
            "dataSource": isCustomWidget ? "CUSTOM" : "DEFAULT",
            apiId: "",
            widgetData: isCustomWidget ? requiredWidget.data.responseFormat : requiredWidget.data,
            "mqttDetails": {
                topic: {}
            },
            "widgetDetails": {
                "headerName": requiredWidget.widgetDisplayName,
                "imageUrl": "",
                "detailPage": "",
                "selectedAttribute": "_uniqueDeviceId",
                "customValues": isCustomWidget ? {} : getCustomValues(requiredWidget.category, widgetName, requiredWidget.data),
                "subHeadingVisibility": true,
                "subHeading": "Sample Sub-Heading",
                "iconVisibility": true,
                "icon": "fad fa-chart-line",
                "iconColor": "#07a0e3",
                "autoRefreshing": false
            },
            "apiDetails": {},
            "customWidgetDetails": isCustomWidget ? {
                ...requiredWidget.data,
                widgetSettings: requiredWidget.widgetSettings,
                type: requiredWidget.subCategory
            } : {},
            preview: true,
            showFullScreen: requiredWidget.showFullScreen,
        }
        widget = this.getDimensionsForWidget(widget)
        return widget
    }

    addWidgetHandler = (widgetName, sourceWidgetId) => {
        let addedWidgets = cloneDeep(this.state.addedWidgets);
        let widget = {};
        if (sourceWidgetId) {
            widget = cloneDeep(addedWidgets.find(el => el.i === sourceWidgetId))
            widget.i = uIdGenerator()
        }
        else {
            widget = this.getWidgetProperties(widgetName)
        }
        widget = this.getCoordinatesForWidget(widget)
        addedWidgets.push(widget)
        this.setState({
            addedWidgets,
        }, () => {
            document.getElementById("reactGridBox").scroll({ top: parseInt(document.getElementById("reactGridBox").firstChild.style.height), left: 0, behavior: 'smooth' })
        })
    }

    onLayoutChange = (layout, y, z) => {
        let addedWidgets = cloneDeep(this.state.addedWidgets);
        addedWidgets = addedWidgets.map((widget) => {
            layout.map((lay) => {
                if (lay.i === widget.i) {
                    lay.h = lay.h
                    widget.h = lay.h
                    widget.w = lay.w
                    widget.x = lay.x
                    widget.y = lay.y
                }
                return lay
            })
            return widget
        })
        this.setState({ layout, addedWidgets });
    }

    connectorChangeHandler = (selectedConnector, initialChange) => {
        let dataSourceValue = "",
            devices = [],
            controlCommands = [],
            selectedConnectorDetails = cloneDeep(this.state.selectedConnectorDetails)
        selectedConnectorDetails.connector = selectedConnector
        selectedConnectorDetails.deviceId = ""
        if (selectedConnector) {
            let requiredConnector = this.state.connectorsList.find(connector => connector.value === selectedConnector),
                userId = localStorage.userId,
                projectId = localStorage.selectedProjectId,
                userIdKey = userId.substring(0, 3) + userId.substring(userId.length - 5),
                projectKey = projectId.substring(0, 3) + projectId.substring(projectId.length - 5)
            dataSourceValue = `ETL_analytics_${requiredConnector.label}_${userIdKey}_${projectKey}`
            devices = requiredConnector.properties.deviceIds ? requiredConnector.properties.deviceIds.map(temp => ({ label: temp.name || temp.deviceId, value: temp.deviceId })) : []
            controlCommands = requiredConnector.controlConfig ? requiredConnector.controlConfig.map(config => {
                config.label = config.controlName;
                config.value = config.controlName;
                return config
            }) : controlCommands
        }
        this.setState(prevState => ({
            dataSourceValue,
            selectedConnector,
            devices,
            selectedConnectorDetails,
            controlCommands,
            tempConnector: "",
            deviceGroups: prevState.allDeviceGroups[selectedConnector] || []
        }), () => {
            if (initialChange) {
                this.getAllWidgetsData()
            }
            if (selectedConnector) {
                this.props.getCollectionDetails(this.state.dataSourceValue)
                this.state.pageType === "aggregate" && !this.state.allDeviceGroups[selectedConnector] && this.props.getDeviceGroups(selectedConnector)
            }
        })
    }

    removeWidgetHandler = (i) => {
        let addedWidgets = cloneDeep(this.state.addedWidgets),
            subscribedTopics = cloneDeep(this.state.subscribedTopics),
            requiredWidget = addedWidgets.find(widget => widget.i === i),
            requiredTopicName = requiredWidget.mqttDetails.topic.name
        addedWidgets = addedWidgets.filter(widget => widget.i != i);
        if (requiredWidget.dataSource === "MQTT" && requiredWidget.mqttDetails.topic.name) {
            if (!addedWidgets.find(widget => widget.dataSource === "MQTT" && widget.mqttDetails.topic.name === requiredTopicName)) {
                unsubscribeTopic(requiredTopicName)
                let topicIndex = subscribedTopics.indexOf(requiredTopicName)
                subscribedTopics.splice(topicIndex, 1)
            }
        }
        this.setState({ addedWidgets, subscribedTopics });
    }

    onCloseHandler = () => {
        this.setState({
            isLoading: false,
        }, () => {
            if (this.state.isSubmitSuccess) {
                this.props.history.push((this.state.deviceType) ? `/dashboards/${this.state.deviceType}` : `/dashboards`);
            }
        })
    }

    setStateFromChild = (newState, callback) => {
        if (typeof (newState) == "function") {
            this.setState(prevState => (newState(prevState)), () => callback ? callback(this.state) : null)
        }
        else
            this.setState((newState), () => callback ? callback(this.state) : null)
    }

    navigateToPageStudio = () => {
        this.props.history.push("/dashboards")
    }

    toggleColorPicker = (pickerId) => {
        let selectedColorPicker = this.state.selectedColorPicker ? null : pickerId;
        this.setState({ selectedColorPicker })
    }

    pageTypeChangeHandler = (pageType) => {
        pageType = pageType || this.state.tempPageType
        let pageName = pageStudioConstants.PAGE_TYPE_OPTIONS[pageType]
        this.setState({
            pageType,
            allWidgets: this.state.widgetMetaData.filter(widget => widget.pageTypes.includes(pageType)),
            addedWidgets: [],
            selectedWidget: "",
            isWidgetView: true,
            pageName
        }, () => {
            if (this.state.selectedConnector && this.state.pageType === "aggregate" && !this.state.allDeviceGroups[this.state.selectedConnector]) {
                this.props.getDeviceGroups(this.state.selectedConnector)
            }
        })
    }

    resetWidgetToDefaultSettings = () => {
        let addedWidgets = cloneDeep(this.state.addedWidgets)
        addedWidgets.map(widget => {
            if (widget.dataSource !== "DEFAULT") {
                widget.widgetData = this.state.widgetMetaData.find(variant => variant.widgetName === widget.widgetName).data
                if (["barChart", "lineChart", "lineChartMultiAxes"].includes(widget.widgetType)) {
                    widget.widgetDetails.customValues = getCustomValues(widget.widgetType, widget.widgetName, widget.widgetData)
                }
                widget.dataSource = "DEFAULT"
                widget.apiDetails = {}
                widget.mqttDetails = {}
                widget.apiId = ""
            }
        })
        this.setState({
            addedWidgets,
        }, () => this.connectorChangeHandler(this.state.tempConnector))
    }

    clearAllWidgets = () => {
        this.state.addedWidgets.length && this.setState({ addedWidgets: [] })
    }

    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>CreateOrEditPage</title>
                    <meta name="description" content="Description of CreateOrEditPage" />
                </Helmet>

                <header className="content-header d-flex">
                    <div className="flex-60">
                        <div className="d-flex">
                            <h6 className="previous" onClick={this.navigateToPageStudio}>Dashboards</h6>
                            <h6 className="active">{this.props.match.params.id ? "Edit" : "Add New"}</h6>
                        </div>
                    </div>
                    <div className="flex-40 text-right">
                        <div className="content-header-group">
                            <button className="btn btn-danger" disabled={!this.state.addedWidgets.length} onClick={this.clearAllWidgets}><i className="far fa-times mr-r-5"></i>Clear All</button>
                            <button className="btn btn-light" onClick={() => this.props.history.push('/dashboards')}><i className="fas fa-angle-left"></i></button>
                        </div>
                    </div>
                </header>

                {this.state.isLoading > 0 ?
                    <Loader /> :
                    <div className="content-body">
                        <div className="form-content-wrapper form-content-wrapper-left">
                            <div className="page-grid-wrapper">
                                <div className="grid-top-left-corner"></div>
                                <div className="grid-top-right-corner"></div>
                                <div className="grid-bottom-left-corner"></div>
                                <div className="grid-bottom-right-corner"></div>
                                <div className="page-grid-layout" id="reactGridBox">
                                    <GridLayout
                                        setState={this.setStateFromChild}
                                        state={this.state}
                                        addWidgetHandler={this.addWidgetHandler}
                                        removeWidgetHandler={this.removeWidgetHandler}
                                        onLayoutChange={this.onLayoutChange}
                                        onCloseHandler={this.onCloseHandler}
                                        parent="createOrEditPage"
                                        {...this.props}
                                    />
                                </div>
                            </div>
                            {this.state.widgetMenu ?
                                <WidgetsMenu
                                    state={this.state}
                                    setState={this.setStateFromChild}
                                    addWidgetHandler={this.addWidgetHandler}
                                    connectorChangeHandler={this.connectorChangeHandler}
                                    setWrapperRef={this.setWrapperRef}
                                    toggleColorPicker={this.toggleColorPicker}
                                    pageTypeChangeHandler={this.pageTypeChangeHandler}
                                    {...this.props}
                                /> :
                                <WidgetConfigWindow
                                    state={this.state}
                                    setState={this.setStateFromChild}
                                    mqttMessageHandler={this.mqttMessageHandler}
                                    setWrapperRef={this.setWrapperRef}
                                    toggleColorPicker={this.toggleColorPicker}
                                    {...this.props}
                                />}
                        </div>
                    </div>
                }
            </React.Fragment>
        );
    }
}

CreateOrEditPage.propTypes = {
    dispatch: PropTypes.func.isRequired
};

let allSelectors = {}
Object.entries(SELECTORS).map(([key, value]) => {
    allSelectors[key] = value()
})

const mapStateToProps = createStructuredSelector(allSelectors)

function mapDispatchToProps(dispatch) {
    let allActions = { dispatch }
    Object.entries(ACTIONS).map(([key, value]) => {
        allActions[key] = (...args) => dispatch(value(...args))
    })
    return allActions
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "CreateOrEditPage", reducer });
const withSaga = injectSaga({ key: "CreateOrEditPage", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(CreateOrEditPage);
