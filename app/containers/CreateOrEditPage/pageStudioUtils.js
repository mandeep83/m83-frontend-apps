//**PAGE STUDIO COMMON UTILS */
import cloneDeep from 'lodash/cloneDeep';
import { unsubscribeTopic } from '../../mqttConnection';
var unflatten = require('flat').unflatten;

const getParamName = (paramName, widgetType) => {
    if (widgetType === "lineChartMultiAxes" && paramName === "id")
        return "idCsv"
    else if (widgetType === "lineChart" && paramName === "attrName")
        return "attrNamesCsv"
    return paramName
}

const getParamValue = (value, paramName, isFromConfiguration) => {
    let valuesArray = window.location.pathname.split('/').filter((path, index) => index > 2)
    if (["id", "idCsv"].includes(paramName) && !isFromConfiguration)
        return valuesArray.join() || value || ""
    return value || ""
}

export const cleanChartData = (widgetType, data) => {
    switch (widgetType) {
        case "pieChart":
        case "gaugeChart":
            data = []
            break;
        case "lineChartMultiAxes":
        case "lineChart":
            if (data.chartData && data.chartData.length > 25) {
                data.chartData.shift()
            }
            break;
        case "table":
            if (typeof data.tableData === undefined) {
                data.tableData = [];
            }
            if (data.tableData && data.tableData.length > 25) {
                data.tableData.shift()
            }
            break;
        case "barChart":
            data.chartData = [];
            break;
    }
    return data
}

export const getMqttChartData = (value, type, data, messages, isLocalTimestamp, topic, connectorsList) => {
    let chartData = data
    let splitArry = [];
    let deviceDetails = connectorsList.find(temp => temp.id === topic.connector).properties.deviceIds.find(temp => temp.deviceId === topic.deviceId);
    value = Array.isArray(value) ? value : [value]
    value.map(temp => {
        let spilt = temp.value.split('.')
        splitArry.push({ key: spilt[spilt.length - 1], spiltArr: spilt, originalKey: temp.value });
    })

    let obj = {}
    if (type === "lineChartMultiAxes" || type === "lineChart" || type === "barChart" || type === "table") {
        chartData.availableAttributes = [];
    }
    splitArry.map((temp) => {
        let json = unflatten(messages[messages.length - 1]);
        temp.spiltArr.map((arr) => {
            json = json[arr]
        })
        switch (type) {
            case "pieChart":
                chartData.push({ category: temp.key, value: json });
                break;
            case "barChart":
                chartData.availableAttributes.push(temp.key);
                chartData.chartData.push({ category: temp.key, [temp.key]: json });
                break;
            case "counter":
                chartData = {
                    value: json
                }
                break;
            case "gaugeChart":
                chartData = [
                    {
                        "max": json > 100 ? Math.round(json * 2) : 100,
                        "min": 0,
                        "name": temp.key,
                        "unit": "",
                        "value": json,
                        "deviceName": deviceDetails.name || deviceDetails.deviceId
                    }
                ]
                break;
            case "lineChart":
                chartData.availableAttributes.push({ attr: temp.key, attrDisplayName: deviceDetails.name || deviceDetails.deviceId });
                chartData.seriesName = temp.key
                obj[temp.key] = json;
                break;
            case "lineChartMultiAxes":
                chartData.availableAttributes.push({ attr: temp.key, attrDisplayName: temp.key });
                chartData.seriesName = deviceDetails.name || deviceDetails.deviceId
                obj[temp.key] = json;
                break;
            case "table":
                chartData.availableAttributes.push(temp.key);
                obj[temp.key] = json;
                break;
        }
    })
    if (type === "lineChart" || type === "lineChartMultiAxes") {
        obj.timestamp = !Object.keys(messages[0]).includes("timestamp") ? new Date() : unflatten(messages[messages.length - 1]).timestamp;
        obj.category = new Date(obj.timestamp).toLocaleString('en-US', { timeZone: localStorage.timeZone })
        chartData.chartData.push(obj);
    }
    if (type === "table") {
        chartData.tableData.push(obj);
    }
    return chartData;
}

export const updateWidgetData = (widget, messages, connectorsList) => {
    let data = widget.widgetData,
        topic = widget.mqttDetails.topic.name
    if (!data || JSON.stringify(data) == "{}")
        data = getChartDataFormat(widget.widgetType);
    else
        data = cleanChartData(widget.widgetType, data);
    let response = cloneDeep(getMqttChartData(widget.mqttDetails.topic.selectedAttributes, widget.widgetType, data, messages[topic], widget.mqttDetails.isLocalTimestamp, widget.mqttDetails.topic, connectorsList))
    if (["barChart", "lineChart", "lineChartMultiAxes"].includes(widget.widgetType) && JSON.stringify(widget.widgetData.availableAttributes) !== JSON.stringify(response.availableAttributes)) {
        if (typeof Object.values(widget.widgetDetails.customValues)[0] == "object") {
            Object.entries(widget.widgetDetails.customValues).map(([key, val]) => {
                widget.widgetDetails.customValues[key] = val.value
            })
        }
        widget.widgetDetails.customValues = getCustomValues(widget.widgetType, widget.widgetName, response, widget.widgetDetails.customValues)
    }
    widget.widgetData = response;
    widget.isLoadingData = false;
    return widget
}

export const invokeApiHandler = (requiredWidget, isFromConfiguration, invokeApiAction, state) => {
    if (requiredWidget.apiId) {
        let url = requiredWidget.apiDetails.requestUri || state.allGatewayApis.find(match => match.id === requiredWidget.apiId).requestUri
        if (requiredWidget.apiDetails.pathParameters && requiredWidget.apiDetails.pathParameters.length > 0) {
            for (let i = 0; i < requiredWidget.apiDetails.pathParameters.length; i++) {
                if (requiredWidget.apiDetails.pathParameters[i].name !== "deviceGroupId" || !state.selectedDeviceGroup) {
                    let paramName = getParamName(requiredWidget.apiDetails.pathParameters[i].name, requiredWidget.widgetType)
                    let paramValue = getParamValue(requiredWidget.apiDetails.pathParameters[i].value, paramName, isFromConfiguration)
                    url += `${i == 0 ? "?" : "&"}${paramName}=${paramValue}`
                }
            }
            if (state.selectedDeviceGroup) {
                url += `&deviceGroupId=${state.selectedDeviceGroup}`
            }
            if (state.selectedTags && state.selectedTags.length) {
                let selectedTags = state.selectedTags.map(tag => tag.value).toString()
                url += `&tagsCsv=${selectedTags}`
            }
            if (requiredWidget.widgetType.includes("lineChart")) {
                url += `&isMultiLine=${!(requiredWidget.widgetType === "lineChartMultiAxes")}`
            }
            url += `&duration=${state.selectedDuration}`
        }
        invokeApiAction(url, requiredWidget.i, "GET");
    }
}

export const getCategoryInResponse = (response) => {
    let hasTimestamp = response.chartData && response.chartData[0] && (response.chartData[0].timestamp || response.chartData[0].timeStamp);
    hasTimestamp && response.chartData.map(item => {
        item.category = new Date(item.timeStamp || item.timestamp).toLocaleString('en-US', {timeZone: localStorage.timeZone})
    })
    return response;
}

export const flattenTopologyData = (widgetData) => {
    let arr = []
    function flattenObj(obj) {
        arr.push(obj)
        if (obj.connectedTo) {
            flattenObj(obj.connectedTo)
        }
    }
    flattenObj(widgetData)
    arr.map(el => delete el.connectedTo)
    return arr
}

export const handleApiData = (addedWidgets, apiData) => {
    addedWidgets = cloneDeep(addedWidgets)
    let requiredWidget = addedWidgets.find(widget => widget.i === apiData.index),
        widgetData = apiData.response.data,
        widgetType = requiredWidget.widgetType

    if (["barChart", "lineChart", "lineChartMultiAxes"].includes(widgetType)) {
        widgetData = getCategoryInResponse(widgetData)
        if (typeof Object.values(requiredWidget.widgetDetails.customValues)[0] == "object") {
            Object.entries(requiredWidget.widgetDetails.customValues).map(([key, val]) => {
                requiredWidget.widgetDetails.customValues[key] = val.value
            })
        }
        requiredWidget.widgetDetails.customValues = getCustomValues(requiredWidget.widgetType, requiredWidget.widgetName, widgetData, requiredWidget.widgetDetails.customValues)
    }

    requiredWidget.widgetData = widgetType === "topology" ? flattenTopologyData(widgetData) : widgetData
    requiredWidget.error = false
    requiredWidget.isLoadingData = false
    return addedWidgets
}


export const getChartDataFormat = (widgetType) => {
    let chartData = {};
    switch (widgetType) {
        case "pieChart":
        case "gaugeChart":
            chartData = []
            break;
        case "lineChartMultiAxes":
        case "lineChart":
        case "barChart":
            chartData = {
                availableAttributes: [],
                chartData: [],
            }
            break;
        case "table":
            chartData = {
                availableAttributes: [],
                tableData: [],
            }
            break;
    }
    return chartData
}

export const getMinMax = (widgetData) => {
    let min = 0, max = 100;
    if (widgetData && widgetData.chartData) {
        let arr = []
        widgetData.chartData.map(dataItem => {
            Object.entries(dataItem).map(([key, value]) => {
                if (widgetData.availableAttributes.some(attr => attr.attr === key) && typeof (value) === "number") {
                    arr.push(value)
                }
            })
        })
        let minVal = Math.min.apply(null, arr)
        let maxVal = Math.max.apply(null, arr)
        let diff = Math.abs(maxVal - minVal)
        min = minVal - Math.floor(diff / 10)
        max = maxVal + Math.ceil(diff / 10)
        if (Math.abs(max - min) > 2) {
            min += 1
            max -= 1
        }
    }
    return [min, max]
}

export const mqttDetailsValidation = (widget) => {
    let topicDetails = widget.mqttDetails.topic
    return Boolean(widget.dataSource === "MQTT" && topicDetails.selectedAttributes && (Array.isArray(topicDetails.selectedAttributes) ? topicDetails.selectedAttributes.length : Object.keys(topicDetails.selectedAttributes).length))
}

export const getCustomValues = (widgetType, widgetName, widgetData, rawCustomValues) => {
    let fontWeightOptions = [100, 300, 400, 500, 700, 900],
        legendPlacementOptions = [{ displayName: "Bottom", value: "bottom" }, { displayName: "Top", value: "top" }, { displayName: "Left", value: "left" }, { displayName: "Right", value: "right" }],
        zoomLevelOptions = [{ displayName: "World", value: 2 }, { displayName: "Country", value: 3.5 }, { displayName: "City", value: 11 }, { displayName: "Locality", value: 15 }],
        noCustomValues = (!rawCustomValues || (JSON.stringify(rawCustomValues) == "{}")),
        customValues;
    switch (widgetType) {
        case "lineChartMultiAxes":
        case "lineChart":
            customValues = {
                legendPlacement: { value: noCustomValues ? "bottom" : rawCustomValues.legendPlacement, type: "select", options: legendPlacementOptions },
                bulletVisibility: { value: noCustomValues ? true : rawCustomValues.bulletVisibility, type: "boolean" },
                bulletSize: { value: noCustomValues ? 8 : rawCustomValues.bulletSize, type: "number", parent: "bulletVisibility" },
                customColors: { value: noCustomValues ? false : rawCustomValues.customColors, type: "boolean" },
                scrollBarVisibility: { value: noCustomValues ? false : rawCustomValues.scrollBarVisibility, type: "boolean" },
                lineVisibility: { value: noCustomValues ? true : rawCustomValues.lineVisibility, type: "boolean" },
                lineThickness: { value: noCustomValues ? 2 : rawCustomValues.lineThickness, type: "number", parent: "lineVisibility" },
            }
            if (widgetType === "lineChart") {
                customValues.customizedValueAxis = { value: noCustomValues ? false : rawCustomValues.customizedValueAxis, type: "boolean" }
                let [min, max] = getMinMax(widgetData)
                customValues.valueAxisMin = { value: noCustomValues ? min : rawCustomValues.valueAxisMin, type: "number", parent: "customizedValueAxis" },
                    customValues.valueAxisMax = { value: noCustomValues ? max : rawCustomValues.valueAxisMax, type: "number", parent: "customizedValueAxis" },
                    customValues.valueAxisGridDistance = { value: noCustomValues ? 30 : rawCustomValues.valueAxisGridDistance, type: "number", parent: "customizedValueAxis" };
            }
            widgetData && widgetData.availableAttributes && widgetData.availableAttributes.map(attr => {
                customValues[attr.attr] = {
                    value: noCustomValues ? "#FF0000" : rawCustomValues[attr.attr],
                    type: "color",
                    parent: "customColors",
                    displayName: attr.attrDisplayName
                }
            })
            return customValues
        case "barChart":
            customValues = {
                barWidth: { value: noCustomValues ? 80 : rawCustomValues.barWidth, type: "number" },
                legendPlacement: { value: noCustomValues ? "bottom" : rawCustomValues.legendPlacement, type: "select", options: legendPlacementOptions },
            }
            if (widgetName === "3DColumnChart") {
                customValues.chartAngle = { value: noCustomValues ? 30 : rawCustomValues.chartAngle, type: "number" }
                customValues.chartDepth = { value: noCustomValues ? 30 : rawCustomValues.chartDepth, type: "number" }
            }
            if (widgetName === "basicBarChart") {
                customValues.columnTopRadius = { value: noCustomValues ? 10 : rawCustomValues.columnTopRadius, type: "number" }
            }
            customValues.customColors = { value: noCustomValues ? false : rawCustomValues.customColors, type: "boolean" };
            widgetData && widgetData.availableAttributes && widgetData.availableAttributes.map(({ attr, attrDisplayName }) => {
                customValues[attr] = {
                    value: noCustomValues ? "#0ba6ef" : rawCustomValues[attr],
                    type: "color",
                    parent: "customColors",
                    displayName: attrDisplayName
                }
            })
            return customValues
        case "gaugeChart":
            customValues = noCustomValues ? {
                rangeColors: {
                    isTwoColored: true,
                    colors: [{ hexCode: "#67b7dc" }, { hexCode: "#6771DC" }]
                }
            } : rawCustomValues
            return customValues
        case "pieChart":
            customValues = {
                legendPlacement: { value: noCustomValues ? "bottom" : rawCustomValues.legendPlacement, type: "select", options: legendPlacementOptions }
            }
            if (widgetName === "3dPieChart") {
                customValues.chartDepth = { value: noCustomValues ? 20 : rawCustomValues.chartDepth, type: "number" }
                customValues.chartAngle = { value: noCustomValues ? 10 : rawCustomValues.chartAngle, type: "number" }
            }
            return customValues
        case "counter":
            customValues = {
                textFontSize: { value: noCustomValues ? 14 : rawCustomValues.textFontSize, type: "number" },
                valueFontSize: { value: noCustomValues ? 24 : rawCustomValues.valueFontSize, type: "number" },
                textFontWeight: { value: noCustomValues ? 400 : rawCustomValues.textFontWeight, type: "select", options: fontWeightOptions },
                valueFontWeight: { value: noCustomValues ? 700 : rawCustomValues.valueFontWeight, type: "select", options: fontWeightOptions },
                contentAlignment: { value: noCustomValues ? "center" : rawCustomValues.contentAlignment, type: "select", options: ["center", "right", "left"] },
                textFontColor: { value: noCustomValues ? widgetName === "counter11" ? "#FFFFFF" : "#4a4a4a" : rawCustomValues.textFontColor, type: "color" },
                valueFontColor: { value: noCustomValues ? "#007bff" : rawCustomValues.valueFontColor, type: "color" },
                backgroundColor: { value: noCustomValues ? "#FFFFFF" : rawCustomValues.backgroundColor, type: "color" },
            }
            if (widgetName === "counter9") {
                customValues.iconColor = { value: noCustomValues ? "#FFFFFF" : rawCustomValues.iconColor, type: "color" }
            }
            if (widgetName !== "counter1" && widgetName !== "counter3") {
                customValues.patchColor = { value: noCustomValues ? "#007bff" : rawCustomValues.patchColor, type: "color" }
                customValues.valueFontColor = { value: noCustomValues ? widgetName === "counter9" || widgetName === "counter11" ? "#007bff" : "#FFFFFF" : rawCustomValues.valueFontColor, type: "color" }
            }
            return customValues
        case "label":
        case "header":
            return {
                textFontSize: { value: noCustomValues ? 13 : rawCustomValues.textFontSize, type: "number" },
                fontWeight: { value: noCustomValues ? 600 : rawCustomValues.fontWeight, type: "select", options: fontWeightOptions },
                backgroundColor: { value: noCustomValues ? widgetType === "label" ? "#FFFFFF" : "rgba(231, 244, 253, 1)" : rawCustomValues.backgroundColor, type: "color" },
                textColor: { value: noCustomValues ? widgetType === "label" ? "#4a4a4a" : "#4a4a4a" : rawCustomValues.textColor, type: "color" },
            }
        case "map":
            return {
                zoomLevel: { value: noCustomValues ? 2 : rawCustomValues.zoomLevel, type: "select", options: zoomLevelOptions },
            }
        default:
            return {}
    }
}

export const unsubscribeAllTopics = (subscribedTopics) => {
    subscribedTopics.map(topic => {
        unsubscribeTopic(topic)
    })
}

export const getActiveClass = (value, option) => {
    return value === option ? "active" : ""
}

let parameterConstructor = function (name, displayName, optionsKey, hidden, value, parent) {
    return {
        required: true,
        name,
        type: "select",
        displayName: `${displayName} :`,
        optionsKey,
        hidden,
        value,
        parent
    }
}

export const getApiTableData = (configWidgetType, identifierValue, isAggregatePage, connectorId, dataSourceValue) => {
    let id, attributes, lastAvailable, counterType, minVal, maxVal, controlCommand;
    let idWidgets = ["control", "weather", "barChart", "lineChart", "pieChart", "gaugeChart", "lineChartMultiAxes"]
    let attributesWidgets = ["barChart", "lineChart", "pieChart", "gaugeChart", "map"]
    let lastAvailableWidgets = ["table", "counter", "map"]

    let connectors = parameterConstructor("connectorId", "Connector", "connectorsList", true, connectorId)
    let dataSource = parameterConstructor("datasource", "Data Source", "dataCollections", true, dataSourceValue)
    let identifier = parameterConstructor("identifier", "Identifier", "attributes", true, identifierValue, "datasource", true)

    if (idWidgets.includes(configWidgetType)) {
        let name = isAggregatePage ? "deviceGroupId" : configWidgetType === "barChart" ? "idCsv" : "id"
        let displayName = isAggregatePage ? "Device Group" : "Device Name/Id"
        let optionsKey = isAggregatePage ? "deviceGroups" : "devices"
        id = parameterConstructor(name, displayName, optionsKey, false, null, "identifier",)
    }
    if (attributesWidgets.includes(configWidgetType)) {
        let parent = configWidgetType === "pieChart" ? "datasource" : "identifier"
        attributes = parameterConstructor("attrNamesCsv", "Attributes", "attributes", false, null, parent)
    }
    if (lastAvailableWidgets.includes(configWidgetType)) {
        lastAvailable = parameterConstructor("lastAvailable", "Offline Timing", "lastAvailableList", false, null, "identifier")
    }
    if (configWidgetType === "counter") {
        counterType = parameterConstructor("type", "Type", "counterTypes", false, null, "identifier")
    }
    else if (configWidgetType === "control") {
        controlCommand = parameterConstructor("command", "Command", "controlCommands", false, null, "datasource")
    }
    else if (configWidgetType === "gaugeChart") {
        minVal = { ...parameterConstructor("minVal", "Min Value", null, false, "0"), type: "int" }
        maxVal = { ...parameterConstructor("maxVal", "Max Value", null, false, "100"), type: "int" }
    }

    switch (configWidgetType) {
        case "pieChart":
            return [connectors, dataSource, identifier, id, attributes]
        case "deviceList":
        case "deviceMap":
            return [connectors, dataSource]
        case "table":
            return [connectors, dataSource, identifier, lastAvailable]
        case "lineChartMultiAxes":
        case "lineChart":
            return [connectors, dataSource, identifier, id, { ...attributes, name: configWidgetType === "lineChartMultiAxes" ? "attrNamesCsv" : "attrName" }]
        case "barChart":
            return [connectors, dataSource, identifier, id, attributes]
        case "map":
            return [connectors, dataSource, identifier, attributes, lastAvailable]
        case "counter":
            return [connectors, dataSource, identifier, counterType, { ...lastAvailable, required: false }]
        case "weather":
            return [connectors, dataSource, id, identifier]
        case "alarms":
            return [connectors, dataSource, identifier]
        case "control":
            delete id.parent
            return [connectors, dataSource, id, controlCommand]
        case "gaugeChart":
            return [connectors, dataSource, identifier, id, { ...attributes, name: "attrName" }, minVal, maxVal]
    }
}

export const commonParamChangeHandler = (event, paramIndex, isReactSelect, state) => {
    let addedWidgets = cloneDeep(state.addedWidgets),
        configWidget = addedWidgets[state.configWidgetIndex],
        apiDetails = configWidget.apiDetails,
        configWidgetType = configWidget.widgetType,
        requiredParam = apiDetails.pathParameters[paramIndex],
        isCommandChanged;
    if (isReactSelect) {
        apiDetails.pathParameters.map(param => {
            if (param.parent === event.id)
                param.value = ""
        })
        isCommandChanged = event.id === "command"
        requiredParam.value = Array.isArray(event.value) ? event.value.map(el => el.value).toString() : event.value.value;
        let counterTypeChanged = configWidgetType === "counter" && requiredParam.name === "type"
        if (counterTypeChanged) {
            let lastAvailableIndex = apiDetails.pathParameters.findIndex(param => param.name === "lastAvailable")
            let isOnlineOrOffline = ["OFFLINE", "ONLINE"].includes(requiredParam.value)
            apiDetails.pathParameters[lastAvailableIndex].required = isOnlineOrOffline
            if (isOnlineOrOffline) {
                apiDetails.pathParameters[lastAvailableIndex].displayName = `${startCase(requiredParam.value.toLowerCase())} Timing :`
            }
            apiDetails.pathParameters[lastAvailableIndex].value = requiredParam.value === "ALL" ? "0" : ""
        }
        else if (isCommandChanged) {
            configWidget.widgetDetails.configCommand = state.controlCommands.find(command => command.value === requiredParam.value).controlCommand
        }
    }
    else {
        let regexInt = /^[0-9\b]+$/;
        let regexFloat = /^[+-]?([0-9]+([.][0-9]*)?|[.][0-9]+)$/;
        let regexString = /^[a-zA-Z0-9!@#$%^&*()_+\-=\[\]{};': "\\|,.<>\/?]*$/;
        //let regexChar = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/;
        let type = apiDetails.pathParameters[paramIndex].type

        if (type === "int") {
            if (regexInt.test(event.currentTarget.value) || event.currentTarget.value === "") {
                apiDetails.pathParameters[paramIndex].value = event.currentTarget.value;
            }
        }
        else if (type === "float") {
            if (regexFloat.test(event.currentTarget.value) || event.currentTarget.value === "") {
                apiDetails.pathParameters[paramIndex].value = event.currentTarget.value;
            }
        }
        else if (type === "String") {
            if (regexString.test(event.currentTarget.value) || event.currentTarget.value === "") {
                apiDetails.pathParameters[paramIndex].value = event.currentTarget.value;
            }
        }
        else if (type === "char") {
            if (regexString.test(event.currentTarget.value) || event.currentTarget.value === "") {
                apiDetails.pathParameters[paramIndex].value = event.currentTarget.value;
            }
        }
        else {
            apiDetails.pathParameters[paramIndex].value = event.currentTarget.value;
        }
    }
    if (apiDetails.pathParameters[paramIndex].value && apiDetails.pathParameters[paramIndex].errorMessage) {
        apiDetails.pathParameters[paramIndex].errorMessage = false
    }
    return addedWidgets
}

export const validateVisibility = (parameter, configWidget) => {
    return !parameter.hidden && (parameter.name !== "lastAvailable" || configWidget.widgetType !== "counter" || ["OFFLINE", "ONLINE"].includes(configWidget.apiDetails.pathParameters.find(param => param.name === "type").value))
}

export const isParentFalse = ({ parent }, configWidget) => {
    let parentIndex = configWidget.apiDetails.pathParameters.findIndex(param => param.name === parent)
    return !configWidget.apiDetails.pathParameters[parentIndex].value
}

export const getOptionsArr = (parameter, configWidget, arr) => {
    if (parameter.name.startsWith("attr") && !["pieChart", "table"].includes(configWidget.widgetType)) {
        let identifierVal = configWidget.apiDetails.pathParameters.find(param => param.name === "identifier").value
        arr = arr.filter(option => option.value !== identifierVal)
    }
    return arr
}

export const getReactSelectValue = ({ value, name }, options) => {
    if (value) {
        if (name.includes("Csv")) {
            let arr = value.split(",");
            return options.filter(el => arr.includes(el.value))
        }
        return options.find(el => el.value === value) || {}
    }
    return name.includes("Csv") ? [] : {}
}