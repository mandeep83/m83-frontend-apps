export const WIDGET_WITHOUT_DATASOURCE = ["header", "images", "customWidgets", "label"]
export const API_COLLECTION_WIDGETS = ["lineChartMultiAxes", "lineChart", "barChart", "table", "gaugeChart", "pieChart", "map", "counter", "weather", "alarms", "deviceList", "deviceMap"]
export const THEMES = [
    "noTheme",
    "dataviz",
    "material",
    "kelly's",
    "dark",
    "frozen",
    "moonriseKingdom",
    "spiritedAway",
]
export const INDENTIFIER_WIDGETS = ["barChart", "pieChart", "gaugeChart", "lineChart", "lineChartMultiAxes", "map", "table", "counter", "weather", "alarms"]
export const LAST_AVAILABLE_LIST = [
    {
        label: "1 minute ago",
        value: 60,
    },
    {
        label: "5 minutes ago",
        value: 300,
    },
    {
        label: "10 minutes ago",
        value: 600,
    },
    {
        label: "15 minutes ago",
        value: 900,
    },
]
export const COUNTER_TYPES = [{
    label: "ALL",
    value: "ALL",
},
{
    label: "OFFLINE",
    value: "OFFLINE",
},
{
    label: "ONLINE",
    value: "ONLINE",
}]

export const PAGE_TYPE_OPTIONS = {
    deviceMap: "Device Map",
    deviceList: "Device List",
    deviceDetail: "Device Detail",
    deviceAggregate: "Device Aggregate",
    deviceAlarms: "Device Alarms",
    eventHistory: "Event History",
    uptimeInfo: "Uptime Info" ,
    deviceTrendsAndForecast: "Device Trends & Forecast",
    deviceMapGeoFencing: "Device Map (Geofencing)",
    devicePredictiveAnalytics: "Device Analytics (Predictive)",
}

export const WIDGETS_WITHOUT_HEADER = ["header", "images", "customWidgets", "label"]

export const MQTT_ENABLED_WIDGETS = ["lineChart", "gaugeChart", "lineChartMultiAxes"]

export const CUSTOMIZATION_WIDGETS = ["counter", "pieChart", "gaugeChart", "barChart", "lineChart", "lineChartMultiAxes", "header", "label", "image", "map"]

export const AUTO_REFRESH_OPTIONS = [
    {
        value: 0,
        label: "Never",
    }, {
        value: 10000,
        label: "Every 10 seconds",
    }, {
        value: 30000,
        label: "Every 30 seconds",
    }, {
        value: 60000,
        label: "Every 60 seconds",
    }
]