/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * Template
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { FormattedMessage } from "react-intl";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import NotificationModal from '../../components/NotificationModal/Loadable';
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import reducer from "./reducer";
import saga from "./saga";
import messages from "./messages";
import * as ACTIONS from './actions'
import * as SELECTORS from "./selectors";
import ConfirmModel from "../../components/ConfirmModel/Loadable";
import Loader from '../../components/Loader/Loadable';
import { getInitials } from '../../commonUtils'
import cloneDeep from "lodash/cloneDeep";
import JSONInput from "react-json-editor-ajrm/dist";



/* eslint-disable react/prefer-stateless-function */
export class Template extends React.Component {
    state = {
        searchKeyword: "",
        allTemplates: { Email: [], Alarm: [], Notification: [], Message: [] },
        templateImages: { Email: "addEmailTemplate.png", Alarm: "addAlarmTemplate.png", Notification: "addNotificationTemplate.png", Message: "addMsgTemplate.png" },
        isFetching: true,
        fileteredTemplates: {}
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.allTemplates) {
            let allTemplates = { Alarm: [], Email: [], Message: [], Notification: [] };
            nextProps.allTemplates.map(template => {
                switch (template.type) {
                    case "EMAIL":
                        allTemplates.Email.push(template)
                        break;
                    case "NOTIFICATION":
                        allTemplates.Notification.push(template)
                        break;
                    case "ALARM":
                        allTemplates.Alarm.push(template)
                        break;
                    default:
                        allTemplates.Message.push(template)
                }

            })
            return {
                allTemplates,
                fileteredTemplates: allTemplates,
                isFetching: false,
            }
        }
        if (nextProps.getAllTemplatesFailure) {
            return {
                isFetching: false,
                isOpen: true,
                message2: nextProps.getAllTemplatesFailure,
                modalType: "error",
            }
        }
        if (nextProps.deleteTemplateSuccess) {
            let allTemplates = cloneDeep(prevState.allTemplates),
                fileteredTemplates = cloneDeep(prevState.fileteredTemplates),
                deletedTicketType = nextProps.deleteTemplateSuccess.templateType,
                deletedTicketId = nextProps.deleteTemplateSuccess.id;
            allTemplates[deletedTicketType] = allTemplates[deletedTicketType].filter(template => template.id !== deletedTicketId)
            fileteredTemplates[deletedTicketType] = fileteredTemplates[deletedTicketType].filter(template => template.id !== deletedTicketId)
            return {
                allTemplates,
                isFetching: false,
                isOpen: true,
                message2: nextProps.deleteTemplateSuccess.message,
                modalType: "success",
                fileteredTemplates,
            }
        }
        if (nextProps.deleteTemplateFailure) {
            return {
                isFetching: false,
                isOpen: true,
                message2: nextProps.deleteTemplateFailure,
                modalType: "error",
            }
        }
        return null;
    }

    componentDidUpdate(prevprops, prevState) {
        if (this.props.allTemplates || this.props.getAllTemplatesFailure || this.props.deleteTemplateSuccess || this.props.deleteTemplateFailure) {
            this.props.resetToInitialState();
        }
    }

    componentDidMount() {
        this.props.getAllTemplates()
    }

    onCloseHandler = () => {
        this.setState({
            isFetching: false,
            isOpen: false,
            modalType: "",
            message2: "",
        })
    }

    cancelClicked = () => {
        this.setState({
            templateToBeDeleted: "",
            selectedType: "",
            confirmState: false,
        })
    }

    confirmDelete = () => {
        this.setState({ isFetching: true, confirmState: false }, () => this.props.deleteTemplate(this.state.templateToBeDeleted, this.state.selectedType))
    }

    deleteTemplateHandler = (id, selectedType) => {
        this.setState({ confirmState: true, templateToBeDeleted: id, selectedType })
    }

    editTemplateHandler = (template) => {
        this.props.history.push(`/addOrEditTemplate/${template.type}/${template.id}`);
    }

    handleSearch = (param) => {
        let allTemplates = cloneDeep(this.state.allTemplates);
        if (typeof (param) == "string")
            this.setState({ searchKeyword: "", fileteredTemplates: allTemplates })
        else {
            let fileteredTemplates = {};
            Object.entries(allTemplates).map(([type, templates]) => {
                fileteredTemplates[type] = templates.filter(template => template.name.toLowerCase().includes(param.target.value.toLowerCase()))
            })
            this.setState({ searchKeyword: param.target.value, fileteredTemplates })
        }
    }

    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>Template</title>
                    <meta name="description" content="Description of Template" />
                </Helmet>
                {this.state.isFetching ? <Loader /> :
                    <React.Fragment>
                        <header className="content-header d-flex">
                            <div className="flex-60">
                                <h6>Template</h6>
                            </div>
                            <div className="flex-40 text-right">
                                <div className="content-header-group">
                                    <div className="search-box">
                                        <span className="search-icon"><i className="far fa-search"></i></span>
                                        <input type="text" className="form-control" placeholder="Search..." value="" />
                                        {/*<button className="search-button" type="button"><i className="far fa-times"></i></button>*/}
                                    </div>
                                    <button className="btn btn-light"><i className="fad fa-list-ul"></i></button>
                                    <button className="btn btn-light"><i className="fad fa-table"></i></button>
                                </div>
                            </div>
                        </header>
    
                        <div className="content-body">
                        
                        </div>
                        
                        {/*<div className="pageBreadcrumb">*/}
                        {/*    <div className="row">*/}
                        {/*        <div className="col-8">*/}
                        {/*            <p><span>Templates</span></p>*/}
                        {/*        </div>*/}
                        {/*        <div className="col-4 text-right">*/}
                        {/*            <div className="flex h-100 justify-content-end align-items-center">*/}
                        {/*                {this.state.allTemplates && this.state.allTemplates.length > 0 &&*/}
                        {/*                    <div className="searchBox">*/}
                        {/*                        <input type="search" value={this.state.searchKeyword} className="form-control" placeholder="Search...." onChange={this.handleSearch} />*/}
                        {/*                        <i className="far fa-search searchBoxIcon"></i>*/}
                        {/*                        <button className="btn" onClick={() => this.handleSearch("clear")}><i className="far fa-times"></i></button>*/}
                        {/*                    </div>*/}
                        {/*                }*/}
                        {/*            </div>*/}
                        {/*        </div>*/}
                        {/*    </div>*/}
                        {/*</div>*/}

                        {/*<div className="outerBox pb-0">*/}
                        {/*    <div className="flex">*/}
                        {/*        {Object.entries(this.state.fileteredTemplates).map(([type, templates], index) => (*/}
                        {/*            <div className="flex-item fx-b25" key={index}>*/}
                        {/*                <div className="transformListBox">*/}
                        {/*                    <div className="transformListHeader templateHeader mb-10">*/}
                        {/*                        <p>{type}<span className="badge badge-pill badge-primary">{templates.length}</span></p>*/}
                        {/*                        {this.state.allTemplates[type].length > 0 &&*/}
                        {/*                            <button onClick={() => { this.props.history.push(`/addOrEditTemplate/${type.toUpperCase()}`) }} type="button" className="btn btn-transparent btn-transparent-primary"><i className="far fa-plus"></i></button>*/}
                        {/*                        }*/}
                        {/*                    </div>*/}
                        {/*                    <div className="transformListBody">*/}
                        {/*                        <ul className="pageList flex">*/}
                        {/*                            {templates.length ? templates.map(template => {*/}
                        {/*                                return (*/}
                        {/*                                    <li className="flex-item fx-b100" key={template.id}>*/}
                        {/*                                        <div className="pageListBox position-relative">*/}
                        {/*                                            <div className="pageListBoxImage cursor-pointer">*/}
                        {/*                                                <img src={`https://content.iot83.com/m83/other/${type.toLowerCase()}.png`} />*/}
                        {/*                                            </div>*/}
                        {/*                                            <div className="pageListBoxContent cursor-pointer">*/}
                        {/*                                                <h5>{template.name}</h5>*/}
                        {/*                                                {type.toUpperCase() === "ALARM" ? <h6><strong>Category :</strong>{template.metaInfo.alarmCategory}<i className="fas fa-circle mr-l-7" style={{ color: template.metaInfo.color }}></i></h6> : ''}*/}
                        {/*                                                <h6><strong>Description :</strong>{template.description}</h6>*/}
                        {/*                                                <h6><strong>Created By :</strong>*/}
                        {/*                                                    <span className="pageListBoxInitial">{getInitials(template.createdBy)}</span>*/}
                        {/*                                                    <span className="mr-r-10 mr-l-10">|</span>*/}
                        {/*                                                    <strong>Updated By :</strong><span className="pageListBoxInitial">{template.updatedBy ? getInitials(template.updatedBy) : "N/A"}</span>*/}
                        {/*                                                </h6>*/}
                        {/*                                            </div>*/}
                        {/*                                            <div className="pageListBoxFooter">*/}
                        {/*                                                <p><strong>Created At:</strong>{new Date(template.createdAt).toLocaleString('en-US')}</p>*/}
                        {/*                                                <p><strong>Updated At:</strong>{template.updatedAt ? new Date(template.updatedAt).toLocaleString('en-US') : "N/A"}</p>*/}
                        {/*                                                <div className="dropdown pageListDropdown">*/}
                        {/*                                                    <button type="button" className="btn dropdown-toggle"*/}
                        {/*                                                        data-toggle="dropdown"><i className="far fa-ellipsis-v"></i>*/}
                        {/*                                                    </button>*/}
                        {/*                                                    <ul className="dropdown-menu pageListDropdownMenu">*/}
                        {/*                                                        <li onClick={() => this.editTemplateHandler(template)}><i className="far fa-pen"></i>Edit</li>*/}
                        {/*                                                        <li onClick={() => this.deleteTemplateHandler(template.id, type)}><i className="far fa-trash-alt"></i>Delete</li>*/}
                        {/*                                                    </ul>*/}
                        {/*                                                </div>*/}
                        {/*                                            </div>*/}
                        {/*                                        </div>*/}
                        
                        {/*                                    </li>)*/}
                        {/*                            }) :*/}
                        {/*                                <li className="flex-item fx-b100">*/}
                        {/*                                    {this.state.allTemplates[type].length ? <p>No results found</p> : <div className="contentAddBox">*/}
                        {/*                                        <div className="contentAddBoxDetail">*/}
                        {/*                                            <div className="contentAddBoxImage">*/}
                        {/*                                                <img src={`https://content.iot83.com/m83/other/${this.state.templateImages[type]}`} />*/}
                        {/*                                            </div>*/}
                        {/*                                            <h6>No {type} template created yet.</h6>*/}
                        {/*                                            <button className="btn btn-primary" type="button" onClick={() => { this.props.history.push(`/addOrEditTemplate/${type.toUpperCase()}`) }}><i className="far fa-plus" />Add new</button>*/}
                        {/*                                        </div>*/}
                        {/*                                    </div>}*/}
                        {/*                                </li>}*/}
                        {/*                        </ul>*/}
                        {/*                    </div>*/}
                        {/*                </div>*/}
                        {/*            </div>*/}
                        {/*        ))}*/}
                        {/*    </div>*/}
                        {/*</div>*/}
                        {this.state.isOpen &&
                            <NotificationModal
                                type={this.state.modalType}
                                message2={this.state.message2}
                                onCloseHandler={this.onCloseHandler}
                            />
                        }

                        {
                            this.state.confirmState &&
                            <ConfirmModel
                                status={"delete"}
                                deleteName="the selected template"
                                confirmClicked={this.confirmDelete}
                                cancelClicked={this.cancelClicked}
                            />
                        }
                    </React.Fragment>}
            </React.Fragment>);
    }
}

Template.propTypes = {
    dispatch: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
    allTemplates: SELECTORS.getAllTemplatesSuccess(),
    getAllTemplatesFailure: SELECTORS.getAllTemplatesFailure(),
    deleteTemplateSuccess: SELECTORS.deleteTemplateSuccess(),
    deleteTemplateFailure: SELECTORS.deleteTemplateFailure(),
});

function mapDispatchToProps(dispatch) {
    return {
        dispatch,
        resetToInitialState: () => dispatch(ACTIONS.resetToInitialState()),
        getAllTemplates: () => dispatch(ACTIONS.getAllTemplates()),
        deleteTemplate: (id, type) => dispatch(ACTIONS.deleteTemplate(id, type)),
    };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "template", reducer });
const withSaga = injectSaga({ key: "template", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(Template);
