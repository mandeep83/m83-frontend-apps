/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
 * Direct selector to the template state domain
 */

const selectTemplateDomain = state => state.get("template", initialState);

export const getAllTemplatesSuccess = () =>
  createSelector(selectTemplateDomain, subState => subState.getAllTemplatesSuccess);
export const getAllTemplatesFailure = () =>
  createSelector(selectTemplateDomain, subState => subState.getAllTemplatesFailure);

export const deleteTemplateSuccess = () =>
  createSelector(selectTemplateDomain, subState => subState.deleteTemplateSuccess);
export const deleteTemplateFailure = () =>
  createSelector(selectTemplateDomain, subState => subState.deleteTemplateFailure);

export { selectTemplateDomain };
