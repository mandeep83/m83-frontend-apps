/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { apiCallHandler } from '../../api';
import { takeEvery, takeLatest } from 'redux-saga';
import * as CONSTANTS from "./constants";

export function* getAllTemplates(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_ALL_TEMPLATES_SUCCESS, CONSTANTS.GET_ALL_TEMPLATES_FAILURE, 'getAllTemplates')]
}
export function* deleteTemplate(action) {
  yield [apiCallHandler(action, CONSTANTS.DELETE_TEMPLATE_SUCCESS, CONSTANTS.DELETE_TEMPLATE_FAILURE, 'deleteTemplate')]
}

export function* watcherGetAllTemplates() {
  yield takeEvery(CONSTANTS.GET_ALL_TEMPLATES, getAllTemplates);
}

export function* watcherDeleteTemplate() {
  yield takeEvery(CONSTANTS.DELETE_TEMPLATE, deleteTemplate);
}

export default function* rootSaga() {
  yield [
    watcherGetAllTemplates(),
    watcherDeleteTemplate(),
  ]
}


