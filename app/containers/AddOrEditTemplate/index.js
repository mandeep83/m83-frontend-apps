/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * AddOrEditTemplate
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { FormattedMessage } from "react-intl";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import * as ACTIONS from './actions'
import * as SELECTORS from "./selectors";
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import reducer from "./reducer";
import saga from "./saga";
import cloneDeep from 'lodash/cloneDeep';
import NotificationModal from '../../components/NotificationModal/Loadable';
import Loader from '../../components/Loader/Loadable';
import { Controlled as CodeMirror } from 'react-codemirror2';
import ReactSelect from "react-select";
import TagsInput from "react-tagsinput";
 
/* eslint-disable react/prefer-stateless-function */
const codeMirrorOptions = {
    theme: 'material',
    lineNumbers: true,
    scrollbarStyle: null,
    lineWrapping: true,
    autoRefresh: true,
    autoFocus: true,
    autoIndent: true,
    indentUnit: 3,
    matchTags: { bothTags: true },
    autoCloseTags: true,
    extraKeys: { "Ctrl-Space": "autocomplete", "Ctrl-Q": function (cm) { cm.foldCode(cm.getCursor()); } },
    foldGutter: true,
    gutters: ["CodeMirror-foldgutter"],
}
export class AddOrEditTemplate extends React.Component {
    state = {
        payload: {
            "content": "",
            "description": "",
            "metaInfo": {
                alarmCategory: "",
                color: "#ff0000"
            },
            "name": "",
            "type": this.props.match.params.type,
        },
        isFetching: false,
        tags: ["@abc@"]
    }

    componentDidMount() {
        this.props.match.params.id && this.setState({ isFetching: true }, () => this.props.getTemplateById(this.props.match.params.id))
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.saveTemplateSuccess) {
            return {
                isOpen: true,
                message2: nextProps.saveTemplateSuccess,
                modalType: "success",
                payload: {
                    "content": "",
                    "description": "",
                    "metaInfo": {},
                    "name": "",
                    "type": "ALARM"
                },
            }
        }
        if (nextProps.saveTemplateFailure) {
            return {
                isOpen: true,
                message2: nextProps.saveTemplateFailure,
                modalType: "error",
            }
        }
        if (nextProps.getTemplateByIdSuccess) {
            return {
                isFetching: false,
                payload: nextProps.getTemplateByIdSuccess,
            }
        }
        if (nextProps.getTemplateByIdFailure) {
            return {
                isFetching: false,
                isOpen: true,
                message2: nextProps.getTemplateByIdFailure,
                modalType: "error",
            }
        }
        else return null;
    }

    componentDidUpdate(prevprops, prevState) {
        if (this.props.saveTemplateSuccess || this.props.saveTemplateFailure || this.props.getTemplateByIdSuccess || this.props.getTemplateByIdFailure) {
            this.props.resetToInitialState();
        }
    }

    onCloseHandler = () => {
        let redirectToTemplatePage = this.state.modalType === "success";
        this.setState({
            isFetching: false,
            isOpen: false,
            modalType: "",
            message2: "",
        }, () => redirectToTemplatePage && this.redirectToTemplatePage())
    }

    onChangeHandler = (param) => {
        let payload = cloneDeep(this.state.payload),
            errorCode = this.state.errorCode
        if (typeof (param) == "string") {
            payload.content = param;
            if (errorCode === 1)
                errorCode = 0
        }
        else if (param.target.id.startsWith("metaInfo_"))
            payload.metaInfo[param.target.id.replace("metaInfo_", "")] = param.target.value;

        else if (param.target.id == "name") {
            let regex = /^[$A-Z_][0-9A-Z_$]*$/i;
            if (regex.test(param.target.value) || param.target.value == "") {
                payload[param.target.id] = param.target.value;
            }
        }
        
        else
            payload[param.target.id] = param.target.value;
        if (event.target.id === "type")
            payload.content = ""
        this.setState({ payload, errorCode });
    }


    saveTemplate = (event) => {
        event.preventDefault();
        let payload = cloneDeep(this.state.payload);
        if (payload.type !== "ALARM" && !payload.content) {
            this.setState({ errorCode: 1 })
            return;
        }
        if (payload.type === "ALARM")
            delete payload.content;
        else
            delete payload.metaInfo
        this.setState({ isFetching: true }, () => this.props.saveTemplate(payload))

    }

    redirectToTemplatePage = () => {
        this.props.history.push('/template')
    }

    tagsInputChangeHandler = tags => {
        this.setState({ tags })
    }

    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>AddOrEditTemplate</title>
                    <meta name="description" content="Description of AddOrEditTemplate" />
                </Helmet>
                {this.state.isFetching ? <Loader /> : <React.Fragment>
                    <div className="pageBreadcrumb">
                        <div className="row">
                            <div className="col-8">
                                <p>
                                    <span className="previousPage" onClick={this.redirectToTemplatePage}>Templates</span>
                                    <span className="active">Add New</span>
                                </p>
                            </div>
                            <div className="col-4 text-right">
                                <div className="flex h-100 justify-content-end align-items-center">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="outerBox pb-0">
                        <form className="contentForm pd-0" onSubmit={this.saveTemplate}>
                            <div className="contentFormDetail border-r-0">
                                <div className="flex pd-t-15">
                                    <div className="flex-item fx-b50 pd-r-7">
                                        <div className="form-group">
                                            <input type="text" className="form-control" id="name" placeholder="Name" value={this.state.payload.name} onChange={this.onChangeHandler} required />
                                            <label className="form-group-label">Name :
                                            <span className="requiredMark"><i className="fa fa-asterisk"></i></span>
                                            </label>
                                        </div>
                                        <div className="form-group">
                                            <select className="form-control" id="type" disabled defaultValue={this.state.payload.type} required>
                                                <option value="ALARM">Alarm</option>
                                                <option value="EMAIL">Email</option>
                                                <option value="MESSAGE">Message</option>
                                                <option value="NOTIFICATION">Notification</option>
                                            </select>
                                            <label className="form-group-label">Type :
                                            <span className="requiredMark"><i className="fa fa-asterisk"></i></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div className="flex-item fx-b50  pd-l-7">
                                        <div className="form-group">
                                            <textarea rows="4" className="form-control" id="description" placeholder="Description" value={this.state.payload.description} onChange={this.onChangeHandler} />
                                            <label className="form-group-label">Description :
                                        </label>
                                        </div>
                                    </div>
                                    {/*--------tepmlate-----------------*/}
                                    {this.state.payload.type !== "ALARM" ?
                                        <div className="flex-item fx-b100">
                                            <div className="form-group mr-b-0">
                                                <label className="form-label">Default Variables :</label>
                                                <ul className="listStyleNone">
                                                    <li className="mr-r-5 d-inline"><span className="badge alert alert-primary">@username@</span></li>
                                                    <li className="mr-r-5 d-inline"><span className="badge alert alert-primary">@tenantname@</span></li>
                                                    <li className="mr-r-5 d-inline"><span className="badge alert alert-primary">@id@</span></li>
                                                    <li className="mr-r-5 d-inline"><span className="badge alert alert-primary">@attribute@</span></li>
                                                    <li className="mr-r-5 d-inline"><span className="badge alert alert-primary">@currentvalue@</span></li>
                                                    <li className="mr-r-5 d-inline"><span className="badge alert alert-primary">@threshold@</span></li>
                                                    <li className="mr-r-5 d-inline"><span className="badge alert alert-primary">@iterations@</span></li>
                                                    <li className="mr-r-5 d-inline"><span className="noteText f-14">( * These attributes will be replaced by actual values)</span></li>
                                                </ul>
                                            </div>
                                            <div className="form-group">
                                                <label className="form-label">Additional Variables :</label>
                                                <TagsInput value={this.state.tags} onChange={this.tagsInputChangeHandler} />
                                            </div>
                                            {this.state.payload.type === "MESSAGE" || this.state.payload.type === "NOTIFICATION" ?
                                                <div className="form-group">
                                                    <textarea required rows="6" className="form-control" id="content" placeholder="Message Template" value={this.state.payload.content} onChange={this.onChangeHandler} />
                                                    <label className="form-group-label">{this.state.payload.type == "MESSAGE" ? "Message" : "Notification"} Template :<span className="requiredMark"><i className="fa fa-asterisk"></i></span></label>
                                                </div> : ''}
                                        </div> : ''}
                                    {/*----------end--------*/}

                                    {this.state.payload.type === "EMAIL" ?
                                        <React.Fragment>
                                            <div className="flex-item fx-b50 pd-r-7">
                                                <div className="templateCodeEditor">
                                                    <p>HTML {this.state.errorCode === 1 && <span className="text-danger"> Please add content for the template.</span>}</p>
                                                    <button type="button" className="btn editor-code-zoom" name="html" onClick={() => this.setState({ showPreview: "HTML" })}>
                                                        <i name="html" className="far fa-expand text-white"></i>
                                                    </button>
                                                    <div className="editor-code">
                                                        {/*<button type="button" className="btn btn-transparent btn-transparent-success" onClick={()=> this.setState({showPreview : true})}>Preview</button>*/}
                                                        <CodeMirror
                                                            value={this.state.payload.content}
                                                            options={{
                                                                mode: 'htmlmixed',
                                                                ...codeMirrorOptions,
                                                            }}
                                                            onBeforeChange={(editor, data, html) => { this.onChangeHandler(html) }}
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="flex-item fx-b50 pd-l-7">
                                                <div className="templateCodeEditor outputBox">
                                                    <section className="widgetPreview">
                                                        <p>Output</p>
                                                        <button type="button" className="btn" onClick={() => this.setState({ showPreview: "OUTPUT" })}><i className="far fa-expand text-dark"></i></button>
                                                        <iframe srcDoc={this.state.payload.content} />
                                                    </section>
                                                </div>
                                            </div>
                                        </React.Fragment>
                                        : this.state.payload.type === "ALARM" ?
                                            <React.Fragment>
                                                <div className="flex-item fx-b50 pd-r-7">
                                                    <div className="form-group">
                                                        <input type="text" className="form-control" id="metaInfo_alarmCategory" placeholder="Category Name" value={this.state.payload.metaInfo.alarmCategory} onChange={this.onChangeHandler} required />
                                                        <label className="form-group-label">Alarm Category:
                                                        <span className="requiredMark"><i className="fa fa-asterisk"></i></span>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div className="flex-item fx-b30 pd-l-7">
                                                    <div className="form-group colorInput">
                                                        <label className="form-label">Color Picker :</label><input type="color" className="form-control" id="metaInfo_color" placeholder="Color Picker" onChange={this.onChangeHandler} value={this.state.payload.metaInfo.color} required />
                                                    </div>
                                                </div>
                                            </React.Fragment> : ""
                                        // <div className="flex-item fx-b100">
                                        //     <div className="form-group">
                                        //         <textarea rows="3" className="form-control" id="content" placeholder="Placeholder" value={this.state.payload.content}  onChange={this.onChangeHandler} required/>
                                        //         <label className="form-group-label">Contents :
                                        //             <span className="requiredMark"><i className="fa fa-asterisk"></i></span>
                                        //         </label>
                                        //     </div>
                                        // </div>
                                    }
                                </div>
                            </div>

                            <div className="bottomButton">
                                <button name="button" className="btn btn-success">{this.props.match.params.id ? "Update" : "Save"}</button>
                                <button type="button" name="button" className="btn btn-dark" onClick={this.redirectToTemplatePage}>Cancel</button>
                            </div>
                        </form>
                    </div>
                </React.Fragment>}

                {this.state.showPreview &&
                    <div className="widgetFullViewModal widgetModalTab">
                        <div className="widgetFullViewBody pd-0">
                            <ul className="nav nav-tabs widgetCodeBoxTab mr-b-0">
                                <li className="nav-item">
                                    <a onClick={() => this.setState({ showPreview: "HTML" })} className={this.state.showPreview === "HTML" ? "nav-link active" : "nav-link"}>HTML</a>
                                </li>
                                <li className="nav-item">
                                    <a onClick={() => this.setState({ showPreview: "OUTPUT" })} className={this.state.showPreview !== "HTML" ? "nav-link active" : "nav-link"}>Output</a>
                                </li>
                            </ul>
                            {this.state.showPreview === "HTML" ?
                                <div className="templateCodeEditor">
                                    <p>HTML {this.state.errorCode === 1 &&
                                        <span className="text-danger"> Please add content for the template.</span>}</p>
                                    <div className="editor-code">
                                        <button type="button" className="btn"><i className="far fa-compress text-white"
                                            onClick={() => this.setState({ showPreview: false })}></i>
                                        </button>
                                        <CodeMirror
                                            value={this.state.payload.content}
                                            options={{
                                                mode: 'htmlmixed',
                                                ...codeMirrorOptions,
                                            }}
                                            onBeforeChange={(editor, data, html) => {
                                                this.onChangeHandler(html)
                                            }}
                                        />
                                    </div>
                                </div>
                                :
                                <div className="templateCodeEditor outputBox">
                                    <p>Output</p>
                                    <section className="widgetPreview">
                                        <button type="button" className="btn"><i className="far fa-compress text-dark" onClick={() => this.setState({ showPreview: false })}></i></button>
                                        <iframe srcDoc={this.state.payload.content} />
                                    </section>
                                </div>
                            }
                        </div>
                    </div>
                }

                {this.state.isOpen &&
                    <NotificationModal
                        type={this.state.modalType}
                        message2={this.state.message2}
                        onCloseHandler={this.onCloseHandler}
                    />
                }

            </React.Fragment>
        );
    }
}

AddOrEditTemplate.propTypes = {
    dispatch: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
    saveTemplateSuccess: SELECTORS.saveTemplateSuccess(),
    saveTemplateFailure: SELECTORS.saveTemplateFailure(),
    getTemplateByIdSuccess: SELECTORS.getTemplateByIdSuccess(),
    getTemplateByIdFailure: SELECTORS.getTemplateByIdFailure(),
});

function mapDispatchToProps(dispatch) {
    return {
        dispatch,
        saveTemplate: (payload) => dispatch(ACTIONS.saveTemplate(payload)),
        getTemplateById: (id) => dispatch(ACTIONS.getTemplateById(id)),
        resetToInitialState: () => dispatch(ACTIONS.resetToInitialState()),

    };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "addOrEditTemplate", reducer });
const withSaga = injectSaga({ key: "addOrEditTemplate", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(AddOrEditTemplate);
