/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/*
 *
 * AddOrEditTemplate constants
 *
 */

export const SAVE_TEMPLATE = "app/AddOrEditTemplate/SAVE_TEMPLATE";
export const SAVE_TEMPLATE_SUCCESS = "app/AddOrEditTemplate/SAVE_TEMPLATE_SUCCESS";
export const SAVE_TEMPLATE_FAILURE = "app/AddOrEditTemplate/SAVE_TEMPLATE_FAILURE";

export const GET_TEMPLATE_BY_ID = "app/AddOrEditTemplate/GET_TEMPLATE_BY_ID";
export const GET_TEMPLATE_BY_ID_SUCCESS = "app/AddOrEditTemplate/GET_TEMPLATE_BY_ID_SUCCESS";
export const GET_TEMPLATE_BY_ID_FAILURE = "app/AddOrEditTemplate/GET_TEMPLATE_BY_ID_FAILURE";

export const RESET_TO_INITIAL_STATE = "app/AddOrEditTemplate/RESET_TO_INITIAL_STATE";