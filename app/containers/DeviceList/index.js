/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * DeviceList
 *
 */

import React, { Fragment } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import ReactSelect from "react-select";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import { allSelectors as SELECTORS } from "./selectors";
import { allActions as ACTIONS } from './actions'
import reducer from "./reducer";
import saga from "./saga";
import { isEqual, cloneDeep } from 'lodash';
import AddNewButton from "../../components/AddNewButton/Loadable"
import Loader from "../../components/Loader/Loadable";
import SearchBarWithSuggestion from "../../components/SearchBarWithSuggestion";
import CollapseOrShowAllFilters from "../../components/CollapseOrShowAllFilters";
import ListingTable from "../../components/ListingTable/Loadable";
import { convertTimestampToDate, getTimeDifference, isNavAssigned } from "../../commonUtils";



let DEFAULT_FILTERS = {
    keyword: "",
    groupId: [],
    tagId: [],
    alarmCategories: [],
}
/* eslint-disable react/prefer-stateless-function */
export const ALARM_CATEGORIES = [
    {
        name: "critical",
        displayName: "Critical",
        className: "bg-critical",
        icon: "far fa-exclamation-triangle",
    },
    {
        name: "major",
        displayName: "Major",
        className: "bg-major",
        icon: "far fa-exclamation-square",
    },
    {
        name: "minor",
        displayName: "Minor",
        className: "bg-minor",
        icon: "far fa-exclamation-circle",
    },
    {
        name: "warning",
        displayName: "Warning",
        className: "bg-alarm-warning",
        icon: "far fa-exclamation",
    },
]

let STATUS_TYPES = [
    {
        type: "online",
        displayName: "Online",
    },
    {
        type: "offline",
        displayName: "Offline",
    },
    {
        type: "not-connected",
        displayName: "Not Connected",
    },
]

const status = {
    "online": <span className="badge badge-pill alert-success list-view-pill"><i className="fad fa-circle mr-r-5"></i>Online</span>,
    "offline": <span className="badge badge-pill alert-danger list-view-pill"><i className="fad fa-circle mr-r-5"></i>Offline</span>,
    "not-connected": <span className="badge badge-pill alert-dark list-view-pill"><i className="fad fa-circle mr-r-5"></i>Not-connected</span>
}

const cardViewStatus = {
    "online": <span className="alert alert-success mr-r-7"><i className="fad fa-circle mr-r-5 text-success"></i>Online</span>,
    "offline": <span className="alert alert-danger mr-r-7"><i className="fad fa-circle mr-r-5 text-danger"></i>Offline</span>,
    "not-connected": <span className="alert alert-dark mr-r-7"><i className="fad fa-circle mr-r-5 text-dark"></i>Not-connected</span>
}

const getTimeDifferenceClasses = {
    "online": "text-green",
    "offline": "text-red",
    "not-connected": "text-gray"
}

export class DeviceList extends React.Component {
    state = { 
        filterExpand: false,
        toggleView:true,
        deviceTypeList: [],
        isFetching: true,
        deviceGroups: [],
        filters: cloneDeep(DEFAULT_FILTERS),
        deviceTags: [],
        allDevices: [],
        selectedDeviceTypes: [],
    }

    shouldComponentUpdate(nextProps, nextState) {
        return !isEqual(this.props.data, nextProps.data) || !isEqual(nextState, this.state) || !isEqual(nextProps.widgetProperties, this.props.widgetProperties) || nextProps.timeZone !== this.props.timeZone
    }

    componentDidMount() {
        this.props.deviceTypeList()
    }

    static getDerivedStateFromProps(nextProps, state) {
        let newProp = Object.keys(SELECTORS).find(prop => nextProps[prop])
        if (newProp) {
            let propData = nextProps[newProp].error ? nextProps[newProp].error : nextProps[newProp].response;

            if (nextProps.deviceTypeListSuccess) {
                let selectedDeviceTypes = state.selectedDeviceTypes
                let deviceTypeList = propData;
                if (nextProps.location.state) {
                    let selectedDeviceDetails = deviceTypeList.find(deviceType => deviceType.id === nextProps.location.state.selectedConnectorId);
                    selectedDeviceTypes = [{ label: selectedDeviceDetails.name || selectedDeviceDetails.id, value: nextProps.location.state.selectedConnectorId }]
                }
                else if (deviceTypeList.length) {
                    let objFilterList = []
                    deviceTypeList.forEach(i => {
                        let objFilter = {}
                        objFilter.label = i.name
                        objFilter.value = i.id
                        objFilterList.push(objFilter);
                    })
                    selectedDeviceTypes = objFilterList
                    // selectedDeviceTypes = [{label: deviceTypeList[0].name, value:  deviceTypeList[0].id}]
                }
                return { deviceTypeList, isFetching: false, selectedDeviceTypes }
            } else if (nextProps.deviceTypeListFailure) {
                return { isFetching: false }
            } else if (nextProps.getDeviceTagsSuccess) {
                return {
                    isFetchingTags: false,
                    deviceTags: propData,
                }
            } else if (nextProps.getAllDevicesSuccess) {
                let allDevices = propData;
                allDevices.map(device => {
                    device.displayName = device.name ? device.name : device.id;

                });
                return {
                    isFetchingDevices: false,
                    allDevices,
                    isFetching: false,
                }
            } else if (nextProps.getAllDevicesFailure) {
                nextProps.showNotification("error", propData, () => {
                })
                return {
                    isFetchingDevices: false,
                    isFetching: false,
                }
            } else if (nextProps.getDeviceTagsFailure) {
                return {
                    isFetchingTags: false,
                }
            } else if (newProp.includes("Failure")) {
                nextProps.showNotification("error", propData, () => {
                })
            }
        }
        return null
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.state.selectedDeviceTypes !== prevState.selectedDeviceTypes) {
            this.connectorChangeHandler()
        }
        let newProp = Object.keys(SELECTORS).find(prop => this.props[prop])
        if (newProp) {
            this.props.resetToInitialStateProps(newProp)
        }
    }

    filterChangeHandler(key, value) {
        let filters = cloneDeep(this.state.filters)
        let previouslyCheckedIndex = filters[key].indexOf(value)
        if (Array.isArray(value)) {
            filters[key] = []
        } else {
            if (previouslyCheckedIndex != -1) {
                filters[key].splice(previouslyCheckedIndex, 1)
            } else {
                filters[key].push(value)
            }
        }
        this.setState({
            filters
        })
    }

    statusFilterChangeHandler = (statusType) => {
        this.setState(prevState => ({
            status: statusType === prevState.status ? "" : statusType
        }))
    }
    
    getFilteredItems = () => {
        let allItems = this.state.allDevices
        if (Object.values(this.state.filters).some(filter => filter.length)) {
            return allItems.filter(device => {
                let groupIdValidation = !this.state.filters.groupId.length || this.state.filters.groupId.includes(device.groupId)
                let tagIdValidation = !this.state.filters.tagId.length || this.state.filters.tagId.includes(device.tagId)
                let keywordValidation = !this.state.filters.keyword || (([(device.name || "").toLowerCase(), device.id.toLowerCase()].includes(this.state.filters.keyword.toLowerCase()) || device.id.toLowerCase().includes(this.state.filters.keyword.toLowerCase())))
                let alarmCategoryValidation = !this.state.filters.alarmCategories.length || this.state.filters.alarmCategories.some(category => device.alarmsCount[category])
                return groupIdValidation && tagIdValidation && keywordValidation && alarmCategoryValidation
            })
        }
        return allItems
    }

    filterExpandCollapseHandler = () => {
        this.setState({
            filterExpand: this.state.filterExpand ? false : true
        })
    }
    
    connectorChangeHandler = (isForRefreshing) => {
        let deviceGroups = [],
            deviceTags = [],
            payload = {
                deviceTypeIds: [],
            };

        if (isForRefreshing && this.state.selectedDeviceTypes.length == 0) {
            this.setState({
                isFetchingDevices: true,
                isFetching: true,
            }, () => {
                this.props.getAllDevices(payload)
            })
        }

        if (this.state.selectedDeviceTypes.length > 0) {
            this.state.selectedDeviceTypes.map(deviceType => {
                let selectedDeviceType = this.state.deviceTypeList.filter(connector => connector.id === deviceType.value)[0];
                Array.prototype.push.apply(deviceGroups, selectedDeviceType.deviceGroups);
                Array.prototype.push.apply(deviceTags, selectedDeviceType.tags);
            })
            let stateObj = isForRefreshing ? {
                isFetchingDevices: true,
                isFetching: true,
            } : {
                    isFetchingDevices: true,
                    allDevices: [],
                    filters: cloneDeep(DEFAULT_FILTERS),
                    status: "",
                    deviceGroups,
                    deviceTags,
                };
            this.state.selectedDeviceTypes.map(deviceType => {
                payload.deviceTypeIds.push(deviceType.value);
            })

            this.setState(stateObj, () => {
                this.props.getAllDevices(payload)
            })
        } else {
            this.setState({
                allDevices: [],
                deviceGroups,
                deviceTags,
                status: '',
                filters: cloneDeep(DEFAULT_FILTERS)
            })
        }

    }

    getActiveClass = (key, value) => {
        return this.state.filters[key].includes(value) ? "active" : ""
    }

    searchHandler = (value) => {
        let filters = cloneDeep(this.state.filters)
        filters.keyword = value
        this.setState({ filters })
    }

    getDeviceTypes = () => {
        let deviceTypeList = this.state.deviceTypeList,
            list = [];
        deviceTypeList.map(deviceType => {
            list.push({
                label: deviceType.name,
                value: deviceType.id,
            })
        })
        return list;
    }

    deviceTypeChangeHandler = (selectedDeviceTypes) => {
        this.setState({
            selectedDeviceTypes,
        })
    }

    copyToClipboard = (id) => {
        navigator.clipboard.writeText(id)
        this.setState({
            type: "success",
            isOpen: true,
            message2: 'Copied to Clipboard successfully !',
        })
    }

    redirectToLiveLocationPage = (e, { deviceTypeId, id, deviceTypeImage }) => {
        e.stopPropagation();
        let url = `/locationTracking/${deviceTypeId}/${id}`
        window.open(url, '_blank');
    }

    getColumns = (isGatewayColumnRequired) => {
        let width = isGatewayColumnRequired ? 10 : 12.5;
        let columns = [
            {
                Header: "Device Detail", width: 20,
                cell: (row) => (
                    <React.Fragment>
                        {this.state.deviceTypeList.find(connector => connector.id === row.deviceTypeId).assetType === "Fixed" ?
                            <span className="list-git-icon alert-warning" data-tooltip data-tooltip-text="Fixed" data-tooltip-place="bottom"><i className="fas fa-map-marker-alt"></i></span>:
                            <span className="list-git-icon alert-info" data-tooltip data-tooltip-text="Movable" data-tooltip-place="bottom"><i className="fas fa-route mr-2"></i></span>
                        }
                        <div className="list-view-icon">
                            <img src={row.deviceTypeImage || "https://content.iot83.com/m83/misc/device-type-fallback.png"}></img>
                        </div>
                        <div className="list-view-icon-box">
                            <h6 className="text-theme fw-600">
                                <button className="btn-transparent btn-transparent-green copy-button" data-tooltip data-tooltip-text="Copy" data-tooltip-place="bottom" onClick={() => this.copyToClipboard(`${row.id}`)}>
                                    <i className="far fa-copy"></i>
                                </button>
                                {row.displayName}
                            </h6>
                            <p>{row.location ? row.location : "-"}</p>
                        </div>
                    </React.Fragment>
                ),
                accessor: "displayName",
                filterable: true,
                sortable: true,
            },
            {
                Header: "Device Type", width, cell: row => (
                    <h6>{row.deviceTypeName}</h6>
                ),
                accessor: "deviceTypeName",
                filterable: true,
                sortable: true,
            },
            {
                Header: "Group", width, cell: (row) => <h6>{row.groupName}</h6>,
                accessor: "groupName",
                filterable: true,
                sortable: true,
            },
            {
                Header: "Tag", width,
                cell: (row) => <span className="badge badge-pill badge-light list-view-badge-pill" style={{ color: row.tagColor }}>{row.tagName}</span>,
                accessor: "tagName",
                filterable: true,
                sortable: true,
            },
            {
                Header: "Last Reported", width: 15, cell: (row) => (
                    <React.Fragment>
                        <h6>{convertTimestampToDate(row.lastReportedAt)}</h6>
                        <p className={getTimeDifferenceClasses[row.status]}>{getTimeDifference(row.lastReportedAt)}</p>
                    </React.Fragment>
                ),
            },
            {
                Header: "Status", width, cell: (row) => <React.Fragment>{status[row.status]}</React.Fragment>,
                accessor: "status",
                filterable: true,
                sortable: true,
                filter: ({ value, onChangeHandler, accessor }) =>
                    <select
                        className="form-control"
                        onChange={event => onChangeHandler(event, accessor)}
                        value={value ? value : 'all'}
                    >
                        <option value="">All</option>
                        <option value="online">Online</option>
                        <option value="offline">Offline</option>
                        <option value="not-connected">Not Connected</option>
                    </select>
            },
            {
                Header: "Other Info", width: 15, cell: (row) => {
                    let totalAlarms = row.alarmsCount.critical + row.alarmsCount.major + row.alarmsCount.minor + row.alarmsCount.warning
                    let alarmCount = totalAlarms
                    let tooltipVisibility = false
                    if (Math.floor((alarmCount) / 1000) > 0) {
                        if (alarmCount % 1000 == 0) {
                            alarmCount = Math.floor(alarmCount / 1000) + "K"
                        } else {
                            alarmCount = Math.floor(alarmCount / 1000) + "K+"
                            tooltipVisibility = true
                        }
                    }
                    return (
                        <div className="button-group">
                            {totalAlarms ?
                                <div className="list-view-alarm-count">
                                    <i className="fad fa-bell text-orange"></i>
                                    <span data-tooltip={tooltipVisibility} data-tooltip-text={totalAlarms} data-tooltip-place="bottom" className="badge badge-danger">{alarmCount}</span>
                                </div>
                                :
                                <div className="list-view-alarm-count">
                                    <i className="fad fa-bell"></i>
                                    <span className="badge badge-light">0</span>
                                </div>
                            }
                            <button className="btn-transparent btn-transparent-yellow mr-l-20" data-tooltip data-tooltip-text="Debug" data-tooltip-place="bottom" onClick={() => isNavAssigned('deviceDiagnostics') && window.open(`/deviceDiagnostics/${row.deviceTypeId}/${row.id}`, '_blank')}><i className="far fa-bug"></i></button>
                            <button className="btn-transparent btn-transparent-cyan" data-tooltip data-tooltip-text="Live Location Tracking" data-tooltip-place="bottom" disabled={this.state.deviceTypeList.find(deviceType => deviceType.id === row.deviceTypeId)['assetType'] !== 'Movable'} onClick={(e) => this.redirectToLiveLocationPage(e, row)}><i className="fal fa-map-marker-alt"></i></button>
                            {this.state.deviceTypeList.find(connector => connector.id === row.deviceTypeId).isGateway && !row.gatewayId &&
                            <div className="list-view-alarm-count">
                                <i className="fad fa-router text-indigo"></i>
                            </div>
                            }
                        </div>
                    )
                }
            },
        ]
        if(isGatewayColumnRequired){
            columns.splice(2, 0, {
                Header: "Gateway Id", width: 10, cell: row => (
                    <h6>{row.gatewayId || "-"}</h6>
                ),
                accessor: "gatewayId",
                filterable: true,
                sortable: true,
            });
        }
        return columns;
    }

    getStatusBarClass = (row) => {
        if (row.status === "online")
            return { className: "green", tooltipText: "Online" }
        if (row.status === "offline")
            return { className: "red", tooltipText: "Offline" }
        return { className: "gray", tooltipText: "Not-connected" }
    }

    onCloseHandler = () => {
        this.setState((previousState, props) => ({
            isOpen: false,
            type: '',
            message2: ''
        }))
    }

    render() {
        let filteredDevices = this.getFilteredItems()
        let onlineDevices = 0
        let offlineDevices = 0
        let disconnectedDevices = 0
        filteredDevices.map(device => {
            if (device.status === "online") onlineDevices++;
            else if (device.status === "offline") offlineDevices++;
            else disconnectedDevices++
        });
        let deviceFilterList = filteredDevices.filter(device => (!this.state.status || device.status === this.state.status));
        let isGatewayColumnRequired = deviceFilterList.some(device => device.gatewayId);

        return (
            <React.Fragment>
                <Helmet>
                    <title>Device List</title>
                    <meta name="description" content="M83-Device Map" />
                </Helmet>

                <header className="content-header d-flex">
                    <div className="flex-60">
                        <h6>Device List</h6>
                    </div>
                    <div className="flex-40 text-right">
                        <div className="content-header-group">
                            <button className={this.state.toggleView == true ? "btn btn-light active" : "btn btn-light"} disabled={this.state.isFetching} data-tooltip="true" data-tooltip-text="List View" data-tooltip-place="bottom" onClick={() => this.setState({ toggleView: true })}><i className="fad fa-list-ul"></i></button>
                            <button className={this.state.toggleView == false ? "btn btn-light active" : "btn btn-light"} disabled={this.state.isFetching} data-tooltip="true" data-tooltip-text="Card View" data-tooltip-place="bottom" onClick={() => this.setState({ toggleView: false })}><i className="fad fa-table"></i></button>
                            <button className="btn btn-light" data-tooltip data-tooltip-text="Refresh" data-tooltip-place="bottom" disabled={this.state.isFetchingDevices || (this.state.deviceTypeList.length && this.state.selectedDeviceTypes.length == 0)} onClick={() => this.connectorChangeHandler(true)} ><i className="far fa-sync-alt"></i></button>
                            <button className={`btn btn-light ${this.state.filterExpand && "active"}`} data-tooltip data-tooltip-text="Filters" data-tooltip-place="bottom" disabled={this.state.isFetchingDevices || !(Boolean(this.state.deviceTypeList.length))} onClick={() => this.filterExpandCollapseHandler()} ><i className="far fa-filter"></i></button>
                        </div>
                    </div>
                </header>

                {this.state.isFetching ?
                    <Loader /> :
                    <React.Fragment>
                        {this.state.deviceTypeList.length ?
                            <React.Fragment>
                                {this.state.filterExpand &&
                                    <div className="device-filter-wrapper">
                                        <div className="device-filter-box">
                                            <h5>Filters <CollapseOrShowAllFilters /></h5>
                                            <h6>Specify Your Search Criteria</h6>
                                            {/* <div className="form-group mb-0">
                                                <div className="search-wrapper">
                                                    <SearchBarWithSuggestion value={this.state.filters.keyword} suggestionKeys={["name", "id"]} onChange={this.searchHandler} data={this.state.allDevices} />
                                                </div>
                                           </div> */}
                                        </div>
                                        <div className="device-filter-box">
                                            <p>Device Type <i className="fas fa-angle-down device-filter-collapse" data-toggle="collapse" data-target="#deviceType__filter__"></i></p>
                                            <div className="collapse show" id="deviceType__filter__">
                                                <div className="device-filter-body">
                                                    <div className="form-group mb-0">
                                                        <ReactSelect
                                                            id="deviceTypes"
                                                            options={this.getDeviceTypes()}
                                                            value={this.state.selectedDeviceTypes}
                                                            onChange={this.deviceTypeChangeHandler}
                                                            isMulti={true}
                                                            isDisabled={this.state.isFetchingDevices}
                                                            className="form-control-multi-select">
                                                        </ReactSelect>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="device-filter-box">
                                            <p>Groups <i className="fas fa-angle-down device-filter-collapse" data-toggle="collapse" data-target="#groups__filter__"></i>
                                                {Boolean(this.state.filters.groupId.length) &&
                                                    <button className="btn btn-link" onClick={() => this.filterChangeHandler("groupId", [])}>Reset</button>
                                                }
                                            </p>
                                            <div className="collapse show" id="groups__filter__">
                                                <div className="device-filter-body">
                                                    <ul className="d-flex list-style-none device-filter-group">
                                                        {this.state.deviceGroups.length ?
                                                            this.state.deviceGroups.map(deviceGroup =>
                                                                <li key={deviceGroup.deviceGroupId} className={this.getActiveClass("groupId", deviceGroup.deviceGroupId)} onClick={() => this.filterChangeHandler("groupId", deviceGroup.deviceGroupId)}>{deviceGroup.name}</li>) :
                                                            <li><p>No Device Groups Found !</p></li>
                                                        }
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="device-filter-box">
                                            <p>Tags <i className="fas fa-angle-down device-filter-collapse" data-toggle="collapse" data-target="#tags__filter__"></i>
                                                {Boolean(this.state.filters.tagId.length) &&
                                                    <button className="btn btn-link" onClick={() => this.filterChangeHandler("tagId", [])}>Reset</button>
                                                }
                                            </p>
                                            <div className="collapse show" id="tags__filter__">
                                                <div className="device-filter-body">
                                                    <ul className="d-flex list-style-none device-filter-group">
                                                        {this.state.isFetchingTags ?
                                                            <div className="text-center flex-100"><i className="fad fa-sync-alt fa-spin f-13 text-theme"></i></div>
                                                            :
                                                            this.state.deviceTags.length ? this.state.deviceTags.map(deviceTag =>
                                                                <li key={deviceTag.id}
                                                                    className={this.getActiveClass("tagId", deviceTag.id)}
                                                                    onClick={() => this.filterChangeHandler("tagId", deviceTag.id)}>{deviceTag.tag}</li>
                                                            ) : <li><p>No Tags found</p></li>
                                                        }
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="device-filter-box">
                                            <p>Alarms <i className="fas fa-angle-down device-filter-collapse" data-toggle="collapse" data-target="#alarms__filter__"></i>
                                                {this.state.filters.alarmCategories.length > 0 &&
                                                    <button className="btn btn-link" onClick={() => this.state.filters.alarmCategories.length && this.filterChangeHandler("alarmCategories", [])}>Reset</button>
                                                }
                                            </p>
                                            <div className="collapse show" id="alarms__filter__">
                                                <div className="device-filter-body">
                                                    <ul className="d-flex list-style-none device-filter-alarms">
                                                        {ALARM_CATEGORIES.map(category =>
                                                            <li key={category.name} className={this.getActiveClass("alarmCategories", category.name)} onClick={() => this.filterChangeHandler("alarmCategories", category.name)}><span className={category.className}><i className={category.icon}></i></span> <p>{category.displayName}</p></li>
                                                        )}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        {/* <div className="device-filter-box">
                                            <p>Status <i className="fas fa-angle-down device-filter-collapse" data-toggle="collapse" data-target="#status__filter__"></i>
                                                {this.state.status &&
                                                    <button className="btn btn-link" onClick={() => this.setState({ status: "" })}>Reset</button>
                                                }
                                            </p>
                                            <div className="collapse show" id="status__filter__">
                                                <div className="device-filter-body">
                                                    {STATUS_TYPES.map((status, i) =>
                                                        <label className="radio-button" key={i}>
                                                            <span className="radio-button-text">{status.displayName}</span>
                                                            <input type="radio" checked={status.type === this.state.status} onChange={() => this.statusFilterChangeHandler(status.type)} />
                                                            <span className="radio-button-mark"></span>
                                                        </label>
                                                    )}
                                                </div>
                                            </div>
                                        </div> */}
                                    </div>
                                }

                                <div className="content-body device-filter-content" style={{ paddingRight: this.state.filterExpand ? 270 : 12 }}>
                                    <ul className="list-style-none d-flex device-counter-list">
                                        <li className="flex-25">
                                            <div className="device-counter-card">
                                                <span className="bg-primary"></span>
                                                <h4>{filteredDevices.length}</h4>
                                                <p>All devices</p>
                                                <div className="device-counter-icon">
                                                    <img src="https://content.iot83.com/m83/misc/icons/totalDevice.png" />
                                                    <div className="device-counter-status bg-primary"></div>
                                                </div>
                                            </div>
                                        </li>
                                        <li className="flex-25">
                                            <div className="device-counter-card">
                                                <span className="bg-green"></span>
                                                <h4>{onlineDevices}</h4>
                                                <p>Online devices</p>
                                                <div className="device-counter-icon">
                                                    <img src="https://content.iot83.com/m83/misc/icons/onlineDevice.png" />
                                                    <div className="device-counter-status bg-green"></div>
                                                </div>
                                            </div>
                                        </li>
                                        <li className="flex-25">
                                            <div className="device-counter-card">
                                                <span className="bg-red"></span>
                                                <h4>{offlineDevices}</h4>
                                                <p>Offline devices</p>
                                                <div className="device-counter-icon">
                                                    <img src="https://content.iot83.com/m83/misc/icons/offlineDevice.png" />
                                                    <div className="device-counter-status bg-red"></div>
                                                </div>
                                            </div>
                                        </li>
                                        <li className="flex-25">
                                            <div className="device-counter-card">
                                                <span className="bg-gray"></span>
                                                <h4>{disconnectedDevices}</h4>
                                                <p>Not connected devices</p>
                                                <div className="device-counter-icon">
                                                    <img src="https://content.iot83.com/m83/misc/icons/notConnectedDevice.png" />
                                                    <div className="device-counter-status bg-gray"></div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                    
                                    {this.state.toggleView ?
                                        <ListingTable
                                            columns={this.getColumns(isGatewayColumnRequired)}
                                            statusBar={this.getStatusBarClass}
                                            itemClickHandler={(row) => { this.props.history.push(`deviceDetails/${row.deviceTypeId}/${row.id}`) }}
                                            data={deviceFilterList}
                                            isLoading={this.state.isFetchingDevices}
                                            // totalItemsCount={filteredDevices.filter(device => (!this.state.status || device.status === this.state.status)).length}
                                            className="cursor-pointer"
                                        />
                                        :
                                        <ul className="card-view-list" id="deviceListCard">
                                            {deviceFilterList.map((item, index) =>
                                                {
                                                    let totalAlarms = item.alarmsCount.critical + item.alarmsCount.major + item.alarmsCount.minor + item.alarmsCount.warning
                                                    let alarmCount = totalAlarms
                                                    let tooltipVisibility = false
                                                    if (Math.floor((alarmCount) / 1000) > 0) {
                                                        if (alarmCount % 1000 == 0) {
                                                            alarmCount = Math.floor(alarmCount / 1000) + "K"
                                                        } else {
                                                            alarmCount = Math.floor(alarmCount / 1000) + "K+"
                                                            tooltipVisibility = true
                                                        }
                                                    }
                                                    return (
                                                        <li key={index}>
                                                            <div className="card-view-box">
                                                                <div className="card-view-header">
                                                                    <span data-tooltip data-tooltip-text={item.status === "online" ? "Online" : item.status === "offline" ? "Offline" : "Not-connected"} data-tooltip-place="bottom" className={item.status === "online" ? "card-view-header-status bg-success" : item.status === "offline" ? "card-view-header-status bg-danger" : "card-view-header-status bg-light"}></span>
                                                                    {this.state.deviceTypeList.find(connector => connector.id === item.deviceTypeId).isGateway &&  !item.gatewayId &&
                                                                        <span className="mr-r-10 f-13"><i className="fad fa-router text-indigo"></i></span>
                                                                    }
                                                                    {cardViewStatus[item.status]}
                                                                    {this.state.deviceTypeList.find(connector => connector.id === item.deviceTypeId).assetType === "Fixed" ?
                                                                        <span className="alert alert-warning"><i className="fas fa-map-marker-alt mr-2"></i>Fixed</span>:
                                                                        <span className="alert alert-info"><i className="fas fa-route mr-2"></i>Movable</span>
                                                                    }
                                                                    <div className="dropdown">
                                                                        <button className="btn-transparent btn-transparent-gray" type="button" data-toggle="dropdown">
                                                                            <i className="fas fa-ellipsis-v"></i>
                                                                        </button>
                                                                        <div className="dropdown-menu">
                                                                            <button className="dropdown-item" onClick={() => isNavAssigned('deviceDiagnostics') && window.open(`/deviceDiagnostics/${item.deviceTypeId}/${item.id}`, '_blank')}><i className="far fa-bug"></i>Debug</button>
                                                                            <button className="dropdown-item" disabled={this.state.deviceTypeList.find(deviceType => deviceType.id === item.deviceTypeId)['assetType'] !== 'Movable'} onClick={(e) => this.redirectToLiveLocationPage(e, item)}><i className="fal fa-map-marker-alt"></i>Location Tracking</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div className="card-view-body">
                                                                    <div className="card-view-icon">
                                                                        <img src={item.deviceTypeImage || "https://content.iot83.com/m83/misc/device-type-fallback.png"} />
                                                                        {totalAlarms ?
                                                                            <div className="card-view-alarm-count">
                                                                                <i className="fad fa-bell text-orange"></i>
                                                                                <span className="badge badge-danger text-white" data-tooltip={tooltipVisibility} data-tooltip-text={totalAlarms} data-tooltip-place="bottom">{alarmCount}</span>
                                                                            </div>
                                                                            :
                                                                            <div className="card-view-alarm-count">
                                                                                <i className="fad fa-bell"></i>
                                                                                <span className="badge badge-light">0</span>
                                                                            </div>
                                                                        }
                                                                    </div>
                                                                    <h6><i className="far fa-address-book"></i>
                                                                        <button className="btn-transparent btn-transparent-green copy-button float-none d-inline-block" data-tooltip data-tooltip-text="Copy" data-tooltip-place="bottom" onClick={() => this.copyToClipboard(`${row.id}`)}>
                                                                            <i className="far fa-copy text-green"></i>
                                                                        </button>{item.displayName ? item.displayName : "-"}
                                                                    </h6>
                                                                    <p><i className="far fa-map-marker-alt"></i>{item.location ? item.location : "-"}</p>
                                                                    <p><span className="badge badge-pill badge-light">Device - {item.deviceTypeName}</span></p>
                                                                    <p><span className="badge badge-pill badge-light">Group - {item.groupName}</span></p>
                                                                    {item.tagName ? <p><span className="badge badge-pill badge-light" style={{ color: item.tagColor }}>{item.tagName}</span></p> : ""}
                                                                </div>
                                                                <div className="card-view-footer d-flex">
                                                                    <div className="flex-50 p-3">
                                                                        <h6>Last Reported At</h6>
                                                                        <p>{convertTimestampToDate(item.lastReportedAt)}</p>
                                                                    </div>
                                                                    <div className="flex-50 p-3">
                                                                        <h6>Last Reported Ago</h6>
                                                                        <p className={getTimeDifferenceClasses[item.status]}>{item.lastReportedAt ? getTimeDifference(item.lastReportedAt) : "-"}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    )
                                                }
                                            )}
                                        </ul>
                                    }
                                </div>
                            </React.Fragment>
                            :
                            <div className="content-body">
                                <AddNewButton
                                    text1="No device(s) available"
                                    text2="You haven't created any device type(s) yet. Please create a device type first."
                                    addButtonEnable={isNavAssigned("deviceTypes")}
                                    createItemOnAddButtonClick={() => this.props.history.push(`/addOrEditDeviceType`)}
                                    imageIcon="addDeviceList.png"
                                />
                            </div>
                        }
                    </React.Fragment>
                }
                {this.state.isOpen &&
                    <NotificationModal
                        type={this.state.type}
                        message2={this.state.message2}
                        onCloseHandler={this.onCloseHandler}
                    />
                }
            </React.Fragment>
        )

    }
}

DeviceList.propTypes = {
    dispatch: PropTypes.func.isRequired
};

let allSelectors = {}
Object.entries(SELECTORS).map(([key, value]) => {
    allSelectors[key] = value()
})

const mapStateToProps = createStructuredSelector(allSelectors)

function mapDispatchToProps(dispatch) {
    let allActions = { dispatch }
    Object.entries(ACTIONS).map(([key, value]) => {
        allActions[key] = (...args) => dispatch(value(...args))
    })
    return allActions
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({
    key: "deviceList",
    reducer
});
const withSaga = injectSaga({ key: "deviceList", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(DeviceList);
