/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/*
 *
 * DeviceList constants
 *
 */


export const RESET_TO_INITIAL_STATE = "app/DeviceList/RESET_TO_INITIAL_STATE";
export const RESET_TO_INITIAL_STATE_PROPS = "app/DeviceList/RESET_TO_INITIAL_STATE_PROPS";


export const GET_CONNECTORS_LIST = {
    action : 'app/DeviceList/GET_CONNECTORS_LIST',
    success: "app/DeviceList/GET_CONNECTORS_LIST_SUCCESS",
    failure: "app/DeviceList/GET_CONNECTORS_LIST_FAILURE",
    urlKey: "deviceTypeList",
    successKey: "deviceTypeListSuccess",
    failureKey: "deviceTypeListFailure",
    actionName: "deviceTypeList",
    actionArguments : []
}

export const GET_DEVICE_TAGS = {
    action : 'app/DeviceList/GET_DEVICE_TAGS',
    success: "app/DeviceList/GET_DEVICE_TAGS_SUCCESS",
    failure: "app/DeviceList/GET_DEVICE_TAGS_FAILURE",
    urlKey: "getAllTags",
    successKey: "getDeviceTagsSuccess",
    failureKey: "getDeviceTagsFailure",
    actionName: "getDeviceTags",
    actionArguments : ["connectorId"]
}

export const GET_ALL_DEVICES = {
    action : 'app/DeviceList/GET_ALL_DEVICES',
    success: "app/DeviceList/GET_ALL_DEVICES_SUCCESS",
    failure: "app/DeviceList/GET_ALL_DEVICES_FAILURE",
    urlKey: "getAllDevicesForMap",
    successKey: "getAllDevicesSuccess",
    failureKey: "getAllDevicesFailure",
    actionName: "getAllDevices",
    actionArguments : ["payload"]
}
