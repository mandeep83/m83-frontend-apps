/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/*
 *
 * ManageFlows constants
 *
 */

export const RESET_TO_INITIAL_STATE = "app/ManageFlows/RESET_TO_INITIAL_STATE";

export const GET_MANAGE_FLOW = {
    action : 'app/ManageFlows/GET_MANAGE_FLOW',
    success: "app/ManageFlows/GET_MANAGE_FLOW_SUCCESS",
    failure: "app/ManageFlows/GET_MANAGE_FLOW_FAILURE",
    urlKey: "getFlowManager",
    successKey: "getFlowManagerSuccess",
    failureKey: "getFlowManagerFailure",
    actionName: "getFlowManager",
	actionArguments: []
}

export const CREATE_FLOW_MANAGER = {
    action : 'app/ManageFlows/CREATE_FLOW_MANAGER',
    success: "app/ManageFlows/CREATE_FLOW_MANAGER_SUCCESS",
    failure: "app/ManageFlows/CREATE_FLOW_MANAGER_FAILURE",
    urlKey: "createFlowManager",
    successKey: "createFlowManagerSuccess",
    failureKey: "createFlowManagerFailure",
    actionName: "createFlowManager",
	actionArguments: ["payload"]
}

export const START_STOP_FLOW_MANAGER = {
    action : 'app/ManageFlows/START_STOP_FLOW_MANAGER',
    success: "app/ManageFlows/START_STOP_FLOW_MANAGER_SUCCESS",
    failure: "app/ManageFlows/START_STOP_FLOW_MANAGER_FAILURE",
    urlKey: "startStopFlowManager",
    successKey: "startStopFlowManagerSuccess",
    failureKey: "startStopFlowManagerFailure",
    actionName: "startStopFlowManager",
	actionArguments: ["state"]
}

export const DELETE_FLOW_MANAGER = {
    action : 'app/ManageFlows/DELETE_FLOW_MANAGER',
    success: "app/ManageFlows/DELETE_FLOW_MANAGER_SUCCESS",
    failure: "app/ManageFlows/DELETE_FLOW_MANAGER_FAILURE",
    urlKey: "deleteFlowManager",
    successKey: "deleteFlowManagerSuccess",
    failureKey: "deleteFlowManagerFailure",
    actionName: "deleteFlowManager",
	actionArguments: []
}

export const GET_FLOW_MANAGER_HISTORY = {
    action : 'app/ManageFlows/GET_FLOW_MANAGER_HISTORY',
    success: "app/ManageFlows/GET_FLOW_MANAGER_HISTORY_SUCCESS",
    failure: "app/ManageFlows/GET_FLOW_MANAGER_HISTORY_FAILURE",
    urlKey: "getFlowManagerHistory",
    successKey: "getFlowManagerHistorySuccess",
    failureKey: "getFlowManagerHistoryFailure",
    actionName: "getFlowManagerHistory",
	actionArguments: []
}