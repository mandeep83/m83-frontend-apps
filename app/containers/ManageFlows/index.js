/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * ManageFlows
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { FormattedMessage } from "react-intl";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import { allSelectors as SELECTORS } from "./selectors";
import { allActions as ACTIONS } from './actions'
import { getTimeDifference } from '../../commonUtils'
import reducer from "./reducer";
import saga from "./saga";
import Loader from "../../components/Loader";
import moment from "moment"
import produce from "immer"
import cloneDeep from "lodash/cloneDeep";
import {subscribeTopic, unsubscribeTopic} from "../../mqttConnection";

let interval;

let returnTimeDifference = (time) => {

    if (time == 0) {
        return "running"
    }
    else {
        let totalSeconds = parseInt(Math.floor((time) / 1000)),
            totalMinutes = parseInt(Math.floor(totalSeconds / 60)),
            totalHours = parseInt(Math.floor(totalMinutes / 60)),
            days = parseInt(Math.floor(totalHours / 24));
        let minuteString = (totalMinutes % 60 == 0 ? '' : `${totalMinutes % 60} ${(totalMinutes % 60) > 1 ? "minutes" : "minute"}`) + (totalSeconds % 60 == 0 ? '' : ` ${totalSeconds % 60} ${(totalSeconds % 60) > 1 ? "seconds" : "second"}`)
        let dayString = `${days} ${days > 1 ? "days" : "day"} ` + (totalHours % 24 == 0 ? '' : `${totalHours % 24} ${(totalHours % 24) > 1 ? "hours" : "hour"} ${minuteString}`)
        if (days) {
            return dayString
        } else if (totalHours) {
            return `${totalHours} ${totalHours > 1 ? "hours" : "hour"} ${minuteString}`
        } else if (totalMinutes) {
            return minuteString
        } else {
            return `${totalSeconds} ${totalSeconds > 1 ? "seconds" : "second"}`
        }
    }
}

/* eslint-disable react/prefer-stateless-function */
export class ManageFlows extends React.Component {
    state = {
        flowManager: {
            name: "Flow does not exist.",
            dns: `xxxxxx.${window.API_URL.split(".")[1]}.com`
        },
        isFetching: true,
        isFullPageLoader: true,
        isFetchingHistory: true,
        isStartStopLoader: '',
        flowManagerHistoryData: { data: [], totalRunningTime: 0 }
    }

    componentDidMount() {
        this.props.getFlowManager()
    }

    componentWillUnmount() {
        clearInterval(interval)
    }

    componentWillReceiveProps(nextProps) {
        let newProp = Object.keys(SELECTORS).find(prop => nextProps[prop])
        if (newProp) {
            let propData = nextProps[newProp].error ? nextProps[newProp].error : nextProps[newProp].response
            if (nextProps.getFlowManagerSuccess) {
                nextProps.getFlowManagerHistory();
                this.setState((prevState) => ({
                    flowManager: Object.keys(propData).length ? propData : prevState.flowManager,
                    isFetching: false,
                    isLaunchingLoader: false,
                }), () => {
                    unsubscribeTopic("flowManager");
                    subscribeTopic("flowManager", this.mqttMessageHandler);
                });
            }
            if (nextProps.getFlowManagerHistorySuccess) {
                let flowManagerHistoryData = cloneDeep(propData);
                flowManagerHistoryData.data.map(el => {
                    if (el.endTime == 0) {
                        el.duration = Date.now() - el.startTime
                        flowManagerHistoryData.totalRunningTime += el.duration
                    }
                })
                this.setState({
                    flowManagerHistoryData,
                    isFetchingHistory: false,
                    isFullPageLoader: false
                }, () => {
                    if (this.state.flowManager.state == "running") {
                        if (flowManagerHistoryData.data[0].endTime == 0) {
                            interval = setInterval(() => {
                                let flowManagerHistoryData = cloneDeep(this.state.flowManagerHistoryData);
                                flowManagerHistoryData.data[0].duration += 10000
                                flowManagerHistoryData.totalRunningTime += 10000
                                this.setState({
                                    flowManagerHistoryData
                                })
                            }, 10000)
                        }
                    }
                })
            }
            if (nextProps.getFlowManagerHistoryFailure) {
                this.setState({
                    isFetchingHistory: false
                })
            }
            if (nextProps.startStopFlowManagerSuccess) {
                let flowManager = cloneDeep(this.state.flowManager);
                flowManager.state = nextProps.startStopFlowManagerSuccess.response.state
                flowManager.updatedAt = Date.now()
                nextProps.getFlowManager()
                this.setState({
                    isStartStopLoader: '',
                    flowManager
                })
            }
            if (nextProps.createFlowManagerSuccess) {
                nextProps.getFlowManager();
                nextProps.showNotification("success", "Request Completed Successfully")
            }
            if (nextProps.createFlowManagerFailure) {
                this.setState({
                    isLaunchingLoader: false,
                    isFetching: false
                })
            }
            if (nextProps.deleteFlowManagerSuccess) {
                nextProps.getFlowManagerHistory();
                nextProps.showNotification("success", "Request Completed Successfully")
                this.setState({
                    flowManager: {
                        name: "Flow does not exist.",
                        dns: `xxxxxx.${window.API_URL.split(".")[1]}.com`
                    }
                })
            }
        }
    }

    componentDidUpdate(prevProps, prevState) {
        let newProp = Object.keys(SELECTORS).find(prop => this.props[prop])
        if (newProp) {
            if (newProp.includes("Failure")) {
                this.props.showNotification("error", this.props[newProp].error)
                this.setState({
                    isFetching: false
                })
            }
            this.props.resetToInitialState(newProp)
        }
    }

    mqttMessageHandler = (message) => {
        console.log('message', message);
    }

    createFlowManager = () => {
        this.setState({
            isLaunchingLoader: true,
            isFetching: true,
        })
        this.props.createFlowManager()
    }

    startStopFlowManager = (state) => {
        if (interval) {
            clearInterval(interval)
            interval = undefined
        }
        this.setState({
            isFetching: true,
            isStartStopLoader: state
        })
        this.props.startStopFlowManager(state)
    }

    deleteFlowManager = () => {
        if (interval) {
            clearInterval(interval)
            interval = undefined
        }
        this.setState({
            confirmState: true
        })
    }

    refreshComponent = () => {
        this.props.getFlowManager()
        this.setState({
            isFullPageLoader: true
        })
    }

    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>ManageFlows</title>
                    <meta name="description" content="Description of ManageFlows" />
                </Helmet>
                <React.Fragment>
                    <header className="content-header d-flex">
                        <div className="flex-60">
                            <h6>Manage Flows</h6>
                        </div>
                        <div className="flex-40 text-right">
                            <div className="content-header-group">
                                <button className="btn btn-light" data-tooltip data-tooltip-text="Refresh" data-tooltip-place="bottom" disabled={this.state.isFullPageLoader} onClick={this.refreshComponent}>
                                    <i className="far fa-sync-alt"></i>
                                </button>
                            </div>
                        </div>
                    </header>

                    {this.state.isFullPageLoader ?
                        <Loader /> :
                        <div className="content-body">
                            <div className="card">
                                <div className="card-body">
                                    <div className="flow-wrapper">
                                        {this.state.flowManager.state ?
                                            this.state.flowManager.state != "stop" ?
                                                <span className="flow-wrapper-state bg-green"></span> :
                                                <span className="flow-wrapper-state bg-red"></span> :
                                                <span className="flow-wrapper-state bg-light-gray"></span>
                                        }
                                        <p className="text-theme m-0">{this.state.flowManager.name}<span>{this.state.flowManager.createdAt ? `Created ${getTimeDifference(this.state.flowManager.createdAt)}` : null}</span></p>
                                        <h4 className="text-dark-theme fw-600 text-capitalize">{localStorage.tenant + " Flow Manager"}</h4>
                                        {this.state.flowManager.state ?
                                            <a className="f-12 mb-2 d-block" href={`https://${this.state.flowManager.name}.${this.state.flowManager.dns}/`} target="_blank">
                                                <i className="far fa-link mr-2"></i>{`https://${this.state.flowManager.name}.${this.state.flowManager.dns}/`}
                                            </a> :
                                            <p ><i className="far fa-link mr-2"></i>{`https://xxxxxx.${this.state.flowManager.dns}/`}</p>}
                                        <h5 className="text-content f-12 m-0">
                                            {this.state.flowManager.state ?
                                                this.state.flowManager.state != "stop" ?
                                                    <React.Fragment>
                                                        <strong className="text-green"><i className="fad fa-spinner fa-spin mr-2"></i>Running</strong> - Started {getTimeDifference(this.state.flowManager.updatedAt)}
                                                    </React.Fragment>
                                                    :
                                                    <React.Fragment>
                                                        <strong className="text-red"><i className="fad fa-stop mr-2"></i>Stopped</strong> - {getTimeDifference(this.state.flowManager.updatedAt)}
                                                    </React.Fragment>
                                                : null
                                            }
                                        </h5>
                                        <div className="button-group">
                                            <button type="button" className="btn btn-primary" disabled={this.state.flowManager.state || this.state.isFetching} onClick={this.createFlowManager}><i className={this.state.isLaunchingLoader ? "far fa-cog fa-spin mr-2" : "far fa-rocket-launch mr-2"}></i>Launch</button>
                                            <button type="button" className="btn btn-success" disabled={this.state.flowManager.state != "stop" || !this.state.flowManager.state || this.state.isStartStopLoader} onClick={() => this.startStopFlowManager("start")}><i className={this.state.isStartStopLoader == "start" ? "far fa-cog fa-spin mr-2" : "far fa-play mr-2"}></i>Start</button>
                                            <button type="button" className="btn btn-warning" disabled={this.state.flowManager.state == "stop" || !this.state.flowManager.state || this.state.isStartStopLoader} onClick={() => this.startStopFlowManager("stop")}><i className={this.state.isStartStopLoader == "stop" ? "far fa-cog fa-spin mr-2" : "far fa-stop mr-2"}></i>Stop</button>
                                            <button type="button" className="btn btn-danger" disabled={this.state.flowManager.state != "stop" || !this.state.flowManager.state || this.state.isFetching} onClick={this.deleteFlowManager}><i className={this.state.isDeleteLoader == "start" ? "far fa-cog fa-spin mr-2" : "far fa-trash mr-2"}></i>Delete</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="card">
                                <div className="card-header">
                                    <h6>Flow Manager History {this.state.flowManagerHistoryData.totalRunningTime ? <span className="float-right f-12 text-content">Total Running Time : <strong className="text-green">{returnTimeDifference(this.state.flowManagerHistoryData.totalRunningTime)}</strong></span> : null}</h6>
                                </div>
                                <div className="card-body">
                                    {this.state.isFetchingHistory ?
                                        <div className="inner-loader-wrapper" style={{ height: 300 }}>
                                            <div className="inner-loader-content">
                                                <i className="fad fa-sync-alt fa-spin"></i>
                                            </div>
                                        </div> :
                                        (this.state.flowManagerHistoryData.data && this.state.flowManagerHistoryData.data.length) ?
                                            <div className="content-table">
                                                <table className="table table-bordered mb-0">
                                                    <thead>
                                                        <tr>
                                                            <th>Start time</th>
                                                            <th>End Time</th>
                                                            <th>Duration</th>
                                                            <th>Started By</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        {this.state.flowManagerHistoryData.data.map((history, index) => {
                                                            return (
                                                                <tr key={index}>
                                                                    <td>{new Date(history.startTime).toLocaleString('en-US', { timeZone: this.props.timeZone })}</td>
                                                                    <td>{history.endTime ? new Date(history.endTime).toLocaleString('en-US', { timeZone: this.props.timeZone }) : <span className="text-green">Running</span>}</td>
                                                                    <td>{returnTimeDifference(history.duration)}</td>
                                                                    <td>{history.startedBy}</td>
                                                                </tr>)
                                                        })}
                                                    </tbody>
                                                </table>
                                            </div> :
                                            <div className="inner-message-wrapper" style={{ height: 300 }}>
                                                <div className="inner-message-content">
                                                    <i className="fad fa-file-exclamation"></i>
                                                    <h6>There is no data to display.</h6>
                                                </div>
                                            </div>}
                                </div>
                            </div>
                        </div>}
                </React.Fragment>
                { this.state.confirmState &&
                    <ConfirmModel
                        deleteName={`Flow ${this.state.flowManager.name}`}
                        confirmClicked={() => {
                            this.setState({
                                confirmState: false,
                                isFullPageLoader: true,
                                isFetchingHistory: true
                            }, () => this.props.deleteFlowManager())
                        }}
                        cancelClicked={() => {
                            this.setState({
                                confirmState: false
                            })
                        }}
                    />
                }
            </React.Fragment>

        );
    }
}

ManageFlows.propTypes = {
    dispatch: PropTypes.func.isRequired
};

let allSelectors = {}
Object.entries(SELECTORS).map(([key, value]) => {
    allSelectors[key] = value()
})

const mapStateToProps = createStructuredSelector(allSelectors)

function mapDispatchToProps(dispatch) {
    let allActions = { dispatch }
    Object.entries(ACTIONS).map(([key, value]) => {
        allActions[key] = (...args) => dispatch(value(...args))
    })
    return allActions
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "manageFlows", reducer });
const withSaga = injectSaga({ key: "manageFlows", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(ManageFlows);
