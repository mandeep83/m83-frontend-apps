import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
 * Direct selector to the manageGroup state domain
 */

const selectManageGroupDomain = state => state.get("manageGroup", initialState);

export const getGroupListSuccess = () =>
    createSelector(selectManageGroupDomain, subState => subState.getGroupListSuccess);

export const getGroupListError = () =>
    createSelector(selectManageGroupDomain, subState => subState.getGroupListError);

export const createOrEditGroupSuccess = () =>
    createSelector(selectManageGroupDomain, subState => subState.createOrEditGroupSuccess);

export const createOrEditGroupError = () =>
    createSelector(selectManageGroupDomain, subState => subState.createOrEditGroupError);

export const getDeviceTypeSuccess = () =>
    createSelector(selectManageGroupDomain, subState => subState.getDeviceTypeSuccess);

export const getDeviceTypeError = () =>
    createSelector(selectManageGroupDomain, subState => subState.getDeviceTypeError);

export const deleteGroupSuccess = () =>
    createSelector(selectManageGroupDomain, subState => subState.deleteGroupSuccess);

export const deleteGroupError = () =>
    createSelector(selectManageGroupDomain, subState => subState.deleteGroupError);


export const developerQuotaSuccess = () =>
    createSelector(selectManageGroupDomain, subState => subState.developerQuotaSuccess);

export const developerQuotaFailure = () =>
    createSelector(selectManageGroupDomain, subState => subState.developerQuotaFailure);

export { selectManageGroupDomain };
