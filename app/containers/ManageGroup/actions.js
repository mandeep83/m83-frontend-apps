import * as CONSTANTS from "./constants";

export function getAllGroups() {
    return {
        type: CONSTANTS.GET_ALL_GROUPS_LIST,
    };
}


export function getDeviceTypes() {
    return {
        type: CONSTANTS.GET_DEVICE_TYPES
    };
}


export function saveGroup(payload, id) {
    return {
        type: CONSTANTS.CREATE_OR_EDIT_GROUP,
        payload,
        id
    };
}

export function deleteGroupById(id) {
    return {
        type: CONSTANTS.DELETE_GROUP,
        id
    };
}


export function getDeveloperQuota(payload) {
    return {
      type: CONSTANTS.GET_DEVELOPER_QUOTA,
      payload
    };
  }


