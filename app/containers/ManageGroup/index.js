/**
 *
 * ManageGroup
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
// import makeSelectManageGroup from "./selectors";
import * as Selectors from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import ReactTable from 'react-table';
import FontAwesomeDisplayList from "../../components/FontAwesomeDisplayList";
import ReactTooltip from "react-tooltip";
import ReactSelect from "react-select";
import * as Actions from "./actions";
import NotificationModal from '../../components/NotificationModal/Loadable';
import ConfirmModel from '../../components/ConfirmModel';
import Loader from "../../components/Loader/Loadable";
import Modal from 'react-modal';
import cloneDeep from 'lodash/cloneDeep';
import jwt_decode from "jwt-decode";
import SlidingPane from 'react-sliding-pane';
import { convertTimestampToDate } from "../../commonUtils";
import ListingTable from "../../components/ListingTable/Loadable";
import NoDataFoundMessage from "../../components/NoDataFoundMessage/Loadable"
import AddNewButton from "../../components/AddNewButton/Loadable"
import Skeleton from "react-loading-skeleton";


/* eslint-disable react/prefer-stateless-function */
export class ManageGroup extends React.Component {
    state = {
        isFetching: true,
        userList: [],
        isAddOrEditGroup: false,
        toggleView:true,
        groupList: [],
        searchTerm: '',
        selectedUsers: [],
        filteredDataList: [],
        payload: {
            name: "",
            users: [],
            description: "",
        },
        listOfConnectors: [],
        availableConnectorsAndGroup: [],
        groupsQuota: {
            total: 0,
            remaining: 0,
            used: 0,
        },
        selectedUserGroupId: null,
        showDeviceGroupsModal: false,
        deviceGroups: [],
    }

    componentWillMount = () => {
        this.props.getDeveloperQuota([{
            dependingProductId: null,
            productName: "groups"
        }]);
        this.props.getAllGroups();
        this.props.getDeviceTypes();
    }

    getAvailableDeviceTypesAndGroup(id, groupClusterMappings) {
        let availableConnectorsAndGroup = [];
        let associatedGroups = [];

        this.state.groupList.map(temp => {
            associatedGroups = [...associatedGroups, ...temp.groupClusterMappings]
        })
        this.state.listOfConnectors.map(deviceType => {
            let deviceGroups = []
            deviceType.selected = false;
            deviceType.selectedGroup = []
            if (id) {
                deviceType.selected = groupClusterMappings.some(temp => temp.deviceTypeId === deviceType.id)
            }
            deviceType.deviceGroups.map(group => {
                if (id) {
                    if (deviceType.selected) {
                        deviceType.selectedGroup = groupClusterMappings.find(temp => temp.deviceTypeId === deviceType.id).deviceGroupId;
                    }
                    if (!associatedGroups.some(temp => temp.deviceGroupId === group.deviceGroupId) || groupClusterMappings.some(temp => temp.deviceGroupId === group.deviceGroupId))
                        deviceGroups.push(group)
                } else if (!associatedGroups.some(temp => temp.deviceGroupId === group.deviceGroupId)) {
                    deviceGroups.push(group);
                }
            })
            if (deviceGroups.length > 0) {
                availableConnectorsAndGroup.push({
                    ...deviceType,
                    deviceGroups
                })
            }
        })
        return availableConnectorsAndGroup.sort((a, b) => {
            if (a === b) {
                return 0;
            }
            return a.name.toUpperCase() < b.name.toUpperCase() ? -1 : 1;
        });
    }

    componentWillReceiveProps = (nextProps) => {
        if (nextProps.getGroupListSuccess && nextProps.getGroupListSuccess !== this.props.getGroupListSuccess) {
            let groupList = nextProps.getGroupListSuccess,
                filteredDataList = cloneDeep(nextProps.getGroupListSuccess),
                searchTerm = this.state.searchTerm
            filteredDataList = (!searchTerm) ? nextProps.getGroupListSuccess : groupList.filter((item) => item.name.toLowerCase().includes(searchTerm.toLowerCase()))
            this.setState({
                groupList,
                filteredDataList,
                isFetching: false,
                addOrEditIsFetching: false,
            })
        }

        if (nextProps.getGroupListError && nextProps.getGroupListError !== this.props.getGroupListError) {
            this.setState({
                isOpen: true,
                message2: nextProps.getGroupListError.message,
                modalType: "error",
                isFetching: false,
                addOrEditIsFetching: false,
            })
        }

        if (nextProps.getDeviceTypeSuccess && nextProps.getDeviceTypeSuccess !== this.props.getDeviceTypeSuccess) {
            this.setState({
                listOfConnectors: nextProps.getDeviceTypeSuccess,
            })
        }

        if (nextProps.getDeviceTypeError && nextProps.getDeviceTypeError !== this.props.getDeviceTypeError) {
            this.setState({
                isOpen: true,
                message2: nextProps.getDeviceTypeError.message,
                modalType: "error",
            })
        }

        if (nextProps.createOrEditGroupSuccess && nextProps.createOrEditGroupSuccess !== this.props.createOrEditGroupSuccess) {
            this.setState({
                isOpen: true,
                isFetching: true,
                message2: nextProps.createOrEditGroupSuccess.message,
                isAddOrEditGroup: false,
                modalType: "success",
            }, () => {
                this.props.getDeveloperQuota([{
                    dependingProductId: null,
                    productName: "groups"
                }]);
                this.props.getAllGroups();
            });
        }

        if (nextProps.createOrEditGroupError && nextProps.createOrEditGroupError !== this.props.createOrEditGroupError) {
            this.setState({
                isOpen: true,
                message2: nextProps.createOrEditGroupError,
                addOrEditIsFetching: false,
                modalType: "error",
            })
        }

        if (nextProps.deleteGroupSuccess && nextProps.deleteGroupSuccess !== this.props.deleteGroupSuccess) {
            let groupList = cloneDeep(this.state.groupList)
            let groupsQuota = cloneDeep(this.state.groupsQuota)

            groupList = groupList.filter(item => item.id !== nextProps.deleteGroupSuccess.id);

            groupsQuota.remaining++;
            groupsQuota.used--;

            this.setState({
                isFetching: false,
                isOpen: true,
                message2: nextProps.deleteGroupSuccess.message,
                filteredDataList: groupList,
                groupList,
                modalType: "success",
                groupsQuota
            })
        }

        if (nextProps.deleteGroupError && nextProps.deleteGroupError !== this.props.deleteGroupError) {
            this.setState({
                isFetching: false,
                isOpen: true,
                message2: nextProps.deleteGroupError.message,
                modalType: "error",
            })
        }

        if (nextProps.developerQuotaSuccess && nextProps.developerQuotaSuccess !== this.props.developerQuotaSuccess) {
            this.setState({
                groupsQuota: nextProps.developerQuotaSuccess[0],
            })
        }

        if (nextProps.developerQuotaFailure && nextProps.developerQuotaFailure !== this.props.developerQuotaFailure) {
            this.setState({
                isOpenModal: true,
                message2: nextProps.developerQuotaFailure,
                type: 'error'
            });
        }
    }

    addNewGroupHandler = () => {
        let payload = {
            description: '',
            name: '',
        }

        this.setState({
            isAddOrEditGroup: true,
            addOrEditIsFetching: false,
            payload,
            selectedGroupId: null,
            availableConnectorsAndGroup: this.getAvailableDeviceTypesAndGroup(),
        });
    }

    onUserChangeHandler = (value) => {
        let payload = cloneDeep(this.state.payload);
        let users = value.map(user => user.value)
        payload.users = users;
        this.setState({
            payload,
            selectedUsers: value,
        })
    }

    onChangeHandler = ({ currentTarget }) => {
        let payload = cloneDeep(this.state.payload);
        if (currentTarget.id === "name") {
            if (/^[a-zA-Z]+(?:_[a-zA-Z]+)*$/.test(currentTarget.value) || /^$/.test(currentTarget.value) || /^[a-zA-Z_]*$/.test(currentTarget.value))
                payload.name = (currentTarget.value).toUpperCase()
        }
        else
            payload[currentTarget.id] = currentTarget.value

        this.setState({ payload })
    }

    onChangeGroupClusterMapping = (index) => ({ currentTarget }) => {
        let availableConnectorsAndGroup = [...this.state.availableConnectorsAndGroup];
        availableConnectorsAndGroup[index].selected = currentTarget.checked;
        availableConnectorsAndGroup[index].selectedGroup = []
        this.setState({
            availableConnectorsAndGroup,
            groupClusterError: false
        })
    }
    handleDeviceGroupChange = (groups, index) => {
        let availableConnectorsAndGroup = [...this.state.availableConnectorsAndGroup];
        availableConnectorsAndGroup[index]['selectedGroup'] = [];
        groups.map(group => {
            availableConnectorsAndGroup[index].selectedGroup.push(group.value);
        });
        this.setState({
            availableConnectorsAndGroup,
            groupClusterError: false
        })
    }

    saveGroup = (event) => {
        event.preventDefault();
        let payload = { ...this.state.payload },
            groupClusterError = false;
        payload.groupClusterMappings = [];
        this.state.availableConnectorsAndGroup.map(deviceType => {
            if (deviceType.selected) {

                payload.groupClusterMappings.push({
                    deviceTypeId: deviceType.id,
                    deviceGroupId: deviceType.selectedGroup,
                })
            }
        })
        if (payload.groupClusterMappings.length) {
            groupClusterError = payload.groupClusterMappings.some(el => el.deviceGroupId.length == 0)
            if (groupClusterError) {
                return this.setState({
                    groupClusterError
                })
            }
        }
        this.setState({
            addOrEditIsFetching: true,
        }, () => this.props.saveGroup(payload, this.state.selectedGroupId));
    }

    deleteGroup = (id) => {
        this.props.deleteGroupById(id)
    }

    cancelClicked = () => {
        this.setState({
            groupToBeDeleted: '',
            confirmState: false,
        });
    }

    onCloseHandler = () => {
        this.setState({
            isOpen: false,
            modalType: "",
            message2: "",
        })
    }

    onSearchHandler = (e) => {
        let filterList = JSON.parse(JSON.stringify(this.state.groupList))
        if (e.target.value.length > 0) {
            filterList = filterList.filter(item => item.name.toLowerCase().includes(e.target.value.toLowerCase()))
        }
        this.setState({
            filteredDataList: filterList,
            searchTerm: e.target.value
        })
    }

    emptySearchBox = () => {
        let filteredDataList = JSON.parse(JSON.stringify(this.state.groupList))
        this.setState({
            searchTerm: '',
            filteredDataList
        })
    }

    getColumns = () => {
        return [
            { Header: "Name", width: 20, accessor: "name",
                cell: (row) => (
                    <React.Fragment>
                        <div className="list-view-icon"><i className="fad fa-users-class"></i></div>
                        <div className="list-view-icon-box">
                            <h6 className="text-theme fw-600 text-truncate">{row.name}</h6>
                        </div>
                    </React.Fragment>
                )
            },
            { Header: "Description", width: 20, accessor: "description" },
            { Header: "Mappings", width: 10, cell: (row) => <span className={`alert alert-success list-view-badge ${row.groupClusterMappings.length > 0 ? "list-view-badge-link" : ""}`} onClick={() => { row.groupClusterMappings.length > 0 && this.showDeviceDeviceGroups(row.id) }}>{row.groupClusterMappings ? row.groupClusterMappings.length : 0}</span> },
            { Header: "Users", width: 10, cell: (row) => <span className={`alert alert-primary list-view-badge ${row.usersCount > 0 ? "list-view-badge-link" : ""}`} onClick={() => { row.usersCount > 0 && this.props.history.push('/manageUsers') }}>{row.usersCount ? row.usersCount : 0}</span> },
            { Header: "Created", width: 15, accessor: "createdAt" },
            { Header: "Updated", width: 15, accessor: "updatedAt" },
            {
                Header: "Actions", width: 10,
                cell: (row) => (
                    <div className="button-group">
                        <button className="btn-transparent btn-transparent-blue" onClick={() => this.editMode(row)} data-tooltip data-tooltip-text="Edit" data-tooltip-place="bottom">
                            <i className="far fa-pencil"></i>
                        </button>
                        <button className="btn-transparent btn-transparent-red" data-tooltip data-tooltip-text="Delete" data-tooltip-place="bottom"
                            onClick={() => this.setState({
                                confirmState: true,
                                groupToBeDeleted: row.id
                            })}>
                            <i className="far fa-trash-alt"></i>
                        </button>
                    </div>
                )
            },
        ]
    }

    editMode = ({ name, description, groupClusterMappings, id }) => {
        this.setState({
            selectedGroupId: id,
            isAddOrEditGroup: true,
            addOrEditIsFetching: false,
            availableConnectorsAndGroup: this.getAvailableDeviceTypesAndGroup(id, groupClusterMappings),
            payload: {
                name,
                description,
                groupClusterMappings,
            }
        });
    }

    showDeviceDeviceGroups = (groupId) => {
        let deviceGroups = this.state.groupList.filter(group => group.id === groupId)[0].groupClusterMappings;
        deviceGroups.map(group => {
            let deviceTypeObj = this.state.listOfConnectors.filter(deviceType => deviceType.id === group.deviceTypeId)[0];
            group.deviceType = deviceTypeObj.name;
        });
        this.setState({
            showDeviceGroupsModal: true,
            selectedUserGroupId: groupId,
            deviceGroups,
        })
    }

    refreshGroups = () => {
        this.setState({
            isFetching: true
        }, () => {
            this.props.getAllGroups()
        })
    }

    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>User Groups</title>
                    <meta name="description" content="M83-Manage Groups" />
                </Helmet>

                <header className="content-header d-flex">
                    <div className="flex-60">
                        <h6>User Groups -
                            <span className="content-header-badge-group">
                                <span className="content-header-badge-item">Allocated <span className="badge badge-pill badge-primary">{this.state.groupsQuota.total}</span></span>
                                <span className="content-header-badge-item">Used <span className="badge badge-pill badge-success">{this.state.groupsQuota.used}</span></span>
                                <span className="content-header-badge-item">Remaining <span className="badge badge-pill badge-warning">{this.state.groupsQuota.remaining}</span></span>
                            </span>
                        </h6>
                    </div>
                    <div className="flex-40 text-right">
                        <div className="content-header-group">
                            <div className="search-box">
                                <span className="search-icon"><i className="far fa-search"></i></span>
                                <input type="text" className="form-control" placeholder="Search..." value={this.state.searchTerm} onChange={this.onSearchHandler} disabled={this.state.groupList.length == 0 || this.state.isFetching} />
                                {this.state.searchTerm.length > 0 && <button className="search-button" onClick={() => this.emptySearchBox()}><i className="far fa-times"></i></button>}
                            </div>
                            <button className={this.state.toggleView == true ? "btn btn-light active" : "btn btn-light"} disabled={this.state.isFetching} data-tooltip="true" data-tooltip-text="List View" data-tooltip-place="bottom" onClick={() => this.setState({ toggleView: true })}><i className="fad fa-list-ul"></i></button>
                            <button className={this.state.toggleView == false ? "btn btn-light active" : "btn btn-light"} disabled={this.state.isFetching} data-tooltip="true" data-tooltip-text="Card View" data-tooltip-place="bottom" onClick={() => this.setState({ toggleView: false })}><i className="fad fa-table"></i></button>
                            <button className="btn btn-light" disabled={this.state.isFetching} data-tooltip data-tooltip-text="Refresh" data-tooltip-place="bottom" onClick={this.refreshGroups} ><i className="far fa-sync-alt"></i></button>
                            <button className="btn btn-primary" data-tooltip data-tooltip-text="Add Group" data-tooltip-place="bottom" disabled={this.state.groupsQuota.remaining == 0 || this.state.isFetching} onClick={() => { this.addNewGroupHandler() }}><i className="far fa-plus"></i></button>
                        </div>
                    </div>
                </header>

                <div className="content-body">
                    {this.state.isFetching ?
                        <Skeleton count={9} className="skeleton-list-loader" /> :
                        <React.Fragment>
                            {this.state.groupList.length > 0 ?
                                this.state.filteredDataList.length > 0 ?
                                    this.state.toggleView ?
                                        <ListingTable
                                            columns={this.getColumns()}
                                            data={this.state.filteredDataList}
                                        />
                                        :
                                        <ul className="card-view-list">
                                            {this.state.groupList.map((item, index) =>
                                                <li key={index}>
                                                    <div className="card-view-box">
                                                        <div className="card-view-header">
                                                            <span className="alert alert-success cursor-pointer text-underline-hover mr-r-7" onClick={() => { item.groupClusterMappings.length > 0 && this.showDeviceDeviceGroups(item.id) }}>Mappings - {item.groupClusterMappings ? item.groupClusterMappings.length : 0}</span>
                                                            <span className="alert alert-primary cursor-pointer text-underline-hover" onClick={() => { item.usersCount > 0 && this.props.history.push('/manageUsers') }}>Users - {item.usersCount ? item.usersCount : 0}</span>
                                                            <div className="dropdown">
                                                                <button className="btn-transparent btn-transparent-gray" type="button" data-toggle="dropdown">
                                                                    <i className="fas fa-ellipsis-v"></i>
                                                                </button>
                                                                <div className="dropdown-menu">
                                                                    <button className="dropdown-item" onClick={() => this.editMode(item)} >
                                                                        <i className="far fa-pencil"></i>Edit
                                                                    </button>
                                                                    <button className="dropdown-item"
                                                                        onClick={() => this.setState({
                                                                            confirmState: true,
                                                                            groupToBeDeleted: item.id
                                                                        })}>
                                                                        <i className="far fa-trash-alt"></i>Delete
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="card-view-body">
                                                            <div className="card-view-icon">
                                                                <i className="fad fa-users-class"></i>
                                                            </div>
                                                            <h6><i className="far fa-address-book"></i>{item.name ? item.name : "N/A"}</h6>
                                                            <p><i className="far fa-file-alt"></i>{item.description ? item.description : "N/A"}</p>
                                                        </div>
                                                        <div className="card-view-footer d-flex">
                                                            <div className="flex-50 p-3">
                                                                <h6>Created</h6>
                                                                <p><strong>By - </strong>{item.createdBy}</p>
                                                                <p><strong>At - </strong>{new Date(item.createdAt).toLocaleString('en-US', { createdAt: localStorage.createdAt })}</p>
                                                            </div>
                                                            <div className="flex-50 p-3">
                                                                <h6>Updated</h6>
                                                                <p><strong>By - </strong>{item.updatedBy}</p>
                                                                <p><strong>At - </strong>{new Date(item.updatedAt).toLocaleString('en-US', { createdAt: localStorage.createdAt })}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            )}
                                        </ul>
                                    :
                                    <NoDataFoundMessage />
                                :
                                <AddNewButton
                                    text1="No Groups available"
                                    text2="You haven't created any groups yet"
                                    imageIcon="addGroup.png"
                                    createItemOnAddButtonClick={this.addNewGroupHandler}
                                />
                            }
                        </React.Fragment>
                    }
                </div>

                {/* add group modal */}
                <SlidingPane
                    className=''
                    overlayClassName='sliding-form'
                    closeIcon={<div></div>}
                    isOpen={this.state.isAddOrEditGroup || false}
                    from='right'
                    width='500px'
                >
                    <div className="modal-content">
                        <div className="modal-header">
                            <h6 className="modal-title">{this.state.selectedGroupId ? "Update Group" : "Add New User Group"}
                                <button className="btn btn-light close" onClick={() => this.setState({
                                    isAddOrEditGroup: false,
                                    selectedGroupId: undefined,
                                    groupClusterError: false
                                })}><i className="fas fa-angle-right"></i>
                                </button>
                            </h6>
                        </div>

                        {this.state.addOrEditIsFetching ?
                            <div className="modal-loader">
                                <i className="fad fa-sync-alt fa-spin text-primary"></i>
                            </div> :
                            <form onSubmit={this.saveGroup}>
                                <div className="modal-body">
                                    <div className="alert alert-info note-text">
                                        <p>User group name must contain only capital letters and underscore.</p>
                                    </div>
                                    <div className="form-group">
                                        <label className="form-group-label">Name :  <i className="fas fa-asterisk form-group-required"></i></label>
                                        <input type="text" name="groupName" id="name" required value={this.state.payload.name} pattern="^[a-zA-Z]+(?:_[a-zA-Z]+)*$"
                                            onChange={this.onChangeHandler} className="form-control"
                                        />
                                    </div>

                                    <div className="form-group">
                                        <label className="form-group-label">Description :</label>
                                        <textarea rows="3" type="text" name="desc" id="description" className="form-control" value={this.state.payload.description}
                                            onChange={this.onChangeHandler}
                                        />
                                    </div>

                                    <div className="form-group">
                                        <label className="form-group-label">Mappings: {this.state.groupClusterError && <span className="form-group-error float-right">Please select at least one group for selected device type.</span>}</label>
                                        <div className="navigation-box">
                                            <div className="d-flex">
                                                <h6 className="flex-10"></h6>
                                                <h6 className="flex-35 pd-r-10">Device Type</h6>
                                                <h6 className="flex-55 pd-l-10">Device Groups</h6>
                                            </div>
                                            {this.state.availableConnectorsAndGroup.length > 0 ?
                                                this.state.availableConnectorsAndGroup.map((deviceType, index) => (
                                                    <div className="d-flex mr-b-10" key={deviceType.id}>
                                                        <div className="flex-10">
                                                            <label className="check-box mt-2">
                                                                <input type="checkbox" checked={deviceType.selected} onChange={this.onChangeGroupClusterMapping(index)} />
                                                                <span className="check-mark"></span>
                                                            </label>
                                                        </div>
                                                        <div className="flex-35 pd-r-10">
                                                            <span className="form-control text-theme">{deviceType.name}</span>
                                                        </div>
                                                        <div className="flex-55 pd-l-10">
                                                            <ReactSelect
                                                                className="form-control-multi-select"
                                                                value={deviceType.deviceGroups.map(el => {
                                                                    if (deviceType.selectedGroup.indexOf(el.deviceGroupId) > -1) {
                                                                        return {
                                                                            value: el.deviceGroupId,
                                                                            label: el.name
                                                                        }
                                                                    }
                                                                })}
                                                                onChange={(value) => this.handleDeviceGroupChange(value, index)}
                                                                options={deviceType.deviceGroups.map(el => {
                                                                    return {
                                                                        value: el.deviceGroupId,
                                                                        label: el.name
                                                                    }
                                                                })}
                                                                isMulti={true}
                                                                isDisabled={!deviceType.selected}
                                                            >
                                                            </ReactSelect>
                                                        </div>
                                                    </div>
                                                )) :
                                                <p className="text-content p-5 text-center">There is no data to display.</p>
                                            }
                                        </div>
                                    </div>

                                    {/*<div className="form-group">
                                    <label className="form-group-label">Access Type :</label>
                                    <div className="d-flex">
                                        <div className="flex-33 pd-r-10">
                                            <label className="radio-button">
                                                <span className="radio-button-text">Platform</span>
                                                <input type="radio" name="accessType" value="PLATFORM"
                                                       checked={this.state.payload.accessType === 'PLATFORM'}
                                                       onChange={() => this.onChangeHandler(event)} />
                                                <span className="radio-button-mark"></span>
                                            </label>
                                        </div>
                                        <div className="flex-33 pd-l-5 pd-r-5">
                                            <label className="radio-button">
                                                <span className="radio-button-text">Application</span>
                                                <input type="radio" name="accessType" value="APPLICATION"
                                                       checked={this.state.payload.accessType === 'APPLICATION'}
                                                       onChange={() => this.onChangeHandler(event)} />
                                                <span className="radio-button-mark"></span>
                                            </label>
                                        </div>
                                        <div className="flex-33 pd-l-10">
                                            <label className="radio-button">
                                                <span className="radio-button-text">Both</span>
                                                <input type="radio" name="accessType" value="BOTH"
                                                       checked={this.state.payload.accessType === 'BOTH'}
                                                       onChange={() => this.onChangeHandler(event)} />
                                                <span className="radio-button-mark"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>*/}

                                    {/*<div className="form-group">
                                    <label className="form-group-label">Users : <i className="fas fa-asterisk form-group-required"></i></label>
                                    <ReactSelect
                                        isMulti={true}
                                        id="users"
                                        onChange={this.onUserChangeHandler}
                                        options={this.state.filteredUserList}
                                        value={this.state.selectedUsers}
                                        className="form-control-multi-select"
                                    />
                                </div>*/}
                                </div>

                                <div className="modal-footer justify-content-start">
                                    <button className="btn btn-primary">{this.state.selectedGroupId ? "Update" : "Save"}</button>
                                    <button type="button" className="btn btn-dark"
                                        onClick={() => this.setState({
                                            isAddOrEditGroup: false,
                                            selectedGroupId: undefined,
                                            groupClusterError: false
                                        })}>Cancel
                                </button>
                                </div>
                            </form>}
                    </div>
                </SlidingPane>
                {/* end add group modal */}

                {this.state.showDeviceGroupsModal &&
                    <div className="modal animated slideInDown show d-block">
                        <div className="modal-dialog modal-dialog-centered">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h6 className="modal-title">Mappings for - <span>{this.state.groupList.filter(group => group.id === this.state.selectedUserGroupId)[0].name}</span>
                                        <button type="button" className="close" data-tooltip data-tooltip-text="Close" data-tooltip-place="bottom" onClick={() => { this.setState({ showDeviceGroupsModal: false }) }}>
                                            <i className="far fa-times"></i>
                                        </button>
                                    </h6>
                                </div>
                                <div className="modal-body overflow-y-auto" style={{ height: 400 }}>
                                    <div className="content-table">
                                        <table className="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th width="40%">Device Type</th>
                                                    <th width="60%">Device Group</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {this.state.deviceGroups.map((deviceGroup, index) =>
                                                    <tr key={index}>
                                                        <td><p className="mb-0 cursor-pointer text-underline-hover text-primary" onClick={() => { this.props.history.push({ pathname: `/addOrEditDeviceType/${deviceGroup.deviceTypeId}`, state: { childViewType: 'GROUP_DETAILS' } }) }}>{deviceGroup.deviceType}</p></td>
                                                        <td>
                                                            {deviceGroup.deviceGroupId.map((groupId, index) =>
                                                                <span className="badge badge-pill alert-success custom-badge-pill mr-1 mb-1 cursor-pointer text-underline-hover" onClick={() => { this.props.history.push({ pathname: `/deviceGroups/${deviceGroup.deviceTypeId}/${groupId}`, state: { childViewType: 'GROUP_DETAILS' } }) }}>
                                                                    {this.state.listOfConnectors.filter(deviceType => deviceType.id === deviceGroup.deviceTypeId)[0]['deviceGroups'].filter(deviceGroup => deviceGroup.deviceGroupId === groupId)[0].name}
                                                                </span>
                                                            )}
                                                        </td>
                                                    </tr>
                                                )}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div className="modal-footer">
                                    <button className="btn btn-success" onClick={() => { this.setState({ showDeviceGroupsModal: false }) }}>OK</button>
                                </div>
                            </div>
                        </div>
                    </div>
                }

                {this.state.confirmState && (
                    <ConfirmModel
                        status={"delete"}
                        confirmClicked={() => {
                            this.setState(
                                {
                                    isFetching: true,
                                    confirmState: false,
                                },
                                () => this.deleteGroup(this.state.groupToBeDeleted)
                            );
                        }}
                        cancelClicked={() => {
                            this.cancelClicked();
                        }}
                    />
                )}

                {this.state.isOpen &&
                    <NotificationModal
                        type={this.state.modalType}
                        message2={this.state.message2}
                        onCloseHandler={this.onCloseHandler}
                    />
                }

            </React.Fragment>
        );
    }
}

ManageGroup.propTypes = {
    dispatch: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
    getGroupListSuccess: Selectors.getGroupListSuccess(),
    getGroupListError: Selectors.getGroupListError(),
    createOrEditGroupSuccess: Selectors.createOrEditGroupSuccess(),
    createOrEditGroupError: Selectors.createOrEditGroupError(),
    getDeviceTypeSuccess: Selectors.getDeviceTypeSuccess(),
    getDeviceTypeError: Selectors.getDeviceTypeError(),
    deleteGroupSuccess: Selectors.deleteGroupSuccess(),
    deleteGroupError: Selectors.deleteGroupError(),
    developerQuotaSuccess: Selectors.developerQuotaSuccess(),
    developerQuotaFailure: Selectors.developerQuotaFailure(),
});

function mapDispatchToProps(dispatch) {
    return {
        dispatch,
        getAllGroups: () => dispatch(Actions.getAllGroups()),
        getDeviceTypes: () => dispatch(Actions.getDeviceTypes()),
        saveGroup: (payload, id) => dispatch(Actions.saveGroup(payload, id)),
        deleteGroupById: (id) => dispatch(Actions.deleteGroupById(id)),
        getDeveloperQuota: payload => dispatch(Actions.getDeveloperQuota(payload)),
    };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "manageGroup", reducer });
const withSaga = injectSaga({ key: "manageGroup", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(ManageGroup);
