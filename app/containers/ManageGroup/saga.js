import { apiCallHandler } from '../../api';
import { takeEvery } from 'redux-saga';
import * as CONSTANTS from './constants';

export function* watcherGetGroupList() {
  yield takeEvery(CONSTANTS.GET_ALL_GROUPS_LIST, getGroupList);
}

export function* getGroupList(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_ALL_GROUPS_LIST_SUCCESS, CONSTANTS.GET_ALL_GROUPS_LIST_FAILURE, 'getGroupList')];
}

export function* watcherDeviceTypeList() {
  yield takeEvery(CONSTANTS.GET_DEVICE_TYPES, getDeviceTypes);
}

export function* apiGetDeveloperQuota(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_DEVELOPER_QUOTA_SUCCESS, CONSTANTS.GET_DEVELOPER_QUOTA_ERROR, 'getDeveloperQuota')];
}

export function* getDeviceTypes(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_DEVICE_TYPES_SUCCESS, CONSTANTS.GET_DEVICE_TYPES_FAILURE, 'deviceTypeList')];
}

export function* watcherCreateOrEditGroup() {
  yield takeEvery(CONSTANTS.CREATE_OR_EDIT_GROUP, createOrEditGroup);
}

export function* createOrEditGroup(action) {
  yield [apiCallHandler(action, CONSTANTS.CREATE_OR_EDIT_GROUP_SUCCESS, CONSTANTS.CREATE_OR_EDIT_GROUP_FAILURE, 'saveGroup')];
}

export function* watcherDeleteGroup() {
  yield takeEvery(CONSTANTS.DELETE_GROUP, deleteGroup);
}

export function* deleteGroup(action) {
  yield [apiCallHandler(action, CONSTANTS.DELETE_GROUP_SUCCESS, CONSTANTS.DELETE_GROUP_FAILURE, 'deleteGroup')];
}


export function* watcherGetDeveloperQuota() {
  yield takeEvery(CONSTANTS.GET_DEVELOPER_QUOTA, apiGetDeveloperQuota);
}

export default function* rootSaga() {
  yield [
    watcherGetGroupList(),
    watcherDeviceTypeList(),
    watcherCreateOrEditGroup(),
    watcherDeleteGroup(),
    watcherGetDeveloperQuota(),
  ];
}