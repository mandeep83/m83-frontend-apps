/*
 *
 * ManageGroup constants
 *
 */
export const GET_DEVICE_TYPES = "app/ManageGroup/GET_DEVICE_TYPES";
export const GET_DEVICE_TYPES_SUCCESS = "app/ManageGroup/GET_DEVICE_TYPES_SUCCESS";
export const GET_DEVICE_TYPES_FAILURE = "app/ManageGroup/GET_DEVICE_TYPES_FAILURE";

export const GET_ALL_GROUPS_LIST = "app/ManageGroup/GET_ALL_GROUPS_LIST";
export const GET_ALL_GROUPS_LIST_SUCCESS = "app/ManageGroup/GET_ALL_GROUPS_LIST_SUCCESS";
export const GET_ALL_GROUPS_LIST_FAILURE = "app/ManageGroup/GET_ALL_GROUPS_LIST_FAILURE";

export const CREATE_OR_EDIT_GROUP = "app/ManageGroup/CREATE_OR_EDIT_GROUP";
export const CREATE_OR_EDIT_GROUP_SUCCESS = "app/ManageGroup/CREATE_OR_EDIT_GROUP_SUCCESS";
export const CREATE_OR_EDIT_GROUP_FAILURE = "app/ManageGroup/CREATE_OR_EDIT_GROUP_FAILURE";

export const DELETE_GROUP = "app/ManageGroup/DELETE_GROUP";
export const DELETE_GROUP_SUCCESS = "app/ManageGroup/DELETE_GROUP_SUCCESS";
export const DELETE_GROUP_FAILURE = "app/ManageGroup/DELETE_GROUP_FAILURE";

export const GET_DEVELOPER_QUOTA = 'app/ManageGroup/GET_DEVELOPER_QUOTA';
export const GET_DEVELOPER_QUOTA_SUCCESS = 'app/ManageGroup/GET_DEVELOPER_QUOTA_SUCCESS';
export const GET_DEVELOPER_QUOTA_ERROR = 'app/ManageGroup/GET_DEVELOPER_QUOTA_ERROR';