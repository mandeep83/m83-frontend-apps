/*
 *
 * ManageGroup reducer
 *
 */

import { fromJS } from "immutable";
import * as CONSTANTS from "./constants";


export const initialState = fromJS({});

function manageGroupReducer(state = initialState, action) {
  switch (action.type) {
    case CONSTANTS.GET_ALL_GROUPS_LIST_SUCCESS:
      return Object.assign({}, state, {
        'getGroupListSuccess': action.response
      });
    case CONSTANTS.GET_ALL_GROUPS_LIST_FAILURE:
      return Object.assign({}, state, {
        'getGroupListError': action.error
      });
    case CONSTANTS.CREATE_OR_EDIT_GROUP_SUCCESS:
      return Object.assign({}, state, {
        'createOrEditGroupSuccess': action.response
      });
    case CONSTANTS.CREATE_OR_EDIT_GROUP_FAILURE:
      return Object.assign({}, state, {
        'createOrEditGroupError': action.error
      });

    case CONSTANTS.GET_DEVICE_TYPES_SUCCESS:
      return Object.assign({}, state, {
        'getDeviceTypeSuccess': action.response
      });
    case CONSTANTS.GET_DEVICE_TYPES_FAILURE:
      return Object.assign({}, state, {
        'getDeviceTypeError': action.error
      });

    case CONSTANTS.DELETE_GROUP_SUCCESS:
      return Object.assign({}, state, {
        'deleteGroupSuccess': { ...action.response, timestamp: Date.now() }
      });
    case CONSTANTS.DELETE_GROUP_FAILURE:
      return Object.assign({}, state, {
        'deleteGroupError': { message: action.error, timestamp: Date.now() }
      });
    case CONSTANTS.GET_DEVELOPER_QUOTA_SUCCESS:
      return Object.assign({}, state, {
        'developerQuotaSuccess': action.response,
      });
    case CONSTANTS.GET_DEVELOPER_QUOTA_ERROR:
      return Object.assign({}, state, {
        'developerQuotaFailure': action.error
      });
    default:
      return state;
  }
}

export default manageGroupReducer;
