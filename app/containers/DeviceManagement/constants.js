/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/*
 *
 * DeviceManagement constants
 *
 */

export const RESET_TO_INITIAL_STATE = "app/DeviceManagement/RESET_TO_INITIAL_STATE";

export const GET_ALL_CONNECTORS_AND_GROUPS = {
	action: 'app/DeviceManagement/GET_ALL_CONNECTORS_AND_GROUPS',
	success: "app/DeviceManagement/GET_ALL_CONNECTORS_AND_GROUPS_SUCCESS",
	failure: "app/DeviceManagement/GET_ALL_CONNECTORS_AND_GROUPS_FAILURE",
	urlKey: "deviceTypeList",
	successKey: "getAllDeviceTypes",
	failureKey: "getAllDeviceTypesFailure",
	actionName: "getAllConnectors",
	actionArguments: []
}

export const EDIT_DEVICE = {
	action: 'app/DeviceManagement/EDIT_DEVICE',
	success: "app/DeviceManagement/EDIT_DEVICE_SUCCESS",
	failure: "app/DeviceManagement/EDIT_DEVICE_FAILURE",
	urlKey: "editDevice",
	successKey: "editDeviceSuccess",
	failureKey: "editDeviceFailure",
	actionName: "editDevice",
	actionArguments: ["payload", "deviceId", "deviceTypeId"]
}

export const GET_ALL_DEVICES_BY_CONNECTOR_ID = {
	action: 'app/DeviceManagement/GET_ALL_DEVICES_BY_CONNECTOR_ID',
	success: "app/DeviceManagement/GET_ALL_DEVICES_BY_CONNECTOR_ID_SUCCESS",
	failure: "app/DeviceManagement/GET_ALL_DEVICES_BY_CONNECTOR_ID_FAILURE",
	urlKey: "getAllDevices",
	successKey: "getAllDevicesByConnectorIdSuccess",
	failureKey: "getAllDevicesByConnectorIdFailure",
	actionName: "getAllDevicesByConnectorId",
	actionArguments: ["payload"]
}

export const DELETE_DEVICE = {
	action: 'app/AddOrEditDeviceType/DELETE_DEVICE',
	success: "app/AddOrEditDeviceType/DELETE_DEVICE_SUCCESS",
	failure: "app/AddOrEditDeviceType/DELETE_DEVICE_FAILURE",
	urlKey: "deleteDevice",
	successKey: "deleteDeviceSuccess",
	failureKey: "deleteDeviceFailure",
	actionName: "deleteDevice",
	actionArguments: ["deviceId", "connectorId"]
}

export const DEVELOPER_QUOTA = {
	action: 'app/DeviceManagement/GET_DEVELOPER_QUOTA',
	success: "app/DeviceManagement/GET_DEVELOPER_QUOTA_SUCCESS",
	failure: "app/DeviceManagement/GET_DEVELOPER_QUOTA_FAILURE",
	urlKey: "getDeveloperQuota",
	successKey: "developerQuotaSuccess",
	failureKey: "developerQuotaFailure",
	actionName: "getDeveloperQuota",
	actionArguments: ["payload"]
}

