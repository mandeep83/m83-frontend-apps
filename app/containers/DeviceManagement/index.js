/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * DeviceManagement
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { FormattedMessage } from "react-intl";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import reducer from "./reducer";
import saga from "./saga";
import { allSelectors as SELECTORS } from "./selectors";
import { allActions as ACTIONS } from './actions'
import {
    convertTimestampToDate,
    getNotificationObj,
    getTimeDifference,
    isNavAssigned
} from "../../commonUtils";
import ListingTable from "../../components/ListingTable/Loadable";
import AddNewButton from "../../components/AddNewButton/Loadable";
import NoDataFoundMessage from "../../components/NoDataFoundMessage/Loadable"
import Loader from "../../components/Loader";
import cloneDeep from "lodash/cloneDeep";
import GoogleMap from "../../components/GoogleMap";
import ConfirmModel from "../../components/ConfirmModel";
import ReactTooltip from "react-tooltip";
import NotificationModal from '../../components/NotificationModal/Loadable'
import { getAddressFromLatLng } from '../../commonUtils'
import MapSearchBox from "../../components/MapSearchBox"
import range from 'lodash/range';
const status = {
    "online": "Online",
    "offline": "Offline",
    "not-connected": "Not-connected"
}
/* eslint-disable react/prefer-stateless-function */
export class DeviceManagement extends React.Component {

    state = {
        isFetching: true,
        toggleView:true,
        searchTerm: '',
        listOfConnectors: [],
        listOfGroupsByConnectorId: [],
        listOfAllDevices: [],
        filteredListOfAllDevices: [],
        selectedConnector: {},
        selectedGroup: '',
        selectedDeviceStatus: 'all',
        tableState: {
            activePageNumber: 1,
            pageSize: 10,
        },
        totalItemsCount: 0,
        isFetchingDevices: false,
        isFetchingDevicesByGroup: false,
        remainingDevicesCount: 0,
        totalDevicesCount: 0,
        usedDevicesCount: 0,
        tags: [],
        selectedTag: 'ALL',
        tempLocation: {
            lat: "",
            lng: "",
        },
        isGateway: false,
    }

    refreshComponent = () => {
        this.setState({
            isFetching: true,
            searchTerm: '',
            listOfConnectors: [],
            listOfGroupsByConnectorId: [],
            listOfAllDevices: [],
            filteredListOfAllDevices: [],
            selectedConnector: {},
            selectedGroup: '',
            selectedDeviceStatus: 'all',
            tableState: {
                activePageNumber: 1,
                pageSize: 10,
            },
            totalItemsCount: 0,
            isFetchingDevices: false,
            isFetchingDevicesByGroup: false,
            remainingDevicesCount: 0,
            totalDevicesCount: 0,
            usedDevicesCount: 0,
            tags: [],
            selectedTag: 'ALL',

        }, () => {
            let payload = [
                {
                    dependingProductId: null,
                    productName: "no_of_devices"
                }

            ]
            this.props.getDeveloperQuota(payload);
        })
    }

    componentDidMount() {
        let payload = [
            {
                dependingProductId: null,
                productName: "no_of_devices"
            }
        ]
        this.props.getDeveloperQuota(payload);
        document.addEventListener('mousedown', this.handleClickOutside);
    }
    
    static getDerivedStateFromProps(nextProps, state) {
        if (nextProps.developerQuotaSuccess) {
            let remainingDevicesCount = nextProps.developerQuotaSuccess.response.filter(product => product.productName === 'no_of_devices')[0]['remaining'],
                totalDevicesCount = nextProps.developerQuotaSuccess.response.filter(product => product.productName === 'no_of_devices')[0]['total'],
                usedDevicesCount = nextProps.developerQuotaSuccess.response.filter(product => product.productName === 'no_of_devices')[0]['used'];
            nextProps.getAllConnectors();
            return {
                remainingDevicesCount,
                totalDevicesCount,
                usedDevicesCount,
            }

        }
        if (nextProps.developerQuotaFailure) {
            nextProps.getAllConnectors();
            return {
                modalType: "error",
                isOpen: true,
                message2: nextProps.developerQuotaFailure.error,
            }
        }

        if (nextProps.getAllDeviceTypes) {
            let selectedConnector,
                selectedGroup,
                payload;
            if (nextProps.getAllDeviceTypes.response.length) {
                selectedConnector = nextProps.match.params.connectorId ?
                    nextProps.getAllDeviceTypes.response.find(el => el.id === nextProps.match.params.connectorId)
                    : nextProps.getAllDeviceTypes.response[0]
                if (selectedConnector) {
                    selectedGroup = selectedConnector.deviceGroups && selectedConnector.deviceGroups.length ?
                        nextProps.match.params.groupId ? nextProps.match.params.groupId : 'ALL' : '';
                    let tags = selectedConnector.tags;
                    payload = {
                        connectorId: selectedConnector.id,
                        deviceGroupId: selectedGroup,
                        tagId: 'ALL',
                        max: 10,
                        offset: 0
                    };
                    nextProps.getAllDevicesByConnectorId(payload);
                    //nextProps.getAllTags(payload.connectorId)
                    return {
                        listOfConnectors: nextProps.getAllDeviceTypes.response,
                        selectedConnector,
                        selectedGroup,
                        payload,
                        tags,
                        isGateway: selectedConnector.isGateway,
                        isFetching: nextProps.getAllDeviceTypes.response.length ? true : false,
                    }
                }
                else {
                    return {
                        isConnectorIdError: true,
                        listOfConnectors: nextProps.getAllDeviceTypes.response,
                        modalType: "error",
                        isOpen: true,
                        message2: "Incorrect connector Id",
                    }
                }
            } else {
                return {
                    isFetching: false,
                }
            }
        }

        if (nextProps.getAllDeviceTypesFailure) {
            return {
                modalType: "error",
                isOpen: true,
                message2: nextProps.getAllDeviceTypesFailure.error,
                isFetchingDevices: false,
                isFetching: false
            }
        }

        if (nextProps.getAllDevicesByConnectorIdSuccess) {
            let listOfAllDevices = nextProps.getAllDevicesByConnectorIdSuccess.response.devices.length ?
                nextProps.getAllDevicesByConnectorIdSuccess.response.devices : [],
                filteredListOfAllDevices = state.selectedDeviceStatus === 'all' ?
                    listOfAllDevices : listOfAllDevices.filter(device => device.isConnected === state.selectedDeviceStatus)

            return {
                listOfAllDevices,
                filteredListOfAllDevices,
                totalItemsCount: nextProps.getAllDevicesByConnectorIdSuccess.response.deviceCount,
                isFetchingDevices: false,
                isFetchingDevicesByGroup: false,
                isFetching: false,
            }
        }

        if (nextProps.getAllDevicesByConnectorIdFailure) {
            return {
                modalType: "error",
                isOpen: true,
                message2: nextProps.getAllDevicesByConnectorIdFailure.error,
                isFetchingDevices: false,
                isFetching: false
            }
        }

        if (nextProps.editDeviceSuccess) {
            let { deviceId, deviceDetails } = nextProps.editDeviceSuccess
            let filteredListOfAllDevices = cloneDeep(state.filteredListOfAllDevices)
            let requiredDeviceIndex = filteredListOfAllDevices.findIndex((obj => obj._uniqueDeviceId == deviceId));
            filteredListOfAllDevices[requiredDeviceIndex] = { ...filteredListOfAllDevices[requiredDeviceIndex], ...state.selectedDeviceDetails }
            return {
                filteredListOfAllDevices,
                deviceDetailsModalLoader: false,
                openLocationSelector: false,
                selectedDeviceDetails: {},
                isOpen: true,
                modalType: "success",
                message2: nextProps.editDeviceSuccess.response,
            }
        }

        if (nextProps.editDeviceFailure) {
            return {
                deviceDetailsModalLoader: false,
                modalType: "error",
                isOpen: true,
                message2: nextProps.editDeviceFailure.error,
            }
        }

        if (nextProps.deleteDeviceSuccess) {
            let filteredListOfAllDevices = cloneDeep(state.filteredListOfAllDevices)
            filteredListOfAllDevices = filteredListOfAllDevices.filter((obj => obj._uniqueDeviceId !== nextProps.deleteDeviceSuccess.deviceId));
            let message2 = JSON.stringify(nextProps.deleteDeviceSuccess.response) === "{}" ? "Device deleted Successfully." : nextProps.deleteDeviceSuccess.response;
            return {
                modalType: "success",
                isOpen: true,
                message2,
                isLoader: false,
                filteredListOfAllDevices,
                isFetching: false
            }
        }

        if (nextProps.deleteDeviceFailure) {
            return {
                modalType: "error",
                isOpen: true,
                message2: nextProps.deleteDeviceFailure.error,
                isLoader: false,
            }
        }

        if (Object.keys(SELECTORS).filter(prop => prop.toLowerCase().includes("success")).some(prop => nextProps[prop])) {
            let propName = Object.keys(SELECTORS).find(props => nextProps[props]);
            return { ...getNotificationObj(nextProps[propName].response.message, "success") }
        }
        if (Object.keys(SELECTORS).filter(prop => prop.toLowerCase().includes("failure")).some(prop => nextProps[prop])) {
            let propName = Object.keys(SELECTORS).find(props => nextProps[props]);
            return { ...getNotificationObj(nextProps[propName].error, "error"), isFetching: false, isFetchingDevices: false, isFetchingDevicesByGroup: false }
        }
        return null
    }

    componentDidUpdate(prevProps, prevState) {
        if (Object.keys(SELECTORS).some(prop => this.props[prop])) {
            this.props.resetToInitialState()
        }
    }
    
    onCloseHandler = () => {
        let isConnectorIdError = this.state.isConnectorIdError
        this.setState({
            isOpen: false,
            message2: '',
            modalType: '',
            isConnectorIdError: false
        }, () => {
            if (isConnectorIdError) {
                let selectedConnector,
                    selectedGroup,
                    payload;
                selectedConnector = cloneDeep(this.state.listOfConnectors)[0]
                selectedGroup = selectedConnector.deviceGroups.length ? 'ALL' : ''
                payload = {
                    connectorId: selectedConnector.id,
                    deviceGroupId: selectedGroup,
                    tagId: 'ALL',
                    max: 5,
                    offset: 0
                }
                this.props.getAllDevicesByConnectorId(payload)
                //this.props.getAllTags(payload.connectorId)
                this.setState({
                    selectedConnector,
                    selectedGroup,
                    payload,
                    isFetching: true,
                }, () => this.props.history.push('/devices'))
            }
        })
    }
    
    onSearchHandler = (e) => {
        let filteredListOfAllDevices = cloneDeep(this.state.listOfAllDevices)
        if (e.target.value.length > 0) {
            filteredListOfAllDevices = filteredListOfAllDevices.filter(item => item.name && item.name.toLowerCase().includes(e.target.value.toLowerCase()))
        }
        this.setState({
            filteredListOfAllDevices,
            searchTerm: e.target.value
        })
    }

    emptySearchBox = () => {
        this.setState({
            searchTerm: '',
            filteredListOfAllDevices: cloneDeep(this.state.listOfAllDevices)
        })
    }

    toggleConfirmModal = (deviceDetails) => {
        let { _uniqueDeviceId, name } = deviceDetails || {},
            filteredListOfAllDevices = cloneDeep(this.state.filteredListOfAllDevices),
            deviceToBeDeletedIndex = _uniqueDeviceId ? filteredListOfAllDevices.findIndex((obj => obj._uniqueDeviceId == _uniqueDeviceId)) : null,
            confirmState = Boolean(_uniqueDeviceId),
            deviceToBeDeletedName = _uniqueDeviceId ? name || _uniqueDeviceId : null
        this.setState({
            deviceToBeDeletedIndex,
            confirmState,
            deviceToBeDeletedName
        })
    }

    deleteDeviceHandler = () => {
        let filteredListOfAllDevices = cloneDeep(this.state.filteredListOfAllDevices),
            deviceId = filteredListOfAllDevices[this.state.deviceToBeDeletedIndex]._uniqueDeviceId,
            connectorId = filteredListOfAllDevices[this.state.deviceToBeDeletedIndex].connectorId
        this.setState({
            isFetching: true,
            confirmState: false,
            isLoader: true,
            deviceToBeDeletedIndex: null,
            deviceToBeDeletedName: null,
        }, () => this.props.deleteDevice(deviceId, connectorId))
    }

    filterChangeHandler = ({ currentTarget }) => {
        let filteredListOfAllDevices = cloneDeep(this.state.listOfAllDevices),
            payload = {
                connectorId: this.state.selectedConnector.id,
                deviceGroupId: this.state.selectedGroup,
                tagId: this.state.selectedTag,
                max: this.state.tableState.pageSize,
                offset: 0
            },
            selectedGroup = this.state.selectedGroup,
            selectedConnector = cloneDeep(this.state.selectedConnector),
            selectedTag = this.state.selectedTag,
            tags = this.state.tags,
            isGateway = this.state.isGateway;

        if (currentTarget.id === 'selectedConnector') {
            selectedConnector = this.state.listOfConnectors.find(el => el.id === currentTarget.value);
            payload.connectorId = selectedConnector.id;
            payload.deviceGroupId = selectedConnector.deviceGroups.length > 0 ? 'ALL' : '';
            payload.tagId = 'ALL';
            selectedTag = 'ALL';
            selectedGroup = selectedConnector.deviceGroups.length > 0 ? 'ALL' : '';
            tags = selectedConnector.tags;
            isGateway = selectedConnector.isGateway;
        }
        else if (currentTarget.id === 'selectedGroup') {
            payload.deviceGroupId = currentTarget.value
            selectedGroup = currentTarget.value
        }
        else if (currentTarget.id === 'selectedTag') {
            payload.tagId = currentTarget.value;
            selectedTag = currentTarget.value;
        }
        let tableState = { ...this.state.tableState }
        tableState.activePageNumber = 1

        this.setState({
            selectedConnector,
            selectedGroup,
            selectedTag,
            tags,
            isFetchingDevices: currentTarget.id === 'selectedConnector' || filteredListOfAllDevices.length === 0,
            isFetchingDevicesByGroup: currentTarget.id === 'selectedGroup' || currentTarget.id === 'selectedTag',
            filteredListOfAllDevices,
            payload,
            tableState,
            isGateway,
        }, () => {
            this.props.getAllDevicesByConnectorId(payload)
            this.props.match.params.connectorId && currentTarget.id === 'selectedConnector' ? this.props.history.push('/devices') : null
        })
    }

    addressHandler = (address, lat, lng) => {
        let selectedDeviceDetails = cloneDeep(this.state.selectedDeviceDetails)
        if (selectedDeviceDetails.lat === lat && selectedDeviceDetails.lng === lng) {
            selectedDeviceDetails.location = address
            this.setState({ selectedDeviceDetails, tempLocation: { lat, lng }, mapError: null })
        }
    }

    openLocationModal = (row) => {
        let { location, _uniqueDeviceId, lat, lng, name } = row
        this.setState({
            openLocationSelector: true,
            selectedDeviceDetails: {
                location,
                _uniqueDeviceId,
                lat,
                lng,
                name
            },
            tempLocation: {
                lat: lat || "",
                lng: lng || "",
            },
            mapError: null,
        }, () => {
            if (!lat && navigator.geolocation) {
                navigator.geolocation.getCurrentPosition((position) => {
                    let selectedDeviceDetails = cloneDeep(this.state.selectedDeviceDetails)
                    selectedDeviceDetails.lat = position.coords.latitude
                    selectedDeviceDetails.lng = position.coords.longitude
                    this.setState({
                        selectedDeviceDetails
                    }, () => getAddressFromLatLng(this.state.selectedDeviceDetails.lat, this.state.selectedDeviceDetails.lng, this.addressHandler)
                    )
                });
            }
        })
    }

    setLocation = (location, lat, lng) => {
        let selectedDeviceDetails = cloneDeep(this.state.selectedDeviceDetails)
        selectedDeviceDetails.lat = lat
        selectedDeviceDetails.lng = lng
        selectedDeviceDetails.location = location
        this.setState({ selectedDeviceDetails, tempLocation: { lat, lng }, mapError: null, isInvalidAddress: false })
    }

    saveDeviceDetailsHandler = () => {
        let selectedDeviceDetails = cloneDeep(this.state.selectedDeviceDetails)
        selectedDeviceDetails.name = this.state.selectedDeviceDetails.name ? this.state.selectedDeviceDetails.name.trim() : ""
        if (selectedDeviceDetails.location && !this.state.selectedDeviceDetails.lat) {
            selectedDeviceDetails.location = ""
        }
        let uniqueDeviceId = selectedDeviceDetails._uniqueDeviceId
        delete selectedDeviceDetails._uniqueDeviceId
        this.setState({
            deviceDetailsModalLoader: true
        }, () => this.props.editDevice(selectedDeviceDetails, uniqueDeviceId, this.state.selectedConnector.id))
    }

    copyToClipboard = (id) => {
        var copyText = document.getElementById(id);
        copyText.select();
        document.execCommand("copy");
        let element = $(`#Copied${id[id.length - 1]}`)[0]
        this.setState({
            modalType: "success",
            isOpen: true,
            message2: 'Copied to Clipboard successfully !',
        })
    }

    getColumns = () => {
        if (this.state.isGateway) {
            return [
                {
                    Header: "Device Id",
                    width: 10,
                    cell: (row) => {
                        return (
                            <React.Fragment>
                                {this.state.selectedConnector.assetType === "Fixed" ?
                                    <div className="list-git-icon alert-warning" data-tooltip data-tooltip-text="Fixed" data-tooltip-place="bottom"><i className="fas fa-map-marker-alt"></i></div>:
                                    <div className="list-git-icon alert-info" data-tooltip data-tooltip-text="Movable" data-tooltip-place="bottom"><i className="fas fa-route"></i></div>
                                }
                                <input type="text" id={`copyText${row.index}`} style={{ opacity: 0, position: 'absolute' }} value={row._uniqueDeviceId} readOnly />
                                <h6 className="text-theme fw-600">
                                    <button className="btn-transparent btn-transparent-green copy-button" data-tooltip data-tooltip-text="Copy" data-tooltip-place="bottom" onClick={() => this.copyToClipboard(`copyText${row.index}`)}>
                                        <i className="far fa-copy"></i>
                                    </button>
                                    {row._uniqueDeviceId}
                                </h6>
                            </React.Fragment>
                        )
                    }
                },
                {
                    Header: "Gateway Id",
                    width: 10,
                    cell: (row) => {
                        return (
                            row.gatewayId ?
                                <React.Fragment>
                                    <input type="text" id={`copyText_gatewayId`} style={{ opacity: 0, position: 'absolute' }} value={row.gatewayId} readOnly />
                                    <h6>
                                        <button className="btn-transparent btn-transparent-green copy-button" data-tooltip data-tooltip-text="Copy" data-tooltip-place="bottom" onClick={() => this.copyToClipboard('copyText_gatewayId')}>
                                            <i className="far fa-copy"></i>
                                        </button>
                                        {row.gatewayId}
                                    </h6>
                                </React.Fragment>: "-"
                        )
                    }
                },
                {
                    Header: "Device Info",
                    width: 15,
                    cell: row => (
                        <React.Fragment>
                            {row.location ?
                                <div className="list-view-icon list-view-icon-link" onClick={() => window.open(`https://www.google.com/maps/place/${row.lat},${row.lng}`)}>
                                    <img src={`https://maps.googleapis.com/maps/api/staticmap?center=${row.location}&zoom=6&size=75x75&key=AIzaSyDhe3XJyqFibPDk5tRdcAMeH09cv7Elc08`}></img>
                                </div> : ""
                            }
                            <div className={row.location ? "list-view-icon-box" : ""}>
                                <h6>{row.name}</h6>
                                <p>{row.location ? row.location : "-"}</p>
                            </div>
                        </React.Fragment>
                    ),
                },
                {
                    Header: "Device Type",
                    width: 10,
                    cell: row => (
                        <div className="button-group-link">
                            <button className="btn btn-link text-truncate w-100" disabled={row.deviceCount === 0} onClick={() => { isNavAssigned('deviceTypes') && this.props.history.push(`/addOrEditDeviceType/${row.connectorId}`) }}>{this.state.selectedConnector.name}</button>
                        </div>
                    )
                },
                {
                    Header: "Device Group",
                    width: 10,
                    cell: row => {
                        let deviceGroup = this.state.selectedConnector['deviceGroups'].find(group => group.deviceGroupId === row.deviceGroupId).name
                        return (
                            <div className="button-group-link">
                                <button className="btn btn-link text-truncate w-100" disabled={row.deviceCount === 0} onClick={() => { isNavAssigned('deviceGroups') && this.props.history.push({ pathname: `/deviceGroups/${this.state.selectedConnector.id}/${row.deviceGroupId}`, state: { childViewType: 'GROUP_DETAILS' } }) }}>{deviceGroup}</button>
                            </div>
                        )
                    }
                },
                {
                    Header: "Tag",
                    width: 10,
                    cell: row => (row.tag &&
                        <span className="badge badge-pill badge-light list-view-badge-pill text-underline-hover" style={{ color: row.tagColor }} onClick={() => { isNavAssigned('deviceGroups') && this.props.history.push({ pathname: `/deviceGroups/${this.state.selectedConnector.id}/${row.deviceGroupId}`, state: { childViewType: 'GROUP_TAGVIEW' } }) }}>{row.tag}</span>
                    )
                },
                {
                    Header: "Last Reported",
                    width: 15,
                    cell: row => (
                        <React.Fragment>
                            <h6>{convertTimestampToDate(row.lastReportedAt)}</h6>
                            <p className={row.status == "online" ? "text-green" : row.status == "offline" ? "text-red" : "text-gray"}>{getTimeDifference(row.lastReportedAt)}</p>
                        </React.Fragment>
                    )
                },
                {
                    Header: "Status",
                    width: 10,
                    cell: row => (
                        <span className={`badge badge-pill list-view-pill ${row.status === "online" ? "alert-success" : row.status === "offline" ? "alert-danger" : "alert-dark"}`}>
                            <i className="fad fa-circle mr-r-5"></i>{status[row.status]}
                        </span>
                    )
                },
                {
                    Header: "Actions",
                    width: 10,
                    cell: (row) => (
                        <div className="button-group">
                            <button className="btn-transparent btn-transparent-blue" data-tooltip data-tooltip-text="Edit" data-tooltip-place="bottom" onClick={() => this.openLocationModal(row)}><i className="far fa-pencil"></i></button>
                            <button className="btn-transparent btn-transparent-yellow" data-tooltip data-tooltip-text="Debug" data-tooltip-place="bottom" disabled={!row.status} onClick={() => isNavAssigned('deviceDiagnostics') && window.open(`/deviceDiagnostics/${row.connectorId}/${row._uniqueDeviceId}`, '_blank')}><i className="far fa-bug"></i></button>
                            {!row.gatewayId &&
                            <div className="list-view-alarm-count">
                                <i className="fad fa-router text-indigo"></i>
                            </div>
                            }
                        </div>
                    )
                }
            ]
        } else {
            return [
                {
                    Header: "Device Id",
                    width: "12_5",
                    cell: (row) => {
                        return (
                            <React.Fragment>
                                {this.state.selectedConnector.assetType === "Fixed" ?
                                    <div className="list-git-icon alert-warning" data-tooltip data-tooltip-text="Fixed" data-tooltip-place="bottom"><i className="fas fa-map-marker-alt"></i></div>:
                                    <div className="list-git-icon alert-info" data-tooltip data-tooltip-text="Movable" data-tooltip-place="bottom"><i className="fas fa-route"></i></div>
                                }
                                <input type="text" id={`copyText${row.index}`} style={{ opacity: 0, position: 'absolute' }} value={row._uniqueDeviceId} readOnly />
                                <h6 className="text-theme fw-600">
                                    <button className="btn-transparent btn-transparent-green copy-button" data-tooltip data-tooltip-text="Copy" data-tooltip-place="bottom" onClick={() => this.copyToClipboard(`copyText${row.index}`)}>
                                        <i className="far fa-copy"></i>
                                    </button>
                                    {row._uniqueDeviceId}
                                </h6>
                            </React.Fragment>
                        )
                    }
                },
                {
                    Header: "Device Info",
                    width: 20,
                    cell: row => (
                        <React.Fragment>
                            {row.location ?
                                <div className="list-view-icon list-view-icon-link" onClick={() => window.open(`https://www.google.com/maps/place/${row.lat},${row.lng}`)}>
                                    <img src={`https://maps.googleapis.com/maps/api/staticmap?center=${row.location}&zoom=6&size=75x75&key=AIzaSyDhe3XJyqFibPDk5tRdcAMeH09cv7Elc08`}></img>
                                </div> : ""
                            }
                            <div className={row.location ? "list-view-icon-box" : ""}>
                                <h6>{row.name}</h6>
                                <p>{row.location ? row.location : "-"}</p>
                            </div>
                        </React.Fragment>
                    ),
                },
                {
                    Header: "Device Type",
                    width: 10,
                    cell: row => (
                        <div className="button-group-link">
                            <button className="btn btn-link text-truncate w-100" disabled={row.deviceCount === 0} onClick={() => { isNavAssigned('deviceTypes') && this.props.history.push(`/addOrEditDeviceType/${row.connectorId}`) }}>{this.state.selectedConnector.name}</button>
                        </div>
                    )
                },
                {
                    Header: "Device Group",
                    width: 10,
                    cell: row => {
                        let deviceGroup = this.state.selectedConnector['deviceGroups'].find(group => group.deviceGroupId === row.deviceGroupId).name
                        return (
                            <div className="button-group-link">
                                <button className="btn btn-link text-truncate w-100" disabled={row.deviceCount === 0} onClick={() => { isNavAssigned('deviceGroups') && this.props.history.push({ pathname: `/deviceGroups/${this.state.selectedConnector.id}/${row.deviceGroupId}`, state: { childViewType: 'GROUP_DETAILS' } }) }}>{deviceGroup}</button>
                            </div>
                        )
                    }
                },
                {
                    Header: "Tag",
                    width: 10,
                    cell: row => (row.tag &&
                        <span className="badge badge-pill badge-light list-view-badge-pill text-underline-hover" style={{ color: row.tagColor }} onClick={() => { isNavAssigned('deviceGroups') && this.props.history.push({ pathname: `/deviceGroups/${this.state.selectedConnector.id}/${row.deviceGroupId}`, state: { childViewType: 'GROUP_TAGVIEW' } }) }}>{row.tag}</span>
                    )
                },
                {
                    Header: "Last Reported",
                    width: "17_5",
                    cell: row => (
                        <React.Fragment>
                            <h6>{convertTimestampToDate(row.lastReportedAt)}</h6>
                            <p className={row.status == "online" ? "text-green" : row.status == "offline" ? "text-red" : "text-gray"}>{getTimeDifference(row.lastReportedAt)}</p>
                        </React.Fragment>
                    )
                },
                {
                    Header: "Status",
                    width: 10,
                    cell: row => (
                        <span className={`badge badge-pill list-view-pill ${row.status === "online" ? "alert-success" : row.status === "offline" ? "alert-danger" : "alert-dark"}`}>
                            <i className="fad fa-circle mr-r-5"></i>{status[row.status]}
                        </span>
                    )
                },
                {
                    Header: "Actions",
                    width: 10,
                    cell: (row) => (
                        <div className="button-group">
                            <button className="btn-transparent btn-transparent-blue" data-tooltip data-tooltip-text="Edit" data-tooltip-place="bottom" onClick={() => this.openLocationModal(row)}><i className="far fa-pencil"></i></button>
                            <button className="btn-transparent btn-transparent-yellow" data-tooltip data-tooltip-text="Debug" data-tooltip-place="bottom" disabled={!row.status} onClick={() => isNavAssigned('deviceDiagnostics') && window.open(`/deviceDiagnostics/${row.connectorId}/${row._uniqueDeviceId}`, '_blank')}><i className="far fa-bug"></i></button>
                        </div>
                    )
                }
            ]
        }
    }

    getStatusBarClass = (row) => {
        if (row.status === 'online')
            return { className: "green", tooltipText: "Online" }
        else if (row.status === 'offline')
            return { className: "red", tooltipText: "Offline" }
        return { className: "gray", tooltipText: "Not connected" }
    }

    pageChangeHandler = (pageSize, activePageNumber) => {
        let pageSizeChanged = pageSize !== this.state.tableState.pageSize;
        let totalPages = Math.ceil(this.state.totalItemsCount / pageSize);
        if (pageSizeChanged) {
            if (totalPages < activePageNumber) {
                activePageNumber = totalPages
            }
        }
        else if (activePageNumber === "...") {
            activePageNumber = Math.ceil((this.getPageNumbers()[3] - 1) / 2)
        }
        let payload = {
            connectorId: this.state.selectedConnector.id,
            deviceGroupId: this.state.selectedGroup,
            max: pageSize,
            offset: (activePageNumber - 1) * pageSize,
        }
        let tableState = { ...this.state.tableState, pageSize, activePageNumber }
        this.setState({
            isFetchingDevices: true,
            tableState,
        }, () => this.props.getAllDevicesByConnectorId(payload))
    }

    getPageNumbers = () => {
        let totalPages = Math.ceil(this.state.totalItemsCount / this.state.tableState.pageSize);
        let activePageNumber = this.state.tableState.activePageNumber;

        if (totalPages < 6)
            return range(1, totalPages + 1)
        else {
            let start = activePageNumber < 4 ? 1 : activePageNumber - 2
            let end = activePageNumber < 4 ? 6 : (activePageNumber + 3 > totalPages) ? totalPages + 1 : activePageNumber + 3
            if (end - start < 5)
                start = end - 5
            let arr = range(start, end)
            if (start !== 1)
                arr = [1, "...", ...arr]
            return arr
        }
    }

    deviceNameChangeHandler = ({ currentTarget }) => {
        let selectedDeviceDetails = cloneDeep(this.state.selectedDeviceDetails)
        let value = currentTarget.value
        if (/^[a-zA-Z0-9][a-zA-Z0-9 _-]*$/.test(value) || /^$/.test(value)) {
            selectedDeviceDetails.name = value.trimStart()
        }
        this.setState({ selectedDeviceDetails })
    }

    locationChangeHandler = ({ currentTarget }) => {
        let selectedDeviceDetails = cloneDeep(this.state.selectedDeviceDetails)
        selectedDeviceDetails.location = currentTarget.value
        selectedDeviceDetails.lat = ""
        selectedDeviceDetails.lng = ""
        this.setState({
            selectedDeviceDetails,
            tempLocation: { lat: "", lng: "" },
            isInvalidAddress: false
        })
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClickOutside);
    }

    handleClickOutside = (event) => {
        if (event.target.id !== "mapSearch") {
            this.setState({
                isInvalidAddress: this.state.selectedDeviceDetails && this.state.selectedDeviceDetails.location && !this.state.selectedDeviceDetails.lat,
            })
        }
    }

    onFocus = () => {
        this.setState({
            isInvalidAddress: this.state.selectedDeviceDetails && this.state.selectedDeviceDetails.location && !this.state.selectedDeviceDetails.lat,
        })
    }

    tempLocationChangeHandler = ({ currentTarget }) => {
        let tempLocation = cloneDeep(this.state.tempLocation)
        let selectedDeviceDetails = cloneDeep(this.state.selectedDeviceDetails)
        tempLocation[currentTarget.id] = currentTarget.value ? Number(currentTarget.value) : ""
        selectedDeviceDetails.location = ""
        selectedDeviceDetails.lat = ""
        selectedDeviceDetails.lng = ""
        this.setState({
            tempLocation,
            mapError: null,
            selectedDeviceDetails,
        })
    }

    isInvalidLatLng = () => {
        let { lat, lng } = this.state.tempLocation
        let inValidLat = !(lat >= -90 && lat <= 90)
        let inValidLng = !(lng >= -180 && lng <= 180)
        return inValidLat || inValidLng
    }

    handleMapError = (mapError) => {
        this.setState({
            mapError
        })
    }

    handleLatLngSearch = () => {
        if (this.state.tempLocation.lat && this.state.tempLocation.lng) {
            return getAddressFromLatLng(this.state.tempLocation.lat, this.state.tempLocation.lng, this.setLocation, this.handleMapError)
        }
        this.setState({
            mapError: "Missing or Invalid Lat/Lng"
        })
    }

    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>Devices</title>
                    <meta name="description" content="Description of Device Management" />
                </Helmet>

                <header className="content-header d-flex">
                    <div className="flex-60">
                        <h6>Devices -
                            <span className="content-header-badge-group">
                                <span className="content-header-badge-item">Allocated <span className="badge badge-pill badge-primary">{this.state.totalDevicesCount}</span></span>
                                <span className="content-header-badge-item">Used <span className="badge badge-pill badge-success">{this.state.usedDevicesCount}</span></span>
                                <span className="content-header-badge-item">Remaining <span className="badge badge-pill badge-warning">{this.state.remainingDevicesCount}</span></span>
                            </span>
                        </h6>
                    </div>
                    <div className="flex-40 text-right">
                        <div className="content-header-group">
                            <button className={this.state.toggleView == true ? "btn btn-light active" : "btn btn-light"} disabled={this.state.isFetching} data-tooltip="true" data-tooltip-text="List View" data-tooltip-place="bottom" onClick={() => this.setState({ toggleView: true })}><i className="fad fa-list-ul"></i></button>
                            <button className={this.state.toggleView == false ? "btn btn-light active" : "btn btn-light"} disabled={this.state.isFetching} data-tooltip="true" data-tooltip-text="Card View" data-tooltip-place="bottom" onClick={() => this.setState({ toggleView: false })}><i className="fad fa-table"></i></button>
                            <button className="btn btn-light" disabled={this.state.isFetching || this.state.isFetchingDevices} data-tooltip data-tooltip-text="Refresh" data-tooltip-place="bottom" onClick={() => this.refreshComponent()}><i className="far fa-sync-alt"></i></button>
                            {/* <div className="search-box">
                                <span className="search-icon"><i className="far fa-search"></i></span>
                                <input type="text" className="form-control" placeholder="Search..." value={this.state.searchTerm} onChange={this.onSearchHandler} />
                                {this.state.searchTerm.length > 0 &&
                                    <button className="search-button" onClick={() => this.emptySearchBox()}><i className="far fa-times"></i></button>
                                }
                            </div>
                            <button className="btn btn-light"><i className="fad fa-list-ul"></i></button>
                            <button className="btn btn-light"><i className="fad fa-table"></i></button> */}
                        </div>
                    </div>
                </header>

                {this.state.isFetching ?
                    <Loader /> :
                    <React.Fragment>
                        {this.state.listOfConnectors.length > 0 &&
                            <div className="d-flex content-filter">
                                <div className="flex-33 form-group pd-r-10">
                                    <div className="input-group">
                                        <div className="input-group-prepend">
                                            <span className="input-group-text">Device Type :</span>
                                        </div>
                                        <select id='selectedConnector' className="form-control" disabled={this.state.isFetchingDevices || this.state.isFetchingDevicesByGroup} onChange={this.filterChangeHandler}>
                                            {this.state.listOfConnectors.map(connector => {
                                                return (
                                                    <option key={connector.id} selected={connector.id === this.state.selectedConnector.id} value={connector.id}>{connector.name}</option>
                                                )
                                            })}
                                        </select>
                                    </div>
                                </div>
                                <div className="flex-33 form-group pd-r-5 pd-l-5">
                                    <div className="input-group">
                                        <div className="input-group-prepend">
                                            <span className="input-group-text">Device Group :</span>
                                        </div>
                                        <select id='selectedGroup' className="form-control" disabled={this.state.isFetchingDevices || this.state.isFetchingDevicesByGroup} onChange={this.filterChangeHandler}>
                                            {this.state.selectedConnector['deviceGroups'].length > 0 ?
                                                <option value='ALL'>All</option> :
                                                <option value=''>No Groups Available</option>
                                            }
                                            {this.state.selectedConnector['deviceGroups'].map(group =>
                                                <option key={group.deviceGroupId} selected={group.deviceGroupId === this.state.selectedGroup} value={group.deviceGroupId}>{group.name}</option>
                                            )}
                                        </select>
                                    </div>
                                </div>
                                <div className="flex-33 form-group pd-l-10">
                                    <div className="input-group">
                                        <div className="input-group-prepend">
                                            <span className="input-group-text">Device Tags :</span>
                                        </div>
                                        <select id='selectedTag' className="form-control" disabled={this.state.isFetchingDevices || this.state.isFetchingDevicesByGroup} onChange={this.filterChangeHandler}>
                                            <option value='ALL'>All</option>
                                            {this.state.tags.map(tagObj =>
                                                <option key={tagObj.id} selected={this.state.selectedTag === tagObj.id} value={tagObj.id}>{tagObj.tag}</option>
                                            )}
                                        </select>
                                    </div>
                                </div>
                                {/*<div className="flex-33 form-group pd-l-10">
                                    <label className="form-group-label">Device Status :</label>
                                    <div className="input-group">
                                        <select id='selectedDeviceStatus' className="form-control" value={this.state.selectedDeviceStatus} onChange={this.filterChangeHandler}>
                                            <option value='all'>All</option>
                                            <option value='online'>Online</option>
                                            <option value='offline'>Offline</option>
                                            <option value='not-connected'>Not Connected</option>
                                        </select>
                                    </div>
                                </div>*/}
                            </div>
                        }
                        
                        <div className={`content-body ${this.state.listOfConnectors.length > 0 && "content-filter-body"}`}>
                            {this.state.isFetchingDevices ?
                                <div className="inner-loader-wrapper h-100">
                                    <div className="inner-loader-content">
                                        <i className="fad fa-sync-alt fa-spin"></i>
                                    </div>
                                </div> :
                                this.state.listOfConnectors.length > 0 ?
                                    this.state.listOfAllDevices.length > 0 ?
                                        this.state.filteredListOfAllDevices.length > 0 ?
                                            this.state.toggleView ?
                                                <ListingTable
                                                    columns={this.getColumns()}
                                                    data={this.state.filteredListOfAllDevices}
                                                    statusBar={this.getStatusBarClass}
                                                    pageSize={this.state.tableState.pageSize}
                                                    activePageNumber={this.state.tableState.activePageNumber}
                                                    totalItemsCount={this.state.totalItemsCount}
                                                    pageChangeHandler={this.pageChangeHandler}
                                                    isLoading={this.state.isFetchingDevicesByGroup}
                                                />
                                                :   
                                                <ul className="card-view-list">
                                                    {this.state.filteredListOfAllDevices.map((item, index) =>
                                                        <li key={index}>
                                                            <div className="card-view-box">
                                                                <div className="card-view-header">
                                                                    <span data-tooltip data-tooltip-text={item.status === "online" ? "Online" : item.status === "offline" ? "Offline" : "Not-connected"} data-tooltip-place="bottom" className={item.status === "online" ? "card-view-header-status bg-success" : item.status === "offline" ? "card-view-header-status bg-danger" : "card-view-header-status bg-light"}></span>
                                                                    {this.state.isGateway ? !item.gatewayId &&
                                                                    <span className="mr-r-10 f-13"><i className="fad fa-router text-indigo"></i></span> : ""
                                                                    }
                                                                    <span className={`alert mr-r-7 ${item.status === "online" ? "alert-success" : item.status === "offline" ? "alert-danger" : "alert-dark"}`} >
                                                                        <i className="fad fa-circle mr-2"></i>{status[item.status]}
                                                                    </span>
                                                                    {this.state.selectedConnector.assetType === "Fixed" ?
                                                                        <span className="alert alert-warning"><i className="fas fa-map-marker-alt mr-2"></i>Fixed</span>:
                                                                        <span className="alert alert-info"><i className="fas fa-route mr-2"></i>Movable</span>
                                                                    }
                                                                    <div className="dropdown">
                                                                        <button className="btn-transparent btn-transparent-gray" type="button" data-toggle="dropdown">
                                                                            <i className="fas fa-ellipsis-v"></i>
                                                                        </button>
                                                                        <div className="dropdown-menu">
                                                                            <button className="dropdown-item" onClick={() => this.openLocationModal(item)}><i className="far fa-pencil"></i>Edit</button>
                                                                            <button className="dropdown-item" disabled={!item.status} onClick={() => isNavAssigned('deviceDiagnostics') && window.open(`/deviceDiagnostics/${item.connectorId}/${item._uniqueDeviceId}`, '_blank')}><i className="far fa-bug"></i>Debug</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div className="card-view-body">
                                                                    <div className={`card-view-icon ${item.location ? "cursor-pointer" : ""}`}>
                                                                        {item.location ?
                                                                            <img src={`https://maps.googleapis.com/maps/api/staticmap?center=${item.location}&zoom=6&size=75x75&key=AIzaSyDhe3XJyqFibPDk5tRdcAMeH09cv7Elc08`} onClick={() => window.open(`https://www.google.com/maps/place/${item.lat},${item.lng}`)} />
                                                                            :
                                                                            <i className="fad fa-map-marker-alt"></i>
                                                                        }
                                                                    </div>
                                                                    <h6><i className="far fa-info"></i>
                                                                        <input type="text" id={`copyText${index}`} style={{ opacity: 0, position: 'absolute' }} value={item._uniqueDeviceId} readOnly />
                                                                        <button className="btn-transparent btn-transparent-green copy-button float-none d-inline-block" data-tooltip data-tooltip-text="Copy" data-tooltip-place="bottom" onClick={() => this.copyToClipboard(`copyText${index}`)}>
                                                                            <i className="far fa-copy text-green"></i>
                                                                        </button>
                                                                        {item._uniqueDeviceId}
                                                                    </h6>
                                                                    {this.state.isGateway ?
                                                                        <p><i className="far fa-router"></i>
                                                                            <input type="text" id={`copyText_gatewayId${index}`} style={{ opacity: 0, position: 'absolute' }} value={item.gatewayId} readOnly />
                                                                            <button className="btn-transparent btn-transparent-green copy-button float-none d-inline-block" data-tooltip data-tooltip-text="Copy" data-tooltip-place="bottom" onClick={() => this.copyToClipboard(`copyText_gatewayId${index}`)}>
                                                                                <i className="far fa-copy text-green"></i>
                                                                            </button>
                                                                            {item.gatewayId}
                                                                        </p>:""
                                                                    }
                                                                    <p><i className="far fa-address-book"></i>{item.name ? item.name : "-"}</p>
                                                                    <p><i className="far fa-map-marker-alt"></i>{item.location ? item.location : "-"}</p>
                                                                    <p className="text-primary text-underline-hover cursor-pointer" disabled={item.deviceCount === 0} onClick={() => { isNavAssigned('deviceTypes') && this.props.history.push(`/addOrEditDeviceType/${item.connectorId}`) }}>
                                                                        <i className="far fa-microchip"></i>{this.state.selectedConnector.name}
                                                                    </p>
                                                                    <p>
                                                                        <i className="far fa-history"></i>{convertTimestampToDate(item.lastReportedAt)}
                                                                        {item.lastReportedAt ?
                                                                            <strong className={`ml-2 ${item.status == "online" ? "text-green" : item.status == "offline" ? "text-red" : "text-gray"}`}>({getTimeDifference(item.lastReportedAt)})</strong>
                                                                            :""
                                                                        }
                                                                    </p>
                                                                </div>
                                                                <div className="card-view-footer d-flex">
                                                                    <div className="flex-50 p-3">
                                                                        <h6>Group</h6>
                                                                        <p className="text-underline-hover cursor-pointer" disabled={item.deviceCount === 0} onClick={() => { isNavAssigned('deviceGroups') && this.props.history.push({ pathname: `/deviceGroups/${this.state.selectedConnector.id}/${item.deviceGroupId}`, state: { childViewType: 'GROUP_DETAILS' } }) }}>
                                                                            {this.state.selectedConnector['deviceGroups'].find(group => group.deviceGroupId === item.deviceGroupId).name}
                                                                        </p>
                                                                    </div>
                                                                    <div className="flex-50 p-3">
                                                                        <h6>Tag</h6>
                                                                        <p className="text-underline-hover cursor-pointer" style={{ color: item.tagColor }} onClick={() => { isNavAssigned('deviceGroups') && this.props.history.push({ pathname: `/deviceGroups/${this.state.selectedConnector.id}/${item.deviceGroupId}`, state: { childViewType: 'GROUP_TAGVIEW' } }) }}>
                                                                            {item.tag ? item.tag : "-"}
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    )}
                                                </ul>
                                            :
                                            <NoDataFoundMessage /> :
                                        <AddNewButton
                                            text1="No device(s) available"
                                            text2="You haven't generate any device(s) yet. Please generate the device(s) first"
                                            imageIcon="addDevice.png"
                                            addButtonEnable={false}
                                        /> :
                                    <AddNewButton
                                        text1="No device(s) available"
                                        text2="You haven't created any device type(s) yet. Please create a device type first."
                                        addButtonEnable={isNavAssigned("deviceTypes")}
                                        createItemOnAddButtonClick={() => this.props.history.push(`/addOrEditDeviceType`)}
                                        imageIcon="addDevice.png"
                                    />
                            }
                        </div>
                    </React.Fragment>
                }

                {/* map modal */}
                {this.state.openLocationSelector &&
                    <div className="modal d-block animated slideInDown" id="locationModal">
                        <div className="modal-dialog modal-lg modal-dialog-centered">
                            <div className="modal-content">
                                <div className="modal-header position-relative">
                                    <h6 className="modal-title">Add Details - <span>{this.state.selectedDeviceDetails.name ? this.state.selectedDeviceDetails.name : this.state.selectedDeviceDetails._uniqueDeviceId}</span>
                                        <button type="button" className="close" data-tooltip data-tooltip-text="Close" data-tooltip-place="bottom" disabled={this.state.deviceDetailsModalLoader} onClick={() => this.setState({ openLocationSelector: false })} data-dismiss="modal">
                                            <i className="far fa-times"></i>
                                        </button>
                                    </h6>
                                </div>
                                <div className="modal-body location-map-wrapper">
                                    {this.state.deviceDetailsModalLoader ?
                                        <div className="inner-loader-wrapper" style={{ height: 409 }}>
                                            <div className="inner-loader-content">
                                                <i className="fad fa-sync-alt fa-spin"></i>
                                            </div>
                                        </div> :
                                        <React.Fragment>
                                            <div className="d-flex">
                                                <div className="flex-40 pd-r-10">
                                                    <div className="form-group">
                                                        <label className="form-group-label">Device Name <small>(minimum 4 characters)</small> :</label>
                                                        <input type="text" id="deviceName" className="form-control" value={this.state.selectedDeviceDetails.name || ""} placeholder="Unnamed Device" onChange={this.deviceNameChangeHandler} maxLength="30" />
                                                    </div>
                                                    <div className="form-group">
                                                        <label className="form-group-label">Location :</label>
                                                        <div className="auto-complete-box">
                                                            <MapSearchBox onFocus={this.onFocus} onSelect={this.setLocation} value={this.state.selectedDeviceDetails.location || ""} onChange={this.locationChangeHandler} />
                                                            {this.state.isInvalidAddress && <span className="form-group-error">No Results Found.</span>}
                                                        </div>
                                                    </div>
                                                    <p>Enter your location OR search using lat / lng.</p>
                                                    <div className="form-group">
                                                        <label className="form-group-label">Latitude :</label>
                                                        <input type="number" id="lat" className="form-control" value={this.state.tempLocation.lat} onChange={this.tempLocationChangeHandler} />
                                                        {(this.isInvalidLatLng() || this.state.mapError) && <span className="form-group-error">{this.isInvalidLatLng() ? "Invalid Lat/Lang" : this.state.mapError}. Device Location will not be Saved.</span>}
                                                    </div>
                                                    <div className="form-group">
                                                        <label className="form-group-label">Longitude :</label>
                                                        <input type="number" id="lng" className="form-control" value={this.state.tempLocation.lng} onChange={this.tempLocationChangeHandler} />
                                                    </div>
                                                    <div className="form-group mb-0 text-right">
                                                        <button className="btn btn-primary w-100" type="button" disabled={this.isInvalidLatLng() || this.state.mapError} onClick={this.handleLatLngSearch}>Search</button>
                                                    </div>
                                                </div>
                                                <div className="flex-60 pd-l-10">
                                                    <div className="location-map">
                                                        <GoogleMap
                                                            markers={[{ lat: this.state.selectedDeviceDetails.lat, lng: this.state.selectedDeviceDetails.lng }]}
                                                            setLocation={this.setLocation}
                                                            zoom={this.state.selectedConnector.zoomLevel}
                                                        />
                                                    </div>
                                                </div>
                                            </div>


                                        </React.Fragment>
                                    }
                                </div>
                                <div className="modal-footer">
                                    <button className="btn btn-dark" type="button" disabled={this.state.deviceDetailsModalLoader} onClick={() => this.setState({ openLocationSelector: false, mapError: null })}>Cancel</button>
                                    <button className="btn btn-success" type="button" disabled={this.state.deviceDetailsModalLoader || this.state.selectedDeviceDetails && (this.state.selectedDeviceDetails.location && !this.state.selectedDeviceDetails.lat) || this.state.selectedDeviceDetails.name && this.state.selectedDeviceDetails.name.trim().length < 4} onClick={this.saveDeviceDetailsHandler}>Save</button>
                                </div>
                            </div>

                        </div>
                    </div>
                }
                {/* end map modal */}

                {
                    this.state.confirmState &&
                    <ConfirmModel
                        status={"delete"}
                        deleteName={this.state.deviceToBeDeletedName}
                        confirmClicked={this.deleteDeviceHandler}
                        cancelClicked={this.toggleConfirmModal}
                    />
                }

                {
                    this.state.isOpen &&
                    <NotificationModal
                        type={this.state.modalType}
                        message2={this.state.message2}
                        onCloseHandler={this.onCloseHandler}
                    />
                }

            </React.Fragment >
        );
    }
}

DeviceManagement.propTypes = {
    dispatch: PropTypes.func.isRequired
};

let allSelectors = {}
Object.entries(SELECTORS).map(([key, value]) => {
    allSelectors[key] = value()
})

const mapStateToProps = createStructuredSelector(allSelectors);

function mapDispatchToProps(dispatch) {
    let allActions = { dispatch }
    Object.entries(ACTIONS).map(([key, value]) => {
        allActions[key] = (...args) => dispatch(value(...args))
    })
    return allActions;
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "deviceManagement", reducer });
const withSaga = injectSaga({ key: "deviceManagement", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(DeviceManagement);
