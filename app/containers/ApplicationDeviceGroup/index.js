/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * ApplicationDeviceGroup
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { FormattedMessage } from "react-intl";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import makeSelectApplicationDeviceGroup from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import messages from "./messages";
import ReactSelect from "react-select";
import SlidingPane from 'react-sliding-pane';

/* eslint-disable react/prefer-stateless-function */
export class ApplicationDeviceGroup extends React.Component {
  state = {
    selectedUser: '',
    count: 0,
    isFetching: true,
    payload: {
        email: '',
        firstName: '',
        lastName: '',
        mobile: '',
        roleId: null,
        assignedProjects: [],
        password: '',
        skipVerification: false
    },
    passwordMatched: true,
    roles: [],
    groups: [],
    usersList: {
        users: [],
        totalCount: 0
    },
    isFetchingUser: true,
    isChangingPassword: false,
    projects: [],
    assignedProjects: [],
    showAddOrEditDeviceGroup: false,
    accessType: "",
    tableState: {
        activePageNumber: 1,
        pageSize: 5,
        filtered: [],
    },
    searchTerm: ''
  };
  handleAddNewUserClick = () => {
    let assignedProjects = JSON.parse(JSON.stringify(this.state.projects));
    assignedProjects.map(project => {
        project.projectAccess = project.projectAndAccessPermission;
        project.repoName = project.repoName && project.repoName !== '' ? project.repoName : null;
        project.projectAndAccessPermission = null;
        project.sourceControlOriginal = project.sourceControlEnabled
        project.sourceControlEnabled = null;
    });

    this.setState({
        assignedProjects,
        showAddOrEditDeviceGroup: true,
        selectedUser: null,
        email: "",
        payload: {
            email: '',
            firstName: '',
            lastName: '',
            mobile: '',
            roleId: null,
            password: '',
            assignedProjects: [],
            skipVerification: false
        },
        errorMessage: null,
        accessType: "",
        isDefaultUser: false,
        saveButtonDisabled: false
    })
  }
  render() {
    return (
      <React.Fragment>
        <Helmet>
          <title>ApplicationDeviceGroup</title>
          <meta
            name="description"
            content="Description of ApplicationDeviceGroup"
          />
        </Helmet>
        <header className="content-header d-flex">
          <div className="flex-60">
              <h6>Application Device Group</h6>
          </div>
          <div className="flex-40 text-right">
              <div className="content-header-group">
                  <div className="search-box">
                      <span className="search-icon"><i className="far fa-search"></i></span>
                      <input type="text" className="form-control" placeholder="Search..." />
                      <button className="search-button" ><i className="far fa-times"></i></button>
                  </div>
                  <button className="btn btn-light"><i className="fad fa-list-ul"></i></button>
                  <button className="btn btn-light" data-toggle="modal" data-target="#userDetail"><i className="fad fa-table"></i></button>
                  <button className="btn btn-primary" onClick={() => { this.handleAddNewUserClick() }}><i className="far fa-plus"></i></button>
              </div>
          </div>
        </header>
        <div className="content-body">
          <div className="list-view">
            <div className="list-view-header d-flex">
              <p className="flex-25">Group Name</p>
              <p className="flex-35">Description</p>
              <p className="flex-25">Devices</p>
              <p className="flex-15">Actions</p>
            </div>
            <div className="list-view-body" id="dataItemDiv">
              <div className="list-view-item">
                <ul className="list-style-none d-flex" id="rowContainer">
                  <li className="flex-25">
                    <h6 className="text-theme"><strong>Device Details</strong></h6>
                  </li>
                  <li className="flex-35">
                    <h6 className=""></h6>
                  </li>
                  <li className="flex-25">
                    <h6><span className="count-box">100</span></h6>
                  </li>
                  <li className="flex-15">
                    <div className="button-group">
                      <button className="btn-transparent btn-transparent-cyan"><i className="far fa-pencil"></i></button>
                      <button className="btn-transparent btn-transparent-red"><i className="far fa-trash-alt"></i></button>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <SlidingPane
              className=''
              overlayClassName='sliding-form'
            closeIcon={<div></div>}
            isOpen={this.state.showAddOrEditDeviceGroup || false}
            from='right'
            width='500px'
          >
          <div className="modal-content">
            <div className="modal-header">
              <h6 className="modal-title">Add New Device Group
                  <button className="btn btn-light close" onClick={() => this.setState({ showAddOrEditDeviceGroup: false, passwordMatched: true, errorMessage: '', email: "" })}>
                      <i className="far fa-angle-right"></i>
                  </button>
              </h6>
            </div>
            <div className="modal-body">
                <div className="form-group">
                    <label className="form-group-label">Group Name :</label>
                    <input type="text" name="groupName" id="name" required value={this.state.payload.name} pattern="^[a-zA-Z]([\w\$-]*[a-zA-Z0-9])?$"
                        onChange={() => this.onChangeHandler(event)} className="form-control"
                    />
                </div>
                <div className="form-group">
                    <label className="form-group-label">Description :</label>
                    <textarea rows="3" type="text" name="desc" id="description" className="form-control" value={this.state.payload.description}
                        onChange={() => this.onChangeHandler(event)}
                    />
                </div>
                <div className="form-group">
                  <div className="navigation-box">
                      <h6>Devices</h6>
                      <ul className="nav-list list-style-none">
                        <li className="d-flex multi-select">
                          <div className="flex-50 d-flex  pd-r-10">
                              <div className="flex-80">
                                <p className="border-none">XYZ</p>
                              </div>
                              <div className="flex-20 ">
                                <label className="check-box">
                                    <input type="checkbox" ></input>
                                    <span className="check-mark"></span>
                                </label>
                              </div>
                          </div>
                          <div className="flex-50 d-flex  pd-l-10">
                              <div className="flex-80">
                                <p className="border-none">XYZ</p>
                              </div>
                              <div className="flex-20">
                                <label className="check-box">
                                    <input type="checkbox" ></input>
                                    <span className="check-mark"></span>
                                </label>
                              </div>
                          </div>
                        </li>
                      </ul>
                  </div>
                </div>
            </div>
            <div className="modal-footer justify-content-start">
              <button className="btn btn-primary" disabled={this.state.roles.length === 0 || this.state.saveButtonDisabled} className={"btn btn-success"} > {this.state.selectedUser ? "Update" : "Save"}</button>
              <button type="button" className="btn btn-dark" onClick={() => this.setState({ showAddOrEditDeviceGroup: false, errorMessage: '', email: "" })}>Cancel</button>
            </div>
          </div>
        </SlidingPane>
      </React.Fragment>
    );
  }
}

ApplicationDeviceGroup.propTypes = {
  dispatch: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
  applicationdevicegroup: makeSelectApplicationDeviceGroup()
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

const withReducer = injectReducer({ key: "applicationDeviceGroup", reducer });
const withSaga = injectSaga({ key: "applicationDeviceGroup", saga });

export default compose(
  withReducer,
  withSaga,
  withConnect
)(ApplicationDeviceGroup);
