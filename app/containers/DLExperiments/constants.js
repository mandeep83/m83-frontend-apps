/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/*
 *
 * DLExperiments constants
 *
 */

export const RESET_TO_INITIAL_STATE = "app/DLExperiments/RESET_TO_INITIAL_STATE";

export const GET_ALL_DL_SENTIMENTS = {
    action: "app/DLExperiments/GET_ALL_DL_SENTIMENTS",
    success: "app/DLExperiments/GET_ALL_DL_SENTIMENTS_SUCCESS",
    failure: "app/DLExperiments/GET_ALL_DL_SENTIMENTS_FAILURE",
    urlKey: "getAllDLSentiments",
    successKey: "getAllDLSentimentsSuccess",
    failureKey: "getAllDLSentimentsFailure",
    actionName: "getAllDLSentiments",
}

export const DELETE_DL_SENTIMENT = {
    action: "app/DLExperiments/DELETE_DL_SENTIMENT",
    success: "app/DLExperiments/DELETE_DL_SENTIMENT_SUCCESS",
    failure: "app/DLExperiments/DELETE_DL_SENTIMENT_FAILURE",
    urlKey: "deleteDLSentiment",
    successKey: "deleteDLSentimentSuccess",
    failureKey: "deleteDLSentimentFailure",
    actionName: "deleteDLSentiment",
    actionArguments: ["id"]
}