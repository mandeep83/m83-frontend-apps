/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * DLExperiments
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import QuotaHeader from "../../components/QuotaHeader/Loadable";
import Skeleton from "react-loading-skeleton";
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import { allActions as ACTIONS } from './actions';
import { allSelectors as SELECTORS } from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import ListingTable from "../../components/ListingTable/Loadable";
import ConfirmModel from "../../components/ConfirmModel";

/* eslint-disable react/prefer-stateless-function */
export class DLExperiments extends React.Component {

    state = {
        isFetching: true,
    }

    qoutaCallback = () => {

    }

    addOrEditDeepLearning = () => {
        this.props.history.push("/addOrEditDlExperiments")
    }

    refreshComponent = () => {
        this.setState({
            isFetching: true,
        }, () => this.props.getAllDLSentiments())
    }

    componentDidMount() {
        this.props.getAllDLSentiments();
    }

    componentDidUpdate(prevProps, prevState) {
        let newProp = Object.keys(SELECTORS).find(prop => this.props[prop])
        if (newProp) {
            if (newProp.includes("Failure")) {
                this.props.showNotification("error", this.props[newProp].error)
            }
            this.props.resetToInitialState(newProp)
        }
    }

    static getDerivedStateFromProps(nextProps, state) {
        let newProp = Object.keys(SELECTORS).find(prop => nextProps[prop])
        if (newProp) {
            let propData = nextProps[newProp].error ? nextProps[newProp].error : nextProps[newProp].response;

            if (nextProps.getAllDLSentimentsSuccess) {
                return {
                    allDLExperiments: propData.data,
                    isFetching: false
                }
            }

            if (nextProps.getAllDLSentimentsFailure) {
                return {
                    isFetching: false,
                }
            }

            if (nextProps.deleteDLSentimentSuccess) {
                nextProps.getAllDLSentiments();
            }

            if (nextProps.deleteDLSentimenFailure) {
                return {
                    isFetching: false
                }
            }
        }
        return null;
    }

    getColumns = () => {
        let column = [
            {
                Header: "Name", width: 30,
                cell: (row) => (
                    <React.Fragment>
                        <h6 className="text-theme fw-600">{row.name}</h6>
                        <p>{`${row.description || "-"}`}</p>
                    </React.Fragment>
                ),
                accessor: "name",
                sortable: true,
                filterable: true
            },
            {
                Header: "Data Source", width: 15,
                cell: (row) => (
                    <React.Fragment>
                        <div className="list-view-icon">
                            <img src="https://content.iot83.com/m83/dataConnectors/amazons3.png" />
                        </div>
                        <div className="list-view-icon-box">
                            <h6>{`${row.dataSource.connectorCategory || "-"}`}</h6>
                        </div>
                    </React.Fragment>
                )
            },
            {
                Header: "Type", width: 15,
                cell: (row) => (
                    <h6>{row.type}</h6>
                )
            },
            { Header: "Created", width: 15, accessor: "createdAt", sortable: true, filterable: true },
            { Header: "Updated", width: 15, accessor: "updatedAt", sortable: true, filterable: true },
            {
                Header: "Actions", width: 10, cell: (row) => (
                    <div className="button-group" >
                        <button className="btn-transparent btn-transparent-cyan" data-tooltip data-tooltip-text="View" data-tooltip-place="bottom" data-toggle="modal" data-target="#viewDeepLearningModal"
                        >
                            <i className="far fa-info"></i>
                        </button>

                        <button className="btn-transparent btn-transparent-blue" data-tooltip data-tooltip-text="Edit" data-tooltip-place="bottom" onClick={() => this.props.history.push('/addOrEditDlExperiments/' + row.id)}>
                            <i className="far fa-pencil"></i>
                        </button>

                        <button className="btn-transparent btn-transparent-red" data-tooltip data-tooltip-text="Delete" data-tooltip-place="bottom" onClick={(e) => { this.deleteDLExperimentHandler(e, row) }}>
                            <i className="far fa-trash-alt"></i>
                        </button>
                    </div>
                )
            },
        ];
        return column
    }

    deleteDLExperimentHandler = (e, { id, name }) => {
        this.setState({
            confirmState: true,
            deletedDLSentimentId: id,
            deletedDLSentimentName: name
        })
    }

    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>DLExperiments</title>
                    <meta name="description" content="Description of DLExperiments"/>
                </Helmet>

                <QuotaHeader
                    showQuota
                    addButtonText="Add DL Sentiment"
                    componentName="Deep Learning"
                    productName="experiments"
                    onAddClick={this.addOrEditDeepLearning}
                    // searchBoxDetails={{
                    //     value: this.state.searchTerm,
                    //     onChangeHandler: this.onSearchHandler,
                    //     isDisabled: !this.state.mlExperimentList.length
                    // }}
                    refreshHandler={this.refreshComponent}
                    refreshDisabled={this.state.isFetching}
                    addDisabled={this.state.isFetching}
                    callBack={this.qoutaCallback}
                    {...this.props}
                />

                <div className="content-body">
                    {this.state.isFetching ?
                        <Skeleton count={9} className="skeleton-list-loader" /> :
                        <ListingTable
                            columns={this.getColumns()}
                            data={this.state.allDLExperiments}
                        />
                    }
                </div>

                {this.state.confirmState &&
                    <ConfirmModel
                        status={"delete"}
                        deleteName={this.state.deletedDLSentimentName}
                        confirmClicked={() => {
                            this.props.deleteDLSentiment(this.state.deletedDLSentimentId);
                            this.setState({
                                isFetching: true,
                                confirmState: false,
                            })
                        }}
                        cancelClicked={() => {
                            this.setState({
                                confirmState: false,
                            })
                        }}
                    />
                }

                {/* view experiment modal */}
                <div className="modal fade animated slideInDown" id="viewDeepLearningModal">

                    <div className="modal-dialog modal-xl modal-dialog-centered">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h6 className="modal-title">Audio Sentiment - <span>Pipeline View & Response</span>
                                    <button type="button" className="close" data-tooltip data-tooltip-text="Close" data-dismiss="modal">
                                        <i className="far fa-times"></i>
                                    </button>
                                </h6>
                            </div>

                            <div className="modal-body">
                                {/*===== Pipeline View ====*/}
                                <div className="d-flex">
                                    <div className="flex-50 pd-r-10">
                                        <div className="card">
                                            <div className="card-header">
                                                <h6>Pipeline View</h6>
                                            </div>

                                            <div className="card-body">
                                                <ul className="pipeline-view d-flex flex-column list-style-none">
                                                    <li>
                                                        <div className="pipeline-text">
                                                            <p>File Conversion and File Uploadation to S3</p>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <div className="pipeline-text">
                                                            <p>Recognization and Uploadation of CSV file to S3</p>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <div className="pipeline-text">
                                                            <p>Sentiment Analysis</p>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <div className="pipeline-text">
                                                            <p>Key Words / Faulty Words Detection</p>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <div className="pipeline-text">
                                                            <p>Word Cloud</p>
                                                        </div>
                                                    </li>

                                                    <p className="text-center">
                                                        <button type="button" className="btn btn-light">Stop</button>
                                                    </p>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="flex-50 pd-l-10">
                                        <div className="card">
                                            <div className="card-header">
                                                <h6>Response / Result <span className="text-theme">(Step 3 Result)</span></h6>
                                            </div>

                                            <div className="card-body">
                                                <div className="content-table">
                                                    <table className="table table-bordered m-0">
                                                        <thead>
                                                            <tr>
                                                                <th>Serial No.</th>
                                                                <th>From</th>
                                                                <th>To</th>
                                                                <th>Word</th>
                                                                <th>Count</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>1</td>
                                                                <td>1.5</td>
                                                                <td>2.5</td>
                                                                <td>alpha</td>
                                                                <td>1</td>
                                                            </tr>

                                                            <tr>
                                                                <td>2</td>
                                                                <td>1.5</td>
                                                                <td>2.5</td>
                                                                <td>beta</td>
                                                                <td>1</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {/*===== End Pipeline View ====*/}
                            </div>
                        </div>
                    </div>

                </div>
                {/* end view experiment modal */}

            </React.Fragment>
        );
    }
}

DLExperiments.propTypes = {
    dispatch: PropTypes.func.isRequired
};

let allSelectors = {};

Object.entries(SELECTORS).map(([key, value]) => {
    allSelectors[key] = value()
})

const mapStateToProps = createStructuredSelector(allSelectors);

function mapDispatchToProps(dispatch) {
    let allActions = { dispatch }
    Object.entries(ACTIONS).map(([key, value]) => {
        allActions[key] = (...args) => dispatch(value(...args))
    })
    return allActions;
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "deepLearningListingPage", reducer });
const withSaga = injectSaga({ key: "deepLearningListingPage", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(DLExperiments);
