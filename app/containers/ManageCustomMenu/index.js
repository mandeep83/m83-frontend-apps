/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * ManageCustomMenu
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import reducer from "./reducer";
import saga from "./saga";
import { allSelectors as SELECTORS } from "./selectors";
import { allActions as ACTIONS } from './actions';
import ListingTable from "../../components/ListingTable/Loadable";
import AddNewButton from "../../components/AddNewButton/Loadable";
import NoDataFoundMessage from "../../components/NoDataFoundMessage/Loadable"
import { getNotificationObj } from "../../commonUtils";
import Loader from '../../components/Loader/Loadable';
import ConfirmModel from '../../components/ConfirmModel/Loadable'
import NotificationModal from '../../components/NotificationModal/Loadable';
import NumberFormat from 'react-number-format';
import Skeleton from "react-loading-skeleton";

/* eslint-disable react/prefer-stateless-function */
export class ManageCustomMenu extends React.Component {
    state = {
        isFetching: true,
        toggleView: true,
        menuList: [],
        modalIsLoading: true,
        selectedCustomMenu: {
            name: '',
            customMenus: []
        },
    }

    componentDidMount() {
        this.props.getAllCustomMenus();
    }


    static getDerivedStateFromProps(nextProps, state) {
        if (nextProps.getAllCustomMenusSuccess) {
            let menuList = nextProps.getAllCustomMenusSuccess.response;
            menuList.map(list => {
                list.roleAssigned = list.roles.length ? list.roles[0] : "-"
            })
            return {
                isFetching: false,
                menuList
            }
        }
        if (nextProps.deleteCustomMenuSuccess) {
            let menuList = state.menuList.filter(el => el.id !== nextProps.deleteCustomMenuSuccess.response.id)
            nextProps.fetchSideNav()

            return {
                isLoadingTable: false,
                menuList,
                isOpen: true,
                message2: nextProps.deleteCustomMenuSuccess.response.message,
                modalType: "success",
            }
        }
        if (nextProps.deleteCustomMenuFailure) {
            return {
                isLoadingTable: false,
                isOpen: true,
                message2: nextProps.deleteCustomMenuFailure.error,
                modalType: "error",
            }
        }
        if (nextProps.getCustomMenuByIdSuccess) {
            let selectedCustomMenu = nextProps.getCustomMenuByIdSuccess.response;
            return {
                selectedCustomMenu,
                modalIsLoading: false,
            }
        }

        if (Object.keys(SELECTORS).filter(prop => prop.toLowerCase().includes("success")).some(prop => nextProps[prop])) {
            let propName = Object.keys(SELECTORS).find(props => nextProps[props]);
            return { ...getNotificationObj(nextProps[propName].response.message, "success") }
        }
        if (Object.keys(SELECTORS).filter(prop => prop.toLowerCase().includes("failure")).some(prop => nextProps[prop])) {
            let propName = Object.keys(SELECTORS).find(props => nextProps[props]);
            return { ...getNotificationObj(nextProps[propName].error, "error"), isFetching: false }
        }
        return null
    }

    componentDidUpdate() {
        if (Object.keys(SELECTORS).some(prop => this.props[prop])) {
            this.props.resetToInitialState()
        }
    }

    deleteCustomMenu = (menu) => {
        this.setState({
            confirmState: true,
            deleteCustomMenuDetails: menu
        })
    }


    cancelClicked = () => {
        this.setState({
            deleteCustomMenuDetails: {},
            confirmState: false
        })
    }

    onCloseHandler = () => {
        this.setState({
            isOpen: false,
            message2: '',
            modalType: "",
        })
    }

    getColumns = () => {
        return [
            {
                Header: "Name",
                accessor: "name",
                width: 20,
                cell: (row) =>
                    <React.Fragment>
                        <div className="list-view-icon"><i className="fad fa-layer-group text-dark-theme"></i></div>
                        <div className="list-view-icon-box">
                            <h6 className="text-theme fw-600">{row.name}</h6>
                            <p>{`${row.description || "-"}`}</p>
                        </div>
                    </React.Fragment>,
                filterable: true,
                sortable: true
            },
            {
                Header: "Assigned Roles",
                accessor: "roleAssigned",
                width: 15,
                cell: (row) => {
                    return (
                        <div className="list-view-pill-wrapper dropdown">
                            {row.roles.length > 0 ?
                                <span className="badge badge-pill alert-success list-view-pill mr-1 w-100">{row.roles[0]}</span> : '-'
                            }
                            {row.roles.length > 1 && <span className="list-view-pill-count" data-toggle="dropdown">+{row.roles.length - 1}</span>}
                            <div className="dropdown-menu">
                                {row.roles.length > 0 ?
                                    row.roles.map((role, index) =>
                                        <span className="d-block"><span className="badge badge-pill alert-success list-view-pill" key={index}>{role}</span></span>
                                    ) :
                                    '-'
                                }
                            </div>
                        </div>
                    )
                },
                filterable: true,
                sortable: true
            },
            {
                Header: "Version",
                width: 10,
                accessor: "version",
                filterable: true,
                sortable: true,
                cell: (row) =>
                    <span className="badge badge-pill alert-warning list-view-pill">{row.version ? row.version : "-"}</span>
            },
            {
                Header: "Menus",
                width: 10,
                cell: (row) =>
                    <span className="alert alert-primary list-view-badge">{row.menuCount ? row.menuCount : 0}</span>
            },
            { Header: "Created", width: 15, accessor: "createdAt", sortable: true, filterable: true },
            { Header: "Updated", width: 15, accessor: "updatedAt", sortable: true, filterable: true },
            {
                Header: "Actions",
                width: 15,
                cell: (row) => (
                    <div className="button-group">
                        <button className="btn-transparent btn-transparent-cyan" data-tooltip data-tooltip-text="View" data-tooltip-place="bottom" onClick={() => {
                            this.props.getCustomMenuById(row.id);
                            this.setState({
                                modalIsLoading: true, selectedCustomMenu: {
                                    name: '',
                                    customMenus: []
                                }
                            })
                        }} data-toggle="modal" data-target="#menuModal">
                            <i className="far fa-info"></i>
                        </button>
                        <button className="btn-transparent btn-transparent-blue" onClick={() => this.props.history.push(`addOrEditApplication/${row.id}`)} data-tooltip data-tooltip-text="Edit" data-tooltip-place="bottom">
                            <i className="far fa-pencil"></i>
                        </button>
                        <button className="btn-transparent btn-transparent-red" disabled={row.roles.length > 0} onClick={() => this.deleteCustomMenu(row)} data-tooltip data-tooltip-text="Delete" data-tooltip-place="bottom">
                            <i className="far fa-trash-alt"></i>
                        </button>
                    </div>
                )
            },
        ]
    }

    goToCreateCustomMenuPage = () => {
        this.props.history.push('/addOrEditApplication')
    }

    render() {
        let filteredList = (this.state.searchTerm ? this.state.menuList.filter(el => (el.name.toLowerCase()).includes(this.state.searchTerm.toLowerCase())) : this.state.menuList);
        return (
            <React.Fragment>
                <Helmet>
                    <title>Applications</title>
                    <meta name="description" content="Description of Applications" />
                </Helmet>

                <header className="content-header d-flex">
                    <div className="flex-60">
                        <h6>Applications</h6>
                    </div>
                    <div className="flex-40 text-right">
                        <div className="content-header-group">
                            {/*<div className="search-box">
                                <span className="search-icon"><i className="far fa-search"></i></span>
                                <input type="text" className="form-control" disabled={this.state.isFetching} value={this.state.searchTerm} onChange={({ currentTarget }) => {
                                    this.setState({
                                        searchTerm: currentTarget.value
                                    })
                                }} placeholder="Search..." />
                                <button className="search-button" onClick={() => { this.setState({ searchTerm: '' }) }}><i className="far fa-times"></i></button>
                            </div>*/}
                            {/*<button className="btn btn-light"><i className="fad fa-list-ul"></i></button>*/}
                            {/*<button className="btn btn-light" data-toggle="modal" data-target="#userDetail"><i className="fad fa-table"></i></button>*/}
                            <button className={this.state.toggleView == true ? "btn btn-light active" : "btn btn-light"} disabled={this.state.isFetching} data-tooltip="true" data-tooltip-text="List View" data-tooltip-place="bottom" onClick={() => this.setState({ toggleView: true })}><i className="fad fa-list-ul"></i></button>
                            <button className={this.state.toggleView == false ? "btn btn-light active" : "btn btn-light"} disabled={this.state.isFetching} data-tooltip="true" data-tooltip-text="Card View" data-tooltip-place="bottom" onClick={() => this.setState({ toggleView: false })}><i className="fad fa-table"></i></button>
                            <button className="btn btn-light" disabled={this.state.isFetching} data-tooltip data-tooltip-text="Refresh" onClick={() => { this.props.getAllCustomMenus(); this.setState({ isFetching: true }) }} data-tooltip-place="bottom"><i className="far fa-sync-alt"></i></button>
                            <button className="btn btn-primary" disabled={this.state.isFetching} data-tooltip data-tooltip-text="Add Application" data-tooltip-place="bottom" onClick={() => { this.props.history.push('/addOrEditApplication') }}><i className="far fa-plus"></i></button>
                        </div>
                    </div>
                </header>

                <div className="content-body">
                    {this.state.isFetching ?
                        <Skeleton count={9} className="skeleton-list-loader" /> :
                        <React.Fragment>
                            {this.state.menuList.length > 0 ?
                                filteredList.length > 0 ?
                                    this.state.toggleView ?
                                        <ListingTable
                                            columns={this.getColumns()}
                                            data={filteredList}
                                            isLoading={this.state.isLoadingTable}
                                        /> :
                                        <ul className="card-view-list">
                                            {filteredList.map((item, index) =>
                                                <li key={index}>
                                                    <div className="card-view-box">
                                                        <div className="card-view-header">
                                                            <span className="alert alert-warning mr-r-5">{item.version ? item.version : "-"}</span>
                                                            <span className="alert alert-primary">Menu - {item.menuCount ? item.menuCount : "-"}</span>
                                                            <div className="dropdown">
                                                                <button className="btn-transparent btn-transparent-gray" type="button" data-toggle="dropdown">
                                                                    <i className="fas fa-ellipsis-v"></i>
                                                                </button>
                                                                <div className="dropdown-menu">
                                                                    <button className="dropdown-item" onClick={() => {
                                                                        this.props.getCustomMenuById(item.id);
                                                                        this.setState({
                                                                            modalIsLoading: true, selectedCustomMenu: {
                                                                                name: '',
                                                                                customMenus: []
                                                                            }
                                                                        })
                                                                    }} data-toggle="modal" data-target="#menuModal">
                                                                        <i className="far fa-info"></i>View
                                                                    </button>
                                                                    <button className="dropdown-item" onClick={() => this.props.history.push(`addOrEditApplication/${item.id}`)}>
                                                                        <i className="far fa-pencil"></i>Edit
                                                                    </button>
                                                                    <button className="dropdown-item" disabled={item.roles.length > 0} onClick={() => this.deleteCustomMenu(item)}>
                                                                        <i className="far fa-trash-alt"></i>Delete
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="card-view-body">
                                                            <div className="card-view-icon">
                                                                <i className="fad fa-layer-group text-dark-theme"></i>
                                                            </div>
                                                            <h6><i className="far fa-address-book"></i>{item.name ? item.name : "-"}</h6>
                                                            <p><i className="far fa-file-alt"></i>{item.description ? item.description : "-"}</p>
                                                            <div className="card-view-pill-wrapper dropdown">
                                                                {item.roles.length > 1 ?
                                                                    <React.Fragment>
                                                                        <span className="badge badge-pill alert-success mr-r-5">{item.roles[0]} </span>
                                                                        <span className="badge badge-pill alert-success">{item.roles[1]} </span>
                                                                    </React.Fragment>
                                                                    : '-'
                                                                }
                                                                {item.roles.length > 2 && <span className="card-view-pill-count" data-toggle="dropdown">+{item.roles.length - 2}</span>}
                                                                <div className="dropdown-menu">
                                                                    {item.roles.length > 0 ?
                                                                        item.roles.map((role, index) =>
                                                                            <span className="d-block"><span className="badge badge-pill badge-light card-view-pill" key={index}>{role}</span></span>
                                                                        ) :
                                                                        '-'
                                                                    }
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="card-view-footer d-flex">
                                                            <div className="flex-50 p-3">
                                                                <h6>Created</h6>
                                                                <p><strong>By - </strong>{item.createdBy}<small></small></p>
                                                                <p><strong>At - </strong>{new Date(item.createdAt).toLocaleString('en-US', { createdAt: localStorage.createdAt })}</p>
                                                            </div>
                                                            <div className="flex-50 p-3">
                                                                <h6>Updated</h6>
                                                                <p><strong>By - </strong>{item.updatedBy}<small></small></p>
                                                                <p><strong>At - </strong>{new Date(item.updatedAt).toLocaleString('en-US', { createdAt: localStorage.createdAt })}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            )}
                                        </ul>
                                    :
                                    <NoDataFoundMessage />
                                :
                                <AddNewButton
                                    text1="No application(s) available"
                                    text2="You haven't created any application(s) yet. Please create an application first."
                                    createItemOnAddButtonClick={this.goToCreateCustomMenuPage}
                                    imageIcon="addCustomMenu.png"
                                />
                            }
                        </React.Fragment>
                    }
                </div>

                {/* menu modal */}
                <div className="modal fade animated slideInDown" id="menuModal">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h6 className="modal-title">Menu for - <span>{this.state.selectedCustomMenu.name}</span>
                                    <button type="button" className="close" data-dismiss="modal" aria-label="Close" data-tooltip data-tooltip-text="Close" data-tooltip-place="bottom" data-dismiss="modal">
                                        <i className="far fa-times"></i>
                                    </button>
                                </h6>
                            </div>

                            <div className="modal-body">
                                {this.state.modalIsLoading ?
                                    <div className="modal-loader" style={{ height: 350 }}>
                                        <i className="fad fa-sync-alt fa-spin"></i>
                                    </div>
                                    :
                                    <ul className="custom-menu">
                                        {this.state.selectedCustomMenu.customMenus.map((menu, index) => {
                                            return (
                                                <li key={index}>
                                                    <p data-toggle="collapse" data-target={`#subMenu${index}`}>
                                                        <i className={`far fa-${menu.fontAwesomeIconClass ? menu.fontAwesomeIconClass : "window"} custom-menu-icon`}></i>{menu.menuName}
                                                        {menu.children.length ? <span><i className="fas fa-caret-up"></i></span> : null}
                                                    </p>
                                                    {menu.children.length ?
                                                        <div className="collapse show" id={`subMenu${index}`}>
                                                            <ul className="custom-sub-menu">
                                                                {menu.children.map((child, childIndex) => {
                                                                    return (<li key={childIndex}><p><i className={`far fa-${child.fontAwesomeIconClass ? child.fontAwesomeIconClass : "window"} custom-menu-icon`}></i>{child.menuName}</p></li>)
                                                                })}
                                                            </ul>
                                                        </div>
                                                        : null
                                                    }
                                                </li>
                                            )
                                        })}
                                    </ul>
                                }
                            </div>

                            <div className="modal-footer">
                                <button type="button" className="btn btn-success" data-dismiss="modal">OK</button>
                            </div>

                        </div>
                    </div>
                </div>
                {/* end menu modal */}

                {this.state.isOpen &&
                    <NotificationModal
                        type={this.state.modalType}
                        message2={this.state.message2}
                        onCloseHandler={this.onCloseHandler}
                    />
                }

                {this.state.confirmState &&
                    <ConfirmModel
                        status={"delete"}
                        deleteName={this.state.deleteCustomMenuDetails.name}
                        confirmClicked={() => {
                            this.setState({
                                isLoadingTable: true,
                                confirmState: false,
                            }, () => this.props.deleteCustomMenu(this.state.deleteCustomMenuDetails.id))
                        }}
                        cancelClicked={() => {
                            this.cancelClicked()
                        }}
                    />
                }

            </React.Fragment>
        );
    }
}

ManageCustomMenu.propTypes = {
    dispatch: PropTypes.func.isRequired
};

let allSelectors = {}
Object.entries(SELECTORS).map(([key, value]) => {
    allSelectors[key] = value()
})

const mapStateToProps = createStructuredSelector(allSelectors);

function mapDispatchToProps(dispatch) {
    let allActions = { dispatch }
    Object.entries(ACTIONS).map(([key, value]) => {
        allActions[key] = (...args) => dispatch(value(...args))
    })
    return allActions;
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "manageCustomMenu", reducer });
const withSaga = injectSaga({ key: "manageCustomMenu", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(ManageCustomMenu);
