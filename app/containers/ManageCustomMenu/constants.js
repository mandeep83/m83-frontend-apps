/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/*
 *
 * ManageCustomMenu constants
 *
 */

export const RESET_TO_INITIAL_STATE = "app/ManageCustomMenu/RESET_TO_INITIAL_STATE";

export const GET_ALL_CUSTOM_MENUS = {
	action: 'app/ManageCustomMenu/GET_ALL_CUSTOM_MENUS',
	success: "app/ManageCustomMenu/GET_ALL_CUSTOM_MENUS_SUCCESS",
	failure: "app/ManageCustomMenu/GET_ALL_CUSTOM_MENUS_FAILURE",
	urlKey: "getAllCustomMenus",
	successKey: "getAllCustomMenusSuccess",
	failureKey: "getAllCustomMenusFailure",
	actionName: "getAllCustomMenus",
	actionArguments: []
}


export const DELETE_CUSTOM_MENU = {
	action: 'app/ManageCustomMenu/DELETE_CUSTOM_MENU',
	success: "app/ManageCustomMenu/DELETE_CUSTOM_MENU_SUCCESS",
	failure: "app/ManageCustomMenu/DELETE_CUSTOM_MENU_FAILURE",
	urlKey: "deleteCustomMenu",
	successKey: "deleteCustomMenuSuccess",
	failureKey: "deleteCustomMenuFailure",
	actionName: "deleteCustomMenu",
	actionArguments: ["id"]
}

export const GET_CUSTOM_MENU_BY_ID = {
	action: 'app/ManageCustomMenu/GET_CUSTOM_MENU_BY_ID',
	success: "app/ManageCustomMenu/GET_CUSTOM_MENU_BY_ID_SUCCESS",
	failure: "app/ManageCustomMenu/GET_CUSTOM_MENU_BY_ID_FAILURE",
	urlKey: "getCustomMenuById",
	successKey: "getCustomMenuByIdSuccess",
	failureKey: "getCustomMenuByIdFailure",
	actionName: "getCustomMenuById",
	actionArguments: ["id"]
}