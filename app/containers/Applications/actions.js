/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import * as CONSTANTS from "./constants";

export function defaultAction() {
  return {
    type: CONSTANTS.DEFAULT_ACTION
  };
}

export function getApplicationsList() {
  return {
    type: CONSTANTS.GET_APPLICATIONS
  };
}

export function deleteApplicationById(id){
  return {
    type:CONSTANTS.DELETE_APPLICATION,
    id
  }
}

export function exportApplication(id){
  return {
    type:CONSTANTS.EXPORT_APPLICATION,
    id
  }
}

export function importApplication(formData){
  return {
    type:CONSTANTS.IMPORT_APPLICATION,
    payload :formData
  }
}
export function changeApplicationPublishStatus(id, isPublished){
  return {
    type:CONSTANTS.CHANGE_APPLICATION_PUBLISH_STATUS,
    id,
    isPublished,
  }
}

export function resetToInitialState() {
  return {
    type: CONSTANTS.RESET_TO_INITIAL_STATE,
  };
}

export function cloneApp(id){
  return {
    type: CONSTANTS.CLONE_APP,
    id
  }
}

export function getConnectorsListByCategory(){
  return {
    type: CONSTANTS.MQTT_CONNECTOR,
  }
}

export function shareApplication(resourceType, id, payload) {
  return {
    type: CONSTANTS.SHARE_APPLICATION,
    resourceType,
    id,
    payload,
  };
}

export function getDeveloperQuota(products) {
  return {
    type: CONSTANTS.GET_DEVELOPER_QUOTA,
    products,
  };
}

