/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { createSelector } from "reselect";
import { initialState } from "./reducer";
import {selectSimulationStudioDomain} from "../SimulationStudio/selectors";


const selectApplicationsDomain = state =>
  state.get("applications", initialState);

export const isFetchingState = state => state.get('loader');

export const getIsFetching = () =>
  createSelector(isFetchingState, subState => subState.get('isFetching'));

export const getApplicationsSuccess = () => createSelector(selectApplicationsDomain, subState => subState.applicationListSuccess);
export const getApplicationsError = () => createSelector(selectApplicationsDomain, subState => subState.applicationListError);

export const deleteApplicationSuccess = () => createSelector(selectApplicationsDomain, subState => subState.deleteApplicationSuccess);
export const deleteApplicationSuccessRandom = () => createSelector(selectApplicationsDomain, subState => subState.deleteApplicationSuccessRandom);

export const deleteApplicationError = () => createSelector(selectApplicationsDomain, subState => subState.deleteApplicationError);


export const importApplicationSuccess = () => createSelector(selectApplicationsDomain, subState => subState.importApplicationSuccess);
export const importApplicationError = () => createSelector(selectApplicationsDomain, subState => subState.importApplicationError);


export const exportApplicationSuccess = () => createSelector(selectApplicationsDomain, subState => subState.exportApplicationSuccess);
export const exportApplicationError = () => createSelector(selectApplicationsDomain, subState => subState.exportApplicationError);


export const changeApplicationPublishStatusSuccess = () => createSelector(selectApplicationsDomain, subState => subState.changeApplicationPublishStatusSuccess);
export const changeApplicationPublishStatusError = () => createSelector(selectApplicationsDomain, subState => subState.changeApplicationPublishStatusError);


export const cloneAppSuccess = () => createSelector(selectApplicationsDomain, subState => subState.cloneAppSuccess);
export const cloneAppFailure = () => createSelector(selectApplicationsDomain, subState => subState.cloneAppFailure);

export const shareAppSuccess = () => createSelector(selectApplicationsDomain, subState => subState.shareAppSuccess);
export const shareAppFailure = () => createSelector(selectApplicationsDomain, subState => subState.shareAppError);

export const getConnectorsListSuccess = () => createSelector(selectApplicationsDomain, subState => subState.getConnectorsListSuccess);
export const getConnectorsListError = () => createSelector(selectApplicationsDomain, subState => subState.getConnectorsListFailure);

export const developerQuotaSuccess = () => createSelector(selectApplicationsDomain, subState => subState.developerQuotaSuccess);
export const developerQuotaFailure = () => createSelector(selectApplicationsDomain, subState => subState.developerQuotaFailure);

