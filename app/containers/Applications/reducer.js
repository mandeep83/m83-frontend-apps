/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { fromJS } from "immutable";
import * as CONSTANTS from "./constants";

export const initialState = fromJS({});

function manageApplicationsStudioReducer(state = initialState, action) {
  switch (action.type) {
    case CONSTANTS.DEFAULT_ACTION:
      return state;

    case CONSTANTS.RESET_TO_INITIAL_STATE:
      return initialState;

    case CONSTANTS.GET_APPLICATIONS_SUCCESS:
      return Object.assign({}, state, {
        applicationListSuccess: action.response
      });
    case CONSTANTS.GET_APPLICATIONS_FAILURE:
      return Object.assign({}, state, {
        applicationListError: action.error
      })
    case CONSTANTS.DELETE_APPLICATION_SUCCESS:
      let temp = Object.assign({}, state, {
        deleteApplicationSuccessRandom: Math.random(),
        deleteApplicationSuccess: "Application Deleted Successfully",
      })
      temp.applicationListSuccess = temp.applicationListSuccess.filter(item => {
        if (item.id != action.response)
          return item;
      })
      return temp

    case CONSTANTS.DELETE_APPLICATION_FAILURE:
      return Object.assign({}, state, {
        deleteApplicationError: action.error,
      })

    case CONSTANTS.EXPORT_APPLICATION_SUCCESS:
      return Object.assign({}, state, {
        exportApplicationSuccess: action.response
      });

    case CONSTANTS.EXPORT_APPLICATION_FAILURE:
      return Object.assign({}, state, {
        exportApplicationError: action.error
      });

    case CONSTANTS.IMPORT_APPLICATION_SUCCESS:
      return Object.assign({}, state, {
        importApplicationSuccess: action.response
      });

    case CONSTANTS.IMPORT_APPLICATION_FAILURE:
      return Object.assign({}, state, {
        importApplicationError: action.error
      })

    case CONSTANTS.CHANGE_APPLICATION_PUBLISH_STATUS_SUCCESS:
      return Object.assign({}, state, {
        changeApplicationPublishStatusSuccess: { message: action.response, timestamp: Date.now() }
      });

    case CONSTANTS.CHANGE_APPLICATION_PUBLISH_STATUS_FAILURE:
      return Object.assign({}, state, {
        changeApplicationPublishStatusError: { message: action.error, timestamp: Date.now() }
      })
    case CONSTANTS.CLONE_APP:
      return Object.assign({}, state, {
        cloneAppSuccess: undefined,
        cloneAppFailure: undefined
      })
    case CONSTANTS.CLONE_APP_SUCCESS:
      return Object.assign({}, state, {
        cloneAppSuccess: action.response
      });

    case CONSTANTS.CLONE_APP_FAILURE:
      return Object.assign({}, state, {
        cloneAppFailure: {
          message: action.error,
          id: action.addOns.id
        }
      });
    case CONSTANTS.SHARE_APPLICATION_SUCCESS:
      return Object.assign({}, state, {
        shareAppSuccess: {
          message: action.response,
          timestamp: Date.now()
        }
      });

    case CONSTANTS.SHARE_APPLICATION_FAILURE:
      return Object.assign({}, state, {
        shareAppError: {
          message: action.error,
          timestamp: Date.now()
        }
      });
    case CONSTANTS.MQTT_CONNECTOR_SUCCESS:
      return Object.assign({}, state, {
        getConnectorsListSuccess: action.response,
      });

    case CONSTANTS.MQTT_CONNECTOR_FAILURE:
      return Object.assign({}, state, {
        getConnectorsListFailure: action.error,
      });
    case CONSTANTS.GET_DEVELOPER_QUOTA_SUCCESS:
      return Object.assign({}, state, {
        developerQuotaSuccess: action.response
      });
    case CONSTANTS.GET_DEVELOPER_QUOTA_FAILURE:
      return Object.assign({}, state, {
        developerQuotaFailure: action.error
      });

    default:
      return state;
  }
}

export default manageApplicationsStudioReducer;
