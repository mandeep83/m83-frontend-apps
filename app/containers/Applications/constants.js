/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

export const DEFAULT_ACTION = "app/Applications/DEFAULT_ACTION";

export const RESET_TO_INITIAL_STATE = "app/Applications/RESET_TO_INITIAL_STATE";

export const GET_APPLICATIONS = "app/Applications/GET_APPLICATIONS";
export const GET_APPLICATIONS_SUCCESS = "app/Applications/GET_APPLICATIONS_SUCCESS";
export const GET_APPLICATIONS_FAILURE = "app/Applications/GET_APPLICATIONS_FAILURE";

export const DELETE_APPLICATION = "app/Applications/DELETE_APPLICATIONS";
export const DELETE_APPLICATION_SUCCESS = "app/Applications/DELETE_APPLICATIONS_SUCCESS";
export const DELETE_APPLICATION_FAILURE = "app/Applications/DELETE_APPLICATIONS_FAILURE";

export const EXPORT_APPLICATION = "app/Applications/EXPORT_APPLICATIONS";
export const EXPORT_APPLICATION_SUCCESS = "app/Applications/EXPORT_APPLICATIONS_SUCCESS";
export const EXPORT_APPLICATION_FAILURE = "app/Applications/EXPORT_APPLICATIONS_FAILURE";

export const IMPORT_APPLICATION = "app/Applications/IMPORT_APPLICATIONS";
export const IMPORT_APPLICATION_SUCCESS = "app/Applications/IMPORT_APPLICATIONS_SUCCESS";
export const IMPORT_APPLICATION_FAILURE = "app/Applications/IMPORT_APPLICATIONS_FAILURE";

export const CHANGE_APPLICATION_PUBLISH_STATUS = "app/Applications/CHANGE_APPLICATION_PUBLISH_STATUS";
export const CHANGE_APPLICATION_PUBLISH_STATUS_SUCCESS = "app/Applications/CHANGE_APPLICATION_PUBLISH_STATUS_SUCCESS";
export const CHANGE_APPLICATION_PUBLISH_STATUS_FAILURE = "app/Applications/CHANGE_APPLICATION_PUBLISH_STATUS_FAILURE";

export const CLONE_APP = "app/Applications/CLONE_APPS";
export const CLONE_APP_SUCCESS = "app/Applications/CLONE_APPS_SUCCESS";
export const CLONE_APP_FAILURE = "app/Applications/CLONE_APPS_FAILURE";

export const SHARE_APPLICATION = "app/Applications/SHARE_APPLICATION";
export const SHARE_APPLICATION_SUCCESS = "app/Applications/SHARE_APPLICATION_SUCCESS";
export const SHARE_APPLICATION_FAILURE = "app/Applications/SHARE_APPLICATION_FAILURE";

export const MQTT_CONNECTOR = "app/Applications/MQTT_CONNECTOR";
export const MQTT_CONNECTOR_SUCCESS = "app/Applications/MQTT_CONNECTOR_SUCCESS";
export const MQTT_CONNECTOR_FAILURE = "app/Applications/MQTT_CONNECTOR_FAILURE";

export const GET_DEVELOPER_QUOTA = 'app/Applications/GET_DEVELOPER_QUOTA';
export const GET_DEVELOPER_QUOTA_SUCCESS = 'app/Applications/GET_DEVELOPER_QUOTA_SUCCESS';
export const GET_DEVELOPER_QUOTA_FAILURE = 'app/Applications/GET_DEVELOPER_QUOTA_FAILURE';
