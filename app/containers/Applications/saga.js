/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { takeEvery } from 'redux-saga';
import {apiCallHandler} from '../../api';
import * as CONSTANTS from "./constants";

export function* getApplicationsListAsync(action) {
  yield[apiCallHandler(action, CONSTANTS.GET_APPLICATIONS_SUCCESS, CONSTANTS.GET_APPLICATIONS_FAILURE, 'getApplications')];
}

export function* watcherApplicationsListRequest() {
  yield takeEvery(CONSTANTS.GET_APPLICATIONS, getApplicationsListAsync);
}

export function* deleteApplicationAsync(action) {
  yield[apiCallHandler(action, CONSTANTS.DELETE_APPLICATION_SUCCESS, CONSTANTS.DELETE_APPLICATION_FAILURE, 'deleteApplication')];
}

export function* watcherDeleteApplicationRequest() {
  yield takeEvery(CONSTANTS.DELETE_APPLICATION, deleteApplicationAsync);
}

export function* exportApplicationAsync(action) {
  yield[apiCallHandler(action, CONSTANTS.EXPORT_APPLICATION_SUCCESS, CONSTANTS.EXPORT_APPLICATION_FAILURE, 'exportApplication')];
}

export function* cloneAppApiHandler(action) {
  yield[apiCallHandler(action, CONSTANTS.CLONE_APP_SUCCESS, CONSTANTS.CLONE_APP_FAILURE, 'cloneAppApi')];
}

export function* watcherExportApplicationRequest() {
  yield takeEvery(CONSTANTS.EXPORT_APPLICATION, exportApplicationAsync);
}

export function* importApplicationAsync(action) {
  yield[apiCallHandler(action, CONSTANTS.IMPORT_APPLICATION_SUCCESS, CONSTANTS.IMPORT_APPLICATION_FAILURE, 'importApplication')];
}

export function* ChangeApplicationPublishStatusHandlerAsync(action) {
  yield[apiCallHandler(action, CONSTANTS.CHANGE_APPLICATION_PUBLISH_STATUS_SUCCESS, CONSTANTS.CHANGE_APPLICATION_PUBLISH_STATUS_FAILURE, 'changeApplicationStatus')];
}

export function* shareApplicationHandlerAsync(action) {
  yield[apiCallHandler(action, CONSTANTS.SHARE_APPLICATION_SUCCESS, CONSTANTS.SHARE_APPLICATION_FAILURE, 'shareResources')];
}

export function* mqttConnectorListHandlerAsync(action) {
  yield[apiCallHandler(action, CONSTANTS.MQTT_CONNECTOR_SUCCESS, CONSTANTS.MQTT_CONNECTOR_FAILURE, 'deviceTypeList')];
}

export function* getDeveloperQuotaHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_DEVELOPER_QUOTA_SUCCESS, CONSTANTS.GET_DEVELOPER_QUOTA_FAILURE, 'getDeveloperQuota')];
}

export function* watcherImportApplicationRequest() {
  yield takeEvery(CONSTANTS.IMPORT_APPLICATION, importApplicationAsync);
}

export function* watcherCloneApp() {
  yield takeEvery(CONSTANTS.CLONE_APP, cloneAppApiHandler);
}

export function* watcherChangeApplicationPublishStatus() {
  yield takeEvery(CONSTANTS.CHANGE_APPLICATION_PUBLISH_STATUS, ChangeApplicationPublishStatusHandlerAsync);
}

export function* watcherShareApplication() {
  yield takeEvery(CONSTANTS.SHARE_APPLICATION, shareApplicationHandlerAsync);
}

export function* watcherMqttConnectorList() {
  yield takeEvery(CONSTANTS.MQTT_CONNECTOR, mqttConnectorListHandlerAsync);
}

export function* watcherGetDeveloperQuota() {
  yield takeEvery(CONSTANTS.GET_DEVELOPER_QUOTA, getDeveloperQuotaHandlerAsync);
}


export default function* rootSaga() {
  yield [
    watcherApplicationsListRequest(),
    watcherDeleteApplicationRequest(),
    watcherExportApplicationRequest(),
    watcherImportApplicationRequest(),
    watcherChangeApplicationPublishStatus(),
    watcherCloneApp(),
    watcherShareApplication(),
    watcherMqttConnectorList(),
    watcherGetDeveloperQuota(),
  ]
}

