/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import * as ACTIONS from "./actions";
import * as SELECTORS from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import NotificationModal from '../../components/NotificationModal/Loadable'
import Loader from '../../components/Loader/Loadable';
import ConfirmModel from "../../components/ConfirmModel";
import ReactTooltip from 'react-tooltip'
import * as Actions from "../AddOrEditFaasList/actions";
import {isNavAssigned} from "../../commonUtils";
import TagsInput from 'react-tagsinput';
import jwt_decode from "jwt-decode";
import cloneDeep from "lodash/cloneDeep";

import ListingTable from "../../components/ListingTable/Loadable";
import NoDataFoundMessage from "../../components/NoDataFoundMessage/Loadable"
import AddNewButton from "../../components/AddNewButton/Loadable"


/* eslint-disable react/prefer-stateless-function */
export class Applications extends React.Component {

    state = {
        shareVia: "E-mail",
        isFetching: true,
        isOpen: false,
        confirmState: false,
        selectedAppName: "",
        selectedAppId: "",
        applicationsList: [],
        shareEmails: [],
        selectedAppURL: "",
        isAddEditApplicationSuccessModal: false,
        exportApplication: {
            fileUrl: "",
            password: "",
            username: "",
            file: ''
        },
        errors: {
            password: "",
            username: "",
            file: ''
        },
        isImportModal: false,
        copyToClipboardMessage: false,
        isAppCloneLoader: false,
        viewType: "TABLE",
        showShareModal: false,
        showSharedEmailModal: false,
        connectorsList: [],
        selectedDeviceTypeId: typeof this.props.match.params.id === 'undefined' ? '' : this.props.match.params.id,
        searchTerm: '',
        totalApplicationsCount: 0,
    }

    componentDidMount() {
        this.props.getDeveloperQuota(['application']);
        this.props.getConnectorsListByCategory()
    }

    componentWillReceiveProps(nextProps) {

        if (nextProps.developerQuota && nextProps.developerQuota !== this.props.developerQuota) {
            let totalApplicationsCount = nextProps.developerQuota.filter(product => product.productName === 'application')[0]['total'];
            this.setState({
                totalApplicationsCount
            });
        }

        if (nextProps.developerQuotaFailure && nextProps.developerQuotaFailure !== this.props.developerQuotaFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.developerQuotaFailure
            });
        }
        if (nextProps.getApplicationsSuccess && nextProps.getApplicationsSuccess !== this.props.getApplicationsSuccess) {
            let applicationsList = nextProps.getApplicationsSuccess 
            this.setState({
                isAddEditApplicationSuccessModal: false,
                applicationsList,
                isAppCloneLoader: false,
                isFetching: false,
            })
        }

        if (nextProps.getApplicationsError && nextProps.getApplicationsError !== this.props.getApplicationsError) {
            this.setState({
                modalType: 'error',
                isOpen: true,
                message2: nextProps.getApplicationsError,
                isFetching: false,
            }, () => this.props.resetToInitialState());
        }
        if (nextProps.deleteApplicationSuccessRandom && nextProps.deleteApplicationSuccessRandom !== this.props.deleteApplicationSuccessRandom) {
            this.setState({
                selectedAppId: '',
                isFetching: false,
                modalType: "success",
                isOpen: true,
                message2: nextProps.deleteApplicationSuccess,
            }, () => this.props.resetToInitialState());
        }

        if (nextProps.deleteApplicationError && nextProps.deleteApplicationError !== this.props.deleteApplicationError) {
            this.setState({
                modalType: 'error',
                isOpen: true,
                message2: nextProps.deleteApplicationError,
                isFetching: false,
            }, () => this.props.resetToInitialState());
        }

        if (nextProps.exportApplicationSuccess && nextProps.exportApplicationSuccess !== this.props.exportApplicationSuccess) {
            this.setState({
                exportApplication: nextProps.exportApplicationSuccess,
                isExporting: false
            })
        }

        if (nextProps.exportApplicationError && nextProps.exportApplicationError !== this.props.exportApplicationError) {
            this.setState({
                modalType: 'error',
                isOpen: true,
                message2: nextProps.exportApplicationError,
                isFetching: false,
            }, () => this.props.resetToInitialState());
        }

        if (nextProps.importApplicationSuccess && nextProps.importApplicationSuccess !== this.props.importApplicationSuccess) {
            this.setState({
                modalType: "success",
                isOpen: true,
                message2: "Application Imported",
            }, () => {
                this.props.getApplicationsList();
                this.props.resetToInitialState();
            })
        }

        if (nextProps.importApplicationError && nextProps.importApplicationError !== this.props.importApplicationError) {
            this.setState({
                modalType: 'error',
                isOpen: true,
                message2: nextProps.importApplicationError,
                isFetching: false,
            }, () => this.props.resetToInitialState());
        }

        if (nextProps.changeApplicationPublishStatusSuccess && nextProps.changeApplicationPublishStatusSuccess !== this.props.changeApplicationPublishStatusSuccess) {
            this.props.getApplicationsList();
        }

        if (nextProps.changeApplicationPublishStatusError && nextProps.changeApplicationPublishStatusError !== this.props.changeApplicationPublishStatusError) {
            this.setState({
                modalType: 'error',
                isOpen: true,
                message2: nextProps.changeApplicationPublishStatusError.message,
                isFetching: false,
            })
        }

        if (nextProps.cloneAppSuccess && (nextProps.cloneAppSuccess !== this.props.cloneAppSuccess)) {
            let applicationsList = JSON.parse(JSON.stringify(this.state.applicationsList));
            applicationsList.map((item) => {
                if (item.id === nextProps.cloneAppSuccess.id) {
                    item.isDisabled = false
                }
            })
            this.setState({
                applicationsList,
                isOpen: true,
                message2: nextProps.cloneAppSuccess.message,
                modalType: 'success',
            }, () => this.props.getApplicationsList());
        }
        if (nextProps.cloneAppFailure && nextProps.cloneAppFailure !== this.props.cloneAppFailure) {
            let applicationsList = JSON.parse(JSON.stringify(this.state.applicationsList));
            applicationsList.map((item) => {
                if (item.id === nextProps.cloneAppFailure.id) {
                    item.isDisabled = false
                }
            })
            this.setState({
                modalType: 'error',
                isOpen: true,
                message2: nextProps.cloneAppFailure.message,
                isAppCloneLoader: false,
                applicationsList,
                isFetching: false,
            })
        }
        if (nextProps.shareAppSuccess && (nextProps.shareAppSuccess !== this.props.shareAppSuccess)) {
            this.setState({
                isOpen: true,
                isFetching: true,
                message2: nextProps.shareAppSuccess.message,
                modalType: 'success',
            }, () => { this.props.getApplicationsList() });

        }
        if (nextProps.shareAppFailure && nextProps.shareAppFailure !== this.props.shareAppFailure) {
            this.setState({
                isOpen: true,
                isFetching: false,
                message2: nextProps.shareAppFailure.message,
                modalType: 'error',
            })
        }

        if (nextProps.getConnectorsListSuccess && nextProps.getConnectorsListSuccess !== this.props.getConnectorsListSuccess) {

            nextProps.getConnectorsListSuccess.forEach(connector => { connector.label = connector.name; connector.value = connector.id });
            let selectedDeviceTypeId = this.state.selectedDeviceTypeId;
            if (typeof this.props.match.params.id === 'undefined' && nextProps.getConnectorsListSuccess.length > 0) {
                selectedDeviceTypeId = nextProps.getConnectorsListSuccess[0].id;
            }
            this.setState({
                selectedDeviceTypeId,
                connectorsList: nextProps.getConnectorsListSuccess,
            }, () => this.props.getApplicationsList())
        }
        if (nextProps.getConnectorsListFailure && nextProps.getConnectorsListFailure !== this.props.getConnectorsListFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.getConnectorsListFailure,
                isFetching: false,
            })
        }
    }

    onCloseHandler = () => {
        this.setState({
            isOpen: false,
            message2: '',
            modalType: "",
        });
    }

    confirmBox = (id, name) => {
        this.setState({
            selectedAppName: name,
            selectedAppId: id,
            confirmState: true,
        })
    }

    cancelClicked = () => {
        this.setState({
            selectedAppName: "",
            selectedAppId: "",
            confirmState: false
        })
    }

    shareViaHandler = (event) => {
        this.setState({ shareVia: event.target.id })
    }

    copyToClipboard = () => {
        var copyText = document.getElementById("copyText");
        copyText.select();
        document.execCommand("copy");
        ReactTooltip.show(this.copyMessage)
        setTimeout(() => ReactTooltip.hide(this.copyMessage), 2500)
    }

    closeAddOrEditApi = (isSuccessfulUpdate) => {
        this.setState({ AddOrEditApplication: false, selectedAppId: "", isFetching: isSuccessfulUpdate ? true : false });
        isSuccessfulUpdate && this.props.getApplicationsList();
    }

    successModalOpen = (url) => {
        this.setState({
            selectedAppURL: url,
            isAddEditApplicationSuccessModal: true,
        })
        $('#linkModal').modal('show');
    }

    successModalHandler = () => {
        if (this.state.isAddEditApplicationSuccessModal) {
            this.setState({ isFetching: true })
            this.props.getApplicationsList();
        }
    }

    onChangeHandler = event => {
        let exportApplication = { ...this.state.exportApplication }
        let isUploadingFile = this.state.isUploadingFile
        let fileTypeError = this.state.fileTypeError
        if (event.target.id === "file") {
            var ext = event.target.files[0].name.match(/\.([^\.]+)$/)[1];
            switch (ext) {
                case 'zip':
                    exportApplication.file = event.target.files[0]
                    isUploadingFile = true;
                    fileTypeError = false
                    setTimeout(() => this.setState({ isUploadingFile: false }), 2000)
                    break;
                default:
                    fileTypeError = true
            }
        } else {
            exportApplication[event.target.id] = event.target.value
        }
        this.setState({
            exportApplication,
            isUploadingFile,
            fileTypeError
        })
    }

    importApplicationFn = (event) => {
        event.preventDefault();
        let formData = new FormData()
        formData.append("username", this.state.exportApplication.username)
        formData.append("password", this.state.exportApplication.password)
        formData.append("file", this.state.exportApplication.file)
        this.props.importApplication(formData)
        $('#addAppModal').modal('hide');
        this.setState({
            isImportModal: false,
            isFetching: true,
        });
    };

    stateCleaner = () => {
        this.setState({
            exportApplication: {
                fileUrl: "",
                password: "",
                username: "",
                file: ''
            }
        })
    }

    toggleApplicationStatus = (id, isPublished) => {
        let applicationsList = cloneDeep(this.state.applicationsList);
        applicationsList.find(app => app.id === id).loader = true
        this.setState({
            applicationsList,
        }, () => { this.props.changeApplicationPublishStatus(id, isPublished) });
    }

    cloneApp = (id) => {
        let applicationsList = JSON.parse(JSON.stringify(this.state.applicationsList));
        applicationsList.map(app => {
            if (app.id === id) {
                app.isDisabled = true;
            }
        })
        this.setState({
            isAppCloneLoader: true,
            applicationsList,
            isOpen: false
        }, () => this.props.cloneApp(id));
    }

    shareEmailsChangeHandler = (shareEmails, newEntry) => {
        if (newEntry[0] === (jwt_decode(localStorage.token).sub)) {
            this.setState({
                modalType: 'error',
                isOpen: true,
                message2: 'Share Email cannot be same as owner`s email',
            })
            return;
        }
        this.setState({
            shareEmails,
        })
    }

    shareApplicationModalHandler = (selectedAppId, app) => {
        this.setState({
            selectedAppId,
            shareEmails: app.sharedEmailList && app.sharedEmailList.length ? app.sharedEmailList : [],
            showShareModal: true
        })
    }

    shareApplication = () => {
        let shareEmails = cloneDeep(this.state.shareEmails),
            payload = {
                user: shareEmails
            };
        this.setState({
            showShareModal: false,
            isFetching: true,
        }, () => { this.props.shareApplication('Applications', this.state.selectedAppId, payload) });
    }

    getColumns = () => {
        return [
            { Header: "Name", width: 20,
                cell: (row) => (
                    <React.Fragment>
                        <div className="list-view-icon"><img src={row.logoImageUrl ? row.logoImageUrl : "https://content.iot83.com/m83/misc/device-type-fallback.png"} /></div>
                        <div className="list-view-icon-box">
                            <h6 className="text-theme fw-600">{row.name}</h6>
                            <p>{row.description}</p>
                        </div>
                    </React.Fragment>
                )
            },
            { Header: "Status", width: 10, cell: (row) => <h6 className={row.isPublished ? "text-green" : "text-red"}>{row.isPublished ? 'Published' : 'Unpublished'}</h6> },
            { Header: "Shared With", width: 10,
                cell: (row) => <span className="alert alert-primary list-view-badge">{row.sharedEmailList.length}</span>
            },
            {
                Header: "Device Type", width: 15, cell: row => (
                    <div className="button-group-link">
                        <button className="btn btn-link text-truncate" disabled onClick={() => { this.props.history.push(`/addOrEditDeviceType/${row.connectorId}`) }}>{row.connectorName}</button>
                    </div>
                )
            },
            { Header: "Created", width: 15, accessor: "createdAt" },
            { Header: "Updated", width: 15, accessor: "updatedAt" },
            {
                Header: "Actions", width: 15, cell: (row) => (
                    <div className="button-group">
                        {row.loader ?
                            <button className="btn-transparent btn-transparent-yellow">
                                <i className="far fa-cog fa-spin"></i>
                            </button>
                            :
                            <button className={`btn-transparent ${row.isPublished ? "btn-transparent-yellow" : "btn-transparent-gray"}`} data-tip data-for={"publish" + row.id} onClick={() => { this.toggleApplicationStatus(row.id, row.isPublished) }}>
                                {row.isPublished ?
                                    <i className="far fa-parking-circle"></i>
                                    :
                                    <i className="far fa-parking-circle-slash"></i>
                                }
                            </button>
                        }
                        {/*<button className="btn-transparent btn-transparent-green" disabled={localStorage.tenantType === "MAKER"} data-tip data-for={"copy" + row.id} onClick={() => this.cloneApp(row.id)}>
                            <i className="far fa-copy"></i>
                        </button>*/}
                        <button className="btn-transparent btn-transparent-cyan" data-tip data-for={"share" + row.id} onClick={() => { this.shareApplicationModalHandler(row.id, row) }} disabled={(!row.isPublished) || (!row.resourceOwner)}>
                            <i className="far fa-share-alt"></i>
                        </button>
                        <button className="btn-transparent btn-transparent-blue" data-tip data-for={"edit" + row.id} onClick={() => { this.props.history.push('/addOrEditApplications/' + row.id) }}>
                            <i className="far fa-pencil"></i>
                        </button>
                        <button className=" btn-transparent btn-transparent-red" data-tip data-for={"delete" + row.id} onClick={() => { row.isPublished || this.confirmBox(row.id, row.name) }} disabled={row.isPublished}>
                            <i className="far fa-trash-alt"></i>
                        </button>
                    </div>
                )
            },
        ]
    }

    filterChangeHandler = ({ currentTarget }) => {
        this.setState({
            selectedDeviceTypeId: currentTarget.value
        })
    }

    getStatusBarClass = (row) => {
        if (row.isPublished)
            return { className: "green", tooltipText: "Published" }
        return { className: "red", tooltipText: "Unpublished" }
    }

    getAddNewButton = () => {
        let connectorId = this.state.selectedDeviceTypeId;
        let text2 = `You haven't created any application(s)${connectorId ? " with this Device Type" : ""} yet`
        return (
            <AddNewButton
                text1="No application(s) available"
                text2={text2}
                imageIcon="addApp.png"
                createItemOnAddButtonClick={() => { this.props.history.push({ pathname: '/addOrEditApplications', state: { connectorId } }) }}
            />
        )
    }
    getFilteredApplications = () => {
        let applicationsList = this.state.applicationsList;
        if ( this.state.selectedDeviceTypeId !== '' && applicationsList.length > 0) {
            let deviceTypeName = this.state.connectorsList.filter(connector => connector.id === this.state.selectedDeviceTypeId)[0].name;
            return applicationsList.filter(application => application.connectorName === deviceTypeName);
        } else {
            return [];
        }

    }

    emptySearchBox = () => {
        this.setState({
            searchTerm: '',
        })
    }

    render() {
        let filteredApplications = this.getFilteredApplications();
        return (
            <React.Fragment>
                <Helmet>
                    <title>Applications</title>
                    <meta name="description" content="M83-Applications" />
                </Helmet>

                <header className="content-header d-flex">
                    <div className="flex-60">
                        <h6>Applications -
                            <span className="content-header-badge-group">
                                <span className="content-header-badge-item">Allocated <span className="badge badge-pill badge-primary">{this.state.totalApplicationsCount}</span></span>
                                <span className="content-header-badge-item">Used <span className="badge badge-pill badge-success">{filteredApplications.length}</span></span>
                                <span className="content-header-badge-item">Remaining <span className="badge badge-pill badge-warning">{this.state.totalApplicationsCount > 0 ? this.state.totalApplicationsCount - filteredApplications.length : 0}</span></span>
                            </span>
                        </h6>
                    </div>
                    {this.state.applicationsList.length > 0 &&
                        <div className="flex-40 text-right">
                            <div className="content-header-group">
                                <div className="search-box">
                                    <span className="search-icon"><i className="far fa-search"></i></span>
                                    <input type="text" className="form-control" id="searchTerm" placeholder="Search..." value={this.state.searchTerm} disabled={this.state.applicationsList.length == 0} />
                                    {this.state.searchTerm.length > 0  && <button className="search-button" onClick={() => this.emptySearchBox()}><i className="far fa-times"></i></button>}
                                </div>
                                {/* <button className="btn btn-light"><i className="fad fa-list-ul"></i></button>
                                    <button className="btn btn-light"><i className="fad fa-table"></i></button> */}
                                <button className="btn btn-primary" disabled={(this.state.totalApplicationsCount - filteredApplications.length) === 0} onClick={() => { this.props.history.push('/addOrEditApplications') }}><i className="far fa-plus"></i></button>
                            </div>
                        </div>
                    }
                </header>

                {this.state.isFetching ? <Loader /> :
                    <React.Fragment>
                        {(this.state.connectorsList.length > 0 && this.state.applicationsList.length > 0) &&
                            <div className="d-flex content-filter">
                                <div className="flex-50 form-group pd-r-10">
                                    <div className="input-group">
                                        <div className="input-group-prepend">
                                            <span className="input-group-text">Device Type :</span>
                                        </div>
                                        <select id='connectorId' className="form-control" value={this.state.selectedDeviceTypeId} onChange={this.filterChangeHandler}>
                                            {this.state.connectorsList.map(connector => {
                                                return (
                                                    <option key={connector.id} value={connector.id}>{connector.name}</option>
                                                )
                                            })}
                                        </select>
                                    </div>
                                </div>
                            </div>
                        }
                        <div className={`content-body ${this.state.applicationsList.length > 0 && "content-filter-body"}`}>
                            {this.state.connectorsList.length > 0 ?
                                this.state.applicationsList && this.state.applicationsList.length > 0 ?
                                    filteredApplications.length > 0 ?
                                        <ListingTable
                                            columns={this.getColumns()}
                                            data={filteredApplications}
                                            statusBar={this.getStatusBarClass}
                                        />
                                        :
                                        this.state.filters.name ?
                                            <NoDataFoundMessage /> : this.getAddNewButton()
                                    :
                                    this.getAddNewButton()
                                :
                                <AddNewButton
                                    text1="No application(s) available"
                                    text2="You haven't created any device type(s) yet. Please create a device type first."
                                    addButtonEnable={isNavAssigned("deviceTypes")}
                                    createItemOnAddButtonClick={() => this.props.history.push(`/addOrEditDeviceType`)}
                                    imageIcon="addDevice.png"
                                />
                            }
                        </div>
                    </React.Fragment>
                }

                {this.state.confirmState &&
                    <ConfirmModel
                        status={"delete"}
                        deleteName={this.state.selectedAppName}
                        confirmClicked={() => {
                            this.props.deleteApplicationById(this.state.selectedAppId)
                            this.setState({
                                isFetching: true,
                                confirmState: false,
                                isOpen: false
                            })
                        }}
                        cancelClicked={() => {
                            this.cancelClicked()
                        }}
                    />
                }

                {/* share modal */}
                {this.state.showShareModal &&
                    <div className="modal d-block animated slideInDown">
                        <div className="modal-dialog modal-dialog-centered">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h6 className="modal-title">Share Application - <span>{this.state.applicationsList.filter(application => application.id === this.state.selectedAppId)[0].name}</span>
                                        <button type="button" className="close" onClick={() => { this.setState({ showShareModal: false }) }}><i className="far fa-times"></i></button>
                                    </h6>
                                </div>
                                <div className="modal-body">
                                    <div className="form-group">
                                        <div className="share-info-wrapper">
                                            <div className="share-info-image">
                                                <img src="https://content.iot83.com/m83/icons/user.png" />
                                            </div>
                                            <span className="badge badge-pill alert-success">Owner</span>
                                            <h5 className="text-theme">{localStorage.Username}</h5>
                                            <p className="text-gray mb-0"><i className="fad fa-envelope mr-r-5 text-green"></i>{jwt_decode(localStorage.token).sub}</p>
                                        </div>
                                    </div>
                                    
                                    <div className="form-group">
                                        <label className="form-group-label">Emails: (Max 3 emails allowed)
                                            <span className="text-cyan f-11 float-right"><strong>Note:</strong> Please press enter(&crarr;) to add email.</span>
                                        </label>
                                        <TagsInput value={this.state.shareEmails} onChange={this.shareEmailsChangeHandler} maxTags={3} onlyUnique={true} validationRegex={/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/} onValidationReject={() => {
                                            this.setState({
                                                modalType: 'error',
                                                isOpen: true,
                                                message2: 'Incorrect Email Format',
                                            })
                                        }} />
                                    </div>
                                </div>

                                <div className="modal-footer">
                                    <button type="button" className="btn btn-success" onClick={() => { this.shareApplication() }}>Share</button>
                                    <button type="button" className="btn btn-dark" onClick={() => { this.setState({ showShareModal: false }) }}>Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                }
                {/* end share modal */}

                {
                    this.state.isOpen &&
                    <NotificationModal
                        type={this.state.modalType}
                        message2={this.state.message2}
                        onCloseHandler={this.onCloseHandler}
                    />
                }

            </React.Fragment>
        );
    }
}

Applications.propTypes = {
    dispatch: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
    isFetching: SELECTORS.getIsFetching(),
    getApplicationsSuccess: SELECTORS.getApplicationsSuccess(),
    getApplicationsError: SELECTORS.getApplicationsError(),
    deleteApplicationSuccess: SELECTORS.deleteApplicationSuccess(),
    deleteApplicationSuccessRandom: SELECTORS.deleteApplicationSuccessRandom(),
    deleteApplicationError: SELECTORS.deleteApplicationError(),
    importApplicationSuccess: SELECTORS.importApplicationSuccess(),
    importApplicationError: SELECTORS.importApplicationError(),
    exportApplicationSuccess: SELECTORS.exportApplicationSuccess(),
    exportApplicationError: SELECTORS.exportApplicationError(),
    changeApplicationPublishStatusSuccess: SELECTORS.changeApplicationPublishStatusSuccess(),
    changeApplicationPublishStatusError: SELECTORS.changeApplicationPublishStatusError(),
    cloneAppSuccess: SELECTORS.cloneAppSuccess(),
    cloneAppFailure: SELECTORS.cloneAppFailure(),
    shareAppSuccess: SELECTORS.shareAppSuccess(),
    shareAppFailure: SELECTORS.shareAppFailure(),
    getConnectorsListSuccess: SELECTORS.getConnectorsListSuccess(),
    getConnectorsListFailure: SELECTORS.getConnectorsListError(),
    developerQuota: SELECTORS.developerQuotaSuccess(),
    developerQuotaError: SELECTORS.developerQuotaFailure(),

});

function mapDispatchToProps(dispatch) {
    return {
        dispatch,
        getApplicationsList: () => dispatch(ACTIONS.getApplicationsList()),
        deleteApplicationById: (id) => dispatch(ACTIONS.deleteApplicationById(id)),
        exportApplication: (id) => dispatch(ACTIONS.exportApplication(id)),
        importApplication: (formData) => dispatch(ACTIONS.importApplication(formData)),
        resetToInitialState: () => dispatch(Actions.resetToInitialState()),
        changeApplicationPublishStatus: (id, isPublished) => dispatch(ACTIONS.changeApplicationPublishStatus(id, isPublished)),
        cloneApp: (id) => dispatch(ACTIONS.cloneApp(id)),
        shareApplication: (type, id, payload) => dispatch(ACTIONS.shareApplication(type, id, payload)),
        getConnectorsListByCategory: () => dispatch(ACTIONS.getConnectorsListByCategory()),
        getDeveloperQuota: (products) => dispatch(ACTIONS.getDeveloperQuota(products)),

    };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "applications", reducer });
const withSaga = injectSaga({ key: "applications", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect,
)(Applications);
