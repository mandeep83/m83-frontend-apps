/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import * as SELECTORS from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import * as ACTIONS from "./actions";
import NotificationModal from '../../components/NotificationModal/Loadable'
import Loader from "../../components/Loader/index";
import cloneDeep from "lodash/cloneDeep";
import PhoneInput from "react-phone-input-2";
import 'react-phone-input-2/lib/style.css';


/* eslint-disable react/prefer-stateless-function */
export class AddOrEditTenant extends React.Component {
    state = {
        allLicenses: [],
        payload: {
            companyName: '',
            tenantName: '',
            description: '',
            companyAddress: '',
            tenantCategory: "MAKER",
            defaultAccount: false,
            defaultResources: true,
            users: [{
                firstName: '',
                lastName: '',
                mobile: '',
                email: '',
            }],
            licenseId: null,
            oAuthProviders: [],
        },
        isFetching: true,
        message: '',
        type: '',
        isUserForm: false,
    };

    componentWillMount() {
        this.props.getAccountList();
        if (this.props.match.params.id) {
            this.props.fetchTenantDetails(this.props.match.params.id);
        } else {
            this.props.fetchLicenseInfo();
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.tenantDetails && nextProps.tenantDetails !== this.props.tenantDetails) {
            let payload = nextProps.tenantDetails;
            this.setState({
                payload,
                isFetching: false,
            });
        }


        if (nextProps.tenantDetailsError && nextProps.tenantDetailsError !== this.props.tenantDetailsError) {
            this.setState({
                modalType: "error",
                isOpen: true,
                isFetching: false,
                message2: nextProps.tenantDetailsError
            });
        }
        if (nextProps.accountList && nextProps.accountList !== this.props.accountList) {
            this.setState({
                tenantCount: nextProps.accountList.length
            });
        }

        if (nextProps.accountListError && nextProps.accountListError !== this.props.accountListError) {
            this.setState({
                modalType: "error",
                isOpen: true,
                isFetching: false,
                message2: nextProps.accountListError
            });
        }

        if (nextProps.submitFormSuccess && nextProps.submitFormSuccess !== this.props.submitFormSuccess) {
            if (nextProps.submitFormSuccess.isSaasTenant) {
                this.setState({
                    selectedLicenseDetails: {},
                    selectedLicenseId: "",
                    modalType: "success",
                    isOpen: true,
                    message2: 'Tenant Created Successfully',
                })
            }
        }

        if (nextProps.submitFormFailure && nextProps.submitFormFailure != this.props.submitFormFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.submitFormFailure,
                isFetching: false
            });
        }

        if (nextProps.licenseList && nextProps.licenseList !== this.props.licenseList) {
            let allLicenses = nextProps.licenseList;
            this.setState({
                allLicenses,
                isFetching: false,
            })
        }

        if (nextProps.licenseError && nextProps.licenseError !== this.props.licenseError) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.licenseError
            });
        }
    }

    inputChangeHandler = (event) => {
        let payload = JSON.parse(JSON.stringify(this.state.payload));
        if (event.target.id === "tenantName" && !/^[^_\s]*$/.test(event.target.value)) {
            return
        }
        if (event.target.id === "mobile" && isNaN(Number(event.target.value))) {
            return;
        }
        if (event.target.id === "defaultAccount" || event.target.id === "defaultResources") {
            payload[event.target.id] = event.target.checked;
        } else if (event.currentTarget.name === "tenantCategory") {
            payload.tenantCategory = event.currentTarget.value;
        } else {
            payload[event.target.id] = event.target.value;
        }

        this.setState({
            payload
        });
    };

    onCloseHandler = () => {
        if (this.state.modalType === 'error') {
            this.setState({
                isOpen: false,
                message2: "",
            });
        } else {
            this.setState({
                isOpen: false,
                message2: "",
            }, () => { this.props.history.push('/tenantList') });
        }
    };

    onAdminDetailsChangeHandler = (event, index) => {
        let payload = this.state.payload;
        payload.users[index][event.target.id] = event.target.value;
        this.setState({
            payload
        })
    }

    onSubmitHandler = event => {
        event.preventDefault()
        let payload = JSON.parse(JSON.stringify(this.state.payload));
        payload.licenseId = this.state.allLicenses.find(el => el.tenantCategory === "MAKER") ? this.state.allLicenses.find(el => el.tenantCategory === "MAKER").id : null
        this.setState({
            isFetching: true
        }, () => {
            this.props.onSubmit(payload)
        });
    };

    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>Add/Edit Tenant</title>
                    <meta name="description" content="M83-AddOrEditTenant" />
                </Helmet>

                {this.state.isFetching ? <Loader /> :
                    <React.Fragment>
                        <header className="content-header d-flex">
                            <div className="flex-60">
                                <div className="d-flex">
                                    <h6 className="previous" onClick={() => { this.props.history.push('/tenantList') }}>Manage Tenant</h6>
                                    <h6 className="active">Add New</h6>
                                </div>
                            </div>
                            <div className="flex-40 text-right">
                                <div className="content-header-group">
                                    <button className="btn btn-light" data-tooltip data-tooltip-text="Back" data-tooltip-place="bottom" onClick={() => this.props.history.push('/tenantList')}><i className="fas fa-angle-left"></i></button>
                                </div>
                            </div>
                        </header>

                        <div className="content-body">
                            <div className="form-content-wrapper">
                                <div className="form-content-box">
                                    <div className="form-content-header">
                                        <p>Tenant Details</p>
                                    </div>
                                    <div className="form-content-body">
                                        <div className="d-flex">
                                            <div className="flex-50 pd-r-10">
                                                <div className="form-group">
                                                    <label className="form-group-label">Tenant Name :</label>
                                                    <input
                                                        type="text" id="tenantName" className="form-control" required
                                                        pattern="^[a-zA-Z]([\w\$-]*[a-zA-Z0-9])?$"
                                                        value={this.state.payload.tenantName}
                                                        disabled={this.props.match.params.name ? true : false}
                                                        onChange={this.inputChangeHandler}
                                                    />
                                                </div>
                                            </div>
                                            <div className="flex-50 pd-l-10">
                                                <div className="form-group">
                                                    <label className="form-group-label">Company Name :</label>
                                                    <input
                                                        type="text" id="companyName" className="form-control" required
                                                        value={this.state.payload.companyName}
                                                        onChange={this.inputChangeHandler}
                                                    />
                                                </div>
                                            </div>
                                            <div className="flex-50 pd-r-10">
                                                <div className="form-group">
                                                    <label className="form-group-label">Description :</label>
                                                    <textarea
                                                        rows="1"
                                                        id="description"
                                                        className="form-control"
                                                        value={this.state.payload.description}
                                                        onChange={this.inputChangeHandler}
                                                    />
                                                </div>
                                            </div>
                                            <div className="flex-25 pd-l-10">
                                                <label className="form-group-label">Tenant Type:</label>
                                                <div className="d-flex">
                                                    <div className="form-group flex-50">
                                                        <label className="radio-button">
                                                            <span className="radio-button-text">IFLEX</span>
                                                            <input
                                                                type="radio" name="tenantCategory"
                                                                disabled={this.props.match.params.id}
                                                                checked={this.state.payload.tenantCategory === "MAKER"}
                                                                onChange={this.inputChangeHandler} value="MAKER"
                                                            />
                                                            <span className="radio-button-mark"></span>
                                                        </label>
                                                    </div>
                                                    {/*<div className="form-group flex-50">
                                                <label className="radio-button form-group-label">Custom
                                                    <input
                                                        type="radio"
                                                        name="tenantCategory"
                                                        className="form-control"
                                                        disabled={this.props.match.params.id || true}
                                                        checked={this.state.payload.tenantCategory === "CUSTOM"}
                                                        onChange={this.inputChangeHandler}
                                                        value="CUSTOM" />
                                                    <span className="radio-button-mark"></span>
                                                </label>
                                            </div>*/}
                                                </div>
                                            </div>
                                            <div className="flex-25">
                                                <label className="form-group-label">Tenant Delete Protected :</label>
                                                <div className="d-flex">
                                                    <div className="toggle-switch-wrapper">
                                                        <label className="toggle-switch mr-l-0">
                                                            <input
                                                                type="checkbox"
                                                                id="defaultAccount"
                                                                className="form-control"
                                                                checked={this.state.payload.defaultAccount}
                                                                onChange={this.inputChangeHandler}
                                                            />
                                                            <span className="toggle-switch-slider"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="form-content-box">
                                    <div className="form-content-header">
                                        <p>Add Admin</p>
                                    </div>
                                    <div className="form-content-body">
                                        {this.state.payload.users.map((user, index) =>
                                            <div className="form-group add-tenant-box">
                                                <div className="d-flex">
                                                    <div className="flex-50 pd-r-10">
                                                        <div className="form-group">
                                                            <label className="form-group-label">First Name :</label>
                                                            <input
                                                                type="text" id="firstName" className="form-control" required
                                                                value={user.firstName} onChange={(e) => this.onAdminDetailsChangeHandler(e, index)}
                                                            />
                                                        </div>
                                                    </div>
                                                    <div className="flex-50 pd-l-10">
                                                        <div className="form-group">
                                                            <label className="form-group-label">Last Name :</label>
                                                            <input
                                                                type="text" id="lastName" className="form-control" required
                                                                value={user.lastName} onChange={(e) => this.onAdminDetailsChangeHandler(e, index)}
                                                            />
                                                        </div>
                                                    </div>
                                                    <div className="flex-50 pd-r-10">
                                                        <div className="form-group">
                                                            <label className="form-group-label">Email :</label>
                                                            <input
                                                                type="email" id="email" className="form-control" required
                                                                value={user.email} onChange={(e) => this.onAdminDetailsChangeHandler(e, index)}
                                                            />
                                                        </div>
                                                    </div>
                                                    <div className="flex-50 pd-l-10">
                                                        <div className="form-group">
                                                            <label className="form-group-label">Phone Number :</label>
                                                            <div className="phone-input">
                                                                <PhoneInput
                                                                    className="form-control"
                                                                    inputProps={{
                                                                        name: 'phone',
                                                                        required: true,
                                                                    }}
                                                                    placeholder="Mobile No."
                                                                    country={'us'}
                                                                    value={user.mobile}
                                                                    onChange={(value) => {
                                                                        let payload = cloneDeep(this.state.payload);
                                                                        payload.users[0].mobile = value;
                                                                        this.setState({
                                                                            payload
                                                                        });
                                                                    }}
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        )}
                                    </div>
                                </div>
                            </div>

                            <div className="form-info-wrapper">
                                <div className="form-info-header">
                                    <h5>Admin List</h5>
                                </div>
                                <div className="form-info-body form-info-body-adjust">
                                    <ul className="list-style-none form-info-list">
                                        {this.state.payload.users.map((admin, index) =>
                                            <li className="mb-3" key={index}>
                                                <h6 className="text-dark-theme">{admin.firstName || admin.lastName ? admin.firstName + " " + admin.lastName : "-"}</h6>
                                                <p><i className="fad fa-envelope mr-2 text-theme"></i>{admin.email || "-"}</p>
                                                <p><i className="fad fa-phone-alt mr-2 text-theme"></i>{admin.mobile || "-"}</p>
                                            </li>
                                        )}
                                    </ul>
                                </div>
                                <div className="form-info-footer">
                                    <button className="btn btn-light" onClick={() => this.props.history.push('/tenantList')}>Cancel</button>
                                    <button className="btn btn-primary" disabled={this.props.match.params.name ? true : false} onClick={this.onSubmitHandler} >Save</button>
                                </div>
                            </div>
                        </div>

                        {/*<div className="outerBox pb-0">
                            <form className="contentForm pd-0" onSubmit={this.onSubmitHandler}>
                                <div className="flex">
                                    <div className="flex-item fx-b40 contentFormDetail">
                                        <div className="contentFormHeading"><p>Details</p></div>

                                        <div className="flex">
                                            <div className="flex-item fx-b50 pd-r-7">
                                                <div className="form-group mr-b-10">
                                                    <input type="text" id="tenantName"
                                                        placeholder="TenantName" pattern="^[a-zA-Z]([\w\$-]*[a-zA-Z0-9])?$" autoFocus
                                                        className="form-control" value={this.state.payload.tenantName}
                                                        disabled={this.props.match.params.name ? true : false} onChange={this.inputChangeHandler} required />
                                                    <label className="form-group-label">Tenant Name :
                                                        <span className="requiredMark"><i className="fa fa-asterisk" /></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div className="flex-item fx-b50 pd-l-7">
                                                <div className="form-group mr-b-10">
                                                    <input type="text" id="companyName" placeholder="CompanyName" required className="form-control" value={this.state.payload.companyName} disabled={this.props.match.params.name ? true : false} onChange={this.inputChangeHandler} />
                                                    <label className="form-group-label">Company Name :
                                                        <span className="requiredMark"><i className="fa fa-asterisk" /></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div className="flex-item fx-b100">
                                                <div className="form-group mr-b-10">
                                                    <textarea rows="4" id="description" placeholder="Description" className="form-control" value={this.state.payload.description} onChange={this.inputChangeHandler} />
                                                    <label className="form-group-label">Description :</label>
                                                </div>
                                            </div>
                                            <div className="flex-item fx-b50 pd-l-7">
                                                <div className="form-group">
                                                    <label className={this.props.match.params.id ? "checkboxLabel checkboxDisabled" : "checkboxLabel"}>
                                                        <span className="checkText">Create default resources ?</span>
                                                        <input type="checkbox" id="defaultResources" disabled={this.props.match.params.id} checked={this.state.payload.defaultResources} onChange={this.inputChangeHandler} />
                                                        <span className="checkmark" />
                                                    </label>
                                                </div>
                                            </div>

                                             <div className="flex-item fx-b100">
                                                <div className="form-group flex mb-3">
                                                    <div className="flex-item fx-b40">
                                                        <label className={this.props.match.params.id ? "radioLabel radioDisabled" : "radioLabel"}>Enterprise
                                                            <input type="radio" name="type" disabled={this.props.match.params.id} checked={this.state.payload.type === "ENTERPRISE"} onChange={this.inputChangeHandler} value="ENTERPRISE" />
                                                            <span className="radioMark" />
                                                        </label>
                                                    </div>
                                                    <div className="flex-item fx-b40">
                                                        <label className={this.props.match.params.id ? "radioLabel radioDisabled" : "radioLabel"}>SaaS
                                                            <input type="radio" name="type" disabled={this.props.match.params.id} checked={this.state.payload.type === "MAKER"} onChange={this.inputChangeHandler} value="MAKER" />
                                                            <span className="radioMark" />
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="flex-item fx-b100">
                                                <div className="form-group mb-0">
                                                    <label className="form-label mr-r-5 f-13">Delete Protection</label>
                                                    <label className="toggleSwitch position-absolute mr-t-3">
                                                        <input type="checkbox" id="defaultAccount" checked={this.state.payload.defaultAccount} onChange={this.inputChangeHandler} />
                                                        <span className="toggleSwitchSlider"></span>
                                                    </label>
                                                </div>
                                            </div>

                                            <div className="flex-item fx-b100">
                                                <div className="form-group mr-b-10">
                                                    {this.state.OAuthProviders.map((category, index) => (
                                                        <ul className="versionControlList" key={index}>
                                                            <p>{category.displayName}:</p>
                                                            {category.providers.map(provider => (
                                                                <li key={provider.id}>
                                                                    <div className="versionControlItem">
                                                                        <div className="versionControlItemImage">
                                                                            <img src={`https://content.iot83.com/m83/tenant/${provider.imageUrl}`} />
                                                                        </div>
                                                                        {provider.displayName}
                                                                        <label className={provider.isDisabled ? "checkboxLabel checkboxDisabled" : "checkboxLabel"}>
                                                                            <input type="checkbox" name="provider" className={provider.isDisabled ? "btn-disabled " : ""} id={provider.id} disabled={provider.isDisabled} value={provider.id} checked={this.state.payload.oAuthProviders && this.state.payload.oAuthProviders.includes(provider.id)}
                                                                                onChange={() => {
                                                                                    this.inputChangeHandlerForProvider(event, provider.id, category.category)
                                                                                }} />
                                                                            <span className="checkmark" />
                                                                        </label>
                                                                    </div>
                                                                </li>
                                                            ))}
                                                        </ul>
                                                    ))}
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                     {this.state.isFetching ?
                                        <div className="formLoaderBox">
                                            <div className="loader"></div>
                                        </div> :
                                        <div className="flex-item fx-b35 contentFormDetail">
                                            <div className="contentFormHeading"><p>Packages</p></div>
                                            <div className="packageCard">
                                                <ul className="packageCardHeader listStyleNone flex">
                                                    <li></li>
                                                    {this.state.licenseDetails.sort((a, b) => a.numberOfDays - b.numberOfDays).map((license, index) =>
                                                        <li key={index}>
                                                            <h4>{license.type}</h4>
                                                            <p>{license.numberOfDays} days</p>
                                                            <label className={license.isDisabled ? "checkboxLabel checkboxDisabled" : "checkboxLabel"}>
                                                                <input type="checkbox" id={license.id} disabled={license.isDisabled} checked={license.id === this.state.selectedLicenseId} onChange={() => { this.licenseChangeHandler(index) }} />
                                                                <span className="checkmark" />
                                                            </label>
                                                        </li>
                                                    )}
                                                </ul>
                                                <div className="packageCardBody">
                                                    {this.state.licenseDetails.length > 0 && Object.keys(this.state.licenseDetails[0]).map((key, index) =>
                                                        (key !== 'id' && key !== 'type' && key !== 'isDisabled') && <div className="packageCardBodyInner flex" key={index}>
                                                            <h5>{this.getLicenseAttributesDisplayNames(key)}</h5>
                                                            {this.state.licenseDetails.sort((a, b) => a.numberOfDays - b.numberOfDays).map((license, index) => {
                                                                return (license.id === this.state.selectedLicenseId && this.state.selectedLicenseDetails && this.props.match.params.id) ?
                                                                    <div key={index} className="licenseInput">
                                                                        <input type="number" className="form-control" id={key} value={this.state.selectedLicenseDetails[key]} required onKeyDown={() => event.keyCode === 69 && event.preventDefault()} onChange={this.licenseModificationHandler} />
                                                                    </div>
                                                                    : <h5 key={index}>{license[key]}</h5>
                                                            })}
                                                        </div>
                                                    )}
                                                </div>
                                            </div>

                                            for SaaS projet

                                            <div className="packageCard saaSPackageCard mr-r-70 mr-l-70">
                                                <ul className="packageCardHeader listStyleNone flex">
                                                    <li className="flex-item fx-b50"></li>
                                                        <li className="flex-item fx-b50">
                                                            <h4>SaaS</h4>
                                                            <p>365 days</p>
                                                            <label className="checkboxLabel">
                                                                <input type="checkbox" id="SaaS"  />
                                                                <span className="checkmark" />
                                                            </label>
                                                        </li>
                                                </ul>
                                                <div className="packageCardBody">
                                                         <div className="packageCardBodyInner flex">
                                                            <h5 className="flex-item fx-b50">Data Connectors</h5>
                                                            <div className="licenseInput">
                                                                <input type="number" className="form-control" value="10"  />
                                                            </div>
                                                             <h5 className="flex-item fx-b50">10</h5>
                                                        </div>
                                                    <div className="packageCardBodyInner flex">
                                                        <h5 className="flex-item fx-b50">APIs</h5>
                                                        <div className="licenseInput">
                                                            <input type="number" className="form-control" value="10" />
                                                        </div>
                                                        <h5 className="flex-item fx-b50">10</h5>
                                                    </div>
                                                    <div className="packageCardBodyInner flex">
                                                        <h5 className="flex-item fx-b50">Roles</h5>
                                                        <div className="licenseInput">
                                                            <input type="number" className="form-control" value="10"  />
                                                        </div>
                                                        <h5 className="flex-item fx-b50">10</h5>
                                                    </div>
                                                    <div className="packageCardBodyInner flex">
                                                        <h5 className="flex-item fx-b50">Transformations</h5>
                                                        <div className="licenseInput">
                                                            <input type="number" className="form-control" value="10"  />
                                                        </div>
                                                        <h5 className="flex-item fx-b50">10</h5>
                                                    </div>
                                                    <div className="packageCardBodyInner flex">
                                                        <h5 className="flex-item fx-b50">JS Functions</h5>
                                                        <div className="licenseInput">
                                                            <input type="number" className="form-control" value="10"  />
                                                        </div>
                                                        <h5 className="flex-item fx-b50">10</h5>
                                                    </div>
                                                    <div className="packageCardBodyInner flex">
                                                        <h5 className="flex-item fx-b50">Pages</h5>
                                                        <div className="licenseInput">
                                                            <input type="number" className="form-control" value="10" />
                                                        </div>
                                                        <h5 className="flex-item fx-b50">10</h5>
                                                    </div>
                                                    <div className="packageCardBodyInner flex">
                                                        <h5 className="flex-item fx-b50">Policies</h5>
                                                        <div className="licenseInput">
                                                            <input type="number" className="form-control" value="10" />
                                                        </div>
                                                        <h5 className="flex-item fx-b50">10</h5>
                                                    </div>
                                                    <div className="packageCardBodyInner flex">
                                                        <h5 className="flex-item fx-b50">Code Blocks</h5>
                                                        <div className="licenseInput">
                                                            <input type="number" className="form-control" value="10" />
                                                        </div>
                                                        <h5 className="flex-item fx-b50">10</h5>
                                                    </div>
                                                    <div className="packageCardBodyInner flex">
                                                        <h5 className="flex-item fx-b50">Validity</h5>
                                                        <div className="licenseInput">
                                                            <input type="number" className="form-control" value="10" />
                                                        </div>
                                                        <h5 className="flex-item fx-b50">10</h5>
                                                    </div>
                                                    <div className="packageCardBodyInner flex">
                                                        <h5 className="flex-item fx-b50">Applications</h5>
                                                        <div className="licenseInput">
                                                            <input type="number" className="form-control" value="10" />
                                                        </div>
                                                        <h5 className="flex-item fx-b50">10</h5>
                                                    </div>
                                                    <div className="packageCardBodyInner flex">
                                                        <h5 className="flex-item fx-b50">Users</h5>
                                                        <div className="licenseInput">
                                                            <input type="number" className="form-control" value="10" />
                                                        </div>
                                                        <h5 className="flex-item fx-b50">10</h5>
                                                    </div>
                                                </div>
                                            </div>
                                         </div>
                                    }

                                    <div className="flex-item fx-b25 contentFormDetail">
                                        <div className="contentFormHeading"><p>Admins</p>
                                            {this.state.message &&
                                                <p className="contentFormError"><i className="fas fa-exclamation-triangle"></i>{this.state.message}</p>
                                            }
                                            <button type="button" className="btn btn-link" onClick={this.addUserHandler}>
                                                <i className="far fa-plus mr-r-5"></i>Add Admin
                                            </button>
                                        </div>

                                        <div className="paramBox">
                                            {this.state.payload.users != 0 ?
                                                <ul className="paramList">
                                                    {this.state.payload.users.map((user, index) => {
                                                        return (
                                                            <li key={index}>
                                                                <div className="paramCard">
                                                                    <h5 className="text-primary">{user.firstName} {user.lastName}</h5>
                                                                    <p><strong>Email :</strong>{user.email}</p>
                                                                    <p><strong>Phone Number :</strong>{user.mobile}</p>
                                                                    <button type="button" className="btn  btn-transparent btn-transparent-primary" id={user.id} onClick={(event) => this.editUserHandler(event, index)} ><i id={user.id} className="fas fa-pencil"></i></button>
                                                                    <button type="button" className="btn btn-transparent btn-transparent-danger" onClick={() => this.deleteUser(index)}>
                                                                        <i className="fas fa-times"></i>
                                                                    </button>
                                                                </div>
                                                            </li>)
                                                    })}
                                                </ul> :
                                                <div className="contentTableEmpty">
                                                    <h6>No Admins added yet.</h6>
                                                </div>
                                            }
                                        </div>
                                    </div>
                                </div>

                                <div className="bottomButton">
                                    <button type="submit" className="btn btn-success" >{this.props.match.params.id ? 'Update' : 'Save'}</button>
                                    <button type="button" className="btn btn-dark" onClick={() => {
                                        this.props.history.push("/tenantList")
                                    }}>Cancel</button>
                                </div>
                            </form>

                        </div>*/}

                    </React.Fragment>
                }

                {this.state.isOpen &&
                    <NotificationModal
                        type={this.state.modalType}
                        message2={this.state.message2}
                        onCloseHandler={this.onCloseHandler}
                    />
                }

                {/* add new tenant */}
                {/*<SlidingPane
                    className=''
                    overlayClassName='slidingFormOverlay'
                    closeIcon={<div></div>}
                    isOpen={this.state.isUserForm}
                    from='right'
                    width='550px'
                >
                    {this.state.isUserForm &&
                        <div className="modal-content">
                            <div className="modal-header">
                                <h6 className="modal-title">{typeof (this.state.editableAdminId) === "number" ? "Edit" : "Add New"} Admin
                                    <button className="close" onClick={() => this.setState({ isUserForm: false })}><i className="far fa-angle-right"></i></button>
                                </h6>
                            </div>
                          
                            <form onSubmit={this.saveUser}>
                                <div className="modal-body">
                                    <div className="form-group">
                                        <input type="text" id="firstName" placeholder="FirstName" className="form-control" required autoFocus value={this.state.user.firstName} onChange={this.onAdminDetailsChangeHandler}/>
                                        <label className="form-group-label">First Name :
                                            <span className="requiredMark"><i className="fa fa-asterisk" /></span>
                                        </label>
                                    </div>

                                    <div className="form-group">
                                        <input type="text" id="lastName" placeholder="LastName" className="form-control" required value={this.state.user.lastName} onChange={this.onAdminDetailsChangeHandler}/>
                                        <label className="form-group-label">Last Name :
                                            <span className="requiredMark"><i className="fa fa-asterisk" /></span>
                                        </label>
                                    </div>

                                    <div className="form-group">
                                        <input type="email" id="email" placeholder="Email" className="form-control" required value={this.state.user.email} onChange={this.onAdminDetailsChangeHandler}/>
                                        <label className="form-group-label">Email :
                                            <span className="requiredMark"><i className="fa fa-asterisk" /></span>
                                        </label>
                                    </div>

                                    <div className="form-group">
                                        <input type="text" maxLength="10" id="mobile" placeholder="Phone Number" className="form-control" required value={this.state.user.mobile} onChange={this.onAdminDetailsChangeHandler}/>
                                        <label className="form-group-label">Phone Number :
                                            <span className="requiredMark"><i className="fa fa-asterisk" /></span>
                                        </label>
                                    </div>
                                </div>

                                <div className="modal-footer">
                                    <button className="btn btn-success" >{typeof (this.state.editableAdminId) === "number" ? "Update" : "Add"}</button>
                                    <button type="button" className="btn btn-dark" onClick={() => this.setState({ isUserForm: false })}>Cancel</button>
                                </div>
                            </form>
                        </div>
                    }
                </SlidingPane>*/}
                {/* end add new tenant */}
            </React.Fragment>
        );
    }
}

AddOrEditTenant.propTypes = {
    dispatch: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
    submitFormSuccess: SELECTORS.getSubmitFormSuccess(),
    submitFormSuccessMessage: SELECTORS.getSubmitFormSuccessMessage(),
    submitFormFailure: SELECTORS.getSubmitFormFailure(),
    isFetching: SELECTORS.getIsFetching(),
    tenantDetails: SELECTORS.getTenantDetailsSuccess(),
    tenantDetailsError: SELECTORS.getTenantDetailsError(),
    licenseList: SELECTORS.getLicenseDetails(),
    licenseError: SELECTORS.getLicenseFailure(),
    accountList: SELECTORS.getAccountListSuccess(),
    accountListError: SELECTORS.getAccountListError(),
    OAuthProviders: SELECTORS.getOAuthProvidersList(),
    OAuthProvidersError: SELECTORS.getOAuthProvidersError(),
});

function mapDispatchToProps(dispatch) {
    return {
        dispatch,
        onSubmit: payload => dispatch(ACTIONS.onSubmitHandler(payload)),
        fetchTenantDetails: id => dispatch(ACTIONS.fetchTenantDetails(id)),
        fetchLicenseInfo: () => dispatch(ACTIONS.fetchLicenseInfo()),
        getAccountList: () => dispatch(ACTIONS.getAccountList()),
        getOAuthProviders: () => dispatch(ACTIONS.getOAuthProviders()),
    };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "addOrEditTenant", reducer });
const withSaga = injectSaga({ key: "addOrEditTenant", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(AddOrEditTenant);
