/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
 * Direct selector to the addOrEditTenant state domain
 */

const selectAdminAddOrEditClientDomain = state =>
    state.get("addOrEditTenant", initialState);


export const getSubmitFormSuccess = () => createSelector(selectAdminAddOrEditClientDomain, subState => subState.submitFormSuccess);

export const getSubmitFormSuccessMessage = () => createSelector(selectAdminAddOrEditClientDomain, subState => subState.submitFormSuccessMessage);

export const getSubmitFormFailure = () => createSelector(selectAdminAddOrEditClientDomain, subState => subState.submitFormFailure);


export const getLicenseDetails = () => createSelector(selectAdminAddOrEditClientDomain, substate => substate.licenseDetails);

export const getLicenseFailure = () => createSelector(selectAdminAddOrEditClientDomain, substate => substate.licenseFailure);

export const getTenantDetailsSuccess = () => createSelector(selectAdminAddOrEditClientDomain, substate => substate.tenantDetails);

export const getTenantDetailsError = () => createSelector(selectAdminAddOrEditClientDomain, substate => substate.tenantDetailsError);

export const getAccountListSuccess = () => createSelector(selectAdminAddOrEditClientDomain, subState => subState.accountList);

export const getAccountListError = () => createSelector(selectAdminAddOrEditClientDomain, subState => subState.accountListError);

export const getOAuthProvidersList = () => createSelector(selectAdminAddOrEditClientDomain, subState => subState.OAuthProviders);

export const getOAuthProvidersError = () => createSelector(selectAdminAddOrEditClientDomain, subState => subState.OAuthProvidersError);

export const isFetchingState = state => state.get('loader');

export const getIsFetching = () =>
    createSelector(isFetchingState, fetchingState => fetchingState.get('isFetching'));

