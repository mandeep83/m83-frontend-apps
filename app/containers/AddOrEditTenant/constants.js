/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

export const DEFAULT_ACTION = "app/AddOrEditTenant/DEFAULT_ACTION";

export const SUBMIT_FORM = "app/AddOrEditTenant/SUBMIT_FORM";
export const SUBMIT_FORM_FAILURE = "app/AddOrEditTenant/SUBMIT_FORM_FAILURE";
export const SUBMIT_FORM_SUCCESS = "app/AddOrEditTenant/SUBMIT_FORM_SUCCESS";

export const IMAGE_UPLOAD_REQUEST = "app/AddOrEditTenant/IMAGE_UPLOAD_REQUEST";
export const IMAGE_UPLOAD_SUCCESS = "app/AddOrEditTenant/IMAGE_UPLOAD_SUCCESS";
export const IMAGE_UPLOAD_FAILURE = "app/AddOrEditTenant/IMAGE_UPLOAD_FAILURE";

export const FETCH_TENANT_DETAILS = "app/AddOrEditTenant/FETCH_TENANT_DETAILS";
export const FETCH_TENANT_DETAILS_SUCCESS = "app/AddOrEditTenant/FETCH_TENANT_DETAILS_SUCCESS";
export const FETCH_TENANT_DETAILS_FAILURE = "app/AddOrEditTenant/FETCH_TENANT_DETAILS_FAILURE";

export const FETCH_LICENSE_DETAILS = "app/AddOrEditTenant/FETCH_LICENSE_DETAILS";
export const FETCH_LICENSE_DETAILS_SUCCESS = "app/AddOrEditTenant/FETCH_LICENSE_DETAILS_SUCCESS";
export const FETCH_LICENSE_DETAILS_FAILURE = "app/AddOrEditTenant/FETCH_LICENSE_DETAILS_FAILURE";

export const GET_ACCOUNT_LIST = 'app/AddOrEditTenant/GET_ACCOUNT_LIST';
export const GET_ACCOUNT_LIST_SUCCESS = 'app/AddOrEditTenant/GET_ACCOUNT_LIST_SUCCESS';
export const GET_ACCOUNT_LIST_FAILURE = 'app/AddOrEditTenant/GET_ACCOUNT_LIST_FAILURE';

export const GET_OAUTH_PROVIDERS = 'app/AddOrEditTenant/GET_OAUTH_PROVIDERS';
export const GET_OAUTH_PROVIDERS_SUCCESS = 'app/AddOrEditTenant/GET_OAUTH_PROVIDERS_SUCCESS';
export const GET_OAUTH_PROVIDERS_FAILURE = 'app/AddOrEditTenant/GET_OAUTH_PROVIDERS_FAILURE';
