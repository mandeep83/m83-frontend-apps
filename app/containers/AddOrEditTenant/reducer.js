/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { fromJS } from "immutable";
import * as CONSTANTS from "./constants";

export const initialState = fromJS({});

function adminAddOrEditClientReducer(state = initialState, action) {
  switch (action.type) {
    case CONSTANTS.DEFAULT_ACTION:
      return state;
    case CONSTANTS.SUBMIT_FORM_SUCCESS:
      return Object.assign({}, state, {
        submitFormSuccess: action.response
      });
    case CONSTANTS.SUBMIT_FORM_FAILURE:
      return Object.assign({}, state, {
        submitFormFailure: action.error,
      });
    case CONSTANTS.FETCH_LICENSE_DETAILS_SUCCESS:
      return Object.assign({}, state, {
        licenseDetails: action.response
      });
    case CONSTANTS.FETCH_LICENSE_DETAILS_FAILURE:
      return Object.assign({}, state, {
        licenseFailure: action.error
      });
    case CONSTANTS.FETCH_TENANT_DETAILS_SUCCESS:
      return Object.assign({}, state, {
        tenantDetails: action.response
      });
    case CONSTANTS.FETCH_TENANT_DETAILS_FAILURE:
      return Object.assign({}, state, {
        tenantDetailsError: action.error
      });
    case CONSTANTS.GET_ACCOUNT_LIST_SUCCESS:
      return Object.assign({}, state, {
        accountList: action.response,
      });
    case CONSTANTS.GET_ACCOUNT_LIST_FAILURE:
      return Object.assign({}, state, {
        error: action.error
      });
    case CONSTANTS.GET_OAUTH_PROVIDERS_SUCCESS:
      return Object.assign({}, state, {
        OAuthProviders: action.response,
      });
    case CONSTANTS.GET_OAUTH_PROVIDERS_FAILURE:
      return Object.assign({}, state, {
        OAuthProvidersError: action.error
      });
    default:
      return state;
  }
}

export default adminAddOrEditClientReducer;
