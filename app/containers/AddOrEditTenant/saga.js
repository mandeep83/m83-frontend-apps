/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { takeEvery } from 'redux-saga';
import * as CONSTANTS from "./constants";

import { apiCallHandler } from '../../api';

export function* addOrEditAccountApiHandler(action) {
    yield [apiCallHandler(action, CONSTANTS.SUBMIT_FORM_SUCCESS, CONSTANTS.SUBMIT_FORM_FAILURE, 'saveAccountDetails')];
}

export function* getLicenseApiHandler(action) {
    yield [apiCallHandler(action, CONSTANTS.FETCH_LICENSE_DETAILS_SUCCESS, CONSTANTS.FETCH_LICENSE_DETAILS_FAILURE, 'fetchLicenseDetails')];
}

export function* tenantDetailsApiHandlerAsync(action) {
    yield [apiCallHandler(action, CONSTANTS.FETCH_TENANT_DETAILS_SUCCESS, CONSTANTS.FETCH_TENANT_DETAILS_FAILURE, 'getTenantDetailsById')];
}

export function* apiGetAccountListHandlerAsync(action) {
    yield[apiCallHandler(action, CONSTANTS.GET_ACCOUNT_LIST_SUCCESS, CONSTANTS.GET_ACCOUNT_LIST_FAILURE, 'getAccountList')];
}

export function* apiGetOAuthProvidersAsync(action) {
    yield[apiCallHandler(action, CONSTANTS.GET_OAUTH_PROVIDERS_SUCCESS, CONSTANTS.GET_OAUTH_PROVIDERS_FAILURE, 'getOAuthProvidersForSuperAdmin')];
}

export function* watcherTenantDetailsRequest() {
    yield takeEvery(CONSTANTS.FETCH_TENANT_DETAILS, tenantDetailsApiHandlerAsync);
}

export function* watcherAddOrEditAccountRequest() {
    yield takeEvery(CONSTANTS.SUBMIT_FORM, addOrEditAccountApiHandler);
}

export function* watcherFetchLicenseRequest() {
    yield takeEvery(CONSTANTS.FETCH_LICENSE_DETAILS, getLicenseApiHandler);
}

export function* watcherGetAccountList() {
    yield takeEvery(CONSTANTS.GET_ACCOUNT_LIST, apiGetAccountListHandlerAsync);
}

export function* watcherGetOAuthProviders() {
    yield takeEvery(CONSTANTS.GET_OAUTH_PROVIDERS, apiGetOAuthProvidersAsync);
}

export default function* rootSaga() {
    yield [
        watcherTenantDetailsRequest(),
        watcherAddOrEditAccountRequest(),
        watcherFetchLicenseRequest(),
        watcherGetAccountList(),
        watcherGetOAuthProviders(),
    ]
}

