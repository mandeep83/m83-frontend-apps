/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * PrivacyPolicy
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { FormattedMessage } from "react-intl";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import makeSelectPrivacyPolicy from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import messages from "./messages";

/* eslint-disable react/prefer-stateless-function */
export class PrivacyPolicy extends React.Component {
    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>Privacy Policy</title>
                    <meta name="description" content="Description of Privacy Policy" />
                </Helmet>

                <div className="content-wrapper">
                    <div className="content-item pl-0 pb-0">
                        <header className="content-header border-bottom d-flex" style={{ left: 0 }}>
                            <div className="flex-60">
                                <h6 className="text-gray">Privacy Policy for the IoT83 - <span className="text-primary">The {localStorage.tenantType !="MAKER" ? "Flex83" : "iFlex83"} Platform</span></h6>
                            </div>
                        </header>

                        <div className="content-body">
                            {localStorage.tenantType == "MAKER" ?
                                <div className="content-text-box">
                                    <ul>
                                        <div className="content-text-bg"><img src="https://content.iot83.com/m83/misc/m83-logo.png" /></div>
                                        <li>
                                            <strong>IoT83 takes management of Licensee data seriously</strong> and we demonstrate that with the care we take with Licensee Data. We purposefully only use Licensee Data as needed to provide Licensee with the iFlex83 Services (Services) and operation of the Services Licensee subscribes to.
                                        </li>

                                        <li><strong>This document is part of IoT83’s Terms and Conditions</strong> for the iFlex83 Platform, and as a direct Licensee or subscriber of that Service, or as an indirect consumer of that service, using the Services as delivered to you by a direct Licensee of that Service (that is a “subscriber” of an application created by a direct subscriber to iFlex83 Service), by using the Service, you agree to the iFlex83 SaaS Platform Terms and Conditions, and this Privacy Policy. By using the Services, you consent to the collection of Your information, and its transfer, processing, or other uses of the data as defined in this Privacy Policy.</li>

                                        <li>
                                            <h6>Licensee Data Collected</h6>
                                            <ul>
                                                <li>
                                                    IoT83 will collect Licensee names, addresses, telephone numbers, business names, email addresses, log-in credentials, information about tiers of Service selected, transactions made with us, customer service and support records, and any other information Licensees provide to IoT83 for the purposes of operating and managing the Services.
                                                </li>
                                                <li>
                                                    It is possible to use the Services in an anonymous fashion by not providing certain information when signing up for Services, Using Paypal (or the like) for payment, etc. However, IoT83 does cannot guarantee or assure that the entirety of Services will operate fully when some Licensee information is withheld.
                                                </li>
                                            </ul>
                                        </li>

                                        <li>
                                            <h6>Sharing Data with Third Parties</h6>
                                            <ul>
                                                <li>
                                                    Billing processing third party services, as selected by you in the sign-up and subscription management process, will have access to the billing information that you provide, solely to provide billing services as selected by you. IoT83 does not retain any Licensee credit card or billing information for subscribed services but may retain billing transaction information for enterprise or corporate accounts as agreed to by the parties.
                                                </li>
                                                <li>
                                                    Third party marketing providers may have access to Your email address solely for promoting IoT83 and Method83 programs. Licensees can opt-out of these programs.
                                                </li>
                                                <li>
                                                    In the delivery of Services, IoT83 may forward Licensee names and passwords securely to certain cloud hosting business partners of IoT83 in the course of delivering to Licensees certain Service elements and functionality. All such data transfers are done using secure transfer technologies.
                                                </li>
                                                <li>
                                                    Should IoT83 become part of a sale, change of control, or merger or other significant form of business transformation, you agree that IoT83 may transfer Licensee Data to that entity as the new provider of the Services. In the event of such a transfer the Services Terms and Conditions and Privacy Policy continues to apply until such time as such policies may be amended or updated with associated notification to Licensees.
                                                </li>
                                                <li>
                                                    IoT83 may integrate the Services with social media platforms. In this case, Licensees can interact with upon their discretion and volition, but IoT83 is not responsible for the any treatment of such data once transferred to such third party platforms, and Licensees will not hold IoT83 responsible or liable for any consequence, actions, or data privacy issues arising from using these third party services.
                                                </li>
                                            </ul>
                                        </li>

                                        <li>
                                            <h6>Operational Use of Licensee Data</h6>
                                            <ul>
                                                <li>
                                                    In the operation of the Services certain Licensee data is associated with operational procedures as outlined in this section of the Data Privacy Policy:
                                                </li>
                                                <li>
                                                    Logs and reports may be created to monitor user access to the Services and Licensee access to Service elements. Such logs will map utilization to user names and / or account names and may include other service identifiers such as IP addresses, devices used to connect, MAC addresses, device ID numbers, errors detected, browser types and settings or other information that may be necessary to optimize the Services provided and to provide diagnostic information to optimize the Services.
                                                </li>
                                                <li>
                                                    In application creation, Licensees may elect to provide location information data for themselves, or for Licensee devices. This data is used to enrich their applications, so this data is retained by the Service to enable this enrichment.
                                                </li>
                                                <li>
                                                    Cookies or other access tracking mechanisms may be used to optimize delivery and operation of the Services. Licensees should note that Licensees can delete cookies and still use the Services, however this may disable some aspect of usability and the functionality of some features.
                                                </li>
                                                <li>
                                                    Certain SDKs deployed for device connectivity to the Service, may include identifiers to tag, classify, or locate devices in order to optimize Service operations. As such, SDK tag related data may be retained.
                                                </li>
                                                <li>
                                                    Personal information is only used by the Services to provide Licensees with Service features and capabilities or to communicate with Licensees regarding these Services.
                                                </li>
                                                <div className="content-text-bg"><img src="https://content.iot83.com/m83/misc/m83-logo.png" /></div>
                                                <li>
                                                    Licensee Data or Licensee Applications are considered Licensee property and are only available to third parties as expressly approved by Licensees. Licensee personal information is only shared with Licensee consent unless such sharing of Licensee data is necessary to perform a Licensee request, it is legally required, it is necessary to execute the Terms of Service, IoT83 is using the data to detect fraud, bypass or compromise security of the Services, or other illegal activities, or to otherwise protect the rights of other Licensees, or IoT83 legal rights or IoT83 property.
                                                </li>
                                            </ul>
                                        </li>

                                        <li>
                                            <h6>Licensee Data and Law Enforcement</h6>
                                            <ul>
                                                <li>IoT83 may disclose Licensee information if: (i) required by law or; (i) if believed to be required to in order to protect the personal safety of Licensees, the Services, or the public; or, (iii) to resolve fraud, technical issues, or security; to protect the rights and property of IoT83; or (iv) to comply with law enforcement legal requirements.</li>
                                            </ul>
                                        </li>

                                        <li>
                                            <h6>European Union Data Compliance</h6>
                                            <ul>
                                                <li>All Licensees are provided with the following General Data Protection Regulation (GDPR) rights, for any Personal Data where IoT83 acts as a Service Provider:
                                                    <ul>
                                                        <li>Upon request to IoT83, IoT83 will provide you a record of any Personal Data we are processing and any Personal Data that has been collected.</li>
                                                        <li>Upon request to IoT83, IoT83 will correct any Personal Data that is in error or in need of correction.</li>
                                                        <li>Upon request to IoT83, IoT83 will delete any Personal Data being used in the operation of the iFlex83 Services according to Your request, although note as before, that IoT83 cannot guarantee correct operation of all Services when certain account data is missing. Personal Data deletion can also be accomplished using the Delete Account function (or similarly named function) in the “Manage Service” function of the services.</li>
                                                        <li>Upon request to IoT83, Licensees may ask that IoT83 restrict or block processing of Personal Data, noting that this may impact some aspects of the delivery of the Services.</li>
                                                        <li>Upon request to IoT83, Licensees may receive a portable record of the data that IoT83 has collected for a Licensee.</li>
                                                        <li>Licensees may complain or issue concerns as to the processing of their Personal Information or report any such concerns to data protection authorities assigned to hear such concerns.</li>
                                                        <li>Licensees may withdraw consent for IoT83 to process Your Personal Data, but will not affect IoT83’s rights to processing of Personal Information retroactively, or establish a requirement for data removal from historical archives.</li>
                                                        <li>Any such request as outlined above can be addressed  to <a href="mailto:support@iflex83.com">support@iflex83.com,</a> or the support URL indicated in the Support sections of the Services.</li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>

                                        <li>
                                            <h6>Rights to Change Privacy Policy</h6>
                                            <ul>
                                                <li>IoT83 reserves the right to update or change this Privacy Policy at any time. If IoT83 makes material changes to the Privacy Policy IoT83 will notify Licensees by posting an announcement on the Service, or by providing Licensees with a notification email. Licensees are bound by the Privacy Policy of the updated policy or terms after such changes have been posted or such notification has been sent. The use of any Personal Information is subject to the Privacy Policy in place at the time such information was received by IoT83.</li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div> :
                                <div className="content-text-box">
                                    <ul>
                                        <div className="content-text-bg"><img src="https://content.iot83.com/m83/misc/m83-logo.png" /></div>
                                        <li>
                                            <strong>IoT83 takes management of Licensee data seriously </strong> and we demonstrate that with the care we take with Licensee Data. We purposefully only use Licensee Data as needed to provide Licensee with the Flex83 Services (Services) and operation of the Services Licensee subscribes to.
                                        </li>

                                        <li><strong>This document is part of IoT83’s Terms and Conditions</strong> for the Flex83 Platform, and as a Licensee or subscriber of that Service, or as an indirect consumer of that service, using the Services as delivered to you by a direct Licensee of that Service (that is a “subscriber” of an application created by a direct subscriber to Flex83 Service), by using the Service, you agree to the Flex83 SaaS Platform Terms and Conditions, and this Privacy Policy. By using the Services, you consent to the collection of Your information, and its transfer, processing, or other uses of the data as defined in this Privacy Policy.</li>

                                        <li>
                                            <h6>Licensee Data Collected</h6>
                                            <ul>
                                                <li>
                                                    IoT83 will collect Licensee names, addresses, telephone numbers, business names, email addresses, log-in credentials, information about tiers of Service selected, transactions made with us, customer service and support records, and any other information Licensees provide to IoT83 for the purposes of operating and managing the Services.
                                                </li>
                                                <li>
                                                    It is possible to use the Services in an anonymous fashion by not providing certain information when signing up for Services, Using Paypal (or the like) for payment, etc. However, IoT83 does cannot guarantee or assure that the entirety of Services will operate fully when some Licensee information is withheld.
                                                </li>
                                            </ul>
                                        </li>

                                        <li>
                                            <h6>Sharing Data with Third Parties</h6>
                                            <ul>
                                                <li>
                                                    Billing processing third party services, as selected by you in the sign-up and subscription management process, will have access to the billing information that you provide, solely to provide billing services as selected by you. IoT83 does not retain any Licensee credit card or billing information for subscribed services but may retain billing transaction information for enterprise or corporate accounts as agreed to by the parties.
                                                </li>
                                                <li>
                                                    Third party marketing providers may have access to Your email address solely for promoting IoT83 and Method83 programs. Licensees can opt-out of these programs.
                                                </li>
                                                <li>
                                                    In the delivery of Services, Iot83 may forward Licensee names and passwords securely to certain cloud hosting business partners of IoT83 in the course of delivering to Licensees certain Service elements and functionality. All such data transfers are done using secure transfer technologies.
                                                </li>
                                                <li>
                                                    Should IoT83 become part of a sale, change of control, or merger or other significant form of business transformation, you agree that IoT83 may transfer Licensee Data to that entity as the new provider of the Services. In the event of such a transfer the Services Terms and Conditions and Privacy Policy continues to apply until such time as such policies may be amended or updated with associated notification to Licensees.
                                                </li>
                                                <li>
                                                    IoT83 may integrate the Services with social media platforms. In this case, Licensees can interact with upon their discretion and volition, but IoT83 is not responsible for the any treatment of such data once transferred to such third party platforms, and Licensees will not hold IoT83 responsible or liable for any consequence, actions, or data privacy issues arising from using these third party services.
                                                </li>
                                            </ul>
                                        </li>

                                        <li>
                                            <h6>Operational Use of Licensee Data</h6>
                                            <ul>
                                                <li>
                                                    In the operation of the Services certain Licensee data is associated with operational procedures as outlined in this section of the Data Privacy Policy:
                                                </li>
                                                <li>
                                                    Logs and reports may be created to monitor user access to the Services and Licensee access to Service elements. Such logs will map utilization to user names and / or account names and may include other service identifiers such as IP addresses, devices used to connect, MAC addresses, device ID numbers, errors detected, browser types and settings or other information that may be necessary to optimize the Services provided and to provide diagnostic information to optimize the Services.
                                                </li>
                                                <li>
                                                    In application creation, Licensees may elect to provide location information data for themselves, or for Licensee devices. This data is used to enrich their applications, so this data is retained by the Service to enable this enrichment.
                                                </li>
                                                <li>
                                                    Cookies or other access tracking mechanisms may be used to optimize delivery and operation of the Services. Licensees should note that Licensees can delete cookies and still use the Services, however this may disable some aspect of usability and the functionality of some features.
                                                </li>
                                                <li>
                                                    Certain SDKs deployed for device connectivity to the Service, may include identifiers to tag, classify, or locate devices in order to optimize Service operations. As such, SDK tag related data may be retained.
                                                </li>
                                                <li>
                                                    Personal information is only used by the Services to provide Licensees with Service features and capabilities or to communicate with Licensees regarding these Services.
                                                </li>
                                                <div className="content-text-bg"><img src="https://content.iot83.com/m83/misc/m83-logo.png" /></div>
                                                <li>
                                                    Licensee Data or Licensee Applications are considered Licensee property and are only available to third parties as expressly approved by Licensees. Licensee personal information is only shared with Licensee consent unless such sharing of Licensee data is necessary to perform a Licensee request, it is legally required, it is necessary to execute the Terms of Service, IoT83 is using the data to detect fraud, bypass or compromise security of the Services, or other illegal activities, or to otherwise protect the rights of other Licensees, or IoT83 legal rights or IoT83 property.
                                                </li>
                                            </ul>
                                        </li>

                                        <li>
                                            <h6>Licensee Data and Law Enforcement</h6>
                                            <ul>
                                                <li>IoT83 may disclose Licensee information if: (i) required by law or; (i) if believed to be required to in order to protect the personal safety of Licensees, the Services, or the public; or, (iii) to resolve fraud, technical issues, or security; to protect the rights and property of IoT83; or (iv) to comply with law enforcement legal requirements.</li>
                                            </ul>
                                        </li>

                                        <li>
                                            <h6>European Union Data Compliance</h6>
                                            <ul>
                                                <li>All Licensees are provided with the following General Data Protection Regulation (GDPR) rights, for any Personal Data where IoT83 acts as a Service Provider:
                                                    <ul>
                                                        <li>Upon request to IoT83, IoT83 will provide you a record of any Personal Data we are processing and any Personal Data that has been collected.</li>
                                                        <li>Upon request to IoT83, IoT83 will correct any Personal Data that is in error or in need of correction.</li>
                                                        <li>Upon request to IoT83, IoT83 will delete any Personal Data being used in the operation of the Flex83 Services according to Your request, although note as before, that IoT83 cannot guarantee correct operation of all Services when certain account data is missing. Personal Data deletion can also be accomplished using the Delete Account function (or similarly named function) in the “Manage Service” function of the services.</li>
                                                        <li>Upon request to IoT83, Licensees may ask that IoT83 restrict or block processing of Personal Data, noting that this may impact some aspects of the delivery of the Services.</li>
                                                        <li>Upon request to IoT83, Licensees may receive a portable record of the data that IoT83 has collected for a Licensee.</li>
                                                        <li>Licensees may withdraw consent for IoT83 to process Your Personal Data, but will not affect IoT83’s rights to processing of Personal Information retroactively, or establish a requirement for data removal from historical archives.</li>
                                                        <li>Licensees may complain or issue concerns as to the processing of their Personal Information or report any such concerns to data protection authorities assigned to hear such concerns.</li>
                                                        <li>Any such request as outlined above can be addressed to <a href="mailto:support@flex83.com">support@flex83.com,</a> or the support URL indicated in the Support sections of the Services.</li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>

                                        <li>
                                            <h6>Rights to Change Privacy Policy</h6>
                                            <ul>
                                                <li>IoT83 reserves the right to update or change this Privacy Policy at any time. If IoT83 makes material changes to the Privacy PolicyIoT83 will notify Licensees by posting an announcement on the Service, or by providing Licensees with a notification email. Licensees are bound by the Privacy Policy of the updated policy or terms after such changes have been posted or such notification has been sent. The use of any Personal Information is subject to the Privacy Policy in place at the time such information was received by IoT83.</li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            }
                        </div>
                    </div>
                </div>

            </React.Fragment>
        );
    }
}

PrivacyPolicy.propTypes = {
    dispatch: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
    privacypolicy: makeSelectPrivacyPolicy()
});

function mapDispatchToProps(dispatch) {
    return {
        dispatch
    };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "privacyPolicy", reducer });
const withSaga = injectSaga({ key: "privacyPolicy", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(PrivacyPolicy);
