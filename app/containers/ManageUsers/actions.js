/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import * as CONSTANTS from './constants';

export function defaultAction() {
  return {
    type: CONSTANTS.DEFAULT_ACTION,
  };
}

export function getManageUsersList(payload) {
  return {
    type: CONSTANTS.GET_USERS_LIST,
    payload
  };
}

export function getDeveloperQuota(payload) {
  return {
    type: CONSTANTS.GET_DEVELOPER_QUOTA,
    payload
  };
}

export function deleteUser(id) {
  return {
    type: CONSTANTS.DELETE_USER_REQUEST,
    id,
  };
}

export function fetchRoles() {
  return {
    type: CONSTANTS.GET_ROLES,
  };
}

export function fetchGroups() {
  return {
    type: CONSTANTS.GET_GROUPS,
  };
}

export function fetchUserDetails(id) {
  return {
    type: CONSTANTS.FETCH_USER_DETAILS,
    id,
  };
}

export function resetToInitialState() {
  return {
    type: CONSTANTS.RESET_TO_INITIAL_STATE,
  };
}

export function createOrEditUser(payload) {
  return {
    type: CONSTANTS.CREATE_OR_EDIT_USER,
    payload,
  };
}

export function getOwnedProjects() {
  return {
    type: CONSTANTS.GET_OWNED_PROJECTS,
  };
}

export function resendEmailInvite(id) {
  return {
    type: CONSTANTS.RESEND_EMAIL_INVITE,
    id
  };
}

export function promoteAsOwner(id) {
  return {
    type: CONSTANTS.PROMOTE_AS_OWNER,
    id
  };
}