/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { takeLatest } from 'redux-saga';
import { put } from 'redux-saga/effects';
import * as CONSTANTS from './constants';
import {apiCallHandler} from '../../api';

export function* getUsersListApiHandlerAsync(action) {
  yield[apiCallHandler(action, CONSTANTS.GOT_USERS_LIST, CONSTANTS.NOT_GOT_USERS_LIST, 'fetchUsersWithPagination')];
}
export function* deleteUserApiHandlerAsync(action) {
  yield[apiCallHandler(action, CONSTANTS.DELETE_USER_SUCCESS, CONSTANTS.DELETE_USER_FAILURE, 'deleteUser')];
}

export function* watcherGetUserListRequest() {
  yield takeLatest(CONSTANTS.GET_USERS_LIST, getUsersListApiHandlerAsync);
}

export function* watcherDeleteUserRequest() {
  yield takeLatest(CONSTANTS.DELETE_USER_REQUEST, deleteUserApiHandlerAsync);
}

export function* apiFetchRolesHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_ROLES_SUCCESS, CONSTANTS.GET_ROLES_FAILURE, 'getRoleList')];
}
export function* apiFetchGroupsHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_GROUPS_SUCCESS, CONSTANTS.GET_GROUPS_FAILURE, 'getGroupList')];
}

export function* apiGetOwnedProjectsAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_OWNED_PROJECTS_SUCCESS, CONSTANTS.GET_OWNED_PROJECTS_FAILURE, 'fetchProjects')];
}

export function* apiGetDeveloperQuota(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_DEVELOPER_QUOTA_SUCCESS, CONSTANTS.GET_DEVELOPER_QUOTA_FAILURE, 'getDeveloperQuota')];
}

export function* watcherFetchGroupsRequests() {
  yield takeLatest(CONSTANTS.GET_GROUPS, apiFetchGroupsHandlerAsync);
}

export function* watcherFetchRolesRequests() {
  yield takeLatest(CONSTANTS.GET_ROLES, apiFetchRolesHandlerAsync);
}

export function* apiFetchUserDetailsHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.FETCH_USER_DETAILS_SUCCESS, CONSTANTS.FETCH_USER_DETAILS_FAILURE, 'getUserDetailsById')];
}

export function* watcherFetchUserDetailsRequests() {
  yield takeLatest(CONSTANTS.FETCH_USER_DETAILS, apiFetchUserDetailsHandlerAsync);
}

export function* apiCreateOrEditUserSubmitAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.CREATE_OR_EDIT_USER_SUCCESS, CONSTANTS.CREATE_OR_EDIT_USER_FAILURE, 'saveUserDetails')];
}
export function* watcherCreateOrEditUserSubmitRequests() {
  yield takeLatest(CONSTANTS.CREATE_OR_EDIT_USER, apiCreateOrEditUserSubmitAsync);
}
export function* watcherGetOwnedProjects() {
  yield takeLatest(CONSTANTS.GET_OWNED_PROJECTS, apiGetOwnedProjectsAsync);
}
export function* watcherGetDeveloperQuota() {
  yield takeLatest(CONSTANTS.GET_DEVELOPER_QUOTA, apiGetDeveloperQuota);
}

export function* apiResendEmailInviteHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.RESEND_EMAIL_INVITE_SUCCESS, CONSTANTS.RESEND_EMAIL_INVITE_FAILURE, 'resendInvite')];
}

export function* apiPromoteAsOwnerHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.PROMOTE_AS_OWNER_SUCCESS, CONSTANTS.PROMOTE_AS_OWNER_FAILURE, 'promoteAsOwner')];
}

export function* watcherResendEmailInvite() {
  yield takeLatest(CONSTANTS.RESEND_EMAIL_INVITE, apiResendEmailInviteHandlerAsync);
}

export function* watcherPromoteAsOwner() {
  yield takeLatest(CONSTANTS.PROMOTE_AS_OWNER, apiPromoteAsOwnerHandlerAsync);
}

export default function* rootSaga() {
  yield [
    watcherGetUserListRequest(),
    watcherDeleteUserRequest(),
    watcherFetchUserDetailsRequests(),
    watcherFetchRolesRequests(),
    watcherFetchGroupsRequests(),
    watcherCreateOrEditUserSubmitRequests(),
    watcherGetOwnedProjects(),
    watcherGetDeveloperQuota(),
    watcherResendEmailInvite(),
    watcherPromoteAsOwner(),
  ];
}
