/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

export const DEFAULT_ACTION = 'app/ManageUsers/DEFAULT_ACTION';

export const GET_USERS_LIST = 'app/ManageUsers/GET_USERS_LIST';
export const GOT_USERS_LIST = 'app/ManageUsers/GOT_USERS_LIST';
export const NOT_GOT_USERS_LIST = 'app/ManageUsers/NOT_GOT_USERS_LIST';

export const DELETE_USER_REQUEST = 'app/ManageUsers/DELETE_USER_REQUEST';
export const DELETE_USER_SUCCESS = 'app/ManageUsers/DELETE_USER_SUCCESS';
export const DELETE_USER_FAILURE = 'app/ManageUsers/DELETE_USER_FAILURE';

export const GET_ROLES = 'app/ManageUsers/GET_ROLES';
export const GET_ROLES_SUCCESS = 'app/ManageUsers/GET_ROLES_SUCCESS';
export const GET_ROLES_FAILURE = 'app/ManageUsers/GET_ROLES_FAILURE';

export const GET_GROUPS = 'app/ManageUsers/GET_GROUPS';
export const GET_GROUPS_SUCCESS = 'app/ManageUsers/GET_GROUPS_SUCCESS';
export const GET_GROUPS_FAILURE = 'app/ManageUsers/GET_GROUPS_FAILURE';

export const FETCH_USER_DETAILS = 'app/ManageUsers/FETCH_USER_DETAILS';
export const FETCH_USER_DETAILS_SUCCESS = 'app/ManageUsers/FETCH_USER_DETAILS_SUCCESS';
export const FETCH_USER_DETAILS_FAILURE = 'app/ManageUsers/FETCH_USER_DETAILS_FAILURE';

export const CREATE_OR_EDIT_USER = 'app/ManageUsers/CREATE_OR_EDIT_USER';
export const CREATE_OR_EDIT_USER_SUCCESS = 'app/ManageUsers/CREATE_OR_EDIT_USER_SUCCESS';
export const CREATE_OR_EDIT_USER_FAILURE = 'app/ManageUsers/CREATE_OR_EDIT_USER_FAILURE';

export const GET_OWNED_PROJECTS = 'app/ManageUsers/GET_OWNED_PROJECTS';
export const GET_OWNED_PROJECTS_SUCCESS = 'app/ManageUsers/GET_OWNED_PROJECTS_SUCCESS';
export const GET_OWNED_PROJECTS_FAILURE = 'app/ManageUsers/GET_OWNED_PROJECTS_FAILURE';

export const SEARCH_USERS = 'app/ManageUsers/SEARCH_USERS';
export const SEARCH_USERS_SUCCESS = 'app/ManageUsers/SEARCH_USERS_SUCCESS';
export const SEARCH_USERS_FAILURE = 'app/ManageUsers/SEARCH_USERS_FAILURE';

export const GET_DEVELOPER_QUOTA = 'app/ManageUsers/GET_DEVELOPER_QUOTA';
export const GET_DEVELOPER_QUOTA_SUCCESS = 'app/ManageUsers/GET_DEVELOPER_QUOTA_SUCCESS';
export const GET_DEVELOPER_QUOTA_FAILURE = 'app/ManageUsers/GET_DEVELOPER_QUOTA_FAILURE';

export const RESEND_EMAIL_INVITE = 'app/ManageUsers/RESEND_EMAIL_INVITE';
export const RESEND_EMAIL_INVITE_SUCCESS = 'app/ManageUsers/RESEND_EMAIL_INVITE_SUCCESS';
export const RESEND_EMAIL_INVITE_FAILURE = 'app/ManageUsers/RESEND_EMAIL_INVITE_FAILURE';

export const PROMOTE_AS_OWNER = 'app/ManageUsers/PROMOTE_AS_OWNER';
export const PROMOTE_AS_OWNER_SUCCESS = 'app/ManageUsers/PROMOTE_AS_OWNER_SUCCESS';
export const PROMOTE_AS_OWNER_FAILURE = 'app/ManageUsers/PROMOTE_AS_OWNER_FAILURE';

export const RESET_TO_INITIAL_STATE = 'app/ManageUsers/RESET_TO_INITIAL_STATE';


