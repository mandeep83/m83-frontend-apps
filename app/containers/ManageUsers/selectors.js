/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectManageUsersDomain = state => state.get('manageUsers', initialState);

const getUsersList = () =>
  createSelector(selectManageUsersDomain, subState => subState.usersList);

const getUsersApiError = () =>
  createSelector(selectManageUsersDomain, subState => subState.usersListError);

const getSubscriberTablePageSize = () =>
  createSelector(selectManageUsersDomain, subState => subState.pageSize);

const getDeleteUserSuccess = () =>
  createSelector(selectManageUsersDomain, subState => subState.deleteUserSuccess);

const getDeleteUserFailure = () =>
  createSelector(selectManageUsersDomain, subState => subState.deleteUserFailure);

const isFetchingState = state => state.get('loader');

const getIsFetching = () =>
  createSelector(isFetchingState, fetchingState => fetchingState.get('isFetching'));

export const getUserDetails = () =>
  createSelector(selectManageUsersDomain, subState => subState.payload);

export const getUserDetailsError = () =>
  createSelector(selectManageUsersDomain, subState => subState.payloadError);

export const getRoles = () =>
  createSelector(selectManageUsersDomain, subState => subState.roles);

export const getRolesApiError = () =>
  createSelector(selectManageUsersDomain, subState => subState.rolesError);

export const getGroups = () =>
  createSelector(selectManageUsersDomain, subState => subState.groups);

export const getGroupsError = () =>
  createSelector(selectManageUsersDomain, subState => subState.groupsError);

export const getUserUpdateSuccess = () =>
  createSelector(selectManageUsersDomain, subState => subState.updateUser);

export const getUserupdateError = () =>
  createSelector(selectManageUsersDomain, subState => subState.updateUserError);

export const getOwnedProjectsSuccess = () =>
  createSelector(selectManageUsersDomain, subState => subState.projects);

export const getOwnedProjectsError = () =>
  createSelector(selectManageUsersDomain, subState => subState.projectsError);

export const developerQuotaSuccess = () =>
  createSelector(selectManageUsersDomain, subState => subState.developerQuotaSuccess);

export const developerQuotaFailure = () =>
  createSelector(selectManageUsersDomain, subState => subState.developerQuotaFailure);

export const resendInviteSuccess = () =>
  createSelector(selectManageUsersDomain, subState => subState.resendInviteSuccess);

export const resendInviteFailure = () =>
  createSelector(selectManageUsersDomain, subState => subState.resendInviteFailure);

export const promoteAsOwnerSuccess = () =>
  createSelector(selectManageUsersDomain, subState => subState.promoteAsOwnerSuccess);

export const promoteAsOwnerFailure = () =>
  createSelector(selectManageUsersDomain, subState => subState.promoteAsOwnerFailure);

export {
  getUsersList, getUsersApiError, getIsFetching, getSubscriberTablePageSize, getDeleteUserSuccess, getDeleteUserFailure
};
