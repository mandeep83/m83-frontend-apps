/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { fromJS } from 'immutable';
import * as CONSTANTS from './constants';

export const initialState = fromJS({});

function manageUsersReducer(state = initialState, action) {
  switch (action.type) {
    case CONSTANTS.DEFAULT_ACTION:
      return state;
    case CONSTANTS.GOT_USERS_LIST:
      return Object.assign({}, state, {
        'usersList': action.response,
      });
    case CONSTANTS.NOT_GOT_USERS_LIST:
      return Object.assign({}, state, {
        'usersListError': action.error
      });
    case CONSTANTS.DELETE_USER_SUCCESS:
      return Object.assign({}, state, {
        'deleteUserSuccess': action.response
      });
    case CONSTANTS.DELETE_USER_FAILURE:
      return Object.assign({}, state, {
        'deleteUserFailure': action.error
      });
    case CONSTANTS.FETCH_USER_DETAILS_SUCCESS:
      return Object.assign({}, state, {
        payload: action.response
      });
    case CONSTANTS.FETCH_USER_DETAILS_FAILURE:
      return Object.assign({}, state, {
        payloadError: action.error
      });
    case CONSTANTS.GET_ROLES_FAILURE:
      return Object.assign({}, state, {
        rolesError: action.error
      });
    case CONSTANTS.GET_ROLES_SUCCESS:
      return Object.assign({}, state, {
        roles: action.response
      });
    case CONSTANTS.GET_GROUPS_FAILURE:
      return Object.assign({}, state, {
        groupsError: action.error
      });
    case CONSTANTS.GET_GROUPS_SUCCESS:
      return Object.assign({}, state, {
        groups: action.response
      });
    case CONSTANTS.CREATE_OR_EDIT_USER_FAILURE:
      return Object.assign({}, state, {
        updateUserError: {
          error: action.error,
          date: new Date()
        }
      });
    case CONSTANTS.CREATE_OR_EDIT_USER_SUCCESS:
      return Object.assign({}, state, {
        updateUser: {
          response: action.response,
          date: new Date()
        }
      });
    case CONSTANTS.GET_OWNED_PROJECTS_FAILURE:
      return Object.assign({}, state, {
        projectsError: action.error
      });
    case CONSTANTS.GET_OWNED_PROJECTS_SUCCESS:
      return Object.assign({}, state, {
        projects: action.response
      });
    case CONSTANTS.GET_DEVELOPER_QUOTA_SUCCESS:
      return Object.assign({}, state, {
        'developerQuotaSuccess': action.response,
      });
    case CONSTANTS.GET_DEVELOPER_QUOTA_FAILURE:
      return Object.assign({}, state, {
        'developerQuotaFailure': action.error
      });
    case CONSTANTS.RESEND_EMAIL_INVITE_SUCCESS:
      return Object.assign({}, state, {
        'resendInviteSuccess': action.response,
      });
    case CONSTANTS.RESEND_EMAIL_INVITE_FAILURE:
      return Object.assign({}, state, {
        'resendInviteFailure': action.error
      });
    case CONSTANTS.PROMOTE_AS_OWNER_SUCCESS:
      return Object.assign({}, state, {
        'promoteAsOwnerSuccess': action.response,
      });
    case CONSTANTS.PROMOTE_AS_OWNER_FAILURE:
      return Object.assign({}, state, {
        'promoteAsOwnerFailure': action.error
      });
    case CONSTANTS.RESET_TO_INITIAL_STATE:
      return initialState;
    default:
      return state;
  }
}

export default manageUsersReducer;
