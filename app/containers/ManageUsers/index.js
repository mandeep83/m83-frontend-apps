/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import * as Selectors from './selectors';
import * as Actions from './actions';
import reducer from './reducer';
import saga from './saga';
import NotificationModal from '../../components/NotificationModal/Loadable'
import jwt_decode from "jwt-decode";
import Loader from '../../components/Loader/Loadable';
import SlidingPane from 'react-sliding-pane';
import ReactSelect from "react-select";
import Modal from 'react-modal';
import ListingTable from "../../components/ListingTable/Loadable";
import AddNewButton from "../../components/AddNewButton/Loadable"
import { cloneDeep } from 'lodash';
import PhoneInput from "react-phone-input-2";
import 'react-phone-input-2/lib/style.css';
import Skeleton from 'react-loading-skeleton';

let loggedInUser = jwt_decode(localStorage['token']).sub;
const tenantType = jwt_decode(localStorage['token']).tenantType

/* eslint-disable react/prefer-stateless-function */
export class ManageUsers extends React.Component {
    state = {
        selectedUser: '',
        count: 0,
        isFetching: true,
        payload: {
            email: '',
            firstName: '',
            lastName: '',
            mobile: '',
            roleId: null,
            assignedProjects: [],
            userGroups: [],
            password: '',
            skipVerification: false
        },
        passwordMatched: true,
        toggleView:true,
        roles: [],
        groups: [],
        usersList: {
            users: [],
            totalCount: 0
        },
        filterUserList: {
            users: [],
            totalCount: 0
        },
        isFetchingUsers: true,
        isChangingPassword: false,
        projects: [],
        assignedProjects: [],
        showAddOrEditUser: false,
        accessType: "",
        tableState: {
            activePageNumber: 1,
            pageSize: 50,
            filtered: [],
        },
        searchTerm: '',
        userQuota: {
            total: 0,
            remaining: 0,
            used: 0,
        }
    };

    componentDidMount() {
        this.props.fetchRoles();
        this.props.fetchGroups();
        Modal.setAppElement('body');
        this.props.getOwnedProjects();
        this.props.getDeveloperQuota([{
            dependingProductId: null,
            productName: "users"
        }]);
    }

    confirmModalHandler = id => {
        this.setState({
            selectedUser: id,
        });
        $('#confirmModal').modal('show');
    };

    filterMethod = (filter, row) => {
        const id = filter.pivotId || filter.id
        return row[id] !== undefined ? String(row[id].toLowerCase()).includes(filter.value.toLowerCase()) : true
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.usersListApiError && this.props.usersListApiError != nextProps.usersListApiError) {
            this.setState({
                isOpenModal: true,
                type: "error",
                message2: nextProps.usersListApiError,
                isFetching: false,
            })
        }

        if (nextProps.usersList && this.props.usersList != nextProps.usersList) {
            let usersList = nextProps.usersList,
                filterUserList = cloneDeep(nextProps.usersList),
                searchTerm = this.state.searchTerm;

            (searchTerm.length <= 0) ?
                filterUserList.users.map(user => {
                    user.accessType = this.state.roles.find(role => role.name === user.role).accessType;
                }) :
                filterUserList.users = filterUserList.users.filter(item => item.name.toLowerCase().includes(searchTerm.toLowerCase()) || item.email.toLowerCase().includes(searchTerm.toLowerCase()))

            this.setState({
                usersList,
                filterUserList,
                searchTerm,
                count: filterUserList.totalCount,
                isFetching: false,
                isFetchingUsers: false
            })
        }

        if (nextProps.projectsError && this.props.projectsError != nextProps.projectsError) {
            this.setState({
                isOpenModal: true,
                type: "error",
                message2: nextProps.projectsError
            })
        }

        if (nextProps.projects && this.props.projects != nextProps.projects) {
            this.setState({
                projects: nextProps.projects,
            })
        }

        if (nextProps.deleteUserFailure && this.props.deleteUserFailure != nextProps.deleteUserFailure) {
            this.setState({
                isFetching: false,
                isOpenModal: true,
                type: "error",
                message2: nextProps.deleteUserFailure
            })
        }

        if (nextProps.deleteUserSuccess && this.props.deleteUserSuccess != nextProps.deleteUserSuccess) {
            let filterUserList = JSON.parse(JSON.stringify(this.state.filterUserList))
            let userQuota = cloneDeep(this.state.userQuota)

            filterUserList.users = filterUserList.users.filter(temp => temp.id != nextProps.deleteUserSuccess.id);
            filterUserList.totalCount--;

            userQuota.remaining++;
            userQuota.used--;

            this.setState({
                isOpenModal: true,
                type: "success",
                message2: nextProps.deleteUserSuccess.message,
                filterUserList,
                isFetching: false,
                userQuota
            })
        }

        if (nextProps.userDetails && nextProps.userDetails !== this.props.userDetails) {
            let payload = JSON.parse(JSON.stringify(nextProps.userDetails)),
                assignedProjects = JSON.parse(JSON.stringify(this.state.projects));
            let accessType = this.state.roles.find((temp) => temp.id === payload.roleId).accessType;
            assignedProjects.map(project => {
                let _projectObj = payload.assignedProjects.find(projectObj => projectObj.id === project.id);
                project.projectAccess = project.projectAndAccessPermission;
                project.sourceControlOriginal = project.sourceControlEnabled
                project.repoName = project.repoName && project.repoName !== '' ? project.repoName : null;
                project.projectAndAccessPermission = _projectObj ? _projectObj.projectAndAccessPermission : null;
                project.sourceControlEnabled = _projectObj ? _projectObj.sourceControlEnabled : null;
                project.isProjectOwner = _projectObj ? _projectObj.owner : false;
            });


            const comparer = (otherArray) => {
                return function (current) {
                    return otherArray.filter(function (other) {
                        return other.id == current.id
                    }).length == 0;
                }
            }

            var diff = payload.assignedProjects.filter(comparer(assignedProjects));

            diff.map((temp) => {
                temp.isReadonly = true;
                return temp;
            })
            payload.password = "";

            this.setState({
                payload,
                assignedProjects: [...assignedProjects, ...diff],
                email: payload.email,
                accessType
            })
        }

        if (nextProps.userDetailsError !== this.props.userDetailsError) {
            this.setState({
                isOpenModal: true,
                message2: nextProps.userDetailsError,
                type: 'error'
            });
        }

        if (nextProps.updateUserSuccess && nextProps.updateUserSuccess !== this.props.updateUserSuccess) {
            this.setState({
                isOpenModal: true,
                message2: nextProps.updateUserSuccess.response,
                type: 'success',
                isSubmitSuccess: true,
                showAddOrEditUser: false,
                isFetchingUsers: true,
                email: "",
                isSavingUser: false,
            }, () => {
                this.getUsers(this.state.tableState)
                this.props.getDeveloperQuota([{
                    dependingProductId: null,
                    productName: "users"
                }]);
            });
        }

        if (nextProps.updateUserError !== this.props.updateUserError) {
            this.setState({
                errorMessage: nextProps.updateUserError.error,
                isSavingUser: false,
            });
        }

        if (nextProps.roles && nextProps.roles !== this.props.roles) {
            let payload = JSON.parse(JSON.stringify(this.state.payload));
            this.setState({
                roles: nextProps.roles.filter(temp => temp.name != "DEFAULT_APP_ROLE"),
                payload,
                isFetching: false,
            }, () => {
                this.props.getManageUsersList({ max: this.state.tableState.pageSize, offset: ((this.state.tableState.activePageNumber - 1) * this.state.tableState.pageSize) })
            })
        }

        if (nextProps.rolesError && nextProps.rolesError !== this.props.rolesError) {
            this.setState({
                isOpenModal: true,
                message2: nextProps.rolesError,
                type: 'error'
            });
        }
        if (nextProps.groups && nextProps.groups !== this.props.groups) {
            let groups = nextProps.groups.map((temp) => { return { value: temp.id, label: temp.name } })
            this.setState({
                groups
            })
        }

        if (nextProps.groupsError && nextProps.groupsError !== this.props.groupsError) {
            this.setState({
                isOpenModal: true,
                message2: nextProps.groupsError,
                type: 'error'
            });
        }
        if (nextProps.developerQuotaSuccess && nextProps.developerQuotaSuccess !== this.props.developerQuotaSuccess) {
            this.setState({
                userQuota: nextProps.developerQuotaSuccess[0],
            })
        }

        if (nextProps.developerQuotaFailure && nextProps.developerQuotaFailure !== this.props.developerQuotaFailure) {
            this.setState({
                isOpenModal: true,
                message2: nextProps.developerQuotaFailure,
                type: 'error'
            });
        }
        if (nextProps.resendInviteSuccess && nextProps.resendInviteSuccess !== this.props.resendInviteSuccess) {
            let filterUserList = cloneDeep(this.state.filterUserList),
                selectedUser = this.state.selectedUser;
            filterUserList.users.filter(user => user.id === selectedUser)[0]['inviteInProgress'] = false;

            this.setState({
                isOpenModal: true,
                type: "success",
                message2: "Email invite sent successfully.",
                filterUserList
            });
        }

        if (nextProps.resendInviteFailure && nextProps.resendInviteFailure !== this.props.resendInviteFailure) {
            let filterUserList = cloneDeep(this.state.filterUserList),
                selectedUser = this.state.selectedUser;

            filterUserList.users.filter(user => user.id === selectedUser)[0]['inviteInProgress'] = false;

            this.setState({
                isOpenModal: true,
                type: 'error',
                message2: nextProps.resendInviteFailure,
                filterUserList
            });
        }

        if (nextProps.promoteAsOwnerSuccess && nextProps.promoteAsOwnerSuccess !== this.props.promoteAsOwnerSuccess) {
            let filterUserList = cloneDeep(this.state.filterUserList),
                selectedUser = this.state.selectedUser;

            filterUserList.users.filter(user => user.id === selectedUser)[0]['promotionInProgress'] = false;
            filterUserList.users.filter(user => user.id === selectedUser)[0]['isDefault'] = true;

            this.setState({
                isOpenModal: true,
                type: "success",
                message2: 'Request completed successfully.',
                filterUserList
            });
        }

        if (nextProps.resendInviteFailure && nextProps.resendInviteFailure !== this.props.resendInviteFailure) {
            let filterUserList = cloneDeep(this.state.filterUserList),
                selectedUser = this.state.selectedUser;
            filterUserList.users.filter(user => user.id === selectedUser)[0]['promotionInProgress'] = false;

            this.setState({
                isOpenModal: true,
                type: 'error',
                message2: nextProps.resendInviteFailure,
                filterUserList
            });
        }
    }

    onCloseHandler = () => {
        this.setState({
            isOpenModal: false,
            type: "",
            message2: "",
        })
    }


    onChangeHandler = ({ currentTarget }) => {
        let payload = JSON.parse(JSON.stringify(this.state.payload));
        let email = this.state.email;
        if (currentTarget.name === "mobile" && isNaN(Number(currentTarget.value))) {
            return;
        }
        if (currentTarget.name === "skipVerification") {
            payload[currentTarget.name] = currentTarget.checked
            payload.password = ""
        } else if (currentTarget.name === "email") {
            email = currentTarget.value;
        } else if (currentTarget.name === "userGroups") {
            payload[currentTarget.name] = currentTarget.value ? [currentTarget.value] : [];
        } else {
            payload[currentTarget.name] = currentTarget.value;
        }
        this.setState({
            payload,
            email,
        });
    }

    phoneNumberChangeHandler = (value) => {
        let payload = cloneDeep(this.state.payload);
        payload.mobile = "+" + value;
        this.setState({
            payload,
        });
    }


    onProjectChangeHandler = (event, selectedProjectIndex) => {
        let assignedProjects = JSON.parse(JSON.stringify(this.state.assignedProjects));
        if (event.target.name === "sourceControl") {
            assignedProjects[selectedProjectIndex].sourceControlEnabled = event.target.checked;
        } else {
            if (event.target.checked) {
                assignedProjects[selectedProjectIndex].projectAndAccessPermission = event.target.value;
            } else {
                assignedProjects[selectedProjectIndex].projectAndAccessPermission = null;
                assignedProjects[selectedProjectIndex].sourceControlEnabled = false;
            }
        }

        this.setState({
            assignedProjects
        })


    }

    createOrEditUserHandler = (event) => {
        event.preventDefault();
        let payload = JSON.parse(JSON.stringify(this.state.payload)),
            assignedProjects = JSON.parse(JSON.stringify(this.state.assignedProjects)),
            isOpenModal = this.state.isOpenModal,
            isSavingUser = this.state.isSavingUser,
            message2 = '';

        payload.id = this.state.selectedUser;
        payload.email = this.state.email;
        let assignedProjectsArray = [];
        const selectedRoleType = this.state.roles.find(temp => temp.id === payload.roleId).type
        assignedProjects.map(project => {
            if (tenantType !== "FLEX") {
                if (project.projectAndAccessPermission !== null) {
                    assignedProjectsArray.push({
                        id: project.id,
                        projectAndAccessPermission: project.projectAndAccessPermission,
                        sourceControlEnabled: project.sourceControlEnabled ? project.sourceControlEnabled : false,
                    })
                }
            } else {
                assignedProjectsArray.push({
                    id: project.id,
                    projectAndAccessPermission: selectedRoleType === "SYSTEM_ROLE" ? "admin" : "read_write",
                    sourceControlEnabled: project.sourceControlEnabled ? project.sourceControlEnabled : false,
                })
            }
        })

        if (payload.password && !payload.password.length > 0) {
            delete payload.password
        }

        if (payload.id === null) {
            delete payload.id
        }
        payload.assignedProjects = assignedProjectsArray;
        if (payload.roleId) {
            if (payload.mobile.length <= 2) {
                payload.mobile = '';
            }
            isSavingUser = true;
            this.props.createOrEditUser(payload);

        }
        else {
            isOpenModal = true
            message2 = "Role not selected !!"
        }
        this.setState({
            isOpenModal,
            message2,
            isSavingUser
        })
    };

    getRoleMenuOptions = (roles) => {
        let options = [];
        roles.map(role => {
            options.push({
                label: role.name,
                value: role.id,
                accessType: role.accessType
            })
        });
        return options;
    }

    getSelectedRoleForMenu = (roleId) => {
        let roles = this.state.roles,
            obj = {};
        if (roleId) {
            obj = {
                label: roles.filter(role => role.id === roleId)[0].name,
                value: roleId,
            }
        }
        return obj;
    }

    handleChangeForRoleMenu = (role) => {
        let payload = JSON.parse(JSON.stringify(this.state.payload));
        let assignedProjects = [...this.state.assignedProjects];
        let accessType = role.accessType,
            isChangingPassword = this.state.isChangingPassword;
        payload.roleId = role.value;
        if (role.label === "ACCOUNT_ADMIN") {
            payload.skipVerification = false;
            payload.userGroups = [];
            isChangingPassword = false;
        }
        assignedProjects.map(temp => {
            if (!temp.isReadonly) {
                temp.sourceControlEnabled = accessType !== "APPLICATION" && temp.sourceControlEnabled
                temp.projectAndAccessPermission = accessType === "APPLICATION" && temp.projectAndAccessPermission != "read_write" ? null : temp.projectAndAccessPermission
            }
            return temp
        })

        this.setState({
            payload,
            isChangingPassword,
            assignedProjects,
            accessType,
        });
    }


    getGroupsMenuOptions = () => {
        let groups = JSON.parse(JSON.stringify(this.state.groups)),
            options = [];
        groups.map(group => {
            options.push({
                label: group.name,
                value: group.id,
            })
        });
        return options;
    }

    getSelectedGroupForMenu = (addedGroups) => {
        let groups = JSON.parse(JSON.stringify(this.state.groups)),
            options = [];
        if (addedGroups && addedGroups.length > 0) {
            addedGroups.map(group => {
                options.push({
                    label: groups.filter(groupObj => groupObj.id === group)[0].name,
                    value: group,
                })
            });
        }

        return options;
    }

    handleChangeForGroupMenu = (groups) => {
        let payload = JSON.parse(JSON.stringify(this.state.payload));
        payload.userGroups = [];
        groups.map(group => {
            payload.userGroups.push(group.value);
        });
        this.setState({
            payload,
        })

    }

    handleAddNewUserClick = () => {
        let assignedProjects = JSON.parse(JSON.stringify(this.state.projects));
        assignedProjects.map(project => {
            project.projectAccess = project.projectAndAccessPermission;
            project.repoName = project.repoName && project.repoName !== '' ? project.repoName : null;
            project.projectAndAccessPermission = null;
            project.sourceControlOriginal = project.sourceControlEnabled
            project.sourceControlEnabled = null;
        });

        this.setState({
            assignedProjects,
            showAddOrEditUser: true,
            selectedUser: null,
            isChangingPassword: false,
            email: "",
            payload: {
                email: '',
                firstName: '',
                lastName: '',
                mobile: '',
                roleId: null,
                password: '',
                userGroups: [],
                assignedProjects: [],
                skipVerification: false
            },
            errorMessage: null,
            accessType: "",
            isDefaultUser: false,
            isSavingUser: false
        })
    }


    getUsers = (newState) => {
        let offset = (typeof newState.activePageNumber === 'undefined' || newState.activePageNumber === 0) ? 0 : (newState.activePageNumber - 1) * newState.pageSize;
        let payload = {
            max: newState.pageSize,
            offset,
        }
        newState.filtersAndSort && newState.filtersAndSort.filters.map(temp => {
            payload[temp.accessor] = temp.value
        })
        if (newState.filtersAndSort && newState.filtersAndSort.sort) {
            payload.orderBy = newState.filtersAndSort.sort.value
            payload.sortBy = newState.filtersAndSort.sort.accessor
        }
        let isSearchedByUser = Object.entries(payload).some(([key, value]) => (["email", "name", "role", "project", "accessType"].includes(key)) && value);
        this.setState({
            tableState: newState,
            isSearchedByUser,
            isFetchingUsers: true
        }, () => this.props.getManageUsersList(payload));
    }


    checkDisableCondition = (permissionType, project) => {
        let payload = { ...this.state.payload }
        let isDisabled = false
        let commonCheck = payload.email === jwt_decode(localStorage.token).sub || project.isProjectOwner || project.isReadonly
        switch (permissionType) {
            case "sourceControl":
                if (!project.projectAndAccessPermission || !project.repoName || localStorage.isGitHubLogin !== 'true' || !project.sourceControlOriginal || this.state.accessType === "APPLICATION" || commonCheck) {
                    isDisabled = true;
                }
                break;
            case "admin":
                if (project.projectAccess !== "admin" || this.state.accessType == "APPLICATION" || commonCheck) {
                    isDisabled = true
                }
                break;
            case "read_write":
                if (project.projectAccess !== "admin" || commonCheck) {
                    isDisabled = true
                }
                break;
        }
        return isDisabled;
    }

    searchUserByNameAndEmail = (e) => {
        let filterUserList = JSON.parse(JSON.stringify(this.state.usersList))
        filterUserList.users = filterUserList.users.filter(item => item.name.toLowerCase().includes(e.target.value.toLowerCase()) || item.email.toLowerCase().includes(e.target.value.toLowerCase()))
        this.setState({ searchTerm: e.target.value, filterUserList })
    }

    removeSearchFieldText = () => {
        let filterUserList = JSON.parse(JSON.stringify(this.state.usersList))
        if (this.state.searchTerm.length > 0) {
            this.setState({ searchTerm: "", filterUserList })
        }
    }

    copyToClipboard = (email) => {
        navigator.clipboard.writeText(email)
        this.setState({
            type: "success",
            isOpenModal: true,
            message2: 'Copied to Clipboard successfully !',
        })
    }

    resendEmailInviteHandler = (userId) => {
        let filterUserList = cloneDeep(this.state.filterUserList);
        filterUserList.users.filter(user => user.id === userId)[0]['inviteInProgress'] = true;

        this.setState({
            selectedUser: userId,
            filterUserList,
        }, () => this.props.resendEmailInvite(userId));
    }

    promoteAsOwner = (userId) => {
        let filterUserList = cloneDeep(this.state.filterUserList);
        filterUserList.users.filter(user => user.id === userId)[0]['promotionInProgress'] = true;

        this.setState({
            selectedUser: userId,
            filterUserList,
        }, () => this.props.promoteAsOwner(userId));
    }

    getStatusBarClass = (row) => {
        if (row.isVerified)
            return { className: "green", tooltipText: "Verified User" }
        return { className: "red", tooltipText: "Non-Verified User" }
    }

    getColumns = () => {
        return [
            {
                Header: "Name", width: "17_5",
                cell: (row) => (
                    <React.Fragment>
                        <div className="list-view-icon">
                            <i className={row.isDefault ? "fad fa-user-crown" : row.role == "ACCOUNT_ADMIN" ? "fad fa-user-shield" : row.roleIcon ? row.roleIcon : "fad fa-user-tie"}></i>
                            {row.isDefault && <span>&#x272A;</span>}
                        </div>
                        <div className="list-view-icon-box">
                            <h6 className="text-theme fw-600">{row.name}</h6>
                            {row.isDefault && <p className="text-orange">OWNER</p>}
                        </div>
                    </React.Fragment>
                ),
                accessor: "name",
                filterable: true,
                sortable: true
            },
            {
                Header: "Role", width: "12_5", cell: (row) => (
                    <h6 className={row.role == "ACCOUNT_ADMIN" ? "text-pink" : "text-indigo"}>
                        {row.role}
                    </h6>
                ),
                filterable: true,
                sortable: true,
                accessor: "role",
                filter: ({ value, onChangeHandler, accessor }) =>
                    <select
                        className="form-control"
                        onChange={event => onChangeHandler(event, accessor)}
                        value={value ? value : 'all'}
                    >
                        <option value="">ALL</option>
                        {this.state.roles.map(temp => <option value={temp.name} key={temp.id}>{temp.name}</option>)}
                    </select>
            },
            {
                Header: "User Group", width: "12_5",
                cell: (row) => (
                    <h6>{row.role === 'ACCOUNT_ADMIN' ? 'ALL' : row.userGroups ? row.userGroups : "-"}</h6>
                ),
                filterable: true,
                sortable: true,
                accessor: "userGroup",
                filter: ({ value, onChangeHandler, accessor }) =>
                    <select
                        className="form-control"
                        onChange={event => onChangeHandler(event, accessor)}
                        value={value ? value : 'all'}
                    >
                        <option value="">ALL</option>
                        {this.state.groups.map(temp => <option value={temp.label} key={temp.value}>{temp.label}</option>)}
                    </select>
            },
            {
                Header: "Contact", width: "15",
                cell: (row) => (
                    <React.Fragment>
                        <button className="btn-transparent btn-transparent-green copy-button" data-tooltip data-tooltip-text="Copy" data-tooltip-place="bottom" onClick={() => this.copyToClipboard(row.email)}>
                            <i className="far fa-copy"></i>
                        </button>
                        <a className="f-13 text-truncate d-block" href={`mailto:${row.email}`} data-tooltip data-tooltip-text={row.email} data-tooltip-place="bottom">{row.email}</a>
                        {row.mobile &&
                            <span className="list-phone-input">
                                <PhoneInput
                                    inputProps={{
                                        name: 'phone',
                                    }}
                                    disabled={true}
                                    disableDropdown={true}
                                    value={row.mobile}
                                    onChange={(value) => null}
                                />
                            </span>
                        }
                    </React.Fragment>
                )
            },
            { Header: "Created", width: "15", accessor: "createdAt" },
            { Header: "Updated", width: "15", accessor: "updatedAt" },
            {
                Header: "Actions", width: "12_5",
                cell: (row) => (
                    <div className="button-group">
                        {row.inviteInProgress ?
                            <button type="button" className="btn-transparent btn-transparent-green" data-tooltip data-tooltip-text="Resend Invite In Progress" data-tooltip-place="bottom">
                                <i className="far fa-cog fa-spin"></i>
                            </button> :
                            <button type="button" className="btn-transparent btn-transparent-green" data-tooltip data-tooltip-text="Resend Invite" data-tooltip-place="bottom"
                                disabled={row.isVerified}
                                onClick={() => this.resendEmailInviteHandler(row.id)}>
                                <i className="far fa-envelope"></i>
                            </button>
                        }
                        {jwt_decode(localStorage.token).defaultAccountAdmin ?
                            row.promotionInProgress ?
                                <button type="button" className="btn-transparent btn-transparent-cyan" data-tooltip data-tooltip-text="Promotion to Owner In Progress" data-tooltip-place="bottom">
                                    <i className="far fa-cog fa-spin"></i>
                                </button> :
                                <button
                                    type="button"
                                    className="btn-transparent btn-transparent-cyan"
                                    data-tooltip data-tooltip-text="Promote As Owner"
                                    data-tooltip-place="bottom"
                                    disabled={row.role !== 'ACCOUNT_ADMIN' || row.isDefault}
                                    onClick={() => this.promoteAsOwner(row.id)}>
                                    <i className="fas fa-user-plus"></i>
                                </button> :
                            null
                        }
                        <button className="btn-transparent btn-transparent-blue"
                            onClick={() => {
                                this.props.fetchUserDetails(row.id)
                                this.setState({ showAddOrEditUser: true, selectedUser: row.id, isChangingPassword: false, isDefaultUser: row.isDefault, errorMessage: '', isSavingUser: false });
                            }}
                            disabled={!row.internalUser || row.isDefault} data-tooltip data-tooltip-text="Edit" data-tooltip-place="bottom"
                        >
                            <i className="far fa-pencil"></i>
                        </button>
                        <button className="btn-transparent btn-transparent-red" data-tooltip data-tooltip-text="Delete" data-tooltip-place="bottom"
                            disabled={!row.internalUser || row.email === loggedInUser || row.isDefault}
                            onClick={() => { this.confirmModalHandler(row.id); }}
                        >
                            <i className="far fa-trash-alt"></i>
                        </button>
                    </div>
                )
            },
        ]
    }

    pageChangeHandler = (pageSize, activePageNumber, filtersAndSort) => {
        let pageSizeChanged = pageSize !== this.state.tableState.pageSize
        if (pageSizeChanged) {
            let totalPages = Math.ceil(this.state.count / pageSize)
            if (totalPages < activePageNumber) {
                activePageNumber = totalPages
            }
        }

        let payload = {
            max: pageSize,
            offset: (activePageNumber - 1) * pageSize,
        }
        filtersAndSort && filtersAndSort.filters.map(temp => {
            payload[temp.accessor] = temp.value
        })
        if (filtersAndSort && filtersAndSort.sort) {
            payload.orderBy = filtersAndSort.sort.value
            payload.sortBy = filtersAndSort.sort.accessor
        }

        let tableState = { ...this.state.tableState, pageSize, activePageNumber, filtersAndSort }
        let isSearchedByUser = Object.entries(payload).some(([key, value]) => (["email", "name", "role", "project", "accessType"].includes(key)) && value);

        this.setState({
            isFetchingUsers: true,
            isSearchedByUser,
            tableState,
            searchTerm: ""
        }, () => this.props.getManageUsersList(payload))
    }

    getTableData = (pageSize, activePageNumber, usersList) => {
        if (activePageNumber === "...") {
            activePageNumber = Math.ceil((getPageNumbers()[3] - 1) / 2)
        }
        let endIndex = (pageSize * activePageNumber);
        let startIndex = endIndex - pageSize
        return usersList.slice(startIndex, endIndex)
    }

    handleChangeForUserGroups = (group) => {

        let payload = cloneDeep(this.state.payload);
        if (group && group.value) {
            payload.userGroups = [group.value];
        }
        else payload.userGroups = []
        this.setState({
            payload,
        })
    }

    getselectedgroup = (selectedGroup) => {
        if (selectedGroup) {
            let groups = this.state.groups
            return {
                value: selectedGroup,
                label: groups.find(groupObj => groupObj.value === selectedGroup).label,
            }
        }
    }

    refreshUser = () => {
        this.setState({
            isFetchingUsers: true
        }, () => this.getUsers(this.state.tableState))
    }

    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>Users</title>
                    <meta name="description" content="M83-Users" />
                </Helmet>

                <header className="content-header d-flex">
                    <div className="flex-50">
                        <h6>Users -
                            <span className="content-header-badge-group">
                                <span className="content-header-badge-item">Allocated <span className="badge badge-pill badge-primary">{this.state.userQuota.total}</span></span>
                                <span className="content-header-badge-item">Used <span className="badge badge-pill badge-success">{this.state.userQuota.used}</span></span>
                                <span className="content-header-badge-item">Remaining <span className="badge badge-pill badge-warning">{this.state.userQuota.remaining}</span></span>
                            </span>
                        </h6>
                    </div>
                    <div className="flex-50 text-right">
                        <div className="content-header-group">
                            <span className="text-content f-11 mr-2"><span className="text-yellow f-13">&#x272A;</span> = Owner of this Account</span>
                            {/* <div className="search-box">
                                <span className="search-icon"><i className="far fa-search"></i></span>
                                <input type="text" className="form-control" disabled={this.state.usersList.users.length === 0} value={this.state.searchTerm} onChange={this.searchUserByNameAndEmail} placeholder="Search..." />
                                {this.state.searchTerm.length > 0 ? <button className="search-button" onClick={() => this.removeSearchFieldText()}><i className="far fa-times"></i></button> : ""}
                            </div> */}

                            <button className={this.state.toggleView == true ? "btn btn-light active" : "btn btn-light"} disabled={this.state.isFetching} data-tooltip="true" data-tooltip-text="List View" data-tooltip-place="bottom" onClick={() => this.setState({ toggleView: true })}><i className="fad fa-list-ul"></i></button>
                            <button className={this.state.toggleView == false ? "btn btn-light active" : "btn btn-light"} disabled={this.state.isFetching} data-tooltip="true" data-tooltip-text="Card View" data-tooltip-place="bottom" onClick={() => this.setState({ toggleView: false })}><i className="fad fa-table"></i></button>
                            <button className="btn btn-light" disabled={this.state.isFetching} data-tooltip data-tooltip-text="Refresh" data-tooltip-place="bottom" onClick={this.refreshUser} ><i className="far fa-sync-alt"></i></button>
                            <button className="btn btn-primary" data-tooltip data-tooltip-text="Add User" data-tooltip-place="bottom" disabled={this.state.userQuota.remaining === 0 || this.state.isFetching} onClick={() => { this.handleAddNewUserClick() }}><i className="far fa-plus"></i></button>
                        </div>
                    </div>
                </header>

                <div className="content-body" id="manageUsers">
                    {this.state.isFetching ? <Skeleton count={9} className="skeleton-list-loader" /> :
                        this.state.filterUserList.totalCount > 0 || this.state.userQuota.total > 0 || this.state.isFetchingUsers || this.state.isSearchedByUser ?
                            this.state.toggleView ?
                                <ListingTable
                                    data={this.state.filterUserList.users}
                                    columns={this.getColumns()}
                                    statusBar={this.getStatusBarClass}
                                    pageSize={this.state.tableState.pageSize}
                                    activePageNumber={this.state.tableState.activePageNumber}
                                    totalItemsCount={this.state.count}
                                    pageChangeHandler={this.pageChangeHandler}
                                    isLoading={this.state.isFetchingUsers}
                                />
                                :
                                <ul className="card-view-list">
                                    {this.state.filterUserList.users.map((item, index) =>
                                        <li key={index}>
                                            <div className="card-view-box">
                                                <div className="card-view-header">
                                                    {item.isDefault && <span className="alert alert-warning text-orange mr-r-7">&#x272A; OWNER</span>}
                                                    <span className={`alert alert-secondary bg-white ${item.role == "ACCOUNT_ADMIN" ? "text-pink" : "text-indigo"}`}>{item.role}</span>
                                                    <div className="dropdown">
                                                        <button className="btn-transparent btn-transparent-gray" type="button" data-toggle="dropdown">
                                                            <i className="fas fa-ellipsis-v"></i>
                                                        </button>
                                                        <div className="dropdown-menu">
                                                            {item.inviteInProgress ?
                                                                <button type="button" className="dropdown-item">
                                                                    <i className="far fa-cog fa-spin"></i>Resend Invite In Progress
                                                                </button> :
                                                                <button type="button" className="dropdown-item"
                                                                    disabled={item.isVerified}
                                                                    onClick={() => this.resendEmailInviteHandler(item.id)}>
                                                                    <i className="far fa-envelope"></i>Resend Invite
                                                                </button>
                                                            }
                                                            {jwt_decode(localStorage.token).defaultAccountAdmin ?
                                                                item.promotionInProgress ?
                                                                    <button type="button" className="dropdown-item">
                                                                        <i className="far fa-cog fa-spin"></i>Promotion to Owner In Progress
                                                                    </button> :
                                                                    <button
                                                                        type="button"
                                                                        className="dropdown-item"
                                                                        disabled={item.role !== 'ACCOUNT_ADMIN' || item.isDefault}
                                                                        onClick={() => this.promoteAsOwner(item.id)}>
                                                                        <i className="fas fa-user-plus"></i>
                                                                    </button> :
                                                                null
                                                            }
                                                            <button className="dropdown-item"
                                                                onClick={() => {
                                                                    this.props.fetchUserDetails(item.id)
                                                                    this.setState({ showAddOrEditUser: true, selectedUser: item.id, isChangingPassword: false, isDefaultUser: item.isDefault, errorMessage: '', isSavingUser: false });
                                                                }}
                                                                disabled={!item.internalUser || item.isDefault}
                                                            >
                                                                <i className="far fa-pencil"></i>Edit
                                                            </button>
                                                            <button className="dropdown-item"
                                                                disabled={!item.internalUser || item.email === loggedInUser || item.isDefault}
                                                                onClick={() => { this.confirmModalHandler(item.id); }}
                                                            >
                                                                <i className="far fa-trash-alt"></i>Delete
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="card-view-body">
                                                    <div className="card-view-icon">
                                                        <i className={item.isDefault ? "fad fa-user-crown" : item.role == "ACCOUNT_ADMIN" ? "fad fa-user-shield" : item.roleIcon ? item.roleIcon : "fad fa-user-tie"}></i>
                                                    </div>
                                                    <h6><i className="far fa-address-book"></i>{item.name ? item.name : "N/A"}</h6>
                                                    <p><i className="fad fa-users-medical"></i>{item.role === 'ACCOUNT_ADMIN' ? 'ALL' : item.userGroups ? item.userGroups : "-"}</p>
                                                    <p>
                                                        <i className="fad fa-envelope"></i>
                                                        <button className="btn-transparent btn-transparent-green copy-button float-none d-inline-block" data-tooltip data-tooltip-text="Copy" data-tooltip-place="bottom" onClick={() => this.copyToClipboard(item.email)}>
                                                            <i className="far fa-copy text-green"></i>
                                                        </button>
                                                        <a href={`mailto:${item.email}`} data-tooltip data-tooltip-text={item.email} data-tooltip-place="bottom">{item.email}</a>
                                                    </p>
                                                    <p>
                                                        <i className="fas fa-mobile-alt"></i>
                                                        {item.mobile ?
                                                            <span className="list-phone-input">
                                                                <PhoneInput
                                                                    containerClass="d-inline-block"
                                                                    inputProps={{
                                                                        name: 'phone',
                                                                    }}
                                                                    disabled={true}
                                                                    disableDropdown={true}
                                                                    value={item.mobile}
                                                                    onChange={(value) => null}
                                                                />
                                                            </span>
                                                        :  "-"
                                                        }
                                                    </p>
                                                </div>
                                                <div className="card-view-footer d-flex">
                                                    <div className="flex-50 p-3">
                                                        <h6>Created</h6>
                                                        <p><strong>By - </strong>{item.createdBy}</p>
                                                        <p><strong>At - </strong>{new Date(item.createdAt).toLocaleString('en-US', { createdAt: localStorage.createdAt })}</p>
                                                    </div>
                                                    <div className="flex-50 p-3">
                                                        <h6>Updated</h6>
                                                        <p><strong>By - </strong>{item.updatedBy}</p>
                                                        <p><strong>At - </strong>{new Date(item.updatedAt).toLocaleString('en-US', { createdAt: localStorage.createdAt })}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    )}
                                </ul>
                            :
                            <AddNewButton
                                text1="No users available"
                                text2="You haven't created any users yet"
                                createItemOnAddButtonClick={this.handleAddNewUserClick}
                                imageIcon="addUser.png"
                            />
                    }
                </div>

                {/* add new user */}
                <SlidingPane
                    className=''
                    overlayClassName='sliding-form'
                    closeIcon={<div></div>}
                    isOpen={this.state.showAddOrEditUser || false}
                    from='right'
                    width='500px'
                >
                    <div className="modal-content">
                        <div className="modal-header">
                            <h6 className="modal-title">{this.state.selectedUser ? "Update" : "Add New"} User
                                <button className="btn btn-light close" onClick={() => this.setState({ showAddOrEditUser: false, passwordMatched: true, errorMessage: '', email: "" })}>
                                    <i className="fas fa-angle-right"></i>
                                </button>
                            </h6>
                        </div>

                        {(this.props.isFetching || this.state.isSavingUser) ?
                            <div className="modal-loader">
                                <i className="fad fa-sync-alt fa-spin"></i>
                            </div> :
                            <form onSubmit={this.createOrEditUserHandler} >
                                <div className="modal-body">
                                    {this.state.errorMessage &&
                                        <React.Fragment>
                                            {Array.isArray(this.state.errorMessage) ?
                                                this.state.errorMessage.map((val, index) => (
                                                    <p className="modal-body-error" key={index}>{val}</p>
                                                )) :
                                                <p className="modal-body-error">{this.state.errorMessage}</p>
                                            }
                                        </React.Fragment>
                                    }

                                    <div className="d-flex">
                                        <div className="flex-50 pd-r-10">
                                            <div className="form-group">
                                                <label className="form-group-label">First Name :  <i className="fas fa-asterisk form-group-required"></i></label>
                                                <input type="text" name="firstName" className="form-control" maxLength="20" pattern="^[a-zA-Z]([\w\$-]*[a-zA-Z0-9])?$" required autoFocus
                                                    value={this.state.payload.firstName} disabled={this.state.isDefaultUser} onChange={this.onChangeHandler}
                                                />
                                            </div>
                                        </div>
                                        <div className="flex-50 pd-l-10">
                                            <div className="form-group">
                                                <label className="form-group-label">Last Name :  <i className="fas fa-asterisk form-group-required"></i></label>
                                                <input type="text" name="lastName" pattern="^[a-zA-Z]([\w\$-]*[a-zA-Z0-9])?$" maxLength="20" className="form-control" required
                                                    value={this.state.payload.lastName} disabled={this.state.isDefaultUser} onChange={this.onChangeHandler}
                                                />
                                            </div>
                                        </div>
                                    </div>

                                    <div className="form-group">
                                        <label className="form-group-label">Email :  <i className="fas fa-asterisk form-group-required"></i></label>
                                        <input type="email" name="email" className="form-control" required
                                            value={this.state.email} disabled={this.state.selectedUser && (this.state.payload.email === loggedInUser || this.state.isDefaultUser)} onChange={this.onChangeHandler}
                                        />
                                    </div>

                                    <div className="d-flex">
                                        <div className="flex-50 pd-r-10">
                                            <div className="form-group">
                                                <label className="form-group-label">Mobile No :</label>
                                                <div className="phone-input">
                                                    <PhoneInput
                                                        className="form-control"
                                                        inputProps={{
                                                            name: 'mobile',
                                                        }}
                                                        country={'us'}
                                                        disabled={this.state.isDefaultUser}
                                                        value={this.state.payload.mobile}
                                                        onChange={this.phoneNumberChangeHandler}
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="flex-50 pd-l-10">
                                            <div className="form-group">
                                                <label className="form-group-label">Role : <i className="fas fa-asterisk form-group-required"></i></label>
                                                <ReactSelect
                                                    name="roles"
                                                    className="form-control-multi-select"
                                                    isMulti={false}
                                                    options={this.getRoleMenuOptions(this.state.roles)}
                                                    value={this.getSelectedRoleForMenu(this.state.payload.roleId)}
                                                    onChange={this.handleChangeForRoleMenu}
                                                    isDisabled={this.state.selectedUser && (this.state.payload.email === jwt_decode(localStorage.token).sub || this.state.isDefaultUser)}
                                                >
                                                </ReactSelect>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="form-group">
                                        <label className="form-group-label">User Group :</label>
                                        <ReactSelect
                                            name="userGroups"
                                            className="form-control-multi-select"
                                            isMulti={false}
                                            isClearable={true}
                                            options={this.state.groups}
                                            placeholder={(this.state.payload.roleId && this.state.roles.find(temp => temp.id === this.state.payload.roleId).name === "ACCOUNT_ADMIN") ? "All" : "Select Group"}
                                            value={this.getselectedgroup(this.state.payload.userGroups[0])}
                                            onChange={this.handleChangeForUserGroups}
                                            isDisabled={this.state.roles.find(temp => temp.id === this.state.payload.roleId) && this.state.roles.find(temp => temp.id === this.state.payload.roleId).name == "ACCOUNT_ADMIN"}
                                        >
                                        </ReactSelect>
                                    </div>

                                    {/*<div className="form-group">
                                            <label className="form-label">Access Type:</label>
                                            <div className="alert alert-primary text-center">
                                                {this.state.accessType ? this.state.accessType === "BOTH" ? "PLATFORM | APPLICATION" : this.state.accessType : "N/A"}
                                            </div>
                                        </div>*/}

                                    {/*{JSON.parse(localStorage.isPlatformLogin) &&
                                            <div className="form-group">
                                                <label className="form-label">Group(s):</label>
                                                <ReactSelect
                                                    name="groups"
                                                    className="form-control-multi-select"
                                                    isMulti={true}
                                                    options={this.getGroupsMenuOptions()}
                                                    value={this.getSelectedGroupForMenu(this.state.payload.userGroups)}
                                                    onChange={this.handleChangeForGroupMenu}
                                                    isDisabled={this.state.selectedUser && (this.state.payload.email === jwt_decode(localStorage.token).sub)}
                                                >
                                                </ReactSelect>
                                            </div>
                                        }*/}

                                    {tenantType !== "FLEX" &&
                                        <div className="navigation-box form-group">
                                            <div className="d-flex">
                                                <h6 className="flex-50">Project</h6>
                                                <h6 className="flex-15">Admin</h6>
                                                <h6 className="flex-15">R/W</h6>
                                                <h6 className="flex-20">Source Control</h6>
                                            </div>
                                            <ul className="nav-list list-style-none">
                                                {this.state.assignedProjects.map((project, index) => {
                                                    return (
                                                        <li key={project.id}>
                                                            <div className="d-flex">
                                                                <div className="flex-50">
                                                                    <p>
                                                                        {project.repoName ?
                                                                            <i className="fab fa-github text-green mr-r-5"></i> :
                                                                            <i className="fab fa-github text-gray mr-r-5"></i>
                                                                        }
                                                                        {project.name}
                                                                    </p>
                                                                </div>
                                                                <div className="flex-15">
                                                                    <label className={this.checkDisableCondition("admin", project) ? "check-box check-box-disabled" : "check-box"} >
                                                                        <input type="checkbox" name="projectAccessAdmin" id={`checkbox_${project.id}_0`} value="admin" disabled={this.checkDisableCondition("admin", project)} checked={project.projectAndAccessPermission === 'admin'} onChange={(e) => this.onProjectChangeHandler(e, index)} />
                                                                        <span className="check-mark"></span>
                                                                    </label>
                                                                </div>
                                                                <div className="flex-15">
                                                                    <label className={this.checkDisableCondition("read_write", project) ? "check-box check-box-disabled" : "check-box"} >
                                                                        <input type="checkbox" name="projectAccessRW" id={`checkbox_${project.id}_1`} value="read_write" disabled={this.checkDisableCondition("read_write", project)} checked={project.projectAndAccessPermission === 'read_write'} onChange={(e) => this.onProjectChangeHandler(e, index)} />
                                                                        <span className="check-mark"></span>
                                                                    </label>
                                                                </div>
                                                                <div className="flex-20">
                                                                    <label className={this.checkDisableCondition("sourceControl", project) ? "check-box check-box-disabled" : "check-box"} >
                                                                        <input type="checkbox" name="sourceControl" id={`checkbox_${project.id}_2`} disabled={this.checkDisableCondition("sourceControl", project)} checked={project.sourceControlEnabled} onChange={(e) => this.onProjectChangeHandler(e, index)} />
                                                                        <span className="check-mark" />
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    )
                                                })}
                                            </ul>
                                        </div>
                                    }

                                    {this.state.selectedUser ?
                                        <div className="form-group">
                                            <label className={(this.state.payload.roleId && this.state.roles.find(temp => temp.id === this.state.payload.roleId).name === "ACCOUNT_ADMIN") ? "check-box check-box-disabled" : "check-box"}>
                                                <span className="check-text">Do you want to change password ?</span>
                                                <input type="checkbox" name="skipVerification" disabled={(this.state.payload.roleId && this.state.roles.find(temp => temp.id === this.state.payload.roleId).name === "ACCOUNT_ADMIN")} checked={this.state.isChangingPassword} onChange={() => this.setState({ isChangingPassword: event.target.checked })} />
                                                <span className="check-mark" />
                                            </label>
                                        </div>
                                        :
                                        <div className="form-group">
                                            <label className={this.state.payload.roleId !== null && this.state.roles.filter(role => role.id === this.state.payload.roleId)[0].name !== 'ACCOUNT_ADMIN' ? "check-box" : "check-box check-box-disabled"} >
                                                <span className="check-text">Do you want to skip email verification ?</span>
                                                <input type="checkbox" name="skipVerification" disabled={this.state.payload.roleId === null || this.state.roles.filter(role => role.id === this.state.payload.roleId)[0].name === 'ACCOUNT_ADMIN'} checked={this.state.payload.skipVerification} onChange={this.onChangeHandler} />
                                                <span className="check-mark" />
                                            </label>
                                        </div>
                                    }

                                    {this.state.isChangingPassword &&
                                        <React.Fragment>
                                            <div className="alert alert-info note-text">
                                                <p>Password must contain at least 1 lowercase character, 1 uppercase character, 1 numeric character, 1 special character. It must be of min 8 characters.</p>
                                            </div>
                                            <div className="form-group">
                                                <label className="form-group-label">New Password : <i className="fas fa-asterisk form-group-required"></i></label>
                                                <div className="input-group">
                                                    <input type={this.state.passwordType ? "text" : "password"} name="password" pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$^+=!*()@%&]).{6,15}$"
                                                        className="form-control" required onChange={this.onChangeHandler} value={this.state.payload.password}
                                                    />
                                                    <div className="input-group-append" onClick={() => this.setState((prevState) => { return { passwordType: !prevState.passwordType } })}>
                                                        <span className="input-group-text cursor-pointer"><i className={this.state.passwordType ? "far fa-eye-slash" : "far fa-eye"}></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </React.Fragment>
                                    }

                                    {this.state.payload.skipVerification &&
                                        <React.Fragment>
                                            <div className="alert alert-info note-text">
                                                <p>System generated mail will not be sent and user will be marked verified. This option can't be changed later.</p>
                                            </div>
                                            <div className="alert alert-info note-text">
                                                <p>Password must contain at least 1 lowercase character, 1 uppercase character, 1 numeric character, 1 special character. It must be of min 8 characters.</p>
                                            </div>
                                            <div className="form-group input-group">
                                                <label className="form-group-label">Password : <i className="fas fa-asterisk form-group-required"></i></label>
                                                <div className="input-group">
                                                    <input type={this.state.passwordType ? "text" : "password"} pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$^+=!*()@%&]).{6,15}$"
                                                        name="password" className="form-control" required autoComplete="new-password"
                                                        onChange={this.onChangeHandler} value={this.state.payload.password}
                                                    />
                                                    <div className="input-group-append" onClick={() => this.setState((prevState) => { return { passwordType: !prevState.passwordType } })}>
                                                        <span className="input-group-text cursor-pointer"><i className={this.state.passwordType ? "far fa-eye-slash" : "far fa-eye"}></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </React.Fragment>
                                    }
                                </div>
                                <div className="modal-footer justify-content-start">
                                    <button className="btn btn-primary" disabled={this.state.roles.length === 0 || this.state.isSavingUser || !this.state.payload.roleId}> {this.state.selectedUser ? "Update" : "Save"}</button>
                                    <button type="button" className="btn btn-dark" onClick={() => this.setState({ showAddOrEditUser: false, errorMessage: '', email: "" })}>Cancel</button>
                                </div>
                            </form>
                        }
                    </div>
                </SlidingPane>
                {/* end add new user */}

                {/* delete user modal */}
                <div className="modal show animated slideInDown" id="confirmModal">
                    <div className="modal-dialog modal-dialog-centered">
                        <div className="modal-content">
                            <div className="modal-body">
                                <div className="delete-content">
                                    <div className="delete-icon">
                                        <i className="fad fa-trash-alt text-red"></i>
                                    </div>
                                    <h4>Delete!</h4>
                                    <h6>Are you sure you want to delete this user ?</h6>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-dark" data-dismiss="modal" onClick={() => { this.setState({ selectedUser: '' }) }}>No</button>
                                <button type="button" className="btn btn-success" data-dismiss="modal" onClick={() => { this.setState({ isFetching: true }); this.props.deleteUser(this.state.selectedUser); }}>Yes</button>
                            </div>
                        </div>
                    </div>
                </div>
                {/* end delete user modal */}

                {this.state.isOpenModal &&
                    <NotificationModal
                        type={this.state.type}
                        message2={this.state.message2}
                        onCloseHandler={this.onCloseHandler}
                    />
                }

            </React.Fragment>
        )

    }
}

ManageUsers.defaultProps = {
    pageSize: 10,
    isFetching: false,
};

ManageUsers.propTypes = {
    pageSize: PropTypes.number,
    isFetching: PropTypes.bool,
};

const mapStateToProps = createStructuredSelector({
    usersList: Selectors.getUsersList(),
    usersListApiError: Selectors.getUsersApiError(),
    isFetching: Selectors.getIsFetching(),
    pageSize: Selectors.getSubscriberTablePageSize(),
    deleteUserSuccess: Selectors.getDeleteUserSuccess(),
    deleteUserFailure: Selectors.getDeleteUserFailure(),
    userDetails: Selectors.getUserDetails(),
    userDetailsError: Selectors.getUserDetailsError(),
    roles: Selectors.getRoles(),
    rolesError: Selectors.getRolesApiError(),
    groups: Selectors.getGroups(),
    getGroupsError: Selectors.getGroupsError(),
    updateUserSuccess: Selectors.getUserUpdateSuccess(),
    updateUserError: Selectors.getUserupdateError(),
    projects: Selectors.getOwnedProjectsSuccess(),
    projectsError: Selectors.getOwnedProjectsError(),
    developerQuotaSuccess: Selectors.developerQuotaSuccess(),
    developerQuotaFailure: Selectors.developerQuotaFailure(),
    resendInviteSuccess: Selectors.resendInviteSuccess(),
    resendInviteFailure: Selectors.resendInviteFailure(),
    promoteAsOwnerSuccess: Selectors.promoteAsOwnerSuccess(),
    promoteAsOwnerFailure: Selectors.promoteAsOwnerFailure(),
});

function mapDispatchToProps(dispatch) {
    return {
        dispatch,
        getManageUsersList: (payload) => dispatch(Actions.getManageUsersList(payload)),
        deleteUser: id => dispatch(Actions.deleteUser(id)),
        fetchRoles: () => dispatch(Actions.fetchRoles()),
        fetchGroups: () => dispatch(Actions.fetchGroups()),
        getOwnedProjects: () => dispatch(Actions.getOwnedProjects()),
        fetchUserDetails: id => dispatch(Actions.fetchUserDetails(id)),
        resetToInitialState: () => dispatch(Actions.resetToInitialState()),
        createOrEditUser: payload => dispatch(Actions.createOrEditUser(payload)),
        getDeveloperQuota: payload => dispatch(Actions.getDeveloperQuota(payload)),
        resendEmailInvite: id => dispatch(Actions.resendEmailInvite(id)),
        promoteAsOwner: id => dispatch(Actions.promoteAsOwner(id)),
    };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'manageUsers', reducer });
const withSaga = injectSaga({ key: 'manageUsers', saga });

export default compose(
    withReducer,
    withSaga,
    withConnect,
)(ManageUsers);

