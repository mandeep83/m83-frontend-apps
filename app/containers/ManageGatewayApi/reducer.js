/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { fromJS } from "immutable";
import * as CONSTANTS from "./constants";

export const initialState = fromJS({});

function manageGatewayApiReducer(state = initialState, action) {
  switch (action.type) {
    case CONSTANTS.RESET_TO_INITIAL_STATE:
      return initialState;
    case CONSTANTS.GET_API_LIST_SUCCESS:
      return Object.assign({}, state, {
        'apiListSuccess': action.response
      });
    case CONSTANTS.GET_API_LIST_FAILURE:
      return Object.assign({}, state, {
        'apiListError': action.error
      });
    case CONSTANTS.DELETE_API_BY_ID:
      return Object.assign({}, state, {
        'deleteApiError': undefined
      });
    case CONSTANTS.DELETE_API_SUCCESS:
      let temp = Object.assign({}, state, {
        'deleteApiSuccess': action.response
      })
      return temp;
    case CONSTANTS.DELETE_API_FAILURE:
      return Object.assign({}, state, {
        'deleteApiError': action.error
      });
    case CONSTANTS.INVOKE_API:
      return Object.assign({}, state, {
        'invokeApiError': undefined,
        'invokeApiSuccess': undefined
      });
    case CONSTANTS.INVOKE_API_SUCCESS:
      let response = {
        apiData: action.response,
        moduleName: action.moduleName,
        apiId: action.apiId,
        date: new Date()
      }
      return Object.assign({}, state, {
        'invokeApiSuccess': response
      })
    case CONSTANTS.INVOKE_API_FAILURE:
      return Object.assign({}, state, {
        'invokeApiError': {
          errorMessage: action.error ? action.error : "There was an error processing your request",
          moduleName: action.addOns.moduleName,
          apiId: action.addOns.apiId
        }
      });

    case CONSTANTS.CLONE_API_SUCCESS:
      return Object.assign({}, state, {
        'cloneApiSuccess': action.response
      });
    case CONSTANTS.CLONE_API_FAILURE:
      return Object.assign({}, state, {
        'cloneApiError': {
          error: action.error,
          moduleName: action.addOns.moduleName,
          apiId: action.addOns.apiId
        }
      });
    case CONSTANTS.GET_FAAS_ACTIONS_SUCCESS:
      return Object.assign({}, state, {
        'faaSListSuccess': action.response,
      });
    case CONSTANTS.GET_FAAS_ACTIONS_FAILURE:
      return Object.assign({}, state, {
        'faaSListFailure': action.error,
      });
    case CONSTANTS.CREATE_API_GATEWAY_SUCCESS:
      return Object.assign({}, state, {
        'apiGatewaySuccess': {
          response: action.response,
          date: new Date()
        }
      });
    case CONSTANTS.CREATE_API_GATEWAY_FAILURE:
      return Object.assign({}, state, {
        "apiGatewayFailure": {
          error: action.error,
          date: new Date()
        }
      });
    case CONSTANTS.GET_API_GATEWAY_BY_ID_SUCCESS:
      return Object.assign({}, state, {
        'getApiDetails': action.response,
      });
    case CONSTANTS.GET_API_GATEWAY_BY_ID_FAILURE:
      return Object.assign({}, state, {
        "getApiDetailsFailure": action.error,
      });
    case CONSTANTS.MULTIPLE_DELETE_API_SUCCESS:
      return Object.assign({}, state, {
        'multipleDeleteApiSuccess': action.response,
      });
    case CONSTANTS.MULTIPLE_DELETE_API_FAILURE:
      return Object.assign({}, state, {
        "multipleDeleteApiFailure": action.error,
      });
    default:
      return state;
  }
}

export default manageGatewayApiReducer;
