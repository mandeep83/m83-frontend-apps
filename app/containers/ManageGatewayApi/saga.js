/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { apiCallHandler } from '../../api';
import { takeEvery } from 'redux-saga';
import * as CONSTANTS from './constants';


export function* apiGetApiList(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_API_LIST_SUCCESS, CONSTANTS.GET_API_LIST_FAILURE, 'getApiList')];
}

export function* apiDeleteApiById(action) {
  yield [apiCallHandler(action, CONSTANTS.DELETE_API_SUCCESS, CONSTANTS.DELETE_API_FAILURE, 'deleteApiById')];
}

export function* apiInvokeApi(action) {
  yield [apiCallHandler(action, CONSTANTS.INVOKE_API_SUCCESS, CONSTANTS.INVOKE_API_FAILURE, 'invokeApi')];
}

export function* cloneAPIHandler(action) {
  yield [apiCallHandler(action, CONSTANTS.CLONE_API_SUCCESS, CONSTANTS.CLONE_API_FAILURE, 'cloneApi')];
}


export function* watcherGetApiList() {
  yield takeEvery(CONSTANTS.GET_API_LIST, apiGetApiList);
}

export function* watcherDeleteApiById() {
  yield takeEvery(CONSTANTS.DELETE_API_BY_ID, apiDeleteApiById);
}

export function* watcherInvokeApi() {
  yield takeEvery(CONSTANTS.INVOKE_API, apiInvokeApi);
}

export function* watcherCloneApi() {
  yield takeEvery(CONSTANTS.CLONE_API, cloneAPIHandler);
}


export function* getApiGateWayListAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_FAAS_ACTIONS_SUCCESS, CONSTANTS.GET_FAAS_ACTIONS_FAILURE, 'getApiGatewayUntouchedList')];
}

export function* saveApiGateWay(action) {
  yield [apiCallHandler(action, CONSTANTS.CREATE_API_GATEWAY_SUCCESS, CONSTANTS.CREATE_API_GATEWAY_FAILURE, 'createApiGateway')];
}

export function* getApiByIdHandler(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_API_GATEWAY_BY_ID_SUCCESS, CONSTANTS.GET_API_GATEWAY_BY_ID_FAILURE, 'getApiById')];
}

export function* multipleDeleteApiHandler(action) {
  yield [apiCallHandler(action, CONSTANTS.MULTIPLE_DELETE_API_SUCCESS, CONSTANTS.MULTIPLE_DELETE_API_FAILURE, 'multipleDeleteApi')];
}

export function* watcherGetApiGateWayListAsync() {
  yield takeEvery(CONSTANTS.GET_FAAS_ACTIONS, getApiGateWayListAsync);
}

export function* watcherSaveApiGateWay() {
  yield takeEvery(CONSTANTS.CREATE_API_GATEWAY, saveApiGateWay);
}

export function* watcherGetApiGatewayByIdAsync() {
  yield takeEvery(CONSTANTS.GET_API_GATEWAY_BY_ID, getApiByIdHandler);
}

export function* watcherMultipleDeleteApiAsync() {
  yield takeEvery(CONSTANTS.MULTIPLE_DELETE_API, multipleDeleteApiHandler);
}

export default function* rootSaga() {
  yield [watcherGetApiList(),
  watcherDeleteApiById(),
  watcherInvokeApi(),
  watcherCloneApi(),
  watcherSaveApiGateWay(),
  watcherGetApiGateWayListAsync(),
  watcherGetApiGatewayByIdAsync(),
  watcherMultipleDeleteApiAsync()
  ];
}
