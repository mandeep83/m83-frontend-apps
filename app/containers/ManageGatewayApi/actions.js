/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import * as CONSTANTS from "./constants";

export function getApiList() {
  return {
    type: CONSTANTS.GET_API_LIST
  };
}

export function deleteApiById(apiDetails) {
  return {
    type: CONSTANTS.DELETE_API_BY_ID,
    apiDetails
  };
}

export function invokeApi(uri, method, moduleName, apiId, payload) {
  return {
    type: CONSTANTS.INVOKE_API,
    uri,
    method,
    moduleName, apiId, payload
  };
}

export function cloneApi(apiId, moduleName) {
  return {
    type: CONSTANTS.CLONE_API,
    apiId, moduleName
  };
}

export function saveApiGateway(payload, id) {
  return {
    type: CONSTANTS.CREATE_API_GATEWAY,
    payload, id
  }
}

export function getApiGatewayByID(id) {
  return {
    type: CONSTANTS.GET_API_GATEWAY_BY_ID,
    id
  }
}

export function getFaaSActions() {
  return {
    type: CONSTANTS.GET_FAAS_ACTIONS
  }
}


export function resetToInitialState() {
  return {
    type: CONSTANTS.RESET_TO_INITIAL_STATE,
  };
}

export function multipleDeleteApi(payload, moduleName) {
  return {
    type: CONSTANTS.MULTIPLE_DELETE_API,
    payload,
    moduleName
  }
}