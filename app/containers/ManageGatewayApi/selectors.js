/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { createSelector } from "reselect";
import { initialState } from "./reducer";


const selectManageGatewayApiDomain = state =>
  state.get("manageGatewayApi", initialState);

export const apiListSuccess = () =>
    createSelector(selectManageGatewayApiDomain, subState => subState.apiListSuccess);

export const apiListError = () =>
    createSelector(selectManageGatewayApiDomain, subState => subState.apiListError);

export const deleteApiSuccess = () =>
    createSelector(selectManageGatewayApiDomain, subState => subState.deleteApiSuccess);

export const deleteApiError = () =>
    createSelector(selectManageGatewayApiDomain, subState => subState.deleteApiError);

export const invokeApiSuccess = () =>
    createSelector(selectManageGatewayApiDomain, subState => subState.invokeApiSuccess);

export const invokeApiError = () =>
    createSelector(selectManageGatewayApiDomain, subState => subState.invokeApiError);

export const cloneApiSuccess = () =>
    createSelector(selectManageGatewayApiDomain, subState => subState.cloneApiSuccess);

export const cloneApiError = () =>
    createSelector(selectManageGatewayApiDomain, subState => subState.cloneApiError);


export const getFaaSListSuccess = () =>
  createSelector(selectManageGatewayApiDomain, subState => subState.faaSListSuccess);

export const getFaaSListFailure = () =>
  createSelector(selectManageGatewayApiDomain, subState => subState.faaSListFailure);

export const apiGatewaySuccess = () =>
  createSelector(selectManageGatewayApiDomain, subState => subState.apiGatewaySuccess);

export const apiGatewayFailure = () =>
  createSelector(selectManageGatewayApiDomain, subState => subState.apiGatewayFailure);

export const getApiDetails = () =>
createSelector(selectManageGatewayApiDomain, subState => subState.getApiDetails);

export const getApiDetailsFailure = () =>
createSelector(selectManageGatewayApiDomain, subState => subState.getApiDetailsFailure);

export const multipleDeleteApiSuccess = () => 
createSelector(selectManageGatewayApiDomain, subState => subState.multipleDeleteApiSuccess);

export const multipleDeleteApiFailure = () => 
createSelector(selectManageGatewayApiDomain, subState => subState.multipleDeleteApiFailure);

export { selectManageGatewayApiDomain };
