/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import PropTypes from "prop-types";
import React from "react";
import { Helmet } from "react-helmet";
import JSONInput from "react-json-editor-ajrm/dist";
import { connect } from "react-redux";
import { compose } from "redux";
import { createStructuredSelector } from "reselect";
import injectReducer from "utils/injectReducer";
import injectSaga from "utils/injectSaga";
import ConfirmModel from "../../components/ConfirmModel";
import Loader from "../../components/Loader";
import NotificationModal from '../../components/NotificationModal/Loadable'
import * as Actions from "./actions";
import reducer from "./reducer";
import saga from "./saga";
import * as Selectors from "./selectors";
import ReactTooltip from "react-tooltip";
import SlidingPane from 'react-sliding-pane';
import Modal from 'react-modal';
import AddNewButton from "../../components/AddNewButton/Loadable";
import NoDataFoundMessage from "../../components/NoDataFoundMessage/Loadable"
import QuotaHeader from "../../components/QuotaHeader/Loadable";
export class ManageGatewayApi extends React.Component {
    state = {
        apiList: [],
        confirmState: false,
        isFetching: true,
        apiToBeDeleted: null,
        selectedApi: "",
        jsonErrorMessage: '',
        isExistingController: "create",
        payload: {
            basePath: "",
            path: "",
            requestMethod: "GET",
            description: "",
            actionName: "",
            moduleName: "",
            pathParameters: [],
            category: "customWidgets",
            existingModule: false,
            addOrEditGatewayApi: false,
        },
        allParameters: {
            description: '',
            in: 'Query',
            name: '',
            required: "true",
            type: 'boolean',
            defaultValue: ''
        },
        methods: [{
            method: "ALL",
            isDisabled: false
        }, {
            method: "GET",
            isDisabled: false
        }, {
            method: "POST",
            isDisabled: false
        }, {
            method: "PUT",
            isDisabled: false
        }, {
            method: "PATCH",
            isDisabled: false

        }, {
            method: "DELETE",
            isDisabled: false
        }],
        actionNames: [],
        moduleNames: [],
        isOpen: false,
        datatype: ['int', 'float', 'char', 'string'],
        isParamFormOpen: false,
        allRoles: [],
        validationMessage: '',
        localPathParams: [],
        multipleDelete: false,
        multipleDeletePayload: null,
        searchApi: '',
        searchModuleName: '',
        filteredApiList: [],
        // searchType: "moduleName",
        selectedModule: {},
        selectedApiMethod: "ALL"
    }

    componentWillMount() {
        Modal.setAppElement('body');
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.apiListSuccess && nextProps.apiListSuccess !== this.props.apiListSuccess) {
            let apiList = nextProps.apiListSuccess
            apiList.map(el => {
                el.deleteModule = false
                el.multipleDelete = false
                el.apis.map(api => api.multipleDelete = false)
            })
            this.setState({
                apiList,
                filteredApiList: apiList,
                moduleNames: apiList,
                selectedModule: apiList.length ? apiList[0] : {},
                isFetching: false,
                multipleDelete: false,
            }, () => this.filterListHandler())
        }

        if (nextProps.apiListError && nextProps.apiListError !== this.props.apiListError) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.apiListError,
                isFetching: false,
            })
        }

        if (nextProps.deleteApiSuccess && nextProps.deleteApiSuccess !== this.props.deleteApiSuccess) {
            let moduleNames = JSON.parse(JSON.stringify(this.state.moduleNames));

            let apiList = [...this.state.apiList];

            let moduleIndex = apiList.findIndex(el => el.moduleName === nextProps.deleteApiSuccess.response.moduleName);

            if (apiList[moduleIndex].apis.length > 1) {
                let apiIndex = apiList[moduleIndex].apis.findIndex(el => el.id === nextProps.deleteApiSuccess.response.apiId);
                apiList[moduleIndex].apis.splice(apiIndex, 1);
            } else {
                moduleNames = moduleNames.filter(val => val.moduleName !== nextProps.deleteApiSuccess.response.moduleName);
                apiList.splice(moduleIndex, 1)
            }

            this.setState((prevState) => ({
                moduleNames,
                apiList,
                modalType: "success",
                isOpen: true,
                message2: nextProps.deleteApiSuccess.message,
                isFetching: false,
                isDeletedSuccess: !prevState.isDeletedSuccess
            }), () => this.filterListHandler())
        }

        if (nextProps.deleteApiError && nextProps.deleteApiError !== this.props.deleteApiError) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.deleteApiError,
                isFetching: false,
            })
        }

        if (nextProps.invokeApiSuccess && nextProps.invokeApiSuccess !== this.props.invokeApiSuccess) {
            let apiList = [...this.state.apiList];

            let moduleIndex = apiList.findIndex(el => el.moduleName === nextProps.invokeApiSuccess.moduleName);
            let apiIndex = apiList[moduleIndex].apis.findIndex(el => el.id === nextProps.invokeApiSuccess.apiId);
            apiList[moduleIndex].apis[apiIndex].json = nextProps.invokeApiSuccess.apiData

            this.setState({
                apiList
            }, () => {
                this.filterListHandler()
            })
        }

        if (nextProps.invokeApiError && nextProps.invokeApiError !== this.props.invokeApiError) {
            let apiList = [...this.state.apiList];
            let moduleIndex = apiList.findIndex(el => el.moduleName === nextProps.invokeApiError.moduleName);
            let apiIndex = apiList[moduleIndex].apis.findIndex(el => el.id === nextProps.invokeApiError.apiId);

            apiList[moduleIndex].apis[apiIndex].jsonErrorMessage = nextProps.invokeApiError.errorMessage
            apiList[moduleIndex].apis[apiIndex].jsonLoading = false

            this.setState({
                apiList,
            }, () => {
                this.filterListHandler()
                this.props.resetToInitialState()
            })
        }

        if (nextProps.cloneApiSuccess && nextProps.cloneApiSuccess !== this.props.cloneApiSuccess) {
            let apiList = [...this.state.apiList]

            let moduleIndex = apiList.findIndex(el => el.moduleName === nextProps.cloneApiSuccess.moduleName);
            let apiIndex = apiList[moduleIndex].apis.findIndex(el => el.id === nextProps.cloneApiSuccess.apiId);
            apiList[moduleIndex].apis[apiIndex].isCloning = false;

            this.setState({
                modalType: "success",
                isOpen: true,
                message2: nextProps.cloneApiSuccess.message,
                apiList,
            }, () => {
                this.props.getApiList();
            })
        }

        if (nextProps.cloneApiError && nextProps.cloneApiError !== this.props.cloneApiError) {
            let apiList = [...this.state.apiList];

            let moduleIndex = apiList.findIndex(el => el.moduleName === nextProps.cloneApiError.moduleName);
            let apiIndex = apiList[moduleIndex].apis.findIndex(el => el.id === nextProps.cloneApiError.apiId);
            apiList[moduleIndex].apis[apiIndex].isCloning = false;

            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.cloneApiError.error,
                apiList,
            }, () => this.filterListHandler())
        }

        if (nextProps.getFaaSListSuccess && nextProps.getFaaSListSuccess !== this.props.getFaaSListSuccess) {
            let actionNames = nextProps.getFaaSListSuccess.filter(faas => !faas.developmentMode)
            this.setState({
                actionNames,
            })
        }

        if (nextProps.getFaaSListFailure && nextProps.getFaaSListFailure !== this.props.getFaaSListFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.getFaaSListFailure
            });
        }

        if (nextProps.getControllerListFailure && nextProps.getControllerListFailure !== this.props.getControllerListFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.getControllerListFailure
            });
        }

        if (nextProps.apiGatewaySuccess && nextProps.apiGatewaySuccess !== this.props.apiGatewaySuccess) {
            this.setState((prevState) => ({
                isOpen: true,
                modalType: "success",
                message2: nextProps.apiGatewaySuccess.response,
                isFetching: true,
                addOrEditIsFetching: false,
                addOrEditGatewayApi: false,
                selectedApi: "",
                validationMessage: '',
                isAddSuccess: !prevState.isAddSuccess,
            }), () => { this.props.getApiList(); });
        }

        if (nextProps.apiGatewayFailure && nextProps.apiGatewayFailure !== this.props.apiGatewayFailure) {
            this.setState({
                apiGatewayFailure: nextProps.apiGatewayFailure.error,
                addOrEditIsFetching: false
            }, () => {
                setTimeout(() => this.setState({ apiGatewayFailure: false }), 4000)
            });
        }

        if (nextProps.getApiDetails && nextProps.getApiDetails !== this.props.getApiDetails) {
            let payload = JSON.parse(JSON.stringify(this.state.payload))
            payload = nextProps.getApiDetails,
                payload.path = (payload.path).substring(1, payload.path.length)
            this.setState({
                addOrEditIsFetching: false,
                isExistingController: "existing",
                payload
            });
        }

        if (nextProps.getApiDetailsFailure && nextProps.getApiDetailsFailure !== this.props.getApiDetailsFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.getApiDetailsFailure,
                addOrEditIsFetching: false
            });
        }

        if (nextProps.multipleDeleteApiSuccess && nextProps.multipleDeleteApiSuccess !== this.props.multipleDeleteApiSuccess) {
            this.setState((previousState) => ({
                modalType: "success",
                isOpen: true,
                message2: nextProps.multipleDeleteApiSuccess.message,
                refetchQuota: !previousState.refetchQuota
            }))
        }

        if (nextProps.multipleDeleteApiFailure && nextProps.multipleDeleteApiFailure !== this.props.multipleDeleteApiFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.multipleDeleteApiFailure,
                isFetching: false,
            })
        }

    }

    confirmBox = (apiId, moduleName, name) => {
        this.setState({
            deleteName: name,
            apiToBeDeleted: { apiId, moduleName },
            confirmState: true,
            collapseModule: moduleName,
            multipleDelete: false
        })
    }

    invokeApi = (uri, method, moduleName, apiId, payload, pathParameters) => {
        let errorMessage = false;
        let apiList = [...this.state.apiList];
        let moduleIndex = apiList.findIndex(el => el.moduleName === moduleName);
        let apiIndex = apiList[moduleIndex].apis.findIndex(el => el.id === apiId);

        if (pathParameters.length > 0) {
            let iCounter = 1;
            pathParameters.map(param => {
                if (param.defaultValue) {
                    if (iCounter === 1) {
                        uri = `${uri}?`;
                        iCounter--;
                    }
                    if (iCounter < 0) {
                        uri = `${uri}&${param.name}=${param.defaultValue}`
                    } else {
                        uri = `${uri}${param.name}=${param.defaultValue}`
                        iCounter--;
                    }
                } else if (!param.defaultValue && param.required) {
                    param.errorMessage = true
                    errorMessage = true;
                }
            })
        }

        apiList[moduleIndex].apis[apiIndex].pathParameters = JSON.parse(JSON.stringify(pathParameters))
        if (!errorMessage) {
            apiList[moduleIndex].apis[apiIndex].json = null;
            apiList[moduleIndex].apis[apiIndex].jsonLoading = true;
        }
        this.setState({ apiList }, () => {
            this.filterListHandler();
            if (!errorMessage) {
                this.props.invokeApi(uri, method, moduleName, apiId, payload);
            }
        })
    }

    inputChangeHandler = event => {
        let payload = JSON.parse(JSON.stringify(this.state.payload));
        let validationMessage = this.state.validationMessage,
            regexPath = /^[^*|\":<>[\]{}`\\()';@&$?!#%^+=,.~\ ]+$/,
            isExistingController = this.state.isExistingController,
            methods = JSON.parse(JSON.stringify(this.state.methods))

        if (event.target.id === "moduleName" && this.state.isExistingController === "existing") {
            if (event.target.value) {
                let moduleObj = this.state.moduleNames.find(temp => temp.moduleName === event.target.value);
                payload.moduleDescription = moduleObj.moduleDescription;
                payload.basePath = moduleObj.apis[0].basePath;
            } else {
                payload.moduleDescription = '';
                payload.basePath = '';
            }
        }

        if (event.target.id === "createEditRadio") {
            payload.moduleDescription = "";
            payload.basePath = "";
            payload.moduleName = "";
            isExistingController = event.target.value
        } else if (event.target.id === "basePath") {
            if (regexPath.test(event.target.value) || /^$/.test(event.target.value)) {
                payload[event.target.id] = event.target.value;
            }
        } else if (event.target.id === "path") {
            if (regexPath.test(event.target.value) || /^$/.test(event.target.value)) {
                if (event.target.value[0] === "/") {
                    validationMessage = "API path can not be started with slash(/)"
                } else {
                    payload[event.target.id] = event.target.value;
                    validationMessage = ""
                }
            }
        } else if (event.target.id === "category") {
            payload.actionName = ""
            payload[event.target.id] = event.target.value;
            if (event.target.value === "other" || event.target.value === "customWidgets" || event.target.value === "utilities")
                methods.forEach((item) => item.isDisabled = false);
            else {
                payload.requestMethod = "GET";
                methods.forEach((options) => {
                    if (options.method === "GET") options.isDisabled = false;
                    else options.isDisabled = true;
                });
            }
        } else {
            payload[event.target.id] = event.target.value;
        }
        this.setState({ payload, isExistingController, validationMessage, methods });
    };

    submitHandler = (event) => {
        event.preventDefault();
        let payload = JSON.parse(JSON.stringify(this.state.payload))
        payload.pathParameters = JSON.parse(JSON.stringify(this.state.localPathParams))
        payload.existingModule = this.state.isExistingController == "existing" ? true : false;
        this.setState({
            addOrEditIsFetching: true,
        }, () => {
            this.props.saveApiGateway(payload, this.state.selectedApi)
        })
    }

    onCloseHandler = () => {
        this.setState({
            isOpen: false,
            message2: '',
            modalType: "",
        })
    }

    cancelClicked = () => {
        this.setState({
            deleteName: "",
            apiToBeDeleted: null,
            confirmState: false,
            multipleDeletePayload: null,
        })
    }

    colorClass = (apiType) => {
        switch (apiType.toUpperCase()) {
            case "PUT":
                return "warning"
            case "POST":
                return "success"
            case "GET":
                return "primary"
            case "PATCH":
                return "info"
            default:
                return "danger"
        }
    }

    tryModeInputHandler = (event, moduleName, apiId, paramIndex, type) => {
        let apiList = JSON.parse(JSON.stringify(this.state.apiList));
        let moduleIndex = apiList.findIndex(el => el.moduleName === moduleName);
        let apiIndex = apiList[moduleIndex].apis.findIndex(el => el.id === apiId);
        let regexInt = /^[0-9\b]+$/,
            regexFloat = /^[+-]?([0-9]+([.][0-9]*)?|[.][0-9]+)$/,
            regexString = /^[a-zA-Z0-9!@#$%^&*()_+\-=\[\]{};': "\\|,.<>\/?]*$/,
            localPathParams = JSON.parse(JSON.stringify(this.state.localPathParams));

        apiList[moduleIndex].apis[apiIndex].errorMessage = false;
        localPathParams[paramIndex].errorMessage = false;
        if (type === "int" && (regexInt.test(event.target.value) || event.target.value === "")) {
            localPathParams[paramIndex].defaultValue = event.target.value;
        } else if (type === "float" && (regexFloat.test(event.target.value) || event.target.value === "")) {
            localPathParams[paramIndex].defaultValue = event.target.value;
        } else if (type === "String" && (regexString.test(event.target.value) || event.target.value === "")) {
            localPathParams[paramIndex].defaultValue = event.target.value;
        } else if (type === "char" && (regexString.test(event.target.value) || event.target.value === "")) {
            localPathParams[paramIndex].defaultValue = event.target.value;
        } else if (type === "Boolean") {
            localPathParams[paramIndex].defaultValue = event.target.value;
        }
        this.setState({ localPathParams, apiList }, () => this.filterListHandler())
    }

    tryItOut = (apiId, moduleName, type) => {
        let apiList = JSON.parse(JSON.stringify(this.state.apiList)), localPathParams;
        let moduleIndex = apiList.findIndex(el => el.moduleName === moduleName);
        let apiIndex = apiList[moduleIndex].apis.findIndex(el => el.id === apiId);
        apiList[moduleIndex].apis[apiIndex].json = "";
        apiList[moduleIndex].apis[apiIndex].jsonLoading = false;

        if ((apiList[moduleIndex].apis[apiIndex].requestMethod.toUpperCase() === "POST" || apiList[moduleIndex].apis[apiIndex].requestMethod.toUpperCase() === "PATCH" || apiList[moduleIndex].apis[apiIndex].requestMethod.toUpperCase() === "PUT"))
            apiList[moduleIndex].apis[apiIndex].payload = {};

        localPathParams = JSON.parse(JSON.stringify(apiList[moduleIndex].apis[apiIndex].pathParameters));

        this.setState({
            apiList,
            localPathParams,
            tryMode: type === "close" ? null : apiId
        }, () => this.filterListHandler())
    }

    rawChangeHandler = (apiId, moduleName) => (rawObject) => {
        let apiList = JSON.parse(JSON.stringify(this.state.apiList));
        let moduleIndex = apiList.findIndex(el => el.moduleName === moduleName);
        let apiIndex = apiList[moduleIndex].apis.findIndex(el => el.id === apiId);

        apiList[moduleIndex].apis[apiIndex].payload = rawObject.jsObject;

        this.setState({
            apiList,
        }, () => this.filterListHandler())
    };

    handleCloneAPI = (apiId, moduleName) => {
        let apiList = JSON.parse(JSON.stringify(this.state.apiList)),
            filteredApiList = JSON.parse(JSON.stringify(this.state.filteredApiList));
        let moduleIndex = apiList.findIndex(el => el.moduleName === moduleName);
        let apiIndex = apiList[moduleIndex].apis.findIndex(el => el.id === apiId);
        apiList[moduleIndex].apis[apiIndex].isCloning = true

        this.setState({
            isOpen: false,
            collapseModule: moduleName,
            apiList,
            filteredApiList
        }, () => {
            this.filterListHandler();
            this.props.cloneApi(apiId, moduleName)
        });
    }

    resetIndex = (module) => ({ target }) => {
        let apiList = JSON.parse(JSON.stringify(this.state.apiList));
        module.moduleName !== this.state.selectedModule.moduleName && apiList.map(el => {
            el.deleteModule = false
            el.multipleDelete = false
            el.apis.map(api => api.multipleDelete = false)
        })
        this.setState((prevState) => ({
            selectedModule: module,
            apiList
        }), () => {
            this.filterListHandler();
        })
    }

    setLocalPathParams = (moduleName, apiId) => {
        let localPathParams = [], filteredApiList = []
        if (this.state.searchApi.trim() === "")
            filteredApiList = [...this.state.apiList]
        else
            filteredApiList = [...this.state.filteredApiList]
        filteredApiList.find(module => {
            if (module.moduleName === moduleName) {
                module.apis.find(api => {
                    if (api.id === apiId) {
                        localPathParams = api.pathParameters
                        this.setState((prevState) => ({
                            localPathParams,
                            selectedTryApi: prevState.selectedTryApi == api.id ? '' : api.id
                        }))
                    }
                })
            }
        })
    }

    selectedApiDelete = (moduleName, apiId) => (event) => {
        let apiList = JSON.parse(JSON.stringify(this.state.apiList));
        let moduleIndex = apiList.findIndex(el => el.moduleName === moduleName);

        apiList[moduleIndex].apis.map(temp => {
            if (temp.id == apiId) {
                temp.multipleDelete = event.target.checked;
            }
            return temp;
        })

        apiList[moduleIndex].deleteModule = apiList[moduleIndex].apis.every(temp => temp.multipleDelete)
        apiList[moduleIndex].multipleDelete = apiList[moduleIndex].apis.some(temp => temp.multipleDelete);

        this.setState({
            apiList,
            deleteModule: apiList[moduleIndex].deleteModule
        }, () => {
            this.filterListHandler();
        })
    }

    deleteModule = () => {
        let apiList = [...this.state.apiList];
        let moduleIndex = apiList.findIndex(el => el.moduleName === this.state.selectedModule.moduleName);
        let multipleDeletePayload = {
            moduleName: apiList[moduleIndex].moduleName
        }
        apiList[moduleIndex].deleteModule = true
        apiList[moduleIndex].multipleDelete = true
        this.setState({
            apiList,
            deleteModule: apiList[moduleIndex].deleteModule,
            confirmState: true,
            multipleDeletePayload,
            multipleDelete: apiList[moduleIndex].multipleDelete
        })
    }

    changeCheckBoxModule = (module) => (event) => {
        event.stopPropagation()
        let apiList = JSON.parse(JSON.stringify(this.state.apiList));
        let moduleIndex = apiList.findIndex(el => el.moduleName === module.moduleName);
        apiList.map(el => {
            el.deleteModule = false
            el.apis.map(temp => temp.multipleDelete = false)
            el.multipleDelete = el.apis.some(temp => temp.multipleDelete);
            if (el.moduleName === module.moduleName) {
                el.apis.map(temp => temp.multipleDelete = event.currentTarget.checked)
                el.deleteModule = event.currentTarget.checked;
                el.multipleDelete = el.apis.some(temp => temp.multipleDelete);
            }
        })
        this.setState({
            apiList,
            selectedModule: module
        }, () => {
            this.filterListHandler();
        })
    }

    deleteMultipleApis = () => {
        let apiList = JSON.parse(JSON.stringify(this.state.apiList))
        let moduleIndex = apiList.findIndex(el => el.multipleDelete);
        let multipleDeletePayload = {
            ids: apiList[moduleIndex].apis.filter(item => item.multipleDelete).map(obj => obj.id)
        }
        this.setState({
            confirmState: true,
            multipleDeletePayload,
            multipleDelete: apiList[moduleIndex].multipleDelete
        })
    }

    filterListHandler = (searchType) => {
        let filteredApiList = JSON.parse(JSON.stringify(this.state.apiList)),
            selectedModule = { ...this.state.selectedModule },
            searchApi = this.state.searchApi,
            searchModuleName = this.state.searchModuleName;
        if (searchType === "requestUri" && searchApi) {
            filteredApiList = filteredApiList.filter(module => {
                module.apis = module.apis.filter(api => api.requestUri.toLowerCase().includes(searchApi.toLowerCase()));
                return module.apis.length > 0
            })
            searchModuleName = ''
        } else if (searchType === "moduleName" && searchModuleName) {
            filteredApiList = filteredApiList.filter(module => module.moduleName.toLowerCase().includes(searchModuleName.toLowerCase()))
            searchApi = ''
        }

        selectedModule = filteredApiList.length ?
            selectedModule.moduleName ?
                filteredApiList.find(module => module.moduleName.toLowerCase() === selectedModule.moduleName.toLowerCase()) ?
                    filteredApiList.find(module => module.moduleName.toLowerCase() === selectedModule.moduleName.toLowerCase()) :
                    filteredApiList[0] : filteredApiList[0] : {}
        this.setState({
            filteredApiList,
            selectedModule,
            searchApi,
            searchModuleName
        })
    }

    searchChangeHandler = (searchType) => ({ currentTarget }) => {
        this.setState({
            [currentTarget.id]: currentTarget.value
        }, () => {
            this.filterListHandler(searchType);
        })
    }

    emptySearchBox = () => {
        let filteredApiList = JSON.parse(JSON.stringify(this.state.apiList)),
            selectedModule = JSON.parse(JSON.stringify(this.state.selectedModule))
        selectedModule = selectedModule.moduleName ?
            selectedModule :
            filteredApiList.length ? filteredApiList[0] : {}
        this.setState({
            searchApi: "",
            searchModuleName: "",
            filteredApiList,
            selectedTryApi: "",
            selectedModule
        })
    }

    isExecutionDisabled = (api) => {
        let needsPayload = (api.requestMethod.toUpperCase() === "POST" || api.requestMethod.toUpperCase() === "PATCH" || api.requestMethod.toUpperCase() === "PUT");
        if (needsPayload) {
            if (!api.payload || Object.keys(api.payload).length == 0) {
                return true
            }
            return false
        }
        return false
    }

    getFilteredActions = () => {
        let filteredActions = this.state.actionNames
        return filteredActions
    }

    closeModalHandler = () => {
        this.setState({
            addOrEditGatewayApi: false,
            selectedRoleId: '',
            selectedApi: "",
            errorMessage: null
        })
    }

    addButtonHandler = () => {
        this.props.getFaaSActions();
        this.setState({
            addOrEditGatewayApi: true,
            apiGatewayFailure: false,
            payload: {
                basePath: "",
                path: "",
                requestMethod: "GET",
                description: "",
                actionName: "",
                moduleName: "",
                pathParameters: [],
                existingModule: false
            },
            validationMessage: ''
        })
    }

    refreshComponent = () => {
        this.props.getApiList();
        this.props.getFaaSActions();
        this.setState({
            apiList: [],
            confirmState: false,
            isFetching: true,
            apiToBeDeleted: null,
            selectedApi: "",
            jsonErrorMessage: '',
            isExistingController: "create",
            payload: {
                basePath: "",
                path: "",
                requestMethod: "GET",
                description: "",
                actionName: "",
                moduleName: "",
                pathParameters: [],
                category: "customWidgets",
                existingModule: false,
                addOrEditGatewayApi: false,
            },
            actionNames: [],
            moduleNames: [],
            isOpen: false,
            isParamFormOpen: false,
            allRoles: [],
            validationMessage: '',
            localPathParams: [],
            multipleDelete: false,
            multipleDeletePayload: null,
            searchApi: '',
            searchModuleName: '',
            filteredApiList: [],
            selectedModule: {},
            selectedApiMethod: "ALL"
        })
    }

    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>APIs</title>
                    <meta name="description" content="M83-APIGateway" />
                </Helmet>

                <QuotaHeader
                    addButtonText="Add API"
                    showQuota
                    componentName="APIs"
                    productName="api"
                    onAddClick={this.addButtonHandler}
                    refreshDisabled={this.state.isFetching}
                    addDisabled={this.state.isFetching}
                    refreshHandler={this.refreshComponent}
                    callBack={() => this.props.getApiList()}
                    isAddSuccess={this.state.isAddSuccess}
                    refetchQuota={this.state.refetchQuota}
                    isDeletedSuccess={this.state.isDeletedSuccess}
                    {...this.props}
                />

                {this.state.isFetching ?
                    <Loader /> :
                    this.state.apiList.length > 0 ?
                        <div className="content-body p-0">
                            <div className="api-wrapper">
                                <div className="api-left-wrapper">
                                    <div className="form-group search-wrapper">
                                        {/* {this.state.filteredApiList.filter(module => module.deleteModule).length ? */}
                                        <div className="search-wrapper-group animated zoomIn">
                                            <input type="text" className="form-control" placeholder="Search..." id="searchModuleName" value={this.state.searchModuleName} onChange={this.searchChangeHandler("moduleName")} />
                                            <span className="search-wrapper-icon"><i className="far fa-search"></i></span>
                                            <button type='button' className="search-wrapper-button" onClick={this.emptySearchBox}><i className="far fa-times"></i></button>
                                        </div>
                                        {/* } */}
                                    </div>

                                    <ul className="list-style-none">
                                        {this.state.filteredApiList.length ?
                                            this.state.filteredApiList.map((module, moduleIndex) => {
                                                return (
                                                    <li key={moduleIndex} onClick={this.resetIndex(module)} className={this.state.selectedModule.moduleName === module.moduleName ? "active" : ""}>
                                                        <h6>{module.moduleName}</h6>
                                                        <p className="text-content f-11 m-0">{`${module.moduleDescription || "N/A"}`}</p>
                                                        <i className="far fa-angle-right f-15 text-content"></i>
                                                    </li>
                                                )
                                            }
                                            ) :
                                            <div className="inner-message-wrapper h-100">
                                                <div className="inner-message-content">
                                                    <i className="fad fa-file-exclamation"></i>
                                                    <h6>There is no data to display.</h6>
                                                </div>
                                            </div>
                                        }
                                    </ul>
                                </div>

                                <div className="api-content-wrapper">
                                    <div className="alert alert-dark note-text">
                                        <p><strong>Name : </strong>{this.state.selectedModule.moduleName}<span className="ml-3 mr-3">|</span> <strong>Description : </strong>{this.state.selectedModule.moduleDescription || "N/A"}</p>
                                        {this.state.filteredApiList.length == 0 || this.state.filteredApiList.find(module => module.moduleName === this.state.selectedModule.moduleName) && (this.state.filteredApiList.find(module => module.moduleName === this.state.selectedModule.moduleName).apis.filter(api => api.multipleDelete).length === 0 || this.state.filteredApiList.find(module => module.moduleName === this.state.selectedModule.moduleName).apis.filter(api => api.multipleDelete).length === this.state.selectedModule.apis.length) ?
                                            <button className="btn btn-link float-right" disabled={this.state.filteredApiList.length == 0} onClick={this.deleteModule}><i className="far fa-trash-alt"></i>Delete Module</button>
                                            :
                                            <button className="btn btn-link float-right"
                                                disabled={(this.state.filteredApiList.find(module => module.moduleName === this.state.selectedModule.moduleName) && (this.state.filteredApiList.find(module => module.moduleName === this.state.selectedModule.moduleName).apis.filter(api => api.multipleDelete).length === 0) || this.state.filteredApiList.find(module => module.deleteModule))}
                                                onClick={() => this.deleteMultipleApis()}>
                                                <i className="far fa-trash-alt"></i>Delete Selected ({this.state.filteredApiList.find(module => module.moduleName === this.state.selectedModule.moduleName) ? this.state.filteredApiList.find(module => module.moduleName === this.state.selectedModule.moduleName).apis.filter(api => api.multipleDelete).length : 0})
                                            </button>
                                        }
                                    </div>

                                    <div className="action-tabs mr-b-10 position-relative">
                                        <ul className="nav nav-tabs list-style-none d-flex">
                                            {this.state.methods.map((methodObj, index) => {
                                                return (
                                                    <li key={index} className={`"nav-item ${this.state.selectedApiMethod === methodObj.method ? "active" : ''}`} data-target={`#${methodObj.method}`} data-toggle="tab" onClick={() => this.setState({ selectedApiMethod: methodObj.method })}>
                                                        <a className="nav-link">{methodObj.method}
                                                            <span className="badge badge-pill badge-light">{this.state.selectedModule.apis ? this.state.selectedModule.apis.filter(api => api.requestMethod === methodObj.method || methodObj.method === "ALL").length : "0"}</span>
                                                        </a>
                                                    </li>
                                                )
                                            })
                                            }
                                        </ul>
                                        <div className="form-group search-wrapper">
                                            <div className="search-wrapper-group">
                                                <input type="text" className="form-control" placeholder="Search..." id="searchApi" value={this.state.searchApi} onChange={this.searchChangeHandler("requestUri")} />
                                                <button type='button' className="search-wrapper-button"><i className="far fa-search"></i></button>
                                            </div>
                                        </div>
                                    </div>

                                    {this.state.methods.map((methodObj, index) => {
                                        if (this.state.selectedApiMethod === methodObj.method)
                                            return (
                                                <div className="tab-content" key={index}>
                                                    <div className={`tab-pane fade show active`} id={methodObj.method}>
                                                        <ul className="list-style-none api-list-view">
                                                            {this.state.filteredApiList.find(module => module.moduleName === this.state.selectedModule.moduleName) ? this.state.filteredApiList.find(module => module.moduleName === this.state.selectedModule.moduleName).apis.filter(api => api.requestMethod === this.state.selectedApiMethod || this.state.selectedApiMethod === "ALL").map((api, apiIndex) => {
                                                                return (
                                                                    <li key={apiIndex}>
                                                                        <div className={`alert alert-${this.colorClass(api.requestMethod)}`}>
                                                                            <label className="check-box">
                                                                                <input type="checkbox"
                                                                                    id={`api${apiIndex}`}
                                                                                    // value={api.multipleDelete}
                                                                                    disabled={api.isAttached}
                                                                                    checked={api.multipleDelete}
                                                                                    onChange={this.selectedApiDelete(this.state.selectedModule.moduleName, api.id)} />
                                                                                <span className="check-mark"></span>
                                                                            </label>
                                                                            <span className={`badge badge-pill badge-${this.colorClass(api.requestMethod)}`}>{api.requestMethod}</span>
                                                                            <h6>
                                                                                <strong className="collapsed" data-toggle="collapse" data-target={"#apiCollapse" + apiIndex}
                                                                                    onClick={() => this.setState((prevState) => ({ selectedTryApi: api.id, }))}>
                                                                                    <i className="fas fa-caret-up"></i>{"/api/faas/gateway" + api.apiPath}
                                                                                </strong>
                                                                                <span className="ml-3 mr-3 text-content">|</span>
                                                                                <strong onClick={() => { this.props.history.push(`/addOrEditFaaS/${api.actionId}`) }}>{api.actionName}()</strong>
                                                                            </h6>
                                                                            <p><strong>Description :</strong>{api.description || "N/A"}</p><p>
                                                                                <strong>Created At :</strong>{new Date(api.createdAt).toLocaleString('en-US', { timeZone: localStorage.timeZone })}<span>|</span>
                                                                                <strong>Created By :</strong>{api.createdBy || "N/A"}<span>|</span>
                                                                                <strong>Updated At :</strong>{new Date(api.updatedAt).toLocaleString('en-US', { timeZone: localStorage.timeZone })}<span>|</span>
                                                                                <strong>Updated By :</strong>{api.updatedBy || "N/A"}
                                                                            </p>
                                                                            <div className="button-group">
                                                                                {/* <button className="btn btn-light text-green" data-tooltip data-tooltip-text="Clone" disabled={api.isCloning}
                                                                                    onClick={() => {
                                                                                        this.handleCloneAPI(api.id, this.state.selectedModule.moduleName)
                                                                                    }} data-tooltip-place="bottom"><i className="far fa-copy"></i></button> */}
                                                                                <button className="btn btn-light text-primary" data-tooltip data-tooltip-text="Edit" onClick={() => {
                                                                                    this.setState({
                                                                                        selectedApi: api.id,
                                                                                        addOrEditGatewayApi: true,
                                                                                        addOrEditIsFetching: true,
                                                                                    }, () => { this.props.getFaaSActions(); this.props.getApiGatewayByID(api.id) })
                                                                                }} data-tooltip-place="bottom"><i className="far fa-pencil"></i></button>
                                                                                <button className="btn btn-light text-red" data-tooltip data-tooltip-text="Delete" disabled={api.isCloning || api.multipleDelete || api.isAttached || (api.createdByUserId !== localStorage["userId"] && localStorage.role !== "ACCOUNT_ADMIN")}
                                                                                    onClick={() => this.confirmBox(api.id, this.state.selectedModule.moduleName, `selected api with path "${api.requestUri}"`)} data-tooltip-place="bottom"><i className="far fa-trash-alt"></i></button>
                                                                            </div>
                                                                        </div>

                                                                        <div className="collapse" id={"apiCollapse" + apiIndex}>
                                                                            {api.id === this.state.selectedTryApi &&
                                                                                <div className="alert alert-primary api-detail-wrapper">
                                                                                    <div className="d-flex">
                                                                                        <div className="flex-65">
                                                                                            <div className="content-table">
                                                                                                <div className="api-detail-header">
                                                                                                    <p className="text-gray m-0 f-12 fw-600">Parameters</p>
                                                                                                    {this.state.tryMode !== api.id &&
                                                                                                        <span>
                                                                                                            "Click here to execute API"
                                                                                                        <button className="btn btn-light" onClick={() => this.tryItOut(api.id, this.state.selectedModule.moduleName, "open")}>Try it out</button>
                                                                                                        </span>
                                                                                                    }
                                                                                                </div>
                                                                                                <table className="table table-bordered bg-white mb-0">
                                                                                                    <thead>
                                                                                                        <tr>
                                                                                                            <th width="30%">Name</th>
                                                                                                            <th width="20%">Type</th>
                                                                                                            <th width="50%">{this.state.tryMode === api.id ? "Value" : "Description"}</th>
                                                                                                        </tr>
                                                                                                    </thead>
                                                                                                    <tbody>
                                                                                                        <tr>
                                                                                                            <td className="fw-600">X-Authorization<sup><i className="fas fa-asterisk f-8 text-red"></i></sup></td>
                                                                                                            <td className="text-primary fw-400i">string</td>
                                                                                                            <td>
                                                                                                                {this.state.tryMode === api.id ?
                                                                                                                    <textarea
                                                                                                                        className="form-control"
                                                                                                                        rows="5"
                                                                                                                        readOnly
                                                                                                                        defaultValue={"Bearer " + localStorage.token}></textarea> :
                                                                                                                    <span>Bearer Access Token required for authentication.</span>
                                                                                                                }
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td className="fw-600">x-project-id<sup><i className="fas fa-asterisk f-8 text-red"></i></sup></td>
                                                                                                            <td className="text-primary fw-400i">string</td>
                                                                                                            <td>
                                                                                                                {this.state.tryMode === api.id ?
                                                                                                                    <input className="form-control"
                                                                                                                        readOnly
                                                                                                                        defaultValue={localStorage.selectedProjectId} /> :
                                                                                                                    <span>x-project-id</span>
                                                                                                                }
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        {(api.requestMethod.toUpperCase() === "POST" || api.requestMethod.toUpperCase() === "PATCH" || api.requestMethod.toUpperCase() === "PUT") &&
                                                                                                            <tr>
                                                                                                                <td className="fw-600">Payload<sup><i className="fas fa-asterisk f-8 text-red"></i></sup></td>
                                                                                                                <td className="text-primary fw-400i">string</td>
                                                                                                                <td>
                                                                                                                    {this.state.tryMode === api.id ?
                                                                                                                        <div className="json-input-box">
                                                                                                                            <JSONInput
                                                                                                                                viewOnly={this.state.tryMode !== api.id}
                                                                                                                                waitAfterKeyPress={5000}
                                                                                                                                placeholder={api.payload ? api.payload : null}
                                                                                                                                onChange={this.rawChangeHandler(api.id, this.state.selectedModule.moduleName)}
                                                                                                                            />
                                                                                                                        </div> :
                                                                                                                        <span className="text-note">Click "Try it out" to Enter Payload</span>
                                                                                                                    }
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        }
                                                                                                        {api.pathParameters != null && this.state.localPathParams.map((parameter, paramIndex) => {
                                                                                                            return (
                                                                                                                <tr key={paramIndex}>
                                                                                                                    <td className="fw-600">{parameter.name}<sup>{parameter.required ? <i className="fas fa-asterisk f-8 text-red"></i> : null}</sup></td>
                                                                                                                    <td className="text-primary fw-400i">{parameter.type}</td>
                                                                                                                    <td>{this.state.tryMode === api.id ?
                                                                                                                        parameter.type === "Boolean" ?
                                                                                                                            <div className="d-flex">
                                                                                                                                <div className="flex-33 pr-2">
                                                                                                                                    <label className="radio-button">
                                                                                                                                        <span className="radio-button-text">True</span>
                                                                                                                                        <input type="radio" id="required" value="true" checked={parameter.defaultValue === "true"}
                                                                                                                                            onChange={(e) => this.tryModeInputHandler(e, this.state.selectedModule.moduleName, api.id, paramIndex, parameter.type)}
                                                                                                                                        />
                                                                                                                                        <span className="radio-button-mark" />
                                                                                                                                    </label>
                                                                                                                                </div>
                                                                                                                                <div className="flex-33 pl-2">
                                                                                                                                    <label className="radio-button">
                                                                                                                                        <span className="radio-button-text">False</span>
                                                                                                                                        <input type="radio" id="required" value="false" checked={parameter.defaultValue === "false"}
                                                                                                                                            onChange={(e) => this.tryModeInputHandler(e, this.state.selectedModule.moduleName, api.id, paramIndex, parameter.type)}
                                                                                                                                        />
                                                                                                                                        <span className="radio-button-mark" />
                                                                                                                                    </label>
                                                                                                                                </div>
                                                                                                                                {parameter.errorMessage ?
                                                                                                                                    <div className="flex-item fx-b100">
                                                                                                                                        <h6 className="text-red">Please fill out this field.</h6>
                                                                                                                                    </div> : null
                                                                                                                                }
                                                                                                                            </div> :
                                                                                                                            <React.Fragment>
                                                                                                                                <input
                                                                                                                                    className="form-control"
                                                                                                                                    onChange={(e) => this.tryModeInputHandler(e, this.state.selectedModule.moduleName, api.id, paramIndex, parameter.type)}
                                                                                                                                    required={parameter.required}
                                                                                                                                    value={parameter.defaultValue} />
                                                                                                                                {parameter.errorMessage ? <h6 className="text-red">Please fill out this field.</h6> : null}
                                                                                                                            </React.Fragment> :
                                                                                                                        <span>{parameter.description}</span>
                                                                                                                    }</td>
                                                                                                                </tr>)
                                                                                                        })}
                                                                                                    </tbody>
                                                                                                </table>

                                                                                                {this.state.tryMode === api.id &&
                                                                                                    <div className="text-right mt-2">
                                                                                                        <button className="btn btn-primary mr-2" disabled={this.isExecutionDisabled(api)}
                                                                                                            onClick={() => this.invokeApi(api.requestUri, api.requestMethod, this.state.selectedModule.moduleName, api.id, api.payload, this.state.localPathParams)}>Execute</button>
                                                                                                        <button className="btn btn-light" onClick={() => this.tryItOut(api.id, this.state.selectedModule.moduleName, "close")}>Cancel</button>
                                                                                                    </div>
                                                                                                }
                                                                                            </div>
                                                                                        </div>

                                                                                        <div className="flex-35">
                                                                                            <div className="card api-output-box">
                                                                                                {api.json ?
                                                                                                    <div className="card-body overflow-y-auto">
                                                                                                        <pre className="text-white f-12">{JSON.stringify(JSON.parse(JSON.stringify(api.json)), undefined, 2)}</pre>
                                                                                                    </div> : api.jsonLoading ?
                                                                                                        <div className="card-loader h-100">
                                                                                                            <i className="fad fa-sync-alt fa-spin text-white    "></i>
                                                                                                        </div> :
                                                                                                        <div className="card-message h-100">
                                                                                                            {api.jsonErrorMessage ?
                                                                                                                <p className="text-red f-12">{api.jsonErrorMessage}</p> :
                                                                                                                <p className="text-light-gray f-12">There is no data to display.</p>}
                                                                                                        </div>
                                                                                                }
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            }
                                                                        </div>
                                                                    </li>
                                                                )
                                                            }) :
                                                                <NoDataFoundMessage />
                                                            }
                                                        </ul>
                                                    </div>
                                                </div>
                                            )
                                    })}
                                </div>
                            </div>
                        </div>
                        :
                        <div className="content-body">
                            <AddNewButton
                                text1="No API(s) available"
                                text2="You haven't created any APIs yet. Please create a API first."
                                createItemOnAddButtonClick={this.addButtonHandler}
                                imageIcon="addAPI.png"
                            />
                        </div>
                }

                {/* add api modal */}
                <SlidingPane
                    className=''
                    overlayClassName='sliding-form'
                    closeIcon={<div></div>}
                    isOpen={this.state.addOrEditGatewayApi || false}
                    from='right'
                    width='500px'
                >
                    <div className="modal-content">
                        <div className="modal-header">
                            <h6 className="modal-title">{this.state.selectedApi ? "Edit" : "Add New"} API
                                <button type="button" className="btn btn-light close" onClick={this.closeModalHandler}>
                                    <i className="fas fa-angle-right"></i>
                                </button>
                            </h6>
                        </div>

                        {this.state.addOrEditIsFetching ?
                            <div className="modal-loader">
                                <i className="fad fa-sync-alt fa-spin text-primary"></i>
                            </div> :

                            <form onSubmit={this.submitHandler}>
                                <div className="modal-body">
                                    {this.state.apiGatewayFailure &&
                                        <React.Fragment>
                                            {Array.isArray(this.state.apiGatewayFailure) ?
                                                this.state.apiGatewayFailure.map((val, index) => (
                                                    <p className="modal-body-error" key={index}>{val}</p>
                                                )) :
                                                <p className="modal-body-error">{this.state.apiGatewayFailure}</p>
                                            }
                                        </React.Fragment>
                                    }

                                    <div className="form-group">
                                        <label className="form-group-label">Select Module :<i className="fas fa-asterisk form-group-required"></i></label>
                                        <div className="d-flex">
                                            <div className="flex-33 pd-r-10">
                                                <label className={this.state.selectedApi && this.state.selectedApi !== "" ? "radio-button radio-button-disabled" : "radio-button"}>
                                                    <span className="radio-button-text">Create New</span>
                                                    <input type="radio" name="isRequired" id="createEditRadio"
                                                        value="create" disabled={this.state.selectedApi}
                                                        checked={this.state.isExistingController === "create"}
                                                        onChange={this.inputChangeHandler}
                                                    />
                                                    <span className="radio-button-mark"></span>
                                                </label>
                                            </div>
                                            <div className="flex-33 pd-r-10">
                                                <label className={this.state.selectedApi && this.state.selectedApi !== "" ? "radio-button radio-button-disabled" : "radio-button"}>
                                                    <span className="radio-button-text">Existing</span>
                                                    <input type="radio" name="isRequired" id="createEditRadio"
                                                        value="existing"
                                                        onChange={this.inputChangeHandler}
                                                        checked={this.state.isExistingController === "existing"}
                                                    />
                                                    <span className="radio-button-mark"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    {this.state.isExistingController === "existing" ?
                                        <div className="form-group">
                                            <label className="form-group-label">Module Name :<i className="fas fa-asterisk form-group-required"></i></label>
                                            <select id="moduleName" className="form-control"
                                                onChange={this.inputChangeHandler}
                                                disabled={this.state.selectedApi && this.state.selectedApi !== ""}
                                                value={this.state.payload.moduleName} required>
                                                <option value="">Select</option>
                                                {this.state.moduleNames.length > 0 &&
                                                    this.state.moduleNames.map((controller, index) => {
                                                        return <option key={index}
                                                            value={controller.moduleName}>{controller.moduleName} </option>
                                                    })}
                                            </select>
                                        </div>
                                        :
                                        <div className="form-group">
                                            <label className="form-group-label">Module Name :<i className="fas fa-asterisk form-group-required"></i></label>
                                            <input type="text" id="moduleName" className="form-control" required
                                                value={this.state.payload.moduleName}
                                                onChange={this.inputChangeHandler} />
                                        </div>
                                    }

                                    <div className="form-group">
                                        <label className="form-group-label">Module Description :</label>
                                        <textarea rows="3" type="text" name="desc" id="moduleDescription"
                                            className="form-control"
                                            onChange={this.inputChangeHandler}
                                            value={this.state.payload.moduleDescription}
                                        />
                                    </div>

                                    <div className="form-group">
                                        <label className="form-group-label"> API Description :</label>
                                        <textarea rows="3" type="text" name="desc" id="description"
                                            className="form-control"
                                            onChange={this.inputChangeHandler}
                                            value={this.state.payload.description}
                                        />
                                    </div>

                                    <div className="form-group">
                                        <label className="form-group-label">Controller Path :<i className="fas fa-asterisk form-group-required"></i></label>
                                        <input type="text" id="basePath" maxLength="50" className="form-control" placeholder="/employee" required
                                            value={this.state.payload.basePath} onChange={this.inputChangeHandler}
                                            disabled={this.state.isExistingController === "existing"}
                                        />
                                    </div>

                                    <div className="form-group">
                                        <label className="form-group-label">API Path : <i className="fas fa-asterisk form-group-required"></i></label>
                                        <input type="text" id="path" maxLength="50" placeholder="payroll" className="form-control" required
                                            onChange={this.inputChangeHandler} value={this.state.payload.path} />
                                        {this.state.validationMessage && <span className="form-group-error">{this.state.validationMessage}</span>}
                                    </div>

                                    <div className="form-group">
                                        <label className="form-group-label">HTTP Method : <i className="fas fa-asterisk form-group-required"></i></label>
                                        <select id="requestMethod" className="form-control"
                                            value={this.state.payload.requestMethod}
                                            onChange={this.inputChangeHandler} required>
                                            {this.state.methods.filter(methodObj => methodObj.method !== "ALL").map((options, index) => {
                                                return <option disabled={options.isDisabled} key={index}
                                                    value={options.method}>{options.method}</option>
                                            })}
                                        </select>
                                    </div>

                                    <div className="form-group">
                                        <label className="form-group-label">Function : <i className="fas fa-asterisk form-group-required"></i></label>
                                        <select id="actionName" className="form-control"
                                            value={this.state.payload.actionName} onChange={this.inputChangeHandler}
                                            required>
                                            <option value="">{this.getFilteredActions().length === 0 ? "No Actions Found" : "Select"}</option>
                                            {this.getFilteredActions().map((actions, index) => <option key={index}
                                                value={actions.actionName}>{actions.actionName}</option>)}
                                        </select>
                                    </div>
                                </div>

                                <div className="modal-footer justify-content-start">
                                    <button className="btn btn-primary">{this.state.selectedApi ? "Update" : "Save"}</button>
                                    <button type="button" className="btn btn-dark" onClick={() => this.setState({
                                        addOrEditGatewayApi: false,
                                        selectedApi: "",
                                        validationMessage: ''
                                    })}>Cancel
                                    </button>
                                </div>
                            </form>
                        }
                    </div>
                </SlidingPane>
                {/* end add api modal */}

                {
                    this.state.isOpen &&
                    <NotificationModal
                        type={this.state.modalType}
                        message2={this.state.message2}
                        onCloseHandler={this.onCloseHandler}
                    />
                }

                {
                    this.state.confirmState &&
                    <ConfirmModel
                        deleteName={this.state.multipleDelete ? this.state.deleteModule ? "Module " + this.state.selectedModule.moduleName : "APIs" : this.state.deleteName}
                        confirmClicked={() => {
                            this.state.multipleDelete ? this.props.multipleDeleteApi(this.state.multipleDeletePayload, this.state.selectedModule.moduleName)
                                : this.props.deleteApiById(this.state.apiToBeDeleted);
                            this.setState({
                                confirmState: false,
                                isOpen: false,
                                isFetching: true
                            })

                        }}
                        cancelClicked={() => {
                            this.cancelClicked()
                        }}
                    />
                }

            </React.Fragment >
        );
    }
}

ManageGatewayApi.propTypes = {
    dispatch: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
    apiListSuccess: Selectors.apiListSuccess(),
    apiListError: Selectors.apiListError(),
    deleteApiSuccess: Selectors.deleteApiSuccess(),
    deleteApiError: Selectors.deleteApiError(),
    invokeApiSuccess: Selectors.invokeApiSuccess(),
    invokeApiError: Selectors.invokeApiError(),
    cloneApiSuccess: Selectors.cloneApiSuccess(),
    cloneApiError: Selectors.cloneApiError(),
    getFaaSListSuccess: Selectors.getFaaSListSuccess(),
    getFaaSListFailure: Selectors.getFaaSListFailure(),
    apiGatewaySuccess: Selectors.apiGatewaySuccess(),
    apiGatewayFailure: Selectors.apiGatewayFailure(),
    getApiDetails: Selectors.getApiDetails(),
    getApiDetailsFailure: Selectors.getApiDetailsFailure(),
    multipleDeleteApiSuccess: Selectors.multipleDeleteApiSuccess(),
    multipleDeleteApiFailure: Selectors.multipleDeleteApiFailure()
});

function mapDispatchToProps(dispatch) {
    return {
        dispatch,
        getApiList: () => dispatch(Actions.getApiList()),
        deleteApiById: (apiToBeDeleted) => dispatch(Actions.deleteApiById(apiToBeDeleted)),
        invokeApi: (uri, method, moduleName, apiId, payload) => dispatch(Actions.invokeApi(uri, method, moduleName, apiId, payload)),
        cloneApi: (apiId, moduleName) => dispatch(Actions.cloneApi(apiId, moduleName)),
        saveApiGateway: (payload, id) => dispatch(Actions.saveApiGateway(payload, id)),
        getApiGatewayByID: (id) => dispatch(Actions.getApiGatewayByID(id)),
        getFaaSActions: () => dispatch(Actions.getFaaSActions()),
        resetToInitialState: () => dispatch(Actions.resetToInitialState()),
        multipleDeleteApi: (payload, moduleName) => dispatch(Actions.multipleDeleteApi(payload, moduleName))
    };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "manageGatewayApi", reducer });
const withSaga = injectSaga({ key: "manageGatewayApi", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(ManageGatewayApi);

