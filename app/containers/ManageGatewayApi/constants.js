/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

export const GET_API_LIST = "app/ManageGatewayApi/GET_API_LIST";
export const GET_API_LIST_SUCCESS = "app/ManageGatewayApi/GET_API_LIST_SUCCESS";
export const GET_API_LIST_FAILURE = "app/ManageGatewayApi/GET_API_LIST_FAILURE";

export const DELETE_API_BY_ID = "app/ManageGatewayApi/DELETE_API_BY_ID";
export const DELETE_API_SUCCESS = "app/ManageGatewayApi/DELETE_API_SUCCESS";
export const DELETE_API_FAILURE = "app/ManageGatewayApi/DELETE_API_FAILURE";

export const INVOKE_API = "app/ManageGatewayApi/INVOKE_API";
export const INVOKE_API_SUCCESS = "app/ManageGatewayApi/INVOKE_API_SUCCESS";
export const INVOKE_API_FAILURE = "app/ManageGatewayApi/INVOKE_API_FAILURE";

export const CLONE_API = "app/ManageGatewayApi/CLONE_API";
export const CLONE_API_SUCCESS = "app/ManageGatewayApi/CLONE_API_SUCCESS";
export const CLONE_API_FAILURE = "app/ManageGatewayApi/CLONE_API_FAILURE";

export const RESET_TO_INITIAL_STATE = "app/ManageGatewayApi/RESET_TO_INITIAL_STATE";



export const CREATE_API_GATEWAY = "app/ManageGatewayApi/CREATE_API_GATEWAY";
export const CREATE_API_GATEWAY_SUCCESS = "app/ManageGatewayApi/CREATE_API_GATEWAY_SUCCESS";
export const CREATE_API_GATEWAY_FAILURE = "app/ManageGatewayApi/CREATE_API_GATEWAY_FAILURE";

export const GET_API_GATEWAY_BY_ID = "app/ManageGatewayApi/GET_API_GATEWAY_BY_ID";
export const GET_API_GATEWAY_BY_ID_SUCCESS = "app/ManageGatewayApi/GET_API_GATEWAY_BY_ID_SUCCESS";
export const GET_API_GATEWAY_BY_ID_FAILURE = "app/ManageGatewayApi/GET_API_GATEWAY_BY_ID_FAILURE";

export const GET_FAAS_ACTIONS = "app/ManageGatewayApi/GET_FAAS_ACTIONS";
export const GET_FAAS_ACTIONS_SUCCESS = "app/ManageGatewayApi/GET_FAAS_ACTIONS_SUCCESS";
export const GET_FAAS_ACTIONS_FAILURE = "app/ManageGatewayApi/GET_FAAS_ACTIONS_FAILURE";

export const MULTIPLE_DELETE_API = "app/ManageGatewayApi/MULTIPLE_DELETE_API";
export const MULTIPLE_DELETE_API_SUCCESS = "app/ManageGatewayApi/MULTIPLE_DELETE_API_SUCCESS";
export const MULTIPLE_DELETE_API_FAILURE = "app/ManageGatewayApi/MULTIPLE_DELETE_API_FAILURE";
