/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * DeviceDashboardsEventHistory
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import injectSaga from "utils/injectSaga";
import ReactSelect from "react-select";
import injectReducer from "utils/injectReducer";
import { allSelectors as SELECTORS } from "./selectors";
import { allActions as ACTIONS } from './actions'
import reducer from "./reducer";
import saga from "./saga";
import { cloneDeep, isEqual } from "lodash";
import AddNewButton from "../../components/AddNewButton/Loadable"
import Loader from "../../components/Loader/Loadable";
import moment from 'moment';
import CollapseOrShowAllFilters from "../../components/CollapseOrShowAllFilters";
import SearchBarWithSuggestion, { clearSearchBox } from "../../components/SearchBarWithSuggestion";
import ListingTable from "../../components/ListingTable/Loadable";
import { isNavAssigned, DURATION_OPTIONS } from "../../commonUtils";
import { ACTION_TYPES } from "../Events";

/* eslint-disable react/prefer-stateless-function */
export class DeviceDashboardsEventHistory extends React.Component {
    state = {
        filterExpand: true,
        deviceTypeList: [],
        eventsList: [],
        items: [],
        hasMore: true,
        isFetching: true,
        dateError: "",
        payload: {
            deviceTypeIds: [],
            from: new Date().getTime() - 259200000, // utc timestamp 3 days before from now
            to: new Date().getTime(), // utc timestamp
        },
        selectedDeviceTypes: [],
        search: "",
        filters: {
            from: "",
            to: "",
            actionType: "",
        },
        selectedEventId: null,
        showDeviceModal: false,
        selectedDuration: 4320,
        deviceList: [],
    }

    refreshComponent = () => {
        let payload = { ...this.state.payload }
        payload.to = new Date().getTime()
        payload.from = (new Date().getTime() - (this.state.selectedDuration * 1000 * 60))
        this.setState({
            isFetchingEvents: true,
            isFetching: true,
            eventsList: [],
            payload
        }, () => {
            this.props.eventList(this.state.payload);
        })
    }

    componentDidMount() {
        this.props.deviceTypeList();
    }

    static getDerivedStateFromProps(nextProps, state) {
        if (nextProps.deviceTypeListSuccess) {
            let deviceTypeList = nextProps.deviceTypeListSuccess.response,
                payload = cloneDeep(state.payload),
                selectedDeviceTypes = [];

            if (deviceTypeList.length > 0) {
                deviceTypeList.map(deviceType => {
                    selectedDeviceTypes.push({
                        label: deviceType.name,
                        value: deviceType.id,
                    });
                    payload.deviceTypeIds.push(deviceType.id);
                });
                nextProps.eventList(payload);
            }
            return {
                payload,
                deviceTypeList,
                selectedDeviceTypes,
                isFetching: false,
                isFetchingEvents: deviceTypeList.length > 0
            }
        }

        else if (nextProps.deviceTypeListFailure) {
            return {
                isFetching: false
            }
        }

        if (nextProps.eventListSuccess) {
            let eventsList = [];
            nextProps.eventListSuccess.response.map(eventListById => {
                let deviceTypeName = state.deviceTypeList.filter(deviceType => deviceType.id === eventListById.deviceTypeId)[0].name;
                eventListById.history.map(eventObj => {
                    eventObj.deviceTypeId = eventListById.deviceTypeId;
                    eventObj.deviceTypeName = deviceTypeName;
                })
                Array.prototype.push.apply(eventsList, eventListById.history);
            });
            return {
                eventsList,
                items: eventsList,
                isFetching: false,
                isFetchingEvents: false,
            }
        }

        else if (nextProps.eventListFailure) {
            return {
                isFetching: false,
                isFetchingEvents: false
            }
        }

        return null
    }

    componentDidUpdate(prevProps, prevState) {
        let newProp = Object.keys(SELECTORS).find(prop => this.props[prop])
        if (newProp) {
            if (newProp.includes("Failure")) {
                let propData = this.props[newProp].error ? this.props[newProp].error : cloneDeep(this.props[newProp].response)
                let type = newProp.includes("Failure") ? "error" : "success"
                this.props.showNotification(type, propData, this.onCloseHandler)
            }
            this.props.resetToInitialStateProps(newProp)
        }
    }

    getMiniTable = (columns, data) => {
        return <React.Fragment>
            <div className="header d-flex">
                {columns.map(column => <h2 className="flex-40">{column.header}</h2>)}
            </div>
            <div className="body">
                {data.map(item => <div className="body-content d-flex">
                    {columns.map(column => <h2 className="flex-40">{item[column.accessor]}</h2>)}
                </div>)}
            </div>
        </React.Fragment>
    }

    clearAllFilters = () => {
        this.setState({
            filters: {
                from: "",
                to: "",
                actionType: "",
            }
        })
    }

    onChangeHandler = (event, type) => {
        let payload = cloneDeep(this.state.payload),
            selectedDuration = this.state.selectedDuration
        if (event.currentTarget.id == "selectedDuration") {
            let timestamp = new Date().getTime()
            payload.to = timestamp
            payload.from = (timestamp - (event.currentTarget.value * 1000 * 60))
            selectedDuration = event.currentTarget.value
        }
        this.setState({
            payload,
            dateError: "",
            eventsList: [],
            isFetchingEvents: this.state.selectedDeviceTypes.length > 0,
            items: [],
            selectedDuration
        }, () => {
            this.props.eventList(this.state.payload);
        });
    }

    searchHandler = (search) => {
        this.setState({
            search,
            items: search ? this.state.eventsList.filter(val => val.eventName === search) : this.state.eventsList,
        })
    }

    clearSearch = () => {
        this.setState({
            search: "",
            items: this.state.eventsList,
        })
    }

    onCloseHandler = () => {
        this.setState({
            isOpen: false,
            type: '',
            message2: ''
        })
    }
    showDeviceList = (eventId, lastReportedAt) => {
        let deviceList = this.state.items.filter(item => item.eventId === eventId && item.lastReportedAt === lastReportedAt)[0]['deviceList'];
        this.setState({
            selectedEventId: eventId,
            showDeviceModal: true,
            deviceList,
        })
    }

    filterExpandCollapseHandler = () => {
        this.setState({
            filterExpand: this.state.filterExpand ? false : true
        })
    }

    getDeviceTypes = (deviceTypeList) => {
        let list = [];
        deviceTypeList.map(deviceType => {
            list.push({
                label: deviceType.name,
                value: deviceType.id,
            })
        })
        return list;
    }
    deviceTypeChangeHandler = (selectedDeviceTypes) => {
        let payload = cloneDeep(this.state.payload);
        payload.deviceTypeIds = [];
        if (selectedDeviceTypes.length > 0) {
            selectedDeviceTypes.map(deviceType => {
                payload.deviceTypeIds.push(deviceType.value);
            });
            this.setState({
                selectedDeviceTypes,
                isFetchingEvents: selectedDeviceTypes.length > 0,
                payload
            }, () => {
                this.props.eventList(payload);
            })
        } else {
            this.setState({
                selectedDeviceTypes,
                eventsList: [],
                isFetchingEvents: false,
                payload
            })
        }


    }

    getColumns = () => {
        return [
            {
                Header: "Happened When?", width: 15,
                cell: (row) => (
                    <React.Fragment>
                        <h6 className="text-theme fw-600">{moment(row.lastReportedAt).format("MMMM Do YYYY")}</h6>
                        <p className="text-dark-theme">{moment(row.lastReportedAt).format('LT')}</p>
                    </React.Fragment>
                )
            },
            {
                Header: "Event Name", width: 10,
                cell: row => (
                    <React.Fragment>
                        <h6 className="text-theme fw-600">{row.eventName}</h6>
                        <p>{row.description != "" ? row.description : "-"}</p>
                    </React.Fragment>
                ),
                accessor: "eventName",
                filterable: true,
                sortable: true
            },
            {
                Header: "Device Type", width: 10,
                cell: (row) => <h6>{row.deviceTypeName}</h6>,
                accessor: "deviceTypeName",
                filterable: true,
                sortable: true
            },
            {
                Header: "Criteria", width: 15,
                cell: (row) => (
                    <React.Fragment>
                        <h6>After <strong className="text-orange">{row.duration}</strong> min(s)</h6>
                        <p><strong className="text-green">{row.occurrence}</strong> Consecutive Occurrence</p>
                    </React.Fragment>
                )
            },
            /*{
                Header: "Occurrences (Consecutive)", width: 15,
                cell: (row) => <span className="alert alert-primary list-view-badge">{row.occurrence}</span>
            },
            {
                Header: "Action Trigger Duration", width: 15,
                cell: (row) => <h6>After {row.duration} min(s)</h6>
            },*/
            {
                Header: "Devices Impacted", width: 10,
                cell: (row) => (
                    <React.Fragment>
                        <span className="alert alert-primary list-view-badge list-view-badge-link" onClick={() => { this.showDeviceList(row.eventId, row.lastReportedAt) }}>{row.deviceCount}</span>
                    </React.Fragment>
                )
            },
            {
                Header: "Success Ratio", width: 10,
                cell: (row) => (
                    <React.Fragment>
                        <span className="badge badge-pill badge-light list-view-badge-pill">{((row.successCount / (row.successCount + row.failureCount)) * 100).toFixed()} %</span>
                    </React.Fragment>
                )
            },
            {
                Header: "Actions Taken", width: 15,
                cell: (row) => (<React.Fragment>
                    <div className="list-view-initial list-style-none d-flex">
                        <span className={row.actionsType.some(val => val.actionType === "sms") ? "active" : null} data-tooltip data-tooltip-text="SMS" data-tooltip-place="bottom"><i className="fad fa-comment-lines"></i></span>
                        <span className={row.actionsType.some(val => val.actionType === "email") ? "active" : null} data-tooltip data-tooltip-text="Email" data-tooltip-place="bottom"><i className="fad fa-envelope"></i></span>
                        <span className={row.actionsType.some(val => val.actionType === "rpc") ? "active" : null} data-tooltip data-tooltip-text="Control" data-tooltip-place="bottom"><i className="fad fa-terminal"></i></span>
                        <span className={row.actionsType.some(val => val.actionType === "webhook") ? "active" : null} data-tooltip data-tooltip-text="Webhook" data-tooltip-place="bottom"><i className="fad fa-code-merge"></i></span>
                    </div>
                </React.Fragment>),
                accessor: "name",
                filterable: true,
                sortable: true,
                filter: ({ value, onChangeHandler, accessor }) =>
                    <select className="form-control" onChange={(event) => onChangeHandler(event, accessor)}>
                        <option value="">All</option>
                        {ACTION_TYPES.map(actionType =>
                            <option key={actionType.name} value={actionType.name}>{actionType.displayName}</option>
                        )}
                    </select>,
                customFilterFunction: (row, selectedValue) => {
                    return row.actionsType.some(val => val.actionType === selectedValue)
                }
            },
            {
                Header: "Conditions Evaluated", width: 15,
                cell: (row) => (
                    <span>{row.conditions.map((condition, i) => <p key={i} >{condition}</p>)}</span>
                )
            },
        ]
    }

    durationValue = (duration) => {
        if (duration) {
            return DURATION_OPTIONS.find(obj => obj.value === duration)
        }
    }

    filterChangeHandler = ({ value }) => {
        let payload = cloneDeep(this.state.payload),
            selectedDuration = this.state.selectedDuration
        let timestamp = new Date().getTime()
        payload.to = timestamp
        payload.from = (timestamp - (value * 1000 * 60))
        selectedDuration = value
        this.setState({
            payload,
            dateError: "",
            eventsList: [],
            isFetchingEvents: this.state.selectedDeviceTypes.length > 0,
            items: [],
            selectedDuration
        }, () => {
            this.props.eventList(payload);
        });
    }

    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>Event History</title>
                    <meta name="description" content="M83-Event History" />
                </Helmet>

                <header className="content-header d-flex">
                    <div className="flex-60">
                        <h6>Event History</h6>
                    </div>
                    <div className="flex-40 text-right">
                        <div className="content-header-group">
                            <button className="btn btn-light" disabled={this.state.isFetching} data-tooltip data-tooltip-text="Refresh" data-tooltip-place="bottom" onClick={this.refreshComponent}><i className="far fa-sync-alt"></i></button>
                            {Boolean(this.state.deviceTypeList.length) && <button className={`btn btn-light ${this.state.filterExpand && "active"}`} data-tooltip data-tooltip-text="Filters" disabled={this.state.isFetching} data-tooltip-place="bottom" onClick={() => this.filterExpandCollapseHandler()} ><i className="far fa-filter"></i></button>}
                        </div>
                    </div>
                </header>

                {this.state.isFetching ?
                    <Loader /> :
                    <React.Fragment>
                        {this.state.deviceTypeList.length ?
                            <React.Fragment>
                                {this.state.filterExpand && <div className="device-filter-wrapper">
                                    <div className="device-filter-box">
                                        <h5>Filters <CollapseOrShowAllFilters /></h5>
                                        <h6>Specify Your Search Criteria</h6>
                                        {/*<div className="form-group mb-0">
                                            <div className="search-wrapper">
                                                <SearchBarWithSuggestion ref="searchBar" disabled={!this.state.eventsList.length} clearSearchBox={""} value={this.state.search} suggestionKey="eventName" onChange={this.searchHandler} data={this.state.eventsList} />
                                            </div>
                                        </div>*/}
                                    </div>
                                    <div className="device-filter-box">
                                        <p>Device Type <i className="fas fa-angle-down device-filter-collapse" data-toggle="collapse" data-target="#deviceType__filter__"></i></p>
                                        <div className="collapse show" id="deviceType__filter__">
                                            <div className="device-filter-body">
                                                <div className="form-group mb-0">
                                                    <ReactSelect
                                                        id="deviceTypes"
                                                        options={this.getDeviceTypes(this.state.deviceTypeList)}
                                                        value={this.state.selectedDeviceTypes}
                                                        onChange={this.deviceTypeChangeHandler}
                                                        isMulti={true}
                                                        isDisabled={this.state.isFetchingEvents}
                                                        className="form-control-multi-select">
                                                    </ReactSelect>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {/* <div className="device-filter-box">
                                        <p className="collapsed" data-toggle="collapse" data-target="#groups">Groups <i className="fas fa-angle-down device-filter-collapse"></i>
                                            <button className="btn btn-link">Reset</button>
                                        </p>
                                        <div className="collapse" id="groups">
                                            <div className="device-filter-body">
                                                <ul className="d-flex list-style-none device-filter-group">
                                                    <div className="text-center flex-100"><i className="fad fa-sync-alt fa-spin f-13 text-theme"></i></div>
                                                    <li className="active">Group 1</li>
                                                    <li><p>No Device Groups Found !</p></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div> */}
                                    {/* <div className="device-filter-box">
                                        <p className="collapsed" data-toggle="collapse" data-target="#tags">Tags <i className="fas fa-angle-down device-filter-collapse"></i>
                                            <button className="btn btn-link">Reset</button>
                                        </p>
                                        <div className="collapse" id="tags">
                                            <div className="device-filter-body">
                                                <ul className="d-flex list-style-none device-filter-group">
                                                    <li className="active">Device Tag</li>
                                                    <li><p>No Tags found for selected Device Group</p></li>
                                                    <li><p>Select a Group to view Tags.</p></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div> */}
                                    <div className="device-filter-box">
                                        <p>Duration <i className="fas fa-angle-down device-filter-collapse" data-toggle="collapse" data-target="#to__filter__"></i></p>
                                        <div className="collapse show" id="to__filter__">
                                            <div className="device-filter-body">
                                                <div className="form-group mb-0">
                                                    <ReactSelect
                                                        className="form-control-multi-select"
                                                        id="selectedDuration"
                                                        options={DURATION_OPTIONS}
                                                        value={this.durationValue(this.state.selectedDuration)}
                                                        onChange={this.filterChangeHandler}
                                                        isMulti={false}>
                                                    </ReactSelect>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {/* <div className="device-filter-box"> */}
                                    {/* <div className="form-group mb-0">
                                            <select id="selectedDuration" className="form-control" onChange={this.onChangeHandler} value={this.state.selectedDuration}>
                                                <option value="60">Last 1 hour</option>
                                                <option value="180">Last 3 hours</option>
                                                <option value="360">Last 6 hours</option>
                                                <option value="1440">Last 1 day</option>
                                                <option value="4320">Last 3 days</option>
                                            </select>
                                        </div> */}
                                    {/* <p>From <i className="fas fa-angle-down device-filter-collapse" data-toggle="collapse" data-target="#from__filter__"></i></p>
                                        <div className="collapse show" id="from__filter__">
                                            <div className="device-filter-body">
                                                <div className="form-group mb-0">
                                                    <Datetime
                                                        className={`${this.state.dateError === "from" ? "invalid-date" : ""}`}
                                                        closeOnSelect={true}
                                                        value={this.state.payload.from}
                                                        onChange={(e) => this.onChangeHandler(e, "from")}
                                                    />
                                                </div>
                                            </div>
                                        </div> */}
                                    {/* </div> */}
                                    {/* <div className="device-filter-box">
                                        <p>To <i className="fas fa-angle-down device-filter-collapse" data-toggle="collapse" data-target="#to__filter__"></i></p>
                                        <div className="collapse show" id="to__filter__">
                                            <div className="device-filter-body">
                                                <div className="form-group mb-0">
                                                    <Datetime
                                                        className={`${this.state.dateError === "to" ? "invalid-date" : ""}`}
                                                        closeOnSelect={true}
                                                        value={this.state.payload.to}
                                                        isValidDate={(currentDate) => {
                                                            return new Date(currentDate._d).getTime() <= new Date().getTime();
                                                        }}
                                                        onChange={(e) => this.onChangeHandler(e, "to")}
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                    </div> */}
                                </div>}

                                <div className="content-body device-filter-content" id="scrollableDiv" style={{ paddingRight: this.state.filterExpand ? 270 : 12 }}>
                                    {<div className="list-view">
                                        <ListingTable
                                            columns={this.getColumns()}
                                            data={this.state.items}
                                            isLoading={this.state.isFetchingEvents}
                                            totalItemsCount={this.state.items.length}
                                        />
                                    </div>}

                                    {/* ==== Please do not delete ==== */}
                                    {/* variant 2 */}
                                    {/*<div className="widget-tab-wrapper" id="eventHistoryVareint_1">
                                            <ul className="nav nav-tabs" id="myTab" role="tablist">
                                                <li className="nav-item">
                                                    <a className="nav-link active" data-toggle="tab" href="#action" role="tab">All Action</a>
                                                </li>
                                                <li className="nav-item">
                                                    <a className="nav-link" data-toggle="tab" href="#email" role="tab">Email</a>
                                                </li>
                                                <li className="nav-item">
                                                    <a className="nav-link" data-toggle="tab" href="#webhook" role="tab">Webhook</a>
                                                </li>
                                                <li className="nav-item">
                                                    <a className="nav-link" data-toggle="tab" href="#sms" role="tab">SMS</a>
                                                </li>
                                                <li className="nav-item">
                                                    <a className="nav-link" data-toggle="tab" href="#rpc" role="tab">RPC</a>
                                                </li>
                                                <div className="form-group mr-b-0">
                                                    <p>Filter By date</p>
                                                    <input type="date" className="form-control" placeholder="Filter By Date" />
                                                    <span className="calender-icon"><i className="fal fa-calendar-alt"></i></span>
                                                </div>
                                            </ul>
                                            <div className="tab-content" id="myTabContent">
                                                <div className="tab-pane fade show active" id="home">
                                                    <div className="widget-list-wrapper">
                                                        <div className="widget-list-header d-flex">
                                                            <p className="flex-15">Event Name</p>
                                                            <p className="flex-20">Description</p>
                                                            <p className="flex-20">Action</p>
                                                            <p className="flex-10">Device Count</p>
                                                            <p className="flex-15">Last Reported At</p>
                                                            <p className="flex-10">Occurrence</p>
                                                            <p className="flex-10">Action</p>
                                                        </div>
                                                        <div className="widget-list-body">
                                                            <ul className="widget-list list-style-none d-flex">
                                                                <li className="flex-15">
                                                                    <p className="text-theme">Event Name</p>
                                                                </li>
                                                                <li className="flex-20">
                                                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting</p>
                                                                </li>
                                                                <li className="flex-20">
                                                                    <ol className="list-style-none">
                                                                        <li>
                                                                            <div className="widget-circular-image">
                                                                                <img src="https://image.flaticon.com/icons/svg/281/281769.svg" />
                                                                            </div>
                                                                        </li>
                                                                        <li>
                                                                            <div className="widget-circular-image">
                                                                                <img src="https://image.flaticon.com/icons/svg/736/736117.svg" />
                                                                            </div>
                                                                        </li>
                                                                        <li>
                                                                            <div className="widget-circular-image">
                                                                                <img src="https://symphony.com/wp-content/uploads/2019/08/webhooks_1024.png" />
                                                                            </div>
                                                                        </li>
                                                                        <li>
                                                                            <div className="widget-circular-image">
                                                                                <img src="https://upload.wikimedia.org/wikipedia/commons/7/77/Logo_de_RPC_2016.png" />
                                                                            </div>
                                                                        </li>
                                                                    </ol>
                                                                </li>
                                                                <li className="flex-10">
                                                                    <div className="alert">10</div>
                                                                </li>
                                                                <li className="flex-15">
                                                                    <p>7/10/2020 <span>|</span> 02:00 PM</p>
                                                                    <h6>2 days Ago</h6>
                                                                </li>
                                                                <li className="flex-10">
                                                                    <div className="alert">10</div>
                                                                </li>
                                                                <li className="flex-10">
                                                                    <div className="button-group">
                                                                        <button className="btn-transparent btn-transparent-blue"><i
                                                                            className="fas fa-broom"></i></button>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>*/}
                                    {/* end variant 2 */}

                                    {/*<div className="variant-box">
                                             <div className="event-box">
                                                 <div className="event-list">
                                                <ul className="list-style-none">
                                                    <li className="d-flex list-item">
                                                        <div className="flex-5 date-box">
                                                            <p>12 JAN</p>
                                                        </div>
                                                        <div className="flex-95">
                                                            <div className="d-flex event-list-item">
                                                                <div className="flex-35">
                                                                    <div className="event-content-item">
                                                                        <h6><span>Name : </span>Highest Temprature</h6>
                                                                    </div>
                                                                    <div className="event-content-item">
                                                                        <h6><span>Description : </span>Temprature Limit Crossed</h6>
                                                                    </div>
                                                                </div>
                                                                <div className="flex-20">
                                                                    <div className="event-content-item">
                                                                        <h6><span>Ocurence : </span>40 Times</h6>
                                                                    </div>
                                                                    <div className="event-content-item">
                                                                        <h6><span>Duration : </span>40 Minutes</h6>
                                                                    </div>
                                                                </div>
                                                                <div className="flex-25">
                                                                    <div className="event-content-item">
                                                                        <span>Actions</span>
                                                                        <ol className="list-style-none">
                                                                            <li>
                                                                                <div className="device-action-icon">
                                                                                    <img src="https://image.flaticon.com/icons/svg/281/281769.svg"/>
                                                                                </div>
                                                                            </li>
                                                                            <li>
                                                                                <div className="device-action-icon">
                                                                                    <img src="https://image.flaticon.com/icons/svg/736/736117.svg"/>
                                                                                </div>
                                                                            </li>
                                                                            <li>
                                                                                <div className="device-action-icon">
                                                                                    <img src="https://symphony.com/wp-content/uploads/2019/08/webhooks_1024.png"/>
                                                                                </div>
                                                                            </li>
                                                                            <li>
                                                                                <div className="device-action-icon">
                                                                                    <img src="https://upload.wikimedia.org/wikipedia/commons/7/77/Logo_de_RPC_2016.png"/>
                                                                                </div>
                                                                            </li>
                                                                        </ol>
                                                                    </div>
                                                                </div>
                                                                <div className="flex-20">
                                                                    <div className="event-content-item">
                                                                        <span>Conditions</span>
                                                                        <h6>Engine Temprature > 40  <i className="fas fa-caret-down"></i></h6>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                            </div>
                                             <div className="event-list">
                                                <div className="event-list-head ">
                                                    <div className="date-range d-flex">
                                                        <div className="flex-80">
                                                            <p><span>12</span> - <span>20</span> <span>July 2020</span></p>
                                                        </div>
                                                        <div className="flex-20">
                                                            <p><i className="fal fa-less-than mr-r-5"></i> <i className="fal fa-greater-than"></i></p>
                                                        </div>
                                                    </div>
                                                    <button className="btn btn-transparent-gray"><i className="far fa-sort-amount-up-alt"></i> Filter</button>
                                                </div>
                                                <ul className="list-style-none">
                                                    <li className="d-flex list-item">
                                                        <div className="flex-10">
                                                            <div className="date-box">
                                                                <p>JAN</p>
                                                                <h3>12</h3>
                                                                <p>Today</p>
                                                            </div>
                                                        </div>
                                                        <div className="flex-90">
                                                            <div className="d-flex event-list-item">
                                                                <div className="flex-45 d-flex pd-r-10">
                                                                    <div className="flex-45 pd-r-10">
                                                                        <div className="event-content-item">
                                                                            <h6 className="badge-content badge-blue">High Temperature</h6>
                                                                        </div>
                                                                    </div>
                                                                    <div className="flex-55 pd-l-10">
                                                                        <div className="event-content-item">
                                                                            <h6>Temprature Limit Crossed</h6>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div className="flex-30 d-flex pd-l-10 pd-r-10">
                                                                    <div className="flex-50 pd-r-10">
                                                                        <div className="event-content-item">
                                                                            <h6 className="badge-content badge-yellow"><i className="far fa-clock"></i> 40 Minutes</h6>
                                                                        </div>
                                                                    </div>
                                                                    <div className="flex-50 pd-l-10">
                                                                        <div className="event-content-item">
                                                                            <h6 className="badge-content badge-green"><i className="far fa-repeat"></i> 40 Times</h6>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div className="flex-25 d-flex pd-l-10">
                                                                    <div className="flex-40 pd-r-10">
                                                                        <div className="event-content-item">
                                                                            <h6><i className="fas fa-caret-down"></i> Actions</h6>
                                                                        </div>
                                                                    </div>
                                                                    <div className="flex-60 pd-l-10">
                                                                        <div className="event-content-item">
                                                                            <h6><i className="fas fa-caret-down"></i> Condition</h6>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="d-flex event-list-item">
                                                                <div className="flex-45 d-flex pd-r-10">
                                                                    <div className="flex-45 pd-r-10">
                                                                        <div className="event-content-item">
                                                                            <h6 className="badge-content badge-blue">High Temperature</h6>
                                                                        </div>
                                                                    </div>
                                                                    <div className="flex-55 pd-l-10">
                                                                        <div className="event-content-item">
                                                                            <h6>Temprature Limit Crossed</h6>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div className="flex-30 d-flex pd-l-10 pd-r-10">
                                                                    <div className="flex-50 pd-r-10">
                                                                        <div className="event-content-item">
                                                                            <h6 className="badge-content badge-yellow"><i className="far fa-clock"></i> 40 Minutes</h6>
                                                                        </div>
                                                                    </div>
                                                                    <div className="flex-50 pd-l-10">
                                                                        <div className="event-content-item">
                                                                            <h6 className="badge-content badge-green"><i className="far fa-repeat"></i> 40 Times</h6>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div className="flex-25 d-flex pd-l-10">
                                                                    <div className="flex-40 pd-r-10">
                                                                        <div className="event-content-item">
                                                                            <h6><i className="fas fa-caret-down"></i> Actions</h6>
                                                                        </div>
                                                                    </div>
                                                                    <div className="flex-60 pd-l-10">
                                                                        <div className="event-content-item">
                                                                            <h6><i className="fas fa-caret-down"></i> Conditions</h6>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>

                                            <div className="event-list">
                                                <div className="d-flex">
                                                    <div className="flex-75">
                                                        <div className="event-content">
                                                            <div className="event-head d-flex">
                                                                <div className="flex-70">
                                                                    <h5>25 January <span>Thursady</span></h5>
                                                                </div>
                                                                <div className="flex-30">
                                                                    <select className="form-control">
                                                                        <option>Search Events...</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div className="event-list-item">
                                                                <ul className="list-style-none">
                                                                    <li className="active">
                                                                        <div className="event-time">
                                                                            <p>10 <span>AM</span></p>
                                                                        </div>
                                                                        <div className="badge-content badge-blue">
                                                                            <div className="d-flex">
                                                                                <div className="flex-35 pd-r-10">
                                                                                    <div className="event-content-item">
                                                                                        <h6 >High Temperature</h6>
                                                                                    </div>
                                                                                    <div className="event-content-item">
                                                                                        <h6>Temprature Limit Crossed</h6>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="flex-20  pd-l-10 pd-r-10">
                                                                                    <div className="event-content-item">
                                                                                        <h6><i className="far fa-clock"></i> 40 Minutes</h6>
                                                                                    </div>
                                                                                    <div className="event-content-item">
                                                                                        <h6><i className="far fa-repeat"></i> 40 Times</h6>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="flex-45  pd-l-10">
                                                                                    <div className="event-content-item">
                                                                                        <div className="badge-group">
                                                                                            <span>Actions : </span>
                                                                                            <span className="badge active">SMS</span>
                                                                                            <span className="badge">Mail</span>
                                                                                            <span className="badge">RPC</span>
                                                                                            <span className="badge">Webhook</span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div className="event-content-item">
                                                                                        <h6>Conditions: <span> Engine Temprature > 40</span> <i className="fas fa-caret-down"></i></h6>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                    <li className="">
                                                                        <div className="event-time">
                                                                            <p>10 <span>AM</span></p>
                                                                        </div>
                                                                        <div className="badge-content badge-green">
                                                                            <div className="d-flex">
                                                                                <div className="flex-35 pd-r-10">
                                                                                    <div className="event-content-item">
                                                                                        <h6 >High Temperature</h6>
                                                                                    </div>
                                                                                    <div className="event-content-item">
                                                                                        <h6>Temprature Limit Crossed</h6>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="flex-20  pd-l-10 pd-r-10">
                                                                                    <div className="event-content-item">
                                                                                        <h6><i className="far fa-clock"></i> 40 Minutes</h6>
                                                                                    </div>
                                                                                    <div className="event-content-item">
                                                                                        <h6><i className="far fa-repeat"></i> 40 Times</h6>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="flex-45  pd-l-10">
                                                                                    <div className="event-content-item">
                                                                                        <div className="badge-group">
                                                                                            <span>Actions : </span>
                                                                                            <span className="badge active">SMS</span>
                                                                                            <span className="badge">Mail</span>
                                                                                            <span className="badge">RPC</span>
                                                                                            <span className="badge">Webhook</span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div className="event-content-item">
                                                                                        <h6>Conditions: <span> Engine Temprature > 40</span> <i className="fas fa-caret-down"></i></h6>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                    <li className="">
                                                                        <div className="event-time">
                                                                            <p>10 <span>AM</span></p>
                                                                        </div>
                                                                        <div className="badge-content badge-yellow">
                                                                            <div className="d-flex ">
                                                                                <div className="flex-35 pd-r-10">
                                                                                    <div className="event-content-item">
                                                                                        <h6 >High Temperature</h6>
                                                                                    </div>
                                                                                    <div className="event-content-item">
                                                                                        <h6>Temprature Limit Crossed</h6>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="flex-20  pd-l-10 pd-r-10">
                                                                                    <div className="event-content-item">
                                                                                        <h6><i className="far fa-clock"></i> 40 Minutes</h6>
                                                                                    </div>
                                                                                    <div className="event-content-item">
                                                                                        <h6><i className="far fa-repeat"></i> 40 Times</h6>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="flex-45  pd-l-10">
                                                                                    <div className="event-content-item">
                                                                                        <div className="badge-group">
                                                                                            <span>Actions : </span>
                                                                                            <span className="badge active">SMS</span>
                                                                                            <span className="badge">Mail</span>
                                                                                            <span className="badge">RPC</span>
                                                                                            <span className="badge">Webhook</span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div className="event-content-item">
                                                                                        <h6>Conditions: <span> Engine Temprature > 40</span> <i className="fas fa-caret-down"></i></h6>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="flex-25">
                                                        <div className="calender-box">
                                                            <div className="month">
                                                                <ul className="list-style-none">
                                                                    <li className="prev">&#10094;</li>
                                                                    <li className="next">&#10095;</li>
                                                                    <li>
                                                                    January
                                                                    <p>2020</p>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                            <ul className="weekdays">
                                                                <li>Mo</li>
                                                                <li>Tu</li>
                                                                <li>We</li>
                                                                <li>Th</li>
                                                                <li>Fr</li>
                                                                <li>Sa</li>
                                                                <li>Su</li>
                                                            </ul>
                                                            <ul className="days">
                                                                <li>1</li>
                                                                <li>2</li>
                                                                <li>3</li>
                                                                <li>4</li>
                                                                <li>5</li>
                                                                <li>6</li>
                                                                <li>7</li>
                                                                <li>8</li>
                                                                <li>9</li>
                                                                <li>10</li>
                                                                <li>11</li>
                                                                <li>12</li>
                                                                <li>13</li>
                                                                <li>14</li>
                                                                <li>15</li>
                                                                <li>16</li>
                                                                <li>17</li>
                                                                <li>18</li>
                                                                <li>19</li>
                                                                <li>20</li>
                                                                <li>21</li>
                                                                <li>22</li>
                                                                <li>23</li>
                                                                <li>24</li>
                                                                <li><span className="active">25</span></li>
                                                                <li>26</li>
                                                                <li>27</li>
                                                                <li>28</li>
                                                                <li>29</li>
                                                                <li>30</li>
                                                                <li>31</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>*/}
                                </div>
                            </React.Fragment>
                            :
                            <div className="content-body">
                                <AddNewButton
                                    text1="No event history available"
                                    text2="You haven't created any device type(s) yet. Please create a device type first."
                                    addButtonEnable={isNavAssigned("deviceTypes")}
                                    createItemOnAddButtonClick={() => this.props.history.push(`/addOrEditDeviceType`)}
                                    imageIcon="addEventHistory.png"
                                />
                            </div>
                        }
                    </React.Fragment>
                }

                {/* device list modal */}
                {this.state.showDeviceModal &&
                    <div className="modal animated slideInDown show d-block">
                        <div className="modal-dialog modal-dialog-centered">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h6 className="modal-title">Devices for - <span>{this.state.eventsList.filter(event => event.eventId === this.state.selectedEventId)[0].eventName}</span>
                                        <button type="button" className="close" data-tooltip data-tooltip-text="Close" data-tooltip-place="bottom" onClick={() => { this.setState({ showDeviceModal: false }) }}>
                                            <i className="far fa-times"></i>
                                        </button>
                                    </h6>
                                </div>
                                <div className="modal-body overflow-y-auto" style={{ height: 400 }}>
                                    <ul className="list-style-none d-flex device-list overflow-y-auto">
                                        {this.state.deviceList.map(device =>
                                            <li className="flex-100">
                                                <div className="device-list-box">
                                                    <p className="text-underline-hover cursor-pointer" onClick={() => { this.props.history.push(`/deviceDetails/${this.state.eventsList.filter(event => event.eventId === this.state.selectedEventId)[0].deviceTypeId}/${device.deviceId}`) }}>{device.deviceName ? device.deviceName : device.deviceId}</p>
                                                </div>
                                            </li>
                                        )}
                                    </ul>
                                </div>
                                <div className="modal-footer">
                                    <button className="btn btn-success" onClick={() => { this.setState({ showDeviceModal: false }) }}>OK</button>
                                </div>
                            </div>
                        </div>
                    </div>
                }
                {/* end device list modal */}

                {this.state.isOpen &&
                    <NotificationModal
                        type={this.state.type}
                        message2={this.state.message2}
                        onCloseHandler={this.onCloseHandler}
                    />
                }
            </React.Fragment >
        );
    }
}
DeviceDashboardsEventHistory.propTypes = {
    dispatch: PropTypes.func.isRequired
};

let allSelectors = {}
Object.entries(SELECTORS).map(([key, value]) => {
    allSelectors[key] = value()
})

const mapStateToProps = createStructuredSelector(allSelectors)

function mapDispatchToProps(dispatch) {
    let allActions = { dispatch }
    Object.entries(ACTIONS).map(([key, value]) => {
        allActions[key] = (...args) => dispatch(value(...args))
    })
    return allActions
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({
    key: "deviceDashboardsEventHistory",
    reducer
});
const withSaga = injectSaga({ key: "deviceDashboardsEventHistory", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(DeviceDashboardsEventHistory);
