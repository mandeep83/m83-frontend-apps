/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/*
 *
 * DeviceDashboardsEventHistory constants
 *
 */

export const RESET_TO_INITIAL_STATE = "app/DeviceDashboardsEventHistory/RESET_TO_INITIAL_STATE";
export const RESET_TO_INITIAL_STATE_PROPS = "app/DeviceDashboardsEventHistory/RESET_TO_INITIAL_STATE_PROPS";


export const GET_CONNECTORS_LIST = {
    action : 'app/DeviceDashboardsEventHistory/GET_CONNECTORS_LIST',
    success: "app/DeviceDashboardsEventHistory/GET_CONNECTORS_LIST_SUCCESS",
    failure: "app/DeviceDashboardsEventHistory/GET_CONNECTORS_LIST_FAILURE",
    urlKey: "deviceTypeList",
    successKey: "deviceTypeListSuccess",
    failureKey: "deviceTypeListFailure",
    actionName: "deviceTypeList",
    actionArguments : []
}

export const GET_EVENT_LIST = {
    action : 'app/DevicdeviceTypeListeDashboardsEventHistory/GET_EVENT_LIST',
    success: "app/DeviceDashboardsEventHistory/GET_EVENT_LIST_SUCCESS",
    failure: "app/DeviceDashboardsEventHistory/GET_EVENT_LIST_FAILURE",
    urlKey: "eventList",
    successKey: "eventListSuccess",
    failureKey: "eventListFailure",
    actionName: "eventList",
    actionArguments : ["payload"]
}