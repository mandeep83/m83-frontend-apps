/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * ManageGitPod
 *
 */

import React from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {Helmet} from "react-helmet";
import {FormattedMessage} from "react-intl";
import {createStructuredSelector} from "reselect";
import {compose} from "redux";

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import makeSelectManageGitPod from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import messages from "./messages";

/* eslint-disable react/prefer-stateless-function */
export class ManageGitPod extends React.Component {
    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>ManageGitPod</title>
                    <meta name="description" content="Description of ManageGitPod"/>
                </Helmet>
    
                <header className="content-header d-flex">
                    <div className="flex-60">
                        <h6>Manage Git Pods</h6>
                    </div>
                    <div className="flex-40 text-right">
                        <div className="content-header-group">
                            <button className="btn btn-light" data-tooltip data-tooltip-text="Refresh" data-tooltip-place="bottom">
                                <i className="far fa-sync-alt"></i>
                            </button>
                        </div>
                    </div>
                </header>
    
                <div className="content-body">
                    <div className="card">
                        <div className="card-body">
                            <div className="flow-wrapper">
                                <span className="flow-wrapper-state bg-green"></span>
                                {/*<span className="flow-wrapper-state bg-red"></span>*/}
                                <p className="text-theme m-0">a61a844b542e4dbf80a8a678a2684cc0<span>Created 12 mins ago</span></p>
                                <h4 className="text-dark-theme fw-700">Git Pod Manager</h4>
                                <a className="f-12 mb-2 d-block" href="" target="_blank"><i className="far fa-link mr-2"></i>https://a61a844b542e4dbf80a8a678a2684cc0.flows-iot83.com/</a>
                                <h5 className="text-content f-12 m-0">
                                    <strong className="text-green"><i className="fad fa-spinner fa-spin mr-2"></i>Running</strong> - Started a min ago
                                    {/*<strong className="text-red"><i className="fad fa-stop mr-2"></i>Stopped</strong> - Stopped a min ago*/}
                                </h5>
                                <div className="button-group">
                                    <button type="button" className="btn btn-primary"><i className="far fa-pencil mr-2"></i>Launch</button>
                                    <button type="button" className="btn btn-success"><i className="far fa-play mr-2"></i>Start</button>
                                    <button type="button" className="btn btn-warning"><i className="far fa-stop mr-2"></i>Stop</button>
                                </div>
                            </div>
                        </div>
                    </div>
    
                    <div className="action-tabs">
                        <ul className="nav nav-tabs mb-3">
                            <li className="nav-item active">
                                <a className="nav-link" data-toggle="tab" data-target="#readme">README</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" data-toggle="tab" data-target="#history">History</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" data-toggle="tab" data-target="#cred">DB Credentials</a>
                            </li>
                        </ul>
                        
                        <div className="tab-content">
                            <div className="tab-pane show active" id="history">
                                <div className="content-table bg-white border-radius-4">
                                    <table className="table table-bordered mb-0">
                                        <thead>
                                        <tr>
                                            <th>Start time</th>
                                            <th>End Time</th>
                                            <th>Duration</th>
                                            <th>Started By</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>3:12:25 PM</td>
                                            <td>3:12:25 PM</td>
                                            <td>10 mins</td>
                                            <td>Nisha Tanwar</td>
                                        </tr>
                                        <tr>
                                            <td>3:12:25 PM</td>
                                            <td>3:12:25 PM</td>
                                            <td>10 mins</td>
                                            <td>Nisha Tanwar</td>
                                        </tr>
                                        <tr>
                                            <td>3:12:25 PM</td>
                                            <td>3:12:25 PM</td>
                                            <td>10 mins</td>
                                            <td>Nisha Tanwar</td>
                                        </tr>
                                        <tr>
                                            <td>3:12:25 PM</td>
                                            <td>3:12:25 PM</td>
                                            <td>10 mins</td>
                                            <td>Nisha Tanwar</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                {/*<div className="inner-loader-wrapper" style={{height: 300}}>
                                    <div className="inner-loader-content">
                                        <i className="fad fa-sync-alt fa-spin"></i>
                                    </div>
                                </div>
                                <div className="inner-message-wrapper" style={{height: 300}}>
                                    <div className="inner-message-content">
                                        <i className="fad fa-file-exclamation"></i>
                                        <h6>There is no data to display.</h6>
                                    </div>
                                </div>*/}
                            </div>

                            <div className="tab-pane" id="cred">
                                <div className="card">
                                    <div className="card-body">
                                        <div className="content-table">
                                            <table className="table table-bordered mb-0">
                                                <thead>
                                                <tr>
                                                    <th width="20%">Database</th>
                                                    <th width="25%">URL</th>
                                                    <th width="15%">Port</th>
                                                    <th width="20%">Username</th>
                                                    <th width="20%">Password</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td className="position-relative pd-l-50">
                                                        <div className="content-table-image"><img src="https://content.iot83.com/m83/dataConnectors/mongodb.png" /></div>Mongo
                                                    </td>
                                                    <td><a className="d-block text-truncate" href="https://developer.dev-iot83.com" target="_blank">https://developer.dev-iot83.com</a></td>
                                                    <td>1888</td>
                                                    <td>Nisha Tanwar</td>
                                                    <td>***************</td>
                                                </tr>
                                                <tr>
                                                    <td className="position-relative pd-l-50">
                                                        <div className="content-table-image"><img src="https://content.iot83.com/m83/dataConnectors/timescaledb.png" /></div>Timescale
                                                    </td>
                                                    <td><a className="d-block text-truncate" href="https://developer.dev-iot83.com" target="_blank">https://developer.dev-iot83.com</a></td>
                                                    <td>1888</td>
                                                    <td>Nisha Tanwar</td>
                                                    <td>***************</td>
                                                </tr>
                                                <tr>
                                                    <td className="position-relative pd-l-50">
                                                        <div className="content-table-image"><img src="https://content.iot83.com/m83/dataConnectors/mysql.png" /></div>My SQL
                                                    </td>
                                                    <td><a className="d-block text-truncate" href="https://developer.dev-iot83.com" target="_blank">https://developer.dev-iot83.com</a></td>
                                                    <td>1888</td>
                                                    <td>Nisha Tanwar</td>
                                                    <td>***************</td>
                                                </tr>
                                                <tr>
                                                    <td className="position-relative pd-l-50">
                                                        <div className="content-table-image"><img src="https://content.iot83.com/m83/dataConnectors/cassandra.png" /></div>Cassandra
                                                    </td>
                                                    <td><a className="d-block text-truncate" href="https://developer.dev-iot83.com" target="_blank">https://developer.dev-iot83.com</a></td>
                                                    <td>1888</td>
                                                    <td>Nisha Tanwar</td>
                                                    <td>***************</td>
                                                </tr>
                                                <tr>
                                                    <td className="position-relative pd-l-50">
                                                        <div className="content-table-image"><img src="https://content.iot83.com/m83/dataConnectors/elastic.png" /></div>Elastic
                                                    </td>
                                                    <td><a className="d-block text-truncate" href="https://developer.dev-iot83.com" target="_blank">https://developer.dev-iot83.com</a></td>
                                                    <td>1888</td>
                                                    <td>Nisha Tanwar</td>
                                                    <td>***************</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div className="tab-pane" id="readme">
                                <div className="card">
                                    <div className="card-body">
                                        <ul>
                                            <li className="text-content f-12 mb-3">
                                                <a href="https://www.gitpod.io/docs/" target="_blank">Gitpods</a> is an open source platform for automated and ready-to-code development environments that blends into your existing workflow. It enables developers to describe their dev environment as code and start instant and fresh development environments for each new task directly from your browser.
                                            </li>
                                            <li className="text-content f-12 mb-3">
                                                Launch your personalized <a href="https://www.gitpod.io/docs/" target="_blank">Gitpods</a> environment for your Tenant and accelerate Engineering Productivity using the buttons above.
                                            </li>
                                            <li className="text-content f-12 mb-3">
                                                Once the <a href="https://www.gitpod.io/docs/" target="_blank">Gitpods</a> environment is launched, provide, workspaces to each developer by following the steps mentioned <a href="https://www.gitpod.io/docs/getting-started/" target="_blank">here</a>
                                            </li>
                                            <li className="text-content f-12 mb-3">
                                                After you are done configuring, you will be able to see your code like <a href="https://gitpod.io/#https://gitlab.com/gitpod/spring-petclinic" target="_blank">this</a>. If your application needs a data base support/connectivity, we provide that out of the box.
                                                We have support for five different data stores viz. <a href="https://www.mongodb.com/" target="_blank">Mongo</a>, <a href="https://www.timescale.com/" target="_blank">Timescale DB</a>, <a href="https://www.elastic.co/blog/found-elasticsearch-as-nosql" target="_blank">Elastic DB</a>, <a href="https://cassandra.apache.org/" target="_blank">Cassandra</a> and <a href="https://www.mysql.com/" target="_blank">MySQL</a>. We are constantly adding more DB support to enhance the connectivity to virtually any database in the world.
                                            </li>
                                            <li className="text-content f-12 mb-3">
                                                The Credentials and connection string for each of the Database if available on the platform under the tab "DB Credentials". Use the connection String from here to be directly used in the <a href="https://www.gitpod.io/docs/" target="_blank">Gitpods</a> environment to establish DB connectivity
                                            </li>
                                            <li className="text-content f-12 mb-3">
                                                Once your are done with the code, we have two files called <a href="https://" target="_blank">build.sh</a> and <a href="https://#" target="_blank">Dockerfile</a> that need to use used to create and push Docker images to your Dockerhub Repo so that the same image can be used while creating a custom service for you. These files will become a part fo your code base so that they can used from anywhere in future.
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
                
            </React.Fragment>
        );
    }
}

ManageGitPod.propTypes = {
    dispatch: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
    managegitpod: makeSelectManageGitPod()
});

function mapDispatchToProps(dispatch) {
    return {
        dispatch
    };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({key: "manageGitPod", reducer});
const withSaga = injectSaga({key: "manageGitPod", saga});

export default compose(
    withReducer,
    withSaga,
    withConnect
)(ManageGitPod);
