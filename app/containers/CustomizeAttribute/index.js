/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

/**
 *
 * CustomizeAttribute
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectCustomizeAttribute from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

/* eslint-disable react/prefer-stateless-function */
export class CustomizeAttribute extends React.Component {
    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>Customize Attributes</title>
                    <meta name="description" content="Description of CustomizeAttribute" />
                </Helmet>
                
                <header className="content-header d-flex">
                    <div className="flex-60">
                        <h6>Customize Attributes</h6>
                    </div>
                    <div className="flex-40 text-right">
                        <div className="content-header-group">
                        </div>
                    </div>
                </header>
                
                <div className="content-body" id="customize-attributes">
                    <div className="form-content-wrapper">
                        <div className="form-content-box">
                            <div className="form-content-header">
                                <p>Basic Information</p>
                            </div>
                            <div className="form-content-body">
                                <div className="form-group">
                                    <label className="form-group-label">Device Type : <i className="fas fa-asterisk form-group-required"></i></label>
                                    <div className="input-group">
                                        <select className="form-control">
                                            <option>select</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div className="form-content-box">
                            <div className="form-content-header">
                                <p>Attributes</p>
                            </div>
                            
                            <div className="form-content-body">
                                <ul className="list-style-none d-flex attribute-icon-list">
                                    <li>
                                        <div className="attribute-icon-box">
                                            <div className="attribute-icon">
                                                <i className="fad fa-image-polaroid"></i>
                                            </div>
                                            <p>Attribute Name</p>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="attribute-icon-box">
                                            <div className="attribute-icon">
                                                <i className="fad fa-image-polaroid"></i>
                                            </div>
                                            <p>Attribute Name</p>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="attribute-icon-box">
                                            <div className="attribute-icon">
                                                <i className="fad fa-image-polaroid"></i>
                                            </div>
                                            <p>Attribute Name</p>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="attribute-icon-box">
                                            <div className="attribute-icon">
                                                <i className="fad fa-image-polaroid"></i>
                                            </div>
                                            <p>Attribute Name</p>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="attribute-icon-box">
                                            <div className="attribute-icon">
                                                <i className="fad fa-image-polaroid"></i>
                                            </div>
                                            <p>Attribute Name</p>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    
                    <div className="form-info-wrapper">
                        <div className="form-info-header">
                            <h5>Icon Library</h5>
                        </div>
                        
                        <div className="form-info-body">
                            <div className="form-group mr-b-10">
                                <div className="search-wrapper">
                                    <div className="search-wrapper-group">
                                        <input type="text" className="form-control" placeholder="Search..." />
                                        <span className="search-wrapper-icon"><i className="far fa-search"></i></span>
                                    </div>
                                </div>
                            </div>
                            
                            <ul className="list-style-none icon-library d-flex">
                                <li>
                                    <div className="icon-image">
                                        <i className="far fa-search"></i>
                                    </div>
                                    <h6>Search</h6>
                                </li>
                                <li>
                                    <div className="icon-image">
                                        <i className="far fa-arrow-up"></i>
                                    </div>
                                    <h6>Up</h6>
                                </li>
                                <li>
                                    <div className="icon-image">
                                        <i className="far fa-clone"></i>
                                    </div>
                                    <h6>Copy</h6>
                                </li>
                                <li>
                                    <div className="icon-image">
                                        <i className="far fa-sync-alt"></i>
                                    </div>
                                    <h6>Refresh</h6>
                                </li>
                                <li>
                                    <div className="icon-image">
                                        <i className="far fa-stop"></i>
                                    </div>
                                    <h6>Stop</h6>
                                </li>
                                <li>
                                    <div className="icon-image">
                                        <i className="far fa-play"></i>
                                    </div>
                                    <h6>Play</h6>
                                </li>
                                <li>
                                    <div className="icon-image">
                                        <i className="far fa-plus"></i>
                                    </div>
                                    <h6>Add</h6>
                                </li>
                                <li>
                                    <div className="icon-image">
                                        <i className="far fa-clock"></i>
                                    </div>
                                    <h6>Status</h6>
                                </li>
                            </ul>
                        </div>
                        <div className="form-info-footer">
                            <button className="btn btn-success">Save</button>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

CustomizeAttribute.propTypes = {
    dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
    customizeattribute: makeSelectCustomizeAttribute(),
});

function mapDispatchToProps(dispatch) {
    return {
        dispatch,
    };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: 'customizeAttribute', reducer });
const withSaga = injectSaga({ key: 'customizeAttribute', saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(CustomizeAttribute);
