/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * AddorEditNoteBook
 *
 */

import React from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {Helmet} from "react-helmet";
import {FormattedMessage} from "react-intl";
import {createStructuredSelector} from "reselect";
import {compose} from "redux";
import { cloneDeep } from "lodash";
import * as SELECTORS from './selectors';
import * as ACTIONS from './actions';
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import Loader from "../../components/Loader";
import makeSelectAddorEditNoteBook from "./selectors";
import MQTT from 'mqtt';
import { getVMQCredentials, convertTimestampToDate } from '../../commonUtils';
import NotificationModal from '../../components/NotificationModal/Loadable';
import reducer from "./reducer";
import saga from "./saga";
import messages from "./messages";
import AceEditor from "react-ace";
import "ace-builds/src-noconflict/mode-scala";
import "ace-builds/src-noconflict/theme-monokai";
import "ace-builds/src-noconflict/ext-language_tools";
import StatsChart from "../DeviceAttributes/Charts/StatsChart";


/* eslint-disable react/prefer-stateless-function */
let client;
export class AddorEditNoteBook extends React.Component {
    
    state={
        payload:{
            description: "",
            notebookName: "",
            type: "etlNotebook",
            notebookId: "",
            codeBlocks: []
        },
        miniLoader: false,
        isDescriptionChanged: false,
        btnDisabled: true,
        isFetching: true,
        responseReceived:[],
        sampleCodes: [
            {
                label:"Read Data from Mongo",
                value:"readMongoData",
                code:`val dataset = spark.read.format("com.mongodb.spark.sql.DefaultSource") \n    .option("spark.mongodb.input.uri","mongodb://localhost:27017")\n    .option("inferSchema",true).option("database","{database Name}")\n    .option("collection","{collection Name}").load()`
            },
            {
                label:"Transformation",
                value:"tranformationCode",
                code:`val dataset = df.select("dateCreated","state","definition","sinkStatus").where("state = 'running'")`
            },
            {
                label:"Write Data to Mongo",
                value:"writeDateToMongo",
                code:`dataset.write.format("com.mongodb.spark.sql")\n.option("uri","mongodb://localhost:27017")\n.option("database", "m83").option("collection", "hundredClub")\n.mode("append").save()`
            },
            {
                label: "MQTT to Mongo",
                value: "mqttToMongo",
                code: `import org.apache.spark.sql.Encoders\nimport org.apache.spark.sql.types._\nimport org.apache.spark.sql.functions._\nimport org.apache.spark.sql.ForeachWriter\n\nval schema=commonUtils.getSchema({SCHEMA},spark)\n\nval dataset = mqtt.createStream(spark,{TOPIC})\n\nval transformedData = dataset.select(from_json(col("payload").cast("String"),schema).as("data"))\n\ntransformedData.select(to_json(struct(col("data.*")))).as(Encoders.STRING)
               .writeStream.queryName({ETL_NAME})
               .outputMode("update")
               .foreach(mongo.mongoWriter({TENANT_NAME} , {COLLECTION_NAME} ))
               .start()
               .awaitTermination()`
            }
            
        ]
        
    }
    componentDidMount()
    {
        if(this.props.match.params.id)
        {
            this.setState({
                btnDisabled: false,
            },() => this.props.getNoteBookById(this.props.match.params.id))
        }
    }
    componentWillReceiveProps(nextProps)
    {
        
        if(nextProps.createNoteBookSuccess && nextProps.createNoteBookSuccess !== this.props.createNoteBookSuccess)
        {
            let payload = cloneDeep(this.state.payload)
            payload.notebookId = nextProps.createNoteBookSuccess.data.noteBookId
            let selectedTopic = `${localStorage.getItem("tenant")}_${localStorage.getItem("selectedProjectId")}_${payload.notebookId}`
            this.setState({
                payload,
                modalType: "success",
                miniLoader: false,
                isDescriptionChanged: false,
                isOpen: true,
                selectedTopic,
                isFetching: false,
                message2: nextProps.createNoteBookSuccess.message,
            }, () => {
                this.props.history.push(`/addOrEditNoteBook/${payload.notebookId}`)
                this.createMqttConnection()
            });
        }
        else if(nextProps.createNoteBookFailure && nextProps.createNoteBookFailure !== this.props.createNoteBookFailure)
        {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.createNoteBookFailure.message,
                miniLoader: false,
            });
        }

        if(nextProps.updateNoteBookSuccess && nextProps.updateNoteBookSuccess !== this.props.updateNoteBookSuccess)
        {
            let payload = cloneDeep(this.state.payload),
            originalDescription = payload.description;
            nextProps.updateNoteBookSuccess.data.codeBlockIds && payload.codeBlocks.map((codeBlock,index)=>{
                if(!codeBlock.codeBlockId) {
                    codeBlock["codeBlockId"] = nextProps.updateNoteBookSuccess.data.codeBlockIds[index],
                    codeBlock["isRestartEnabled"] = false,
                    codeBlock["monitoringData"] = null,
                    codeBlock["monitoringDuration"] = 5
                }
            })
            
            this.setState((prevState)=>({
                payload,
                isFetching: false,
                miniLoader: false,
                //isUpdateMode: true,
                originalDescription,
                isCodeBlockDeleted: false,
                isDescriptionChanged: nextProps.updateNoteBookSuccess.data.codeBlockIds ? prevState.isDescriptionChanged: false,
                modalType: "success",
                isOpen: true,
                message2: nextProps.updateNoteBookSuccess.message,
            }))
        }
        if(nextProps.updateNoteBookFailure && nextProps.updateNoteBookFailure !== this.props.updateNoteBookFailure)
        {
            let payload =  cloneDeep(this.state.payload); 
            if (this.state.isCodeBlockDeleted) {
                payload.codeBlocks = this.state.oldCodeBlock
            }
            this.setState({
                modalType: "error",
                isOpen: true,
                payload,
                isFetching: false,
                isCodeBlockDeleted: false,
                isDescriptionChanged: false,
                miniLoader: false,
                message2: nextProps.updateNoteBookFailure.message,
            })
        }

        if(nextProps.debugNoteBookSuccess && nextProps.debugNoteBookSuccess !== this.props.debugNoteBookSuccess){
            let payload = cloneDeep(this.state.payload)
            payload.codeBlocks[this.state.actionIndex].status = "start"
            
            this.setState({
                payload,
                playbtnAnimation: false,
                isclearClicked: false,
                btnDisabled: false,
                modalType: "success",
                isOpen: true,
                message2: nextProps.debugNoteBookSuccess.message,
            })    
        }

        if(nextProps.debugNoteBookFailure && nextProps.debugNoteBookFailure !== this.props.debugNoteBookFailure){
            this.setState({
                modalType: "error",
                playbtnAnimation: false,
                btnDisabled: false,
                isclearClicked: false,
                isOpen: true,
                message2: nextProps.debugNoteBookFailure.message,
            }) 
        }
        if(nextProps.stopNoteBookCodeBlockSuccess && nextProps.stopNoteBookCodeBlockSuccess !== this.props.stopNoteBookCodeBlockSuccess){
            let payload = cloneDeep(this.state.payload)
            payload.codeBlocks[this.state.actionIndex].status = "stop"
            this.setState({
                payload,
                btnDisabled: false,
                modalType: "success",
                isOpen: true,
                playbtnAnimation: false,
                message2: nextProps.stopNoteBookCodeBlockSuccess.message,
            })    
        }

        if(nextProps.stopNoteBookCodeBlockFailure && nextProps.stopNoteBookCodeBlockFailure !== this.props.stopNoteBookCodeBlockFailure){
            this.setState({
                modalType: "error",
                playbtnAnimation: false,
                isOpen: true,
                message2: nextProps.stopNoteBookCodeBlockFailure.message,
            }) 
        }

        if(nextProps.getNoteBookByIdSuccess && nextProps.getNoteBookByIdSuccess !== this.props.getNoteBookByIdSuccess)
        {
            let payload = cloneDeep(nextProps.getNoteBookByIdSuccess);
            
            if (!payload.codeBlocks) {
                payload.codeBlocks = []
            }

            payload.codeBlocks && payload.codeBlocks.map((codeblock) => {
                codeblock["monitoringData"] = null,
                codeblock["monitoringDuration"] = 5
            })

            let selectedTopic = `${localStorage.getItem("tenant")}_${localStorage.getItem("selectedProjectId")}_${payload.notebookId}`
            this.setState({
                payload,
                selectedTopic,
                isFetching: false,
                isDescriptionChanged: false,
            },()=>{
                this.createMqttConnection()
            })
        }
        
        if(nextProps.getNoteBookByIdFailure && nextProps.getNoteBookByIdFailure !== this.props.getNoteBookByIdFailure)
        {
            this.setState({
                modalType: "error",
                isOpen: true,
                isFetching: false,
                isDescriptionChanged: false,
                message2: nextProps.getNoteBookByIdFailure.message,
            }) 
        }

        if(nextProps.restartNoteBookCodeBlockSuccess && nextProps.restartNoteBookCodeBlockSuccess !== this.props.restartNoteBookCodeBlockSuccess) {
            let payload = cloneDeep(this.state.payload)
            let id = nextProps.restartNoteBookCodeBlockSuccess.codeBlockId
            payload.codeBlocks.find(({codeBlockId}) => codeBlockId === id).isRestartEnabled = false
            this.setState({
                payload,
                modalType: "success",
                isOpen: true,
                message2: nextProps.restartNoteBookCodeBlockSuccess.message
            })
        }

        if(nextProps.restartNoteBookCodeBlockFailure && nextProps.restartNoteBookCodeBlockFailure !== this.props.restartNoteBookCodeBlockFailure) {
            let payload = cloneDeep(this.state.payload)
            let id = nextProps.restartNoteBookCodeBlockFailure.addOns.payload.codeBlockId
            payload.codeBlocks.find(({codeBlockId}) => codeBlockId === id).isRestartEnabled = false
            this.setState({
                payload,
                modalType: "error",
                isOpen: true,
                message2: nextProps.restartNoteBookCodeBlockFailure.error,
            }) 
        }

        if (nextProps.monitorNotebookSuccess && nextProps.monitorNotebookSuccess !== this.props.monitorNotebookSuccess) {
            let payload = cloneDeep(this.state.payload);
            let data = nextProps.monitorNotebookSuccess.data;
            payload.codeBlocks.find(({ codeBlockId }) => codeBlockId === this.state.currentCodeBlockId).monitoringData = data;
            this.setState({
                payload,
                isfetchingStatsData: false,
            })
        }

        if(nextProps.monitorNotebookFailure && nextProps.monitorNotebookFailure !== this.props.monitorNotebookFailure) {
            this.setState({
                modalType: "error",
                isfetchingStatsData: false,
                isOpen: true,
                message2: nextProps.monitorNotebookFailure,
            });
        }
    }
    onChangeHandler = ({ currentTarget }) => {
        let payload = cloneDeep(this.state.payload),
        isDescriptionChanged = this.state.isDescriptionChanged
        payload[currentTarget.id] = currentTarget.value
        if (currentTarget.id === "description") {
            isDescriptionChanged = (this.props.match.params.id) ? (this.state.originalDescription !== currentTarget.value) : true
        }
        this.setState({
            payload,
            isDescriptionChanged
        })     
    }
    createNoteBookHandler = () => {
        let payload = cloneDeep(this.state.payload)
        payload.description = (payload.description.trim() === "") ? "NA" : payload.description;
        let originalDescription = payload.description;
        this.setState({
            payload,
            miniLoader: true,
            originalDescription,
            isDescriptionChanged: false
        }, () => this.props.createNoteBook(payload))
    }
    onCloseHandler = () => {
        this.setState({
            isOpen: false,
            message2: '',
            modalType: '',
        })
    }
    addNewBlockHandler = () => {
        let payload = cloneDeep(this.state.payload)
        payload.codeBlocks.length < 3 && payload.codeBlocks.push({
            codeBlockName: "testStream",
            codeBlockContent: `val dataset = spark.read.format("com.mongodb.spark.sql.DefaultSource") \n    .option("spark.mongodb.input.uri","mongodb://localhost:27017")\n    .option("inferSchema",true).option("database","{database Name}")\n    .option("collection","{collection Name}").load()`,
            enableBlock: false,
            scheduledAt: "",
            status: "stop"
        })
        this.setState({
            payload,
            btnDisabled: false,
        })
    }
    updateDescriptionHandler = () => {
        let payload = cloneDeep(this.state.payload)
        payload.description = (payload.description.trim() === "") ? "NA" : payload.description;
        this.setState({
            miniLoader: true,
            payload
        },() => this.props.updateNoteBook({
            "description": payload.description,
            "notebookName": payload.notebookName,
            "id": payload.notebookId
        }))
    }
    updateNoteBookHandler = () =>{
        let payload = cloneDeep(this.state.payload)
        this.setState({
            isFetching: true
        },() => this.props.updateNoteBook({
            "codeBlocks": payload.codeBlocks,
            "id": payload.notebookId
        }))
    }
    onCodeChangeHandler = (content,index) =>{
        let payload = cloneDeep(this.state.payload)
        payload.codeBlocks[index].codeBlockContent = content
        this.setState({payload})
    }
    codeBlockHandler = (index,type) =>{
        let payload = cloneDeep(this.state.payload),
        codeBlockPayload = {
            "codeBlockContent": payload.codeBlocks[index].codeBlockContent,
            "codeBlockId": payload.codeBlocks[index].codeBlockId,
            "noteBookId": payload.notebookId
        };
        (type === "play") ? this.props.debugNoteBook(codeBlockPayload) : this.props.stopNoteBookCodeBlock(codeBlockPayload)
        this.setState({
            payload,
            playbtnAnimation: true,
            btnDisabled: true,
            actionIndex: index,
        })
    }
    
    showModal=()=>{
        let selectedTopic = `${localStorage.getItem("tenant")}_${localStorage.getItem("selectedProjectId")}_${this.state.payload.notebookId}`
        this.setState({
            showSniffingModal:  true,
            selectedTopic,
        })
    }
    
    createMqttConnection = () => {
        let brokerURL = window.location.origin === "http://localhost:3001" ? window.API_URL.split("//")[1].substring(0, window.API_URL.split("//")[1].length - 1) : window.location.host,
            count = 0,
            credentials = getVMQCredentials();
        const options = {
            host: brokerURL,
            protocol: 'wss',
            path: '/connectmqtt',
            username: credentials.userName,
            password: credentials.password,
        };
        client = MQTT.connect(options);
        let self = this;
        client.on('close', function (response) {
            if (response && count > 4) {
                client.end();
            }
        });
        client.on("connect",function(){
            self.subscribeMqttTopic(self.state.selectedTopic)
        })
        client.on('reconnect', function () {
            count++;
        });
    }
    subscribeMqttTopic = (selectedTopic) => {
        let _this = this
        client.subscribe(selectedTopic, function (err) {
            if (!err) {
                client.on('message', function (topic, message) {
                    let responseReceived = [... _this.state.responseReceived] 
                    responseReceived.push(message.toString())
                    _this.setState({responseReceived}) 
                });
            }
        })
    }

    clearSniffingModal = () => {
        let responseReceived= cloneDeep(this.state.responseReceived);
        responseReceived = [];
        this.setState({
            responseReceived,
            isclearClicked: true,
        })
    }

    closeSniffingModal = () => {
        this.setState({
            showSniffingModal: false,
        })
    }

    deleteCodeBlockHandler = (itemIndex) => {
        let payload =  cloneDeep(this.state.payload)
        let oldCodeBlock = cloneDeep(payload.codeBlocks);
        payload.codeBlocks.splice(itemIndex,1)
        let btnDisabled = (payload.codeBlocks.length == 0)
        this.setState({
            payload,
            btnDisabled,
            oldCodeBlock,
            deletedIndex: itemIndex,
            isCodeBlockDeleted: true,
            isFetching: true
        },() => this.props.updateNoteBook({
            "codeBlocks": payload.codeBlocks,
            "id": payload.notebookId
        }))
    }

    refreshComponent = () => {
        if(this.props.match.params.id)
        {
            this.setState({
                isFetching: true,
            },()=>{
                client && client.end();
                this.props.getNoteBookById(this.props.match.params.id)
            })
        }
    }

    selectCodeForCodeBlock = (value,index) => {
        let payload = cloneDeep(this.state.payload)
        payload.codeBlocks[index].codeBlockContent = value
        this.setState({payload})
    }

    restartCodeBlockHandler = (index) => {
        let payload = cloneDeep(this.state.payload);
        payload.codeBlocks[index].isRestartEnabled = true
        this.setState({
            payload,
        },() =>  this.props.restartNoteBookCodeBlock({
                    codeBlockId: payload.codeBlocks[index].codeBlockId,
                    noteBookId: payload.notebookId
                })
        )
    }

    handleNoteBookMonitoring = (notebookId, currentCodeBlockId) => {
        let payload = {
            "notebookId": notebookId,
            "codeBlockId": currentCodeBlockId,
            "duration": 5
        }
        this.setState({
            showInfoModal: true,
            isfetchingStatsData: true,
            currentCodeBlockId
        },() => { this.props.monitorNotebook(payload) })
    }

    render() {
        let codeBlocks = this.state.payload.codeBlocks;
        let currentMonitoringCodeBlockIndex = this.state.currentCodeBlockId && codeBlocks.findIndex(({codeBlockId}) => codeBlockId === this.state.currentCodeBlockId);
        return (
            <React.Fragment>
                <Helmet>
                    <title>AddOrEditNoteBook</title>
                    <meta name="description" content="Description of AddOrEditNoteBook"/>
                </Helmet>

                <header className="content-header d-flex">
                    <div className="flex-60">
                        <div className="d-flex">
                            <h6 className="previous" onClick={() => {this.props.history.push('/notebooks')}}>Notebooks</h6>
                            <h6 className="active">Add New</h6>
                        </div>
                    </div>
                    <div className="flex-40 text-right">
                        <div className="content-header-group">
                            <button className="btn btn-light" data-tooltip data-tooltip-text="Refresh" disabled={!this.props.match.params.id} data-tooltip-place="bottom" onClick={this.refreshComponent}>
                                <i className="far fa-sync-alt"></i>
                            </button>
                            <button className="btn btn-light" data-tooltip data-tooltip-text="Back" data-tooltip-place="bottom" onClick={() => this.props.history.push('/notebooks')}>
                                <i className="fas fa-angle-left"></i>
                            </button>
                            {/* <button className="btn btn-primary" data-tooltip data-tooltip-text="Add New CodeBlock (Maximum 3)" data-tooltip-place="bottom" onClick={this.addNewBlockHandler} disabled={!(this.props.match.params.id && this.state.payload.notebookId && this.state.payload.codeBlocks.length < 3)}>
                                <i className="far fa-plus"></i>
                            </button> */}
                        </div>
                    </div>
                </header>
                
                {this.state.isFetching && this.props.match.params.id ?
                    <Loader/> :
                    <div className="content-body">
                        <div className="form-content-wrapper" style={{paddingBottom : this.state.showSniffingModal && '370px'}}>
                            <div className="form-content-box">
                                <div className="form-content-header">
                                    <p>Basic Information</p>
                                </div>
                                <div className="form-content-body">
                                    <div className="d-flex">
                                        <div className="flex-50 pd-r-10">
                                            <div className="form-group">
                                                <label className="form-group-label">Name : <i className="fas fa-asterisk form-group-required"></i></label>
                                                <input type="text" className="form-control" id="notebookName" value={(this.state.payload.notebookName)} readOnly={this.props.match.params.id} pattern="^[a-zA-z0-9]{1,}$" onChange={this.onChangeHandler}/>
                                            </div>
                                        </div>
                                        <div className="flex-50 pd-l-10">
                                            <div className="form-group">
                                                <label className="form-group-label">Description :</label>
                                                <input type="text" className="form-control" id="description" value={(this.state.payload.description)} onChange={this.onChangeHandler}/>
                                            </div>
                                        </div>
                                    </div>
                                    {this.state.miniLoader &&
                                        <div className="form-content-body-overlay">
                                            <i className="fad fa-sync-alt fa-spin text-theme f-20"></i>
                                        </div>
                                    }
                                </div>
                                <div className="form-content-footer">
                                {this.props.match.params.id ?
                                    <button type='button' className="btn btn-primary" disabled={!(this.props.match.params.id && this.state.isDescriptionChanged)} onClick={this.updateDescriptionHandler}>Update</button>
                                    :
                                    <button className="btn btn-success" type="button" onClick={this.createNoteBookHandler} disabled={this.props.match.params.id || (!this.state.payload.notebookName)}>
                                        Next<i className="far fa-angle-double-right mr-l-5"></i>
                                    </button>
                                }
                                </div>
                            </div>
    
                            <div className="form-content-box">
                                <div className="form-content-header">
                                    <p>CodeBlocks <small>Note: Maximum 3 CodeBlocks are allowed</small></p>
                                </div>
                                
                                <div className="form-content-body">
                                    {codeBlocks && codeBlocks.map((block,index)=>(
                                        <div className="card shadow-none border" key={index}>
                                            <div className="card-header">
                                                <h6>Action {index + 1}</h6>
                                                <div className="form-group card-header-filter" style={{marginRight: "140px"}}>
                                                    <label className="form-group-label d-inline-block mr-2">Sample Code :</label>
                                                    <select id='selectedCode' value={block.codeBlockContent} disabled={codeBlocks[index].status === "start"} className="form-control d-inline-block w-auto"  onChange={({currentTarget})=>this.selectCodeForCodeBlock(currentTarget.value,index)}>
                                                        <option>Select</option>
                                                        {this.state.sampleCodes.map( val => (
                                                            <option value={val.code} key={val.value} >{val.label}</option>
                                                        ))}
                                                    </select>
                                                </div>
                                                <div className="button-group">
                                                    {/*<button className="btn-transparent btn-transparent-yellow" data-tooltip data-tooltip-text="Add Schedule" data-tooltip-place="bottom" data-toggle="modal" data-target="#scheduleModal">
                                                    <i className="far fa-clock"></i>
                                                </button>
                                                <button className="btn-transparent btn-transparent-blue" data-tooltip data-tooltip-text="Add Dependency" data-tooltip-place="bottom" data-toggle="modal" data-target="#dependencyModal">
                                                    <i className="far fa-plus"></i>
                                                </button>*/}
                                                    {(codeBlocks[index].status === "stop") ?
                                                        <button type="button" className="btn-transparent btn-transparent-green" disabled={!block.codeBlockId || block.codeBlockContent.trim() === ""} data-tooltip data-tooltip-text="Run" data-tooltip-place="bottom" onClick={() => this.codeBlockHandler(index,"play")}>
                                                            {this.state.playbtnAnimation && this.state.actionIndex === index ? <i className="far fa-cog fa-spin"></i> : <i className="far fa-play"></i> }
                                                        </button> :
                                                        <button type="button" className="btn-transparent btn-transparent-gray" disabled={(!block.codeBlockId)? true : false} data-tooltip data-tooltip-text="Stop" data-tooltip-place="bottom" onClick={() => this.codeBlockHandler(index,"stop")}>
                                                            {this.state.playbtnAnimation && this.state.actionIndex === index ? <i className="far fa-cog fa-spin"></i> : <i className="far fa-stop"></i> }
                                                        </button>
                                                    }
                                                    <button className="btn-transparent btn-transparent-cyan" data-tooltip data-tooltip-text="Statistics" data-tooltip-place="bottom" data-toggle="modal" disabled={codeBlocks[index].status === "stop"} onClick={() => this.handleNoteBookMonitoring(this.state.payload.notebookId, codeBlocks[index].codeBlockId)}>
                                                        <i className="far fa-chart-bar"></i>
                                                    </button>
                                                    <button type="button" className="btn-transparent btn-transparent-yellow" data-tooltip data-tooltip-text="Restart" data-tooltip-place="bottom" onClick={() => this.restartCodeBlockHandler(index)} disabled={codeBlocks[index].status === "stop"}> 
                                                        <i className={codeBlocks[index].isRestartEnabled ? "far fa-cog fa-spin" : "far fa-sync-alt"}></i>
                                                    </button>
                                                    <button className="btn-transparent btn-transparent-red" disabled={(codeBlocks[index].status === "start")} data-tooltip data-tooltip-text="Delete" data-tooltip-place="bottom" onClick={()=>this.deleteCodeBlockHandler(index)}>
                                                        <i className="far fa-trash-alt"></i>
                                                    </button>
                                                    {/*
                                                <button className="btn-transparent btn-transparent-blue" data-tooltip data-tooltip-text="Refresh" data-tooltip-place="bottom">
                                                    <i className="far fa-sync-alt"></i>
                                                </button>
                                                <button className="btn-transparent btn-transparent-cyan" data-tooltip data-tooltip-text="Clone" data-tooltip-place="bottom">
                                                    <i className="far fa-clone"></i>
                                                </button>
                                                <button className="btn-transparent btn-transparent-red" data-tooltip data-tooltip-text="Up" data-tooltip-place="bottom">
                                                    <i className="far fa-arrow-up"></i>
                                                </button>
                                                <button className="btn-transparent btn-transparent-red" data-tooltip data-tooltip-text="Down" data-tooltip-place="bottom">
                                                    <i className="far fa-arrow-down"></i>
                                                </button>*/}
                                                </div>
                                            </div>
            
                                            <div className="card-body p-2">
                                                <div className="ace-editor">
                                                    <AceEditor
                                                        placeholder="Type some code..."
                                                        theme="monokai"
                                                        mode="scala"
                                                        name="ace_editor"
                                                        onChange={content => this.onCodeChangeHandler(content,index)}
                                                        value={this.state.payload.codeBlocks[index].codeBlockContent}
                                                        showPrintMargin={false}
                                                        showGutter={true}
                                                        highlightActiveLine={true}
                                                        setOptions={{
                                                            enableBasicAutocompletion: false,
                                                            enableLiveAutocompletion: true,
                                                            enableSnippets: false,
                                                            showLineNumbers: true,
                                                            tabSize: 2,
                                                        }}
                                                        height="100%"
                                                        width="100%"
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                    ))}
    
                                    <div className="p-5 text-center">
                                        <button className="btn btn-link" onClick={this.addNewBlockHandler} disabled={!(this.props.match.params.id && this.state.payload.notebookId && codeBlocks.length < 3)}><i className="far fa-plus"></i>Add New CodeBlock</button>
                                    </div>
                                </div>
    
                                <div className="form-content-footer">
                                    <button type='button' className="btn btn-primary" disabled={!Boolean(this.props.match.params.id) || this.state.btnDisabled || codeBlocks.length === 0} onClick={this.updateNoteBookHandler}>{codeBlocks.length > 0 && codeBlocks[0].codeBlockId ? 'Update' : 'Save' }</button>
                                </div>
                            </div>
                        </div>
        
                        <div className="form-info-wrapper">
                            <div className="form-info-body">
                                <div className="form-info-icon"><img src={require('../../assets/images/other/notebook.png')} /></div>
                                <h5>What are Notebooks ?</h5>
                                <ul className="list-style-none form-info-list mb-5">
                                    <li><p>In Notebook you can write code in scala along with apache spark jobs (ETLs : Extract transform and Load).</p></li>
                                    <li><p>One can write streaming jobs & batch jobs and can use the power of spark in-memory processing engine.</p></li>
                                    <li><p>It currently supports the following Data Sources & Sinks - Kafka , MQTT, RabbitMQ, Mongo, HDFS, Amazon S3, TimescaleDB.</p></li>
                                    <li><p>Notebooks also comes with the scheduling feature for its code block execution.</p></li>
                                    <li><p>Uses include: Data Cleaning and Transformation, Numerical Simulation, Statistical Modeling, Data Visualization, Machine Learning, Spark-Structured Streaming and much more.</p></li>
                                </ul>
                            </div>
                            <div className="form-info-footer">
                                <button type='button' className="btn btn-light" onClick={() => {this.props.history.push('/notebooks')}}>Cancel</button>
                                <button type="button" className="btn btn-warning" disabled={this.state.payload.codeBlocks.length === 0 || !this.state.payload.codeBlocks.find(codeBlock => codeBlock.codeBlockId) } onClick={() => this.showModal()}><i className="fad fa-bug mr-r-5"></i>Debug</button>
                            </div>
                        </div>
                    </div>
                }
                
                {/* schedule modal */}
                <div className="modal animated slideInDown" id="scheduleModal">
                    <div className="modal-dialog modal-lg modal-dialog-centered">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h6 className="modal-title">Add schedule for - <span>Code Block -1</span>
                                    <button type="button" className="close" data-dismiss="modal"><i className="far fa-times"></i></button>
                                </h6>
                            </div>
                            <div className="modal-body">
                                <div className="form-group">
                                    <label className="form-group-label">Cron Expression : <i className="fas fa-asterisk form-group-required"></i></label>
                                    <textarea rows="5" className="form-control"/>
                                </div>
                                <div className="form-group">
                                    <label className="form-group-label">Time Interval :</label>
                                    <select className="form-control">
                                        <option>Select</option>
                                    </select>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-dark" data-dismiss="modal">Cancel</button>
                                <button type="button" className="btn btn-success"> Save</button>
                            </div>
                        </div>
                    </div>
                </div>
                {/* end schedule modal */}

                {/* dependency modal  */}
                <div className="modal animated slideInDown" id="dependencyModal">
                    <div className="modal-dialog modal-lg modal-dialog-centered">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h6 className="modal-title">Add dependencies for - <span>Code Block -1</span>
                                    <button type="button" className="close" data-dismiss="modal"><i className="far fa-times"></i></button>
                                </h6>
                            </div>
                            <div className="modal-body">
                                <div className="attribute-list">
                                    <div className="attribute-list-header d-flex">
                                        <p className="flex-45">Artifact</p>
                                        <p className="flex-45">Exclude</p>
                                        <p className="flex-10">Actions</p>
                                    </div>
                                    <div className="attribute-list-body">
                                        <ul className="list-style-none d-flex align-items-center">
                                            <li className="flex-45">
                                                <input type="text" id="name" name="name" className="form-control"/>
                                            </li>
                                            <li className="flex-45">
                                                <input type="text" id="name" name="name" className="form-control"/>
                                            </li>
                                            <li className="flex-10">
                                                <div className="button-group">
                                                    <button className="btn-transparent btn-transparent-blue" data-tooltip="true" data-tooltip-text="Edit" data-tooltip-place="bottom">
                                                        <i className="far fa-pencil"></i>
                                                    </button>
                                                    <button className="btn-transparent btn-transparent-red" data-tooltip="true" data-tooltip-text="Delete" data-tooltip-place="bottom">
                                                        <i className="far fa-trash-alt"></i>
                                                    </button>
                                                </div>
                                            </li>
                                        </ul>
                                        <ul className="list-style-none d-flex align-items-center">
                                            <li className="flex-45">
                                                <input type="text" id="name" name="name" className="form-control"/>
                                            </li>
                                            <li className="flex-45">
                                                <input type="text" id="name" name="name" className="form-control"/>
                                            </li>
                                            <li className="flex-10">
                                                <div className="button-group">
                                                    <button className="btn-transparent btn-transparent-blue" data-tooltip="true" data-tooltip-text="Edit" data-tooltip-place="bottom">
                                                        <i className="far fa-pencil"></i>
                                                    </button>
                                                    <button className="btn-transparent btn-transparent-red" data-tooltip="true" data-tooltip-text="Delete" data-tooltip-place="bottom">
                                                        <i className="far fa-trash-alt"></i>
                                                    </button>
                                                </div>
                                            </li>
                                        </ul>
                                        <ul className="list-style-none d-flex align-items-center">
                                            <li className="flex-45">
                                                <input type="text" id="name" name="name" className="form-control"/>
                                            </li>
                                            <li className="flex-45">
                                                <input type="text" id="name" name="name" className="form-control"/>
                                            </li>
                                            <li className="flex-10">
                                                <div className="button-group">
                                                    <button className="btn-transparent btn-transparent-blue" data-tooltip="true" data-tooltip-text="Edit" data-tooltip-place="bottom">
                                                        <i className="far fa-pencil"></i>
                                                    </button>
                                                    <button className="btn-transparent btn-transparent-red" data-tooltip="true" data-tooltip-text="Delete" data-tooltip-place="bottom">
                                                        <i className="far fa-trash-alt"></i>
                                                    </button>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-dark" data-dismiss="modal">Cancel</button>
                                <button type="button" className="btn btn-success"> Save</button>
                            </div>
                        </div>
                    </div>
                </div>
                {/* end dependency modal  */}
                
                {/* debug window */}
                {this.state.showSniffingModal &&
                <div className="debug-wrapper animated slideInUp">
                    <div className="debug-header">
                        <h6>Debug for - <span className="text-dark-theme">{this.state.payload.notebookName}</span></h6>
                        <div className="button-group">
                            {/*<button className={`btn btn-light ${this.state.dataView === 'FILE' ? 'active' : ''}`} data-tooltip data-tooltip-text="JSON View" data-tooltip-place="bottom" onClick={() => this.setState({dataView: 'FILE'})}>
                                <i className="far fa-file-alt"></i>
                            </button>
                            <button className={`btn btn-light ${this.state.dataView === 'TREE' ? 'active' : ''}`} data-tooltip data-tooltip-text="Tree View" data-tooltip-place="bottom" onClick={() => this.setState({dataView: 'TREE'})}>
                                <i className="far fa-folder-tree"></i>
                            </button>*/}
                            <button className="btn btn-light" data-tooltip data-tooltip-text="Clear" data-tooltip-place="bottom" onClick={this.clearSniffingModal}>
                                <i className="far fa-broom"></i>
                            </button>
                            <button type="button" className="btn btn-light" data-tooltip data-tooltip-text="Close" data-tooltip-place="bottom" onClick={this.closeSniffingModal}>
                                <i className="far fa-times"></i>
                            </button>
                        </div>
                    </div>
                    
                    <div className="debug-body">
                        {this.state.responseReceived && this.state.responseReceived.length > 0  ?
                            <ul className="list-style-none overflow-y-auto h-100">
                                {this.state.responseReceived.map((msg,index) =>
                                    <li className="text-content f-12 mb-2" key={index}>{msg}</li>
                                )}
                            </ul>
                            :
                            this.state.isclearClicked ?
                                <div className="inner-message-wrapper h-100">
                                    <div className="inner-message-content">
                                        <i className="fad fa-file-exclamation"></i>
                                        <h6>There is no data to display.</h6>
                                    </div>
                                </div>
                            :
                            !this.state.payload.codeBlocks.find(codeBlock => codeBlock.status === "start") ?
                            <div className="inner-loader-wrapper h-100">
                                <div className="inner-loader-content">
                                    <h6>Please Run Any Code on CodeBlock</h6>
                                    <p className="text-gray">No RPC Generated Yet.</p>
                                </div>
                            </div>        
                            :
                            <div className="inner-loader-wrapper h-100">
                                <div className="inner-loader-content">
                                    <h6>Debugging...</h6>
                                    <img src="https://content.iot83.com/m83/misc/uploading.gif" />
                                    <p className="text-gray">Please be patient. This may take few seconds.</p>
                                </div>
                            </div>
                        }
                    </div>
                </div>
                }
                {/* end debug window */}
                
                {/* etl graph modal */}
                {this.state.showInfoModal &&
                    <div className="modal d-block animated slideInDown" role="dialog">
                        <div className="modal-dialog modal-xl modal-dialog-centered">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h6 className="modal-title">Stats for - <span> { this.state.payload.notebookName }</span>
                                        <button type="button" className="close" data-dismiss="modal" data-tooltip data-tooltip-text="Close" data-tooltip-place="bottom" onClick={() => this.setState({showInfoModal: false})}><i className="far fa-times"></i></button>
                                    </h6>
                                    <div className="form-group mb-0 mr-r-45">
                                        <select disabled={ this.state.isfetchingStatsData } className="form-control" value={ codeBlocks[currentMonitoringCodeBlockIndex].monitoringDuration } onChange={({target: {value}}) => {
                                            let payload = cloneDeep(this.state.payload);
                                            payload.codeBlocks.find(({ codeBlockId }) => codeBlockId === this.state.currentCodeBlockId).monitoringDuration = value;
                                            this.setState({
                                                payload,
                                                isfetchingStatsData: true,
                                            }, () => this.props.monitorNotebook({
                                                    "notebookId": this.state.payload.notebookId,
                                                    "codeBlockId": this.state.currentCodeBlockId,
                                                    "duration": this.state.payload.codeBlocks[currentMonitoringCodeBlockIndex].monitoringDuration
                                                }))
                                            }}
                                        >
                                            <option value={5}>Last 5 mins</option>
                                            <option value={15}>Last 15 mins</option>
                                            <option value={30}>Last 30 mins</option>
                                            <option value={60}>Last 1 hour</option>
                                            <option value={180}>Last 3 hours</option>
                                        </select>
                                    </div>
                                    <ul className="modal-header-list list-style-none">
                                        <li className="btn btn-light" onClick={() => {
                                            this.setState({
                                                isfetchingStatsData: true
                                            }, () => this.props.monitorNotebook({
                                                    "notebookId": this.state.payload.notebookId,
                                                    "codeBlockId": this.state.currentCodeBlockId,
                                                    "duration": this.state.payload.codeBlocks[currentMonitoringCodeBlockIndex].monitoringDuration
                                                }))
                                            }} data-tooltip data-tooltip-text="Refresh" data-tooltip-place="bottom">
                                        <i className="far fa-sync-alt"></i>
                                        </li>
                                    </ul>
                                </div>
                                <div className="modal-body p-0">
                                    <ul className="list-style-none d-flex stats-chart-list">
                                        <li className="flex-50">
                                            <div className="card">
                                                <div className="card-header">
                                                    <h6>Input Rows/sec</h6>
                                                </div>
                                                <div className="card-body">
                                                    {this.state.isfetchingStatsData ? 
                                                        <div className="card-loader">
                                                                <i className="fad fa-sync-alt fa-spin"></i>
                                                        </div> :
                                                        codeBlocks[currentMonitoringCodeBlockIndex].monitoringData && codeBlocks[currentMonitoringCodeBlockIndex].monitoringData.length ?
                                                        <div className="card-chart-box">
                                                            <StatsChart timeZone={this.props.timeZone} name="Input Rows/sec" divId="inputRowsPerSecond" data={codeBlocks[currentMonitoringCodeBlockIndex].monitoringData} />
                                                        </div> :
                                                        <div className="card-message">
                                                            <p>There is no data to display !</p>
                                                        </div>
                                                    }
                                                </div>
                                            </div>
                                        </li>
                                        <li className="flex-50">
                                            <div className="card">
                                                <div className="card-header">
                                                    <h6>Processed Rows/sec</h6>
                                                </div>
                                                <div className="card-body">
                                                    {this.state.isfetchingStatsData ? 
                                                        <div className="card-loader">
                                                            <i className="fad fa-sync-alt fa-spin"></i>
                                                        </div> :
                                                        codeBlocks[currentMonitoringCodeBlockIndex].monitoringData && codeBlocks[currentMonitoringCodeBlockIndex].monitoringData.length ?
                                                        <div className="card-chart-box">
                                                            <StatsChart timeZone={this.props.timeZone} name="Processed Rows/sec" divId="processedRowsPerSecond" data={codeBlocks[currentMonitoringCodeBlockIndex].monitoringData} />
                                                        </div> :
                                                        <div className="card-message">
                                                            <p>There is no data to display !</p>
                                                        </div>
                                                    }
                                                </div>
                                            </div>
                                        </li>
                                        <li className="flex-50">
                                            <div className="card">
                                                <div className="card-header">
                                                    <h6>No. of Input Rows</h6>
                                                </div>
                                                <div className="card-body">
                                                    {this.state.isfetchingStatsData ? 
                                                        <div className="card-loader">
                                                            <i className="fad fa-sync-alt fa-spin"></i>
                                                        </div> :
                                                        codeBlocks[currentMonitoringCodeBlockIndex].monitoringData && codeBlocks[currentMonitoringCodeBlockIndex].monitoringData.length ?
                                                        <div className="card-chart-box">
                                                            <StatsChart timeZone={this.props.timeZone} name="No. of Input Rows" divId="numInputRows" data={codeBlocks[currentMonitoringCodeBlockIndex].monitoringData} />
                                                        </div> :
                                                        <div className="card-message">
                                                            <p>There is no data to display !</p>
                                                        </div>
                                                    }
                                                </div>
                                            </div>
                                        </li>
                                        <li className="flex-50">
                                            <div className="card">
                                                <div className="card-header">
                                                    <h6>Trigger Execution (in ms)</h6>
                                                </div>
                                                <div className="card-body">
                                                    {this.state.isfetchingStatsData ? 
                                                        <div className="card-loader">
                                                            <i className="fad fa-sync-alt fa-spin"></i>
                                                        </div> :
                                                        codeBlocks[currentMonitoringCodeBlockIndex].monitoringData && codeBlocks[currentMonitoringCodeBlockIndex].monitoringData.length ?
                                                        <div className="card-chart-box">
                                                            <StatsChart timeZone={this.props.timeZone} name="Trigger Execution (in ms)" divId="triggerExecution" data={codeBlocks[currentMonitoringCodeBlockIndex].monitoringData} />
                                                        </div> :
                                                        <div className="card-message">
                                                            <p>There is no data to display !</p>
                                                        </div>
                                                    }
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    }
                    {/* end etl graph modal */}
                {this.state.isOpen &&
                    <NotificationModal
                        type={this.state.modalType}
                        message2={this.state.message2}
                        onCloseHandler={this.onCloseHandler}
                    />
                }

            </React.Fragment>
        );
    }
}

AddorEditNoteBook.propTypes = {
    dispatch: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
    //addoreditnotebook: makeSelectAddorEditNoteBook(),
    createNoteBookSuccess: SELECTORS.createNoteBookSuccess(),
    createNoteBookFailure: SELECTORS.createNoteBookFailure(),
    updateNoteBookSuccess: SELECTORS.updateNoteBookSuccess(),
    updateNoteBookFailure: SELECTORS.updateNoteBookFailure(),
    debugNoteBookSuccess:  SELECTORS.debugNoteBookSuccess(),
    debugNoteBookFailure:  SELECTORS.debugNoteBookFailure(),
    stopNoteBookCodeBlockSuccess:  SELECTORS.stopNoteBookCodeBlockSuccess(),
    stopNoteBookCodeBlockFailure:  SELECTORS.stopNoteBookCodeBlockFailure(),
    getNoteBookByIdSuccess:  SELECTORS.getNoteBookByIdSuccess(),
    getNoteBookByIdFailure:  SELECTORS.getNoteBookByIdFailure(),
    restartNoteBookCodeBlockSuccess: SELECTORS.restartNoteBookCodeBlockSuccess(),
    restartNoteBookCodeBlockFailure: SELECTORS.restartNoteBookCodeBlockFailure(),
    monitorNotebookSuccess: SELECTORS.monitorNotebookSuccess(),
    monitorNotebookFailure: SELECTORS.monitorNotebookFailure(),

});

function mapDispatchToProps(dispatch) {
    return {
        dispatch,
        createNoteBook: (payload) => dispatch(ACTIONS.createNoteBook(payload)),
        updateNoteBook: (payload) => dispatch(ACTIONS.updateNoteBook(payload)),
        debugNoteBook : (payload) => dispatch(ACTIONS.debugNoteBook(payload)),
        stopNoteBookCodeBlock: (payload) => dispatch(ACTIONS.stopNoteBookCodeBlock(payload)),
        getNoteBookById: (payload) => dispatch(ACTIONS.getNoteBookById(payload)),
        restartNoteBookCodeBlock: (payload) => dispatch(ACTIONS.restartNoteBookCodeBlock(payload)),
        monitorNotebook: (payload) => dispatch(ACTIONS.monitorNotebook(payload)),
    };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({key: "addorEditNoteBook", reducer});
const withSaga = injectSaga({key: "addorEditNoteBook", saga});

export default compose(
    withReducer,
    withSaga,
    withConnect
)(AddorEditNoteBook);
