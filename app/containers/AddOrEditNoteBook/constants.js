/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/*
 *
 * AddorEditNoteBook constants
 *
 */

export const DEFAULT_ACTION = "app/AddorEditNoteBook/DEFAULT_ACTION";

export const CREATE_NOTEBOOK = "app/AddorEditNoteBook/CREATE_NOTEBOOK";
export const CREATE_NOTEBOOK_SUCCESS = "app/AddorEditNoteBook/CREATE_NOTEBOOK_SUCCESS";
export const CREATE_NOTEBOOK_FAILURE = "app/AddorEditNoteBook/CREATE_NOTEBOOK_FAILURE";

export const UPDATE_NOTEBOOK = "app/AddorEditNoteBook/UPDATE_NOTEBOOK";
export const UPDATE_NOTEBOOK_SUCCESS = "app/AddorEditNoteBook/UPDATE_NOTEBOOK_SUCCESS";
export const UPDATE_NOTEBOOK_FAILURE = "app/AddorEditNoteBook/UPDATE_NOTEBOOK_FAILURE";

export const DEBUG_NOTEBOOK = "app/AddorEditNoteBook/DEBUG_NOTEBOOK";
export const DEBUG_NOTEBOOK_SUCCESS = "app/AddorEditNoteBook/DEBUG_NOTEBOOK_SUCCESS";
export const DEBUG_NOTEBOOK_FAILURE = "app/AddorEditNoteBook/DEBUG_NOTEBOOK_FAILURE"; 

export const STOP_NOTEBOOK_CODEBLOCK = "app/AddorEditNoteBook/STOP_NOTEBOOK_CODEBLOCK";
export const STOP_NOTEBOOK_CODEBLOCK_SUCCESS = "app/AddorEditNoteBook/STOP_NOTEBOOK_CODEBLOCK_SUCCESS";
export const STOP_NOTEBOOK_CODEBLOCK_FAILURE = "app/AddorEditNoteBook/STOP_NOTEBOOK_CODEBLOCK_FAILURE"; 

export const GET_NOTEBOOK_BY_ID = "app/AddorEditNoteBook/GET_NOTEBOOK_BY_ID";
export const GET_NOTEBOOK_BY_ID_SUCCESS = "app/AddorEditNoteBook/GET_NOTEBOOK_BY_ID_SUCCESS";
export const GET_NOTEBOOK_BY_ID_FAILURE = "app/AddorEditNoteBook/GET_NOTEBOOK_BY_ID_FAILURE";

export const RESTART_NOTEBOOK_CODEBLOCK = "app/AddorEditNoteBook/RESTART_NOTEBOOK_CODEBLOCK";
export const RESTART_NOTEBOOK_CODEBLOCK_SUCCESS = "app/AddorEditNoteBook/RESTART_NOTEBOOK_CODEBLOCK_SUCCESS";
export const RESTART_NOTEBOOK_CODEBLOCK_FAILURE = "app/AddorEditNoteBook/RESTART_NOTEBOOK_CODEBLOCK_FAILURE";

export const MONITOR_NOTEBOOK = 'app/AddorEditNoteBook/MONITOR_NOTEBOOK';
export const MONITOR_NOTEBOOK_SUCCESS = 'app/AddorEditNoteBook/MONITOR_NOTEBOOK_SUCCESS';
export const MONITOR_NOTEBOOK_FAILURE = 'app/AddorEditNoteBook/MONITOR_NOTEBOOK_FAILURE';
