/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/*
 *
 * AddorEditNoteBook reducer
 *
 */

import { fromJS } from "immutable";
import * as CONSTANTS from './constants';

export const initialState = fromJS({});

function addorEditNoteBookReducer(state = initialState, action) {
  switch (action.type) {
    case CONSTANTS.DEFAULT_ACTION:
      return state;
      
    case CONSTANTS.CREATE_NOTEBOOK_SUCCESS:
      return Object.assign({}, state, {
        createNoteBookSuccess: action.response
      });

    case CONSTANTS.CREATE_NOTEBOOK_FAILURE:
      return Object.assign({}, state, {
        createNoteBookFailure: { message: action.error, timestamp: Date.now() }
      });

    case CONSTANTS.UPDATE_NOTEBOOK_SUCCESS:
      return Object.assign({},state,{
        updateNoteBookSuccess: action.response
      }) 

    case CONSTANTS.UPDATE_NOTEBOOK_FAILURE:
      return Object.assign({}, state, {
        updateNoteBookFailure: { message: action.error, timestamp: Date.now() }
      });
      
    case CONSTANTS.DEBUG_NOTEBOOK_SUCCESS:
      return Object.assign({},state,{
        debugNoteBookSuccess: action.response
      });

    case CONSTANTS.DEBUG_NOTEBOOK_FAILURE:
      return Object.assign({}, state, {
        debugNoteBookFailure: { message: action.error, timestamp: Date.now() }
      });

    case CONSTANTS.STOP_NOTEBOOK_CODEBLOCK_SUCCESS:
      return Object.assign({},state,{
        stopNoteBookCodeBlockSuccess: action.response
      });

    case CONSTANTS.STOP_NOTEBOOK_CODEBLOCK_FAILURE:
      return Object.assign({}, state, {
        stopNoteBookCodeBlockFailure: { message: action.error, timestamp: Date.now() }
      });  
    
    case CONSTANTS.GET_NOTEBOOK_BY_ID_SUCCESS:
      return Object.assign({},state,{
        getNoteBookByIdSuccess: action.response
      });

    case CONSTANTS.GET_NOTEBOOK_BY_ID_FAILURE:
      return Object.assign({}, state, {
        getNoteBookByIdFailure: { message: action.error, timestamp: Date.now() }
      });  
    
    case CONSTANTS.RESTART_NOTEBOOK_CODEBLOCK_SUCCESS:
      return Object.assign({},state,{
        restartNoteBookCodeBlockSuccess: action.response
      });
    
    case CONSTANTS.RESTART_NOTEBOOK_CODEBLOCK_FAILURE:
      return Object.assign({},state,{
        restartNoteBookCodeBlockFailure: action
      }); 
      
    case CONSTANTS.MONITOR_NOTEBOOK_SUCCESS:
      return Object.assign({}, state, {
        monitorNotebookSuccess: action.response
      });

    case CONSTANTS.MONITOR_NOTEBOOK_FAILURE:
      return Object.assign({}, state, {
        monitorNotebookFailure: action.error
      });
    
    
    default:
      return state;
  }
}

export default addorEditNoteBookReducer;
