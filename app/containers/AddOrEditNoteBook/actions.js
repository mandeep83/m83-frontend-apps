/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/*
 *
 * AddorEditNoteBook actions
 *
 */

import * as CONSTANTS from './constants';

export function defaultAction() {
  return {
    type: CONSTANTS.DEFAULT_ACTION
  };
}

export function createNoteBook(payload) {
  return {
    type: CONSTANTS.CREATE_NOTEBOOK,
    payload
  };
}

export function updateNoteBook(payload) {
  return{
    type: CONSTANTS.UPDATE_NOTEBOOK,
    payload
  };
} 

export function debugNoteBook(payload) {
  return{
    type: CONSTANTS.DEBUG_NOTEBOOK,
    payload
  };
} 

export function getNoteBookById(notebookId) {
  return{
    type: CONSTANTS.GET_NOTEBOOK_BY_ID,
    notebookId
  }
}
export function stopNoteBookCodeBlock(payload) {
  return{
    type: CONSTANTS.STOP_NOTEBOOK_CODEBLOCK,
    payload
  };
}

export function restartNoteBookCodeBlock(payload) {
  return{
    type: CONSTANTS.RESTART_NOTEBOOK_CODEBLOCK,
    payload
  }
}

export function monitorNotebook(payload) {
  return {
    type: CONSTANTS.MONITOR_NOTEBOOK,
    payload
  }
}