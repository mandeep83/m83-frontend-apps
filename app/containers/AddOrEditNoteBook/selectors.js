/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
 * Direct selector to the addorEditNoteBook state domain
 */

export const selectAddorEditNoteBookDomain = state =>
  state.get("addorEditNoteBook", initialState);

/**
 * Other specific selectors
 */
export const createNoteBookSuccess = () => createSelector(selectAddorEditNoteBookDomain, substate => substate.createNoteBookSuccess);
export const createNoteBookFailure = () => createSelector(selectAddorEditNoteBookDomain, substate => substate.createNoteBookFailure);

export const updateNoteBookSuccess = () => createSelector(selectAddorEditNoteBookDomain, substate => substate.updateNoteBookSuccess);
export const updateNoteBookFailure = () => createSelector(selectAddorEditNoteBookDomain, substate => substate.updateNoteBookFailure);

export const debugNoteBookSuccess = () => createSelector(selectAddorEditNoteBookDomain, substate => substate.debugNoteBookSuccess);
export const debugNoteBookFailure = () => createSelector(selectAddorEditNoteBookDomain, substate => substate.debugNoteBookFailure);

export const getNoteBookByIdSuccess = () => createSelector(selectAddorEditNoteBookDomain, substate => substate.getNoteBookByIdSuccess);
export const getNoteBookByIdFailure = () => createSelector(selectAddorEditNoteBookDomain, substate => substate.getNoteBookByIdFailure);

export const stopNoteBookCodeBlockSuccess = () => createSelector(selectAddorEditNoteBookDomain, substate => substate.stopNoteBookCodeBlockSuccess);
export const stopNoteBookCodeBlockFailure = () => createSelector(selectAddorEditNoteBookDomain, substate => substate.stopNoteBookCodeBlockFailure);

export const restartNoteBookCodeBlockSuccess = () => createSelector(selectAddorEditNoteBookDomain, substate => substate.restartNoteBookCodeBlockSuccess);
export const restartNoteBookCodeBlockFailure = () => createSelector(selectAddorEditNoteBookDomain, substate => substate.restartNoteBookCodeBlockFailure);

export const monitorNotebookSuccess = () => createSelector(selectAddorEditNoteBookDomain, substate => substate.monitorNotebookSuccess);
export const monitorNotebookFailure = () => createSelector(selectAddorEditNoteBookDomain, substate => substate.monitorNotebookFailure);

