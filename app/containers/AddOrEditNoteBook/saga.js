/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

// import { take, call, put, select } from 'redux-saga/effects';
import { takeEvery } from 'redux-saga';
import * as CONSTANTS from './constants';
import { apiCallHandler } from '../../api';
// Individual exports for testing

export function* createNoteBookHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.CREATE_NOTEBOOK_SUCCESS, CONSTANTS.CREATE_NOTEBOOK_FAILURE, 'createNoteBook')];
}

export function* updateNoteBookHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.UPDATE_NOTEBOOK_SUCCESS, CONSTANTS.UPDATE_NOTEBOOK_FAILURE, 'updateNoteBook')];
}

export function* debugNoteBookHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.DEBUG_NOTEBOOK_SUCCESS, CONSTANTS.DEBUG_NOTEBOOK_FAILURE, 'debugNoteBook')];
}

export function* stopNoteBookCodeBlockHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.STOP_NOTEBOOK_CODEBLOCK_SUCCESS, CONSTANTS.STOP_NOTEBOOK_CODEBLOCK_FAILURE, 'stopNoteBookCodeBlock')];
}

export function* getNoteBookByIdHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_NOTEBOOK_BY_ID_SUCCESS, CONSTANTS.GET_NOTEBOOK_BY_ID_FAILURE, 'getNoteBookById')];
}

export function* restartNoteBookHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.RESTART_NOTEBOOK_CODEBLOCK_SUCCESS, CONSTANTS.RESTART_NOTEBOOK_CODEBLOCK_FAILURE, 'restartNoteBookCodeBlock')];
}

export function* watcherNoteBookHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.MONITOR_NOTEBOOK_SUCCESS, CONSTANTS.MONITOR_NOTEBOOK_FAILURE, 'monitorNotebook')];
}

export function* watcherCreateNoteBook() {
  yield takeEvery(CONSTANTS.CREATE_NOTEBOOK,createNoteBookHandlerAsync)
}

export function* watcherUpdateNoteBook() {
  yield takeEvery(CONSTANTS.UPDATE_NOTEBOOK,updateNoteBookHandlerAsync)
}

export function* watcherDebugNoteBook() {
  yield takeEvery(CONSTANTS.DEBUG_NOTEBOOK,debugNoteBookHandlerAsync)
}

export function* watcherStopNoteBookCodeBlock() {
  yield takeEvery(CONSTANTS.STOP_NOTEBOOK_CODEBLOCK,stopNoteBookCodeBlockHandlerAsync)
}

export function* watcherGetNoteBookById() {
  yield takeEvery(CONSTANTS.GET_NOTEBOOK_BY_ID,getNoteBookByIdHandlerAsync)
}

export function* watcherRestartNoteBook() {
  yield takeEvery(CONSTANTS.RESTART_NOTEBOOK_CODEBLOCK,restartNoteBookHandlerAsync)
}

export function* watcherMonitorNoteBook() {
  yield takeEvery(CONSTANTS.MONITOR_NOTEBOOK,watcherNoteBookHandlerAsync)
}
export default function* rootSaga() {
  // See example in containers/HomePage/saga.js
  yield[
    watcherCreateNoteBook(),
    watcherUpdateNoteBook(),
    watcherDebugNoteBook(),
    watcherStopNoteBookCodeBlock(),
    watcherGetNoteBookById(),
    watcherRestartNoteBook(),
    watcherMonitorNoteBook(),
  ]
}
