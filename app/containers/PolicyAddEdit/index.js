/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import Select from 'react-select';
import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import * as SELECTORS from './selectors';
import reducer from './reducer';
import saga from './saga';
import * as ACTIONS from './actions';
import Loader from '../../components/Loader/Loadable';
import NotificationModal from '../../components/NotificationModal/Loadable'
import ReactSelect from "react-select";
import { Helmet } from "react-helmet";
import cloneDeep  from 'lodash/cloneDeep';

let eventObject =  {
    "action": {
        alarm : {
            "templateId": ""
        }
    },
    "attribute": "",
    "duration": 0,
    "occurrences": 0,
    "operator": "", 
    "triggerType": "", 
    "value": 0
}
/* eslint-disable react/prefer-stateless-function */
export class PolicyAddEdit extends React.Component {
    state = {
        isOpen: false,
        etlList: [],
        policyData: [],
        policyDetails: {
            "etlId": "",
            "active": false,
            "groupBy": "",
            // "status": "STOP",
            // "notifications": false,
            "event": [ cloneDeep(eventObject) ],
            "name": "",
            "description": "",
            "scheduleId": "",
        },
        isFetching: 5, //value is equal to the number of api calls in didMount method
        activeTab: "basicInfo",
        allGroups: [],
        allTemplates: [],
        allSchedulers : [],
        actionTypes : ["email", "notification", "message", "ticket"]
    }

    componentDidMount() {
        this.props.getAllPolicyData();
        this.props.getEtlList("policy");
        this.props.getAllSchedulers();
        this.props.getAllTemplates();
        this.props.getAllGroups();
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.policySaveSuccess && nextProps.policySaveSuccess != this.props.policySaveSuccess) {
            this.setState({
                isOpen: true,
                modalType: "success",
                message2: nextProps.policySaveSuccess.message,
            })
        }

        if (nextProps.policySaveFailure && nextProps.policySaveFailure != this.props.policySaveFailure) {
            this.setState(prevState =>({
                isOpen: true,
                isFetching: prevState.isFetching - 1,
                message2: nextProps.policySaveFailure,
                modalType: "error"
            }),()=> this.props.resetToInitialState())
        }

        if (nextProps.etlListSuccess && nextProps.etlListSuccess !== this.props.etlListSuccess) {
            this.setState(prevState =>({
                etlList: nextProps.etlListSuccess,
                isFetching : prevState.isFetching - 1,
            }))
        }

        if (nextProps.etlListFailure && nextProps.etlListFailure !== this.props.etlListFailure) {
            this.setState(prevState =>({
                isOpen: true,
                message2: nextProps.etlListFailure,
                modalType: 'error',
                isFetching : prevState.isFetching - 1,
            }),()=> this.props.resetToInitialState())
        }

        if (nextProps.etlAttributes && nextProps.etlAttributes !== this.props.etlAttributes) {
            this.setState({
                isFetchingAttributes : false,
                etlAttributes: nextProps.etlAttributes,
            })
        }

        if (nextProps.etlAttributesError && nextProps.etlAttributesError !== this.props.etlAttributesError) {
            this.setState({
                isFetchingAttributes : false,
                message2: nextProps.etlAttributesError,
                isOpen: true,
                modalType: 'error'
            },()=> this.props.resetToInitialState())
        }

        if (nextProps.policyDataSuccess && nextProps.policyDataSuccess !== this.props.policyDataSuccess) {
            let policyDetails = cloneDeep(nextProps.policyDataSuccess);
            this.props.fetchEtlAttributes(nextProps.policyDataSuccess.etlId)
            this.setState(prevState => ({
                policyDetails,
                isFetching: prevState.isFetching -1,
            }))
        }

        if (nextProps.policyDataFailure && nextProps.policyDataFailure !== this.props.policyDataFailure) {
            this.setState(prevState => ({
                isOpen: true,
                message2: nextProps.policyDataFailure,
                modalType: 'error',
                isFetching: prevState.isFetching -1
            }),()=> this.props.resetToInitialState())
        }

        if (nextProps.policyData && nextProps.policyData !== this.props.policyData) {
            this.setState(prevState =>({
                policyData: nextProps.policyData,
                isFetching: prevState.isFetching - 1,
            }))
        }

        if (nextProps.policyDataError && nextProps.policyDataError !== this.props.policyDataError) {
            this.setState(prevState =>({
                isOpen: true,
                isFetching: prevState.isFetching - 1,
                message2: nextProps.policyDataError,
                modalType: "error"
            }),()=> this.props.resetToInitialState())
        }

        if (nextProps.getAllSchedulersSuccess && nextProps.getAllSchedulersSuccess !== this.props.getAllSchedulersSuccess) {
            let allSchedulers = nextProps.getAllSchedulersSuccess.filter(scheduler => scheduler.schedulerType === "policy").map(scheduler => ({value : scheduler.id, label : scheduler.name}))
            this.setState(prevState => ({
                allSchedulers,
                isFetching : prevState.isFetching -1,
            }))
        }

        if (nextProps.getAllSchedulersFailure && nextProps.getAllSchedulersFailure !== this.props.getAllSchedulersFailure) {
            this.setState(prevState => ({
                isOpen: true,
                isFetching: prevState.isFetching -1,
                message2: nextProps.getAllSchedulersFailure,
                modalType: "error",
            }),()=> this.props.resetToInitialState())
        }

        if (nextProps.getAllTemplatesSuccess && nextProps.getAllTemplatesSuccess !== this.props.getAllTemplatesSuccess) {
            let allTemplates = nextProps.getAllTemplatesSuccess.map(template=> ({label : template.name, value: template.id, type: template.type}))
            this.setState(prevState => ({
                allTemplates,
                isFetching : prevState.isFetching -1,
            }),()=> this.getPolicyById())
        }
        
        if (nextProps.getAllTemplatesFailure && nextProps.getAllTemplatesFailure !== this.props.getAllTemplatesFailure) {
            this.setState(prevState => ({
                isOpen: true,
                isFetching: prevState.isFetching -1,
                message2: nextProps.getAllTemplatesFailure,
                modalType: "error",
                errorCode : this.props.match.params.id ? 2 : prevState.errorCode,
            }),()=> this.props.resetToInitialState())
        }

        if (nextProps.getAllGroupsSuccess && nextProps.getAllGroupsSuccess !== this.props.getAllGroupsSuccess) {
            let allGroups = nextProps.getAllGroupsSuccess.map(group => ({
                                label: group.name,
                                value: group.id
                            }))
            this.setState(prevState => ({
                allGroups,
                isFetching : prevState.isFetching -1,
            }),()=> this.getPolicyById())
        }
        
        if (nextProps.getAllGroupsFailure && nextProps.getAllGroupsFailure !== this.props.getAllGroupsFailure) {
            this.setState(prevState => ({
                isOpen: true,
                isFetching: prevState.isFetching -1,
                message2: nextProps.getAllGroupsFailure,
                modalType: "error",
                errorCode : this.props.match.params.id ? 2 : prevState.errorCode,
            }),()=> this.props.resetToInitialState())
        }
    }

    getPolicyById = ()=>{
        if(this.props.match.params.id){
            if(this.state.allGroups.length && this.state.allTemplates.length){
                    this.setState(prevState => ({
                        isFetching : prevState.isFetching + 1
                    }),() => this.props.loadPolicyData(this.props.match.params.id))
            }
        }
    }

    onCloseHandler = () => {
        this.setState({
            isOpen: false,
            message2: "",
        },()=> this.state.modalType === "success" && this.props.history.push(`/policyManager`))
    }

    inputChangeHandler = (event, isForScheduler) => {
        let policyDetails = JSON.parse(JSON.stringify(this.state.policyDetails)),
        isFetchingAttributes = this.state.isFetchingAttributes,
        fetchEtlAttributes;
        if(isForScheduler){
            policyDetails.scheduleId = event.value; 
        }
        else{
            if (event.target.id === "etlId") {
                fetchEtlAttributes = event.target.value;
                isFetchingAttributes = fetchEtlAttributes;
                policyDetails.etlId = event.target.value
                // policyDetails.collectionName = this.state.etlList.find(val => val.etlId === event.target.value).collectionName
            }
            else {
            policyDetails[event.target.id] = event.target.value
            }
        }
        this.setState({ policyDetails, isFetchingAttributes },()=> fetchEtlAttributes && this.props.fetchEtlAttributes(policyDetails.etlId))
    }

    eventChangeHandler = (e, index) => {
        let policyDetails = JSON.parse(JSON.stringify(this.state.policyDetails));
        if(e.target.id === "alarmTemplate"){
            policyDetails.event[index].action.alarm.templateId = e.target.value
        }
        else if (e.target.id === "occurrences" || e.target.id === "value") {
            policyDetails.event[index][e.target.id] = parseInt(e.target.value) || ""
        } else {
            policyDetails.event[index][e.target.id] = e.target.value
        }
        this.setState({ policyDetails })

    }

    handleConditionGroup = (index) => {
        let policyDetails = JSON.parse(JSON.stringify(this.state.policyDetails));
        if (index != undefined) {
            policyDetails.event.splice(index, 1);
        }
        else {
            policyDetails.event.push(cloneDeep(eventObject))
        }
        this.setState({ policyDetails })
    }

    actionTypeChangeHandler = (event, index, actionType) => {
        let policyDetails = JSON.parse(JSON.stringify(this.state.policyDetails)),
        activeActionIndex = this.state.activeActionIndex;
        if(event.target.checked){
            activeActionIndex = index;
            let actionObj = actionType === "ticket" ? {
                                "description": "string",
                                "groupIds": [
                                "string"
                                ],
                                "priority": "HIGH",
                                "type": "SETUP"
                            }: {
                                "groupIds": [],
                                "templateId": ""
                                }
            policyDetails.event[index].action = {[actionType] : actionObj, ...policyDetails.event[index].action}
        }
        else
            delete policyDetails.event[index].action[actionType]
        policyDetails.event[index].errorCode = 0
        this.setState({ policyDetails, activeActionIndex })
    }

    policyFormSubmit = (event) => {
        event.preventDefault();
        let policyDetails = JSON.parse(JSON.stringify(this.state.policyDetails));
        let isReadyForSubmit = true;

        policyDetails.event.map(val => {
            val.errorCode = 0;
            Object.entries(val.action).map(([key, value]) => {
                if(key !== "alarm" && (key === "ticket" ? !value.groupIds.length : !(value.groupIds.length && value.templateId))){
                    val.errorCode = 2;
                    isReadyForSubmit = false;
                }
            })
            return val;
        })
        if (isReadyForSubmit) {
            policyDetails.event.map(val => {
                delete val.errorCode
            
            })
            this.setState(prevState =>({
                isFetching: prevState.isFetching + 1,
            }), () => {
                this.props.savePolicy(policyDetails, this.props.match.params.id)
            })
        } else {
            this.setState({
                policyDetails
            })
        }
    }

    stepOneSubmitHandler = (event) => {
        event.preventDefault();
        let activeTab = this.state.activeTab;
            activeTab = "definition";
        this.setState({
            activeTab,
        })
    }

    tabChange = () => {
        this.setState({ activeTab: 'basicInfo' })
    }

    getKeyValue = (key, isFor)=>{
        switch(key){
            case "email" :
                return isFor === "heading" ? "Email" : isFor === "className" ? "fas fa-envelope mr-r-7" : "EMAIL"
            case "notification" : 
                return isFor === "heading" ? "Notification" : isFor === "className" ? "fas fa-bell mr-r-7" : "NOTIFICATION"
            case "message" :      
                return isFor === "heading" ? "SMS" : isFor === "className" ? "fas fa-sms mr-r-7" : "MESSAGE"
            default : 
                return isFor === "heading" ? "Ticket" : "fas fa-ticket mr-r-7"
    }}

    reactSelectChangeHandler = (val, index, key, isForTemplate)=> {
        let policyDetails = cloneDeep(this.state.policyDetails);
        if(isForTemplate){
            policyDetails.event[index].action[key].templateId = val.value
        }
        else{
            let groupIds = val.map(group => group.value);
            policyDetails.event[index].action[key].groupIds = groupIds;
        }
        policyDetails.event[index].errorCode = policyDetails.event[index].errorCode === 2 ? 0 : policyDetails.event[index].errorCode
        this.setState({policyDetails}); 
    }

    getValueForReactSelect = (payloadValue, isFor)=>{
        if(isFor === "scheduleId")
            return this.state.allSchedulers ? this.state.allSchedulers.find(element => element.value === payloadValue) : null;
        if(isFor === "template")
            return this.state.allTemplates ? this.state.allTemplates.find(element => element.value === payloadValue) : null;
        else
            return this.state.allGroups.filter(element => payloadValue && payloadValue.find(el => el === element.value));
    }

    handleActiveActionIndex = (index)=> {
        let isOpen = this.state.activeActionIndex === index,
        activeActionIndex = isOpen ? -1 : index;
        this.setState({ activeActionIndex })
    }

    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>Add/Edit Policy</title>
                    <meta name="description" content="M83-PolicyAddOrEdit" />
                </Helmet>

                {this.state.isFetching ? <Loader /> :
                    <React.Fragment>
                        <div className="pageBreadcrumb">
                            <p>
                                <span className="previousPage" onClick={() => { this.props.history.push('/policyManager') }}>Policies {this.state.policyData.length > 0 && '(' + this.state.policyData.length + ')'}</span>
                                <span className="active">{this.props.match.params.id ? "Edit" : "Create"} Policy</span>
                            </p>
                        </div>

                        <div className="outerBox pb-0 pd-t-90">
                            <ul className="nav nav-tabs stepWizardTop">
                                <li onClick={this.tabChange} className={this.state.activeTab === "basicInfo" ? "nav-item active" : "nav-item"}>
                                    <a className="nav-link" >
                                        <span className="stepWizardNumber">1</span>
                                        Configuration
                                    </a>
                                </li>
                                <li className={this.state.activeTab === "definition" ? "nav-item active" : "nav-item"}>
                                    <a className="nav-link" >
                                        <span className="stepWizardNumber">2</span>
                                        Condition & Actions
                                    </a>
                                </li>
                            </ul>

                            <div className="tab-content">
                                {this.state.activeTab === "basicInfo" ?
                                    <div className="tab-pane active fade show">
                                        <form onSubmit={this.stepOneSubmitHandler}>
                                            {this.state.errorCode === 2 && <p className="text-danger"> Could not fetch groups/templates for policy.</p>}
                                            <div className="stepWizardForm">
                                                <div className="flex">
                                                    <div className="flex-item fx-b50 pd-r-7">
                                                        <div className="form-group">
                                                            <input type="text" id="name" className="form-control" placeholder="PolicyName" value={this.state.policyDetails.name} onChange={this.inputChangeHandler} required />
                                                            <label className="form-group-label">Policy name :</label>
                                                        </div>
                                                        <div className="form-group">
                                                            <select className="form-control" id="etlId" value={this.state.policyDetails.etlId} onChange={this.inputChangeHandler} disabled={this.props.match.params.id} required>
                                                                <option value="">Select</option>
                                                                {this.state.etlList && this.state.etlList.map(etl => {
                                                                    return <option value={etl.etlId} key={etl.etlId}>{etl.name}</option>
                                                                })}
                                                            </select>
                                                            <label className="form-group-label">ETL :<span className="requiredMark"><i className="fa fa-asterisk" /></span></label>
                                                        </div>
                                                        {/*<div className="form-group">
                                                            <input className="form-control" value={this.state.policyDetails.collectionName} readOnly />
                                                            <label className="form-group-label">Collection :<span className="requiredMark"><i className="fa fa-asterisk" /></span></label>
                                                            </div>*/}
                                                        <div className="form-group">
                                                            <label className="form-label">Choose Schedules : </label>
                                                            <ReactSelect
                                                                id = "scheduleId"
                                                                options={this.state.allSchedulers}
                                                                onChange = {(value)=> this.inputChangeHandler(value, "Scheduler")}
                                                                value = {this.getValueForReactSelect(this.state.policyDetails.scheduleId, "scheduleId")}
                                                                isMulti={false}
                                                                className="form-control-multi-select">
                                                            </ReactSelect>
                                                        </div>
                                                    </div>
                                                    <div className="flex-item fx-b50 pd-l-7">
                                                        <div className="form-group mr-b-35">
                                                            <textarea id="description" rows="5" className="form-control" placeholder="Description" value={this.state.policyDetails.description} onChange={this.inputChangeHandler}></textarea>
                                                            <label className="form-group-label">Description :</label>
                                                        </div>
                                                        <div className="form-group">
                                                            <select className="form-control" disabled={Boolean(!this.state.policyDetails.etlId || this.state.isFetchingAttributes || this.props.match.params.id)} id="groupBy" value={this.state.policyDetails.groupBy} onChange={this.inputChangeHandler} required >
                                                                <option value="">{this.state.isFetchingAttributes ? "Loading.." : this.state.policyDetails.etlId ? "Select" : "Select an ETL first"}</option>
                                                                {this.state.etlAttributes && this.state.etlAttributes.groupBy.map((group, groupIndex) => {
                                                                    return <option value={group} key={groupIndex}>{group}</option>
                                                                })}
                                                            </select>
                                                            <label className="form-group-label">Group By :<span className="requiredMark"><i className="fa fa-asterisk" /></span></label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="bottomButton">
                                                <button className="btn btn-success" disabled={this.state.errorCode === 2}>Next</button>
                                                <button type="button" className="btn btn-dark" onClick={()=> {this.props.history.push('/policyManager')}}>Cancel</button>
                                            </div>
                                        </form>
                                    </div>

                                    :
                                    <form onSubmit={this.policyFormSubmit}>
                                        <div className="tab-pane active fade show">
                                                <div className="stepWizardForm">
                                                    {this.state.policyDetails.event.map((val, index) => {
                                                        return (
                                                            <div className="policyConditionalBox mr-b-10" key={index}>
                                                                <div className="flex">
                                                                    <div className="flex-item fx-b100">
                                                                        <div className="flex policyHead">
                                                                            <div className="flex-item fx-b5">
                                                                                <div className="form-group"> <input className="form-control text-center" value="if" readOnly /> </div>
                                                                            </div>
                                                                            <div className="flex-item fx-b10">
                                                                                <button type="button" onClick={() => this.handleConditionGroup(index)} disabled={this.state.policyDetails.event.length === 1} className="btn btn-transparent btn-transparent-danger">
                                                                                    <i className="fas fa-trash-alt"></i>
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div className="flex-item fx-b100 policyHeadContent position-relative">
                                                                        <div className="flex">
                                                                            <div className="flex-item fx-b16_6 pd-r-7">
                                                                                <div className="form-group">
                                                                                    <select className="form-control" value={val.attribute} id="attribute" onChange={() => this.eventChangeHandler(event, index)} required>
                                                                                        <option value="">---Select---</option>
                                                                                        {
                                                                                            this.state.etlAttributes && this.state.etlAttributes.attributes.map((val, atrIndex) => {
                                                                                                return <option value={val} key={val}>{val}</option>
                                                                                            })
                                                                                        }
                                                                                    </select>
                                                                                    <label className="form-group-label" htmlFor="entity">Entity :
                                                                                        <span className="requiredMark"><i className="fa fa-asterisk" /></span>
                                                                                    </label>
                                                                                </div>
                                                                            </div>

                                                                            <div className="flex-item fx-b16_6 pd-r-7 pd-l-7">
                                                                                <div className="form-group">
                                                                                    <select className="form-control" required value={val.triggerType} id="triggerType" onChange={() => this.eventChangeHandler(event, index)}>
                                                                                        <option value="">---Select---</option>
                                                                                        <option value="PERCENTAGE_CHANGE" > % Change</option>
                                                                                        <option value="VALUE">Value</option>
                                                                                        <option value="THRESHOLD_VALUE" >Threshold Value</option>
                                                                                    </select>
                                                                                    <label className="form-group-label" htmlFor="operationType">Operation Type :
                                                                                        <span className="requiredMark"><i className="fa fa-asterisk" /></span>
                                                                                    </label>
                                                                                </div>
                                                                            </div>

                                                                            <div className="flex-item fx-b16_6 pd-r-7 pd-l-7">
                                                                                <div className="form-group">
                                                                                    <select className="form-control" required id="operator" value={val.operator} onChange={() => this.eventChangeHandler(event, index)}>
                                                                                        <option value="">---Select---</option>
                                                                                        <option value="GREATER_THAN_EQUALS_TO" >Greater than equals to</option>
                                                                                        <option value="LESS_THAN_EQUALS_TO">Less than equals to</option>
                                                                                        <option value="EQUALS_TO">Equals to</option>
                                                                                    </select>
                                                                                    <label className="form-group-label" htmlFor="operator">Operator :
                                                                                        <span className="requiredMark"><i className="fa fa-asterisk" /></span>
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                            <div className="flex-item fx-b16_6 pd-r-7 pd-l-7">
                                                                                <div className="form-group">
                                                                                    <input required type="number" className="form-control" id="value" value={val.value} onChange={(event) => this.eventChangeHandler(event, index)} />
                                                                                    <label className="form-group-label" htmlFor="percentageValue">Value :
                                                                                        <span className="requiredMark"><i className="fa fa-asterisk" /></span>
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                            <div className="flex-item fx-b16_6 pd-l-7 pd-r-7">
                                                                                <div className="form-group">
                                                                                    <select className="form-control" value={val.occurrences} id="occurrences" onChange={() => this.eventChangeHandler(event, index)} required>
                                                                                        <option value="">Select</option>
                                                                                        <option value={1}>1</option>
                                                                                        <option value={2}>2</option>
                                                                                        <option value={3}>3</option>
                                                                                        <option value={4}>4</option>
                                                                                        <option value={5}>5</option>
                                                                                        <option value={6}>6</option>
                                                                                        <option value={7}>7</option>
                                                                                    </select>
                                                                                    <label className="form-group-label">Iteration count :
                                                                                        <span className="requiredMark"><i className="fa fa-asterisk" /></span>
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                            <div className="flex-item fx-b16_6 pd-l-7">
                                                                                <div className="form-group">
                                                                                    <select className="form-control" id="alarmTemplate" value={this.state.policyDetails.event[index].action.alarm.templateId} onChange={() => this.eventChangeHandler(event, index)} required>
                                                                                        <option value="">Select</option>
                                                                                        {this.state.allTemplates.filter(template => template.type === "ALARM").map(({value, label}) => 
                                                                                            (<option key={value} value={value}>{label}</option>))
                                                                                        }
                                                                                    </select>
                                                                                    <label className="form-group-label">Template :
                                                                                        <span className="requiredMark"><i className="fa fa-asterisk" /></span>
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                            <div className="flex-item fx-b100">
                                                                                <div className="flex">
                                                                                    <div className="flex-item fx-b100">
                                                                                        <div className="actionItem flex">
                                                                                            <strong>Actions :</strong>
                                                                                            {this.state.actionTypes.map(type => <div className="actionBox flex-item fx-b20" key={type}>
                                                                                                <span className="checkText"><i className={`fas ${this.getKeyValue(type,"className")} mr-r-7`}></i>{this.getKeyValue(type,"heading")}</span>
                                                                                                <label className="checkboxLabel" key={type}>
                                                                                                    <input type="checkbox" name="smsbox" id={type} checked={Boolean(val.action[type])}
                                                                                                        onChange={(event) => this.actionTypeChangeHandler(event, index, type)} />
                                                                                                    <span className="checkmark"></span>
                                                                                                </label>
                                                                                            </div>)}
                                                                                            <span className="actionButton " onClick={()=> this.handleActiveActionIndex(index)}><i className="fas fa-chevron-down"></i></span>
                                                                                        </div>
                                                                                        {val.errorCode === 1 && <p className="text-danger">Please select at least one option.</p>}
                                                                                        {val.errorCode === 2 && <p className="text-danger">Group/Template is missing in one the selected actions.</p>}
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div className="flex-item fx-b100">
                                                                                {this.state.activeActionIndex === index && <div className="policyActionContentBox collapse d-block">
                                                                                    <div className="policyActionContent flex">
                                                                                        {Object.entries(val.action).map(([key,value]) => {
                                                                                                if(key !== "alarm")
                                                                                                    return (<div className="flex-item fx-b50 pd-8" key={key}>
                                                                                                        <div className="policyMailBox">
                                                                                                            <h4><i className={this.getKeyValue(key,"className")}></i>{this.getKeyValue(key,"heading")}</h4>
                                                                                                            <div className="flex">
                                                                                                                <div className={`flex-item pd-r-7 ${key === "ticket" ? "" : "fx-b50"}`}>
                                                                                                                    <div className="form-group mr-b-0">
                                                                                                                        <label className="form-label">Groups : <span className="requiredMark"><i className="fa fa-asterisk" /></span></label>
                                                                                                                        <ReactSelect 
                                                                                                                            value={this.getValueForReactSelect(val.action[key].groupIds)}
                                                                                                                            options= {this.state.allGroups}
                                                                                                                            onChange={(value)=> this.reactSelectChangeHandler(value, index, key)}
                                                                                                                            className="form-control-multi-select"
                                                                                                                            isMulti={true}
                                                                                                                        >
                                                                                                                        </ReactSelect>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                {key != "ticket" && <div className="flex-item fx-b50 pd-l-7">
                                                                                                                    <div className="form-group mr-b-0">
                                                                                                                        <label className="form-label">Template : <span className="requiredMark"><i className="fa fa-asterisk" /></span></label>
                                                                                                                        <ReactSelect
                                                                                                                            options= {this.state.allTemplates.filter(template => template.type === this.getKeyValue(key))}
                                                                                                                            value={this.getValueForReactSelect(val.action[key].templateId, "template")}
                                                                                                                            onChange={(value)=> this.reactSelectChangeHandler(value, index, key, "template")}
                                                                                                                            className="form-control-multi-select"
                                                                                                                            isMulti={false}
                                                                                                                        >
                                                                                                                        </ReactSelect>
                                                                                                                    </div>
                                                                                                                </div>}
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>)
                                                                                        })}
                                                                                        
                                                                                    </div>
                                                                                </div>}
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        )
                                                    })}
                                                    <button type="button" className="btn btn-link float-right" onClick={() => this.handleConditionGroup()} disabled>
                                                        <i className="far fa-plus"></i>Add new
                                                    </button>
                                                </div>
                                            <div className="border-bottom"></div>
                                            <div className="bottomButton">
                                                <button name="button" type="submit" className="btn btn-success">{this.props.match.params.id ? "Update" : "Save"}</button>
                                            </div>
                                        </div>
                                    </form>
                                }
                            </div>
                        </div>
                    </React.Fragment>
                }
                {this.state.isOpen &&
                    <NotificationModal 
                        type={this.state.modalType}
                        message2 = {this.state.message2}
                        onCloseHandler = {this.onCloseHandler}
                    />
                }
            </React.Fragment >
        );
    }
}

PolicyAddEdit.propTypes = {
    dispatch: PropTypes.func.isRequired,
};

const
    mapStateToProps = createStructuredSelector({
        policyDataSuccess: SELECTORS.getPolicyDataSuccess(),
        policyDataFailure: SELECTORS.getPolicyDataFailure(),
        etlAttributes: SELECTORS.getEntityList(),
        etlAttributesError: SELECTORS.getEntityListError(),
        isFetching: SELECTORS.getIsFetching(),
        etlListFailure: SELECTORS.getEtlListFailure(),
        etlListSuccess: SELECTORS.getEtlListSuccess(),
        policySaveSuccess: SELECTORS.savePolicySuccess(),
        policySaveFailure: SELECTORS.savePolicyFailure(),
        policyData: SELECTORS.getPolicyData(),
        policyDataError: SELECTORS.getPolicyDataError(),
        getAllSchedulersSuccess: SELECTORS.getAllSchedulersSuccess(),
        getAllSchedulersFailure: SELECTORS.getAllSchedulersFailure(),
        getAllTemplatesSuccess: SELECTORS.getAllTemplatesSuccess(),
        getAllTemplatesFailure: SELECTORS.getAllTemplatesFailure(),
        getAllGroupsSuccess: SELECTORS.getAllGroupsSuccess(),
        getAllGroupsFailure: SELECTORS.getAllGroupsFailure(),
    });

function
    mapDispatchToProps(dispatch) {
    return {
        dispatch,
        resetToInitialState: () => dispatch(ACTIONS.resetToInitialState()),
        loadPolicyData: id => dispatch(ACTIONS.loadPolicyData(id)),
        fetchEtlAttributes: (id) => dispatch(ACTIONS.fetchEntities(id)),
        getEtlList: (category) => dispatch(ACTIONS.getEtlList(category)),
        savePolicy: (payload, id) => dispatch(ACTIONS.savePolicy(payload, id)),
        getAllPolicyData: () => dispatch(ACTIONS.getAllPolicyData()),
        getAllSchedulers: () => dispatch(ACTIONS.getAllSchedulers()),
        getAllTemplates: () => dispatch(ACTIONS.getAllTemplates()),
        getAllGroups: () => dispatch(ACTIONS.getAllGroups()),
    };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'policyAddEdit', reducer });
const withSaga = injectSaga({ key: 'policyAddEdit', saga });

export default compose(withReducer, withSaga, withConnect)(PolicyAddEdit);
