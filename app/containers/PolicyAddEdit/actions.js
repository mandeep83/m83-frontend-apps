/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import * as CONSTANTS from './constants';

export function defaultAction() {
  return {
    type: CONSTANTS.DEFAULT_ACTION,
  };
}

export function fetchEntities(id) {
  return {
    type: CONSTANTS.FETCH_ENTITIES_REQUEST,
    id
  };
}

export function loadPolicyData(id) {
  return {
    type: CONSTANTS.LOAD_POLICY,
    deviceId: id
  };
}

export function resetToInitialState() {
  return {
    type: CONSTANTS.RESET_TO_INITIAL_STATE,
  };
}

export function getEtlList(category) {
  return {
    type: CONSTANTS.GET_ETL_LIST,
    category
  };
}

export function savePolicy(payload, id) {
  return {
    payload,
    id,
    type: CONSTANTS.SAVE_POLICY,
  };
}

export function getAllPolicyData(){
  return{
    type: CONSTANTS.GET_POLICY_DATA
  }
} 

export function getAllSchedulers(){
  return{
    type: CONSTANTS.GET_ALL_SCHEDULERS
  }
} 

export function getAllTemplates(){
  return{
    type: CONSTANTS.GET_ALL_TEMPLATES
  }
}
export function getAllGroups(){
  return{
    type: CONSTANTS.GET_ALL_GROUPS
  }
}