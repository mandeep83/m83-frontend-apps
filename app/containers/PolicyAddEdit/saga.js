/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { takeEvery } from 'redux-saga';
import * as CONSTANTS from './constants';
import { apiCallHandler } from '../../api';

export function* getPolicyApiHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.POLICY_DATA_SUCCESS, CONSTANTS.POLICY_DATA_FAILURE, 'fetchPolicies')];
}

export function* fetchEntitiesApiHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.FETCH_ENTITIES_SUCCESS, CONSTANTS.FETCH_ENTITIES_FAILURE, 'fetchEntityList')];
}

export function* getEtlListHandler(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_ETL_LIST_SUCCESS, CONSTANTS.GET_ETL_LIST_FAILURE, 'etlListByCategory')];
}

export function* savePolicyDetailsApi(action) {
  yield [apiCallHandler(action, CONSTANTS.SAVE_POLICY_SUCCESS, CONSTANTS.SAVE_POLICY_FAILURE, 'savePolicy')];
}

export function* watcherGetPolicyDataRequest() {
  yield takeEvery(CONSTANTS.LOAD_POLICY, getPolicyApiHandlerAsync);
}

export function* watcherFetchEntities() {
  yield takeEvery(CONSTANTS.FETCH_ENTITIES_REQUEST, fetchEntitiesApiHandlerAsync);
}

export function* watcherGetEtlList() {
  yield takeEvery(CONSTANTS.GET_ETL_LIST, getEtlListHandler);
}

export function* watcherSavePolicyDetails() {
  yield takeEvery(CONSTANTS.SAVE_POLICY, savePolicyDetailsApi);
}



export function* getPolicyData(action){
  yield[apiCallHandler(action, CONSTANTS.GET_POLICY_DATA_SUCCESS, CONSTANTS.GET_POLICY_ERROR, 'getAllPolicyData' )]
}

export function* getAllSchedulers(action){
  yield[apiCallHandler(action, CONSTANTS.GET_ALL_SCHEDULERS_SUCCESS, CONSTANTS.GET_ALL_SCHEDULERS_FAILURE, 'getAllSchedule' )]
}

export function* getAllTemplates(action){
  yield[apiCallHandler(action, CONSTANTS.GET_ALL_TEMPLATES_SUCCESS, CONSTANTS.GET_ALL_TEMPLATES_FAILURE, 'getAllTemplates' )]
}

export function* getAllGroups(action){
  yield[apiCallHandler(action, CONSTANTS.GET_ALL_GROUPS_SUCCESS, CONSTANTS.GET_ALL_GROUPS_FAILURE, 'getGroupList' )]
}

export function* watcherGetPolicyData(){
  yield takeEvery(CONSTANTS.GET_POLICY_DATA, getPolicyData)
}

export function* watcherGetAllSchedulers(){
  yield takeEvery(CONSTANTS.GET_ALL_SCHEDULERS, getAllSchedulers)
}

export function* watcherGetAllTemplates(){
  yield takeEvery(CONSTANTS.GET_ALL_TEMPLATES, getAllTemplates)
}

export function* watcherGetAllGroups(){
  yield takeEvery(CONSTANTS.GET_ALL_GROUPS, getAllGroups)
}

export default function* rootSaga() {
  yield [
    watcherFetchEntities(),
    watcherGetEtlList(),
    watcherSavePolicyDetails(),
    watcherGetPolicyDataRequest(),
    watcherGetPolicyData(),
    watcherGetAllSchedulers(),
    watcherGetAllTemplates(),
    watcherGetAllGroups(),
  ];
}
