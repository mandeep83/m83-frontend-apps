/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { fromJS } from 'immutable';
import * as CONSTANTS from './constants';

export const initialState = fromJS({});

function policyAddEditReducer(state = initialState, action) {
    switch (action.type) {
        case CONSTANTS.DEFAULT_ACTION:
            return state;
        case CONSTANTS.POLICY_DATA_SUCCESS:
            return Object.assign({}, state, {
                policyDataSuccess: action.response
            });
        case CONSTANTS.POLICY_DATA_FAILURE:
            return Object.assign({}, state, {
                policyDataError: action.error
            });
        case CONSTANTS.FETCH_ENTITIES_SUCCESS:
            return Object.assign({}, state, {
                entitiesList: action.response
            });
        case CONSTANTS.FETCH_ENTITIES_FAILURE:
            return Object.assign({}, state, {
                entitiesListError: action.error
            });
        case CONSTANTS.GET_ETL_LIST_SUCCESS:
            return Object.assign({}, state, {
                etlListSuccess: action.response
            });
        case CONSTANTS.GET_ETL_LIST_FAILURE:
            return Object.assign({}, state, {
                etlListFailure: action.error
            });
        case CONSTANTS.SAVE_POLICY_SUCCESS:
            return Object.assign({}, state, {
                savePolicySuccess: {
                    message: action.response,
                    date: new Date()
                }
            });
        case CONSTANTS.SAVE_POLICY_FAILURE:
            return Object.assign({}, state, {
                savePolicyFailure: action.error
            });
        case CONSTANTS.RESET_TO_INITIAL_STATE:
            return initialState;
        case CONSTANTS.GET_POLICY_DATA_SUCCESS:
            return Object.assign({}, state, {
                getPolicyData: action.response
            })
        case CONSTANTS.GET_POLICY_ERROR:
            return Object.assign({}, state, {
                getPolicyDataError: action.error
            })
        case CONSTANTS.GET_ALL_SCHEDULERS_SUCCESS:
        return Object.assign({}, state, {
            getAllSchedulersSuccess: action.response
        });
        case CONSTANTS.GET_ALL_SCHEDULERS_FAILURE:
        return Object.assign({}, state, {
            getAllSchedulersFailure: action.error
        });
        case CONSTANTS.GET_ALL_TEMPLATES_SUCCESS:
            return Object.assign({}, state, {
                'getAllTemplatesSuccess':action.response,
            });
        case CONSTANTS.GET_ALL_TEMPLATES_FAILURE:
            return Object.assign({}, state, {
                'getAllTemplatesFailure': action.error,
            });
        case CONSTANTS.GET_ALL_GROUPS_SUCCESS:
            return Object.assign({}, state, {
                'getAllGroupsSuccess':action.response,
            });
        case CONSTANTS.GET_ALL_GROUPS_FAILURE:
            return Object.assign({}, state, {
                'getAllGroupsFailure': action.error,
            });
        default:
            return state;
    }
}

export default policyAddEditReducer;
