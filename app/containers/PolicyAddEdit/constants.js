/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

export const DEFAULT_ACTION = 'app/PolicyAddEdit/DEFAULT_ACTION';

export const LOAD_POLICY = 'app/PolicyAddEdit/LOAD_POLICY';
export const POLICY_DATA_SUCCESS = 'app/PolicyAddEdit/POLICY_DATA_SUCCESS';
export const POLICY_DATA_FAILURE = 'app/PolicyAddEdit/POLICY_DATA_FAILURE';

export const RESET_TO_INITIAL_STATE = 'app/PolicyAddEdit/RESET_TO_INITIAL_STATE';

export const FETCH_ENTITIES_REQUEST = "app/PolicyAddEdit/FETCH_ENTITIES_REQUEST";
export const FETCH_ENTITIES_SUCCESS = "app/PolicyAddEdit/FETCH_ENTITIES_SUCCESS";
export const FETCH_ENTITIES_FAILURE = "app/PolicyAddEdit/FETCH_ENTITIES_FAILURE";

export const GET_ETL_LIST = 'app/PolicyAddEdit/GET_ETL_LIST';
export const GET_ETL_LIST_SUCCESS = 'app/PolicyAddEdit/GET_ETL_LIST_SUCCESS';
export const GET_ETL_LIST_FAILURE = 'app/PolicyAddEdit/GET_ETL_LIST_FAILURE';

export const SAVE_POLICY = 'app/PolicyAddEdit/SAVE_POLICY';
export const SAVE_POLICY_SUCCESS = 'app/PolicyAddEdit/SAVE_POLICY_SUCCESS';
export const SAVE_POLICY_FAILURE = 'app/PolicyAddEdit/SAVE_POLICY_FAILURE';

export const GET_POLICY_DATA = 'app/PolicyAddEdit/GET_POLICY_DATA';
export const GET_POLICY_DATA_SUCCESS = 'app/PolicyAddEdit/GET_POLICY_DATA_SUCCESS';
export const GET_POLICY_ERROR = "app/PolicyAddEdit/GET_POLICY_ERROR";

export const GET_ALL_TEMPLATES = 'app/PolicyAddEdit/GET_ALL_TEMPLATES';
export const GET_ALL_TEMPLATES_SUCCESS = 'app/PolicyAddEdit/GET_ALL_TEMPLATES_SUCCESS';
export const GET_ALL_TEMPLATES_FAILURE = "app/PolicyAddEdit/GET_ALL_TEMPLATES_FAILURE";

export const GET_ALL_SCHEDULERS = 'app/PolicyAddEdit/GET_ALL_SCHEDULERS';
export const GET_ALL_SCHEDULERS_SUCCESS = 'app/PolicyAddEdit/GET_ALL_SCHEDULERS_SUCCESS';
export const GET_ALL_SCHEDULERS_FAILURE = "app/PolicyAddEdit/GET_ALL_SCHEDULERS_FAILURE";

export const GET_ALL_GROUPS = 'app/PolicyAddEdit/GET_ALL_GROUPS';
export const GET_ALL_GROUPS_SUCCESS = 'app/PolicyAddEdit/GET_ALL_GROUPS_SUCCESS';
export const GET_ALL_GROUPS_FAILURE = "app/PolicyAddEdit/GET_ALL_GROUPS_FAILURE";