/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the policyAddEdit state domain
 */

const selectPolicyAddEditDomain = state =>
  state.get('policyAddEdit', initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by PolicyAddEdit
 */



const makeSelectPolicyAddEdit = () =>
  createSelector(selectPolicyAddEditDomain, substate => substate.toJS());

export const getEntityList = () =>
  createSelector(selectPolicyAddEditDomain, substate => substate.entitiesList);

export const getEntityListError = () =>
  createSelector(selectPolicyAddEditDomain, substate => substate.entitiesListError);

export const getEtlListSuccess = () => createSelector(selectPolicyAddEditDomain, substate => substate.etlListSuccess);
export const getEtlListFailure = () => createSelector(selectPolicyAddEditDomain, substate => substate.etlListFailure);

export const getPolicyDataSuccess = () => createSelector(selectPolicyAddEditDomain, substate => substate.policyDataSuccess);
export const getPolicyDataFailure = () => createSelector(selectPolicyAddEditDomain, substate => substate.policyDataError);

export const savePolicySuccess = () => createSelector(selectPolicyAddEditDomain, substate => substate.savePolicySuccess);
export const savePolicyFailure = () => createSelector(selectPolicyAddEditDomain, substate => substate.savePolicyFailure);

export const getPolicyData = () => createSelector(selectPolicyAddEditDomain, substate => substate.getPolicyData);
export const getPolicyDataError = () => createSelector(selectPolicyAddEditDomain, substate => substate.getPolicyDataError);

export const getAllSchedulersSuccess = () => createSelector(selectPolicyAddEditDomain, substate => substate.getAllSchedulersSuccess);
export const getAllSchedulersFailure = () => createSelector(selectPolicyAddEditDomain, substate => substate.getAllSchedulersFailure);

export const getAllTemplatesSuccess = () => createSelector(selectPolicyAddEditDomain, substate => substate.getAllTemplatesSuccess);
export const getAllTemplatesFailure = () => createSelector(selectPolicyAddEditDomain, substate => substate.getAllTemplatesFailure);

export const getAllGroupsSuccess = () => createSelector(selectPolicyAddEditDomain, substate => substate.getAllGroupsSuccess);
export const getAllGroupsFailure = () => createSelector(selectPolicyAddEditDomain, substate => substate.getAllGroupsFailure);

export const isFetchingState = state => state.get('loader');
export const getIsFetching = () => createSelector(isFetchingState, fetchingState => fetchingState.get('isFetching'));

export default makeSelectPolicyAddEdit;
export { selectPolicyAddEditDomain };
