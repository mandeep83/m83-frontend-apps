/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { FormattedMessage } from "react-intl";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import makeSelectLicenseExpiryPage from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import {logoutHandler} from '../Header/actions'

/* eslint-disable react/prefer-stateless-function */
export class LicenseExpiryPage extends React.Component {

  logoutHandler = () => {
    this.props.logoutHandler();
}

  render() {
    return (
      <div>
        <Helmet>
          <title>License Expired</title>
          <meta name="description" content="M83-LicenseExpiryPage" />
        </Helmet>

        <div className="licenseExpiryBox">
          <div className="licenseExpiryDetail">
            <div className="contentAddBoxImage licenseExpiryImage">
              <img src="https://content.iot83.com/m83/other/license.png" />
            </div>
            <h6 className="mr-b-15"><span className="mr-r-10"><i className="fas fa-exclamation-triangle"></i></span>Your license has expired.</h6>
            <button className="btn btn-primary mr-b-15" onClick={this.logoutHandler}><i className="far fa-power-off mr-r-10"></i>Logout</button>
            <p>For assistance contact <strong>support@83incs.com</strong></p>
          </div>
        </div>
      </div>
    );
  }
}

LicenseExpiryPage.propTypes = {
  dispatch: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
  licenseexpirypage: makeSelectLicenseExpiryPage()
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    logoutHandler: () => dispatch(logoutHandler()),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

const withReducer = injectReducer({ key: "licenseExpiryPage", reducer });
const withSaga = injectSaga({ key: "licenseExpiryPage", saga });

export default compose(
  withReducer,
  withSaga,
  withConnect
)(LicenseExpiryPage);
