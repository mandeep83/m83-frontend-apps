/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
 * Direct selector to the deviceDiagnostics state domain
 */

const selectDeviceDiagnosticsDomain = state => state.get("deviceDiagnostics", initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by DeviceDiagnostics
 */

//export const makeSelectDeviceDiagnostics = () => createSelector(selectDeviceDiagnosticsDomain, substate => substate.toJS());

export const getDeviceInfoSuccess = () => createSelector(selectDeviceDiagnosticsDomain, subState => subState.getDeviceInfoSuccess);
export const getDeviceInfoFailure = () => createSelector(selectDeviceDiagnosticsDomain, subState => subState.getDeviceInfoFailure);

export const getConnectorsListSuccess = () => createSelector(selectDeviceDiagnosticsDomain, subState => subState.getConnectorsListSuccess);
export const getConnectorsListError = () => createSelector(selectDeviceDiagnosticsDomain, subState => subState.getConnectorsListFailure);

export const getSearchByIdOrNameSuccess = () => createSelector(selectDeviceDiagnosticsDomain, subState => subState.getSearchByIdOrNameSuccess);
export const getSearchByIdOrNameFailure = () => createSelector(selectDeviceDiagnosticsDomain, subState => subState.getSearchByIdOrNameFailure);

export const mqttCredentialsSuccess = () => createSelector(selectDeviceDiagnosticsDomain, subState => subState.mqttCredentialsSuccess);
export const mqttCredentialsFailure = () => createSelector(selectDeviceDiagnosticsDomain, subState => subState.mqttCredentialsFailure);
