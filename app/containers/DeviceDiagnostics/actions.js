/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/*
 *
 * DeviceDiagnostics actions
 *
 */

import * as CONSTANTS from "./constants";

export function getDeviceInformation(connectorId,deviceId) {
  return {
    type: CONSTANTS.GET_DEVICE_INFO,
    connectorId,
    deviceId
  };
}

export function defaultAction() {
  return {
    type: CONSTANTS.DEFAULT_ACTION
  };
}

export function getConnectorsListByCategory(){
  return {
    type: CONSTANTS.MQTT_CONNECTOR,
  }
}

export function searchByIdOrName(payload,deviceTypeId){
  return {
    type: CONSTANTS.SEARCH_BY_ID_NAME,
    payload,deviceTypeId
  }
}

export function getMQTTCredentials(deviceTypeId) {
  return {
    type: CONSTANTS.GET_MQTT_CREDENTIALS,
    deviceTypeId,
  }
}