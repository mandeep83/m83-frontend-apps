/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * DeviceDiagnostics
 *
 */

'use strict';


import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import { isNavAssigned } from "../../commonUtils";
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import * as SELECTORS from "./selectors";
import Loader from '../../components/Loader/Loadable';
import reducer from "./reducer";
import saga from "./saga";
import * as ACTIONS from './actions';
import MQTT from 'mqtt';
import NotificationModal from '../../components/NotificationModal/Loadable'
import { subscribeTopic, unsubscribeTopic } from "../../mqttConnection";
import ReactJson from "react-json-view";
import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';
import cloneDeep from 'lodash/cloneDeep';
import AddNewButton from "../../components/AddNewButton/Loadable";
import { filter, isEqual } from "lodash";

/* eslint-disable react/prefer-stateless-function */
let client;

export class DeviceDiagnostics extends React.Component {
    state = {
        connectorsList: [],
        deviceInfo: {
            deviceInfo: null
        },
        isFetching: true,
        showTabs: false,
        tab1: false,
        tab2: false,
        connectorId: '',
        connectorName: '',
        deviceId: this.props.match.params.deviceId ? this.props.match.params.deviceId : "",
        showList: false,
        allTopics: [],
        debugLoader: false,
        isOpen: false,
        isSubscribe: false,
        isConnectedToMQTT: false,
        suggestionList: [],
        isSuggestionFetched: false,
        searchKey: this.props.match.params.deviceId ? this.props.match.params.deviceId : "",
        dataView: "TABLE",
        credentials: null,
        fetchingMQTTCredentials: false,
    }

    refreshComponent = () => {
        this.setState({
            connectorsList: [],
            deviceInfo: {},
            isFetching: true,
            showTabs: false,
            tab1: false,
            tab2: false,
            connectorId: '',
            connectorName: '',
            showList: false,
            allTopics: [],
            isOpen: false,
            suggestionList: [],
            isSuggestionFetched: false,
            dataView: "TABLE"
        }, () => {
            this.props.getConnectorsListByCategory("MQTT")
        })
    }
    
    componentWillMount() {
        this.props.getConnectorsListByCategory("MQTT")
    }

    componentDidMount() {
        document.addEventListener('mousedown', this.handleClickOutside);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.getConnectorsListSuccess && nextProps.getConnectorsListSuccess !== this.props.getConnectorsListSuccess) {
            let connectorsList = [],
                connectorId = '';
            if (nextProps.getConnectorsListSuccess.length) { //check if list is empty or not
                connectorsList = nextProps.getConnectorsListSuccess.map(connector => {
                    connector.label = connector.name;
                    connector.value = connector.id;
                    return connector;
                })
                connectorId = this.props.match.params.connectorId ?
                    this.props.match.params.connectorId : nextProps.getConnectorsListSuccess[0].id
            }
            this.setState({
                connectorsList,
                isFetching: false,
                connectorId,
                isSearching: Boolean(this.props.match.params.deviceId)
            }, () => {
                this.props.match.params.deviceId && this.props.getDeviceInformation(connectorId, this.props.match.params.deviceId)
            })
        }
        if (nextProps.getConnectorsListFailure && nextProps.getConnectorsListFailure !== this.props.getConnectorsListFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.getConnectorsListFailure[0],
                isSearching: false,
            })
        }

        if (nextProps.getDeviceInfoSuccess && nextProps.getDeviceInfoSuccess !== this.props.getDeviceInfoSuccess) {
            let gatewayId = nextProps.getDeviceInfoSuccess.gatewayId,
                _uniqueDeviceId = nextProps.getDeviceInfoSuccess._uniqueDeviceId;
            let allTopics = nextProps.getDeviceInfoSuccess.topics.map((topicName) => ({
                topicName: nextProps.getDeviceInfoSuccess.gatewayId ? topicName.replace(_uniqueDeviceId, gatewayId) : topicName,
                columns: [],
                messages: [],
                isConnectedToMQTT: false,
                isSubscribe: false,
                debugLoader: false,
            }))
            this.setState({
                deviceInfo: nextProps.getDeviceInfoSuccess,
                allTopics,
                isFetching: false,
                showList: true,
                isSearching: false,
                selectedTopic: allTopics[0]['topicName']
            })
        }

        if (nextProps.getDeviceInfoFailure && nextProps.getDeviceInfoFailure !== this.props.getDeviceInfoFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.getDeviceInfoFailure[0],
                isSearching: false,
            })
        }

        if (nextProps.searchByIdOrNameSuccess && nextProps.searchByIdOrNameSuccess !== this.props.searchByIdOrNameSuccess) {
            this.setState({
                suggestionList: nextProps.searchByIdOrNameSuccess,
                isSuggestionFetched: true
            })
        }

        if (nextProps.SearchByIdOrNameFailure && nextProps.SearchByIdOrNameFailure !== this.props.SearchByIdOrNameFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.SearchByIdOrNameFailure,
                isSearching: false,
            })
        }

        if (nextProps.mqttCredentialsSuccess && nextProps.mqttCredentialsSuccess !== this.props.mqttCredentialsSuccess) {
            let password = window.atob(nextProps.mqttCredentialsSuccess.password);
            password = password.substring(8, password.length - 6);
            let credentials = {
                userName: this.state.connectorId,
                password: password,
                clientId: `${this.state.deviceId}-read`,
            };
            this.setState({
                credentials,
                fetchingMQTTCredentials: false,
            }, () => { this.showSniffingModal() })
        }

        if (nextProps.mqttCredentialsFailure && nextProps.mqttCredentialsFailure !== this.props.mqttCredentialsFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                fetchingMQTTCredentials: false,
                message2: nextProps.mqttCredentialsFailure,
            })
        }
    }

    getDeviceDetails = () => {
        let allTopics = [...this.state.allTopics];
        if (allTopics.length) {
            allTopics.map(topic => {
                if (topic.messages.length) {
                    topic.messages = [];
                }
            })
        }
        this.setState({
            isConnectedToMQTT: false,
            isSearching: true,
            allTopics,
        }, () => {
            this.props.getDeviceInformation(this.state.connectorId, this.state.deviceId)
        })
    }
    
    onCloseHandler = () => {
        this.setState({
            isOpen: false,
            message2: '',
            modalType: "",
        })
    }

    searchByConnectorIdOnChangeHandler = ({ currentTarget }) => {
        if (client && client.connected) {
            client.end();
        }
        this.setState({
            searchKey: currentTarget.value,
            isStartTyping: currentTarget.value.length > 3,
            isSuggestionFetched: false,
            deviceInfo: {},
            deviceId: '',
        }, () => {
            this.state.searchKey.length > 3 && this.props.searchByIdOrName({ searchText: this.state.searchKey }, this.state.connectorId)
        })
    }
    
    getDeviceIdFromConnectorList = (e) => {
        if (client && client.connected) {
            client.end();
        }
        this.setState({
            connectorId: e.target.value,
            credentials: null,
            deviceId: '',
            searchKey: '',
            deviceInfo: {}
        }, () => {
            this.props.match.params.connectorId && this.props.history.push('/deviceDiagnostics')
        })
    }
    
    fetchMQTTCredentials = () => {
        if (this.state.credentials) {
            this.showSniffingModal();
        } else {
            this.setState({
                fetchingMQTTCredentials: true,
            }, () => { this.props.getMQTTCredentials(this.state.connectorId) });
        }
    }

    mqttMessageHandler = (message, topic) => {
        try {
            let jsonObj = message,
                allTopics = cloneDeep(this.state.allTopics);
            allTopics.map(topicObj => {
                if ((topicObj.topicName === topic || (topicObj.topicName.split('/')[1] === 'miscellaneous' && topic.split('/')[1] === 'miscellaneous')) && topicObj.isSubscribe) {
                    let isColumnsChanged = !isEqual(topicObj.columns, this.getColumns(jsonObj));

                    if (topicObj.columns.length === 0 || isColumnsChanged) {
                        topicObj.columns = this.getColumns(jsonObj);
                    }

                    topicObj.messages.unshift(jsonObj);

                    topicObj.isConnectedToMQTT = true;
                    topicObj.debugLoader = false;
                }
            });
            this.setState({
                allTopics,
            });
        } catch (e) {
            let allTopics = cloneDeep(this.state.allTopics);
            let requiredTopic = allTopics.find(topic => topic.topicName === this.state.selectedTopic)
            requiredTopic.isSubscribe = false;
            requiredTopic.debugLoader = false;

            this.setState({
                modalType: "error",
                isOpen: true,
                message2: 'The debug data is not a valid JSON data format.',
                allTopics
            })
        }
    }

    showSniffingModal = () => {
        let allTopics = cloneDeep(this.state.allTopics);
        let requiredTopic = allTopics.find(topic => topic.topicName === this.state.selectedTopic);
        requiredTopic.isSubscribe = true;
        requiredTopic.debugLoader = true;

        this.setState({
            allTopics,
        }, () => {
            subscribeTopic(this.state.selectedTopic, this.mqttMessageHandler, null, this.state.credentials);
        });
    }

    clearSniffData = () => {
        let allTopics = this.state.allTopics;
        allTopics.find(topic => topic.topicName === this.state.selectedTopic).messages = []
        this.setState({
            allTopics,
        });
    }

    subscribeUnsubscribeHandler = () => {
        const allTopics = cloneDeep(this.state.allTopics);
        let requiredTopic = allTopics.find(topic => topic.topicName === this.state.selectedTopic)
        requiredTopic.isSubscribe = !requiredTopic.isSubscribe;
        requiredTopic.debugLoader = !requiredTopic.messages.length
        this.setState({
            allTopics,
        })
    }

    emptySearchFieldOnCrossClick = () => {
        if (client && client.connected) {
            client.end();
        }
        this.setState({
            deviceId: '',
            searchKey: "",
            deviceInfo: {}
        })
    }

    getColumns = (dataObj) => {
        let columns = [];
        Object.keys(dataObj).map(key => {
            if (typeof dataObj[key] === 'object') {
                Object.keys(dataObj[key]).map(subKey => {
                    if (typeof dataObj[key][subKey] === 'object') {
                        Object.keys(dataObj[key][subKey]).map(subSubKey => {
                            columns.push({
                                headerName: `${key}.${subKey}.${subSubKey}`,
                                field: subSubKey,
                                sortable: true,
                            })
                        })
                    } else {
                        columns.push({
                            headerName: `${key}.${subKey}`,
                            field: subKey,
                            sortable: true,
                        })
                    }

                })
            } else {
                columns.push({
                    headerName: key,
                    field: key,
                    sortable: true,
                })
            }
        });
        return columns;
    }

    getTableData = () => {
        let allTopics = this.state.allTopics,
            data = [];

        allTopics.find(topic => topic.topicName === this.state.selectedTopic).messages.map(dataObj => {
            let obj = {};
            Object.keys(dataObj).map(key => {
                if (typeof dataObj[key] === 'object') {
                    Object.keys(dataObj[key]).map(subKey => {
                        if (typeof dataObj[key][subKey] === 'object') {
                            Object.keys(dataObj[key][subKey]).map(subSubKey => {
                                obj[subSubKey] = dataObj[key][subKey][subSubKey];
                            })
                        } else {
                            obj[subKey] = dataObj[key][subKey];
                        }

                    })
                } else {
                    obj[key] = key === 'timestamp' ? new Date(dataObj[key]).toLocaleString('en-US', { timeZone: localStorage.timeZone }) : dataObj[key];
                }

            });
            data.push(obj);
        });
        return data;
    }

    onCloseHandler = () => {
        this.setState({
            isOpen: false,
            message2: '',
            modalType: '',
        });
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClickOutside);
    }

    handleClickOutside = (event) => {
        if (this.refs.searchBox && !this.refs.searchBox.contains(event.target)) {
            if (this.state.isStartTyping) {
                let splitIdOfElement = event.target.id.includes("suggestion") && event.target.id.split("_")
                let getSearchKey = (searchKey) => {
                    if (event.target.id.includes("suggestion")) {
                        splitIdOfElement.shift();
                        splitIdOfElement.pop();
                        searchKey = splitIdOfElement.join("_")
                    }
                    return searchKey === "null" ? event.target.id.split("_")[2] : searchKey
                }

                this.setState((previousState) => ({
                    isStartTyping: false,
                    isSuggestionFetched: false,
                    deviceId: event.target.id.includes("suggestion") ? splitIdOfElement[splitIdOfElement.length - 1] : previousState.deviceId,
                    searchKey: getSearchKey(previousState.searchKey),
                }), () => {
                    event.target.id.includes("suggestion") && this.getDeviceDetails()
                })
            }
        }
    }

    topicChangeHandler = (topicName) => {
        if (topicName === this.state.selectedTopic)
            return;
        this.setState({
            selectedTopic: topicName,
        })
    }
    
    render() {
        const defaultColumnDefs = { filter: true, floatingFilter: true, }
        let currentTopic = this.state.allTopics && this.state.allTopics.find(topic => topic.topicName === this.state.selectedTopic);
        return (
            <React.Fragment>
                <Helmet>
                    <title>Device Diagnostics</title>
                    <meta name="description" content="Description of Device Diagnostics" />
                </Helmet>

                <header className="content-header d-flex">
                    <div className="flex-60">
                        <h6>Device Diagnostics</h6>
                    </div>
                    
                    <div className="flex-40 text-right">
                        <div className="content-header-group">
                            {Object.keys(this.state.deviceInfo).length > 0 ?
                                (this.state.allTopics.length > 0 && this.state.allTopics.find(topic => topic.topicName === this.state.selectedTopic).messages.length > 0) ?
                                    <React.Fragment>
                                        <button className={this.state.dataView === 'TABLE' ? "btn btn-light active" : "btn btn-light"} onClick={() => this.setState({ dataView: 'TABLE' })}>
                                            <i className="fad fa-table"></i>
                                        </button>
                                        <button className={this.state.dataView === 'TREE' ? "btn btn-light active" : "btn btn-light"} onClick={() => this.setState({ dataView: 'TREE' })}>
                                            <i className="far fa-folder-tree"></i>
                                        </button>
                                    </React.Fragment> : null
                                : ""
                            }
                            {(!this.state.deviceInfo.isConnected) &&
                                <button className="btn btn-light" disabled={this.state.isFetching} data-tooltip data-tooltip-text="Refresh" data-tooltip-place="bottom" onClick={() => this.refreshComponent()}><i className="far fa-sync-alt"></i></button>
                            }
                        </div>
                    </div>
                </header>

                {this.state.isFetching ?
                    <Loader /> :
                    <React.Fragment>
                        {this.state.connectorsList.length > 0 ?
                            <div className="content-body diagnostics-content-body">
                                <div className="form-info-wrapper form-info-wrapper-left pb-0">
                                    <div className="form-info-header">
                                        <h5>Basic Information</h5>
                                    </div>
                                    <div className="form-info-body">
                                        <div className="form-group">
                                            <label className="form-group-label">Device Type :</label>
                                            <select className="form-control" value={this.state.connectorId} disabled={currentTopic && currentTopic.isSubscribe} onChange={(e) => this.getDeviceIdFromConnectorList(e)}>
                                                {this.state.connectorsList.length > 0 ?
                                                    this.state.connectorsList.map((connector, index) => {
                                                        return <option value={connector.id} key={index}>{connector.label}</option>
                                                    }) : <option value='' >No Devices available</option>}
                                            </select>
                                        </div>
                                        <div className="form-group">
                                            <label className="form-group-label">Device Id :</label>
                                            <div className="search-wrapper">
                                                <div className="search-wrapper-group">
                                                    <input type="text" className="form-control" ref="searchBox" onChange={(e) => this.searchByConnectorIdOnChangeHandler(e)} disabled={this.state.connectorId.length === 0 || currentTopic && currentTopic.isSubscribe} value={this.state.searchKey} placeholder="Search...(min 4 characters)" />
                                                    <span className="search-wrapper-icon"><i className="far fa-search"></i></span>
                                                    {this.state.isStartTyping &&
                                                    <div className="search-wrapper-suggestion">
                                                        <ul className="list-style-none">
                                                            {this.state.isSuggestionFetched ?
                                                                this.state.suggestionList.length > 0 ? this.state.suggestionList.map((suggestion) =>
                                                                        <li key={suggestion.id} id={"suggestion_" + (suggestion.name + "_" + suggestion.id)} >{suggestion.name || suggestion.id}</li>)
                                                                    :
                                                                    <li>No Match Found!</li>
                                                                :
                                                                <li>Fetching...</li>
                                                            }
                                                        </ul>
                                                    </div>
                                                    }
                                                    {this.state.searchKey.length > 0 &&
                                                        <button type="button" className="search-wrapper-button" disabled={currentTopic && currentTopic.isSubscribe} onClick={() => this.emptySearchFieldOnCrossClick()}>
                                                            <i className="far fa-times"></i>
                                                        </button>
                                                    }
                                                </div>
                                            </div>
                                        </div>
    
                                        {this.state.isSearching ?
                                            <div className="diagnostic-info-wrapper">
                                                <div className="inner-loader-wrapper h-100">
                                                    <div className="inner-loader-content">
                                                        <i className="fad fa-sync-alt fa-spin"></i>
                                                    </div>
                                                </div>
                                            </div> :
                                            Object.keys(this.state.deviceInfo).length !== 0 ? this.state.showList &&
                                                <div className="diagnostic-info-wrapper">
                                                    <div className="diagnostic-info-content pl-3">
                                                        <div className={`diagnostic-info-border bg-${this.state.deviceInfo.isConnected ? "green" : 'red'}`} data-tooltip data-tooltip-text={this.state.deviceInfo.isConnected ? "Online" : 'Offline'} data-tooltip-place="bottom" ></div>
                                                        <p>Actions :</p>
                                                        <div className="button-group">
                                                            <button className="btn-transparent btn-transparent-blue" data-tooltip data-tooltip-text="Debug" data-tooltip-place="bottom" disabled={currentTopic.isConnectedToMQTT || currentTopic.debugLoader || this.state.fetchingMQTTCredentials} onClick={() => this.fetchMQTTCredentials()}>{this.state.fetchingMQTTCredentials ? <i className="far fa-cog mr-r-5"></i> :<i className="far fa-bug"></i>}</button>
                                                            <button className={currentTopic.isSubscribe ? "btn-transparent btn-transparent-yellow" : "btn-transparent btn-transparent-green"} data-tooltip data-tooltip-text={currentTopic.isSubscribe ? "Pause" : "Start"} data-tooltip-place="bottom" disabled={(!this.state.deviceInfo.isConnected) || currentTopic.isConnectedToMQTT === false} onClick={() => { this.subscribeUnsubscribeHandler() }}>{currentTopic.isSubscribe ? <i className="far fa-pause"></i> : <i className="far fa-play"></i>}</button>
                                                            <button className="btn-transparent btn-transparent-gray" data-tooltip data-tooltip-text="Clear" data-tooltip-place="bottom" disabled={(!this.state.deviceInfo.isConnected) || this.state.allTopics.length === 0 || this.state.allTopics.find(topic => topic.topicName === this.state.selectedTopic).messages.length === 0} onClick={() => this.clearSniffData()}><i className="far fa-broom"></i></button>
                                                        </div>
                                                    </div>
                                                    
                                                    <div className="diagnostic-info-content">
                                                        <p>Device Info :</p>
                                                        {this.state.deviceInfo.name ?
                                                            <div className="diagnostic-info-location">
                                                                {this.state.deviceInfo.location &&
                                                                    <img src={`https://maps.googleapis.com/maps/api/staticmap?center=${this.state.deviceInfo.location}&zoom=6&size=75x75&key=AIzaSyDhe3XJyqFibPDk5tRdcAMeH09cv7Elc08`} onClick={() => window.open(`https://www.google.com/maps/place/${this.state.deviceInfo.lat},${this.state.deviceInfo.lng}`)} />
                                                                }
                                                                <h6 className="text-gray word-wrap-break">{this.state.deviceInfo.name}</h6>
                                                                <p className="mb-0">{this.state.deviceInfo.location}</p>
                                                            </div> :
                                                            <h6 className="text-gray mb-0 word-wrap-break">{this.state.deviceInfo._uniqueDeviceId}</h6>
                                                        }
                                                    </div>
    
                                                    <div className="diagnostic-info-content">
                                                        <p>Gateway Id :</p>
                                                        <h6 className="text-gray mb-0 word-wrap-break">{this.state.deviceInfo.gatewayId ? this.state.deviceInfo.gatewayId : '-'}</h6>
                                                    </div>
                                                    
                                                    <div className="diagnostic-info-content">
                                                        <p>Device Type :</p>
                                                        <button className="btn btn-link text-truncate" onClick={() => { this.props.history.push(`/addOrEditDeviceType/${this.state.deviceInfo.connectorId}`) }}>
                                                            {this.state.connectorsList.find((temp) => temp.id == this.state.deviceInfo.connectorId).name}
                                                        </button>
                                                    </div>
                                                    
                                                    <div className="diagnostic-info-content">
                                                        <p>Device Group :</p>
                                                        <button className="btn btn-link text-truncate" onClick={() => { this.props.history.push(`/deviceGroups/${this.state.deviceInfo.connectorId}/${this.state.deviceInfo.deviceGroupId}`) }}>
                                                            {this.state.deviceInfo.deviceGroupName}
                                                        </button>
                                                    </div>
                                                    
                                                    <div className="diagnostic-info-content">
                                                        <p>Device Tag :</p>
                                                        {this.state.deviceInfo.tag ?
                                                            <span className="badge badge-pill badge-light" style={{ color: this.state.deviceInfo.tagColor }} onClick={() => { this.props.history.push({ pathname: `/deviceGroups/${this.state.deviceInfo.connectorId}/${this.state.deviceInfo.deviceGroupId}`, state: { childViewType: 'GROUP_TAGVIEW' } }) }}>
                                                                {this.state.deviceInfo.tag}
                                                            </span>
                                                            : "-"
                                                        }
                                                    </div>
                                                    
                                                    <div className="diagnostic-info-content">
                                                        <p>Mqtt Session :</p>
                                                        {this.state.deviceInfo.isConnected ?
                                                            <span className="badge badge-pill alert-success"><i className="fad fa-circle mr-r-5"></i>Active</span> :
                                                            <span className="badge badge-pill alert-danger"><i className="fad fa-circle mr-r-5"></i>In-Active</span>
                                                        }
                                                    </div>
                                                </div> :
                                                <div className="diagnostic-info-wrapper">
                                                    <div className="inner-message-wrapper h-100">
                                                        <div className="inner-message-content">
                                                            <i className="fad fa-microchip"></i>
                                                            <h6>Please enter a valid device id.</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                        }
                                    </div>
                                </div>
    
                                <div className="form-content-wrapper form-content-wrapper-left h-100">
                                    <div className="form-content-box mb-0 h-100">
                                        <div className="form-content-body h-100">
                                            {this.state.isSearching ?
                                                <div className="inner-loader-wrapper h-100">
                                                    <div className="inner-loader-content">
                                                        <i className="fad fa-sync-alt fa-spin"></i>
                                                    </div>
                                                </div> :
                                                Object.keys(this.state.deviceInfo).length > 0 ?
                                                    <React.Fragment>
                                                        {this.state.allTopics.length > 0 ?
                                                            <div className="action-tabs">
                                                                <ul className="nav nav-tabs">
                                                                    {this.state.allTopics.map((topic,index) => {
                                                                        return (
                                                                            <li key={index} className={this.state.selectedTopic === topic.topicName ? "nav-item flex-16_6 active" : "nav-item flex-16_6"}>
                                                                                <a className="nav-link" onClick={() => this.topicChangeHandler(topic.topicName)}>{topic.topicName.includes("miscellaneous") ? topic.topicName.substring(topic.topicName.indexOf('/') + 1, topic.topicName.length) :  topic.topicName.split('/')[2]}</a>
                                                                            </li>
                                                                        )
                                                                    })}
                                                                </ul>
                                                            </div>: ""
                                                        }
                
                                                        {(this.state.allTopics.length > 0 && this.state.allTopics.find(topic => topic.topicName === this.state.selectedTopic).messages.length > 0) ?
                                                            <div className="diagnose-content-wrapper">
                                                                {this.state.dataView === 'TABLE' ?
                                                                    <div className="debug-table-wrapper h-100">
                                                                        <div className="ag-theme-alpine">
                                                                            <AgGridReact
                                                                                columnDefs={this.state.allTopics.find(topic => topic.topicName === this.state.selectedTopic).columns.map((col) => ({
                                                                                    ...col, filter: 'agTextColumnFilter',
                                                                                    filterParams: {
                                                                                        newRowsAction: "keep"
                                                                                    }
                                                                                }))}
                                                                                rowData={this.getTableData()}
                                                                                animateRows={true}
                                                                                pagination={true}
                                                                                paginationPageSize={10}
                                                                                defaultColDef = {defaultColumnDefs}
                                                                            >
                                                                            </AgGridReact>
                                                                        </div>
                                                                    </div>
                                                                    :
                                                                    <div className="h-100 overflow-y-auto">
                                                                        {this.state.allTopics.find(topic => topic.topicName === this.state.selectedTopic).messages.map((messageObj, index) =>
                                                                            <ReactJson key={index} src={messageObj} />)
                                                                        }
                                                                    </div>
                                                                }
                                                            </div> :
                                                            currentTopic && (currentTopic.debugLoader || currentTopic.isSubscribe) ?
                                                                <div className="diagnose-content-wrapper">
                                                                    <div className="inner-loader-wrapper h-100">
                                                                        <div className="inner-loader-content">
                                                                            <h6>Debugging...</h6>
                                                                            <img src="https://content.iot83.com/m83/misc/uploading.gif" />
                                                                            <p className="text-gray">Please be patient. This may take few seconds.</p>
                                                                        </div>
                                                                    </div>
                                                                </div> :
                                                                <div className={this.state.showList ? "diagnose-content-wrapper" : "h-100"}>
                                                                    <AddNewButton
                                                                        text1="No debug data available"
                                                                        text2={this.state.deviceInfo.isConnected ? "Please start debug to view data." : "Please enter a valid device id."}
                                                                        imageIcon="noData.png"
                                                                        addButtonEnable={false}
                                                                    />
                                                                </div>
                                                        }
                                                    </React.Fragment>
                                                    :
                                                    <AddNewButton
                                                        text1="No debug data available"
                                                        text2="Please enter a valid device id."
                                                        imageIcon="noData.png"
                                                        addButtonEnable={false}
                                                    />
                                            }
                                        </div>
                                    </div>
                                </div>
                            </div>
                            :
                            <div className="content-body">
                                <AddNewButton
                                    text1="No device type(s) available"
                                    imageIcon="addDeviceDiagnostics.png"
                                    text2="You haven't created any device type(s) yet. Please create a device type first"
                                    addButtonEnable={isNavAssigned("deviceTypes")}
                                    createItemOnAddButtonClick={() => this.props.history.push(`/addOrEditDeviceType`)}
                                />
                            </div>
                        }
                    </React.Fragment>
                }
                
                {/*{this.state.isFetching ? <Loader /> :
                    <React.Fragment>
                        {this.state.connectorsList.length > 0 ?
                            <div className="content-body">
                                <div className="form-info-wrapper form-info-wrapper-left">
                                    <div className="form-info-header">
                                        <h5>Basic Information</h5>
                                    </div>
                                    <div className="form-info-body">
                                        <div className="form-group">
                                            <label className="form-group-label">Device Type : <i className="fas fa-asterisk form-group-required"></i></label>
                                            <select className="form-control" value={this.state.connectorId} disabled={currentTopic && currentTopic.isSubscribe} onChange={(e) => this.getDeviceIdFromConnectorList(e)}>
                                                {this.state.connectorsList.length > 0 ?
                                                    this.state.connectorsList.map((connector, index) => {
                                                        return <option value={connector.id} key={index}>{connector.label}</option>
                                                    }) : <option value='' >No Devices available</option>}
                                            </select>
                                        </div>
                                        
                                        <div className="form-group">
                                            <label className="form-group-label">Device Id : <i className="fas fa-asterisk form-group-required"></i></label>
                                            <div className="search-wrapper">
                                                <div className="search-wrapper-group">
                                                    <input type="text" className="form-control" ref="searchBox" onChange={(e) => this.searchByConnectorIdOnChangeHandler(e)} disabled={this.state.connectorId.length === 0 || currentTopic && currentTopic.isSubscribe} value={this.state.searchKey} placeholder="Search...(min 4 characters)" />
                                                    <span className="search-wrapper-icon"><i className="far fa-search"></i></span>
                                                    {this.state.isStartTyping &&
                                                        <div className="search-wrapper-suggestion">
                                                            <ul className="list-style-none">
                                                                {this.state.isSuggestionFetched ?
                                                                    this.state.suggestionList.length > 0 ? this.state.suggestionList.map((suggestion) =>
                                                                        <li key={suggestion.id} id={"suggestion_" + (suggestion.name + "_" + suggestion.id)} >{suggestion.name || suggestion.id}</li>)
                                                                        :
                                                                        <li>No Match Found!</li>
                                                                    :
                                                                    <li>Fetching...</li>
                                                                }
                                                            </ul>
                                                        </div>
                                                    }
                                                    {this.state.searchKey.length > 0 &&
                                                        <button type="button" className="search-wrapper-button" disabled={currentTopic && currentTopic.isSubscribe} onClick={() => this.emptySearchFieldOnCrossClick()}><i className="far fa-times"></i></button>
                                                    }
                                                </div>
                                            </div>
                                        </div>
                                        
                                        {this.state.isSearching ?
                                            <div className="inner-loader-wrapper h-100">
                                                <div className="inner-loader-content">
                                                    <i className="fad fa-sync-alt fa-spin"></i>
                                                </div>
                                            </div> :
                                            Object.keys(this.state.deviceInfo).length !== 0 ?
                                                this.state.showList &&
                                                <div className="diagnostic-info-wrapper">
                                                    <p className="text-truncate"><strong>Device Info : </strong> ebcb298ac7354e898b0d27d0f2237185</p>
                                                    <p className="text-truncate"><strong>Location : </strong></p>
                                                    <p className="text-truncate"><strong>Device Info : </strong></p>
        
                                                    <p><strong>Device Type : </strong>
                                                        <span className="button-group-link">
                                                        <button className="btn btn-link text-truncate">shakherTest</button>
                                                    </span>
                                                    </p>
                                                    <p><strong>Group : </strong>
                                                        <span className="button-group-link">
                                                        <button className="btn btn-link text-truncate">Default.ShakherTest</button>
                                                    </span>
                                                    </p>
                                                    <p className="text-truncate"><strong>Tag : </strong>
                                                        <span className="badge badge-pill badge-light list-view-badge-pill text-underline-hover">N/A</span>
                                                    </p>
                                                    <p className="text-truncate"><strong>Status : </strong>
                                                        <span className="badge badge-pill alert-success list-view-pill"><i className="fad fa-circle mr-r-5"></i>Online</span>
                                                        <span className="badge badge-pill alert-danger list-view-pill"><i className="fad fa-circle mr-r-5"></i>Offline</span>
                                                    </p>
                                                    <p className="text-truncate"><strong>Action : </strong>
                                                        <div className="button-group">
                                                            <button className="btn-transparent btn-transparent-blue" data-tooltip data-tooltip-text="Debug" data-tooltip-place="bottom" >
                                                                <i className="far fa-cog mr-r-5"></i>
                                                                <i className="far fa-bug"></i>
                                                            </button>
                                                            <button className="btn-transparent btn-transparent-green" data-tooltip data-tooltip-text="Start" data-tooltip-place="bottom" ><i className="far fa-play"></i></button>
                                                            <button className="btn-transparent btn-transparent-gray" data-tooltip data-tooltip-text="Clear" data-tooltip-place="bottom"><i className="far fa-broom"></i></button>
                                                        </div>
                                                    </p>
                                                </div>
                                                :
                                                <div className="diagnostic-info-wrapper">
                                                    <div>
                                                        <div className="diagnostic-info-wrapper-image">
                                                            <img src="https://content.iot83.com/m83/other/noSearchResult.png" />
                                                        </div>
                                                        <p>No data matched your search. Try using other search options.</p>
                                                    </div>
                                                </div>
                                        }
                                    </div>
                                </div>
    
                                <div className="form-content-wrapper form-content-wrapper-left">
                                    {Object.keys(this.state.deviceInfo).length > 0 &&
                                    <div className="form-content-box mb-0">
                                        <div className="form-content-body diagnose-content-wrapper">
                                            {this.state.allTopics.length > 0 ?
                                                <div className="action-tabs mb-0">
                                                    <ul className="nav nav-tabs">
                                                        {this.state.allTopics.map((topic,index) => {
                                                            return (
                                                                <li key={index} className="nav-item">
                                                                    <a className={this.state.selectedTopic === topic.topicName ? "nav-link active" : "nav-link"} onClick={() => this.topicChangeHandler(topic.topicName)}>{topic.topicName.includes("miscellaneous") ? topic.topicName.substring(topic.topicName.indexOf('/') + 1, topic.topicName.length) :  topic.topicName.split('/')[2]}</a>
                                                                </li>
                                                            )
                                                        })}
                                                    </ul>
                                                </div> : ""
                                            }
                                            
                                            {(this.state.allTopics.length > 0 && this.state.allTopics.find(topic => topic.topicName === this.state.selectedTopic).messages.length > 0) ?
                                                <div className="diagnose-content-wrapper">
                                                    {this.state.dataView === 'TABLE' ?
                                                        <div className="debug-table-wrapper h-100">
                                                            <div className="ag-theme-alpine">
                                                                <AgGridReact
                                                                    columnDefs={this.state.allTopics.find(topic => topic.topicName === this.state.selectedTopic).columns.map((col) => ({
                                                                        ...col, filter: 'agTextColumnFilter',
                                                                        filterParams: {
                                                                            newRowsAction: "keep"
                                                                        }
                                                                    }))}
                                                                    rowData={this.getTableData()}
                                                                    animateRows={true}
                                                                    pagination={true}
                                                                    paginationPageSize={10}
                                                                    defaultColDef={defaultColumnDefs}
                                                                >
                        
                                                                </AgGridReact>
                                                            </div>
                                                        </div>
                                                        :
                                                        <div className="card h-100 overflow-y-auto">
                                                            <div className="card-body">
                                                                {this.state.allTopics.find(topic => topic.topicName === this.state.selectedTopic).messages.map((messageObj, index) =>
                                                                    <ReactJson key={index} src={messageObj} />)
                                                                }
                                                            </div>
                                                        </div>
                                                    }
                                                </div> :
                                                currentTopic && (currentTopic.debugLoader || currentTopic.isSubscribe) ?
                                                    <div className="inner-loader-wrapper h-100">
                                                        <div className="inner-loader-content">
                                                            <h6>Debugging...</h6>
                                                            <img src="https://content.iot83.com/m83/misc/uploading.gif" />
                                                            <p className="text-gray">Please be patient. This may take few seconds.</p>
                                                        </div>
                                                    </div> :
                                                    <AddNewButton
                                                        text1="No debug data available"
                                                        text2={this.state.deviceInfo.isConnected ? "Please start debug to view data." : "Device is not online."}
                                                        imageIcon="noData.png"
                                                        addButtonEnable={false}
                                                    />
                                            }
                                        </div>
                                    </div>
                                    }
                                </div>
                            </div>
                            :
                            <div className="content-body">
                                <AddNewButton
                                    text1="No device type(s) available"
                                    imageIcon="addDeviceDiagnostics.png"
                                    text2="You haven't created any device type(s) yet. Please create a device type first"
                                    addButtonEnable={isNavAssigned("deviceTypes")}
                                    createItemOnAddButtonClick={() => this.props.history.push(`/addOrEditDeviceType`)}
                                />
                            </div>
                        }
                    </React.Fragment>
                }*/}
                
                {this.state.isOpen &&
                    <NotificationModal
                        type={this.state.modalType}
                        message2={this.state.message2}
                        onCloseHandler={this.onCloseHandler}
                    />
                }
            </React.Fragment>
        );
    }
}

DeviceDiagnostics.propTypes = {
    dispatch: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
    getConnectorsListSuccess: SELECTORS.getConnectorsListSuccess(),
    getConnectorsListFailure: SELECTORS.getConnectorsListError(),
    getDeviceInfoSuccess: SELECTORS.getDeviceInfoSuccess(),
    getDeviceInfoFailure: SELECTORS.getDeviceInfoFailure(),
    searchByIdOrNameSuccess: SELECTORS.getSearchByIdOrNameSuccess(),
    SearchByIdOrNameFailure: SELECTORS.getSearchByIdOrNameFailure(),
    mqttCredentialsSuccess: SELECTORS.mqttCredentialsSuccess(),
    mqttCredentialsFailure: SELECTORS.mqttCredentialsFailure(),
});

function mapDispatchToProps(dispatch) {
    return {
        dispatch,
        getConnectorsListByCategory: (category) => dispatch(ACTIONS.getConnectorsListByCategory(category)),
        getDeviceInformation: (connectorId, deviceId) => dispatch(ACTIONS.getDeviceInformation(connectorId, deviceId)),
        searchByIdOrName: (payload, deviceId) => dispatch(ACTIONS.searchByIdOrName(payload, deviceId)),
        getMQTTCredentials: (deviceId) => dispatch(ACTIONS.getMQTTCredentials(deviceId)),
    };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "deviceDiagnostics", reducer });
const withSaga = injectSaga({ key: "deviceDiagnostics", saga });


export default compose(
    withConnect,
    withReducer,
    withSaga,
)(DeviceDiagnostics);
