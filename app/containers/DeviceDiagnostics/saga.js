/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

// import { take, call, put, select } from 'redux-saga/effects';
import { apiCallHandler } from '../../api';
import { takeEvery ,takeLatest} from 'redux-saga';
import * as CONSTANTS from "./constants";

export function* getDeviceInformations(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_DEVICE_INFO_SUCCESS, CONSTANTS.GET_DEVICE_INFO_ERROR, 'getDeviceInfo')];
}

export function* watcherGetDeviceInfo() {
  yield takeEvery(CONSTANTS.GET_DEVICE_INFO, getDeviceInformations);
}

export function* mqttConnectorListHandlerAsync(action) {
  yield[apiCallHandler(action, CONSTANTS.MQTT_CONNECTOR_SUCCESS, CONSTANTS.MQTT_CONNECTOR_FAILURE, 'diagnosticDeviceList')];
}


export function* watcherMqttConnectorList() {
  yield takeEvery(CONSTANTS.MQTT_CONNECTOR, mqttConnectorListHandlerAsync);
}

export function* suggestionSearchHandlerAsync(action) {
  yield[apiCallHandler(action, CONSTANTS.SEARCH_BY_ID_NAME_SUCCESS, CONSTANTS.SEARCH_BY_ID_NAME_FAILURE, 'searchByIdOrNameDeviceType')];
}


export function* watcherGetSuggestionByIdOrName() {
  yield takeLatest(CONSTANTS.SEARCH_BY_ID_NAME, suggestionSearchHandlerAsync);
}

export function* getMQTTCredentialsHandlerAsync(action) {
  yield[apiCallHandler(action, CONSTANTS.GET_MQTT_CREDENTIALS_SUCCESS, CONSTANTS.GET_MQTT_CREDENTIALS_FAILURE, 'getMQTTCredentials')];
}


export function* watcherGetMQTTCredentials() {
  yield takeLatest(CONSTANTS.GET_MQTT_CREDENTIALS, getMQTTCredentialsHandlerAsync);
}

export default function* rootSaga() {
  yield [
      watcherGetDeviceInfo(),
      watcherMqttConnectorList(),
      watcherGetSuggestionByIdOrName(),
      watcherGetMQTTCredentials(),
  ];
}
