/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/*
 *
 * DeviceDiagnostics reducer
 *
 */

import { fromJS } from "immutable";
import * as CONSTANTS from "./constants";

export const initialState = fromJS({});

function deviceDiagnosticsReducer(state = initialState, action) {
  switch (action.type) {
    case CONSTANTS.GET_DEVICE_INFO_SUCCESS:
      return Object.assign({}, state, {
        'getDeviceInfoSuccess': action.response,
      })
    case CONSTANTS.GET_DEVICE_INFO_ERROR:
      return Object.assign({}, state, {
        'getDeviceInfoFailure': action.error,
      })
    case CONSTANTS.MQTT_CONNECTOR_SUCCESS:
      return Object.assign({}, state, {
        getConnectorsListSuccess: action.response,
      });

    case CONSTANTS.MQTT_CONNECTOR_FAILURE:
      return Object.assign({}, state, {
        getConnectorsListFailure: action.error,
      });
    case CONSTANTS.SEARCH_BY_ID_NAME_SUCCESS:
      return Object.assign({}, state, {
        getSearchByIdOrNameSuccess: action.response,
      });

    case CONSTANTS.SEARCH_BY_ID_NAME_FAILURE:
      return Object.assign({}, state, {
        getSearchByIdOrNameFailure: action.error,
      });
    case CONSTANTS.GET_MQTT_CREDENTIALS_SUCCESS:
      return Object.assign({}, state, {
        mqttCredentialsSuccess: action.response,
      });

    case CONSTANTS.GET_MQTT_CREDENTIALS_FAILURE:
      return Object.assign({}, state, {
        mqttCredentialsFailure: action.error,
      });
    case CONSTANTS.DEFAULT_ACTION:
      return state;

    default:
      return state;
  }
}

export default deviceDiagnosticsReducer;
