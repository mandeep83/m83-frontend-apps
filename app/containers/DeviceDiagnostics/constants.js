/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/*
 *
 * DeviceDiagnostics constants
 *
 */

export const DEFAULT_ACTION = "app/DeviceDiagnostics/DEFAULT_ACTION";

export const GET_DEVICE_INFO = 'app/DeviceDiagnostics/GET_DEVICE_INFO';
export const GET_DEVICE_INFO_SUCCESS = 'app/DeviceDiagnostics/GET_DEVICE_INFO_SUCCESS';
export const GET_DEVICE_INFO_ERROR = 'app/DeviceDiagnostics/GET_DEVICE_INFO_ERROR';

export const MQTT_CONNECTOR = "app/DeviceDiagnostics/MQTT_CONNECTOR";
export const MQTT_CONNECTOR_SUCCESS = "app/DeviceDiagnostics/MQTT_CONNECTOR_SUCCESS";
export const MQTT_CONNECTOR_FAILURE = "app/DeviceDiagnostics/MQTT_CONNECTOR_FAILURE";

export const SEARCH_BY_ID_NAME = "app/DeviceDiagnostics/SEARCH_BY_ID_NAME";
export const SEARCH_BY_ID_NAME_SUCCESS = "app/DeviceDiagnostics/SEARCH_BY_ID_NAME_SUCCESS";
export const SEARCH_BY_ID_NAME_FAILURE = "app/DeviceDiagnostics/SEARCH_BY_ID_NAME_FAILURE";

export const GET_MQTT_CREDENTIALS = "app/DeviceDiagnostics/GET_MQTT_CREDENTIALS";
export const GET_MQTT_CREDENTIALS_SUCCESS = "app/DeviceDiagnostics/GET_MQTT_CREDENTIALS_SUCCESS";
export const GET_MQTT_CREDENTIALS_FAILURE = "app/DeviceDiagnostics/GET_MQTT_CREDENTIALS_FAILURE";