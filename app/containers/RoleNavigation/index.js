/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { getRoleList } from './actions'
import injectSaga from 'utils/injectSaga';
import Loader from '../../components/Loader/Loadable'
import injectReducer from 'utils/injectReducer';
import { roleList, getRoleListFailure, getIsFetching } from './selectors'
import reducer from './reducer';
import saga from './saga';
import ReactTooltip from "react-tooltip";
import messages from './messages';

/* eslint-disable react/prefer-stateless-function */
export class RoleNavigation extends React.Component {

    componentDidMount() {
        this.props.getRoleList();
    }

    componentWillReceiveProps(nextProps) {

    }

    render() {

        let listHtml;
        if (this.props.roleList && this.props.roleList.length > 0) {
            listHtml = (
                <div className="contentTableBox">
                    <table className="table">
                        <thead>
                            <tr>
                                <th width="25%">Name</th>
                                <th width="25%">Type</th>
                                <th width="35%">Description</th>
                                <th width="15%">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.props.roleList.map((roleObject, index) => (
                                <tr key={index}>
                                    <td className="text-primary">{roleObject.name}</td>
                                    <td>{roleObject.type}</td>
                                    <td>{roleObject.description}</td>
                                    <td>
                                        <button
                                            className={roleObject.type == "SYSTEM_ROLE" ? "btn btn-transparent btn-disabled" : "btn btn-transparent text-primary"}
                                            disabled={roleObject.type == "SYSTEM_ROLE"} data-tip data-for="edit" onClick={() => {
                                                this.props.history.push('/addOrEditRoleNav/' + roleObject.id + "/" + roleObject.name)
                                            }}>
                                            <i className="far fa-pen" />
                                            <ReactTooltip id="edit" place="bottom" type="dark">
                                                <div className="tooltipText"><p>Edit</p></div>
                                            </ReactTooltip>

                                        </button>
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            )
        } else {
            listHtml =
                <div className="emptyMessage mt-5">
                    <p className="text-dark-gray">
                        <i className="fal fa-file-exclamation" />Sorry! no reports found.
                    </p>
                </div>
        }
        return (
            <div>
                <Helmet>
                    <title>Manage Role Navigation</title>
                    <meta name="description" content="IoT83 - Manage Role Navigation" />
                </Helmet>
                <div className="pageBreadcrumb">
                    <div className="row">
                        <div className="col-8">
                            <p><span>Manage Role Navigation</span></p>
                        </div>
                        <div className="col-4">
                        </div>
                    </div>
                </div>

                <div className="outerBox">
                    {this.props.isFetching ? <Loader /> : listHtml}
                </div>

            </div>);
    }
}

RoleNavigation.propTypes = {
    dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
    roleList: roleList(),
    getRoleListFailure: getRoleListFailure(),
    isFetching: getIsFetching()
});

function mapDispatchToProps(dispatch) {
    return {
        dispatch,
        getRoleList: () => dispatch(getRoleList()),
    };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'roleNavigation', reducer });
const withSaga = injectSaga({ key: 'roleNavigation', saga });

export default compose(
    withReducer,
    withSaga,
    withConnect,
)(RoleNavigation);
