/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the roleNavigation state domain
 */

const selectRoleNavigationDomain = state =>
  state.get('roleNavigation', initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by RoleNavigation
 */

export const roleList = () =>
createSelector(selectRoleNavigationDomain, substate => substate.roleList);

export const getRoleListFailure = () =>
  createSelector(selectRoleNavigationDomain, substate => substate.roleListFailure);

const isFetchingState = state => state.get('loader');

export const getIsFetching = () =>
      createSelector(isFetchingState, fetchingState => fetchingState.get('isFetching'));

export { selectRoleNavigationDomain };
