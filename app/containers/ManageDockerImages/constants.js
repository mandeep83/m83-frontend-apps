/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/*
 *
 * ManageDockerImages constants
 *
 */

export const GET_DOCKER_IMAGE = "app/ManageDockerImages/GET_DOCKER_IMAGE";
export const GET_DOCKER_IMAGE_SUCCESS = "app/ManageDockerImages/GET_DOCKER_IMAGE_SUCCESS";
export const GET_DOCKER_IMAGE_FAILURE = "app/ManageDockerImages/GET_DOCKER_IMAGE_FAILURE";

export const CREATE_OR_EDIT_DOCKER_IMAGE = "app/ManageDockerImages/CREATE_OR_EDIT_DOCKER_IMAGE";
export const CREATE_OR_EDIT_DOCKER_IMAGE_SUCCESS = "app/ManageDockerImages/CREATE_OR_EDIT_DOCKER_IMAGE_SUCCESS";
export const CREATE_OR_EDIT_DOCKER_IMAGE_FAILURE = "app/ManageDockerImages/CREATE_OR_EDIT_DOCKER_IMAGE_FAILURE";

export const DELETE_DOCKER_IMAGE = "app/ManageDockerImages/DELETE_DOCKER_IMAGE";
export const DELETE_DOCKER_IMAGE_SUCCESS = "app/ManageDockerImages/DELETE_DOCKER_IMAGE_SUCCESS";
export const DELETE_DOCKER_IMAGE_FAILURE = "app/ManageDockerImages/DELETE_DOCKER_IMAGE_FAILURE";

export const GET_DEPENDCY_LIST = "app/ManageDockerImages/GET_DEPENDCY_LIST";
export const GET_DEPENDCY_LIST_SUCCESS = "app/ManageDockerImages/GET_DEPENDCY_LIST_SUCCESS";
export const GET_DEPENDCY_LIST_FAILURE = "app/ManageDockerImages/GET_DEPENDCY_LIST_FAILURE";




