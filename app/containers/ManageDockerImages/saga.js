/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

// import { take, call, put, select } from 'redux-saga/effects';
import { apiCallHandler } from '../../api';
import { takeEvery } from 'redux-saga';
import * as CONSTANTS from './constants';


export function* getAllDockerImage(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_DOCKER_IMAGE_SUCCESS, CONSTANTS.GET_DOCKER_IMAGE_FAILURE, 'getDockerImage')];
}


export function* createOrEditDockerImage(action) {
  yield [apiCallHandler(action, CONSTANTS.CREATE_OR_EDIT_DOCKER_IMAGE_SUCCESS, CONSTANTS.CREATE_OR_EDIT_DOCKER_IMAGE_FAILURE, 'createOrEditDockerImage')];
}

export function* deleteDockerImage(action) {
  yield [apiCallHandler(action, CONSTANTS.DELETE_DOCKER_IMAGE_SUCCESS, CONSTANTS.DELETE_DOCKER_IMAGE_FAILURE, 'deleteDockerImage')];
}

export function* getDependcyList(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_DEPENDCY_LIST_SUCCESS, CONSTANTS.GET_DEPENDCY_LIST_FAILURE, 'getDependencyList')];
}

export function* watcherDockerImageList() {
  yield takeEvery(CONSTANTS.GET_DOCKER_IMAGE, getAllDockerImage);
}

export function* watcherCreateOrEditDockerImage() {
  yield takeEvery(CONSTANTS.CREATE_OR_EDIT_DOCKER_IMAGE, createOrEditDockerImage);
}

export function* watcherDeleteDockerImage() {
  yield takeEvery(CONSTANTS.DELETE_DOCKER_IMAGE, deleteDockerImage);
}

export function* watcherDependecyList() {
  yield takeEvery(CONSTANTS.GET_DEPENDCY_LIST, getDependcyList);
}

export default function* rootSaga() {
  yield [watcherDockerImageList(),
  watcherCreateOrEditDockerImage(),
  watcherDeleteDockerImage(),
  watcherDependecyList(),
  ];
}
