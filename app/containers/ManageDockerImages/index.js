/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * ManageDockerImages
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { FormattedMessage } from "react-intl";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import * as Selectors from "./selectors";
import reducer from "./reducer";
import * as Actions from "./actions";
import saga from "./saga";
import Select from "react-select";
import SlidingPane from "react-sliding-pane";
import messages from "./messages";
import cloneDeep from "lodash/cloneDeep";
import TagsInput from "react-tagsinput";
import NotificationModal from '../../components/NotificationModal/Loadable';
import Loader from "../../components/Loader";
import ReactTooltip from "react-tooltip";
import { getInitials } from '../../commonUtils';
import AddNewButton from "../../components/AddNewButton/Loadable";
import NoDataFoundMessage from "../../components/NoDataFoundMessage/Loadable";
import SearchBarWithSuggestion from "../../components/SearchBarWithSuggestion";
import ListingTable from "../../components/ListingTable/Loadable";
import QuotaHeader from "../../components/QuotaHeader/Loadable";
import { subscribeTopic, unsubscribeTopic } from "../../mqttConnection";
import Skeleton from "react-loading-skeleton";

/* eslint-disable react/prefer-stateless-function */
const STATUS_STATE = ["PENDING", "PUSHED", "FAILED"]
export class ManageDockerImages extends React.Component {
    state = {
        isDockerImageModal: false,
        toggleView: false,
        dockerImageList: [],
        isFetching: true,
        filteredList: [],
        isOpen: false,
        searchTerm: '',
        isConfirmModal: false,
        tag: '',
        isShowSuggestion: false,
        addOrEditInProgress: false,
        payload: {
            dependencies: [],
            image: "",
            description: "",
            tag: "",
            interpreter: "python:3",
            dependencySearch: ''
        },
        interpreters: [
            {
                value: "python:3",
                label: "Python 3",
                isDisable: false,
                fileType: "python",
            },
            {
                value: "nodejs:12",
                label: "NodeJS 12",
                isDisable: false,
                fileType: "javascript",
            },
            {
                value: "swift:4.2",
                label: "Swift 4.2",
                isDisable: true,
                fileType: "swift",
            },
            {
                value: "go:1.11",
                label: "Golang 1.11",
                isDisable: true,
                fileType: "golang",
            },
            {
                value: "ruby:2.5",
                label: "Ruby 2.5",
                isDisable: true,
                fileType: "ruby",
            },
            {
                value: "php:7.4",
                label: "PHP 7.4",
                isDisable: true,
                fileType: "php",
            }

        ],
        searchInterpreter: "All",
        isSearchFetching: false
    }

    qoutaCallback = () => {
        this.props.getDockerImage(this.state.searchInterpreter);
        document.addEventListener('click', this.handleClickOutside);
    }

    componentWillUnmount() {
        document.removeEventListener('click', this.handleClickOutside);
    }

    myRef = React.createRef();

    mqttMessageHandler = (message) => {
        let dockerImageList = cloneDeep(this.state.dockerImageList);
        let filteredList = cloneDeep(this.state.filteredList);
        new Promise((resolve, reject) => {
            dockerImageList.map(val => {
                if (val.id === JSON.parse(JSON.stringify(message)).imageId) {
                    val.status = JSON.parse(JSON.stringify(message)).status
                }
            })
            filteredList.map(val => {
                if (val.id === JSON.parse(JSON.stringify(message)).imageId) {
                    val.status = JSON.parse(JSON.stringify(message)).status
                }
            })
            resolve();
        }).then(() => {
            this.setState({
                dockerImageList,
                filteredList
            })
        })
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.getAllDockerImage && nextProps.getAllDockerImage !== this.props.getAllDockerImage) {
            let dockerImageList = nextProps.getAllDockerImage.filter(dockerImage => !dockerImage.default);
            dockerImageList.map(list => {
                list.interpreterDisplayName = this.state.interpreters.find(interpreter => interpreter.value === list.interpreter).label
            })
            let filteredList = dockerImageList;
            this.setState({
                dockerImageList,
                filteredList,
                isFetching: false
            }, () => {
                unsubscribeTopic("dockerImage");
                subscribeTopic("dockerImage", this.mqttMessageHandler);
            })
        }

        if (nextProps.getAllDockerImageError && nextProps.getAllDockerImageError !== this.props.getAllDockerImageError) {
            this.setState({
                isFetching: false,
                isOpen: true,
                message2: nextProps.getAllDockerImageError.response,
                modalType: "error"
            })
        }

        if (nextProps.createOrEditDockerImages && nextProps.createOrEditDockerImages !== this.props.createOrEditDockerImages) {
            let isAddSuccess = this.state.isAddSuccess
            if (!this.state.indexToBeEdited) {
                isAddSuccess = !isAddSuccess
            }
            this.setState({
                modalType: "success",
                isFetching: true,
                isAddSuccess,
                isOpen: true,
                isDockerImageModal: false,
                addOrEditInProgress: false,
                message2: nextProps.createOrEditDockerImages.response.message
            }, () => this.props.getDockerImage(this.state.searchInterpreter))
        }

        if (nextProps.createOrEditDockerImageError && nextProps.createOrEditDockerImageError !== this.props.createOrEditDockerImageError) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.createOrEditDockerImageError.response,
                isFetching: false,
                addOrEditInProgress: false,
                isDockerImageModal: true
            })
        }
        if (nextProps.deleteDockerImages && nextProps.deleteDockerImages !== this.props.deleteDockerImages) {
            let filteredList = cloneDeep(this.state.dockerImageList);
            filteredList = filteredList.filter(temp => temp.id !== this.state.accountToBeDeleted)
            this.setState((previousState) => ({
                filteredList,
                dockerImageList: filteredList,
                modalType: "success",
                isOpen: true,
                message2: nextProps.deleteDockerImages.response.message,
                isFetching: false,
                isDeletedSuccess: !previousState.isDeletedSuccess
            }))
        }

        if (nextProps.deleteDockerImagesError && nextProps.deleteDockerImagesError !== this.props.deleteDockerImagesError) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.deleteDockerImagesError.response,
                isFetching: false,
            })
        }

        if (nextProps.getDependencyLists && nextProps.getDependencyLists !== this.props.getDependencyList) {
            let sortedList = nextProps.getDependencyLists.response.sort((a, b) => {
                if (a.name < b.name)
                    return -1;
                else if (a.name > b.name)
                    return 1;
                else
                    return 0;
            })
            this.setState({
                dependencyList: sortedList,
                isFetching: false,
                isShowSuggestion: true,
                isSearchFetching: false
            })
        }

        if (nextProps.getDependencyListsError && nextProps.getDependencyListsError !== this.props.getDependencyListsError) {
            this.setState({
                isFetching: false,
                isOpen: true,
                message2: nextProps.getDependencyListsError.response,
                modalType: "error",
                isSearchFetching: false,
                dependencyList: []
            })
        }
    }


    onChangeInputHandler = ({ currentTarget }) => {
        let payload = cloneDeep(this.state.payload),
            searchInterpreter = this.state.searchInterpreter,
            id = currentTarget.id,
            value = currentTarget.value;

        if (id == "image") {
            if (/^[a-z0-9_]+$/.test(value) || /^$/.test(value))
                payload[id] = value;
        } else {
            payload[id] = value;
        }

        if (id == "searchInterpreter") {
            searchInterpreter = value;
            this.setState({
                isFetching: true
            }, () => this.props.getDockerImage(searchInterpreter))
        }
        this.setState({ payload, searchInterpreter, errorMessage: 0 })
    }
    reactSelectInputHandler = (valueObject) => {
        let payload = cloneDeep(this.state.payload)
        payload.interpreter = valueObject.value
        payload.dependencies = []

        this.setState({
            payload
        })
    }

    tagsInputHandler = (tags) => {
        let payload = cloneDeep(this.state.payload);
        payload.dependencies = tags
        this.setState({ payload, isShowSuggestion: true })
    }

    createDockerImageHandler = (e) => {
        e.preventDefault();
        let payload = cloneDeep(this.state.payload);
        if (payload.tag == "latest") {
            return (
                this.setState({
                    isOpen: true,
                    message2: `Image tag value should not be "latest", it's a reserve keyword !`,
                    modalType: "error"
                })
            )
        }
        if (this.state.payload.dependencies.length) {
            delete payload.dependencySearch;
            if (this.state.indexToBeEdited) {
                this.setState({ addOrEditInProgress: true }, () => this.props.createOrEditDockerImage(payload, this.state.indexToBeEdited))
            }
            else {
                this.setState({ addOrEditInProgress: true }, () => this.props.createOrEditDockerImage(payload))
            }
        } else {
            this.setState({
                errorMessage: 1
            })
        }
    }

    editDockerImageHandler = (index) => {
        let editedObj = this.state.dockerImageList.find(e => e.id == index)
        let payload = cloneDeep(this.state.payload);
        payload.dependencies = editedObj.dependencies;
        payload.image = editedObj.image;
        payload.description = editedObj.description;
        payload.tag = editedObj.tag;
        payload.interpreter = editedObj.interpreter;
        this.setState({ isDockerImageModal: true, payload, indexToBeEdited: index })
    }

    addDockerImage = () => {
        let payload = cloneDeep(this.state.payload);
        payload.dependencies = [];
        payload.image = '';
        payload.description = '';
        payload.tag = '';
        payload.interpreter = 'python:3';
        this.setState({
            isDockerImageModal: true,
            payload,
            indexToBeEdited: '',
            dependencyList: [],
            tag: '',
            errorMessage: 0,
        })
    }

    onCloseHandler = () => {
        this.setState({
            isOpen: false,
            message2: '',
            modalType: "",
        })
    }

    searchHandler = (value) => {
        let filteredList = cloneDeep(this.state.dockerImageList);
        if (value.length > 0) {
            filteredList = filteredList.filter((item) => item.image.toLowerCase().includes(`${value.toLowerCase()}`));
        }
        this.setState({ searchTerm: value, filteredList })
    }

    confirmModalHandler = (id) => {
        this.setState({
            accountToBeDeleted: id,
            isConfirmModal: true
        })
    }

    addDependency = (name) => {
        let payload = cloneDeep(this.state.payload);
        payload.dependencySearch = '';
        payload.dependencies.push(name)
        this.setState({ payload, dependencyList: [], tag: '', errorMessage: 0 })
    }

    handleClickOutside = e => {
        if (this.myRef.current && !this.myRef.current.contains(e.target)) {
            this.setState({ isShowSuggestion: false, dependencyList: [], tag: '' });
        }
    };

    handleClickInside = () => this.setState({ isShowSuggestion: true });

    toggleView = () => {
        this.setState((prevState) => ({ toggleView: !prevState.toggleView }))
    }

    searchDependenciesHandler = () => {
        this.setState({ isSearchFetching: true }, () => this.props.getDependencyList(this.state.payload.dependencySearch, this.state.payload.interpreter))
    }

    getColumns = () => {
        return [
            {
                Header: "Name",
                accessor: "displayName",
                width: 25,
                cell: (row) => (
                    <React.Fragment>
                        <div className="list-view-icon">
                            <i className="fab fa-docker text-dark-theme"></i>
                        </div>
                        <div className="list-view-icon-box">
                            <h6 className="text-theme fw-600">{row.displayName}</h6>
                            <p>{`${row.description || "-"}`}</p>
                        </div>
                    </React.Fragment>
                ),
                filterable: true,
                sortable: true
            },
            {
                Header: "Status",
                width: 10,
                accessor: "status",
                filterable: true,
                sortable: true,
                cell: (row) => (
                    <h6 className={row.status === "PUSHED" ? "text-green" : "text-red"}>{row.status}</h6>
                ),
                filter: ({ value, onChangeHandler, accessor }) =>
                    <select
                        className="form-control"
                        onChange={event => onChangeHandler(event, accessor)}
                        value={value ? value : 'all'}
                    >
                        <option value="">ALL</option>
                        {STATUS_STATE.map(val => <option value={val}>{val}</option>)}
                    </select>
            },
            {
                Header: "Image Tag",
                width: 15,
                accessor: "tag",
                sortable: true,
                filterable: true
            },
            {
                Header: "Interpreter",
                width: 10,
                accessor: "interpreterDisplayName",
                filterable: true,
                sortable: true,
                cell: (row) => (
                    <span className="badge badge-pill alert-success list-view-pill w-40">{row.interpreterDisplayName}</span>
                ),
                filter: ({ value, onChangeHandler, accessor }) =>
                    <select
                        className="form-control"
                        onChange={event => onChangeHandler(event, accessor)}
                        value={value ? value : 'all'}
                    >
                        <option value="">ALL</option>
                        {this.state.interpreters.map((list, index) => <option key={index} value={list.label} disabled={list.isDisable}>{list.label}</option>)}
                    </select>

            },
            { Header: "Created", width: 15, accessor: "createdAt", sortable: true, filterable: true },
            { Header: "Updated", width: 15, accessor: "updatedAt", sortable: true, filterable: true },
            {
                Header: "Actions", width: 10,
                cell: (row) => (
                    <div className="button-group">
                        <button className="btn-transparent btn-transparent-blue" disabled={row.status === "PENDING"} data-tooltip data-tooltip-text="Edit" data-tooltip-place="bottom" onClick={() => this.editDockerImageHandler(row.id)}>
                            <i className="far fa-pencil"></i>
                        </button>
                        <button className="btn-transparent btn-transparent-red" disabled={row.status === "PENDING"} data-tooltip data-tooltip-text="Delete" data-tooltip-place="bottom" onClick={() => this.confirmModalHandler(row.id)}>
                            <i className="far fa-trash-alt"></i>
                        </button>
                    </div>
                )
            },
        ]
    }

    getStatusBarClass = (row) => {
        if (row.status === "PUSHED")
            return { className: "green", tooltipText: "Pushed" }
        return { className: "red", tooltipText: "Not Pushed" }
    }

    refreshComponent = () => {
        this.setState({
            isFetching: true
        }, () => this.props.getDockerImage(this.state.searchInterpreter))
    }

    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>Docker Images</title>
                    <meta name="description" content="M83-ManageDockerImages" />
                </Helmet>

                <QuotaHeader
                    showQuota
                    addButtonText="Add Docker Image"
                    componentName="Docker Images"
                    productName="docker_images"
                    onAddClick={this.addDockerImage}
                    toggleButton={this.toggleView}
                    // searchBoxDetails={{
                    //     value: this.state.searchTerm,
                    //     onChangeHandler: this.searchHandler,
                    //     isDisabled: !this.state.dockerImageList.length || this.state.isFetching
                    // }}
                    refreshDisabled={this.state.isFetching}
                    addDisabled={this.state.isFetching}
                    refreshHandler={this.refreshComponent}
                    callBack={this.qoutaCallback}
                    isAddSuccess={this.state.isAddSuccess}
                    isDeletedSuccess={this.state.isDeletedSuccess}
                    {...this.props}
                />

                <div className="content-body">
                    {this.state.isFetching ?
                        <Skeleton count={9} className="skeleton-list-loader" /> :
                        <React.Fragment>
                            {this.state.dockerImageList.length > 0 ?
                                !this.state.toggleView ?
                                    <ListingTable
                                        data={this.state.filteredList}
                                        columns={this.getColumns()}
                                        statusBar={this.getStatusBarClass}
                                        isLoading={this.state.isFetching}
                                    />
                                    :
                                    <ul className="card-view-list">
                                        {this.state.dockerImageList.map((item, index) =>
                                            <li key={index}>
                                                <div className="card-view-box">
                                                    <div className="card-view-header">
                                                        <span data-tooltip data-tooltip-text={item.status === "PUSHED" ? "PUSHED" : "FAILED"} data-tooltip-place="bottom" className={item.status === "PUSHED" ? "card-view-header-status bg-success" : "card-view-header-status bg-danger"}></span>
                                                        {item.status === "PUSHED"  ?
                                                            <span className="alert alert-success mr-r-7">PUSHED</span>
                                                            :
                                                            <span className="alert alert-danger mr-r-7">FAILED</span>
                                                        }
                                                        <span className="alert alert-primary"><strong>Interpreter - </strong>{item.interpreterDisplayName}</span>
                                                        <div className="dropdown">
                                                            <button className="btn-transparent btn-transparent-gray" type="button" data-toggle="dropdown">
                                                                <i className="fas fa-ellipsis-v"></i>
                                                            </button>
                                                            <div className="dropdown-menu">
                                                                <button className="dropdown-item" disabled={item.status === "PENDING"} onClick={() => this.editDockerImageHandler(item.id)}>
                                                                    <i className="far fa-pencil"></i>Edit
                                                                </button>
                                                                <button className="dropdown-item" disabled={item.status === "PENDING"}  onClick={() => this.confirmModalHandler(item.id)}>
                                                                    <i className="far fa-trash-alt"></i>Delete
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="card-view-body">
                                                        <div className="card-view-icon">
                                                            <i className="fab fa-docker text-dark-theme"></i>
                                                        </div>
                                                        <h6><i className="far fa-address-book"></i>{item.displayName ? item.displayName : "-"}</h6>
                                                        <p><i className="far fa-file-alt"></i>{item.description ? item.description : "-"}</p>
                                                        <p><span className="badge badge-pill badge-light">Image Tag - {item.tag}</span></p>
                                                    </div>
                                                    <div className="card-view-footer d-flex">
                                                        <div className="flex-50 p-3">
                                                            <h6>Created</h6>
                                                            <p><strong>By - </strong>{item.createdBy}<small></small></p>
                                                            <p><strong>At - </strong>{new Date(item.createdAt).toLocaleString('en-US', { createdAt: localStorage.createdAt })}</p>
                                                        </div>
                                                        <div className="flex-50 p-3">
                                                            <h6>Updated</h6>
                                                            <p><strong>By - </strong>{item.updatedBy}<small></small></p>
                                                            <p><strong>At - </strong>{new Date(item.updatedAt).toLocaleString('en-US', { createdAt: localStorage.createdAt })}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        )}
                                    </ul>
                                :
                                <AddNewButton
                                    text1="No Docker Image(s) available"
                                    text2="You haven't created any Docker Images yet. Please create a Docker Image first."
                                    imageIcon="addDockerImage.png"
                                    addButtonEnable={true}
                                    createItemOnAddButtonClick={this.addDockerImage}
                                />
                            }
                        </React.Fragment>
                    }
                </div>

                {/* add docker image modal */}
                <SlidingPane
                    className=''
                    overlayClassName='sliding-form'
                    closeIcon={<div></div>}
                    isOpen={this.state.isDockerImageModal || false}
                    from='right'
                    width='500px'
                    ariaHideApp={false}
                >
                    <div className="modal-content">
                        <div className="modal-header">
                            <h6 className="modal-title">{this.state.indexToBeEdited ? "Update" : "Add New Docker Image"}
                                <button className="close" onClick={() => this.setState({ isDockerImageModal: false, indexToBeEdited: '' })}><i className="fas fa-angle-right"></i></button>
                            </h6>
                        </div>

                        {this.state.addOrEditInProgress ?
                            <div className="modal-loader">
                                <i className="fad fa-sync-alt fa-spin"></i>
                            </div> :

                            <form onSubmit={this.createDockerImageHandler}>
                                <div className="modal-body">
                                    <div className="alert alert-info note-text">
                                        <p>Image Name shouldn't have Capital letter, No space and No special character</p>
                                    </div>

                                    <div className="form-group">
                                        <label className="form-group-label">Image Name :<i className="fas fa-asterisk form-group-required"></i></label>
                                        <input id="image" className="form-control" value={this.state.payload.image} required onChange={this.onChangeInputHandler} readOnly={this.state.indexToBeEdited} />
                                    </div>

                                    <div className="form-group">
                                        <label className="form-group-label">Image Tag :<i className="fas fa-asterisk form-group-required"></i></label>
                                        <input id="tag" className="form-control" value={this.state.payload.tag} required onChange={this.onChangeInputHandler} readOnly={this.state.indexToBeEdited} />
                                    </div>

                                    <div className="form-group">
                                        <label className="form-group-label">Description :</label>
                                        <textarea rows="3" id="description" name="description" className="form-control" value={this.state.payload.description} onChange={this.onChangeInputHandler} />
                                    </div>

                                    {/*<div className="form-group">
                                        <label className="form-group-label">Search Dependencies :</label>
                                        <div className="search-wrapper">
                                            <div className="search-wrapper-group">
                                                <div className="input-group">
                                                    <input id="dependencySearch" name="dependencySearch" className="form-control" value={this.state.payload.dependencySearch} onChange={this.onChangeInputHandler} />
                                                    <span className="search-wrapper-icon"><i className="far fa-search"></i></span>
                                                    <div className="input-group-append">
                                                        <button type="button" className="btn btn-primary" disabled={this.state.payload.dependencySearch.length == 0} onClick={() => this.searchDependenciesHandler()}>Search</button>
                                                    </div>
                                                </div>
                                            </div>
                                            {this.state.isShowSuggestion &&
                                                <div className="search-wrapper-suggestion">
                                                    <ul className="list-style-none" ref={this.myRef} onClick={this.handleClickInside}>
                                                        {this.state.dependencyList && this.state.dependencyList.map((val, index) =>
                                                            <li key={index} value={val.name} onClick={() => this.addDependency(val.name)}>{val.name}</li>
                                                        )}
                                                    </ul>
                                                </div>
                                            }
                                        </div>
                                    </div>*/}

                                    {/*{this.state.isSearchFetching &&
                                        <div className="alert alert-info note-text">
                                            <p>Please wait while we search for libraries <i className="fas fa-spinner fa-spin"></i></p>
                                        </div>
                                    }*/}

                                    <div className="form-group">
                                        <label className="form-group-label">Interpreter : <i className="fas fa-asterisk form-group-required"></i></label>
                                        <Select
                                            id="interpreter"
                                            className="form-control-multi-select"
                                            value={this.state.interpreters.find(temp => temp.value == this.state.payload.interpreter)}
                                            onChange={this.reactSelectInputHandler}
                                            options={this.state.interpreters}
                                            isOptionDisabled={(option) => option.isDisable}
                                            isDisabled={this.state.indexToBeEdited}
                                        />
                                    </div>

                                    <div className="form-group">
                                        <label className="form-group-label">Dependencies (Please provide exact names) : <i className="fas fa-asterisk form-group-required"></i></label>
                                        <TagsInput value={this.state.payload.dependencies} required addKeys="[13]" onChange={this.tagsInputHandler} inputValue={this.state.tag} onChangeInput={this.handleChangeInput} />
                                        {this.state.errorMessage == 1 && <span className="form-group-error">Please fill out this field.</span>}
                                    </div>

                                </div>

                                <div className="modal-footer justify-content-start">
                                    <button className="btn btn-primary">{this.state.indexToBeEdited ? "Update" : "Create"}</button>
                                    <button type="button" className="btn btn-dark" onClick={() => this.setState({ isDockerImageModal: false, indexToBeEdited: '', dependencyList: [], tag: '' })}>Cancel</button>
                                </div>
                            </form>
                        }
                    </div>
                </SlidingPane>
                {/* end add docker image modal */}

                {this.state.isOpen &&
                    <NotificationModal
                        type={this.state.modalType}
                        message2={this.state.message2}
                        onCloseHandler={this.onCloseHandler}
                    />
                }

                {/* delete docker image modal */}
                {this.state.isConfirmModal &&
                    <div className="modal show d-block animated slideInDown" id="deleteModal">
                        <div className="modal-dialog modal-dialog-centered">
                            <div className="modal-content">
                                <div className="modal-body">
                                    <div className="delete-content">
                                        <div className="delete-icon">
                                            <i className="fad fa-trash-alt text-red"></i>
                                        </div>
                                        <h4>Delete!</h4>
                                        <h6>Are you sure you want to delete <b>{this.state.dockerImageList.filter(dockerImage => dockerImage.id === this.state.accountToBeDeleted)[0]['displayName']}</b> ?</h6>
                                    </div>
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-dark" data-dismiss="modal" onClick={() => this.setState({ accountToBeDeleted: '', isConfirmModal: false })}>No</button>
                                    <button type="button" className="btn btn-success" data-dismiss="modal" onClick={() => this.setState({ isConfirmModal: false, isFetching: true }, () => this.props.deleteDockerImage(this.state.accountToBeDeleted))}>Yes</button>
                                </div>
                            </div>
                        </div>
                    </div>
                }
                {/* end delete docker image modal */}
            </React.Fragment>
        );
    }
}

ManageDockerImages.propTypes = {
    dispatch: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
    getAllDockerImage: Selectors.getAllDockerImage(),
    getAllDockerImageError: Selectors.getAllDockerImageError(),
    createOrEditDockerImages: Selectors.createOrEditDockerImages(),
    createOrEditDockerImageError: Selectors.createOrEditDockerImageError(),
    deleteDockerImages: Selectors.deleteDockerImages(),
    deleteDockerImagesError: Selectors.deleteDockerImagesError(),
    getDependencyLists: Selectors.getDependencyLists(),
    getDependencyListsError: Selectors.getDependencyListsError()
});

function mapDispatchToProps(dispatch) {
    return {
        dispatch,
        getDockerImage: (interpreter) => dispatch(Actions.getDockerImage(interpreter)),
        createOrEditDockerImage: (payload, id) => dispatch(Actions.createOrEditDockerImage(payload, id)),
        deleteDockerImage: (id) => dispatch(Actions.deleteDockerImage(id)),
        getDependencyList: (term, interpreter) => dispatch(Actions.getDependencyList(term, interpreter))
    };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "manageDockerImages", reducer });
const withSaga = injectSaga({ key: "manageDockerImages", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(ManageDockerImages);
