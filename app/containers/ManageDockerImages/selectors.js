/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
 * Direct selector to the manageDockerImages state domain
 */

const selectManageDockerImagesDomain = state =>
    state.get("manageDockerImages", initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by ManageDockerImages
 */

export const getAllDockerImage = () =>
    createSelector(selectManageDockerImagesDomain, subState => subState.getAllDockerImage);

export const getAllDockerImageError = () =>
    createSelector(selectManageDockerImagesDomain, subState => subState.getAllDockerImageError);

export const createOrEditDockerImages = () =>
    createSelector(selectManageDockerImagesDomain, subState => subState.createOrEditDockerImage);

export const createOrEditDockerImageError = () =>
    createSelector(selectManageDockerImagesDomain, subState => subState.createOrEditDockerImageError);

export const deleteDockerImages = () =>
    createSelector(selectManageDockerImagesDomain, subState => subState.deleteDockerImage);

export const deleteDockerImagesError = () =>
    createSelector(selectManageDockerImagesDomain, subState => subState.deleteDockerImageError);

export const getDependencyLists = () =>
    createSelector(selectManageDockerImagesDomain, subState => subState.getDependencyList);

export const getDependencyListsError = () =>
    createSelector(selectManageDockerImagesDomain, subState => subState.getDependencyListError);
