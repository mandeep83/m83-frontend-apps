/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/*
 *
 * ManageDockerImages actions
 *
 */
import * as CONSTANTS from "./constants";

export function getDockerImage(interpreter) {
  return {
    type: CONSTANTS.GET_DOCKER_IMAGE,
    interpreter
  };
}

export function createOrEditDockerImage(payload, id) {
  return {
    type: CONSTANTS.CREATE_OR_EDIT_DOCKER_IMAGE,
    payload,
    id
  };
}

export function deleteDockerImage(id) {
  return {
    type: CONSTANTS.DELETE_DOCKER_IMAGE,
    id
  };
}

export function getDependencyList(term, interpreter) {
  return {
    type: CONSTANTS.GET_DEPENDCY_LIST,
    term,
    interpreter
  };
}
