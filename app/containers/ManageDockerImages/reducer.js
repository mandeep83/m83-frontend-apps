/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/*
 *
 * ManageDockerImages reducer
 *
 */

import { fromJS } from "immutable";
import * as CONSTANTS from './constants';

export const initialState = fromJS({});

function manageDockerImagesReducer(state = initialState, action) {
  switch (action.type) {
    case CONSTANTS.GET_DOCKER_IMAGE_SUCCESS:
      return Object.assign({}, state, {
        'getAllDockerImage': action.response
      });
    case CONSTANTS.GET_DOCKER_IMAGE_FAILURE:
      return Object.assign({}, state, {
        'getAllDockerImageError': { response: action.error, timestamp: Date.now() }
      });
    case CONSTANTS.CREATE_OR_EDIT_DOCKER_IMAGE_SUCCESS:
      return Object.assign({}, state, {
        'createOrEditDockerImage': { timestamp: Date.now(), response: action.response }
      });
    case CONSTANTS.CREATE_OR_EDIT_DOCKER_IMAGE_FAILURE:
      return Object.assign({}, state, {
        'createOrEditDockerImageError': { response: action.error, timestamp: Date.now() }
      });

    case CONSTANTS.DELETE_DOCKER_IMAGE_SUCCESS:
      return Object.assign({}, state, {
        'deleteDockerImage': { timestamp: Date.now(), response: action.response}
      });
    case CONSTANTS.DELETE_DOCKER_IMAGE_FAILURE:
      return Object.assign({}, state, {
        'deleteDockerImageError': { response: action.error, timestamp: Date.now() }
      });

    case CONSTANTS.GET_DEPENDCY_LIST_SUCCESS:
      return Object.assign({}, state, {
        'getDependencyList': { timestamp: Date.now(), response: action.response }
      });
    case CONSTANTS.GET_DEPENDCY_LIST_FAILURE:
      return Object.assign({}, state, {
        'getDependencyListError': { response: action.error, timestamp: Date.now() }
      });
    default:
      return state;
  }
}

export default manageDockerImagesReducer;
