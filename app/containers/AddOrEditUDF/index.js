/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import AceEditor from "react-ace";
// import "ace-builds/src-noconflict/mode-python";
// import "ace-builds/src-noconflict/mode-javascript";
// import "ace-builds/src-noconflict/mode-r";
import "ace-builds/src-noconflict/theme-monokai";
import "ace-builds/src-noconflict/ext-language_tools"
import 'brace/mode/python';
import 'brace/mode/javascript';
import 'brace/mode/r';
// import 'brace/mode/json';
// import 'brace/theme/monokai';
// import 'brace/theme/github';
// import 'brace/ext/searchbox'
// import 'brace/ext/language_tools'
import ReactSelect from "react-select";

import {
    saveEtlFunctionSuccess,
    saveEtlFunctionFailure,
    getEtlFunctionSuccess,
    getEtlFunctionFailure,
    getAllEtlFunctionSuccess,
    getAllEtlFunctionFailure,
    getInvitationStatusSuccess,
    getInvitationStatusFailure,
    executeSuccess,
    executeFailure,
    libraryListSuccess,
    libraryListError
} from "./selectors";
import Loader from '../../components/Loader/Loadable'
import reducer from "./reducer";
import saga from "./saga";
import { saveEtlFunction, getEtlFunctionById, resetToInitialState, getAllEtlFunctions, getInvitationStatus, executeFunctionCode, getLibraryList } from "./actions";
import NotificationModal from '../../components/NotificationModal/Loadable'
import { Prompt } from 'react-router'
import MQTT from 'mqtt';
import ReactTooltip from 'react-tooltip'
import ReactDiffViewer from 'react-diff-viewer';

import { getVMQCredentials, isUsedForEnabled } from "../../commonUtils";
import { cloneDeep } from "lodash";
let codeSample = {
    javascript: `function(str) { \n const raw = atob(str);\n let result = '';\n for (let i = 0; i < raw.length; i++) { \n  const hex = raw.charCodeAt(i).toString(16);\n  result += (hex.length === 2 ? hex : '0' + hex);\n }\n return result.toLowerCase();\n}`,
    r: `new.function <- function(a,b) {
    result <- a * b + 1000
}
new.function(5,6)`,
    python: `def swapList(newList):\n    size = len(newList)\n    temp = newList[0]\n    newList[0] = newList[size - 1]\n    newList[size - 1] = temp\n    return newList\nnewList = [12, 35, 9, 56, 24]\nprint(swapList(newList))`,
    "node.js": `var wrappy = require('wrappy')\n\nvar once = wrappy(function (cb) {\n    var called = false\n    return function () {\n      if (called) return\n      called = true\n      return cb.apply(this, arguments)\n    }\n})\n\nfunction printBoo () {\n    console.log('Hello World')\n}\n\nprintBoo.iAmBooPrinter = true\n\nvar onlyPrintOnce = once(printBoo)\n\nonlyPrintOnce()\nonlyPrintOnce()`
}

let client;

export class AddOrEditUDF extends React.Component {
    state = {
        etlFunctions: [],
        saveSuccess: false,
        result: '',
        payload: {
            tenantName: localStorage.tenant,
            functionName: '',
            originalFunction: codeSample.python,
            description: '',
            type: "python"
        },
        copyCode: "",
        isFetching: true,
        editorModal: false,
        isaceEditorBox: true,
        showWarningMessage: false,
        commitMessageModal: false,
        commitMessage: "",
        isStatusChecked: false,
        showExecutionModal: false,
        responseReceived: undefined,
        rpcGenerated: false,
        libraryList: undefined,
        interpreterOptions: [
            {
                label: 'Python',
                value: 'python',
                isDisabled: false,
            },
            {
                label: 'Node.js',
                value: 'node.js',
                isDisabled: false,
            },
            {
                label: 'Javascript',
                value: 'javascript',
                isDisabled: false,
            }
        ],
    };

    componentWillMount() {
        this.props.resetToInitialState();
        if (this.props.match.params.id) {
            this.setState({
                isFetching: true,
            }, () => this.props.getEtlFunctionById(this.props.match.params.id))
        } else {
            this.props.getAllEtlFunctions()
            this.props.getLibraryList(this.state.payload.type)
        }

        if (this.isProjectSourceControlled) {
            this.props.getInvitationStatus(localStorage.getItem("selectedProjectId"))
        }
        this.getMqttConnection();
    }

    componentDidUpdate = () => {
        if (this.state.showWarningMessage) {
            window.onbeforeunload = () => true
        } else {
            window.onbeforeunload = undefined
        }
    }

    getMqttConnection = () => {
        let brokerURL = window.location.origin === "http://localhost:3001" ? window.API_URL.split("//")[1].substring(0, window.API_URL.split("//")[1].length - 1) : window.location.host,
            count = 0,
            credentials = getVMQCredentials();
        const options = {
            host: brokerURL,
            protocol: 'wss',
            path: '/connectmqtt',
            username: credentials.userName,
            password: credentials.password,
        };
        client = MQTT.connect(options);
        let _this = this;
        client.on("error", function (error) {
            _this.setState({
                mqttConnectionError: true
            })
            client.end();
        })
        client.on('close', function (response) {
            if (response && count > 4) {
                client.end();
            }
        });

        count++;
        client.on('reconnect', function () {
        });
    }

    subscribeTopic = () => {
        if(!this.state.mqttConnectionError){
            let _this = this;
            client.subscribe(`DEBUG_${localStorage.tenant}_${localStorage.selectedProjectId}_${_this.state.payload.functionName}`, function (err) {
                if (!err) {
                    client.on('message', function (topic, message) {
                        let responseReceived = []
                        responseReceived.push(message.toString())
                        _this.setState({ responseReceived,errorOnDebug: JSON.parse(message.toString()).hasError, rpcGenerated: !JSON.parse(message.toString()).hasError})
                    });
                }
            })
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.libraryListSuccess && nextProps.libraryListSuccess !== this.props.libraryListSuccess) {
            this.setState({
                libraryList: nextProps.libraryListSuccess
            })
        }

        if (nextProps.libraryListError && nextProps.libraryListError !== this.props.libraryListError) {
            this.setState({
                modalType: "error",
                isOpen: true,
                libraryList: [],
                message2: nextProps.libraryListError.message,
            })
        }

        if (nextProps.executeSuccess && nextProps.executeSuccess !== this.props.executeSuccess) {
            this.setState((prevState) => ({
                // rpcGenerated: true,
                copyCode: prevState.payload.originalFunction,
                showDebug: true
            }))
        }

        if (nextProps.executeFailure && nextProps.executeFailure !== this.props.executeFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.executeFailure
            })
        }

        if (nextProps.getAllEtlFunctionSuccess && nextProps.getAllEtlFunctionSuccess !== this.props.getAllEtlFunctionSuccess) {
            this.setState({
                etlFunctions: nextProps.getAllEtlFunctionSuccess,
                isFetching: false
            })
        }

        if (nextProps.saveEtlFunctionSuccess && nextProps.saveEtlFunctionSuccess !== this.props.saveEtlFunctionSuccess) {
            this.setState({
                modalType: "success",
                isOpen: true,
                message2: nextProps.saveEtlFunctionSuccess,
                saveSuccess: true,
                showWarningMessage: false,
                commitMessageModal: false,
                commitMessage: "",
            })
        }

        if (nextProps.saveEtlFunctionFailure && nextProps.saveEtlFunctionFailure !== this.props.saveEtlFunctionFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.saveEtlFunctionFailure.message,
                isFetching: false,
                commitMessageModal: false,
                commitMessage: "",
            })
        }

        if (nextProps.getEtlFunctionSuccess && nextProps.getEtlFunctionSuccess !== this.props.getEtlFunctionSuccess) {
            this.props.getAllEtlFunctions()
            this.setState({
                rpcGenerated: true,
                payload: nextProps.getEtlFunctionSuccess,
            }, () => this.props.getLibraryList(nextProps.getEtlFunctionSuccess.type))
        }

        if (nextProps.getEtlFunctionFailure && nextProps.getEtlFunctionFailure !== this.props.getEtlFunctionFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.getEtlFunctionFailure,
            })
        }

        if (nextProps.invitationStatusSuccess && nextProps.invitationStatusSuccess !== this.props.invitationStatusSuccess) {
            this.setState({
                invitationStatus: nextProps.invitationStatusSuccess.verifiedUser,
                isStatusChecked: true
            })
        }

        if (nextProps.invitationStatusFailure && nextProps.invitationStatusFailure !== this.props.invitationStatusFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.invitationStatusFailure,
            })
        }
    }

    onCloseHandler = () => {
        this.setState({
            isOpen: false,
            message2: '',
            modalType: '',
        }, () => {
            this.state.saveSuccess && this.navigateToJsFunctions();
        });
    }

    submitHandler = event => {
        event.preventDefault();
        let payload = { ...this.state.payload }
        this.setState({ isFetching: true, commitMessage: "", commitMessageModal: false, showDebug: false, mqttConnectionError: false });
        this.props.saveEtlFunction(payload, this.state.commitMessage);
    };

    onChangeHandler = (event) => {
        let payload = { ...this.state.payload };
        if ((event.target.id === "functionName" && /^[a-zA-Z]([\w]*[a-zA-Z0-9_])?$/.test(event.target.value) || /^$/.test(event.target.value)) || event.target.id === "description") {
            payload[event.target.id] = event.target.value;
        }
        this.setState({
            payload
        })
    }

    onChangeJsEditor = (value) => {
        let payload = { ...this.state.payload }, rpcGenerated = true
        payload.originalFunction = value;
        if (this.state.copyCode != value) {
            rpcGenerated = false
        }
        this.setState({
            payload,
            showWarningMessage: true,
            rpcGenerated,
            errorOnDebug: false
        })
    }

    executeETLJSFunction = () => {
        this.subscribeTopic();
        this.setState({
            showDebug: true
        }, () => {
            this.props.executeFunctionCode({
                functionName: this.state.payload.functionName,
                originalFunction: this.state.payload.originalFunction,
                type: this.state.payload.type
            })
        })
        // let result = eval(originalFunction);
        // if (typeof result !== 'undefined') {
        //     result = JSON.stringify(JSON.parse(result.toString()), undefined, 2);
        // }
        // this.setState({
        //     result,
        //     showExecutionModal: true
        // });
    }

    getSelectedInterpreter = () => {
        let interpreterOptions = this.state.interpreterOptions,
            selectedInterpreter = {
                label: interpreterOptions.find(interpreter => interpreter.value === this.state.payload.type).label,
                value: this.state.payload.type,
            };
        return selectedInterpreter;
    }

    changeGitDiff = (pullFromGit) => {
        let payload = JSON.parse(JSON.stringify(this.state.payload));
        if (pullFromGit)
            payload.originalFunction = this.state.payload.gitCode;
        payload.codeMismatch = false;
        this.setState({
            gitDiffModal: false,
            payload,
            modalType: "success",
            isOpen: pullFromGit,
            message2: "Code Updated",
            noRedirect: true
        });
    }

    isProjectSourceControlled = () => {
        let selectedProjectId = localStorage.getItem("selectedProjectId");
        let projectList = JSON.parse(localStorage.getItem("myProjects"));
        return projectList.some((project) => project.id === selectedProjectId && project.repoName)
    }

    isUserPermissionSourceControlled = () => {
        let selectedProjectId = localStorage.getItem("selectedProjectId");
        let projectList = JSON.parse(localStorage.getItem("myProjects"));
        return projectList.some((project) => project.id === selectedProjectId && project.sourceControlEnabled)
    }

    navigateToJsFunctions = () => {
        this.props.history.push({ pathname: '/udf', state: JSON.parse(JSON.stringify(this.props.location.state || null)) })
    }

    onInterpreterChangeHandler = (interpreter) => {
        let payload = { ...this.state.payload }, rpcGenerated = true
        payload.type = interpreter.value;
        payload.originalFunction = codeSample[interpreter.value];
        if (this.state.copyCode != codeSample[interpreter.value]) {
            rpcGenerated = false
        }
        this.setState({
            payload,
            rpcGenerated,
            errorOnDebug: false,
            libraryList:undefined
        },()=>this.props.getLibraryList(this.state.payload.type))
    }

    returnAceEditorHtml = () => {
        return (
            <div className="ace-editor h-100">
                <AceEditor
                    placeholder="Type code here..."
                    mode={this.state.payload.type === "node.js" ? "javascript" : this.state.payload.type}
                    theme="monokai"
                    name="ace_editor"
                    onChange={this.onChangeJsEditor}
                    value={this.state.payload.originalFunction}
                    showPrintMargin={true}
                    showGutter={true}
                    highlightActiveLine={true}
                    setOptions={{
                        enableBasicAutocompletion: true,
                        enableLiveAutocompletion: true,
                        enableSnippets: true,
                        showLineNumbers: true,
                        tabSize: 2,
                    }}
                    height="100%"
                    width="100%"
                />
            </div>
        )
    }

    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>Add/Edit UDFs</title>
                    <meta name="description" content="M83-AddOrEditUDFFunctions" />
                </Helmet>

                <Prompt when={this.state.showWarningMessage} message='Leave site? Changes you made may not be saved.' />

                <header className="content-header d-flex">
                    <div className="flex-60">
                        <div className="flex-60">
                            <div className="d-flex">
                                <h6 className="previous" onClick={this.navigateToJsFunctions}>UDFs</h6>
                                <h6 className="active">{this.props.match.params.id ? this.state.payload.functionName : 'Add New'}</h6>
                            </div>
                        </div>
                    </div>
                    <div className="flex-40 text-right">
                        <div className="content-header-group">
                            <button className="btn btn-light" data-tooltip data-tooltip-text="Back" data-tooltip-place="bottom" onClick={this.navigateToJsFunctions}><i className="fas fa-angle-left"></i></button>
                        </div>
                    </div>
                </header>

                <div className="content-body">
                    {this.state.isFetching ?
                        <Loader />
                        : <form onSubmit={this.submitHandler}>
                            <div className="form-content-wrapper form-content-wrapper-left">
                                <div className="form-content-box mb-0">
                                    <div className="form-content-header">
                                        <p>Definition
                                            {(this.props.match.params.id && this.state.payload.codeMismatch) &&
                                                <span className="form-content-error">Remote is ahead by 1 or more commits. Kindly take latest pull.</span>
                                            }
                                            {this.state.errorOnDebug && <span className="form-content-error">There is an error in code.</span>}
                                            <span className="text-cyan f-12 float-right"><sup>*</sup>UDF cannot be saved without debugging and ensuring that the function is Syntactically Correct.</span>
                                            {/* <button type="button" disabled={this.state.showDebug || this.state.payload.functionName == "" || this.state.payload.originalFunction == ""} className="btn btn-link" data-toggle="modal" data-target="#executeModal" onClick={() => this.executeETLJSFunction()}><i className="far fa-share"></i>Execute</button> */}
                                        </p>
                                    </div>

                                    <div className="form-content-body p-2">
                                        <div className="form-editor">
                                            {this.returnAceEditorHtml()}
                                            <button type="button" className="btn btn-light" onClick={() => { this.setState({ editorModal: true }) }} data-toggle="modal">
                                                <i className="fas fa-expand"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="form-info-wrapper form-info-wrapper-left">
                                <div className="form-info-header">
                                    <h5>Basic Information</h5>
                                </div>
                                <div className="form-info-body form-info-body-adjust pd-b-50">
                                    <div className="form-group">
                                        <label className="form-group-label">Name : <i className="fas fa-asterisk form-group-required"></i></label>
                                        <input type="text" className="form-control" id="functionName"
                                            autoFocus required value={(this.state.payload.functionName)} onChange={this.onChangeHandler} pattern="^[a-zA-Z0-9_]*$"
                                            disabled={this.props.match.params.id ? true : false || this.state.showDebug}
                                        />
                                    </div>

                                    <div className="form-group">
                                        <label className="form-group-label">Description :</label>
                                        <textarea rows="2" type="text" id="description" className="form-control"
                                            value={(this.state.payload.description)} onChange={this.onChangeHandler}>
                                        </textarea>
                                    </div>

                                    <div className="form-group">
                                        <label className="form-group-label">Interpreter : <i className="fas fa-asterisk form-group-required"></i></label>
                                        <ReactSelect
                                            className="form-control-multi-select"
                                            isMulti={false}
                                            isDisabled={this.state.showDebug || this.props.match.params.id}
                                            onChange={this.onInterpreterChangeHandler}
                                            options={this.state.interpreterOptions}
                                            value={this.getSelectedInterpreter()}
                                        >
                                        </ReactSelect>
                                    </div>
    
                                    <div className="form-group border border-radius-4 p-2">
                                        <label className="form-group-label">Supported Libraries For <b>{this.getSelectedInterpreter().label}</b> :</label>
                                        {this.state.libraryList ? this.state.libraryList.length > 0 ?
                                            <ul className="list-style-none form-info-list overflow-y-auto" style={{height: 125}}>
                                                {this.state.libraryList.map( (val,index) => (
                                                    <li key={index}><p>{val.name}</p></li>
                                                ))}
                                            </ul>
                                            :
                                            <div className="inner-message-wrapper" style={{height: 125}}>
                                                <div className="inner-message-content">
                                                    <i className="fad fa-file-exclamation"></i>
                                                    <h6>No Supported Libraries Found.</h6>
                                                </div>
                                            </div>
                                            :
                                            <div className="inner-loader-wrapper" style={{height: 125}}>
                                                <div className="inner-loader-content">
                                                    <i className="fad fa-sync-alt fa-spin"></i>
                                                </div>
                                            </div>
                                        }
                                    </div>
                                </div>

                                <div className="form-info-footer">
                                    {(isUsedForEnabled("js") && this.props.match.params.id && this.state.payload.codeMismatch && this.isProjectSourceControlled() && (localStorage.getItem('isGitHubLogin') == "true") && this.isUserPermissionSourceControlled()) &&
                                        <button type="button" className="btn btn-warning" onClick={() => this.setState({ gitDiffModal: true })} data-tip data-for="gitDiff">
                                            View Diff
                                    </button>
                                    }
                                    <button className="btn btn-light" onClick={this.navigateToJsFunctions} >Cancel</button>
                                    <button type="button" disabled={this.state.showDebug || this.state.payload.functionName == "" || this.state.payload.originalFunction == ""} className="btn btn-warning" onClick={() => this.executeETLJSFunction()}><i className="fad fa-bug mr-r-5"></i>Debug</button>
                                    {(this.isProjectSourceControlled() && isUsedForEnabled("js")) ?
                                        <button type="button" className="btn btn-primary" onClick={() => this.setState({ commitMessageModal: true })} disabled={(!(localStorage.getItem('isGitHubLogin') == "true") || (this.isProjectSourceControlled() && !this.isUserPermissionSourceControlled())) || this.state.payload.codeMismatch || !(this.state.payload.originalFunction.length > 0 && this.state.payload.functionName.length > 0) || (!this.state.rpcGenerated && this.state.errorOnDebug)}>
                                            {this.props.match.params.id ? "Update & Commit" : "Save & Commit"}
                                        </button> :
                                        <button type="submit" className="btn btn-primary" disabled={!(this.state.payload.originalFunction.length > 0 && this.state.payload.functionName.length > 0) || this.isProjectSourceControlled() && isUsedForEnabled("js") && !(localStorage.getItem('isGitHubLogin') == "true") || (!this.state.rpcGenerated || this.state.errorOnDebug)}>
                                            {this.props.match.params.id ? "Update" : "Save"}
                                        </button>
                                    }
                                </div>
                            </div>
                        </form>
                    }
                </div>

                {/* debug window */}
                {this.state.showDebug &&
                <div className="debug-wrapper debug-wrapper-right animated slideInUp">
                    <div className="debug-header">
                        <h6>Debug window</h6>
                        <div className="button-group">
                            {this.state.mqttConnectionError &&
                                <button type="button" onClick={() => this.setState({ mqttConnectionError: false }, () => this.getMqttConnection())} className="btn btn-light" data-tooltip data-tooltip-text="Connect MQTT" data-tooltip-place="bottom">
                                    <i class="fas fa-sync-alt"></i>
                                </button>
                            }
                            {this.state.responseReceived && this.state.responseReceived.length > 0 &&
                                <button type="button" onClick={() => { this.setState({ responseReceived: [] }) }} className="btn btn-light" data-tooltip data-tooltip-text="Clear" data-tooltip-place="bottom">
                                    <i className="far fa-broom"></i>
                                </button>
                            }
                            <button type="button" onClick={() => { this.setState({ showDebug: false, mqttConnectionError: false, responseReceived: undefined }) }} className="btn btn-light" data-tooltip data-tooltip-text="Close" data-tooltip-place="bottom">
                                <i className="far fa-times"></i>
                            </button>
                        </div>
                    </div>

                    <div className="debug-body">
                        {this.state.mqttConnectionError ?
                            <div className="inner-message-wrapper h-100">
                                <div className="inner-message-content">
                                    <i className="fad fa-exclamation-triangle text-red"></i>
                                    <h6 className='text-red'>There is an error while connecting with MQTT.</h6>
                                </div>
                            </div>
                            : this.state.responseReceived ? this.state.responseReceived.length > 0 ?
                                <ul className="list-style-none overflow-y-auto h-100">
                                    {this.state.responseReceived.map((msg, index) =>
                                        <li style={JSON.parse(msg).hasError ? {color:"red"} : {}} className={JSON.parse(msg).hasError ? "f-12 mb-2" : "text-content f-12 mb-2"} key={index}>{msg}</li>
                                    )}
                                </ul> :
                                <div className="inner-message-wrapper h-100">
                                    <div className="inner-message-content">
                                        <i className="fad fa-file-exclamation"></i>
                                        <h6>There is no data to display.</h6>
                                    </div>
                                </div>:
                                <div className="inner-loader-wrapper h-100">
                                    <div className="inner-loader-content">
                                        <h6>Debugging...</h6>
                                        <img src="https://content.iot83.com/m83/misc/uploading.gif" />
                                        <p className="text-gray">Please be patient. This may take few seconds.</p>
                                    </div>
                                </div>
                        }
                    </div>
                </div>
                }
                {/* end debug window */}
    
                {/* full screen modal */}
                {this.state.editorModal &&
                <div className="modal d-block animated slideInDown">
                    <div className="modal-dialog modal-full">
                        <div className="modal-content">
                            <div className="modal-body p-2">
                                <div className="modal-full-editor">
                                    {this.returnAceEditorHtml()}
                                    <button type="button" className="btn btn-light" onClick={() => this.setState({ editorModal: false })}><i className="fas fa-compress"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                }
                {/* end full screen modal */}

                {/* execute modal */}
                {this.state.showExecutionModal &&
                    <div className="modal d-block animated slideInDown" id="executeModal">
                        <div className="modal-dialog modal-lg">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h6 className="modal-title">Result/Output
                                        <button className="close" data-dismiss="modal" onClick={() => this.setState({ showExecutionModal: false })}><i className="far fa-times"></i></button>
                                    </h6>
                                </div>
                                <div className="modal-body">
                                    <div className="modal-editor">
                                        <AceEditor
                                            mode="json"
                                            theme="github"
                                            name="ace_editor"
                                            value={this.state.result}
                                            fontSize={13}
                                            height="100%"
                                            width="100%"
                                        />
                                    </div>
                                </div>
                                <div className="modal-footer">
                                    <button className="btn btn-success" data-dismiss="modal" onClick={() => this.setState({ showExecutionModal: false })}>OK</button>
                                </div>
                            </div>
                        </div>
                    </div>
                }
                {/* end execute modal */}

                {/* git commit modal */}
                {this.state.commitMessageModal &&
                    <div className="modal d-block animated slideInDown">
                        <div className="modal-dialog modal-lg modal-dialog-centered">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h6 className="modal-title">Commit Message
                                    <button type="button" className="close" data-dismiss="modal" onClick={() => this.setState({ commitMessageModal: false, commitMessage: "" })} aria-label="Close">
                                            <i className="far fa-times"></i>
                                        </button>
                                    </h6>
                                </div>
                                {this.props.isFetching ?
                                    <div className="commitFormLoader">
                                        <div className="formLoaderBox">
                                            <i className="fad fa-sync-alt fa-spin"></i>
                                        </div>
                                    </div> :
                                    <form onSubmit={this.submitHandler}>
                                        <div className="modal-body">
                                            <div className="form-group mb-0">
                                                <textarea rows="6" id="name" className="form-control" placeholder="Username" autoComplete="off" required autoFocus
                                                    value={this.state.commitMessage} onChange={(e) => this.setState({ commitMessage: e.target.value })}>
                                                </textarea>
                                            </div>
                                        </div>
                                        <div className="modal-footer">
                                            <button type="submit" className="btn btn-success">Commit</button>
                                            <button type="button" className="btn btn-dark" data-dismiss="modal" onClick={() => this.setState({ commitMessageModal: false, commitMessage: "" })}>Cancel</button>
                                        </div>
                                    </form>
                                }
                            </div>
                        </div>
                    </div>
                }
                {/* end git commit modal */}

                {/* git difference modal */}
                {this.state.gitDiffModal &&
                    <div className="modal d-block animated slideInDown">
                        <div className="modal-dialog modal-full">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h6 className="modal-title">Git Difference
                                            <button className="close" onClick={() => this.setState({ gitDiffModal: false })}><i className="far fa-times"></i></button>
                                    </h6>
                                </div>

                                {this.props.isFetching ?
                                    <div className="gitDiffLoader">
                                        <div className="formLoaderBox">
                                            <i className="fad fa-sync-alt fa-spin"></i>
                                        </div>
                                    </div> :
                                    <div className="gitDiffBox">
                                        <div className="gitDiffBoxHeader flex">
                                            <div className="fx-b50 pd-r-10">
                                                <h6>
                                                    <i className="fad fa-database mr-r-5 text-primary"></i>Database
                                                    <button type="button" className="btn btn-link" data-dismiss="modal" onClick={() => this.changeGitDiff(false)}>(Get Contents)</button>
                                                </h6>
                                            </div>
                                            <div className="fx-b50 pd-l-10">
                                                <h6>
                                                    <i className="fad fa-database mr-r-5 text-primary"></i>Github
                                                    <button type="button" className="btn btn-link" disabled={!this.state.payload.gitCode} onClick={() => this.changeGitDiff(true)}>(Get Contents)</button>
                                                </h6>
                                            </div>
                                        </div>
                                        <div className="gitDiffContent">
                                            <ReactDiffViewer
                                                oldValue={this.state.payload.originalFunction}
                                                newValue={this.state.payload.gitCode}
                                                splitView={true}
                                            />
                                        </div>
                                    </div>
                                }
                            </div>
                        </div>
                    </div>
                }
                {/* end git difference modal */}

                {/* validity modal */}
                {(this.state.isStatusChecked && this.isProjectSourceControlled() && isUsedForEnabled("js") && localStorage.getItem('isGitHubLogin') == "true" && this.isUserPermissionSourceControlled() && !this.state.invitationStatus) &&
                    <div className="modal d-block animated slideInDown">
                        <div className="modal-dialog modal-dialog-centered">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h6 className="modal-title">Permission Denied </h6>
                                </div>
                                {this.props.isFetching ?
                                    <div className="validityBoxLoader">
                                        <div className="formLoaderBox">
                                            <i className="fad fa-sync-alt fa-spin"></i>
                                        </div>
                                    </div> :
                                    <React.Fragment>
                                        <div className="modal-body">
                                            <div className="validityBox">
                                                <div className="validityBoxImage">
                                                    <img src='https://image.flaticon.com/icons/svg/1167/1167224.svg' />
                                                </div>
                                                <h5>Kindly accept the invitation from Github before making any changes</h5>
                                            </div>
                                        </div>
                                        <div className="modal-footer">
                                            <button type="button" className="btn btn-success" onClick={() => { this.props.history.push('/projects') }}>OK</button>
                                        </div>
                                    </React.Fragment>
                                }
                            </div>
                        </div>
                    </div>
                }
                {/* end validity modal */}

                {this.state.isOpen &&
                    <NotificationModal
                        type={this.state.modalType}
                        message2={this.state.message2}
                        onCloseHandler={this.onCloseHandler}
                    />
                }

            </React.Fragment>
        );
    }
}

AddOrEditUDF.propTypes = {
    dispatch: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
    saveEtlFunctionSuccess: saveEtlFunctionSuccess(),
    saveEtlFunctionFailure: saveEtlFunctionFailure(),
    getEtlFunctionSuccess: getEtlFunctionSuccess(),
    getEtlFunctionFailure: getEtlFunctionFailure(),
    getAllEtlFunctionSuccess: getAllEtlFunctionSuccess(),
    getAllEtlFunctionFailure: getAllEtlFunctionFailure(),
    invitationStatusSuccess: getInvitationStatusSuccess(),
    invitationStatusFailure: getInvitationStatusFailure(),
    executeSuccess: executeSuccess(),
    executeFailure: executeFailure(),
    libraryListSuccess: libraryListSuccess(),
    libraryListError: libraryListError()
});

function mapDispatchToProps(dispatch) {
    return {
        dispatch,
        saveEtlFunction: (payload, commitMessage) => dispatch(saveEtlFunction(payload, commitMessage)),
        getEtlFunctionById: payload => dispatch(getEtlFunctionById(payload)),
        resetToInitialState: () => dispatch(resetToInitialState()),
        getAllEtlFunctions: () => dispatch(getAllEtlFunctions()),
        getInvitationStatus: (id) => dispatch(getInvitationStatus(id)),
        executeFunctionCode: (payload) => dispatch(executeFunctionCode(payload)),
        getLibraryList: (libType) => dispatch(getLibraryList(libType))
    };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "addOrEditUDF", reducer });
const withSaga = injectSaga({ key: "addOrEditUDF", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(AddOrEditUDF);
