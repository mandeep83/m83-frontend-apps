/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { fromJS } from "immutable";
import { 
    SAVE_ETL_FUNCTION_SUCCESS, SAVE_ETL_FUNCTION_FAILURE, 
    GET_ETL_FUNCTION_SUCCESS, GET_ETL_FUNCTION_FAILURE, RESET_TO_INITIAL_STATE, 
    GET_ALL_ETL_FUNCTION_SUCCESS, GET_ALL_ETL_FUNCTION_FAILURE,
    GET_INVITATION_STATUS_SUCCESS, GET_INVITATION_STATUS_FAILURE,
    EXECUTE_FUNCTION_SUCCESS,EXECUTE_FUNCTION_FAILURE,
    GET_LIBRARY_SUCCESS,GET_LIBRARY_FAILURE
} from "./constants";

export const initialState = fromJS({});

function addOrEditEtlJsFunctionReducer(state = initialState, action) {
    switch (action.type) {
        case RESET_TO_INITIAL_STATE:
            return initialState;
        case SAVE_ETL_FUNCTION_SUCCESS:
            return Object.assign({}, state, {
                'saveEtlFunctionSuccess': action.response
            });

        case SAVE_ETL_FUNCTION_FAILURE:
            return Object.assign({}, state, {
                'saveEtlFunctionFailure': {message : action.error, timestamp : Date.now()}
            });

        case GET_ETL_FUNCTION_SUCCESS:
            return Object.assign({}, state, {
                'getEtlFunctionSuccess': action.response
            });

        case GET_ETL_FUNCTION_FAILURE:
            return Object.assign({}, state, {
                'getEtlFunctionFailure': action.error
            });
        case GET_ALL_ETL_FUNCTION_SUCCESS:
            return Object.assign({}, state, {
                'getAllEtlSuccess': action.response
      });
      
    case GET_ALL_ETL_FUNCTION_FAILURE:
           return Object.assign({}, state, {
               'getAllEtlFailure': action.response
      });

    case GET_INVITATION_STATUS_SUCCESS:
        return Object.assign({}, state, {
            'InvitationStatusSuccess': action.response
        });

    case GET_INVITATION_STATUS_FAILURE:
        return Object.assign({}, state, {
            'InvitationStatusFailure': action.error
        });

    case EXECUTE_FUNCTION_SUCCESS:
        return Object.assign({}, state, {
            'executeSuccess': action.response
        });

    case GET_LIBRARY_SUCCESS:
        return Object.assign({}, state, {
            'libraryListSuccess': action.response
        });

    case GET_LIBRARY_FAILURE:
        return Object.assign({}, state, {
            'libraryListError': action.error
        });

    case EXECUTE_FUNCTION_FAILURE:
        return Object.assign({}, state, {
            'executeFailure': action.error
        });
        default:
            return state;
    }
}

export default addOrEditEtlJsFunctionReducer;
