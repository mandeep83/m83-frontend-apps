/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

export const DEFAULT_ACTION = "app/AddOrEditUDF/DEFAULT_ACTION";

export const SAVE_ETL_FUNCTION = "app/AddOrEditUDF/SAVE_ETL_FUNCTION";
export const SAVE_ETL_FUNCTION_SUCCESS = "app/AddOrEditUDF/SAVE_ETL_FUNCTION_SUCCESS";
export const SAVE_ETL_FUNCTION_FAILURE = "app/AddOrEditUDF/SAVE_ETL_FUNCTION_FAILURE";

export const GET_ALL_ETL_FUNCTION = "app/AddOrEditUDF/GET_ALL_ETL_FUNCTION"
export const GET_ALL_ETL_FUNCTION_SUCCESS = "app/AddOrEditUDF/GET_ALL_ETL_FUNCTION_SUCCESS"
export const GET_ALL_ETL_FUNCTION_FAILURE = "app/AddOrEditUDF/GET_ALL_ETL_FUNCTION_FAILURE"

export const GET_ETL_FUNCTION = "app/AddOrEditUDF/GET_ETL_FUNCTION";
export const GET_ETL_FUNCTION_SUCCESS = "app/AddOrEditUDF/GET_ETL_FUNCTION_SUCCESS";
export const GET_ETL_FUNCTION_FAILURE = "app/AddOrEditUDF/GET_ETL_FUNCTION_FAILURE";

export const GET_INVITATION_STATUS = "app/AddOrEditUDF/GET_INVITATION_STATUS";
export const GET_INVITATION_STATUS_SUCCESS = "app/AddOrEditUDF/GET_INVITATION_STATUS_SUCCESS";
export const GET_INVITATION_STATUS_FAILURE = "app/AddOrEditUDF/GET_INVITATION_STATUS_FAILURE";

export const EXECUTE_FUNCTION = "app/AddOrEditUDF/EXECUTE_FUNCTION";
export const EXECUTE_FUNCTION_SUCCESS = "app/AddOrEditUDF/EXECUTE_FUNCTION_SUCCESS";
export const EXECUTE_FUNCTION_FAILURE = "app/AddOrEditUDF/EXECUTE_FUNCTION_FAILURE";

export const GET_LIBRARY = "app/AddOrEditUDF/GET_LIBRARY";
export const GET_LIBRARY_SUCCESS = "app/AddOrEditUDF/GET_LIBRARY_SUCCESS";
export const GET_LIBRARY_FAILURE = "app/AddOrEditUDF/GET_LIBRARY_FAILURE";


export const RESET_TO_INITIAL_STATE = "app/AddOrEditUDF/RESET_TO_INITIAL_STATE";




