/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
 * Direct selector to the AddOrEditUDF state domain
 */

const selectAddOrEditEtlJsFunctionDomain = state =>
  state.get("addOrEditUDF", initialState);

export const saveEtlFunctionSuccess = () => createSelector(selectAddOrEditEtlJsFunctionDomain, substate => substate.saveEtlFunctionSuccess);
export const saveEtlFunctionFailure = () => createSelector(selectAddOrEditEtlJsFunctionDomain, substate => substate.saveEtlFunctionFailure);

export const getAllEtlFunctionSuccess = () => createSelector(selectAddOrEditEtlJsFunctionDomain, substate => substate.getAllEtlSuccess);
export const getAllEtlFunctionFailure = () => createSelector(selectAddOrEditEtlJsFunctionDomain, substate => substate.getAllEtlFailure);

export const getEtlFunctionSuccess = () => createSelector(selectAddOrEditEtlJsFunctionDomain, substate => substate.getEtlFunctionSuccess);
export const getEtlFunctionFailure = () => createSelector(selectAddOrEditEtlJsFunctionDomain, substate => substate.getEtlFunctionFailure);

export const getInvitationStatusSuccess = () => createSelector(selectAddOrEditEtlJsFunctionDomain, substate => substate.InvitationStatusSuccess);
export const getInvitationStatusFailure = () => createSelector(selectAddOrEditEtlJsFunctionDomain, substate => substate.InvitationStatusFailure);

export const executeSuccess = () => createSelector(selectAddOrEditEtlJsFunctionDomain, substate => substate.executeSuccess);
export const executeFailure = () => createSelector(selectAddOrEditEtlJsFunctionDomain, substate => substate.executeFailure);

export const libraryListSuccess = () => createSelector(selectAddOrEditEtlJsFunctionDomain, substate => substate.libraryListSuccess);
export const libraryListError = () => createSelector(selectAddOrEditEtlJsFunctionDomain, substate => substate.libraryListError);

export { selectAddOrEditEtlJsFunctionDomain };

