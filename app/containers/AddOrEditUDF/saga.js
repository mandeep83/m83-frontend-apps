/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import {apiCallHandler} from '../../api';
import {takeEvery, takeLatest} from 'redux-saga'; 
import {  
  SAVE_ETL_FUNCTION, SAVE_ETL_FUNCTION_SUCCESS, SAVE_ETL_FUNCTION_FAILURE, 
  GET_ETL_FUNCTION, GET_ETL_FUNCTION_SUCCESS, GET_ETL_FUNCTION_FAILURE, 
  GET_ALL_ETL_FUNCTION, GET_ALL_ETL_FUNCTION_SUCCESS, GET_ALL_ETL_FUNCTION_FAILURE, 
  GET_INVITATION_STATUS, GET_INVITATION_STATUS_SUCCESS, GET_INVITATION_STATUS_FAILURE,
  EXECUTE_FUNCTION,EXECUTE_FUNCTION_SUCCESS,EXECUTE_FUNCTION_FAILURE,
  GET_LIBRARY, GET_LIBRARY_SUCCESS, GET_LIBRARY_FAILURE
} from "./constants";



export function* saveEtlFunction(action) {
  yield [apiCallHandler(action, SAVE_ETL_FUNCTION_SUCCESS, SAVE_ETL_FUNCTION_FAILURE, 'saveEtlFunction')]
}

export function* getEtlFunctionById(action) {
  yield [apiCallHandler(action, GET_ETL_FUNCTION_SUCCESS, GET_ETL_FUNCTION_FAILURE, 'getEtlFunctionById')]
}

export function* getAllEtlFunction(action) {
  yield [apiCallHandler(action, GET_ALL_ETL_FUNCTION_SUCCESS, GET_ALL_ETL_FUNCTION_FAILURE, 'getEtlFunctionsUrl')]
}

export function* getInvitationStatus(action) {
  yield [apiCallHandler(action, GET_INVITATION_STATUS_SUCCESS, GET_INVITATION_STATUS_FAILURE, 'getInvitationStatus')]
}

export function* executecode(action) {
  yield [apiCallHandler(action, EXECUTE_FUNCTION_SUCCESS, EXECUTE_FUNCTION_FAILURE, 'executeFunctions')]
}

export function* getLibraryData(action) {
  yield [apiCallHandler(action, GET_LIBRARY_SUCCESS, GET_LIBRARY_FAILURE, 'getLibrary')]
}

export function* watcherSaveEtlFunction() {
  yield takeEvery(SAVE_ETL_FUNCTION,  saveEtlFunction);
}

export function* watcherGetEtlFunctionById() {
  yield takeEvery(GET_ETL_FUNCTION,  getEtlFunctionById);
}

export function* watchergetAllEtlFunctions() {
  yield takeEvery(GET_ALL_ETL_FUNCTION,  getAllEtlFunction);
}

export function* watcherGetInvitationStatus() {
  yield takeEvery(GET_INVITATION_STATUS,  getInvitationStatus);
}

export function* watcherExecuteFunction() {
  yield takeEvery(EXECUTE_FUNCTION,  executecode);
}

export function* watcherLibraryGetFunction() {
  yield takeEvery(GET_LIBRARY, getLibraryData);
}

export default function* rootSaga() {
  yield [
    watcherSaveEtlFunction(),
    watcherGetEtlFunctionById(),
    watchergetAllEtlFunctions(),
    watcherGetInvitationStatus(),
    watcherExecuteFunction(),
    watcherLibraryGetFunction()
  ]
}


