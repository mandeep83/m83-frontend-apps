/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { SAVE_ETL_FUNCTION, GET_ETL_FUNCTION ,RESET_TO_INITIAL_STATE, GET_ALL_ETL_FUNCTION, GET_INVITATION_STATUS,EXECUTE_FUNCTION,GET_LIBRARY} from "./constants";


export function getAllEtlFunctions() {
  return {
    type: GET_ALL_ETL_FUNCTION
  };
}

export function saveEtlFunction(payload,commitMessage){
  return {
    type: SAVE_ETL_FUNCTION,
    payload,
    commitMessage
  }
}

export function getEtlFunctionById(id){
  return {
    type: GET_ETL_FUNCTION,
    id,
  }
}

export function getInvitationStatus(id){
  return {
    type: GET_INVITATION_STATUS,
    id,
  }
}

export function resetToInitialState() {
  return {
    type: RESET_TO_INITIAL_STATE,
  };
}

export function executeFunctionCode(payload) {
  return {
    type: EXECUTE_FUNCTION,
    payload
  };
}

export function getLibraryList(langType) {
  return {
    type: GET_LIBRARY,
    langType
  };
}
