/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * CreateFaq
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { FormattedMessage } from "react-intl";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import reducer from "./reducer";
import saga from "./saga";
import ReactSelect from "react-select";
import messages from "./messages";
import { Editor } from 'react-draft-wysiwyg';
import { stateToHTML } from 'draft-js-export-html';
import { EditorState, convertFromRaw, convertToRaw } from 'draft-js';
import cloneDeep from 'lodash/cloneDeep';
import * as SELECTORS from "./selectors";
import * as ACTIONS from "./actions";
import NotificationModal from '../../components/NotificationModal/Loadable';
import Loader from "../../components/Loader/Loadable"

/* eslint-disable react/prefer-stateless-function */
export class CreateFaq extends React.Component {
  state = {
    isFetching: this.props.match.params.id,
    editorState: EditorState.createEmpty(),
    payload: {
      category: "",
      answer: {},
      question: ""
    },
    category: [{
      name: 'connectors',
      displayName: 'Connectors'
    },
    {
      name: 'transformations',
      displayName: 'ETL/JS Functions'
    },
    {
      name: 'codeEngine',
      displayName: 'FaaS/APIs'
    },
    {
      name: 'dashboardStudio',
      displayName: 'Page/Widgets'
    },
    {
      name: 'mlStudio',
      displayName: 'ML Studio'
    },
    {
      name: 'simulationStudio',
      displayName: 'Simulation Studio'
    },
    {
      name: 'operations',
      displayName: 'Operations'
    },
    {
      name: 'devops',
      displayName: 'Devops'
    }
    ]
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.onSubmitSuccess) {
      return {
        ...prevState,
        isOpen: true,
        message2: nextProps.onSubmitSuccess,
        modalType: "success",
      }
    }

    if (nextProps.getByIdSuccess) {
      return {
        ...prevState,
        payload: nextProps.getByIdSuccess,
        editorState: EditorState.createWithContent(convertFromRaw(nextProps.getByIdSuccess.answer)),
        isFetching: false
      }
    }


    if (Object.keys(SELECTORS).filter(prop => prop.toLowerCase().includes("failure")).some(prop => nextProps[prop])) {
      let propName = Object.keys(SELECTORS).find(props => nextProps[props])
      return {
        ...prevState,
        isFetching: false,
        isOpen: true,
        message2: nextProps[propName],
        modalType: "error",
      }
    }
    return null
  }


  componentDidUpdate(prevProps, prevState) {
    // the below if statement should handle only step api success cases, add a check in case a new success case selector is added 
    if (Object.keys(SELECTORS).some(prop => this.props[prop])) {
      this.props.resetToInitialState()
    }
  }

  componentDidMount() {
    if (this.props.match.params.id)
      this.props.getFaqDetails(this.props.match.params.id)
  }

  onEditorStateChange = (editorState) => {
    this.setState({ editorState })
  }

  inputChangeHandler = (event) => {
    let payload = cloneDeep(this.state.payload);
    payload[event.target.id] = event.target.value

    this.setState({
      payload
    })
  }

  onSubmitHandler = (event) => {
    event.preventDefault();
    let payload = cloneDeep(this.state.payload);
    payload.question = payload.question.replace(/^\s\s*/, '').replace(/\s\s*$/, '')
    payload.answer = convertToRaw(this.state.editorState.getCurrentContent())
    this.setState({
      isFetching: true
    }, () => {
      this.props.onSubmitHandler(payload);
    })
  }

  onCloseHandler = () => {
    if (this.state.modalType === "success")
      this.props.history.push("/help")
    this.setState({
      isOpen: false,
      message2: '',
      modalType: "",
    })
  }
  render() {
    return (
      <React.Fragment>
        <Helmet>
          <title>CreateFaq</title>
          <meta name="description" content="Description of CreateFaq" />
        </Helmet>
        <div className="pageBreadcrumb">
          <div className="row">
            <div className="col-8">
              <p>
                <span className="previousPage" onClick={() => this.props.history.push(`/help`)}>Help</span>
                <span className="active">{this.props.match.params.id ? "Update Help" : "Add Help"}</span>
              </p>
            </div>
            <div className="col-4 text-right">
            </div>
          </div>
        </div>
        {this.state.isFetching ?
          <Loader />
          :
          <div className="outerBox pb-0">
            <form className="contentForm pd-0" onSubmit={this.onSubmitHandler}>
              <div className="contentFormDetail border-r-0">
                <div className="flex">
                  <div className="fx-b100">
                    <div className="form-group mr-b-20">
                      <label className="form-label">Category : <span className="requiredMark"><i className="fa fa-asterisk" /></span></label>
                      <select className="form-control" value={this.state.payload.category} onChange={this.inputChangeHandler} id="category" required>
                        <option value="">Select</option>
                        {this.state.category.map((temp, index) => <option key={index} value={temp.name}>{temp.displayName}</option>)}
                      </select>
                    </div>
                  </div>
                  <div className="fx-b100">
                    <div className="form-group mr-b-20">
                      <input type="text" value={this.state.payload.question} onChange={this.inputChangeHandler} id="question" required className="form-control" placeholder="Name" />
                      <label className="form-group-label">Question :
                      <span className="requiredMark"><i className="fa fa-asterisk"></i></span>
                      </label>
                    </div>
                  </div>
                </div>
                <div className="form-group mr-b-0">
                  <Editor
                    editorState={this.state.editorState}
                    toolbarClassName="toolbarClassName"
                    editorClassName="editor-content"
                    onEditorStateChange={this.onEditorStateChange}
                  />
                </div>
              </div>

              <div className="bottomButton">
                <button name="button" className="btn btn-success">Save</button>
              </div>
            </form>
          </div>
        }
        {
          this.state.isOpen &&
          <NotificationModal
            type={this.state.modalType}
            message2={this.state.message2}
            onCloseHandler={this.onCloseHandler}
          />
        }
      </React.Fragment>
    );
  }
}

CreateFaq.propTypes = {
  dispatch: PropTypes.func.isRequired
};

let allSelectors = {}
Object.entries(SELECTORS).map(([key, value]) => {
  allSelectors[key] = value()
})

const mapStateToProps = createStructuredSelector(allSelectors)

function mapDispatchToProps(dispatch) {
  let allActions = { dispatch }
  Object.entries(ACTIONS).map(([key, value]) => {
    allActions[key] = (...args) => dispatch(value(...args))
  })
  return allActions
}


const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

const withReducer = injectReducer({ key: "createFaq", reducer });
const withSaga = injectSaga({ key: "createFaq", saga });

export default compose(
  withReducer,
  withSaga,
  withConnect
)(CreateFaq);
