/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { apiCallHandler } from '../../api';
import { takeEvery } from 'redux-saga';
import * as CONSTANTS from "./constants";


export function* getSubmitRequestApiHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.ON_SUBMIT_HANDLER_SUCCESS, CONSTANTS.ON_SUBMIT_HANDLER_FAILURE, 'submitRequestFaq')];
}

export function* watcherOnSubmitRequest() {
  yield takeEvery(CONSTANTS.ON_SUBMIT_HANDLER, getSubmitRequestApiHandlerAsync);
}

export function* getByIdApiHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_FAQ_DETAILS_SUCCESS, CONSTANTS.GET_FAQ_DETAILS_FAILURE, 'getByIdFaq')];
}

export function* watcherGetByIdRequest() {
  yield takeEvery(CONSTANTS.GET_FAQ_DETAILS, getByIdApiHandlerAsync);
}

export default function* rootSaga() {
  yield [
    watcherOnSubmitRequest(),
    watcherGetByIdRequest(),
  ];
}
