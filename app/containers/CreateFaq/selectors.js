/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
 * Direct selector to the createFaq state domain
 */

const selectCreateFaqDomain = state => state.get("createFaq", initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by CreateFaq
 */

export const onSubmitSuccess = () => createSelector(selectCreateFaqDomain, substate => substate.onSubmitSuccess);
export const onSubmitFailure = () => createSelector(selectCreateFaqDomain, substate => substate.onSubmitFailure);

export const getByIdSuccess = () => createSelector(selectCreateFaqDomain, substate => substate.getByIdSuccess);
export const getByIdFailure = () => createSelector(selectCreateFaqDomain, substate => substate.getByIdFailure);

