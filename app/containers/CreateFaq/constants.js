/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/*
 *
 * CreateFaq constants
 *
 */

export const RESET_TO_INITIAL_STATE = "app/CreateFaq/RESET_TO_INITIAL_STATE";

export const ON_SUBMIT_HANDLER = "app/CreateFaq/ON_SUBMIT_HANDLER";
export const ON_SUBMIT_HANDLER_SUCCESS = "app/CreateFaq/ON_SUBMIT_HANDLER_SUCCESS";
export const ON_SUBMIT_HANDLER_FAILURE = "app/CreateFaq/ON_SUBMIT_HANDLER_FAILURE";

export const GET_FAQ_DETAILS = "app/CreateFaq/GET_FAQ_DETAILS";
export const GET_FAQ_DETAILS_SUCCESS = "app/CreateFaq/GET_FAQ_DETAILS_SUCCESS";
export const GET_FAQ_DETAILS_FAILURE = "app/CreateFaq/GET_FAQ_DETAILS_FAILURE";
