/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import * as CONSTANTS from './constants';

export function defaultAction() {
  return {
    type: CONSTANTS.DEFAULT_ACTION,
  };
}

export function fetchSideNav() {
  return {
    type: CONSTANTS.FETCH_SIDE_NAV_REQUEST,
  };
}

export function getVMQCredentials(){
  return {
    type: CONSTANTS.GET_VMQ_CREDENTIALS
  }
}

export function getApplicationsByUser() {
  return {
    type: CONSTANTS.GET_APPLICATIONS_BY_USER,
  };
}

export function getOauthStatus() {
  return {
    type: CONSTANTS.GET_OAUTH_STATUS,
  };
}

export function getAllProjects() {
  return {
    type: CONSTANTS.GET_ALL_PROJECTS,
  };
}

export function oauthLogoutHandler(name) {
  return {
    type: CONSTANTS.OAUTH_PROVIDER_LOGOUT_HANDLER,
    name
  };
}


export function getDeveloperQuota() {
  return {
    type: CONSTANTS.GET_DEVELOPER_QUOTA,
  };
}


export function getAllAccounts() {
  return {
    type: CONSTANTS.GET_ALL_ACCOUNTS,
  };
}

export function showToast(showToastType,showToastMessage) {
  return {
    type: CONSTANTS.SHOW_TOAST,
    showToastType,
    showToastMessage
  }
}

