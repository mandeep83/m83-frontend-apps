/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { fromJS } from 'immutable';
import * as CONSTANTS from './constants';

export const initialState = fromJS({});

function homePageReducer(state = initialState, action) {

  switch (action.type) {
    case CONSTANTS.DEFAULT_ACTION:
      return state;
    case CONSTANTS.SIDE_NAV_API_SUCCESS:
      return Object.assign({}, state, {
        sideNav: action.response
      });
    case CONSTANTS.SIDE_NAV_API_FAILURE:
      return Object.assign({}, state, {
        sideNavError: { message: action.error, timestamp: Date.now() }
      });
    case CONSTANTS.GET_VMQ_CREDENTIALS_SUCCESS:
      return Object.assign({}, state, {
        vmqCredentialsSuccess: action.response
      });
    case CONSTANTS.GET_VMQ_CREDENTIALS_FAILURE:
      return Object.assign({}, state, {
        vmqCredentialsFailure: { message: action.error, timestamp: Date.now() }
      });
    case CONSTANTS.GET_APPLICATIONS_BY_USER_SUCCESS:
      return Object.assign({}, state, {
        applications: action.response
      });
    case CONSTANTS.GET_APPLICATIONS_BY_USER_FAILURE:
      return Object.assign({}, state, {
        applicationsFailure: { message: action.error, timestamp: Date.now() }
      });
    case CONSTANTS.GET_OAUTH_STATUS_SUCCESS:
      return Object.assign({}, state, {
        oAuthStatus: action.response
      });
    case CONSTANTS.GET_OAUTH_STATUS_FAILURE:
      return Object.assign({}, state, {
        oAuthStatusFailure: { message: action.error, timestamp: Date.now() }
      });
    case CONSTANTS.GET_ALL_PROJECTS_SUCCESS:
      return Object.assign({}, state, {
        projectsSuccess: action.response
      });
    case CONSTANTS.GET_ALL_PROJECTS_FAILURE:
      return Object.assign({}, state, {
        projectsFailure: action.error
      });
    case CONSTANTS.DELETE_PROJECT_SUCCESS:
      return Object.assign({}, state, {
        projectsSuccess: state.projectsSuccess.filter(project => project.id !== action.id)
      });
    case CONSTANTS.OAUTH_PROVIDER_LOGOUT_HANDLER_SUCCESS:
      return Object.assign({}, state, {
        oAuthLogoutSuccess: action.response
      });
    case CONSTANTS.OAUTH_PROVIDER_LOGOUT_HANDLER_FAILURE:
      return Object.assign({}, state, {
        oAuthLogoutError: action.error
      });
    case CONSTANTS.GET_DEVELOPER_QUOTA_SUCCESS:
      return Object.assign({}, state, {
        developerQuotaSuccess: action.response
      });
    case CONSTANTS.GET_DEVELOPER_QUOTA_FAILURE:
      return Object.assign({}, state, {
        developerQuotaError: action.error
      });
    case CONSTANTS.GET_ALL_ACCOUNTS_SUCCESS:
      return Object.assign({}, state, {
        allAccountsSuccess: action.response
      });
    case CONSTANTS.GET_ALL_ACCOUNTS_FAILURE:
      return Object.assign({}, state, {
        allAccountsError: action.error
      });
    case CONSTANTS.HANDLE_402:
      return Object.assign({}, state, {
        handle402:{
          message:action.error,
          timestamp:new Date()
        } 
      });
    case CONSTANTS.SHOW_TOAST:
      return Object.assign({}, state, {
        showToast:{
          showToastType: action.showToastType,
          showToastMessage: action.showToastMessage
        } 
      });  
    default:
      return state;
  }
}

export default homePageReducer;
