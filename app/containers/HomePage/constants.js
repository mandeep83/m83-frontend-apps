/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

export const DEFAULT_ACTION = 'app/HomePage/DEFAULT_ACTION';

export const FETCH_SIDE_NAV_REQUEST = 'app/HomePage/FETCH_SIDE_NAV_REQUEST';
export const SIDE_NAV_API_SUCCESS = 'app/HomePage/SIDE_NAV_API_SUCCESS';
export const SIDE_NAV_API_FAILURE = 'app/HomePage/SIDE_NAV_API_FAILURE';

export const GET_VMQ_CREDENTIALS = 'app/HomePage/GET_VMQ_CREDENTIALS';
export const GET_VMQ_CREDENTIALS_SUCCESS = 'app/HomePage/GET_VMQ_CREDENTIALS_SUCCESS';
export const GET_VMQ_CREDENTIALS_FAILURE = 'app/HomePage/GET_VMQ_CREDENTIALS_FAILURE';

export const GET_APPLICATIONS_BY_USER = 'app/HomePage/GET_APPLICATIONS_BY_USER';
export const GET_APPLICATIONS_BY_USER_SUCCESS = 'app/HomePage/GET_APPLICATIONS_BY_USER_SUCCESS';
export const GET_APPLICATIONS_BY_USER_FAILURE = 'app/HomePage/GET_APPLICATIONS_BY_USER_FAILURE';

export const GET_OAUTH_STATUS = 'app/HomePage/GET_OAUTH_STATUS';
export const GET_OAUTH_STATUS_SUCCESS = 'app/HomePage/GET_OAUTH_STATUS_SUCCESS';
export const GET_OAUTH_STATUS_FAILURE = 'app/HomePage/GET_OAUTH_STATUS_FAILURE';

export const GET_ALL_PROJECTS = 'app/HomePage/GET_ALL_PROJECTS';
export const GET_ALL_PROJECTS_SUCCESS = 'app/HomePage/GET_ALL_PROJECTS_SUCCESS';
export const GET_ALL_PROJECTS_FAILURE = 'app/HomePage/GET_ALL_PROJECTS_FAILURE';

export const DELETE_PROJECT_SUCCESS = 'app/HomePage/DELETE_PROJECT_SUCCESS';

export const OAUTH_PROVIDER_LOGOUT_HANDLER = 'app/HomePage/OAUTH_PROVIDER_LOGOUT_HANDLER';
export const OAUTH_PROVIDER_LOGOUT_HANDLER_SUCCESS = 'app/HomePage/OAUTH_PROVIDER_LOGOUT_HANDLER_SUCCESS';
export const OAUTH_PROVIDER_LOGOUT_HANDLER_FAILURE = 'app/HomePage/OAUTH_PROVIDER_LOGOUT_HANDLER_FAILURE';

export const GET_DEVELOPER_QUOTA = 'app/HomePage/GET_DEVELOPER_QUOTA';
export const GET_DEVELOPER_QUOTA_SUCCESS = 'app/HomePage/GET_DEVELOPER_QUOTA_SUCCESS';
export const GET_DEVELOPER_QUOTA_FAILURE = 'app/HomePage/GET_DEVELOPER_QUOTA_FAILURE';

export const GET_ALL_ACCOUNTS = 'app/HomePage/GET_ALL_ACCOUNTS';
export const GET_ALL_ACCOUNTS_SUCCESS = 'app/HomePage/GET_ALL_ACCOUNTS_SUCCESS';
export const GET_ALL_ACCOUNTS_FAILURE = 'app/HomePage/GET_ALL_ACCOUNTS_FAILURE';

export const HANDLE_402 = 'app/HomePage/HANDLE_402';

export const SHOW_TOAST = 'app/HomePage/SHOW_TOAST';
