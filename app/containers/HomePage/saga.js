/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { takeEvery } from 'redux-saga';
import * as CONSTANTS from './constants';
import { apiCallHandler } from '../../api';

export function* getSideNavApiHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.SIDE_NAV_API_SUCCESS, CONSTANTS.SIDE_NAV_API_FAILURE, 'getNavigationAttribute')];
}

export function* getVMQCredentialsHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_VMQ_CREDENTIALS_SUCCESS, CONSTANTS.GET_VMQ_CREDENTIALS_FAILURE, 'getVMQCredentials')];
}

export function* getApplicationsByUserApiHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_APPLICATIONS_BY_USER_SUCCESS, CONSTANTS.GET_APPLICATIONS_BY_USER_FAILURE, 'getApplicationsByUser')];
}

export function* getOAuthStatusApiHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_OAUTH_STATUS_SUCCESS, CONSTANTS.GET_OAUTH_STATUS_FAILURE, 'oAuthValidity')];
}

export function* apiGetAllProjectsHandlerAsync(action) {
    yield [apiCallHandler(action, CONSTANTS.GET_ALL_PROJECTS_SUCCESS, CONSTANTS.GET_ALL_PROJECTS_FAILURE, 'fetchProjects')];
}

export function* apiOauthLogoutHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.OAUTH_PROVIDER_LOGOUT_HANDLER_SUCCESS, CONSTANTS.OAUTH_PROVIDER_LOGOUT_HANDLER_FAILURE, 'oauthLogoutRequest')];
}

export function* getDeveloperQuotaHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_DEVELOPER_QUOTA_SUCCESS, CONSTANTS.GET_DEVELOPER_QUOTA_FAILURE, 'getDeveloperQuota')];
}

export function* getAllAccountsHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_ALL_ACCOUNTS_SUCCESS, CONSTANTS.GET_ALL_ACCOUNTS_FAILURE, 'getAllAccounts')];
}

export function* watcherGetSideNavRequest() {
  yield takeEvery(CONSTANTS.FETCH_SIDE_NAV_REQUEST, getSideNavApiHandlerAsync);
}

export function* watcherGetVMQCredentials() {
  yield takeEvery(CONSTANTS.GET_VMQ_CREDENTIALS, getVMQCredentialsHandlerAsync);
}

export function* watcherGetApplicationsByUser() {
  yield takeEvery(CONSTANTS.GET_APPLICATIONS_BY_USER, getApplicationsByUserApiHandlerAsync);
}

export function* watcherGetOAuthStatus() {
  yield takeEvery(CONSTANTS.GET_OAUTH_STATUS, getOAuthStatusApiHandlerAsync);
}

export function* watcherGetALlProjectsRequest() {
  yield takeEvery(CONSTANTS.GET_ALL_PROJECTS, apiGetAllProjectsHandlerAsync);
}

export function* watcherOauthLogoutRequest() {
  yield takeEvery(CONSTANTS.OAUTH_PROVIDER_LOGOUT_HANDLER, apiOauthLogoutHandlerAsync);
}

export function* watcherGetDeveloperQuota() {
  yield takeEvery(CONSTANTS.GET_DEVELOPER_QUOTA, getDeveloperQuotaHandlerAsync);
}

export function* watcherGetAllAccounts() {
  yield takeEvery(CONSTANTS.GET_ALL_ACCOUNTS, getAllAccountsHandlerAsync);
}

export default function* rootSaga() {
  yield [
    watcherGetSideNavRequest(),
    watcherGetApplicationsByUser(),
    watcherGetOAuthStatus(),
    watcherGetALlProjectsRequest(),
    watcherOauthLogoutRequest(),
    watcherGetVMQCredentials(),
    watcherGetDeveloperQuota(),
    watcherGetAllAccounts(),
  ];
}

