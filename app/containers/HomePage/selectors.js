/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the homePage state domain
 */

const selectHomePageDomain = state => state.get('homePage', initialState);

export const getSideNav = () => createSelector(selectHomePageDomain, subState => subState.sideNav);

export const getSideNavError = () => createSelector(selectHomePageDomain, subState => subState.sideNavError);

export const vmqCredentialsSuccess = () => createSelector(selectHomePageDomain, subState => subState.vmqCredentialsSuccess);

export const vmqCredentialsFailure = () => createSelector(selectHomePageDomain, subState => subState.vmqCredentialsFailure);

export const getApplications = () => createSelector(selectHomePageDomain, subState => subState.applications);

export const getApplicationsFailure = () => createSelector(selectHomePageDomain, subState => subState.applicationsFailure);

export const getOAuthStatus = () => createSelector(selectHomePageDomain, subState => subState.oAuthStatus);

export const getOauthStatusFailure = () => createSelector(selectHomePageDomain, subState => subState.oAuthStatusFailure);

export const projectsSuccess = () => createSelector(selectHomePageDomain, subState => subState.projectsSuccess);

export const projectsFailure = () => createSelector(selectHomePageDomain, subState => subState.projectsFailure);

export const getOauthLogoutSuccess = () => createSelector(selectHomePageDomain, subState => subState.oAuthLogoutSuccess);

export const getOauthLogoutError = () => createSelector(selectHomePageDomain, subState => subState.oAuthLogoutError);

export const developerQuotaSuccess = () => createSelector(selectHomePageDomain, subState => subState.developerQuotaSuccess);

export const developerQuotaError = () => createSelector(selectHomePageDomain, subState => subState.developerQuotaError);

export const allAccountsSuccess = () => createSelector(selectHomePageDomain, subState => subState.allAccountsSuccess);

export const allAccountsError = () => createSelector(selectHomePageDomain, subState => subState.allAccountsError);

export const handle402 = () => createSelector(selectHomePageDomain, subState => subState.handle402);

export const showToast = () => createSelector(selectHomePageDomain, subState => subState.showToast);