/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import React from 'react';
import { Route, Switch } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import jwt_decode from "jwt-decode";
import * as SELECTORS from './selectors';
import reducer from './reducer';
import saga from './saga';
// import PubNub from 'pubnub'
import * as ACTIONS from './actions';
import { compare, getNotificationObj } from '../../commonUtils';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Link } from 'react-router-dom';
import cx from 'classnames';
import Select from 'react-select';
import ReactSelect from 'react-select';
import {ManageGitPod} from "../ManageGitPod";
import {AnalyticsManager} from "../AnalyticsManager";

let allContainers = ["Logs", "AddOrEditTenant", "AlarmsAndAlertsManager", "ChangePassword", "AddOrEditEtlDataSource", "PageStudio", "Header", "Connectors", "ManageNavigation",
    "ManageReports", "PolicyAddEdit", "PolicyList", "RoleNavigation", "TenantList", "TenantDetails", "TicketManagement", "ViewAlarmsAndAlerts", "WidgetStudio",
    "Home", "AddOrEditEtlConfig", "Transformations", "AddOrEditFaasList", "FaaS", "ManageGatewayApi", "AddOrEditUDF", "UDF", "AddOrEditApplication", "AuditLogs",
    "SimulationStudio", "MyActivities", "CodePlayGround", "License", "CreateOrEditWidget", "ReportBuilder", "FormBuilder", "DataCollections", "LicenseExpiry", "ManagePodLogs",
    "MlStudio", "AddOrEditMl", "AddOrEditReports", "ManageGroup", "ProjectList", "OAuthConfigurations", "Scheduler", "Setting", "Template", "AddOrEditTemplate", "ManageRoles",
    "ManageUsers", "CreateFaq", "AddOrEditSimulation", "Applications", "ApplicationsNotFound", "Help", "SystemProperties", "DeviceAttributes", "iSignUp",
    "ManageImages", "ManageNotification", "EtlDiagnostic", "ManageDockerImages", "MyAccount", "MyPlan", "AddOrEditConnector", "NewAddOrEditEtlConfig", "NewAddOrEditMl",
    "AddOrEditDeviceGroup", "DeviceGroups", "CreateOrEditPage", "PagePreview", "DeviceManagement", "Events", "AddOrEditEvent", "DeviceDiagnostics", "SignUp",
    "AddOrEditClient", "ClientList", "ApplicationRole", "ApplicationUser", "ApplicationUserGroup", "ApplicationDeviceGroup", "DeviceTypes", "DeviceDashboardsEventHistory",
    "DeviceDashboardsDeviceMap", "DeviceList", "DeviceDashboardsAggregate", "DeviceDashboardsAlarms", "DeviceDashboardsConnectionHistory", "DeviceDashboardsDeviceDetails",
    "DeviceDashboardsTrendsForecast", "AlarmConfig", "ActionList", "PrivacyPolicy", "TermsAndConditions", "AddOrEditDeviceType", "CustomizeAttribute", "NoteBooks", "AddOrEditNoteBook",
    "ManageAddOn", "ManageCustomMenu", "AddOrEditCustomMenu", "PreviewPage", "ManageDiscounts", "ManageFlows", "ManageServices", "AddOrEditService",  "LocationTracker", "ManageRepositories",
    "ManageDockerCred", "ManageFlows", "AddOrEditService", "ManageRepositories", "ManageDockerCred", "ManageGitPod","ManageEtlSchedules", "DLExperiments", "AddOrEditDLExperiment"
]

let allComponents = ["NotificationModal", "NotFoundPage", "NoNavigationAssigned", "Loader", "ForbiddenPage", "IFrameComponent", "ConfirmModel"];

let timeZoneArray = [
    {
        "value": "Etc/GMT+12",
        "label": "(GMT-12:00) International Date Line West"
    },
    {
        "value": "Pacific/Midway",
        "label": "(GMT-11:00) Midway Island, Samoa"
    },
    {
        "value": "Pacific/Honolulu",
        "label": "(GMT-10:00) Hawaii"
    },
    {
        "value": "US/Alaska",
        "label": "(GMT-09:00) Alaska"
    },
    {
        "value": "America/Los_Angeles",
        "label": "(GMT-08:00) Pacific Time (US & Canada)"
    },
    {
        "value": "America/Tijuana",
        "label": "(GMT-08:00) Tijuana, Baja California"
    },
    {
        "value": "US/Arizona",
        "label": "(GMT-07:00) Arizona"
    },
    {
        "value": "America/Chihuahua",
        "label": "(GMT-07:00) Chihuahua, La Paz, Mazatlan"
    },
    {
        "value": "US/Mountain",
        "label": "(GMT-07:00) Mountain Time (US & Canada)"
    },
    {
        "value": "America/Managua",
        "label": "(GMT-06:00) Central America"
    },
    {
        "value": "US/Central",
        "label": "(GMT-06:00) Central Time (US & Canada)"
    },
    {
        "value": "America/Mexico_City",
        "label": "(GMT-06:00) Guadalajara, Mexico City, Monterrey"
    },
    {
        "value": "Canada/Saskatchewan",
        "label": "(GMT-06:00) Saskatchewan"
    },
    {
        "value": "America/Bogota",
        "label": "(GMT-05:00) Bogota, Lima, Quito, Rio Branco"
    },
    {
        "value": "US/Eastern",
        "label": "(GMT-05:00) Eastern Time (US & Canada)"
    },
    {
        "value": "US/East-Indiana",
        "label": "(GMT-05:00) Indiana (East)"
    },
    {
        "value": "Canada/Atlantic",
        "label": "(GMT-04:00) Atlantic Time (Canada)"
    },
    {
        "value": "America/Caracas",
        "label": "(GMT-04:00) Caracas, La Paz"
    },
    {
        "value": "America/Manaus",
        "label": "(GMT-04:00) Manaus"
    },
    {
        "value": "America/Santiago",
        "label": "(GMT-04:00) Santiago"
    },
    {
        "value": "Canada/Newfoundland",
        "label": "(GMT-03:30) Newfoundland"
    },
    {
        "value": "America/Sao_Paulo",
        "label": "(GMT-03:00) Brasilia"
    },
    {
        "value": "America/Argentina/Buenos_Aires",
        "label": "(GMT-03:00) Buenos Aires, Georgetown"
    },
    {
        "value": "America/Godthab",
        "label": "(GMT-03:00) Greenland"
    },
    {
        "value": "America/Montevideo",
        "label": "(GMT-03:00) Montevideo"
    },
    {
        "value": "America/Noronha",
        "label": "(GMT-02:00) Mid-Atlantic"
    },
    {
        "value": "Atlantic/Cape_Verde",
        "label": "(GMT-01:00) Cape Verde Is."
    },
    {
        "value": "Atlantic/Azores",
        "label": "(GMT-01:00) Azores"
    },
    {
        "value": "Africa/Casablanca",
        "label": "(GMT+00:00) Casablanca, Monrovia, Reykjavik"
    },
    {
        "value": "Etc/Greenwich",
        "label": "(GMT+00:00) Greenwich Mean Time : Dublin, Edinburgh, Lisbon, London"
    },
    {
        "value": "Europe/Amsterdam",
        "label": "(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna"
    },
    {
        "value": "Europe/Belgrade",
        "label": "(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague"
    },
    {
        "value": "Europe/Brussels",
        "label": "(GMT+01:00) Brussels, Copenhagen, Madrid, Paris"
    },
    {
        "value": "Europe/Sarajevo",
        "label": "(GMT+01:00) Sarajevo, Skopje, Warsaw, Zagreb"
    },
    {
        "value": "Africa/Lagos",
        "label": "(GMT+01:00) West Central Africa"
    },
    {
        "value": "Asia/Amman",
        "label": "(GMT+02:00) Amman"
    },
    {
        "value": "Europe/Athens",
        "label": "(GMT+02:00) Athens, Bucharest, Istanbul"
    },
    {
        "value": "Asia/Beirut",
        "label": "(GMT+02:00) Beirut"
    },
    {
        "value": "Africa/Cairo",
        "label": "(GMT+02:00) Cairo"
    },
    {
        "value": "Africa/Harare",
        "label": "(GMT+02:00) Harare, Pretoria"
    },
    {
        "value": "Europe/Helsinki",
        "label": "(GMT+02:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius"
    },
    {
        "value": "Asia/Jerusalem",
        "label": "(GMT+02:00) Jerusalem"
    },
    {
        "value": "Europe/Minsk",
        "label": "(GMT+02:00) Minsk"
    },
    {
        "value": "Africa/Windhoek",
        "label": "(GMT+02:00) Windhoek"
    },
    {
        "value": "Asia/Kuwait",
        "label": "(GMT+03:00) Kuwait, Riyadh, Baghdad"
    },
    {
        "value": "Europe/Moscow",
        "label": "(GMT+03:00) Moscow, St. Petersburg, Volgograd"
    },
    {
        "value": "Africa/Nairobi",
        "label": "(GMT+03:00) Nairobi"
    },
    {
        "value": "Asia/Tbilisi",
        "label": "(GMT+03:00) Tbilisi"
    },
    {
        "value": "Asia/Tehran",
        "label": "(GMT+03:30) Tehran"
    },
    {
        "value": "Asia/Muscat",
        "label": "(GMT+04:00) Abu Dhabi, Muscat"
    },
    {
        "value": "Asia/Baku",
        "label": "(GMT+04:00) Baku"
    },
    {
        "value": "Asia/Yerevan",
        "label": "(GMT+04:00) Yerevan"
    },
    {
        "value": "Asia/Kabul",
        "label": "(GMT+04:30) Kabul"
    },
    {
        "value": "Asia/Yekaterinburg",
        "label": "(GMT+05:00) Yekaterinburg"
    },
    {
        "value": "Asia/Karachi",
        "label": "(GMT+05:00) Islamabad, Karachi, Tashkent"
    },
    {
        "value": "Asia/Calcutta",
        "label": "(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi"
    },
    {
        "value": "Asia/Katmandu",
        "label": "(GMT+05:45) Kathmandu"
    },
    {
        "value": "Asia/Almaty",
        "label": "(GMT+06:00) Almaty, Novosibirsk"
    },
    {
        "value": "Asia/Dhaka",
        "label": "(GMT+06:00) Astana, Dhaka"
    },
    {
        "value": "Asia/Rangoon",
        "label": "(GMT+06:30) Yangon (Rangoon)"
    },
    {
        "value": "Asia/Bangkok",
        "label": "(GMT+07:00) Bangkok, Hanoi, Jakarta"
    },
    {
        "value": "Asia/Krasnoyarsk",
        "label": "(GMT+07:00) Krasnoyarsk"
    },
    {
        "value": "Asia/Hong_Kong",
        "label": "(GMT+08:00) Beijing, Chongqing, Hong Kong, Urumqi"
    },
    {
        "value": "Asia/Kuala_Lumpur",
        "label": "(GMT+08:00) Kuala Lumpur, Singapore"
    },
    {
        "value": "Asia/Irkutsk",
        "label": "(GMT+08:00) Irkutsk, Ulaan Bataar"
    },
    {
        "value": "Australia/Perth",
        "label": "(GMT+08:00) Perth"
    },
    {
        "value": "Asia/Taipei",
        "label": "(GMT+08:00) Taipei"
    },
    {
        "value": "Asia/Tokyo",
        "label": "(GMT+09:00) Osaka, Sapporo, Tokyo"
    },
    {
        "value": "Asia/Seoul",
        "label": "(GMT+09:00) Seoul"
    },
    {
        "value": "Asia/Yakutsk",
        "label": "(GMT+09:00) Yakutsk"
    },
    {
        "value": "Australia/Adelaide",
        "label": "(GMT+09:30) Adelaide"
    },
    {
        "value": "Australia/Darwin",
        "label": "(GMT+09:30) Darwin"
    },
    {
        "value": "Australia/Brisbane",
        "label": "(GMT+10:00) Brisbane"
    },
    {
        "value": "Australia/Canberra",
        "label": "(GMT+10:00) Canberra, Melbourne, Sydney"
    },
    {
        "value": "Australia/Hobart",
        "label": "(GMT+10:00) Hobart"
    },
    {
        "value": "Pacific/Guam",
        "label": "(GMT+10:00) Guam, Port Moresby"
    },
    {
        "value": "Asia/Vladivostok",
        "label": "(GMT+10:00) Vladivostok"
    },
    {
        "value": "Asia/Magadan",
        "label": "(GMT+11:00) Magadan, Solomon Is., New Caledonia"
    },
    {
        "value": "Pacific/Auckland",
        "label": "(GMT+12:00) Auckland, Wellington"
    },
    {
        "value": "Pacific/Fiji",
        "label": "(GMT+12:00) Fiji, Kamchatka, Marshall Is."
    },
    {
        "value": "Pacific/Tongatapu",
        "label": "(GMT+13:00) Nuku'alofa"
    }
]

allContainers.map(container => {
    window[container] = require(`../${container}/Loadable`).default;
})

allComponents.map(comp => {
    window[comp] = require(`../../components/${comp}/Loadable`).default;
})


class ErrorBoundary extends React.Component {
    state = {
        error: null,
    };

    componentDidCatch(error, errorInfo) {
        // Catch errors in any child components and re-renders with an error message
        this.setState({
            error: error,
        });
    }

    render() {
        if (this.state.error) {
            // Fallback UI if an error occurs
            return (
                <div className="error-box">
                    <div className="error-content">
                        <h1>
                            <span>O</span>
                            <span>O</span>
                            <span>P</span>
                            <span>S</span>
                        </h1>
                        <h6>Something went wrong, Please refresh the page !</h6>
                    </div>
                </div>
            );
        }
        // component normally just renders children
        return this.props.children;
    }
}

let client;
let theme = {
    themeSideBarTextColor: "",
    themeSideBar: "",
    themeHeaderTextColor: "",
    themeHeader: "",
}


/* eslint-disable react/prefer-stateless-function */
export class HomePage extends React.Component {

    state = {
        adminRoutes: [
            { "path": "/addOrEditDeviceType/:id?", "component": AddOrEditDeviceType },
            { "path": "/addOrEditConnector/:connector?/:id?", "component": AddOrEditConnector },
            { "path": "/addOrEditFaaS/:id?", "component": AddOrEditFaasList },
            { "path": "/addOrEditEtlPipeline/:createdFor?/:id?", "component": NewAddOrEditEtlConfig },
            { "path": "/addOrEditUdf/:id?", "component": AddOrEditUDF },
            { "path": "/addOrEditDashboard/:id?", "component": CreateOrEditPage },
            { "path": "/addOrEditSimulation/:id?/:isView?", "component": AddOrEditSimulation },
            { "path": "/addOrEditTenant/:name?/:id?/", "component": AddOrEditTenant },
            { "path": "/apiGateway", "component": ManageGatewayApi },
            { "path": "/alarmsAndAlerts", "component": AlarmsAndAlertsManager },
            { "path": "/connectors/:connector?", "component": Connectors },
            { "path": "/deviceTypes", "component": DeviceTypes },
            { "path": "/faaS/:category?", "component": FaaS },
            { "path": "/createOrEditPage/:id?", "component": CreateOrEditWidget },
            { "path": "/deviceAttributes/:connectorId?", "component": DeviceAttributes },
            { "path": "/databases", "component": DataCollections },
            { "path": "/home", "component": Home },
            { "path": "/udf", "component": UDF },
            { "path": "/manageLicense", "component": License },
            { "path": "/manageNavigation", "component": ManageNavigation },
            { "path": "/manageRoles", "component": ManageRoles },
            { "path": "/manageUsers", "component": ManageUsers },
            { "path": "/page/:id/:uniqueId?/:uniqueId2?/:uniqueId3?", "component": PagePreview },
            { "path": "/dashboards/:type?/:model?", "component": PageStudio },
            { "path": "/policyAddEdit/:id?", "component": PolicyAddEdit },
            { "path": "/policyManager", "component": PolicyList },
            { "path": "/previewPage/:id/:uniqueId?/:uniqueId2?/:uniqueId3?", "component": PagePreview },
            { "path": "/reportBuilder", "component": ReportBuilder },
            { "path": "/simulation", "component": SimulationStudio },
            { "path": "/summary", "component": TenantDetails },
            { "path": "/tenantDetails/:tenantName/:id", "component": TenantDetails },
            { "path": "/tenantList", "component": TenantList },
            { "path": "/etlPipeline", "component": Transformations },
            { "path": "/ticketManagement/:ticketId?/:id?", "component": TicketManagement },
            { "path": "/viewRolesNavigation", "component": RoleNavigation },
            { "path": "/viewAlarmsAndAlerts", "component": ViewAlarmsAndAlerts },
            { "path": "/pages", "component": WidgetStudio },
            { "path": "/logsManager", "component": ManagePodLogs },
            { "path": "/mlExperiments", "component": MlStudio },
            { "path": "/addOrEditMlOld/:id?", "component": AddOrEditMl },
            { "path": "/reports", "component": ManageReports },
            { "path": "/addOrEditReports", "component": AddOrEditReports },
            { "path": "/userGroups", "component": ManageGroup },
            { "path": "/projects", "component": ProjectList },
            { "path": "/oAuthConfigurations", "component": OAuthConfigurations },
            { "path": "/auditLogs", "component": AuditLogs },
            { "path": "/node-red", "component": IFrameComponent },
            { "path": "/grafana", "component": IFrameComponent },
            { "path": "/jupyter", "component": IFrameComponent },
            { "path": "/etlDiagnostics", "component": EtlDiagnostic },
            { "path": "/notifications", "component": ManageNotification },
            { "path": "/systemProperties", "component": SystemProperties },
            { "path": "/myAccount", "component": MyAccount },
            { "path": "/myPlan", "component": MyPlan },
            { "path": "/devices/:connectorId?/:groupId?", "component": DeviceManagement },
            { "path": "/events/:id?", "component": Events },
            { "path": "/addOrEditEvent/:id?", "component": AddOrEditEvent },
            { "path": "/deviceGroups/:connectorId?/:groupId?", "component": DeviceGroups },
            { "path": "/deviceDiagnostics/:connectorId?/:deviceId?", "component": DeviceDiagnostics },
            { "path": "/eventHistory", "component": DeviceDashboardsEventHistory },
            { "path": "/deviceMap", "component": DeviceDashboardsDeviceMap },
            { "path": "/deviceList", "component": DeviceList },
            { "path": "/deviceAggregate", "component": DeviceDashboardsAggregate },
            { "path": "/deviceAlarms", "component": DeviceDashboardsAlarms },
            { "path": "/deviceDetails/:deviceTypeId/:deviceId", "component": DeviceDashboardsDeviceDetails },
            { "path": "/deviceInfo/:deviceTypeId/:deviceId", "component": DeviceDashboardsDeviceDetails },
            { "path": "/uptimeInfo", "component": DeviceDashboardsConnectionHistory },
            { "path": "/trendsAndForecast", "component": DeviceDashboardsTrendsForecast }, //not being used in iflex or flex
            { "path": "/manageImages", "component": ManageImages },
            { "path": "/dockerImages", "component": ManageDockerImages },
            { "path": "/notebooks", "component": NoteBooks },
            { "path": "/addOrEditNoteBook/:id?", "component": AddOrEditNoteBook },
            { "path": "/alarmConfig", "component": AlarmConfig },
            { "path": "/actions/:actionType?/:actionId?", "component": ActionList },
            { "path": "/manageAddOns", "component": ManageAddOn },
            { "path": "/schedules", "component": Scheduler },
            { "path": "/applications", "component": ManageCustomMenu },
            { "path": "/addOrEditApplication/:id?", "component": AddOrEditCustomMenu },
            { "path": "/manageGitPods", "component": ManageGitPod },
            { "path": "/manageDockerCreds", "component": ManageDockerCred },
            { "path": "/manageServices", "component": ManageServices },
            { "path": "/addOrEditService", "component": AddOrEditService },
            { "path": "/manageFlows", "component": ManageFlows },
            { "path": "/analyticsManager", "component": AnalyticsManager },
            { "path": "/etlSchedules", "component": ManageEtlSchedules },
            { "path": "/addOrEditMlExperiments/:id?", "component": NewAddOrEditMl },
        ],
        fetchingProjects: false,
        sideNavLoading: false,
        fetchingApplications: false,
        sideNav: [],
        projects: [],
        timeZone: localStorage.timeZone ? localStorage.timeZone : Intl.DateTimeFormat().resolvedOptions().timeZone,
        routes: [
            { "path": "/forbidden", "component": ForbiddenPage },
            { "path": "/", "component": Loader },
            { "path": "/changePassword", "component": ChangePassword },
            { "path": "/myActivities", "component": MyActivities },
            { "path": "/settings", "component": Setting },
            { "path": "/dashboard/:id/:param1?/:param2?", "component": PreviewPage },
            { "path": "/noMenusAssigned", "component": NoNavigationAssigned },
            { "path": "/locationTracking/:deviceTypeId/:deviceId", "component": LocationTracker },
            /*Temporary Routes - Please remove later*/
            { "path": "/logs", "components": Logs },
            { "path": "/customizeAttribute", "component": CustomizeAttribute },
            { "path": "/addOrEditMlOld/:id?", "component": AddOrEditMl },
            { "path": "/manageDiscounts", "component": ManageDiscounts },
            { "path": "/manageRepositories", "component": ManageRepositories },
            { "path": "/trendsAndForecast", "component": DeviceDashboardsTrendsForecast },
            { "path": "/dlExperiments", "component": DLExperiments},
            { "path": "/addOrEditDlExperiments/:id?", "component": AddOrEditDLExperiment},
        ],
        defaultRoutesApp: [
            { "path": "/forbidden", "component": ForbiddenPage },
            { "path": "/", "component": Loader },
        ],
        noNavAssign: [
            { "path": "/changePassword", "component": ChangePassword },
            { "path": "/myActivities", "component": MyActivities },
            { "path": "/", "component": NoNavigationAssigned },
            { "path": "/noApplicationsFound", "component": ApplicationsNotFound },
            { "path": "/projects", "component": ProjectList },
            { "path": "/oAuthConfigurations", "component": OAuthConfigurations },
            { "path": "/licenseExpiry", "component": LicenseExpiry },
            { "path": "/settings", "component": Setting },
            { "path": "/privacyPolicy", "component": PrivacyPolicy },
            { "path": "/termsAndConditions", "component": TermsAndConditions },
            { "path": "/myPlan", "component": MyPlan },
        ],
        isGitHubLogin: false,
        sourceControlModal: false,
        oAuthProviderList: (localStorage.oAuthProviders && localStorage.oAuthProviders !== "undefined") ? JSON.parse(localStorage.oAuthProviders) : [],
        theme: theme,
        isSmallSideNav: false,
        isJiraLoggedIn: false,
        isFetchedValidity: false,
        noRedirection: false,
        accounts: [],
    }

    componentDidMount() {
        localStorage.setItem('timeZone', this.state.timeZone);
        if (localStorage.isVerified === 'true') {
            if (localStorage.role === "SYSTEM_ADMIN") {
                this.setState({
                    sideNavLoading: true,
                }, () => this.props.fetchSideNav());
            } else {
                this.props.getAllAccounts();
                if (window.location.pathname !== '/projects') {
                    this.setState({
                        fetchingProjects: true,
                    }, () => { this.props.getAllProjects() });
                } else {
                    if (typeof localStorage.selectedProjectId !== 'undefined') {
                        this.setState({
                            sideNavLoading: true,
                        }, () => this.props.fetchSideNav());
                    }
                }

            }
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.vmqCredentials && nextProps.vmqCredentials !== this.props.vmqCredentials) {
            localStorage.VMQ_Credentials = JSON.stringify(nextProps.vmqCredentials);
        }

        if (nextProps.vmqCredentialsFailure && nextProps.vmqCredentialsFailure !== this.props.vmqCredentialsFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                isFetching: false,
                message2: nextProps.vmqCredentialsFailure.message,
            })
        }

        if (nextProps.oAuthStatus && nextProps.oAuthStatus !== this.props.oAuthStatus) {
            localStorage.isGitHubLogin = nextProps.oAuthStatus.gitHub
            localStorage.isJiraLoggedIn = nextProps.oAuthStatus.jira
            this.setState({
                isGitHubLogin: nextProps.oAuthStatus.gitHub,
                isJiraLoggedIn: nextProps.oAuthStatus.jira,
                isFetchedValidity: true
            })
        }

        if (nextProps.oAuthStatusFailure && nextProps.oAuthStatusFailure !== this.props.oAuthStatusFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                isFetching: false,
                message2: nextProps.oAuthStatusFailure.message,
            })
        }

        if (nextProps.sideNav && nextProps.sideNav !== this.props.sideNav) {

            if (!localStorage.VMQ_Credentials && localStorage.role !== 'SYSTEM_ADMIN') {
                this.props.getVMQCredentials();
            }
            let sideNav = nextProps.sideNav;
            if (sideNav.filter(nav => nav.navName !== "Help").length === 0) {
                sideNav = [];
            }
            localStorage.setItem('sideNav', JSON.stringify(sideNav));
            let routes = [...this.state.routes], findComponent;
            if (localStorage.tenantType === "FLEX") {
                routes.push({ "path": "/myPlan", "component": MyPlan })
            }
            else if (localStorage.tenantType === "MAKER") {
                routes.push({ "path": "/projects", "component": ProjectList },
                    { "path": "/myAccount", "component": MyAccount })
            }
            sideNav.map((nav) => {
                nav.connectedRoutes.map(route => {
                    findComponent = this.state.adminRoutes.find(temp => temp.path.includes(route))
                    findComponent && routes.push(findComponent);
                })
                if (nav.subMenus && nav.subMenus.length > 0) {
                    nav.subMenus.map(menu => {
                        menu.connectedRoutes.map(route => {
                            findComponent = this.state.adminRoutes.find(temp => temp.path.includes(route))
                            findComponent && routes.push(findComponent);
                        })
                        findComponent = this.state.adminRoutes.find(temp => temp.path.includes(menu.url))
                        findComponent && routes.push(findComponent);
                    })
                } else {
                    findComponent = this.state.adminRoutes.find(temp => temp.path.includes(nav.url))
                    findComponent && routes.push(findComponent);
                }
            });
            let systemAdminRoutes = [{ "path": "/createFaq/:id?", "component": CreateFaq }, { "path": "/home", "component": Home }]

            localStorage.role === "SYSTEM_ADMIN" ? routes = [...routes, ...systemAdminRoutes] : routes.push({ "path": "/summary", "component": TenantDetails });

            let isRedirect = (this.props.location.pathname === "/" || this.props.location.pathname === "/login" || this.props.location.pathname === "/resetPassword" || this.props.location.pathname === "/reset/password") ? true : false,
                navUrl;

            if (isRedirect) {
                if (localStorage.tenantType === 'MAKER') {
                    if (localStorage.role === 'ACCOUNT_ADMIN') {
                        navUrl = 'projects'
                    } else {
                        navUrl = 'myAccount'
                    }
                } else {
                    if (localStorage.role === 'ACCOUNT_ADMIN') {
                        navUrl = 'myPlan'
                    } else {
                        navUrl = sideNav.length > 0 ? sideNav[0].subMenus.length > 0 ? sideNav[0].subMenus[0].url : sideNav[0].url : 'noMenusAssigned';
                    }
                }
            } else {
                navUrl = sideNav.length > 0 ? sideNav[0].subMenus.length > 0 ? sideNav[0].subMenus[0].url : sideNav[0].url : 'noMenusAssigned';
            }



            this.setState({
                sideNavLoading: false,
                sideNav,
                routes
            }, () => { isRedirect && this.props.history.push(`/${navUrl}`) })
        }

        if (nextProps.sideNavError && nextProps.sideNavError !== this.props.sideNavError) {
            localStorage.setItem('sideNav', JSON.stringify([]));
            let routes = [...this.state.routes], findComponent;
            let systemAdminRoutes = [{ "path": "/createFaq/:id?", "component": CreateFaq }, { "path": "/home", "component": Home }]
            localStorage.role === "SYSTEM_ADMIN" ? routes = [...routes, ...systemAdminRoutes] : routes.push({ "path": "/summary", "component": TenantDetails });
            if (localStorage.tenantType === "FLEX") {
                routes.push({ "path": "/myPlan", "component": MyPlan })
            }
            else if (localStorage.tenantType === "MAKER") {
                routes.push({ "path": "/projects", "component": ProjectList },
                    { "path": "/myAccount", "component": MyAccount })
            }
            this.setState({
                isFetching: false,
                noRedirection: false,
                sideNavLoading: false,
                sideNav: [],
                routes,
            }, () => {
                if (localStorage.tenantType === "FLEX") {
                    this.props.history.push('/myPlan')
                }
                else if (localStorage.tenantType === "MAKER") {
                    this.props.history.push('/myAccount')
                }
            });
        }


        if (nextProps.projectsSuccess && nextProps.projectsSuccess !== this.props.projectsSuccess) {
            if (window.location.pathname !== '/projects') {
                localStorage['myProjects'] = JSON.stringify(nextProps.projectsSuccess);

                if (nextProps.projectsSuccess.length > 0) {
                    localStorage['selectedProjectId'] = nextProps.projectsSuccess[0].id;
                }
                this.setState({
                    fetchingProjects: false,
                    sideNavLoading: true,
                    projects: nextProps.projectsSuccess,
                }, () => this.props.fetchSideNav());


            } else {
                this.setState({
                    fetchingProjects: false,
                    projects: nextProps.projectsSuccess,

                });
            }
        }

        if (nextProps.projectsFailure && nextProps.projectsFailure !== this.props.projectsFailure) {
            if (jwt_decode(localStorage.token).defaultAccountAdmin && localStorage.tenantType === 'FLEX') {
                if (nextProps.projectsFailure.code === 402) {
                    this.setState({
                        isLicenseExpired: true,
                        fetchingProjects: false,
                        modalType: "error",
                        isOpen: true,
                        message2: nextProps.projectsFailure.message,
                    }, () => this.props.history.push('/myPlan'));
                }
            }

        }
        if (nextProps.oAuthLogoutSuccess && nextProps.oAuthLogoutSuccess !== this.props.oAuthLogoutSuccess) {
            if (nextProps.oAuthLogoutSuccess.name === "gitHub") {
                delete localStorage['isGitHubLogin'];
            } else {
                delete localStorage['isJiraLoggedIn'];
            }
            this.setState((preState, props) => ({
                isGitHubLogin: nextProps.oAuthLogoutSuccess.name === "gitHub" ? false : preState.isGitHubLogin,
                isJiraLoggedIn: nextProps.oAuthLogoutSuccess.name === "jira" ? false : preState.isJiraLoggedIn,
            }))
        }

        if (nextProps.oAuthLogoutError && nextProps.oAuthLogoutError !== this.props.oAuthLogoutError) {

        }
        if (nextProps.developerQuotaSuccess && nextProps.developerQuotaSuccess !== this.props.developerQuotaSuccess) {
            localStorage['Developer_Usage_Quota'] = JSON.stringify(nextProps.developerQuotaSuccess);
        }

        if (nextProps.developerQuotaError && nextProps.developerQuotaError !== this.props.developerQuotaError) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.developerQuotaError.message,
            })
        }
        if (nextProps.allAccountsSuccess && nextProps.allAccountsSuccess !== this.props.allAccountsSuccess) {
            this.setState({
                accounts: nextProps.allAccountsSuccess.map(el => { return { value: el, label: el } }),
            })
        }

        if (nextProps.allAccountsError && nextProps.allAccountsError !== this.props.allAccountsError) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.allAccountsError.message,
            })
        }

        if (nextProps.handle402 && nextProps.handle402 !== this.props.handle402) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.handle402.message,
                isLicenceExpired: true
            })
        }

        if (nextProps.showToast && nextProps.showToast !== this.props.showToast) {
            this.showNotification(nextProps.showToast.showToastType, nextProps.showToast.showToastMessage)        
        }
    }

    componentWillUnmount() {
        if (client && client.connected) {
            client.unsubscribe({
                channels: ['pushNotification', 'policyNotification']
            })
        }
    }


    toggleWizard(action) {
        if (action == 'show') {
            $('.wizardBox').toggle().addClass('slideInRight');
        } else {
            $(".wizardBox").animate({ width: "toggle" }, 300);
        }
    }

    onCloseHandler = () => {
        if (this.state.isLicenceExpired) {
            this.props.history.push("/")
        }
        this.setState({
            isOpen: false,
            message2: '',
            modalType: "",
            pubnubVal: false,
            isLicenceExpired: false
        })
    }

    oAuthProviderStateChange = (oAuthProviderList) => {
        this.setState({ oAuthProviderList }, () => {
            localStorage.setItem("oAuthProviders", JSON.stringify(oAuthProviderList));
            if (!this.state.oAuthProviderList.length)
                this.props.oauthLogoutHandler("gitHub")
        });
    }

    getKey = () => {
        return window.btoa(JSON.stringify({ token: localStorage.getItem('token'), referer: window.location.href }))
    }

    isUsedForEnabled = () => {
        let oAuthList = localStorage.oAuthProviders ? JSON.parse(localStorage.getItem("oAuthProviders")) : [], matchCounter = 0;
        if (oAuthList.length === 0) {
            return false;
        } else {
            let gitHubOauth = oAuthList.find(provider => provider.name === 'gitHub')
            let usedForList = gitHubOauth ? gitHubOauth.usedFor.split(',') : [];
            let pathName = window.location.pathname.toLowerCase();
            pathName = pathName.includes("codeengine") ? 'faas' : pathName;
            for (var i = 0; i < usedForList.length; i++) {
                if (pathName.indexOf(usedForList[i]) > -1) {
                    matchCounter++;
                }
            }
            return (matchCounter > 0);
        }
    }

    hasProjectAccess = () => {
        let projectList = localStorage.myProjects ? JSON.parse(localStorage.myProjects) : [], isAccess;
        if (localStorage.selectedProjectId) {
            let requiredProject = projectList.find(project => project.id == localStorage.selectedProjectId);
            isAccess = requiredProject && requiredProject.repoName && requiredProject.sourceControlEnabled && this.isUsedForEnabled();
        } else {
            isAccess = false;
        }
        let oAuthList = localStorage.oAuthProviders ? JSON.parse(localStorage.getItem("oAuthProviders")) : []
        if (oAuthList.length > 0 && (localStorage.role === "ACCOUNT_ADMIN" || isAccess)) {
            return true;
        } else
            return false;
    }

    toggleSwitchSideBar = (isSmallSideNav) => {
        this.setState({
            isSmallSideNav
        })
    }

    getHref = (Oauth) => {
        switch (Oauth.name) {
            case "gitHub":
                return `${Oauth.authorizationUri}?client_id=${Oauth.clientId}&state=${this.getKey()}&scope=${Oauth.scope}&redirect_uri=${Oauth.redirectUri}`;;
            case "jira":
                return `${Oauth.authorizationUri}&client_id=${Oauth.clientId}&state=${this.getKey()}&scope=${Oauth.scope}&response_type=code&prompt=consent&redirect_uri=${Oauth.redirectUri}`;
        }
    }

    showNotification = (type, message, onClose, position) => {
        let config = {
            position: position || "bottom-right",
            autoClose: 2500,
            hideProgressBar: false,
            closeOnClick: true,
            draggable: true,
            onClose,
        };

        let getNotification = (msg) => {
            let iconClass = type === 'success' ? "check" : "exclamation-circle"
            let html = <p><i className={`fas fa-${iconClass}`}></i><span className="mr-7">{msg}</span></p>
            toast[type](html, config)
        }
        if (Array.isArray(message)) {
            message.map(messageObj => {
                getNotification(messageObj)
            });
        } else {
            getNotification(message)
        }
    }

    getComponent = ({ component, path }, props) => {
        let Comp = component;
        switch (path) {
            case "/oAuthConfigurations":
                return <Comp {...props} oAuthStateChange={this.oAuthProviderStateChange} showNotification={this.showNotification} showConfirmationModal={this.showConfirmationModal} />;
            case "/page/:id/:uniqueId?/:uniqueId2?/:uniqueId3?":
            case "/settings":
                return <Comp {...props} showNotification={this.showNotification} showConfirmationModal={this.showConfirmationModal} fetchSideNav={() => this.props.fetchSideNav()} />;
            case "/projects":
                return <Comp {...props} showNotification={this.showNotification} showConfirmationModal={this.showConfirmationModal} />
            case "/ticketManagement/:ticketId?/:id?":
                return <Comp {...props} isFetchedValidity={this.state.isFetchedValidity} showNotification={this.showNotification} showConfirmationModal={this.showConfirmationModal} />
            default:
                return <Comp {...props} showNotification={this.showNotification} showConfirmationModal={this.showConfirmationModal} fetchSideNav={() => this.props.fetchSideNav()} timeZone={this.state.timeZone} />
        }
    }

    showConfirmationModal = (confirmationMessage, confirmationHandler, cancelClickHandler, confirmationType = "delete") => {
        this.confirmationHandler = () => {
            this.setState({
                confirmModalVisibility: false,
            }, confirmationHandler)
        }
        this.cancelClickHandler = () => {
            this.setState({
                confirmModalVisibility: false,
            }, cancelClickHandler)
        }
        this.setState({
            confirmationMessage,
            confirmModalVisibility: true,
            confirmationType
        })
    }

    accountChangeHandler = ({ value }) => {
        let baseUrl = window.API_URL.split('.')[1],
            url = `https://${value}.${baseUrl}.com${window.location.pathname}`;
        window.open(url, "_blank");
    }

    render() {
        return (
            <React.Fragment>
                {this.state.sideNavLoading || this.state.fetchingProjects ? <Loader /> :
                    <div className={cx("content-wrapper", { 'content-wrapper-active': this.state.isSmallSideNav, })}>
                        <Header {...this.props} sideNav={this.state.sideNav} toggleSwitchSideBar={this.toggleSwitchSideBar} setSideNav={this.setSideNav} />
                        <div className={"content-item"}>
                            <ErrorBoundary>
                                {typeof localStorage.selectedProjectId === "undefined" && this.state.sideNav.length === 0 ?
                                    <Switch>
                                        {this.state.noNavAssign.map((route, routeIndex) => {
                                            return (
                                                <Route
                                                    key={routeIndex}
                                                    exact
                                                    path={route.path}
                                                    render={props => this.getComponent(route, props)}
                                                />
                                            );
                                        })}
                                        <Route component={NoNavigationAssigned} />
                                    </Switch>
                                    :
                                    <Switch>
                                        {this.state.routes.map((route, routeIndex) => {
                                            return (
                                                <Route
                                                    key={routeIndex}
                                                    exact
                                                    path={route.path}
                                                    render={props => this.getComponent(route, props)}
                                                />
                                            );
                                        })}
                                        <Route component={NotFoundPage} />
                                    </Switch>
                                }
                            </ErrorBoundary>
                        </div>

                        {/*<React.Fragment>
                            {this.hasProjectAccess() && this.state.oAuthProviderList.length > 0 &&
                                <div className="versionControlBox">
                                    {this.state.sourceControlModal &&
                                    <ul className="versionList">
                                        {this.state.oAuthProviderList.find(temp => temp.name === "jira") && window.location.pathname === "/ticketManagement" ?
                                            <li >
                                                <a href={this.getHref(this.state.oAuthProviderList.find(temp => temp.name === "jira"))} id="github-button">
                                                    <div className="versionImage">
                                                        <img src="https://content.iot83.com/m83/tenant/jira.png" />
                                                    </div>
                                                    <h5>jira</h5>
                                                </a>
                                            </li>
                                            :
                                            this.state.oAuthProviderList.filter(temp => temp.name != "jira").map((temp, index) => {
                                                return <li key={index}>
                                                    <a href={this.getHref(temp)} id="github-button">
                                                        <div className="versionImage">
                                                            <img src={`https://content.iot83.com/m83/tenant/${temp.name.trim()}.png`} />
                                                        </div>
                                                        <h5>{temp.name}</h5>
                                                    </a>
                                                </li>
                                            })
                                        }
                                    </ul>
                                    }
                                </div>
                            }
                        </React.Fragment>*/}

                        <footer className="footer">
                            <ul className="list-style-none d-flex">
                                <li>&copy; IoT83, All rights reserved. Version- {process.env.BUILD_VERSION}</li>
                                <li><a href="https://iot83.com/aboutUs.html" target="_blank"><i className="fas fa-users mr-1"></i> About Us</a></li>
                                <li><a href={localStorage.tenantType == "MAKER" ? "mailto:support@iflex83.com" : "mailto:support@flex83.com"}><i className="fas fa-envelope mr-1"></i> Contact Us</a></li>
                                <li><a href="/privacyPolicy" target="_blank"><i className="fas fa-user-lock mr-1"></i> Privacy Policy</a></li>
                            </ul>
                            <div className="form-group-block">
                                {this.state.accounts.length > 1 &&
                                <div className="form-group">
                                    <label className="form-group-label d-inline-block m-0">Switch Accounts :</label>
                                    <Select
                                        className="form-control-multi-select d-inline-block"
                                        value={this.state.accounts.find(el => el.value == localStorage.tenant)}
                                        onChange={this.accountChangeHandler}
                                        options={this.state.accounts}
                                        menuPlacement={"top"}
                                    />
                                    <span className="ml-2 mr-2 text-gray">|</span>
                                </div>
                                }
                                <div className="form-group">
                                    <label className="form-group-label d-inline-block m-0">TimeZone :</label>
                                    <Select
                                        className="form-control-multi-select d-inline-block"
                                        value={timeZoneArray.find(el => el.value == this.state.timeZone)}
                                        onChange={({ value }) => {
                                            localStorage.setItem('timeZone', value);
                                            this.setState({ timeZone: value })
                                        }}
                                        options={timeZoneArray}
                                        menuPlacement={"top"}
                                    />
                                    <button type="button" className="btn-transparent btn-transparent-green d-inline-block" data-tooltip data-tooltip-text="Reset" data-tooltip-place="bottom"
                                            onClick={() => {
                                                localStorage.setItem('timeZone', Intl.DateTimeFormat().resolvedOptions().timeZone);
                                                this.setState({ timeZone: Intl.DateTimeFormat().resolvedOptions().timeZone })}}
                                    >
                                        <i className="far fa-redo-alt"></i>
                                    </button>
                                </div>
                            </div>
                        </footer>
                    </div>
                }


                {
                    this.state.isOpen &&
                    <NotificationModal
                        type={this.state.modalType}
                        isPubnub={this.state.pubnubVal}
                        message2={this.state.message2}
                        onCloseHandler={this.onCloseHandler}
                    />
                }

                <ToastContainer
                    position="bottom-right"
                    autoClose={2500}
                    newestOnTop={false}
                    closeOnClick
                    rtl={false}
                    pauseOnFocusLoss
                    draggable
                    pauseOnHover
                />

                {this.state.confirmModalVisibility && <ConfirmModel
                    status={this.state.confirmationType}
                    title={this.state.confirmationType.charAt(0).toUpperCase() + this.state.confirmationType.slice(1) + "!"}
                    deleteName={this.state.confirmationMessage}
                    confirmClicked={this.confirmationHandler}
                    cancelClicked={this.cancelClickHandler}
                />}

            </React.Fragment >
        );
    }
}

HomePage.propTypes = {
    dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
    sideNav: SELECTORS.getSideNav(),
    sideNavError: SELECTORS.getSideNavError(),
    vmqCredentials: SELECTORS.vmqCredentialsSuccess(),
    vmqCredentialsFailure: SELECTORS.vmqCredentialsFailure(),
    applications: SELECTORS.getApplications(),
    applicationsFailure: SELECTORS.getApplicationsFailure(),
    oAuthStatus: SELECTORS.getOAuthStatus(),
    oAuthStatusFailure: SELECTORS.getOauthStatusFailure(),
    projectsSuccess: SELECTORS.projectsSuccess(),
    projectsFailure: SELECTORS.projectsFailure(),
    oAuthLogoutSuccess: SELECTORS.getOauthLogoutSuccess(),
    oAuthLogoutError: SELECTORS.getOauthLogoutError(),
    developerQuotaSuccess: SELECTORS.developerQuotaSuccess(),
    developerQuotaError: SELECTORS.developerQuotaError(),
    allAccountsSuccess: SELECTORS.allAccountsSuccess(),
    allAccountsError: SELECTORS.allAccountsError(),
    handle402: SELECTORS.handle402(),
    showToast: SELECTORS.showToast(),
});

function mapDispatchToProps(dispatch) {
    return {
        dispatch,
        fetchSideNav: () => dispatch(ACTIONS.fetchSideNav()),
        getVMQCredentials: () => dispatch(ACTIONS.getVMQCredentials()),
        getApplicationsByUser: () => dispatch(ACTIONS.getApplicationsByUser()),
        getOauthStatus: () => dispatch(ACTIONS.getOauthStatus()),
        getAllProjects: () => dispatch(ACTIONS.getAllProjects()),
        oauthLogoutHandler: (name) => dispatch(ACTIONS.oauthLogoutHandler(name)),
        getDeveloperQuota: () => dispatch(ACTIONS.getDeveloperQuota()),
        getAllAccounts: () => dispatch(ACTIONS.getAllAccounts()),
    };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'homePage', reducer });
const withSaga = injectSaga({ key: 'homePage', saga });

export default compose(
    withReducer,
    withSaga,
    withConnect,
)(HomePage);
