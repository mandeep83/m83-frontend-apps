/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { takeEvery } from 'redux-saga';
import { apiCallHandler } from '../../api';
import * as CONSTANTS from "./constants";

export function* getAllRolesApiHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_ALL_ROLES_SUCCESS, CONSTANTS.GET_ALL_ROLES_FAILURE, 'getApplicationRoleList')];
}

export function* watcherGetAllRolesRequest() {
  yield takeEvery(CONSTANTS.GET_ALL_ROLES, getAllRolesApiHandlerAsync);
}

export function* getAllPagesListApiHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_PAGES_LIST_SUCCESS, CONSTANTS.GET_PAGES_LIST_ERROR, 'getParentPages')];
}

export function* getAllNavsForSelectedRoles(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_ALL_NAVS_SUCCESS, CONSTANTS.GET_ALL_NAVS_FAILURE, 'getAllNavsForSelectedRole')];
}

export function* watcherGetAllPagesListRequest() {
  yield takeEvery(CONSTANTS.GET_PAGES_LIST, getAllPagesListApiHandlerAsync);
}

export function* saveApplicationApiHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.SAVE_UPDATE_APPLICATION_SUCCESS, CONSTANTS.SAVE_UPDATE_APPLICATION_FAILURE, 'saveApplication')];
}

export function* watcherApplicationApiHandlerAsync() {
  yield takeEvery(CONSTANTS.SAVE_UPDATE_APPLICATION, saveApplicationApiHandlerAsync);
}

export function* getApplicationDetailsApiHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_APPLICATION_DETAILS_SUCCESS, CONSTANTS.GET_APPLICATION_DETAILS_FAILURE, 'getApplications')];
}

export function* watcherApplicationDetailsApiHandlerAsync() {
  yield takeEvery(CONSTANTS.GET_APPLICATION_DETAILS, getApplicationDetailsApiHandlerAsync);
}

export function* watcherGetAllNavsForSelectedRoles() {
  yield takeEvery(CONSTANTS.GET_ALL_NAVS, getAllNavsForSelectedRoles);
}

export function* imageUploadRequestHandler(action) {
  yield [apiCallHandler(action, CONSTANTS.IMAGE_UPLOAD_REQUEST_SUCCESS, CONSTANTS.IMAGE_UPLOAD_REQUEST_FAILURE, 'fileUploadRequest', false)]
}

export function* watcherImageUploadRequest() {
  yield takeEvery(CONSTANTS.IMAGE_UPLOAD_REQUEST, imageUploadRequestHandler)
}


export function* mqttConnectorListHandlerAsync(action) {
  yield[apiCallHandler(action, CONSTANTS.MQTT_CONNECTOR_SUCCESS, CONSTANTS.MQTT_CONNECTOR_FAILURE, 'deviceTypeList')];
}

export function* watcherMqttConnectorList() {
  yield takeEvery(CONSTANTS.MQTT_CONNECTOR, mqttConnectorListHandlerAsync);
}


export default function* rootSaga() {
  yield [
    watcherGetAllRolesRequest(),
    watcherGetAllPagesListRequest(),
    watcherApplicationApiHandlerAsync(),
    watcherApplicationDetailsApiHandlerAsync(),
    watcherGetAllNavsForSelectedRoles(),
    watcherImageUploadRequest(),
    watcherMqttConnectorList()
  ]
}

