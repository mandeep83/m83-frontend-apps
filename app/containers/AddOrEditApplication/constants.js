/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

export const DEFAULT_ACTION = "app/AddOrEditApplication/DEFAULT_ACTION";

export const GET_PAGES_LIST = 'app/AddOrEditApplication/GET_PAGES_LIST';
export const GET_PAGES_LIST_SUCCESS = 'app/AddOrEditApplication/GET_PAGES_LIST_SUCCESS';
export const GET_PAGES_LIST_ERROR = 'app/AddOrEditApplication/GET_PAGES_LIST_ERROR';

export const GET_ALL_ROLES = "app/AddOrEditApplication/GET_ALL_ROLES";
export const GET_ALL_ROLES_SUCCESS = "app/AddOrEditApplication/GET_ALL_ROLES_SUCCESS";
export const GET_ALL_ROLES_FAILURE = "app/AddOrEditApplication/GET_ALL_ROLES_FAILURE";

export const SAVE_UPDATE_APPLICATION = "app/AddOrEditApplication/SAVE_UPDATE_APPLICATION";
export const SAVE_UPDATE_APPLICATION_SUCCESS = "app/AddOrEditApplication/SAVE_UPDATE_APPLICATION_SUCCESS";
export const SAVE_UPDATE_APPLICATION_FAILURE = "app/AddOrEditApplication/SAVE_UPDATE_APPLICATION_FAILURE";

export const GET_APPLICATION_DETAILS = "app/AddOrEditApplication/GET_APPLICATION_DETAILS";
export const GET_APPLICATION_DETAILS_SUCCESS = "app/AddOrEditApplication/GET_APPLICATION_DETAILS_SUCCESS";
export const GET_APPLICATION_DETAILS_FAILURE = "app/AddOrEditApplication/GET_APPLICATION_DETAILS_FAILURE";

export const GET_ALL_NAVS = "app/AddOrEditApplication/GET_ALL_NAVS";
export const GET_ALL_NAVS_SUCCESS = "app/AddOrEditApplication/GET_ALL_NAVS_SUCCESS";
export const GET_ALL_NAVS_FAILURE = "app/AddOrEditApplication/GET_ALL_NAVS_FAILURE";

export const IMAGE_UPLOAD_REQUEST = "app/AddOrEditApplication/IMAGE_UPLOAD_REQUEST";
export const IMAGE_UPLOAD_REQUEST_SUCCESS = "app/AddOrEditApplication/IMAGE_UPLOAD_REQUEST_SUCCESS";
export const IMAGE_UPLOAD_REQUEST_FAILURE = "app/AddOrEditApplication/IMAGE_UPLOAD_REQUEST_FAILURE";

export const MQTT_CONNECTOR = "app/AddOrEditApplication/MQTT_CONNECTOR";
export const MQTT_CONNECTOR_SUCCESS = "app/AddOrEditApplication/MQTT_CONNECTOR_SUCCESS";
export const MQTT_CONNECTOR_FAILURE = "app/AddOrEditApplication/MQTT_CONNECTOR_FAILURE";
