/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
 * Direct selector to the addOrEditApplication state domain
 */

const selectAddOrEditApplicationBuilderDomain = state =>
  state.get("addOrEditApplication", initialState);

export const isFetchingState = state => state.get('loader');

export const getIsFetching = () => createSelector(isFetchingState, substate => substate.get('isFetching'));

export const getRoles = () => createSelector(selectAddOrEditApplicationBuilderDomain, subState => subState.roles);
export const getRolesFailure = () => createSelector(selectAddOrEditApplicationBuilderDomain, subState => subState.rolesError);

export const getPagesList = () => createSelector(selectAddOrEditApplicationBuilderDomain, substate => substate.pagesList);
export const getPagesListError = () => createSelector(selectAddOrEditApplicationBuilderDomain, lookUpState => lookUpState.pagesListError);

export const saveApplicationSuccess = () => createSelector(selectAddOrEditApplicationBuilderDomain, substate => substate.applicationSuccess);
export const saveApplicationError = () => createSelector(selectAddOrEditApplicationBuilderDomain, substate => substate.applicationError);

export const applicationDetails = () => createSelector(selectAddOrEditApplicationBuilderDomain, substate => substate.getApplicationDetailsSuccess);
export const applicationDetailsError = () => createSelector(selectAddOrEditApplicationBuilderDomain, substate => substate.getApplicationDetailsError);

export const navList = () => createSelector(selectAddOrEditApplicationBuilderDomain, substate => substate.navList);
export const navListError = () => createSelector(selectAddOrEditApplicationBuilderDomain, substate => substate.navListError);

export const getImageUploadSuccess = () => createSelector(selectAddOrEditApplicationBuilderDomain, subState => subState.imageUploadSuccess);
export const getImageUploadFailure = () => createSelector(selectAddOrEditApplicationBuilderDomain, subState => subState.imageUploadFailure);

export const getConnectorsListSuccess = () => createSelector(selectAddOrEditApplicationBuilderDomain, subState => subState.getConnectorsListSuccess);
export const getConnectorsListError = () => createSelector(selectAddOrEditApplicationBuilderDomain, subState => subState.getConnectorsListFailure);

