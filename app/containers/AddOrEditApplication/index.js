/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import RGL, { WidthProvider } from "react-grid-layout";
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import * as SELECTORS from './selectors'
import * as ACTIONS from './actions';
import reducer from "./reducer";
import saga from "./saga";
import NotificationModal from '../../components/NotificationModal/Loadable';
import Loader from '../../components/Loader/Loadable';
import FontAwesomeDisplayList from "../../components/FontAwesomeDisplayList";
import ReactSelect from "react-select";
import SlidingPane from "react-sliding-pane";
import Modal from 'react-modal';
import ReactTooltip from "react-tooltip";
import jwt_decode from "jwt-decode";
import { Link } from "react-router-dom";
import cloneDeep from "lodash/cloneDeep";
import ConfirmModel from '../../components/ConfirmModel/Loadable'
const ReactGridLayout = WidthProvider(RGL);

/* eslint-disable react/prefer-stateless-function */
export class AddOrEditApplication extends React.Component {
    state = {
        isFetching: true,
        isOpen: false,
        rolesList: [],
        applicationList: [],
        pages: [],
        filteredPages: [],
        availablePages: [],
        payload: {
            name: "",
            description: "",
            collapsedView: false,
            roleIds: [],
            pageIds: [],
            isPublished: false,
            appVersion: '',
            isPublicApplication: false,
            logoImageUrl: '',
            style: {
                themeHeader: "",
                themeSideBar: "",
                themeSideBarTextColor: "",
                themeHeaderTextColor: ""
            },
            connectorId: (this.props.location.state && this.props.location.state.connectorId) || ""
        },
        applicationUrl: "",
        assignMenu: [{
            navName: '',
            pageId: '',
            children: []
        }],
        errorMessageCode: 0,
        inheritedMenu: [],
        sideNavData: (JSON.parse(localStorage.getItem("sideNav"))).filter(nav => nav.navName === "IAM" || nav.navName === "Policy Manager"),
        searchPage: '',
        themeModal: false,
        themeHeader: "",
        themeSideBar: "",
        solidColors: ["bg-danger backgroundImage", "bgBlack backgroundImage", "bgWhite backgroundImage", "bg-success backgroundImage", "bg-warning backgroundImage", "bg-info backgroundImage", "bg-primary backgroundImage", "bg-secondary backgroundImage", "bg-dark backgroundImage", "bg-solid-warning backgroundImage", "bg-solid-brown backgroundImage", "bg-solid-green backgroundImage", "bg-solid-blue backgroundImage", "bg-navy-blue backgroundImage"],
        gradientColors: ["bg-midnight-bloom", "bg-vicious-stance", "bg-night-sky", "bg-slick-carbon", "bg-asteroid", "bg-plum-plate", "bg-happy-fisher", "bg-desert-hump", "bg-jungle-day", "bg-aqua-splash", "bg-arielle-smile"],
        textColors: { bgLight: "text-light", bgDanger: "text-danger", bgPrimary: "text-primary", bgWarning: "text-warning", bgDark: "text-dark", bgSuccess: "text-success", bgInfo: "text-info", bgMuted: "text-muted", bgWhite: "text-white" },
        connectorsList: [],
    }
    componentDidMount() {
        this.props.getAllRoles();
        this.props.getAllPages();
        this.props.getConnectorsListByCategory()
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.navList && nextProps.navList !== this.props.navList) {
            let inheritedMenu = JSON.parse(JSON.stringify(this.state.inheritedMenu)),
                assignMenu = JSON.parse(JSON.stringify(this.state.assignMenu)),
                sideNavData = nextProps.navList;
            sideNavData = sideNavData.filter(nav => nav.navName === "Operations" || nav.navName === "IAM" || nav.navName === "Templates");
            let _inheritedMenu = [],
                _assignMenu = [];
            inheritedMenu.map(menu => {
                if (sideNavData.filter(navObj => navObj.id === menu.value).length > 0) {
                    _inheritedMenu.push(menu);
                }
            })
            assignMenu.map(menu => {
                if (menu.menuId) {
                    if (sideNavData.filter(navObj => navObj.id === menu.menuId).length > 0) {
                        _assignMenu.push(menu);
                    }
                } else {
                    _assignMenu.push(menu)
                }
            })
            this.setState({
                sideNavData,
                assignMenu: _assignMenu,
                inheritedMenu: _inheritedMenu,
            })
        }

        if (nextProps.rolesList && nextProps.rolesList !== this.props.rolesList) {
            let rolesList = nextProps.rolesList.filter(temp => temp.name != "DEFAULT_APP_ROLE"),
                payload = JSON.parse(JSON.stringify(this.state.payload));
            if (localStorage.tenantType === "SAAS") {
                payload.roleIds.push(rolesList.filter(role => role.name === "DEVELOPER")[0].id);
            } else {
                payload.roleIds.push(rolesList.filter(role => role.name === "ACCOUNT_ADMIN")[0].id);
            }
            let _payload = {
                roleIds: payload.roleIds
            };
            this.setState({
                rolesList,
                payload
            }, () => { this.props.getNavsForSelectedRole(_payload) })
        }

        if (nextProps.rolesError && nextProps.rolesError !== this.props.rolesError) {
            this.setState({
                modalType: "error",
                isOpen: true,
                isFetching: false,
                message2: nextProps.rolesError
            })
        }
        if (nextProps.pages && (nextProps.pages !== this.props.pages)) {
            let _pages = nextProps.pages.filter(el => el.resourceOwner);
            _pages.map(page => page.added = false);
            this.setState({
                pages: _pages,
                // availablePages: _pages,
                // filteredPages: _pages,
                isFetching: this.props.match.params.id ? true : false,
            }, () => {
                if (this.props.match.params.id) {
                    this.props.getApplicationDetailsById(this.props.match.params.id);
                }
            });
        }

        if (nextProps.pagesListError && nextProps.pagesListError !== this.props.pagesListError) {
            this.setState({
                modalType: 'error',
                isOpen: true,
                message2: nextProps.pagesListError,
                isFetching: false,
            })
        }

        if (nextProps.saveApplicationSuccess && nextProps.saveApplicationSuccess !== this.props.saveApplicationSuccess) {
            this.setState({
                modalType: 'success',
                isOpen: true,
                message2: nextProps.saveApplicationSuccess.message,
            })
        }

        if (nextProps.saveApplicationError && nextProps.saveApplicationError !== this.props.saveApplicationError) {
            this.setState({
                modalType: 'error',
                isOpen: true,
                message2: nextProps.saveApplicationError.error,
                isFetching: false,
            })
        }

        if (nextProps.getApplicationById && nextProps.getApplicationById !== this.props.getApplicationById) {
            let pages = cloneDeep(this.state.pages),
                availablePages,
                inheritedMenu = cloneDeep(this.state.inheritedMenu),
                assignMenu = cloneDeep(this.state.assignMenu);
            nextProps.getApplicationById.page.map(menuObj => {
                if (menuObj.children && menuObj.children.length) {
                    menuObj.children.map(childObj => {
                        pages.map(page => {
                            if (page.id === childObj.pageId) {
                                page.added = true;
                            }
                        })
                    })
                } else if (menuObj.pageId) {
                    pages.map(page => {
                        if (page.id === menuObj.pageId) {
                            page.added = true;
                        }
                    })
                }
            })
            availablePages = pages.filter(page => page.added === false && page.connectorId === nextProps.getApplicationById.connectorId);
            assignMenu = nextProps.getApplicationById.page,
                assignMenu.map(menu => {
                    this.state.sideNavData.map(sideNav => {
                        if (sideNav.id === menu.menuId) {
                            menu.isInheritedMenu = true
                            inheritedMenu.push({
                                value: menu.menuId,
                                label: menu.navName
                            })
                            menu.children && menu.children.map(child => child.isInheritedMenu = true)
                        }
                    })
                })
            let payload = cloneDeep(nextProps.getApplicationById)
            if (!payload.style) {
                payload.style = {
                    themeHeader: "",
                    themeSideBar: "",
                    themeSideBarTextColor: "",
                    themeHeaderTextColor: ""
                }
            }
            if (nextProps.getApplicationById.isPublicApplication) {
                payload.roleIds = []
            }
            this.setState({
                payload,
                pages,
                isFetching: false,
                assignMenu,
                inheritedMenu,
                filteredPages: availablePages,
                availablePages,
            })
        }

        if (nextProps.imageUploadSuccess && nextProps.imageUploadSuccess !== this.props.imageUploadSuccess) {
            let payload = JSON.parse(JSON.stringify(this.state.payload));
            payload.logoImageUrl = nextProps.imageUploadSuccess.secure_url;
            this.setState({
                payload,
                isUploadingImage: false,
            });
        }

        if (nextProps.imageUploadFailure && nextProps.imageUploadFailure !== this.props.imageUploadFailure) {
            this.setState({
                type: "error",
                isOpen: true,
                message1: "Oops !",
                message2: nextProps.imageUploadFailure,
                isUploadingImage: false
            });
        }

        if (nextProps.getConnectorsListSuccess && nextProps.getConnectorsListSuccess !== this.props.getConnectorsListSuccess) {
            nextProps.getConnectorsListSuccess.forEach(connector => { connector.label = connector.name; connector.value = connector.id })
            this.setState({
                connectorsList: nextProps.getConnectorsListSuccess,
            })
        }
        if (nextProps.getConnectorsListFailure && nextProps.getConnectorsListFailure !== this.props.getConnectorsListFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.getConnectorsListFailure,
                isFetching: false,
            })
        }

    }
    skipScreen = tab => {
        this.setState({ tab });
    };

    onChangeHandler = (event, themeColor) => {
        let payload = JSON.parse(JSON.stringify(this.state.payload)),
            errorMessageCode = 0,
            pages = this.state.pages.map(temp => {
                temp.added = false
                return temp
            }),
            availablePages = cloneDeep(this.state.availablePages),
            assignMenu = cloneDeep(this.state.assignMenu);

        if (event.target.id === "connectorId") {
            for (var i = 0; i < assignMenu.length; i++) {
                if (assignMenu[i].pageId) {
                    this.setState({
                        confirmModal: true,
                        selectedConnectorForApplication: event.target.value
                    })
                    return;
                }
                if (assignMenu[i].children && assignMenu[i].children.length) {
                    for (var j = 0; j < assignMenu[i].children.length; j++) {
                        if (assignMenu[i].children[j].pageId) {
                            this.setState({
                                confirmModal: true,
                                selectedConnectorForApplication: event.target.value
                            })
                            return;
                        }
                    }
                }
            }
            availablePages = pages.filter(temp => temp.connectorId === event.target.value)
        }

        if (event.target.id === "collapsedView") {
            payload[event.target.id] = event.target.checked;
        } else if (event.target.id.includes("theme")) {
            payload.style[event.target.id] = themeColor
        } else {
            payload[event.target.id] = event.target.value.trim() === '' ? '' : event.target.value;
        }

        this.setState({
            payload,
            errorMessageCode,
            availablePages,
            filteredPages: availablePages
        })
    }

    checkBoxOnChange = index => event => {
        let payload = JSON.parse(JSON.stringify(this.state.payload));
        if (event.target.checked === true) {
            payload[event.target.name].push(event.target.id)
        } else {
            payload[event.target.name] = payload[event.target.name].filter(id => {
                if (id != event.target.id) {
                    return id
                }
            })
        }
        let _payload = {
            roleIds: payload.roleIds
        };
        this.setState({
            payload
        }, () => { this.props.getNavsForSelectedRole(_payload) })
    }

    findBlankNameMenu = (menu) => {
        let index = menu.findIndex(nav => {
            if (nav.children.length && (!nav.isInheritedMenu)) {
                let x;
                nav.children.map(el => {
                    if (!el.navName && el.pageId) {
                        x = true
                    }
                    else if (el.navName && !el.pageId) {
                        x = true
                    }
                    else if (!el.navName && !el.pageId) {
                        x = true
                    }
                })
                return x
            }
            else if ((!nav.isInheritedMenu)) {
                if (!nav.navName && nav.pageId) {
                    return true
                }
                else if (nav.navName && !nav.pageId) {
                    return true
                }
                else if (!nav.navName && !nav.pageId) {
                    return true
                }
            }
        })
        return index
    }

    validateParentName = (menu) => {
        let index = menu.findIndex(el => !el.navName && el.children.length)
        return index
    }

    onSubmitHandler = (event) => {
        event.preventDefault()
        let payload = JSON.parse(JSON.stringify(this.state.payload)),
            blankNameMenu = this.findBlankNameMenu(this.state.assignMenu);

        if ((!payload.isPublicApplication) && payload.roleIds.length === 0) {
            this.setState({ errorMessageCode: 1 })
            return;
        }
        if (blankNameMenu != -1) {
            this.setState({
                errorMessageCode: 2 //page assigned to menu but no menu name
            })
            return;
        }
        let noParentName = this.validateParentName(this.state.assignMenu)
        if (noParentName != -1) {
            this.setState({ errorMessageCode: 3 })
            return; //nav has children but it has no name
        }
        this.saveApplicationHandler()
    }

    saveApplicationHandler = () => {
        let payload = JSON.parse(JSON.stringify(this.state.payload));
        payload.page = JSON.parse(JSON.stringify(this.state.assignMenu));
        delete payload.pageIds;
        payload.page = payload.page.map(pageObj => {
            if (pageObj.isInheritedMenu) {
                let obj = {
                    menuId: pageObj.menuId,
                    children: pageObj.children,
                    isInheritedMenu: pageObj.isInheritedMenu
                }
                obj.children = obj.children.map(childObj => {
                    let child = {
                        menuId: childObj.menuId,
                        children: childObj.children,
                        isInheritedMenu: childObj.isInheritedMenu
                    }
                    return child;
                })
                return obj;
            }
            else {
                delete pageObj.showIconBox;
                pageObj.children.map(childObj => {
                    delete childObj.showIconBox;
                })
                return pageObj;
            }

        });
        this.setState({ isFetching: true }, () => {
            this.props.saveApplication(this.props.match.params.id, payload)
        })
    }

    onCloseHandler = () => {
        let modalType = this.state.modalType;
        this.setState({
            isOpen: false,
            message2: '',
            modalType: "",
        }, () => {
            if (modalType === "success") {
                this.props.history.push('/applications')
            }
        })
    }

    redirectHandel = () => {
        this.setState({
            applicationUrl: "",
        }, () => this.props.history.push('/applications'));
    }

    copyToClipboard = () => {
        var copyText = document.getElementById("copyText");
        copyText.select();
        document.execCommand("copy");
    }

    addDeleteHandler = (type, index, childIndex, parent) => (event) => {
        let assignMenu = JSON.parse(JSON.stringify(this.state.assignMenu)),
            pages = JSON.parse(JSON.stringify(this.state.pages)),
            availablePages = JSON.parse(JSON.stringify(this.state.availablePages)),
            errorMessageCode = 0;
        if (parent) {
            if (type === "add") {
                assignMenu.push({
                    navName: '',
                    pageId: '',
                    children: [],
                })
            }
            else {
                if (assignMenu.length > 1) {
                    pages.map(page => {
                        if (page.id === assignMenu[index].pageId) {
                            page.added = false;
                        }
                    })
                    assignMenu.splice(index, 1)
                }
            }
        }
        else {
            if (type === "add") {
                assignMenu[index].children.push({
                    navName: '',
                    pageId: '',
                    children: []
                })
                pages.map(page => {
                    if (page.id === assignMenu[index].pageId) {
                        page.added = false;
                        assignMenu[index].pageId = '';
                    }
                })
            }
            else {
                pages.map(page => {
                    if (page.id === assignMenu[index].children[childIndex].pageId) {
                        page.added = false;
                    }
                })
                assignMenu[index].children.splice(childIndex, 1)
            }
        }
        assignMenu.map(menuObj => {

            menuObj.showIconBox = false;

            if (menuObj.children.length) {
                menuObj.children.map(childObj => {
                    childObj.showIconBox = false;
                    pages.map(page => {
                        if (page.id === childObj.pageId) {
                            page.added = true;
                        }
                    })
                })
            } else if (menuObj.pageId) {
                pages.map(page => {
                    if (page.id === menuObj.pageId) {
                        page.added = true;
                    }
                })
            }

        })
        availablePages = pages.filter(page => page.added === false && page.connectorId === this.state.payload.connectorId);
        this.setState({
            errorMessageCode,
            assignMenu,
            pages,
            availablePages,
            filteredPages: availablePages,
        });
    }

    editHandler = (index, childIndex, parent) => (event) => {
        let assignMenu = JSON.parse(JSON.stringify(this.state.assignMenu)),
            errorMessageCode = 0
        if (parent) {
            assignMenu[index].navName = event.target.value.trim() === '' ? '' : event.target.value
        }
        else {
            assignMenu[index].children[childIndex].navName = event.target.value.trim() === '' ? '' : event.target.value
        }

        this.setState({
            errorMessageCode,
            assignMenu,
        });
    }

    allowDrop = ev => {
        ev.preventDefault();
    }

    drag = (ev, pageId) => {
        this.setState({
            pageId,
            menuDrag: "false"
        })
    }

    drop = (ev, index, childIndex, isParent) => {
        ev.preventDefault();
        let assignMenu = JSON.parse(JSON.stringify(this.state.assignMenu)),
            pages = JSON.parse(JSON.stringify(this.state.pages)),
            availablePages = JSON.parse(JSON.stringify(this.state.availablePages)),
            errorMessageCode = 0;
        if (this.state.menuDrag === "menuDrag") {
            let b = assignMenu[index];
            assignMenu[index] = assignMenu[this.state.dragMenuIndex];
            assignMenu[this.state.dragMenuIndex] = b;
        }
        else if (this.state.menuDrag === "false") {
            if (isParent) {
                assignMenu[index].pageId = this.state.pageId
            }
            else {
                assignMenu[index].children[childIndex].pageId = this.state.pageId
            }
            pages.map(page => {
                page.added = false;
            })
            assignMenu.map(menuObj => {
                if (menuObj.children.length) {
                    menuObj.children.map(childObj => {
                        pages.map(page => {
                            if (page.id === childObj.pageId) {
                                page.added = true;
                            }
                        })
                    })
                } else if (menuObj.pageId) {
                    pages.map(page => {
                        if (page.id === menuObj.pageId) {
                            page.added = true;
                        }
                    })
                }
            })
            availablePages = pages.filter(page => page.added === false && page.connectorId === this.state.payload.connectorId);
        }
        this.setState({
            assignMenu,
            pageId: '',
            pages,
            availablePages,
            filteredPages: availablePages,
            errorMessageCode,
            dragMenuIndex: '',
            menuDrag: ''
        }, () => this.pageSearchHandler())
    }

    pageRemoveHandler = (index, childIndex, isParent) => {
        let assignMenu = JSON.parse(JSON.stringify(this.state.assignMenu)),
            pages = JSON.parse(JSON.stringify(this.state.pages)),
            availablePages = JSON.parse(JSON.stringify(this.state.availablePages));

        pages.map(page => {
            if (isParent) {
                if (page.id === assignMenu[index].pageId) {
                    page.added = false;
                    assignMenu[index].pageId = '';
                }
            }
            else {
                if (page.id === assignMenu[index].children[childIndex].pageId) {
                    page.added = false;
                    assignMenu[index].children[childIndex].pageId = '';
                }
            }

        })
        availablePages = pages.filter(page => page.added === false && page.connectorId === this.state.payload.connectorId);
        this.setState({
            assignMenu,
            pages,
            availablePages,
            filteredPages: availablePages,
        })
    }

    handleMenuIconBox = (parentIndex, childIndex, type) => {
        let assignMenu = this.state.assignMenu;
        assignMenu.map((menuObj, menuParentIndex) => {
            if (type === 'PARENT') {
                if (menuParentIndex === parentIndex) {
                    menuObj.showIconBox = menuObj.showIconBox ? !menuObj.showIconBox : true;
                } else {
                    menuObj.showIconBox = false;
                }
                menuObj.children.map((menuChildObj) => {
                    menuChildObj.showIconBox = false;
                });
            } else {
                menuObj.showIconBox = false;
                if (parentIndex === menuParentIndex) {
                    menuObj.children.map((menuChildObj, menuChildIndex) => {
                        if (childIndex === menuChildIndex) {
                            menuChildObj.showIconBox = menuChildObj.showIconBox ? !menuChildObj.showIconBox : true;
                        } else {
                            menuChildObj.showIconBox = false;
                        }
                    });
                }

            }
        })

        this.setState({
            assignMenu,
        })
    }

    handleChange = (value, type) => {
        let icon = value;

        let assignMenu = this.state.assignMenu;
        assignMenu.map((menuObj, menuIndex) => {
            if (type === 'PARENT') {
                if (menuObj.showIconBox) {
                    menuObj.iconName = value;
                    menuObj.showIconBox = false;
                }
            } else {
                menuObj.children.map((menuChildObj, menuChildIndex) => {
                    if (menuChildObj.showIconBox) {
                        menuChildObj.iconName = value;
                        menuChildObj.showIconBox = false;
                    }
                });
            }
        })

        this.setState({
            assignMenu,
        })
    }
    handleColorChange = (event) => {
        let payload = JSON.parse(JSON.stringify(this.state.payload));
        payload.theme = event.target.value;

        this.setState({
            payload
        });
    }

    menuDrag = (index) => {
        this.setState({
            dragMenuIndex: index,
            menuDrag: "menuDrag"
        })
    }

    optionsForServicesMenu = () => {
        let sideNavData = JSON.parse(JSON.stringify(this.state.sideNavData)),
            options = sideNavData.map(nav => {
                return {
                    value: nav.id,
                    label: nav.navName
                }
            })
        return options
    }

    handleChangeServicesMenu = (value, type) => {
        let assignMenu = JSON.parse(JSON.stringify(this.state.assignMenu)),
            sideNavData = JSON.parse(JSON.stringify(this.state.sideNavData)),
            inheritedMenu = JSON.parse(JSON.stringify(this.state.inheritedMenu));
        inheritedMenu = value
        if (type.action === "select-option") {
            sideNavData.map(sideNav => {
                if (sideNav.id === type.option.value) {
                    assignMenu.push({
                        navName: type.option.label,
                        menuId: type.option.value,
                        iconName: sideNav.fontAwesomeIconClass,
                        children: sideNav.subMenus.length ? sideNav.subMenus.map(subMenu => {
                            return {
                                navName: subMenu.navName,
                                menuId: subMenu.id,
                                children: [],
                                iconName: subMenu.fontAwesomeIconClass,
                                isInheritedMenu: true
                            }
                        }) : [],
                        isInheritedMenu: true
                    })
                }
            })
        }
        else if (type.action === "remove-value") {
            assignMenu = assignMenu.filter(menu => menu.menuId != type.removedValue.value)
        }
        else if (type.action === "clear") {
            assignMenu = assignMenu.filter(menu => menu.isInheritedMenu != true)
        }
        if (assignMenu.length === 0) {
            assignMenu.push({
                navName: '',
                pageId: '',
                children: [],
            })
        }
        this.setState({
            assignMenu,
            inheritedMenu
        })

    }
    pageSearchHandler = (e) => {
        let filteredPages = JSON.parse(JSON.stringify(this.state.availablePages))
        let searchCharacter = e ? e.target.value : this.state.searchPage
        if (searchCharacter.length > 0) {
            filteredPages = filteredPages.filter(item => item.name.toLowerCase().includes(searchCharacter.toLowerCase()))
        }
        let searchPage = e ? e.target.value : this.state.searchPage;
        this.setState({
            filteredPages,
            searchPage
        })
    }

    emptySearchBox = () => {
        let filteredPages = JSON.parse(JSON.stringify(this.state.availablePages))
        this.setState({
            searchPage: '',
            filteredPages
        })
    }

    resetDefault = (reset) => {
        let payload = JSON.parse(JSON.stringify(this.state.payload))
        if (reset === "logoImageUrl") {
            payload[reset] = ""
        } else {
            payload.style[reset] = ""
        }
        this.setState({
            payload
        })
    }

    fileUploadHandler = event => {
        event.persist();
        this.setState({
            isUploadingImage: true
        }, () => this.props.uploadImageHandler(event.target.files[0], event.target.name));

    };

    getTheme = (type) => {
        let themeClass = "";
        switch (type) {
            case "themeHeader":
                themeClass = this.state.payload.style.themeHeader;
                break;
            case "themeHeaderTextColor":
                themeClass = this.state.payload.style.themeHeaderTextColor;
                break;

            case "themeSideBar":
                themeClass = this.state.payload.collapsedView ? this.state.payload.style.themeSideBar : "";
                break;

            case "themeSideBarTextColor":
                themeClass = this.state.payload.style.themeSideBarTextColor
                break;
        }
        return (" " + themeClass + " ")
    }

    resetAllAssignedPages = () => {
        let assignMenu = cloneDeep(this.state.assignMenu),
            pages = this.state.pages.map(temp => {
                temp.added = false
                return temp
            }),
            availablePages = cloneDeep(this.state.availablePages),
            payload = cloneDeep(this.state.payload);

        payload.connectorId = this.state.selectedConnectorForApplication
        availablePages = pages.filter(temp => temp.connectorId === this.state.selectedConnectorForApplication)

        assignMenu.map(menu => {
            menu.pageId = ''
            if (menu.children && menu.children.length) {
                menu.children.map(child => child.pageId = '')
            }
        })
        this.setState({
            assignMenu,
            confirmModal: false,
            availablePages,
            filteredPages: availablePages,
            selectedConnectorForApplication: '',
            payload
        })
    }



    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>Add/Edit Application</title>
                    <meta name="description" content="M83-Add/EditApplication" />
                </Helmet>

                {/*{this.state.isFetching ? <Loader /> :
                    <React.Fragment>
                        <div className="outerBox pb-0">
                            <form className="contentForm" onSubmit={this.onSubmitHandler}>
                                <div className="flex">
                                    <div className="flex-item fx-b25 contentFormDetail pd-r-10">
                                        <div className="contentFormHeading">
                                            <p>Basic Info</p>
                                            <button type="button" className="btn btn-link" onClick={() => this.setState({ themeModal: true })}>
                                                <i className="fas fa-palette"></i>Add theme
                                            </button>
                                        </div>

                                        <div className="contentFormBody">
                                            <div className="form-group">
                                                <label className="form-label">App Logo :</label>
                                                <button type="button" className="btn btn-link float-right p-0 f-12" onClick={() => { this.resetDefault("logoImageUrl") }}>Reset</button>
                                            </div>

                                            {localStorage.tenantType !== 'SAAS' ? !this.state.payload.isPublicApplication &&
                                                <div className="form-group">
                                                    <label className="form-label">Assign Roles :
                                                    <span className="requiredMark"><i className="fa fa-asterisk" /></span>
                                                    </label>
                                                    <div className="flex">
                                                        {(this.state.rolesList && this.state.rolesList.length > 0) &&
                                                            this.state.rolesList.map((item, index) => {
                                                                return (
                                                                    <div className="flex-item fx-b50" key={index}>
                                                                        <label className={item.name == 'ACCOUNT_ADMIN' ? "checkboxLabel checkboxDisabled" : "checkboxLabel"}>
                                                                            <span className="checkText"><i className={item.icon ? item.icon : 'fas fa-user'}></i>{item.name}</span>
                                                                            <input name="roleIds" type="checkbox" id={item.id} disabled={item.name === "ACCOUNT_ADMIN"} checked={this.state.payload.roleIds.includes(item.id)} htmlFor="selectedDeviceType" onChange={this.checkBoxOnChange(index)} />
                                                                            <span className="checkmark" />
                                                                        </label>
                                                                    </div>
                                                                )
                                                            })
                                                        }
                                                    </div>
                                                    {this.state.errorMessageCode === 1 ?
                                                        <p className="formErrorMessage"><i className="fas fa-exclamation-triangle"></i>Please select role(s).</p> : ''
                                                    }
                                                </div> : ''
                                            }

                                             <div className="form-group flex">
                                                <div className="form-group-left-label fx-b30">
                                                    <label className="form-label">Scope :
                                                                <span className="requiredMark"><i className="fa fa-asterisk" /></span>
                                                    </label>
                                                </div>
                                                <div className="form-group-right-label fx-b70">
                                                    <div className="flex">
                                                        <div className="flex-item fx-b50">
                                                            <label className="radioLabel radioDisabled">Open
                                                                        <input type="radio" name="isPublicApplication" disabled checked={this.state.payload.isPublicApplication} value={true} onChange={this.onChangeHandler} />
                                                                <span className="radioMark" />
                                                            </label>
                                                        </div>
                                                        <div className="flex-item fx-b50">
                                                            <label className="radioLabel">Secure
                                                                        <input type="radio" name="isPublicApplication" defaultChecked={!this.state.payload.isPublicApplication} />
                                                                <span className="radioMark" />
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="flex-item fx-b50 contentFormDetail pd-r-5 pd-l-5">
                                        <div className="contentFormHeading"><p>Assign Menu</p>
                                            {this.state.errorMessageCode === 2 ? <span className="contentFormError"><i className="fas fa-exclamation-triangle"></i>Please create the application menu.</span> : ''}
                                            {this.state.errorMessageCode === 3 ? <span className="contentFormError"><i className="fas fa-exclamation-triangle"></i>Please create the child menu.</span> : ''}
                                        </div>

                                        <div className="contentFormBody">
                                            {localStorage.tenantType !== 'SAAS' &&
                                                <div className="inheritedForm">
                                                    <label className="form-label">Services Menu :</label>
                                                    <ReactSelect
                                                        className="form-control-multi-select"
                                                        isMulti={true}
                                                        options={this.optionsForServicesMenu()}
                                                        onChange={this.handleChangeServicesMenu}
                                                        value={this.state.inheritedMenu}
                                                    >
                                                    </ReactSelect>
                                                </div>
                                            }
                                            <ul className="menuList listStyleNone">
                                                {this.state.assignMenu.map((el, index) => {
                                                    return (
                                                        <li className={el.isInheritedMenu ? "menuListOuter menuListOuterBg" : "menuListOuter"} draggable key={index} onDragStart={() => this.menuDrag(index)} onDrop={() => { this.state.menuDrag === "menuDrag" && this.drop(event, index, null, null) }} onDragOver={(event) => { this.allowDrop(event) }} >
                                                            {el.isInheritedMenu && <p className="inheritedMark"><i className="fad fa-medal text-orange"></i></p>}
                                                            <button type="button" className="btn dragButton"><i className="fal fa-arrows-alt"></i></button>
                                                            <div className="flex menuListBox">
                                                                <div className="fx-b10 form-group pd-r-7">
                                                                    <div className={el.isInheritedMenu ? "form-control menuListBoxIcon disabled" : "form-control menuListBoxIcon"} onClick={() => { !el.isInheritedMenu && this.handleMenuIconBox(index, null, 'PARENT') }}>
                                                                        <i className={el.iconName ? el.iconName.includes("fa-") ? el.iconName : `fal fa-${el.iconName}` : 'fal fa-window'}></i>
                                                                    </div>
                                                                    {el.showIconBox &&
                                                                        <div className="menuIconBox">
                                                                            <FontAwesomeDisplayList changeHandlerForSelect={(value) => this.handleChange(value, 'PARENT')}></FontAwesomeDisplayList>
                                                                        </div>
                                                                    }
                                                                </div>
                                                                <div className="fx-b45 form-group pd-r-7">
                                                                    <input id={"navName" + index} className="form-control" placeholder="Menu Name" disabled={el.isInheritedMenu} value={el.navName} onChange={this.editHandler(index, null, true)} />
                                                                    <strong>{index + 1}</strong>
                                                                </div>
                                                                {el.children && el.children.length === 0 &&
                                                                    <div className="fx-b45 form-group">
                                                                        {el.isInheritedMenu ?
                                                                            this.state.sideNavData.map((sideNav, index) => {
                                                                                if (sideNav.id === el.menuId) {
                                                                                    return (<input key={index} disabled={true} className="form-control menuDropBox active" value={sideNav.url} onChange={() => { return null; }} />)
                                                                                }
                                                                            })
                                                                            :
                                                                            <input id="navName" className={this.state.pages.find(el => el.id == this.state.assignMenu[index].pageId) ? "form-control menuDropBox active" : "form-control menuDropBox"} value={this.state.pages.find(el => el.id == this.state.assignMenu[index].pageId) ? this.state.pages.find(el => el.id == this.state.assignMenu[index].pageId).name : "Drag a page and drop here !"} onDrop={(event) => { this.drop(event, index, null, true) }} onDragOver={(event) => { this.allowDrop(event) }} onChange={() => { return null; }} />}
                                                                    </div>
                                                                }
                                                                <div className="form-group text-center button-group">
                                                                    <button type="button" className="btn btn-transparent btn-transparent-primary" data-tip data-for={"addMenu" + index} disabled={this.state.availablePages.length === 0 || el.isInheritedMenu} onClick={this.addDeleteHandler("add", index, null, false)}>
                                                                        <i className="far fa-plus"></i>
                                                                    </button>
                                                                    <ReactTooltip id={"addMenu" + index} place="bottom" type="dark">
                                                                        <div className="tooltipText"><p>Add Submenu</p></div>
                                                                    </ReactTooltip>
                                                                    {el.children && el.children.length === 0 &&
                                                                        <button type="button" className="btn btn-transparent btn-transparent-success" data-tip data-for={"reset" + index} disabled={el.isInheritedMenu} onClick={() => this.pageRemoveHandler(index, null, true)} >
                                                                            <i className="fas fa-redo-alt"></i>
                                                                        </button>
                                                                    }
                                                                    <ReactTooltip id={"reset" + index} place="bottom" type="dark">
                                                                        <div className="tooltipText"><p>Reset</p></div>
                                                                    </ReactTooltip>
                                                                    <button type="button" className="btn btn-transparent btn-transparent-danger" data-tip data-for={"delete" + index} disabled={this.state.assignMenu.length === 1 || el.isInheritedMenu} onClick={this.addDeleteHandler("delete", index, null, true)}>
                                                                        <i className="fas fa-trash-alt"></i>
                                                                    </button>
                                                                    <ReactTooltip id={"delete" + index} place="bottom" type="dark">
                                                                        <div className="tooltipText"><p>Delete</p></div>
                                                                    </ReactTooltip>
                                                                </div>
                                                            </div>
                                                            <ul className="listStyleNone menuInnerList">
                                                                {el.children && el.children.map((child, childIndex) => {
                                                                    return (
                                                                        <li key={childIndex}>
                                                                            <div className="flex menuListBox menuListBoxInner">
                                                                                <div className="fx-b10 form-group pd-r-7">
                                                                                    <div className={el.isInheritedMenu ? "form-control menuListBoxIcon disabled" : "form-control menuListBoxIcon"} onClick={() => { !el.isInheritedMenu && this.handleMenuIconBox(index, childIndex, 'CHILD') }}>
                                                                                        <i className={child.iconName ? child.iconName.includes("fa-") ? child.iconName : `fal fa-${child.iconName}` : 'fal fa-window'}></i>
                                                                                    </div>
                                                                                    {child.showIconBox &&
                                                                                        <div className="menuIconBox">
                                                                                            <FontAwesomeDisplayList changeHandlerForSelect={(value) => this.handleChange(value, 'CHILD')}></FontAwesomeDisplayList>
                                                                                        </div>
                                                                                    }
                                                                                </div>
                                                                                <div className="fx-b45 form-group pd-r-7">
                                                                                    <input className="form-control" placeholder="Menu Name" id={"navName" + childIndex} disabled={el.isInheritedMenu} value={child.navName} onChange={this.editHandler(index, childIndex, false)} />
                                                                                    <strong>{(index + 1) + '.' + (childIndex + 1)}</strong>
                                                                                </div>
                                                                                <div className="fx-b45 form-group pd-l-7">
                                                                                    {child.isInheritedMenu ?
                                                                                        this.state.sideNavData.map((sideNav, index) => {
                                                                                            let html;
                                                                                            if (sideNav.id === el.menuId) {
                                                                                                sideNav.subMenus.map((subMenu, subIndex) => {
                                                                                                    if (subMenu.id === child.menuId)
                                                                                                        html = <input type="text" disabled={true} key={subIndex} className="form-control menuDropBox active" value={subMenu.url} onChange={() => { return null; }} />
                                                                                                }
                                                                                                )
                                                                                            }
                                                                                            return html;
                                                                                        })
                                                                                        :
                                                                                        <input id="navName" className={this.state.pages.find(el => el.id == this.state.assignMenu[index].children[childIndex].pageId) ? "form-control menuDropBox active" : "form-control menuDropBox"} value={this.state.pages.find(el => el.id == this.state.assignMenu[index].children[childIndex].pageId) ? this.state.pages.find(el => el.id == this.state.assignMenu[index].children[childIndex].pageId).name : "Drag a page and drop here !"} onDrop={(event) => { this.drop(event, index, childIndex, false) }} onDragOver={(event) => { this.allowDrop(event) }} onChange={() => { return null; }} />}
                                                                                </div>
                                                                                <div className="form-group text-center button-group">
                                                                                    <button type="button" className="btn btn-transparent btn-transparent-success" disabled={el.isInheritedMenu} onClick={() => this.pageRemoveHandler(index, childIndex, false)}>
                                                                                        <i className="fas fa-redo-alt"></i>
                                                                                    </button>
                                                                                    <button type="button" className="btn btn-transparent btn-transparent-danger" disabled={el.isInheritedMenu} onClick={this.addDeleteHandler("delete", index, childIndex, false)}>
                                                                                        <i className="fas fa-trash-alt"></i>
                                                                                    </button>
                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                    )
                                                                })}
                                                            </ul>
                                                        </li>
                                                    )
                                                })}
                                            </ul>

                                            <div className="form-group text-right">
                                                <button type="button" className="btn btn-link" disabled={this.state.availablePages.length === 0} onClick={this.addDeleteHandler("add", null, null, true)}>
                                                    <i className="far fa-plus"></i>Add New
                                                </button>
                                            </div>
                                        </div>
                                    </div>


                                </div>


                            </form>
                        </div>
                    </React.Fragment >
                }*/}

                <header className="content-header d-flex">
                    <div className="flex-60">
                        <div className="d-flex">
                            <h6 className="previous" onClick={() => { this.props.history.push('/applications') }}>Applications</h6>
                            <h6 className="active">{this.props.match.params.id ? this.state.payload.name : 'Add New'}</h6>
                        </div>
                    </div>
                    <div className="flex-40 text-right">
                        <div className="content-header-group">
                            <button className="btn btn-light" onClick={() => this.props.history.push('/applications')}><i className="fas fa-angle-left"></i></button>
                        </div>
                    </div>
                </header>

                {this.state.isFetching ?
                    <Loader /> :
                    <div className="content-body">
                        <form onSubmit={this.onSubmitHandler}>
                            <div className="form-content-wrapper form-content-wrapper-left">
                                <div className="d-flex">
                                    <div className="flex-70 pd-r-7">
                                        <div className="form-content-box">
                                            <div className="form-content-header">
                                                <p>Add Menu
                                                    {this.state.errorMessageCode === 2 ? <span className="form-content-error">Please create the application menu.</span> : ''}
                                                    {this.state.errorMessageCode === 3 ? <span className="form-content-error">Please create the child menu.</span> : ''}
                                                </p>
                                                <button type="button" className="btn btn-link" onClick={() => this.setState({ themeModal: true })} disabled={true}>
                                                    <i className="fad fa-palette"></i>Add Theme
                                                </button>
                                            </div>

                                            <div className="form-content-body p-3">
                                                <div className="application-wrapper">
                                                    <div className="application-menu-wrapper">
                                                        <ul className="list-style-none mb-4">
                                                            {this.state.assignMenu.map((menu, index) => {
                                                                return (
                                                                    <li className="application-menu-active" key={index} draggable key={index} onDragStart={() => this.menuDrag(index)} onDrop={() => { this.state.menuDrag === "menuDrag" && this.drop(event, index, null, null) }} onDragOver={(event) => { this.allowDrop(event) }}>
                                                                        <div className="d-flex application-menu">
                                                                            <div className="application-menu-icon">
                                                                                <i className="far fa-window"></i>
                                                                            </div>
                                                                            <div className="flex-45 form-group pd-r-5">
                                                                                <input type="text" id={"navName" + index} name="menuName" className="form-control" placeholder="Menu Name" disabled={menu.isInheritedMenu} value={menu.navName} onChange={this.editHandler(index, null, true)} />
                                                                            </div>
                                                                            {menu.children && menu.children.length === 0 &&
                                                                                <div className="flex-45 form-group pd-l-5">
                                                                                    <input type="text" id="dragdrop" name="dragdrop" className="form-control"
                                                                                        className={this.state.pages.find(el => el.id == this.state.assignMenu[index].pageId) ? "form-control active" : "form-control"}
                                                                                        value={this.state.pages.find(el => el.id == this.state.assignMenu[index].pageId) ? this.state.pages.find(el => el.id == this.state.assignMenu[index].pageId).name : "Drag a page and drop here !"}
                                                                                        onDrop={(event) => { this.drop(event, index, null, true) }}
                                                                                        onDragOver={(event) => { this.allowDrop(event) }}
                                                                                        onChange={() => { return null; }} placeholder="Drag and drop pages here !" />
                                                                                </div>}
                                                                            <div className="form-group button-group">
                                                                                {menu.children && menu.children.length === 0 &&
                                                                                    <button type='button' className="btn-transparent btn-transparent-green"
                                                                                        disabled={menu.isInheritedMenu}
                                                                                        onClick={() => this.pageRemoveHandler(index, null, true)}><i className="far fa-redo-alt"></i>
                                                                                    </button>
                                                                                }
                                                                                <button type='button' className="btn-transparent btn-transparent-red"
                                                                                    disabled={this.state.assignMenu.length === 1 || menu.isInheritedMenu}
                                                                                    onClick={this.addDeleteHandler("delete", index, null, true)}><i className="far fa-trash-alt"></i>
                                                                                </button>
                                                                                <button type='button' className="btn-transparent btn-transparent-blue"
                                                                                    disabled={this.state.availablePages.length === 0 || menu.isInheritedMenu}
                                                                                    onClick={this.addDeleteHandler("add", index, null, false)}><i className="far fa-plus"></i>
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                        <ul className="list-style-none">
                                                                            {menu.children && menu.children.map((child, childIndex) => {
                                                                                return (
                                                                                    <li key={childIndex}>
                                                                                        <div className="d-flex application-menu application-sub-menu">
                                                                                            <div className="application-menu-icon">
                                                                                                <i className="far fa-window"></i>
                                                                                            </div>
                                                                                            <div className="flex-45 form-group pd-r-5">
                                                                                                <input type="text" className="form-control" id={"navName" + childIndex} disabled={menu.isInheritedMenu} value={child.navName} onChange={this.editHandler(index, childIndex, false)} placeholder="Menu Name" />
                                                                                            </div>
                                                                                            <div className="flex-45 form-group pd-l-5">
                                                                                                <input type="text" className="form-control" placeholder="Drag and drop pages here !" className={this.state.pages.find(el => el.id == this.state.assignMenu[index].children[childIndex].pageId) ? "form-control active" : "form-control"}
                                                                                                    value={this.state.pages.find(el => el.id == this.state.assignMenu[index].children[childIndex].pageId) ? this.state.pages.find(el => el.id == this.state.assignMenu[index].children[childIndex].pageId).name : "Drag a page and drop here !"}
                                                                                                    onDrop={(event) => { this.drop(event, index, childIndex, false) }}
                                                                                                    onDragOver={(event) => { this.allowDrop(event) }}
                                                                                                    onChange={() => { return null; }} />
                                                                                            </div>
                                                                                            <div className="form-group button-group">
                                                                                                <button type='button' className="btn-transparent btn-transparent-green"
                                                                                                    disabled={menu.isInheritedMenu}
                                                                                                    onClick={() => this.pageRemoveHandler(index, childIndex, false)}><i className="far fa-redo-alt"></i>
                                                                                                </button>
                                                                                                <button type='button' className="btn-transparent btn-transparent-red"
                                                                                                    disabled={menu.isInheritedMenu}
                                                                                                    onClick={this.addDeleteHandler("delete", index, childIndex, false)}><i className="far fa-trash-alt"></i>
                                                                                                </button>
                                                                                            </div>
                                                                                        </div>
                                                                                    </li>)
                                                                            })}
                                                                        </ul>
                                                                    </li>)
                                                            })}
                                                        </ul>
                                                        <div className="application-menu-button">
                                                            <button type="button" className="btn btn-link" disabled={this.state.availablePages.length === 0 || this.state.availablePages.length == 0 } onClick={this.addDeleteHandler("add", null, null, true)}>
                                                                <i className="far fa-plus"></i>Add New
                                                                </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="flex-30 pd-l-7">
                                        <div className="form-content-box">
                                            <div className="form-content-header">
                                                <p>Select Pages</p>
                                            </div>

                                            <div className="form-content-body p-3">
                                                <div className="application-wrapper">
                                                    <div className="application-search-wrapper">
                                                        {this.state.payload.connectorId ?
                                                            <React.Fragment>
                                                                {this.state.availablePages.length > 0 &&
                                                                    <div className="form-group">
                                                                        <input type="text" className="form-control" value={this.state.searchPage} onChange={this.pageSearchHandler} placeholder="Search Pages..." />
                                                                        <i className="far fa-search application-search-icon"></i>
                                                                        {this.state.searchPage.length > 0 ?
                                                                            <button onClick={() => this.emptySearchBox()} className="btn"><i className="far fa-times"></i></button> : ''
                                                                        }
                                                                    </div>}
                                                                {this.state.availablePages.length > 0 ?
                                                                    this.state.filteredPages.length > 0 ?
                                                                        <React.Fragment>
                                                                            <ul className="list-style-none">
                                                                                {this.state.filteredPages.map((item, index) => {
                                                                                    if (item.added === false)
                                                                                        return (
                                                                                            <li key={index} draggable={!item.added} onDragStart={() => { this.drag(event, item.id) }}>
                                                                                                <p className="m-0">{item.name}</p>
                                                                                            </li>
                                                                                        )
                                                                                })}
                                                                            </ul>
                                                                        </React.Fragment>
                                                                        :
                                                                        <div className="application-search-content">
                                                                            <div>
                                                                                <div className="application-search-image">
                                                                                    <img src="https://content.iot83.com/m83/other/noSearchResult.png" />
                                                                                </div>
                                                                                <p>No data matched your search. Try using other search options.</p>
                                                                            </div>
                                                                        </div> :
                                                                    <div className="application-search-content">
                                                                        <div>
                                                                            <div className="application-search-image">
                                                                                <img src="https://content.iot83.com/m83/misc/addPage.png" />
                                                                            </div>
                                                                            <p>You have not created any pages yet for this device type.</p>
                                                                            <button className="btn btn-link" type="button" onClick={() => { this.props.history.push({ pathname: '/addOrEditDashboard', state: { selectedConnector: this.state.payload.connectorId } }) }}>
                                                                                <i className="far fa-plus"></i>Add new
                                                                                </button>
                                                                        </div>
                                                                    </div>
                                                                }
                                                            </React.Fragment> :
                                                            <div className="application-search-content">
                                                                <div>
                                                                    <div className="application-search-image">
                                                                        <img src="https://content.iot83.com/m83/misc/addDeviceType.png" />
                                                                    </div>
                                                                    <p>Please select device type first !</p>
                                                                </div>
                                                            </div>
                                                        }
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div className="form-info-wrapper form-info-wrapper-left">
                                <div className="form-info-header">
                                    <h5>Basic Information</h5>
                                </div>
                                <div className="form-info-body form-info-body-adjust">
                                    <div className="form-group">
                                        <label className="form-group-label">Device Type :  <i className="fas fa-asterisk form-group-required"></i></label>
                                        <select className="form-control" value={this.state.payload.connectorId} required id="connectorId" onChange={this.onChangeHandler} >
                                            <option value="Null">Select</option>
                                            {this.state.connectorsList.map((connector, index) => {
                                                return <option value={connector.value} key={index}>{connector.label}</option>
                                            })}
                                        </select>
                                    </div>
                                    <div className="form-group">
                                        <label className="form-group-label">Name : <i className="fas fa-asterisk form-group-required"></i></label>
                                        <input type="text" id="name" className="form-control"
                                               pattern="^[A-Za-z0-9]+(?:[' _-][A-Za-z0-9]+)*$" autoFocus
                                               value={this.state.payload.name} onChange={this.onChangeHandler} required
                                        />
                                    </div>
                                    <div className="form-group">
                                        <label className="form-group-label">Version : <i className="fas fa-asterisk form-group-required"></i></label>
                                        <input type="text" id="appVersion" className="form-control" required
                                            value={this.state.payload.appVersion} onChange={this.onChangeHandler}
                                        />
                                    </div>
                                    <div className="form-group">
                                        <label className="form-group-label">Description :</label>
                                        <textarea rows="3" type="text" id="description" className="form-control"
                                            value={this.state.payload.description} onChange={this.onChangeHandler}
                                        />
                                    </div>
                                    <div className="form-group">
                                        <label className="form-group-label">Logo :
                                            <button type="button" className="btn btn-link float-right p-0" onClick={() => { this.resetDefault("logoImageUrl") }}>
                                            <i className="far fa-redo-alt"></i>Reset
                                            </button>
                                        </label>
                                        <div className="file-upload-box">
                                            {this.state.isUploadingImage ?
                                                <div className="file-upload-loader">
                                                    <img src="https://content.iot83.com/m83/misc/uploading.gif" />
                                                    <p>Uploading...</p>
                                                </div> :
                                                this.state.payload.logoImageUrl ?
                                                    <div className="file-upload-preview">
                                                        <img src={this.state.payload.logoImageUrl} />
                                                        <input type="file" name="logoImageUrl" onChange={this.fileUploadHandler} />
                                                        <h6><i className="far fa-pen" />Update</h6>
                                                    </div> :
                                                    <div className="file-upload-form">
                                                        <input type="file" name="logoImageUrl" onChange={this.fileUploadHandler} />
                                                        <p>Click to upload file...!</p>
                                                    </div>
                                            }
                                        </div>
                                    </div>

                                    {localStorage.tenantType !== 'SAAS' ? !this.state.payload.isPublicApplication &&
                                        <div className="form-group">
                                            <label className="form-group-label">Assign Roles :</label>
                                            {this.state.errorMessageCode === 1 ?
                                                <p><i className="fas fa-exclamation-triangle"></i>Please select role(s).</p> : ''
                                            }
                                            <ul className="list-style-none">
                                                {(this.state.rolesList && this.state.rolesList.length > 0) &&
                                                    this.state.rolesList.map((item, index) => {
                                                        return (
                                                            <li key={index}>
                                                                <label className="check-box mt-2">
                                                                    <input type="checkbox" id={item.id} disabled={item.name === "ACCOUNT_ADMIN"}
                                                                        checked={this.state.payload.roleIds.includes(item.id)}
                                                                        onChange={this.checkBoxOnChange(index)} />
                                                                    <span className="check-mark"></span>
                                                                    <span className="check-text">{item.name}</span>
                                                                </label>
                                                            </li>
                                                        )
                                                    })}
                                            </ul>
                                        </div> : ''
                                    }
                                </div>
                                <div className="form-info-footer">
                                    <button className="btn btn-light" onClick={() => { this.props.history.push('/applications') }}>Cancel</button>
                                    <button className="btn btn-primary" disabled={!this.state.payload.isPublicApplication && this.state.rolesList.length === 0}>{this.props.match.params.id ? "Update" : "Save"}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                }

                {/* add theme modal */}
                {this.state.themeModal &&
                    <div className="modal d-block animated slideInDown">
                        <div className="modal-dialog modal-full modal-dialog-centered">
                            <div className="modal-content ">
                                <div className="modal-header">
                                    <h6 className="modal-title">Add Theme
                                        <button type="button" className="close" data-dismiss="modal" onClick={() => this.setState({ themeModal: false })}>
                                            <i className="far fa-times"></i>
                                        </button>
                                    </h6>
                                </div>
                                <div className="modal-body">
                                    <div className="d-flex">
                                        <div className="flex-30">
                                            <div className="theme-color-box">
                                                <div className="theme-collapse">
                                                    <p>Collapsed Menu</p>
                                                    <div className="toggle-switch-wrapper">
                                                        <label className="toggle-switch">
                                                            <input type="checkbox" id="isAuthenticated" />
                                                            <span className="toggle-switch-slider"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                                <ul className="list-style-none d-flex mode-selection-list">
                                                    <li>
                                                        <div className="custom-theme d-flex">
                                                            <div className=" flex-25 demo-side-nav"> </div>
                                                            <div className="flex-75 ">
                                                                <div className="demo-head"></div>
                                                                <div className="demo-body"></div>
                                                                <div className="demo-head"></div>
                                                            </div>
                                                        </div>
                                                        <h6>Light Mode</h6>
                                                    </li>
                                                    <li>
                                                        <div className="custom-theme d-flex bg-dark-gray">
                                                            <div className=" flex-25 demo-side-nav bg-gray"> </div>
                                                            <div className="flex-75 ">
                                                                <div className="demo-head bg-gray"></div>
                                                                <div className="demo-body bg-gray"></div>
                                                                <div className="demo-head bg-gray"></div>
                                                            </div>
                                                        </div>
                                                        <h6>Dark Mode</h6>
                                                    </li>
                                                </ul>
                                                <div className="theme-selection-box">
                                                    <h5>Choose Theme</h5>
                                                    <ul className="list-style-none theme-type-list">
                                                        <li>
                                                            <h6>Solid Themes</h6>
                                                            <ul className="list-style-none d-flex theme-color-list">
                                                                <li>
                                                                    <div className="custom-theme d-flex bg-theme-dark-info">
                                                                        <div className=" flex-25 demo-side-nav bg-theme-info"> </div>
                                                                        <div className="flex-75 ">
                                                                            <div className="demo-head bg-theme-info"></div>
                                                                            <div className="demo-body bg-theme-info"></div>
                                                                            <div className="demo-head bg-theme-info"></div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li>
                                                                    <div className="custom-theme d-flex bg-theme-dark-cyan">
                                                                        <div className=" flex-25 demo-side-nav bg-theme-cyan"> </div>
                                                                        <div className="flex-75 ">
                                                                            <div className="demo-head bg-theme-cyan"></div>
                                                                            <div className="demo-body bg-theme-cyan"></div>
                                                                            <div className="demo-head bg-theme-cyan"></div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li>
                                                                    <div className="custom-theme d-flex bg-theme-dark-green">
                                                                        <div className=" flex-25 demo-side-nav bg-theme-green"> </div>
                                                                        <div className="flex-75 ">
                                                                            <div className="demo-head bg-theme-green"></div>
                                                                            <div className="demo-body bg-theme-green"></div>
                                                                            <div className="demo-head bg-theme-green"></div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li>
                                                                    <div className="custom-theme d-flex bg-theme-dark-purple">
                                                                        <div className=" flex-25 demo-side-nav bg-theme-purple"> </div>
                                                                        <div className="flex-75 ">
                                                                            <div className="demo-head bg-theme-purple"></div>
                                                                            <div className="demo-body bg-theme-purple"></div>
                                                                            <div className="demo-head bg-theme-purple"></div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li>
                                                                    <div className="custom-theme d-flex bg-theme-dark-orange">
                                                                        <div className=" flex-25 demo-side-nav bg-theme-orange"> </div>
                                                                        <div className="flex-75 ">
                                                                            <div className="demo-head bg-theme-orange"></div>
                                                                            <div className="demo-body bg-theme-orange"></div>
                                                                            <div className="demo-head bg-theme-orange"></div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li>
                                                                    <div className="custom-theme d-flex bg-theme-dark-slate">
                                                                        <div className=" flex-25 demo-side-nav bg-theme-slate"> </div>
                                                                        <div className="flex-75  ">
                                                                            <div className="demo-head bg-theme-slate"></div>
                                                                            <div className="demo-body bg-theme-slate"></div>
                                                                            <div className="demo-head bg-theme-slate"></div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li>
                                                            <h6>Gradient Themes</h6>
                                                            <ul className="list-style-none d-flex theme-color-list">

                                                                <li>
                                                                    <div className="custom-theme d-flex bg-night-day">
                                                                        <div className=" flex-25 demo-side-nav bg-night-day"> </div>
                                                                        <div className="flex-75 ">
                                                                            <div className="demo-head bg-night-day"></div>
                                                                            <div className="demo-body bg-night-day"></div>
                                                                            <div className="demo-head bg-night-day"></div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li>
                                                                    <div className="custom-theme d-flex bg-red-darkred">
                                                                        <div className=" flex-25 demo-side-nav bg-red-darkred"> </div>
                                                                        <div className="flex-75 ">
                                                                            <div className="demo-head bg-red-darkred"></div>
                                                                            <div className="demo-body bg-red-darkred"></div>
                                                                            <div className="demo-head bg-red-darkred"></div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li>
                                                                    <div className="custom-theme d-flex bg-cyan-blue">
                                                                        <div className=" flex-25 demo-side-nav bg-cyan-blue"> </div>
                                                                        <div className="flex-75 ">
                                                                            <div className="demo-head bg-cyan-blue"></div>
                                                                            <div className="demo-body bg-cyan-blue"></div>
                                                                            <div className="demo-head bg-cyan-blue"></div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li>
                                                                    <div className="custom-theme d-flex bg-green-dark">
                                                                        <div className=" flex-25 demo-side-nav bg-green-dark"> </div>
                                                                        <div className="flex-75 ">
                                                                            <div className="demo-head bg-green-dark"></div>
                                                                            <div className="demo-body bg-green-dark"></div>
                                                                            <div className="demo-head bg-green-dark"></div>
                                                                        </div>
                                                                    </div>
                                                                </li>

                                                                <li>
                                                                    <div className="custom-theme d-flex bg-light-green">
                                                                        <div className=" flex-25 demo-side-nav bg-light-green"> </div>
                                                                        <div className="flex-75 ">
                                                                            <div className="demo-head bg-light-green"></div>
                                                                            <div className="demo-body bg-light-green"></div>
                                                                            <div className="demo-head bg-light-green"></div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li>
                                                                    <div className="custom-theme d-flex bg-purple-bliss">
                                                                        <div className=" flex-25 demo-side-nav bg-purple-bliss"> </div>
                                                                        <div className="flex-75 ">
                                                                            <div className="demo-head bg-purple-bliss"></div>
                                                                            <div className="demo-body bg-purple-bliss"></div>
                                                                            <div className="demo-head bg-purple-bliss"></div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>

                                                </div>
                                            </div>
                                        </div>

                                        <div className=" flex-70">
                                            <div className="application-preview-box">
                                                <nav className="side-nav-wrapper">
                                                    <div className="side-nav-menu">
                                                        <div className="side-nav-menu-header">
                                                            <h4>Method83</h4>
                                                            <p><i className="far fa-home mr-r-5"></i>Application Builder</p>
                                                        </div>
                                                        <ul className="side-nav-menu-list list-style-none">
                                                            <div className="  collapse show leftMenuBox " id="topNav">
                                                                <ul className="navbar-nav">
                                                                    <li>
                                                                        <a href="/deviceTypes" className="active">
                                                                            <span><i className="fad fa-chart-network"></i></span>Device Types</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="/deviceManagement"><span><i className="fad fa-tasks"></i></span>Device Management</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="/deviceGroups"><span><i className="fad fa-ball-pile"></i></span>Device Groups</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="/deviceAttributes"><span><i className="fad fa-brackets-curly"></i></span>Device Attributes</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="/events"><span><i className="fad fa-clipboard-list-check"></i></span>Events</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="/dashboards"><span><i className="fad fa-chart-line"></i></span>Dashboards</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="/applications"><span><i className="fad fa-desktop-alt"></i></span>Applications</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="/deviceDiagnostics"><span><i className="fad fa-stethoscope"></i></span>Device Diagnostics</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="/simulation"><span><i className="fad fa-cube"></i></span>Simulation</a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </ul>
                                                        <div className="side-nav-menu-footer">
                                                            <div className="dropdown">
                                                                <button className="dropdown-toggle">
                                                                    <span className="dropdown-initials">NT</span>Nisha Tanwar</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </nav>
                                                <div className="content-item">
                                                    <header className="content-header d-flex">
                                                        <div className="flex-60">
                                                            <h6 onClick={() => { this.props.history.push('/applications') }}>Sample Application</h6>
                                                        </div>
                                                    </header>
                                                </div>
                                                {/* <nav className={"navbar navbar-expand-lg topNav" + this.getTheme("themeHeader")}>
                                                    <p className={"appNavBar" + this.getTheme("themeHeaderTextColor")}>{this.state.payload.name || "Sample Application"}</p>
                                                    {this.state.payload.logoImageUrl ?
                                                        <div className="appLogo">
                                                            <img src={this.state.payload.logoImageUrl} />
                                                        </div> :
                                                        <div className="appLogo">
                                                            <img src="https://content.iot83.com/m83/other/apps.png" />
                                                        </div>
                                                    }
                                                    <button className="navbar-toggler" type="button">
                                                        <i className={this.getTheme("themeHeaderTextColor") + " fal fa-align-left"}> </i>
                                                    </button>
                                                    <ul className="topNavProfile">
                                                        {localStorage.tenantType !== 'SAAS' && <li>
                                                            <div className="topNavNotifyBtn">
                                                                <button type="button" id="notificationBtn" className="btn">
                                                                    <i className={this.getTheme("themeHeaderTextColor") + " fas fa-bell"}></i>
                                                                    <span>0</span>
                                                                </button>
                                                                <button type="button" id="activityBtn" className="btn">
                                                                    <i className={this.getTheme("themeHeaderTextColor") + " fas fa-calendar-alt"}></i>
                                                                </button>
                                                            </div>
                                                        </li>}
                                                        <li>
                                                            <div className="dropdown topNavDropdown">
                                                                <button type="button" className="btn">
                                                                    <i className={"fas fa-chevron-down" + this.getTheme("themeHeaderTextColor")}></i>
                                                                </button>
                                                                <h5 className={this.getTheme("themeHeaderTextColor")}>{localStorage.Username}</h5>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </nav> */}
                                                {/* <div className={"collapse show leftMenuBox " + (this.state.payload.collapsedView ? "" : "appLeftMenuBox ") + this.state.payload.style.themeSideBar} id="topNav">
                                                    <ul className="navbar-nav">
                                                        {this.state.assignMenu.map((nav, index) => {
                                                            let html
                                                            nav.children.length === 0 ?
                                                                html = <li className={(index == 0 ? "active " : "") + "nav-item "}>
                                                                    <a className="nav-link" href="#">
                                                                        <span className={this.getTheme("themeSideBar") + this.getTheme("themeSideBarTextColor") + " nav-link-text"}>{nav.navName}</span>
                                                                        <span className="nav-link-icon">
                                                                            <i className={this.getTheme("themeSideBarTextColor") + " " + (nav.iconName ? nav.iconName.includes('fa-') ? nav.iconName : `fal fa-${nav.iconName}` : "")}></i>
                                                                        </span>
                                                                    </a>
                                                                </li>
                                                                :
                                                                html = <li className="nav-item">
                                                                    <a className="nav-link collapsed" data-toggle="collapse" data-target={"#side" + index}>
                                                                        <span className={this.getTheme("themeSideBarTextColor") + " nav-link-icon"} >
                                                                            <i className={this.getTheme("themeSideBarTextColor") + " " + (nav.iconName ? nav.iconName.includes('fa-') ? nav.iconName : `fal fa-${nav.iconName}` : "")}></i>
                                                                        </span>
                                                                        {!this.state.payload.collapsedView && <span className={this.getTheme("themeSideBar") + this.getTheme("themeSideBarTextColor") + " nav-link-text"}>{nav.navName}</span>}
                                                                        <i className={this.getTheme("themeSideBarTextColor") + " fas fa-caret-down nav-link-arrow"}></i>
                                                                    </a>

                                                                    <div className="collapse" id={"side" + index}>
                                                                        <ul className="listStyleNone">
                                                                            {nav.children.map((temp, childIndex) => (
                                                                                <li className="nav-item" key={childIndex}>
                                                                                    <a className="nav-link text-left" href="#">
                                                                                        <span className="nav-link-icon">
                                                                                            <i className={this.getTheme("themeSideBarTextColor") + (temp.iconName ? temp.iconName.includes('fa-') ? " " + temp.iconName : ` fal fa-${temp.iconName}` : "")}></i>
                                                                                        </span>
                                                                                        <span className={this.getTheme("themeSideBar") + this.getTheme("themeSideBarTextColor") + " nav-link-text"}>{temp.navName}</span>
                                                                                    </a>
                                                                                </li>
                                                                            ))}
                                                                        </ul>
                                                                    </div>
                                                                </li>
                                                            return html
                                                        })}
                                                    </ul>
                                                </div> */}
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-success" onClick={() => this.setState({ themeModal: false })}>Apply</button>
                                    <button type="button" className="btn btn-dark" onClick={() => this.setState({ themeModal: false })}>Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                }
                {/* end add theme modal */}

                {this.state.isOpen &&
                    <NotificationModal
                        type={this.state.modalType}
                        message2={this.state.message2}
                        onCloseHandler={this.onCloseHandler}
                    />
                }
                {this.state.confirmModal &&
                    <ConfirmModel
                        status={"delete"}
                        deleteName="the previously selected device type. All the assigned page would be removed. Do you want to continue ?"
                        confirmClicked={this.resetAllAssignedPages}
                        cancelClicked={() => { this.setState({ confirmModal: false }) }}
                    />
                }
            </React.Fragment >
        );
    }
}

AddOrEditApplication.propTypes = {
    dispatch: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
    isFetching: SELECTORS.getIsFetching(),
    rolesList: SELECTORS.getRoles(),
    rolesError: SELECTORS.getRolesFailure(),
    pages: SELECTORS.getPagesList(),
    pagesListError: SELECTORS.getPagesListError(),
    saveApplicationSuccess: SELECTORS.saveApplicationSuccess(),
    saveApplicationError: SELECTORS.saveApplicationError(),
    getApplicationById: SELECTORS.applicationDetails(),
    getApplicationByIdError: SELECTORS.applicationDetailsError(),
    navList: SELECTORS.navList(),
    navListError: SELECTORS.navListError(),
    imageUploadSuccess: SELECTORS.getImageUploadSuccess(),
    imageUploadFailure: SELECTORS.getImageUploadFailure(),
    getConnectorsListSuccess: SELECTORS.getConnectorsListSuccess(),
    getConnectorsListFailure: SELECTORS.getConnectorsListError(),
});

function mapDispatchToProps(dispatch) {
    return {
        dispatch,
        getAllRoles: () => dispatch(ACTIONS.getAllRoles()),
        getAllPages: () => dispatch(ACTIONS.getAllPages()),
        saveApplication: (id, payload) => dispatch(ACTIONS.saveApplication(id, payload)),
        getApplicationDetailsById: (id) => dispatch(ACTIONS.getApplicationDetailsById(id)),
        uploadImageHandler: (payload) => dispatch(ACTIONS.uploadImageHandler(payload)),
        getNavsForSelectedRole: (roleIds) => dispatch(ACTIONS.getNavsForSelectedRole(roleIds)),
        getConnectorsListByCategory: () => dispatch(ACTIONS.getConnectorsListByCategory()),
    };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "addOrEditApplication", reducer });
const withSaga = injectSaga({ key: "addOrEditApplication", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect,
)(AddOrEditApplication);
