/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import * as CONSTANTS from "./constants";

export function defaultAction() {
  return {
    type: CONSTANTS.DEFAULT_ACTION
  };
}
export function getAllRoles() {
  return {
    type: CONSTANTS.GET_ALL_ROLES,
  };
}

export function getNavsForSelectedRole(roleIds) {
  return {
    type: CONSTANTS.GET_ALL_NAVS,
    roleIds,
  };
}

export function getAllPages() {
  return {
    type: CONSTANTS.GET_PAGES_LIST,
  };
}

export function saveApplication(id,payload) {
  return {
    type: CONSTANTS.SAVE_UPDATE_APPLICATION,
    payload,id
  };
}

export function getApplicationDetailsById(id){
  return {
    type: CONSTANTS.GET_APPLICATION_DETAILS,
    id
  }
}

export function uploadImageHandler(payload) {
  return {
    type: CONSTANTS.IMAGE_UPLOAD_REQUEST,
    payload,
  };
}

export function getConnectorsListByCategory(){
  return {
    type: CONSTANTS.MQTT_CONNECTOR
  }
}