/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { fromJS } from "immutable";
import * as CONSTANTS from "./constants";

export const initialState = fromJS({});

function addOrEditApplicationBuilderReducer(state = initialState, action) {
  switch (action.type) {
    case CONSTANTS.DEFAULT_ACTION:
      return state;
    case CONSTANTS.GET_ALL_ROLES_SUCCESS:
      return Object.assign({}, state, {
        'roles': action.response
      });
    case CONSTANTS.GET_ALL_ROLES_FAILURE:
      return Object.assign({}, state, {
        'rolesError': action.error
      });
    case CONSTANTS.GET_PAGES_LIST_SUCCESS:
      return Object.assign({}, state, {
        pagesList: action.response,
      });
    case CONSTANTS.GET_PAGES_LIST_ERROR:
      return Object.assign({}, state, {
        pagesListError: action.error,
      });
    case CONSTANTS.SAVE_UPDATE_APPLICATION_SUCCESS:
      return Object.assign({}, state, {
        applicationSuccess: {
          message: action.response,
          timestamp: Date.now()
        }
      });
    case CONSTANTS.SAVE_UPDATE_APPLICATION_FAILURE:
      return Object.assign({}, state, {
        applicationError: {
          error: action.error,
          timestamp: Date.now()
        }
      });
    case CONSTANTS.GET_APPLICATION_DETAILS_SUCCESS:
      return Object.assign({}, state, {
        getApplicationDetailsSuccess: action.response,
      });
    case CONSTANTS.GET_APPLICATION_DETAILS_FAILURE:
      return Object.assign({}, state, {
        getApplicationDetailsError: action.error,
      });
    case CONSTANTS.GET_ALL_NAVS_SUCCESS:
      return Object.assign({}, state, {
        navList: action.response,
      });
    case CONSTANTS.GET_ALL_NAVS_FAILURE:
      return Object.assign({}, state, {
        navListError: action.error,
      });
    case CONSTANTS.IMAGE_UPLOAD_REQUEST_SUCCESS:
      return Object.assign({}, state, {
        imageUploadSuccess: action.response
      });
    case CONSTANTS.IMAGE_UPLOAD_REQUEST_FAILURE:
      return Object.assign({}, state, {
        imageUploadFailure: action.error
      });
    case CONSTANTS.MQTT_CONNECTOR_SUCCESS:
      return Object.assign({}, state, {
        getConnectorsListSuccess: action.response,
      });

    case CONSTANTS.MQTT_CONNECTOR_FAILURE:
      return Object.assign({}, state, {
        getConnectorsListFailure: action.error,
      });
    default:
      return state;
  }
}

export default addOrEditApplicationBuilderReducer;
