/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import Loader from '../../components/Loader/Loadable';
import { allSelectors as SELECTORS } from "./selectors";
import { allActions as ACTIONS } from './actions'
//import * as Actions from './actions';
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
//import * as Selectors from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import ReactTooltip from "react-tooltip";
import NotificationModal from '../../components/NotificationModal/Loadable'
import JSONInput from "react-json-editor-ajrm/dist";
import ReactJson from "react-json-view";
import ConfirmModel from "../../components/ConfirmModel";
import AceEditor from "react-ace";
import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';
import 'brace/theme/monokai';
import { compare } from '../../commonUtils';
import SlidingPane from 'react-sliding-pane';
import cloneDeep from 'lodash/cloneDeep';
import AddNewButton from "../../components/AddNewButton/Loadable";
import { getLastPageNumber, getPageNumbers } from "../../components/ListingTable/tableFunctionsRequired";
import range from 'lodash/range';
import debounce from 'lodash/debounce'

/* eslint-disable react/prefer-stateless-function */
let DEFAULT_MONGO_PAYLOAD = {
    collectionName: "",
    documents: []
}
let DEFAULT_TIMESCALE_PAYLOAD = {
    tableName: "",
    documents: [],
    ttl: 30,
    indexes: null,
}
// let DEFAULT_TS_DATA_TYPE = ["SMALLINT", "INTEGER", "BIGINT", "DECIMAL", "NUMERIC", "REAL", "SERIAL", "BIGSERIAL", "CHAR", "CHARACTER", "VARCHAR", "TEXT", "DATE", "TIME", "TIMESTAMP", "TIMESTAMPTZ", "INTERVAL", "BOOLEAN"]
let DEFAULT_TS_DATA_TYPE = ["TEXT", "BIGINT", "DOUBLE_PRECISION"]
//let DEFAULT_TS_CONSTRAINT = ["NOTNULL", "PRIMARYKEY", "UNIQUE", "FOREIGNKEY"]
const PAGE_SIZE_OPTIONS = [5, 10, 20, 30, 50]
export class DataCollections extends React.Component {
    state = {
        filteredList: [],
        collectionDetails: [],
        isFetching: 2,
        selectedCollection: {
            name: '',
            indexes: [],
            offset: 0,
            count: 20,
            documents: {
                objects: [],
                totalCount: 0
            }
        },
        documentsViewType: 'TABLE',
        max: 20,
        deleteCollection: false,
        pagingDirection: true,
        addCollectionPayload: cloneDeep(DEFAULT_MONGO_PAYLOAD),
        addCollectionParam: false,
        errorMessageCode: 0,
        isForCreatingDocument: false,
        searchTerm: '',
        isAddDataCollection: false,
        dataCollectionsData: [],
        queryModal: false,
        openTabs: [],
        timeScaleDataCollectionsData: [],
        activeTabIndex: 0,
        activeCollectionTab: "Mongo",
        selectedCollectionType: "MongoDB",
        alterTablePayload: {
            "addColumnsNameDataType": [],
            "dropColumns": [],
            "tableName": ""
        },
    }

    componentDidMount() {
        this.props.getAllMongoDataCollections();
        this.props.getAllTimeScaleDataCollections();
    }

    static getDerivedStateFromProps(nextProps, state) {
        let newProp = Object.keys(SELECTORS).find(prop => nextProps[prop])
        if (newProp) {
            let propData = nextProps[newProp].error ? nextProps[newProp].error : nextProps[newProp].response;
            let failureAddOns = nextProps[newProp].addOns;

            if (nextProps.mongodataCollectionsSuccess) {
                let openTabs = state.openTabs;
                let activeCollectionTab = state.activeCollectionTab;
                let collectionTypes = ["http", "report", "policy", "analytics"]
                propData.map(dataItem => { // should be handled on BE
                    for (let i = 0; i < collectionTypes.length; i++) {
                        if (dataItem.name.toLowerCase().includes(collectionTypes[i])) {
                            return dataItem.type = collectionTypes[i]
                        }
                    }
                    if (!dataItem.type)
                        dataItem.type = "other"

                    let reqIndex = openTabs.findIndex(({ name }) => dataItem.name === name)
                    if (reqIndex !== -1)
                        openTabs[reqIndex] = {
                            ...openTabs[reqIndex],
                            ...dataItem,
                        }
                })

                if (!openTabs.length && propData.length) {
                    activeCollectionTab = "Mongo"
                    openTabs = [{
                        ...propData[0],
                        activePageNumber: 1,
                        pageSize: 20,
                        dBType: "MongoDB",
                    }]
                }

                propData.length && openTabs.map(tab => {
                    if (tab.dBType === 'MongoDB') {
                        tab.isFetchingDocuments = true
                        tab.error = null
                        let offset = (tab.activePageNumber - 1) * tab.pageSize
                        nextProps.fetchCollectionDocuments(tab.name, tab.pageSize, offset)
                    }
                })

                let isFetching = typeof state.isFetching == "number" ? state.isFetching - 1 : false;
                return ({
                    dataCollectionsData: propData,
                    addCollectionParam: false,
                    openTabs,
                    isFetching,
                    activeCollectionTab,
                    activeTabIndex: openTabs[state.activeTabIndex] ? state.activeTabIndex : 0
                })
            }

            if (nextProps.mongodataCollectionsFailure) {
                let isFetching = typeof state.isFetching == "number" ? state.isFetching - 1 : false;
                return ({
                    isFetching,
                })
            }

            if (nextProps.getSchemaFailure || nextProps.getSchema) {
                let openTabs = cloneDeep(state.openTabs)
                let collectionId = nextProps.getSchemaFailure ? nextProps.getSchemaFailure.addOns.id : nextProps.getSchema.id
                let requiredTab = openTabs.find(collection => collection.name === collectionId)
                if (requiredTab) {
                    requiredTab.isFetchingDocuments = false;
                    if (nextProps.getSchemaFailure) {
                        requiredTab.error = propData;
                    }
                    else {
                        requiredTab.documents = propData.data;
                        requiredTab.count = propData.totalCount
                    }
                }

                return ({
                    openTabs,
                })
            }

            if (nextProps.deleteDataCollectionSuccess) {
                let dataCollectionsData = cloneDeep(state.dataCollectionsData)
                let deletedCollectionIndex = dataCollectionsData.findIndex(collection => collection.name === propData.id)
                dataCollectionsData.splice(deletedCollectionIndex, 1)

                let openTabs = cloneDeep(state.openTabs)
                let requiredTabIndex = openTabs.findIndex(collection => collection.name === propData.id)
                let isNewTabNeeded = false
                let activeTabIndex = state.activeTabIndex
                if (requiredTabIndex !== -1) {
                    openTabs.splice(requiredTabIndex, 1)
                    let areOtherTabsAvailabe = (dataCollectionsData.length || state.timeScaleDataCollectionsData.length)
                    isNewTabNeeded = !openTabs.length && areOtherTabsAvailabe
                    let isTabChangeNeeded = requiredTabIndex === activeTabIndex && openTabs.length
                    if (isNewTabNeeded) {
                        openTabs = [{
                            ...(dataCollectionsData[0] || state.timeScaleDataCollectionsData[0]),
                            isFetchingDocuments: true,
                            activePageNumber: 1,
                            pageSize: 20,
                            dBType: dataCollectionsData[0] ? 'MongoDB' : 'TimescaleDB'
                        }]
                        activeTabIndex = 0;
                        if (dataCollectionsData[0]) {
                            nextProps.fetchCollectionDocuments(openTabs[0].name, 20, 0)
                        } else {
                            nextProps.getTimeScaleDataByTableName(openTabs[0].name, 20, 0)
                        }
                    }
                    else if (isTabChangeNeeded) {
                        activeTabIndex = openTabs.length === requiredTabIndex ? requiredTabIndex - 1 : requiredTabIndex
                    }
                }
                nextProps.showNotification("success", propData.message)

                return {
                    activeTabIndex,
                    openTabs,
                    isFetching: false,
                    dataCollectionsData,
                }
            }

            if (nextProps.deleteDataCollectionFailure) {
                return {
                    deleteCollection: false,
                    isFetching: false,
                }
            }

            if (nextProps.deleteTimescaleDataCollectionSuccess) {
                let timeScaleDataCollectionsData = cloneDeep(state.timeScaleDataCollectionsData)
                let deletedCollectionIndex = timeScaleDataCollectionsData.findIndex(table => table.name === propData.id)
                timeScaleDataCollectionsData.splice(deletedCollectionIndex, 1)
                let openTabs = cloneDeep(state.openTabs)
                let requiredTabIndex = openTabs.findIndex(collection => collection.name === propData.id)
                let isNewTabNeeded = false
                let activeTabIndex = state.activeTabIndex
                if (requiredTabIndex !== -1) {
                    openTabs.splice(requiredTabIndex, 1)
                    let areOtherTabsAvailabe = (state.dataCollectionsData.length || timeScaleDataCollectionsData.length)
                    isNewTabNeeded = !openTabs.length && areOtherTabsAvailabe
                    let isTabChangeNeeded = requiredTabIndex === activeTabIndex && openTabs.length
                    if (isNewTabNeeded) {
                        openTabs = [{
                            ...(timeScaleDataCollectionsData[0] || state.dataCollectionsData[0]),
                            isFetchingDocuments: true,
                            activePageNumber: 1,
                            pageSize: 20,
                            dBType: timeScaleDataCollectionsData[0] ? 'TimescaleDB' : 'MongoDB'
                        }]
                        activeTabIndex = 0
                    }
                    else if (isTabChangeNeeded) {
                        activeTabIndex = openTabs.length === requiredTabIndex ? requiredTabIndex - 1 : requiredTabIndex
                    }
                }
                if (isNewTabNeeded) {
                    if (openTabs[0].dBType === 'TimescaleDB') {
                        nextProps.getTimeScaleDataByTableName(openTabs[0].name, 20, 0)
                    } else {
                        nextProps.fetchCollectionDocuments(openTabs[0].name, 20, 0)
                    }
                }
                nextProps.showNotification("success", propData.message)

                return {
                    activeTabIndex,
                    openTabs,
                    isFetching: false,
                    timeScaleDataCollectionsData,
                }
            }

            if (nextProps.deleteTimescaleDataCollectionFailure) {
                return {
                    deleteCollection: false,
                    isFetching: false,
                }
            }

            if (nextProps.addDataCollectionSuccess) {
                nextProps.showNotification("success", propData.message);
                let isFetching = state.isFetching;
                let openTabs = state.openTabs;
                if (state.isForCreatingDocument) {
                    openTabs = cloneDeep(openTabs)
                    let isPresent = openTabs.find(table => table.name === state.addCollectionPayload.collectionName)
                    if (isPresent) {
                        isPresent.isFetchingDocuments = true
                        nextProps.fetchCollectionDocuments(state.addCollectionPayload.collectionName, isPresent.pageSize, 0)
                    }
                }
                else {
                    isFetching = true
                    nextProps.getAllMongoDataCollections()
                }
                return {
                    isSavingCollection: false,
                    isAddDataCollection: false,
                    isFetching,
                    openTabs,
                    addCollectionPayload: cloneDeep(DEFAULT_MONGO_PAYLOAD),
                }
            }

            if (nextProps.addDataCollectionFailure) {
                return {
                    isSavingCollection: false
                }
            }

            if (nextProps.addMongoDocumentSuccess) {
                nextProps.showNotification("success", propData.message);
                let openTabs = cloneDeep(state.openTabs);
                let isPresent = openTabs.find(table => table.name === state.addCollectionPayload.collectionName)
                if (isPresent) {
                    isPresent.isFetchingDocuments = true
                    nextProps.fetchCollectionDocuments(state.addCollectionPayload.collectionName, isPresent.pageSize, 0)
                }
                return {
                    isSavingCollection: false,
                    isAddDataCollection: false,
                    openTabs,
                    addCollectionPayload: cloneDeep(DEFAULT_MONGO_PAYLOAD),
                }
            }

            if (nextProps.addMongoDocumentFailure) {
                return {
                    isSavingCollection: false
                }
            }

            if (nextProps.clearDataCollectionSuccess) {
                let dataCollectionsData = cloneDeep(state.dataCollectionsData)
                let clearedCollectionName = nextProps.clearDataCollectionSuccess.name
                let requiredCollection = dataCollectionsData.find(collection => collection.name === clearedCollectionName)
                requiredCollection.count = 0

                let openTabs = cloneDeep(state.openTabs)
                let requiredTab = openTabs.find(collection => collection.name === clearedCollectionName)
                if (requiredTab) {
                    requiredTab.documents = []
                    requiredTab.isFetchingDocuments = false
                }

                return ({
                    dataCollectionsData,
                    openTabs
                })
            }

            if (nextProps.clearDataCollectionFailure) {
                let clearedCollectionName = failureAddOns.name
                let openTabs = cloneDeep(state.openTabs)
                let requiredTab = openTabs.find(collection => collection.name === clearedCollectionName)
                if (requiredTab) {
                    requiredTab.isFetchingDocuments = false
                }
                return ({
                    openTabs,
                })
            }

            if (nextProps.getQuerySuccess) {
                let queryResponse = nextProps.getQuerySuccess.response.data.objects;
                return ({
                    queryResponse,
                    isExecutingQuery: false
                })
            }

            if (nextProps.getQueryFailure) {
                return ({
                    queryError: nextProps.getQueryFailure.error,
                    isExecutingQuery: false
                })
            }

            if (nextProps.getTimeScaleQuerySuccess) {
                let queryResponse = nextProps.getTimeScaleQuerySuccess.response.data;
                return ({
                    queryResponse,
                    isExecutingQuery: false
                })
            }

            if (nextProps.getTimeScaleQueryFailure) {
                return ({
                    queryError: nextProps.getTimeScaleQueryFailure.error,
                    isExecutingQuery: false
                })
            }



            if (nextProps.timeScaleDataCollectionsSuccess) {
                let openTabs = cloneDeep(state.openTabs);
                let activeCollectionTab = state.activeCollectionTab;
                if (!openTabs.length && propData.length) {
                    activeCollectionTab = "TS"
                    openTabs = [{
                        ...propData[0],
                        activePageNumber: 1,
                        pageSize: 20,
                        dBType: "TimescaleDB",
                    }]
                }
                propData.length && openTabs.map(tab => {
                    if (tab.dBType === 'TimescaleDB') {
                        let offset = (tab.activePageNumber - 1) * tab.pageSize
                        tab.isFetchingDocuments = true
                        tab.error = null
                        nextProps.getTimeScaleDataByTableName(tab.name, 20, 0)
                    }
                })

                let isFetching = typeof state.isFetching == "number" ? state.isFetching - 1 : false;
                return ({
                    isFetching,
                    timeScaleDataCollectionsData: propData,
                    openTabs,
                    activeCollectionTab,
                    activeTabIndex: openTabs[state.activeTabIndex] ? state.activeTabIndex : 0
                })
            }

            if (nextProps.timeScaleDataCollectionsFailure) {
                let isFetching = typeof state.isFetching == "number" ? state.isFetching - 1 : false;
                return ({
                    isFetching,
                })
            }


            if (nextProps.createTimeScaleDBTableSuccess) {
                nextProps.showNotification("success", propData.message);
                nextProps.getAllTimeScaleDataCollections();
                return {
                    isFetching: true,
                    isSavingCollection: false,
                    addCollectionPayload: cloneDeep(DEFAULT_MONGO_PAYLOAD),
                    isAddDataCollection: false,
                }
            }

            if (nextProps.createTimeScaleDBTableFailure) {
                return {
                    isSavingCollection: false
                }
            }

            if (nextProps.timeScaleDataByTableNameSuccess) {
                let name = nextProps.timeScaleDataByTableNameSuccess.tableName;
                let openTabs = state.openTabs;
                let tabIndex = openTabs.findIndex(tab => tab.name === name)
                if (tabIndex !== -1) {
                    openTabs = cloneDeep(state.openTabs)
                    let requiredTab = openTabs[tabIndex]
                    requiredTab.isFetchingDocuments = false;
                    requiredTab.documents = propData.data;
                    requiredTab.count = propData.totalCount;
                    requiredTab.indexes = propData.indexes;
                }
                return ({
                    openTabs,
                })
            }
            if (nextProps.timeScaleDataByTableNameFailure) {
                let name = failureAddOns.table;
                let openTabs = state.openTabs
                let tabIndex = openTabs.findIndex(tab => tab.name === name)
                if (tabIndex !== -1) {
                    openTabs = cloneDeep(state.openTabs)
                    let requiredTab = openTabs[tabIndex]
                    requiredTab.isFetchingDocuments = false;
                    requiredTab.error = propData
                }
                return ({
                    openTabs,
                })
            }

            if (nextProps.clearTSTableDataSuccess) {
                let clearedCollectionName = nextProps.clearTSTableDataSuccess.tableName
                let openTabs = cloneDeep(state.openTabs)
                let requiredTab = openTabs.find(collection => collection.name === clearedCollectionName)
                if (requiredTab) {
                    requiredTab.documents = []
                    requiredTab.isFetchingDocuments = false
                }
                return ({
                    openTabs
                })
            }

            if (nextProps.clearTSTableDataFailure) {
                let openTabs = cloneDeep(state.openTabs)
                let requiredCollectionName = nextProps.clearTSTableDataFailure.addOns.payload.tableName
                let requiredTab = openTabs.find(collection => collection.name === requiredCollectionName)
                if (requiredTab) {
                    requiredTab.isFetchingDocuments = false
                }
                return {
                    openTabs
                }
            }
            if (nextProps.addTimescaleDocumentsSuccess) {
                nextProps.showNotification("success", propData);
                // nextProps.getAllMongoDataCollections()
                let openTabs = cloneDeep(state.openTabs)
                let isPresent = openTabs.find(table => table.name === nextProps.addTimescaleDocumentsSuccess.tableName)
                if (isPresent) {
                    isPresent.isFetchingDocuments = true
                    nextProps.getTimeScaleDataByTableName(nextProps.addTimescaleDocumentsSuccess.tableName, isPresent.pageSize, 0)
                }

                return {
                    //isFetching: true,
                    openTabs,
                    addCollectionPayload: cloneDeep(DEFAULT_MONGO_PAYLOAD),
                    isSavingCollection: false,
                    isAddDataCollection: false,
                }
            }
            if (nextProps.addTimescaleDocumentsFailure) {
                return {
                    isSavingCollection: false,
                }
            }
            if (nextProps.describeTimeScaleTableSuccess) {
                return {
                    timeScaleColumnsDescription: propData,
                    isFetchingTimeScaleColumns: false,
                }
            }
            if (nextProps.describeTimeScaleTableFailure) {
                return {
                    isFetchingTimeScaleColumns: false,
                }
            }
            if (nextProps.alterTimeScaleTableSuccess) {
                let openTabs = cloneDeep(state.openTabs);
                let openTab = openTabs[state.activeTabIndex];
                openTab.isFetchingDocuments = true;
                nextProps.showNotification("success", nextProps.alterTimeScaleTableSuccess.response.message)
                nextProps.getTimeScaleDataByTableName(openTab.name, openTab.pageSize, 0);
                return {
                    openTabs,
                    isAddingColumn: false,
                    isAlterTSTable: false,
                    isAddDataCollection: false,
                }
            }

            if (nextProps.alterTimeScaleTableFailure) {
                return {
                    isAddingColumn: false,
                }
            }
        }
        return null;
    }

    componentDidUpdate(prevProps, prevState) {
        let newProp = Object.keys(SELECTORS).find(prop => this.props[prop])
        if (newProp) {
            if (newProp.includes("Failure")) {
                this.props.showNotification("error", this.props[newProp].error)
            }

            this.props.resetToInitialState(newProp)
        }
    }

    onCloseHandler = () => {
        this.setState({
            message2: '',
            isOpen: false,
            type: ''
        })
    }

    fetchSchema = (element) => {
        let collectionDetails = this.state.collectionDetails;
        if (collectionDetails.some(collection => collection.name === element.name)) {
            collectionDetails = collectionDetails.filter(collection => collection.name !== element.name)
        }
        if (collectionDetails.length === 5) {
            collectionDetails.splice(0, 1);
        }
        let customDocumentObject = JSON.parse(JSON.stringify(element))
        customDocumentObject["offset"] = 0;
        customDocumentObject["count"] = 20;
        customDocumentObject["isAddDocDisbaled"] = (customDocumentObject.name.startsWith("ETL_") || customDocumentObject.name.startsWith("SYSTEM_")) ? true : false;
        let selectedCollection = customDocumentObject;
        selectedCollection.documents = selectedCollection.documents ? selectedCollection.documents : { objects: null, totalCount: element.count }

        collectionDetails.push(selectedCollection);

        this.setState({
            collectionDetails,
            selectedCollection,
            max: 20,
            isFetchingCollectionDocuments: true
        }, () => this.props.fetchCollectionDocuments(element.name, this.state.max, this.state.selectedCollection.offset))
    }

    removeCollectionDetails = (event, collectionName) => {
        event.stopPropagation();
        let collectionDetails = JSON.parse(JSON.stringify(this.state.collectionDetails));
        collectionDetails = collectionDetails.filter(collection => collection.name !== collectionName);
        this.setState({
            collectionDetails,
            selectedCollection: collectionDetails[collectionDetails.length - 1],
        });
    }

    tabChangeHandler = (selectedCollection) => {
        this.setState({
            selectedCollection,
        });
    }

    handleViewChange = (type) => {
        this.setState({
            documentsViewType: type,
        })
    }

    deleteDataCollection = (name, collectionType) => {
        let actionType = collectionType === "timescaleDB" ? "deleteTimescaleDataCollection" : "deleteDataCollection"
        let confirmationHandler = () => {
            this.setState({
                isFetching: true
            }, () => this.props[actionType](name, collectionType))
        }
        this.props.showConfirmationModal(name, confirmationHandler)
    }

    clearDataCollection = (name, collectionType) => {
        let confirmationHandler = () => {
            let openTabs = cloneDeep(this.state.openTabs)
            openTabs[this.state.activeTabIndex].isFetchingDocuments = true
            openTabs[this.state.activeTabIndex].count = 0
            this.setState({
                openTabs
            }, () => collectionType ? this.props.clearDataCollection(name, collectionType) :
                this.props.clearTSTableData({
                    conditions: [],
                    tableName: name
                }))
        }

        this.props.showConfirmationModal(name, confirmationHandler, null, "clear")
    }

    onNextFetch = () => {
        if (this.state.selectedCollection.count < this.state.selectedCollection.documents.totalCount) {
            let selectedCollection = this.state.selectedCollection;
            selectedCollection.offset += 20;
            selectedCollection.count += 20
            this.setState({
                selectedCollection,
                pagingDirection: true,
                isFetchingCollectionDocuments: true
            }, () => this.props.fetchCollectionDocuments(this.state.selectedCollection.name, this.state.max, this.state.selectedCollection.offset, this.state.selectedCollection.count))
        }
    }

    onPreviousFetch = () => {
        if (this.state.selectedCollection.offset > 0) {
            let selectedCollection = this.state.selectedCollection;
            selectedCollection.offset -= 20;
            selectedCollection.count -= 20
            this.setState({
                selectedCollection,
                pagingDirection: false,
                isFetchingCollectionDocuments: true
            }, () => this.props.fetchCollectionDocuments(this.state.selectedCollection.name, this.state.max, this.state.selectedCollection.offset, this.state.selectedCollection.count))
        }
    }

    handleInput = (event, isJsonInput = false) => {
        let addCollectionPayload = JSON.parse(JSON.stringify(this.state.addCollectionPayload)),
            errorMessageCode = 0;

        if (isJsonInput) {
            addCollectionPayload.documents = event.jsObject;
            this.setState({
                addCollectionPayload,
                errorMessageCode
            })
        }
        else {
            if (/^[a-zA-Z0-9_]*$/.test(event.target.value) || /^$/.test(event.target.value)) {
                addCollectionPayload[event.target.id] = event.target.value
            }
            this.setState({
                addCollectionPayload
            })
        }
    }

    handletimescaleDocumentAddition = (isArray) => {
        let errorMessageCode = isArray ? 0 : 1
        this.setState({
            isSavingCollection: isArray,
            errorMessageCode,
        }, () => isArray && this.props.addTimescaleDocuments(this.state.addCollectionPayload))
    }


    addDataCollection = () => {
        event.preventDefault();
        if (this.state.isAlterTSTable) {
            if (this.state.alterTablePayload.addColumnsNameDataType.length && this.state.alterTablePayload.addColumnsNameDataType.some((column) => (!column.name || !column.dataType))) {
                this.props.showNotification("error", "Missing Data Type in added column(s)")
                return;
            }
            let columnDataType = {};
            let addColumnsNameDataType = cloneDeep(this.state.alterTablePayload.addColumnsNameDataType);
            for (let i = 0; i < addColumnsNameDataType.length; i++) {
                columnDataType[addColumnsNameDataType[i].name] = addColumnsNameDataType[i].dataType
            }
            this.setState({
                isAddingColumn: true
            }, () => this.props.alterTimeScaleTable(
                {
                    addColumnsNameDataType: columnDataType,
                    dropColumns: this.state.alterTablePayload.dropColumns,
                    tableName: this.state.alterTablePayload.tableName,
                }))
            return;
        }
        let isDocumentEmpty = !(Array.isArray(this.state.addCollectionPayload.documents) && this.state.addCollectionPayload.documents.length)
        if (isDocumentEmpty) {
            this.setState({
                errorMessageCode: 1,
            })
            return;
        }
        
        if (this.state.isForCreatingDocument && this.state.selectedCollectionType === "TimescaleDB") {
            this.handletimescaleDocumentAddition(this.state.addCollectionPayload.documents)
            return;
        }

        let actionType = this.state.selectedCollectionType === "MongoDB" ? this.state.isForCreatingDocument ? "addMongoDocument" : "addDataCollection" : "createTimeScaleDBTable"
        this.setState({
            isSavingCollection: true
        }, () => this.props[actionType](this.state.addCollectionPayload))
    }

    addCollectionOrDocument = (isForCreatingDocument, index = -1) => {
        let requiredCollection = this.state.openTabs[index],
            isTimescaleCollection = requiredCollection && requiredCollection.dBType === "TimescaleDB";
        let addCollectionPayload = cloneDeep(isTimescaleCollection ? DEFAULT_TIMESCALE_PAYLOAD : DEFAULT_MONGO_PAYLOAD);
        if (isForCreatingDocument) {
            if (isTimescaleCollection) {
                addCollectionPayload.tableName = requiredCollection.name
            }
            else {
                addCollectionPayload.collectionName = requiredCollection.name
            }
        }
        this.setState({
            addCollectionPayload,
            //isAlterTSTable: false,
            isForCreatingDocument,
            selectedCollectionType: isTimescaleCollection ? "TimescaleDB" : "MongoDB",
            isAddDataCollection: true,
        })
    }

    getQuery = () => {
        let query = JSON.parse(JSON.stringify(this.state.query)),
            encodedQuery = btoa(query),
            payload = {
                offset: 0,
                query: encodedQuery
            };
        this.setState({
            isExecutingQuery: true,
            queryError: false
        }, () => this.props.getQuery(payload))
    }

    getTimeScaleQuery = () => {
        let query = JSON.parse(JSON.stringify(this.state.query)),
            encodedQuery = btoa(query),
            payload = {
                query: encodedQuery
            };
        this.setState({
            isExecutingQuery: true,
            queryError: false
        }, () => this.props.timeScaleQuery(payload))
    }

    databaseQueryHandler = ({ currentTarget }) => {
        let query = this.state.openTabs[this.state.activeTabIndex].dBType === "MongoDB" ? `db.${this.state.openTabs[this.state.activeTabIndex].name}.find({})` : `SELECT * FROM ${this.state.openTabs[this.state.activeTabIndex].tableName}`;
        this.setState({
            selectedDatabase: currentTarget.value,
            query
        })
    }

    toggleActiveCategory = (collectionCategory) => {
        let isActive = collectionCategory === this.state.activeCategory;
        this.setState({
            activeCategory: isActive ? false : collectionCategory
        })
    }

    openTabsHandler = (collection, dBType) => {
        let openTabs = cloneDeep(this.state.openTabs);
        if (!openTabs.find(alreadyPresent => alreadyPresent.name === collection.name)) {
            if (!collection.documents) {
                collection = {
                    ...collection,
                    dBType,
                    isFetchingDocuments: true,
                    activePageNumber: 1,
                    goToPageValue: 1,
                    pageSize: 20,
                    error: null
                }
            }
            openTabs.push(collection)
            this.setState({
                openTabs,
                activeTabIndex: openTabs.length - 1
            }, () => {
                dBType === 'MongoDB' ?
                    collection.isFetchingDocuments && this.props.fetchCollectionDocuments(collection.name, this.state.max, 0) :
                    collection.isFetchingDocuments && this.props.getTimeScaleDataByTableName(collection.name, this.state.max, 0);

            })
        } else {
            let activeTabIndex = openTabs.findIndex(alreadyPresent => alreadyPresent.name === collection.name)
            this.setState({ activeTabIndex })
        }
    }

    closeTabHandler = (event) => {
        event.stopPropagation()
        let openTabs = cloneDeep(this.state.openTabs);
        let index = Number(event.currentTarget.id)
        openTabs.splice(index, 1)
        let activeTabIndex = openTabs.length === index ? index - 1 : index
        this.setState({
            openTabs,
            activeTabIndex
        })
    }

    setActiveTab = ({ currentTarget }) => {
        let activeTabIndex = Number(currentTarget.id)
        this.setState({
            activeTabIndex
        })
    }

    onCloseModal = () => {
        this.setState({
            addCollectionPayload: cloneDeep(DEFAULT_MONGO_PAYLOAD),
            isAddDataCollection: false,
            isAlterTSTable: false,
            errorMessageCode: 0
        })
    }

    refreshJsonInputHandler = (index) => {
        let openTabs = cloneDeep(this.state.openTabs);
        openTabs[index].isFetchingDocuments = true;
        openTabs[index].error = null;
        openTabs[index].activePageNumber = openTabs[index].activePageNumber === "" ? 1 : openTabs[index].activePageNumber;
        openTabs[index].goToPageValue = openTabs[index].activePageNumber;
        this.setState({ openTabs }, () => openTabs[index].dBType === 'MongoDB' ?
            this.props.fetchCollectionDocuments(openTabs[index].name, openTabs[index].pageSize, ((openTabs[index].activePageNumber - 1) * openTabs[index].pageSize)) :
            this.props.getTimeScaleDataByTableName(openTabs[index].name, openTabs[index].pageSize, ((openTabs[index].activePageNumber - 1) * openTabs[index].pageSize)))
    }

    pageChangeHandler = (pageSize, activePageNumber) => {
        let openTabs = cloneDeep(this.state.openTabs)
        let activeCollectionTab = openTabs[this.state.activeTabIndex]

        let pageSizeChanged = pageSize !== activeCollectionTab.pageSize
        let totalPages = Math.ceil(activeCollectionTab.count / pageSize)
        if (pageSizeChanged) {
            if (totalPages && totalPages < activePageNumber) {
                activePageNumber = totalPages
            }
        } else if (activePageNumber === "...") {
            activePageNumber = Math.ceil((getPageNumbers(totalPages, activeCollectionTab.activePageNumber)[3] - 1) / 2)
        }
        activeCollectionTab.pageSize = pageSize;
        activeCollectionTab.activePageNumber = activePageNumber;
        activeCollectionTab.isFetchingDocuments = true;
        activeCollectionTab.error = null;
        activeCollectionTab.goToPageValue = activePageNumber;
        this.setState({
            openTabs,
        }, () => {
            let offset = (activeCollectionTab.activePageNumber - 1) * activeCollectionTab.pageSize
            activeCollectionTab.dBType === "MongoDB" ?
                this.props.fetchCollectionDocuments(activeCollectionTab.name, activeCollectionTab.pageSize, offset) :
                this.props.getTimeScaleDataByTableName(activeCollectionTab.name, activeCollectionTab.pageSize, offset)
        })
    }

    refreshComponent = () => {
        let openTabs = cloneDeep(this.state.openTabs)
        let activeCollectionTab = openTabs[this.state.activeTabIndex]
        if (activeCollectionTab) {
            activeCollectionTab.activePageNumber = activeCollectionTab.activePageNumber !== "" ? activeCollectionTab.activePageNumber : 1;
            activeCollectionTab.goToPageValue = activeCollectionTab.activePageNumber;
            activeCollectionTab.error = null;
        }
        this.setState({
            openTabs,
            isFetching: 2,
        }, () => {
            this.props.getAllMongoDataCollections()
            this.props.getAllTimeScaleDataCollections()
        })
    }

    collectionTypeChangeHandler = ({ currentTarget }) => {
        let selectedCollectionType = currentTarget.value
        let addCollectionPayload = selectedCollectionType === "MongoDB" ? cloneDeep(DEFAULT_MONGO_PAYLOAD) : cloneDeep(DEFAULT_TIMESCALE_PAYLOAD)
        this.setState({
            selectedCollectionType: currentTarget.value,
            addCollectionPayload,
            errorMessageCode: 0
        })
    }

    getGoToPageOptions = (activeCollectionTab) => {
        let options = range(1, Math.ceil(activeCollectionTab.count / activeCollectionTab.pageSize) + 1)
        if (options.length) {
            return <ul className="dropdown-menu">
                {options.map((pageNumber, index) => (
                    <li key={index} onClick={() => pageNumber !== activeCollectionTab.activePageNumber && this.pageChangeHandler(activeCollectionTab.pageSize, pageNumber)}>{pageNumber}</li>
                ))}
            </ul>
        }
    }

    copyToClipboard = (name) => {
        navigator.clipboard.writeText(name)
        this.setState({
            type: "success",
            isOpen: true,
            message2: 'Copied to Clipboard successfully !',
        })
    }

    alterTSTable = (index) => {
        let requiredCollection = this.state.openTabs[index],
            addCollectionPayload = cloneDeep(DEFAULT_TIMESCALE_PAYLOAD),
            alterTablePayload = cloneDeep(this.state.alterTablePayload);
        alterTablePayload.addColumnsNameDataType = [];
        alterTablePayload.dropColumns = [];
        alterTablePayload.tableName = requiredCollection.name
        addCollectionPayload.tableName = requiredCollection.name
        this.setState({
            addCollectionPayload,
            alterTablePayload,
            isAddColumnClicked: false,
            isForCreatingDocument: false,
            selectedCollectionType: "TimescaleDB",
            isAddDataCollection: true,
            isAlterTSTable: true,
            isFetchingTimeScaleColumns: true,
        }, () => this.props.describeTimeScaleTable(requiredCollection.name))
    }

    columnHandlerInTSTable = ({ currentTarget }, index) => {
        let alterTablePayload = cloneDeep(this.state.alterTablePayload);
        alterTablePayload.addColumnsNameDataType[index][currentTarget.id] = currentTarget.value.startsWith("Select") ? "" : currentTarget.value;

        this.setState({
            alterTablePayload
        })
    }

    deleteTSColumn = (columnName) => {
        let timeScaleColumnsDescription = cloneDeep(this.state.timeScaleColumnsDescription);
        let alterTablePayload = cloneDeep(this.state.alterTablePayload);
        alterTablePayload.dropColumns.push(columnName)
        delete timeScaleColumnsDescription[columnName]

        this.setState({
            timeScaleColumnsDescription,
            alterTablePayload
        })

    }

    addColumnInTSTable = () => {
        let alterTablePayload = cloneDeep(this.state.alterTablePayload)
        alterTablePayload.addColumnsNameDataType.push({
            name: "",
            dataType: "",
        })
        this.setState({
            isAddColumnClicked: true,
            alterTablePayload
        })
    }

    removeColumnChild = (index) => {
        let alterTablePayload = cloneDeep(this.state.alterTablePayload)
        alterTablePayload.addColumnsNameDataType.splice(index, 1);
        this.setState({
            alterTablePayload
        })
    }

    // foreignKeyHandler = ({currentTarget} , index) => {
    //     let foreignKeyPayload = cloneDeep(this.state.foreignKeyPayload);
    //     foreignKeyPayload[currentTarget.id] = currentTarget.value
    //     currentTarget.id === "TableNameForForeignKey" && this.props.describeTimeScaleTable(currentTarget.value)

    //     this.setState({
    //         foreignKeyPayload,
    //         fetchingColumns: true,
    //         isForiegnKeySelected: this.state.alterTablePayload.addColumnsNameDataType[index].constraint === DEFAULT_TS_CONSTRAINT[3]
    //     })

    // }

    getAllColumnsForAgGrid = () => {
        let activeCollectionTab = this.state.openTabs[this.state.activeTabIndex]
        let columnData = activeCollectionTab && activeCollectionTab.documents && Object.values(activeCollectionTab.documents)[0]
        if (columnData) {
            columnData = Object.keys(columnData).map((column) => ({ 'headerName': column, 'field': column }))
            return columnData;
        }
    }

    getTableData = () => {
        let activeCollectionTab = this.state.openTabs[this.state.activeTabIndex];
        let data = activeCollectionTab.documents ? activeCollectionTab.documents : [];
        return data;
    }

    getQueryResponseColumn = () => {
        let columnData = this.state.queryResponse && this.state.queryResponse[0]
        if (columnData) {
            columnData = Object.keys(columnData).map((column) => ({ 'headerName': column, 'field': column }))
            return columnData;
        }
    }

    debouncedPageChangeHandler = debounce((pageSize, pageNumber) => this.pageChangeHandler(pageSize, pageNumber), 500)

    gotoPageChangeHandler = (value, totalPages) => {
        value = value ? Number(value) : "";
        let isValidValue = value === "" || (value > 0 && value <= totalPages)
        if (isValidValue) {
            let openTabs = cloneDeep(this.state.openTabs)
            let activeCollectionTab = openTabs[this.state.activeTabIndex]
            activeCollectionTab.goToPageValue = value
            this.setState({
                openTabs
            }, () => value && this.debouncedPageChangeHandler(activeCollectionTab.pageSize, value))
        }
    }

    render() {
        let activeCollectionTab = this.state.openTabs[this.state.activeTabIndex];
        let totalPages = activeCollectionTab && activeCollectionTab.count ? Math.ceil(activeCollectionTab.count / activeCollectionTab.pageSize) : 0
        let mongoFilterList = (this.state.searchTerm ? this.state.dataCollectionsData.filter(element => (element.name.toLowerCase()).includes(this.state.searchTerm.toLowerCase())) : this.state.dataCollectionsData)
        let timeScaleFilterList = (this.state.searchTerm ? this.state.timeScaleDataCollectionsData.filter(element => (element.name.toLowerCase()).includes(this.state.searchTerm.toLowerCase())) : this.state.timeScaleDataCollectionsData)
        let isSystemGeneratedDatabase = activeCollectionTab && (activeCollectionTab.name.startsWith("etl_") || activeCollectionTab.name.startsWith("system_"))
        return (
            <React.Fragment>
                <Helmet>
                    <title>Databases</title>
                    <meta name="description" content="M83-DataCollections" />
                </Helmet>

                <header className="content-header d-flex">
                    <div className="flex-60">
                        <h6>Databases</h6>
                    </div>

                    <div className="flex-40 text-right">
                        <div className="content-header-group">
                            <div className="search-box">
                                <span className="search-icon"><i className="far fa-search"></i></span>
                                <input type="text" disabled={this.state.isFetching} className="form-control" placeholder="Search..." value={this.state.searchTerm} onChange={({ currentTarget }) => {
                                    this.setState({
                                        searchTerm: currentTarget.value
                                    })
                                }} />
                                <button className="search-button" onClick={() => { this.setState({ searchTerm: '' }) }}><i className="far fa-times"></i></button>
                            </div>
                            <button className="btn btn-light" disabled={this.state.isFetching} data-tooltip data-tooltip-text="Refresh" data-tooltip-place="bottom" onClick={() => this.refreshComponent()}><i className="far fa-sync-alt"></i></button>
                            <button className="btn btn-primary" disabled={this.state.isFetching} data-tooltip data-tooltip-text="Add Collection" data-tooltip-place="bottom" onClick={() => this.addCollectionOrDocument(false)}>
                                <i className="far fa-plus"></i>
                            </button>
                        </div>
                    </div>
                </header>

                {this.state.isFetching ? <Loader /> :
                    <React.Fragment>
                        {(this.state.dataCollectionsData.length || this.state.timeScaleDataCollectionsData.length) ?
                            <div className="content-body collection-content-wrapper">
                                <div className="form-content-wrapper form-content-wrapper-left">
                                    <div className="form-content-box mb-0">
                                        <ul className="list-style-none d-flex collection-tabs">
                                            {this.state.openTabs.map((tab, index) =>
                                                <li className="flex-16_6" key={index}>
                                                    <a id={index} onClick={this.setActiveTab} className={this.state.activeTabIndex === index ? "active" : ""} data-tooltip data-tooltip-text={tab.name} data-tooltip-place="bottom">
                                                        <img src={`https://content.iot83.com/m83/dataConnectors/${tab.dBType === "MongoDB" ? "mongodb" : "timescaledb"}.png`} />
                                                        {tab.name}
                                                        {this.state.openTabs.length > 1 && <button className="btn btn-link" id={index} onClick={this.closeTabHandler} ><i className="far fa-times"></i></button>}
                                                    </a>
                                                </li>
                                            )}
                                        </ul>

                                        <div className="form-content-body p-3">
                                            <div className="collection-content-header d-flex align-items-center">
                                                <div className="flex-65">
                                                    {activeCollectionTab.ttl &&
                                                        <React.Fragment>
                                                            <h6> TimeToLive(in days):</h6>
                                                            <span className="badge badge-pill badge-info">{activeCollectionTab.ttl} </span>
                                                        </React.Fragment>
                                                    }
                                                    {activeCollectionTab.indexes &&
                                                        <React.Fragment>
                                                            <h6>Indexes :</h6>
                                                            {activeCollectionTab.indexes.map((index, i) =>
                                                                <span className="badge badge-pill badge-info" key={i} >{index.indexName}</span>
                                                            )}
                                                        </React.Fragment>
                                                    }
                                                </div>
                                                <div className="flex-35">
                                                    <div className="button-group text-right">
                                                        <button className="btn-transparent btn-transparent-blue" data-tooltip data-tooltip-text={isSystemGeneratedDatabase ? "Cannot Add Document in System Generated Database." : "Add Document"} data-tooltip-place="bottom" disabled={isSystemGeneratedDatabase} onClick={() => this.addCollectionOrDocument(true, this.state.activeTabIndex)} ><i className="far fa-plus"></i></button>
                                                        {activeCollectionTab.dBType === "TimescaleDB" && <button className="btn-transparent btn-transparent-yellow" data-tooltip data-tooltip-text="Alter Table" data-tooltip-place="bottom" onClick={() => this.alterTSTable(this.state.activeTabIndex)}><i className="far fa-edit"></i></button>}
                                                        <button
                                                            type="button"
                                                            className="btn-transparent btn-transparent-green"
                                                            disabled={!activeCollectionTab.documents || activeCollectionTab.documents.length === 0}
                                                            data-tooltip data-tooltip-text="Execute Query"
                                                            data-tooltip-place="bottom"
                                                            onClick={() => {
                                                                let query = activeCollectionTab && activeCollectionTab.dBType === "MongoDB" ? `db.${activeCollectionTab.name}.find({})` : activeCollectionTab.dBType === "TimescaleDB" ? `SELECT * FROM ${activeCollectionTab.name}` : "";
                                                                let selectedDatabase = activeCollectionTab.dBType
                                                                this.setState({ query, queryResponse: [], queryModal: true, selectedDatabase })
                                                            }}>
                                                            <i className="far fa-cog"></i>
                                                        </button>
                                                        <button className={`btn-transparent btn-transparent-cyan ${this.state.documentsViewType === 'TABLE' ? "active" : ""}`} data-tooltip data-tooltip-text="Table View" data-tooltip-place="bottom" onClick={() => this.handleViewChange('TABLE')}><i className="far fa-table"></i></button>
                                                        <button className={`btn-transparent btn-transparent-yellow ${this.state.documentsViewType === 'FILE' ? "active" : ""}`} data-tooltip data-tooltip-text="JSON View" data-tooltip-place="bottom" onClick={() => this.handleViewChange('FILE')}><i className="far fa-folder"></i></button>
                                                        <button className={`btn-transparent btn-transparent-cyan ${this.state.documentsViewType === 'TREE' ? "active" : ""}`} data-tooltip data-tooltip-text="Tree View" data-tooltip-place="bottom" onClick={() => this.handleViewChange('TREE')}><i className="far fa-window-alt"></i></button>
                                                        <button className="btn-transparent btn-transparent-green" data-tooltip data-tooltip-text="Refresh" data-tooltip-place="bottom" onClick={() => this.refreshJsonInputHandler(this.state.activeTabIndex)}><i className="far fa-sync-alt"></i></button>
                                                        <button className="btn-transparent btn-transparent-red" data-tooltip data-tooltip-text="Clear" data-tooltip-place="bottom" onClick={() => this.clearDataCollection(activeCollectionTab.name, activeCollectionTab.type)}><i className="far fa-broom"></i></button>
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="collection-content-body">
                                                {activeCollectionTab.isFetchingDocuments ?
                                                    <div className="inner-loader-wrapper border border-radius-4 h-100">
                                                        <div className="inner-loader-content">
                                                            <i className="fad fa-sync-alt fa-spin"></i>
                                                        </div>
                                                    </div> :
                                                    activeCollectionTab.error ?
                                                        <div className="inner-message-wrapper border border-radius-4 h-100">
                                                            <div className="inner-message-content">
                                                                <i className="fad fa-exclamation-triangle text-red"></i>
                                                                <h6 className="text-red">{activeCollectionTab.error}</h6>
                                                            </div>
                                                        </div> :
                                                        this.state.documentsViewType === 'TREE' ?
                                                            <div className="h-100 overflow-y-auto">
                                                                <ReactJson src={activeCollectionTab.documents} />
                                                            </div> :
                                                            this.state.documentsViewType === 'TABLE' ?
                                                                <div className="debug-table-wrapper h-100">
                                                                    {this.getTableData().length ?
                                                                        <div className="ag-theme-alpine">
                                                                            <AgGridReact
                                                                                columnDefs={this.getAllColumnsForAgGrid()}
                                                                                rowData={this.getTableData()}
                                                                                animateRows={true}
                                                                                overlayNoRowsTemplate="There is no data to display."
                                                                            />
                                                                        </div> :
                                                                        <div className="inner-message-wrapper border border-radius-4 h-100">
                                                                            <div className="inner-message-content">
                                                                                <i className="fad fa-file-exclamation"></i>
                                                                                <h6>There is no data to display.</h6>
                                                                            </div>
                                                                        </div>
                                                                    }
                                                                </div> :
                                                                <div className="json-input-box h-100">
                                                                    <JSONInput viewOnly={true} placeholder={activeCollectionTab.documents} />
                                                                </div>
                                                }
                                            </div>

                                            <div className="d-flex align-items-center pagination-box">
                                                <div className="flex-50">
                                                    <h6>Showing {activeCollectionTab.count ? (activeCollectionTab.activePageNumber === 1 ? 1 : (((activeCollectionTab.activePageNumber - 1) * activeCollectionTab.pageSize) + 1)) + ' to ' + getLastPageNumber(activeCollectionTab.activePageNumber, activeCollectionTab.pageSize, activeCollectionTab.count) + ' of ' : null}
                                                        <strong className="text-theme"> {activeCollectionTab.count || 0} rows</strong>
                                                        <span className="mr-l-10 mr-r-10">|</span>
                                                        Records Per Page:
                                                        <button className="btn btn-light dropdown-toggle" type="button" data-toggle="dropdown">{activeCollectionTab.pageSize}</button>
                                                        <ul className="dropdown-menu">
                                                            {PAGE_SIZE_OPTIONS.map((size, index) => (
                                                                <li key={index} onClick={() => size !== activeCollectionTab.pageSize && this.pageChangeHandler(size, activeCollectionTab.activePageNumber)}>{size}</li>
                                                            ))}
                                                        </ul>
                                                    </h6>
                                                </div>
                                                <div className="flex-50">
                                                    <ul className="list-style-none d-flex justify-content-end">
                                                        <li className="page-item" onClick={() => activeCollectionTab.activePageNumber > 1 && this.pageChangeHandler(activeCollectionTab.pageSize, activeCollectionTab.activePageNumber - 1)}>
                                                            <a className={`page-link ${activeCollectionTab.activePageNumber > 1 ? "bg-theme text-white" : "text-light-gray"}`}>
                                                                <i className="far fa-angle-double-left"></i>
                                                            </a>
                                                        </li>
                                                        {Boolean(activeCollectionTab.count) && getPageNumbers(Math.ceil(activeCollectionTab.count / activeCollectionTab.pageSize), activeCollectionTab.activePageNumber).map((pageNumber, index) => (
                                                            <li key={index} className="page-item" onClick={() => pageNumber !== activeCollectionTab.activePageNumber && this.pageChangeHandler(activeCollectionTab.pageSize, pageNumber)}>
                                                                <a className={`page-link ${Number(activeCollectionTab.activePageNumber) === pageNumber ? "bg-theme text-white" : "text-gray"}`}>{pageNumber}</a>
                                                            </li>
                                                        ))}
                                                        <li className="page-item" onClick={() => (activeCollectionTab.activePageNumber < Math.ceil(activeCollectionTab.count / activeCollectionTab.pageSize)) && this.pageChangeHandler(activeCollectionTab.pageSize, activeCollectionTab.activePageNumber + 1)}>
                                                            <a className={`page-link ${(activeCollectionTab.activePageNumber < Math.ceil(activeCollectionTab.count / activeCollectionTab.pageSize)) ? "bg-theme text-white" : "text-light-gray"}`}><i className="far fa-angle-double-right"></i></a>
                                                        </li>
                                                        <h6>Go to Page :
                                                            <input className="btn btn-light" type="text" value={activeCollectionTab.count ? activeCollectionTab.goToPageValue !== undefined ? activeCollectionTab.goToPageValue : 1 : 0} onChange={({ currentTarget }) => this.gotoPageChangeHandler(currentTarget.value, totalPages)} disabled={!activeCollectionTab.count} /> of <strong className="text-theme">{totalPages}</strong>
                                                        </h6>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="form-info-wrapper form-info-wrapper-left pb-0">
                                    <div className="action-tabs action-tabs-image">
                                        <ul className="nav nav-tabs list-style-none d-flex">
                                            <li className={`nav-item flex-20 ${this.state.activeCollectionTab === "Mongo" ? 'active' : null}`} onClick={() => this.setState({ activeCollectionTab: "Mongo" })}>
                                                <a className="nav-link text-center" data-tooltip data-tooltip-text="Mongo DB" data-tooltip-place="bottom"><img src="https://content.iot83.com/m83/dataConnectors/mongodb.png" /></a>
                                            </li>
                                            <li className={`nav-item flex-20 ${this.state.activeCollectionTab === "TS" ? 'active' : null}`} onClick={() => this.setState({ activeCollectionTab: "TS" })}>
                                                <a className="nav-link text-center" data-tooltip data-tooltip-text="Timescale DB" data-tooltip-place="bottom"><img src="https://content.iot83.com/m83/dataConnectors/timescaledb.png" /></a>
                                            </li>
                                            <li className={`nav-item flex-20 disabled`}>
                                                <a className="nav-link text-center" data-tooltip data-tooltip-text="My SQL" data-tooltip-place="bottom"><img src="https://content.iot83.com/m83/dataConnectors/mysql.png" /></a>
                                            </li>
                                            <li className={`nav-item flex-20 disabled`}>
                                                <a className="nav-link text-center" data-tooltip data-tooltip-text="Cassandra" data-tooltip-place="bottom"><img src="https://content.iot83.com/m83/dataConnectors/cassandra.png" /></a>
                                            </li>
                                            <li className={`nav-item flex-20 disabled`}>
                                                <a className="nav-link text-center" data-tooltip data-tooltip-text="Elastic" data-tooltip-place="bottom"><img src="https://content.iot83.com/m83/dataConnectors/elastic.png" /></a>
                                            </li>
                                        </ul>
                                    </div>

                                    <div className="form-info-body form-info-body-adjust">
                                        {this.state.activeCollectionTab === "Mongo" ? mongoFilterList.length ?
                                            <ul className="list-style-none collection-list">
                                                {mongoFilterList.map((collection, index) => {
                                                    return <li key={index}>
                                                        <div className="collection-list-box">
                                                            <span className="collapsed" data-toggle="collapse" data-target={"#collectionList" + index}><i className="fas fa-caret-up"></i></span>
                                                            <h6 className="cursor-pointer" id={collection.name} onClick={() => this.openTabsHandler(collection, 'MongoDB')}>{collection.name}</h6>
                                                            {/*<p className="f-11 text-content mb-0 mt-2">Documents :<strong className="ml-2 text-primary">{collection.count}</strong></p>*/}
                                                            <div className="button-group">
                                                                <button className="btn-transparent btn-transparent-green" data-tooltip data-tooltip-text="Copy" data-tooltip-place="bottom" onClick={() => this.copyToClipboard(collection.name)}>
                                                                    <i className="far fa-copy"></i>
                                                                </button>
                                                                <button className="btn-transparent btn-transparent-red" data-tooltip data-tooltip-text="Delete" data-tooltip-place="bottom" onClick={() => this.deleteDataCollection(collection.name, collection.type)}>
                                                                    <i className="far fa-trash-alt"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <div className="collapse" id={"collectionList" + index}>
                                                            <ul className="list-style-none collection-sub-list">
                                                                <li>
                                                                    <div className="collection-list-box">
                                                                        <span className="collapsed" data-toggle="collapse" data-target={"#collectionSubList" + index}><i className="fas fa-caret-up"></i></span>
                                                                        <h6 className="f-12 mb-2"><i className="fad fa-folder text-orange mr-2"></i>Indexes</h6>
                                                                        <div className="collapse" id={"collectionSubList" + index}>
                                                                            {collection.indexes && collection.indexes.length ? collection.indexes.map((index, i) =>
                                                                                <p key={i}><i className="fad fa-file mr-2"></i>{index.indexName}</p>
                                                                            ) : <p ><i className="fad fa-file mr-2"></i>No Indexes Found.</p>}
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                })}
                                            </ul>
                                            :
                                            <div className="inner-message-wrapper h-100">
                                                <div className="inner-message-content">
                                                    <i className="fad fa-file-exclamation"></i>
                                                    <h6>No Mongo collections found !</h6>
                                                </div>
                                            </div>
                                            :
                                            timeScaleFilterList.length ?
                                                <ul className="list-style-none collection-list">
                                                    {timeScaleFilterList.map((table, index) => (
                                                        <li key={index}>
                                                            <div className="collection-list-box">
                                                                <span className="collapsed" data-toggle="collapse" data-target={"#timeScaleDataCollectionList" + index}><i className="fas fa-caret-up"></i></span>
                                                                <h6 className="cursor-pointer" onClick={() => this.openTabsHandler(table, 'TimescaleDB')}>{table.name}</h6>
                                                                <div className="button-group">
                                                                    <button className="btn-transparent btn-transparent-green" data-tooltip data-tooltip-text="Copy" data-tooltip-place="bottom" onClick={() => this.copyToClipboard(table.name)}>
                                                                        <i className="far fa-copy"></i>
                                                                    </button>
                                                                    <button className="btn-transparent btn-transparent-red" disabled={(table.name.startsWith("etl_") || table.name.startsWith("system_"))} data-tooltip data-tooltip-text="Delete" data-tooltip-place="bottom" onClick={() => this.deleteDataCollection(table.name, "timescaleDB")}>
                                                                        <i className="far fa-trash-alt"></i>
                                                                        {/*data-tooltip data-tooltip-text="Cannot Delete System Generated Tables" data-tooltip-place="bottom"*/}
                                                                    </button>
                                                                </div>
                                                            </div>
                                                            <div className="collapse" id={"timeScaleDataCollectionList" + index}>
                                                                <ul className="list-style-none collection-sub-list">
                                                                    <li>
                                                                        <div className="collection-list-box">
                                                                            <span className="collapsed" data-toggle="collapse" data-target={"#collectionSubList" + index}><i className="fas fa-caret-up"></i></span>
                                                                            <h6 className="f-12 mb-2"><i className="fad fa-folder text-orange mr-2"></i>Indexes</h6>
                                                                            <div className="collapse" id={"collectionSubList" + index}>
                                                                                {table.indexes && table.indexes.length ? table.indexes.map((index, i) =>
                                                                                    <p key={i}><i className="fad fa-file mr-2"></i>{index.indexName}</p>
                                                                                ) : <p ><i className="fad fa-file mr-2"></i>No Indexes Found.</p>}
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </li>
                                                    ))}
                                                </ul>
                                                :
                                                <div className="inner-message-wrapper h-100">
                                                    <div className="inner-message-content">
                                                        <i className="fad fa-file-exclamation"></i>
                                                        <h6>No TimeScale tables found !</h6>
                                                    </div>
                                                </div>
                                        }
                                    </div>
                                </div>
                            </div>
                            :
                            <AddNewButton
                                text1="No Data Collection(s) available"
                                text2="You haven't created any data collections yet. Please create a data collection first."
                                imageIcon="addDataCollection.png"
                                addButtonEnable={true}
                                createItemOnAddButtonClick={() => this.addCollectionOrDocument(false)}
                            />
                        }
                    </React.Fragment>
                }

                {/* add collection modal */}
                <SlidingPane
                    className=''
                    overlayClassName='sliding-form'
                    closeIcon={<div></div>}
                    isOpen={this.state.isAddDataCollection || false}
                    from='right'
                    width='500px'
                >
                    <div className="modal-content">
                        <div className="modal-header">
                            <h6 className="modal-title">{this.state.isAlterTSTable ? "Alter" : "Add New"} {this.state.isForCreatingDocument ? "Document" : this.state.selectedCollectionType === "TimescaleDB" ? "Table" : "Collection"}
                                <button type="button" className="close" onClick={this.onCloseModal} ><i className="fas fa-angle-right"></i></button>
                            </h6>
                        </div>

                        <form onSubmit={this.addDataCollection}>
                            <div className="modal-body">
                                {this.state.isSavingCollection || this.state.isAddingColumn ?
                                    <div className="modal-loader">
                                        <i className="fad fa-sync-alt fa-spin"></i>
                                    </div> :
                                    <React.Fragment>
                                        {!(this.state.isForCreatingDocument || this.state.isAlterTSTable) &&
                                            <div className="alert alert-info note-text">
                                                <p>{this.state.selectedCollectionType === "TimescaleDB" ? "Table" : "Collection"} name should not begin with default keywords (System and ETL).</p>
                                            </div>
                                        }
                                        {this.state.errorMessageCode === 1 && <p className="modal-body-error">Documents should be an Array containing atleast one item.</p>}

                                        {!this.state.isAlterTSTable &&
                                            <div className="form-group">
                                                <label className="form-group-label">Database : <i className="fas fa-asterisk form-group-required"></i></label>
                                                <select className="form-control" value={this.state.selectedCollectionType} onChange={this.collectionTypeChangeHandler} disabled={this.state.isForCreatingDocument || this.state.isAlterTSTable}>
                                                    <option value="MongoDB">MongoDB</option>
                                                    <option value="TimescaleDB">TimescaleDB</option>
                                                </select>
                                            </div>
                                        }
                                        {this.state.selectedCollectionType === "MongoDB" ?
                                            <div className="form-group">
                                                <label className="form-group-label">Name : <i className="fas fa-asterisk form-group-required"></i></label>
                                                <input type="text" id="collectionName" className="form-control" readOnly={this.state.isForCreatingDocument} value={this.state.addCollectionPayload.collectionName} onChange={this.handleInput} required autoFocus />
                                            </div> :
                                            <div className="form-group">
                                                <label className="form-group-label">Table Name :<i className="fas fa-asterisk form-group-required"></i></label>
                                                <input type="text" id="tableName" className="form-control" readOnly={this.state.isForCreatingDocument || this.state.isAlterTSTable} value={this.state.addCollectionPayload.tableName} onChange={this.handleInput} required autoFocus />
                                            </div>
                                        }

                                        {this.state.isAlterTSTable ?
                                            <div className="form-group">
                                                <label className="form-group-label">Columns :<i className="fas fa-asterisk form-group-required"></i>
                                                    <button type="button" className="btn btn-link" disabled={this.state.isFetchingTimeScaleColumns} onClick={this.addColumnInTSTable}><i className="far fa-plus"></i>Add New</button>
                                                </label>
                                                {this.state.alterTablePayload.addColumnsNameDataType.map((column, index) => {
                                                    return (
                                                        <div className="form-group d-flex mb-2" key={index}>
                                                            <div className="flex-45 pd-r-5">
                                                                <input type="text" id="name" className="form-control" placeholder="Name" required autoFocus value={this.state.alterTablePayload.addColumnsNameDataType[index].name} onChange={(currentTarget) => this.columnHandlerInTSTable(currentTarget, index)} />
                                                            </div>
                                                            <div className="flex-45 pd-l-5">
                                                                <select id="dataType" className="form-control" value={this.state.alterTablePayload.addColumnsNameDataType[index].dataType} required onChange={(currentTarget) => this.columnHandlerInTSTable(currentTarget, index)}>
                                                                    <option value="Select Data Type">Select Data Type</option>
                                                                    {DEFAULT_TS_DATA_TYPE.map((dataType, i) => (
                                                                        <option value={dataType} key={i}>{dataType === "DOUBLE_PRECISION" ? "DOUBLE PRECISION" : dataType}</option>
                                                                    ))}
                                                                </select>
                                                            </div>
                                                            <div className="flex-10">
                                                                <button type="button" className="btn-transparent btn-transparent-red mr-t-5 ml-auto" onClick={() => this.removeColumnChild(index)}><i className="far fa-trash-alt"></i></button>
                                                            </div>

                                                            {/*<div className="form-group">
                                                                <label className="form-group-label">Constraint : <i className="fas fa-asterisk form-group-required"></i></label>
                                                                <select id="constraint" className="form-control" value={this.state.alterTablePayload.addColumnsNameDataType[index].constraint} onChange={(currentTarget) => this.columnHandlerInTSTable(currentTarget , index)}>
                                                                    <option value="Select Constraint">Select Constraint </option>
                                                                    {DEFAULT_TS_CONSTRAINT.map((constraint ,index) => (
                                                                        <option value={constraint} key={index}>{constraint}</option>
                                                                    ) )}
                                                                </select>
                                                                {this.state.alterTablePayload.addColumnsNameDataType[index].constraint === DEFAULT_TS_CONSTRAINT[3] &&
                                                                    <React.Fragment className = "d-flex">
                                                                        <div className="form-group mt-3 flex-50">
                                                                            <label className="form-group-label">Select Table : <i className="fas fa-asterisk form-group-required"></i></label>
                                                                            <select id="TableNameForForeignKey" className="form-control" value={this.state.foreignKeyPayload.TableNameForForeignKey} onChange={(currentTarget) => this.foreignKeyHandler(currentTarget,index)}>
                                                                                <option value="Select Table">Select Table</option>
                                                                                {this.state.timeScaleDataCollectionsData.map((tableName,index) => (
                                                                                    <option value={tableName} key={index}>{tableName}</option>
                                                                                ))}
                                                                            </select>
                                                                        </div>
                                                                        <div className="form-group mt-3 flex-50">
                                                                            <label className="form-group-label">Select Column : <i className="fas fa-asterisk form-group-required"></i></label>
                                                                            <select className="form-control" disabled={this.state.fetchingColumns}>
                                                                                    <option value="Select Table">Select Column</option>
                                                                                    { this.state.columnNameforForeignTable && Object.keys(this.state.columnNameforForeignTable).map((column,index) => (
                                                                                         <option value={column} key={index}>{column}</option>
                                                                                    ))
                                                                                }
                                                                            </select>
                                                                        </div>
                                                                   </React.Fragment>
                                                                }
                                                            </div>*/}
                                                        </div>
                                                    )
                                                })}
                                                {this.state.isFetchingTimeScaleColumns && this.state.isFetchingTimeScaleColumns ?
                                                    <div className="inner-loader-wrapper" style={{ height: 200 }}>
                                                        <div className="inner-loader-content">
                                                            <i className="fad fa-sync-alt fa-spin"></i>
                                                        </div>
                                                    </div>
                                                    :
                                                    <div className="content-table">
                                                        <table className="table table-bordered">
                                                            <thead>
                                                                <tr>
                                                                    <th width="45%">Column Name</th>
                                                                    <th width="40%">Data Type</th>
                                                                    <th width="15%"></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                {Object.entries(this.state.timeScaleColumnsDescription).map(([key, value], index) => (
                                                                    <tr key={index}>
                                                                        <td>{key}</td>
                                                                        <td>{value}</td>
                                                                        <td><button type="button" className="btn-transparent btn-transparent-red" data-tooltip data-tooltip-text="Delete" data-tooltip-place="bottom" onClick={() => this.deleteTSColumn(key)}><i className="far fa-trash-alt"></i></button></td>
                                                                    </tr>
                                                                ))
                                                                }
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                }
                                            </div>
                                            :
                                            <div className="form-group">
                                                <div className="json-input-box">
                                                    <JSONInput
                                                        id="addCollectionJsonInput"
                                                        theme="light_mitsuketa_tribute"
                                                        placeholder={this.state.addCollectionPayload.documents}
                                                        colors={{
                                                            default: "#212F3D",
                                                            number: "#2ECC71",
                                                            string: "#ab210f",
                                                            keys: "#550fab",
                                                        }}
                                                        onChange={(event) => this.handleInput(event, true)}
                                                    />
                                                </div>
                                            </div>
                                        }
                                    </React.Fragment>
                                }
                            </div>

                            <div className="modal-footer justify-content-start">
                                <button className="btn btn-primary" disabled={this.state.isSavingCollection}>Save</button>
                                <button type="button" className="btn btn-dark" disabled={this.state.isSavingCollection} onClick={this.onCloseModal} >Cancel</button>
                            </div>
                        </form>
                    </div>
                </SlidingPane>
                {/* end add collection  modal */}

                {/* execute query modal */}
                {this.state.queryModal &&
                    <SlidingPane
                        className=''
                        overlayClassName='sliding-form'
                        closeIcon={<div></div>}
                        isOpen={this.state.queryModal || true}
                        from='right'
                        width='50%'
                    >
                        <div className="modal-content">
                            <div className="modal-header">
                                <h6 className="modal-title">Query Window
                                    <button type="button" className="close" onClick={() => { this.setState({ queryModal: false, isExecutingQuery: false, queryError: null }) }}><i className="fas fa-angle-right"></i></button>
                                </h6>
                            </div>
                            <div className="modal-body">
                                <div className="form-group">
                                    <div className="form-group">
                                        <label className="form-group-label">Database : <i className="fas fa-asterisk form-group-required"></i></label>
                                        <select id="database" value={this.state.selectedDatabase} className="form-control" disabled onChange={this.databaseQueryHandler}>
                                            <option value="MongoDB">MongoDB</option>
                                            <option value="TimescaleDB">TimescaleDB</option>
                                        </select>
                                    </div>

                                    <div className="ace-editor" style={{ height: 100 }}>
                                        <AceEditor
                                            placeholder="Type code here..."
                                            mode="javascript"
                                            theme="monokai"
                                            name="ace_editor"
                                            onChange={(code) => this.setState({ query: code })}
                                            value={this.state.query}
                                            fontSize={14}
                                            showPrintMargin={true}
                                            showGutter={true}
                                            highlightActiveLine={true}
                                            setOptions={{
                                                enableBasicAutocompletion: true,
                                                enableLiveAutocompletion: true,
                                                enableSnippets: true,
                                                showLineNumbers: true,
                                                tabSize: 2,
                                                hScrollBarAlwaysVisible: true,
                                            }}
                                            height="100%"
                                            width="100%"
                                        />
                                    </div>
                                    <div className="text-right border p-2">
                                        <button type='button' className="btn btn-success" onClick={this.state.selectedDatabase === "MongoDB" ? this.getQuery : this.getTimeScaleQuery} disabled={this.state.isExecutingQuery}>Execute</button>
                                    </div>
                                </div>
                                <div className="form-group">
                                    <label className="form-group-label">Output :</label>
                                    {this.state.isExecutingQuery ?
                                        <div className="inner-loader-wrapper" style={{ height: 100 }}>
                                            <div className="inner-loader-content">
                                                <i className="fad fa-sync-alt fa-spin"></i>
                                            </div>
                                        </div> :
                                        this.state.queryError ?
                                            <p className="modal-body-error">{this.state.queryError}</p> :
                                            <div className="debug-table-wrapper" style={{ height: 400 }}>
                                                {this.state.queryResponse ?
                                                    this.state.queryResponse.length ?
                                                        <div className="ag-theme-alpine">
                                                            <AgGridReact
                                                                columnDefs={this.getQueryResponseColumn()}
                                                                rowData={this.state.queryResponse}
                                                                animateRows={true}
                                                                overlayNoRowsTemplate="There is no data to display."
                                                            />
                                                        </div> :
                                                        <div className="inner-message-wrapper border border-radius-4 h-100">
                                                            <div className="inner-message-content">
                                                                <i className="fad fa-file-exclamation"></i>
                                                                <h6>There is no data to display.</h6>
                                                            </div>
                                                        </div>
                                                    :
                                                    <p className="modal-body-error">An error occurred while executing the query.</p>
                                                }

                                            </div>
                                    }
                                </div>
                            </div>
                            <div className="modal-footer justify-content-start">
                                <button type="button" className="btn btn-primary" onClick={() => { this.setState({ queryModal: false, isExecutingQuery: false, queryError: false }) }}>OK</button>
                            </div>
                        </div>
                    </SlidingPane>
                }
                {/* end execute query modal */}

                {this.state.isOpen &&
                    <NotificationModal
                        type={this.state.type}
                        message2={this.state.message2}
                        onCloseHandler={this.onCloseHandler}
                    />
                }

            </React.Fragment >
        );
    }
}


DataCollections.propTypes = {
    dispatch: PropTypes.func.isRequired
};

let allSelectors = {}

Object.entries(SELECTORS).map(([key, value]) => {
    allSelectors[key] = value()
})

const mapStateToProps = createStructuredSelector(allSelectors)

function mapDispatchToProps(dispatch) {
    let allActions = { dispatch }
    Object.entries(ACTIONS).map(([key, value]) => {
        allActions[key] = (...args) => dispatch(value(...args))
    })
    return allActions
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "dataCollections", reducer });
const withSaga = injectSaga({ key: "dataCollections", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(DataCollections);
