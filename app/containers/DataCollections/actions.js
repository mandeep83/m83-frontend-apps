/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import * as CONSTANTS from "./constants";

let allCases = Object.values(CONSTANTS).filter(constant => typeof (constant) == "object")
export const allActions = {
  resetToInitialState : (prop)=>{
    return {
      type: CONSTANTS.RESET_TO_INITIAL_STATE,
      prop
    };
  }
}
allCases.map(constant => {
  allActions[constant.actionName] = (...args) => {
    let actionObj = { type: constant.action }
    constant.actionArguments && constant.actionArguments.map((arg, index) => {
      actionObj[arg] = args[index]
    })
    return actionObj
  }
})


// export function resetToInitialState(prop) {
//   return {
//     type: CONSTANTS.RESET_TO_INITIAL_STATE,
//     prop
//   };
// }

// export function getAllDataCollections() {
//   return {
//     type: CONSTANTS.GET_DATA_COLLECTIONS
//   };
// }

// export function fetchCollectionDocuments(id, max, offset) {
//   return {
//     type: CONSTANTS.FETCH_SCHEMA_DETAILS,
//     id,
//     max,
//     offset
//   };
// }

// export function clearDataCollection(name) {
//   return {
//     type: CONSTANTS.CLEAR_DATA_COLLECTION,
//     name,
//   };
// }

// export function addDataCollection(payload) {
//   return {
//     type: CONSTANTS.ADD_DATA_COLLECTION,
//     payload
//   }
// }

// export function deleteDataCollection(name) {
//   return {
//     type: CONSTANTS.DELETE_DATA_COLLECTION,
//     name,
//   }
// }

// export function getQuery(payload) {
//   return {
//     type: CONSTANTS.GET_QUERY,
//     payload
//   }
// }