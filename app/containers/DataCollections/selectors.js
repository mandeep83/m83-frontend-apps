/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/


import { createSelector } from "reselect";
import { initialState } from "./reducer";
import * as CONSTANTS from "./constants"

const selectDataExpiryDomain = state => state.get("dataCollections", initialState);

let allCases = Object.values(CONSTANTS).filter(constant => typeof (constant) == "object");
export const allSelectors = {}
allCases.map(constant => {
  allSelectors[constant.successKey] = () => createSelector(selectDataExpiryDomain, subState => subState[constant.successKey]);
  allSelectors[constant.failureKey] = () => createSelector(selectDataExpiryDomain, subState => subState[constant.failureKey]);
})



// import { createSelector } from "reselect";
// import { initialState } from "./reducer";

// /**
//  * Direct selector to the dataCollections state domain
//  */

// const selectDataExpiryDomain = state => state.get("dataCollections", initialState);

// /**
//  * Other specific selectors
//  */

// /**
//  * Default selector used by DataExpiry
//  */
// export const getDataCollections = () => createSelector(selectDataExpiryDomain, substate => substate.dataCollections);
// export const getDataCollectionsFailure = () => createSelector(selectDataExpiryDomain, substate => substate.getDataCollectionsFailure);

// export const getSchema = () => createSelector(selectDataExpiryDomain, substate => substate.getSchema);
// export const getSchemaFailure = () => createSelector(selectDataExpiryDomain, substate => substate.getSchemaFailure);

// export const clearDataCollectionSuccess = () => createSelector(selectDataExpiryDomain, substate => substate.clearDataCollectionSuccess);
// export const clearDataCollectionFailure = () => createSelector(selectDataExpiryDomain, substate => substate.clearDataCollectionFailure);

// export const addDataCollectionSuccess = () => createSelector(selectDataExpiryDomain, substate => substate.addDataCollectionSuccess);
// export const addDataCollectionFailure = () => createSelector(selectDataExpiryDomain, substate => substate.addDataCollectionFailure);

// export const deleteDataCollectionSuccess = () => createSelector(selectDataExpiryDomain, substate => substate.deleteDataCollectionSuccess);
// export const deleteDataCollectionFailure = () => createSelector(selectDataExpiryDomain, substate => substate.deleteDataCollectionFailure);

// export const getQuerySuccess = () => createSelector(selectDataExpiryDomain, substate => substate.getQuerySuccess);
// export const getQueryFailure = () => createSelector(selectDataExpiryDomain, substate => substate.getQueryFailure);

