/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { fromJS } from "immutable";
import * as CONSTANTS from "./constants";

export const initialState = fromJS({});

let allCases = Object.values(CONSTANTS).filter(constant => typeof (constant) == "object")

let getValueObj = (action, index) => {
  let isSuccessCase = action.type.includes("SUCCESS")
  let key = isSuccessCase ? allCases[index].successKey : allCases[index].failureKey
  return {
    [key]: action
  }
}

function dataCollectionsReducer(state = initialState, action) {
  if (action.type.includes("RESET_TO_INITIAL_STATE"))
    return Object.assign({}, state, {
      [action.prop]: undefined,
    })
  for (let i = 0; i < allCases.length; i++) {
    if (action.type === allCases[i].success || action.type === allCases[i].failure) {
      return Object.assign({}, state, getValueObj(action, i));
    }
  }
  return state
}

export default dataCollectionsReducer;




// import { fromJS } from "immutable";
// import * as CONSTANTS from "./constants";

// export const initialState = fromJS({});

// function dataCollectionsReducer(state = initialState, action) {
//   switch (action.type) {
//     case CONSTANTS.RESET_TO_INITIAL_STATE:
//       return Object.assign({}, state, {
//         [action.prop]: null,
//       })
//     case CONSTANTS.GET_DATA_COLLECTIONS_SUCCESS:
//       return Object.assign({}, state, {
//         dataCollections: action.response
//       });
//     case CONSTANTS.GET_DATA_COLLECTIONS_FAILURE:
//       return Object.assign({}, state, {
//         getDataCollectionsFailure: action.error
//       });
//     case CONSTANTS.FETCH_SCHEMA_DETAILS_SUCCESS:
//       return Object.assign({}, state, {
//         getSchema: action.response
//       });
//     case CONSTANTS.FETCH_SCHEMA_DETAILS_FAILURE:
//       return Object.assign({}, state, {
//         getSchemaFailure: action
//       });
//     case CONSTANTS.CLEAR_DATA_COLLECTION_SUCCESS:
//       return Object.assign({}, state, {
//         clearDataCollectionSuccess: action.response
//       });
//     case CONSTANTS.CLEAR_DATA_COLLECTION_FAILURE:
//       return Object.assign({}, state, {
//         clearDataCollectionFailure: action.error
//       });
//     case CONSTANTS.ADD_DATA_COLLECTION_SUCCESS:
//       return Object.assign({}, state, {
//         addDataCollectionSuccess: action.response
//       });
//     case CONSTANTS.ADD_DATA_COLLECTION_FAILURE:
//       return Object.assign({}, state, {
//         addDataCollectionFailure: action.error
//       });
//     case CONSTANTS.DELETE_DATA_COLLECTION_SUCCESS:
//       return Object.assign({}, state, {
//         deleteDataCollectionSuccess: action.response
//       });
//     case CONSTANTS.DELETE_DATA_COLLECTION_FAILURE:
//       return Object.assign({}, state, {
//         deleteDataCollectionFailure: action.error
//       });
//     case CONSTANTS.GET_QUERY_SUCCESS:
//       return Object.assign({}, state, {
//         getQuerySuccess: action.response
//       });
//     case CONSTANTS.GET_QUERY_FAILURE:
//       return Object.assign({}, state, {
//         getQueryFailure: action.error
//       });
//     default:
//       return state;
//   }
// }

// export default dataCollectionsReducer;
