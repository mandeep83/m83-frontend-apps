/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

export const RESET_TO_INITIAL_STATE = "app/DataCollections/RESET_TO_INITIAL_STATE";

export const GET_MONGO_DATA_COLLECTIONS = {
    action: "app/DataCollections/GET_MONGO_DATA_COLLECTIONS",
	success: "app/DataCollections/GET_MONGO_DATA_COLLECTIONS_SUCCESS",
	failure: "app/DataCollections/GET_MONGO_DATA_COLLECTIONS_FAILURE",
	urlKey: "getDataCollection",
	successKey: "mongodataCollectionsSuccess",
	failureKey: "mongodataCollectionsFailure",
	actionName: "getAllMongoDataCollections",
}

export const GET_TIMESCALE_DATA_COLLECTIONS = {
	action: "app/DataCollections/GET_TIMESCALE_DATA_COLLECTIONS",
	success: "app/DataCollections/GET_TIMESCALE_DATA_COLLECTIONS_SUCCESS",
	failure: "app/DataCollections/GET_TIMESCALE_DATA_COLLECTIONS_FAILURE",
	urlKey: "timeScaleDataCollection",
	successKey: "timeScaleDataCollectionsSuccess",
	failureKey: "timeScaleDataCollectionsFailure",
	actionName: "getAllTimeScaleDataCollections",
}

export const GET_TIMESCALE_DATA_BY_TABLENAME = {
	action: "app/DataCollections/GET_TIMESCALE_DATA_BY_TABLENAME",
	success: "app/DataCollections/GET_TIMESCALE_DATA_BY_TABLENAME_SUCCESS",
	failure: "app/DataCollections/GET_TIMESCALE_DATA_BY_TABLENAME_FAILURE",
	urlKey: "timeScaleDataByTableName",
	successKey: "timeScaleDataByTableNameSuccess",
	failureKey: "timeScaleDataByTableNameFailure",
	actionName: "getTimeScaleDataByTableName",
	actionArguments: ["table","max","offset"]
}

export const FETCH_SCHEMA_DETAILS = {
    action: "app/DataCollections/FETCH_SCHEMA_DETAILS",
	success: "app/DataCollections/FETCH_SCHEMA_DETAILS_SUCCESS",
	failure: "app/DataCollections/FETCH_SCHEMA_DETAILS_FAILURE",
	urlKey: "getDataCollectionsById",
	successKey: "getSchema",
	failureKey: "getSchemaFailure",
    actionName: "fetchCollectionDocuments",
    actionArguments: ["id","max","offset"],
}

export const CLEAR_DATA_COLLECTION = {
    action: "app/DataCollections/CLEAR_DATA_COLLECTION",
	success: "app/DataCollections/CLEAR_DATA_COLLECTION_SUCCESS",
	failure: "app/DataCollections/CLEAR_DATA_COLLECTION_FAILURE",
	urlKey: "clearDataCollection",
	successKey: "clearDataCollectionSuccess",
	failureKey: "clearDataCollectionFailure",
    actionName: "clearDataCollection",
    actionArguments: ["name", "collectionType"],
}

export const CLEAR_TS_TABLE_DATA = {
	action: "app/DataCollections/CLEAR_TS_TABLE_DATA",
	success: "app/DataCollections/CLEAR_TS_TABLE_DATA_SUCCESS",
	failure: "app/DataCollections/CLEAR_TS_TABLE_DATA_FAILURE",
	urlKey: "clearTSTableData",
	successKey: "clearTSTableDataSuccess",
	failureKey: "clearTSTableDataFailure",
    actionName: "clearTSTableData",
    actionArguments: ["payload"],
}

export const ADD_DATA_COLLECTION = {
    action: "app/DataCollections/ADD_DATA_COLLECTION",
	success: "app/DataCollections/ADD_DATA_COLLECTION_SUCCESS",
	failure: "app/DataCollections/ADD_DATA_COLLECTION_FAILURE",
	urlKey: "addDataCollection",
	successKey: "addDataCollectionSuccess",
	failureKey: "addDataCollectionFailure",
    actionName: "addDataCollection",
    actionArguments: ["payload"],
}

export const ADD_MONGO_DOCUMENT = {
    action: "app/DataCollections/ADD_MONGO_DOCUMENT",
	success: "app/DataCollections/ADD_MONGO_DOCUMENT_SUCCESS",
	failure: "app/DataCollections/ADD_MONGO_DOCUMENT_FAILURE",
	urlKey: "addMongoDocument",
	successKey: "addMongoDocumentSuccess",
	failureKey: "addMongoDocumentFailure",
    actionName: "addMongoDocument",
    actionArguments: ["payload"],
}

export const CREATE_TIME_SCALE_DB_TABLE = {
    action: "app/DataCollections/CREATE_TIME_SCALE_DB_TABLE_COLLECTION",
	success: "app/DataCollections/CREATE_TIME_SCALE_DB_TABLE_SUCCESS",
	failure: "app/DataCollections/CREATE_TIME_SCALE_DB_TABLE_FAILURE",
	urlKey: "createTimeScaleDBTable",
	successKey: "createTimeScaleDBTableSuccess",
	failureKey: "createTimeScaleDBTableFailure",
    actionName: "createTimeScaleDBTable",
    actionArguments: ["payload"],
}

export const DELETE_DATA_COLLECTION = {
    action: "app/DataCollections/DELETE_DATA_COLLECTION",
	success: "app/DataCollections/DELETE_DATA_COLLECTION_SUCCESS",
	failure: "app/DataCollections/DELETE_DATA_COLLECTION_FAILURE",
	urlKey: "deleteDataCollection",
	successKey: "deleteDataCollectionSuccess",
	failureKey: "deleteDataCollectionFailure",
    actionName: "deleteDataCollection",
    actionArguments: ["name", "collectionType"],
}

export const DELETE_TIMESCALE_DATA_COLLECTION = {
    action: "app/DataCollections/DELETE_TIMESCALE_DATA_COLLECTION",
	success: "app/DataCollections/DELETE_TIMESCALE_DATA_COLLECTION_SUCCESS",
	failure: "app/DataCollections/DELETE_TIMESCALE_DATA_COLLECTION_FAILURE",
	urlKey: "deleteTimescaleDataCollection",
	successKey: "deleteTimescaleDataCollectionSuccess",
	failureKey: "deleteTimescaleDataCollectionFailure",
    actionName: "deleteTimescaleDataCollection",
    actionArguments: ["name"],
}

export const GET_QUERY = {
    action: "app/DataCollections/GET_QUERY",
	success: "app/DataCollections/GET_QUERY_SUCCESS",
	failure: "app/DataCollections/GET_QUERY_FAILURE",
	urlKey: "getQueryDataCollection",
	successKey: "getQuerySuccess",
	failureKey: "getQueryFailure",
    actionName: "getQuery",
    actionArguments: ["payload"],
}

export const GET_TIMESCALE_QUERY = {
    action: "app/DataCollections/GET_TIMESCALE_QUERY",
	success: "app/DataCollections/GET_TIMESCALE_QUERY_SUCCESS",
	failure: "app/DataCollections/GET_TIMESCALE_QUERY_FAILURE",
	urlKey: "timeScaleQuery",
	successKey: "getTimeScaleQuerySuccess",
	failureKey: "getTimeScaleQueryFailure",
    actionName: "timeScaleQuery",
    actionArguments: ["payload"],
}



export const DEVELOPER_QUOTA = {
	action: 'app/DataCollections/GET_DEVELOPER_QUOTA',
	success: "app/DataCollections/GET_DEVELOPER_QUOTA_SUCCESS",
	failure: "app/DataCollections/GET_DEVELOPER_QUOTA_FAILURE",
	urlKey: "getDeveloperQuota",
	successKey: "developerQuotaSuccess",
	failureKey: "developerQuotaFailure",
	actionName: "getDeveloperQuota",
	actionArguments: ["payload"]
}

export const ADD_TIMESCALE_DOCUMENTS = {
    action: "app/DataCollections/ADD_TIMESCALE_DOCUMENTS",
	success: "app/DataCollections/ADD_TIMESCALE_DOCUMENTS_SUCCESS",
	failure: "app/DataCollections/ADD_TIMESCALE_DOCUMENTS_FAILURE",
	urlKey: "addTimescaleDocuments",
	successKey: "addTimescaleDocumentsSuccess",
	failureKey: "addTimescaleDocumentsFailure",
	actionName: "addTimescaleDocuments",
	actionArguments: ["payload"]
}

export const DESCRIBE_TIMESCALE_TABLE = {
	action: "app/DataCollections/DESCRIBE_TIMESCALE_TABLE",
	success: "app/DataCollections/DESCRIBE_TIMESCALE_TABLE_SUCCESS",
	failure: "app/DataCollections/DESCRIBE_TIMESCALE_TABLE_FAILURE",
	urlKey: "describeTimeScaleTable",
	successKey: "describeTimeScaleTableSuccess",
	failureKey: "describeTimeScaleTableFailure",
	actionName: "describeTimeScaleTable",
	actionArguments: ["payload"]
}

export const ALTER_TIMESCALE_TABLE = {
	action: "app/DataCollections/ALTER_TIMESCALE_TABLE",
	success: "app/DataCollections/ALTER_TIMESCALE_TABLE_SUCCESS",
	failure: "app/DataCollections/ALTER_TIMESCALE_TABLE_FAILURE",
	urlKey: "alterTimeScaleTable",
	successKey: "alterTimeScaleTableSuccess",
	failureKey: "alterTimeScaleTableFailure",
	actionName: "alterTimeScaleTable",
	actionArguments: ["payload"]
}
// export const GET_DATA_COLLECTIONS = "app/DataCollections/GET_DATA_COLLECTIONS";
// export const GET_DATA_COLLECTIONS_SUCCESS = "app/DataCollections/GET_DATA_COLLECTIONS_SUCCESS";
// export const GET_DATA_COLLECTIONS_FAILURE = "app/DataCollections/GET_DATA_COLLECTIONS_FAILURE";

// export const FETCH_SCHEMA_DETAILS = 'app/DataCollections/FETCH_SCHEMA_DETAILS';
// export const FETCH_SCHEMA_DETAILS_SUCCESS = 'app/DataCollections/FETCH_SCHEMA_DETAILS_SUCCESS';
// export const FETCH_SCHEMA_DETAILS_FAILURE = 'app/DataCollections/FETCH_SCHEMA_DETAILS_FAILURE';

// export const CLEAR_DATA_COLLECTION = 'app/DataCollections/CLEAR_DATA_COLLECTION';
// export const CLEAR_DATA_COLLECTION_SUCCESS = 'app/DataCollections/CLEAR_DATA_COLLECTION_SUCCESS';
// export const CLEAR_DATA_COLLECTION_FAILURE = 'app/DataCollections/CLEAR_DATA_COLLECTION_FAILURE';

// export const ADD_DATA_COLLECTION = 'app/DataCollections/ADD_DATA_COLLECTION';
// export const ADD_DATA_COLLECTION_SUCCESS = 'app/DataCollections/ADD_DATA_COLLECTION_SUCCESS';
// export const ADD_DATA_COLLECTION_FAILURE = 'app/DataCollections/ADD_DATA_COLLECTION_FAILURE';

// export const DELETE_DATA_COLLECTION = 'app/DataCollections/DELETE_DATA_COLLECTION';
// export const DELETE_DATA_COLLECTION_SUCCESS = 'app/DataCollections/DELETE_DATA_COLLECTION_SUCCESS';
// export const DELETE_DATA_COLLECTION_FAILURE = 'app/DataCollections/DELETE_DATA_COLLECTION_FAILURE';

// export const GET_QUERY = 'app/DataCollections/GET_QUERY';
// export const GET_QUERY_SUCCESS = 'app/DataCollections/GET_QUERY_SUCCESS';
// export const GET_QUERY_FAILURE = 'app/DataCollections/GET_QUERY_FAILURE';