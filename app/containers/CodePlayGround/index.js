/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { compose } from "redux";
import { Controlled as CodeMirror } from 'react-codemirror2';
import Iframe from '../../components/Iframe/Loadable'
import 'react-splitter-layout/lib/index.css';
import axios from 'axios';
import ReactTooltip from 'react-tooltip'
import ConfirmModel from "../../components/ConfirmModel";
import * as SELECTORS from "./selectors";
import * as Actions from "./actions";
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import reducer from "./reducer";
import saga from "./saga";
import { createStructuredSelector } from "reselect";
import Loader from '../../components/Loader/Loadable';
import { CDN_LIST } from "../../utils/constants";
import cloneDeep from "lodash/cloneDeep";
import startCase from "lodash/startCase";
import { debounce } from "lodash";
import Select from 'react-select';

/* eslint-disable react/prefer-stateless-function */
export class CodePlayGround extends React.Component {
    state = {
        html: this.props.html,
        css: this.props.css,
        js: this.props.js,
        settings: this.props.settings,
        widgetModal: false,
        cdnLink: "",
        fullViewEditor: "",
        view: {
            isHTMLView: true,
            isCSSView: true,
            isJSView: true,
            isOutputView: false,
        },
        listOfBucketImages: [],
        viewClass: 'flex-33',
        activeSettingsIndex: 0,
        editorSettings: [
            /*{
                displayName: "HTML",
                name: "html",
                settingsEnabled: false,
                preProcessors: []
            },*/
            {
                displayName: "CSS",
                name: "css",
                settingsEnabled: true,
                preProcessors: []
            },
            {
                displayName: "JavaScript",
                name: "js",
                settingsEnabled: true,
                preProcessors: [
                    {
                        value: "BABEL",
                        label: "BABEL",
                        cdnList: CDN_LIST
                    },
                    {
                        value: "Typescript",
                        label: "Typescript",
                        cdnList: []
                    }
                ]
            },
            {
                displayName: "Upload Image",
                name: "image",
                cdnList: [],
                settingsEnabled: true
            }
        ],
        finalCDNData: [],
        cdnValue: '',
        indexToBeEdited: -1,
        isWidgetSettingModalOpen: false,
        payload: {
            file: {},
            fileType: 'APP_IMAGES'
        },
        isUploadingImage: false,
        apis: [
            "https://cors-anywhere.herokuapp.com/https://5ef4cf47ef110d001687c1be.mockapi.io/widgets/barChart",
            "https://cors-anywhere.herokuapp.com/https://5ef4cf47ef110d001687c1be.mockapi.io/widgets/lineChart",
            "https://cors-anywhere.herokuapp.com/https://5ef4cf47ef110d001687c1be.mockapi.io/widgets/gaugeChart",
            "https://cors-anywhere.herokuapp.com/https://5ef4cf47ef110d001687c1be.mockapi.io/widgets/pieChart",
        ]
    };
    mirrorHTMLInstance = null;
    mirrorCSSInstance = null;
    mirrorJSInstance = null;

    componentDidMount() {
        this.props.getBucketImages("APP_IMAGES");
        setTimeout(() => {
            this.mirrorHTMLInstance.refresh();
            this.mirrorCSSInstance.refresh();
            this.mirrorJSInstance.refresh();
        }, 1);
        // $(document.body).click(function (e) {
        //     if (e.target.id != 'dialog') {
        //         $(".dialog").fadeOut(200);
        //     }
        // });
        if (this.props.settings.api) {
            this.setState({
                isFetching: true
            }, () => this.props.invokeApi(this.props.settings.api))
        }
    }

    handlePlayGroundChange = debounce((nextState) => this.props.playGroundChange(nextState.html, nextState.css, nextState.js, nextState.settings), 1000)

    UNSAFE_componentWillUpdate(nextProps, nextState) {
        if (JSON.stringify(nextState) != JSON.stringify(this.state)) {
            setTimeout(() => {
                this.mirrorHTMLInstance.refresh();
                this.mirrorCSSInstance.refresh();
                this.mirrorJSInstance.refresh();
            }, 1)
            this.props.playGroundChange(nextState.html, nextState.css, nextState.js, nextState.settings)
        }

        if (nextProps.html !== this.state.html) {
            this.setState({ html: nextProps.html })
        }

        if (nextProps.css !== this.state.css) {
            this.setState({ css: nextProps.css })
        }
        if (nextProps.js !== this.state.js) {
            this.setState({ js: nextProps.js })
        }

        if (nextProps.settings && nextProps.settings !== this.state.settings) {
            this.setState({ settings: nextProps.settings })
        }

        if (nextProps.uploadImageSuccess && nextProps.uploadImageSuccess !== this.props.uploadImageSuccess) {
            let listOfBucketImages = JSON.parse(JSON.stringify(this.state.listOfBucketImages));
            listOfBucketImages.push({ secure_url: nextProps.uploadImageSuccess.response.secureUrl })
            this.setState({
                listOfBucketImages,
                isOpen: true,
                isUploadingImage: false,
                type: 'success',
                message2: nextProps.uploadImageSuccess.response.message,
                imageUrl: ''
            });
        }

        if (nextProps.uploadImageFailure && nextProps.uploadImageFailure != this.props.uploadImageFailure) {
            this.setState({
                isOpen: true,
                type: 'error',
                message2: nextProps.uploadImageFailure.response
            })
        }

        if (nextProps.getCdnListSuccess && nextProps.getCdnListSuccess !== this.props.getCdnListSuccess) {
            this.setState({
                finalCDNData: nextProps.getCdnListSuccess.response
            });
        }

        if (nextProps.getCdnListFailure && nextProps.getCdnListFailure != this.props.getCdnListFailure) {
            this.setState({
                isOpen: true,
                type: 'error',
                message2: nextProps.getCdnListFailure.response
            })
        }

        if (nextProps.getBucketImagesSuccess && nextProps.getBucketImagesSuccess !== this.props.getBucketImagesSuccess) {
            this.setState({
                listOfBucketImages: nextProps.getBucketImagesSuccess
            })
        }

        if (nextProps.getBucketImagesError && nextProps.getBucketImagesError != this.props.getBucketImagesError) {
            this.setState({
                isOpen: true,
                type: 'error',
                message2: nextProps.getBucketImagesError.response
            })
        }
    }

    getHtmlCode(html) {
        return html;
    }

    inputChangeHandler = (value) => {
        let activeSetting = this.state.editorSettings[this.state.activeSettingsIndex];
        let settings = JSON.parse(JSON.stringify(this.state.settings));
        settings[activeSetting.name].cdnList = settings[activeSetting.name].cdnList.filter(el => el.isRemovable);
        if (value.value) {
            settings[activeSetting.name].preProcessor = value.value;
            settings[activeSetting.name].cdnList = settings[activeSetting.name].cdnList.concat(value.cdnList)
        }
        else {
            settings[activeSetting.name].preProcessor = "";
        }
        this.setState({
            settings,
        })
    }

    handleCodePlayGroundView = (event) => {
        let view = this.state.view,
            count = 0,
            initialCount = 0;

        Object.keys(view).map(key => {
            if (view[key]) {
                initialCount++
            }
        });

        if (initialCount === 1) {
            if (!view[event.target.id]) {
                view[event.target.id] = !view[event.target.id];
            }
        } else {
            view[event.target.id] = !view[event.target.id];
        }

        Object.keys(view).map(key => {
            if (view[key]) {
                count++
            }
        });
        let viewClass = `flex-${parseInt(100 / count)}`;
        this.setState({
            view,
            viewClass
        })
    }

    saveCdn = () => {
        if (!this.state.cdnLink) {
            this.setState({ errorMessage: true });
            return;
        }
        let settings = JSON.parse(JSON.stringify(this.state.settings));
        let activeSettingName = this.state.editorSettings[this.state.activeSettingsIndex].name
        settings[activeSettingName].cdnList.push(
            {
                value: this.state.cdnLink,
                isRemovable: true
            }
        )
        this.setState({ addCdn: false, cdnLink: "", settings })
    }

    handleEdit = (e) => {
        let errorMessage = this.state.errorMessage;
        if (errorMessage) {
            errorMessage = !e.target.value
        }
        this.setState({ cdnLink: e.target.value, errorMessage })
    }

    deleteCdn = (index) => {
        let settings = JSON.parse(JSON.stringify(this.state.settings));
        let activeSettingName = this.state.editorSettings[this.state.activeSettingsIndex].name;
        settings[activeSettingName].cdnList.splice(index, 1)
        this.setState({
            cdnToBeDeletedIndex: null,
            confirmState: false,
            settings,
        })
    }

    cancelClicked = () => {
        this.setState({
            cdnToBeDeletedIndex: null,
            confirmState: false
        })
    }

    openConfirmModal = (index) => {
        this.setState({
            cdnToBeDeletedIndex: index,
            confirmState: true,
        })
    }

    cdnChangeHandler = debounce((cdnValue) => {
        if (cdnValue === "") {
            this.setState({ finalCDNData: [], cdnValue })
        }
        else {
            this.setState({ cdnValue }, () => this.props.getCdnList(cdnValue))
        }
    }, 1000)

    addCDNLink = (index) => {
        let activeSetting = this.state.editorSettings[this.state.activeSettingsIndex];
        let settings = JSON.parse(JSON.stringify(this.state.settings));
        settings[activeSetting.name].cdnList.push({
            value: this.state.finalCDNData[index].latest,
            isRemovable: true,
            name: this.state.finalCDNData[index].name
        })
        this.setState({ settings, finalCDNData: [] }, () => { $("#cdnInput").val(''); });
    }

    copyToClipboard = (data) => {
        const copyText = document.createElement('textarea');
        copyText.value = data;
        copyText.setAttribute('readonly', '');
        copyText.style.position = 'absolute';
        copyText.style.left = '-9999px';
        document.body.appendChild(copyText);
        copyText.select();
        document.execCommand('copy');
        document.body.removeChild(copyText);
        ReactTooltip.show(this.copyMessage)
        ReactTooltip.hide();
        setTimeout(() => ReactTooltip.hide(this.copyMessage), 1000)
    }

    editCDNHandler = (id) => {
        this.setState({ indexToBeEdited: id });
    }

    onEditCDNHandler = (id) => {
        let activeSetting = this.state.editorSettings[this.state.activeSettingsIndex];
        let settings = JSON.parse(JSON.stringify(this.state.settings));
        settings[activeSetting.name].cdnList[id].value = document.getElementById(id).value
        this.setState({ indexToBeEdited: -1, settings })
    }

    isProjectSourceControlled = () => {
        let selectedProjectId = localStorage.getItem("selectedProjectId");
        let projectList = JSON.parse(localStorage.getItem("myProjects"));
        return projectList.some((project) => project.id === selectedProjectId && project.repoName)
    }

    isUserPermissionSourceControlled = () => {
        let selectedProjectId = localStorage.getItem("selectedProjectId");
        let projectList = JSON.parse(localStorage.getItem("myProjects"));
        return projectList.some((project) => project.id === selectedProjectId && project.sourceControlEnabled)
    }

    imageChangeHandler = ({ currentTarget }) => {
        let payload = cloneDeep(this.state.payload)
        var reader = new FileReader();
        reader.onload = (e) => {
            this.setState({
                imageUrl: e.target.result
            })
        }
        payload.file = currentTarget.files[0]
        reader.readAsDataURL(currentTarget.files[0]);
        this.setState({ payload })
    }

    uploadImageHandler = () => {
        event.preventDefault();
        this.setState({
            isUploadingImage: true
        })
        this.props.uploadImage(this.state.payload);
    }

    copyImagePath = (imagePath) => {
        navigator.clipboard.writeText(imagePath)
        this.setState({
            type: "success",
            isOpen: true,
            message2: 'Copied to Clipboard successfully !',
        })
    }

    closeHandler = () => {
        this.setState({
            isOpen: false,
            type: '',
            message1: '',
            message2: ''
        })
    }

    handleApiChange = ({ currentTarget }) => {
        let settings = cloneDeep(this.state.settings)
        settings.api = currentTarget.value
        this.setState({
            settings,
            modalLoader: Boolean(settings.api),
        }, () => settings.api && this.props.invokeApi(event.target.value))
    }

    render() {

        let { html, js, css, settings } = this.state;
        let activeSetting = this.state.editorSettings[this.state.activeSettingsIndex];
        const codeMirrorOptions = {
            theme: 'material',
            lineNumbers: true,
            scrollbarStyle: null,
            lineWrapping: true,
            autoRefresh: true,
            autoFocus: true,
            autoIndent: true,
            indentUnit: 3,
            matchTags: { bothTags: true },
            autoCloseTags: true,
            extraKeys: { "Ctrl-Space": "autocomplete", "Ctrl-Q": function (cm) { cm.foldCode(cm.getCursor()); } },
            foldGutter: true,
            gutters: ["CodeMirror-foldgutter"]
        },

            htmlEditor = (
                <div className="widget-code-editor-body">
                    {!this.state.widgetModal && <button type="button" className="btn btn-link" name="html" onClick={(e) => this.setState({ widgetModal: e.target.getAttribute('name') })}><i name="html" className="fas fa-expand"></i></button>}
                    {this.state.widgetModal && <button className="btn btn-light" onClick={() => this.setState({ widgetModal: false })}><i className="fas fa-compress"></i></button>}
                    <CodeMirror
                        value={html}
                        options={{
                            mode: 'htmlmixed',
                            ...codeMirrorOptions,
                        }}
                        editorDidMount={editor => { this.mirrorHTMLInstance = editor }}
                        onBeforeChange={(editor, data, html) => {
                            this.setState({ html });
                        }}
                    />
                </div>
            ),
            cssEditor = (
                <div className="widget-code-editor-body">
                    {!this.state.widgetModal && <button className="btn btn-link" name="css" onClick={(e) => this.setState({ widgetModal: e.target.getAttribute('name') })}><i name="css" className="fas fa-expand"></i></button>}
                    {this.state.widgetModal && <button className="btn btn-light" onClick={() => this.setState({ widgetModal: false })}><i className="fas fa-compress"></i></button>}
                    <CodeMirror
                        value={css}
                        options={{
                            mode: 'css',
                            ...codeMirrorOptions,
                        }}
                        editorDidMount={editor => {
                            this.mirrorCSSInstance = editor
                        }}
                        onBeforeChange={(editor, data, css) => {
                            this.setState({ css });
                        }}
                    />
                </div>
            ),
            jsEditor = (
                <div className="widget-code-editor-body">
                    {!this.state.widgetModal && <button className="btn btn-link" name="js" onClick={(e) => this.setState({ widgetModal: e.target.getAttribute('name') })}><i name="js" className="fas fa-expand"></i></button>}
                    {this.state.widgetModal && <button className="btn btn-light" onClick={() => this.setState({ widgetModal: false })}><i className="fas fa-compress"></i></button>}
                    <CodeMirror
                        value={js}
                        options={{
                            mode: 'javascript',
                            ...codeMirrorOptions,
                        }}
                        editorDidMount={editor => { this.mirrorJSInstance = editor }}
                        onBeforeChange={(editor, data, js) => { this.setState({ js }); }}
                    />
                </div>
            ),
            outputEditor = (
                <div className="widget-code-editor-body">
                    {!this.state.widgetModal && <button className="btn btn-link" name="output" onClick={(e) => this.setState({ widgetModal: e.target.getAttribute('name') })}><i name="output" className="fas fa-expand"></i></button>}
                    {this.state.widgetModal && <button className="btn btn-light text-dark" onClick={() => this.setState({ widgetModal: false })}><i className="fas fa-compress"></i></button>}
                    <Iframe css={this.state.css} js={this.state.js} html={this.state.html} setting={this.state.settings} id="iframe" />
                </div>
            )


        return (
            this.state.isFetching ?
                <Loader /> :
                <React.Fragment>
                    <div className="action-tabs mr-b-10">
                        <ul className="nav nav-tabs">
                            <li className={`nav-item ${this.state.view.isHTMLView ? "active" : ''}`}>
                                <a className="nav-link" id='isHTMLView' onClick={this.handleCodePlayGroundView}>HTML</a>
                            </li>
                            <li className={`nav-item ${this.state.view.isCSSView ? "active" : ''}`}>
                                <a className="nav-link" id='isCSSView' onClick={this.handleCodePlayGroundView}>CSS</a>
                            </li>
                            <li className={`nav-item ${this.state.view.isJSView ? "active" : ''}`}>
                                <a className="nav-link" id='isJSView' onClick={this.handleCodePlayGroundView} >JS</a>
                            </li>
                            <li className={`nav-item ${this.state.view.isOutputView ? "active" : ''}`}>
                                <a className="nav-link" id='isOutputView' onClick={this.handleCodePlayGroundView}>Output</a>
                            </li>
                            <button type="button" className="btn btn-link" onClick={() => this.setState({ isWidgetSettingModalOpen: true })}>
                                <i className="far fa-cog"></i>Settings
                            </button>
                        </ul>
                    </div>

                    <section className="d-flex">
                        {this.state.view.isHTMLView &&
                            <div className={this.state.viewClass}>
                                <div className="widget-code-editor">
                                    <div className="widget-code-editor-header">HTML{settings.html.preProcessor ? `(${settings.html.preProcessor})` : ""}
                                    </div>
                                    {htmlEditor}
                                </div>
                            </div>
                        }
                        {this.state.view.isCSSView &&
                            <div className={this.state.viewClass}>
                                <div className="widget-code-editor">
                                    <div className="widget-code-editor-header">CSS{settings.css.preProcessor ? `(${settings.css.preProcessor})` : ""}
                                    </div>
                                    {cssEditor}
                                </div>
                            </div>
                        }
                        {this.state.view.isJSView &&
                            <div className={this.state.viewClass}>
                                <div className="widget-code-editor">
                                    <div className="widget-code-editor-header">JS{settings.js.preProcessor ? `(${settings.js.preProcessor})` : ""}</div>
                                    {jsEditor}
                                </div>
                            </div>
                        }
                        {this.state.view.isOutputView &&
                            <div className={this.state.viewClass}>
                                <div className="widget-code-editor">
                                    <div className="widget-code-editor-header">Output</div>
                                    {outputEditor}
                                </div>
                            </div>
                        }
                    </section>

                    {/* widget tab full modal */}
                    {this.state.widgetModal &&
                        <div className="modal d-block animated slideInDown">
                            <div className="modal-dialog modal-full">
                                <div className="modal-content">
                                    <div className="modal-body p-2">
                                        <div className="modal-full-editor">
                                            <div className="action-tabs mb-0">
                                                <ul className="nav nav-tabs">
                                                    <li className={`nav-item ${this.state.widgetModal === "html" ? "active" : ""}`}>
                                                        <a className="nav-link" name="html" onClick={(e) => this.setState({ widgetModal: e.target.getAttribute('name') })}>HTML</a>
                                                    </li>
                                                    <li className={`nav-item ${this.state.widgetModal === "css" ? "active" : ""}`}>
                                                        <a className="nav-link" name="css" onClick={(e) => this.setState({ widgetModal: e.target.getAttribute('name') })}>CSS</a>
                                                    </li>
                                                    <li className={`nav-item ${this.state.widgetModal === "js" ? "active" : ""}`}>
                                                        <a className="nav-link" name="js" onClick={(e) => this.setState({ widgetModal: e.target.getAttribute('name') })}>JS</a>
                                                    </li>
                                                    <li className={`nav-item ${this.state.widgetModal === "output" ? "active" : ""}`}>
                                                        <a className="nav-link" name="output" onClick={(e) => this.setState({ widgetModal: e.target.getAttribute('name') })}>Output</a>
                                                    </li>
                                                </ul>
                                            </div>

                                            <div className="widget-code-editor">
                                                {this.state.widgetModal === "html" && htmlEditor}

                                                {this.state.widgetModal === "css" && cssEditor}

                                                {this.state.widgetModal === "js" && jsEditor}

                                                {this.state.widgetModal === "output" && outputEditor}
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    }
                    {/* end widget tab full modal */}

                    {/* widget setting modal */}
                    {this.state.isWidgetSettingModalOpen &&
                        <div className="modal animated slideInDown d-block" id="addCDNModal">
                            <div className="modal-dialog modal-lg modal-dialog-centered">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <h6 className="modal-title">Page Settings
                                            <button type="button" className="close" data-dismiss="modal" aria-label="Close" data-tooltip data-tooltip-text="Close" data-tooltip-place="bottom" onClick={() => this.setState({ imagePath: '', imageUrl: '', isWidgetSettingModalOpen: false })}>
                                                <i className="far fa-times"></i>
                                            </button>
                                        </h6>
                                    </div>

                                    <div className="modal-body">
                                        <div className="action-tabs">
                                            <ul className="nav nav-tabs">
                                                {this.state.editorSettings.map((mode, index) =>
                                                    <li className={`nav-item ${mode.settingsEnabled ? "" : "disabled"} ${mode.displayName === activeSetting.displayName ? "active" : ""} `} key={index} onClick={() => {
                                                        mode.settingsEnabled && this.setState((prevState) => ({
                                                            activeSettingsIndex: index,
                                                            finalCDNData: prevState.activeSettingsIndex != index ? [] : prevState.finalCDNData
                                                        }), () => {
                                                            this.state.finalCDNData.length == 0 && $("#cdnInput").val('');
                                                        })
                                                    }}>
                                                        <a className="nav-link">{mode.displayName}</a>
                                                    </li>
                                                )}
                                                {!this.state.addCdn && activeSetting.name !== "image" &&
                                                    <button type="button" className="btn btn-link" onClick={() => this.setState({ addCdn: true })}><i className="far fa-plus"></i>Add New</button>
                                                }
                                            </ul>
                                        </div>

                                        {activeSetting.name !== "image" ?
                                            <React.Fragment>
                                                <div className="form-group">
                                                    <label className="form-group-label">{activeSetting.displayName} Preprocessor :</label>
                                                    <Select
                                                        id="preProcessor"
                                                        value={activeSetting.preProcessors.find(el => el.value === settings[activeSetting.name].preProcessor) || { value: "", label: 'None' }}
                                                        onChange={this.inputChangeHandler}
                                                        options={[{ value: "", label: 'None' },
                                                        ...activeSetting.preProcessors
                                                        ]}
                                                    ></Select>
                                                </div>

                                                <div className="form-group search-wrapper">
                                                    <div className="search-wrapper-group">
                                                        <div className="input-group">
                                                            <input type="search" className="form-control" id="cdnInput" placeholder="Search CDN here..." onChange={({ currentTarget }) => this.cdnChangeHandler(currentTarget.value)} />
                                                            <span className="search-wrapper-icon"><i className="far fa-search"></i></span>
                                                            <button type="button" className="search-wrapper-button"><i className="far fa-times"></i></button>
                                                            <div className="input-group-append">
                                                                <button type="button" className="btn btn-primary">Search</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="search-wrapper-suggestion">
                                                        {this.state.finalCDNData && this.state.finalCDNData.length > 0 &&
                                                            <ul className="list-style-none">
                                                                <li className="text-orange">{this.state.finalCDNData.length} Results found</li>
                                                                {this.state.finalCDNData.map((item, index) =>
                                                                    <li key={index} value={item.value} onClick={() => this.addCDNLink(index)}>
                                                                        {item.name} <br />
                                                                        <span className="text-primary f-11">{item.latest}</span>
                                                                    </li>
                                                                )}
                                                            </ul>
                                                        }
                                                    </div>
                                                </div>

                                                {settings[activeSetting.name].cdnList && settings[activeSetting.name].cdnList.length > 0 ?
                                                    <ul className="list-style-none widget-cdn-list">
                                                        {settings[activeSetting.name].cdnList.map((cdn, index) => {
                                                            if (this.state.indexToBeEdited === index) {
                                                                return (
                                                                    <li className="d-flex align-items-center mr-b-10">
                                                                        <div className="flex-90">
                                                                            <div className="form-group">
                                                                                <input type="text" id={index} placeholder="CDN Link" onChange={this.handleEdit} required className="form-control" defaultValue={cdn.value} autoFocus />
                                                                                {this.state.errorMessage && <span className="form-group-error">Please enter a valid CDN link.</span>}
                                                                            </div>
                                                                        </div>
                                                                        <div className="flex-10 text-right">
                                                                            <div className="button-group">
                                                                                <button type="button" className="btn-transparent btn-transparent-green" data-tooltip data-tooltip-text="Save" data-tooltip-place="bottom" onClick={() => this.onEditCDNHandler(index)}>
                                                                                    <i className="far fa-check"></i>
                                                                                </button>
                                                                                <button type="button" className="btn-transparent btn-transparent-gray" data-tooltip data-tooltip-text="Cancel" data-tooltip-place="bottom" onClick={() => this.setState({ indexToBeEdited: -1 })}>
                                                                                    <i className="far fa-times"></i>
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                )
                                                            } else {
                                                                return (
                                                                    <li key={index} className="d-flex align-items-center mr-b-10">
                                                                        <div className="flex-85">
                                                                            <div className="alert alert-primary form-group">
                                                                                <a href={cdn.value} target="_blank" className="text-truncate d-block"><i className="far fa-link mr-2"></i>{cdn.value}</a>
                                                                            </div>
                                                                        </div>
                                                                        <div className="flex-15 text-right">
                                                                            <div className="button-group">
                                                                                <button type="button" className="btn-transparent btn-transparent-green" data-tooltip data-tooltip-text="Copy" data-tooltip-place="bottom" onClick={() => this.copyToClipboard(cdn.value)}>
                                                                                    <i className="far fa-copy"></i>
                                                                                </button>
                                                                                <button type="button" className="btn-transparent btn-transparent-blue" data-tooltip data-tooltip-text="Edit" data-tooltip-place="bottom" onClick={() => this.editCDNHandler(index)}>
                                                                                    <i className="far fa-pencil"></i>
                                                                                </button>
                                                                                <button type="button" className="btn-transparent btn-transparent-red" data-tooltip data-tooltip-text="Delete" data-tooltip-place="bottom" onClick={() => this.openConfirmModal(index)} disabled={!cdn.isRemovable}>
                                                                                    <i className="far fa-trash-alt"></i>
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                )
                                                            }
                                                        })}
                                                        {this.state.addCdn &&
                                                            <li className="d-flex align-items-center mr-b-10">
                                                                <div className="flex-90">
                                                                    <div className="form-group mb-0">
                                                                        <input type="text" id="name" className="form-control" placeholder="CDN Link" value={this.state.cdnLink} onChange={this.handleEdit} required autoFocus />
                                                                        {this.state.errorMessage && <span className="form-group-error">Please enter a valid CDN link.</span>}
                                                                    </div>
                                                                </div>
                                                                <div className="flex-10 text-right">
                                                                    <div className="button-group">
                                                                        <button type="button" className="btn-transparent btn-transparent-green" data-tooltip data-tooltip-text="Save" data-tooltip-place="bottom" onClick={this.saveCdn}>
                                                                            <i className="far fa-check"></i>
                                                                        </button>
                                                                        <button type="button" className="btn-transparent btn-transparent-gray" data-tooltip data-tooltip-text="Cancel" data-tooltip-place="bottom"
                                                                            onClick={() => this.setState({
                                                                                cdnLink: "",
                                                                                addCdn: false,
                                                                                errorMessage: false
                                                                            })}>
                                                                            <i className="far fa-times"></i>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        }
                                                    </ul> :
                                                    this.state.addCdn ?
                                                        <ul className="list-style-none widget-cdn-list">
                                                            <li className="d-flex align-items-center mr-b-10">
                                                                <div className="flex-90">
                                                                    <div className="form-group mb-0">
                                                                        <input type="text" id="name" className="form-control" placeholder="CDN Link" value={this.state.cdnLink} onChange={this.handleEdit} required autoFocus />
                                                                        {this.state.errorMessage && <span className="form-group-error">Please enter a valid CDN link.</span>}
                                                                    </div>
                                                                </div>
                                                                <div className="flex-10 text-right">
                                                                    <div className="button-group">
                                                                        <button type="button" className="btn-transparent btn-transparent-green" data-tooltip data-tooltip-text="Save" data-tooltip-place="bottom" onClick={this.saveCdn}>
                                                                            <i className="far fa-check"></i>
                                                                        </button>
                                                                        <button type="button" className="btn-transparent btn-transparent-gray" data-tooltip data-tooltip-text="Cancel" data-tooltip-place="bottom"
                                                                            onClick={() => this.setState({
                                                                                cdnLink: "",
                                                                                addCdn: false,
                                                                                errorMessage: false
                                                                            })}>
                                                                            <i className="far fa-times"></i>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        </ul> :
                                                        <div className="widget-cdn-list-empty">
                                                            <div>
                                                                <div className="mb-3"><i className="fad fa-link text-primary f-26"></i></div>
                                                                <h6 className="text-dark-theme">You haven't added any CDN link yet.</h6>
                                                                <p className="text-content m-0">Please add a CDN link first.</p>
                                                            </div>
                                                        </div>
                                                }
                                            </React.Fragment>
                                            :
                                            <div className="d-flex">
                                                <div className="flex-40 pd-r-10">
                                                    <div className="form-group">
                                                        <label className="form-group-label">Image :</label>
                                                        <div className="file-upload-box">
                                                            {this.state.isUploadingImage ?
                                                                <div className="file-upload-loader">
                                                                    <img src="https://content.iot83.com/m83/misc/uploading.gif" />
                                                                    <p>Uploading...</p>
                                                                </div> :
                                                                this.state.imageUrl ?
                                                                    <div className="file-upload-preview">
                                                                        <img src={this.state.imageUrl} />
                                                                        <input type="file" accept="image/*" name="image" id="UploadImage" onChange={this.imageChangeHandler} />
                                                                        <button type="button" className="btn-transparent btn-transparent-gray" data-tooltip data-tooltip-text="Update" data-tooltip-place="bottom" onClick={() => document.getElementById("UploadImage").click()}>
                                                                            <i className="far fa-pen" />
                                                                        </button>
                                                                    </div> :
                                                                    <div className="file-upload-form">
                                                                        <input type="file" name="image" onChange={this.imageChangeHandler} />
                                                                        <p>Click to upload file...!</p>
                                                                    </div>
                                                            }
                                                        </div>
                                                    </div>
                                                    <button type="submit" className="btn btn-success w-100" disabled={!this.state.imageUrl} onClick={() => this.uploadImageHandler()}>Upload</button>
                                                </div>

                                                <div className="flex-60 pd-l-10">
                                                    {this.state.listOfBucketImages.length > 0 ?
                                                        <ul className="list-style-none d-flex widget-image-list">
                                                            {this.state.listOfBucketImages.map((image, imageIndex) => {
                                                                return (
                                                                    <li key={imageIndex} className="flex-33">
                                                                        <div className="widget-image-list-box">
                                                                            <div className="widget-image-list-icon">
                                                                                <img src={image.secure_url} />
                                                                            </div>
                                                                            <button type='button' className="btn-transparent btn-transparent-green copy-button" data-tooltip data-tooltip-text="Copy" data-tooltip-place="bottom" onClick={() => this.copyImagePath(image.secure_url)}>
                                                                                <i className="far fa-copy"></i>
                                                                            </button>
                                                                            <p>{image.secure_url}</p>
                                                                        </div>
                                                                    </li>
                                                                )
                                                            })}
                                                        </ul> :
                                                        <div className="widget-image-list-empty">
                                                            <div>
                                                                <div className="mb-3"><i className="fad fa-image-polaroid text-primary f-26"></i></div>
                                                                <h6 className="text-dark-theme">You haven't uploaded any image yet.</h6>
                                                                <p className="text-content m-0">Please upload a image first.</p>
                                                            </div>
                                                        </div>
                                                    }
                                                </div>
                                            </div>
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                    }
                    {/* end widget setting modal */}


                    {
                        this.state.confirmState &&
                        <ConfirmModel
                            status={"delete"}
                            deleteName={settings[activeSetting.name].cdnList[this.state.cdnToBeDeletedIndex].value}
                            confirmClicked={() => {
                                this.deleteCdn(this.state.cdnToBeDeletedIndex)
                            }}
                            cancelClicked={() => {
                                this.cancelClicked()
                            }}
                        />
                    }

                    {
                        this.state.isOpen &&
                        <NotificationModal
                            type={this.state.type}
                            message2={this.state.message2}
                            onCloseHandler={this.closeHandler}
                        />
                    }
                </React.Fragment >
        );
    }
}

CodePlayGround.propTypes = {
    dispatch: PropTypes.func.isRequired
};


const mapStateToProps = createStructuredSelector({
    uploadImageSuccess: SELECTORS.uploadImageSuccess(),
    uploadImageFailure: SELECTORS.uploadImageFailure(),
    getCdnListSuccess: SELECTORS.getCdnListSuccess(),
    getCdnListFailure: SELECTORS.getCdnListFailure(),
    invokeApiSuccess: SELECTORS.invokeApiSuccess(),
    invokeApiError: SELECTORS.invokeApiError(),
    getBucketImagesSuccess: SELECTORS.getBucketImagesSuccess(),
    getBucketImagesError: SELECTORS.getBucketImagesError(),
}
);


function mapDispatchToProps(dispatch) {
    return {
        dispatch,
        uploadImage: (payload) => dispatch(Actions.uploadImage(payload)),
        getCdnList: (term) => dispatch(Actions.getCdnList(term)),
        invokeApi: (uri) => dispatch(Actions.invokeApi(uri)),
        getBucketImages: (bucketName) => dispatch(Actions.getBucketImages(bucketName)),
    };
}

const withReducer = injectReducer({ key: "CodePlayGround", reducer });
const withSaga = injectSaga({ key: "CodePlayGround", saga });

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

export default compose(
    withReducer,
    withSaga,
    withConnect
)(CodePlayGround);

