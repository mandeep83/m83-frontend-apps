

export const UPLOAD_IMAGE = "app/CodePlayGround/UPLOAD_IMAGE";
export const UPLOAD_IMAGE_SUCCESS = "app/CodePlayGround/UPLOAD_IMAGE_SUCCESS";
export const UPLOAD_IMAGE_FAILURE = "app/CodePlayGround/UPLOAD_IMAGE_FAILURE";

export const GET_CDN_LIST = "app/CodePlayGround/GET_CDN_LIST";
export const GET_CDN_LIST_SUCCESS = "app/CodePlayGround/GET_CDN_LIST_SUCCESS";
export const GET_CDN_LIST_FAILURE = "app/CodePlayGround/GET_CDN_LIST_FAILURE";

export const INVOKE_API = "app/CodePlayGround/INVOKE_API";
export const INVOKE_API_SUCCESS = "app/CodePlayGround/INVOKE_API_SUCCESS";
export const INVOKE_API_FAILURE = "app/CodePlayGround/INVOKE_API_FAILURE";

export const GET_BUCKET_IMAGES = "app/CodePlayGround/GET_BUCKET_IMAGES";
export const GET_BUCKET_IMAGES_SUCCESS = "app/CodePlayGround/GET_BUCKET_IMAGES_SUCCESS";
export const GET_BUCKET_IMAGES_FAILURE = "app/CodePlayGround/GET_BUCKET_IMAGES_FAILURE";
