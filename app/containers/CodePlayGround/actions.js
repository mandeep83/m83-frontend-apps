
import * as CONSTANTS from "./constants";

export function uploadImage(payload) {
  return {
    type: CONSTANTS.UPLOAD_IMAGE,
    payload,
  }
}

export function getCdnList(term) {
  return {
    type: CONSTANTS.GET_CDN_LIST,
    term,
  }
}

export function invokeApi(uri) {
  return {
    type: CONSTANTS.INVOKE_API,
    uri,
    method: "GET",
    // index,
  };
}

export function getBucketImages(bucketName) {
  return {
    type: CONSTANTS.GET_BUCKET_IMAGES,
    bucketName
  }
}