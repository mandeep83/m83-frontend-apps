export const CHART_TEMPLATES =  {
    lineChart : {
        template : `

    // Use themes
    am4core.useTheme(am4themes_animated);

    // Create chart instance
    var chart = am4core.create("chartdiv", am4charts.XYChart);
    chart.paddingRight = 20;

    // Create axes
    var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
    dateAxis.renderer.minGridDistance = 50;
    dateAxis.renderer.grid.template.location = 0.5;
    dateAxis.startLocation = 0.5;
    dateAxis.endLocation = 0.5;

    // Create value axis
    var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

    // Create series
    var series1 = chart.series.push(new am4charts.LineSeries());
    series1.dataFields.valueY = "value";
    series1.dataFields.dateX = "date";
    series1.strokeWidth = 3;
    series1.tensionX = 0.8;
    series1.bullets.push(new am4charts.CircleBullet());
    series1.data = CHART_DATA`,
    defaultData: [{
        "date": new Date(2018, 3, 20),
        "value": 990
        }, {
        "date": new Date(2018, 3, 23),
        "value": 125
        }, {
        "date": new Date(2018, 3, 26),
        "value": 77
        }, {
        "date": new Date(2018, 3, 28),
        "value": 113
    }]
},
    barChart: {
        template: `
        // Themes begin
        am4core.useTheme(am4themes_animated);
        // Themes end
        
        // Create chart instance
        var chart = am4core.create("chartdiv", am4charts.XYChart);
        chart.scrollbarX = new am4core.Scrollbar();
        
        // Add data
        chart.data = CHART_DATA;
        
        // Create axes
        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "country";
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 30;
        categoryAxis.renderer.labels.template.horizontalCenter = "right";
        categoryAxis.renderer.labels.template.verticalCenter = "middle";
        categoryAxis.renderer.labels.template.rotation = 270;
        categoryAxis.tooltip.disabled = true;
        categoryAxis.renderer.minHeight = 110;
        
        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.renderer.minWidth = 50;
        
        // Create series
        var series = chart.series.push(new am4charts.ColumnSeries());
        series.sequencedInterpolation = true;
        series.dataFields.valueY = "visits";
        series.dataFields.categoryX = "country";
        series.tooltipText = "[{categoryX}: bold]{valueY}[/]";
        series.columns.template.strokeWidth = 0;
        
        series.tooltip.pointerOrientation = "vertical";
        
        series.columns.template.column.cornerRadiusTopLeft = 10;
        series.columns.template.column.cornerRadiusTopRight = 10;
        series.columns.template.column.fillOpacity = 0.8;
        
        // on hover, make corner radiuses bigger
        var hoverState = series.columns.template.column.states.create("hover");
        hoverState.properties.cornerRadiusTopLeft = 0;
        hoverState.properties.cornerRadiusTopRight = 0;
        hoverState.properties.fillOpacity = 1;
        
        series.columns.template.adapter.add("fill", function(fill, target) {
        return chart.colors.getIndex(target.dataItem.index);
        });
        
        // Cursor
        chart.cursor = new am4charts.XYCursor()`,
        defaultData: [{
            "country": "United States",
            "visits": 325
            }, {
            "country": "China",
            "visits": 1882
            }, {
            "country": "Japan",
            "visits": 1809
            }, {
            "country": "Germany",
            "visits": 1322
            }, {
            "country": "UK",
            "visits": 1122
            }, {
            "country": "France",
            "visits": 1114
            }, {
            "country": "India",
            "visits": 984
            }, {
            "country": "Spain",
            "visits": 711
            }, {
            "country": "Netherlands",
            "visits": 665
            }, {
            "country": "Russia",
            "visits": 580
            }, {
            "country": "South Korea",
            "visits": 443
            }, {
            "country": "Canada",
            "visits": 441
            }]},
    pieChart: {
        template: `
        // Themes begin
        am4core.useTheme(am4themes_animated);
        // Themes end
        
        // Create chart instance
        var chart = am4core.create("chartdiv", am4charts.PieChart);
        
        // Add data
        chart.data = CHART_DATA;
        
        // Add and configure Series
        var pieSeries = chart.series.push(new am4charts.PieSeries());
        pieSeries.dataFields.value = "litres";
        pieSeries.dataFields.category = "country";
        pieSeries.slices.template.stroke = am4core.color("#fff");
        pieSeries.slices.template.strokeOpacity = 1;
        
        // This creates initial animation
        pieSeries.hiddenState.properties.opacity = 1;
        pieSeries.hiddenState.properties.endAngle = -90;
        pieSeries.hiddenState.properties.startAngle = -90;
        
        chart.hiddenState.properties.radius = am4core.percent(0)`,
        defaultData: [ {
            "country": "Denmark",
            "litres": 51.9
            }, {
            "country": "Czechia",
            "litres": 301.9
            }, {
            "country": "Ireland",
            "litres": 201.1
            }, {
            "country": "Germany",
            "litres": 165.8
            }, {
            "country": "Australia",
            "litres": 139.9
            }, {
            "country": "Austria",
            "litres": 128.3
            }, {
            "country": "UK",
            "litres": 99
            }
            ]},
    gaugeChart : {
        template: `
    // Themes begin
    am4core.useTheme(am4themes_animated);
    // Themes end
    
    var chartMin = -50;
    var chartMax = 100;
    
    var data = CHART_DATA;
    
    /**
    Grading Lookup
     */
    function lookUpGrade(lookupScore, grades) {
      // Only change code below this line
      for (var i = 0; i < grades.length; i++) {
        if (
          grades[i].lowScore < lookupScore &&
          grades[i].highScore >= lookupScore
        ) {
          return grades[i];
        }
      }
      return null;
    }
    
    // create chart
    var chart = am4core.create("chartdiv", am4charts.GaugeChart);
    chart.hiddenState.properties.opacity = 0;
    chart.fontSize = 11;
    chart.innerRadius = am4core.percent(80);
    chart.resizable = true;
    
    /**
     * Normal axis
     */
    
    var axis = chart.xAxes.push(new am4charts.ValueAxis());
    axis.min = chartMin;
    axis.max = chartMax;
    axis.strictMinMax = true;
    axis.renderer.radius = am4core.percent(80);
    axis.renderer.inside = true;
    axis.renderer.line.strokeOpacity = 0.1;
    axis.renderer.ticks.template.disabled = false;
    axis.renderer.ticks.template.strokeOpacity = 1;
    axis.renderer.ticks.template.strokeWidth = 0.5;
    axis.renderer.ticks.template.length = 5;
    axis.renderer.grid.template.disabled = true;
    axis.renderer.labels.template.radius = am4core.percent(15);
    axis.renderer.labels.template.fontSize = "0.9em";
    
    /**
     * Axis for ranges
     */
    
    var axis2 = chart.xAxes.push(new am4charts.ValueAxis());
    axis2.min = chartMin;
    axis2.max = chartMax;
    axis2.strictMinMax = true;
    axis2.renderer.labels.template.disabled = true;
    axis2.renderer.ticks.template.disabled = true;
    axis2.renderer.grid.template.disabled = false;
    axis2.renderer.grid.template.opacity = 0.5;
    axis2.renderer.labels.template.bent = true;
    axis2.renderer.labels.template.fill = am4core.color("#000");
    axis2.renderer.labels.template.fontWeight = "bold";
    axis2.renderer.labels.template.fillOpacity = 0.3;
    
    
    
    /**
    Ranges
    */
    
    for (let grading of data.gradingData) {
        var range = axis2.axisRanges.create();
        range.axisFill.fill = am4core.color(grading.color);
        range.axisFill.fillOpacity = 0.8;
        range.axisFill.zIndex = -1;
        range.value = grading.lowScore > chartMin ? grading.lowScore : chartMin;
        range.endValue = grading.highScore < chartMax ? grading.highScore : chartMax;
        range.grid.strokeOpacity = 0;
        range.stroke = am4core.color(grading.color).lighten(-0.1);
        range.label.inside = true;
        range.label.text = grading.title.toUpperCase();
        range.label.inside = true;
        range.label.location = 0.5;
        range.label.inside = true;
        range.label.radius = am4core.percent(10);
        range.label.paddingBottom = -5; // ~half font size
        range.label.fontSize = "0.9em";
    }
    
    var matchingGrade = lookUpGrade(data.score, data.gradingData);
    
    /**
     * Label 1
     */
    
    var label = chart.radarContainer.createChild(am4core.Label);
    label.isMeasured = false;
    label.fontSize = "6em";
    label.x = am4core.percent(50);
    label.paddingBottom = 15;
    label.horizontalCenter = "middle";
    label.verticalCenter = "bottom";
    //label.dataItem = data;
    label.text = data.score.toFixed(1);
    //label.text = "{score}";
    label.fill = am4core.color(matchingGrade.color);
    
    /**
     * Label 2
     */
    
    var label2 = chart.radarContainer.createChild(am4core.Label);
    label2.isMeasured = false;
    label2.fontSize = "2em";
    label2.horizontalCenter = "middle";
    label2.verticalCenter = "bottom";
    label2.text = matchingGrade.title.toUpperCase();
    label2.fill = am4core.color(matchingGrade.color);
    
    
    /**
     * Hand
     */
    
    var hand = chart.hands.push(new am4charts.ClockHand());
    hand.axis = axis2;
    hand.innerRadius = am4core.percent(55);
    hand.startWidth = 8;
    hand.pin.disabled = true;
    hand.value = data.score;
    hand.fill = am4core.color("#444");
    hand.stroke = am4core.color("#000");
    
    hand.events.on("positionchanged", function(){
        label.text = axis2.positionToValue(hand.currentPosition).toFixed(1);
        var value2 = axis.positionToValue(hand.currentPosition);
        var matchingGrade = lookUpGrade(axis.positionToValue(hand.currentPosition), data.gradingData);
        label2.text = matchingGrade.title.toUpperCase();
        label2.fill = am4core.color(matchingGrade.color);
        label2.stroke = am4core.color(matchingGrade.color);  
        label.fill = am4core.color(matchingGrade.color);
    })`,
    defaultData: {
        score: 5.7,
        gradingData: [
          {
            title: "Unsustainable",
            color: "#ee1f25",
            lowScore: -100,
            highScore: -20
          },
          {
            title: "Volatile",
            color: "#f04922",
            lowScore: -20,
            highScore: 0
          },
          {
            title: "Foundational",
            color: "#fdae19",
            lowScore: 0,
            highScore: 20
          },
          {
            title: "Developing",
            color: "#f3eb0c",
            lowScore: 20,
            highScore: 40
          },
          {
            title: "Maturing",
            color: "#b0d136",
            lowScore: 40,
            highScore: 60
          },
          {
            title: "Sustainable",
            color: "#54b947",
            lowScore: 60,
            highScore: 80
          },
          {
            title: "High Performing",
            color: "#0f9747",
            lowScore: 80,
            highScore: 100
          }
        ]
      }
}

}