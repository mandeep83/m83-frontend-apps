import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
 * Direct selector to the createOrEditWidget state domain
 */

const selectCodePlayGroundDomain = state =>
  state.get("CodePlayGround", initialState);

export const uploadImageSuccess = () =>
  createSelector(selectCodePlayGroundDomain, substate => substate.uploadImageSuccess);
export const uploadImageFailure = () =>
  createSelector(selectCodePlayGroundDomain, substate => substate.uploadImageFailure);

export const getCdnListSuccess = () =>
  createSelector(selectCodePlayGroundDomain, substate => substate.getCdnListSuccess);
export const getCdnListFailure = () =>
  createSelector(selectCodePlayGroundDomain, substate => substate.getCdnListError);

export const invokeApiSuccess = () =>
  createSelector(selectCodePlayGroundDomain, substate => substate.invokeApiSuccess);
export const invokeApiError = () =>
  createSelector(selectCodePlayGroundDomain, substate => substate.invokeApiError);

export const getBucketImagesSuccess = () =>
  createSelector(selectCodePlayGroundDomain, substate => substate.getBucketImagesSuccess);
export const getBucketImagesError = () =>
  createSelector(selectCodePlayGroundDomain, substate => substate.getBucketImagesError);

