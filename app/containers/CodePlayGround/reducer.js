import { fromJS } from "immutable";
import * as CONSTANTS from "./constants";

export const initialState = fromJS({});

function CodePlayGround(state = initialState, action) {
  switch (action.type) {
    case CONSTANTS.UPLOAD_IMAGE_SUCCESS:
      return Object.assign({}, state, {
        'uploadImageSuccess': { timestamp: Date.now(), response: action.response }
      });

    case CONSTANTS.UPLOAD_IMAGE_FAILURE:
      return Object.assign({}, state, {
        'uploadImageFailure': { response: action.error, timestamp: Date.now() }
      });

    case CONSTANTS.GET_CDN_LIST_SUCCESS:
      return Object.assign({}, state, {
        'getCdnListSuccess': { timestamp: Date.now(), response: action.response }
      });

    case CONSTANTS.GET_CDN_LIST_FAILURE:
      return Object.assign({}, state, {
        'getCdnListError': { response: action.error, timestamp: Date.now() }
      });

    case CONSTANTS.INVOKE_API_SUCCESS:
      return Object.assign({}, state, {
        'invokeApiSuccess': action.response
      });

    case CONSTANTS.INVOKE_API_FAILURE:
      return Object.assign({}, state, {
        'invokeApiError': { response: action.error, timestamp: Date.now() }
      });

    case CONSTANTS.GET_BUCKET_IMAGES_SUCCESS:
      return Object.assign({}, state, {
        'getBucketImagesSuccess': action.response
      });

    case CONSTANTS.GET_BUCKET_IMAGES_FAILURE:
      return Object.assign({}, state, {
        'getBucketImagesError': { response: action.error, timestamp: Date.now() }
      });
    default:
      return state;
  }
}

export default CodePlayGround;