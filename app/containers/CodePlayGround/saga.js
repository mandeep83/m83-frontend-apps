import { takeEvery } from 'redux-saga';
import * as CONSTANTS from './constants';
import { apiCallHandler } from '../../api';


export function* uploadImage(action) {
  yield [apiCallHandler(action, CONSTANTS.UPLOAD_IMAGE_SUCCESS, CONSTANTS.UPLOAD_IMAGE_FAILURE, 'uploadFile')];
}

export function* getCdnList(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_CDN_LIST_SUCCESS, CONSTANTS.GET_CDN_LIST_FAILURE, 'getCdnList')];
}

export function* invokeApi(action) {
  yield [apiCallHandler(action, CONSTANTS.INVOKE_API_SUCCESS, CONSTANTS.INVOKE_API_FAILURE, 'invokeApi', false)];
}

export function* getBucketImagesApiHandler(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_BUCKET_IMAGES_SUCCESS, CONSTANTS.GET_BUCKET_IMAGES_FAILURE, 'getBucketImages', false)];
}

export function* watcherUploadImage() {
    yield takeEvery(CONSTANTS.UPLOAD_IMAGE,  uploadImage);
}

export function* watcherGetCdnList() {
    yield takeEvery(CONSTANTS.GET_CDN_LIST,  getCdnList);
}

export function* watcherInvokeApi() {
    yield takeEvery(CONSTANTS.INVOKE_API,  invokeApi);
}

export function* watcherGetBucketImages() {
    yield takeEvery(CONSTANTS.GET_BUCKET_IMAGES,  getBucketImagesApiHandler);
}

export default function* rootSaga() {
    yield [
        watcherUploadImage(),
        watcherGetCdnList(),
        watcherInvokeApi(),
        watcherGetBucketImages(),
    ];
  }