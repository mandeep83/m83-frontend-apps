/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import React from "react";
import { Redirect } from "react-router";
import { Switch, Route } from "react-router-dom";
import HomePage from "../HomePage/Loadable";
import LoginPage from "../LoginPage/Loadable";
import ResetPassword from "../ResetPassword/Loadable";
import ChangePassword from "../ChangePassword/Loadable";
import NotFoundPage from "../../components/NotFoundPage/Loadable";
import iSignUp from "../iSignUp/Loadable";
import TermsAndConditions from "../TermsAndConditions/Loadable";
import PrivacyPolicy from "../PrivacyPolicy/Loadable";

import SignUp from "../SignUp/Loadable";
import jwt_decode from "jwt-decode";
import OauthCallBack from '../OauthCallBack/Loadable';
import Tooltip from '../../components/Tooltip/Loadable';


const offlineDiv = (
    <div id="noNetworkDiv" className="outer-error">
        <div className="outer-error-content">
            <div className="outer-error-icon">
                <img src={require('../../assets/images/other/noInternet.gif')} />
            </div>
            <h1>No internet</h1>
            <h5>There is no internet connection!</h5>
            <p>Check your connection or try again</p>
        </div>
    </div>
);
export default function App() {
    const token = localStorage.token;
    let expTime;
    var link = document.createElement('link');
    link.id = "themeCSS"
    link.rel = "stylesheet"
    link.href = window.API_URL + "api/unauth/user/theme.css" + `?userId=${localStorage.getItem("userId")}`;
    document.getElementsByTagName('head')[0].appendChild(link);
    if (token) {
        expTime = parseInt(jwt_decode(token).exp + "000");
    }
    if (typeof typeof token !== "undefined" && token !== "" && expTime >= new Date() && !window.location.pathname.includes("changePassword") && !window.location.pathname.includes("termsAndConditions") && !window.location.pathname.includes("privacyPolicy")) {
        return (
            <React.Fragment>
                <Switch>
                    <Route
                        path="/"
                        render={props =>
                            navigator.onLine ? <HomePage {...props} /> : offlineDiv
                        }
                    />
                    <Route component={NotFoundPage} />
                </Switch>
                <Tooltip />
            </React.Fragment>
        );
    } else {
        return (
            <React.Fragment>
                <Switch>
                    <Route
                        exact
                        path="/login/:registration?/:errorCode?/:email?"
                        component={navigator.onLine ? LoginPage : offlineDiv}
                    />
                    <Route
                        path="/oauthCallBack"
                        render={props => (navigator.onLine ? <OauthCallBack {...props} /> : offlineDiv)} />
                    <Route
                        exact
                        path="/forgotPassword"
                        component={navigator.onLine ? ResetPassword : offlineDiv}
                    />
                    <Route
                        exact
                        path="/iSignUp"
                        component={navigator.onLine ? iSignUp : offlineDiv}
                    />
                    <Route
                        exact
                        path="/changePassword"
                        component={navigator.onLine ? ChangePassword : offlineDiv}
                    />
                    <Route
                        exact
                        path="/resetCredential"
                        component={navigator.onLine ? ResetPassword : offlineDiv}
                    />
                    {window.API_URL.split(".")[0].split("//")[1] === 'platform' &&
                        <Route
                            exact
                            path="/signUp"
                            component={navigator.onLine ? SignUp : offlineDiv}
                        />
                    }
                    <Route
                        exact
                        path="/termsAndConditions"
                        component={navigator.onLine ? TermsAndConditions : offlineDiv}
                    />
                    <Route
                        exact
                        path="/privacyPolicy"
                        component={navigator.onLine ? PrivacyPolicy : offlineDiv}
                    />
                    <Redirect to="/login" />
                </Switch>
                <Tooltip />
            </React.Fragment>
        );
    }
}
