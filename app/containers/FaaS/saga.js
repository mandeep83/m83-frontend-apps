/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { apiCallHandler } from '../../api';
import { takeEvery } from 'redux-saga';
import * as CONSTANTS from "./constants";

//get faas list 
export function* getFaaSList(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_FAAS_LIST_SUCCESS, CONSTANTS.GET_FAAS_LIST_FAILURE, 'getFaaSList')];
}

//get faas list watcher
export function* watcherGetFaaSList() {
  yield takeEvery(CONSTANTS.GET_FAAS_LIST, getFaaSList);
}

//delete faas by id
export function* deleteFaasById(action) {
  yield [apiCallHandler(action, CONSTANTS.DELETE_FAAS_SUCCESS, CONSTANTS.DELETE_FAAS_FAILURE, 'deleteFaas')];
}

////delete faas by id watcher
export function* watcherDeleteFaasById() {
  yield takeEvery(CONSTANTS.DELETE_FAAS, deleteFaasById);
}

export function* invokeFaasFunction(action) {
  yield [apiCallHandler(action, CONSTANTS.INVOKE_FAAS_FUNCTION_SUCCESS, CONSTANTS.INVOKE_FAAS_FUNCTION_FAILURE, 'invokeFaasFunction')];
}

export function* cloneFaaSFunction(action) {
  yield [apiCallHandler(action, CONSTANTS.CLONE_FAAS_FUNCTION_SUCCESS, CONSTANTS.CLONE_FAAS_FUNCTION_FAILURE, 'cloneFaaSFunction')];
}

export function* getDebugLogsHandler(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_DEBUG_LOGS_SUCCESS, CONSTANTS.GET_DEBUG_LOGS_FAILURE, 'debugLogs')]
}

export function* getDeveloperQuota(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_DEVELOPER_QUOTA_SUCCESS, CONSTANTS.GET_DEVELOPER_QUOTA_FAILURE, 'getDeveloperQuota')]
}

export function* watcherGetDeveloperQuota() {
  yield takeEvery(CONSTANTS.GET_DEVELOPER_QUOTA, getDeveloperQuota);
}

export function* watcherInvokeFaasFunction() {
  yield takeEvery(CONSTANTS.INVOKE_FAAS_FUNCTION, invokeFaasFunction);
}

export function* watcherCloneFaaSFunction() {
  yield takeEvery(CONSTANTS.CLONE_FAAS_FUNCTION, cloneFaaSFunction);
}

export function* watcherGetDebugLogs() {
  yield takeEvery(CONSTANTS.GET_DEBUG_LOGS, getDebugLogsHandler);
}

export default function* rootSaga() {
  yield [
    watcherGetFaaSList(),
    watcherDeleteFaasById(),
    watcherInvokeFaasFunction(),
    watcherCloneFaaSFunction(),
    watcherGetDebugLogs(),
    watcherGetDeveloperQuota(),
  ];
}
