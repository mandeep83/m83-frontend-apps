/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
 * Direct selector to the faaSList state domain
 */

const selectFaasListDomain = state => state.get("faaSList", initialState);

export const makeSelectFaasList = () =>
  createSelector(selectFaasListDomain, subState => subState.toJS());

export const getFassListSuccess = () => createSelector(selectFaasListDomain, subState => subState.getFassListSuccess);
export const getFassListFailure = () => createSelector(selectFaasListDomain, subState => subState.getFassListFailure);

export const getdeleteFaasByIdSuccess = () => createSelector(selectFaasListDomain, subState => subState.getDeleteFaasSuccess)
export const getdeleteFaasByIdFailure = () => createSelector(selectFaasListDomain, subState => subState.getDeleteFaasFailure)
export const getDeleteFaas = () => createSelector(selectFaasListDomain, subState => subState.getDeleteFaas)

export const invokeFaasFunctionSuccess = () => createSelector(selectFaasListDomain, subState => subState.invokeFaasFunctionSuccess);
export const invokeFaasFunctionFailure = () => createSelector(selectFaasListDomain, subState => subState.invokeFaasFunctionFailure);

export const cloneFaaSSuccess = () => createSelector(selectFaasListDomain, subState => subState.cloneFaaSSuccess);
export const cloneFaaSFailure = () => createSelector(selectFaasListDomain, subState => subState.cloneFaaSFailure);

export const isFetchingState = state => state.get('loader');

export const getIsFetching = () =>
  createSelector(isFetchingState, fetchingState => fetchingState.get('isFetching'));

export const getDebugLogsSuccess = () => createSelector(selectFaasListDomain, substate => substate.debugLogsSuccess);
export const getDebugLogsFailure = () => createSelector(selectFaasListDomain, substate => substate.debugLogsFailure);

export const getDeveloperQuotaSuccess = () => createSelector(selectFaasListDomain, substate => substate.developerQuotaSuccess);
export const getDeveloperQuotaFailure = () => createSelector(selectFaasListDomain, substate => substate.developerQuotaFailure);