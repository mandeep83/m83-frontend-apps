/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

export const DEFAULT_ACTION = "app/FaaSList/DEFAULT_ACTION";

export const DELETE_FAAS = "app/FaaSList/DELETE_FAAS";
export const DELETE_FAAS_SUCCESS = "app/FaaSList/DELETE_FAAS_SUCCESS";
export const DELETE_FAAS_FAILURE = "app/FaaSList/DELETE_FAAS_FAILURE";

export const GET_FAAS_LIST = "app/FaaSList/GET_FAAS_LIST";
export const GET_FAAS_LIST_SUCCESS = "app/FaaSList/GET_FAAS_LIST_SUCCESS";
export const GET_FAAS_LIST_FAILURE = "app/FaaSList/GET_FAAS_LIST_FAILURE";

export const INVOKE_FAAS_FUNCTION = "app/FaaSList/INVOKE_FAAS_FUNCTION";
export const INVOKE_FAAS_FUNCTION_SUCCESS = "app/FaaSList/INVOKE_FAAS_FUNCTION_SUCCESS";
export const INVOKE_FAAS_FUNCTION_FAILURE = "app/FaaSList/INVOKE_FAAS_FUNCTION_FAILURE";

export const CLONE_FAAS_FUNCTION = "app/FaaSList/CLONE_FAAS_FUNCTION";
export const CLONE_FAAS_FUNCTION_SUCCESS = "app/FaaSList/CLONE_FAAS_FUNCTION_SUCCESS";
export const CLONE_FAAS_FUNCTION_FAILURE = "app/FaaSList/CLONE_FAAS_FUNCTION_FAILURE";

export const GET_DEBUG_LOGS = "app/FaaSList/GET_DEBUG_LOGS";
export const GET_DEBUG_LOGS_SUCCESS = "app/FaaSList/GET_DEBUG_LOGS_SUCCESS";
export const GET_DEBUG_LOGS_FAILURE = "app/FaaSList/GET_DEBUG_LOGS_FAILURE";

export const GET_DEVELOPER_QUOTA = "app/FaaSList/GET_DEVELOPER_QUOTA";
export const GET_DEVELOPER_QUOTA_SUCCESS = "app/FaaSList/GET_DEVELOPER_QUOTA_SUCCESS";
export const GET_DEVELOPER_QUOTA_FAILURE = "app/FaaSList/GET_DEVELOPER_QUOTA_FAILURE";

export const RESET_TO_INITIAL_STATE = "app/FaaSList/RESET_TO_INITIAL_STATE";
