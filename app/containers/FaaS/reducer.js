/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { fromJS } from "immutable";
import * as CONSTANTS from "./constants";

export const initialState = fromJS({});

function faasListReducer(state = initialState, action) {
  switch (action.type) {
    case CONSTANTS.DEFAULT_ACTION:
      return initialState;

    case CONSTANTS.GET_FAAS_LIST_SUCCESS:
      return Object.assign({}, state, {
        'getFassListSuccess': action.response,
      })
    case CONSTANTS.GET_FAAS_LIST_FAILURE:
      return Object.assign({}, state, {
        'getFassListFailure': action.error,
      })
    case CONSTANTS.DELETE_FAAS_SUCCESS:
      return Object.assign({}, state, {
        'getDeleteFaasSuccess': action.response,
      })
    case CONSTANTS.DELETE_FAAS_FAILURE:
      return Object.assign({}, state, {
        'getDeleteFaasFailure': action.error,
        "getDeleteFaas": Math.random()
      })
    case CONSTANTS.INVOKE_FAAS_FUNCTION:
      return Object.assign({}, state, {
        'invokeFaasFunctionSuccess': undefined,
        'invokeFaasFunctionFailure': undefined,
      })
    case CONSTANTS.INVOKE_FAAS_FUNCTION_SUCCESS:
      return Object.assign({}, state, {
        'invokeFaasFunctionSuccess': action.response,
      })
    case CONSTANTS.INVOKE_FAAS_FUNCTION_FAILURE:
      return Object.assign({}, state, {
        'invokeFaasFunctionFailure': action.error,
      })

    case CONSTANTS.CLONE_FAAS_FUNCTION_SUCCESS:
      return Object.assign({}, state, {
        'cloneFaaSSuccess': {
          response: action.response,
          id: action.id
        },
      })
    case CONSTANTS.CLONE_FAAS_FUNCTION_FAILURE:
      return Object.assign({}, state, {
        'cloneFaaSFailure': {
          error: action.error,
          id: action.addOns.id
        },
      });
    case CONSTANTS.GET_DEBUG_LOGS_SUCCESS:
      return Object.assign({}, state, {
        'debugLogsSuccess': action.response
      });

    case CONSTANTS.GET_DEBUG_LOGS_FAILURE:
      return Object.assign({}, state, {
        'debugLogsFailure': action.error
      });
    case CONSTANTS.GET_DEVELOPER_QUOTA_SUCCESS:
      return Object.assign({}, state, {
        'developerQuotaSuccess': action.response
      });
    case CONSTANTS.GET_DEVELOPER_QUOTA_FAILURE:
      return Object.assign({}, state, {
        'developerQuotaFailure': action.error
      });
    case CONSTANTS.RESET_TO_INITIAL_STATE:
      return initialState;
    default:
      return state;

  }
}

export default faasListReducer;
