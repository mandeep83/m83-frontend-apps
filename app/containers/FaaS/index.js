/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import ReactTable from 'react-table';
import ReactTooltip from 'react-tooltip';
import { createStructuredSelector } from "reselect";
import JSONInput from "react-json-editor-ajrm/dist";
import { compose } from "redux";
import { cloneDeep } from "lodash";
import * as ACTIONS from "./actions";
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import ConfirmModel from "../../components/ConfirmModel";
import * as SELECTORS from "./selectors";
import isPlainObject from 'lodash/isPlainObject'
import reducer from "./reducer";
import saga from "./saga";
import NotificationModal from '../../components/NotificationModal/Loadable';
import Loader from '../../components/Loader/Loadable';
import ListingTable from "../../components/ListingTable/Loadable";
import NoDataFoundMessage from "../../components/NoDataFoundMessage/Loadable"
import AddNewButton from "../../components/AddNewButton/Loadable"
import Skeleton from "react-loading-skeleton";

/* eslint-disable react/prefer-stateless-function */
export class FaaSList extends React.Component {
    state = {
        tableState: {
            page: this.props.location.state && this.props.location.state.page ? parseInt(this.props.location.state.page) : 0,
            pageSize: this.props.location.state && this.props.location.state.pageSize ? parseInt(this.props.location.state.pageSize) : 10,
            searchActionName: this.props.location.state && this.props.location.state.searchActionName ? this.props.location.state.searchActionName : '',
            createdBy: this.props.location.state && this.props.location.state.createdBy ? this.props.location.state.createdBy : '',
            updatedBy: this.props.location.state && this.props.location.state.updatedBy ? this.props.location.state.updatedBy : '',
            kind: this.props.location.kind && this.props.location.state.kind ? this.props.location.state.kind : '',
        },
        isOpen: false,
        toggleView:true,
        faaSList: [],
        tryItOut: false,
        actionName: '',
        pathParameters: [],
        functionResponse: "{}",
        error: false,
        isInvoking: false,
        isFetchingLogs: false,
        isFetching: 1,
        faaSToBeDeleted: '',
        defaultCloneLoader: false,
        functionResponseError: '',
        filteredList: [],
        debugLogs: [],
        fullViewModal: false,
        interpreters: [
            {
                name: "python:3",
                displayName: "Python 3",
                isDisable: false,
                fileType: "python",
            },
            {
                name: "nodejs:12",
                displayName: "NodeJS 12",
                isDisable: false,
                fileType: "javascript",
            },
            {
                name: "swift:4.2",
                displayName: "Swift 4.2",
                isDisable: false,
                fileType: "swift",
            },
            {
                name: "go:1.11",
                displayName: "Golang 1.11",
                isDisable: false,
                fileType: "golang",
            },
            {
                name: "ruby:2.5",
                displayName: "Ruby 2.5",
                isDisable: false,
                fileType: "ruby",
            },
            {
                name: "php:7.4",
                displayName: "PHP 7.4",
                isDisable: false,
                fileType: "php",
            }

        ],
        search: "",
        remainingFaaSCount: 0,
        usedFaaSCount: 0,
        totalFaaSCount: 0,
        faaSListFilter: [],
        activeTab: "parameters"
    }

    componentWillMount() {
        window.history.pushState(null, '');
        let payload = [
            {
                dependingProductId: null,
                productName: "faas",
            }
        ];
        this.props.getDeveloperQuota(payload)

    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.getFassListSuccess && nextProps.getFassListSuccess !== this.props.getFassListSuccess) {
            let isFetching = this.state.isFetching
            let faaSList = nextProps.getFassListSuccess
            faaSList.map((list) => {
                list.kindDisplayName = this.state.interpreters.find(interpreter => interpreter.name === list.kind).displayName
                list.pathParametersPresent = list.pathParameters.length ? "YES" : "NO"
            })
            this.setState({
                isTableLoading: false,
                faaSList,
                faaSListFilter: faaSList,
                isFetching: isFetching ? isFetching - 1 : isFetching
            })
        }

        if (nextProps.getFassListFailure && nextProps.getFassListFailure !== this.props.getFassListFailure) {
            this.setState({
                type: "error",
                isOpen: true,
                message2: nextProps.getFassListFailure
            });
        }

        if (nextProps.getdeleteFaasSuccess && nextProps.getdeleteFaasSuccess !== this.props.getdeleteFaasSuccess) {
            let faaSList = JSON.parse(JSON.stringify(this.state.faaSList)),
                faaSListFilter = JSON.parse(JSON.stringify(this.state.faaSList));
            faaSList = faaSList.filter(faas => faas.id !== nextProps.getdeleteFaasSuccess.id);
            faaSListFilter = faaSList.filter(faas => Boolean(this.state.search) ? faas.actionName.toLowerCase().includes(this.state.search.toLowerCase()) : true)

            this.setState((prevState) => ({
                faaSList,
                type: 'success',
                isOpen: true,
                isFetching: false,
                faaSListFilter,
                message2: nextProps.getdeleteFaasSuccess.message,
                faaSToBeDeleted: '',
                remainingFaaSCount: prevState.remainingFaaSCount + 1,
                usedFaaSCount: prevState.usedFaaSCount - 1,
            }))

        }

        if (nextProps.getdeleteFaasFailure && nextProps.getdeleteFaasFailure !== this.props.getdeleteFaasFailure) {
            this.setState({
                type: "error",
                isOpen: true,
                message2: nextProps.getdeleteFaasFailure,
                isFetching: false
            });
        }

        if (nextProps.invokeFaasFunctionSuccess && nextProps.invokeFaasFunctionSuccess !== this.props.invokeFaasFunctionSuccess) {
            let functionResponse = JSON.parse(JSON.stringify(nextProps.invokeFaasFunctionSuccess))
            this.setState({
                functionResponse: functionResponse.isError ? undefined : functionResponse.response ? functionResponse.response : "Request format and response format do not match.",
                isInvoking: false,
                functionResponseError: functionResponse.isError ? functionResponse.response : undefined,

            }, () => {
                this.props.getDebugLogs(nextProps.invokeFaasFunctionSuccess.activationId)
                this.props.resetToInitialState()
            })
        }

        if (nextProps.invokeFaasFunctionFailure && nextProps.invokeFaasFunctionFailure !== this.props.invokeFaasFunctionFailure) {
            let functionResponseError = nextProps.invokeFaasFunctionFailure
            this.setState({
                functionResponseError,
                isInvoking: false,
            });
        }

        if (nextProps.cloneFaaSSuccess && nextProps.cloneFaaSSuccess !== this.props.cloneFaaSSuccess) {
            this.setState((previousState) => ({
                type: "success",
                isOpen: true,
                message2: nextProps.cloneFaaSSuccess.response,
                remainingFaaSCount: previousState.remainingFaaSCount--,
                usedFaaSCount: previousState.usedFaaSCount++
            }), () => {
                this.props.getFaaSList();
                this.props.resetToInitialState()
            });
        }

        if (nextProps.cloneFaaSFailure && nextProps.cloneFaaSFailure !== this.props.cloneFaaSFailure) {
            this.setState({
                isTableLoading: false,
                type: "error",
                faaSList,
                isOpen: true,
                message2: nextProps.cloneFaaSFailure.error,
                isFetching: false,
            }, () => this.props.resetToInitialState());
        }

        if (nextProps.debugLogsSuccess && nextProps.debugLogsSuccess !== this.props.debugLogsSuccess) {
            this.setState({
                debugLogs: nextProps.debugLogsSuccess,
                isFetchingLogs: false
            })
        }

        if (nextProps.debugLogsFailure && nextProps.debugLogsFailure !== this.props.debugLogsFailure) {
            this.setState({
                debugLogsFailure: nextProps.debugLogsFailure,
                isFetchingLogs: false,
            })
        }

        if (nextProps.developerQuotaSuccess && nextProps.developerQuotaSuccess !== this.props.developerQuotaSuccess) {
            const findObject = nextProps.developerQuotaSuccess.find(product => product.productName === 'faas')
            let remainingFaaSCount = findObject['remaining'],
                totalFaaSCount = findObject['total'],
                usedFaaSCount = findObject['used'];
            this.setState({
                remainingFaaSCount,
                totalFaaSCount,
                usedFaaSCount
            }, () => this.props.getFaaSList());
        }

        if (nextProps.developerQuotaFailure && nextProps.developerQuotaFailure !== this.props.developerQuotaFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.developerQuotaFailure,
            }, () => this.props.getFaaSList())
        }
    }

    tryItOut = () => {
        let pathParameters = [...this.state.pathParameters].map((parameters) => {
            parameters.value = parameters.defaultValue
            parameters.errorMessage = false
            return parameters
        })
        this.setState((prevState, props) => ({
            tryItOut: !prevState.tryItOut,
            pathParameters,
            functionResponse: "",
            functionResponseError: "",
            isInvoking: false,
            debugLogs: [],
        }))
    }

    tryItOutInputHandler = (event, index, type) => {
        let inputValue = event.target.value
        let pathParameters = [...this.state.pathParameters]
        if (pathParameters[index].errorMessage === true || inputValue != "") {
            pathParameters[index].errorMessage = false
            this.setState({
                error: true,
                pathParameters,
                errorMessage: false
            })
        }
        let regexInt = /^[0-9\b]+$/;
        let regexFloat = /^[+-]?([0-9]+([.][0-9]*)?|[.][0-9]+)$/;
        let regexString = /^[a-zA-Z0-9!@#$%^&*()_+\-=\[\]{};': "\\|,.<>\/?]*$/;
        if (type === "int" && (regexInt.test(inputValue) || inputValue === "")) {
            pathParameters[index].value = inputValue ? Number(inputValue) : "";
        }
        else if (type === "String" && (regexString.test(inputValue) || inputValue === "")) {
            pathParameters[index].value = inputValue;
        } else if (type === "float" && (regexFloat.test(inputValue) || inputValue === "")) {
            pathParameters[index].value = inputValue ? Number(inputValue) : "";
        }
        else if (type === "char" && (regexString.test(inputValue) || inputValue === "")) {
            pathParameters[index].value = inputValue ? inputValue[inputValue.length - 1] : "";
        }
        else if (type === "Boolean") {
            pathParameters[index].value = inputValue === "true";
        }
        else if (type === "map") {
            pathParameters[index].value = inputValue;
        }
        this.setState({
            pathParameters
        });
    }

    tryItOutMapHandler = (value, index) => {
        let pathParameters = cloneDeep(this.state.pathParameters);
        pathParameters[index].value = JSON.stringify(value);
        if (pathParameters[index].errorMessage === true && value) {
            pathParameters[index].errorMessage = false
        }
        this.setState({ pathParameters })
    }

    invokeFaasFunction = () => {
        let error = false
        let pathParameters = [...this.state.pathParameters];
        let isInvoking = false, isFetchingLogs = false
        if (pathParameters.length > 0) {
            pathParameters.map((params) => {
                if (params.required && (params.value === "" || params.value === undefined)) {
                    params.errorMessage = true;
                    error = true;
                }
            })
        }

        if (!error) {
            let payload = {};
            isInvoking = true
            isFetchingLogs = true
            this.state.pathParameters.map((temp) => {
                if (temp.type === "map") {
                    if (!payload.map) {
                        payload.map = {}
                    }
                    payload.map[temp.name] = temp.value
                }
                else {
                    if (!payload.parameters) {
                        payload.parameters = {}
                    }
                    payload.parameters[temp.name] = temp.value
                }
            })
            this.props.invokeFaasFunction(payload, this.state.actionId);
        }

        this.setState(prevState => ({
            pathParameters,
            isInvoking,
            isFetchingLogs,
            functionResponse: undefined,
            debugLogs: [],
            activeTab: error ? prevState.activeTab : "result"
        }))
    }

    modalCloseHandler = () => {
        let pathParameters = this.state.pathParameters
        pathParameters.map((params) => {
            params.errorMessage = false
        })
        this.setState({
            tryItOut: false,
            isInvoking: false,
            isFetchingLogs: false,
            error: false,
            activeTab: "parameters",
            pathParameters,
        })
    }

    onCloseHandler = () => {
        this.setState({
            isOpen: false,
            type: '',
            message2: ''
        })
    }

    cancelClicked = () => {
        this.setState({
            deleteName: "",
            faaSToBeDeleted: null,
            confirmState: false
        })
    }

    openModalForInvoking = (actionName, parameters, selectedDescription, id) => {
        let pathParameters = parameters ? [...parameters].map(temp => {
            temp.value = temp.defaultValue;
            return temp
        }) : []
        this.setState({
            actionName,
            pathParameters,
            selectedDescription,
            tryItOut: false,
            isInvoking: false,
            isFetchingLogs: false,
            functionResponse: false,
            functionResponseError: '',
            actionId: id,
            debugLogs: [],
        })
    }

    filterCaseInsensitive = (filter, row) => {
        const id = filter.pivotId || filter.id;
        return (
            row[id] !== undefined ?
                String(row[id].toLowerCase()).startsWith(filter.value.toLowerCase())
                :
                true
        );
    }

    navigateToaddOrEditCodeEngine = (functionId) => {
        let pathname = `/addOrEditFaaS`;
        let state = JSON.parse(JSON.stringify(this.state.tableState))
        if (functionId)
            pathname += `/${functionId}`;
        this.props.history.push({ state, pathname })
    }

    handlePageOrSizeChange = (key, value) => {
        let tableState = JSON.parse(JSON.stringify(this.state.tableState));
        tableState[key] = value;
        this.setState({ tableState });
    }

    filterFaaSListByName = (e) => {
        let faaSListFilter = JSON.parse(JSON.stringify(this.state.faaSList))
        faaSListFilter = faaSListFilter.filter(item => item.actionName.toLowerCase().includes(e.target.value.toLowerCase()))
        this.setState({ search: e.target.value, faaSListFilter })
    }

    removeSearchFieldText = () => {
        let faaSListFilter = JSON.parse(JSON.stringify(this.state.faaSList))
        this.setState({ faaSListFilter, search: "" })
    }

    cloneFaaSFunction = (id) => {
        this.props.cloneFaaSFunction(id);
        this.setState({
            isTableLoading: true
        })
    }

    getColumns = () => {
        return [
            {
                Header: "Name", width: 20,
                accessor: "actionName",
                cell: (row) => (
                    <React.Fragment>
                        <div className="list-view-icon"><i className="fad fa-brackets-curly"></i></div>
                        <div className="list-view-icon-box">
                            <h6 className="text-theme text-truncate fw-600">{row.actionName}</h6>
                            <p>{`${row.description || "-"}`}</p>
                        </div>
                    </React.Fragment>
                ),
                filterable: true,
                sortable: true
            },
            {
                Header: "Interpreter", width: 15, accessor: "kindDisplayName", filterable: true, sortable: true,
                cell: (row) => (
                    <span className="badge badge-pill alert-success list-view-pill w-50">{row.kindDisplayName}</span>
                )
            },
            { Header: "Memory (in MB)", width: 10, accessor: "memory" },
            {
                Header: "Parameterized", width: 10,
                accessor: "pathParametersPresent",
                filterable: true,
                sortable: true,
                cell: (row) =>
                    row.pathParameters && row.pathParameters.length > 0 ?
                        <h6 className="text-green"><i className="fad fa-check-circle mr-2"></i>Yes</h6> :
                        <h6 className="text-red"><i className="fad fa-times-circle mr-2"></i>No</h6>,
                filter: ({ value, onChangeHandler, accessor }) =>
                    <select
                        className="form-control"
                        onChange={event => onChangeHandler(event, accessor)}
                        value={value ? value : 'all'}
                    >
                        <option value="">ALL</option>
                        <option value="YES">YES</option>
                        <option value="NO">NO</option>
                    </select>

            },
            { Header: "Created", width: 15, accessor: "createdAt", sortable: true, filterable: true },
            { Header: "Updated", width: 15, accessor: "updatedAt", sortable: true, filterable: true },
            {
                Header: "Actions", width: 15, cell: (row) => (
                    <div className="button-group">
                        <button className="btn-transparent btn-transparent-yellow"
                            data-tooltip data-tooltip-text="Invoke" data-tooltip-place="bottom"
                            onClick={() => this.openModalForInvoking(row.actionName, row.pathParameters, row.description, row.id)}
                            disabled={row.saveAsDraft} data-toggle="modal" data-target="#invokeModal">
                            <i className="far fa-brackets-curly"></i>
                        </button>
                        {/*<button className="btn-transparent btn-transparent-green" data-tooltip data-tooltip-text="Clone" data-tooltip-place="bottom" onClick={() => this.cloneFaaSFunction(row.id)}>
                            <i className="far fa-copy"></i>
                        </button>*/}
                        <button className="btn-transparent btn-transparent-blue" data-tooltip data-tooltip-text="Edit" data-tooltip-place="bottom" onClick={() => this.navigateToaddOrEditCodeEngine(row.id)}>
                            <i className="far fa-pencil"></i>
                        </button>
                        <button className="btn-transparent btn-transparent-red" disabled={row.isAttached === true} data-tooltip data-tooltip-text="Delete" data-tooltip-place="bottom"
                            onClick={() => this.setState({
                                deleteName: row.actionName,
                                faaSToBeDeleted: row.id,
                                confirmState: true
                            })}>
                            <i className="far fa-trash-alt"></i>
                        </button>
                    </div>
                )
            },
        ]
    }

    getStatusBarClass = (row) => {
        if (row.saveAsDraft)
            return { className: "yellow", tooltipText: "Saved as Draft" }
        if (row.isAttached === true)
            return { className: "green", tooltipText: "In Use" }
        return { className: "gray", tooltipText: "Not In Use" }
    }

    createItemOnAddButtonClick = () => {
        this.props.history.push('/addOrEditFaaS')
    }

    tabChangeHandler = (tab) => {
        this.setState({
            activeTab: tab
        })
    }

    refreshComponent = () => {
        window.history.pushState(null, '');
        let payload = [
            {
                dependingProductId: null,
                productName: "faas",
            }
        ];
        this.props.getDeveloperQuota(payload)

        this.setState({
            tableState: {
                page: this.props.location.state && this.props.location.state.page ? parseInt(this.props.location.state.page) : 0,
                pageSize: this.props.location.state && this.props.location.state.pageSize ? parseInt(this.props.location.state.pageSize) : 10,
                searchActionName: this.props.location.state && this.props.location.state.searchActionName ? this.props.location.state.searchActionName : '',
                createdBy: this.props.location.state && this.props.location.state.createdBy ? this.props.location.state.createdBy : '',
                updatedBy: this.props.location.state && this.props.location.state.updatedBy ? this.props.location.state.updatedBy : '',
                kind: this.props.location.kind && this.props.location.state.kind ? this.props.location.state.kind : '',
            },
            isOpen: false,
            faaSList: [],
            tryItOut: false,
            actionName: '',
            pathParameters: [],
            functionResponse: "{}",
            error: false,
            isInvoking: false,
            isFetchingLogs: false,
            isFetching: 1,
            faaSToBeDeleted: '',
            defaultCloneLoader: false,
            functionResponseError: '',
            filteredList: [],
            debugLogs: [],
            fullViewModal: false,
            search: "",
            remainingFaaSCount: 0,
            usedFaaSCount: 0,
            totalFaaSCount: 0,
            faaSListFilter: []
        })
    }

    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>FaaS</title>
                    <meta name="description" content="M83-FaaSFunctions" />
                </Helmet>

                <header className="content-header d-flex">
                    <div className="flex-60">
                        <h6>FaaS -
                            <span className="content-header-badge-group">
                                <span className="content-header-badge-item">Allocated <span className="badge badge-pill badge-primary">{this.state.totalFaaSCount}</span></span>
                                <span className="content-header-badge-item">Used <span className="badge badge-pill badge-success">{this.state.usedFaaSCount}</span></span>
                                <span className="content-header-badge-item">Remaining <span className="badge badge-pill badge-warning">{this.state.remainingFaaSCount}</span></span>
                            </span>
                        </h6>
                    </div>
                    <div className="flex-40 text-right">
                        <div className="content-header-group">
                            {/*<div className="search-box">
                                <span className="search-icon"><i className="far fa-search"></i></span>
                                <input type="text" className="form-control" disabled={this.state.faaSList.length === 0 || this.state.isFetching} value={this.state.search} onChange={this.filterFaaSListByName} placeholder="Search..." />
                                {this.state.search.length > 0 && <button className="search-button" onClick={() => this.removeSearchFieldText()}><i className="far fa-times"></i></button>}
                            </div>*/}
                            <button className={this.state.toggleView == true ? "btn btn-light active" : "btn btn-light"} disabled={this.state.isFetching} data-tooltip="true" data-tooltip-text="List View" data-tooltip-place="bottom" onClick={() => this.setState({ toggleView: true })}><i className="fad fa-list-ul"></i></button>
                            <button className={this.state.toggleView == false ? "btn btn-light active" : "btn btn-light"} disabled={this.state.isFetching} data-tooltip="true" data-tooltip-text="Card View" data-tooltip-place="bottom" onClick={() => this.setState({ toggleView: false })}><i className="fad fa-table"></i></button>
                            <button className="btn btn-light" data-tooltip data-tooltip-text="Refresh" disabled={this.state.isFetching} onClick={this.refreshComponent} data-tooltip-place="bottom"><i className="far fa-sync-alt"></i></button>
                            <button className="btn btn-primary" data-tooltip data-tooltip-text="Add Function" data-tooltip-place="bottom" disabled={this.state.remainingFaaSCount === 0 || this.state.isFetching} onClick={() => this.createItemOnAddButtonClick()}><i className="far fa-plus" ></i></button>
                        </div>
                    </div>
                </header>

                <div className="content-body">
                    {this.state.isFetching ?
                        <Skeleton count={9} className="skeleton-list-loader" /> :
                        <React.Fragment>
                            {this.state.faaSList.length > 0 ?
                                this.state.faaSListFilter.length > 0 ?
                                    this.state.toggleView ?
                                        <ListingTable
                                            data={this.state.faaSListFilter}
                                            columns={this.getColumns()}
                                            isLoading={this.state.isTableLoading}
                                            statusBar={this.getStatusBarClass}
                                        />
                                        :
                                        <ul className="card-view-list">
                                            {this.state.faaSList.map((item, index) =>
                                                <li key={index}>
                                                    <div className="card-view-box">
                                                        <div className="card-view-header">
                                                            {item.pathParameters && item.pathParameters.length > 0 ?
                                                                <span className="alert alert-success">Parameterized - YES</span>
                                                                :
                                                                <span className="alert alert-danger">Parameterized - NO</span>
                                                            }
                                                             <span className="alert alert-primary mr-l-7"><i className="far fa-sd-card mr-r-5"></i>{item.memory} MB</span>
                                                            <div className="dropdown">
                                                                <button className="btn-transparent btn-transparent-gray" type="button" data-toggle="dropdown">
                                                                    <i className="fas fa-ellipsis-v"></i>
                                                                </button>
                                                                <div className="dropdown-menu">
                                                                    <button className="dropdown-item"
                                                                        onClick={() => this.openModalForInvoking(item.actionName, item.pathParameters, item.description, item.id)}
                                                                        disabled={item.saveAsDraft} data-toggle="modal" data-target="#invokeModal">
                                                                        <i className="far fa-brackets-curly"></i>Invoke
                                                                    </button>
                                                                    <button className="dropdown-item"  onClick={() => this.navigateToaddOrEditCodeEngine(item.id)}>
                                                                        <i className="far fa-pencil"></i>Edit
                                                                    </button>
                                                                    <button className="dropdown-item" disabled={item.isAttached === true} 
                                                                        onClick={() => this.setState({
                                                                            deleteName: item.actionName,
                                                                            faaSToBeDeleted: item.id,
                                                                            confirmState: true
                                                                        })}>
                                                                        <i className="far fa-trash-alt"></i>Delete
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="card-view-body">
                                                            <div className="card-view-icon">
                                                                <i className="fad fa-brackets-curly"></i>
                                                            </div>
                                                            <h6><i className="far fa-address-book"></i>{item.actionName ? item.actionName : "-"}</h6>
                                                            <p><i className="far fa-file-alt"></i>{item.description ? item.description : "-"}</p>
                                                            <p><span className="badge badge-pill badge-light"><strong>Interpreter - </strong>{item.kindDisplayName}</span></p>
                                                        </div>
                                                        <div className="card-view-footer d-flex">
                                                            <div className="flex-50 p-3">
                                                                <h6>Created</h6>
                                                                <p><strong>By - </strong>{item.createdBy}<small></small></p>
                                                                <p><strong>At - </strong>{new Date(item.createdAt).toLocaleString('en-US', { createdAt: localStorage.createdAt })}</p>
                                                            </div>
                                                            <div className="flex-50 p-3">
                                                                <h6>Updated</h6>
                                                                <p><strong>By - </strong>{item.updatedBy}<small></small></p>
                                                                <p><strong>At - </strong>{new Date(item.updatedAt).toLocaleString('en-US', { createdAt: localStorage.createdAt })}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            )}
                                        </ul>
                                    : <NoDataFoundMessage /> :
                                <AddNewButton
                                    text1="No FaaS available "
                                    text2="You haven't created any FaaS yet. Please create a FaaS first."
                                    createItemOnAddButtonClick={this.createItemOnAddButtonClick}
                                    imageIcon="addFaaS.png"
                                />
                            }
                        </React.Fragment>
                    }
                </div>

                {/* invoke function modal */}
                <div className="modal fade animated slideInDown" role="dialog" id="invokeModal">
                    <div className="modal-dialog modal-lg modal-dialog-centered">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h6 className="modal-title">Function - <span>{this.state.actionName}</span>
                                    <button type="button" className="close" data-tooltip data-tooltip-text="Close" data-tooltip-place="bottom" data-dismiss="modal" onClick={this.modalCloseHandler}>
                                        <i className="far fa-times"></i>
                                    </button>
                                </h6>
                            </div>

                            <div className="modal-body">
                                <div className="action-tabs mb-3">
                                    <ul className="nav nav-tabs list-style-none d-flex">
                                        <li className={`nav-item ${this.state.activeTab === "parameters" ? "active" : ""}`} onClick={() => this.tabChangeHandler("parameters")}>
                                            <a className="nav-link">Parameters</a>
                                        </li>
                                        <li className={`nav-item ${this.state.activeTab === "result" ? "active" : ""}`} onClick={() => this.tabChangeHandler("result")}>
                                            <a className="nav-link">Result</a>
                                        </li>
                                        <li className={`nav-item ${this.state.activeTab === "logs" ? "active" : ""}`} onClick={() => this.tabChangeHandler("logs")}>
                                            <a className="nav-link">Logs</a>
                                        </li>
                                        {(this.state.activeTab === "parameters") &&
                                            <button className="btn btn-link" onClick={this.tryItOut}><i className={!this.state.tryItOut ? "fas fa-share" : "far fa-times"}></i>{!this.state.tryItOut ? "Try It Out" : "Close"}</button>
                                        }
                                    </ul>
                                </div>
                                <div className="tab-content">
                                    {this.state.activeTab === "parameters" ?
                                        <div className={`tab-pane fade ${this.state.activeTab == "parameters" ? "show active" : null}`}>
                                            <div className="function-invoke-wrapper border-none">
                                                <div className="content-table">
                                                    <table className="table table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th width="30%">Name</th>
                                                                <th width="20%">Type</th>
                                                                <th width="50%">{this.state.tryItOut ? 'Value' : 'Description'}</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td className="fw-600">X-Authorization<sup><i className="fas fa-asterisk f-8 text-red"></i></sup></td>
                                                                <td className="text-primary fw-400i">string</td>
                                                                <td>
                                                                    {this.state.tryItOut ?
                                                                        <textarea rows="5" className="form-control" readOnly value={"Bearer " + localStorage.token}></textarea> :
                                                                        <span>Bearer Access Token required for authentication.</span>
                                                                    }
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td className="fw-600">x-project-id<sup><i className="fas fa-asterisk f-8 text-red"></i></sup></td>
                                                                <td className="text-primary fw-400i">string</td>
                                                                {this.state.tryItOut ?
                                                                    <td><input className="form-control" readOnly value={localStorage.selectedProjectId} /></td> :
                                                                    <td>x-project-id</td>
                                                                }
                                                            </tr>
                                                            {this.state.pathParameters.map((parameters, index) =>
                                                                <tr key={index}>
                                                                    <td className="fw-600">{parameters.name}{parameters.required ? <sup><i className="fas fa-asterisk f-8 text-red"></i></sup> : ""}</td>
                                                                    <td className="text-primary fw-400i">{parameters.type}</td>
                                                                    <td>
                                                                        {this.state.tryItOut ? parameters.type === "Boolean" ?
                                                                            <div className="d-flex">
                                                                                <div className="flex-50 pr-2">
                                                                                    <label className="radio-button">
                                                                                        <span className="radio-button-text">True</span>
                                                                                        <input type="radio" id="required" onChange={(e) => this.tryItOutInputHandler(e, index, "Boolean")} value="true" checked={["true", true].includes(parameters.value)} />

                                                                                        <span className="radio-button-mark" />
                                                                                    </label>
                                                                                </div>
                                                                                <div className="flex-50 pl-2">
                                                                                    <label className="radio-button">
                                                                                        <span className="radio-button-text">False</span>
                                                                                        <input type="radio" id="required" onChange={(e) => this.tryItOutInputHandler(e, index, "Boolean")} value="false" checked={["false", false].includes(parameters.value)} />

                                                                                        <span className="radio-button-mark" />
                                                                                    </label>
                                                                                </div>
                                                                                {parameters.errorMessage &&
                                                                                    <div className="flex-item fx-b100"><h6 className="text-red">Please fill out this field.</h6></div>
                                                                                }
                                                                            </div> :
                                                                            parameters.type === "map" ?
                                                                                <div className="json-input-box">
                                                                                    <JSONInput
                                                                                        theme="light_mitsuketa_tribute"
                                                                                        placeholder={parameters.value ? JSON.parse(parameters.value) : parameters.value}
                                                                                        colors={{
                                                                                            default: "#212F3D",
                                                                                            number: "#2ECC71",
                                                                                            string: "#ab210f",
                                                                                            keys: "#550fab",
                                                                                        }}
                                                                                        onChange={(e) => this.tryItOutMapHandler(e.jsObject, index)}
                                                                                    />
                                                                                    {parameters.errorMessage &&
                                                                                        <div className="flex-item fx-b100"><h6 className="text-red">Please fill out this field.</h6></div>
                                                                                    }
                                                                                </div>
                                                                                :
                                                                                <React.Fragment>
                                                                                    <input className="form-control" required={parameters.required} id={parameters.description}
                                                                                        onChange={(event) => this.tryItOutInputHandler(event, index, parameters.type)}
                                                                                        value={parameters.value}
                                                                                    />
                                                                                    {parameters.errorMessage ? <h6 className="text-red">Please fill out this field.</h6> : null}
                                                                                </React.Fragment> :
                                                                            <span>{parameters.description}</span>
                                                                        }
                                                                    </td>
                                                                </tr>
                                                            )}
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div> :
                                        this.state.activeTab === "result" ?
                                            <div className={`tab-pane fade ${this.state.activeTab == "result" ? "show active" : null}`}>
                                                <div className="card function-invoke-wrapper">
                                                    {this.state.functionResponse ?
                                                        <div className="json-input-box h-100 border-none">
                                                            <JSONInput viewOnly={true} placeholder={this.state.functionResponse} />
                                                        </div> :
                                                        this.state.isInvoking ?
                                                            <div className="card-loader">
                                                                <i className="fad fa-sync-alt fa-spin"></i>
                                                            </div> :
                                                            <div className="card-message">
                                                                <p className="text-red">{this.state.functionResponseError}</p>
                                                            </div>
                                                    }
                                                    <button className="btn btn-light text-gray" onClick={() => { this.setState({ fullViewModal: "Result" }) }}>
                                                        <i className="fas fa-expand"></i>
                                                    </button>
                                                </div>
                                            </div> :
                                            this.state.activeTab === "logs" ?
                                                <div className={`tab-pane fade ${this.state.activeTab == "logs" ? "show active" : null}`}>
                                                    <div className="card function-invoke-wrapper bg-dark">
                                                        {this.state.debugLogs.length > 0 ?
                                                            <div className="card-body">
                                                                {this.state.debugLogs.map((log, logIndex) => (
                                                                    <pre className="text-white f-12" key={logIndex}>{log}</pre>
                                                                ))}
                                                            </div> :
                                                            this.state.isFetchingLogs ?
                                                                <div className="card-loader">
                                                                    <i className="fad fa-sync-alt fa-spin text-white"></i>
                                                                </div> :
                                                                <div className="card-message">
                                                                    <p className="text-red">{this.state.debugLogsFailure}</p>
                                                                </div>
                                                        }
                                                        <button className="btn btn-light" onClick={() => { this.setState({ fullViewModal: "Logs" }) }}>
                                                            <i className="fas fa-expand"></i>
                                                        </button>
                                                    </div>
                                                </div> : ""
                                    }
                                </div>
                            </div>

                            <div className="modal-footer">
                                <button type="button" className="btn btn-success" onClick={this.invokeFaasFunction} disabled={!this.state.tryItOut}>Execute</button>
                                <button type="button" className="btn btn-dark" data-dismiss="modal" onClick={this.modalCloseHandler}>Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
                {/* end invoke function modal */}

                {/* log full modal */}
                {this.state.fullViewModal &&
                    <div className="modal d-block animated slideInDown">
                        <div className="modal-dialog modal-full">
                            <div className="modal-content">
                                <div className="modal-body p-2">
                                    {this.state.fullViewModal === "Logs" ?
                                        <div className="modal-full-editor bg-dark p-3">
                                            <button type="button" className="btn btn-light" onClick={() => { this.setState({ fullViewModal: false }) }}>
                                                <i className="fas fa-compress"></i>
                                            </button>
                                            {this.state.debugLogs.length > 0 ?
                                                this.state.debugLogs.map((log, logIndex) => (
                                                    <pre className="text-white" key={logIndex}>{log}</pre>
                                                )) :
                                                this.state.isFetchingLogs ?
                                                    <div className="inner-loader-wrapper h-100">
                                                        <div className="inner-loader-content">
                                                            <i className="fad fa-sync-alt fa-spin text-white"></i>
                                                        </div>
                                                    </div> :
                                                    <div className="inner-message-wrapper h-100">
                                                        <div className="inner-message-content">
                                                            <i className="fad fa-exclamation-triangle text-red"></i>
                                                            <h6 className="text-red">{this.state.debugLogsFailure}</h6>
                                                        </div>
                                                    </div>
                                            }
                                        </div>
                                        :
                                        <div className="modal-full-editor">
                                            <button type="button" className="btn btn-light text-dark" onClick={() => { this.setState({ fullViewModal: false }) }}>
                                                <i className="fas fa-compress"></i>
                                            </button>
                                            {this.state.functionResponse ?
                                                <div className="json-input-box h-100">
                                                    <JSONInput viewOnly={true} placeholder={this.state.functionResponse} />
                                                </div> :
                                                this.state.isInvoking ?
                                                    <div className="inner-loader-wrapper h-100">
                                                        <div className="inner-loader-content">
                                                            <i className="fad fa-sync-alt fa-spin"></i>
                                                        </div>
                                                    </div> :
                                                    <div className="inner-message-wrapper h-100">
                                                        <div className="inner-message-content">
                                                            <i className="fad fa-exclamation-triangle text-red"></i>
                                                            <h6 className="text-red">{this.state.functionResponseError}</h6>
                                                        </div>
                                                    </div>
                                            }
                                        </div>
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                }
                {/* end log full modal */}

                {this.state.confirmState &&
                    <ConfirmModel
                        status={"delete"}
                        deleteName={this.state.deleteName}
                        confirmClicked={() => {
                            this.props.deleteFaaS(this.state.faaSToBeDeleted)
                            this.setState({
                                isFetching: true,
                                confirmState: false,
                                isOpen: false
                            })
                        }}
                        cancelClicked={() => {
                            this.cancelClicked()
                        }}
                    />
                }

                {this.state.isOpen &&
                    <NotificationModal
                        type={this.state.type}
                        message2={this.state.message2}
                        onCloseHandler={this.onCloseHandler}
                    />
                }

            </React.Fragment>
        );
    }
}

FaaSList.propTypes = {
    dispatch: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
    getFassListSuccess: SELECTORS.getFassListSuccess(),
    getFassListFailure: SELECTORS.getFassListFailure(),
    getdeleteFaasSuccess: SELECTORS.getdeleteFaasByIdSuccess(),
    getdeleteFaasFailure: SELECTORS.getdeleteFaasByIdFailure(),
    getDeleteFaas: SELECTORS.getDeleteFaas(),
    invokeFaasFunctionSuccess: SELECTORS.invokeFaasFunctionSuccess(),
    invokeFaasFunctionFailure: SELECTORS.invokeFaasFunctionFailure(),
    cloneFaaSSuccess: SELECTORS.cloneFaaSSuccess(),
    cloneFaaSFailure: SELECTORS.cloneFaaSFailure(),
    debugLogsSuccess: SELECTORS.getDebugLogsSuccess(),
    debugLogsFailure: SELECTORS.getDebugLogsFailure(),
    developerQuotaSuccess: SELECTORS.getDeveloperQuotaSuccess(),
    developerQuotaFailure: SELECTORS.getDeveloperQuotaFailure(),
});

function mapDispatchToProps(dispatch) {
    return {
        dispatch,
        getFaaSList: () => dispatch(ACTIONS.getFaaSList()),
        deleteFaaS: (id) => dispatch(ACTIONS.deleteFaaS(id)),
        invokeFaasFunction: (payload, actionName) => dispatch(ACTIONS.invokeFaasFunction(payload, actionName)),
        cloneFaaSFunction: (id) => dispatch(ACTIONS.cloneFaaSFunction(id)),
        resetToInitialState: () => dispatch(ACTIONS.resetToInitialState()),
        getDebugLogs: (activationId) => dispatch(ACTIONS.getDebugLogs(activationId)),
        getDeveloperQuota: (payload) => dispatch(ACTIONS.getDeveloperQuota(payload))
    };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "faaSList", reducer });
const withSaga = injectSaga({ key: "faaSList", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(FaaSList);
