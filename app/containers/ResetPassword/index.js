/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Link } from 'react-router-dom';
import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import * as SELECTORS from './selectors';
import * as ACTIONS from './actions'
import reducer from './reducer';
import saga from './saga';
import Loader from '../../components/Loader/Loadable'
import NotificationModal from '../../components/NotificationModal/Loadable'

/* eslint-disable react/prefer-stateless-function */
export class ResetPassword extends React.Component {
    state = {
        email: "",
        isLoading: false,
        payload: {
            password: "",
            confPassword: ""
        },
        tenantDetails: {},
        isFetching: false,
        isFetchingTenantDetails: true
    };

    componentDidMount() {
        this.props.resetToIntialState();
        if (localStorage.tenantType != "MAKER") {
            this.props.getTenantDetails();
        }
        if (window.location.pathname === "/resetCredential") {
            let query = new URLSearchParams(this.props.location.search);
            let verifyJson = {
                'token': query.get('token'),
                'tokenId': query.get('id')
            }
            this.props.verifyToken(verifyJson)
            this.setState({
                isLoading: true
            })
        }
    }

    inputChangeHandler = (event) => {
        this.setState({
            email: event.target.value
        })
    }

    resetPasswordHandler = (event) => {
        event.preventDefault();
        this.setState({
            isFetching: true
        }, () => {
            this.props.onForgotPassword(this.state.email)
        })
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.changePasswordSuccess && nextProps.changePasswordSuccess !== this.props.changePasswordSuccess) {
            this.setState({
                modalType: "success",
                openModal: true,
                message2: "Password changed successfully."
            })
        }

        if (nextProps.changePasswordError && nextProps.changePasswordError !== this.props.changePasswordError) {
            this.setState({
                modalType: "error",
                openModal: true,
                message2: nextProps.changePasswordError.error,
                isFetching: false
            })
        }

        if (nextProps.getTenantDetailsSuccess && nextProps.getTenantDetailsSuccess !== this.props.getTenantDetailsSuccess) {
            this.setState({
                tenantDetails: nextProps.getTenantDetailsSuccess,
                isFetchingTenantDetails: false
            })
        }

        if (nextProps.getTenantDetailsError && nextProps.getTenantDetailsError !== this.props.getTenantDetailsError) {
            this.setState({
                modalType: "error",
                openModal: true,
                message2: nextProps.getTenantDetailsError,
                isFetchingTenantDetails: false
            })
        }

        if (nextProps.resetPasswordSuccess && nextProps.resetPasswordSuccess != this.props.resetPasswordSuccess) {
            this.setState({
                modalType: 'success',
                openModal: true,
                message2: "An email has been sent to your email address with a link you can use to reset your password."
            })
        }

        if (nextProps.resetPasswordError && nextProps.resetPasswordError != this.props.resetPasswordError) {
            this.setState({
                modalType: 'error',
                openModal: true,
                message2: nextProps.resetPasswordError.errorMessage,
                isFetching: false
            })
        }

        if (nextProps.accessGrant && nextProps.accessGrant != this.props.accessGrant) {
            this.setState({
                isLoading: false,
                showChangePassword: true
            })
        }

        if (nextProps.accessDenied && nextProps.accessDenied != this.props.accessDenied) {
            this.setState({
                isLoading: false,
                showAccessDenied: true,
                showChangePassword: false
            })
        }
    }

    onCloseHandler = () => {
        this.setState({
            openModal: false,
            message2: ''
        }, () => this.state.modalType === "success" && this.props.history.push('/login'))
    }

    changePasswordHandler = (event) => {
        event.preventDefault();
        let payload = JSON.parse(JSON.stringify(this.state.payload)),
            query = new URLSearchParams(this.props.location.search);

        let sendingJson = {
            'newPassword': payload.password,
            'token': query.get('token'),
            'tokenId': query.get('id'),
        }
        this.setState({
            isFetching: true
        }, () => {
            this.props.resetpasswordhandler(sendingJson);
        })
    }

    passwordChangeHandler = (event) => {
        let payload = JSON.parse(JSON.stringify(this.state.payload))
        payload[event.target.id] = event.target.value
        this.setState({ payload })
    }

    formValidate = () => {
        const regex = /^(?=.*?[a-z])(?=.*[A-Z])(?=.*?[^\w\s]).{8,}$/
        if (!regex.test(this.state.payload.confPassword) || this.passwordsMismatch()) {
            return true
        }
        return false
    }

    passwordsMismatch = () => {
        return this.state.payload.confPassword && this.state.payload.password && this.state.payload.confPassword !== this.state.payload.password
    }

    getBorderColor = (id) => {
        const regex = /^(?=.*?[a-z])(?=.*[A-Z])(?=.*?[^\w\s]).{8,}$/
        let styles = { borderColor: 'red' }
        if (id === "password") {
            if (this.state.payload.password && !regex.test(this.state.payload.password))
                return styles
            return null
        }
        else if (id === "confPassword") {
            if (this.state.payload.confPassword && !regex.test(this.state.payload.confPassword))
                return styles
            return null
        }
    }

    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>Reset Password</title>
                    <meta name="description" content="M83-ResetPassword" />
                </Helmet>

                {this.state.isLoading ?
                    <Loader /> :
                    localStorage.tenantType == "MAKER" ?
                        <div className="login-outer-box" id="makerLogin">
                            <div className="login-image-content">
                                <div className="login-image-box">
                                    <img src="https://content.iot83.com/m83/tenant/welcome.png" />
                                </div>
                                <h1>Welcome to iFlex83</h1>
                                <h5>Connect . Configure . Visualize</h5>
                                <p>A powerful and cost effective path to create dynamic IoT Applications. ​</p>
                                <p>Fast new and legacy device on-boarding.</p>
                                <p>Few clicks and your dashboards ready.</p>
                                <p>Not a single line of code.</p>
                                <p>Security, reliability and scalability assured.</p>
                            </div>

                            <div className="login-form-wrapper">
                                <div className="login-logo">
                                    <img src="https://content.iot83.com/m83/misc/iFlexLogo.png" />
                                </div>

                                {this.state.showChangePassword ?
                                    <div className="login-form-box">
                                        {this.state.isFetching ?
                                            <div className="login-form-loader">
                                                <div id="ld3">
                                                    <div></div>
                                                    <div></div>
                                                    <div></div>
                                                    <div></div>
                                                </div>
                                            </div> :
                                            <form onSubmit={this.changePasswordHandler}>
                                                <div className="login-form-text">
                                                    <h5>Change Password</h5>
                                                    <p>It must contain at least 1 lowercase character, 1 uppercase character, 1 numeric character, 1 special character. It must be of min 8 characters.</p>
                                                </div>

                                                <div className="form-group">
                                                    <input type="password" id="password" placeholder="New Password" className="form-control" style={this.getBorderColor("password")} value={this.state.payload.password} onChange={this.passwordChangeHandler} required />
                                                    <span className="form-control-icon"><i className="fad fa-lock-alt"></i></span>
                                                </div>

                                                <div className="form-group">
                                                    <input type="password" id="confPassword" placeholder="Confirm Password" className="form-control" style={this.getBorderColor("confPassword")} value={this.state.payload.confPassword} onChange={this.passwordChangeHandler} required />
                                                    <span className="form-control-icon"><i className="fad fa-lock"></i></span>
                                                </div>

                                                <div className="form-group">
                                                    <button type="submit" name="button" disabled={this.formValidate()} className="btn btn-primary">Reset</button>
                                                    <Link className="" to="/"><i className="fas fa-angle-double-left mr-r-5"></i>Back to login</Link>
                                                </div>

                                                {this.state.errors && this.state.errors.matchError ? <div className="login-form-error">Both the password should be same.</div> : ""}
                                                {this.passwordsMismatch() && <div className="login-form-error">Passwords do not match.</div>}
                                            </form>
                                        }
                                    </div> :
                                    <div className="login-form-box">
                                        {this.state.isFetching ?
                                            <div className="login-form-loader">
                                                <div id="ld3">
                                                    <div></div>
                                                    <div></div>
                                                    <div></div>
                                                    <div></div>
                                                </div>
                                            </div> :
                                            <form onSubmit={this.resetPasswordHandler}>
                                                <div className="login-form-text">
                                                    <h5>Reset Password</h5>
                                                    <p>Enter your email address associated and we will send you an email with link to reset your password.</p>
                                                </div>

                                                {this.state.showAccessDenied &&
                                                    <div className="alert alert-danger">
                                                        <i className="fad fa-exclamation-triangle mr-r-5"></i>Your password reset link has expired.
                                                </div>
                                                }

                                                <div className="form-group">
                                                    <input type="email" name="email" placeholder="Email" className="form-control" value={this.state.email} onChange={this.inputChangeHandler} required />
                                                    <span className="form-control-icon"><i className="fad fa-envelope"></i></span>
                                                </div>
                                                <div className="form-group">
                                                    <button type="submit" name="button" className="btn btn-primary">Continue</button>
                                                    <Link className="" to="/"><i className="fas fa-angle-double-left mr-r-5"></i>Back to login</Link>
                                                </div>
                                            </form>
                                        }
                                    </div>
                                }
                            </div>
                        </div>
                        :
                        <div className="login-outer-box" id="flexLogin" style={{ backgroundImage: `url(https://content.iot83.com/m83/misc/flexLoginBg.png)` }}>
                            <div className="d-flex h-100">
                                <div className="flex-70 h-100">
                                    <div className="login-outer-image">
                                        {this.state.isFetchingTenantDetails ?
                                            <i className="fad fa-spinner fa-spin"></i> :
                                            <img src={this.state.tenantDetails.brandingImageUrl ? this.state.tenantDetails.brandingImageUrl : "https://content.iot83.com/m83/misc/flexLoginImage_new.png"} />
                                        }
                                        {/*<div className="login-outer-content">
                                            <div className="mr-b-3">
                                                <p>A powerful path to dynamic applications</p>
                                                <p>Cost Effective IoT application  solutions</p>
                                                <p>Click - Through dashboard creation</p>
                                            </div>
                                            <div>
                                                <p>Security, reliability and scalability assured</p>
                                                <p>Low - Code workflow to "Add the Magic"</p>
                                                <p>Fast and new legacy device on-boarding.</p>
                                            </div>
                                        </div>*/}
                                    </div>
                                </div>

                                <div className="flex-30 bg-white position-relative">
                                    <div className="login-logo">
                                        {this.state.isFetchingTenantDetails ?
                                            <i className="fad fa-spinner text-theme fa-spin f-18"></i> :
                                            <img src={this.state.tenantDetails.logoImage ? this.state.tenantDetails.logoImage : "https://content.iot83.com/m83/misc/flexLogo.png"} />
                                        }
                                    </div>

                                    {this.state.showChangePassword ?
                                        <div className="login-form-box">
                                            <div className="login-form-text">
                                                <h1>Change Password</h1>
                                                <p>It must contain at least 1 lowercase character, 1 uppercase character, 1 numeric character, 1 special character. It must be of min 8 characters.</p>
                                            </div>
                                            {this.state.isFetching || this.state.isFetchingTenantDetails ?
                                                <div className="login-form-loader">
                                                    <div id="ld3">
                                                        <div></div>
                                                        <div></div>
                                                        <div></div>
                                                        <div></div>
                                                    </div>
                                                </div> :
                                                <form onSubmit={this.changePasswordHandler}>
                                                    <div className="form-group">
                                                        <input type="password" id="password" placeholder="New Password" className="form-control" style={this.getBorderColor("password")} value={this.state.payload.password} onChange={this.passwordChangeHandler} required />
                                                        <span className="form-control-icon"><i className="fad fa-lock-alt"></i></span>
                                                    </div>

                                                    <div className="form-group">
                                                        <input type="password" id="confPassword" placeholder="Confirm Password" className="form-control" style={this.getBorderColor("confPassword")} value={this.state.payload.confPassword} onChange={this.passwordChangeHandler} required />
                                                        <span className="form-control-icon"><i className="fad fa-lock"></i></span>
                                                    </div>

                                                    <div className="form-group">
                                                        <button type="submit" name="button" disabled={this.formValidate()} className="btn btn-primary">Reset</button>
                                                        <Link className="" to="/"><i className="fas fa-angle-double-left mr-r-5"></i>Back to login</Link>
                                                    </div>

                                                    {this.state.errors && this.state.errors.matchError ? <div className="login-form-error position-absolute">Both the password should be same.</div> : ""}
                                                    {this.passwordsMismatch() && <div className="login-form-error position-absolute">Passwords do not match.</div>}
                                                </form>
                                            }
                                        </div> :
                                        <div className="login-form-box">
                                            <div className="login-form-text">
                                                <h1>Reset Password</h1>
                                                <p>Enter your email address associated and we will send you an email with link to reset your password.</p>
                                            </div>
                                            {this.state.isFetching ?
                                                <div className="login-form-loader">
                                                    <div id="ld3">
                                                        <div></div>
                                                        <div></div>
                                                        <div></div>
                                                        <div></div>
                                                    </div>
                                                </div> :
                                                <form onSubmit={this.resetPasswordHandler}>
                                                    {this.state.showAccessDenied &&
                                                        <div className="alert alert-danger">
                                                            <i className="fad fa-exclamation-triangle mr-r-5"></i>Your password reset link has expired.
                                                    </div>
                                                    }

                                                    <div className="form-group">
                                                        <input type="email" name="email" placeholder="Email" className="form-control" value={this.state.email} onChange={this.inputChangeHandler} required />
                                                        <span className="form-control-icon"><i className="fad fa-envelope"></i></span>
                                                    </div>

                                                    <div className="form-group">
                                                        <button type="submit" name="button" className="btn btn-primary">Continue</button>
                                                        <Link className="" to="/"><i className="fas fa-angle-double-left mr-r-5"></i>Back to login</Link>
                                                    </div>
                                                </form>
                                            }
                                        </div>
                                    }
                                </div>
                            </div>
                        </div>
                }

                {this.state.openModal &&
                    <NotificationModal
                        type={this.state.modalType}
                        message2={this.state.message2}
                        onCloseHandler={this.onCloseHandler}
                    />
                }
            </React.Fragment>
        );
    }
}

ResetPassword.propTypes = {
    dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
    resetPasswordSuccess: SELECTORS.getResetPasswordSuccess(),
    resetPasswordError: SELECTORS.getResetPasswordError(),
    changePasswordSuccess: SELECTORS.getChangePasswordSuccess(),
    changePasswordError: SELECTORS.getChangePasswordError(),
    getTenantDetailsSuccess: SELECTORS.getTenantDetailsSuccess(),
    getTenantDetailsError: SELECTORS.getTenantDetailsError(),
    isFetching: SELECTORS.getIsFetching(),
    accessGrant: SELECTORS.getAccessGrant(),
    accessDenied: SELECTORS.getAccessDenied()
});

function mapDispatchToProps(dispatch) {
    return {
        dispatch,
        onForgotPassword: (email) => dispatch(ACTIONS.onForgotPassword(email)),
        resetpasswordhandler: (data) => dispatch(ACTIONS.resetpasswordhandler(data)),
        verifyToken: (verifyJson) => dispatch(ACTIONS.verifyToken(verifyJson)),
        getTenantDetails: () => dispatch(ACTIONS.getTenantDetails()),
        resetToIntialState: () => dispatch(ACTIONS.resetToIntialState()),
    };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'resetPassword', reducer });
const withSaga = injectSaga({ key: 'resetPassword', saga });

export default compose(
    withReducer,
    withSaga,
    withConnect,
)(ResetPassword);
