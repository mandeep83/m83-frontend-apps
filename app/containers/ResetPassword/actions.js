/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import * as CONSTANTS from './constants';

export function defaultAction() {
  return {
    type: CONSTANTS.DEFAULT_ACTION,
  };
}



export function onForgotPassword(email) {
  let payload = {
    username: email
  }
  return {
    type: CONSTANTS.FORGOT_PASSWORD_INITIATED,
    payload,
  };
}


export function resetpasswordhandler(data) {

  return {
    type: CONSTANTS.CHANGE_PASSSWORD_REQUEST,
    data: data
  };
}

export function resetToIntialState() {
  return {
    type: CONSTANTS.RESET_TO_INTIAL_STATE,
  };
}

export function verifyToken(data) {
  return {
    type: CONSTANTS.ACCESS_PERMISSION,
    data: data
  };
}

export function getTenantDetails() {
  return {
    type: CONSTANTS.GET_TENANT_DETAILS
  };
}