/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

export const DEFAULT_ACTION = 'app/ResetPassword/DEFAULT_ACTION';
// export const RESET_INITIATED = 'app/ResetPassword/RESET_INITIATED';
// export const RESET_SUCCESSFULLY = 'app/ResetPassword/RESET_SUCCESSFULLY';
// export const RESET_FAILED = 'app/ResetPassword/RESET_FAILED';


export const FORGOT_PASSWORD_INITIATED = 'M83/ResetPassword/FORGOT_PASSWORD_INITIATED';
export const FORGOT_PASSWORD_SUCCESS = 'M83/ResetPassword/FORGOT_PASSWORD_SUCCESS';
export const FORGOT_PASSWORD_FAILURE = 'M83/ResetPassword/FORGOT_PASSWORD_FAILURE';

export const RESET_TO_INTIAL_STATE = 'app/ResetPassword/RESET_TO_INTIAL_STATE';

export const CHANGE_PASSSWORD_REQUEST = 'app/ResetPassword/CHANGE_PASSSWORD_REQUEST';
export const CHANGE_PASSSWORD_SUCCESS = 'app/ResetPassword/CHANGE_PASSSWORD_SUCCESS';
export const CHANGE_PASSSWORD_FAILURE = 'app/ResetPassword/CHANGE_PASSSWORD_FAILURE';

export const ACCESS_PERMISSION = 'app/ResetPassword/ACCESS_PERMISSION';
export const ACCESS_DENIED = 'app/ResetPassword/ACCESS_DENIED';
export const ACCESS_GRANT = 'app/ResetPassword/ACCESS_GRANT';

export const GET_TENANT_DETAILS = 'app/ResetPassword/GET_TENANT_DETAILS';
export const GET_TENANT_DETAILS_SUCCESS = 'app/ResetPassword/GET_TENANT_DETAILS_SUCCESS';
export const GET_TENANT_DETAILS_ERROR = 'app/ResetPassword/GET_TENANT_DETAILS_ERROR';