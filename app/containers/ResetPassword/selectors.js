/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the resetPassword state domain
 */

const selectResetPasswordDomain = state =>
  state.get('resetPassword', initialState);

export const getResetPasswordSuccess = () =>
  createSelector(selectResetPasswordDomain, substate => substate.forgotPasswordSuccess);

export const getResetPasswordError = () =>
  createSelector(selectResetPasswordDomain, substate => substate.forgotPasswordError);

export const getChangePasswordSuccess = () =>
  createSelector(selectResetPasswordDomain, substate => substate.changePasswordSuccess);

export const getChangePasswordError = () =>
  createSelector(selectResetPasswordDomain, substate => substate.changePasswordError);

export const getAccessGrant = () =>
  createSelector(selectResetPasswordDomain, substate => substate.accessGrant);

export const getAccessDenied = () =>
  createSelector(selectResetPasswordDomain, substate => substate.accessDenied);

export const getTenantDetailsSuccess = () =>
  createSelector(selectResetPasswordDomain, substate => substate.getTenantDetailsSuccess);

export const getTenantDetailsError = () =>
  createSelector(selectResetPasswordDomain, substate => substate.getTenantDetailsError);

const isFetchingState = state => state.get('loader');

export const getIsFetching = () =>
  createSelector(isFetchingState, fetchingState => fetchingState.get('isFetching'));
export { selectResetPasswordDomain };
