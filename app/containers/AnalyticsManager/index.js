/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * AnalyticsManager
 *
 */

import React from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {Helmet} from "react-helmet";
import {FormattedMessage} from "react-intl";
import {createStructuredSelector} from "reselect";
import {compose} from "redux";

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import makeSelectAnalyticsManager from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import messages from "./messages";

/* eslint-disable react/prefer-stateless-function */
export class AnalyticsManager extends React.Component {
    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>AnalyticsManager</title>
                    <meta name="description" content="Description of AnalyticsManager"/>
                </Helmet>
    
                <header className="content-header d-flex">
                    <div className="flex-60">
                        <h6>Analytics Manager</h6>
                    </div>
                    <div className="flex-40 text-right">
                        <div className="content-header-group">
                            <button className="btn btn-light" data-tooltip data-tooltip-text="Refresh" data-tooltip-place="bottom">
                                <i className="far fa-sync-alt"></i>
                            </button>
                        </div>
                    </div>
                </header>
    
                <div className="content-body">
                    <div className="card">
                        <div className="card-body">
                            <div className="flow-wrapper">
                                <span className="flow-wrapper-state bg-green"></span>
                                {/*<span className="flow-wrapper-state bg-red"></span>*/}
                                <p className="text-theme m-0">a61a844b542e4dbf80a8a678a2684cc0<span>Created 12 mins ago</span></p>
                                <h4 className="text-dark-theme fw-700">Analytics Manager</h4>
                                <a className="f-12 mb-2 d-block" href="" target="_blank"><i className="far fa-link mr-2"></i>https://a61a844b542e4dbf80a8a678a2684cc0.flows-iot83.com/</a>
                                <h5 className="text-content f-12 m-0">
                                    <strong className="text-green"><i className="fad fa-spinner fa-spin mr-2"></i>Running</strong> - Started a min ago
                                    {/*<strong className="text-red"><i className="fad fa-stop mr-2"></i>Stopped</strong> - Stopped a min ago*/}
                                </h5>
                                <div className="button-group">
                                    <button type="button" className="btn btn-primary"><i className="far fa-pencil mr-2"></i>Launch</button>
                                    <button type="button" className="btn btn-success"><i className="far fa-play mr-2"></i>Start</button>
                                    <button type="button" className="btn btn-warning"><i className="far fa-stop mr-2"></i>Stop</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="card">
                        <div className="card-header">
                            <h6>Flow Manager History <span className="float-right f-12 text-content">Total Running Time : <strong className="text-green">20 mins</strong></span></h6>
                        </div>
                        <div className="card-body">
                            <div className="content-table">
                                <table className="table table-bordered mb-0">
                                    <thead>
                                    <tr>
                                        <th>Start time</th>
                                        <th>End Time</th>
                                        <th>Duration</th>
                                        <th>Started By</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>3:12:25 PM</td>
                                        <td>3:12:25 PM</td>
                                        <td>10 mins</td>
                                        <td>Nisha Tanwar</td>
                                    </tr>
                                    <tr>
                                        <td>3:12:25 PM</td>
                                        <td>3:12:25 PM</td>
                                        <td>10 mins</td>
                                        <td>Nisha Tanwar</td>
                                    </tr>
                                    <tr>
                                        <td>3:12:25 PM</td>
                                        <td>3:12:25 PM</td>
                                        <td>10 mins</td>
                                        <td>Nisha Tanwar</td>
                                    </tr>
                                    <tr>
                                        <td>3:12:25 PM</td>
                                        <td>3:12:25 PM</td>
                                        <td>10 mins</td>
                                        <td>Nisha Tanwar</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            {/*<div className="inner-loader-wrapper" style={{height: 300}}>
                                <div className="inner-loader-content">
                                    <i className="fad fa-sync-alt fa-spin"></i>
                                </div>
                            </div>
                            <div className="inner-message-wrapper" style={{height: 300}}>
                                <div className="inner-message-content">
                                    <i className="fad fa-file-exclamation"></i>
                                    <h6>There is no data to display.</h6>
                                </div>
                            </div>*/}
                        </div>
                    </div>
                </div>
                
            </React.Fragment>
        );
    }
}

AnalyticsManager.propTypes = {
    dispatch: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
    analyticsmanager: makeSelectAnalyticsManager()
});

function mapDispatchToProps(dispatch) {
    return {
        dispatch
    };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({key: "analyticsManager", reducer});
const withSaga = injectSaga({key: "analyticsManager", saga});

export default compose(
    withReducer,
    withSaga,
    withConnect
)(AnalyticsManager);
