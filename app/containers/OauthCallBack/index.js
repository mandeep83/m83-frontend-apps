/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import React from "react";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";
import { compose } from "redux";
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import {getAllProjects} from "./actions";
import {projectsSuccess, projectsFailure} from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import axios from 'axios';
import Loader from '../../components/Loader'
import jwt_decode from 'jwt-decode';
import NotificationModal from '../../components/NotificationModal/Loadable';

/* eslint-disable react/prefer-stateless-function */
export class OauthCallBack extends React.Component {

  state = {
    isOpen: false
  }

  componentDidMount() {
    const self = this;
    const hash = this.props.location.hash;
    if (hash.split('&')[1].split('state=')[1] === 'AmQFtlXPHU7DYi1XLS8ElBmjuYwBtBdVF74fb777e093f647b9a0ec8d6ff3ff2bd6') {
      axios.get(`${window.API_URL}api/unauth/v1/oauth2/token?code=${window.location.hash.substr(6)}&redirect_uri=${window.location.origin}/oauthCallBack`, {
        headers: {
          'apiKey': 'bTgzLWVsYXN0aWNzZWFyY2gtc2VjcmV0a2V5',
          'isSecure': true,
        },
      }).then(function (response) {
        if (response.status === 200) {
          const token = response.data.data.accessToken;
          let name = jwt_decode(token).name ? jwt_decode(token).name : "Admin";
          localStorage['Username'] = name;
          localStorage['userId'] = jwt_decode(token).jti;
          localStorage['token'] = token;
          localStorage['tenant'] = jwt_decode(token).tenant;
          localStorage['role'] = jwt_decode(token).role;
          localStorage.isVerified = response.data.data.verify;
          if ( jwt_decode(token).role === 'SYSTEM_ADMIN') {
            self.props.history.push('/');
          } else {
            self.props.getAllProjects(token);
          }
        } else {
          self.setState({
            isCodeFailure: true,
            codeFailureMessage: response.data.message,
          });
        }
      }).catch(function (error) {
        if (error.response) {
          if (error.response.status == 400 || error.response.status == 403) {
            self.props.history.push(`login?errorCode=${error.response.status}`);
          }
        }
      });
    } else {
      self.setState({
        isFailure: true,
        error: 'CSRF attack detected.',
        showLogin: true,
      });
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.projectsSuccess && nextProps.projectsSuccess !== this.props.projectsSuccess) {
      localStorage['myProjects'] = JSON.stringify(nextProps.projectsSuccess);
      this.props.history.push('/projects');
    }

    if (nextProps.projectsFailure && nextProps.projectsFailure !== this.props.projectsFailure) {

    }
  }

  onCloseHandler = () => {
    this.setState({
        isOpen: false,
        modalType: "",
        message2: "",
    })
}

  render() {
    return (
    <React.Fragment>
         <Loader />
         { this.state.isOpen &&
            <NotificationModal
                type={this.state.modalType}
                message2={this.state.message2}
                onCloseHandler={this.onCloseHandler}
            />
          }
    </React.Fragment>
    )
   
  }
}

const mapStateToProps = createStructuredSelector({
  projectsSuccess: projectsSuccess(),
  projectsFailure: projectsFailure(),
});


export function mapDispatchToProps(dispatch) {
  return {
    getAllProjects: (accessToken) => dispatch(getAllProjects(accessToken)),
  };
}


const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

const withReducer = injectReducer({ key: "oauthCallBack", reducer });
const withSaga = injectSaga({ key: "oauthCallBack", saga });

export default compose(
  withReducer,
  withSaga,
  withConnect
)(OauthCallBack);
