/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * ManageDockerCred
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { FormattedMessage } from "react-intl";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import makeSelectManageDockerCred from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import messages from "./messages";
import ListingTable from "../../components/ListingTable/Loadable";
import SlidingPane from "react-sliding-pane";

/* eslint-disable react/prefer-stateless-function */
export class ManageDockerCred extends React.Component {
    state = {
        filteredList: [{
            name: "iot83/pymongo:pymongo",
            id: "4ee7e3c63dcc458999adec632f87077b",
            url: "https://platform.dev-iot83.com/",
            username: "Nisha Tanwar",
            password: "**********",
            createdBy: "Nisha Tanwar",
            createdAt: "12/29/2020, 10:45:18 AM",
            updatedBy: "Nisha Tanwar",
            updatedAt: "12/29/2020, 10:45:18 AM",
        }, {
            name: "iot83/pymongo:pymongo",
            id: "4ee7e3c63dcc458999adec632f87077b",
            url: "https://platform.dev-iot83.com/",
            username: "Nisha Tanwar",
            password: "**********",
            createdBy: "Nisha Tanwar",
            createdAt: "12/29/2020, 10:45:18 AM",
            updatedBy: "Nisha Tanwar",
            updatedAt: "12/29/2020, 10:45:18 AM",
        }],
        toggleView: true,
    }

    getColumns = () => {
        return [
            {
                Header: "Name", width: 15,
                cell: (row) => (
                    <h6 className="text-theme fw-600">{row.name}</h6>
                )
            },
            { Header: "Username", width: 12.5, accessor: "username" },
            {
                Header: "Password", width: 12.5,
                cell: (row) => (
                    <h6>{row.password}</h6>
                )
            },
            {
                Header: "URL", width: 20,
                cell: (row) => (
                    <a className="text-truncate d-block f-12" href={row.url} target="_blank">{row.url}</a>
                )
            },
            { Header: "Created", width: 15, accessor: "createdAt" },
            { Header: "Updated", width: 15, accessor: "updatedAt" },
            {
                Header: "Actions", width: 10, cell: (row) => (
                    <div className="button-group" >
                        <button className="btn-transparent btn-transparent-blue" data-tooltip data-tooltip-text="Edit" data-tooltip-place="bottom">
                            <i className="far fa-pencil"></i>
                        </button>
                        <button className="btn-transparent btn-transparent-red" data-tooltip data-tooltip-text="Delete" data-tooltip-place="bottom">
                            <i className="far fa-trash-alt"></i>
                        </button>
                    </div>
                )
            },
        ]
    }

    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>ManageDockerCred</title>
                    <meta name="description" content="Description of ManageDockerCred" />
                </Helmet>

                <header className="content-header d-flex">
                    <div className="flex-60">
                        <h6>Manage Docker Cred -
                            <span className="content-header-badge-group">
                                <span className="content-header-badge-item">Allocated <span className="badge badge-pill badge-primary">5</span></span>
                                <span className="content-header-badge-item">Used <span className="badge badge-pill badge-success">5</span></span>
                                <span className="content-header-badge-item">Remaining <span className="badge badge-pill badge-warning">5</span></span>
                            </span>
                        </h6>
                    </div>
                    <div className="flex-40 text-right">
                        <div className="content-header-group">
                            <button className={this.state.toggleView == true ? "btn btn-light active" : "btn btn-light"} disabled={this.state.isFetching} data-tooltip="true" data-tooltip-text="List View" data-tooltip-place="bottom" onClick={() => this.setState({ toggleView: true })}><i className="fad fa-list-ul"></i></button>
                            <button className={this.state.toggleView == false ? "btn btn-light active" : "btn btn-light"} disabled={this.state.isFetching} data-tooltip="true" data-tooltip-text="Card View" data-tooltip-place="bottom" onClick={() => this.setState({ toggleView: false })}><i className="fad fa-table"></i></button>
                            <button className="btn btn-light" data-tooltip data-tooltip-text="Refresh" data-tooltip-place="bottom">
                                <i className="far fa-sync-alt"></i>
                            </button>
                            <button type='button' className="btn btn-primary" data-tooltip data-tooltip-text="Add Docker Cred" data-tooltip-place="bottom" onClick={() => { this.setState({ isDockerCredModal: true }) }}>
                                <i className="far fa-plus"></i>
                            </button>
                        </div>
                    </div>
                </header>

                <div className="content-body">
                    {this.state.toggleView ?
                        <ListingTable
                            columns={this.getColumns()}
                            data={this.state.filteredList}
                        />
                        :
                        <ul className="card-view-list">
                            {this.state.filteredList.map((item, index) =>
                                <li key={index}>
                                    <div className="card-view-box">
                                        <div className="card-view-header">
                                            <a className="text-truncate d-block f-12" href={item.url} target="_blank"><i className="far fa-link mr-r-4"></i>{item.url}</a>
                                            <div className="dropdown">
                                                <button className="btn-transparent btn-transparent-gray" type="button" data-toggle="dropdown">
                                                    <i className="fas fa-ellipsis-v"></i>
                                                </button>
                                                <div className="dropdown-menu">
                                                    <button className="dropdown-item">
                                                        <i className="far fa-pencil"></i>Edit
                                                    </button>
                                                    <button className="dropdown-item">
                                                        <i className="far fa-trash-alt"></i>Delete
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="card-view-body">
                                            <div className="card-view-icon">
                                                <i className="fad fa-unlock-alt text-dark-theme"></i>
                                            </div>
                                            <h6><i className="far fa-address-book"></i>{item.name ? item.name : "N/A"}</h6>
                                            <p><i className="far fa-user"></i>{item.username}</p>
                                            <p><i className="far fa-lock"></i>{item.password}</p>
                                            {/* <p><span className="badge badge-pill badge-light">Associated Users - {item.userCount}</span></p> */}
                                        </div>
                                        <div className="card-view-footer d-flex">
                                            <div className="flex-50 p-3">
                                                <h6>Created</h6>
                                                <p><strong>By - </strong>{item.createdBy}</p>
                                                <p><strong>At - </strong>{new Date(item.createdAt).toLocaleString('en-US', { createdAt: localStorage.createdAt })}</p>
                                            </div>
                                            <div className="flex-50 p-3">
                                                <h6>Updated</h6>
                                                <p><strong>By - </strong>{item.updatedBy}</p>
                                                <p><strong>At - </strong>{new Date(item.updatedAt).toLocaleString('en-US', { createdAt: localStorage.createdAt })}</p>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            )}
                        </ul>
                    }
                </div>

                {/* add docker cred modal */}
                <SlidingPane
                    className=''
                    overlayClassName='sliding-form'
                    closeIcon={<div></div>}
                    isOpen={this.state.isDockerCredModal || false}
                    from='right'
                    width='500px'
                    ariaHideApp={false}
                >
                    <div className="modal-content">
                        <div className="modal-header">
                            <h6 className="modal-title">Add New Docker Cred
                                <button className="close" onClick={() => { this.setState({ isDockerCredModal: false }) }}><i className="fas fa-angle-right"></i></button>
                            </h6>
                        </div>

                        {/*<div className="modal-loader">
                            <i className="fad fa-sync-alt fa-spin"></i>
                        </div>*/}

                        <form>
                            <div className="modal-body">
                                <div className="form-group">
                                    <label className="form-group-label">Name :<i className="fas fa-asterisk form-group-required"></i></label>
                                    <input id="name" className="form-control" required />
                                </div>

                                <div className="form-group">
                                    <label className="form-group-label">URL :<i className="fas fa-asterisk form-group-required"></i></label>
                                    <input id="url" className="form-control" required />
                                </div>

                                <div className="form-group">
                                    <label className="form-group-label">Username :<i className="fas fa-asterisk form-group-required"></i></label>
                                    <input id="username" className="form-control" required />
                                </div>

                                <React.Fragment>
                                    <div className="alert alert-info note-text">
                                        <p>Password must contain at least 1 lowercase character, 1 uppercase character, 1 numeric character, 1 special character. It must be of min 8 characters.</p>
                                    </div>
                                    <div className="form-group">
                                        <label className="form-group-label">Password : <i className="fas fa-asterisk form-group-required"></i></label>
                                        <div className="input-group">
                                            <input type="password" name="password" pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$^+=!*()@%&]).{6,15}$" className="form-control" required />
                                            <div className="input-group-append">
                                                <span className="input-group-text cursor-pointer"><i className="far fa-eye"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </React.Fragment>
                            </div>

                            <div className="modal-footer justify-content-start">
                                <button className="btn btn-primary" onClick={() => { this.setState({ isDockerCredModal: false }) }}>Create</button>
                                <button type="button" className="btn btn-dark" onClick={() => { this.setState({ isDockerCredModal: false }) }}>Cancel</button>
                            </div>
                        </form>

                    </div>
                </SlidingPane>
                {/* end add docker image modal */}

            </React.Fragment>
        );
    }
}

ManageDockerCred.propTypes = {
    dispatch: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
    managedockercred: makeSelectManageDockerCred()
});

function mapDispatchToProps(dispatch) {
    return {
        dispatch
    };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "manageDockerCred", reducer });
const withSaga = injectSaga({ key: "manageDockerCred", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(ManageDockerCred);
