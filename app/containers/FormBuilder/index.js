/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import JSONInput from "react-json-editor-ajrm/dist";
import Form from "react-jsonschema-form";
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import makeSelectFormBuilder from "./selectors";
import reducer from "./reducer";
import saga from "./saga";



/* eslint-disable react/prefer-stateless-function */
export class FormBuilder extends React.Component {
    state = {
        schema: {
            "title": "A registration form",
            "type": "object",
            "required": [
                "firstName",
                "lastName"
            ],
            "properties": {
                "firstName": {
                    "type": "string",
                    "title": "First name",
                    "default": "Vinay"
                },
                "lastName": {
                    "type": "string",
                    "title": "Last name",
                    "default": "Singh"
                },
                "age": {
                    "type": "integer",
                    "title": "Age",
                    "default": 31
                },
                "bio": {
                    "type": "string",
                    "title": "Bio",
                    "default": "IoT 83"
                },
                "password": {
                    "type": "string",
                    "title": "Password",
                    "minLength": 3,
                    "default": "Hello@123"
                },
                "telephone": {
                    "type": "string",
                    "title": "Telephone",
                    "minLength": 10,
                    "default": "7838908925"
                }
            }
        }
    };

    handleSchemaChange =  (rawObject) => {
        this.setState({
            schema: rawObject.jsObject
        })
    };

    onSubmit = ({formData}, e) => {
        alert(JSON.stringify(formData));
    }
  render() {
    return (
      <div>
        <Helmet>
          <title>Form Builder</title>
          <meta name="description" content="M83-FormBuilder" />
        </Helmet>
          <React.Fragment>
              <div className="pageBreadcrumb">
                  <div className="row">
                      <div className="col-12">
                          <p><span>Form Builder</span></p>
                      </div>
                  </div>
              </div>
              <div className="outerBox">
                  <div className="flex">
                      <div className="flex-item fx-b50 contentFormDetail">
                          <Form schema={this.state.schema}
                                onSubmit={this.onSubmit}
                          />
                      </div>
                      <div className="flex-item fx-b50 contentFormDetail">
                          <div className="aceEditorBox">
                              <JSONInput
                                  placeholder={this.state.schema}
                                  onChange={this.handleSchemaChange}
                              />
                          </div>
                      </div>
                  </div>
              </div>
          </React.Fragment>
      </div>
    );
  }
}

FormBuilder.propTypes = {
  dispatch: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
  formbuilder: makeSelectFormBuilder()
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

const withReducer = injectReducer({ key: "formBuilder", reducer });
const withSaga = injectSaga({ key: "formBuilder", saga });

export default compose(
  withReducer,
  withSaga,
  withConnect
)(FormBuilder);
