/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/*
 *
 * DeviceDashboardsAggregate constants
 *
 */

export const RESET_TO_INITIAL_STATE_PROPS = "app/DeviceMap/RESET_TO_INITIAL_STATE_PROPS";

export const GET_CONNECTORS_LIST = {
    action : 'app/DeviceDashboardsAggregate/GET_CONNECTORS_LIST',
    success: "app/DeviceDashboardsAggregate/GET_CONNECTORS_LIST_SUCCESS",
    failure: "app/DeviceDashboardsAggregate/GET_CONNECTORS_LIST_FAILURE",
    urlKey: "deviceTypeList",
    successKey: "deviceTypeListSuccess",
    failureKey: "deviceTypeListFailure",
    actionName: "deviceTypeList",
}

export const ALARMS_DISTRIBUTION_DETAILS = {
    action : 'app/DeviceDashboardsAggregate/ALARMS_DISTRIBUTION_DETAILS',
    success: "app/DeviceDashboardsAggregate/ALARMS_DISTRIBUTION_DETAILS_SUCCESS",
    failure: "app/DeviceDashboardsAggregate/ALARMS_DISTRIBUTION_DETAILS_FAILURE",
    urlKey: "getAlarmsCount",
    successKey: "getAlarmsCountSuccess",
    failureKey: "getAlarmsCountFailure",
    actionName: "getAlarmsCount",
    actionArguments : ["payload"]
}

export const GET_ALARMS_PIE_CHART_DATA = {
    action : 'app/DeviceDashboardsAggregate/GET_ALARMS_PIE_CHART_DATA',
    success: "app/DeviceDashboardsAggregate/GET_ALARMS_PIE_CHART_DATA_SUCCESS",
    failure: "app/DeviceDashboardsAggregate/GET_ALARMS_PIE_CHART_DATA_FAILURE",
    urlKey: "getAlarmsPieChartData",
    successKey: "getAlarmsPieChartDataSuccess",
    failureKey: "getAlarmsPieChartDataFailure",
    actionName: "getAlarmsPieChartData",
    actionArguments : ["payload"]
}

export const GET_DAILY_ALARMS = {
    action : 'app/DeviceDashboardsAggregate/GET_DAILY_ALARMS',
    success: "app/DeviceDashboardsAggregate/GET_DAILY_ALARMS_SUCCESS",
    failure: "app/DeviceDashboardsAggregate/GET_DAILY_ALARMS_FAILURE",
    urlKey: "getDailyAlarms",
    successKey: "getDailyAlarmsSuccess",
    failureKey: "getDailyAlarmsFailure",
    actionName: "getDailyAlarms",
    actionArguments : ["payload"]
}

export const ALARMS_DISTRIBUTION_BY_TYPE = {
    action : 'app/DeviceDashboardsAggregate/ALARMS_DISTRIBUTION_BY_TYPE',
    success: "app/DeviceDashboardsAggregate/ALARMS_DISTRIBUTION_BY_TYPE_SUCCESS",
    failure: "app/DeviceDashboardsAggregate/ALARMS_DISTRIBUTION_BY_TYPE_FAILURE",
    urlKey: "getAlarmsDistribution",
    successKey: "getAlarmsDistributionSuccess",
    failureKey: "getAlarmsDistributionFailure",
    actionName: "getAlarmsDistribution",
    actionArguments : ["payload"]
}


