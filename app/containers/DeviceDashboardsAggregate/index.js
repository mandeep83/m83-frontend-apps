/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * DeviceDashboardsAggregate
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import { allSelectors as SELECTORS } from "./selectors";
import { allActions as ACTIONS } from './actions'
import reducer from "./reducer";
import saga from "./saga";
import * as am4core from "@amcharts/amcharts4/core";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import * as am4charts from "@amcharts/amcharts4/charts";
import CollapseOrShowAllFilters from "../../components/CollapseOrShowAllFilters";
import Loader from "../../components/Loader/Loadable";
import AddNewButton from "../../components/AddNewButton/Loadable";
import ReactSelect from "react-select";
import { isNavAssigned, DURATION_OPTIONS } from "../../commonUtils";

let pieChart;
/* eslint-disable react/prefer-stateless-function */
export class DeviceDashboardsAggregate extends React.Component {
    state = {
        filterExpand: true,
        deviceTypeList: [],
        isFetching: true,
        deviceGroups: [],
        selectedGroups: [],
        duration: 1440,
        alarmsCount: {
            totalAlarms: 0,
            Critical: 0,
            Major: 0,
            Minor: 0,
            Warning: 0
        }
    }

    componentDidMount() {
        this.props.deviceTypeList()
    }

    static getDerivedStateFromProps(nextProps, state) {
        if (nextProps.timeZone !== state.timeZone) {
            return { timeZone: nextProps.timeZone }
        }
        let newProp = Object.keys(SELECTORS).find(prop => nextProps[prop])
        if (newProp) {
            let propData = nextProps[newProp].error ? nextProps[newProp].error : nextProps[newProp].response
            if (nextProps.deviceTypeListSuccess) {
                var selectedDeviceTypes = [];
                let deviceTypeList = propData
                if (deviceTypeList.length) {
                    deviceTypeList.map(temp => {
                        selectedDeviceTypes.push({
                            value: temp.id,
                            label: temp.name
                        })
                    })
                }
                return { deviceTypeList, isFetching: false, selectedDeviceTypes }
            }
            else if (nextProps.deviceTypeListFailure) {
                nextProps.showNotification("error", propData)
                return { isFetching: false }
            }
            else if (nextProps.getAlarmsCountSuccess) {
                let alarmsCount = propData
                let barChart = []
                if (Object.keys(propData)) {
                    Object.keys(propData).sort().map(temp => {
                        let obj = {};
                        obj.alarmType = temp
                        obj.count = alarmsCount[temp] || 0;
                        barChart.push(obj)
                    })
                    alarmsCount.totalAlarms = alarmsCount.Warning + alarmsCount.Minor + alarmsCount.Major + alarmsCount.Critical
                }
                return {
                    isFetchingAlarmsCount: false,
                    alarmsCount,
                    barChart,
                    isFetchingBarChartData: false
                }
            }
            else if (nextProps.getAlarmsPieChartDataSuccess) {
                pieChart && pieChart.dispose()
                return {
                    isFetchingPieChartData: false,
                    pieChartData: propData
                }
            }
            else if (nextProps.getDailyAlarmsSuccess) {
                return {
                    isFetchingDailyAlarms: false,
                    dailyAlarmsData: propData
                }
            } else if (nextProps.getAlarmsDistributionSuccess) {
                let alarmsDistribution = propData
                if (alarmsDistribution.data && !alarmsDistribution.data.some(item => Object.values(item).filter(el => typeof el == "number").some(el => el))) {
                    alarmsDistribution.availableAttributes = []
                }
                return {
                    isFetchingAlarmsDistribution: false,
                    alarmsDistribution
                }
            }
            if (newProp.includes("Failure")) {
                let loaderKeys = {
                    getAlarmsCountFailure: "isFetchingAlarmsCount",
                    getAlarmsPieChartDataFailure: "isFetchingPieChartData",
                    getAlarmsDistributionFailure: "isFetchingDailyAlarms",

                }
                let errorKeys = {
                    getAlarmsPieChartDataFailure: "pieChartError",
                    isFetchingDailyAlarmsFailure: "dailyAlarmError",
                    isFetchingAlarmsDistribution: "alarmsDistributionError"
                }
                return { [loaderKeys[newProp]]: false, [errorKeys[newProp]]: propData }
            }
        }
        return null
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.state.selectedDeviceTypes !== prevState.selectedDeviceTypes) {
            this.connectorChangeHandler()
        }

        if (this.state.timeZone !== prevState.timeZone && this.state.dailyAlarmsData.length) {
            this.getVarienceLineChart()
        }

        if (this.state.dailyAlarmsData !== prevState.dailyAlarmsData && this.state.dailyAlarmsData.length) {
            this.getVarienceLineChart("varianceChart")
        }

        if (this.state.barChart !== prevState.barChart && this.state.barChart.length && this.state.barChart.some(item => item.count)) {
            this.getBarChart("barChartByAlarmType")
        }

        if (this.state.alarmsDistribution !== prevState.alarmsDistribution && this.state.alarmsDistribution && this.state.alarmsDistribution.availableAttributes && this.state.alarmsDistribution.availableAttributes.length) {
            this.getStackedChart("stackAlarmTypeDiv")
        }

        if (this.state.pieChartData !== prevState.pieChartData && this.state.pieChartData && this.state.pieChartData.length) {
            this.getPieChart("pieChartDiv")
        }

        let newProp = Object.keys(SELECTORS).find(prop => this.props[prop])
        if (newProp) {
            this.props.resetToInitialStateProps(newProp)
        }
    }

    connectorChangeHandler = (isForRefreshing) => {
        am4core.disposeAllCharts()
        if (!this.state.selectedDeviceTypes.length) {
            return this.setState({
                alarmsCount: {},
                pieChartData: [],
                alarmsDistribution: [],
                barChart: [],
                dailyAlarmsData: [],
                deviceGroups: []
            })
        }
        let deviceTypeId = [],
            stateObj = isForRefreshing ? {
                isFetchingAlarmsCount: true,
                isFetchingPieChartData: true,
                isFetchingDailyAlarms: true,
                isFetchingAlarmsDistribution: true,
                isFetchingBarChartData: true,
                pieChartError: false,
            } : {
                    isFetchingAlarmsCount: true,
                    isFetchingPieChartData: true,
                    isFetchingDailyAlarms: true,
                    isFetchingAlarmsDistribution: true,
                    isFetchingBarChartData: true,
                    deviceGroups: [],
                    duration: 1440,
                    selectedGroups: [],
                    pieChartError: false
                }
        this.state.selectedDeviceTypes.map(deviceType => {
            const selectedDeviceType = this.state.deviceTypeList.find(connector => connector.id === deviceType.value);
            if (selectedDeviceType.deviceGroups.length)
                stateObj.selectedGroups.push(selectedDeviceType.deviceGroups[0].deviceGroupId)
            stateObj.deviceGroups = [...stateObj.deviceGroups, ...selectedDeviceType.deviceGroups];
            deviceTypeId.push(deviceType.value)
        })
        this.setState(stateObj, () => {
            let payload = {
                "deviceGroupId": this.state.selectedGroups,
                deviceTypeId,
                "duration": this.state.duration,
            }
            this.props.getAlarmsDistribution(payload)
            this.props.getAlarmsCount(payload)
            this.props.getAlarmsPieChartData(payload)
            this.props.getDailyAlarms(payload)
        })
    }

    getVarienceLineChart = (chartId) => {
        am4core.useTheme(am4themes_animated);
        // Themes end
        am4core.options.autoDispose = true;
        // Create chart instance
        var chart = am4core.create(chartId, am4charts.XYChart);
        chart.dateFormatter.dateFormat = "dd MMM yyyy HH:mm";

        // Add data
        chart.data = this.state.dailyAlarmsData.sort((a, b) => a.timestamp - b.timestamp)
        chart.maskBullets = false;
        // Populate data
        for (var i = 0; i < (chart.data.length - 1); i++) {
            chart.data[i].valueNext = chart.data[i + 1].value;
        }

        chart.dateFormatter.inputDateFormat = "x";

        var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
        dateAxis.dateFormats.setKey("minute", "MMM dd\nHH:mm");
        dateAxis.periodChangeDateFormats.setKey("minute", "MMM dd\nHH:mm");
        dateAxis.renderer.grid.template.location = 0;
        dateAxis.dateFormats.setKey("day", "dd MMM");
        dateAxis.dateFormats.setKey("hour", "dd MMM HH:mm");
        dateAxis.dateFormats.setKey("minute", "dd MMM HH:mm");
        dateAxis.dateFormats.setKey("second", "HH:mm:ss");
        dateAxis.dateFormats.setKey("millisecond", "HH:mm:ss");
        dateAxis.renderer.baseGrid.disabled = true;
        dateAxis.renderer.grid.template.disabled = true;
        dateAxis.renderer.line.strokeOpacity = 0.5;
        dateAxis.renderer.line.strokeWidth = 0.6;
        dateAxis.markUnitChange = false;
        dateAxis.skipEmptyPeriods = true;
        dateAxis.dateFormatter.timezone = localStorage.timeZone
        var label = dateAxis.renderer.labels.template;
        label.wrap = true;
        label.maxWidth = 100

        let max = chart.data[0].value;
        for (let i = 1; i < chart.data.length; i++) {
            if (chart.data[i].value > max) {
                max = chart.data[i].value;
            }
        }
        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.min = 0;
        valueAxis.max = max + Math.pow(10, max.toString().length - 1);

        // Create series
        var series = chart.series.push(new am4charts.ColumnSeries());
        series.dataFields.valueY = "value";
        series.dataFields.dateX = "timestamp";
        series.columns.template.tooltipText = "{dateX}: [bold]{value}[/]";
        series.columns.template.width = am4core.percent(80);

        // Add series for showing variance arrows
        var series2 = chart.series.push(new am4charts.ColumnSeries());
        series2.dataFields.valueY = "valueNext";
        series2.dataFields.openValueY = "value";
        series2.dataFields.dateX = "timestamp";
        series2.columns.template.width = 1;
        series2.fill = am4core.color("#555");
        series2.stroke = am4core.color("#555");

        // Add a triangle for arrow tip
        var arrow = series2.bullets.push(new am4core.Triangle);
        arrow.width = 10;
        arrow.height = 10;
        arrow.horizontalCenter = "middle";
        arrow.verticalCenter = "top";
        arrow.dy = -1;

        // Set up a rotation adapter which would rotate the triangle if its a negative change
        arrow.adapter.add("rotation", function (rotation, target) {
            return getVariancePercent(target.dataItem) < 0 ? 180 : rotation;
        });

        // Set up a rotation adapter which adjusts Y position
        arrow.adapter.add("dy", function (dy, target) {
            return getVariancePercent(target.dataItem) < 0 ? 1 : dy;
        });

        // Hide arrow in case of no change
        arrow.adapter.add("disabled", function (disabled, target) {
            return !getVariancePercent(target.dataItem)
        });

        series2.columns.template.adapter.add("disabled", function (disabled, target) {
            return !getVariancePercent(target.dataItem)
        });

        // Add a label
        var label = series2.bullets.push(new am4core.Label);
        label.padding(10, 10, 10, 10);
        label.text = "";
        label.fill = am4core.color("#0c0");
        label.strokeWidth = 0;
        label.visible = true;
        label.horizontalCenter = "middle";
        label.verticalCenter = "bottom";
        label.fontWeight = "bolder";

        // Adapter for label text which calculates change in percent
        label.adapter.add("textOutput", function (text, target) {
            var percent = getVariancePercent(target.dataItem);
            return percent ? percent + "%" : text;
        });

        // Adapter which shifts the label if it's below the variance column
        label.adapter.add("verticalCenter", function (center, target) {
            return getVariancePercent(target.dataItem) < 0 ? "top" : center;
        });

        // Adapter which changes color of label to red
        label.adapter.add("fill", function (fill, target) {
            return getVariancePercent(target.dataItem) < 0 ? am4core.color("#c00") : fill;
        });

        function getVariancePercent(dataItem) {
            if (dataItem) {
                var value = dataItem.valueY;
                var openValue = dataItem.openValueY;
                var change = value - openValue;
                return Math.round(change / openValue * 100);
            }
            return 0;
        }
    }

    getBarChart = (chartId) => {
        am4core.useTheme(am4themes_animated);
        // Themes end

        // Create chart instance
        var chart = am4core.create(chartId, am4charts.XYChart);
        am4core.options.autoDispose = true;
        // Add data
        chart.data = this.state.barChart

        // Create axes
        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "alarmType";
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 30;

        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.min = 0;

        // Create series
        var series = chart.series.push(new am4charts.ColumnSeries());
        series.dataFields.valueY = "count";
        series.dataFields.categoryX = "alarmType";
        series.name = "count";
        series.columns.template.tooltipText = "{categoryX}: [bold]{valueY}[/]";
        series.columns.template.fillOpacity = .8;
        series.columns.template.width = 40;

        var columnTemplate = series.columns.template;
        columnTemplate.strokeWidth = 2;
        columnTemplate.strokeOpacity = 1;
    }

    getStackedChart = (chartId) => {
        am4core.useTheme(am4themes_animated);
        let data = this.state.alarmsDistribution;
        // Themes end

        // Create chart instance
        var chart = am4core.create(chartId, am4charts.XYChart);
        am4core.options.autoDispose = true;
        // Add data
        chart.data = data.data;

        // Create axes
        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "category";
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 30;

        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.renderer.inside = true;
        valueAxis.renderer.labels.template.disabled = true;
        valueAxis.min = 0;

        // Create series
        function createSeries(field, name) {
            // Set up series
            var series = chart.series.push(new am4charts.ColumnSeries());
            series.name = name;
            series.dataFields.valueY = field;
            series.dataFields.categoryX = "category";
            series.sequencedInterpolation = true;

            // Make it stacked
            series.stacked = true;

            // Configure columns
            series.columns.template.width = 40;
            series.columns.template.tooltipText = "[bold]{name}[/]\n[font-size:14px]{categoryX}: {valueY}";

            // Add label
            var labelBullet = series.bullets.push(new am4charts.LabelBullet());
            labelBullet.label.text = "{valueY}";
            labelBullet.locationY = 0.5;
            labelBullet.label.hideOversized = true;

            return series;
        }
        data.availableAttributes.map(temp => {
            createSeries(temp.key, temp.displayName);
        })

        // Legend
        chart.legend = new am4charts.Legend();
        chart.legend.maxHeight = 320
        chart.legend.maxWidth = 30
        chart.legend.scrollable = true
        chart.legend.scrollbar.showSystemTooltip = false
        chart.legend.position = "right"
        chart.legend.labels.template.fontSize = 11
        chart.legend.labels.template.fill = am4core.color("#808080")
        chart.legend.markers.template.width = 12
        chart.legend.markers.template.height = 12
    }

    getPieChart = (chartId) => {
        am4core.useTheme(am4themes_animated);
        // Themes end

        var container = am4core.create(chartId, am4core.Container);
        am4core.options.autoDispose = true;
        container.width = am4core.percent(100);
        container.height = am4core.percent(100);
        container.layout = "horizontal";

        pieChart = container.createChild(am4charts.PieChart);

        // Add data
        pieChart.data = this.state.pieChartData
        pieChart.radius = am4core.percent(85);
        pieChart.innerRadius = am4core.percent(35);

        // Add and configure Series
        var pieSeries = pieChart.series.push(new am4charts.PieSeries());
        pieSeries.dataFields.value = "count";
        pieSeries.dataFields.category = "alarmType";
        pieSeries.slices.template.states.getKey("active").properties.shiftRadius = 0;
        pieSeries.ticks.template.disabled = true;
        pieSeries.labels.template.fontSize = 11;
        pieSeries.labels.template.fill = am4core.color("#808080");
        pieSeries.alignLabels = false;
        pieSeries.labels.template.bent = true;
        pieSeries.labels.template.padding(0, 0, 0, 0);
        pieSeries.labels.template.text = "{category} : \n {value.percent.formatNumber('#.#')}%";

        pieSeries.slices.template.events.on("hit", function (event) {
            selectSlice(event.target.dataItem);
        })

        var chart2 = container.createChild(am4charts.PieChart);
        chart2.width = am4core.percent(30);
        chart2.radius = am4core.percent(80);

        // Add and configure Series
        var pieSeries2 = chart2.series.push(new am4charts.PieSeries());
        pieSeries2.dataFields.value = "value";
        pieSeries2.dataFields.category = "deviceGroup";
        pieSeries2.slices.template.states.getKey("active").properties.shiftRadius = 0;
        pieSeries2.labels.template.disabled = true;
        pieSeries2.ticks.template.disabled = true;
        pieSeries2.events.on("positionchanged", updateLines);

        var interfaceColors = new am4core.InterfaceColorSet();

        var line1 = container.createChild(am4core.Line);
        line1.strokeDasharray = "2,2";
        line1.strokeOpacity = 0.5;
        line1.stroke = interfaceColors.getFor("alternativeBackground");
        line1.isMeasured = false;

        var line2 = container.createChild(am4core.Line);
        line2.strokeDasharray = "2,2";
        line2.strokeOpacity = 0.5;
        line2.stroke = interfaceColors.getFor("alternativeBackground");
        line2.isMeasured = false;

        var selectedSlice;

        function selectSlice(dataItem) {
            selectedSlice = dataItem.slice;

            var fill = selectedSlice.fill;

            var count = dataItem.dataContext.subData.length;
            pieSeries2.colors.list = [];
            if (fill) {
                for (var i = 0; i < count; i++) {
                    pieSeries2.colors.list.push(fill.brighten(i * 2 / count));
                }
            }
            chart2.data = dataItem.dataContext.subData;
            pieSeries2.appear();

            var middleAngle = selectedSlice.middleAngle;
            var firstAngle = pieSeries.slices.getIndex(0).startAngle;
            var animation = pieSeries.animate([{ property: "startAngle", to: firstAngle - middleAngle }, { property: "endAngle", to: firstAngle - middleAngle + 360 }], 600, am4core.ease.sinOut);
            animation.events.on("animationprogress", updateLines);

            selectedSlice.events.on("transformed", updateLines);
            //  var animation = chart2.animate({property:"dx", from:-container.pixelWidth / 2, to:0}, 2000, am4core.ease.elasticOut)
            //  animation.events.on("animationprogress", updateLines)
        }

        function updateLines() {
            if (selectedSlice) {
                var p11 = { x: selectedSlice.radius * am4core.math.cos(selectedSlice.startAngle), y: selectedSlice.radius * am4core.math.sin(selectedSlice.startAngle) };
                var p12 = { x: selectedSlice.radius * am4core.math.cos(selectedSlice.startAngle + selectedSlice.arc), y: selectedSlice.radius * am4core.math.sin(selectedSlice.startAngle + selectedSlice.arc) };

                p11 = am4core.utils.spritePointToSvg(p11, selectedSlice);
                p12 = am4core.utils.spritePointToSvg(p12, selectedSlice);

                var p21 = { x: 0, y: -pieSeries2.pixelRadius };
                var p22 = { x: 0, y: pieSeries2.pixelRadius };

                p21 = am4core.utils.spritePointToSvg(p21, pieSeries2);
                p22 = am4core.utils.spritePointToSvg(p22, pieSeries2);

                line1.x1 = p11.x;
                line1.x2 = p21.x;
                line1.y1 = p11.y;
                line1.y2 = p21.y;

                line2.x1 = p12.x;
                line2.x2 = p22.x;
                line2.y1 = p12.y;
                line2.y2 = p22.y;
            }
        }

        pieChart.events.on("ready", function () {
            selectSlice(pieSeries.dataItems.getIndex(0));
        });
    }

    filterChangeHandler = (key, value) => {
        let duration = this.state.duration
        let selectedGroups = [...this.state.selectedGroups]
        if (key === "selectedGroups") {
            let previouslyCheckedIndex = this.state.selectedGroups.indexOf(value)
            if (Array.isArray(value)) {
                selectedGroups = [];
                this.state.selectedDeviceTypes.map(deviceType => {
                    const selectedDeviceType = this.state.deviceTypeList.find(connector => connector.id === deviceType.value);
                    if (selectedDeviceType.deviceGroups.length)
                        selectedGroups.push(selectedDeviceType.deviceGroups[0].deviceGroupId)
                })
            } else {
                if (previouslyCheckedIndex != -1) {
                    selectedGroups.splice(previouslyCheckedIndex, 1)
                } else {
                    selectedGroups.push(value)
                }
            }
        }
        else {
            duration = value
        }
        am4core.disposeAllCharts()
        this.setState({
            isFetchingAlarmsCount: true,
            isFetchingPieChartData: true,
            isFetchingDailyAlarms: true,
            isFetchingAlarmsDistribution: true,
            isFetchingBarChartData: true,
            pieChartError: false,
            alarmsCount: { Critical: 0, Major: 0, Minor: 0, Warning: 0, totalAlarms: 0 },
            duration,
            selectedGroups,
        }, () => {
            let payload = {
                "deviceGroupId": selectedGroups,
                "deviceTypeId": this.state.selectedDeviceTypes.map(deviceType => deviceType.value),
                duration,
            }
            this.props.getAlarmsDistribution(payload)
            this.props.getAlarmsCount(payload)
            this.props.getAlarmsPieChartData(payload)
            this.props.getDailyAlarms(payload)
        })
    }

    getMiniLoader = () => {
        return <div className="card-loader">
            <i className="fad fa-sync-alt fa-spin"></i>
        </div>
    }

    getErrorMessage = (errorMessage) => {
        return <div className="card-message">
            {Array.isArray(errorMessage) ? errorMessage.map((message, i) => (<p key={i}>{message}</p>)) :
                <p>{errorMessage}</p>
            }
        </div>
    }

    filterExpandCollapseHandler = () => {
        this.setState({
            filterExpand: this.state.filterExpand ? false : true
        })
    }

    getDeviceTypes = () => {
        return this.state.deviceTypeList.map(deviceType => ({
            label: deviceType.name,
            value: deviceType.id,
        }))
    }

    deviceTypeChangeHandler = (selectedDeviceTypes) => {
        this.setState({
            selectedDeviceTypes,
        })
    }

    validateReactSelectDisabling = () => {
        return this.state.isFetchingAlarmsCount || this.state.isFetchingPieChartData || this.state.isFetchingDailyAlarms || this.state.isFetchingAlarmsDistribution || this.state.isFetchingBarChartData
    }

    durationValue = (duration) => {
        if (duration) {
            return DURATION_OPTIONS.find(obj => obj.value === duration)
        }
    }

    durationChangeHandler = ({ value }) => {
        let duration = value
        am4core.disposeAllCharts()
        this.setState({
            isFetchingAlarmsCount: true,
            isFetchingPieChartData: true,
            isFetchingDailyAlarms: true,
            isFetchingAlarmsDistribution: true,
            isFetchingBarChartData: true,
            pieChartError: false,
            alarmsCount: { Critical: 0, Major: 0, Minor: 0, Warning: 0, totalAlarms: 0 },
            duration,
        }, () => {
            let payload = {
                "deviceGroupId": this.state.selectedGroups,
                "deviceTypeId": this.state.selectedDeviceTypes.map(deviceType => deviceType.value),
                duration,
            }
            this.props.getAlarmsDistribution(payload)
            this.props.getAlarmsCount(payload)
            this.props.getAlarmsPieChartData(payload)
            this.props.getDailyAlarms(payload)
        })
    }

    refreshDeviceAggrigate = () => {
        this.setState({
            filterExpand: true,
            deviceTypeList: [],
            isFetching: true,
            deviceGroups: [],
            selectedGroups: [],
            duration: 1440,
            alarmsCount: {
                totalAlarms: 0,
                Critical: 0,
                Major: 0,
                Minor: 0,
                Warning: 0
            }
        }, () => { this.props.deviceTypeList() })
    }

    openGraphModal = (name) => {
        let selectedGraph;
        switch (name) {
            case "pieChart": {
                selectedGraph = {
                    title: "Total Alarms Distribution as % (across Groups)",
                    data: Boolean(this.state.pieChartData && this.state.pieChartData.length)
                }
                break;
            }
            case "varianceChart": {
                selectedGraph = {
                    title: "Total Alarms (over Time)",
                    data: Boolean(this.state.dailyAlarmsData && this.state.dailyAlarmsData.length)
                }
                break;
            }
            case "stackAlarmType": {
                selectedGraph = {
                    title: "Device Group Alarms Distribution (by Type)",
                    data: Boolean(this.state.alarmsDistribution && this.state.alarmsDistribution.availableAttributes && this.state.alarmsDistribution.availableAttributes.length)
                }
                break;
            }
            case "barChartByAlarmType": {
                selectedGraph = {
                    title: "Total Alarms (by Type)",
                    data: Boolean(this.state.barChart && this.state.barChart.length && this.state.barChart.some(item => item.count))
                }
                break;
            }
        }
        this.setState({
            openGraphFullScreen: true,
            selectedGraph
        }, () => {
            if (name === "pieChart") {
                this.getPieChart("ModalChartDiv")
            }
            if (name === "varianceChart") {
                this.getVarienceLineChart("ModalChartDiv")
            }
            if (name === "stackAlarmType") {
                this.getStackedChart("ModalChartDiv")
            }
            if (name === "barChartByAlarmType") {
                this.getBarChart("ModalChartDiv")
            }
        })
    }

    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>Device Aggregate</title>
                    <meta name="description" content="Description of M83-DeviceAggregate" />
                </Helmet>

                <header className="content-header d-flex">
                    <div className="flex-60">
                        <h6>Device Aggregate</h6>
                    </div>
                    <div className="flex-40 text-right">
                        <div className="content-header-group">
                            <button className="btn btn-light" data-tooltip data-tooltip-text="Refresh" data-tooltip-place="bottom" disabled={this.state.isFetching && this.state.deviceTypeList.length == 0} onClick={() => this.refreshDeviceAggrigate()} ><i className="far fa-sync-alt"></i></button>
                            <button className={`btn btn-light ${this.state.filterExpand && "active"}`} data-tooltip data-tooltip-text="Filters" data-tooltip-place="bottom" disabled={this.state.isFetching || !Boolean(this.state.deviceTypeList.length)} onClick={() => this.filterExpandCollapseHandler()} ><i className="far fa-filter"></i></button>
                        </div>
                    </div>
                </header>

                {this.state.isFetching ?
                    <Loader /> :
                    this.state.deviceTypeList.length ?
                        <React.Fragment>
                            {this.state.filterExpand &&
                                <div className="device-filter-wrapper">
                                    <div className="device-filter-box">
                                        <h5>Filters <CollapseOrShowAllFilters /></h5>
                                        <h6>Specify Your Search Criteria</h6>
                                    </div>
                                    <div className="device-filter-box">
                                        <p>Device Type <i className="fas fa-angle-down device-filter-collapse" data-toggle="collapse" data-target="#deviceType__filter__"></i></p>
                                        <div className="collapse show" id="deviceType__filter__">
                                            <div className="device-filter-body">
                                                <div className="form-group mb-0">
                                                    <ReactSelect
                                                        id="deviceTypes"
                                                        options={this.getDeviceTypes()}
                                                        value={this.state.selectedDeviceTypes}
                                                        onChange={this.deviceTypeChangeHandler}
                                                        isMulti={true}
                                                        isDisabled={this.validateReactSelectDisabling()}
                                                        className="form-control-multi-select">
                                                    </ReactSelect>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="device-filter-box">
                                        <p>Groups <i className="fas fa-angle-down device-filter-collapse" data-toggle="collapse" data-target="#groups__filter__"></i>
                                            {Boolean(this.state.selectedGroups.length) &&
                                                <button className="btn btn-link" onClick={() => this.state.selectedGroups.length && this.filterChangeHandler("selectedGroups", [])}>Reset</button>
                                            }
                                        </p>
                                        <div className="collapse show" id="groups__filter__">
                                            <div className="device-filter-body">
                                                <ul className="d-flex list-style-none device-filter-group">
                                                    {this.state.deviceGroups.length ?
                                                        this.state.deviceGroups.map(deviceGroup =>
                                                            <li key={deviceGroup.deviceGroupId} className={this.state.selectedGroups.includes(deviceGroup.deviceGroupId) ? "active" : ""} onClick={() => this.filterChangeHandler("selectedGroups", deviceGroup.deviceGroupId)}>{deviceGroup.name}</li>) :
                                                        <li><p>No Device Groups Found !</p></li>
                                                    }
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="device-filter-box">
                                        <p>Duration <i className="fas fa-angle-down device-filter-collapse" data-toggle="collapse" data-target="#duration__filter__"></i></p>
                                        <div className="collapse show" id="duration__filter__">
                                            <div className="device-filter-body">
                                                <ReactSelect
                                                    className="form-control-multi-select"
                                                    id="selectedDuration"
                                                    options={DURATION_OPTIONS}
                                                    value={this.durationValue(this.state.duration)}
                                                    onChange={this.durationChangeHandler}
                                                    isMulti={false}>
                                                </ReactSelect>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            }

                            <div className="content-body device-filter-content" style={{ paddingRight: this.state.filterExpand ? 270 : 12 }}>
                                <ul className="list-style-none device-counter-list d-flex">
                                    <li className="flex-20">
                                        <div className="device-counter-card">
                                            <span className="bg-critical"></span>
                                            <h4>{this.state.alarmsCount.totalAlarms || 0}</h4>
                                            <p>Total Alarms</p>
                                            <div className="device-counter-icon">
                                                <i className="fad fa-bell-exclamation text-cyan"></i>
                                            </div>
                                        </div>
                                    </li>
                                    <li className="flex-20">
                                        <div className="device-counter-card">
                                            <span className="bg-critical"></span>
                                            <h4>{this.state.alarmsCount.Critical || 0}</h4>
                                            <p>Critical</p>
                                            <div className="device-counter-icon">
                                                <i className="fad fa-exclamation-triangle text-critical"></i>
                                            </div>
                                        </div>
                                    </li>
                                    <li className="flex-20">
                                        <div className="device-counter-card">
                                            <span className="bg-major"></span>
                                            <h4>{this.state.alarmsCount.Major || 0}</h4>
                                            <p>Major</p>
                                            <div className="device-counter-icon">
                                                <i className="fad fa-exclamation-square text-major"></i>
                                            </div>
                                        </div>
                                    </li>
                                    <li className="flex-20">
                                        <div className="device-counter-card">
                                            <span className="bg-minor"></span>
                                            <h4>{this.state.alarmsCount.Minor || 0}</h4>
                                            <p>Minor</p>
                                            <div className="device-counter-icon">
                                                <i className="fad fa-exclamation-circle text-minor"></i>
                                            </div>
                                        </div>
                                    </li>
                                    <li className="flex-20">
                                        <div className="device-counter-card">
                                            <span className="bg-alarm-warning"></span>
                                            <h4>{this.state.alarmsCount.Warning || 0}</h4>
                                            <p>Warning</p>
                                            <div className="device-counter-icon">
                                                <i className="fad fa-exclamation text-alarm-warning"></i>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                                <div className="d-flex">
                                    <div className="flex-50 pd-r-7">
                                        <div className="card">
                                            <div className="card-header">
                                                <h6>Total Alarms Distribution as % <span className="text-theme">(across Groups)</span></h6>
                                            </div>
                                            <div className="card-body">
                                            {(this.state.pieChartData && this.state.pieChartData.length) ? <button className="btn btn-light box-top-right-button" onClick={() => this.openGraphModal("pieChart")}><i className="far fa-expand"></i></button> : null}
                                                {this.state.isFetchingPieChartData ?
                                                    this.getMiniLoader() : this.state.pieChartError ?
                                                        this.getErrorMessage(this.state.pieChartError) : this.state.pieChartData && this.state.pieChartData.length ?
                                                            <div className="card-chart-box" id="pieChartDiv" />
                                                            :
                                                            <div className="card-message">
                                                                <p>There is no data to display.</p>
                                                            </div>
                                                }
                                            </div>
                                        </div>
                                    </div>
                                    <div className="flex-50 pd-l-7">
                                        <div className="card">
                                            <div className="card-header">
                                                <h6>Total Alarms <span className="text-theme">(over Time)</span></h6>
                                            </div>
                                            <div className="card-body">
                                                {(this.state.dailyAlarmsData && this.state.dailyAlarmsData.length) ? <button className="btn btn-light box-top-right-button" onClick={() => this.openGraphModal("varianceChart")}><i className="far fa-expand"></i></button> : null}
                                                {this.state.isFetchingDailyAlarms
                                                    ? this.getMiniLoader() : this.state.dailyAlarmError ? this.getErrorMessage(this.state.dailyAlarmError) :
                                                        this.state.dailyAlarmsData && this.state.dailyAlarmsData.length ?
                                                            <div className="card-chart-box" id="varianceChart"> </div>
                                                            :
                                                            <div className="card-message">
                                                                <p>There is no data to display.</p>
                                                            </div>
                                                }
                                            </div>
                                        </div>
                                    </div>
                                    <div className="flex-50 pd-r-7">
                                        <div className="card">
                                            <div className="card-header">
                                                <h6>Device Group Alarms Distribution <span className="text-theme">(by Type)</span></h6>
                                            </div>
                                            <div className="card-body">
                                                {(this.state.alarmsDistribution && this.state.alarmsDistribution.availableAttributes && this.state.alarmsDistribution.availableAttributes.length) ? <button className="btn btn-light box-top-right-button" onClick={() => this.openGraphModal("stackAlarmType")}><i className="far fa-expand"></i></button> : null}
                                                {this.state.isFetchingAlarmsDistribution ?
                                                    this.getMiniLoader() : this.state.alarmsDistributionError ?
                                                        this.getErrorMessage(this.state.alarmsDistributionError) : this.state.alarmsDistribution && this.state.alarmsDistribution.availableAttributes && this.state.alarmsDistribution.availableAttributes.length ?
                                                            <div className="card-chart-box" id="stackAlarmTypeDiv"></div>
                                                            :
                                                            <div className="card-message">
                                                                <p>There is no data to display.</p>
                                                            </div>
                                                }

                                            </div>
                                        </div>
                                    </div>
                                    <div className="flex-50 pd-l-7">
                                        <div className="card">
                                            <div className="card-header">
                                                <h6>Total Alarms <span className="text-theme">(by Type)</span></h6>
                                            </div>
                                            <div className="card-body">
                                                {(this.state.barChart && this.state.barChart.length && this.state.barChart.some(item => item.count)) ? <button className="btn btn-light box-top-right-button" onClick={() => this.openGraphModal("barChartByAlarmType")}><i className="far fa-expand"></i></button> : null}
                                                {this.state.isFetchingBarChartData ?
                                                    this.getMiniLoader() : this.state.barChartError ?
                                                        this.getErrorMessage(this.state.barChartError) : this.state.barChart && this.state.barChart.length && this.state.barChart.some(item => item.count) ?
                                                            <div className="card-chart-box" id="barChartByAlarmType"> </div>
                                                            :
                                                            <div className="card-message">
                                                                <p>There is no data to display.</p>
                                                            </div>
                                                }
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </React.Fragment>
                        :
                        <div className="content-body">
                            <AddNewButton
                                text1="No aggregation(s) available"
                                text2="You haven't created any device type(s) yet. Please create a device type first."
                                addButtonEnable={isNavAssigned("deviceTypes")}
                                createItemOnAddButtonClick={() => this.props.history.push(`/addOrEditDeviceType`)}
                                imageIcon="addDeviceAggregate.png"
                            />
                        </div>
                }

                {/* graph full screen modal */}
                {this.state.openGraphFullScreen &&
                    <div className="modal d-block animated slideInDown">
                        <div className="modal-dialog modal-full">
                            <div className="modal-content">
                                <div className="modal-body p-2">
                                    <div className="card modal-full-editor m-0 shadow-none">
                                        <div className="card-header border">
                                            <h6>
                                                {this.state.selectedGraph.title}
                                            </h6>
                                        </div>
                                        <div className="card-body border">
                                            {this.state.selectedGraph.data ?
                                                <div className="card-chart-box" id="ModalChartDiv" /> :
                                                <div className="card-message h-100"><p>There is no data to display</p></div>
                                            }

                                        </div>
                                        <button type="button" className="btn btn-light text-gray" onClick={() => this.setState({ openGraphFullScreen: false })}>
                                            <i className="fas fa-compress"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                }
                {/* end graph full screen modal */}

            </React.Fragment>
        );
    }
}

DeviceDashboardsAggregate.propTypes = {
    dispatch: PropTypes.func.isRequired
};

let allSelectors = {}
Object.entries(SELECTORS).map(([key, value]) => {
    allSelectors[key] = value()
})

const mapStateToProps = createStructuredSelector(allSelectors)

function mapDispatchToProps(dispatch) {
    let allActions = { dispatch }
    Object.entries(ACTIONS).map(([key, value]) => {
        allActions[key] = (...args) => dispatch(value(...args))
    })
    return allActions
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({
    key: "deviceDashboardsAggregate",
    reducer
});
const withSaga = injectSaga({ key: "deviceDashboardsAggregate", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(DeviceDashboardsAggregate);