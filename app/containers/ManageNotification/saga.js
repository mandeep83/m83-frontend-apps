/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

// import { take, call, put, select } from 'redux-saga/effects';
import { takeEvery } from 'redux-saga';
import { put } from 'redux-saga/effects';
import { apiCallHandler } from '../../api';
import * as CONSTANT from './constants';

export function* getNotificationListHandler(action) {
  yield [apiCallHandler(action, CONSTANT.GET_NOTIFICATIONS_LIST_SUCCESS, CONSTANT.GET_NOTIFICATIONS_LIST_ERROR, 'getNotificationList')];
}

export function* watcherGetNotificationsList() {
  yield takeEvery(CONSTANT.GET_NOTIFICATIONS_LIST, getNotificationListHandler);
}

export function* updateNotificationListHandler(action){
 yield [apiCallHandler(action, CONSTANT.UPDATE_NOTIFICATIONS_LIST_SUCCESS, CONSTANT.UPDATE_NOTIFICATIONS_LIST_ERROR, 'updateNotificationList')]
}

export function* wathcerUpdateNotificationList() {
  yield takeEvery(CONSTANT.UPDATE_NOTIFICATIONS_LIST, updateNotificationListHandler);
}

export default function* rootSaga() {
 yield[
  watcherGetNotificationsList(),
  wathcerUpdateNotificationList()
 ]
}
