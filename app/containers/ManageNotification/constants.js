/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/*
 *
 * ManageNotification constants
 *
 */


export const GET_NOTIFICATIONS_LIST = 'app/ManageRoles/GET_NOTIFICATIONS_LIST';
export const GET_NOTIFICATIONS_LIST_SUCCESS = 'app/ManageRoles/GET_NOTIFICATIONS_LIST_SUCCESS';
export const GET_NOTIFICATIONS_LIST_ERROR = 'app/ManageRoles/GET_NOTIFICATIONS_LIST_ERROR';

export const UPDATE_NOTIFICATIONS_LIST = 'app/ManageRoles/UPDATE_NOTIFICATIONS_LIST';
export const UPDATE_NOTIFICATIONS_LIST_SUCCESS = 'app/ManageRoles/UPDATE_NOTIFICATIONS_LIST_SUCCESS';
export const UPDATE_NOTIFICATIONS_LIST_ERROR = 'app/ManageRoles/UPDATE_NOTIFICATIONS_LIST_ERROR';

