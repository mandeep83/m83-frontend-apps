/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * ManageNotification
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { FormattedMessage } from "react-intl";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import ReactTooltip from "react-tooltip";

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import * as SELECTORS from './selectors';
import * as ACTIONS from './actions';
import reducer from "./reducer";
import saga from "./saga";
import messages from "./messages";
import NotificationModal from '../../components/NotificationModal/Loadable'


/* eslint-disable react/prefer-stateless-function */
export class ManageNotification extends React.Component {
  state = {
    notificationModal: false,
    isFetching: true,
    isOpen: false,
    data: [{
      eventName: 'Hello',
      configName: 'hello',
      threshold: 'No',
      emails: ["hello@.com"],
      active: false
    },
    {
      eventName: 'hi',
      configName: 'hi',
      threshold: 'Yes',
      emails: ["hi@.com"],
      active: true
    },
    {
      eventName: 'Gautam',
      configName: 'gautam',
      threshold: 'YEs/no',
      emails: ["gautam@.com"],
      active: false
    }
    ]
  }

  componentDidMount() {
    this.props.getNotificationList();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.getNotificationList && nextProps.getNotificationList !== this.props.getNotificationList) {
      this.setState({
        isFetching: false
      })
    }
    if (nextProps.getNotificationListError && nextProps.getNotificationListError !== this.props.getNotificationListError) {
      this.setState({
        modalType: "error",
        isOpen: true,
        message2: nextProps.getNotificationListError,
        isFetching: false
      })
    }
    if (nextProps.getUpdateNotificationList && nextProps.getUpdateNotificationList !== this.props.getUpdateNotificationList) {
      this.setState({
        modalType: "success",
        isOpen: true,
        message2: nextProps.getUpdateNotificationList,
        isFetching: false
      })
    }
    if (nextProps.getUpdateNotificationListError && nextProps.getUpdateNotificationListError !== this.props.getUpdateNotificationListError) {
      this.setState({
        modalType: "error",
        isOpen: true,
        message2: nextProps.getUpdateNotificationListError,
        isFetching: false
      })
    }
  }

  addEmailHandler = (index) => {
    let data = JSON.parse(JSON.stringify(this.state.data));
    data[index].emails.push("");
    this.setState({ data })
  }

  inputChangeHandler = (index) => {
    let data = JSON.parse(JSON.stringify(this.state.data));
    if (event.target.id == "triggerCheckbox") {
      data[index].active = event.target.checked;
    }
    else {
      let currentIndex = event.target.id.split('_')[1];
      data[index].emails[currentIndex] = event.target.value;
    }
    this.setState({ data })
  }

  deleteEmailHandler = (index, id) => {
    let data = JSON.parse(JSON.stringify(this.state.data));
    if (data[index].emails.length == 1) {
      data[index].emails[id] = "";
    }
    else {
      let tempData = data[index].emails.filter((temp, index) => index != id);
      data[index].emails = tempData;
    }
    this.setState({ data })
  }

  copyEmailHandler = (id) => {
    var copyText = document.getElementById(id);
    copyText.select();
    document.execCommand("copy");
  }

  onCloseHandler = (index) => {

    this.setState({
        isOpen: false,
        message2: ''
    });
}

  render() {
    return (
      <React.Fragment>
        <Helmet>
          <title>ManageNotification</title>
          <meta
            name="description"
            content="Description of ManageNotification"
          />
        </Helmet>
        <div className="pageBreadcrumb">
          <div className="row">
            <div className="col-8">
              <p><span>Manage Notifications</span></p>
            </div>
            <div className="col-4 text-right">
              <div className="flex h-100 justify-content-end align-items-center">
              </div>
            </div>
          </div>
        </div>
        <div className="outerBox">
          <div className="contentTableBox notificationTable mr-0">
            <table className="table">
              <thead>
                <tr>
                  <th width="10%">Trigger</th>
                  <th width="20%">Event Name</th>
                  <th width="20%">Config Name</th>
                  <th width="20%">Threshold</th>
                  <th width="20%">Emails</th>
                  <th width="10%">Settings</th>
                </tr>
              </thead>
              <tbody>
                {this.state.data.map((val, index) =>
                  <tr key={index}>
                    <td>
                      <label className="checkboxLabel">
                        <input name="roleIds" id="triggerCheckbox" type="checkbox" checked={val.active} onChange={() => this.inputChangeHandler(index)} />
                        <span className="checkmark" />
                      </label>
                    </td>
                    <td>
                      {val.eventName}
                    </td>
                    <td>
                      {val.configName}
                    </td>
                    <td>
                      {val.threshold}
                    </td>
                    <td>
                      {val.emails.map((temp, id) =>
                        <div className="input-group mr-b-10" key={id}>
                          <input type="text" value={temp} id={val.eventName + "_" + id} className="form-control" onChange={() => this.inputChangeHandler(index)} />

                          <div className="input-group-append">
                            <div className="input-group-text bg-transparent border-0 pd-0 pd-l-10">
                              <button className="btn btn-transparent btn-transparent-success" onClick={() => this.copyEmailHandler(val.eventName + "_" + id)}><i className="far fa-copy"></i></button>
                              <button className="btn btn-transparent btn-transparent-danger" onClick={() => this.deleteEmailHandler(index, id)}><i className="far fa-trash-alt"></i></button>
                              {val.emails.length - 1 === id ?
                                <button className="btn btn-transparent btn-transparent-primary" onClick={() => this.addEmailHandler(index)}><i className="far fa-plus"></i></button>
                                :
                                null
                              }
                            </div>
                          </div>
                        </div>
                      )}
                    </td>
                    <td>
                      <button className="btn btn-transparent btn-transparent-warning" onClick={() => this.setState({ notificationModal: true })} disabled><i className="far fa-cog"></i></button>
                    </td>
                  </tr>
                )}
              </tbody>
            </table>
            <button type="button" onClick={() => this.props.updateNotificationList(this.state.data)}>Submit</button>
          </div>
        </div>
        {this.state.notificationModal &&
        <div className="modal fade show d-block" tabIndex="-1" role="dialog">
          <div className="modal-dialog modal-lg animated slideInDown" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h6 className="modal-title">Modal title
                <button type="button" className="close" onClick={()=>this.setState({notificationModal: false})} data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true"><i className="far fa-times"></i></span>
                </button>
                </h6>
              </div>
              <div className="modal-body">
                <div className="contentTableBox notificationTable mr-0">
                  <table className="table">
                    <thead>
                    <tr>
                      <th width="90%">Property Name</th>
                      <th width="10%">Is Active</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                      <td>INFO_HDFS_DS_HOST_FACT_ACTION</td>
                      <td>
                        <label className="toggleSwitch">
                          <input type="checkbox" id="isAuthenticated"/>
                          <span className="toggleSwitchSlider"></span>
                        </label>
                      </td>
                    </tr>
                    <tr>
                      <td>INFO_HDFS_DS_HOST_FACT_ACTION</td>
                      <td>
                        <label className="toggleSwitch">
                          <input type="checkbox" id="isAuthenticated"/>
                          <span className="toggleSwitchSlider"></span>
                        </label>
                      </td>
                    </tr>
                    <tr>
                      <td>INFO_HDFS_DS_HOST_FACT_ACTION</td>
                      <td>
                        <label className="toggleSwitch">
                          <input type="checkbox" id="isAuthenticated"/>
                          <span className="toggleSwitchSlider"></span>
                        </label>
                      </td>
                    </tr>
                    <tr>
                      <td>INFO_HDFS_DS_HOST_FACT_ACTION</td>
                      <td>
                        <label className="toggleSwitch">
                          <input type="checkbox" id="isAuthenticated"/>
                          <span className="toggleSwitchSlider"></span>
                        </label>
                      </td>
                    </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <div className="modal-footer">
              <button type="button" className="btn btn-success">Save</button>
              </div>
            </div>
          </div>
          </div>
        }
        {this.state.isOpen &&
          <NotificationModal
            type={this.state.modalType}
            message2={this.state.message2}
            onCloseHandler={this.onCloseHandler}
          />
        }
      </React.Fragment>
    );
  }
}

ManageNotification.propTypes = {
  dispatch: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
  getNotificationListData: SELECTORS.getNotificationListData(),
  getNotificationListError: SELECTORS.getNotificationListError(),
  getUpdateNotificationList: SELECTORS.getUpdateNotificationList(),
  getUpdateNotificationListError: SELECTORS.getUpdateNotificationListError()
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    getNotificationList: () => dispatch(ACTIONS.getNotificationList()),
    updateNotificationList: (payload) => dispatch(ACTIONS.updateNotificationList(payload))
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

const withReducer = injectReducer({ key: "manageNotification", reducer });
const withSaga = injectSaga({ key: "manageNotification", saga });

export default compose(
  withReducer,
  withSaga,
  withConnect
)(ManageNotification);
