/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { createSelector } from "reselect";
import { initialState } from "./reducer";

const selectDeviceListDomain = state => state.get("login", initialState);

export const getIsFetching = () => createSelector(selectDeviceListDomain, subState => subState.isFetching);

export const loginSuccess = () => createSelector(selectDeviceListDomain, subState => subState.loginSuccess);

export const loginFailure = () => createSelector(selectDeviceListDomain, subState => subState.loginFailure);

export const getTenantDetailsSuccess = () => createSelector(selectDeviceListDomain, subState => subState.getTenantDetailsSuccess);

export const getTenantDetailsFailure = () => createSelector(selectDeviceListDomain, subState => subState.getTenantDetailsFailure);

export const forgotPasswordSuccess = () => createSelector(selectDeviceListDomain, subState => subState.forgotPasswordSuccess);

export const forgotPasswordError = () => createSelector(selectDeviceListDomain, subState => subState.forgotPasswordError);
