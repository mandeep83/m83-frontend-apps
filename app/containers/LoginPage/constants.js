/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

export const LOGGED_SUCCESSFULLY = 'M83/Login/LOGGED_SUCCESSFULLY';
export const LOGIN_FAILED = 'M83/Login/LOGIN_FAILED';
export const LOGIN_INITIATED = 'M83/Login/LOGIN_INITIATED';

export const GET_TENANT_DETAILS = 'M83/Login/GET_TENANT_DETAILS';
export const GET_TENANT_DETAILS_SUCCESS = 'M83/Login/GET_TENANT_DETAILS_SUCCESS';
export const GET_TENANT_DETAILS_FAILURE = 'M83/Login/GET_TENANT_DETAILS_FAILURE';
