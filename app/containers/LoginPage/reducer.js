/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { fromJS } from 'immutable';
import * as CONSTANTS from "./constants";


export const initialState = fromJS({});

function loginReducer(state = initialState, action) {
    switch (action.type) {
        case CONSTANTS.LOGIN_FAILED:
            return Object.assign({}, state, {
                loginFailure: { message: action.error, timestamp: Date.now() }
            });
        case CONSTANTS.LOGGED_SUCCESSFULLY:
            return Object.assign({}, state, {
                loginSuccess: action.response
            });
        case CONSTANTS.GET_TENANT_DETAILS_SUCCESS:
            return Object.assign({}, state, {
                getTenantDetailsSuccess: {
                    response: action.response,
                    timestamp: Date.now()
                }
            });
        case CONSTANTS.GET_TENANT_DETAILS_FAILURE:
            return Object.assign({}, state, {
                getTenantDetailsFailure: action.error
            });
        default:
            return state;
    }
}

export default loginReducer;
