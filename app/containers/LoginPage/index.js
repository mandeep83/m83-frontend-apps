/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import React from "react";
import { createStructuredSelector } from "reselect";
import jwt_decode from "jwt-decode";
import { connect } from "react-redux";
import { compose } from "redux";
import injectReducer from "utils/injectReducer";
import injectSaga from "utils/injectSaga";
import { Helmet } from "react-helmet";
import * as Actions from './actions';
import * as Selectors from './selectors';
import reducer from "./reducer";
import saga from "./saga";
import Loader from "../../components/Loader";
import NoTenantExists from "../../components/NoTenantExists/Loadable";
import CountersWidget from '../../components/CountersWidget';
import NotificationModal from '../../components/NotificationModal/Loadable'


/* eslint-disable react/prefer-stateless-function */

export class LoginPage extends React.PureComponent {

    state = {
        tenantDetails: {},
        isFetching: true,
        email: '',
        password: '',
        isTenantExist: false,
        isLoginInitiated: false,
    };

    loginHandler = event => {
        event.preventDefault();
        this.setState({
            isLoginInitiated: true,
        }, () => {
            this.props.onLoginRequest(this.state.email, this.state.password)
        });
    };

    componentWillMount() {
        const params = {};
        let query = window.location.hash.substr(1);
        let pairs = query.split("&");
        for (let i in pairs) {
            let couplet = pairs[i].split("=");
            params[couplet[0]] = couplet[1];
        }
        let tenantName = (window.API_URL.split(".")[0]).split("//")[1];
        if (tenantName !== 'localhost:9093/' && tenantName.toUpperCase() !== 'PLATFORM') {
            this.props.fetchTenantDetails(tenantName);
        } else {
            this.setState({
                isFetching: false,
                isTenantExist: true,
            });
        }

        if (params.id_token) {
            localStorage["token"] = params.id_token;
            let name = jwt_decode(params.id_token).username
                ? jwt_decode(params.id_token).username
                : "Admin";
            localStorage["Username"] = name;
            localStorage['userId'] = jwt_decode(params.id_token).jti
            this.props.history.push("./");
        }

        const queryString = window.location.search;
        const urlParams = new URLSearchParams(queryString);
        if (urlParams && urlParams.get("registration")) {
            this.setState({
                message2: urlParams.get("registration") === "true" ? "Your Registration was successful, please check email for your credentials." : "Your Registration was not successful, please try again after sometime",
                modalType: urlParams.get("registration") === "true" ? 'success' : "error",
                email: urlParams.get('email') ? urlParams.get('email') : '',
                isOpen: true,
            })
        } else if (urlParams && urlParams.get("errorCode")) {
            this.setState({
                message2: urlParams.get("errorCode") === "403" ? "Access denied for this resource." : "Something went wrong.",
                modalType: "error",
                isOpen: true,
            })
        } else if (urlParams && urlParams.get('email')) {
            this.setState({
                email: urlParams.get('email'),
            })
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.forgotPasswordSuccess && nextProps.forgotPasswordSuccess !== this.props.forgotPasswordSuccess) {
            this.setState({
                isLoginInitiated: false,
                successMessage: nextProps.forgotPasswordSuccess,
            })
        }
        if (nextProps.forgotPasswordError && nextProps.forgotPasswordError !== this.props.forgotPasswordError) {
            this.setState({
                isLoginInitiated: false,
                errorMessage: nextProps.forgotPasswordError,
            })
        }

        if (nextProps.getTenantDetailsSuccess && nextProps.getTenantDetailsSuccess !== this.props.getTenantDetailsSuccess) {
            let tenantDetails = { ...nextProps.getTenantDetailsSuccess.response }
            localStorage['oAuthProviders'] = JSON.stringify(tenantDetails.oAuthProviders);
            if (tenantDetails.isPackageCreated) {
                localStorage['SAAS_Projects'] = JSON.stringify(tenantDetails.projects);
            }
            localStorage.theme = JSON.stringify(tenantDetails.style);
            localStorage.tenantType = tenantDetails.tenantType;
            tenantDetails.logoImage = tenantDetails.logoImage ? tenantDetails.logoImage + `?lastmod=${new Date()}` : "";
            tenantDetails.brandingImageUrl = tenantDetails.brandingImageUrl ? tenantDetails.brandingImageUrl + `?lastmod=${new Date()}` : "";
            localStorage.tenantLogoURL = tenantDetails.logoImage

            this.setState({
                tenantDetails,
                isTenantExist: tenantDetails.isTenantExist,
                isFetching: false
            })
        }
        if (nextProps.getTenantDetailsFailure && nextProps.getTenantDetailsFailure !== this.props.getTenantDetailsFailure) {
            this.setState({
                isTenantExist: false,
                isFetching: false
            })
        }

        if (nextProps.loginSuccess && nextProps.loginSuccess !== this.props.loginSuccess) {

            let token = nextProps.loginSuccess.accessToken,
                name = jwt_decode(token).name ? jwt_decode(token).name : "Admin";

            localStorage['userId'] = jwt_decode(token).jti
            localStorage['Username'] = name;
            localStorage['token'] = token;
            localStorage['tenant'] = jwt_decode(token).tenant;
            localStorage['role'] = jwt_decode(token).role;
            localStorage['isVerified'] = nextProps.loginSuccess.isVerified;

            if (document.getElementById("themeCSS")) {
                setTimeout(() => {
                    let appliedLinkTag = document.getElementById("themeCSS")
                    document.getElementsByTagName('head')[0].removeChild(appliedLinkTag);
                }, 2000);
            }

            var link = document.createElement('link');
            link.id = "themeCSS"
            link.rel = "stylesheet"
            link.href = window.API_URL + "api/unauth/user/theme.css" + `?userId=${localStorage.getItem("userId")}`;
            document.getElementsByTagName('head')[0].appendChild(link);

            if (!nextProps.loginSuccess.isVerified) {
                this.props.history.push('/changePassword');
            } else if (nextProps.loginSuccess.isLicenseExpired) {
                if (jwt_decode(token).role === 'ACCOUNT_ADMIN') {
                    this.props.history.push('/');
                } else {
                    // delete localStorage['userId'];
                    delete localStorage['Username'];
                    delete localStorage['token'];
                    delete localStorage['tenant'];
                    delete localStorage['role'];
                    delete localStorage['isVerified'];

                    this.setState({
                        isLoginInitiated: false,
                        message2: "License expired, Please contact your system administrator !",
                        modalType: "error",
                        isOpen: true,
                    })
                }
            } else {
                this.props.history.push('/');
            }
        }
        if (nextProps.loginFailure && nextProps.loginFailure !== this.props.loginFailure) {
            this.setState({
                isLoginInitiated: false,
                errorMessage: nextProps.loginFailure.message,
            })
        }
    }

    onChangeHandler = event => {
        this.setState({
            [event.target.id]: event.target.value
        });
    }

    redirectToResetScreen = () => {
        this.props.history.push('/forgotPassword')
    }

    onCloseHandler = () => {
        this.setState({ isOpen: false, message2: '', modalType: '' })
    }


    render() {
        if (this.state.isFetching) {
            return <Loader />;
        } else if (!this.state.isTenantExist) {
            return <NoTenantExists />;
        } else {
            return (
                <React.Fragment>
                    <Helmet>
                        <title>Login</title>
                        <meta name="description" content="M83-Login" />
                    </Helmet>

                    {localStorage.tenantType == "MAKER" ?
                        <div className="login-outer-box" id="makerLogin">
                            <div className="login-image-content">
                                <div className="login-image-box">
                                    <img src="https://content.iot83.com/m83/tenant/welcome.png" />
                                </div>
                                <h1>Welcome to iFlex83</h1>
                                <h5>Connect . Configure . Visualize</h5>
                                <p>A powerful and cost effective path to create dynamic IoT Applications. ​</p>
                                <p>Fast new and legacy device on-boarding.</p>
                                <p>Few clicks and your dashboards ready.</p>
                                <p>Not a single line of code.</p>
                                <p>Security, reliability and scalability assured.</p>
                            </div>

                            <div className="login-form-wrapper">
                                <div className="login-logo">
                                    <img src="https://content.iot83.com/m83/misc/iFlexLogo.png" />
                                </div>
                                <div className="login-form-box">
                                    {this.state.isLoginInitiated ?
                                        <div className="login-form-loader">
                                            <div id="ld3">
                                                <div></div>
                                                <div></div>
                                                <div></div>
                                                <div></div>
                                            </div>
                                        </div>
                                        :
                                        <form onSubmit={e => { this.loginHandler(e) }}>
                                            <div className={this.state.errorMessage || this.state.successMessage ? "form-group form-group-error" : "form-group"}>
                                                <input type="email" id="email" placeholder="Username" className="form-control" required value={this.state.email} onChange={this.onChangeHandler} />
                                                <span className="form-control-icon"><i className="fad fa-envelope"></i></span>
                                            </div>
                                            <div className={this.state.errorMessage || this.state.successMessage ? "form-group form-group-error" : "form-group"}>
                                                <input type="password" id="password" placeholder="Password" className="form-control" required value={this.state.password} onChange={this.onChangeHandler} />
                                                <span className="form-control-icon"><i className="fad fa-lock"></i></span>
                                            </div>
                                            <div className="form-group">
                                                <button name="button" className="btn btn-primary">Login</button>
                                                <a onClick={() => { this.redirectToResetScreen() }}>Forgot Password ?</a>
                                            </div>
                                            {this.state.errorMessage && <div className="login-form-error">{this.state.errorMessage}</div>}
                                            {this.state.successMessage && <div className="login-form-error">{this.state.successMessage}</div>}
                                        </form>
                                    }
                                </div>
                                {this.state.tenantDetails.isPackageCreated &&
                                    <div className="sign-up-text">
                                        <h6>Don't have account ?
                                        <button name="button" className="btn btn-link" onClick={() => this.props.history.push('/iSignUp')}>Sign Up Here <i className="fas fa-angle-double-right"></i></button>
                                        </h6>
                                    </div>
                                }
                            </div>
                        </div>
                        :
                        <div className="login-outer-box" id="flexLogin">
                            <div className="d-flex h-100">
                                <div className="flex-70 h-100">
                                    <div className="login-outer-image">
                                        <img src={this.state.tenantDetails.brandingImageUrl ? this.state.tenantDetails.brandingImageUrl : "https://content.iot83.com/m83/misc/flexLoginImage_new.png"} />
                                        {/*<div className="login-outer-content">
                                            <div className="mr-b-3">
                                                <p>A powerful path to dynamic applications</p>
                                                <p>Cost Effective IoT application  solutions</p>
                                                <p>Click - Through dashboard creation</p>
                                            </div>
                                            <div>
                                                <p>Security, reliability and scalability assured</p>
                                                <p>Low - Code workflow to "Add the Magic"</p>
                                                <p>Fast and new legacy device on-boarding.</p>
                                            </div>
                                        </div>*/}
                                    </div>
                                </div>

                                <div className="flex-30 bg-white position-relative">
                                    <div className="login-logo">
                                        <img src={this.state.tenantDetails.logoImage ? this.state.tenantDetails.logoImage : "https://content.iot83.com/m83/misc/flexLogo.png"} />
                                    </div>
                                    <div className="login-form-box">
                                        <div className="login-form-text">
                                            <h1>{this.state.tenantDetails.message ? this.state.tenantDetails.message : "Welcome to Flex83"}</h1>
                                            <h6>{this.state.tenantDetails.brandingText ? this.state.tenantDetails.brandingText : "Connect. Configure. Visualize. Operate. Extend"}</h6>
                                        </div>
                                        {this.state.isLoginInitiated ?
                                            <div className="login-form-loader">
                                                <div id="ld3">
                                                    <div></div>
                                                    <div></div>
                                                    <div></div>
                                                    <div></div>
                                                </div>
                                            </div>
                                            :
                                            <form onSubmit={e => { this.loginHandler(e) }}>
                                                {window.API_URL.split(".")[0].split("//")[1] === 'platform' &&
                                                    <div className="social-form-group">
                                                        <button type="button" className="btn btn-light" onClick={() => window.open(`https://login.microsoftonline.com/e56b1c85-a99a-4fab-92e1-60d145efdb0e/oauth2/v2.0/authorize?client_id=807a241d-cebb-4394-ab6a-3d58be0fe3f4&response_type=code&redirect_uri=${window.location.origin}/oauthCallBack&scope=openid%20profile&response_mode=fragment&state=AmQFtlXPHU7DYi1XLS8ElBmjuYwBtBdVF74fb777e093f647b9a0ec8d6ff3ff2bd6`, '_self')}>
                                                            <img src="https://content.iot83.com/m83/tenant/microsoft.png" />Login With Microsoft
                                                    </button>
                                                        <p>OR</p>
                                                    </div>
                                                }
                                                <div className="form-group">
                                                    <input type="email" id="email" placeholder="Username" className="form-control" required value={this.state.email} onChange={this.onChangeHandler} />
                                                    <span className="form-control-icon"><i className="fad fa-envelope"></i></span>
                                                </div>

                                                <div className="form-group">
                                                    <input type="password" id="password" placeholder="Password" className="form-control" required value={this.state.password} onChange={this.onChangeHandler} />
                                                    <span className="form-control-icon"><i className="fad fa-lock"></i></span>
                                                </div>

                                                <div className="form-group d-flex">
                                                    <div className="flex-60 pr-1">
                                                        <button name="button" className="btn btn-primary">Login</button>
                                                    </div>
                                                    <div className="flex-40 pl-1">
                                                        <a className="mt-3 text-right" onClick={() => { this.redirectToResetScreen() }}>Forgot Password ?</a>
                                                    </div>
                                                </div>

                                                {this.state.errorMessage && <div className="login-form-error position-absolute">{this.state.errorMessage}</div>}
                                                {this.state.successMessage && <div className="login-form-error position-absolute">{this.state.successMessage}</div>}

                                                {window.API_URL.split(".")[0].split("//")[1] === 'platform' &&
                                                    <div className="social-form-group">
                                                        <p>OR</p>
                                                        <div className="form-group">
                                                            {/*<div className="flex-42_5">*/}
                                                            {/*    <button type="button" className="btn btn-dark" onClick={() => window.open('https://developer.flex83.com/registration')}>Individual</button>*/}
                                                            {/*</div>*/}
                                                            {/*<div className="flex-15 pr-1 pl-1">*/}
                                                            {/*    <h6 className="text-content m-0 text-center f-12">OR</h6>*/}
                                                            {/*</div>*/}
                                                            {/*<div className="flex-42_5">*/}
                                                            <button type="button" className="btn btn-dark" onClick={() => this.props.history.push('/signUp')}>
                                                                <i className="fas fa-user mr-2"></i>Don't have an account ? Sign Up Here !
                                                        </button>
                                                            {/*</div>*/}
                                                        </div>
                                                    </div>
                                                }
                                            </form>
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                    }

                    {this.state.isOpen &&
                        <NotificationModal
                            type={this.state.modalType}
                            message2={this.state.message2}
                            onCloseHandler={this.onCloseHandler}
                        />
                    }
                </React.Fragment>
            );
        }
    }
}

export function mapDispatchToProps(dispatch) {
    return {
        onLoginRequest: (username, password) => dispatch(Actions.loginInitiated(username, password)),
        fetchTenantDetails: (tenantName) => dispatch(Actions.fetchTenantDetails(tenantName)),
    };
}

const mapStateToProps = createStructuredSelector({
    isFetching: Selectors.getIsFetching(),
    forgotPasswordError: Selectors.forgotPasswordError(),
    forgotPasswordSuccess: Selectors.forgotPasswordSuccess(),
    loginSuccess: Selectors.loginSuccess(),
    loginFailure: Selectors.loginFailure(),
    getTenantDetailsSuccess: Selectors.getTenantDetailsSuccess(),
    getTenantDetailsFailure: Selectors.getTenantDetailsFailure(),
});

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);
const withReducer = injectReducer({ key: "login", reducer });
const withSaga = injectSaga({ key: "login", saga });

export default compose(
    withReducer,
    withConnect,
    withSaga
)(LoginPage);
