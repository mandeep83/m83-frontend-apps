/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/*
 *
 * AddOrEditDLExperiment constants
 *
 */


export const RESET_TO_INITIAL_STATE = "app/AddOrEditDLExperiment/RESET_TO_INITIAL_STATE";

export const CREATE_DL_SENTIMENT = {
    action: "app/AddOrEditDLExperiment/CREATE_DL_SENTIMENT",
    success: "app/AddOrEditDLExperiment/CREATE_DL_SENTIMENT_SUCCESS",
    failure: "app/AddOrEditDLExperiment/CREATE_DL_SENTIMENT_FAILURE",
    urlKey: "createDLSentiment",
    successKey: "createDLSentimentSuccess",
    failureKey: "createDLSentimentFailure",
    actionName: "createDLSentiment",
    actionArguments: ["payload"],
}

export const UPDATE_DL_SENTIMENT = {
    action: "app/AddOrEditDeepLearningExperiment/UPDATE_DL_SENTIMENT",
    success: "app/AddOrEditDeepLearningExperiment/UPDATE_DL_SENTIMENT_SUCCESS",
    failure: "app/AddOrEditDeepLearningExperiment/UPDATE_DL_SENTIMENT_FAILURE",
    urlKey: "updateDLSentiment",
    successKey: "updateDLSentimentSuccess",
    failureKey: "updateDLSentimentFailure",
    actionName: "updateDLSentiment",
    actionArguments: ["id", "payload"],
}

export const GET_DL_SENTIMENT_BY_ID = {
    action: "app/AddOrEditDLExperiment/GET_DL_SENTIMENT_BY_ID",
    success: "app/AddOrEditDLExperiment/GET_DL_SENTIMENT_BY_ID_SUCCESS",
    failure: "app/AddOrEditDLExperiment/GET_DL_SENTIMENT_BY_ID_FAILURE",
    urlKey: "getAllDLSentimentById",
    successKey: "getAllDLSentimentByIdSuccess",
    failureKey: "getAllDLSentimentByIdFailure",
    actionName: "getAllDLSentimentById",
    actionArguments: ["id"],
}

export const FILE_UPLOAD_HANDLER = {
	action: 'app/AddOrEditDLExperiment/FILE_UPLOAD_HANDLER',
	success: "app/AddOrEditDLExperiment/FILE_UPLOAD_HANDLER_SUCCESS",
	failure: "app/AddOrEditDLExperiment/FILE_UPLOAD_HANDLER_FAILURE",
	urlKey: "uploadFile",
	successKey: "fileUploadSuccess",
	failureKey: "fileUploadFailure",
	actionName: "uploadFile",
	actionArguments: ["payload", "addOns"]
}