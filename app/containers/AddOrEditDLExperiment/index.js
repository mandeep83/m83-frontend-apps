/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * AddOrEditDLExperiment
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { FormattedMessage } from "react-intl";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import { allActions as ACTIONS } from './actions';
import { allSelectors as SELECTORS } from "./selectors";
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import makeSelectAddOrEditDeepLearningExperiment from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import messages from "./messages";
import Loader from "../../components/Loader";
import ReactSelect from 'react-select';
import produce from "immer";
import TagsInput from 'react-tagsinput';
import 'react-tagsinput/react-tagsinput.css';
import cloneDeep from 'lodash/cloneDeep';
import ImageUploader from "../../components/ImageUploader";

/* eslint-disable react/prefer-stateless-function */

const EXPERIMENT_TYPES = [
  {
    value: "audioSentiment",
    label: "Audio Sentiment Analytics"
  },
  {
    value: "videoAnalytics",
    label: "Video Analytics"
  }
], AUDIO_SENTIMENT_ANALYTICS_PAYLOAD = {
  fileType: "audio",
  fileName: "", // For Uploaded File
  name: '',
  description: '',
  type: "audioSentiment",
  dataSource: {
    //no dirId anymore
    connectorCategory: "S3",
    fileUrl: ""   //For uploaded File Url
  },
  speechSentiment: {
    category: {}
  },
}, recognizeWithOptions = [
  {
    label: "Google",
    value: "google"
  },
  {
    label: "IBM",
    value: "ibm"
  },
  {
    label: "Sphinix",
    value: "sphinix"
  },
  {
    label: "Wit",
    value: "wit"
  }
], optionsForAudio = [
  {
    label: "True",
    value: true
  },
  {
    label: "False",
    value: false
  }
], defaultSelectedOption = {
  label: "False",
  value: false
} ;
export class AddOrEditDLExperiment extends React.Component {

  state = {
    isFetching: Boolean(this.props.match.params.id),
    payload: { ...AUDIO_SENTIMENT_ANALYTICS_PAYLOAD },
    activeTab: "dataConfiguration",
  }

  handleChange = (value, id) => {
    let payload = cloneDeep(this.state.payload);
    if (id === "type") {
      if (value === "audioSentiment") {
        payload = { ...AUDIO_SENTIMENT_ANALYTICS_PAYLOAD }
      } else {
        payload = { ...AUDIO_SENTIMENT_ANALYTICS_PAYLOAD }
      }
    } else {
      payload[id] = value;
    }
    this.setState({
      payload
    })
  }

  changeActiveTab = (activeTab) => {
    this.setState({
      activeTab
    })
  }
  handleAudioAnalyticsTransformation = (value, id) => {
    let payload = produce(this.state.payload, payload => {
      if (id === "wordDetect") {
        if (!value) {
          payload.speechSentiment.category = {}
        } else {
          payload.speechSentiment.category = {
            specificWords: [],
            categories: []
          }
        }
      } else if (id === "specificWords" || id === "categories") {
        payload.speechSentiment.category[id] = value
      } else {
        payload.speechSentiment[id] = value.value;
      }
    })
    this.setState({ payload })
  }

  saveDLSentiment = () => {
    let payload = cloneDeep(this.state.payload);

    if (!payload.name) {
      this.props.showNotification("error", "Experiment Name or DataSource cannot be empty");
      return;
    }
    this.setState({
      isFetching: true,
    }, () => {
      if (this.props.match.params.id) {

        this.props.updateDLSentiment(this.props.match.params.id, payload)

      }
      else
        this.props.createDLSentiment(payload)
    })
  }

  componentDidMount() {
    this.props.match.params.id && this.props.getAllDLSentimentById(this.props.match.params.id);
  }

  static getDerivedStateFromProps(nextProps, state) {
    let newProp = Object.keys(SELECTORS).find(prop => nextProps[prop])
    if (newProp) {
      let propData = nextProps[newProp].error ? nextProps[newProp].error : nextProps[newProp].response;

      if (nextProps.createDLSentimentSuccess) {
        nextProps.history.push('/dlExperiments');
        return {
          isFetching: false
        }
      }

      if (nextProps.createDLSentimentFailure) {
        return {
          isFetching: false,
        }
      }

      if (nextProps.getAllDLSentimentByIdSuccess) {
        return {
          isFetching: false,
          payload: propData.data
        }
      }

      if (nextProps.getAllDLSentimentByIdFailure) {
        return {
          isFetching: false
        }
      }
      if (nextProps.updateDLSentimentSuccess) {
        nextProps.history.push('/dlExperiments');
        return {
          isFetching: false
        }
      }
      if (nextProps.updateDLSentimentFailure) {
        return {
          isFetching: false
        }
      }

      if (nextProps.fileUploadSuccess) {
        let uploadedFileName = nextProps.fileUploadSuccess.response.addOns.fileName,
        uploadedFileUrl = nextProps.fileUploadSuccess.response.secureUrl,
        payload = produce(state.payload, payload => {
            payload.fileName = uploadedFileName
            payload.dataSource.fileUrl = uploadedFileUrl
        });
        return { isUploadingImage: false ,payload }
      }

      if (nextProps.fileUploadFailure) {
        return { isUploadingImage: false}
      }
    }
    return null;
  }

  componentDidUpdate(prevProps, prevState) {
    let newProp = Object.keys(SELECTORS).find(prop => this.props[prop])
    if (newProp) {
      if (newProp.includes("Failure")) {
        this.props.showNotification("error", this.props[newProp].error)
      }
      this.props.resetToInitialState(newProp)
    }
  }

  uploadFileHandler = ({ currentTarget }) => {
    this.setState({
      isUploadingImage: true
    }, () => this.props.uploadFile({ file: currentTarget.files[0], fileType: "DEEPLEARNING_EXPERIMENT" },{fileName: currentTarget.files[0].name}))
  }

  render() {
    let speechSentimentCategory = this.state.payload.speechSentiment.category;
    return (
      <React.Fragment>
        <Helmet>
          <title>AddOrEditDLExperiment</title>
          <meta
            name="description"
            content="Description of AddOrEditDLExperiment"
          />
        </Helmet>

        {this.state.isFetching ?
          <Loader /> :
          <React.Fragment>
            <header className="content-header d-flex">
              <div className="flex-60">
                <div className="d-flex">
                  <h6 className="previous" onClick={() => this.props.history.push('/dlExperiments')}>Deep Learning</h6>
                  <h6 className="active">Add New</h6>
                </div>
              </div>

              <div className="flex-40 text-right">
                <div className="content-header-group">
                  <button className="btn btn-light" data-tooltip="true" data-tooltip-text="Back" data-tooltip-place="bottom" onClick={() => this.props.history.push('/dlExperiments')}>
                    <i className="fas fa-angle-left"></i>
                  </button>
                </div>
              </div>
            </header>

            <div className="content-body">
              <div className="form-content-wrapper form-content-wrapper-left">
                <div id="tabs" className="action-tabs mb-3 position-sticky">
                  <ul className="nav nav-tabs list-style-none d-flex border-0">
                    <li className={`nav-item ${this.state.activeTab == "dataConfiguration" ? "active" : ""}`} data-toggle="tab" onClick={() => this.changeActiveTab("dataConfiguration")} href="#dataConfiguration">
                      <a className="nav-link">Data Configuration</a>
                    </li>
                    <li className={`nav-item ${this.state.activeTab == "cleaningTransformation" ? "active" : ""}`} data-toggle="tab" onClick={() => this.changeActiveTab("cleaningTransformation")} href="#cleaningTransformation">
                      <a className="nav-link">{"Cleaning & Transformation"}</a>
                    </li>
                  </ul>
                </div>
                {this.state.activeTab === "dataConfiguration" ?
                  <div className={`tab-pane ${this.state.activeTab == "dataConfiguration" ? "active" : ""}`} id="dataConfiguration">
                    {this.state.payload.type === "audioSentiment" ?
                      <React.Fragment>

                        <div className="d-flex">
                          <div className="flex-100">
                            <div className="card">
                              <div className="card-header"><h6>Data Configuration</h6></div>
                              <div className="card-body">
                                <div className="form-group max-width-65 mx-auto">
                                    <div className="file-upload-box">
                                    {this.state.isUploadingImage ?
                                        <div className="file-upload-loader">
                                            <img src="https://content.iot83.com/m83/misc/uploading.gif" />
                                            <p>Uploading...</p>
                                        </div> :
                                        this.state.payload.fileName ?
                                        <div className="file-upload-preview">
                                            <input type="file" name="excelUpload" onChange={this.uploadFileHandler} />
                                            <button type="button" className="btn-transparent btn-transparent-gray" data-tooltip data-tooltip-text="Update" data-tooltip-place="bottom">
                                                <i className="far fa-pen" />
                                            </button>
                                            <i class="far fa-file-pdf"></i>
                                            <p>{this.state.payload.fileName}</p>
                                        </div> :
                                        <div className="file-upload-form">
                                            <input required type="file" name="excelUpload" /* accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel text/plain" */ onChange={this.uploadFileHandler} />
                                            <p>Click to upload file...!</p>
                                        </div>
                                    }
                                    </div>
                                </div>

                                <h6 className="text-center form-text">Or</h6>

                                <div className="form-group max-width-65 mx-auto">
                                  <label className="form-group-label">S3 URL : </label>
                                  <input type="text" className="form-control" />
                                </div>
                              </div>
                            </div>
                          </div>

                        </div>



                      </React.Fragment>
                      : ""
                    }
                  </div>
                  :
                  <div className="tab-pane" id="cleaningTransformation">
                    <div className="d-flex">
                      {this.state.payload.type === "audioSentiment" ?
                        <div className="flex-100">
                          <div className="card">
                            <div className="card-header"><h6>Cleaning & Transformation</h6></div>
                            <div className="card-body d-flex">
                              <div className="form-group flex-50 pd-r-7">
                                <label className="form-group-label">Diarization : <i className="fas fa-asterisk form-group-required"></i> </label>
                                <ReactSelect className="form-control-multi-select"
                                  value={optionsForAudio.find(({ value }) => value === this.state.payload.speechSentiment.diarization)}
                                  options={optionsForAudio}
                                  onChange={(e) => this.handleAudioAnalyticsTransformation(e, "diarization")}
                                />
                              </div>

                              <div className="form-group flex-50 pd-l-7">
                                <label className="form-group-label">Recognize With : <i className="fas fa-asterisk form-group-required"></i> </label>
                                <ReactSelect className="form-control-multi-select"
                                  value={recognizeWithOptions.find(({ value }) => value === this.state.payload.speechSentiment.recognizeWith)}
                                  options={recognizeWithOptions}
                                  onChange={(e) => this.handleAudioAnalyticsTransformation(e, "recognizeWith")}
                                  isOptionDisabled={(option) => this.state.payload.speechSentiment.diarization ? (option.value !== "ibm") : false}
                                />
                              </div>

                              <div className="form-group flex-33 pd-r-7">
                                <label className="form-group-label">Remove Noise : </label>
                                <ReactSelect className="form-control-multi-select"
                                  value={optionsForAudio.find(({ value }) => value === this.state.payload.speechSentiment.noiseReduction) || defaultSelectedOption}
                                  options={optionsForAudio}
                                  onChange={(e) => this.handleAudioAnalyticsTransformation(e, "noiseReduction")}
                                />
                              </div>

                              <div className="form-group flex-33 pd-l-7 pd-r-7">
                                <label className="form-group-label">Polarity : </label>
                                <ReactSelect className="form-control-multi-select"
                                  value={optionsForAudio.find(({ value }) => value === this.state.payload.speechSentiment.polarity) || defaultSelectedOption}
                                  options={optionsForAudio}
                                  onChange={(e) => this.handleAudioAnalyticsTransformation(e, "polarity")}
                                />
                              </div>

                              <div className="form-group flex-33 pd-l-7">
                                <label className="form-group-label">Word Cloud : </label>
                                <ReactSelect className="form-control-multi-select"
                                  value={optionsForAudio.find(({ value }) => value === this.state.payload.speechSentiment.wordCloud) || defaultSelectedOption}
                                  options={optionsForAudio}
                                  onChange={(e) => this.handleAudioAnalyticsTransformation(e, "wordCloud")}
                                />
                              </div>

                              <div className="custom-control flex-100 custom-checkbox form-group form-group-headings ">
                                <input type="checkbox" className="custom-control-input" id="wordDetect" checked={speechSentimentCategory.specificWords} onChange={({ currentTarget }) => this.handleAudioAnalyticsTransformation(currentTarget.checked, "wordDetect")} />
                                <label className="custom-control-label" htmlFor="wordDetect">
                                  Word Detection <small>(Check Word Detection, If you want detect words.)</small>
                                </label>
                              </div>

                              {speechSentimentCategory.specificWords &&
                                <React.Fragment>
                                  <div className="form-group flex-50 pd-r-7">
                                    <label className="form-group-label">Existing Categories : </label>
                                    <select className="form-control">
                                      <option>Select</option>
                                      <option>Lorem Ipsum</option>
                                    </select>
                                  </div>

                                  <div className="form-group flex-50 pd-l-7">
                                    <label className="form-group-label">Add Categories : </label>
                                    <input type="text" className="form-control" />
                                  </div>

                                  <div className="position-relative flex-100 d-flex align-items-center">
                                    <div className="form-group flex-100">
                                      <label className="form-group-label">Specific Words : </label>
                                      <TagsInput value={this.state.payload.speechSentiment.category.specificWords || []} id="tag_select" onChange={(e) => this.handleAudioAnalyticsTransformation(e, "specificWords")} placeholder="Specific Words" />
                                    </div>
                                  </div>
                                </React.Fragment>
                              }
                            </div>
                          </div>
                        </div>
                        : ""}
                    </div>
                  </div>
                }

              </div>
              <form>
                <div className="form-info-wrapper form-info-wrapper-left">
                  <div className="form-info-header"><h5>Basic Information</h5></div>
                  <div className="form-info-body form-info-body-adjust">
                    <div className="form-group">
                      <label className="form-group-label">Experiment Type : <i className="fas fa-asterisk form-group-required"></i> </label>
                      <ReactSelect className="form-control-multi-select"
                        value={EXPERIMENT_TYPES.find(({ value }) => value === this.state.payload.type)}
                        options={EXPERIMENT_TYPES}
                        onChange={(e) => this.handleChange(e.value, "type")}
                      />
                    </div>

                    <div className="form-group">
                      <label className="form-group-label">Experiment Name : <i className="fas fa-asterisk form-group-required"></i> </label>
                      <input type="text" className="form-control" id="name" disabled={this.props.match.params.id} onChange={({ currentTarget }) => this.handleChange(currentTarget.value, "name")} value={this.state.payload.name} />
                    </div>

                    <div className="form-group">
                      <label className="form-group-label">Description :</label>
                      <textarea rows="3" type="text" id="description" className="form-control" onChange={({ currentTarget }) => this.handleChange(currentTarget.value, "description")} value={this.state.payload.description} />
                    </div>
                  </div>

                  <div className="form-info-footer">
                    <button type="button" className="btn btn-light" onClick={() => this.props.history.push('/dlExperiments')}>Cancel</button>
                    <button type="button" className="btn btn-primary" onClick={this.saveDLSentiment}>{this.props.match.params.id ? 'Update' : 'Save'}</button>
                  </div>
                </div>
              </form>
            </div>
          </React.Fragment>
        }

      </React.Fragment>
    );
  }
}

AddOrEditDLExperiment.propTypes = {
  dispatch: PropTypes.func.isRequired
};
let allSelectors = {}
Object.entries(SELECTORS).map(([key, value]) => {
  allSelectors[key] = value()
})

const mapStateToProps = createStructuredSelector(allSelectors);

function mapDispatchToProps(dispatch) {
  let allActions = { dispatch }
  Object.entries(ACTIONS).map(([key, value]) => {
    allActions[key] = (...args) => dispatch(value(...args))
  })
  return allActions;
}
const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

const withReducer = injectReducer({ key: "addOrEditDeepLearningExperiment", reducer });
const withSaga = injectSaga({ key: "addOrEditDeepLearningExperiment", saga });

export default compose(
  withReducer,
  withSaga,
  withConnect
)(AddOrEditDLExperiment);
