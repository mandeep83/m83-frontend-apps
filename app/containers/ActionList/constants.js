/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/*
 *
 * ActionList constants
 *
 */

export const RESET_TO_INITIAL_STATE = "app/ActionList/RESET_TO_INITIAL_STATE";

export const DEVELOPER_QUOTA = {
	action: 'app/ActionList/GET_DEVELOPER_QUOTA',
	success: "app/ActionList/GET_DEVELOPER_QUOTA_SUCCESS",
	failure: "app/ActionList/GET_DEVELOPER_QUOTA_FAILURE",
	urlKey: "getDeveloperQuota",
	successKey: "developerQuotaSuccess",
	failureKey: "developerQuotaFailure",
	actionName: "getDeveloperQuota",
	actionArguments: ["payload"]
}

export const GET_ACTIONS = {
	action: 'app/ActionList/GET_ACTIONS',
	success: "app/ActionList/GET_ACTIONS_SUCCESS",
	failure: "app/ActionList/GET_ACTIONS_FAILURE",
	urlKey: "getAllActions",
	successKey: "getActionsListSuccess",
	failureKey: "getActionsListFailure",
	actionName: "getActionsList",
	actionArguments: ["payload"]
}

export const CREATE_ACTION = {
	action: 'app/ActionList/CREATE_ACTION',
	success: "app/ActionList/CREATE_ACTION_SUCCESS",
	failure: "app/ActionList/CREATE_ACTION_FAILURE",
	urlKey: "createorEditAction",
	successKey: "createActionSuccess",
	failureKey: "createActionFailure",
	actionName: "createAction",
	actionArguments: ["payload"]
}

export const DELETE_ACTION = {
	action: 'app/ActionList/DELETE_ACTION',
	success: "app/ActionList/DELETE_ACTION_SUCCESS",
	failure: "app/ActionList/DELETE_ACTION_FAILURE",
	urlKey: "deleteAction",
	successKey: "deleteActionSuccess",
	failureKey: "deleteActionFailure",
	actionName: "deleteAction",
	actionArguments: ["id", "actionType"]
}

export const GET_DEVICE_TYPE_LIST = {
	action: 'app/ActionList/GET_DEVICE_TYPE_LIST',
	success: "app/ActionList/GET_DEVICE_TYPE_LIST_SUCCESS",
	failure: "app/ActionList/GET_DEVICE_TYPE_LIST_FAILURE",
	urlKey: "deviceTypeList",
	successKey: "getDeviceTypeListSuccess",
	failureKey: "getDeviceTypeListFailure",
	actionName: "getDeviceTypeList",
}

export const SAVE_RPC_ACTION = {
	action: 'app/ActionList/SAVE_RPC_ACTION',
	success: "app/ActionList/SAVE_RPC_ACTION_SUCCESS",
	failure: "app/ActionList/SAVE_RPC_ACTION_FAILURE",
	urlKey: "saveCommands",
	successKey: "saveRpcActionSuccess",
	failureKey: "saveRpcActionFailure",
	actionName: "saveRpcAction",
	actionArguments: ["payload", "isAddMode", "commandImage"]
}


export const DELETE_RPC_ACTION = {
	action: 'app/ActionList/DELETE_RPC_ACTION',
	success: "app/ActionList/DELETE_RPC_ACTION_SUCCESS",
	failure: "app/ActionList/DELETE_RPC_ACTION_FAILURE",
	urlKey: "deleteCommands",
	successKey: "deleteRpcActionSuccess",
	failureKey: "deleteRpcActionFailure",
	actionName: "deleteRpcAction",
	actionArguments: ["id"]
}

export const GET_USERS_EMAILS = {
	action: 'app/ActionList/GET_USERS_EMAILS',
	success: "app/ActionList/GET_USERS_EMAILS_SUCCESS",
	failure: "app/ActionList/GET_USERS_EMAILS_FAILURE",
	urlKey: "fetchUsersWithPagination",
	failureKey: "getEmailSuggestionsFailure",
	successKey: "getEmailSuggestionsSuccess",
	actionName: "getEmailSuggestions",
	actionArguments: ["payload"]
}
