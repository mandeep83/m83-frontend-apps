/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * ActionList
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { FormattedMessage } from "react-intl";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import Loader from '../../components/Loader/Loadable';
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import { allSelectors as SELECTORS } from "./selectors";
import { allActions as ACTIONS } from './actions'
import reducer from "./reducer";
import saga from "./saga";
import messages from "./messages";
import PhoneInput from "react-phone-input-2";
import 'react-phone-input-2/lib/style.css';
import JSONInput from "react-json-editor-ajrm/dist";
import SlidingPane from 'react-sliding-pane';
import produce from "immer"
import isPlainObject from 'lodash/isPlainObject'
import ListingTable from "../../components/ListingTable/Loadable";
import SearchBox from "../../components/SearchBox/Loadable";
import TagsWithSuggestions from "../../components/TagsWithSuggestions/Loadable";
import ImageUploader from "../../components/ImageUploader";
import Skeleton from "react-loading-skeleton";
import RpcImageUploader from "../../components/RpcImageUploader";

/* eslint-disable react/prefer-stateless-function */

let ACTION_TYPES = [
    {
        name: "sms",
        displayName: "SMS",
        className: "fad fa-comment-lines",
    }, {
        name: "email",
        displayName: "Email",
        className: "fad fa-envelope",
    }, {
        name: "rpc",
        displayName: "Controls",
        className: "fad fa-terminal",
    }, {
        name: "webhook",
        displayName: "Webhook",
        className: "fad fa-code-merge",
    }
]

let getWebhookHeaders = (headers) => {
    let webhookHeaders = Object.entries(headers).map(([key, value]) => {
        return {
            key,
            value
        }
    })
    return webhookHeaders
}

export class ActionList extends React.Component {
    state = {
        triggerType: "sms",
        deviceTypes: [],
        isCreateActionModalOpen: false,
        isFetchingActions: true,
        activeTab: "sms",
        actionsQuota: {},
        actionDetails: {
            name: "",
            metaData: {
                deviceTypeId: this.props.location.state ? this.props.location.state.deviceTypeId : null
            }
        },
        webhookHeaders: [],
        controlCommand: {},
        actions: {
            sms: [],
            email: [],
            rpc: [],
            webhook: []
        },
        searchedKeyword: "",
        emailSuggestions: [],
        controlImageUrl: ""
    }

    componentDidMount() {
        let payload = [
            {
                dependingProductId: null,
                productName: "sms"
            },
            {
                dependingProductId: null,
                productName: "email"
            },
            {
                dependingProductId: null,
                productName: "webhooks"
            },
            {
                dependingProductId: null,
                productName: "rpc"
            }
        ];
        this.props.getDeveloperQuota(payload);
        this.props.getDeviceTypeList();
    }

    getColumns = () => {
        const columns = {
            sms: [
                {
                    Header: "Action Name", width: 25,
                    cell: (row) => {
                        return <h6 className="text-theme fw-600">{row.name}</h6>
                    }
                },
                {
                    Header: "Mobile Number", width: 20,
                    cell: (row) => {
                        return (
                            row.metaData.numbers &&
                            <div className="list-phone-input">
                                <PhoneInput
                                    inputProps={{
                                        name: 'phone',
                                    }}
                                    disabled={true}
                                    disableDropdown={true}
                                    value={row.metaData.numbers[0].number}
                                    onChange={(value) => null}
                                />
                            </div>
                        )
                    }
                },
                {
                    Header: "Created", width: 20,
                    accessor: "createdAt"
                },
                {
                    Header: "Updated", width: 20,
                    accessor: "updatedAt"
                },
                {
                    Header: "Actions", width: 15,
                    cell: (row) => {
                        return this.getEditDeleteButtons(row)
                    }
                }
            ],
            email: [
                {
                    Header: "Action Name", width: 25,
                    cell: (row) => {
                        return <h6 className="text-theme fw-600">{row.name}</h6>
                    }
                },
                {
                    Header: "Email", width: 20,
                    cell: (row) => {
                        return (
                            <div className="list-view-pill-wrapper">
                                {row.metaData.emails && row.metaData.emails.map((email, index) =>
                                    <span className="badge badge-pill alert-success list-view-pill mr-1 mb-1" key={index}>{email}</span>
                                )}
                            </div>
                        )
                    }
                },
                {
                    Header: "Created", width: 20,
                    accessor: "createdAt"
                },
                {
                    Header: "Updated", width: 20,
                    accessor: "updatedAt"
                },
                {
                    Header: "Actions", width: 15,
                    cell: (row) => {
                        return this.getEditDeleteButtons(row)
                    }
                }
            ],
            rpc: [
                {
                    Header: "Action Name", width: 25,
                    cell: (row) => {
                        return (
                            <React.Fragment>
                                <div className="list-view-icon">{row.imageUrl ? <img src={row.imageUrl} /> : <i className="fad fa-joystick"></i>}</div>
                                <div className="list-view-icon-box">
                                    <h6 className="text-theme fw-600">{row.name}</h6>
                                </div>
                            </React.Fragment>
                        )
                    }
                },
                {
                    Header: "Device Type", width: 20,
                    cell: (row) => {
                        return (
                            <div className="button-group button-group-link">
                                <button className="btn btn-link" disabled={!row.metaData.deviceTypeId} onClick={() => { this.props.history.push(`/addOrEditDeviceType/${row.metaData.deviceTypeId}`) }}>{row.metaData.deviceTypeId && this.getDeviceTypeName(row.metaData.deviceTypeId)}</button>
                            </div>
                        )
                    }
                },
                {
                    Header: "Created", width: 20,
                    accessor: "createdAt"
                },
                {
                    Header: "Updated", width: 20,
                    accessor: "updatedAt"
                },
                {
                    Header: "Actions", width: 15,
                    cell: (row) => {
                        return this.getEditDeleteButtons(row)
                    }
                }
            ],
            webhook: [
                {
                    Header: "Action Name", width: 25,
                    cell: (row) => {
                        return <h6 className="text-theme fw-600">{row.name}</h6>
                    }
                },
                {
                    Header: "URL", width: 20,
                    cell: (row) => {
                        return (
                            <React.Fragment>
                                <a className="text-truncate d-block f-13" href={row.metaData.url} target="_blank" style={{ lineHeight: "15px" }}>{row.metaData.url}</a>
                                <p><strong>Method: </strong>
                                    <strong className={row.metaData.method == "GET" ? "text-primary" : row.metaData.method == "DELETE" ? "text-red" : row.metaData.method == "PUT" ? "text-yellow" : "text-green"}>{row.metaData.method}</strong>
                                </p>
                            </React.Fragment>
                        )
                    }
                },
                {
                    Header: "Created", width: 20,
                    accessor: "createdAt"
                },
                {
                    Header: "Updated", width: 20,
                    accessor: "updatedAt"
                },
                {
                    Header: "Actions", width: 15,
                    cell: (row) => {
                        return this.getEditDeleteButtons(row)
                    }
                }
            ]
        }
        return columns[this.state.activeTab]
    }

    static getDerivedStateFromProps(nextProps, state) {
        let newProp = Object.keys(SELECTORS).find(prop => nextProps[prop])
        if (newProp) {
            let propData = nextProps[newProp].error ? nextProps[newProp].error : nextProps[newProp].response
            if (nextProps.developerQuotaSuccess) {
                let actionsQuota = {}
                propData.map(quota => {
                    actionsQuota[quota.productName === "webhooks" ? "webhook" : quota.productName] = quota
                })
                return {
                    actionsQuota
                }
            }
            if (nextProps.getActionsListSuccess) {
                let stateObj = { actions: propData, isFetchingActions: false }
                let requiredActionType = nextProps.match.params.actionType
                if (requiredActionType) {
                    let requiredActionId = nextProps.match.params.actionId
                    if (requiredActionId) {
                        let requiredAction = propData[requiredActionType].find(action => action.id === requiredActionId)
                        stateObj.actionDetails = requiredAction
                        stateObj.webhookHeaders = requiredAction.triggerType === "webhook" ? getWebhookHeaders(requiredAction.metaData.headers) : []
                        stateObj.controlCommand = requiredAction.triggerType === "rpc" ? JSON.parse(requiredAction.metaData.controlCommand) : {}
                        stateObj.activeTab = requiredAction.triggerType
                    }

                    stateObj.triggerType = requiredActionType
                    stateObj.isCreateActionModalOpen = true
                }
                return stateObj
            }
            if (nextProps.getActionsListFailure) {
                return { isFetchingActions: false }
            }
            if (nextProps.createActionSuccess || nextProps.saveRpcActionSuccess) {
                let actionType = state.triggerType
                nextProps.showNotification("success", propData.message)
                let isNewAction = !state.actionDetails.id
                let actions = produce(state.actions, draftState => {
                    if (isNewAction) {
                        draftState[actionType].push(propData.data)
                    } else {
                        let editedActionIndex = draftState[actionType].findIndex(action => action.id === state.actionDetails.id)
                        draftState[actionType][editedActionIndex] = { ...state.actionDetails, ...propData.data }
                    }
                })

                return {
                    isSavingAction: false,
                    isCreateActionModalOpen: false,
                    activeTab: actionType,
                    actions,
                    controlImageUrl: "",
                    imageFile: "",
                }
            }
            if (nextProps.createActionFailure || nextProps.saveRpcActionFailure) {
                return {
                    isSavingAction: false
                }
            }
            if (nextProps.deleteActionSuccess || nextProps.deleteRpcActionSuccess) {
                let deletedActionId = propData.id
                let deletedActionType = propData.actionType

                const actions = produce(state.actions, draftState => {
                    draftState[deletedActionType] = draftState[deletedActionType].filter(action => action.id !== deletedActionId)
                })

                nextProps.showNotification("success", propData.message)
                return {
                    actions,
                    isFetchingActions: false
                }
            }
            if (nextProps.deleteActionFailure || nextProps.deleteRpcActionFailure) {
                return {
                    isFetchingActions: false
                }
            }
            if (nextProps.getDeviceTypeListSuccess) {
                nextProps.getActionsList(["sms", "email", "rpc", "webhook"]);
                return {
                    deviceTypes: propData,
                }
            }

            if (nextProps.getDeviceTypeListFailure) {
                nextProps.getActionsList(["sms", "email", "rpc", "webhook"]);
            }
            if (nextProps.getEmailSuggestionsSuccess) {
                return {
                    emailSuggestions: propData.users.map(user => user.email),
                    isFetchingSuggestions: false
                }
            }
        }
        return null
    }

    componentDidUpdate(prevProps, prevState) {
        let newProp = Object.keys(SELECTORS).find(prop => this.props[prop])
        if (newProp) {
            if (newProp.includes("Failure")) {
                this.props.showNotification("error", this.props[newProp].error)
            }
            this.props.resetToInitialState(newProp)
        }
    }

    refreshComponent = () => {
        this.setState({
            deviceTypes: [],
            isCreateActionModalOpen: false,
            isFetchingActions: true,
            actionsQuota: {},
            actionDetails: {
                name: "",
                metaData: {
                    deviceTypeId: this.props.location.state ? this.props.location.state.deviceTypeId : null
                }
            },
            webhookHeaders: [],
            controlCommand: {},
            actions: {
                sms: [],
                email: [],
                rpc: [],
                webhook: []
            }
        }, () => {
            // this.props.getActionsList(["sms", "email", "rpc", "webhook"]);
            let payload = [
                {
                    dependingProductId: null,
                    productName: "sms"
                },
                {
                    dependingProductId: null,
                    productName: "email"
                },
                {
                    dependingProductId: null,
                    productName: "webhooks"
                },
                {
                    dependingProductId: null,
                    productName: "rpc"
                }
            ];
            this.props.getDeveloperQuota(payload);
            this.props.getDeviceTypeList();
        })
    }

    addNewActionHandler = () => {
        this.setState({
            isAddOrEditActionList: true,
        })
    }

    getHeaderClassname = (tabType) => {
        return this.state.activeTab === tabType ? "nav-item flex-25 active" : "nav-item flex-25"
    }

    handleActiveTab = (activeTab) => {
        this.setState({
            activeTab
        })
    }

    getFormattedPhoneNo = (numberDetails) => {
        let countryCode = numberDetails.country.value.slice(1)
        let phoneNumber = numberDetails.number.replace(countryCode, "")
        return `+${countryCode}-${phoneNumber}`
    }

    editAction = (actionDetails) => {
        this.setState({
            isCreateActionModalOpen: true,
            actionDetails,
            triggerType: actionDetails.triggerType,
            webhookHeaders: actionDetails.triggerType === "webhook" ? getWebhookHeaders(actionDetails.metaData.headers) : [],
            controlCommand: actionDetails.triggerType === "rpc" ? JSON.parse(actionDetails.metaData.controlCommand) : {},
            controlImageUrl: actionDetails.triggerType === "rpc" ? actionDetails.imageUrl : "",
        })
    }

    deleteAction = (actionId) => {
        this.setState({
            isFetchingActions: true
        }, () => {
            let actionName = this.state.activeTab === "rpc" ? "deleteRpcAction" : "deleteAction"
            this.props[actionName](actionId, this.state.activeTab)
        })
    }
    table = React.createRef();

    confirmActionDeletion = ({ id, name }) => {
        const confirmationMessage = name;
        const actionId = id;
        let confirmationHandler = () => {
            this.deleteAction(actionId)
        }
        this.props.showConfirmationModal(confirmationMessage, confirmationHandler)
    }

    getEditDeleteButtons = (action) => {
        return <div className="button-group">
            <button className="btn-transparent btn-transparent-blue" data-tooltip data-tooltip-text="Edit" data-tooltip-place="bottom" onClick={() => this.editAction(action)}>
                <i className="far fa-pencil"></i>
            </button>
            <button className="btn-transparent btn-transparent-red" data-tooltip data-tooltip-text="Delete" data-tooltip-place="bottom" onClick={() => this.confirmActionDeletion(action)}>
                <i className="far fa-trash-alt"></i>
            </button>
        </div>
    }

    getDeviceTypeName = (requiredDeviceId) => {
        let requiredDeviceType = this.state.deviceTypes.find(deviceType => deviceType.id === requiredDeviceId)
        return requiredDeviceType && requiredDeviceType.name || requiredDeviceId
    }

    getActionNameInputBox = () => {
        return (
            <div className="form-group">
                <label className="form-group-label">Action Name : <i className="fas fa-asterisk form-group-required"></i></label>
                <input type="text" id="name" className="form-control" value={this.state.actionDetails.name} onChange={this.actionNameChangeHandler} />
            </div>
        )
    }

    getFilteredActionsList = () => {
        return this.state.actions[this.state.activeTab].filter(action => action.name.toLowerCase().includes(this.state.searchedKeyword.toLowerCase()))
    }

    getHTMLForActionList = () => {
        if (!this.state.actions[this.state.activeTab] || !this.state.actions[this.state.activeTab].length) {
            let activeTabDisplayName = ACTION_TYPES.find(actionType => actionType.name === this.state.activeTab).displayName
            return (
                <div className="action-tab-message">
                    <div className="content-add-wrapper">
                        <div className="content-add-bg"><img src="https://content.iot83.com/m83/misc/m83-logo.png" />
                        </div>
                        <div className="content-add-detail">
                            <div className="content-add-image-outer">
                                <div className="content-add-image">
                                    <img src="https://content.iot83.com/m83/misc/noData.png" />
                                </div>
                            </div>
                            <h5>No {activeTabDisplayName} action found</h5>
                            <h6>You haven't create any {activeTabDisplayName} action yet</h6>
                        </div>
                    </div>
                </div>
            )
        }
        return <ListingTable
            columns={this.getColumns()}
            data={this.state.searchedKeyword ? this.getFilteredActionsList() : this.state.actions[this.state.activeTab]}
            ref={this.table}
            id={this.state.activeTab}
        />
    }

    setControlImageDetails = (controlImageDetails) => {
        this.setState(controlImageDetails)
    }

    getRPCInputs = () => {
        return (
            <React.Fragment>
                <div className="form-group">
                    <label className="form-group-label">Device Type :<i className="fas fa-asterisk form-group-required"></i></label>
                    <select className="form-control" disabled={this.state.actionDetails.id} value={this.state.actionDetails.metaData.deviceTypeId || ""} onChange={({ currentTarget }) => this.actionMetaDataChangeHandler(currentTarget.value, "deviceTypeId")}>
                        <option value="">Select</option>
                        {this.state.deviceTypes.map(deviceType =>
                            <option key={deviceType.id} value={deviceType.id} name={deviceType.name}>{deviceType.name}</option>
                        )}
                    </select>
                </div>
                <RpcImageUploader
                    controlImageUrl={this.state.controlImageUrl}
                    setControlImageDetails={this.setControlImageDetails}
                    controlCommandId={this.state.actionDetails.id}
                    onDeleteImage={this.onDeleteImage}
                    deleteRpcImageHandler={this.deleteRpcImageHandler}
                    deletionErrorCallBack={this.RPCImagedeletionErrorCallBack}
                />
            </React.Fragment>
        )
    }

    getSmsActionForm = () => {
        return (
            <React.Fragment>
                <div className="form-group">
                    <label className="form-group-label">Mobile No. : <i className="fas fa-asterisk form-group-required"></i></label>
                    <div className="phone-input">
                        <PhoneInput
                            className="form-control"
                            inputExtraProps={{
                                name: 'phone',
                                required: true,
                                autoFocus: true
                            }}
                            country='us'
                            value={this.state.actionDetails.metaData.numbers && this.state.actionDetails.metaData.numbers[0].number}
                            onChange={this.telChangeHandler}
                        />
                    </div>
                </div>
                <div className="form-group">
                    <label className="form-group-label">Message Format :</label>
                    <div className="action-info bg-light-gray">
                        <h5 className="text-red fw-600">ALERT!</h5>
                        <p>Attribute(s) threshold violated for Device Type - <strong>XYZ</strong></p>
                        <p><strong>(X)</strong> - Devices affected.</p>
                        <p>Please take necessary action .</p>
                        <p>Check web portal for more details.</p>
                    </div>
                </div>
            </React.Fragment>
        )
    }

    fetchEmailSuggestions = (value) => {
        clearTimeout(this.suggestionsTimeout)
        this.setState({
            emailSuggestions: [],
            isFetchingSuggestions: Boolean(value)
        }, () => {
            if (value)
                this.suggestionsTimeout = setTimeout(() => this.props.getEmailSuggestions({ email: value }), 1000)
        })
    }

    getEmailActionForm = () => {
        return (
            <React.Fragment>
                <div className="form-group">
                    <label className="form-group-label">Recipient Emails : <i className="fas fa-asterisk form-group-required"></i>
                        <span className="text-cyan f-10 float-right"><strong>Note:</strong> Please press enter(&crarr;) to add email.</span>
                    </label>
                    <div className="search-wrapper">
                        <TagsWithSuggestions
                            selectedTags={this.state.actionDetails.metaData.emails || []}
                            suggestionData={this.state.emailSuggestions}
                            fetchSuggestions={this.fetchEmailSuggestions}
                            isLoading={this.state.isFetchingSuggestions}
                            onAddition={(tags) => this.actionMetaDataChangeHandler(tags, "emails")}
                            validationRegex={{
                                regex: /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/,
                                validationMessage: "Invalid Email"
                            }}
                            allowNewEntries
                            allowOnlyUnique
                            showNotification={this.props.showNotification}
                            fetchSuggestionsFromServer
                            autoFocus
                        />
                    </div>
                </div>
                <div className="form-group">
                    <label className="form-group-label">Message Format :</label>
                    <div className="action-info bg-light-gray">
                        <h5 className="text-theme fw-600">Hi, User</h5>
                        <p>As per your created rules, on the platform for your Device Type - <strong>XYZ</strong>, it is observed that the Device Id <strong>(X)</strong> has some alarm / anomalies that need some attention.</p>
                        <p>Please take necessary action.</p>
                    </div>
                </div>
            </React.Fragment>
        )
    }

    getRpcActionForm = () => {
        return (
            <div className="form-group">
                <div className="json-input-box">
                    <JSONInput
                        theme="light_mitsuketa_tribute"
                        value={this.state.controlCommand}
                        onChange={this.commandChangeHandler}
                        placeholder={this.state.controlCommand}
                    />
                </div>
            </div>
        )
    }

    getWebhookActionForm = () => {
        return (
            <React.Fragment>
                <div className="form-group">
                    <label className="form-group-label">Request URL :<i className="fas fa-asterisk form-group-required"></i></label>
                    <div className="input-group">
                        <div className="input-group-prepend">
                            <select className="form-control input-group-text" id="method" required value={this.state.actionDetails.metaData.method || ""} onChange={this.actionMetaDataMethodChangeHandler}>
                                <option value="">SELECT</option>
                                <option value="GET">GET</option>
                                <option value="DELETE">DELETE</option>
                                <option value="PUT">PUT</option>
                                <option value="POST">POST</option>
                            </select>
                        </div>
                        <input type="text" id="url" className="form-control" placeholder="https://" value={this.state.actionDetails.metaData.url || ""} onChange={({ currentTarget }) => this.actionMetaDataChangeHandler(currentTarget.value, "url")} />
                    </div>
                </div>
                {["PUT", "POST"].includes(this.state.actionDetails.metaData.method) && <div className="form-group">
                    <div>
                        <JSONInput
                            placeholder={this.state.actionDetails.metaData.payload}
                            value={this.state.actionDetails.metaData.payload}
                            onChange={this.changeHandlerEditor} />
                    </div>
                </div>
                }
                <div className="form-group action-add-box">
                    {this.state.webhookHeaders.length > 0 &&
                        <React.Fragment>
                            <div className="d-flex">
                                <div className="flex-42_5 pd-r-10">
                                    <label className="form-group-label">Key :</label>
                                </div>
                                <div className="flex-42_5 pd-l-5 pd-r-5">
                                    <label className="form-group-label">Value :</label>
                                </div>
                                <div className="flex-15 pd-l-10">
                                    <label className="form-group-label">Action :</label>
                                </div>
                            </div>
                            {this.state.webhookHeaders.map((header, index) =>
                                <div className="d-flex mb-2" key={index}>
                                    <div className="flex-42_5 pd-r-10">
                                        <input type="text" id="key" className="form-control"
                                            value={header.key} onChange={({ currentTarget }) => this.webhookHeaderChangeHandler(index, "key", currentTarget.value)}
                                        />
                                    </div>
                                    <div className="flex-42_5 pd-l-5 pd-r-5">
                                        <input type="text" id="value" className="form-control"
                                            value={header.value} onChange={({ currentTarget }) => this.webhookHeaderChangeHandler(index, "value", currentTarget.value)}
                                        />
                                    </div>
                                    <div className="flex-15 pd-l-10">
                                        <button type="button" className="btn-transparent btn-transparent-red mt-2" data-tooltip data-tooltip-text="Delete" data-tooltip-place="bottom" onClick={() => this.deleteWebhookHeader(index)}>
                                            <i className="far fa-trash-alt"></i>
                                        </button>
                                    </div>
                                </div>
                            )}
                        </React.Fragment>
                    }
                    <div className="text-center">
                        <button type="button" className="btn btn-link" onClick={this.addWebhookHeader}><i className="far fa-plus"></i>Add Header</button>
                    </div>
                </div>
            </React.Fragment>
        )
    }

    getHTMLForActionDetailsModal = () => {
        switch (this.state.triggerType) {
            case "sms":
                return this.getSmsActionForm()
            case "email":
                return this.getEmailActionForm()
            case "rpc":
                return this.getRpcActionForm()
            case "webhook":
                return this.getWebhookActionForm()
        }
    }

    commandChangeHandler = ({ jsObject }) => {
        this.setState({
            controlCommand: jsObject
        })
    }

    toggleCreateActionModal = () => {
        this.setState(prevState => ({
            isCreateActionModalOpen: !prevState.isCreateActionModalOpen,
            actionDetails: {
                name: "",
                metaData: {}
            },
            triggerType: prevState.activeTab,
            webhookHeaders: [],
            controlCommand: {},
            imageFile: "",
            controlImageUrl: "",
        }))
    }

    actionMetaDataMethodChangeHandler = ({ currentTarget }) => {
        let value = currentTarget.value
        let actionDetails = produce(this.state.actionDetails, draftState => {
            draftState.metaData.method = value;
            if ((value === "PUT" || value === "POST") && !draftState.metaData.payload)
                draftState.metaData.payload = {}
            else if (!value || value === "GET" || value === "DELETE")
                delete draftState.metaData.payload
        })
        this.setState({ actionDetails })
    }

    actionMetaDataChangeHandler = (value, key) => {
        let actionDetails = produce(this.state.actionDetails, draftState => {
            draftState.metaData[key] = key === 'url' ? value.trim() : value
        })
        this.setState({ actionDetails })
    }

    actionNameChangeHandler = ({ currentTarget }) => {
        let actionDetails = produce(this.state.actionDetails, draftState => {
            draftState.name = currentTarget.value.trim()
        })
        this.setState({ actionDetails })
    }

    telChangeHandler = (value, country) => {
        let actionDetails = produce(this.state.actionDetails, draftState => {
            draftState.metaData.numbers = [{
                number: value,
                country: {
                    value: "+" + country.dialCode,
                    label: country.name
                }
            }]
        })
        this.setState({ actionDetails })
    }

    actionTypeChangeHandler = ({ currentTarget }) => {
        this.setState({
            actionDetails: {
                name: "",
                metaData: {}
            },
            triggerType: currentTarget.value
        })
    }

    saveRpcAction = () => {
        let payload = {
            controlCommand: JSON.stringify(this.state.controlCommand),
            controlName: this.state.actionDetails.name.trim(),
            deviceTypeId: this.state.actionDetails.metaData.deviceTypeId,
            id: this.state.actionDetails.id
        }
        let actionDetails = produce(this.state.actionDetails, draftState => {
            draftState.triggerType = this.state.triggerType
            draftState.metaData.controlCommand = JSON.stringify(this.state.controlCommand)
        })
        this.setState({
            isSavingAction: true,
            actionDetails,
        }, () => this.props.saveRpcAction(payload, null, this.state.imageFile))
    }

    saveActionHandler = () => {
        if (this.state.actionDetails.name.includes(" ")) {
            return this.props.showNotification("error", "Action name should not include spaces.")
        }
        if (this.state.triggerType === "rpc") {
            return this.saveRpcAction()
        }
        let payload = produce(this.state.actionDetails, draftState => {
            draftState.triggerType = this.state.triggerType
            draftState.name = draftState.name.trim();
            if (draftState.triggerType === "webhook") {
                draftState.metaData.headers = {}
                this.state.webhookHeaders.map(header => {
                    draftState.metaData.headers[header.key] = header.value
                })
            }
        })
        this.setState({
            isSavingAction: true,
            actionDetails: payload
        }, () => this.props.createAction(payload))
    }

    actionSaveButtonValidation = () => {
        switch (this.state.triggerType) {
            case "sms":
                return !(this.state.actionDetails.name.trim() && this.state.actionDetails.metaData.numbers && this.state.actionDetails.metaData.numbers[0].number)
            case "email":
                return !(this.state.actionDetails.name.trim() && this.state.actionDetails.metaData.emails && this.state.actionDetails.metaData.emails.length)
            case "rpc":
                return !(this.state.actionDetails.name.trim() && this.state.actionDetails.metaData.deviceTypeId && isPlainObject(this.state.controlCommand) && Object.keys(this.state.controlCommand).length)
            case "webhook":
                return !(this.state.actionDetails.name.trim() && this.state.actionDetails.metaData.method && this.state.actionDetails.metaData.url && (!["PUT", "POST"].includes(this.state.actionDetails.metaData.method) || typeof this.state.actionDetails.metaData.payload == "object"))
        }
    }

    addWebhookHeader = () => {
        let webhookHeaders = produce(this.state.webhookHeaders, draftState => {
            draftState.push({
                key: "",
                value: "",
            })
        })
        this.setState({ webhookHeaders })
    }

    deleteWebhookHeader = (index) => {
        let webhookHeaders = produce(this.state.webhookHeaders, draftState => {
            draftState.splice(index, 1)
        })
        this.setState({ webhookHeaders })
    }

    webhookHeaderChangeHandler = (index, keyName, value) => {
        let webhookHeaders = produce(this.state.webhookHeaders, draftState => {
            if (keyName == "key") {
                if (/^[a-zA-Z0-9-_]+$/.test(value) || /^$/.test(value))
                    draftState[index][keyName] = value
            }
            else {
                draftState[index][keyName] = value
            }
        })
        this.setState({ webhookHeaders })
    }

    changeHandlerEditor = (code) => {
        let actionDetails = produce(this.state.actionDetails, draftState => {
            draftState.metaData.payload = code.jsObject
        })
        this.setState({ actionDetails })
    }

    searchBoxChangeHandler = (searchedKeyword) => {
        this.setState({ searchedKeyword })
    }

    deleteRpcImageHandler = () => {
        this.setState({
            isSavingAction: true
        })
    }

    onDeleteImage = () => {
        let actions = produce(this.state.actions, actions => {
            let requiredAction = actions[this.state.activeTab].find(action => action.id === this.state.actionDetails.id)
            requiredAction.imageUrl = ""
        })
        this.setState(prevState => ({
            isCreateActionModalOpen: false,
            actionDetails: {
                name: "",
                metaData: {}
            },
            triggerType: prevState.activeTab,
            webhookHeaders: [],
            controlCommand: {},
            imageFile: "",
            controlImageUrl: "",
            actions,
            isSavingAction: false,
        }), () => this.props.showNotification("success", "Image Reset successfully."))
    }

    RPCImagedeletionErrorCallBack = (errorMessage)=> {
        this.setState({
            isSavingAction: false,
        }, () => this.props.showNotification("error", errorMessage))
    }

    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>Actions</title>
                    <meta name="description" content="Description of ActionList" />
                </Helmet>

                <header className="content-header d-flex">
                    <div className="flex-35">
                        <h6>Actions</h6>
                    </div>
                    <div className="flex-65 text-right">
                        <div className="content-header-group">
                            <span className="text-content f-11 mr-2"><strong className="text-cyan"><sup>*</sup>Note :</strong> You can create unlimited actions. <br/> Quota is decided when these actions are consumed by the event.</span>
                            <SearchBox onChangeHandler={this.searchBoxChangeHandler} value={this.state.searchedKeyword} isDisabled={!this.state.actions[this.state.activeTab] || !this.state.actions[this.state.activeTab].length || this.state.isFetchingActions} />
                            <button className="btn btn-light" disabled={this.state.isFetchingActions} data-tooltip data-tooltip-text="Refresh" data-tooltip-place="bottom" onClick={() => this.refreshComponent()}><i className="far fa-sync-alt"></i></button>
                            <button className="btn btn-primary" disabled={this.state.isFetchingActions} onClick={this.toggleCreateActionModal} data-tooltip data-tooltip-text="Add Action" data-tooltip-place="bottom"><i className=" far fa-plus"></i></button>
                        </div>
                    </div>
                </header>

                <div className="content-body">
                    <div className="action-tabs-box">
                        <ul className="nav nav-tabs list-style-none d-flex action-list">
                            {ACTION_TYPES.map(actiontype => {
                                let actionTypeQuota = this.state.actionsQuota[actiontype.name]
                                return (
                                    <li key={actiontype.name} className={this.getHeaderClassname(actiontype.name)}>
                                        <div className="action-list-box" onClick={() => this.handleActiveTab(actiontype.name)}>
                                            <span className="action-tab-icon"><i className={actiontype.className}></i></span>
                                            <h6>{actiontype.displayName}</h6>
                                            <div className="action-badge-group">
                                                <p>Allocated : <span className="badge badge-pill badge-primary">{actionTypeQuota ? actionTypeQuota.total : 0}</span></p>
                                                <p>Used : <span className="badge badge-pill badge-success">{actionTypeQuota ? actionTypeQuota.used : 0}</span></p>
                                                <p>Remaining : <span className="badge badge-pill badge-warning">{actionTypeQuota ? actionTypeQuota.remaining : 0}</span></p>
                                            </div>
                                        </div>
                                    </li>
                                )
                            })}
                        </ul>

                        {this.state.isFetchingActions ? <Skeleton count={7} className="skeleton-list-loader" /> :
                            <div className="tab-content">
                                <div className="tab-pane fade show active" id="tab1">
                                    {this.getHTMLForActionList()}
                                    
                                </div>
                            </div>
                        }
                    </div>
                </div>

                {/* add action modal */}
                <SlidingPane
                    className=''
                    overlayClassName='sliding-form'
                    closeIcon={<div></div>}
                    isOpen={this.state.isCreateActionModalOpen}
                    from='right'
                    width='500px'
                >
                    <div className="modal-content">
                        <div className="modal-header">
                            <h6 className="modal-title">{this.state.actionDetails.id ? "Update" : "Add"} Actions
                                <button className="btn btn-light close" onClick={this.toggleCreateActionModal}>
                                    <i className="fas fa-angle-right"></i>
                                </button>
                            </h6>
                        </div>
                        {this.state.isSavingAction ?
                            <div className="modal-loader">
                                <i className="fad fa-sync-alt fa-spin"></i>
                            </div> :
                            <div className="modal-body">
                                <div className="form-group">
                                    <label className="form-group-label">Select Action :</label>
                                    <div className="d-flex">
                                        {ACTION_TYPES.map(actionType =>
                                            <div key={actionType.name} className="flex-25 pd-r-10">
                                                <label className={"radio-button " + (this.state.actionDetails.id ? "radio-button-disabled" : "")}>
                                                    <span className="radio-button-text">{actionType.displayName}</span>
                                                    <input type="radio" disabled={this.state.actionDetails.id} name="connectorType" value={actionType.name} checked={this.state.triggerType === actionType.name} onChange={this.actionTypeChangeHandler} />
                                                    <span className="radio-button-mark"></span>
                                                </label>
                                            </div>
                                        )}
                                    </div>
                                </div>

                                <div className="form-info-body form-info-body-adjust">
                                    {this.getActionNameInputBox()}
                                    {this.state.triggerType === "rpc" && this.getRPCInputs()}
                                    {this.getHTMLForActionDetailsModal()}
                                </div>
                            </div>
                        }
                        <div className="modal-footer justify-content-start">
                            <button className="btn btn-primary" disabled={this.actionSaveButtonValidation()} onClick={this.saveActionHandler}>{this.state.actionDetails.id ? "Update" : "Save"}</button>
                            <button type="button" className="btn btn-dark" onClick={this.toggleCreateActionModal}>Cancel</button>
                        </div>
                    </div>
                </SlidingPane>
                {/* end add action modal */}
            </React.Fragment>
        );
    }
}

ActionList.propTypes = {
    dispatch: PropTypes.func.isRequired
};

let allSelectors = {}
Object.entries(SELECTORS).map(([key, value]) => {
    allSelectors[key] = value()
})

const mapStateToProps = createStructuredSelector(allSelectors);


function mapDispatchToProps(dispatch) {
    let allActions = { dispatch }
    Object.entries(ACTIONS).map(([key, value]) => {
        allActions[key] = (...args) => dispatch(value(...args))
    })
    return allActions;
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "actionList", reducer });
const withSaga = injectSaga({ key: "actionList", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(ActionList);
