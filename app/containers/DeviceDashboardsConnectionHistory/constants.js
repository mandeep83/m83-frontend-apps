/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/*
 *
 * DeviceDashboardsConnectionHistory constants
 *
 */

export const RESET_TO_INITIAL_STATE = "app/DeviceDashboardsConnectionHistory/RESET_TO_INITIAL_STATE";
export const RESET_TO_INITIAL_STATE_PROPS = "app/DeviceDashboardsConnectionHistory/RESET_TO_INITIAL_STATE_PROPS";


export const GET_DEVICE_TYPE_LIST = {
  action: 'app/DeviceDashboardsConnectionHistory/GET_DEVICE_TYPE_LIST',
  success: "app/DeviceDashboardsConnectionHistory/GET_DEVICE_TYPE_LIST_SUCCESS",
  failure: "app/DeviceDashboardsConnectionHistory/GET_DEVICE_TYPE_LIST_FAILURE",
  urlKey: "deviceTypeList",
  successKey: "deviceTypeListSuccess",
  failureKey: "deviceTypeListFailure",
  actionName: "deviceTypeList",
  actionArguments: []
}

export const GET_GROUP_DATA_BY_CONNECTOR = {
  action: 'app/DeviceDashboardsConnectionHistory/GET_GROUP_DATA_BY_CONNECTOR',
  success: "app/DeviceDashboardsConnectionHistory/GET_GROUP_DATA_BY_CONNECTOR_SUCCESS",
  failure: "app/DeviceDashboardsConnectionHistory/GET_GROUP_DATA_BY_CONNECTOR_FAILURE",
  urlKey: "getGroupDataByConnector",
  successKey: "getGroupDataByConnectorSuccess",
  failureKey: "getGroupDataByConnectorFailure",
  actionName: "getGroupDataByConnector",
  actionArguments: ["payload","duration"]
}

export const NOTIFICATION_STATUS = {
  action: 'app/DeviceDashboardsConnectionHistory/GET_NOTIFICATION_STATUS',
  success: "app/DeviceDashboardsConnectionHistory/GET_NOTIFICATION_STATUS_SUCCESS",
  failure: "app/DeviceDashboardsConnectionHistory/GET_NOTIFICATION_STATUS_FAILURE",
  urlKey: "getNotificationStatus",
  successKey: "notificationStatusSuccess",
  failureKey: "notificationStatusFailure",
  actionName: "getNotificationStatus",
  actionArguments: []
}
export const NOTIFICATION_STATUS_CHANGE = {
  action: 'app/DeviceDashboardsConnectionHistory/NOTIFICATION_STATUS_CHANGE',
  success: "app/DeviceDashboardsConnectionHistory/NOTIFICATION_STATUS_CHANGE_SUCCESS",
  failure: "app/DeviceDashboardsConnectionHistory/NOTIFICATION_STATUS_CHANGE_FAILURE",
  urlKey: "changeNotificationStatus",
  successKey: "notificationStatusChangeSuccess",
  failureKey: "notificationStatusChangeFailure",
  actionName: "changeNotificationStatus",
  actionArguments: ["payload"]
}
