/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * DeviceDashboardsConnectionHistory
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { createStructuredSelector } from "reselect";
import ReactSelect from "react-select";
import { compose } from "redux";

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import reducer from "./reducer";
import saga from "./saga";
import { cloneDeep, isEqual } from "lodash";
import { allSelectors as SELECTORS } from "./selectors";
import { allActions as ACTIONS } from './actions'
import AddNewButton from "../../components/AddNewButton/Loadable"
import Loader from "../../components/Loader/Loadable";
import CollapseOrShowAllFilters from "../../components/CollapseOrShowAllFilters";
import ConnectionHistoryChart from "../../components/ConnectionHistoryChart";
import {isNavAssigned, DURATION_OPTIONS} from "../../commonUtils";

/* eslint-disable react/prefer-stateless-function */

export class DeviceDashboardsConnectionHistory extends React.Component {

    state = {
        filterExpand: true,
        deviceTypeList: [],
        isFetching: true,
        deviceConnectivityData: [
            {
                "header": "Device - Connected/Disconnected by %",
                "data": [],
                "isLoading": false
            }
        ],
        selectedDeviceType: [],
        selectedDuration: 10080,
        counter: {
            connected: 0,
            disconnected: 0,
            allDevices: 0
        },
        isNotificationEnabled: false,
        notificationChangeInProgress: false,
        selectedGraph: {}
    }

    refreshComponent = () => {
        this.setState({
            deviceTypeList: [],
            isFetching: true,
            deviceConnectivityData: [
                {
                    "header": "Device - Connected/Disconnected by %",
                    "data": [],
                    "isLoading": false
                }
            ],
            selectedDeviceType: [],
            selectedDuration: "10080",
            counter: {
                connected: 0,
                disconnected: 0,
                allDevices: 0
            }
        }, () => {
            this.props.deviceTypeList()
        })
    }

    shouldComponentUpdate(nextProps) {
        return !isEqual(nextProps.widgetData, this.state.deviceConnectivityData)
    }

    componentDidMount() {
        this.props.deviceTypeList();
        this.props.getNotificationStatus();
    }

    static getDerivedStateFromProps(nextProps, state) {
        if (nextProps.deviceTypeListSuccess) {
            let deviceTypeList = nextProps.deviceTypeListSuccess.response,
                selectedDeviceType = deviceTypeList.length ? deviceTypeList.map(type => { return { label: type.name, value: type.id } }) : null,
                deviceConnectivityData = [];
            if (selectedDeviceType) {
                deviceTypeList.map(devices => {
                    if (selectedDeviceType.some(selectedDevice => selectedDevice.value === devices.id)) {
                        devices.deviceGroups.map(group => {
                            nextProps.getGroupDataByConnector({
                                deviceGroupId: group.deviceGroupId,
                                deviceTypeId: devices.id
                            }, state.selectedDuration)
                            deviceConnectivityData.push({
                                "deviceId": devices.id,
                                "deviceName": devices.name,
                                "header": `Devices for  ${group.name} (Group) - Connected/Disconnected by %`,
                                "groupId": group.deviceGroupId,
                                "groupName": group.name,
                                "data": [],
                                "isLoading": true,
                                "errorOccurred": false,
                            })
                        })
                    }
                })
            }
            return {
                deviceTypeList,
                isFetching: false,
                deviceConnectivityData,
                selectedDeviceType: selectedDeviceType || []
            }
        }

        if (nextProps.deviceTypeListFailure) {
            return { isFetching: false }
        }

        if (nextProps.getGroupDataByConnectorSuccess) {
            let counter = {
                connected: 0,
                disconnected: 0,
                allDevices: 0
            }
            let deviceConnectivityData = cloneDeep(state.deviceConnectivityData);
            deviceConnectivityData.map(device => {
                if (device.groupId === nextProps.getGroupDataByConnectorSuccess.payload.deviceGroupId) {
                    device.data = nextProps.getGroupDataByConnectorSuccess.response
                    device.isLoading = false
                }
                if (device.data.length) {
                    let lastRecord = device.data[device.data.length - 1];
                    counter.connected += lastRecord.connected;
                    counter.disconnected += lastRecord.disconnected;
                }
            })
            counter.allDevices += counter.connected + counter.disconnected
            return {
                deviceConnectivityData,
                counter
            }
        }

        if (nextProps.getGroupDataByConnectorFailure) {
            let deviceConnectivityData = cloneDeep(state.deviceConnectivityData);
            deviceConnectivityData.map(device => {
                if (device.groupId === nextProps.getGroupDataByConnectorFailure.addOns.payload.deviceGroupId) {
                    device.errorOccurred = true
                    device.isLoading = false
                }
            })
            return {
                deviceConnectivityData
            }
        }

        if (nextProps.notificationStatusSuccess) {
            return {
                isNotificationEnabled: nextProps.notificationStatusSuccess.response.enableDeviceNotifications
            }
        }

        if (nextProps.notificationStatusChangeSuccess) {
            nextProps.showNotification("success", nextProps.notificationStatusChangeSuccess.response.message);
            return {
                isNotificationEnabled: state.enableDeviceNotificationsInterimState,
                notificationChangeInProgress: false,
            }
        }

        if (nextProps.notificationStatusChangeFailure) {
            return {
                enableDeviceNotificationsInterimState: !state.enableDeviceNotificationsInterimState,
                notificationChangeInProgress: false,
            }
        }

        return null
    }

    componentDidUpdate(prevProps, prevState) {
        let newProp = Object.keys(SELECTORS).find(prop => this.props[prop])
        if (newProp) {
            if (newProp.includes("Failure")) {
                let propData = this.props[newProp].error ? this.props[newProp].error : cloneDeep(this.props[newProp].response)
                let type = newProp.includes("Failure") ? "error" : "success"
                this.props.showNotification(type, propData, this.onCloseHandler)
            }
            this.props.resetToInitialStateProps(newProp)
        }
    }

    onCloseHandler = () => {
        this.setState({
            isOpen: false,
            type: '',
            message2: ''
        })
    }

    deviceTypeChangeHandler = (selectedDeviceTypes) => {
        let deviceTypeList = cloneDeep(this.state.deviceTypeList),
            selectedDeviceType = cloneDeep(this.state.selectedDeviceType),
            selectedDuration = this.state.selectedDuration,
            deviceTypeChangeLoader = this.state.deviceTypeChangeLoader,
            deviceConnectivityData = cloneDeep(this.state.deviceConnectivityData);

        selectedDeviceType = selectedDeviceTypes
        deviceTypeChangeLoader = true
        deviceConnectivityData = []
        selectedDuration = "10080"

        deviceTypeList.map(devices => {
            if (selectedDeviceType.some(selectedDevice => selectedDevice.value === devices.id)) {
                devices.deviceGroups.map(group => {
                    this.props.getGroupDataByConnector({
                        deviceGroupId: group.deviceGroupId,
                        deviceTypeId: devices.id
                    }, selectedDuration)
                    deviceConnectivityData.push({
                        "deviceId": devices.id,
                        "deviceName": devices.name,
                        "header": `Devices for  ${group.name} (Group) - Connected/Disconnected by %`,
                        "groupId": group.deviceGroupId,
                        "groupName": group.name,
                        "data": [],
                        "isLoading": true,
                        "errorOccurred": false,
                    })
                })
            }
        })

        this.setState({
            selectedDuration,
            selectedDeviceType,
            deviceConnectivityData,
            deviceTypeChangeLoader,
            counter: {
                allDevices: 0,
                connected: 0,
                disconnected: 0
            }
        })
    }

    filterChangeHandler = ({ value }) => {
        let selectedDuration = this.state.selectedDuration,
            deviceTypeChangeLoader = this.state.deviceTypeChangeLoader,
            deviceConnectivityData = cloneDeep(this.state.deviceConnectivityData);

        selectedDuration = value
        deviceConnectivityData.map(group => {
            group.data = []
            group.isLoading = true
            this.props.getGroupDataByConnector({
                deviceGroupId: group.groupId,
                deviceTypeId: group.deviceId
            }, selectedDuration)
        })

        this.setState({
            selectedDuration,
            deviceConnectivityData,
            deviceTypeChangeLoader,
            counter: {
                allDevices: 0,
                connected: 0,
                disconnected: 0
            }
        })
    }

    filterExpandCollapseHandler = () => {
        this.setState({
            filterExpand: this.state.filterExpand ? false : true
        })
    }

    getDeviceTypes = (deviceTypeList) => {
        let list = [];
        deviceTypeList.map(deviceType => {
            list.push({
                label: deviceType.name,
                value: deviceType.id,
            })
        })
        return list;
    }

    notificationChangeHandler = (event) => {
        let checked = event.target.checked
        this.setState({
            enableDeviceNotificationsInterimState: checked,
            notificationChangeInProgress: true
        }, () => {
            this.props.changeNotificationStatus({
                enableDeviceNotifications: checked,
            });
        });
    }

    durationValue = (duration) => {
        if(duration){
        return DURATION_OPTIONS.find(obj => obj.value === duration)
        }
    }

    openGraphModal = (chart) => {
        this.setState({
            openGraphFullScreen: true,
            selectedGraph: chart,
        })
    }

    render() {
        const { allDevices, connected, disconnected } = this.state.counter;
        return (
            <React.Fragment>
                <Helmet>
                    <title>Uptime Info</title>
                    <meta name="description" content="M83-Uptime Info" />
                </Helmet>

                <header className="content-header d-flex">
                    <div className="flex-60">
                        <h6>Uptime Info <small>(Refreshed every 15 minutes)</small></h6>
                    </div>
                    <div className="flex-40 text-right">
                        <div className="content-header-group">
                            {/*<span className="text-content f-11 mr-2">(Send Connectivity Report every <span className="text-cyan">60 minutes</span>)</span>
                            {this.state.notificationChangeInProgress ?
                                <button type="button" className="btn-transparent btn-transparent-green"><i className="far fa-cog fa-spin"></i></button>
                                :
                                <div className={`toggle-switch-wrapper ${!this.state.deviceTypeList.length && "toggle-switch-wrapper-disabled"}`} data-tooltip data-tooltip-text="Disconnected devices notification" data-tooltip-place="bottom">
                                    <label className="toggle-switch">
                                        <input type="checkbox" id="isNotification" disabled={!this.state.deviceTypeList.length} checked={this.state.isNotificationEnabled} onChange={this.notificationChangeHandler} />
                                        <span className="toggle-switch-slider"></span>
                                    </label>
                                </div>
                            }*/}
                            <button className="btn btn-light" data-tooltip data-tooltip-text="Refresh" disabled={this.state.isFetching} data-tooltip-place="bottom" onClick={() => this.refreshComponent()}><i className="far fa-sync-alt"></i></button>
                            <button className={`btn btn-light ${this.state.filterExpand && "active"}`} data-tooltip data-tooltip-text="Filters" data-tooltip-place="bottom" disabled={this.state.isFetching || Boolean(this.state.deviceTypeList.length)} onClick={() => this.filterExpandCollapseHandler()} ><i className="far fa-filter"></i></button>
                        </div>
                    </div>
                </header>

                {this.state.isFetching ?
                    <Loader /> :
                    <React.Fragment>
                        {this.state.deviceTypeList.length ?
                            <React.Fragment>
                                {this.state.filterExpand &&
                                    <div className="device-filter-wrapper">
                                        <div className="device-filter-box">
                                            <h5>Filters <CollapseOrShowAllFilters /></h5>
                                            <h6>Specify Your Search Criteria</h6>
                                        </div>
                                        <div className="device-filter-box">
                                            <p>Device Type <i className="fas fa-angle-down device-filter-collapse" data-toggle="collapse" data-target="#deviceType__filter__"></i></p>
                                            <div className="collapse show" id="deviceType__filter__">
                                                <div className="device-filter-body">
                                                    <div className="form-group mb-0">
                                                        <ReactSelect
                                                            id="deviceTypes"
                                                            options={this.getDeviceTypes(this.state.deviceTypeList)}
                                                            value={this.state.selectedDeviceType}
                                                            onChange={this.deviceTypeChangeHandler}
                                                            isMulti={true}
                                                            isDisabled={this.state.deviceConnectivityData.some(item => item.isLoading)}
                                                            className="form-control-multi-select">
                                                        </ReactSelect>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="device-filter-box">
                                            <p>Duration <i className="fas fa-angle-down device-filter-collapse" data-toggle="collapse" data-target="#duration__filter__"></i></p>
                                            <div className="collapse show" id="duration__filter__">
                                                <div className="device-filter-body">
                                                    <div className="form-group mb-0">
                                                        <ReactSelect
                                                            className="form-control-multi-select"
                                                            id="selectedDuration"
                                                            options={DURATION_OPTIONS}
                                                            value={this.durationValue(this.state.selectedDuration)}
                                                            onChange={this.filterChangeHandler}
                                                            isMulti={false}
                                                        >
                                                        </ReactSelect>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                }

                                <div className="content-body device-filter-content" style={{ paddingRight: this.state.filterExpand ? 270 : 12 }}>
                                    <ul className="list-style-none d-flex device-counter-list">
                                        <li className="flex-33">
                                            <div className="device-counter-card">
                                                <span className="bg-primary"></span>
                                                <h4>{allDevices}</h4>
                                                <p>All devices</p>
                                                <div className="device-counter-icon">
                                                    <img src="https://content.iot83.com/m83/misc/icons/totalDevice.png" />
                                                    <div className="device-counter-status bg-primary"></div>
                                                </div>
                                            </div>
                                        </li>
                                        <li className="flex-33">
                                            <div className="device-counter-card">
                                                <span className="bg-green"></span>
                                                <h4>{connected}</h4>
                                                <p>Online devices</p>
                                                <div className="device-counter-icon">
                                                    <img src="https://content.iot83.com/m83/misc/icons/onlineDevice.png" />
                                                    <div className="device-counter-status bg-green"></div>
                                                </div>
                                            </div>
                                        </li>
                                        <li className="flex-33">
                                            <div className="device-counter-card">
                                                <span className="bg-red"></span>
                                                <h4>{disconnected}</h4>
                                                <p>Offline devices</p>
                                                <div className="device-counter-icon">
                                                    <img src="https://content.iot83.com/m83/misc/icons/offlineDevice.png" />
                                                    <div className="device-counter-status bg-red"></div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>

                                    {this.state.deviceConnectivityData.map((chart, index) => {
                                        return (
                                            <div className="card" key={index}>
                                                <div className="card-header">
                                                    <h6>
                                                        Device Type : <strong className="text-theme fw-600 mr-l-5 text-underline-hover" onClick={() => { isNavAssigned("deviceTypes") && this.props.history.push(`/addOrEditDeviceType/${chart.deviceId}`) }}>{chart.deviceName}</strong>
                                                        <span className="mr-3 ml-3 f-11 text-content">|</span>
                                                        Device Group : <strong className="text-theme fw-600 mr-l-5 text-underline-hover" onClick={() => { isNavAssigned("deviceGroups") && this.props.history.push(`/deviceGroups/${chart.deviceId}/${chart.groupId}`) }}>{chart.groupName}</strong>
                                                    </h6>
                                                </div>
                                                <div className="card-body">
                                                    {(chart.data && chart.data.length) ? <button className="btn btn-light box-top-right-button" onClick={() => this.openGraphModal(chart)}><i className="far fa-expand"></i></button> : null}
                                                    {chart.isLoading ?
                                                        <div className="card-loader">
                                                            <i className="fad fa-sync-alt fa-spin"></i>
                                                        </div>
                                                        :
                                                        <React.Fragment>
                                                            {(chart.data && chart.data.length) ?
                                                                <ConnectionHistoryChart timeZone={this.props.timeZone} chartData={chart.data} chartId={"connectivityChart" + index} /> :
                                                                <div className="card-message"><p>There is no data to display.</p></div>
                                                            }
                                                        </React.Fragment>
                                                    }
                                                </div>
                                            </div>
                                        )
                                    }
                                    )}
                                </div>
                            </React.Fragment>
                            :
                            <div className="content-body">
                                <AddNewButton
                                    text1="No uptime information available"
                                    text2="You haven't created any device type(s) yet. Please create a device type first."
                                    addButtonEnable={isNavAssigned("deviceTypes")}
                                    createItemOnAddButtonClick={() => this.props.history.push(`/addOrEditDeviceType`)}
                                    imageIcon="addConnectionHistory.png"
                                />
                            </div>
                        }

                    </React.Fragment>
                }

                {/* graph full screen modal */}
                {this.state.openGraphFullScreen &&
                    <div className="modal d-block animated slideInDown">
                        <div className="modal-dialog modal-full">
                            <div className="modal-content">
                                <div className="modal-body p-2">
                                    <div className="card modal-full-editor m-0 shadow-none">
                                        <div className="card-header border">
                                            <h6>
                                                Device Type : <strong className="text-theme fw-600 mr-l-5 text-underline-hover" onClick={() => { isNavAssigned("deviceTypes") && this.props.history.push(`/addOrEditDeviceType/${this.state.selectedGraph.deviceId}`) }}>{this.state.selectedGraph.deviceName}</strong>
                                                <span className="mr-3 ml-3">|</span>
                                            Device Group : <strong className="text-theme fw-600 mr-l-5 text-underline-hover" onClick={() => { isNavAssigned("deviceGroups") && this.props.history.push(`/deviceGroups/${this.state.selectedGraph.deviceId}/${this.state.selectedGraph.groupId}`) }}>{this.state.selectedGraph.groupName}</strong>
                                            </h6>
                                        </div>
                                        <div className="card-body border">
                                            {(this.state.selectedGraph.data && this.state.selectedGraph.data.length) ?
                                                <ConnectionHistoryChart timeZone={this.props.timeZone} chartData={this.state.selectedGraph.data} chartId={"connectivityChartFull"} /> :
                                                <div className="card-message h-100"><p>There is no data to display.</p></div>
                                            }
                                        </div>
                                        <button type="button" className="btn btn-light text-gray" onClick={() => this.setState({ openGraphFullScreen: false })}>
                                            <i className="fas fa-compress"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                }
                {/* end graph full screen modal */}

            </React.Fragment>
        );
    }
}

DeviceDashboardsConnectionHistory.propTypes = {
    dispatch: PropTypes.func.isRequired
};

let allSelectors = {}
Object.entries(SELECTORS).map(([key, value]) => {
    allSelectors[key] = value()
})

const mapStateToProps = createStructuredSelector(allSelectors);


function mapDispatchToProps(dispatch) {
    let allActions = { dispatch }
    Object.entries(ACTIONS).map(([key, value]) => {
        allActions[key] = (...args) => dispatch(value(...args))
    })
    return allActions;
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({
    key: "deviceDashboardsConnectionHistory",
    reducer
});
const withSaga = injectSaga({ key: "deviceDashboardsConnectionHistory", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(DeviceDashboardsConnectionHistory);