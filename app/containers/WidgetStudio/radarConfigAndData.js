/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

let data = {
  availableAttributes : ["litres"],
  chartData : [
  {
    category: "Lithuania",
    litres: 501.9,
  },
  {
    category: "Czech Republic",
    litres: 301.9,
  },
  {
    category: "Ireland",
    litres: 201.1,
  },
  {
    category: "Germany",
    litres: 165.8,
  },
  {
    category: "Australia",
    litres: 139.9,
  },
  {
    category: "Austria",
    litres: 128.3,
  },
  {
    category: "UK",
    litres: 99,
  },
  {
    category: "Belgium",
    litres: 60,
  },
  {
    category: "The Netherlands",
    litres: 50,
  }
]}

export function createRadarFromConfig(widgetsTypes, chartData, id) {
  if (chartData && chartData.chartData && chartData.chartData[0].timeStamp) {
    chartData.chartData = chartData.chartData.map(chartDataUnit => {
        chartDataUnit.category = new Date(chartDataUnit.timeStamp).toLocaleTimeString()
        return chartDataUnit;
    })
  }
  chartData = (chartData && chartData.chartData) ? chartData : data;
  let series = chartData.availableAttributes.map((attr, index) => {
    return {
        type: "RadarSeries",
        strokeWidth: 1,
        id: "series" + index,
        name: attr,
        "dataFields": {
          "valueY": attr,
          "categoryX": "category",
        },
        columns: {
          tooltipText:
            "Country: {categoryX}\n: {valueY}",
          fill: "#8F3985"
        },
                
    }
});

  let config = {
    container: id,
    type: "RadarChart",
    data: chartData.chartData,
    xAxes: [
      {
        type: "CategoryAxis",
        dataFields: {
          category: "category"
        }
      }
    ],
    yAxes: [
      {
        type: "ValueAxis"
      }
    ],
    cursor: {
      stroke: "#8F3985",
      strokeWidth: 4,
      strokeOpacity: 1,
    },
    "series": series
  }
  

  switch (widgetsTypes) {
    case "simpleRadarChart":
      series.map(temp => (temp.type = "RadarSeries"));
      break;
    case "zoomableRadarChart":
      series.map(temp => (temp.type = "RadarColumnSeries"));
      break;
    case "angledRadarChart":
      series.map(temp => (temp.type = "RadarColumnSeries"));    
      config.startAngle = -170;
      config.endAngle = -10;
      config.innerRadius = 50;
      break;
    case "stackedRadarColumns":   
      let obj =  {
          type: "RadarColumnSeries",
          dataFields: {
            valueY: "units",
            categoryX: "category",
            stacked: true
          },
          columns: {
            tooltipText:
            "Country: {categoryX}\nLitres Consumed: {valueY}",
            fill: "#00BFFF"
          }
        }
      series.push(obj)
      series.map(temp => (temp.type = "RadarColumnSeries"));
      break;
      case "mixedRadarChart":
        series.map(temp => (temp.type = "RadarSeries"));
        let axisBreak = {
          "type": "RadarColumnSeries",
          "name": "Units",
          "dataFields": {
          "valueY": "units",
          "categoryX": "country"
          },
          columns: {
            tooltipText:
            "Country: {categoryX}\nLitres Consumed: {valueY}",
            fill: "#8F3985"
          }
        }
        series.push(axisBreak)
        break;
    default:
  }
  return config ;
}
