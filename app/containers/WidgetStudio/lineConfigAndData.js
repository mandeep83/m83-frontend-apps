/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import * as am4core from "@amcharts/amcharts4/core";

let getBulletsValue = (customValues) => {
    if (!customValues.bulletVisibility.value) {
        return []
    }
    let bullets = [{
        "type": "Circle",
        "fill": am4core.color("#fff"),
        "cursorOverStyle": am4core.MouseCursorStyle.pointer,
        "resizable": true,
        "width": Number(customValues.bulletSize.value),
        "height": Number(customValues.bulletSize.value),
        "states": {
            "hover": {
                "properties": {
                    "scale": 1.7
                }
            }
        },
        "strokeWidth": 3,
        "filters": [{
            "type": "DropShadowFilter",
            "color": "#B8B8B8",
            "dx": 3,
            "dy": 3
        }],
    }]
    return bullets
}


export function createLineFromConfig(widgetType, chartData, customValues, id, deleteLegend) {
    let series = chartData.availableAttributes.map((attr, index) => {
        let seriesObj = {
            "type": "LineSeries",
            "strokeWidth": customValues.lineVisibility.value ? Number(customValues.lineThickness.value) : 0,
            "id": "series" + index,
            "name": attr.attrDisplayName,
            "yAxis": widgetType === "multiAxisChart" ? attr.attr : false,
            "bullets": getBulletsValue(customValues),
            "tooltipHTML": `<center class="chart-tooltip">
            <p>${attr.attrDisplayName} :<strong>{valueY}${chartData.units && chartData.units[attr.attr] ? "(" + chartData.units[attr.attr] + ")" : ""}</strong></p>
            <p>{categoryX}<p>
                    </center>`,
            "tooltip": {
                "getFillFromObject": false,
                label: {
                    "fill": "#000",
                    "fillOpacity": "1",
                },
                "background": {
                    "fill": "#fff",
                    "fillOpacity": ".85",
                }
            },
            // attr.attrDisplayName + " : {" + attr.attr + "} " + (chartData.units && chartData.units[attr.attr] ? chartData.units[attr.attr] : ""),
            "propertyFields": {
                "stroke": ""
            },
            "filters": [{
                "type": "DropShadowFilter",
                "color": "#B8B8B8",
                "dx": 8,
                "dy": 8
            }],
            "fillOpacity": 0.4,
            "segments": {
                "fillModifier": {
                    "type": "LinearGradientModifier",
                    "opacities": [0.5, 0.2, 0.1, , 0, 0],
                    "gradient": {
                        "rotation": 90
                    }
                }
            },
            "dataFields": {
                "valueY": attr.attr,
                "categoryX": "category",
            },
        }
        if (customValues.customColors.value) {
            seriesObj.stroke = customValues[attr.attr].value
            seriesObj.fill = customValues[attr.attr].value
        }
        return seriesObj
    })
    let config = {
        "type": "XYChart",
        "titles": widgetType === "multiAxisChart" ? [{
            "text": chartData.seriesName,
            "marginBottom": 0,
            "fontSize": 9,
        }] : false,
        "container": id,
        "maskBullets": false,
        "data": chartData.chartData,
        "legend": {
            "scrollable": true,
            "maxHeight": 30,
            "useDefaultMarker": true,
            "markers": {
                "children": [{
                    "cornerRadiusTopLeft": 12,
                    "cornerRadiusTopRight": 12,
                    "cornerRadiusBottomRight": 12,
                    "cornerRadiusBottomLeft": 12,
                    // "strokeWidth": 2,
                    // "strokeOpacity": 1,
                    // "stroke": "#ccc"
                }],
                "dy": -1,
                "dx": -4,
                "width": 12,
                "height": 12,
            },
            "labels": {
                "template": {
                    "fill": "#808080",
                    "fontSize": "12"
                }
            },
            "position": customValues.legendPlacement.value,
            "marginTop": 4,
            "marginBottom": -16,
        },
        "width": "100%",
        "height": "100%",
        "xAxes": [{
            "cursorTooltipEnabled": false,
            "id": "a1",
            "startLocation": 0.5,
            "endLocation": 0.5,
            "type": "CategoryAxis",
            "dataFields": {
                "category": "category",
            },
            "renderer": {
                "labels": {
                    "wrap": true,
                    "maxWidth": 90,
                    "template": {
                        "fill": "#808080",
                        "fontSize": 9
                    }
                },
                "minLabelPosition": 0.05,
                "maxLabelPosition": 0.95,
                "line": {
                    "strokeOpacity": 0.5,
                    "strokeWidth": 1,
                },
                "grid": {
                    "strokeOpacity": 0.2,
                    // "stroke": "#A0CA92",
                    "strokeWidth": 0.3
                },
                "minGridDistance": 90,
            },
        }],

        "cursor": {
            "behavior": "zoomXY"
        },
        "yAxes": [{
            "cursorTooltipEnabled": false,
            "type": "ValueAxis",
            "title": {
                "text": `${chartData.seriesName} ${(chartData.units && chartData.units[chartData.availableAttributes[0].attr] ? "(" + chartData.units[chartData.availableAttributes[0].attr] + ")" : "")}`,
                "fontSize": 10,
                "fontWeight": 500,
                "fill": "#808080",
            },
            "renderer": {
                "labels": {
                    "template": {
                        "fill": "#808080",
                        "fontSize": 9,
                    }
                },
                "line": {
                    "strokeOpacity": 0,
                    // "strokeWidth": 1,
                },
                "grid": {
                    "strokeOpacity": 0.2,
                    "strokeWidth": 0.3
                },
            },
            "axisRanges": {
                // "type": "range",
                // "value": 50,
                // "endValue": 1200,
                "contents": {
                    "stroke": "#FF0000",
                    "fill": "#FF0000"
                },
            },
            // strictMinMax: true,
            // baseValue: 50
        }],
        "responsive": {
            "enabled": true,
        },
        "series": series,
    };
    if (customValues.scrollBarVisibility && customValues.scrollBarVisibility.value) {
        config.scrollbarX = {
            "type": "XYChartScrollbar",
            "series": chartData.availableAttributes.map((attr, index) => "series" + index), //make it dynamic with above series
        }
    }

    if (widgetType !== "multiAxisChart" && customValues.customizedValueAxis.value) {
        config.yAxes[0].renderer.minGridDistance = customValues.valueAxisGridDistance.value
        config.yAxes[0].min = customValues.valueAxisMin.value
        config.yAxes[0].max = customValues.valueAxisMax.value
        config.yAxes[0].strictMinMax = true
    }

    switch (widgetType) {
        case "simpleLineChart":
            break;
        case "simpleLineChartWithArea":
            series.map(series => {
                series.fillOpacity = "0.5"
            })
            break;
        case "simpleLineChartWithArea&Scroll":
            series.map(series => {
                series.fillOpacity = "0.5"
            })
            config.scrollbarX = {
                "type": "XYChartScrollbar",
                "series": chartData.availableAttributes.map((attr, index) => "series" + index), //make it dynamic with above series
            }
            break;
        case "smoothedLineChart":
            series.map(series => {
                series.tensionX = 0.8
                series.tensionY = 1
            })
            break;
        case "stepLineChart":
            series.map(series => {
                series.type = "StepLineSeries"
                delete series.bullets
            })
            config.cursor = {
                xAxis: "a1",
                "fullWidthLineX": true,
                "lineX": {
                    "strokeWidth": 0,
                    "fill": "#8F3985",
                    "fillOpacity": 0.1
                },
                "behavior": "zoomXY"
            }
            break;
        case "multiAxisChart":
            series.map(series => {
                series.tensionX = 0.8
                series.tensionY = 1
            })
            config.yAxes = chartData.availableAttributes.map((attr, index) => {
                let seriesObj = {
                    "type": "ValueAxis",
                    "cursorTooltipEnabled": false,
                    renderer: {
                        opposite: index % 2 === 0 ? false : true,
                        line: {
                            strokeOpacity: 0,
                            // strokeWidth: 2,
                            // stroke: series.stroke
                        },
                        labels: {
                            template: {
                                fill: "#808080",
                                fontSize: 9,
                            }
                        },
                        grid: {
                            strokeOpacity: 0.2,
                            // "stroke": "#A0CA92",
                            strokeWidth: 0.3
                        },
                    },
                    fontSize: 14,
                    fontWeight: 500,
                    id: attr.attr,
                    name: attr.attr,
                    axisRanges: {
                        "contents": {
                            "stroke": "#FF0000",
                            "fill": "#FF0000"
                        },
                    },
                    title: {
                        text: `${attr.attrDisplayName} ${(chartData.units && chartData.units[attr.attr] ? "(" + chartData.units[attr.attr] + ")" : "")}`,
                        fill: "#808080",
                        strokeWidth: 2,
                        fontSize: 10,
                        fontWeight: 500
                    },
                }
                if (index > 0)
                    seriesObj.syncWithAxis = chartData.availableAttributes[0].attr
                return seriesObj
            })
            break;
        default:
    }
    if (deleteLegend) {
        delete config.legend
    }
    return config;

}

export function createLineFromConfigForMl(widgetType, chartData, customValues, id, deleteLegend) {
    let series = chartData.availableAttributes.map((attr, index) => {
        let seriesObj = {
            "type": "LineSeries",
            "strokeWidth": Number(customValues.lineThickness.value),
            "id": "series" + index,
            "name": attr,
            "yAxis": widgetType === "multiAxisChart" ? attr : false,
            "bullets": getBulletsValue(customValues),
            "tooltipText": attr + " : {" + attr + "} " + (chartData.units && chartData.units[attr] ? chartData.units[attr] : ""),
            "propertyFields": {
                "stroke": ""
            },
            "fillOpacity": "",
            "dataFields": {
                "valueY": attr,
                "categoryX": "category",
            },
        }
        if (customValues.customColors.value) {
            seriesObj.stroke = customValues[attr].value
            seriesObj.fill = customValues[attr].value
        }
        return seriesObj
    })

    let config = {
        "type": "XYChart",
        "container": id,
        "data": chartData.chartData,
        "legend": {
            "useDefaultMarker": true,
            "markers": {
                "children": [{
                    "cornerRadiusTopLeft": 12,
                    "cornerRadiusTopRight": 12,
                    "cornerRadiusBottomRight": 12,
                    "cornerRadiusBottomLeft": 12,
                    // "strokeWidth": 2,
                    // "strokeOpacity": 1,
                    // "stroke": "#ccc"
                }],
                "dy": -1,
                "dx": -4,
                "width": 12,
                "height": 12,
            },
            "labels": {
                "template": {
                    "fill": "#808080",
                    "fontSize": 9
                }
            },
            "position": customValues.legendPlacement.value,
            "marginTop": 4,
            "marginBottom": -16,
        },
        "width": "100%",
        "height": "100%",
        "xAxes": [{
            id: "a1",
            "startLocation": 0.5,
            "endLocation": 0.5,
            "type": "CategoryAxis",
            "dataFields": {
                "category": "category",
            },
            "renderer": {
                "labels": {
                    "wrap": true,
                    "maxWidth": 90
                },
                "grid": {
                    "strokeOpacity": 0.2,
                    // "stroke": "#A0CA92",
                    "strokeWidth": 0.3
                },
            },
            "fontSize": "9",
            minGridDistance: 90,
        }],
        "cursor": {
            "behavior": "zoomXY"
        },
        "yAxes": [{
            "type": "ValueAxis",
            renderer: {
                // opposite: true,
                "grid": {
                    "strokeOpacity": 0.2,
                    // "stroke": "#A0CA92",
                    "strokeWidth": 0.3
                },
            },
            // min: 0,
            // max: 100,
            axisRanges: {
                // "type": "range",
                // "value": 50,
                // "endValue": 1200,
                "contents": {
                    "stroke": "#FF0000",
                    "fill": "#FF0000"
                },
            },
            // baseValue: 50
        }],
        "responsive": {
            "enabled": true,
        },
        "series": series,
    };

    switch (widgetType) {
        case "simpleLineChartWithScrollAndZoom":
            config.scrollbarX = {
                "type": "XYChartScrollbar",
                "series": chartData.availableAttributes.map((attr, index) => "series" + index), //make it dynamic with above series
            }
            break;
        case "simpleLineChart":
            break;
        case "simpleLineChartWithArea":
            series.map(series => {
                series.fillOpacity = "0.5"
            })
            break;
        case "simpleLineChartWithArea&Scroll":
            series.map(series => {
                series.fillOpacity = "0.5"
            })
            config.scrollbarX = {
                "type": "XYChartScrollbar",
                "series": chartData.availableAttributes.map((attr, index) => "series" + index), //make it dynamic with above series
            }
            break;
        case "smoothedLineChart":
            series.map(series => {
                series.tensionX = 0.8
                series.tensionY = 1
            })
            break;
        case "stepLineChart":
            series.map(series => {
                series.type = "StepLineSeries"
                delete series.bullets
            })
            config.cursor = {
                xAxis: "a1",
                "fullWidthLineX": true,
                "lineX": {
                    "strokeWidth": 0,
                    "fill": "#8F3985",
                    "fillOpacity": 0.1
                },
                "behavior": "zoomXY"
            }
            break;
        case "multiAxisChart":
            series.map(series => {
                series.tensionX = 0.8
                series.tensionY = 1
            })
            config.yAxes = chartData.availableAttributes.map((attr, index) => {
                return {
                    "type": "ValueAxis",
                    renderer: {
                        opposite: index % 2 === 0 ? false : true,
                        line: {
                            strokeOpacity: 1,
                            strokeWidth: 2,
                            stroke: series.stroke
                        },
                        grid: {
                            strokeOpacity: 0.2,
                            // "stroke": "#A0CA92",
                            strokeWidth: 0.3
                        },
                        labels: {
                            template: { fill: series.stroke },
                            fontSize: 9
                        }
                    },
                    id: attr,
                    axisRanges: {
                        "contents": {
                            "stroke": "#FF0000",
                            "fill": "#FF0000"
                        },
                    },
                    title: {
                        text: attr
                    }
                }
            })
            break;
        default:
    }
    if (deleteLegend) {
        delete config.legend
    }
    return config;

}
