/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { takeEvery } from 'redux-saga';
import * as CONSTANTS from './constants';
import { apiCallHandler } from '../../api';


// export function* getAllWidgetsApiHandlerAsync(action) {
//   yield [apiCallHandler(action, CONSTANTS.GET_ALL_WIDGETS_SUCCESS, CONSTANTS.GET_ALL_WIDGETS_FAILURE, 'getAllWidgets')];
// }

// export function* deleteWidgetApiHandlerAsync(action) {
//   yield [apiCallHandler(action, CONSTANTS.DELETE_WIDGET_SUCCESS, CONSTANTS.DELETE_WIDGET_FAILURE, 'deleteWidgetById')];
// }

// export function* cloneWidgetApiHandlerAsync(action){
//   yield[apiCallHandler(action,CONSTANTS.CLONE_WIDGET_SUCCESS,CONSTANTS.CLONE_WIDGET_FAILURE,'cloneWidget')]
// }

// export function* importWidgetApiHandlerAsync(action){
//   yield[apiCallHandler(action,CONSTANTS.IMPORT_WIDGET_SUCCESS,CONSTANTS.IMPORT_WIDGET_FAILURE,'importWidget')]
// }

// export function* exportWidgetApiHandlerAsync(action){
//   yield[apiCallHandler(action,CONSTANTS.EXPORT_WIDGET_SUCCESS,CONSTANTS.EXPORT_WIDGET_FAILURE,'exportWidget')]
// }

// export function* getWidgetByIdHandlerAsync(action){
//   yield[apiCallHandler(action,CONSTANTS.GET_WIDGET_BY_ID_SUCCESS,CONSTANTS.GET_WIDGET_BY_ID_FAILURE,'getWidgetById')]
// }


// export function* getCommitHistoryAsyncHandler(action) {
//   yield [apiCallHandler(action, CONSTANTS.GET_COMMIT_HISTORY_SUCCESS, CONSTANTS.GET_COMMIT_HISTORY_FAILURE, 'getCommitHistory')];
// }

// export function* getInvitationStatus(action) {
//   yield [apiCallHandler(action, CONSTANTS.GET_INVITATION_STATUS_SUCCESS, CONSTANTS.GET_INVITATION_STATUS_FAILURE, 'getInvitationStatus')]
// }

// export function* invokeApi(action) {
//   yield [apiCallHandler(action, CONSTANTS.INVOKE_API_SUCCESS, CONSTANTS.INVOKE_API_FAILURE, 'invokeApi')]
// }

// export function* watcherGetAllWidgets() {
//   yield takeEvery(CONSTANTS.GET_ALL_WIDGETS, getAllWidgetsApiHandlerAsync);
// }

// export function* watcherDeleteWidget() {
//   yield takeEvery(CONSTANTS.DELETE_WIDGET, deleteWidgetApiHandlerAsync);
// }

// export function* watcherCloneWidget(){
//   yield takeEvery(CONSTANTS.CLONE_WIDGET,cloneWidgetApiHandlerAsync)
// }


// export function* watcherExportWidget(){
//   yield takeEvery(CONSTANTS.EXPORT_WIDGET,exportWidgetApiHandlerAsync)
// }


// export function* watcherImportWidget(){
//   yield takeEvery(CONSTANTS.IMPORT_WIDGET ,importWidgetApiHandlerAsync)
// }

// export function* watcherGetWidgetById(){
//   yield takeEvery(CONSTANTS.GET_WIDGET_BY_ID ,getWidgetByIdHandlerAsync)
// }

// export function* watcherGetCommitHistory() {
//   yield takeEvery(CONSTANTS.GET_COMMIT_HISTORY, getCommitHistoryAsyncHandler);
// }

// export function* watcherGetInvitationStatus() {
//   yield takeEvery(CONSTANTS.GET_INVITATION_STATUS,  getInvitationStatus);
// }

// export function* watcherInvokeApi() {
//   yield takeEvery(CONSTANTS.INVOKE_API,  invokeApi);
// }

// export default function* rootSaga() {
//   yield [
//       watcherGetAllWidgets(),
//       watcherDeleteWidget(),
//       watcherCloneWidget(),
//       watcherImportWidget(),
//       watcherExportWidget(),
//       watcherGetWidgetById(),
//       watcherGetCommitHistory(),
//       watcherGetInvitationStatus(),
//       watcherInvokeApi(),
//   ];
// }



let allApiCalls = Object.values(CONSTANTS).filter(constant => typeof (constant) == "object")

allApiCalls.map(apiCall => {
  apiCall.apiCallHandler = function* (action) {
    yield [apiCallHandler(action, apiCall.success, apiCall.failure, apiCall.urlKey)]
  }
  apiCall.watcher = function* () {
    yield takeEvery(apiCall.action, apiCall.apiCallHandler);
  }
})

export default function* rootSaga() {
  yield allApiCalls.map(apiCall => apiCall.watcher());
  
}
