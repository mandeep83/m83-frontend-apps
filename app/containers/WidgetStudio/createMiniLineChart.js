/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

export default function createMiniLineChart(chartData, id) {
    let config = {
        "type": "XYChart",
        "container": id,
        "data": chartData.attrTrend,
        "width": "100%",
        "height": "100%",
        "xAxes": [{
            "startLocation" : 0.5,
            "endLocation" :0.7,
            "type": "DateAxis",
            "dataFields": {
                "category": "timestamp",
            },
            "cursorTooltipEnabled": false,
            "renderer": {
                "grid": {
                    "disabled": true
                },
                "labels": {
                    "disabled": true
                },
            },
            "fontSize": "12px",
        }],
        "yAxes": [{
            "type": "ValueAxis",
            "grid": {
                "disabled": true
            },
            "renderer": {
                "grid": {
                    "disabled": true
                },
                "baseGrid": {
                    "disabled": true
                },
                "labels": {
                    "disabled": true
                },
            },
            "cursorTooltipEnabled": false,
        }],
        "series": [{
            "type": "LineSeries",
            "strokeWidth": 2,
            "name": "attr.attrDisplayName",
            "tooltipText": `{timestamp}: [bold]{${chartData.attr}}`,
            "propertyFields": {
                "stroke": ""
            },
            "dataFields": {
                "valueY": chartData.attr,
                "dateX": "timestamp",
            },
            "tensionX": 0.8,
            "fillOpacity" : 0.5
        }],
    };
    return config;
}
