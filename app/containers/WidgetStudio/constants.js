/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

export const DEFAULT_ACTION = "app/PageStudio/DEFAULT_ACTION";
export const RESET_TO_INITIAL_STATE = "app/PageStudio/RESET_TO_INITIAL_STATE";

export const GET_ALL_PAGES = {
	action: 'app/PageStudio/GET_ALL_PAGES',
	success: "app/PageStudio/GET_ALL_PAGES_SUCCESS",
	failure: "app/PageStudio/GET_ALL_PAGES_FAILURE",
	urlKey: "getAllPages",
	successKey: "getAllPagesSuccess",
	failureKey: "getAllPagesFailure",
	actionName: "getAllPages"
	
}

export const DELETE_PAGE = {
	action: 'app/PageStudio/DELETE_PAGE',
	success: "app/PageStudio/DELETE_PAGE_SUCCESS",
	failure: "app/PageStudio/DELETE_PAGE_FAILURE",
	urlKey: "deletePage",
	successKey: "deletePageSuccess",
	failureKey: "deletePageFailure",
    actionName: "deletePage",
    actionArguments: ["id"]
	
} 

export const GET_PAGE_BY_ID = {
	action: 'app/PageStudio/GET_PAGE_BY_ID',
	success: "app/PageStudio/GET_PAGE_BY_ID_SUCCESS",
	failure: "app/PageStudio/GET_PAGE_BY_ID_FAILURE",
	urlKey: "getPageById",
	successKey: "getPageByIdSuccess",
	failureKey: "getPageByIdFailure",
	actionName: "getPageById",
    actionArguments: ["id"]
}
