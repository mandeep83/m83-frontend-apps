/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";


let data = [
  {
    "date": "2015-01-01",
    "ay": 6.5,
    "by": 2.2,
    "aValue": 150,
    "bValue": 10,
    "x": 100,
    "value": 123
  }, {
    "date": "2015-01-02",
    "ay": 12.3,
    "by": 4.9,
    "aValue": 280,
    "bValue": 3,
    "x": 300,
    "value": 127
  }, {
    "date": "2015-01-03",
    "ay": 12.3,
    "by": 5.1,
    "aValue": 160,
    "bValue": 4,
    "x": 500,
    "value": 131
  }, {
    "date": "2015-01-04",
    "ay": 2.8,
    "by": 13.3,
    "aValue": 390,
    "bValue": 13,
    "x": 700,
    "value": 137
  }, {
    "date": "2015-01-05",
    "ay": 3.5,
    "by": 6.1,
    "aValue": 500,
    "bValue": 2,
    "x": 900,
    "value": 141
  }, {
    "date": "2015-01-06",
    "ay": 5.1,
    "by": 8.3,
    "aValue": 510,
    "bValue": 17,
    "x": 110,
    "value": 147
  }, {
    "date": "2015-01-07",
    "ay": 6.7,
    "by": 10.5,
    "aValue": 230,
    "bValue": 10,
    "x": 130,
    "value": 151
  }, {
    "date": "2015-01-08",
    "ay": 8,
    "by": 12.3,
    "aValue": 170,
    "bValue": 13,
    "x": 150,
    "value": 157
  }, {
    "date": "2015-01-09",
    "ay": 8.9,
    "by": 4.5,
    "aValue": 580,
    "bValue": 11,
    "x": 170,
    "value": 161
  }, {
    "date": "2015-01-10",
    "ay": 9.7,
    "by": 15,
    "aValue": 150,
    "bValue": 10,
    "x": 190,
    "value": 167
  }, {
    "date": "2015-01-11",
    "ay": 10.4,
    "by": 10.8,
    "aValue": 110,
    "bValue": 11,
    "x": 210,
    "value": 171
  }, {
    "date": "2015-01-12",
    "ay": 1.7,
    "by": 19,
    "aValue": 130,
    "bValue": 3,
    "x": 230,
    "value": 177
  }
];

export function createXYchartFromConfig(widgetsTypes, chartData, id) {
  if (chartData && chartData.chartData && chartData.chartData[0].timeStamp) {
    chartData.chartData = chartData.chartData.map(chartDataUnit => {
      chartDataUnit.date = new Date(chartDataUnit.timeStamp).toLocaleTimeString()
      return chartDataUnit;
    })
  }
  let series = chartData && chartData.availableAttributes ? chartData.availableAttributes.map((attr, index) => {
    return {
      type: "ColumnSeries",
      strokeWidth: 1,
      id: "series" + index,
      name: attr,
      columns: {
        tooltipText:
          "Country: {categoryX}\nLitres Consumed: {valueY}",
        fill: "#CDA2AB"
      },
      bullets: [],
      heatRules: [],
      plotContainer: [],
      dataFields: {
        valueY: attr,
        categoryX: "category",
      },
    }
  }) : [
      {
        type: "ColumnSeries",
        dataFields: {
          valueY: "by",
          categoryX: "date"
        },
        columns: {
          tooltipText: "Category: {categoryX}\nValue: {valueY}",
          fill: "#CDA2AB",
        },
        bullets: [],
        heatRules: [],
        plotContainer: []
      }
    ]
  let config = {
    container: id,
    type: "XYChart",
    data: data,
    xAxes: [
      {
        type: "CategoryAxis",
        dataFields: {
          category: "date"
        }
      }
    ],
    yAxes: [
      {
        type: "ValueAxis"
      }
    ],
    series: series
  }

  switch (widgetsTypes) {
    case "withDateBasedAxis":
      series[0].dataFields = {
        valueY: "ay",
        value: "aValue",
        categoryX: "date"
      }
      series[0].strokeOpacity = 0;

      series[0].bullets.push({
        type: "CircleBullet",
        fill: "#CDA2AB",
        strokeOpacity: 0,
        strokeWidth: -1,
        fillOpacity: 0.6,
        tooltipText: "Population: {value.value}\Male: {valueX.value}\nFemale:{valueY.value}"
      });
      series[0].heatRules.push({
        target: "bullets.0.circle",
        min: 4,
        max: 50,
        property: "radius"
      });
      config.cursor = {
        stroke: "#8F3985",
        strokeWidth: 2,
        strokeOpacity: 0.2,
      }
      series.map(temp => (temp.type = "LineSeries"));
      break;
    case "withValueBasedLine":
      series[0].dataFields = {
        valueX: "ay",
        valueY: "by",
        value: "value",
        categoryX: "date",
      }
      series[0].bullets.push({
        type: "CircleBullet",
        fill: "#8F3985",
        radius: 10,
        tooltipText: "Vehicles Tested: {valueX}\nPollution Count: {valueY}",
      });
      series[0].heatRules.push({
        target: "bullets.0.circle",
        min: 4,
        max: 50,
        property: "radius"
      });
      config.cursor = {
        stroke: "#8F3985",
        strokeWidth: 2,
        strokeOpacity: 0.2,
      }
      series.map(temp => (temp.type = "LineSeries"));
      break;
    case "zoomableBubbleChart":
      series[0].dataFields = {
        valueX: "x",
        valueY: "aValue",
        value: "value",
        categoryX: "date"
      }
      series[0].strokeOpacity = 0;
      series[0].bullets.push({
        type: "CircleBullet",
        fill: "#ff0000",
        fillOpacity: 0.6,
        radius: 10,
        strokeOpacity: 0,
        tooltipText: "Population:\nMale: {valueX.value}\nFemale:{valueY.value}",
      });
      series[0].heatRules.push({
        target: "bullets.0.circle",
        min: 5,
        max: 60,
        property: "radius"
      });
      config.cursor = {
        stroke: "#8F3985",
        strokeWidth: 2,
        strokeOpacity: 0.2,
      }
      // config.theme = "am4themes_animated"
      series.map(temp => (temp.type = "LineSeries"));
      break;
    default:
  }
  return config;
}
