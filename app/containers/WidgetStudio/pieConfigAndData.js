/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

export function createPieFromConfig(widgetsType, chartData, customValues, id) {
    let data = chartData && chartData.length !== 0 ? chartData : [{ 'category': 'AQI', 'value': 217.51444444444445 }, { 'category': 'ECO2', 'value': 787.2222222222222 }, { 'category': 'ENVCOMFORT', 'value': 69.0 }, { 'category': 'HUMIDITY', 'value': 32.595555555555556 }, { 'category': 'PRESSURE', 'value': 985.0922222222222 }, { 'category': 'TEMPERATURE', 'value': 32.25555555555555 }, { 'category': 'THERMALCOMFORT', 'value': 75.0 }, { 'category': 'TVOC', 'value': 58.111111111111114 }, { 'category': 'VISUALCOMFORT', 'value': 0.0 }]
    let series = [{
        "type": "PieSeries",
        "id": "series1",
        "name": "Series 1",
        "fontSize": "12px",
        "depth": 35,
        "angle": 30,
        "ticks": {
            "width": "100",
            "template": {
                "stroke": "#4a4a4a",
            }
        },
        "labels": {
            template: {
                text: "{value.percent}%",
            }
        },
        "slices": {
            "template": {
                "tooltipText": "{valueY} : {value}",
                "cursorOverStyle": [{
                    "property": "cursor",
                    "value": "pointer"
                }],
                "filters": [{
                    "type": "DropShadowFilter",
                    "opacity": 0,
                }],
                "states": {
                    "hover": {
                        "filters": [{
                            "type": "DropShadowFilter",
                            "opacity": 0.7,
                            "blur": 5
                        }]
                    }
                },
            },
        },
        "dataFields": {
            "value": "value",
            "category": "category",
            "valueY": "categoryDisplayName"
        },
    }]
    let config = {
        "type": "PieChart",
        "container": id,
        "innerRadius": "30%",
        "legend": {
            "fontSize": "12px",
            "labels": {
                "text": "{category} "
            },
            "markers": {
                "children": [{
                    "cornerRadiusTopLeft": 12,
                    "cornerRadiusTopRight": 12,
                    "cornerRadiusBottomRight": 12,
                    "cornerRadiusBottomLeft": 12,
                    // "strokeWidth": 2,
                    // "strokeOpacity": 1,
                    // "stroke": "#ccc"
                }],
                "dy": -1,
                "dx": -4,
                "width": 12,
                "height": 12
            },
            "labels": {
                "template": {
                    "fill": "#4a4a4a",
                    "fontSize": "13"
                }
            },
            "paddingTop": 10,
            "position": customValues.legendPlacement.value,
        },
        "data": data,
        "series": series,
        "export": {
            "enabled": true
        },
    }
    switch (widgetsType) {
        case "variableRadius3dPieChart":
            config.hiddenState = {
                "properties": {
                    "opacity": 0
                }
            };
            config.innerRadius = 0
            config.series[0].dataFields.radiusValue = "value";
            config.series[0].slices.template.cornerRadius = 6
            config.series[0].colors = {
                "step": 3
            };
            config.series[0].hiddenState = {
                "properties": {
                    "endAngle": -90
                }
            };
            break;
        case "3dPieChart":
            config.type = "PieChart3D"
            config.hiddenState = {
                "properties": {
                    "opacity": 0
                }
            };
            config.innerRadius = 0
            config.series[0].type = "PieSeries3D";
            config.depth = typeof (customValues.chartDepth.value) === "number" ? customValues.chartDepth.value : 20;
            config.angle = typeof (customValues.chartAngle.value) === "number" ? customValues.chartAngle.value : 10

            break;
    }
    return config;
}
