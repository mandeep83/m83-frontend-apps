/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import reducer from "./reducer";
import saga from "./saga";
import { allActions as ACTIONS } from './actions';
import { allSelectors as SELECTORS } from "./selectors";
import Loader from '../../components/Loader/Loadable'
import { Helmet } from "react-helmet";
import Iframe from '../../components/Iframe/Loadable';
import NotificationModal from '../../components/NotificationModal/Loadable'
import ListingTable from "../../components/ListingTable/Loadable";
import AddNewButton from "../../components/AddNewButton/Loadable"
import QuotaHeader from "../../components/QuotaHeader/Loadable";
import produce from "immer"
import { cloneDeep } from "lodash";
import Skeleton from "react-loading-skeleton";

/* eslint-disable react/prefer-stateless-function */
export class WidgetStudio extends React.Component {
    state = {
        isFetchingPages: true,
        toggleView: false,
        widgetsList: [],
        filteredList: [],
        searchTerm: '',
        editorModal: false,
        isCloneWidgetLoader: false,
        importWidget: {
            file: ''
        },
        exportWidgetParam: false,
        exportWidgets: {
            fileUrl: ''
        },
        isStatusChecked: false,
    };

    qoutaCallback = () => {
        this.props.getAllPages()
    }

    static getDerivedStateFromProps(nextProps, state) {
        let newProp = Object.keys(SELECTORS).find(prop => nextProps[prop])
        if (newProp) {
            let propData = nextProps[newProp].error ? nextProps[newProp].error : nextProps[newProp].response
            if (nextProps.getAllPagesSuccess) {
                propData.map(list => {
                    list.menuEnabled = list.isMenuAttached ? "Yes" : "No"
                })
                let filterList = propData;
                if (state.searchTerm.length > 0) {
                    filterList = filterList.filter(item => item.name.toLowerCase().includes(state.searchTerm.toLowerCase()))
                }
                return {
                    isFetchingPages: false,
                    widgetsList: propData,
                    filteredList: filterList,
                    isCloneWidgetLoader: false
                }
            }


            if (nextProps.deletePageSuccess) {
                let widgetsList = state.widgetsList.filter(widget => widget.id != nextProps.deletePageSuccess.response.id)
                let filteredList = state.filteredList.filter(widget => widget.id != nextProps.deletePageSuccess.response.id)
                return {
                    widgetsList,
                    message2: nextProps.deletePageSuccess.response.message,
                    isOpen: true,
                    type: 'success',
                    isFetching: false,
                    isDeletedSuccess: !state.isDeletedSuccess,
                    filteredList
                }
            }
            if (nextProps.deletePageFailure) {
                return {
                    isFetching: false,
                }
            }

            if (nextProps.getPageByIdSuccess) {
                let fullScreenPage = produce(propData, fullScreenPage => {
                    fullScreenPage.code.html = decodeURIComponent(escape(atob(fullScreenPage.code.html)));
                    fullScreenPage.code.css = decodeURIComponent(escape(atob(fullScreenPage.code.css)));
                    fullScreenPage.code.js = decodeURIComponent(escape(atob(fullScreenPage.code.js)));
                })
                return {
                    fullScreenPage,
                    isFetchingPage: false
                }
            }

            if (nextProps.getPageByIdFailure) {
                return {
                    isFetchingPage: false,
                    showFullScreenPage: false,
                }
            }

        }
        return null;
    }

    componentDidUpdate(prevProps, prevState) {
        let newProp = Object.keys(SELECTORS).find(prop => this.props[prop])
        if (newProp) {
            if (newProp.includes("Failure")) {
                this.props.showNotification("error", this.props[newProp].error)
            }
            this.props.resetToInitialState(newProp)
        }
    }

    editWidget = ({ currentTarget }) => {
        this.props.history.push({ pathname: `/createOrEditPage/${currentTarget.id}` })
    }

    deleteWidget = (id, deleteWidgetName) => {
        this.setState({ deleteItemId: id, confirmState: true, deleteWidgetName })
    }

    onCloseHandler = () => {
        this.setState({
            message2: '',
            isOpen: false,
            type: ''
        })
    }

    cancelClicked = () => {
        this.setState({
            deleteItemId: '',
            confirmState: false,
        });
    }

    fullScreenPage = ({ currentTarget }) => {
        this.setState({
            showFullScreenPage: true,
            isFetchingPage: true
        }, () => this.props.getPageById(currentTarget.id))
    }

    filteredDataList = (value) => {
        let filterList = cloneDeep(this.state.widgetsList);
        if (value) {
            filterList = filterList.filter(item => item.name.toLowerCase().includes(value.toLowerCase()))
        }

        this.setState({
            searchTerm: value,
            filteredList: filterList
        })
    }

    cloneWidget = (id) => {
        this.setState({ isCloneWidgetLoader: true, isOpen: false }, () => this.props.cloneWidget(id));
    }

    importWidget = (event) => {
        event.preventDefault();
        let formData = new FormData()
            , importWidget = { ...this.state.importWidget };
        formData.append("file", importWidget.file)
        this.props.importWidget(formData)
        importWidget.file = '';
        $('#addWidgetModal').modal('hide');
        this.setState({
            isImportModal: false,
            isFetching: true,
            importWidget
        });
    };

    onChangeHandler = event => {
        let importWidget = { ...this.state.importWidget }
        let isUploadingFile = this.state.isUploadingFile
        let fileTypeError = this.state.fileTypeError
        if (event.target.id === "file") {
            var ext = event.target.files[0].name.match(/\.([^\.]+)$/)[1];
            switch (ext) {
                case 'zip':
                    importWidget.file = event.target.files[0]
                    isUploadingFile = true;
                    fileTypeError = false
                    setTimeout(() => this.setState({ isUploadingFile: false }), 2000)
                    break;
                default:
                    fileTypeError = true
            }
        } else {
            importWidget[event.target.id] = event.target.value
        }
        this.setState({
            importWidget,
            isUploadingFile,
            fileTypeError
        })
    }

    cancelImportAddModal = () => {
        let importWidget = {
            file: '',
            fileUrl: ''
        }
        this.setState({ isImportModal: false, importWidget })
    }

    checkExport = (index) => {
        let widgetsList = JSON.parse(JSON.stringify(this.state.widgetsList))
        widgetsList[index].exportSelection = !widgetsList[index].exportSelection;

        this.setState({ widgetsList, exportErrorMessage: false })
    }

    exportWidget = () => {
        let exportPayload = {
            ids: []
        },
            widgetsList = this.state.widgetsList;
        exportPayload.ids = widgetsList.filter((item) => item.exportSelection).map((widget) => widget.id);
        if (exportPayload.ids.length < 1) {
            this.setState({ exportErrorMessage: true })
        }
        else {
            $('#exportModal').modal('show')
            this.setState({ isExporting: true }, () => this.props.exportWidget(exportPayload))
        }

    }

    goToCreateOrEditWidgetPage = () => {
        this.props.history.push("/createOrEditPage")
    }

    toggleView = () => {
        this.setState((prevState) => ({ toggleView: !prevState.toggleView }))
    }

    getColumns = () => {
        return [
            {
                Header: "Name", width: 25,
                accessor: "name",
                cell: (row) => (
                    <React.Fragment>
                        <div className="list-view-icon"><i className="fad fa-file-invoice text-dark-theme"></i></div>
                        <div className="list-view-icon-box">
                            <h6 className="text-theme fw-600">{row.name}</h6>
                            <p>{`${row.description || "-"}`}</p>
                        </div>
                    </React.Fragment>
                ),
                filterable: true,
                sortable: true
            },
            {
                Header: "Menu Enabled", width: 10,
                accessor: "menuEnabled",
                cell: (row) => (
                    <h6 className={row.isMenuAttached ? "text-green" : "text-red"}>{row.isMenuAttached ? "Yes" : "No"}</h6>
                ),
                filter: ({ value, onChangeHandler, accessor }) =>
                    <select
                        className="form-control"
                        onChange={event => onChangeHandler(event, accessor)}
                        value={value ? value : 'ALL'}
                    >
                        <option value="">ALL</option>
                        <option value="Yes">Yes</option>
                        <option value="No">No</option>
                    </select>,
                filterable: true,
                sortable: true
            },
            {
                Header: "Files Added", width: 20,
                cell: (row) => (
                    <React.Fragment>
                        <span className="alert alert-primary list-view-badge">HTML</span>
                        {row.fieldsAdded.css &&
                            <React.Fragment>
                                <span className="mr-l-10 mr-r-10">/</span>
                                <span className="alert alert-success list-view-badge">CSS</span>
                            </React.Fragment>
                        }
                        {row.fieldsAdded.js &&
                            <React.Fragment>
                                <span className="mr-l-10 mr-r-10">/</span>
                                <span className="alert alert-warning list-view-badge">JS</span>
                            </React.Fragment>
                        }
                    </React.Fragment>
                )
            },
            { Header: "Created", width: 15, sortable: true, accessor: "createdAt", filterable: true },
            { Header: "Updated", width: 15, sortable: true, accessor: "updatedAt", filterable: true },
            {
                Header: "Actions", width: 15, cell: (row) => (
                    <div className="button-group">
                        <button className="btn-transparent btn-transparent-cyan" data-tooltip data-tooltip-text="Preview" data-tooltip-place="bottom" id={row.id} onClick={this.fullScreenPage} >
                            <i className="far fa-info"></i>
                        </button>
                        <button className="btn-transparent btn-transparent-green" data-tooltip data-tooltip-text="View" data-tooltip-place="bottom" id={row.id} onClick={() => window.open(window.location.origin + '/dashboard/' + row.id, "_blank")} >
                            <i className="far fa-eye"></i>
                        </button>
                        <button className="btn-transparent btn-transparent-blue" /*disabled={row.isMenuAttached}*/ data-tooltip data-tooltip-text="Edit" data-tooltip-place="bottom" id={row.id} onClick={this.editWidget}>
                            <i className="far fa-pencil"></i>
                        </button>
                        <button className="btn-transparent btn-transparent-red" disabled={row.isMenuAttached} data-tooltip data-tooltip-text="Delete" data-tooltip-place="bottom" onClick={() => this.confirmActionDeletion(row)}>
                            <i className="far fa-trash-alt"></i>
                        </button>
                    </div>
                )
            },
        ]
    }

    getStatusBarClass = (row) => {
        if (row.isMenuAttached)
            return { className: "green", tooltipText: "Menu Enabled" }
        return { className: "gray", tooltipText: "Not Enabled" }
    }

    deletePage = (pageId) => {
        this.setState({
            isFetching: true,
        }, () => {
            this.props.deletePage(pageId)
        })
    }

    confirmActionDeletion = ({ id, name }) => {
        const confirmationMessage = name;
        const pageId = id;
        let confirmationHandler = () => {
            this.deletePage(pageId)
        }
        this.props.showConfirmationModal(confirmationMessage, confirmationHandler)
    }

    refreshComponent = () => {
        this.props.getAllPages()
        this.setState({
            isFetchingPages: true,
            widgetsList: [],
            filteredList: [],
            editorModal: false,
            isCloneWidgetLoader: false,
            importWidget: {
                file: ''
            },
            exportWidgetParam: false,
            exportWidgets: {
                fileUrl: ''
            },
            commitHistory: {
                html: [],
                css: [],
                js: [],
                htmlCommitOffset: 1,
                cssCommitOffset: 1,
                jsCommitOffset: 1,
                htmlHasMoreCommitHistory: true,
                cssHasMoreCommitHistory: true,
                jsHasMoreCommitHistory: true,
            },
            isFetchingCommitHistory: false,
            activeCommitHistoryTab: "html",
            isStatusChecked: false,
        })
    }

    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>Pages</title>
                    <meta name="description" content="M83-Pages" />
                </Helmet>

                <QuotaHeader
                    showQuota
                    addButtonText="Add Page"
                    componentName="Pages"
                    productName="pages"
                    onAddClick={this.goToCreateOrEditWidgetPage}
                    toggleButton={this.toggleView}
                    // searchBoxDetails={{
                    //     value: this.state.searchTerm,
                    //     onChangeHandler: this.filteredDataList,
                    //     isDisabled: !this.state.widgetsList.length || this.state.isFetchingPages
                    // }}
                    refreshDisabled={this.state.isFetchingPages}
                    addDisabled={this.state.isFetchingPages}
                    refreshHandler={this.refreshComponent}
                    callBack={this.qoutaCallback}
                    isDeletedSuccess={this.state.isDeletedSuccess}
                    {...this.props}
                />

                <div className="content-body">
                    {this.state.isFetchingPages ? <Skeleton count={9} className="skeleton-list-loader" /> :
                        <React.Fragment>
                            {this.state.widgetsList.length > 0 ?
                                !this.state.toggleView ?
                                    <ListingTable
                                        columns={this.getColumns()}
                                        data={this.state.filteredList}
                                        isLoading={this.state.isFetching}
                                        statusBar={this.getStatusBarClass}
                                    />
                                    :
                                    <ul className="card-view-list">
                                        {this.state.widgetsList.map((item, index) =>
                                            <li key={index}>
                                                <div className="card-view-box">
                                                    <div className="card-view-header">
                                                        {item.isMenuAttached ?
                                                            <span className="alert alert-success">Menu Enabled</span>
                                                            :
                                                            <span className="alert alert-danger">Menu Not Enabled</span>
                                                        }
                                                        <div className="dropdown">
                                                            <button className="btn-transparent btn-transparent-gray" type="button" data-toggle="dropdown">
                                                                <i className="fas fa-ellipsis-v"></i>
                                                            </button>
                                                            <div className="dropdown-menu">
                                                                <button className="dropdown-item" id={item.id} onClick={this.fullScreenPage} >
                                                                    <i className="far fa-info"></i>Preview
                                                                </button>
                                                                <button className="dropdown-item" id={item.id} onClick={() => window.open(window.location.origin + '/dashboard/' + item.id, "_blank")} >
                                                                    <i className="far fa-eye"></i>View
                                                                </button>
                                                                <button className="dropdown-item" /*disabled={row.isMenuAttached}*/ id={item.id} onClick={this.editWidget}>
                                                                    <i className="far fa-pencil"></i>Edit
                                                                </button>
                                                                <button className="dropdown-item" disabled={item.isMenuAttached} onClick={() => this.confirmActionDeletion(item)}>
                                                                    <i className="far fa-trash-alt"></i>Delete
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="card-view-body">
                                                        <div className="card-view-icon">
                                                            <i className="fad fa-file-invoice text-dark-theme"></i>
                                                        </div>
                                                        <h6><i className="far fa-address-book"></i>{item.name ? item.name : "N/A"}</h6>
                                                        <p><i className="far fa-file-alt"></i>{item.description ? item.description : "N/A"}</p>
                                                        <p>
                                                            <span className="badge badge-pill alert-primary mr-r-10">HTML</span>
                                                            {item.fieldsAdded.css &&
                                                                <span className="badge badge-pill badge-success mr-r-10">CSS</span>
                                                            }
                                                            {item.fieldsAdded.js &&
                                                                <span className="badge badge-pill alert-warning">JS</span>
                                                            }
                                                        </p>
                                                    </div>
                                                    <div className="card-view-footer d-flex">
                                                        <div className="flex-50 p-3">
                                                            <h6>Created</h6>
                                                            <p><strong>By - </strong>{item.createdBy}<small></small></p>
                                                            <p><strong>At - </strong>{new Date(item.createdAt).toLocaleString('en-US', { createdAt: localStorage.createdAt })}</p>
                                                        </div>
                                                        <div className="flex-50 p-3">
                                                            <h6>Updated</h6>
                                                            <p><strong>By - </strong>{item.updatedBy}<small></small></p>
                                                            <p><strong>At - </strong>{new Date(item.updatedAt).toLocaleString('en-US', { createdAt: localStorage.createdAt })}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        )}
                                    </ul>
                                :
                                <AddNewButton
                                    text1="No Page(s) available"
                                    text2="You haven't created any page(s) yet. Please create a page first."
                                    imageIcon="addPage.png"
                                    createItemOnAddButtonClick={this.goToCreateOrEditWidgetPage}
                                />
                            }
                        </React.Fragment>
                    }
                </div>

                {/* page full modal */}
                {this.state.showFullScreenPage &&
                    <div className="modal d-block animated slideInDown">
                        <div className="modal-dialog modal-full">
                            <div className="modal-content">
                                <div className="modal-body p-2">
                                    <div className="modal-full-editor">
                                        {this.state.isFetchingPage ?
                                            <div className="modal-loader">
                                                <i className="fad fa-sync-alt fa-spin"></i>
                                            </div> :
                                            this.state.fullScreenPage ?
                                                <div className="iframe-wrapper">
                                                    <Iframe css={this.state.fullScreenPage.code.css} js={this.state.fullScreenPage.code.js}
                                                        setting={this.state.fullScreenPage.settings} html={this.state.fullScreenPage.code.html} id="iframe"
                                                    />
                                                </div> :
                                                <p className="text-danger"> An error occured while fetching the widget.</p>
                                        }
                                        <button type="button" className="btn btn-light text-dark" onClick={() => this.setState({ fullScreenPage: null, showFullScreenPage: false, isFetchingPage: false })}>
                                            <i className="fas fa-compress"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                }
                {/* end page full modal */}

                {this.state.isOpen &&
                    <NotificationModal
                        type={this.state.type}
                        message2={this.state.message2}
                        onCloseHandler={this.onCloseHandler}
                    />
                }
            </React.Fragment>
        );
    }
}

WidgetStudio.propTypes = {
    dispatch: PropTypes.func.isRequired
};

let allSelectors = {}
Object.entries(SELECTORS).map(([key, value]) => {
    allSelectors[key] = value()
})

const mapStateToProps = createStructuredSelector(allSelectors);

function mapDispatchToProps(dispatch) {
    let allActions = { dispatch }
    Object.entries(ACTIONS).map(([key, value]) => {
        allActions[key] = (...args) => dispatch(value(...args))
    })
    return allActions;
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "widgetStudio", reducer });
const withSaga = injectSaga({ key: "widgetStudio", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(WidgetStudio);
