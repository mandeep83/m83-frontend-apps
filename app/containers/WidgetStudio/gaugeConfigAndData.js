/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import * as am4core from "@amcharts/amcharts4/core";
let colorSe = new am4core.ColorSet();
let arr = []

export function createGaugeFromConfig(widgetsType, chartData, id) {
    if ((chartData && chartData.length === 0) || JSON.stringify(chartData) === "{}") {
        chartData = null;
    }
    let min, max;
    var colorSet = ["#67b7dc", "#dc67ab", "#8067dc"]
    if (widgetsType === "animatedGauge" || widgetsType === "configurableGauge") {
        min = Array.isArray(chartData) ? Math.ceil(chartData[0].min) : 0;
        max = Array.isArray(chartData) ? Math.ceil(chartData[0].max) : 100;
    }
    for (var i = 0; i <= 10; i++) {
        arr.push(colorSe.next())
    }
    let diff;
    if((min <0 && max<0) || (min >0 && max>0)){
        diff = Math.abs(Math.abs(max) - Math.abs(min));
    }
    else{
        diff = Math.abs(max) + Math.abs(min);
    }
    
    let config = {
        "type": "GaugeChart",
        "container": id,
        "xAxes": [{
            "type": "ValueAxis",
            "min": min ,
            "max": max ,
            "strictMinMax": true,
            "axisRanges": [{
                "value": min,
                "endValue": min + (diff / 2),
                "axisFill": {
                    "fillOpacity": 1,
                    "fill": colorSet[0],
                    "zIndex": -1
                }
            }, {
                "value": min + (diff / 2),
                "endValue": max,
                "axisFill": {
                    "fillOpacity": 1,
                    "fill": colorSet[1],
                    "zIndex": -1
                }
            }]
        }],
        "export": {
            "enabled": true
        }
    }

    switch (widgetsType) {
        case "clock":
            config.hiddenState = {
                "properties": {
                    "opacity": 0
                }
            };

            config.startAngle = -90;
            config.endAngle = 270;
            config.xAxes = [{
                "type": "ValueAxis",
                "min": 0,
                "max": 12,
                "strictMinMax": true,
                "renderer": {
                    "line": {
                        "strokeWidth": 8,
                        "strokeOpacity": 8,
                    },
                    "minLabelPosition": 0.05,
                    "inside": true,
                    "labels": {
                        "template": {
                            "radius": 35
                        }
                    },
                    "axisFills": {
                        "template": {
                            "disabled": true
                        }
                    },
                    "grid": {
                        "template": {
                            "disabled": true
                        }
                    },
                    "ticks": {
                        "template": {
                            "length": 12,
                            "strokeOpacity": 1
                        }
                    },
                },
                "axisRanges": {
                    "startValue": 0,
                    "endValue": 12,
                    "grid": {
                        "visible": false
                    },
                    "tick": {
                        "visible": false
                    },
                    "label": {
                        "visible": false
                    },
                    "axisFill": {
                        "fillOpacity": 1,
                        "disabled": false,
                        "fill": "fill"
                    }
                }
            }];
            config.hands = [
                {
                    "type": "ClockHand",
                    "radius": "60%",
                    "startWidth": 10,
                    "endWidth": 10,
                    "rotationDirection": "clockWise",
                    "zIndex": 0,
                    "pin": {
                        "radius": 8
                    }
                },
                {
                    "type": "ClockHand",
                    "radius": "78%",
                    "startWidth": 7,
                    "endWidth": 7,
                    "rotationDirection": "clockWise",
                    "zIndex": 1
                },
                {
                    "type": "ClockHand",
                    "radius": "85%",
                    "fill": "#DD0000",
                    "stroke": "#DD0000",
                    "startWidth": 1,
                    "rotationDirection": "clockWise",
                    "zIndex": 2
                }
            ];
            break;
        case "animatedGauge" :
        case "configurableGauge" : 
            config.innerRadius = "-25";
            config.radarContainer = {
                "children": [{
                    "type": widgetsType,
                    "children": [{
                        "type": "Label",
                        "text": chartData && typeof chartData[0].value === "number" ? chartData[0].value.toFixed(2) + " " + chartData[0].unit : "Test - 50%",
                        "fontWeight": 600,
                        "horizontalCenter": "middle",
                        "verticalCenter": "bottom",
                        "align": "center",
                    }]
                }]
            }
            // config.xAxes[0].axisRanges.push({
            //     "value": 80,
            //     "endValue": 100,
            //     "axisFill": {
            //         "fillOpacity": 1,
            //         "fill": colorSet.getIndex(2),
            //         "zIndex": -1
            //     }
            // })
            config.hands = [{
                "type": "ClockHand",
                "fill": colorSet[2],
                "axis": "axis1",
                "innerRadius": "15%",
                "startWidth": 10,
                "value": chartData && typeof chartData[0].value === "number" ? chartData[0].value : 50,
                "pin": {
                    "disabled": true
                },
            }]

            if (chartData && chartData[0].name) {
                config.legend = {
                    "y": am4core.percent(100),
                    // "parent": widgetsType,   gauge container error 
                    verticalCenter: "bottom",
                    "data": [{
                        "name": chartData[0].name,
                        "fill": colorSet[2]
                    }]
                }
            }
            if(widgetsType === "configurableGauge") {
                if(chartData && chartData[0].axisRanges){
                    config.xAxes[0].axisRanges = axisRanges
                }
                else{
                    let diff;
                    if((min <0 && max<0) || (min >0 && max>0)){
                        diff = Math.abs(Math.abs(max) - Math.abs(min));
                    }
                    else{
                        diff = Math.abs(max) + Math.abs(min);
                    }
                    let quarter = diff/4
                    config.xAxes[0].axisRanges = [{
                        "value": min,
                        "endValue": (min + quarter),
                        "axisFill": {
                            "fillOpacity": 1,
                            "fill": "#E5391A",
                            "zIndex": -1
                        }
                    },
                    {
                        "value": (min + quarter),
                        "endValue": (min + (2*quarter)),
                        "axisFill": {
                            "fillOpacity": 1,
                            "fill": "#F5EA0C",
                            "zIndex": -1
                        }
                    },
                    {
                        "value": (min + (2*quarter)),
                        "endValue": (min + (3*quarter)),
                        "axisFill": {
                            "fillOpacity": 1,
                            "fill": "#0CABF5",
                            "zIndex": -1
                        }
                    },
                    {
                        "value": (min + (3*quarter)),
                        "endValue": max,
                        "axisFill": {
                            "fillOpacity": 1,
                            "fill": "#6FF50C",
                            "zIndex": -1
                        }
                    }];
                }
            }
            break;
        case "animatedGaugeWithTwoAxes":
            let mins, maxs;
            if (chartData) {
                mins = [Math.floor(chartData[0]).min, chartData[1] && Math.floor(chartData[1]).max];
                maxs = [Math.floor(chartData[0]).max, chartData[1] && Math.floor(chartData[1]).max];
            }
            config.innerRadius = "-25";

            config.listTemplate = {
                type: "Label",
                template: {
                    "background": {
                        "strokeWidth": 2
                    },
                    "fontSize": 25,
                    "y": am4core.percent(50),
                    "horizontalCenter": "middle",
                    "labels": [{
                        // "parent": widgetsType,
                        "x": am4core.percent(40),
                        "background": {
                            "stroke": colorSet[0],
                        },
                        "fill": colorSet[0],
                        "text": "0",
                    },
                    {
                        // "parent": widgetsType,
                        "x": am4core.percent(60),
                        "background": {
                            "stroke": colorSet[1],
                        },
                        "fill": colorSet[1],
                        "text": "0",
                    }]
                }
            }
            config.xAxes = [{
                min: chartData ? chartData[0].min : 0,
                max: chartData ? chartData[0].max : 100,
                id: "axis1",
                "type": "ValueAxis",
                strictMinMax: true,
                hiddenState: {
                    properties: {
                        opacity: 1,
                        visible: true
                    }
                },
                setStateOnChildren: true,
                stroke: colorSet[0],
                renderer: {
                    inside: true,
                    radius: am4core.percent(97),
                    hiddenState: {
                        properties: {
                            endAngle: 180
                        }
                    },
                    line: {
                        strokeOpacity: 1,
                        strokeWidth: 5,
                        stroke: colorSet[0],
                    },
                    grid: {
                        template: {
                            disabled: true
                        }
                    },
                    ticks: {
                        template: {
                            strokeOpacity: 1,
                            inside: true,
                            length: 10,
                            stroke: colorSet[0],
                        }
                    },
                    labels: {
                        template: {
                            radius: 35
                        }
                    }
                },
                "axisRanges": [{
                    "startValue": chartData ? mins[0] : 0,
                    "endValue": chartData ? mins[0] + ((maxs[0] - mins[0]) / 2) : 50,
                    "grid": {
                        "visible": false
                    },
                    "tick": {
                        "visible": false
                    },
                    "label": {
                        "visible": false
                    },
                    "axisFill": {
                        "fillOpacity": 1,
                        "fill": colorSet[0],
                        "zIndex": -1
                    }
                },
                {
                    "startValue": chartData ? mins[0] + ((maxs[0] - mins[0]) / 2) : 50,
                    "endValue": chartData ? maxs[0] : 100,
                    "grid": {
                        "visible": false
                    },
                    "tick": {
                        "visible": false
                    },
                    "label": {
                        "visible": false
                    },
                    "axisFill": {
                        "fillOpacity": 1,
                        "fill": colorSet[1],
                        "zIndex": -1
                    }
                }]
            },
            {
                min: chartData && chartData[1] ? chartData[1].min : 0,
                max: chartData && chartData[1] ? chartData[1].max : 100,
                id: "axis2",
                "type": "ValueAxis",
                strictMinMax: true,
                hiddenState: {
                    properties: {
                        opacity: 1,
                        visible: true
                    }
                },
                setStateOnChildren: true,
                stroke: colorSet[1],
                renderer: {
                    hiddenState: {
                        properties: {
                            endAngle: 180
                        }
                    },
                    line: {
                        strokeOpacity: 1,
                        strokeWidth: 2,
                        stroke: colorSet[1],
                    },
                    grid: {
                        template: {
                            disabled: true
                        }
                    },
                    ticks: {
                        template: {
                            strokeOpacity: 1,
                            length: 5,
                            stroke: colorSet[1],
                        }
                    },
                },
                "axisRanges": [{
                    "startValue": chartData ? mins[1] : 0,
                    "endValue": chartData ? mins[1] + ((maxs[1] - mins[1]) / 2) : 50,
                    "grid": {
                        "visible": false
                    },
                    "tick": {
                        "visible": false
                    },
                    "label": {
                        "visible": false
                    },
                    "axisFill": {
                        "fillOpacity": 1,
                        "fill": colorSet[0],
                        "zIndex": -1
                    }
                },
                {
                    "startValue": chartData ? mins[1] + ((maxs[1] - mins[1]) / 2) : 50,
                    "endValue": chartData ? maxs[1] : 100,
                    "grid": {
                        "visible": false
                    },
                    "tick": {
                        "visible": false
                    },
                    "label": {
                        "visible": false
                    },
                    "axisFill": {
                        "fillOpacity": 1,
                        "fill": colorSet[1],
                        "zIndex": -1
                    }
                }]

            }]
            config.hands = [{
                "type": "ClockHand",
                id: "h1",
                "fill": colorSet[0],
                "axis": "axis1",
                "innerRadius": "15%",
                "startWidth": 10,
                "value": chartData ? chartData[0].value : 10,
                // "pin": {
                //     "radius": 14
                // },
            }, {
                "type": "ClockHand",
                "fill": colorSet[1],
                "axis": "axis2",
                "innerRadius": "15%",
                "startWidth": 10,
                "value": chartData && chartData[1] ? chartData[1].value : 15,
                // "pin": {
                //     "radius": 10
                // },
            }]
            if (chartData) {
                config.radarContainer = {
                    "children": [{
                        "type": widgetsType,
                        "children": [{
                            "type": "Label",
                            "text": typeof chartData[0].value === "number" ? chartData[0].value.toFixed(2) + " " + chartData[0].unit : "Test - 50%",
                            "fontWeight": 600,
                            "fill": colorSet[0],
                            "horizontalCenter": "right",
                            "verticalCenter": "bottom",
                            "align": "center",
                        },
                        {
                            "type": "Label",
                            "text": chartData && chartData[1] && typeof chartData[1].value == "number" ? chartData[1].value.toFixed(2) + " " + chartData[1].unit : "Test - 50%",
                            "fontWeight": 600,
                            "horizontalCenter": "left",
                            "fill": colorSet[1],
                            "verticalCenter": "bottom",
                            "align": "center",
                        }]
                    }]
                }
                config.legend = {
                    "y": am4core.percent(100),
                    // "parent": widgetsType,
                    "data": [{
                        "name": chartData[0].name,
                        "fill": colorSet[0]
                    }, {
                        "name": chartData && chartData[1] && chartData[1].name,
                        "fill": colorSet[1]
                    }]

                }
            }
            break;
    }
    return config;
}
