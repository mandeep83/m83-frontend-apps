/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import * as am4core from "@amcharts/amcharts4/core";

export function createBarFromConfig(widgetType, chartData, customValues, id, deleteLegend, theme) {
    let colorSet = new am4core.ColorSet();
    let scrollbarX
    let scrollbarY
    let series = chartData.availableAttributes.map((attr, index) => {
        return {
            "type": "XYChart",
            "strokeWidth": 0,
            "id": attr.attr,
            "name": attr.attrDisplayName,
            "dataFields": {
                "valueY": attr.attr,
                "categoryX": "category",
                "categoryY": "categoryDisplayName"
            },
            adapter: {
                tooltipHTML: (text, target) => {
                    return "{categoryY}" + " : {valueY} " + (chartData.units && chartData.units[target.tooltipDataItem.dataContext.category] && chartData.units[target.tooltipDataItem.dataContext.category] ? chartData.units[target.tooltipDataItem.dataContext.category] : "")

                },
            },
            tension: 1,
            bullets: [{
                type: "LabelBullet",
                interactionsEnabled: false,
                label: {
                    text: "{valueY}",
                    dy: -14,
                    fontSize: 10
                }
            }],
            tooltip: {
                "getFillFromObject": true,
                // "background": {
                //     "fill": "#0000",
                //     "filters": [{
                //         "opacity": 2
                //     }]
                // },   
                "label": {
                    "fill": "#fff",
                    "fontSize": 12,
                },
            },
            "columns": {
                "template": {
                    "width": Number(customValues.barWidth.value),
                    "height": am4core.percent(100),
                    "sequencedInterpolation": true,
                    fillOpacity: 0.75,
                    "adapter": {
                        "fill": function (fill, target) {
                            return colorSet.list[target.dataItem.index];
                        }
                    },
                    "states": {
                        "hover": {
                            "properties": {
                                "fillOpacity": 1,
                                "tension": 0.8
                            }
                        },
                    }
                },
            },
            "stacked": false
        }
    })
    let config = {
        "type": "XYChart",
        "container": id,
        "data": chartData.chartData,
        "legend": {
            "useDefaultMarker": true,
            "markers": {
                "children": [{
                    "cornerRadiusTopLeft": 12,
                    "cornerRadiusTopRight": 12,
                    "cornerRadiusBottomRight": 12,
                    "cornerRadiusBottomLeft": 12,
                    // "strokeWidth": 2,
                    // "strokeOpacity": 1,
                    // "stroke": "#ccc"
                }],
                "dy": -1,
                "dx": -4,
                "width": 12,
                "height": 12
            },
            "labels": {
                "template": {
                    "fill": "#4a4a4a",
                    "fontSize": "13"
                }
            },
            "position": customValues.legendPlacement.value,
            "paddingTop": 10,
        },
        "width": "100%",
        "height": "100%",
        "xAxes": [{
            "type": "CategoryAxis",
            // "adapter": {
            //     "getTooltipText": function (text, target, key) {
            //         return "{valueY}";
            //     },
            // },
            "dataFields": {
                // "valueY": "categoryDisplayName",
                "category": "category",
            },
            "renderer": {
                "labels": {
                    // "text": "test",
                    "truncate": true,
                    "maxWidth": 100,
                    "template": {
                        "adapter": {
                            "textOutput": function (text, target) {
                                if (target.dataItem.dataContext)
                                    return target.dataItem.dataContext.categoryDisplayName + (chartData.units && chartData.units[target.dataItem.dataContext.category] ? " (" + chartData.units[target.dataItem.dataContext.category] + ")" : "");
                            }
                        },
                    }
                },
                "line": {
                    "strokeOpacity": 0.5,
                    "strokeWidth": 1,
                },
                "grid": {
                    "strokeOpacity": 0.2,
                    "strokeWidth": 0.5
                },
            },
            "fontSize": 12,
            grid: {
                template: {
                    location: 0
                }
            },
            minGridDistance: 30
        }],
        "yAxes": [{
            "type": "ValueAxis",
            // "title":{  // can be used for giving title to value axis
            //     text : "Turnover ($M)"
            // },
            renderer: {
                // opposite: true,
                "line": {
                    "strokeOpacity": 0.5,
                    "strokeWidth": 1,
                },
                "grid": {
                    "strokeOpacity": 0.2,
                    // "stroke": "#A0CA92",
                    "strokeWidth": 0.5
                },
            },
            extraMax: 0.1,
            // min: 0,
            // max: 100,
            axisRanges: {
                // "type": "range",
                // "value": 50,
                // "endValue": 1200,
                "contents": {
                    "stroke": "#FF0000",
                    "fill": "#FF0000"
                },
            },
            // baseValue: 50
        }],
        "series": series,
        "cursor": {
            "behavior": "zoomXY"
        },
        "scrollbarX": scrollbarX,
        "scrollbarY": scrollbarY,
    };

    switch (widgetType) {
        case "3DCylinderChart":
            config.type = "XYChart3D"
            series.map(temp => temp.type = "ConeSeries")
            break;
        case "3DColumnChart":
            config.type = "XYChart3D"
            config.depth = Number(customValues.chartDepth.value);
            config.angle = Number(customValues.chartAngle.value);
            series.map(temp => temp.type = "ColumnSeries3D")
            break;
        case "basicBarChart":
            series.map(temp => {
                temp.type = "ColumnSeries";
                temp.columns.template.column = {
                    "fillOpacity": 0.8,
                    "cornerRadiusTopRight": Number(customValues.columnTopRadius.value),
                    "cornerRadiusTopLeft": Number(customValues.columnTopRadius.value),
                    "states": {
                        "hover": {
                            "properties": {
                                "cornerRadiusTopLeft": 0,
                                "cornerRadiusTopRight": 0,
                                "fillOpacity": 1,
                                // "tension": 0.8
                            }
                        },
                    }
                }
            })
            break;
        case "layeredColumnChart":
            series.map(temp => {
                temp.type = "ColumnSeries"
                temp.clustered = false
                let columns = JSON.parse(JSON.stringify(temp.columns))
                temp.columns = {
                    ...columns,
                    "propertyFields": {
                        "fill": "color"
                    },
                    "tooltipText": "{valueX}",
                    "column3D": {
                        "stroke": "#fff",
                        "strokeOpacity": 0.2
                    }
                }
            })
            series[1].columns.template = { width: am4core.percent(50) }
            break;
        default:
    }
    if (deleteLegend) {
        delete config.legend;
    }
    return config;
}

export function createBarFromConfigForMl(widgetType, chartData, customValues, id, deleteLegend, theme) {
    let colorSet = new am4core.ColorSet();
    let scrollbarX
    let scrollbarY
    let series = chartData.availableAttributes.map((attr, index) => {
        return {
            "type": "XYChart",
            "strokeWidth": 1,
            "id": "series" + index,
            "name": attr,
            "dataFields": {
                "valueY": attr,
                "categoryX": "category",
            },
            "tooltipText": "[bold]{valueY}[/] " + (chartData.units && chartData.units[attr] ? chartData.units[attr] === "PERCENT" ? "%" : chartData.units[attr] : ""),
            tension: 1,
            "tooltip": {
                "getFillFromObject": true,
                // "background": {
                //     "fill": "#0000",
                //     "filters": [{
                //         "opacity": 2
                //     }]
                // },
                "label": {
                    "fill": "#fff"
                }
            },
            "columns": {
                "template": {
                    "width": Number(customValues.barWidth.value),
                    "height": am4core.percent(100),
                    "sequencedInterpolation": true,
                    "tooltipText": attr + ":{" + attr + "} ",
                    fillOpacity: 0.75,
                    "adapter": {
                        "fill": function (fill, target) {
                            return colorSet.list[target.dataItem.index];
                        }
                    },
                    "states": {
                        "hover": {
                            "properties": {
                                "fillOpacity": 1,
                                "tension": 0.8
                            }
                        },
                    }
                },
            },
            "stacked": false
        }
    })
    let config = {
        "type": "XYChart",
        "container": id,
        "data": chartData.chartData,
        "legend": {
            "position": customValues.legendPlacement.value,
        },
        "width": "100%",
        "height": "100%",
        "xAxes": [{
            "type": "CategoryAxis",
            "dataFields": {
                "category": "category",
                // "title": {
                //     "text": "Devices"
                // }
            },
            "renderer": {
                "labels": {
                    "wrap": true,
                    "maxWidth": 90
                }
            },
            "fontSize": "12px",
            grid: {
                template: {
                    location: 0
                }
            },
            minGridDistance: 30
        }],
        "yAxes": [{
            "type": "ValueAxis",
            // "title":{  // can be used for giving title to value axis
            //     text : "Turnover ($M)"
            // },
            renderer: {
                // opposite: true,
            },
            // min: 0,
            // max: 100,
            axisRanges: {
                // "type": "range",
                // "value": 50,
                // "endValue": 1200,
                "contents": {
                    "stroke": "#FF0000",
                    "fill": "#FF0000"
                },
            },
            // baseValue: 50
        }],
        "series": series,
        "cursor": {
            "behavior": "zoomXY"
        },
        "scrollbarX": scrollbarX,
        "scrollbarY": scrollbarY,
    };

    switch (widgetType) {
        case "3DCylinderChart":
            config.type = "XYChart3D"
            series.map(temp => temp.type = "ConeSeries")
            break;
        case "3DColumnChart":
            config.type = "XYChart3D"
            config.depth = Number(customValues.chartDepth.value);
            config.angle = Number(customValues.chartAngle.value);
            series.map(temp => temp.type = "ColumnSeries3D")
            break;
        case "basicBarChart":
            series.map(temp => {
                temp.type = "ColumnSeries";
                temp.columns.template.column = {
                    "fillOpacity": 0.8,
                    "cornerRadiusTopRight": Number(customValues.columnTopRadius.value),
                    "cornerRadiusTopLeft": Number(customValues.columnTopRadius.value),
                    "states": {
                        "hover": {
                            "properties": {
                                "cornerRadiusTopLeft": 0,
                                "cornerRadiusTopRight": 0,
                                "fillOpacity": 1,
                                // "tension": 0.8
                            }
                        },
                    }
                }
            })
            break;
        case "layeredColumnChart":
            series.map(temp => {
                temp.type = "ColumnSeries"
                temp.clustered = false
                let columns = JSON.parse(JSON.stringify(temp.columns))
                temp.columns = {
                    ...columns,
                    "propertyFields": {
                        "fill": "color"
                    },
                    "tooltipText": "{valueX}",
                    "column3D": {
                        "stroke": "#fff",
                        "strokeOpacity": 0.2
                    }
                }
            })
            series[1].columns.template = { width: am4core.percent(50) }
            break;
        default:
    }
    if (deleteLegend) {
        delete config.legend;
    }
    return config;
}





