/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/*
 *
 * SignUp constants
 *
 */

export const RESET_TO_INITIAL_STATE = "app/SignUp/RESET_TO_INITIAL_STATE";


export const GET_ALL_PRODUCTS = {
	action: 'app/SignUp/GET_ALL_PRODUCTS',
	success: "app/SignUp/GET_ALL_PRODUCTS_SUCCESS",
	failure: "app/SignUp/GET_ALL_PRODUCTS_FAILURE",
	urlKey: "getAllProductsUnAuth",
	successKey: "allProducts",
	failureKey: "allProductsFailure",
	actionName: "getAllProducts",
	actionArguments: []
}

export const GET_LICENSE_INFO = {
	action: 'app/SignUp/GET_LICENSE_INFO',
	success: "app/SignUp/GET_LICENSE_INFO_SUCCESS",
	failure: "app/SignUp/GET_LICENSE_INFO_FAILURE",
	urlKey: "fetchLicenseDetails",
	successKey: "getLicenseSuccess",
	failureKey: "getLicenseFailure",
	actionName: "getLicenseInfo",
	actionArguments: []
}

export const CREATE_TENANT = {
	action: 'app/SignUp/CREATE_TENANT',
	success: "app/SignUp/CREATE_TENANT_SUCCESS",
	failure: "app/SignUp/CREATE_TENANT_FAILURE",
	urlKey: "saveAccountDetails",
	successKey: "createTenantSuccess",
	failureKey: "createTenantFailure",
	actionName: "createTenant",
	actionArguments: ["payload"]
}

export const GET_ADD_ONS = {
	action: 'app/SignUp/GET_ADD_ONS',
	success: "app/SignUp/GET_ADD_ONS_SUCCESS",
	failure: "app/SignUp/GET_ADD_ONS_FAILURE",
	urlKey: "getAddOns",
	successKey: "getAddOnsSuccess",
	failureKey: "getAddOnsFailure",
	actionName: "getAddOns",
	actionArguments: []
}

export const VALIDATE_TENANT_NAME = {
	action: 'app/SignUp/VALIDATE_TENANT_NAME',
	success: "app/SignUp/VALIDATE_TENANT_NAME_SUCCESS",
	failure: "app/SignUp/VALIDATE_TENANT_NAME_FAILURE",
	urlKey: "validateTenantName",
	successKey: "validateTenantNameSuccess",
	failureKey: "validateTenantNameFailure",
	actionName: "validateTenantName",
	actionArguments: ["tenantName"]
}

export const GET_OTP = {
	action: 'app/SignUp/GET_OTP',
	success: "app/SignUp/GET_OTP_SUCCESS",
	failure: "app/SignUp/GET_OTP_FAILURE",
	urlKey: "getOtp",
	successKey: "getOtpSuccess",
	failureKey: "getOtpFailure",
	actionName: "getOtp",
	actionArguments: ["payload"]


}


export const OTP_STATUS = {
action: 'app/SignUp/OTP_STATUS',
success: "app/SignUp/OTP_STATUS_SUCCESS",
failure: "app/SignUp/OTP_STATUS_FAILURE",
urlKey: "otpStatus",
successKey: "otpStatusSuccess",
failureKey: "otpStatusFailure",
actionName: "otpStatus",
actionArguments: [""]

		
}


