/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * SignUp
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import reducer from "./reducer";
import saga from "./saga";
import { allSelectors as SELECTORS } from "./selectors";
import { allActions as ACTIONS } from './actions'
import { getNotificationObj } from "../../commonUtils";
import cloneDeep from "lodash/cloneDeep";
import PhoneInput from "react-phone-input-2";
import 'react-phone-input-2/lib/style.css';
import NotificationModal from '../../components/NotificationModal/Loadable';
import ReCAPTCHA from "react-google-recaptcha";
import * as yup from 'yup';

/* eslint-disable react/prefer-stateless-function */
let interval = undefined;

export class SignUp extends React.Component {

    state = {
        isFetching: true,
        allProducts: [],
        payload: {
            tenantName: "",
            companyName: "",
            users: [{
                firstName: "",
                lastName: "",
                mobile: "",
                email: "",
            }],
            otp: "",
            tenantCategory: "FLEX",
            oAuthProviders: null,
            licenseId: "",
            addOnsList: [],
            trialOpted: true,
            captchaValue: null,
        },
        otpCount: 0,
        license: null,
        isTenantNameUnique: false,
        isTenantNameChanged: true,
        isConfirm: false,
        isMobileVerification: false,
        btnDisabled: false,
        isTenantNameValidateLoader: false,
        isTenantNameChangedAfterOtp: false,
        isTenantCreatedSuccessfully: false,
        createTimeInterval: () => this.createIntervalForButton(),
        timeSeconds: 60,
    }

    componentDidMount() {
        this.props.getLicenseInfo();
        this.props.otpStatus();
    }

    createIntervalForButton = () => {
        interval = setInterval(() => {
            if ((this.state.timeSeconds - 1) > 0) {
                this.setState(prevState => ({
                    timeSeconds: prevState.timeSeconds - 1
                }))
            } else {
                clearInterval(interval)
                interval = undefined
                this.setState({
                    timeSeconds: 60
                })
            }
        }, 1000)
    }

    static getDerivedStateFromProps(nextProps, state) {
        if (nextProps.getLicenseSuccess) {
            let license = nextProps.getLicenseSuccess.response.find(el => el.tenantCategory === "FLEX") || {}
            let payload = cloneDeep(state.payload)
            payload.licenseId = license.id;
            return {
                license,
                payload,
                isFetching: false
            }
        }

        if (nextProps.createTenantSuccess) {
            return {
                isTenantCreatedSuccessfully: true,
            }
        }

        if (nextProps.createTenantFailure) {
            return {
                signUpInProgress: false,
                modalType: "error",
                isOpen: true,
                message2: nextProps.createTenantFailure.error,
                isLoading: false
            }

        }

        if (nextProps.validateTenantNameSuccess) {
            let response = nextProps.validateTenantNameSuccess.response
            return {
                isTenantNameUnique: response,
                isTenantNameChanged: false,
                isTenantNameValidateLoader: false,
            }
        }

        if (nextProps.validateTenantNameFailure) {
            return {
                isTenantNameValidateLoader: false,
                isOpen: true,
                message2: nextProps.validateTenantNameFailure.error,
                modalType: "error",
            }
        }

        if (nextProps.otpStatusSuccess) {
            let response = nextProps.otpStatusSuccess.response.data.enableOtp
            return {
                isLoading: false,
                isMobileVerification: response
            }
        }

        if (nextProps.otpStatusFailure) {
            return {
                modalType: "error",
                isOpen: true,
                message2: nextProps.otpStatusFailure.error,
                isLoading: false,
            }
        }

        if (nextProps.getOtpSuccess) {
            let { message, otpCount, tenantName } = nextProps.getOtpSuccess.response,
                isTenantNameChangedAfterOtp = false;
            if (tenantName !== state.payload.tenantName) {
                otpCount = 0
                isTenantNameChangedAfterOtp = true
            }
            if (otpCount < 2)
                state.createTimeInterval();
            return {
                modalType: "success",
                isOpen: true,
                btnDisabled: false,
                message2: message,
                isLoading: false,
                otpCount,
                isTenantNameChangedAfterOtp
            }
        }

        if (nextProps.getOtpFailure) {
            let otpCount = state.otpCount
            if (nextProps.getOtpFailure.error == "OTP Limit reached for this Phone Number") {
                otpCount = 3
            }
            return {
                modalType: "error",
                isOpen: true,
                isLoading: false,
                btnDisabled: false,
                message2: nextProps.getOtpFailure.error,
                otpCount
            }
        }

        if (Object.keys(SELECTORS).filter(prop => prop.toLowerCase().includes("success")).some(prop => nextProps[prop])) {
            let propName = Object.keys(SELECTORS).find(props => nextProps[props]);
            return { ...getNotificationObj(nextProps[propName].response, "success") }
        }

        if (Object.keys(SELECTORS).filter(prop => prop.toLowerCase().includes("failure")).some(prop => nextProps[prop])) {
            let propName = Object.keys(SELECTORS).find(props => nextProps[props]);
            return { ...getNotificationObj(nextProps[propName].error, "error"), isFetching: false }
        }
        return null
    }

    componentDidUpdate(prevProps, prevState) {
        if (Object.keys(SELECTORS).some(prop => this.props[prop])) {
            this.props.resetToInitialState()
        }
    }

    onCloseHandler = () => {
        this.setState({
            isOpen: false,
            message2: '',
            modalType: "",
        })
    }

    basicInfoChangeHandler = ({ currentTarget }) => {
        let payload = cloneDeep(this.state.payload),
            isTenantNameChanged = this.state.isTenantNameChanged,
            isTenantNameChangedAfterOtp = this.state.isTenantNameChangedAfterOtp,
            otpCount = this.state.otpCount,
            isTenantNameValidateLoader = this.state.isTenantNameValidateLoader;
        if (currentTarget.name === "users") {
            payload.users[0][currentTarget.id] = currentTarget.value
        }
        else if (currentTarget.id === "tenantName") {
            if (/^[a-zA-Z0-9-]+$/.test(currentTarget.value) || /^$/.test(currentTarget.value)) {
                payload.tenantName = (currentTarget.value).toLowerCase()
                isTenantNameChanged = true
                isTenantNameChangedAfterOtp = Boolean(otpCount)
                if (isTenantNameChangedAfterOtp) {
                    payload.otp = ""
                    otpCount = 0
                }
                isTenantNameValidateLoader = payload.tenantName.length > 5
                this.validateTenantName(payload.tenantName)
            }
        } else if (currentTarget.id === "otp") {
            const value = currentTarget.value.slice(0, currentTarget.maxLength);
            payload[currentTarget.id] = value;
        }
        else {
            payload[currentTarget.id] = currentTarget.value
        }
        this.setState({
            payload,
            isTenantNameChanged,
            isTenantNameChangedAfterOtp,
            otpCount,
            isTenantNameValidateLoader
        })
    }

    reviewPayload = () => {
        return yup.object().shape({
            "tenantName": yup.string().required("Tenant Name is Required").matches(/^\S*$/, 'Tenant Name should not include spaces').min(6, "The Tenant name must be minimum 6 characters long").max(20, "The Tenant name must be maximum 20 characters long"),
            // "companyName": yup.string().required("Company Name is required"),
            "users": yup.array().of(
                yup.object().shape({
                    firstName: yup.string().required("First Name is required"),
                    lastName: yup.string().required("Last Name is required"),
                    mobile: this.state.isMobileVerification ? yup.string().required("Mobile is required") : yup.string(),
                    email: yup.string().required("Email is required").email("Email must be a valid email"),
                })
            ),
        });
    }

    signUpHandler = async (e) => {
        e.preventDefault();
        let payload = cloneDeep(this.state.payload);
        let PAYLOAD_SCHEMA = this.reviewPayload();
        if (this.state.isMobileVerification && (this.state.isTenantNameChangedAfterOtp || !this.state.otpCount)) {
            return this.setState({
                modalType: "error",
                isOpen: true,
                message2: "Please generate an OTP to move to next step.",
            })
        }
        try {
            await PAYLOAD_SCHEMA.validate(payload);
            this.setState({
                signUpInProgress: true,
            }, () => {
                this.props.createTenant(payload);
            })
        }
        catch (error) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: error.message,
            })
        }
    }

    validateTenantName = (tenantName) => { // check whether tenantName is unique
        if (tenantName.length >= 6) {
            clearTimeout(this.timer);
            this.timer = setTimeout(() => {
                this.props.validateTenantName(tenantName)
            }, 1000)
        }
    }

    validateNextButton = () => {
        return (this.state.payload.tenantName.length == 0 ||
            (this.state.isMobileVerification && this.state.payload.users[0].mobile.length < 10) ||
            this.state.payload.users[0].email.length == 0 ||
            this.state.isTenantNameValidateLoader ||
            (this.state.isMobileVerification ? this.state.payload.otp.length === 0 : !(this.state.isConfirm)) ||
            ((!this.state.isTenantNameChanged) && !this.state.isTenantNameUnique) ||
            this.state.payload.users[0].firstName.length == 0 || this.state.payload.users[0].lastName.length == 0)
    }

    captchaOnChangeHandler = (value) => {
        let payload = cloneDeep(this.state.payload);
        payload.captchaValue = value;
        this.setState({
            payload,
            isConfirm: true,
        })
    }

    closeTenantCreationModal = () => {
        this.setState({
            signUpInProgress: false,
            isTenantCreatedSuccessfully: false,
        }, () => { window.location.reload() });
    }

    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>Sign Up</title>
                    <meta name="description" content="Description of Sign Up" />
                </Helmet>

                {this.state.isFetching ?
                    <div className="login-outer-box sign-up-wrapper">
                        <div className="sign-up-wrapper-inner sign-up-wrapper-loader">
                            <div className="login-logo">
                                <img src="https://content.iot83.com/m83/misc/flexLogoWhite.png" />
                            </div>

                            <div className="login-form-box">
                                <div className="form-group">
                                    <div className="form-group-label blink-animation"></div>
                                    <div className="form-control blink-animation"></div>
                                </div>
                                <div className="form-group">
                                    <div className="alert alert-primary blink-animation"></div>
                                </div>
                                <div className="form-group">
                                    <div className="form-group-label blink-animation"></div>
                                    <div className="form-control blink-animation"></div>
                                </div>
                                <div className="form-group">
                                    <div className="form-group-label blink-animation"></div>
                                    <div className="form-control blink-animation"></div>
                                </div>
                                <div className="form-group">
                                    <div className="form-group-label blink-animation"></div>
                                    <div className="form-control blink-animation"></div>
                                </div>
                                <div className="form-group">
                                    <div className="form-group-label blink-animation"></div>
                                    <div className="form-control blink-animation"></div>
                                </div>
                                <div className="form-group">
                                    <button className="btn btn-primary blink-animation"></button>
                                </div>
                            </div>
                        </div>
                    </div> :

                    <div className="login-outer-box sign-up-wrapper">
                        <div className="sign-up-wrapper-inner">
                            <div className="login-logo">
                                <img src="https://content.iot83.com/m83/misc/flexLogoWhite.png" />
                            </div>

                            <div className="login-form-box">
                                <p className="text-yellow fw-600 text-center">
                                    Enjoy {this.state.license.trialValidityInfo.trialDays} days <span className="text-orange fw-600">FREE</span> trial, Sign up Now ! <span className="text-orange fw-600">No CREDIT CARD Needed</span>
                                </p>
                                <React.Fragment>
                                    <div className="form-group">
                                        <label className="form-group-label">Tenant Name<sup><i className="fas fa-asterisk form-group-required"></i></sup></label>
                                        <div className="input-group">
                                            <input type="text" className="form-control pd-r-40" id="tenantName" disabled={this.state.signUpInProgress} onChange={this.basicInfoChangeHandler} value={this.state.payload.tenantName} placeholder="your-tenant-name" />
                                            <span className="form-control-icon"><i className="fad fa-user-crown"></i></span>
                                            {(this.state.payload.tenantName.length > 5 && !this.state.isTenantNameChanged) ?
                                                this.state.isTenantNameUnique ?
                                                    <span className="form-control-icon-right cursor-pointer">
                                                        <i className="fas fa-check-circle text-green"></i>
                                                    </span> :
                                                    <span className="form-control-icon-right cursor-pointer">
                                                        <i className="fas fa-exclamation-triangle text-red"></i>
                                                    </span> : null
                                            }
                                            {this.state.isTenantNameValidateLoader ?
                                                <span className="form-control-icon-right">
                                                    <i className="fas fa-spinner fa-spin text-primary"></i>
                                                </span> : null
                                            }
                                            {!this.state.isTenantNameChanged &&
                                                <span className={`form-group-tooltip ${this.state.isTenantNameUnique ? "alert alert-success" : "alert alert-danger"}`}>
                                                    <p>{this.state.isTenantNameUnique ?
                                                        "Congratulations ! This tenant name is available."
                                                        : "This tenant name is already taken. Please choose something else."}
                                                    </p>
                                                </span>
                                            }
                                            <div className="input-group-append cursor-pointer" data-tooltip data-tooltip-text="Please choose tenant name carefully, you will not be allowed to change it later." data-tooltip-place="bottom">
                                                <i className="fad fa-info text-primary" data-tooltip data-tooltip-text="Please choose tenant name carefully, you will not be allowed to change it later." data-tooltip-place="bottom"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <div className="alert alert-primary note-text pt-2 pb-2">
                                            <p className="text-truncate f-11 text-dark-theme">Your instance URL will be <strong className="text-underline text-theme ml-1">https://{this.state.payload.tenantName ? this.state.payload.tenantName : "your-tenant-name"}.{window.API_URL.split('.')[1]}.com</strong></p>
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label className="form-group-label">Organization Name</label>
                                        <input type="text" className="form-control" id="companyName" disabled={this.state.signUpInProgress} onChange={this.basicInfoChangeHandler} value={this.state.payload.companyName} placeholder="Awesome Inc." />
                                        <span className="form-control-icon"><i className="fad fa-building"></i></span>
                                    </div>
                                    <div className="d-flex">
                                        <div className="flex-50 pr-2">
                                            <div className="form-group">
                                                <label className="form-group-label">First Name<sup><i className="fas fa-asterisk form-group-required"></i></sup></label>
                                                <input type="text" className="form-control" name="users" id="firstName" disabled={this.state.signUpInProgress} onChange={this.basicInfoChangeHandler} value={this.state.payload.users[0].firstName} placeholder="John" />
                                                <span className="form-control-icon"><i className="fad fa-id-card-alt"></i></span>
                                            </div>
                                        </div>
                                        <div className="flex-50 pl-2">
                                            <div className="form-group">
                                                <label className="form-group-label">Last Name<sup><i className="fas fa-asterisk form-group-required"></i></sup></label>
                                                <input type="text" className="form-control" name="users" id="lastName" disabled={this.state.signUpInProgress} onChange={this.basicInfoChangeHandler} value={this.state.payload.users[0].lastName} placeholder="Doe" />
                                                <span className="form-control-icon"><i className="fad fa-id-card-alt"></i></span>
                                            </div>
                                        </div>
                                    </div>

                                    {this.state.isMobileVerification ?
                                        <React.Fragment>
                                            <div className="d-flex">
                                                <div className="flex-50 pr-2">
                                                    <div className="form-group">
                                                        <label className="form-group-label">Email<sup><i className="fas fa-asterisk form-group-required"></i></sup></label>
                                                        <input type="email" className="form-control" name="users" id="email" onChange={this.basicInfoChangeHandler} value={this.state.payload.users[0].email} placeholder="you@example.com" />
                                                        <span className="form-control-icon"><i className="fad fa-envelope"></i></span>
                                                    </div>
                                                </div>
                                                <div className="flex-50 pl-2">
                                                    <div className="form-group">
                                                        <label className="form-group-label">Phone<sup><i className="fas fa-asterisk form-group-required"></i></sup></label>
                                                        <div className="phone-input">
                                                            <PhoneInput
                                                                className="form-control"
                                                                inputProps={{
                                                                    name: 'phone',
                                                                    required: true,
                                                                }}
                                                                placeholder="Mobile No."
                                                                country={'us'}
                                                                value={this.state.payload.users[0].mobile}
                                                                onChange={(value) => {
                                                                    let payload = cloneDeep(this.state.payload);
                                                                    payload.users[0].mobile = value;
                                                                    payload.otp = '';
                                                                    this.setState({
                                                                        payload,
                                                                        otpCount: 0
                                                                    });
                                                                }}
                                                                required
                                                            />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="d-flex">
                                                <div className="flex-70 pr-2">
                                                    <div className="form-group">
                                                        <label className="form-group-label">OTP<sup><i className="fas fa-asterisk form-group-required"></i></sup></label>
                                                        <input type="number" maxLength="5" className="form-control" id="otp"
                                                            value={this.state.payload.otp}
                                                            disabled={this.state.disabled || this.state.signUpInProgress}
                                                            required onChange={this.basicInfoChangeHandler} />
                                                        <span className="form-control-icon"><i className="fad fa-mobile-alt"></i></span>
                                                        {this.state.otpCount > 2 ?
                                                            <span className="form-group-error">You have exhausted the OTP limit for this Phone number</span>
                                                            : interval != undefined ? <span
                                                                className="form-group-error text-cyan">{`You can generate a new OTP after ${this.state.timeSeconds} sec`}</span> :
                                                                <span className="form-group-error text-cyan">Note : You can request for maximum 2 OTPs</span>
                                                        }
                                                    </div>
                                                </div>
                                                <div className="flex-30 pl-2">
                                                    <button className="btn btn-success" type="button" disabled={this.state.isLoading || interval != undefined || this.state.otpCount > 2 || this.state.payload.users[0].mobile.length < 8 || this.state.payload.tenantName.length < 6} onClick={() => {
                                                        let otpPayload = { mobile: this.state.payload.users[0].mobile, tenantName: this.state.payload.tenantName },
                                                            payload = cloneDeep(this.state.payload);
                                                        payload.otp = ""
                                                        this.setState({
                                                            isLoading: true,
                                                            btnDisabled: true,
                                                            payload
                                                        }, () => this.props.getOtp(otpPayload))
                                                    }}>{this.state.isLoading ? <i className="fas fa-spinner fa-spin" /> : this.state.otpCount === 0 ? "Generate OTP" : "Regenerate OTP"}</button>
                                                </div>
                                            </div>
                                        </React.Fragment> :
                                        <div className="form-group-sign-up">
                                            <div className="form-group">
                                                <label className="form-group-label">Email<sup><i className="fas fa-asterisk form-group-required"></i></sup></label>
                                                <input type="email" className="form-control" name="users" id="email" disabled={this.state.signUpInProgress} onChange={this.basicInfoChangeHandler} value={this.state.payload.users[0].email} placeholder="you@example.com" />
                                                <span className="form-control-icon"><i className="fad fa-envelope"></i></span>
                                            </div>
                                            <div className="form-group">
                                                <label className="form-group-label">Phone</label>
                                                <div className="phone-input">
                                                    <PhoneInput
                                                        className="form-control"
                                                        inputProps={{
                                                            name: 'phone',
                                                            required: true,
                                                        }}
                                                        placeholder="Mobile No."
                                                        disabled={this.state.signUpInProgress}
                                                        country={'us'}
                                                        value={this.state.payload.users[0].mobile}
                                                        onChange={(value) => {
                                                            let payload = cloneDeep(this.state.payload);
                                                            payload.users[0].mobile = value;
                                                            payload.otp = '';
                                                            this.setState({
                                                                payload,
                                                                otpCount: 0
                                                            });
                                                        }}
                                                        required
                                                    />
                                                </div>
                                            </div>
                                            <div className="form-group-captcha">
                                                <ReCAPTCHA
                                                    sitekey="6LfHbyUaAAAAAKt1EWxNLVvL6NCmPbNcTqc8eZmI"
                                                    onChange={this.captchaOnChangeHandler}
                                                    size="compact"
                                                />
                                            </div>
                                        </div>
                                    }

                                    <div className="form-group pt-2">
                                        <button className="btn btn-primary" disabled={this.validateNextButton() || this.state.signUpInProgress} onClick={this.signUpHandler}>Sign Up
                                            {this.state.signUpInProgress && <i className="fas fa-spinner-third fa-spin ml-3"></i>}
                                        </button>
                                    </div>

                                    <p className="text-center f-11 mb-0 text-light">By signing up, I agree to the Flex83 <span className="text-theme text-underline-hover" onClick={() => window.open(`/termsAndConditions`, '_blank')}>Terms & Conditions</span> and <span className="text-theme text-underline-hover" onClick={() => window.open(`/privacyPolicy`, '_blank')}>Privacy Policy</span></p>
                                </React.Fragment>
                            </div>
                        </div>
                    </div>
                }

                {/* account created modal */}
                {this.state.isTenantCreatedSuccessfully &&
                    <div className="modal d-block d-block animated slideInDown">
                        <div className="modal-dialog modal-dialog-centered">
                            <div className="modal-content">
                                <div className="modal-body">
                                    <div className="delete-content">
                                        <div className="delete-icon">
                                            <i className="fad fa-user-check text-green"></i>
                                        </div>
                                        <h4 className="text-green">Account Created Successfully !</h4>
                                        <p>You will shortly receive an email containing your login details.</p>
                                    </div>
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-success" onClick={this.closeTenantCreationModal}>OK</button>
                                </div>
                            </div>
                        </div>
                    </div>
                }
                {/* end account created modal */}

                {this.state.isOpen &&
                    <NotificationModal
                        type={this.state.modalType}
                        message2={this.state.message2}
                        onCloseHandler={this.onCloseHandler}
                    />
                }

            </React.Fragment>
        );
    }

    componentWillUnmount() {
        clearInterval(interval);
    }
}

SignUp.propTypes = {
    dispatch: PropTypes.func.isRequired
};

let allSelectors = {}
Object.entries(SELECTORS).map(([key, value]) => {
    allSelectors[key] = value()
})

const mapStateToProps = createStructuredSelector(allSelectors);

function mapDispatchToProps(dispatch) {
    let allActions = { dispatch }
    Object.entries(ACTIONS).map(([key, value]) => {
        allActions[key] = (...args) => dispatch(value(...args))
    })
    return allActions;
}


const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "enterpriseRegistration", reducer });
const withSaga = injectSaga({ key: "enterpriseRegistration", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(SignUp);
