/*
 *
 * ProjectList actions
 *
 */

import * as CONSTANTS from "./constants";

export function deleteProject(id, deleteRepo) {
  return {
    type: CONSTANTS.DELETE_PROJECT,
    id, deleteRepo
  };
}

export function addOrEditProject(payload, id) {
  return {
    type: CONSTANTS.ADD_EDIT_PROJECT,
    payload, id
  };
}

export function uploadImageHandler(payload, imageType) {
  return {
    type: CONSTANTS.IMAGE_UPLOAD_REQUEST,
    payload,
    imageType,
  };
}

export function getContributers() {
  return {
    type: CONSTANTS.GET_CONTRIBUTERS
  };
}

export function getFrequency() {
  return {
    type: CONSTANTS.GET_FREQUENCY
  };
}

export function getParticipation(numOfWeeks) {
  return {
    type: CONSTANTS.GET_PARTICIPATION,
    numOfWeeks
  };
}

export function getContributerList() {
  return {
    type: CONSTANTS.GET_CONTRIBUTER_LIST
  };
}
export function getContributerActivities(contributorName, numOfWeeks) {
  return {
    type: CONSTANTS.GET_CONTRIBUTER_ACTIVITIES,
    contributorName, numOfWeeks
  };
}

export function getProjectsList() {
  return {
    type: CONSTANTS.GET_ALL_PROJECTS,
  };
}

export function getProjectDetails(id) {
  return {
    type: CONSTANTS.GET_PROJECT_DETAILS,
    id
  };
}

export function getAllProducts() {
  return {
    type: CONSTANTS.GET_ALL_PRODUCTS,
  };
}