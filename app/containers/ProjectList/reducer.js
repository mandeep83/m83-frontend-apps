/*
 *
 * ProjectList reducer
 *
 */

import { fromJS } from "immutable";
import * as CONSTANTS from "./constants";

export const initialState = fromJS({});

function projectListReducer(state = initialState, action) {
  switch (action.type) {
    case CONSTANTS.DELETE_PROJECT_SUCCESS:
      return Object.assign({}, state, {
        deleteProject: { message: action.response, id: action.id }
      })
    case CONSTANTS.DELETE_PROJECT_FAILURE:
      return Object.assign({}, state, {
        deleteProjectFailure: { error: action.error, timestamp: Date.now() }
      })
    case CONSTANTS.ADD_EDIT_PROJECT_SUCCESS:
      return Object.assign({}, state, {
        addOrEditProjectSuccess: { message: action.response, id: action.id, timestamp: Date.now() }
      })
    case CONSTANTS.ADD_EDIT_PROJECT_FAILURE:
      return Object.assign({}, state, {
        addOrEditProjectFailure: { message: action.error, timestamp: Date.now() }
      })
    case CONSTANTS.IMAGE_UPLOAD_REQUEST_SUCCESS:
      return Object.assign({}, state, {
        imageUploadSuccess: action.response
      });
    case CONSTANTS.IMAGE_UPLOAD_REQUEST_FAILURE:
      return Object.assign({}, state, {
        imageUploadFailure: action.error
      });
    case CONSTANTS.GET_CONTRIBUTERS_SUCCESS:
      return Object.assign({}, state, {
        contributersSuccess: action.response
      });
    case CONSTANTS.GET_CONTRIBUTERS_FAILURE:
      return Object.assign({}, state, {
        contributersFailure: { message: action.error, timestamp: Date.now() }
      });
    case CONSTANTS.GET_FREQUENCY_SUCCESS:
      return Object.assign({}, state, {
        frequencySuccess: action.response
      });
    case CONSTANTS.GET_FREQUENCY_FAILURE:
      return Object.assign({}, state, {
        frequencyFailure: { message: action.error, timestamp: Date.now() }
      });
    case CONSTANTS.GET_PARTICIPATION_SUCCESS:
      return Object.assign({}, state, {
        participationSuccess: action.response
      });
    case CONSTANTS.GET_PARTICIPATION_FAILURE:
      return Object.assign({}, state, {
        participationFailure: { message: action.error, timestamp: Date.now() }
      });
    case CONSTANTS.GET_CONTRIBUTER_LIST_SUCCESS:
      return Object.assign({}, state, {
        contributerListSuccess: action.response
      });
    case CONSTANTS.GET_CONTRIBUTER_LIST_FAILURE:
      return Object.assign({}, state, {
        contributerListFailure: action.error
      });
    case CONSTANTS.GET_CONTRIBUTER_ACTIVITIES_SUCCESS:
      return Object.assign({}, state, {
        contributerActivitiesSuccess: action.response
      });
    case CONSTANTS.GET_CONTRIBUTER_ACTIVITIES_FAILURE:
      return Object.assign({}, state, {
        contributerActivitiesFailure: { message: action.error, timestamp: Date.now() }
      });
    case CONSTANTS.GET_PROJECT_DETAILS_SUCCESS:
      return Object.assign({}, state, {
        projectDetailsSuccess: action.response
      });
    case CONSTANTS.GET_PROJECT_DETAILS_FAILURE:
      return Object.assign({}, state, {
        projectDetailsFailure: { message: action.error, timestamp: Date.now() }
      });
    case CONSTANTS.GET_ALL_PROJECTS_SUCCESS:
      return Object.assign({}, state, {
        projectsSuccess: action.response
      });
    case CONSTANTS.GET_ALL_PROJECTS_FAILURE:
      return Object.assign({}, state, {
        projectsFailure: { message: action.error, timestamp: Date.now() }
      });
    case CONSTANTS.GET_ALL_PRODUCTS_SUCCESS:
      return Object.assign({}, state, {
        productsSuccess: action.response
      });
    case CONSTANTS.GET_ALL_PRODUCTS_FAILURE:
      return Object.assign({}, state, {
        productsFailure: { message: action.error, timestamp: Date.now() }
      });
    default:
      return state;
  }
}

export default projectListReducer;
