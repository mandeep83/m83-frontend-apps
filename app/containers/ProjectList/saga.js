
import { apiCallHandler } from '../../api';
import { takeEvery } from 'redux-saga';
import * as CONSTANTS from './constants';

export function* deleteProjectHandler(action) {
  yield [apiCallHandler(action, CONSTANTS.DELETE_PROJECT_SUCCESS, CONSTANTS.DELETE_PROJECT_FAILURE, 'deleteProject')]
}

export function* watcherDeleteProject() {
  yield takeEvery(CONSTANTS.DELETE_PROJECT, deleteProjectHandler)
}

export function* addOrEditProjectHandler(action) {
  yield [apiCallHandler(action, CONSTANTS.ADD_EDIT_PROJECT_SUCCESS, CONSTANTS.ADD_EDIT_PROJECT_FAILURE, 'addOrEditProject', false)]
}

export function* watcherAddOrEditProject() {
  yield takeEvery(CONSTANTS.ADD_EDIT_PROJECT, addOrEditProjectHandler)
}

export function* imageUploadRequestHandler(action) {
  yield [apiCallHandler(action, CONSTANTS.IMAGE_UPLOAD_REQUEST_SUCCESS, CONSTANTS.IMAGE_UPLOAD_REQUEST_FAILURE, 'fileUploadRequest', false)]
}

export function* watcherImageUploadRequest() {
  yield takeEvery(CONSTANTS.IMAGE_UPLOAD_REQUEST, imageUploadRequestHandler)
}

export function* getContributersHandler(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_CONTRIBUTERS_SUCCESS, CONSTANTS.GET_CONTRIBUTERS_FAILURE, 'getContributers', false)]
}

export function* watcherGetContributers() {
  yield takeEvery(CONSTANTS.GET_CONTRIBUTERS, getContributersHandler)
}

export function* getCodeFrequencyHandler(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_FREQUENCY_SUCCESS, CONSTANTS.GET_FREQUENCY_FAILURE, 'getCodeFrequency', false)]
}

export function* watcherGetCodeFrequency() {
  yield takeEvery(CONSTANTS.GET_FREQUENCY, getCodeFrequencyHandler)
}

export function* getParticipationHandler(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_PARTICIPATION_SUCCESS, CONSTANTS.GET_PARTICIPATION_FAILURE, 'getParticipation', false)]
}

export function* watcherGetParticipation() {
  yield takeEvery(CONSTANTS.GET_PARTICIPATION, getParticipationHandler)
}

export function* getContributerListHandler(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_CONTRIBUTER_LIST_SUCCESS, CONSTANTS.GET_CONTRIBUTER_LIST_FAILURE, 'contributerList', false)]
}

export function* watcherGetContributerList() {
  yield takeEvery(CONSTANTS.GET_CONTRIBUTER_LIST, getContributerListHandler)
}

export function* getContributerActivitiesHandler(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_CONTRIBUTER_ACTIVITIES_SUCCESS, CONSTANTS.GET_CONTRIBUTER_ACTIVITIES_FAILURE, 'contributerActivities', false)]
}

export function* watcherGetContributerActivities() {
  yield takeEvery(CONSTANTS.GET_CONTRIBUTER_ACTIVITIES, getContributerActivitiesHandler)
}

export function* getAllProjectsHandler(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_ALL_PROJECTS_SUCCESS, CONSTANTS.GET_ALL_PROJECTS_FAILURE, 'fetchProjects', false)]
}

export function* getProjectDetailsHandler(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_PROJECT_DETAILS_SUCCESS, CONSTANTS.GET_PROJECT_DETAILS_FAILURE, 'getInvitationStatus', false)]
}

export function* watcherGetAllProjects() {
  yield takeEvery(CONSTANTS.GET_ALL_PROJECTS, getAllProjectsHandler)
}

export function* watcherGetProjectDetails() {
  yield takeEvery(CONSTANTS.GET_PROJECT_DETAILS, getProjectDetailsHandler)
}

export function* getAllProductsHandler(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_ALL_PRODUCTS_SUCCESS, CONSTANTS.GET_ALL_PRODUCTS_FAILURE, 'getAllProducts', false)]
}

export function* watcherGetAllProducts() {
  yield takeEvery(CONSTANTS.GET_ALL_PRODUCTS, getAllProductsHandler)
}

export default function* rootSaga() {
  yield [
    watcherDeleteProject(),
    watcherAddOrEditProject(),
    watcherImageUploadRequest(),
    watcherGetContributers(),
    watcherGetCodeFrequency(),
    watcherGetParticipation(),
    watcherGetContributerList(),
    watcherGetContributerActivities(),
    watcherGetAllProjects(),
    watcherGetProjectDetails(),
    watcherGetAllProducts(),
  ]
}