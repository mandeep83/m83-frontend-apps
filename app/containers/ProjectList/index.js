/**
 *
 * ProjectList
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import NotificationModal from '../../components/NotificationModal/Loadable'
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import reducer from "./reducer";
import saga from "./saga";
import { fetchSideNav } from "../HomePage/actions.js"
import * as ACTIONS from './actions';
import * as SELECTORS from './selectors'
import DeleteModel from "../../components/DeleteModel";
import Loader from "../../components/Loader";
import SlidingPane from "react-sliding-pane";
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import cloneDeep from 'lodash/cloneDeep';
import { convertTimestampToDate } from "../../commonUtils";
import ListingTable from "../../components/ListingTable/Loadable";

let chartLoaded = { "contributers": undefined, "frequency": undefined, "participation": undefined, "activities": undefined },
    chart = undefined;

export class ProjectList extends React.Component {

    state = {
        projectList: [],
        payload: {
            description: '',
            image: '',
            name: '',
            packages: []
        },
        searchProject: '',
        edit: false,
        isFetching: true,
        isUploadingImage: false,
        addOrEditModal: false,
        repoName: "",
        weeksOfParticipation: 4,
        weeksOfContribution: 52,
        chartLoaded: chartLoaded,
        selectedTab: 0,
        carouselItemsArr: [],
        activeTabDiv: 0,
        allPackages: [
            {
                name: 'TRIAL',
                description: '',
                currency: 'USD',
                price: 0,
                subscriptionPrice: 0,
                numberOfDays: 31,
                added: false,
                assignedProducts: [
                    {
                        name: 'device_types',
                        displayName: 'Device Type(s)',
                        count: 1
                    },
                    {
                        name: 'no_of_devices',
                        displayName: 'Device(s)',
                        count: 5
                    },
                    {
                        name: 'device_groups',
                        displayName: 'Device Group(s)',
                        count: 2
                    },
                    {
                        name: 'attributeKeys',
                        displayName: 'Device Attribute(s)',
                        count: 10
                    },
                    {
                        name: 'events',
                        displayName: 'Event(s)',
                        count: 2
                    },
                    {
                        name: 'rules',
                        displayName: 'Rule(s)',
                        count: 3
                    },
                    {
                        name: 'sms',
                        displayName: 'SMS',
                        count: 30
                    },
                    {
                        name: 'email',
                        displayName: 'Email(s)',
                        count: 30
                    },
                    {
                        name: 'rpc',
                        displayName: 'RPC',
                        count: 20
                    },
                    {
                        name: 'webhooks',
                        displayName: 'Webhook(s)',
                        count: 20
                    },
                    {
                        name: 'retention',
                        displayName: 'Data Retention',
                        count: 90
                    }
                ],
                productList: []
            }, {
                name: 'BRONZE',
                description: '',
                currency: 'USD',
                price: 0,
                subscriptionPrice: 5,
                numberOfDays: 30,
                added: false,
                assignedProducts: [
                    {
                        name: 'device_types',
                        displayName: 'Device Type(s)',
                        count: 1
                    },
                    {
                        name: 'no_of_devices',
                        displayName: 'Device(s)',
                        count: 5
                    },
                    {
                        name: 'device_groups',
                        displayName: 'Device Group(s)',
                        count: 2
                    },
                    {
                        name: 'attributeKeys',
                        displayName: 'Device Attribute(s)',
                        count: 10
                    },
                    {
                        name: 'events',
                        displayName: 'Event(s)',
                        count: 2
                    },
                    {
                        name: 'rules',
                        displayName: 'Rule(s)',
                        count: 3
                    },
                    {
                        name: 'sms',
                        displayName: 'SMS',
                        count: 30
                    },
                    {
                        name: 'email',
                        displayName: 'Email(s)',
                        count: 30
                    },
                    {
                        name: 'rpc',
                        displayName: 'RPC',
                        count: 20
                    },
                    {
                        name: 'webhooks',
                        displayName: 'Webhook(s)',
                        count: 20
                    },
                    {
                        name: 'retention',
                        displayName: 'Data Retention',
                        count: 90
                    }
                ],
                productList: []
            }, {
                name: 'SILVER',
                description: '',
                currency: 'USD',
                price: 0,
                subscriptionPrice: 20,
                numberOfDays: 30,
                added: false,
                assignedProducts: [
                    {
                        name: 'device_types',
                        displayName: 'Device Type(s)',
                        count: 2
                    },
                    {
                        name: 'no_of_devices',
                        displayName: 'Device(s)',
                        count: 20
                    },
                    {
                        name: 'device_groups',
                        displayName: 'Device Group(s)',
                        count: 2
                    },
                    {
                        name: 'attributeKeys',
                        displayName: 'Device Attribute(s)',
                        count: 12
                    },
                    {
                        name: 'events',
                        displayName: 'Event(s)',
                        count: 4
                    },
                    {
                        name: 'rules',
                        displayName: 'Rule(s)',
                        count: 6
                    },
                    {
                        name: 'sms',
                        displayName: 'SMS',
                        count: 60
                    },
                    {
                        name: 'email',
                        displayName: 'Email(s)',
                        count: 60
                    },
                    {
                        name: 'rpc',
                        displayName: 'RPC',
                        count: 40
                    },
                    {
                        name: 'webhooks',
                        displayName: 'Webhook(s)',
                        count: 40
                    },
                    {
                        name: 'retention',
                        displayName: 'Data Retention',
                        count: 180
                    }
                ],
                productList: []
            }, {
                name: 'GOLD',
                description: '',
                currency: 'USD',
                price: 0,
                subscriptionPrice: 50,
                numberOfDays: 30,
                added: false,
                assignedProducts: [
                    {
                        name: 'device_types',
                        displayName: 'Device Type(s)',
                        count: 3
                    },
                    {
                        name: 'no_of_devices',
                        displayName: 'Device(s)',
                        count: 50
                    },
                    {
                        name: 'device_groups',
                        displayName: 'Device Group(s)',
                        count: 2
                    },
                    {
                        name: 'attributeKeys',
                        displayName: 'Device Attribute(s)',
                        count: 14
                    },
                    {
                        name: 'events',
                        displayName: 'Event(s)',
                        count: 5
                    },
                    {
                        name: 'rules',
                        displayName: 'Rule(s)',
                        count: 6
                    },
                    {
                        name: 'sms',
                        displayName: 'SMS',
                        count: 90
                    },
                    {
                        name: 'email',
                        displayName: 'Email(s)',
                        count: 90
                    },
                    {
                        name: 'rpc',
                        displayName: 'RPC',
                        count: 60
                    },
                    {
                        name: 'webhooks',
                        displayName: 'Webhook(s)',
                        count: 60
                    },
                    {
                        name: 'retention',
                        displayName: 'Data Retention',
                        count: 270
                    }
                ],
                productList: []
            }, {
                name: 'PLATINUM',
                description: '',
                currency: 'USD',
                price: 0,
                subscriptionPrice: 100,
                numberOfDays: 30,
                added: false,
                assignedProducts: [
                    {
                        name: 'device_types',
                        displayName: 'Device Type(s)',
                        count: 5
                    },
                    {
                        name: 'no_of_devices',
                        displayName: 'Device(s)',
                        count: 100
                    },
                    {
                        name: 'device_groups',
                        displayName: 'Device Group(s)',
                        count: 3
                    },
                    {
                        name: 'attributeKeys',
                        displayName: 'Device Attribute(s)',
                        count: 16
                    },
                    {
                        name: 'events',
                        displayName: 'Event(s)',
                        count: 10
                    },
                    {
                        name: 'rules',
                        displayName: 'Rule(s)',
                        count: 10
                    },
                    {
                        name: 'sms',
                        displayName: 'SMS',
                        count: 120
                    },
                    {
                        name: 'email',
                        displayName: 'Email(s)',
                        count: 120
                    },
                    {
                        name: 'rpc',
                        displayName: 'RPC',
                        count: 120
                    },
                    {
                        name: 'webhooks',
                        displayName: 'Webhook(s)',
                        count: 120
                    },
                    {
                        name: 'retention',
                        displayName: 'Data Retention',
                        count: 360
                    }
                ],
                productList: []
            }
        ],
        allProducts: [],
    }

    componentWillMount() {
        this.props.getProjectsList();
        this.props.getAllProducts();
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.projectsList && nextProps.projectsList !== this.props.projectsList) {
            let filteredProjectList = nextProps.projectsList;
            localStorage['myProjects'] = JSON.stringify(nextProps.projectsList);
            if (this.state.searchProject) {
                filteredProjectList = nextProps.projectsList.filter(item => item.name.toLowerCase().includes(this.state.searchProject.toLowerCase()))
            }

            this.setState({
                projectList: nextProps.projectsList,
                filteredProjectList,
                isFetching: false,
            })
        }

        if (nextProps.projectsListFailure && nextProps.projectsListFailure !== this.props.projectsListFailure) {
            this.setState({
                isOpen: true,
                isFetching: false,
                message1: "Oops !",
                message2: nextProps.projectsListFailure,
                type: "error"
            })
        }

        if (nextProps.deleteProjectSuccess && nextProps.deleteProjectSuccess !== this.props.deleteProjectSuccess) {
            let projectList = this.state.projectList.filter(project => project.id !== nextProps.deleteProjectSuccess.id);

            let filteredProjectList = this.state.filteredProjectList.filter(project => project.id !== nextProps.deleteProjectSuccess.id);

            if (nextProps.deleteProjectSuccess.id === localStorage['selectedProjectId']) {
                if (filteredProjectList.length > 0) {
                    localStorage['selectedProjectId'] = filteredProjectList[0].id;
                } else {
                    delete localStorage['selectedProjectId'];
                }
            }
            localStorage['myProjects'] = JSON.stringify(projectList);

            this.setState({
                filteredProjectList,
                isFetching: false,
                projectList,
                isOpen: true,
                type: "success",
                message1: "Hurray !",
                message2: nextProps.deleteProjectSuccess.message
            })
        }

        if (nextProps.deleteProjectFailure && nextProps.deleteProjectFailure !== this.props.deleteProjectFailure) {
            this.setState({
                isOpen: true,
                isFetching: false,
                message1: "Oops !",
                message2: nextProps.deleteProjectFailure.error,
                type: "error"
            })
        }

        if (nextProps.addOrEditProjectSuccess && nextProps.addOrEditProjectSuccess !== this.props.addOrEditProjectSuccess) {
            this.setState({
                modalLoader: false,
                addOrEditModal: false,
                isOpen: true,
                type: "success",
                message2: nextProps.addOrEditProjectSuccess.message,
                repoName: "",
                isFetching: true,
                selectedProjectId: null
            }, () => {
                this.props.getProjectsList()
            })
        }

        if (nextProps.addOrEditProjectFailure && nextProps.addOrEditProjectFailure !== this.props.addOrEditProjectFailure) {
            this.setState({
                modalLoader: false,
                isOpen: true,
                isFetching: false,
                message2: nextProps.addOrEditProjectFailure.message,
                type: "error"
            })
        }

        if (nextProps.imageUploadSuccess && nextProps.imageUploadSuccess !== this.props.imageUploadSuccess) {
            let payload = JSON.parse(JSON.stringify(this.state.payload));
            let isUploadingImage = this.state.isUploadingImage;
            payload.image = nextProps.imageUploadSuccess.secure_url;
            isUploadingImage = false;
            this.setState({
                payload,
                isUploadingImage,
            });
        }

        if (nextProps.imageUploadFailure && nextProps.imageUploadFailure !== this.props.imageUploadFailure) {
            this.setState({
                type: "error",
                isOpen: true,
                message1: "Oops !",
                message2: nextProps.imageUploadFailure
            });
        }

        if (nextProps.contributersSuccess && nextProps.contributersSuccess !== this.props.contributersSuccess) {
            let chartLoaded = JSON.parse(JSON.stringify(this.state.chartLoaded))
            chartLoaded.contributers = true;
            this.setState({
                contributers: nextProps.contributersSuccess,
                chartLoaded
            }, () => this.getInsightChartsConfig(this.state.contributers, "chartForContributers"))
        }

        if (nextProps.contributersFailure && nextProps.contributersFailure !== this.props.contributersFailure) {
            this.setState({
                contributters: [],
                isOpen: true,
                message1: "Oops !",
                message2: nextProps.contributersFailure.message,
                type: "error",
            })
        }

        if (nextProps.frequencySuccess && nextProps.frequencySuccess !== this.props.frequencySuccess) {
            let chartLoaded = JSON.parse(JSON.stringify(this.state.chartLoaded))
            chartLoaded.frequency = true;
            this.setState({
                frequencies: nextProps.frequencySuccess,
                chartLoaded
            }, () => this.getInsightChartsConfig(this.state.frequencies, "chartForFrequency"))
        }

        if (nextProps.frequencyFailure && nextProps.frequencyFailure !== this.props.frequencyFailure) {
            this.setState({
                frequencies: [],
                isOpen: true,
                message1: "Oops !",
                message2: nextProps.frequencyFailure.message,
                type: "error",
            })
        }

        if (nextProps.participationSuccess && nextProps.participationSuccess !== this.props.participationSuccess) {
            let chartLoaded = JSON.parse(JSON.stringify(this.state.chartLoaded))
            chartLoaded.participation = true;
            this.setState({
                participation: nextProps.participationSuccess,
                chartLoaded
            }, () => this.getInsightChartsConfig(this.state.participation, "chartForParticipation"))
        }

        if (nextProps.participationFailure && nextProps.participationFailure !== this.props.participationFailure) {
            this.setState({
                participation: [],
                isOpen: true,
                message1: "Oops !",
                message2: nextProps.participationFailure.message,
                type: "error",
            })
        }

        if (nextProps.contributerListSuccess && nextProps.contributerListSuccess !== this.props.contributerListSuccess) {
            this.setState({
                contributerList: nextProps.contributerListSuccess,
            }, () => {
                if (nextProps.contributerListSuccess.length > 0) {
                    this.setState({
                        contributerName: nextProps.contributerListSuccess[0].name,
                        contributerUrl: nextProps.contributerListSuccess[0].url,
                    })
                    this.props.getContributerActivities(nextProps.contributerListSuccess[0].name, this.state.weeksOfContribution)
                }
                else
                    this.setState({ contributerActivities: [] })
            })
        }

        if (nextProps.contributerListFailure && nextProps.contributerListFailure !== this.props.contributerListFailure) {
            this.setState({
                contributerActivities: [],
                contributerList: [],
                isOpen: true,
                message1: "Oops !",
                message2: nextProps.contributerListFailure,
                type: "error",
            })
        }

        if (nextProps.contributerActivitiesSuccess && nextProps.contributerActivitiesSuccess !== this.props.contributerActivitiesSuccess) {
            let chartLoaded = JSON.parse(JSON.stringify(this.state.chartLoaded))
            chartLoaded.activities = true;
            this.setState({
                contributerActivities: nextProps.contributerActivitiesSuccess.data.length > 0 ? nextProps.contributerActivitiesSuccess.data[0].activities : [],
                chartLoaded,
            }, () => this.getInsightChartsConfig(this.state.contributerActivities, "chartForActivities"))
        }

        if (nextProps.contributerActivitiesFailure && nextProps.contributerActivitiesFailure !== this.props.contributerActivitiesFailure) {
            this.setState({
                contributerActivities: [],
                isOpen: true,
                message1: "Oops !",
                message2: nextProps.contributerActivitiesFailure.message,
                type: "error",
            })
        }

        if (nextProps.projectDetailsSuccess && nextProps.projectDetailsSuccess !== this.props.projectDetailsSuccess) {
            let payload = cloneDeep(nextProps.projectDetailsSuccess),
                carouselItemsArr = [],
                allPackages = cloneDeep(this.state.allPackages);

            if (localStorage.tenantType === "MAKER") {
                carouselItemsArr = this.getCarouselArr(payload.packages.length, 3);
                payload.packages.map(_package => {
                    allPackages.filter(pack => pack.name === _package.name)[0].added = true
                })
            }

            this.setState({
                modalLoader: false,
                payload,
                selectedTab: 0,
                carouselItemsArr,
                allPackages,
            })

        }

        if (nextProps.projectDetailsFailure && nextProps.projectDetailsFailure !== this.props.projectDetailsFailure) {
            this.setState({
                contributerActivities: [],
                isOpen: true,
                message1: "Oops !",
                message2: nextProps.projectDetailsFailure.message,
                type: "error",
            })
        }
        if (nextProps.allProducts && nextProps.allProducts !== this.props.allProducts) {
            let allProducts = nextProps.allProducts,
                allPackages = cloneDeep(this.state.allPackages);
            allPackages.map(selectedPackage => {
                selectedPackage.assignedProducts.map(product => {
                    let masterProduct = allProducts.filter(productObj => productObj.name === product.name)[0];
                    let obj = {
                        id: masterProduct.id,
                        count: product.count,
                        periodic: masterProduct.periodic,
                        visible: true,
                        displayName: product.displayName,
                        name: product.name,
                    };
                    selectedPackage.productList.push(obj);
                });

                allProducts.map(product => {
                    if (selectedPackage.assignedProducts.filter(productObj => productObj.name === product.name).length === 0) {
                        let obj = {
                            id: product.id,
                            count: product.name === 'simulation' ? selectedPackage.assignedProducts.filter(productObj => productObj.name === 'device_types')[0].count : 0,
                            periodic: product.periodic,
                            visible: false,
                            displayName: product.displayName,
                            name: product.name,
                        }
                        selectedPackage.productList.push(obj);
                    }

                });
            })



            this.setState({
                allProducts,
                allPackages,
            })

        }

        if (nextProps.productsFailure && nextProps.productsFailure !== this.props.productsFailure) {
            this.setState({
                isOpen: true,
                message1: "Oops !",
                message2: nextProps.productsFailure.message,
                type: "error",
            })
        }

    }

    closeHandler = () => {
        this.setState({
            isOpen: false,
            type: '',
            message1: '',
            message2: ''
        })
    }

    cancelClicked = () => {
        this.setState({
            projectToBeDeleted: null,
            confirmState: false,
            isSourceControlEnabled: null,
        })
    }

    saveProject = (event) => {
        event.preventDefault();
        let payload = JSON.parse(JSON.stringify(this.state.payload));

        if (!payload.repoName && this.state.repoName) {
            payload.repoName = this.state.repoName;
        }

        if (localStorage.tenantType === 'MAKER') {
            payload.packages.map(_package => {
                delete _package.added;
                if (!_package.id) {
                    _package.operation = 'ADD';
                }
            })
        } else {
            delete payload.packages;
        }
        this.setState({
            modalLoader: true
        }, () => this.props.addOrEditProject(payload, this.state.selectedProjectId))


    }

    onChangeHandler = (event) => {
        let errorCode = this.state.errorCode;
        let payload = JSON.parse(JSON.stringify(this.state.payload));
        let weeksOfParticipation = this.state.weeksOfParticipation;
        let chartLoaded = JSON.parse(JSON.stringify(this.state.chartLoaded));
        if (event.target.id === "repoName") {
            let repoName = event.target.value.trim();
            this.setState({ repoName })
            return;
        }
        else if (event.target.id === "weeks") {
            weeksOfParticipation = event.target.value;
            chartLoaded.participation = undefined
            // this.props.getParticipation(event.target.value)
        }
        else if (event.target.id === "contributerName") {
            let contributerList = JSON.parse(JSON.stringify(this.state.contributerList))
            contributerList.find(val => {
                if (val.name === event.target.value)
                    this.setState({ contributerName: val.name, contributerUrl: val.url })
            })
            chartLoaded.activities = undefined
            this.props.getContributerActivities(event.target.value, this.state.weeksOfContribution)
        }

        else if (event.target.id === "name") {
            if ((event.target.value).length <= 25) {
                payload[event.target.id] = event.target.value;
            }
            let regex = /^[\w\-\s]+$/
            errorCode = event.target.value.match(regex) ? errorCode === 1 ? 0 : errorCode : 1
        }
        else
            payload[event.target.id] = event.target.value;

        this.setState({ payload, errorCode, weeksOfParticipation, chartLoaded })
    }

    filterProjectList = (e) => {
        let filteredProjectList = JSON.parse(JSON.stringify(this.state.projectList));
        if (e.target.value.length > 0) {
            filteredProjectList = this.state.projectList.filter(item => item.name.toLowerCase().includes(e.target.value.toLowerCase()))
        }
        this.setState({
            searchProject: e.target.value,
            filteredProjectList
        })
    }

    emptySearchBox = () => {
        let filteredProjectList = JSON.parse(JSON.stringify(this.state.projectList))
        this.setState({
            searchProject: "",
            filteredProjectList
        })
    }

    fileUploadHandler = event => {
        event.persist();
        this.setState({
            isUploadingImage: true
        }, () => this.props.uploadImageHandler(event.target.files[0], event.target.name));

    };

    closeModalHandler = () => {
        this.setState({ selectedProjectId: null, addOrEditModal: false, repoName: "" })
    }

    handleProjectClick = (id) => {
        localStorage['selectedProjectId'] = id;
        this.props.fetchSideNav();
    }

    isDeleteAllowed = (project, isOwner) => {
        return project.repoName ? localStorage.isGitHubLogin == "true" && isOwner && localStorage.tenantType !== "MAKER" : isOwner && localStorage.tenantType !== "MAKER";
    }

    isEditAllowed = (project, isOwner) => {
        return project.repoName ? localStorage.isGitHubLogin == "true" && isOwner : isOwner;
    }

    getInsightChartsConfig(chartData, chartId) {

        if (chartData && chartData.length > 0) {
            am4core.addLicense("CH145187641");
            chart = am4core.create(chartId, am4charts.XYChart);
            chart.data = chartData;

            var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
            dateAxis.renderer.grid.template.disabled = true;
            dateAxis.renderer.grid.template.location = 0;
            dateAxis.renderer.minGridDistance = 15;
            dateAxis.paddingLeft = am4core.percent(2);

            let label = dateAxis.renderer.labels.template
            label.rotation = (chartId === "chartForParticipation" || chartId === "chartForActivities") && 90;

            function createSeries(field, name, color, value, opposite) {
                var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                var series = chart.series.push(new am4charts.LineSeries());
                series.dataFields.valueY = field;
                series.dataFields.dateX = value;
                series.name = name;
                series.tooltipText = "{name} : [b]{valueY}[/]";
                series.stroke = color;
                series.strokeWidth = 0;
                series.fillOpacity = 0.6;
                series.fill = color;
                series.tensionX = 0.77;
                series.yAxis = valueAxis;
                series.stacked = true;

                valueAxis.renderer.line.stroke = color;
                valueAxis.renderer.line.strokeOpacity = 1;
                valueAxis.renderer.line.strokeWidth = 1.2;
                valueAxis.renderer.opposite = opposite;
                valueAxis.renderer.baseGrid.disabled = true;
            }
            switch (chartId) {

                case "chartForContributers":
                    createSeries("NumberOfDeletions", "Deletions", "#FA550D", "StartOfTheWeek", false);
                    createSeries("NumberOfCommits", "Commits", "#0E09F5", "StartOfTheWeek", true);
                    createSeries("NumberOfAdditions", "Additions", "#15F509", "StartOfTheWeek", false);
                    break;
                case "chartForFrequency":
                    createSeries("deletions", "Deletions", "#FA550D", "timestamp", false);
                    createSeries("additions", "Additions", "#15F509", "timestamp", true);
                    break;
                case "chartForParticipation":
                    createSeries("owner", "Owner", "#FB44EC", "week", false);
                    createSeries("all", "All", "#2A64F7", "week", true);
                    break;
                case "chartForActivities":
                    createSeries("NumberOfDeletions", "Deletions", "#FA550D", "StartOfTheWeek", false);
                    createSeries("NumberOfCommits", "Commits", "#0E09F5", "StartOfTheWeek", true);
                    createSeries("NumberOfAdditions", "Additions", "#15F509", "StartOfTheWeek", false);
                    break;
            }
            chart.legend = new am4charts.Legend();
            let marker = chart.legend.markers.template.children.getIndex(0);
            chart.legend.useDefaultMarker = true;
            marker.width = 15;
            marker.height = 15;
            marker.cornerRadius(12, 12, 12, 12);
            marker.strokeWidth = 1;
            marker.strokeOpacity = 0.6
            marker.stroke = am4core.color("#ccc");

            chart.cursor = new am4charts.XYCursor();

            if (chartId === "chartForActivities") {
                chart.scrollbarX = new am4core.Scrollbar();
            }
        }
    }

    getChartBoxHtml = (id) => {
        switch (id) {
            case "contributers":
                return (
                    <div className="flex">
                        <div className="flex-item fx-b100">
                            <div className="card panel">
                                <div className="card-body panelBody">
                                    <div className="chartBox" id="chartForContributers">
                                        {this.state.chartLoaded.contributers ?
                                            this.state.contributers.length === 0 &&
                                            <div className="panelMessage">
                                                <p>
                                                    <i className="far fa-file-exclamation"></i>No Data Found !
                                            </p>
                                            </div>
                                            :
                                            <div className="panelLoader">
                                                <i className="fal fa-sync-alt fa-spin"></i>
                                            </div>
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="flex-item fx-b100">
                            <div className="card panel">
                                <div className="card-header panelHeader">
                                    <div className="initialsBox"><span className="initialsImage">{this.state.contributerName && this.state.contributerName.charAt(0).toUpperCase()}</span>
                                        <a href="" className="text-capitalize">{this.state.contributerName && this.state.contributerName}</a>
                                        <h2>{this.state.contributerUrl && this.state.contributerUrl}</h2>
                                    </div>
                                    <select id="contributerName" name="contributerName" value={this.state.contributerName} onChange={() => this.onChangeHandler(event)}>
                                        {this.state.contributerList && this.state.contributerList.length > 0 ?
                                            this.state.contributerList.map((contributer, index) => < option value={contributer.name} key={index}>{contributer.name}</option>)
                                            :
                                            < option>--No Data--</option>
                                        }
                                    </select>
                                    {/* <p className="alert alert-info"></p> */}
                                    {/* <h6>Total commit <span className="badge badge-info">4</span></h6> */}
                                </div>
                                <div className="card-body panelBody">
                                    <div className="chartBox" id="chartForActivities">
                                        {this.state.chartLoaded.activities ?
                                            this.state.contributerActivities.length > 0 &&
                                            <div className="panelMessage">
                                                <p>
                                                    <i className="far fa-exclamation-triangle"></i>No Data Found !
                                                </p>
                                            </div>
                                            :
                                            <div className="panelLoader">
                                                <i className="fal fa-sync-alt fa-spin"></i>
                                            </div>
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div >
                    /* <ul className="contributerList"> */
                    /*<li>
                   <div className="card panel">
                   <div className="card-header panelHeader">
                       <h5>Nisha Tanwar</h5>
                       <p className="alert alert-info"></p>
                       <h6>Total commit <span className="badge badge-info">4</span></h6>
                   </div>
                   <div className="card-body panelBody">
                       <div className="chartBox">
                           <img src='https://upload.wikimedia.org/wikipedia/commons/8/84/Line_graph.png' />
                       </div>
                   </div>
               </div>
           </li>
     
           <li>
               <div className="card panel">
                   <div className="card-header panelHeader">
                       <h5>UserName</h5>
                       <p className="alert alert-info">2</p>
                       <h6>Total commit <span className="badge badge-info">50</span></h6>
                   </div>
                   <div className="card-body panelBody">
                       <div className="chartBox">
                           <img src='https://upload.wikimedia.org/wikipedia/commons/8/84/Line_graph.png' />
                       </div>
                   </div>
               </div>
           </li>*/
                    /* </ul>  */
                );
            case "frequency":
                return (
                    <div className="card panel">
                        <div className="card-body panelBody">
                            <div className="chartBox" id="chartForFrequency">
                                {this.state.chartLoaded.frequency ?
                                    this.state.frequencies.length === 0 &&
                                    <div className="panelMessage">
                                        <p>
                                            <i className="far fa-exclamation-triangle"></i>No Data Found !</p>
                                    </div>
                                    :
                                    <div className="panelLoader">
                                        <i className="fal fa-sync-alt fa-spin"></i>
                                    </div>
                                }
                            </div>
                        </div>
                    </div>
                    /* <div className="card panel">
                        <div className="card-header panelHeader">
                            <h5>Contribution to master</h5>
                        </div>
                        <div className="card-body panelBody">
                            <div className="chartBox">
                                <img src='https://upload.wikimedia.org/wikipedia/commons/8/84/Line_graph.png' />
                            </div>
                        </div>
                    </div>
                    <div className="card panel">
                        <div className="card-header panelHeader">
                            <h5>Addition / Deletion graph</h5>
                            <select id="days" name="days">
                                <option value="day">Day</option>
                                <option value="week">Week</option>
                            </select>
                        </div>
                        <div className="card-body panelBody">
                            <div className="chartBox">
                                <img src='https://upload.wikimedia.org/wikipedia/commons/8/84/Line_graph.png' />
                            </div>
                        </div>
                    </div> */
                )
            case "participation":
                return (
                    <div className="card panel">
                        <div className="card-header panelHeader">
                            <select id="weeks" name="days" value={this.state.weeksOfParticipation} onChange={() => this.onChangeHandler(event)}>
                                <option value="4">Last Month</option>
                                <option value="12">Last 3 months</option>
                                <option value="24">Last 6 months</option>
                                <option value="48">Last 12 months</option>
                            </select>
                        </div>
                        <div className="card-body panelBody">
                            <div className="chartBox" id="chartForParticipation">
                                {this.state.chartLoaded.participation ?
                                    this.state.participation.length > 0 &&
                                    <div className="panelMessage">
                                        <p>
                                            <i className="far fa-exclamation-triangle"></i>No Data Found !
                                            </p>
                                    </div>
                                    :
                                    <div className="panelLoader">
                                        <i className="fal fa-sync-alt fa-spin"></i>
                                    </div>
                                }
                            </div>
                        </div>
                    </div>
                )
            case "commit":
                return (
                    <React.Fragment>
                        <div className="flex">
                            <div className="flex-item fx-b50 pd-r-10">
                                <div className="card panel">
                                    <div className="card-header panelHeader">
                                        <h5>Commits per week</h5>
                                    </div>
                                    <div className="card-body panelBody">
                                        <div className="chartBox">
                                            <img src='https://www.mathworks.com/help/examples/graphics/win64/OverlayBarGraphsExample_01.png' />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="flex-item fx-b50 pd-l-10">
                                <div className="card panel">
                                    <div className="card-header panelHeader">
                                        <h5>Commits per day</h5>
                                    </div>
                                    <div className="card-body panelBody">
                                        <div className="chartBox">
                                            <img src='https://upload.wikimedia.org/wikipedia/commons/8/84/Line_graph.png' />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="card panel">
                            <div className="card-header panelHeader">
                                <h5>Commits by contributers</h5>
                                <select id="days" name="days">
                                    <option value="day">Day</option>
                                    <option value="week">Week</option>
                                </select>
                            </div>
                            <div className="card-body panelBody">
                                <div className="chartBox barChart">
                                    <img src='https://www.mathworks.com/help/examples/graphics/win64/OverlayBarGraphsExample_01.png' />
                                </div>
                            </div>
                        </div>
                    </React.Fragment>
                )
            case "traffic":
                return (
                    <div className="flex">
                        <div className="flex-item fx-b50 pd-r-10">
                            <div className="card panel">
                                <div className="card-header panelHeader">
                                    <h5>Referring Sites</h5>
                                </div>
                                <div className="card-body panelBody">
                                    <div className="tableBox">
                                        <table className="table table-bordered m-0 agentTable">
                                            <thead>
                                                <tr>
                                                    <th width="50%">Site</th>
                                                    <th width="50%">Views</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Site 1</td>
                                                    <td>30</td>
                                                </tr>
                                                <tr>
                                                    <td>Site 2</td>
                                                    <td>30</td>
                                                </tr>
                                                <tr>
                                                    <td>Site 3</td>
                                                    <td>30</td>
                                                </tr>
                                                <tr>
                                                    <td>Site 4</td>
                                                    <td>30</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="flex-item fx-b50 pd-l-10">
                            <div className="card panel">
                                <div className="card-header panelHeader">
                                    <h5>Popular content (Repos)</h5>
                                </div>
                                <div className="card-body panelBody">
                                    <div className="tableBox">
                                        <table className="table table-bordered m-0 agentTable">
                                            <thead>
                                                <tr>
                                                    <th width="50%">Content</th>
                                                    <th width="50%">Views</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Repo 1</td>
                                                    <td>30</td>
                                                </tr>
                                                <tr>
                                                    <td>Repo 2</td>
                                                    <td>30</td>
                                                </tr>
                                                <tr>
                                                    <td>Repo 3</td>
                                                    <td>30</td>
                                                </tr>
                                                <tr>
                                                    <td>Repo 4</td>
                                                    <td>30</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                )
        }
    }

    projectInsightClickHandler = (project) => {
        if (project.repoUrl && localStorage.isGitHubLogin === "true" && project.isOwner) {
            this.setState({
                contributerName: undefined,
                contributerUrl: undefined,
                projectName: project.name,
                selectedTab: 'contributers',
                chartLoaded: chartLoaded,
                showInsightModal: true
            })
            localStorage['selectedProjectId'] = project.id;
            this.props.getContributers()
            this.props.getContributerList()
            this.props.getParticipation(this.state.weeksOfParticipation)
            this.props.getFrequency()
        } else {
            return false
        }
    }
    getPackageProductList = (packageName) => {
        let allProducts = this.state.allProducts;

    }

    addOrDeletePackages = (operation, name) => {
        let payload = cloneDeep(this.state.payload),
            allPackages = cloneDeep(this.state.allPackages),
            selectedPackage;

        if (operation === 'ADD') {
            if (payload.packages.length === 0) {
                selectedPackage = allPackages.filter(_package => _package.name === 'TRIAL')[0];
            } else {
                selectedPackage = allPackages.filter(_package => _package.added === false)[0];
            }
            allPackages.filter(_package => _package.name === selectedPackage.name)[0].added = true;
            payload.packages.push(selectedPackage);

        } else {
            payload.packages = payload.packages.filter(_package => _package.name !== name);
            allPackages.filter(_package => _package.name === name)[0].added = false;

        }

        this.setState((preState) => ({
            payload,
            allPackages,
            carouselItemsArr: this.getCarouselArr(payload.packages.length, 3),
            activeTabDiv: operation === "ADD" ? parseInt((payload.packages.length - 1) / 3) : (payload.packages.length % 3 == 0 && preState.activeTabDiv !== 0) ? (preState.activeTabDiv - 1) : preState.activeTabDiv
        }));
    }

    handlePackageChange = (event, existingPackageName) => {
        let payload = cloneDeep(this.state.payload),
            allPackages = cloneDeep(this.state.allPackages);

        let newPackage = event.target.value,
            existingIndex = payload.packages.findIndex(_package => _package.name === existingPackageName);

        payload.packages[existingIndex] = allPackages.filter(_package => _package.name === newPackage)[0];
        allPackages.filter(_package => _package.name === existingPackageName)[0].added = false;
        allPackages.filter(_package => _package.name === newPackage)[0].added = true;

        this.setState({
            payload,
            allPackages,
        })
    }


    previousNextHandler = (action) => {
        this.setState(function (prevState) {
            return {
                activeTabDiv: action == "prev" ? prevState.activeTabDiv - 1 : prevState.activeTabDiv + 1
            };
        });
    }

    changeActiveDivHandler = (index) => {
        this.setState({ activeTabDiv: index })
    }

    getCarouselArr = (totalLength, carouselItemsCount) => {
        let carouselsCount = Math.ceil(totalLength / carouselItemsCount),

            carouselItemsArr = [...Array(carouselsCount)].map((el, index) => {
                let startIndex = index === 0 ? 0 : index * carouselItemsCount;
                return ({
                    startIndex,
                    endIndex: startIndex + carouselItemsCount,
                })
            })
        return carouselItemsArr
    }

    addNewProjectHandler = () => {
        let payload = cloneDeep(this.state.payload);
        payload.name = '',
            payload.description = '',
            payload.image

        this.setState({
            payload,
            isEdit: false,
            addOrEditModal: true,
            errorCode: 0, carouselItemsArr: localStorage.tenantType == "MAKER" ? this.getCarouselArr(this.state.payload.packages.length, 3) : null
        })
    }

    getColumns = () => {
        return [
            {
                Header: "Name", width: 20,
                cell: (row) => (
                    <React.Fragment>
                        <div className="list-view-icon"><img src={row.image || "https://content.iot83.com/m83/misc/addProject.png"}></img></div>
                        <div className="list-view-icon-box">
                            <h6 className="text-theme fw-600">{row.name}</h6>
                            <p>{`${row.description || "-"}`}</p>
                        </div>
                    </React.Fragment>
                )
            },
            {
                Header: "Owner", width: "17_5",
                cell: (row) => (
                    <React.Fragment>
                        <h6>{row.owner}</h6>
                        <p><i className="fad fa-envelope text-primary mr-r-10"></i>{row.ownerEmail}</p>
                    </React.Fragment>
                )
            },
            {
                Header: "Access Level", width: 10, accessor: "projectAndAccessPermission",
                cell: (row) => (<h6 className={row.projectAndAccessPermission === 'admin' ? "text-green" : ""}>{row.projectAndAccessPermission === 'admin' ? "Admin" : row.projectAndAccessPermission === 'read_write' ? "Read Write" : ""}</h6>)
            },
            {
                Header: "Packages", width: "27_5",
                cell: (row) => (
                    <div className="button-group-link">
                        {row.resourcePackageName.map((pack, index) =>
                            <span className="alert alert-primary list-view-badge f-10 mr-1 pl-2 pr-2">{pack}</span>
                        )}
                    </div>
                )
            },
            { Header: "Created", width: 15, accessor: "createdAt" },
            {
                Header: "Actions", width: 10, cell: (row) => (
                    <div className="button-group">
                        {/*<button className="btn-transparent btn-transparent-green" onClick={() => { this.projectInsightClickHandler(row) }}><i className="far fa-chart-bar"></i></button>*/}
                        <button className="btn-transparent btn-transparent-blue" data-tooltip data-tooltip-text="Edit" data-tooltip-place="bottom"
                            onClick={() => {
                                if (this.isEditAllowed(row, row.isOwner || localStorage.role === "ACCOUNT_ADMIN")) {
                                    this.setState({
                                        edit: true,
                                        addOrEditModal: true,
                                        selectedProjectId: row.id,
                                        errorCode: 0,
                                        modalLoader: true
                                    }, () => this.props.getProjectDetails(row.id))
                                } else {
                                    return false
                                }
                            }}>
                            <i className="far fa-pencil"></i>
                        </button>
                        <button className="btn-transparent btn-transparent-red" data-tooltip data-tooltip-text="Delete" data-tooltip-place="bottom"
                            onClick={() => {
                                if (this.isDeleteAllowed(row, row.isOwner)) {
                                    this.setState({
                                        projectToBeDeleted: row.id,
                                        projectName: row.name,
                                        confirmState: true,
                                        isSourceControlEnabled: row.sourceControlEnabled
                                    })
                                } else {
                                    return false
                                }
                            }}>
                            <i className="far fa-trash-alt"></i>
                        </button>
                    </div>
                )
            },
        ]
    }
    productCountChangeHandler = (event, productId, packIndex) => {
        let payload = cloneDeep(this.state.payload);
        payload.packages[packIndex].productList.map(product => {
            if (product.id === productId) {
                if (product.name === 'device_types') {
                    payload.packages[packIndex].productList.filter(productObj => productObj.name === 'simulation')[0].count = event.target.value !== '' ? parseInt(event.target.value) : '';
                }
                product.count = event.target.value !== '' ? parseInt(event.target.value) : '';
            }
        })
        this.setState({
            payload
        });
    }

    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>Projects</title>
                    <meta name="description" content="M83-Projects" />
                </Helmet>

                {this.state.isFetching ? <Loader /> :
                    <React.Fragment>
                        <header className="content-header d-flex">
                            <div className="flex-60">
                                {this.state.addOrEditModal ?
                                    <div className="d-flex">
                                        <h6 className="previous" onClick={() => { this.setState({ addOrEditModal: false }) }}>Projects</h6>
                                        <h6 className="active">Add New</h6>
                                    </div> :
                                    <div className="d-flex">
                                        <h6>Projects</h6>
                                    </div>
                                }
                            </div>
                            {localStorage.tenantType !== "MAKER" && !this.state.addOrEditModal &&
                                <div className="flex-40 text-right">
                                    <div className="content-header-group">
                                        <div className="search-box">
                                            <span className="search-icon"><i className="far fa-search"></i></span>
                                            <input type="text" className="form-control" placeholder="Search..." value={this.state.searchProject} onChange={() => this.filterProjectList(event)} />
                                            {this.state.searchProject ?
                                                <button className="search-button" onClick={() => this.emptySearchBox()}><i className="far fa-times"></i></button> : ''}
                                        </div>
                                        {/*<button className="btn btn-light"><i className="fad fa-list-ul"></i></button>*/}
                                        {/*<button className="btn btn-light"><i className="fad fa-table"></i></button>*/}
                                        <button className="btn btn-primary" onClick={() => this.addNewProjectHandler()}><i className="far fa-plus"></i></button>
                                    </div>
                                </div>
                            }

                        </header>

                        {localStorage.tenantType === "MAKER" && this.state.addOrEditModal ?
                            <React.Fragment >
                                {this.state.modalLoader ? <Loader /> :
                                    <React.Fragment>
                                        <div className="content-body">
                                            {this.state.payload.packages.length === 0 ?
                                                <div className="form-content-wrapper form-content-wrapper-left h-100">
                                                    <div className="form-content-box h-100">
                                                        <div className="form-content-body h-100">
                                                            <div className="content-add-wrapper">
                                                                <div className="content-add-bg">
                                                                    <img src="https://content.iot83.com/m83/misc/m83-logo.png" />
                                                                </div>
                                                                <div className="content-add-detail">
                                                                    <div className="content-add-image-outer">
                                                                        <div className="content-add-image">
                                                                            <img src="https://content.iot83.com/m83/misc/addProject.png" />
                                                                        </div>
                                                                    </div>
                                                                    <h5>No packages available</h5>
                                                                    <h6>You have not created any packages yet.</h6>
                                                                    <button className="btn btn-primary" onClick={() => this.addOrDeletePackages("ADD")}>
                                                                        <i className="far fa-plus"></i>Add Package
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> :
                                                <div className="form-content-wrapper form-content-wrapper-left">
                                                    {this.state.payload.packages.map((pack, index) =>
                                                        <div className="form-content-box">
                                                            <div className="form-content-header">
                                                                <p><span className="text-theme fw-600">{pack.name}</span> Package</p>
                                                                <button type="button"
                                                                    disabled={pack.id || this.state.payload.packages.length === 1}
                                                                    className="btn-transparent btn-transparent-red"
                                                                    onClick={() => this.addOrDeletePackages("REMOVE", pack.name)}>
                                                                    <i className="far fa-trash-alt" />
                                                                </button>
                                                            </div>
                                                            <div className="form-content-body">
                                                                <div className="d-flex">
                                                                    <div className="flex-25 pd-r-10">
                                                                        <div className="form-group ">
                                                                            <label className="form-group-label">Plan Name :</label>
                                                                            <select
                                                                                className="form-control" id="name" disabled={pack.id} value={pack.name}
                                                                                onChange={(e) => {
                                                                                    this.handlePackageChange(e, pack.name)
                                                                                }}>
                                                                                <option
                                                                                    value={pack.name}>{pack.name}</option>
                                                                                {this.state.allPackages.filter(_package => _package.added === false).map((_package, index) =>
                                                                                    <option key={index} value={_package.name}>{_package.name}</option>
                                                                                )}
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div className="flex-25 pd-l-5 pd-r-7">
                                                                        <div className="form-group">
                                                                            <label className="form-group-label">Subscription
                                                                            Amount :</label>
                                                                            <input
                                                                                type="number"
                                                                                min="0"
                                                                                value={pack.subscriptionPrice}
                                                                                className="form-control"
                                                                                id="subscriptionPrice"
                                                                                disabled
                                                                            />
                                                                        </div>
                                                                    </div>
                                                                    <div className="flex-25 pd-l-5 pd-r-7">
                                                                        <div className="form-group">
                                                                            <label
                                                                                className="form-group-label">{pack.name !== 'TRIAL' ? 'Billing Cycle (In Days)' : 'Validity (In Days)'} :</label>
                                                                            <input
                                                                                type="number"
                                                                                min="0"
                                                                                value={pack.numberOfDays}
                                                                                className="form-control"
                                                                                id="numberOfDays"
                                                                                disabled
                                                                            />
                                                                        </div>
                                                                    </div>
                                                                    <div className="flex-25 pd-l-10">
                                                                        <div className="form-group">
                                                                            <label
                                                                                className="form-group-label">Data Retention :</label>
                                                                            <input
                                                                                type="text"
                                                                                value={`${(pack.productList.filter(product => product.name === 'retention')[0].count) / 30} Months`}
                                                                                className="form-control"
                                                                                id="numberOfDays"
                                                                                disabled
                                                                            />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <ul className="list-style-none d-flex custom-attribute-list">
                                                                    {pack.productList.map(product =>
                                                                        (product.visible && product.name !== 'retention') && <li key={product.id}>
                                                                            <div className="form-group">
                                                                                <label className="check-box">
                                                                                    <span className="check-text">{product.displayName}</span>
                                                                                    <input type="checkbox" checked />
                                                                                    <span className="check-mark"></span>
                                                                                </label>
                                                                                <input
                                                                                    type="number"
                                                                                    min="0"
                                                                                    value={product.count}
                                                                                    className="form-control fx-b50"
                                                                                    id={product.name}
                                                                                    onChange={(e) => { this.productCountChangeHandler(e, product.id, index) }}
                                                                                />
                                                                            </div>
                                                                        </li>
                                                                    )}
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    )}
                                                    <div className="text-center">
                                                        {this.state.payload.packages.length < 5 &&
                                                            <button className="btn-link" onClick={(e) => this.addOrDeletePackages("ADD")}>
                                                                <i className="far fa-plus"></i>Add New Package
                                                    </button>
                                                        }
                                                    </div>
                                                </div>
                                            }

                                            <div className="form-info-wrapper form-info-wrapper-left">
                                                <div className="form-info-header">
                                                    <h5>Basic Information</h5>
                                                </div>
                                                <div className="form-info-body form-info-body-adjust">
                                                    <div className="form-group">
                                                        <label className="form-group-label">Project Name : </label>
                                                        <input
                                                            type="text" id="name" className="form-control" pattern="^[a-zA-Z]([\w\$-]*[a-zA-Z0-9])?$" autoComplete="off" required
                                                            value={this.state.payload.name} pattern="^[\w\-\s]+$"
                                                            onChange={this.onChangeHandler}
                                                        />
                                                    </div>
                                                    {localStorage.isGitHubLogin === "true" &&
                                                        <div className="form-group">
                                                            <label className="form-group-label">Github Repository Name : </label>
                                                            <input
                                                                type="text" id="repoName" className="form-control" autoComplete="off" pattern="^[a-zA-Z0-9_-]*$" required
                                                                value={(this.state.selectedProjectId && this.state.payload.repoName) ? this.state.payload.repoName : this.state.repoName}
                                                                onChange={this.onChangeHandler}
                                                                disabled={this.state.selectedProjectId && this.state.payload.repoName}
                                                            />
                                                        </div>
                                                    }
                                                    <div className="form-group">
                                                        <label className="form-group-label">Description : </label>
                                                        <textarea
                                                            rows="3"
                                                            id="description"
                                                            placeholder="Description"
                                                            className="form-control"
                                                            value={this.state.payload.description}
                                                            onChange={this.onChangeHandler}
                                                        />
                                                    </div>
                                                    <div className="form-group">
                                                        <label className="form-group-label text-dark-theme">Project Image :</label>
                                                        <div className="file-upload-box">
                                                            {this.state.isUploadingImage ?
                                                                <div className="file-upload-loader">
                                                                    <img src="https://content.iot83.com/m83/misc/uploading.gif" />
                                                                    <p>Uploading...</p>
                                                                </div> :
                                                                this.state.payload.image ?
                                                                    <div className="file-upload-preview">
                                                                        <img src={this.state.payload.image} />
                                                                        <div className="fileUploadOverlay">
                                                                            <input type="file" name="image" id="UploadInput" onChange={this.fileUploadHandler} />
                                                                            <button type="button" className="btn-transparent btn-transparent-gray" data-tooltip data-tooltip-text="Update" data-tooltip-place="bottom" onClick={() => document.getElementById("UploadInput").click()}>
                                                                                <i className="far fa-pen" />
                                                                            </button>
                                                                        </div>
                                                                    </div> :
                                                                    <div className="file-upload-form">
                                                                        <input type="file" name="image" onChange={this.fileUploadHandler} />
                                                                        <p>Click to upload file...!</p>
                                                                    </div>
                                                            }
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="form-info-footer">
                                                    <button className="btn btn-light" onClick={() => { this.setState({ addOrEditModal: false }) }}>Cancel</button>
                                                    <button type="button" className="btn btn-primary" onClick={this.saveProject}>{this.state.selectedProjectId ? "Update" : "Save"}</button>
                                                </div>
                                            </div>
                                        </div>
                                    </React.Fragment>
                                }
                            </React.Fragment>
                            :
                            <div className="content-body">
                                {this.state.projectList && this.state.projectList.length > 0 ?
                                    this.state.filteredProjectList.length > 0 ?
                                        <ListingTable
                                            data={this.state.filteredProjectList}
                                            columns={this.getColumns()}
                                            icon={{
                                                className: "",
                                                position: "top-left",
                                                iconName: "fab fa-github",
                                                dynamic: "boolean",
                                                status: {
                                                    key: "repoName",
                                                    activeClass: "text-green",
                                                    inActiveClass: "text-gray"
                                                }
                                            }}
                                            itemClickHandler={(row) => { localStorage.role !== 'DEVELOPER' && row.projectAndAccessPermission && this.handleProjectClick(row.id) }}
                                        /> :
                                        <div className="content-add-wrapper">
                                            <div className="content-add-bg"><img src="https://content.iot83.com/m83/misc/m83-logo.png" /></div>
                                            <div className="content-add-detail">
                                                <div className="content-add-image-outer">
                                                    <div className="content-add-image">
                                                        <img src="https://content.iot83.com/m83/other/noSearchResult.png" />
                                                    </div>
                                                </div>
                                                <h5>No data matched your search</h5>
                                                <h6>Try using other search options</h6>
                                            </div>
                                        </div> :
                                    <div className="content-add-wrapper">
                                        <div className="content-add-bg"><img src="https://content.iot83.com/m83/misc/m83-logo.png" /></div>
                                        <div className="content-add-detail">
                                            <div className="content-add-image-outer">
                                                <div className="content-add-image">
                                                    <img src="https://content.iot83.com/m83/misc/addProject.png" />
                                                </div>
                                            </div>
                                            <h5>No projects available</h5>
                                            <h6>{localStorage.role === 'ACCOUNT_ADMIN' ? 'You have not created any projects yet.' : 'You do not have any projects assigned, Please contact your system administrator !'}</h6>
                                            {localStorage.role === 'ACCOUNT_ADMIN' &&
                                                <button className="btn btn-primary" onClick={() => this.addNewProjectHandler()}>
                                                    <i className="far fa-plus"></i>Add new
                                                    </button>
                                            }
                                        </div>
                                    </div>
                                }
                            </div>
                        }
                    </React.Fragment>
                }

                {/* add new project */}
                {localStorage.tenantType !== "MAKER" &&
                    <SlidingPane
                        className=''
                        overlayClassName='sliding-form'
                        closeIcon={<div></div>}
                        isOpen={this.state.addOrEditModal || false}
                        from='right'
                        width='500px'
                    >
                        <div className="modal-content">
                            <div className="modal-header">
                                <h6 className="modal-title">{this.state.selectedProjectId ? "Update Project" : "Add New Project"}
                                    <button type="button" className="btn btn-light close" data-dismiss="modal" onClick={this.closeModalHandler} aria-label="Close">
                                        <i className="far fa-angle-right"></i>
                                    </button>
                                </h6>
                            </div>
                            {this.state.modalLoader ?
                                <div className="modal-loader">
                                    <i className="fad fa-synce-alt fa-spin"></i>
                                </div> :
                                <form onSubmit={this.saveProject}>
                                    <div className="modal-body">
                                        {this.state.errorCode ?
                                            <p>Name should only contain alphanumeric characters, hyphen, underscore or space.</p> : ""
                                        }

                                        <div className="form-group">
                                            <label className="form-group-label">Project Name :</label>
                                            <input type="text" id="name" className="form-control" pattern="^[a-zA-Z]([\w\$-]*[a-zA-Z0-9])?$" autoComplete="off" required
                                                value={this.state.payload.name} pattern="^[\w\-\s]+$"
                                                onChange={() => this.onChangeHandler(event)}
                                            />
                                        </div>

                                        {localStorage.isGitHubLogin === "true" &&
                                            <div className="form-group">
                                                <label className="form-group-label">Github Repository name :</label>
                                                <input type="text" id="repoName" className="form-control" autoComplete="off" pattern="^[a-zA-Z0-9_-]*$" required
                                                    value={(this.state.selectedProjectId && this.state.payload.repoName) ? this.state.payload.repoName : this.state.repoName}
                                                    onChange={() => this.onChangeHandler(event)}
                                                    disabled={this.state.selectedProjectId && this.state.payload.repoName}
                                                />
                                            </div>
                                        }

                                        <div className="form-group">
                                            <label className="form-group-label">Description :</label>
                                            <textarea rows="3" id="description" className="form-control" value={this.state.payload.description}
                                                onChange={() => this.onChangeHandler(event)}
                                            />
                                        </div>

                                        <div className="form-group">
                                            <label className="form-label">Project Image :</label>
                                            <div className="file-upload-box">
                                                {this.state.isUploadingImage ?
                                                    <div className="file-upload-loader">
                                                        <img src="https://content.iot83.com/m83/misc/uploading.gif" />
                                                        <p>Uploading...</p>
                                                    </div> :
                                                    this.state.payload.image ?
                                                        <div className="file-upload-preview">
                                                            <img src={this.state.payload.image} />
                                                            <div className="fileUploadOverlay">
                                                                <input type="file" name="image" id="UploadInput" onChange={this.fileUploadHandler} />
                                                                <button type="button" className="btn-transparent btn-transparent-gray" data-tooltip data-tooltip-text="Update" data-tooltip-place="bottom" onClick={() => document.getElementById("UploadInput").click()}>
                                                                    <i className="far fa-pen" />
                                                                </button>
                                                            </div>
                                                        </div> :
                                                        <div className="file-upload-form">
                                                            <input type="file" accept="image/*" name="image" onChange={this.fileUploadHandler} />
                                                            <p>Click to upload file...!</p>
                                                        </div>
                                                }
                                            </div>
                                        </div>
                                    </div>
                                    <div className="modal-footer justify-content-start">
                                        <button className="btn btn-primary">{this.state.selectedProjectId ? "Update" : "Save"}</button>
                                        <button type="button" className="btn btn-dark" data-dismiss="modal" onClick={this.closeModalHandler}>Cancel</button>
                                    </div>
                                </form>
                            }
                        </div>
                    </SlidingPane>
                }
                {/* end add new project */}

                {/* project insights */}
                {this.state.showInsightModal &&
                    <div className="modal show d-block animated slideInDown" id="projectInsightModal" >
                        <div className="modal-dialog modal-xl modal-dialog-centered">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h6 className="modal-title">{this.state.projectName}
                                        <button type="button" className="close" data-dismiss="modal" onClick={() => {
                                            if (chart) {
                                                chart.dispose();
                                                delete chart.XYChart;
                                            }
                                            this.setState({
                                                showInsightModal: false
                                            })
                                        }}><i className="far fa-times"></i></button>
                                    </h6>
                                </div>

                                <div className="modal-body">
                                    <ul className="nav nav-tabs">
                                        <li><a className="nav-link active" data-toggle="tab" data-target="#contributers">Contributers</a></li>
                                        <li><a className="nav-link" data-toggle="tab" data-target="#frequency">Code Frequency</a></li>
                                        <li><a className="nav-link" data-toggle="tab" data-target="#participation" >Participation</a></li>
                                    </ul>
                                    <div className="tab-content">
                                        <div className="tab-pane fade show active" id="contributers">
                                            {this.getChartBoxHtml("contributers")}
                                        </div>
                                        <div className="tab-pane fade" id="frequency">
                                            {this.getChartBoxHtml("frequency")}
                                        </div>
                                        <div className="tab-pane fade" id="participation">
                                            {this.getChartBoxHtml("participation")}
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                }
                {/* end project insights */}

                {this.state.isOpen &&
                    <NotificationModal
                        type={this.state.type}
                        message2={this.state.message2}
                        onCloseHandler={this.closeHandler}
                    />
                }

                {this.state.confirmState &&
                    <DeleteModel
                        projectName={this.state.projectName}
                        isSourceControlEnabled={this.state.isSourceControlEnabled}
                        cancelClicked={() => {
                            this.cancelClicked()
                        }}
                        confirmClicked={(deleteRepo) => {
                            this.props.deleteProject(this.state.projectToBeDeleted, deleteRepo)
                            this.setState({
                                isFetching: true,
                                confirmState: false,
                                isSourceControlEnabled: null,
                            })
                        }}
                    />
                }
            </React.Fragment>
        );
    }
}

ProjectList.propTypes = {
    dispatch: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
    projectsList: SELECTORS.projectsSuccess(),
    projectsListFailure: SELECTORS.projectsFailure(),
    deleteProjectSuccess: SELECTORS.deleteProject(),
    deleteProjectFailure: SELECTORS.deleteProjectFailure(),
    addOrEditProjectSuccess: SELECTORS.addOrEditProjectSuccess(),
    addOrEditProjectFailure: SELECTORS.addOrEditProjectFailure(),
    imageUploadSuccess: SELECTORS.getImageUploadSuccess(),
    imageUploadFailure: SELECTORS.getImageUploadFailure(),
    contributersSuccess: SELECTORS.getContributersSuccess(),
    contributersFailure: SELECTORS.getContributersFailure(),
    frequencySuccess: SELECTORS.getFrequencySuccess(),
    frequencyFailure: SELECTORS.getFrequencyFailure(),
    participationSuccess: SELECTORS.getParticipationSuccess(),
    participationFailure: SELECTORS.getParticipationFailure(),
    contributerListSuccess: SELECTORS.getContributerListSuccess(),
    contributerListFailure: SELECTORS.getContributerListFailure(),
    contributerActivitiesSuccess: SELECTORS.getContributerActivitiesSuccess(),
    contributerActivitiesFailure: SELECTORS.getContributerActivitiesFailure(),
    projectDetailsSuccess: SELECTORS.projectDetailsSuccess(),
    projectDetailsFailure: SELECTORS.projectDetailsFailure(),
    allProducts: SELECTORS.productsSuccess(),
    productsFailure: SELECTORS.productsFailure(),
});

function mapDispatchToProps(dispatch) {
    return {
        dispatch,
        getProjectsList: () => dispatch(ACTIONS.getProjectsList()),
        fetchSideNav: () => dispatch(fetchSideNav()),
        deleteProject: (id, deleteRepo) => dispatch(ACTIONS.deleteProject(id, deleteRepo)),
        addOrEditProject: (payload, id) => dispatch(ACTIONS.addOrEditProject(payload, id)),
        uploadImageHandler: (payload, imageType) => dispatch(ACTIONS.uploadImageHandler(payload, imageType)),
        getContributers: () => dispatch(ACTIONS.getContributers()),
        getFrequency: () => dispatch(ACTIONS.getFrequency()),
        getParticipation: (numOfWeeks) => dispatch(ACTIONS.getParticipation(numOfWeeks)),
        getContributerList: () => dispatch(ACTIONS.getContributerList()),
        getContributerActivities: (contributorName, numOfWeeks) => dispatch(ACTIONS.getContributerActivities(contributorName, numOfWeeks)),
        getProjectDetails: (id) => dispatch(ACTIONS.getProjectDetails(id)),
        getAllProducts: () => dispatch(ACTIONS.getAllProducts())
    };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "projectList", reducer });
const withSaga = injectSaga({ key: "projectList", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(ProjectList);
