import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
 * Direct selector to the projectList state domain
 */

const selectProjectListDomain = state => state.get("projectList", initialState);

export const isFetchingState = state => state.get('loader');

export const deleteProject = () => createSelector(selectProjectListDomain, substate => substate.deleteProject);
export const deleteProjectFailure = () => createSelector(selectProjectListDomain, substate => substate.deleteProjectFailure);

export const addOrEditProjectSuccess = () => createSelector(selectProjectListDomain, substate => substate.addOrEditProjectSuccess);
export const addOrEditProjectFailure = () => createSelector(selectProjectListDomain, substate => substate.addOrEditProjectFailure);


export const getImageUploadSuccess = () => createSelector(selectProjectListDomain, subState => subState.imageUploadSuccess);
export const getImageUploadFailure = () => createSelector(selectProjectListDomain, subState => subState.imageUploadFailure);

export const getContributersSuccess = () => createSelector(selectProjectListDomain, subState => subState.contributersSuccess);
export const getContributersFailure = () => createSelector(selectProjectListDomain, subState => subState.contributersFailure);

export const getFrequencySuccess = () => createSelector(selectProjectListDomain, subState => subState.frequencySuccess);
export const getFrequencyFailure = () => createSelector(selectProjectListDomain, subState => subState.frequencyFailure);

export const getParticipationSuccess = () => createSelector(selectProjectListDomain, subState => subState.participationSuccess);
export const getParticipationFailure = () => createSelector(selectProjectListDomain, subState => subState.participationFailure);

export const getContributerListSuccess = () => createSelector(selectProjectListDomain, subState => subState.contributerListSuccess);
export const getContributerListFailure = () => createSelector(selectProjectListDomain, subState => subState.contributerListFailure);

export const getContributerActivitiesSuccess = () => createSelector(selectProjectListDomain, subState => subState.contributerActivitiesSuccess);
export const getContributerActivitiesFailure = () => createSelector(selectProjectListDomain, subState => subState.contributerActivitiesFailure);

export const projectsSuccess = () => createSelector(selectProjectListDomain, subState => subState.projectsSuccess);
export const projectsFailure = () => createSelector(selectProjectListDomain, subState => subState.projectsFailure);

export const projectDetailsSuccess = () => createSelector(selectProjectListDomain, subState => subState.projectDetailsSuccess);
export const projectDetailsFailure = () => createSelector(selectProjectListDomain, subState => subState.projectDetailsFailure);

export const productsSuccess = () => createSelector(selectProjectListDomain, subState => subState.productsSuccess);
export const productsFailure = () => createSelector(selectProjectListDomain, subState => subState.productsFailure);

export const isFetchingStatus = state => state.get('loader');

export const getIsFetching = () => createSelector(isFetchingStatus, substate => substate.get('isFetching'));

const selectHomePageDomain = state => state.get('homePage', initialState);

export const getProjectsList = () => createSelector(selectHomePageDomain, subState => subState.projectsSuccess);

export const getProjectsListFailure = () => createSelector(selectHomePageDomain, subState => subState.projectsFailure);

export { selectProjectListDomain };
