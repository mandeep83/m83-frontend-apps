/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * ManageDiscounts
 *
 */

import React from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {Helmet} from "react-helmet";
import {FormattedMessage} from "react-intl";
import {createStructuredSelector} from "reselect";
import {compose} from "redux";

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import makeSelectManageDiscounts from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import messages from "./messages";

/* eslint-disable react/prefer-stateless-function */
export class ManageDiscounts extends React.Component {
    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>ManageDiscounts</title>
                    <meta name="description" content="Description of ManageDiscounts"/>
                </Helmet>
    
                <header className="content-header d-flex">
                    <div className="flex-60">
                        <h6>Manage Discounts
                            <span className="content-header-badge-group">
                                <span className="content-header-badge-item">
                                    <span className="badge badge-pill badge-primary">20</span>
                                </span>
                            </span>
                        </h6>
                    </div>
                    <div className="flex-40 text-right">
                        <div className="content-header-group">
                            <button className="btn btn-light" data-tooltip data-tooltip-text="Refresh" data-tooltip-place="bottom">
                                <i className="far fa-sync-alt"></i>
                            </button>
                        </div>
                    </div>
                </header>
                
                <div className="content-body">
                    <div className="collapse-wrapper">
                        <div className="collapse-header collapsed" data-toggle="collapse" data-target="#item1">
                            <h6>Users <strong className="text-theme">(2)</strong><i className="fad fa-chevron-double-up collapse-header-toggle collapsed"></i></h6>
                        </div>
                        <div className="collapse" id="item1">
                            <div className="collapse-body">
                                <div className="content-table discount-table">
                                    <table className="table table-bordered mb-0">
                                        <thead>
                                        <tr>
                                            <th>Quantity (From)</th>
                                            <th>Quantity (To)</th>
                                            <th>Unit Price (in $)</th>
                                            <th>Discount (%)</th>
                                            <th>Final Price (in $)</th>
                                        </tr>
                                        </thead>
                                    </table>
                                    <div className="d-flex">
                                        <div className="flex-20 form-group mb-0"><input typeof="text" className="form-control" placeholder="1" /></div>
                                        <div className="flex-20 form-group mb-0"><input typeof="text" className="form-control" placeholder="100" /></div>
                                        <div className="flex-20 form-group mb-0"><input typeof="text" className="form-control" placeholder="1" /></div>
                                        <div className="flex-20 form-group mb-0"><input typeof="text" className="form-control" placeholder="10" /></div>
                                        <div className="flex-20 form-group mb-0"><input typeof="text" className="form-control" placeholder="0.9" /></div>
                                    </div>
                                    <div className="d-flex">
                                        <div className="flex-20 form-group mb-0"><input typeof="text" className="form-control" placeholder="1" /></div>
                                        <div className="flex-20 form-group mb-0"><input typeof="text" className="form-control" placeholder="100" /></div>
                                        <div className="flex-20 form-group mb-0"><input typeof="text" className="form-control" placeholder="1" /></div>
                                        <div className="flex-20 form-group mb-0"><input typeof="text" className="form-control" placeholder="10" /></div>
                                        <div className="flex-20 form-group mb-0"><input typeof="text" className="form-control" placeholder="0.9" /></div>
                                    </div>
                                    <div className="text-center mt-2">
                                        <button className="btn btn-link"><i className="far fa-plus"></i> Add More</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
    
                    <div className="collapse-wrapper">
                        <div className="collapse-header collapsed" data-toggle="collapse" data-target="#item2">
                            <h6>Devices <strong className="text-theme">(2)</strong><i className="fad fa-chevron-double-up collapse-header-toggle collapsed"></i></h6>
                        </div>
                        <div className="collapse" id="item2">
                            <div className="collapse-body">
                                <div className="content-table discount-table">
                                    <table className="table table-bordered mb-0">
                                        <thead>
                                        <tr>
                                            <th>Quantity From</th>
                                            <th>Quantity To</th>
                                            <th>Unit Price</th>
                                            <th>Discount</th>
                                            <th>Final Price</th>
                                        </tr>
                                        </thead>
                                    </table>
                                    <div className="d-flex">
                                        <div className="flex-20 form-group mb-0"><input typeof="text" className="form-control" placeholder="1" /></div>
                                        <div className="flex-20 form-group mb-0"><input typeof="text" className="form-control" placeholder="100" /></div>
                                        <div className="flex-20 form-group mb-0"><input typeof="text" className="form-control" placeholder="1" /></div>
                                        <div className="flex-20 form-group mb-0"><input typeof="text" className="form-control" placeholder="10" /></div>
                                        <div className="flex-20 form-group mb-0"><input typeof="text" className="form-control" placeholder="0.9" /></div>
                                    </div>
                                    <div className="d-flex">
                                        <div className="flex-20 form-group mb-0"><input typeof="text" className="form-control" placeholder="1" /></div>
                                        <div className="flex-20 form-group mb-0"><input typeof="text" className="form-control" placeholder="100" /></div>
                                        <div className="flex-20 form-group mb-0"><input typeof="text" className="form-control" placeholder="1" /></div>
                                        <div className="flex-20 form-group mb-0"><input typeof="text" className="form-control" placeholder="10" /></div>
                                        <div className="flex-20 form-group mb-0"><input typeof="text" className="form-control" placeholder="0.9" /></div>
                                    </div>
                                    <div className="text-center mt-2">
                                        <button className="btn btn-link"><i className="far fa-plus"></i> Add More</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
    
                    <div className="collapse-wrapper">
                        <div className="collapse-header collapsed" data-toggle="collapse" data-target="#item3">
                            <h6>Attributes <strong className="text-theme">(2)</strong><i className="fad fa-chevron-double-up collapse-header-toggle collapsed"></i></h6>
                        </div>
                        <div className="collapse" id="item3">
                            <div className="collapse-body">
                                <div className="content-table discount-table">
                                    <table className="table table-bordered mb-0">
                                        <thead>
                                        <tr>
                                            <th>Quantity From</th>
                                            <th>Quantity To</th>
                                            <th>Unit Price</th>
                                            <th>Discount</th>
                                            <th>Final Price</th>
                                        </tr>
                                        </thead>
                                    </table>
                                    <div className="d-flex">
                                        <div className="flex-20 form-group mb-0"><input typeof="text" className="form-control" placeholder="1" /></div>
                                        <div className="flex-20 form-group mb-0"><input typeof="text" className="form-control" placeholder="100" /></div>
                                        <div className="flex-20 form-group mb-0"><input typeof="text" className="form-control" placeholder="1" /></div>
                                        <div className="flex-20 form-group mb-0"><input typeof="text" className="form-control" placeholder="10" /></div>
                                        <div className="flex-20 form-group mb-0"><input typeof="text" className="form-control" placeholder="0.9" /></div>
                                    </div>
                                    <div className="d-flex">
                                        <div className="flex-20 form-group mb-0"><input typeof="text" className="form-control" placeholder="1" /></div>
                                        <div className="flex-20 form-group mb-0"><input typeof="text" className="form-control" placeholder="100" /></div>
                                        <div className="flex-20 form-group mb-0"><input typeof="text" className="form-control" placeholder="1" /></div>
                                        <div className="flex-20 form-group mb-0"><input typeof="text" className="form-control" placeholder="10" /></div>
                                        <div className="flex-20 form-group mb-0"><input typeof="text" className="form-control" placeholder="0.9" /></div>
                                    </div>
                                    <div className="text-center mt-2">
                                        <button className="btn btn-link"><i className="far fa-plus"></i> Add More</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
                
            </React.Fragment>
        );
    }
}

ManageDiscounts.propTypes = {
    dispatch: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
    managediscounts: makeSelectManageDiscounts()
});

function mapDispatchToProps(dispatch) {
    return {
        dispatch
    };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({key: "manageDiscounts", reducer});
const withSaga = injectSaga({key: "manageDiscounts", saga});

export default compose(
    withReducer,
    withSaga,
    withConnect
)(ManageDiscounts);
