
/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
* [2017] - [2022] 83incs Ltd.
* All Rights Reserved.
*
* NOTICE: All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any). The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { compose } from "redux";
import * as am4core from "@amcharts/amcharts4/core";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import * as am4charts from "@amcharts/amcharts4/charts";

let chart = {};
// let valueChanged, minMaxChanged, unitChanged, colorChanged, chartTitleChanged, bottomTextChanged;
import startCase from 'lodash/startCase'

/* eslint-disable react/prefer-stateless-function */
export class GaugeChart extends React.Component {
    state = {
        data: this.props.data,
        chartNumber: 0
    }

    componentDidMount() {
        this.createChart(this.props.data);
    }

    componentWillUnmount() {
        if (chart[this.props.id]) {
            chart[this.props.id].dispose();
            delete chart[this.props.id];
        }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if (prevState.data !== nextProps.data) {
            return {
                data: nextProps.data,
                theme: nextProps.theme,
                id: nextProps.id
            }
        }
        return null
    }

    changes = {
        valueChanged: false,
        minMaxChanged: false,
        unitChanged: false,
        colorChanged: false,
        chartTitleChanged: false,
        bottomTextChanged: false
    }

    shouldComponentUpdate(nextProps, nextState) {
        let data = nextProps.data[0]
        this.changes.valueChanged = data.value !== this.props.data[0].value
        this.changes.minMaxChanged = (data.min !== this.props.data[0].min) || (data.max !== this.props.data[0].max)
        this.changes.unitChanged = data.unit !== this.props.data[0].unit
        this.changes.colorChanged = JSON.stringify(data.rangeColors) !== JSON.stringify(this.props.data[0].rangeColors)
        this.changes.chartTitleChanged = data.deviceName !== this.props.data[0].deviceName
        this.changes.bottomTextChanged = data.attrDisplayName !== this.props.data[0].attrDisplayName
        if (this.changes.valueChanged || this.changes.minMaxChanged || this.changes.unitChanged || this.changes.colorChanged) {
            return true;
        }
        return false
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.changes.valueChanged) {
            chart[this.props.id].hands.values[0].showValue(this.props.data[0].value, 1000, am4core.ease.cubicOut);
        }
        if (this.changes.minMaxChanged) {
            chart[this.props.id].xAxes.values.map(value => {
                value.min = this.props.data[0].min;
                value.max = this.props.data[0].max;
            })
        }
        if (this.changes.unitChanged) {
            chart[this.props.id].radarContainer.children.values.find(child => child._className === "Label").text = `${this.props.data[0].value.toFixed(1)} ${this.props.data[0].unit || ""}`
        }
        if (this.changes.colorChanged) {
            this.createChart(this.props.data);
        }
        if (this.changes.chartTitleChanged) {
            if (!prevProps.data[0].deviceName) {
                var title = chart[this.props.id].titles.create();
                title.text = this.props.data[0].deviceName;
                title.fontSize = 20;
                title.marginBottom = 0;
            } else {
                chart[this.props.id].titles.values[0].text = this.props.data[0].deviceName || "";
            }
        }
        if (this.changes.bottomTextChanged) {
            if (!prevProps.data[0].deviceName || (!chart[this.props.id].chartContainer.children.values[3] && this.props.data[0].deviceName)) {
                var label = chart[this.props.id].chartContainer.createChild(am4core.Label);
                label.text = startCase(this.props.data[0].attrDisplayName);
                label.align = "center";
                label.fontWeight = 500;
            } else {
                if (chart[this.props.id].chartContainer.children.values[3])
                    chart[this.props.id].chartContainer.children.values[3].text = startCase(this.props.data[0].attrDisplayName) || ""
            }
        }
    }

    createChart = (data) => {
        if (chart[this.props.id]) {
            chart[this.props.id].dispose()
        }
        am4core.addLicense("CH145187641");
        am4core.useTheme(am4themes_animated);
        chart[this.props.id] = am4core.create(this.props.id, am4charts.GaugeChart);
        // if (data[0].deviceName) {
        // var title = chart[this.props.id].titles.create();
        // title.text = data[0].deviceName;
        // title.fontSize = 14;
        // title.fontWeight = 500;
        // title.fill = " #466081";
        // title.marginBottom = 5;
        // }
        if (data[0].attrDisplayName) {
            var label = chart[this.props.id].chartContainer.createChild(am4core.Label);
            label.text = startCase(data[0].attrDisplayName) + " (" + data[0].deviceName + ")";
            label.align = "center";
            label.fontWeight = 500;
            label.fontSize = 13;
            label.fill = "#808080";
            label.marginTop = 25;
            label.marginBottom = 0;
        }
        chart.height = am4core.percent(100);
        chart[this.props.id].innerRadius = am4core.percent(82);
        chart[this.props.id].radius = am4core.percent(100);
        chart[this.props.id].startAngle = -190;
        chart[this.props.id].endAngle = 10;
        var axis = chart[this.props.id].xAxes.push(new am4charts.ValueAxis());
        axis.min = data[0].min;
        axis.max = data[0].max;
        axis.strictMinMax = true;
        axis.renderer.line.stroke = "##808080";
        axis.renderer.line.strokeWidth = 1;
        axis.renderer.radius = am4core.percent(78);
        axis.renderer.inside = true;
        axis.renderer.line.strokeOpacity = 1;
        axis.renderer.ticks.template.disabled = true
        axis.renderer.ticks.template.strokeOpacity = 10;
        axis.renderer.ticks.template.length = 5;
        axis.renderer.grid.template.disabled = true;
        axis.renderer.labels.template.disabled = true;
        axis.renderer.labels.template.radius = 25;
        axis.renderer.labels.template.fontSize = 10;
        axis.renderer.labels.template.fontWeight = 500;
        axis.renderer.labels.template.fill = "#808080";
        axis.renderer.labels.template.adapter.add("text", function (text) {
            return text;
        });
        let shadowAxis = axis.filters.push(new am4core.DropShadowFilter());
        shadowAxis.dy = 4;
        shadowAxis.dx = 0;
        // shadowAxis.opacity = .6;
        shadowAxis.color = "#b8b8b8";

        var axis2 = chart[this.props.id].xAxes.push(new am4charts.ValueAxis());
        axis2.min = data[0].min;
        axis2.max = data[0].max;
        axis2.renderer.ticks.template.disabled = true
        axis2.renderer.innerRadius = am4core.percent(84);
        axis2.strictMinMax = true;
        axis2.renderer.labels.template.disabled = true;
        axis2.renderer.ticks.template.disabled = true;
        axis2.renderer.grid.template.disabled = true;
        let shadowAxis2 = axis2.filters.push(new am4core.DropShadowFilter());
        shadowAxis2.dy = 4;
        shadowAxis2.dx = 0;
        // shadowAxis2.opacity = .6;
        shadowAxis2.color = "#b8b8b8";
        /**
        * Ranges
        */
        var range1, range0;
        if (!data[0].rangeColors || data[0].rangeColors.isTwoColored) {
            let customColors = data[0].rangeColors
            range0 = axis2.axisRanges.create();
            range0.value = data[0].min;
            range0.endValue = data[0].value;
            range0.axisFill.fillOpacity = 1;
            range0.axisFill.fill = customColors ? customColors.colors[0].hexCode : "#6771DC";

            range1 = axis2.axisRanges.create();
            range1.value = data[0].value;
            range1.endValue = data[0].max;
            range1.axisFill.fillOpacity = 1;
            range1.axisFill.fill = customColors ? customColors.colors[1].hexCode : "#67b7dc";
        } else {
            let customColors = data[0].rangeColors.colors,
                totalRange = Math.abs(data[0].max - data[0].min)
            customColors.map(({ hexCode, end }, i) => {
                var range = axis2.axisRanges.create();
                range.value = i === 0 ? data[0].min : data[0].min + ((customColors[i - 1].end / 100) * totalRange)
                range.endValue = data[0].min + ((end / 100) * totalRange);
                range.axisFill.fillOpacity = 1;
                range.axisFill.fill = hexCode;
            })
        }

        /**
        * Label
        */

        var label = chart[this.props.id].radarContainer.createChild(am4core.Label);
        label.isMeasured = false;
        label.fontSize = 16;
        label.fontWeight = 600;
        label.fill = "#4a4a4a";
        label.x = am4core.percent(50);
        label.y = am4core.percent(100);
        label.horizontalCenter = "middle";
        label.verticalCenter = "bottom";
        label.dy = 45;
        label.text = `${data[0].value} ${data[0].unit || ""}`;
        /**
        * Hand
        */

        var hand = chart[this.props.id].hands.push(new am4charts.ClockHand());
        hand.axis = axis2;
        hand.radius = am4core.percent(80);
        hand.pin.disabled = false;
        hand.fill = am4core.color("#4a4a4a");
        hand.stroke = am4core.color("#4a4a4a");
        hand.value = data[0].value;
        hand.startWidth = 6;
        hand.endWidth = 2;
        let shadowHand = hand.filters.push(new am4core.DropShadowFilter());
        shadowHand.dy = 2;
        shadowHand.color = "#b8b8b8";
        let shadowPin = hand.pin.filters.push(new am4core.DropShadowFilter());
        shadowPin.dy = -2;
        shadowPin.dx = -2;
        shadowPin.color = "#b8b8b8";
        hand.pin.radius = "6"
        hand.pin.strokeWidth = 4;
        hand.pin.stroke = "#fff";
        hand.pin.fill = "#4a4a4a";
        hand.pin.zIndex = 1;
        hand.events.on("propertychanged", (ev) => {
            let min = this.props.data[0].min,
                max = this.props.data[0].max;
            axis2.min = min
            axis2.max = max
            axis.min = min
            axis.max = max
            if (this.props.type === "animatedGauge" && (!data[0].rangeColors || data[0].rangeColors.isTwoColored)) {
                range0.value = min;
                range0.endValue = ev.target.value;
                range1.value = ev.target.value;
                range1.endValue = max;
            } else {
                let customColors = this.props.data[0].rangeColors.colors,
                    positiveMinMax = min >= 0 && max >= 0,
                    negativeMinMax = min <= 0 && max <= 0,
                    totalRange = Math.abs(max - min);
                customColors.map(({ hexCode, end }, i) => {
                    var range = axis2.axisRanges.create();
                    range.value = i === 0 ? min : min + ((customColors[i - 1].end / 100) * totalRange)
                    range.endValue = min + ((end / 100) * totalRange);
                    range.axisFill.fillOpacity = 1;
                    range.axisFill.fill = hexCode;
                })
            }
            label.text = `${this.props.data[0].value.toFixed(1)} ${this.props.data[0].unit || ""}`;
            let ranges = []
            for (var i = this.props.data[0].min; i <= this.props.data[0].max; i = i + ((this.props.data[0].max - this.props.data[0].min) / 4)) {
                ranges.push(i);
            }
            ranges.map((val, i) => {
                axis.axisRanges.values[i].value = val.toString().includes(".") ? val.toFixed(2) : val;
            })
            axis2.invalidate();
        });

        function createGrid(value) {
            var range = axis.axisRanges.create();
            range.value = value;
            range.label.text = "{value}";
            range.grid.disabled = true
        }

        for (var i = data[0].min; i <= data[0].max; i = i + (Math.abs(data[0].max - data[0].min) / 4)) {
            createGrid(i);
        }
    }

    render() {
        return (
            <React.Fragment>
                <div id={this.props.id} className="dashboard-chart-gauge" />
                <ul className="custom-legends legends-max-height">
                    <li>
                        <i class="far fa-repeat-alt text-primary" />
                        <p>Average :<strong>{this.props.data[0].avgVal ? this.props.data[0].avgVal.toFixed(2) : "-"}</strong></p>
                    </li>
                    <li>
                        <i className="far fa-arrow-to-top text-danger" />
                        <p>Max Value :<strong>{this.props.data[0].maxVal ? this.props.data[0].maxVal : "-"}</strong></p>
                    </li>
                    <li>
                        <i class="far fa-arrow-to-bottom text-warning" />
                        <p>Min Value :<strong>{this.props.data[0].minVal ? this.props.data[0].minVal : "-"}</strong></p>
                    </li>
                    <li>
                        <i class="far fa-random text-secondary" />
                        <p>Std. Deviation :<strong>{this.props.data[0].stdDev ? this.props.data[0].stdDev.toFixed(2) : "-"}</strong></p>
                    </li>
                </ul>

                {/* <ul className="list-style-none d-flex">
                    <li><h5>{this.props.data[0].avgVal ? this.props.data[0].avgVal.toFixed(2) : "-"}</h5><p>Average</p></li>
                    <li><h5>{this.props.data[0].maxVal ? this.props.data[0].maxVal : "-"}</h5><p>Max Value</p></li>
                    <li><h5>{this.props.data[0].minVal ? this.props.data[0].minVal : "-"}</h5><p>Min Value</p></li>
                    <li><h5>{this.props.data[0].stdDev ? this.props.data[0].stdDev.toFixed(2) : "-"}</h5><p>Std. Deviation</p></li>
                </ul> */}
            </React.Fragment>
        )
    }
}

GaugeChart.propTypes = {
    dispatch: PropTypes.func.isRequired
};

function mapDispatchToProps(dispatch) {
    return {
        dispatch
    };
}

const withConnect = connect(
    null,
    mapDispatchToProps
);

export default compose(withConnect)(GaugeChart);
