/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * ManageScheduler
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { FormattedMessage } from "react-intl";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import reducer from "./reducer";
import saga from "./saga";
import SlidingPane from "react-sliding-pane";
import ReactSelect from "react-select";
import ReactTooltip from "react-tooltip";
import Loader from "../../components/Loader";
import * as SELECTORS from "./selectors";
import * as ACTIONS from "./actions";
import NotificationModal from '../../components/NotificationModal/Loadable'
import ConfirmModel from "../../components/ConfirmModel";
import cloneDeep from "lodash/cloneDeep";
import { stringAppend, getInitials } from '../../commonUtils';
import AddNewButton from "../../components/AddNewButton/Loadable";
import NoDataFoundMessage from "../../components/NoDataFoundMessage/Loadable";
import ListingTable from "../../components/ListingTable/Loadable";
import QuotaHeader from "../../components/QuotaHeader/Loadable";
import Skeleton from "react-loading-skeleton";

/* eslint-disable react/prefer-stateless-function */
export class ManageScheduler extends React.Component {
    state = {
        schedulerModal: false,
        scheduleList: [],
        isFetching: true,
        toggleView: true,
        searchTerm: '',
        filteredList: [],
        isFetchingSchedule: 0,
        payload: {
            name: '',
            description: '',
            schedulerType: 'FaaS',
            repeats: {
                type: 'interval',
                runsOn: [],
                repeatIntervalUnit: 'hours',
                every: '',
                inject: true,
            },
            appliedOn: []
        },
        daysList: ["MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY", "SUNDAY"],
        isOpen: false,
        startingHours: [],
        endingHours: [],
        minutesArray: [],
        allPolicies: [],
        allReports: [],
        allFaaS: [],
        errorMessage: null,
        saveScheduleInProgress: false,
    };

    qoutaCallback = () => {
        this.props.getScheduleList();
        this.generateHoursAndMinutes();
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.getScheduleListSuccess && nextProps.getScheduleListSuccess !== this.props.getScheduleListSuccess) {
            this.setState({
                scheduleList: nextProps.getScheduleListSuccess,
                isFetching: false,
                filteredList: nextProps.getScheduleListSuccess,
            })
        }

        if (nextProps.getScheduleListFailure && nextProps.getScheduleListFailure !== this.props.getScheduleListFailure) {
            this.setState({
                isOpen: true,
                modalType: "error",
                message2: nextProps.getScheduleListFailure,
                isFetching: false
            }, () => this.props.resetToInitialState())
        }

        if (nextProps.createUpdateScheduleSuccess && nextProps.createUpdateScheduleSuccess !== this.props.createUpdateScheduleSuccess) {
            let isAddSuccess = this.state.isAddSuccess
            if (!nextProps.createUpdateScheduleSuccess.id) {
                isAddSuccess = !isAddSuccess
            }
            this.setState((prevState) => ({
                isOpen: true,
                modalType: "success",
                message2: nextProps.createUpdateScheduleSuccess.message,
                isAddSuccess,
                saveScheduleInProgress: false,
                schedulerModal: false,
                isFetching: true
            }), () => { this.props.getScheduleList(); this.props.resetToInitialState() })
        }

        if (nextProps.createUpdateScheduleError && nextProps.createUpdateScheduleError !== this.props.createUpdateScheduleError) {
            this.setState({
                isOpen: true,
                modalType: "error",
                message2: nextProps.createUpdateScheduleError,
                isFetching: false,
                saveScheduleInProgress: false,
                isFetchingSchedule: 0
            }, () => this.props.resetToInitialState())
        }

        if (nextProps.getScheduleByIdSuccess && nextProps.getScheduleByIdSuccess !== this.props.getScheduleByIdSuccess) {
            let startingHours = JSON.parse(JSON.stringify(this.state.startingHours)),
                endingHours = JSON.parse(JSON.stringify(this.state.endingHours)),
                minutesArray = JSON.parse(JSON.stringify(this.state.minutesArray)),
                payload = nextProps.getScheduleByIdSuccess;


            if (payload.repeats.type === "at a specific time") {
                payload.repeats.fromHours = payload.repeats.at.split(":")[0]
                payload.repeats.fromMinutes = payload.repeats.at.split(":")[1]
            }
            else if (payload.repeats.type === "interval between times") {
                payload.repeats.fromHours = payload.repeats.from.split(":")[0]
                payload.repeats.fromMinutes = payload.repeats.from.split(":")[1]
                payload.repeats.endHours = payload.repeats.to.split(":")[0]
                payload.repeats.endMinutes = payload.repeats.to.split(":")[1]
                for (let i = 0; i < startingHours.length; i++) {
                    if (startingHours[i].time === payload.repeats.fromHours) {
                        startingHours[i].selected = true
                    }
                    startingHours[i].selected = false
                    endingHours[i].disabled = true
                    if (parseInt(payload.repeats.fromHours) <= parseInt(endingHours[i].time)) {
                        endingHours[i].disabled = false
                    }
                }
                for (let i = 0; i < minutesArray.length; i++) {
                    minutesArray[i].selected = false
                    minutesArray[i].disabled = false
                    if (minutesArray[i].time === payload.repeats.fromMinutes) {
                        minutesArray[i].selected = true
                    }

                    if (payload.repeats.fromHours === payload.repeats.endHours && payload.repeats.endHours !== '') {
                        for (let i = 0; i < minutesArray.length; i++) {
                            minutesArray[i].disabled = true
                            if (minutesArray[i].selected) {
                                minutesArray[i].disabled = true
                                break;
                            }
                        }
                    }

                }
            }
            this.setState(prevState => ({
                isFetchingSchedule: prevState.isFetchingSchedule - 1,
                payload,
                startingHours,
                endingHours,
                minutesArray,
            }), () => this.props.resetToInitialState())
        }

        if (nextProps.getScheduleByIdError && nextProps.getScheduleByIdError !== this.props.getScheduleByIdError) {
            this.setState({
                isOpen: true,
                modalType: "error",
                message2: nextProps.getScheduleByIdError,
                isFetching: false
            }, () => this.props.resetToInitialState())
        }

        if (nextProps.deleteScheduleSuccess && nextProps.deleteScheduleSuccess !== this.props.deleteScheduleSuccess) {
            let scheduleList = JSON.parse(JSON.stringify(this.state.scheduleList))
            scheduleList = scheduleList.filter(list => list.id !== nextProps.deleteScheduleSuccess.id)
            this.setState((prevState) => ({
                isOpen: true,
                modalType: "success",
                message2: nextProps.deleteScheduleSuccess.message,
                scheduleList,
                filteredList: scheduleList,
                isFetching: false,
                isDeletedSuccess: !prevState.isDeletedSuccess
            }), () => this.props.resetToInitialState())
        }

        if (nextProps.deleteScheduleError && nextProps.deleteScheduleError !== this.props.deleteScheduleError) {
            this.setState({
                isOpen: true,
                modalType: "error",
                message2: nextProps.deleteScheduleError,
                isFetching: false
            }, () => this.props.resetToInitialState())
        }

        if (nextProps.getAllReportsSuccess && nextProps.getAllReportsSuccess !== this.props.getAllReportsSuccess) {
            let allReports = nextProps.getAllReportsSuccess.map(report => {
                return ({
                    id: report.id,
                    value: report.id,
                    type: "report",
                    label: report.name
                })
            })
            this.setState(prevState => ({
                allReports,
                isFetchingSchedule: prevState.isFetchingSchedule - 1,
            }), () => this.props.resetToInitialState())
        }

        if (nextProps.getAllReportsFailure && nextProps.getAllReportsFailure !== this.props.getAllReportsFailure) {
            this.setState(prevState => ({
                isOpen: true,
                modalType: "error",
                message2: nextProps.getAllReportsFailure,
                isFetchingSchedule: prevState.isFetchingSchedule - 1,
            }), () => this.props.resetToInitialState())
        }

        if (nextProps.getAllFaaSSuccess && nextProps.getAllFaaSSuccess !== this.props.getAllFaaSSuccess) {
            let allFaaS = nextProps.getAllFaaSSuccess.map(action => {
                return ({
                    id: action.id,
                    value: action.id,
                    type: "action",
                    label: action.actionName
                })
            })
            this.setState(prevState => ({
                allFaaS,
                isFetchingSchedule: prevState.isFetchingSchedule - 1,
            }), () => this.props.resetToInitialState())
        }

        if (nextProps.getAllFaaSFailure && nextProps.getAllFaaSFailure !== this.props.getAllFaaSFailure) {
            this.setState(prevState => ({
                isOpen: true,
                modalType: "error",
                message2: nextProps.getAllFaaSFailure,
                isFetchingSchedule: prevState.isFetchingSchedule - 1,
            }), () => this.props.resetToInitialState())
        }

        if (nextProps.getAllPoliciesSuccess && nextProps.getAllPoliciesSuccess !== this.props.getAllPoliciesSuccess) {
            let allPolicies = nextProps.getAllPoliciesSuccess.map(policy => {
                return ({
                    id: policy.id,
                    value: policy.id,
                    type: "policy",
                    label: policy.name
                })
            })
            this.setState(prevState => ({
                allPolicies,
                isFetchingSchedule: prevState.isFetchingSchedule - 1,
            }), () => this.props.resetToInitialState())
        }

        if (nextProps.getAllPoliciesFailure && nextProps.getAllPoliciesFailure !== this.props.getAllPoliciesFailure) {
            this.setState(prevState => ({
                isOpen: true,
                modalType: "error",
                message2: nextProps.getAllPoliciesFailure,
                isFetchingSchedule: prevState.isFetchingSchedule - 1,
            }), () => this.props.resetToInitialState())
        }

        if (nextProps.schedulerStateSuccess && nextProps.schedulerStateSuccess !== this.props.schedulerStateSuccess) {
            let filteredList = JSON.parse(JSON.stringify(this.state.filteredList))
            filteredList.map((temp) => {
                if (temp.id === nextProps.schedulerStateSuccess.id) {
                    temp.state = nextProps.schedulerStateSuccess.state
                    temp.loader = false
                }
            })
            let scheduleList = JSON.parse(JSON.stringify(this.state.scheduleList))
            scheduleList.map((temp) => {
                if (temp.id === nextProps.schedulerStateSuccess.id) {
                    temp.state = nextProps.schedulerStateSuccess.state

                }
            })
            this.setState({
                isOpen: true,
                modalType: "success",
                message2: nextProps.schedulerStateSuccess.response,
                filteredList,
                scheduleList
            })
        }

        if (nextProps.schedulerStateFailure && nextProps.schedulerStateFailure !== this.props.schedulerStateFailure) {
            let filteredList = JSON.parse(JSON.stringify(this.state.filteredList))
            filteredList.map((temp) => {
                if (temp.id === nextProps.schedulerStateFailure.id) {
                    temp.loader = false
                }
            })
            this.setState({
                isOpen: true,
                filteredList,
                modalType: "error",
                message2: nextProps.schedulerStateFailure.error,
            })
        }
    }

    generateHoursAndMinutes = () => {
        let startingHours = [],
            endingHours = [],
            minutesArray = [];
        for (let i = 0; i <= 23; i++) {
            startingHours.push({
                time: `${i <= 9 ? '0' + i : i}`,
                selected: false,
            })
            endingHours.push({
                time: `${i <= 9 ? '0' + i : i}`,
                disabled: true,
            })
        }
        for (let i = 0; i <= 59; i++) {
            minutesArray.push({
                time: `${i <= 9 ? '0' + i : i}`,
                selected: false,
                disabled: false,
            })
        }
        this.setState({
            startingHours,
            endingHours,
            minutesArray
        })
    }

    onCloseHandler = () => {
        this.setState({
            isOpen: false,
            type: "",
            message2: "",
        })
    }

    cancelClicked = () => {
        this.setState({
            selectedScheduleId: "",
            confirmState: false,
            selectedScheduleName: ''
        })
    }
    filteredDataList = (value) => {
        let filterList = cloneDeep(this.state.scheduleList);
        if (value) {
            filterList = filterList.filter(item => item.name.toLowerCase().includes(value.toLowerCase()))
        }

        this.setState({
            searchTerm: value,
            filteredList: filterList
        })
    }

    emptySearchBox = () => {
        let filteredList = JSON.parse(JSON.stringify(this.state.scheduleList))
        this.setState({
            searchTerm: "",
            filteredList
        })
    }

    changeHandler = (event) => {
        let payload = JSON.parse(JSON.stringify(this.state.payload));
        if (event.target.name === 'repeat') {
            if (event.target.id === "inject") {
                payload.repeats.inject = event.target.checked;
            }
            else if (event.target.id === "type") {
                payload.repeats.type = event.target.value;
                payload.repeats.fromHours = ''
                payload.repeats.fromMinutes = ''
                payload.repeats.endHours = ''
                payload.repeats.endMinutes = ''
                payload.repeats.every = ''
            }
            else if (event.target.id === "every") {
                payload.repeats.every = event.target.value > 0 ? event.target.value : ''
            }
            else {
                payload.repeats[event.target.id] = event.target.value
            }
        } else if (event.target.id === "injectInterval") {
            payload.repeats[event.target.id] = event.target.value
        }
        else if (event.currentTarget.name === "schedulerType") {
            if (event.currentTarget.value === "policy") {
                payload.repeats.type = "interval between times"
            }
            payload.schedulerType = event.currentTarget.value
            payload.appliedOn.length = 0
        } else {
            payload[event.target.id] = event.target.value
        }
        this.setState({
            payload,
            errorMessage: null
        })
    }

    addRemoveDays = (day) => {
        let payload = JSON.parse(JSON.stringify(this.state.payload));
        if (payload.repeats.runsOn.includes(day)) {
            let removeIndex = payload.repeats.runsOn.findIndex(repeatDay => repeatDay === day)
            payload.repeats.runsOn.splice(removeIndex, 1)
        }
        else {
            payload.repeats.runsOn.push(day)
        }
        this.setState({
            payload,
            errorMessage: null
        })
    }

    timeChangeHandler = (event) => {
        let startingHours = JSON.parse(JSON.stringify(this.state.startingHours)),
            endingHours = JSON.parse(JSON.stringify(this.state.endingHours)),
            minutesArray = JSON.parse(JSON.stringify(this.state.minutesArray)),
            payload = JSON.parse(JSON.stringify(this.state.payload));
        if (event.target.id === 'fromHours') {
            payload.repeats[event.target.id] = event.target.value;
            for (let i = 0; i < startingHours.length; i++) {
                if (startingHours[i].time === event.target.value) {
                    startingHours[i].selected = true
                }
                startingHours[i].selected = false
                endingHours[i].disabled = true
                if (parseInt(event.target.value) <= parseInt(endingHours[i].time)) {
                    endingHours[i].disabled = false
                }
            }
            payload.repeats.endHours = '';
            payload.repeats.endMinutes = '';
        } else if (event.target.id === 'fromMinutes') {
            payload.repeats[event.target.id] = event.target.value;
            for (let i = 0; i < minutesArray.length; i++) {
                minutesArray[i].selected = false
                minutesArray[i].disabled = false
                if (minutesArray[i].time === event.target.value) {
                    minutesArray[i].selected = true
                }
            }
            if (payload.repeats.fromHours === payload.repeats.endHours && payload.repeats.endHours !== '') {
                for (let i = 0; i < minutesArray.length; i++) {
                    minutesArray[i].disabled = true
                    if (minutesArray[i].selected) {
                        minutesArray[i].disabled = true
                        break;
                    }
                }
            }
            payload.repeats.endMinutes = '';
        } else if (event.target.id === 'endHours') {
            payload.repeats[event.target.id] = event.target.value;
            for (let i = 0; i < endingHours.length; i++) {
                if (endingHours[i].time === event.target.value) {
                    endingHours[i].selected = true
                }
                endingHours[i].selected = false
            }
            for (let i = 0; i < minutesArray.length; i++) {
                minutesArray[i].disabled = false
            }
            if (payload.repeats.fromHours === event.target.value) {
                for (let i = 0; i < minutesArray.length; i++) {
                    minutesArray[i].disabled = true
                    if (minutesArray[i].selected) {
                        minutesArray[i].disabled = true
                        break;
                    }
                }
            }
            payload.repeats.endMinutes = '';
        } else if (event.target.id === 'endMinutes') {
            payload.repeats[event.target.id] = event.target.value;
        }
        this.setState({
            payload,
            startingHours,
            endingHours,
            minutesArray,
            errorMessage: null
        })
    }

    createUpdateSchedule = (event) => {
        event.preventDefault();
        let payload = cloneDeep(this.state.payload);
        payload.name = payload.name.trim()
        if (payload.appliedOn.length === 0) {
            this.setState({
                errorMessage: "Please choose FaaS/ETL before creating schedule."
            })
            return;
        } else if (payload.repeats.type === "at a specific time") {
            if ((!payload.repeats.fromHours) || (!payload.repeats.fromMinutes)) {
                this.setState({
                    errorMessage: "Please choose valid time for schedule."
                })
                return;
            }
            else if (payload.repeats.runsOn.length === 0) {
                this.setState({
                    errorMessage: "Please choose valid day for schedule."
                })
                return;
            } else {
                payload.repeats.at = `${payload.repeats.fromHours}:${payload.repeats.fromMinutes}`
                payload.repeats.timeZone = Intl.DateTimeFormat().resolvedOptions().timeZone;
                delete payload.repeats.fromHours
                delete payload.repeats.fromMinutes
            }
        }
        else if (payload.repeats.type === "interval between times") {
            if ((!payload.repeats.fromHours) || (!payload.repeats.fromMinutes) || (!payload.repeats.endHours) || (!payload.repeats.endMinutes)) {
                this.setState({
                    errorMessage: "Please choose valid time for schedule."
                })
                return;
            }
            else if (payload.repeats.runsOn.length === 0) {
                this.setState({
                    errorMessage: "Please choose valid day for schedule."
                })
                return;
            } else {
                payload.repeats.from = `${payload.repeats.fromHours}:${payload.repeats.fromMinutes}`
                payload.repeats.to = `${payload.repeats.endHours}:${payload.repeats.endMinutes}`
                payload.repeats.timeZone = Intl.DateTimeFormat().resolvedOptions().timeZone;
                delete payload.repeats.fromHours
                delete payload.repeats.fromMinutes
                delete payload.repeats.endHours
                delete payload.repeats.endMinutes
            }
        }
        this.setState({
            saveScheduleInProgress: true,
            errorMessage: null
        }, () => this.props.createUpdateSchedule(payload))
    }

    openModal = (el, schedulerType) => {
        let fetchData = !(this.state.allReports.length && this.state.allPolicies.length && this.state.allFaaS.length),
            isFetchingSchedule = el && fetchData ? 4 : el && !fetchData ? 1 : !el && fetchData ? 3 : 0,
            payload;
        if (el) {
            payload = el
        }
        else {
            payload = {
                name: '',
                description: '',
                schedulerType: schedulerType,
                repeats: {
                    type: 'interval',
                    runsOn: [],
                    repeatIntervalUnit: 'hours',
                    every: '',
                    inject: true,
                },
                appliedOn: []
            }
        }
        this.setState({
            payload,
            schedulerModal: true,
            isFetchingSchedule,
        }, () => {
            if (fetchData) {
                this.props.getAllReports();
                this.props.getAllPolicies();
                this.props.getAllFaaS();
            }
            el && this.props.getScheduleById(el.id)
        })
    }

    reactSelectChangeHandler = (list) => {
        let payload = cloneDeep(this.state.payload);
        payload.appliedOn = [];
        list.map(listObj => {
            payload.appliedOn.push({
                type: listObj.type,
                id: listObj.id,
            })
        })
        this.setState({ payload });
    }

    getValueForReactSelect = (type) => {
        if (type === 'FaaS') {
            let valueArr = this.state.payload.appliedOn,
                value = cloneDeep(this.state.allFaaS).filter(element => valueArr.find(el => el.id === element.id));
            return value;
        }
    }

    getColumns = () => {
        let column = [
            {
                Header: "Name", width: 20,
                cell: (row) => (
                    <React.Fragment>
                        <div className="list-view-icon"><i className="fad fa-calendar-alt"></i></div>
                        <div className="list-view-icon-box">
                            <h6 className="text-theme fw-600">{row.name}</h6>
                            <p>{`${row.description || "-"}`}</p>
                        </div>
                    </React.Fragment>
                )
            },
            {
                Header: "State", width: 10,
                cell: (row) => (
                    <h6 className={`text-underline-hover cursor-pointer ${row.state === "stop" ? "text-red" : "text-green"}`}>
                        {row.state == "stop" ? "Stop" : "Start"}
                    </h6>
                )
            },
            {
                Header: "Trigger Type", width: 15,
                cell: (row) => (
                    <React.Fragment>
                        <h6 className="text-green text-capitalize">{row.repeats.type}</h6>
                        {row.repeats.type == "at a specific time" ? <p>{`${row.repeats.at}`}</p> : null}
                    </React.Fragment>
                )
            },
            {
                Header: "Repeat", width: 10,
                cell: (row) => (
                    <React.Fragment>
                        <h6>{row.repeats.type == "none" ? "N/A" : row.repeats.type == "at a specific time" ? row.repeats.runsOn.map((el, index) => { return index == row.repeats.runsOn.length - 1 ? el.substring(0, 3) : `${el.substring(0, 3)}, ` }) : `${row.repeats.every} ${row.repeats.every == 1 ? row.repeats.repeatIntervalUnit.substring(0, row.repeats.repeatIntervalUnit.length - 1) : row.repeats.repeatIntervalUnit}`}</h6>
                        {row.repeats.type == "interval between times" ? <p>{row.repeats.runsOn.map((el, index) => { return index == row.repeats.runsOn.length - 1 ? el.substring(0, 3) : `${el.substring(0, 3)}, ` })}</p> : null}
                    </React.Fragment>
                )
            },
            { Header: "Created", width: 15, accessor: "createdAt" },
            { Header: "Updated", width: 15, accessor: "updatedAt" },
            {
                Header: "Actions", width: 15,
                cell: (row) => (
                    <div className="button-group">
                        {row.loader ?
                            <button type="button" className="btn-transparent btn-transparent-green">
                                <i className="far fa-cog fa-spin"></i>
                            </button> :
                            <button
                                className={row.state === "start" ? "btn-transparent btn-transparent-gray" : "btn-transparent btn-transparent-green"}
                                data-tooltip data-tooltip-text={row.state === "stop" ? "Start" : "Stop"}
                                data-tooltip-place="bottom" onClick={() => {
                                    let filteredList = JSON.parse(JSON.stringify(this.state.filteredList))
                                    filteredList.map((temp) => {
                                        if (temp.id === row.id) {
                                            temp.loader = true
                                        }
                                    })
                                    this.setState({ filteredList }, () => this.props.changeSchedulerState(row.id, row.state === "stop" ? "start" : "stop"))
                                }}>
                                <i className={row.state === "stop" ? "far fa-play" : "far fa-stop"}></i>
                            </button>
                        }
                        <button className="btn-transparent btn-transparent-blue" data-tooltip data-tooltip-text="Edit" data-tooltip-place="bottom" disabled={row.state === "start"} onClick={() => { this.openModal(row) }}>
                            <i className="far fa-pencil"></i>
                        </button>
                        <button className="btn-transparent btn-transparent-red" data-tooltip data-tooltip-text="Delete" data-tooltip-place="bottom" disabled={row.state === "start"} onClick={() => { this.setState({ confirmState: true, selectedScheduleId: row.id, selectedScheduleName: row.name }) }}>
                            <i className="far fa-trash-alt"></i>
                        </button>
                    </div>
                )
            }
        ];
        return column;
    }

    getStatusBarClass = (row) => {
        if (row.state === "start")
            return { className: "green", tooltipText: "Running" }
        return { className: "red", tooltipText: "Stopped" }
    }

    addNewScheduleHandler = () => {
        let payload = {
            name: '',
            description: '',
            schedulerType: 'FaaS',
            repeats: {
                type: 'interval',
                runsOn: [],
                repeatIntervalUnit: 'hours',
                every: '',
                inject: true,
            },
            appliedOn: [],
        }
        this.setState({
            payload,
            schedulerModal: true,
            isFetchingSchedule: 1,
            errorMessage: null,
        }, () => this.props.getAllFaaS());
    }

    refreshComponent = () => {
        this.setState({
            isFetching: true,
        }, () =>
            this.props.getScheduleList(),
            this.generateHoursAndMinutes()
        )
    }

    toggleView = () => {
        this.setState((prevState) => ({ toggleView: !prevState.toggleView }))
    }

    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>Scheduler</title>
                    <meta name="description" content="Description of ManageScheduler" />
                </Helmet>

                <QuotaHeader
                    showQuota
                    addButtonText="Add Schedule"
                    componentName="Schedules"
                    productName="schedules"
                    onAddClick={this.addNewScheduleHandler}
                    toggleButton={this.toggleView}
                    searchBoxDetails={{
                        value: this.state.searchTerm,
                        onChangeHandler: this.filteredDataList,
                        isDisabled: !this.state.scheduleList.length || this.state.isFetching
                    }}
                    refreshDisabled={this.state.isFetching}
                    addDisabled={this.state.isFetching}
                    refreshHandler={this.refreshComponent}
                    callBack={this.qoutaCallback}
                    isAddSuccess={this.state.isAddSuccess}
                    isDeletedSuccess={this.state.isDeletedSuccess}
                    {...this.props}
                />

                <div className="content-body">
                    {this.state.isFetching ?
                        <Skeleton count={9} className="skeleton-list-loader" /> :
                        <React.Fragment>
                            {this.state.scheduleList.length > 0 ?
                                this.state.filteredList.length > 0 ?
                                    this.state.toggleView ?
                                        <ListingTable
                                            columns={this.getColumns()}
                                            data={this.state.filteredList}
                                            statusBar={this.getStatusBarClass}
                                        /> :
                                        <ul className="card-view-list">
                                            {this.state.scheduleList.map((item, index) =>
                                                <li key={index}>
                                                    <div className="card-view-box">
                                                        <div className="card-view-header">
                                                            <span data-tooltip data-tooltip-text={item.state === "start" ? "Start" : "Stop"} data-tooltip-place="bottom" className={item.state === "start" ? "card-view-header-status bg-success" : "card-view-header-status bg-danger"}></span>
                                                            <span className="alert alert-primary">Repeat {item.repeats.type == "none" ? "N/A" : item.repeats.type == "at a specific time" ? item.repeats.runsOn.map((el, index) => { return index == item.repeats.runsOn.length - 1 ? el.substring(0, 3) : `${el.substring(0, 3)}, ` }) : `${item.repeats.every} ${item.repeats.every == 1 ? item.repeats.repeatIntervalUnit.substring(0, item.repeats.repeatIntervalUnit.length - 1) : item.repeats.repeatIntervalUnit}`}
                                                                {item.repeats.type == "interval between times" ? <strong className="ml-2">({item.repeats.runsOn.map((el, index) => { return index == item.repeats.runsOn.length - 1 ? el.substring(0, 3) : `${el.substring(0, 3)}, ` })})</strong> : null}
                                                            </span>
                                                            <div className="dropdown">
                                                                <button className="btn-transparent btn-transparent-gray" type="button" data-toggle="dropdown">
                                                                    <i className="fas fa-ellipsis-v"></i>
                                                                </button>
                                                                <div className="dropdown-menu">
                                                                    {item.loader ?
                                                                        <button type="button" className="dropdown-item">
                                                                            <i className="far fa-cog fa-spin"></i>
                                                                        </button> :
                                                                        <button
                                                                            className="dropdown-item"
                                                                            onClick={() => {
                                                                                let filteredList = JSON.parse(JSON.stringify(this.state.filteredList))
                                                                                filteredList.map((temp) => {
                                                                                    if (temp.id === item.id) {
                                                                                        temp.loader = true
                                                                                    }
                                                                                })
                                                                                this.setState({ filteredList }, () => this.props.changeSchedulerState(item.id, item.state === "stop" ? "start" : "stop"))
                                                                            }}>
                                                                            <i className={item.state === "stop" ? "far fa-play" : "far fa-stop"}></i>{item.state === "stop" ? "Start" : "Stop"}
                                                                        </button>
                                                                    }
                                                                    <button className="dropdown-item" disabled={item.state === "start"} onClick={() => { this.openModal(item) }}>
                                                                        <i className="far fa-pencil"></i>Edit
                                                                    </button>
                                                                    <button className="dropdown-item" disabled={item.state === "start"} onClick={() => { this.setState({ confirmState: true, selectedScheduleId: item.id, selectedScheduleName: item.name }) }}>
                                                                        <i className="far fa-trash-alt"></i>Delete
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="card-view-body">
                                                            <div className="card-view-icon">
                                                                <i className="fad fa-calendar-alt"></i>
                                                            </div>
                                                            <h6><i className="far fa-address-book"></i>{item.name ? item.name : "N/A"}</h6>
                                                            <p><i className="far fa-file-alt"></i>{item.description ? item.description : "N/A"}</p>
                                                            <p className={item.schedulerType === "FaaS" ? "text-pink" : "text-indigo"} onClick={() => this.props.history.push(`${item.schedulerType.toLowerCase() == "faas" ? item.schedulerType.toLowerCase() : "etlPipeline"}`)}>
                                                                <i className="far fa-align-left"></i>{item.schedulerType}
                                                            </p>
                                                            <p className="text-green text-capitalize">
                                                                <i className="far fa-clock"></i>
                                                                {item.repeats.type} {item.repeats.type == "at a specific time" ? "(" + item.repeats.at + ")" : null}
                                                            </p>
                                                        </div>
                                                        <div className="card-view-footer d-flex">
                                                            <div className="flex-50 p-3">
                                                                <h6>Created</h6>
                                                                <p><strong>By - </strong>{item.createdBy}</p>
                                                                <p><strong>At - </strong>{new Date(item.createdAt).toLocaleString('en-US', { createdAt: localStorage.createdAt })}</p>
                                                            </div>
                                                            <div className="flex-50 p-3">
                                                                <h6>Updated</h6>
                                                                <p><strong>By - </strong>{item.updatedBy}</p>
                                                                <p><strong>At - </strong>{new Date(item.updatedAt).toLocaleString('en-US', { createdAt: localStorage.createdAt })}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            )}
                                        </ul>
                                    :
                                    <NoDataFoundMessage /> :
                                <AddNewButton
                                    text1="No Schedule(s) available"
                                    text2="You haven't created any schedules yet. Please create a schedule first."
                                    imageIcon="addSchedule.png"
                                    addButtonEnable={true}
                                    createItemOnAddButtonClick={this.addNewScheduleHandler}
                                />
                            }
                        </React.Fragment>
                    }
                </div>

                {/* add new schedule */}
                <SlidingPane
                    className=''
                    overlayClassName='sliding-form'
                    closeIcon={<div></div>}
                    isOpen={this.state.schedulerModal || false}
                    from='right'
                    width='500px'
                    ariaHideApp={false}
                >
                    <div className="modal-content">
                        <div className="modal-header">
                            <h6 className="modal-title">{this.state.payload.id ? "Update" : "Add New"} Schedule
                                <button className="btn btn-light close" onClick={() => { this.setState({ schedulerModal: false }) }}><i className="fas fa-angle-right"></i></button>
                            </h6>
                        </div>

                        {this.state.saveScheduleInProgress || this.state.isFetchingSchedule ?
                            <div className="modal-loader">
                                <i className="fad fa-sync-alt fa-spin"></i>
                            </div> :
                            <form onSubmit={this.createUpdateSchedule}>
                                <div className="modal-body">
                                    {this.state.errorMessage &&
                                        <p className="modal-body-error">{this.state.errorMessage}</p>
                                    }

                                    {/* <div className="form-group">
                                        <label className="form-group-label">Type :</label>
                                        <div className="d-flex">
                                            <div className="flex-25 pd-r-10">
                                                <label className="radio-button">
                                                    <span className="radio-button-text">FaaS</span>
                                                    <input type="radio" name="schedulerType" value="FaaS" readOnly={this.state.payload.id} checked={this.state.payload.schedulerType === "FaaS"} onChange={this.changeHandler} />
                                                    <span className="radio-button-mark"></span>
                                                </label>
                                            </div>
                                            <div className="flex-25 pd-r-10">
                                                <label className="radio-button radio-button-disabled">
                                                    <span className="radio-button-text">ETL</span>
                                                    <input type="radio" name="schedulerType" value="ETL" disabled checked={this.state.payload.schedulerType === "ETL"} onChange={this.changeHandler} />
                                                    <span className="radio-button-mark"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div> */}

                                    <div className="form-group">
                                        <label className="form-group-label">FaaS : <i className="fas fa-asterisk form-group-required"></i></label>
                                        <ReactSelect
                                            options={this.state.allFaaS}
                                            value={this.getValueForReactSelect(this.state.payload.schedulerType)}
                                            onChange={this.reactSelectChangeHandler}
                                            className="form-control-multi-select"
                                            isMulti={true}
                                            name={this.state.payload.schedulerType}
                                        >
                                        </ReactSelect>
                                    </div>

                                    <div className="form-group">
                                        <label className="form-group-label">Name : <i className="fas fa-asterisk form-group-required"></i></label>
                                        <input type="text" id="name" name="scheduler" className="form-control" readOnly={this.state.payload.id}
                                            value={this.state.payload.name} onChange={this.changeHandler} required
                                        />
                                    </div>

                                    <div className="form-group">
                                        <label className="form-group-label">Description :</label>
                                        <textarea id="description" name="description" rows="3" className="form-control"
                                            value={this.state.payload.description} onChange={this.changeHandler}
                                        />
                                    </div>

                                    <div className="form-group">
                                        <label className="form-group-label">Repeat : <i className="fas fa-asterisk form-group-required"></i></label>
                                        <select id="type" name="repeat" className="form-control" value={this.state.payload.repeats.type} onChange={this.changeHandler}>
                                            {/* <option value="none" disabled={this.state.payload.schedulerType === "policy"}>None</option> */}
                                            <option value="interval" disabled={this.state.payload.schedulerType === "policy"}>Interval</option>
                                            <option value="at a specific time" disabled={this.state.payload.schedulerType === "policy"}>At specific time</option>
                                            <option value="interval between times">Interval between time</option>
                                        </select>
                                    </div>

                                    {this.state.payload.repeats.type === "interval" &&
                                        <div className="form-group">
                                            <label className="form-group-label">Every : <i className="fas fa-asterisk form-group-required"></i></label>
                                            <div className="input-group">
                                                <input type="number" id="every" name="repeat" className="form-control" value={this.state.payload.repeats.every}
                                                    onChange={this.changeHandler} required
                                                />
                                                <div className="input-group-append">
                                                    <div className="input-group-text">
                                                        <select id="repeatIntervalUnit" name="repeat" className="cursor-pointer" value={this.state.payload.repeats.repeatIntervalUnit} onChange={this.changeHandler}>
                                                            <option value="hours">Hours</option>
                                                            <option value="minutes">Minutes</option>
                                                            {/* <option value="seconds">Seconds</option> */}
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    }

                                    {this.state.payload.repeats.type === "at a specific time" &&
                                        <React.Fragment>
                                            <div className="d-flex">
                                                <div className="form-group flex-50 pd-r-10">
                                                    <label className="form-group-label">Hours :</label>
                                                    <select className="form-control" name="repeat" id="fromHours" value={this.state.payload.repeats.fromHours} onChange={this.timeChangeHandler}>
                                                        <option value=''>Select</option>
                                                        {this.state.startingHours.map((hours, index) => (
                                                            <option key={index} value={hours.time}>{hours.time}</option>
                                                        ))}
                                                    </select>
                                                </div>
                                                <div className="form-group flex-50 pd-l-10">
                                                    <label className="form-group-label">Minutes :</label>
                                                    <select className="form-control" name="repeat" id="fromMinutes" value={this.state.payload.repeats.fromMinutes} onChange={this.timeChangeHandler}>
                                                        <option value=''>Select</option>
                                                        {this.state.minutesArray.map((hours, index) => (
                                                            <option key={index} value={hours.time}>{hours.time}</option>
                                                        ))}
                                                    </select>
                                                </div>
                                            </div>

                                            <div className="form-group">
                                                <label className="form-group-label">Days :</label>
                                                <ul className="day-list d-flex">
                                                    {this.state.daysList.map((day, index) => {
                                                        return (
                                                            <li key={index} onClick={() => this.addRemoveDays(day)} className={this.state.payload.repeats.runsOn.includes(day) ? "active" : ''}>
                                                                {day.slice(0, 1)}
                                                            </li>
                                                        )
                                                    })}
                                                </ul>
                                            </div>
                                        </React.Fragment>
                                    }

                                    {this.state.payload.repeats.type === "interval between times" &&
                                        <React.Fragment>
                                            <div className="form-group">
                                                <label className="form-group-label">Every : <i className="fas fa-asterisk form-group-required"></i></label>
                                                <div className="input-group">
                                                    <input type="number" id="every" name="repeat" className="form-control" value={this.state.payload.repeats.every} onChange={this.changeHandler} required />
                                                    <div className="input-group-append">
                                                        <span className="input-group-text">
                                                            <select id="repeatIntervalUnit" name="repeat" value={this.state.payload.repeats.repeatIntervalUnit} onChange={this.changeHandler}>
                                                                <option value="hours"> Hours </option>
                                                                <option value="minutes">Minutes</option>
                                                                {/* <option value="seconds">Seconds</option> */}
                                                            </select>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="form-group d-flex">
                                                <div className="flex-50 pd-r-10">
                                                    <label className="form-group-label">From :</label>
                                                    <div className="border border-radius-4 p-3">
                                                        <div className="form-group">
                                                            <label className="form-group-label">Hours :</label>
                                                            <select className="form-control" name="repeat" id="fromHours" value={this.state.payload.repeats.fromHours} onChange={this.timeChangeHandler}>
                                                                <option value=''>Select</option>
                                                                {this.state.startingHours.map((hours, index) => (
                                                                    <option key={index} value={hours.time}>{hours.time}</option>
                                                                )
                                                                )}
                                                            </select>
                                                        </div>
                                                        <div className="form-group mb-0">
                                                            <label className="form-group-label">Minutes :</label>
                                                            <select className="form-control" name="repeat" id="fromMinutes" value={this.state.payload.repeats.fromMinutes} onChange={this.timeChangeHandler}>
                                                                <option value=''>Select</option>
                                                                {this.state.minutesArray.map((hours, index) => (
                                                                    <option key={index} value={hours.time}>{hours.time}</option>
                                                                )
                                                                )}
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="flex-50 pd-l-10">
                                                    <label className="form-group-label fw-600">To :</label>
                                                    <div className="border border-radius-4 p-3">
                                                        <div className="form-group">
                                                            <label className="form-group-label">Hours :</label>
                                                            <select className="form-control" name="repeat" id="endHours" value={this.state.payload.repeats.endHours} onChange={this.timeChangeHandler}>
                                                                <option value=''>Select</option>
                                                                {this.state.endingHours.map((hours, index) => (
                                                                    <option key={index} disabled={hours.disabled} value={hours.time}>{hours.time}</option>
                                                                )
                                                                )}
                                                            </select>
                                                        </div>
                                                        <div className="form-group mb-0">
                                                            <label className="form-group-label">Minutes :</label>
                                                            <select className="form-control" name="repeat" id="endMinutes" value={this.state.payload.repeats.endMinutes} onChange={this.timeChangeHandler}>
                                                                <option value=''>Select</option>
                                                                {this.state.minutesArray.map((hours, index) => (
                                                                    <option key={index} disabled={hours.disabled} value={hours.time}>{hours.time}</option>
                                                                )
                                                                )}
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="form-group">
                                                <label className="form-group-label">Days :</label>
                                                <ul className="day-list d-flex">
                                                    {this.state.daysList.map((day, index) => {
                                                        return (
                                                            <li key={index} onClick={() => this.addRemoveDays(day)} className={this.state.payload.repeats.runsOn.includes(day) ? "active" : ''}>{day.slice(0, 1)}</li>
                                                        )
                                                    })
                                                    }
                                                </ul>
                                            </div>
                                        </React.Fragment>
                                    }

                                    {/* {(this.state.payload.repeats.type === "none" || this.state.payload.repeats.type === "interval") ?
                                        <div className="form-group">
                                            <label className="form-group-label">Trigger after : <i className="fas fa-asterisk form-group-required"></i>
                                                {/*<span className="check-box float-left">
                                                    <input type="checkbox" name="repeat" id="inject" checked={this.state.payload.repeats.inject} onChange={this.changeHandler} />
                                                    <span className="check-mark"></span>
                                                </span> // to be commented 
                                            </label>
                                            <div className="input-group pt-1">
                                                <input type="number" id="injectInterval" name="repeat" className="form-control" value={this.state.payload.repeats.injectInterval} onChange={this.changeHandler} required={this.state.payload.repeats.inject} />
                                                <div className="input-group-append">
                                                    <span className="input-group-text">minutes</span>
                                                </div>
                                            </div>
                                        </div> : ''
                                    } */}

                                    {/*<div className="form-group">
                                        <label className="form-label">{this.state.payload.schedulerType === "policy" ? "Policy" : this.state.payload.schedulerType === "report" ? "Report" : "FaaS"} :</label>
                                        <ReactSelect
                                            options={this.state.payload.schedulerType === "policy" ? this.state.allPolicies : this.state.payload.schedulerType === "report" ? this.state.allReports : this.state.allFaaS}
                                            value={this.getValueForReactSelect(this.state.payload.schedulerType)}
                                            onChange={this.reactSelectChangeHandler}
                                            className="form-control-multi-select"
                                            isMulti={true}
                                            name={this.state.payload.schedulerType}
                                        >
                                        </ReactSelect>
                                    </div>*/}
                                </div>

                                <div className="modal-footer justify-content-start">
                                    <button className="btn btn-primary">{this.state.payload.id ? "Update" : "Save"}</button>
                                    <button type="button" className="btn btn-dark" onClick={() => { this.setState({ schedulerModal: false }) }}>Cancel</button>
                                </div>
                            </form>
                        }
                    </div>
                </SlidingPane>
                {/* end add new scheduler */}

                {this.state.isOpen &&
                    <NotificationModal
                        type={this.state.modalType}
                        message2={this.state.message2}
                        onCloseHandler={this.onCloseHandler}
                    />
                }

                {this.state.confirmState &&
                    <ConfirmModel
                        status={"delete"}
                        deleteName={this.state.selectedScheduleName}
                        confirmClicked={() => {
                            this.props.deleteSchedule(this.state.selectedScheduleId)
                            this.setState({
                                isFetching: true,
                                confirmState: false,
                                isOpen: false,
                                selectedScheduleId: '',
                                selectedScheduleName: ''
                            })
                        }}
                        cancelClicked={() => {
                            this.cancelClicked()
                        }}
                    />
                }
            </React.Fragment >
        );
    }
}

ManageScheduler.propTypes = {
    dispatch: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
    getScheduleListSuccess: SELECTORS.getScheduleListSuccess(),
    getScheduleListFailure: SELECTORS.getScheduleListFailure(),
    createUpdateScheduleSuccess: SELECTORS.createUpdateScheduleSuccess(),
    createUpdateScheduleError: SELECTORS.createUpdateScheduleError(),
    getScheduleByIdSuccess: SELECTORS.getScheduleByIdSuccess(),
    getScheduleByIdError: SELECTORS.getScheduleByIdError(),
    deleteScheduleSuccess: SELECTORS.deleteScheduleSuccess(),
    deleteScheduleError: SELECTORS.deleteScheduleError(),
    getAllPoliciesSuccess: SELECTORS.getAllPoliciesSuccess(),
    getAllPoliciesFailure: SELECTORS.getAllPoliciesFailure(),
    getAllReportsSuccess: SELECTORS.getAllReportsSuccess(),
    getAllReportsFailure: SELECTORS.getAllReportsFailure(),
    getAllFaaSSuccess: SELECTORS.getAllFaaSSuccess(),
    getAllFaaSFailure: SELECTORS.getAllFaaSFailure(),
    schedulerStateSuccess: SELECTORS.changeSchedulerStateSuccess(),
    schedulerStateFailure: SELECTORS.changeSchedulerStateFailure()
});

function mapDispatchToProps(dispatch) {
    return {
        dispatch,
        getScheduleList: () => dispatch(ACTIONS.getScheduleList()),
        createUpdateSchedule: (payload) => dispatch(ACTIONS.createUpdateSchedule(payload)),
        getScheduleById: (id) => dispatch(ACTIONS.getScheduleById(id)),
        deleteSchedule: (id) => dispatch(ACTIONS.deleteSchedule(id)),
        resetToInitialState: () => dispatch(ACTIONS.resetToInitialState()),
        getAllPolicies: () => dispatch(ACTIONS.getAllPolicies()),
        getAllReports: () => dispatch(ACTIONS.getAllReports()),
        getAllFaaS: () => dispatch(ACTIONS.getAllFaaS()),
        changeSchedulerState: (id, action) => dispatch(ACTIONS.changeSchedulerState(id, action))
    };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "manageScheduler", reducer });
const withSaga = injectSaga({ key: "manageScheduler", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(ManageScheduler);
