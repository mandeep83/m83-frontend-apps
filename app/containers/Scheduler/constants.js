/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/*
 *
 * ManageScheduler constants
 *
 */

export const GET_ALL_SCHEDULE = "app/ManageScheduler/GET_ALL_SCHEDULE";
export const GET_ALL_SCHEDULE_SUCCESS = "app/ManageScheduler/GET_ALL_SCHEDULE_SUCCESS";
export const GET_ALL_SCHEDULE_FAILURE = "app/ManageScheduler/GET_ALL_SCHEDULE_FAILURE";

export const CREATE_UPDATE_SCHEDULE = "app/ManageScheduler/CREATE_UPDATE_SCHEDULE";
export const CREATE_UPDATE_SCHEDULE_SUCCESS = "app/ManageScheduler/CREATE_UPDATE_SCHEDULE_SUCCESS";
export const CREATE_UPDATE_SCHEDULE_FAILURE = "app/ManageScheduler/CREATE_UPDATE_SCHEDULE_FAILURE";

export const GET_SCHEDULE_BY_ID = "app/ManageScheduler/GET_SCHEDULE_BY_ID";
export const GET_SCHEDULE_BY_ID_SUCCESS = "app/ManageScheduler/GET_SCHEDULE_BY_ID_SUCCESS";
export const GET_SCHEDULE_BY_ID_FAILURE = "app/ManageScheduler/GET_SCHEDULE_BY_ID_FAILURE";

export const DELETE_SCHEDULE = "app/ManageScheduler/DELETE_SCHEDULE";
export const DELETE_SCHEDULE_SUCCESS = "app/ManageScheduler/DELETE_SCHEDULE_SUCCESS";
export const DELETE_SCHEDULE_FAILURE = "app/ManageScheduler/DELETE_SCHEDULE_FAILURE";

export const RESET_TO_INITIAL_STATE = "app/ManageScheduler/RESET_TO_INITIAL_STATE";


export const GET_ALL_REPORTS = "app/ManageScheduler/GET_ALL_REPORTS";
export const GET_ALL_REPORTS_SUCCESS = "app/ManageScheduler/GET_ALL_REPORTS_SUCCESS";
export const GET_ALL_REPORTS_FAILURE = "app/ManageScheduler/GET_ALL_REPORTS_FAILURE";


export const GET_ALL_POLICIES = "app/ManageScheduler/GET_ALL_POLICIES";
export const GET_ALL_POLICIES_SUCCESS = "app/ManageScheduler/GET_ALL_POLICIES_SUCCESS";
export const GET_ALL_POLICIES_FAILURE = "app/ManageScheduler/GET_ALL_POLICIES_FAILURE";


export const GET_ALL_FAAS = "app/ManageScheduler/GET_ALL_FAAS";
export const GET_ALL_FAAS_SUCCESS = "app/ManageScheduler/GET_ALL_FAAS_SUCCESS";
export const GET_ALL_FAAS_FAILURE = "app/ManageScheduler/GET_ALL_FAAS_FAILURE";


export const CHANGE_SCHEDULER_STATE = "app/ManageScheduler/CHANGE_SCHEDULER_STATE";
export const CHANGE_SCHEDULER_STATE_SUCCESS = "app/ManageScheduler/CHANGE_SCHEDULER_STATE_SUCCESS";
export const CHANGE_SCHEDULER_STATE_FAILURE = "app/ManageScheduler/CHANGE_SCHEDULER_STATE_FAILURE";