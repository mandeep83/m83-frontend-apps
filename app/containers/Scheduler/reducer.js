/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/*
 *
 * ManageScheduler reducer
 *
 */

import { fromJS } from "immutable";
import * as CONSTANTS from "./constants";

export const initialState = fromJS({});

function manageSchedulerReducer(state = initialState, action) {
  switch (action.type) {
    case CONSTANTS.RESET_TO_INITIAL_STATE:
      return initialState;
    case CONSTANTS.GET_ALL_SCHEDULE_SUCCESS:
      return Object.assign({}, state, {
        getScheduleListSuccess: action.response
      });
    case CONSTANTS.GET_ALL_SCHEDULE_FAILURE:
      return Object.assign({}, state, {
        getScheduleListFailure: action.error
      });
    case CONSTANTS.CREATE_UPDATE_SCHEDULE_SUCCESS:
      return Object.assign({}, state, {
        createUpdateScheduleSuccess: action.response
      });
    case CONSTANTS.CREATE_UPDATE_SCHEDULE_FAILURE:
      return Object.assign({}, state, {
        createUpdateScheduleError: action.error
      });
    case CONSTANTS.GET_SCHEDULE_BY_ID_SUCCESS:
      return Object.assign({}, state, {
        getScheduleByIdSuccess: action.response
      });
    case CONSTANTS.GET_SCHEDULE_BY_ID_FAILURE:
      return Object.assign({}, state, {
        getScheduleByIdError: action.error
      });
    case CONSTANTS.DELETE_SCHEDULE_SUCCESS:
      return Object.assign({}, state, {
        deleteScheduleSuccess: action.response
      });
    case CONSTANTS.DELETE_SCHEDULE_FAILURE:
      return Object.assign({}, state, {
        deleteScheduleError: action.error
      });
    case CONSTANTS.GET_ALL_POLICIES_SUCCESS:
      return Object.assign({}, state, {
        getAllPoliciesSuccess: action.response
      });
    case CONSTANTS.GET_ALL_POLICIES_FAILURE:
      return Object.assign({}, state, {
        getAllPoliciesFailure: action.error
      });
    case CONSTANTS.GET_ALL_REPORTS_SUCCESS:
      return Object.assign({}, state, {
        getAllReportsSuccess: action.response
      });
    case CONSTANTS.GET_ALL_REPORTS_FAILURE:
      return Object.assign({}, state, {
        getAllReportsFailure: action.error
      });
    case CONSTANTS.GET_ALL_FAAS_SUCCESS:
      return Object.assign({}, state, {
        getAllFaaSSuccess: action.response
      });
    case CONSTANTS.GET_ALL_FAAS_FAILURE:
      return Object.assign({}, state, {
        getAllFaaSFailure: action.error
      });
    case CONSTANTS.CHANGE_SCHEDULER_STATE_SUCCESS:
      return Object.assign({}, state, {
        schedulerStateSuccess: action.response
      });
    case CONSTANTS.CHANGE_SCHEDULER_STATE_FAILURE:
      return Object.assign({}, state, {
        schedulerStateFailure: {
          error: action.error,
          id: action.addOns.id
        }
      });
    default:
      return state;
  }
}

export default manageSchedulerReducer;
