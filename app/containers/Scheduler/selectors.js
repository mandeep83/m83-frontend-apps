/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
 * Direct selector to the manageScheduler state domain
 */

const selectManageSchedulerDomain = state =>
  state.get("manageScheduler", initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by ManageScheduler
 */

export const getScheduleListSuccess = () => createSelector(selectManageSchedulerDomain, substate => substate.getScheduleListSuccess);
export const getScheduleListFailure = () => createSelector(selectManageSchedulerDomain, substate => substate.getScheduleListFailure);

export const createUpdateScheduleSuccess = () => createSelector(selectManageSchedulerDomain, substate => substate.createUpdateScheduleSuccess);
export const createUpdateScheduleError = () => createSelector(selectManageSchedulerDomain, substate => substate.createUpdateScheduleError);

export const getScheduleByIdSuccess = () => createSelector(selectManageSchedulerDomain, substate => substate.getScheduleByIdSuccess);
export const getScheduleByIdError = () => createSelector(selectManageSchedulerDomain, substate => substate.getScheduleByIdError);

export const deleteScheduleSuccess = () => createSelector(selectManageSchedulerDomain, substate => substate.deleteScheduleSuccess);
export const deleteScheduleError = () => createSelector(selectManageSchedulerDomain, substate => substate.deleteScheduleError);

export const getAllPoliciesSuccess = () => createSelector(selectManageSchedulerDomain, substate => substate.getAllPoliciesSuccess);
export const getAllPoliciesFailure = () => createSelector(selectManageSchedulerDomain, substate => substate.getAllPoliciesFailure);

export const getAllReportsSuccess = () => createSelector(selectManageSchedulerDomain, substate => substate.getAllReportsSuccess);
export const getAllReportsFailure = () => createSelector(selectManageSchedulerDomain, substate => substate.getAllReportsFailure);

export const getAllFaaSSuccess = () => createSelector(selectManageSchedulerDomain, substate => substate.getAllFaaSSuccess);
export const getAllFaaSFailure = () => createSelector(selectManageSchedulerDomain, substate => substate.getAllFaaSFailure);

export const changeSchedulerStateSuccess = () => createSelector(selectManageSchedulerDomain, substate => substate.schedulerStateSuccess);
export const changeSchedulerStateFailure = () => createSelector(selectManageSchedulerDomain, substate => substate.schedulerStateFailure);


export { selectManageSchedulerDomain };
