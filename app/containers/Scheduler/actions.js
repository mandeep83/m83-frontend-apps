/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/*
 *
 * ManageScheduler actions
 *
 */


import * as CONSTANTS from "./constants";

export function getScheduleList() {
  return {
    type: CONSTANTS.GET_ALL_SCHEDULE
  };
}

export function createUpdateSchedule(payload) {
  return {
    type: CONSTANTS.CREATE_UPDATE_SCHEDULE,
    payload
  };
}

export function getScheduleById(id) {
  return {
    type: CONSTANTS.GET_SCHEDULE_BY_ID,
    id
  };
}

export function deleteSchedule(id) {
  return {
    type: CONSTANTS.DELETE_SCHEDULE,
    id
  };
}


export function resetToInitialState() {
  return {
    type: CONSTANTS.RESET_TO_INITIAL_STATE,
  };
}

export function getAllReports() {
  return {
    type: CONSTANTS.GET_ALL_REPORTS
  };
}

export function getAllFaaS() {
  return {
    type: CONSTANTS.GET_ALL_FAAS
  };
}

export function getAllPolicies() {
  return {
    type: CONSTANTS.GET_ALL_POLICIES
  };
}

export function changeSchedulerState(id, action) {
  return {
    type: CONSTANTS.CHANGE_SCHEDULER_STATE,
    id, action
  };
}
