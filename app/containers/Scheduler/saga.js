/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

// import { take, call, put, select } from 'redux-saga/effects';

import { takeEvery } from 'redux-saga';
import * as CONSTANTS from './constants';

import { apiCallHandler } from '../../api';


export function* getScheduleListApiHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_ALL_SCHEDULE_SUCCESS, CONSTANTS.GET_ALL_SCHEDULE_FAILURE, 'getAllSchedule')];
}

export function* createUpdateScheduleApiHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.CREATE_UPDATE_SCHEDULE_SUCCESS, CONSTANTS.CREATE_UPDATE_SCHEDULE_FAILURE, 'createUpdateSchedule')];
}

export function* getScheduleByIdApiHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_SCHEDULE_BY_ID_SUCCESS, CONSTANTS.GET_SCHEDULE_BY_ID_FAILURE, 'getScheduleById')];
}

export function* deleteScheduleApiHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.DELETE_SCHEDULE_SUCCESS, CONSTANTS.DELETE_SCHEDULE_FAILURE, 'deleteSchedule')];
}

export function* getAllPoliciesApiHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_ALL_POLICIES_SUCCESS, CONSTANTS.GET_ALL_POLICIES_FAILURE, 'getAllPolicyData')];
}

export function* getAllReportsApiHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_ALL_REPORTS_SUCCESS, CONSTANTS.GET_ALL_REPORTS_FAILURE, 'getAllReports')];
}

export function* getAllFaaSApiHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_ALL_FAAS_SUCCESS, CONSTANTS.GET_ALL_FAAS_FAILURE, 'getAllFaaSForScheduler')];
}

export function* changeSchedulerStateHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.CHANGE_SCHEDULER_STATE_SUCCESS, CONSTANTS.CHANGE_SCHEDULER_STATE_FAILURE, 'changeSchedulerState')];
}

export function* watcherGetSchedulerList() {
  yield takeEvery(CONSTANTS.GET_ALL_SCHEDULE, getScheduleListApiHandlerAsync);
}

export function* watcherCreateUpdateSchedule() {
  yield takeEvery(CONSTANTS.CREATE_UPDATE_SCHEDULE, createUpdateScheduleApiHandlerAsync);
}

export function* watcherGetScheduleById() {
  yield takeEvery(CONSTANTS.GET_SCHEDULE_BY_ID, getScheduleByIdApiHandlerAsync);
}

export function* watcherDeleteSchedule() {
  yield takeEvery(CONSTANTS.DELETE_SCHEDULE, deleteScheduleApiHandlerAsync);
}

export function* watcherGetAllPolicies() {
  yield takeEvery(CONSTANTS.GET_ALL_POLICIES, getAllPoliciesApiHandlerAsync);
}

export function* watcherGetAllReports() {
  yield takeEvery(CONSTANTS.GET_ALL_REPORTS, getAllReportsApiHandlerAsync);
}

export function* watcherChangeSchedulerState() {
  yield takeEvery(CONSTANTS.CHANGE_SCHEDULER_STATE, changeSchedulerStateHandlerAsync);
}

export function* watcherGetAllFaaS() {
  yield takeEvery(CONSTANTS.GET_ALL_FAAS, getAllFaaSApiHandlerAsync);
}

export default function* rootSaga() {
  yield [
    watcherGetSchedulerList(),
    watcherCreateUpdateSchedule(),
    watcherGetScheduleById(),
    watcherDeleteSchedule(),
    watcherGetAllPolicies(),
    watcherGetAllReports(),
    watcherGetAllFaaS(),
    watcherChangeSchedulerState()
  ];
}
