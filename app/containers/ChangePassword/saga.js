/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { takeEvery } from 'redux-saga';

import * as CONSTANTS from "./constants";
import { apiCallHandler } from '../../api';

export function* changepasswordApiHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.CHANGE_PASSWORD_SUCCESS, CONSTANTS.CHANGE_PASSWORD_ERROR, 'changePassword')];
}

export function* getTenantDetailsApiHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_TENANT_DETAILS_SUCCESS, CONSTANTS.GET_TENANT_DETAILS_ERROR, 'getTenantDetails')];
}


export function* watcherChangePasswordRequest() {
  yield takeEvery(CONSTANTS.CHANGE_PASSWORD, changepasswordApiHandlerAsync);
}

export function* watcherGetTenantDetails() {
  yield takeEvery(CONSTANTS.GET_TENANT_DETAILS, getTenantDetailsApiHandlerAsync);
}

export default function* rootSaga() {
  yield [
    watcherChangePasswordRequest(),
    watcherGetTenantDetails(),
  ]
}

