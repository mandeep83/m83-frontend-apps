/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

export const DEFAULT_ACTION = 'app/ChangePassword/DEFAULT_ACTION';

export const CHANGE_PASSWORD = 'app/ChangePassword/CHANGE_PASSWORD';
export const CHANGE_PASSWORD_SUCCESS = 'app/ChangePassword/CHANGE_PASSWORD_SUCCESS';
export const CHANGE_PASSWORD_ERROR = 'app/ChangePassword/CHANGE_PASSWORD_ERROR';

export const GET_TENANT_DETAILS = 'app/ChangePassword/GET_TENANT_DETAILS';
export const GET_TENANT_DETAILS_SUCCESS = 'app/ChangePassword/GET_TENANT_DETAILS_SUCCESS';
export const GET_TENANT_DETAILS_ERROR = 'app/ChangePassword/GET_TENANT_DETAILS_ERROR';

export const RESET_TO_INTIAL_STATE = 'app/ChangePassword/RESET_TO_INTIAL_STATE';
