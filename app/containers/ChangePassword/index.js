/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Redirect } from 'react-router';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import {
    getChangePasswordSuccess, getChangePasswordError, getIsFetching,
    getTenantDetailsError, getTenantDetailsSuccess
} from './selectors';
import reducer from './reducer';
import saga from './saga';
import { changeHandler, resetToIntialState, getTenantDetails } from './actions';
import Loader from '../../components/Loader/Loadable'
import NotificationModal from '../../components/NotificationModal/Loadable'
import { Link } from "react-router-dom";

/* eslint-disable react/prefer-stateless-function */
export class ChangePassword extends React.Component {

    state = {
        isFetching: false,
        payload: {
            oldPassword: "",
            newPassword: "",
            confirmPassword: ""
        },
        tenantDetails: {},
        isFetchingTenantDetails: localStorage.tenantType != "MAKER"
    }


    componentDidMount() {
        if (localStorage.tenantType != "MAKER") {
            this.props.getTenantDetails()
        }
    }

    changePasswordHandler = (event) => {
        event.preventDefault();
        let payload = {};
        if (localStorage.isVerified === 'true') {
            payload = {
                newPassword: this.state.payload.newPassword,
                oldPassword: this.state.payload.oldPassword
            }
        } else {
            payload = {
                newPassword: this.state.payload.newPassword,
            }
        }
        this.setState({ isFetching: true }, () => { this.props.changeHandler(payload) });

    }

    formValidate = () => {
        const regex = /^(?=.*?[a-z])(?=.*[A-Z])(?=.*?[^\w\s]).{8,}$/
        if (!regex.test(this.state.payload.newPassword) || !regex.test(this.state.payload.confirmPassword) || this.passwordsMismatch()) {
            return true
        } else {
            return false
        }
    }

    inputChangehandler = (event) => {
        let payload = JSON.parse(JSON.stringify(this.state.payload))
        payload[event.target.id] = event.target.value
        this.setState({ payload })

    }


    componentWillReceiveProps(nextProps) {
        if (nextProps.changePasswordSuccess && nextProps.changePasswordSuccess !== this.props.changePasswordSuccess) {
            this.setState({
                modalType: 'success',
                openModal: true,
                message2: "Password changed successfully."
            });
        }

        if (nextProps.changePasswordError && nextProps.changePasswordError !== this.props.changePasswordError) {
            this.setState({
                modalType: 'error',
                openModal: true,
                message2: nextProps.changePasswordError,
                isFetching: false
            })
        }

        if (nextProps.tenantDetails && nextProps.tenantDetails !== this.props.tenantDetails) {
            this.setState({
                isFetchingTenantDetails: false,
                tenantDetails: nextProps.tenantDetails
            })
        }

        if (nextProps.tenantDetailsError && nextProps.tenantDetailsError !== this.props.tenantDetailsError) {
            this.setState({
                modalType: 'error',
                openModal: true,
                message2: nextProps.tenantDetailsError,
                isFetchingTenantDetails: false
            })
        }
    }

    onCloseHandler = () => {
        if (this.props.changePasswordSuccess) {
            this.setState({
                openModal: false,
                message2: ''
            }, () => {
                delete localStorage['token'];
                this.props.history.push('/login');
            });

        } else {
            this.setState({
                openModal: false,
                message2: ''
            });
        }
    }

    passwordsMismatch = () => {
        return this.state.payload.newPassword && this.state.payload.confirmPassword && this.state.payload.newPassword !== this.state.payload.confirmPassword && this.state.payload.newPassword && this.state.payload.confirmPassword && this.state.payload.newPassword !== this.state.payload.confirmPassword
    }

    getBorderColor = (id) => {
        const regex = /^(?=.*?[a-z])(?=.*[A-Z])(?=.*?[^\w\s]).{8,}$/
        let styles = { borderColor: 'red' }
        if (id === "newPassword") {
            if (this.state.payload.newPassword && !regex.test(this.state.payload.newPassword))
                return styles
            return null
        }
        else if (id === "confirmPassword") {
            if (this.state.payload.confirmPassword && !regex.test(this.state.payload.confirmPassword))
                return styles
            return null
        }
    }

    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>Change Password</title>
                    <meta name="description" content="M83-ChangePassword" />
                </Helmet>

                {localStorage.tenantType == "MAKER" ?
                    <div className="login-outer-box" id="makerLogin">
                        <div className="login-image-content">
                            <div className="login-image-box">
                                <img src="https://content.iot83.com/m83/tenant/welcome.png" />
                            </div>
                            <h1>Welcome to iFlex83</h1>
                            <h5>Connect . Configure . Visualize</h5>
                            <p>A powerful and cost effective path to create dynamic IoT Applications. ​</p>
                            <p>Fast new and legacy device on-boarding.</p>
                            <p>Few clicks and your dashboards ready.</p>
                            <p>Not a single line of code.</p>
                            <p>Security, reliability and scalability assured.</p>
                        </div>

                        <div className="login-form-wrapper">
                            <div className="login-logo">
                                <img src="https://content.iot83.com/m83/misc/iFlexLogo.png" />
                            </div>

                            <div className="login-form-box">
                                {this.state.isFetching ?
                                    <div className="login-form-loader">
                                        <div id="ld3">
                                            <div></div>
                                            <div></div>
                                            <div></div>
                                            <div></div>
                                        </div>
                                    </div> :
                                    <form onSubmit={this.changePasswordHandler}>
                                        <div className="login-form-text">
                                            <h5>Change Your Password</h5>
                                            <p>It must contain at least 1 lowercase character, 1 uppercase character, 1 numeric character, 1 special character. It must be of min 8 characters.</p>
                                        </div>

                                        {localStorage.isVerified === 'true' &&
                                            <div className="form-group">
                                                <input type="password" value={this.state.payload.oldPassword} id="oldPassword" placeholder="Existing Password"
                                                    className="form-control" autoFocus onChange={this.inputChangehandler} required />
                                                <span className="form-control-icon"><i className="fad fa-lock-alt"></i></span>
                                            </div>
                                        }

                                        <div className="form-group">
                                            <input type="password" className="form-control" style={this.getBorderColor("newPassword")} value={this.state.payload.newPassword} id="newPassword" placeholder="New Password"
                                                onChange={this.inputChangehandler} required />
                                            <span className="form-control-icon"><i className="fad fa-lock-alt"></i></span>
                                        </div>

                                        <div className="form-group">
                                            <input type="password" className="form-control" style={this.getBorderColor("confirmPassword")} value={this.state.payload.confirmPassword} id="confirmPassword" placeholder="Confirm Password"
                                                onChange={this.inputChangehandler} required />
                                            <span className="form-control-icon"><i className="fad fa-lock"></i></span>
                                        </div>

                                        <div className="form-group">
                                            <button type="submit" name="button" disabled={this.formValidate()} className="btn btn-primary">Change Password</button>
                                            {localStorage.isVerified === 'true' && <Link className="" to="/"><i className="fas fa-angle-double-left mr-r-5"></i>Back</Link>}
                                        </div>

                                        {this.state.errors && this.state.errors.matchError ? <div className="login-form-error">Both the password should be same.</div> : ""}
                                        {this.passwordsMismatch() && <div className="login-form-error">Passwords do not match.</div>}
                                    </form>
                                }
                            </div>
                        </div>
                    </div>
                    :
                    <div className="login-outer-box" id="flexLogin">
                        <div className="d-flex h-100">
                            <div className="flex-70 h-100">
                                <div className="login-outer-image">
                                    {this.state.isFetchingTenantDetails ?
                                        <i className="fad fa-spinner fa-spin"></i> :
                                        <img src={this.state.tenantDetails.brandingImageUrl ? this.state.tenantDetails.brandingImageUrl : "https://content.iot83.com/m83/misc/flexLoginImage_new.png"} />
                                    }
                                    {/*<div className="login-outer-content">
                                        <div className="mr-b-3">
                                            <p>A powerful path to dynamic applications</p>
                                            <p>Cost Effective IoT application  solutions</p>
                                            <p>Click - Through dashboard creation</p>
                                        </div>
                                        <div>
                                            <p>Security, reliability and scalability assured</p>
                                            <p>Low - Code workflow to "Add the Magic"</p>
                                            <p>Fast and new legacy device on-boarding.</p>
                                        </div>
                                    </div>*/}
                                </div>
                            </div>
    
                            <div className="flex-30 bg-white position-relative">
                                <div className="login-logo">
                                    {this.state.isFetchingTenantDetails ?
                                        <i className="fad fa-spinner text-theme fa-spin f-18"></i> :
                                        <img src={this.state.tenantDetails.logoImage ? this.state.tenantDetails.logoImage : "https://content.iot83.com/m83/misc/flexLogo.png"} />
                                    }
                                </div>
                                <div className="login-form-box" style={{minHeight: 390}}>
                                    <div className="login-form-text">
                                        <h1>Change Password</h1>
                                        <p>It must contain at least 1 lowercase character, 1 uppercase character, 1 numeric character, 1 special character. It must be of min 8 characters.</p>
                                    </div>
                                    {this.state.isFetching || this.state.isFetchingTenantDetails ?
                                        <div className="login-form-loader">
                                            <div id="ld3">
                                                <div></div>
                                                <div></div>
                                                <div></div>
                                                <div></div>
                                            </div>
                                        </div> :
                                        <form onSubmit={this.changePasswordHandler}>
                                            {localStorage.isVerified === 'true' &&
                                            <div className="form-group">
                                                <input type="password" value={this.state.payload.oldPassword} id="oldPassword" placeholder="Existing Password"
                                                       className="form-control" autoFocus onChange={this.inputChangehandler} required />
                                                <span className="form-control-icon"><i className="fad fa-lock-alt"></i></span>
                                            </div>
                                            }
                    
                                            <div className="form-group">
                                                <input type="password" value={this.state.payload.newPassword} id="newPassword" placeholder="New Password"
                                                       className="form-control" style={this.getBorderColor("newPassword")} onChange={this.inputChangehandler} required />
                                                <span className="form-control-icon"><i className="fad fa-lock-alt"></i></span>
                                            </div>
                    
                                            <div className="form-group">
                                                <input type="password" value={this.state.payload.confirmPassword} id="confirmPassword" placeholder="Confirm Password"
                                                       className="form-control" style={this.getBorderColor("confirmPassword")} onChange={this.inputChangehandler} required />
                                                <span className="form-control-icon"><i className="fad fa-lock"></i></span>
                                            </div>
                    
                                            <div className="form-group">
                                                <button type="submit" name="button" disabled={this.formValidate()} className="btn btn-primary">Change Password</button>
                                                {localStorage.isVerified === 'true' && <Link className="" to="/"><i className="fas fa-angle-double-left mr-r-5"></i>Back</Link>}
                                            </div>
                    
                                            {this.state.errors && this.state.errors.matchError ? <div className="login-form-error position-absolute">Both the password should be same.</div> : ""}
                                            {this.passwordsMismatch() && <div className="login-form-error position-absolute">Passwords do not match.</div>}
                                        </form>
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                }

                {   this.state.openModal &&
                    <NotificationModal
                        type={this.state.modalType}
                        message2={this.state.message2}
                        onCloseHandler={this.onCloseHandler}
                    />
                }
            </React.Fragment>
        );
    }
}

ChangePassword.propTypes = {
    dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
    changePasswordSuccess: getChangePasswordSuccess(),
    changePasswordError: getChangePasswordError(),
    tenantDetails: getTenantDetailsSuccess(),
    tenantDetailsError: getTenantDetailsError(),
});

function mapDispatchToProps(dispatch) {
    return {
        dispatch,
        changeHandler: (data) => dispatch(changeHandler(data)),
        getTenantDetails: () => dispatch(getTenantDetails()),
        resetToIntialState: () => dispatch(resetToIntialState()),
    };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'changePassword', reducer });
const withSaga = injectSaga({ key: 'changePassword', saga });

export default compose(
    withReducer,
    withSaga,
    withConnect,
)(ChangePassword);
