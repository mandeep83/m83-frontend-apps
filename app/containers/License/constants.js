/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/


export const GET_ALL_LICENSES = "app/License/GET_ALL_LICENSES";
export const GET_ALL_LICENSES_SUCCESS = "app/License/GET_ALL_LICENSES_SUCCESS";
export const GET_ALL_LICENSES_FAILURE = "app/License/GET_ALL_LICENSES_FAILURE";

export const GET_ALL_PRODUCTS = "app/License/GET_ALL_PRODUCTS";
export const GET_ALL_PRODUCTS_SUCCESS = "app/License/GET_ALL_PRODUCTS_SUCCESS";
export const GET_ALL_PRODUCTS_FAILURE = "app/License/GET_ALL_PRODUCTS_FAILURE";

export const SAVE_PACKAGE = "app/License/SAVE_PACKAGE";
export const SAVE_PACKAGE_SUCCESS = "app/License/SAVE_PACKAGE_SUCCESS";
export const SAVE_PACKAGE_FAILURE = "app/License/SAVE_PACKAGE_FAILURE";
