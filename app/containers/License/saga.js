/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import * as CONSTANTS from './constants';
import {apiCallHandler} from '../../api';
import {takeEvery} from 'redux-saga';


export function* getAllLicensesHandler(action) {
  yield[apiCallHandler(action, CONSTANTS.GET_ALL_LICENSES_SUCCESS, CONSTANTS.GET_ALL_LICENSES_FAILURE, 'getAllLicenses')];
}

export function* getAllProductsHandler(action) {
  yield[apiCallHandler(action, CONSTANTS.GET_ALL_PRODUCTS_SUCCESS, CONSTANTS.GET_ALL_PRODUCTS_FAILURE, 'getAllProducts')];
}

export function* savePackageHandler(action) {
  yield[apiCallHandler(action, CONSTANTS.SAVE_PACKAGE_SUCCESS, CONSTANTS.SAVE_PACKAGE_FAILURE, 'saveLicense')];
}


export function* watcherGetAllLicenses() {
  yield takeEvery(CONSTANTS.GET_ALL_LICENSES, getAllLicensesHandler);
}

export function* watcherGetAllProducts() {
  yield takeEvery(CONSTANTS.GET_ALL_PRODUCTS, getAllProductsHandler);
}

export function* watcherSavePackage() {
  yield takeEvery(CONSTANTS.SAVE_PACKAGE, savePackageHandler);
}

export default function* rootSaga() {
  yield [
      watcherGetAllLicenses(),
      watcherGetAllProducts(),
      watcherSavePackage(),
  ]
}

