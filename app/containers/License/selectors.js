/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
 * Direct selector to the license state domain
 */

const selectLicenseDomain = state => state.get("license", initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by License
 */

const allLicenses = () => createSelector(selectLicenseDomain, subState => subState.allLicenses);
const allLicensesFailure = () => createSelector(selectLicenseDomain, subState => subState.allLicensesFailure);

const allProducts = () => createSelector(selectLicenseDomain, subState => subState.allProducts);
const allProductsFailure = () => createSelector(selectLicenseDomain, subState => subState.allProductsFailure);

const saveLicense = () => createSelector(selectLicenseDomain, subState => subState.saveLicense);
const saveLicenseError = () => createSelector(selectLicenseDomain, subState => subState.saveLicenseError);


export {
    allLicenses, allLicensesFailure, allProducts, allProductsFailure, saveLicense, saveLicenseError
};
