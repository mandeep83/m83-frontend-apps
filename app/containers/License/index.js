/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { FormattedMessage } from "react-intl";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import { allLicenses, allLicensesFailure, allProducts, allProductsFailure, saveLicense, saveLicenseError } from "./selectors";
import { getAllLicenses, getAllProducts, savePackage } from "./actions";
import reducer from "./reducer";
import saga from "./saga";
import messages from "./messages";
import NotificationModal from '../../components/NotificationModal/Loadable'
import Loader from '../../components/Loader/Loadable';
import ListingTable from "../../components/ListingTable/Loadable";
import NoDataFoundMessage from "../../components/NoDataFoundMessage/Loadable";
import AddNewButton from "../../components/AddNewButton/Loadable";
import ReactTooltip from "react-tooltip";



/* eslint-disable react/prefer-stateless-function */
export class License extends React.Component {


    state = {
        isFetching: true,
        isAddOrEditLicense: false,
        toggleView: true,
        allLicenses: [],
        allProducts: [],
        payload: {
            amount: 0,
            attributeIds: [],
            currency: 'USD',
            description: '',
            menuIds: [],
            name: '',
            noOfDays: 0,
            productList: [],
            tenantCategory: "MSAAS",
            type: '',
            validity: 0
        }
    }

    componentWillMount() {
        this.props.getAllLicenses();
        this.props.getAllProducts();
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.allLicenses && nextProps.allLicenses !== this.props.allLicenses) {
            this.setState({
                allLicenses: nextProps.allLicenses,
                filteredList: nextProps.allLicenses,
                isFetching: false,
            })
        }

        if (nextProps.allLicensesFailure && nextProps.allLicensesFailure !== this.props.allLicensesFailure) {
            this.setState({
                type: 'error',
                isOpen: true,
                message2: nextProps.allLicensesFailure,
                isFetching: false
            })
        }
        if (nextProps.allProducts && nextProps.allProducts !== this.props.allProducts) {
            this.setState({
                allProducts: nextProps.allProducts
            })
        }

        if (nextProps.allLicensesFailure && nextProps.allLicensesFailure !== this.props.allLicensesFailure) {
            this.setState({
                type: 'error',
                isOpen: true,
                message2: nextProps.allLicensesFailure,
                isFetching: false
            })
        }
        if (nextProps.saveLicense && nextProps.saveLicense !== this.props.saveLicense) {
            let payload = {
                amount: 0,
                attributeIds: [],
                currency: 'USD',
                description: '',
                menuIds: [],
                name: '',
                noOfDays: 0,
                productList: [],
                tenantCategory: "MSAAS",
                type: '',
                validity: 0
            };
            let allProducts = this.state.allProducts;
            allProducts.map(product => {
                delete product.isChecked;
                delete product.count;
            })
            this.setState({
                payload,
                allProducts,
                isAddOrEditLicense: false,
                modalType: "success",
                isOpen: true,
                message2: nextProps.saveLicense.message,
            }, () => { this.props.getAllLicenses() });
        }

        if (nextProps.saveLicenseError && nextProps.saveLicenseError !== this.props.saveLicenseError) {
            this.setState({
                type: 'error',
                isOpen: true,
                message2: nextProps.saveLicenseError,
                isFetching: false
            })
        }
    }



    onCloseHandler = () => {
        this.setState({
            message2: '',
            isOpen: false,
            type: ''
        })
    }

    getColumns = () => {
        return [
            {
                Header: "Name",
                width: 20,
                accessor: "type",
                className: "text-theme",
                bold: true
            },
            {
                Header: "Description",
                width: 20,
                accessor: "description"
            },
            {
                Header: "Category",
                width: 20,
                accessor: "tenantCategory",
                cell: row =>
                    <h6 className={row.tenantCategory == "MAKER" ? "text-green" : "text-orange"}>{row.tenantCategory}</h6>
            },
            {
                Header: "Validity",
                width: 20,
                accessor: "numberOfDays",
                cell: row =>
                    <h6 className="alert alert-primary list-view-badge">{row.numberOfDays}</h6>
            },
            {
                Header: "Actions",
                width: 20,
                cell: (row) => (
                    <div className="button-group">
                        <button className="btn-transparent btn-transparent-cyan" data-tooltip data-tooltip-text="View" data-tooltip-place="bottom"><i className="far fa-info"></i></button>
                        <button className="btn-transparent btn-transparent-blue" data-tooltip data-tooltip-text="Edit" data-tooltip-place="bottom"><i className="far fa-pencil"></i></button>
                        <button className="btn-transparent btn-transparent-red" data-tooltip data-tooltip-text="Delete" data-tooltip-place="bottom"><i className="far fa-trash-alt"></i></button>
                    </div>
                )
            },
        ]
    }
    productChangeHandler = (event, id) => {
        let allProducts = this.state.allProducts;
        if (event.target.name === 'productCheckbox') {
            allProducts.filter(product => product.id === id)[0].isChecked = event.target.checked;
        } else {
            allProducts.filter(product => product.id === id)[0].count = event.target.value;
        }
        this.setState({
            allProducts
        })
    }

    onChangeHandler = (event) => {
        let payload = this.state.payload;
        payload[event.target.name] = event.target.value;
        this.setState({
            payload
        })
    }

    savePackage = () => {
        let payload = this.state.payload,
            allProducts = this.state.allProducts;
        allProducts.map(product => {
            if (product.isChecked) {
                payload.productList.push({
                    count: product.count,
                    id: product.id,
                    periodic: true,
                    visible: true
                })
            }
        });
        payload.productList.map(product => {
            payload.attributeIds.push(product.id);
        });
        this.setState({
            isFetching: true,
        }, () => { this.props.savePackage(payload) });

    }

    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>Manage License</title>
                    <meta name="description" content="M83-ManageLicense" />
                </Helmet>

                {this.state.isFetching ? <Loader /> :
                    <React.Fragment>
                        <header className="content-header d-flex">
                            <div className="flex-60">
                                <h6>Manage Licenses
                                    <span className="content-header-badge-group">
                                        <span className="content-header-badge-item">
                                            <span className="badge badge-pill badge-primary">{this.state.allLicenses.length > 0 && this.state.allLicenses.length}</span>
                                        </span>
                                    </span>
                                </h6>
                            </div>
                            <div className="flex-40 text-right">
                                <div className="content-header-group">
                                    <button className={this.state.toggleView == true ? "btn btn-light active" : "btn btn-light"} disabled={this.state.isFetching} data-tooltip="true" data-tooltip-text="List View" data-tooltip-place="bottom" onClick={() => this.setState({ toggleView: true })}><i className="fad fa-list-ul"></i></button>
                                    <button className={this.state.toggleView == false ? "btn btn-light active" : "btn btn-light"} disabled={this.state.isFetching} data-tooltip="true" data-tooltip-text="Card View" data-tooltip-place="bottom" onClick={() => this.setState({ toggleView: false })}><i className="fad fa-table"></i></button>
                                    {this.state.isAddOrEditLicense ?
                                        <button className="btn btn-light" data-tooltip data-tooltip-text="Back" data-tooltip-place="bottom" onClick={() => { this.setState({ isAddOrEditLicense: false }) }}><i className="fas fa-angle-left"></i></button>
                                        : <button className="btn btn-primary" data-tooltip data-tooltip-text="Add License" data-tooltip-place="bottom" onClick={() => { this.setState({ isAddOrEditLicense: true }) }}><i className="far fa-plus"></i></button>
                                    }
                                </div>
                            </div>
                        </header>

                        {this.state.isAddOrEditLicense ?
                            <div className="content-body">
                                <div className="form-content-wrapper form-content-wrapper-left">
                                    <div className="form-content-box">
                                        <div className="form-content-header">
                                            <p>Package Attributes</p>
                                        </div>
                                        <div className="form-content-body">
                                            <ul className="list-style-none d-flex custom-attribute-list">
                                                {this.state.allProducts.map((product, index) =>
                                                    <li key={index}>
                                                        <div className="form-group">
                                                            <label className="check-box">
                                                                <span className="check-text">{product.displayName}</span>
                                                                <input type="checkbox" checked={product.isChecked} name="productCheckbox" onChange={(e) => { this.productChangeHandler(e, product.id) }} />
                                                                <span className="check-mark"></span>
                                                            </label>
                                                            <input type="text" name={product.name} className="form-control" onChange={(e) => { this.productChangeHandler(e, product.id) }} value={product.count} />
                                                        </div>
                                                    </li>
                                                )}
                                            </ul>
                                        </div>
                                    </div>

                                    <div className="form-content-box">
                                        <div className="form-content-header">
                                            <p>Package Menu</p>
                                        </div>
                                        <div className="form-content-body">
                                            <ul className="list-style-none d-flex custom-menu-list">
                                                <li>
                                                    <div className="custom-menu-list-item active">
                                                        <button className="btn text-red"><i className="far fa-minus"></i></button>
                                                        <h6>Device Type </h6>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div className="custom-menu-list-item active">
                                                        <button className="btn text-red"><i className="far fa-minus"></i></button>
                                                        <h6>Devices</h6>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div className="custom-menu-list-item active">
                                                        <button className="btn text-red"><i className="far fa-minus"></i></button>
                                                        <h6>Data Attributes</h6>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div className="custom-menu-list-item">
                                                        <button className="btn text-green"><i className="far fa-plus"></i></button>
                                                        <h6>Device Groups</h6>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div className="custom-menu-list-item">
                                                        <button className="btn text-green"><i className="far fa-plus"></i></button>
                                                        <h6>Simulation</h6>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div className="custom-menu-list-item active">
                                                        <button className="btn text-red"><i className="far fa-minus"></i></button>
                                                        <h6>Pages</h6>
                                                        <p>Dashboard Studio</p>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div className="custom-menu-list-item active">
                                                        <button className="btn text-red"><i className="far fa-minus"></i></button>
                                                        <h6>Applications</h6>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div className="custom-menu-list-item">
                                                        <button className="btn text-green"><i className="far fa-plus"></i></button>
                                                        <h6>Databases</h6>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div className="custom-menu-list-item">
                                                        <button className="btn text-green"><i className="far fa-plus"></i></button>
                                                        <h6>FaaS</h6>
                                                        <p>Code Engine</p>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div className="custom-menu-list-item">
                                                        <button className="btn text-green"><i className="far fa-plus"></i></button>
                                                        <h6>API</h6>
                                                        <p>Code Engine</p>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div className="custom-menu-list-item">
                                                        <button className="btn text-green"><i className="far fa-plus"></i></button>
                                                        <h6>Templates</h6>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div className="custom-menu-list-item">
                                                        <button className="btn text-green"><i className="far fa-plus"></i></button>
                                                        <h6>Auditing</h6>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div className="custom-menu-list-item">
                                                        <button className="btn text-green"><i className="far fa-plus"></i></button>
                                                        <h6>ETL</h6>
                                                        <p>Transformations</p>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div className="custom-menu-list-item">
                                                        <button className="btn text-green"><i className="far fa-plus"></i></button>
                                                        <h6>JS Functions</h6>
                                                        <p>Transformations</p>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div className="custom-menu-list-item active">
                                                        <button className="btn text-red"><i className="far fa-minus"></i></button>
                                                        <h6>User</h6>
                                                        <p>IAM</p>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div className="custom-menu-list-item active">
                                                        <button className="btn text-red"><i className="far fa-minus"></i></button>
                                                        <h6>Roles</h6>
                                                        <p>IAM</p>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div className="custom-menu-list-item active">
                                                        <button className="btn text-red"><i className="far fa-minus"></i></button>
                                                        <h6>Groups</h6>
                                                        <p>IAM</p>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div className="form-info-wrapper form-info-wrapper-left">
                                    <div className="form-info-header">
                                        <h5>Basic Information</h5>
                                    </div>
                                    <div className="form-info-body form-info-body-adjust">
                                        <div className="form-group">
                                            <label className="form-group-label">Name :</label>
                                            <input type="text" id="name" name="name" placeholder="Name" className="form-control" value={this.state.payload.name} onChange={this.onChangeHandler} />
                                        </div>
                                        <div className="form-group">
                                            <label className="form-group-label">Description :</label>
                                            <input type="text" id="description" name="description" placeholder="Description" className="form-control" value={this.state.payload.description} onChange={this.onChangeHandler} />
                                        </div>
                                        <div className="form-group  ">
                                            <label className="form-group-label">Amount (In USD) :</label>
                                            <input type="text" id="amount" name="amount" placeholder="Amount" className="form-control" value={this.state.payload.amount} onChange={this.onChangeHandler} />
                                        </div>
                                        <div className="form-group ">
                                            <label className="form-group-label">Validity :</label>
                                            <input type="text" id="validity" name="validity" placeholder="Validity" className="form-control" value={this.state.payload.validity} onChange={this.onChangeHandler} />
                                        </div>
                                        <div className="form-group ">
                                            <label className="form-group-label">Type :</label>
                                            <select
                                                className="form-control"
                                                id="tenantCategory"
                                                name="tenantCategory"
                                                value={this.state.payload.tenantCategory}
                                                onChange={this.onChangeHandler}>
                                                <option value="MSAAS">M-SaaS</option>
                                                <option value="ESAAS">E-SaaS</option>
                                                <option value="CUSTOM">Custom</option>

                                            </select>
                                        </div>
                                    </div>
                                    <div className="form-info-footer">
                                        <button className="btn btn-light" onClick={() => { this.setState({ isAddOrEditLicense: false }) }}>cancel</button>
                                        <button className="btn btn-primary" onClick={this.savePackage}>Save</button>
                                    </div>
                                </div>
                            </div> :
                            <div className="content-body">
                                {this.state.allLicenses.length > 0 ?
                                    this.state.filteredList.length > 0 ?
                                        this.state.toggleView ?
                                            <ListingTable
                                                columns={this.getColumns()}
                                                data={this.state.filteredList}
                                            />
                                            :
                                            <ul className="card-view-list" id="deviceListCard">
                                                {this.state.filteredList.map((item, index) =>
                                                    <li key={index}>
                                                        <div className="card-view-box">
                                                            <div className="card-view-header pd-l-10">
                                                                <span className={item.tenantCategory == "MAKER" ? "alert alert-success" : "alert alert-danger"}>{item.tenantCategory}</span>
                                                                <div className="dropdown">
                                                                    <button className="btn-transparent btn-transparent-gray" type="button" data-toggle="dropdown">
                                                                        <i className="fas fa-ellipsis-v"></i>
                                                                    </button>
                                                                    <div className="dropdown-menu">
                                                                        <button className="dropdown-item"><i className="far fa-info"></i>View</button>
                                                                        <button className="dropdown-item"><i className="far fa-pencil"></i>Edit</button>
                                                                        <button className="dropdown-item"><i className="far fa-trash-alt"></i>Delete</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="card-view-body">
                                                                <div className="card-view-icon">
                                                                    <i className="fad fa-file-certificate text-dark-theme"></i>
                                                                </div>
                                                                <h6><i className="far fa-address-book"></i>{item.type ? item.type : "N/A"}</h6>
                                                                <p><i className="far fa-file-alt"></i>{item.description ? item.description : "N/A"}</p>
                                                                <p>
                                                                    <span className="badge badge-pill alert-primary">Validity - {item.numberOfDays} days</span>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </li>
                                                )}
                                            </ul>
                                        :
                                        <NoDataFoundMessage />
                                    :
                                    <AddNewButton
                                        text1="No licenses available"
                                        text2="You haven't created any license yet"
                                        imageIcon="addPage.png"
                                        createItemOnAddButtonClick={() => { this.setState({ isAddOrEditLicense: true }) }}
                                    />
                                }
                            </div>}
                    </React.Fragment>
                }

                {this.state.isOpen &&
                    <NotificationModal
                        type={this.state.type}
                        message2={this.state.message2}
                        onCloseHandler={this.onCloseHandler}
                    />
                }
            </React.Fragment>
        );
    }
}

License.propTypes = {
    dispatch: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
    allLicenses: allLicenses(),
    allLicensesFailure: allLicensesFailure(),
    allProducts: allProducts(),
    allProductsFailure: allProductsFailure(),
    saveLicense: saveLicense(),
    saveLicenseError: saveLicenseError(),
});

function mapDispatchToProps(dispatch) {
    return {
        dispatch,
        getAllLicenses: () => dispatch(getAllLicenses()),
        getAllProducts: () => dispatch(getAllProducts()),
        savePackage: (payload) => dispatch(savePackage(payload)),
    };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "license", reducer });
const withSaga = injectSaga({ key: "license", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(License);
