/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * ApplicationUser
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { FormattedMessage } from "react-intl";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import makeSelectApplicationUser from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import messages from "./messages";
import ReactSelect from "react-select";
import SlidingPane from 'react-sliding-pane';

/* eslint-disable react/prefer-stateless-function */
export class ApplicationUser extends React.Component {
  state = {
    selectedUser: '',
    count: 0,
    isFetching: true,
    payload: {
        email: '',
        firstName: '',
        lastName: '',
        mobile: '',
        roleId: null,
        assignedProjects: [],
        password: '',
        skipVerification: false
    },
    passwordMatched: true,
    roles: [],
    groups: [],
    usersList: {
        users: [],
        totalCount: 0
    },
    isFetchingUser: true,
    isChangingPassword: false,
    projects: [],
    assignedProjects: [],
    showAddOrEditUser: false,
    accessType: "",
    tableState: {
        activePageNumber: 1,
        pageSize: 5,
        filtered: [],
    },
    searchTerm: ''
  };
  handleAddNewUserClick = () => {
    let assignedProjects = JSON.parse(JSON.stringify(this.state.projects));
    assignedProjects.map(project => {
        project.projectAccess = project.projectAndAccessPermission;
        project.repoName = project.repoName && project.repoName !== '' ? project.repoName : null;
        project.projectAndAccessPermission = null;
        project.sourceControlOriginal = project.sourceControlEnabled
        project.sourceControlEnabled = null;
    });

    this.setState({
        assignedProjects,
        showAddOrEditUser: true,
        selectedUser: null,
        email: "",
        payload: {
            email: '',
            firstName: '',
            lastName: '',
            mobile: '',
            roleId: null,
            password: '',
            assignedProjects: [],
            skipVerification: false
        },
        errorMessage: null,
        accessType: "",
        isDefaultUser: false,
        saveButtonDisabled: false
    })
  }
  render() {
    return (
      <React.Fragment>
        <Helmet>
          <title>ApplicationUser</title>
          <meta name="description" content="Description of ApplicationUser" />
        </Helmet>
        <header className="content-header d-flex">
          <div className="flex-60">
              <h6>Application User</h6>
          </div>
          <div className="flex-40 text-right">
              <div className="content-header-group">
                  <div className="search-box">
                      <span className="search-icon"><i className="far fa-search"></i></span>
                      <input type="text" className="form-control" placeholder="Search..." />
                      <button className="search-button" ><i className="far fa-times"></i></button>
                  </div>
                  <button className="btn btn-light"><i className="fad fa-list-ul"></i></button>
                  <button className="btn btn-light" data-toggle="modal" data-target="#userDetail"><i className="fad fa-table"></i></button>
                  <button className="btn btn-primary" onClick={() => { this.handleAddNewUserClick() }}><i className="far fa-plus"></i></button>
              </div>
          </div>
        </header>
        <div className="content-body">
          <div className="list-view">
            <div className="list-view-header d-flex">
              <p className="flex-15">Name</p>
              <p className="flex-25">Email</p>
              <p className="flex-15">Mobile</p>
              <p className="flex-15">Role</p>
              <p className="flex-20">User group</p>
              <p className="flex-10">Actions</p>
            </div>
            <div className="list-view-body" id="dataItemDiv">
              <div className="list-view-item">
                <ul className="list-style-none d-flex" id="rowContainer">
                  <li className="flex-15">
                    <h6 className="text-theme"><strong>Device Details</strong></h6>
                  </li>
                  <li className="flex-25">
                    <h6 className="">utkarsh.chaturvedi@83incs.com</h6>
                  </li>
                  <li className="flex-15">
                    <h6></h6>
                  </li>
                  <li className="flex-15">
                    <span className="text-capitalize "><i className="f-12 mr-r-7 fad "></i></span>
                  </li>
                  <li className="flex-20">
                    <h6></h6>
                  </li>
                  <li className="flex-10">
                    <div className="button-group">
                      <button className="btn-transparent btn-transparent-cyan"><i className="far fa-pencil"></i></button>
                      <button className="btn-transparent btn-transparent-red"><i className="far fa-trash-alt"></i></button>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <SlidingPane
              className=''
              overlayClassName='sliding-form'
            closeIcon={<div></div>}
            isOpen={this.state.showAddOrEditUser || false}
            from='right'
            width='500px'
          >
          <div className="modal-content">
            <div className="modal-header">
              <h6 className="modal-title">Add New User
                  <button className="btn btn-light close" onClick={() => this.setState({ showAddOrEditUser: false, passwordMatched: true, errorMessage: '', email: "" })}>
                      <i className="far fa-angle-right"></i>
                  </button>
              </h6>
            </div>
            <div className="modal-body">
              <div className="d-flex">
                  <div className="flex-50 pd-r-10">
                    <div className="form-group">
                        <label className="form-group-label">First Name :</label>
                        <input type="text" name="firstName" className="form-control" pattern="^[a-zA-Z]([\w\$-]*[a-zA-Z0-9])?$" required autoFocus
                            value={this.state.payload.firstName} disabled={this.state.isDefaultUser} onChange={this.onChangeHandler}
                        />
                    </div>
                  </div>
                  <div className="flex-50 pd-l-10">
                    <div className="form-group">
                        <label className="form-group-label">Last Name :</label>
                        <input type="text" name="lastName" pattern="^[a-zA-Z]([\w\$-]*[a-zA-Z0-9])?$" className="form-control" required
                            value={this.state.payload.lastName} disabled={this.state.isDefaultUser} onChange={this.onChangeHandler}
                        />
                    </div>
                  </div>
              </div>
                  <div className="form-group">
                      <label className="form-group-label">Email :</label>
                      <input type="email" name="email" className="form-control" required
                          value={this.state.email} disabled={this.state.selectedUser && (this.state.payload.email === loggedInUser || this.state.isDefaultUser)} onChange={this.onChangeHandler}
                      />
                  </div>
              <div className="d-flex">
                  <div className="flex-50 pd-r-10">
                    <div className="form-group">
                        <label className="form-group-label">Mobile No :</label>
                        <input type="text" maxLength="10" name="mobile" disabled={this.state.isDefaultUser} className="form-control" required
                                value={this.state.payload.mobile} onChange={this.onChangeHandler}
                        />
                    </div>
                  </div>
                  <div className="flex-50 pd-l-10">
                    <div className="form-group">
                        <label className="form-group-label">Role :</label>
                        <ReactSelect
                            name="roles"
                            className="form-control-multi-select"
                        >
                        </ReactSelect>
                    </div>
                  </div>
              </div>
              <div className="form-group">
                  <label className="form-group-label">User Group :</label>
                  <ReactSelect
                      name="roles"
                      className="form-control-multi-select"
                  >
                  </ReactSelect>
              </div>
              <div className="form-group">
                  <label className={this.state.payload.roleId !== null && this.state.roles.filter(role => role.id === this.state.payload.roleId)[0].name !== 'ACCOUNT_ADMIN' ? "check-box" : "check-box check-box-disabled"} >
                      <span className="check-text">Do you want to skip email verification ?</span>
                      <input type="checkbox" name="skipVerification" disabled={this.state.payload.roleId === null || this.state.roles.filter(role => role.id === this.state.payload.roleId)[0].name === 'ACCOUNT_ADMIN'} checked={this.state.payload.skipVerification} value={this.state.payload.skipVerification} onChange={this.onChangeHandler} />
                      <span className="check-mark" />
                  </label>
              </div>
              <div className="form-group input-group">
                  <label className="form-group-label">Password :</label>
                  <div className="input-group">
                      <input type={this.state.passwordType ? "text" : "password"} pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$^+=!*()@%&]).{6,15}$"
                              name="password" className="form-control" required autoComplete="new-password"
                              onChange={this.onChangeHandler} value={this.state.payload.password}
                      />
                      <div className="input-group-append" onClick={() => this.setState((prevState) => { return { passwordType: !prevState.passwordType } })}>
                          <span className="input-group-text cursor-pointer"><i className={this.state.passwordType ? "far fa-eye-slash" : "far fa-eye"}></i></span>
                      </div>
                  </div>
              </div>
            </div>
            <div className="modal-footer justify-content-start">
              <button className="btn btn-primary" disabled={this.state.roles.length === 0 || this.state.saveButtonDisabled} className={"btn btn-success"} > {this.state.selectedUser ? "Update" : "Save"}</button>
              <button type="button" className="btn btn-dark" onClick={() => this.setState({ showAddOrEditUser: false, errorMessage: '', email: "" })}>Cancel</button>
            </div>
          </div>
        </SlidingPane>
      </React.Fragment>
    );
  }
}

ApplicationUser.propTypes = {
  dispatch: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
  applicationuser: makeSelectApplicationUser()
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

const withReducer = injectReducer({ key: "applicationUser", reducer });
const withSaga = injectSaga({ key: "applicationUser", saga });

export default compose(
  withReducer,
  withSaga,
  withConnect
)(ApplicationUser);
