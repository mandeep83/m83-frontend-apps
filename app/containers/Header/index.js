/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import { Link } from 'react-router-dom';
import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import * as SELECTORS from './selectors';
import * as ACTIONS from './actions';

import reducer from './reducer';
import saga from './saga';
import jwt_decode from "jwt-decode";
import MyActivities from '../MyActivities/Loadable';
import SlidingPane from 'react-sliding-pane';
import Modal from 'react-modal';
import ThemeSelection from '../../components/ThemeSelection/Loadable';
import cx from 'classnames';
import { getMiniLoader } from "../../commonUtils"
function getNavigation(navs) {
    let customNavs = navs.filter(nav => nav.menuType === "CUSTOM"),
        nonCustomNavs = navs.filter(nav => nav.menuType !== "CUSTOM");

    if (localStorage.role === 'ACCOUNT_ADMIN') {
        return [...nonCustomNavs];
    } else {
        let selectedApp = [...new Set(customNavs.map(nav => nav.customMenuName))][0];
        nonCustomNavs = nonCustomNavs.filter(nav => nav.navName !== "Help");
        customNavs = customNavs.filter(nav => nav.customMenuName === selectedApp);
        return [...nonCustomNavs, ...customNavs];
    }
}

/* eslint-disable react/prefer-stateless-function */
export class Header extends React.Component {
    state = {
        myActivities: false,
        themes: [],
        navigation: localStorage.tenantType === 'FLEX' ? getNavigation(this.props.sideNav) : this.props.sideNav,
        isAppView: false
    }

    componentWillMount() {
        if (this.props.sideNav.length > 0) {
            this.props.getAllCssThemes();
        }
        Modal.setAppElement('body');
        if (localStorage.role !== "SYSTEM_ADMIN") {
            this.props.getUserDetails();
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.logoutHandleSuccess && nextProps.logoutHandleSuccess !== this.props.logoutHandleSuccess) {
            let userId = localStorage.userId
            localStorage.clear()
            localStorage.setItem('userId', userId)
            this.props.history.push('/login')
        }

        if (nextProps.getCssThemeSuccess && nextProps.getCssThemeSuccess !== this.props.getCssThemeSuccess) {
            let themes = JSON.parse(JSON.stringify(nextProps.getCssThemeSuccess));
            themes.map(theme => {
                theme.isSelecting = false;
                return theme;
            })
            this.setState({ themes })
        }

        if (nextProps.getCssThemeError && nextProps.getCssThemeError !== this.props.getCssThemeError) {
            this.setState({
                isOpen: true,
                isFetching: false,
                message2: nextProps.getCssThemeError.message,
                modalType: "error"
            })
        }

        if (nextProps.getSelectedThemeSuccess && nextProps.getSelectedThemeSuccess !== this.props.getSelectedThemeSuccess) {
            this.addStyleTagDynamically(nextProps.getSelectedThemeSuccess);
        }

        if (nextProps.getSelectedThemeError && nextProps.getSelectedThemeError !== this.props.getSelectedThemeError) {
            this.setState({
                isOpen: true,
                isFetching: false,
                message2: nextProps.getSelectedThemeError.message,
                modalType: "error"
            })
        }

        if (nextProps.getUserDetailsError && nextProps.getUserDetailsError !== this.props.getUserDetailsError) {
            this.setState({
                isOpen: true,
                isFetching: false,
                message2: nextProps.getUserDetailsError.message,
                modalType: "error"
            })
        }
    }

    logoutHandler = () => {
        this.props.logoutHandler();
    }

    getNavClass(url, connectedRoutes) {
        if (Array.isArray(url)) {
            let isActive = url.some(child => {
                let isChildActive = window.location.pathname.includes(child.url),
                    isGrandChildActive = child.connectedRoutes && child.connectedRoutes.some(grandChild => window.location.pathname.includes(grandChild))
                return isChildActive || isGrandChildActive

            })
            if (isActive) {
                return 'active'
            }
        }
        if ((window.location.pathname.includes(url)) || connectedRoutes && (connectedRoutes.some(route => window.location.pathname.includes(route)))) {
            return 'active'
        }
    }

    getChildNavCLass = (subNav) => {
        let isActive = window.location.pathname.includes(subNav.url),
            isChildActive = subNav.connectedRoutes.some(route => window.location.pathname.includes(route));
        return (isActive || isChildActive) ? 'nav-item active' : 'nav-item'
    }

    applyTheme = (id) => {
        let themes = JSON.parse(JSON.stringify(this.state.themes));
        themes.map(theme => {
            if (theme.id == id)
                theme.isSelecting = true;
            return theme;
        })
        this.setState({ themes });
        this.props.getSelectTheme(id);
    }

    openThemePopUp = () => {
        localStorage.setItem("showThemePopUp", JSON.stringify(true))
    }

    addStyleTagDynamically = (id) => {
        let themes = JSON.parse(JSON.stringify(this.state.themes));
        if (document.getElementById("themeCSS")) {
            setTimeout(() => {
                let appliedLinkTag = document.getElementById("themeCSS")
                document.getElementsByTagName('head')[0].removeChild(appliedLinkTag);
            }, 2000);
        }

        var link = document.createElement('link');
        link.id = "themeCSS"
        link.rel = "stylesheet"
        link.href = window.API_URL + "api/unauth/user/theme.css" + `?userId=${localStorage.getItem("userId")}`;
        document.getElementsByTagName('head')[0].appendChild(link);

        themes.map(theme => {
            theme.isUserSelectedTheme = false
            if (theme.id == id) {
                theme.isSelecting = false;
                theme.isUserSelectedTheme = true
            }
            return theme;
        })
        this.setState({ themes });
    }

    toggle = () => {
        this.setState(prevstate => ({
            display: !prevstate.display,
        }), () => this.props.toggleSwitchSideBar(this.state.display));
    };

    applicationChangeHandler = (event) => {
        let selectedApp = event.target.value,
            customNavs = this.props.sideNav.filter(nav => nav.menuType === "CUSTOM"),
            nonCustomNavs = this.props.sideNav.filter(nav => nav.menuType !== "CUSTOM" && nav.navName !== "Help");

        customNavs = customNavs.filter(nav => nav.customMenuName === selectedApp);
        let navigation = [...nonCustomNavs, ...customNavs];

        this.setState({
            navigation,
        }, () => {
            if (window.location.pathname.includes('dashboard')) {
                let navUrl = navigation[0].subMenus.length > 0 ? navigation[0].subMenus[0].url : navigation[0].url;
                this.props.history.push(`/${navUrl}`)
            }
        });
    }

    getApplicationNavHTML = () => {
        let customNavs = this.props.sideNav.filter(nav => nav.menuType === "CUSTOM"),
            uniqueApps = [...new Set(customNavs.map(nav => { return { name: nav.customMenuName, icon: nav.fontAwesomeIconClass } }))],
            navigation = [];
        uniqueApps.map((app, index) => {
            navigation.push({
                navName: app.name,
                subMenus: [],
                connectedRoutes: [],
                fontAwesomeIconClass: app.icon,
                newTab: false,
                url: '',
                id: `app_${index}`
            })
        })
        customNavs.map(customMenu => {
            navigation.filter(nav => nav.navName === customMenu.customMenuName)[0].subMenus.push(customMenu);
        })
        return (
            <React.Fragment>
                { navigation.map(nav =>
                    nav.subMenus && nav.subMenus.length > 0 &&
                    <li key={nav.id}>
                        <a className="collapsed" data-toggle="collapse" data-target={"#collapse" + nav.id} onClick={(e) => { nav.enabled || localStorage.role === "SYSTEM_ADMIN" ? null : e.preventDefault() }}>
                            {this.state.display ?
                                <span data-tooltip data-tooltip-text={nav.navName} data-tooltip-place="right">
                                    <i className={window.location.pathname.split('/')[1] === 'page' ? nav.fontAwesomeIconClass : `fad fa-${nav.fontAwesomeIconClass}`}></i>
                                </span> :
                                <span>
                                    <i className={window.location.pathname.split('/')[1] === 'page' ? nav.fontAwesomeIconClass : `fad fa-${nav.fontAwesomeIconClass}`}></i>
                                </span>
                            }
                            <strong>{nav.navName}</strong>
                            <i className="fas fa-angle-down side-nav-collapse"></i>
                        </a>

                        <div className="collapse" id={"collapse" + nav.id}>
                            <div className="side-nav-sub-menu">
                                <ul className="list-style-none">
                                    {nav.subMenus.map(subNav =>
                                        subNav.subMenus.length === 0 ?
                                            subNav.enabled ? subNav.newTab ?
                                                <li key={subNav.id} onClick={() => this.setState({ myActivities: false })}>
                                                    <Link to="" className={this.getChildNavCLass(subNav)} onClick={(e) => {
                                                        e.preventDefault();
                                                        if (subNav.enabled || localStorage.role === "SYSTEM_ADMIN") {
                                                            window.open(window.location.origin + '/' + subNav.url, "_blank")
                                                        }
                                                    }}>
                                                        {this.state.display ?
                                                            <span data-tooltip data-tooltip-text={subNav.navName} data-tooltip-place="right">
                                                                <i className={(window.location.pathname.split('/')[1] === 'page' ? subNav.fontAwesomeIconClass : `fad fa-${subNav.fontAwesomeIconClass}`)}></i>
                                                            </span> :
                                                            <span>
                                                                <i className={(window.location.pathname.split('/')[1] === 'page' ? subNav.fontAwesomeIconClass : `fad fa-${subNav.fontAwesomeIconClass}`)}></i>
                                                            </span>
                                                        }
                                                        <strong>{subNav.navName}</strong>
                                                    </Link>
                                                </li> :
                                                subNav.url ?
                                                    <li key={subNav.id} onClick={() => this.setState({ myActivities: false })}>
                                                        <Link to={"/" + subNav.url} className={this.getChildNavCLass(subNav)} onClick={(e) => { subNav.enabled || localStorage.role === "SYSTEM_ADMIN" ? null : e.preventDefault() }}>
                                                            {this.state.display ?
                                                                <span data-tooltip data-tooltip-text={subNav.navName} data-tooltip-place="right">
                                                                    <i className={(window.location.pathname.split('/')[1] === 'page' ? subNav.fontAwesomeIconClass : `fad fa-${subNav.fontAwesomeIconClass}`)}></i>
                                                                </span> :
                                                                <span>
                                                                    <i className={(window.location.pathname.split('/')[1] === 'page' ? subNav.fontAwesomeIconClass : `fad fa-${subNav.fontAwesomeIconClass}`)}></i>
                                                                </span>
                                                            }
                                                            <strong>{subNav.navName}</strong>
                                                        </Link>
                                                    </li> :
                                                    <li key={subNav.id} onClick={() => this.setState({ myActivities: false })}>
                                                        <Link to={"/" + subNav.url} className={this.getChildNavCLass(subNav)} onClick={(e) => { e.preventDefault() }}>
                                                            {this.state.display ?
                                                                <span data-tooltip data-tooltip-text={subNav.navName} data-tooltip-place="right">
                                                                    <i className={(window.location.pathname.split('/')[1] === 'page' ? subNav.fontAwesomeIconClass : `fad fa-${subNav.fontAwesomeIconClass}`)}></i>
                                                                </span> :
                                                                <span>
                                                                    <i className={(window.location.pathname.split('/')[1] === 'page' ? subNav.fontAwesomeIconClass : `fad fa-${subNav.fontAwesomeIconClass}`)}></i>
                                                                </span>
                                                            }
                                                            <strong>{subNav.navName}</strong>
                                                        </Link>
                                                    </li>
                                                :
                                                null
                                            : <li key={subNav.id}>
                                                <a className="collapsed" data-toggle="collapse" data-target={`#collapseSubSubMenu_${subNav.id}`}>
                                                    {this.state.display ?
                                                        <span data-tooltip data-tooltip-text={subNav.navName} data-tooltip-place="right">
                                                            <i className={(window.location.pathname.split('/')[1] === 'page' ? subNav.fontAwesomeIconClass : `fad fa-${subNav.fontAwesomeIconClass}`)}></i>
                                                        </span> :
                                                        <span>
                                                            <i className={(window.location.pathname.split('/')[1] === 'page' ? subNav.fontAwesomeIconClass : `fad fa-${subNav.fontAwesomeIconClass}`)}></i>
                                                        </span>
                                                    }
                                                    <strong>{subNav.navName}</strong>
                                                    <i className="fas fa-angle-down side-nav-collapse"></i>
                                                </a>
                                                <div className="collapse" id={`collapseSubSubMenu_${subNav.id}`}>
                                                    <div className="side-nav-sub-menu side-nav-sub-inner-menu">
                                                        <ul className="list-style-none">
                                                            {subNav.subMenus.map(subSubMenu =>
                                                                <li key={subSubMenu.id}>
                                                                    <Link to={"/" + subSubMenu.url} className={this.getChildNavCLass(subSubMenu)}>
                                                                        {this.state.display ?
                                                                            <span data-tooltip data-tooltip-text={subSubMenu.navName} data-tooltip-place="right">
                                                                                <i className={(window.location.pathname.split('/')[1] === 'page' ? subSubMenu.fontAwesomeIconClass : `fad fa-${subSubMenu.fontAwesomeIconClass}`)}></i>
                                                                            </span> :
                                                                            <span>
                                                                                <i className={(window.location.pathname.split('/')[1] === 'page' ? subSubMenu.fontAwesomeIconClass : `fad fa-${subSubMenu.fontAwesomeIconClass}`)}></i>
                                                                            </span>
                                                                        }
                                                                        <strong>{subSubMenu.navName}</strong>
                                                                    </Link>
                                                                </li>
                                                            )}
                                                        </ul>
                                                    </div>
                                                </div>
                                            </li>
                                    )}
                                </ul>
                            </div>
                        </div>
                    </li>
                )}
            </React.Fragment>
        )
    }

    getApplications = (navs) => {
        let nav = navs.filter(nav => nav.menuType === "CUSTOM"),
            apps = [...new Set(nav.map(nav => nav.customMenuName))];
        return apps;
    }

    getFlexNavHtml = (mapData) => {
        return (
            <React.Fragment>
                {
                    mapData.map(nav =>
                        nav.subMenus && nav.subMenus.length > 0 ?
                            <li key={nav.id}>
                                <a className="collapsed" data-toggle="collapse" data-target={"#collapse" + nav.id} onClick={(e) => { nav.enabled || localStorage.role === "SYSTEM_ADMIN" ? null : e.preventDefault() }}>
                                    {this.state.display ?
                                        <span data-tooltip data-tooltip-text={nav.navName} data-tooltip-place="right">
                                            <i className={window.location.pathname.split('/')[1] === 'page' ? nav.fontAwesomeIconClass : `fad fa-${nav.fontAwesomeIconClass}`}></i>
                                        </span> :
                                        <span>
                                            <i className={window.location.pathname.split('/')[1] === 'page' ? nav.fontAwesomeIconClass : `fad fa-${nav.fontAwesomeIconClass}`}></i>
                                        </span>
                                    }
                                    <strong>{nav.navName}</strong>
                                    <i className="fas fa-angle-down side-nav-collapse"></i>
                                </a>

                                <div className="collapse" id={"collapse" + nav.id}>
                                    <div className="side-nav-sub-menu">
                                        <ul className="list-style-none">
                                            {nav.subMenus.map(subNav => {
                                                return (
                                                    subNav.enabled ? subNav.newTab ?
                                                        <li key={subNav.id} onClick={() => this.setState({ myActivities: false })}>
                                                            <Link to="" className={this.getChildNavCLass(subNav)} onClick={(e) => {
                                                                e.preventDefault();
                                                                if (subNav.enabled || localStorage.role === "SYSTEM_ADMIN") {
                                                                    let url = window.location.origin + "/" + subNav.url;
                                                                    window.open(url, "_blank")
                                                                }
                                                            }}>
                                                                {this.state.display ?
                                                                    <span data-tooltip data-tooltip-text={subNav.navName} data-tooltip-place="right">
                                                                        <i className={(window.location.pathname.split('/')[1] === 'page' ? subNav.fontAwesomeIconClass : `fad fa-${subNav.fontAwesomeIconClass}`)}></i>
                                                                    </span> :
                                                                    <span>
                                                                        <i className={(window.location.pathname.split('/')[1] === 'page' ? subNav.fontAwesomeIconClass : `fad fa-${subNav.fontAwesomeIconClass}`)}></i>
                                                                    </span>
                                                                }
                                                                <strong>{subNav.navName}</strong>
                                                            </Link>
                                                        </li> :
                                                        subNav.url ?
                                                            <li key={subNav.id} onClick={() => this.setState({ myActivities: false })}>
                                                                <Link to={"/" + subNav.url} className={this.getChildNavCLass(subNav)} onClick={(e) => { subNav.enabled || localStorage.role === "SYSTEM_ADMIN" ? null : e.preventDefault() }}>
                                                                    {this.state.display ?
                                                                        <span data-tooltip data-tooltip-text={subNav.navName} data-tooltip-place="right">
                                                                            <i className={(window.location.pathname.split('/')[1] === 'page' ? subNav.fontAwesomeIconClass : `fad fa-${subNav.fontAwesomeIconClass}`)}></i>
                                                                        </span> :
                                                                        <span>
                                                                            <i className={(window.location.pathname.split('/')[1] === 'page' ? subNav.fontAwesomeIconClass : `fad fa-${subNav.fontAwesomeIconClass}`)}></i>
                                                                        </span>
                                                                    }
                                                                    <strong>{subNav.navName}</strong>
                                                                </Link>
                                                            </li> :
                                                            <li key={subNav.id} onClick={() => this.setState({ myActivities: false })}>
                                                                <Link to={"/" + subNav.url} className={this.getChildNavCLass(subNav)} onClick={(e) => { e.preventDefault() }}>
                                                                    {this.state.display ?
                                                                        <span data-tooltip data-tooltip-text={subNav.navName} data-tooltip-place="right">
                                                                            <i className={(window.location.pathname.split('/')[1] === 'page' ? subNav.fontAwesomeIconClass : `fad fa-${subNav.fontAwesomeIconClass}`)}></i>
                                                                        </span> :
                                                                        <span>
                                                                            <i className={(window.location.pathname.split('/')[1] === 'page' ? subNav.fontAwesomeIconClass : `fad fa-${subNav.fontAwesomeIconClass}`)}></i>
                                                                        </span>
                                                                    }
                                                                    <strong>{subNav.navName}</strong>
                                                                </Link>
                                                            </li>
                                                        :
                                                        null
                                                )
                                            })}
                                        </ul>
                                    </div>
                                </div>
                            </li> :
                            nav.newTab ?
                                <li key={nav.id} onClick={() => this.setState({ myActivities: false })}>
                                    <a href={window.location.origin + '/' + `${nav.url}`} target="_blank" className={this.getNavClass(nav.url, nav.connectedRoutes)}>
                                        {this.state.display ?
                                            <span data-tooltip data-tooltip-text={this.state.display ? nav.navName : ""} data-tooltip-place="right">
                                                <i className={window.location.pathname.split('/')[1] === 'page' ? nav.fontAwesomeIconClass : `fad fa-${nav.fontAwesomeIconClass}`}></i>
                                            </span> :
                                            <span>
                                                <i className={window.location.pathname.split('/')[1] === 'page' ? nav.fontAwesomeIconClass : `fad fa-${nav.fontAwesomeIconClass}`}></i>
                                            </span>
                                        }
                                        <strong>{nav.navName}</strong>
                                    </a>
                                </li> :
                                nav.url != null ?
                                    <li key={nav.id} onClick={() => this.setState({ myActivities: false })}>
                                        <Link to={"/" + nav.url} className={this.getNavClass(nav.url, nav.connectedRoutes)} onClick={(e) => { nav.enabled || localStorage.role === "SYSTEM_ADMIN" ? null : e.preventDefault() }}>
                                            {this.state.display ?
                                                <span data-tooltip data-tooltip-text={this.state.display ? nav.navName : ""} data-tooltip-place="right">
                                                    <i className={window.location.pathname.split('/')[1] === 'page' ? nav.fontAwesomeIconClass : `fad fa-${nav.fontAwesomeIconClass}`}></i>
                                                </span> :
                                                <span>
                                                    <i className={window.location.pathname.split('/')[1] === 'page' ? nav.fontAwesomeIconClass : `fad fa-${nav.fontAwesomeIconClass}`}></i>
                                                </span>
                                            }
                                            <strong>{nav.navName}</strong>
                                        </Link>
                                    </li>
                                    : null
                    )
                }
            </React.Fragment>)
    }

    render() {
        let token;
        let firstName;
        let lastName;
        let userDetails = this.props.getUserDetailsSuccess || {}
        if (localStorage.token && localStorage['Username']) {
            token = localStorage.token;
            firstName = localStorage['Username'].split(' ')[0];
            lastName = localStorage['Username'].split(' ')[1];
        }

        let showApplicationsMenu = localStorage.tenantType === 'FLEX' && localStorage.role === 'ACCOUNT_ADMIN' && this.getApplications(this.props.sideNav).length > 0 ? true : false;
        return (
            <React.Fragment>
                <nav className={cx('side-nav-wrapper', { 'side-nav-wrapper-collapse': this.state.display, })}>
                    <div className="side-nav-menu">
                        <div className="side-nav-menu-header">
                            <div className="side-nav-menu-logo">
                                {localStorage.tenantLogoURL ? <img src={localStorage.tenantLogoURL} /> :
                                    this.state.display ?
                                        <img src={localStorage.tenantType == "MAKER" ? "https://content.iot83.com/m83/misc/iFlexLogoCollapse.png" : "https://content.iot83.com/m83/misc/flexLogoCollapse.png"} /> :
                                        <img src={localStorage.tenantType == "MAKER" ? "https://content.iot83.com/m83/misc/iFlexLogoWhite.png" : "https://content.iot83.com/m83/misc/flexLogoWhite.png"} />
                                }
                            </div>

                            <button className="btn-transparent btn-transparent-blue" onClick={this.toggle} >
                                <i className={this.state.display ? "fas fa-angle-right" : "fas fa-angle-left"}></i>
                            </button>
                        </div>

                        <ul className="side-nav-menu-list list-style-none">
                            {localStorage.tenantType === 'FLEX' && localStorage.role !== 'ACCOUNT_ADMIN' &&
                                this.getApplications(this.props.sideNav).length > 1 &&
                                <select className="side-nav-menu-list-header cursor-pointer text-initial" onChange={this.applicationChangeHandler}>
                                    {this.getApplications(this.props.sideNav).map((application, index) =>
                                        <option key={index} value={application}>{application}</option>
                                    )}
                                </select>
                            }

                            {Boolean(localStorage.tenantType === 'FLEX' && localStorage.role === 'ACCOUNT_ADMIN' && this.state.navigation.length) && <div className="side-nav-menu-list-header">No Code</div>}
                            {this.getFlexNavHtml(this.state.navigation.slice(0, 5))}

                            {Boolean(localStorage.tenantType === 'FLEX' && localStorage.role === 'ACCOUNT_ADMIN' && this.state.navigation.length) && <div className="side-nav-menu-list-header">Full Code</div>}
                            {this.getFlexNavHtml(this.state.navigation.slice(5, this.state.navigation.length))}

                            {showApplicationsMenu &&
                                <React.Fragment>
                                    <div className="side-nav-menu-list-header">My Web Apps</div>
                                    {this.getApplicationNavHTML()}
                                </React.Fragment>
                            }
                        </ul>

                        <div className="side-nav-menu-footer">
                            <div className="dropdown">
                                <button className="dropdown-toggle" data-toggle="dropdown" id="admin">
                                    {this.props.isLoadingUserDetails ?
                                        <span className="dropdown-initials"><i className="far fa-spinner fa-spin f-16"></i></span> :
                                        <span className="dropdown-initials">
                                            {userDetails.imageUrl ? <img src={userDetails.imageUrl} /> :
                                                firstName ? lastName ? (firstName.charAt(0).toUpperCase() + lastName.charAt(0).toUpperCase()) : firstName.substring(0, 2).toUpperCase() : lastName ? lastName.substring(0, 2).toUpperCase() : "AD"}
                                        </span>
                                    }
                                    {localStorage['Username']}
                                </button>
                                <div className="dropdown-menu">
                                    <div className="dropdown-menu-header">
                                        <div className="dropdown-initials m-auto">{firstName ? lastName ? (firstName.charAt(0).toUpperCase() + lastName.charAt(0).toUpperCase()) : firstName.substring(0, 2).toUpperCase() : lastName ? lastName.substring(0, 2).toUpperCase() : "AD"}</div>
                                        <h6>{localStorage['Username']}</h6>
                                        <p>{token && jwt_decode(token).role}</p>
                                    </div>
                                    <div className="dropdown-menu-body pt-3">
                                        {localStorage.tenantType == "MAKER" && localStorage.role === "DEVELOPER" &&
                                            <Link to="/myAccount" >
                                                Account Details
                                            </Link>
                                        }
                                        {localStorage.tenantType == "FLEX" && localStorage.role === "ACCOUNT_ADMIN" &&
                                            <Link to="/myPlan" >
                                                Account Details
                                            </Link>
                                        }
                                        {(localStorage.tenantType == "FLEX" || localStorage.tenantType == "MAKER" && this.state.themes.length > 0) &&
                                            <Link to="#" onClick={() => this.openThemePopUp()}>
                                                Change Theme
                                            </Link>
                                        }
                                        {localStorage.tenantType == "MAKER" && localStorage.role === "ACCOUNT_ADMIN" &&
                                            <Link to="/projects">
                                                All Projects
                                            </Link>
                                        }
                                        {localStorage.role !== "SYSTEM_ADMIN" &&
                                            <Link onClick={(e) => { token && !jwt_decode(token).internalUser && e.preventDefault() }} to="/changePassword">
                                                Change Password
                                            </Link>
                                        }
                                        {localStorage.tenantType == "FLEX" && jwt_decode(localStorage["token"]).defaultAccountAdmin &&
                                            <Link to="/settings" >
                                                Settings
                                            </Link>
                                        }
                                        <button className="btn btn-light" onClick={this.logoutHandler} type="button">Logout</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </nav>

                {/* theme selection box */}
                <ThemeSelection
                    show={localStorage.getItem("showThemePopUp")}
                    applyTheme={this.applyTheme}
                    themes={this.state.themes}
                />
                {/* end theme selection box */}

                {/* my activities list */}
                <SlidingPane
                    className='some-custom-class reactSlidingPane'
                    overlayClassName='some-custom-overlay-class activityLogOverlay slidingFormOverlay '
                    closeIcon={<div></div>}
                    isOpen={this.state.myActivities}
                    from='right'
                    width='400px'
                    onRequestClose={() => this.setState({ myActivities: false })}
                >
                    <MyActivities getActivityBox={(element) => this.setState({ activityBox: element })} onClose={() => this.setState((prevState) => ({ myActivities: !prevState.myActivities }))} />
                </SlidingPane>
                {/* end my activities list */}

            </React.Fragment >
        );
    }
}

Header.propTypes = {
    dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
    logoutHandleSuccess: SELECTORS.logoutHandleSuccess(),
    logoutHandleFailure: SELECTORS.logoutHandleFailure(),
    getCssThemeSuccess: SELECTORS.getThemeSuccess(),
    getCssThemeError: SELECTORS.getThemeError(),
    getSelectedThemeSuccess: SELECTORS.selectedThemeSuccess(),
    getSelectedThemeError: SELECTORS.selectedThemeError(),
    getUserDetailsSuccess: SELECTORS.getUserDetailsSuccess(),
    getUserDetailsError: SELECTORS.getUserDetailsError(),
    isLoadingUserDetails: SELECTORS.isLoadingUserDetails(),
});

function mapDispatchToProps(dispatch) {
    return {
        dispatch,
        logoutHandler: () => dispatch(ACTIONS.logoutHandler()),
        getAllCssThemes: () => dispatch(ACTIONS.getAllCssThemes()),
        getSelectTheme: (id) => dispatch(ACTIONS.getSelectTheme(id)),
        getUserDetails: () => dispatch(ACTIONS.getUserDetails(localStorage.getItem("userId")))
    };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'header', reducer });
const withSaga = injectSaga({ key: 'header', saga });

export default compose(
    withReducer,
    withSaga,
    withConnect,
)(Header);
