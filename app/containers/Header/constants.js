/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

export const DEFAULT_ACTION = 'app/Header/DEFAULT_ACTION';

export const LOGOUT_HANDLE = 'app/Header/LOGOUT_HANDLE';
export const LOGOUT_HANDLE_SUCCESS = 'app/Header/LOGOUT_HANDLE_SUCCESS';
export const LOGOUT_HANDLE_ERROR = 'app/Header/LOGOUT_HANDLE_ERROR';

export const GET_CSS_THEMES = 'app/Header/GET_CSS_THEMES';
export const GET_CSS_THEMES_SUCCESS = 'app/Header/GET_CSS_THEMES_SUCCESS';
export const GET_CSS_THEMES_ERROR ="app/Header/GET_CSS_THEMES_ERROR";

export const SELECTED_THEME = 'app/Header/SELECTED_THEME';
export const SELECTED_THEME_SUCCESS = 'app/Header/SELECTED_THEME_SUCCESS';
export const SELECTED_THEME_ERROR ="app/Header/SELECTED_THEME_ERROR";

export const GET_USER_DETAILS = 'app/Header/GET_USER_DETAILS';
export const GET_USER_DETAILS_SUCCESS = 'app/Header/GET_USER_DETAILS_SUCCESS';
export const GET_USER_DETAILS_ERROR ="app/Header/GET_USER_DETAILS_ERROR";

