/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/
import * as CONSTANTS from './constants';
import { apiCallHandler } from '../../api';
import { takeEvery } from 'redux-saga';
import { call, put } from 'redux-saga/effects';


export function* logoutHandler(action) {
  yield [apiCallHandler(action, CONSTANTS.LOGOUT_HANDLE_SUCCESS, CONSTANTS.LOGOUT_HANDLE_ERROR, 'logout')];
}

export function* watcherLogoutHandler() {
  yield takeEvery(CONSTANTS.LOGOUT_HANDLE, logoutHandler);
}

export function* getThemes(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_CSS_THEMES_SUCCESS, CONSTANTS.GET_CSS_THEMES_ERROR, 'getCssThemes')]
}

export function* watcherGetAllCssThemes() {
  yield takeEvery(CONSTANTS.GET_CSS_THEMES, getThemes)
}

export function* selectedTheme(action) {
  yield [apiCallHandler(action, CONSTANTS.SELECTED_THEME_SUCCESS, CONSTANTS.SELECTED_THEME_ERROR, 'getSelectedTheme')]
}

export function* getUserDetails(action) {
  yield put({ type: 'isLoadingUserDetails' })
  yield [apiCallHandler(action, CONSTANTS.GET_USER_DETAILS_SUCCESS, CONSTANTS.GET_USER_DETAILS_ERROR, 'getUserDetailsById')]
}

export function* watcherSelectedCssTheme() {
  yield takeEvery(CONSTANTS.SELECTED_THEME, selectedTheme)
}

export function* watcherGetUserDetails() {
  yield takeEvery(CONSTANTS.GET_USER_DETAILS, getUserDetails)
}

export default function* rootSaga() {
  yield [watcherLogoutHandler(),
  watcherGetAllCssThemes(),
  watcherSelectedCssTheme(),
  watcherGetUserDetails(),
  ];
}