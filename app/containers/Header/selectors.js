/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the header state domain
 */

const selectHeaderDomain = state => state.get('header', initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by Header
 */

export const logoutHandleSuccess = () => createSelector(selectHeaderDomain, substate => substate.logoutHandler);
export const logoutHandleFailure = () => createSelector(selectHeaderDomain, substate => substate.logoutHandlerFailure);

export const getThemeSuccess = () => createSelector(selectHeaderDomain, substate => substate.getThemeSuccess);
export const getThemeError = () => createSelector(selectHeaderDomain, substate => substate.getThemeError);

export const selectedThemeSuccess = () => createSelector(selectHeaderDomain, substate => substate.selectedThemeSuccess);
export const selectedThemeError = () => createSelector(selectHeaderDomain, substate => substate.selectedThemeError);

export const getUserDetailsSuccess = () => createSelector(selectHeaderDomain, substate => substate.getUserDetailsSuccess);
export const getUserDetailsError = () => createSelector(selectHeaderDomain, substate => substate.getUserDetailsError);
export const isLoadingUserDetails = () => createSelector(selectHeaderDomain, substate => substate.isLoadingUserDetails);
