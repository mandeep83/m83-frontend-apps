/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import * as CONSTANTS from './constants';

export function logoutHandler() {
  return {
    type: CONSTANTS.LOGOUT_HANDLE
  };
}

export function getAllCssThemes() {
  return {
    type: CONSTANTS.GET_CSS_THEMES,
  }
}

export function getSelectTheme(id) {
  return {
    type: CONSTANTS.SELECTED_THEME,
    id
  }
}

export function getUserDetails(id) {
  return {
    type: CONSTANTS.GET_USER_DETAILS,
    id,
  }
}