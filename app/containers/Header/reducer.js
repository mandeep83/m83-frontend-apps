/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { fromJS } from 'immutable';
import * as CONSTANTS from './constants';

export const initialState = fromJS({});

function headerReducer(state = initialState, action) {

  switch (action.type) {
    case CONSTANTS.LOGOUT_HANDLE_SUCCESS:
      return Object.assign({}, state, {
        'logoutHandler': { response: action.response, time: Date.now() },
      })
    case CONSTANTS.LOGOUT_HANDLE_ERROR:
      return Object.assign({}, state, {
        'logoutHandlerFailure': action.error,
      })
    case CONSTANTS.GET_CSS_THEMES_SUCCESS:
      return Object.assign({}, state, {
        getThemeSuccess: action.response
      })
    case CONSTANTS.GET_CSS_THEMES_ERROR:
      return Object.assign({}, state, {
        getThemeError: action.error
      })
    case CONSTANTS.SELECTED_THEME_SUCCESS:
      return Object.assign({}, state, {
        selectedThemeSuccess: action.response
      })
    case CONSTANTS.SELECTED_THEME_ERROR:
      return Object.assign({}, state, {
        selectedThemeError: action.error
      })
    case CONSTANTS.GET_USER_DETAILS_SUCCESS:
      return Object.assign({}, state, {
        isLoadingUserDetails: false,
        getUserDetailsSuccess: action.response
      })
    case CONSTANTS.GET_USER_DETAILS_ERROR:
      return Object.assign({}, state, {
        isLoadingUserDetails: false,
        getUserDetailsError: action.error
      })
    case "isLoadingUserDetails":
      return Object.assign({}, state, {
        isLoadingUserDetails: true
      })
    case "updateUserDisplayPicture":
      return Object.assign({}, state, {
        getUserDetailsSuccess: { ...(state.getUserDetailsSuccess || {}), imageUrl: action.imageUrl }
      })
    default:
      return state;
  }
}

export default headerReducer;
