/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * ManageImages
 *
 */

import React from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {Helmet} from "react-helmet";
import {FormattedMessage} from "react-intl";
import {createStructuredSelector} from "reselect";
import {compose} from "redux";
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import {allSelectors as SELECTORS} from "./selectors";
import {allActions as ACTIONS} from './actions'
import reducer from "./reducer";
import saga from "./saga";
import messages from "./messages";
import ReactTooltip from "react-tooltip";
import SlidingPane from "react-sliding-pane";
import cloneDeep from 'lodash/cloneDeep';
import Loader from "../../components/Loader/Loadable";
import { getNotificationObj ,getInitials} from "../../commonUtils";
import ConfirmModel from '../../components/ConfirmModel/Loadable'

/* eslint-disable react/prefer-stateless-function */
const IMAGE_TYPES = [
    {
        displayName: "Logo",
        name: "LOGOS"
    },
    {
        displayName: "Project",
        name: "PROJECT_IMAGES"
    },
    {
        displayName: "Application",
        name: "APP_IMAGES"
    },
    {
        displayName: "Temporary",
        name: "TEMP"
    },
]

let initialPayload = {
    fileType: "LOGOS",
    file: null
}

export class ManageImages extends React.Component {
    state= {
        payload: cloneDeep(initialPayload),
        allImages: [
            {
                name : "Sample Image",
                url : "https://image.flaticon.com/icons/svg/2899/2899181.svg",
                uploadedBy : "N/A",
                uploadedAt : "N/A",
            }
        ],
        isFetching: true
    }

    payloadChangeHandler = ({ currentTarget }) => {
        let _this= this
        let payload = cloneDeep(this.state.payload),
        id = currentTarget.id,
        value = id === "file" ? currentTarget.files[0] : currentTarget.value;
        if(id === "file"){
            var reader = new FileReader();
            reader.onload = function (e) {
                _this.setState({
                    imageUrl : e.target.result
                })
            }
            reader.readAsDataURL(currentTarget.files[0]);
        }
        payload[id] = value
        this.setState({ payload })
    };

    submitHandler = ()=> {
        this.setState({
            isFetching : true,
        },()=> this.props.uploadImage(this.state.payload))
    }

    componentDidMount(){
        this.props.getAllImages()
    }

    static getDerivedStateFromProps(nextProps, state) {
        if(nextProps.getAllImagesSuccess){
            return {
                isFetching: false,
                allImages: nextProps.getAllImagesSuccess.response
            }
        }
        if(nextProps.deleteImageSuccess){
            let allImages = state.allImages.filter(image => image.url !== nextProps.deleteImageSuccess.response.fileUrl)
            return {
                allImages,
                ...getNotificationObj(nextProps[propName].response.message, "success")
            }
        }
        if(nextProps.uploadImageSuccess){
            nextProps.getAllImages()
            return {...getNotificationObj(nextProps.uploadImageSuccess.response.message, "success"), isFetching: true, payload: cloneDeep(initialPayload), uploadImageModal: false}
        }
        if(nextProps.uploadImageFailure){
            return {...getNotificationObj(nextProps.uploadImageFailure.error, "error")}
        }
        if (Object.keys(SELECTORS).filter(prop => prop.toLowerCase().includes("success")).some(prop => nextProps[prop])) {
            let propName = Object.keys(SELECTORS).find(props => nextProps[props]);
            return getNotificationObj(nextProps[propName].response.message, "success")
        }
        if (Object.keys(SELECTORS).filter(prop => prop.toLowerCase().includes("failure")).some(prop => nextProps[prop])) {
            let propName = Object.keys(SELECTORS).find(props => nextProps[props]);
            return getNotificationObj(nextProps[propName].error, "error")
        }

        return null
    }

    componentDidUpdate(prevProps, prevState) {
        if (Object.keys(SELECTORS).some(prop => this.props[prop])) {
            this.props.resetToInitialState()
        }
    }

    onCloseHandler = () => {
        this.setState({
            isOpen: false,
            modalType: "",
            message2: "",
        })
    }

    deleteImageHandlder = () => {
        let imageToBeDeleted = this.state.imageToBeDeleted;
        this.setState({
            isFetching: true,
            confirmState: false,
            isOpen: false,
            imageToBeDeleted : "",
        },() => this.props.deleteImage(imageToBeDeleted));
    }

    toggleConfirmModal = (imageToBeDeleted, confirmState) => {
        this.setState({
            imageToBeDeleted,
            confirmState,
        });
    }

    handleImagePreview = ( previewImageName, previewImageUrl ) => {
        this.setState({
            previewImageName,
            previewImageUrl,
        })
    }

    closeUploadModal = ()=> {
        this.setState({
            uploadImageModal: false,
            payload: cloneDeep(initialPayload)
        })
    }

    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>ManageImages</title>
                    <meta name="description" content="M83-ManageImages"/>
                </Helmet>

                <div className="pageBreadcrumb">
                    <div className="row">
                        <div className="col-8">
                            <p><span>Manage Images {this.state.allImages.length > 0 && `(${this.state.allImages.length})`}</span></p>
                        </div>
                        <div className="col-4 text-right">
                            <div className="flex h-100 justify-content-end align-items-center">
                                {this.state.allImages.length > 0 &&
                                    <button type="button" name="button" className="btn btn-primary" onClick={() => this.setState({ uploadImageModal: true})}>
                                        <span className="btn-primary-icon"><i className="far fa-plus" /></span>
                                    </button>
                                }
                            </div>
                        </div>
                    </div>
                </div>

                {this.state.isFetching ? <Loader/> : 
                    <React.Fragment>
                        {this.state.allImages.length ?
                            <div className="outerBox">
                                <ul className="pageList">
                                    {this.state.allImages.map(image =>
                                        <li key={image.url}>
                                            <div className="pageListBox cursor-pointer" >
                                                <div className="pageListBoxImage" data-toggle="modal" data-target="#imageModal">
                                                    <img src="https://content.iot83.com/m83/other/browser.png" />
                                                </div>
                                                <div className="pageListBoxContent" data-toggle="modal" data-target="#imageModal" onClick={() => this.handleImagePreview(image.name, image.url)}>
                                                    <h5>{image.name}</h5>
                                                    <h6><strong>URL :</strong>{image.url}</h6>
                                                    <h6>
                                                        <strong>Uploaded By :</strong>
                                                        <span className="pageListBoxInitial" data-tip data-for="createdBy">{getInitials(image.uploadedBy)}</span>
                                                        <ReactTooltip id="createdBy" place="bottom" type="dark">
                                                            <div className="tooltipText"><p>{image.uploadedBy}</p></div>
                                                        </ReactTooltip>
                                                    </h6>
                                                </div>
                                                <div className="pageListBoxFooter">
                                                    <p><strong>Uploaded At: </strong> {image.uploadedAt}</p>
                                                    <div className="dropdown pageListDropdown">
                                                        <button type="button" className="btn dropdown-toggle" data-toggle="dropdown">
                                                            <i className="far fa-ellipsis-v"></i>
                                                        </button>
                                                        <ul className="dropdown-menu pageListDropdownMenu" >
                                                            <li data-toggle="modal" data-target="#imageModal" onClick={() => this.handleImagePreview(image.name, image.url)}><i className="far fa-eye"></i>View</li>
                                                            <li onClick={()=> this.toggleConfirmModal(image.url, true)}><i className="far fa-trash-alt"></i>Delete</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    )}
                                </ul>
                            </div> :
                            <div className="contentAddBox">
                                <div className="contentAddBoxDetail">
                                    <div className="contentAddBoxImage">
                                        <img src="https://content.iot83.com/m83/other/addProject.png" />
                                    </div>
                                    <h6>You have not uploaded any images yet.</h6>
                                    <button className="btn btn-primary" onClick={() => this.setState({ uploadImageModal: true})}>
                                        <i className="far fa-plus"></i>Add new
                                    </button>
                                </div>
                            </div>
                        }
                    </React.Fragment>
                }

                {/* view image modal */}
                <div className="modal fade animated slideInDown" role="dialog" id="imageModal">
                    <div className="modal-dialog modal-lg modal-dialog-centered">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h6 className="modal-title">Image Name - <span>{this.state.previewImageName}</span>
                                    <button className="close" data-dismiss="modal" onClick={() => this.setState({payload: cloneDeep(initialPayload)})}><i className="far fa-times"></i></button>
                                </h6>
                            </div>
                            <div className="modal-body">
                                <div className="imagePreview">
                                    <img src={this.state.previewImageUrl} />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/* view image modal */}

                {/* add new image modal */}
                {this.state.uploadImageModal &&
                <div className="modal d-block animated slideInDown" role="dialog" id="addImageModal">
                    <div className="modal-dialog modal-lg modal-dialog-centered">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h6 className="modal-title">Add New Image
                                    <button className="close" onClick={this.closeUploadModal}><i className="far fa-times"></i></button>
                                </h6>
                            </div>
                            <div className="modal-body">
                                <div className="form-group">
                                    <select className="form-control" value={this.state.payload.fileType} id="fileType" onChange={this.payloadChangeHandler} >
                                        {IMAGE_TYPES.map((image, index) => {
                                            return <option value={image.name} key={index}>{image.displayName}</option>
                                        })}
                                    </select>
                                    <label className="form-group-label">Type: </label>
                                </div>
                                <div className="fileUploadBox" style={{height: 250}}>
                                    {this.state.payload.file ?
                                        <div className="fileUploadPreview">
                                            <img src={this.state.imageUrl} />
                                            <div className="fileUploadOverlay">
                                                <input type="file" id="file" onChange={this.payloadChangeHandler} />
                                                <h6><i className="fal fa-image" />Update </h6>
                                            </div>
                                        </div> :
                                        <div className="fileUploadForm">
                                            <input type="file" id="file" name="tenantLogo" onChange={this.payloadChangeHandler} required />
                                            <p>Click to upload image...!</p>
                                        </div>
                                    }
                                </div>
                                {/*<div className="alert alert-primary m-0">*/}
                                {/*    <p className="m-0">Uploaded URL here.....</p>*/}
                                {/*</div    >*/}
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-success" onClick={this.submitHandler}>Upload</button>
                                <button type="button" className="btn btn-dark" onClick={this.closeUploadModal} >Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
                }
                {/* end add new image modal */}

                {this.state.isOpen &&
                    <NotificationModal
                        type={this.state.modalType}
                        message2={this.state.message2}
                        onCloseHandler={this.onCloseHandler}
                    />
                }

                {this.state.confirmState &&
                    <ConfirmModel
                        status={"delete"}
                        deleteName= "the selected image"
                        confirmClicked={this.deleteImageHandlder}
                        cancelClicked={()=> this.toggleConfirmModal()}
                    />
                }
            </React.Fragment>
        );
    }
}

ManageImages.propTypes = {
    dispatch: PropTypes.func.isRequired
};

let allSelectors = {}
Object.entries(SELECTORS).map(([key,value]) => {
    allSelectors[key] = value()
})

const mapStateToProps = createStructuredSelector(allSelectors)

function mapDispatchToProps(dispatch) {
    let allActions = {dispatch}
    Object.entries(ACTIONS).map(([key,value]) => {
        allActions[key] = (...args)=> dispatch(value(...args))
    })
    return allActions
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({key: "manageImages", reducer});
const withSaga = injectSaga({key: "manageImages", saga});

export default compose(
    withReducer,
    withSaga,
    withConnect
)(ManageImages);
