/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */
export const RESET_TO_INITIAL_STATE = "app/ManageImages/RESET_TO_INITIAL_STATE";

export const UPLOAD_IMAGE = {
    action : 'app/ManageImages/UPLOAD_IMAGE',
    success: "app/ManageImages/UPLOAD_IMAGE_SUCCESS",
    failure: "app/ManageImages/UPLOAD_IMAGE_FAILURE",
    urlKey: "uploadFile",
    successKey: "uploadImageSuccess",
    failureKey: "uploadImageFailure",
    actionName: "uploadImage",
    actionArguments : ["payload"]
}

export const DELETE_IMAGE = {
    action : 'app/ManageImages/DELETE_IMAGE',
    success: "app/ManageImages/DELETE_IMAGE_SUCCESS",
    failure: "app/ManageImages/DELETE_IMAGE_FAILURE",
    urlKey: "deleteFile",
    successKey: "deleteImageSuccess",
    failureKey: "deleteImageFailure",
    actionName: "deleteImage",
    actionArguments : ["fileUrl"]
}

export const GET_ALL_IMAGES = {
    action : 'app/ManageImages/GET_ALL_IMAGES',
    success: "app/ManageImages/GET_ALL_IMAGES_SUCCESS",
    failure: "app/ManageImages/GET_ALL_IMAGES_FAILURE",
    urlKey: "getAllImages",
    successKey: "getAllImagesSuccess",
    failureKey: "getAllImagesFailure",
    actionName: "getAllImages",
}
