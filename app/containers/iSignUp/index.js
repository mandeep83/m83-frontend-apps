/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * iSignUp
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import * as SELECTORS from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import Loader from "../../components/Loader";
import cloneDeep from 'lodash/cloneDeep';
import * as ACTIONS from "./actions"
import NotificationModal from '../../components/NotificationModal/Loadable'
import PhoneInput from "react-phone-input-2";
import 'react-phone-input-2/lib/style.css';
import ReactTooltip from "react-tooltip";
import ClientCaptcha from "react-client-captcha";

let interval = undefined

const getCarouselArr = (totalLength, carouselItemsCount) => {
    let carouselsCount = Math.ceil(totalLength / carouselItemsCount),
        carouselItemsArr = [...Array(carouselsCount)].map((el, index) => {
            let startIndex = index === 0 ? 0 : index * carouselItemsCount;
            return ({
                startIndex,
                endIndex: startIndex + carouselItemsCount,
            })
        })
    return carouselItemsArr
}

/* eslint-disable react/prefer-stateless-function */
export class iSignUp extends React.Component {
    state = {
        allProjects: [],
        isFetching: true,
        isShowForm: true,
        payload: {
            email: '',
            firstName: '',
            lastName: '',
            mobile: '',
            activationKey: '',
            otp: '',
            userProjectRequest: {}
        },
        selectedProject: {},
        carouselItemsArr: [],
        paymentModal: false,
        otpCount: 0,
        features: [{
            name: "device_types",
            displayName: "Device Type(s)",
            showDescription: false,
        }, {
            name: "no_of_devices",
            displayName: "Device(s)",
            showDescription: false,
        }, {
            name: "device_groups",
            displayName: "Device Group(s)",
            showDescription: false,
        }, {
            name: "attributeKeys",
            displayName: "Device Attribute(s)",
            showDescription: false,
        }, {
            name: "events",
            displayName: "Event(s)",
            showDescription: true,
        }, {
            name: "sms",
            displayName: "SMS",
            showDescription: false,
        }, {
            name: "email",
            displayName: "Email(s)",
            showDescription: false,
        }, {
            name: "webhooks",
            displayName: "Webhook(s)",
            showDescription: true,
        }, {
            name: "rpc",
            displayName: "Control(s)",
            showDescription: true,
        }, {
            name: "retention",
            displayName: "Retention",
            showDescription: true,
        }
        ],
        givenCaptchaCode: '',
        createTimeInterval: () => this.createIntervalForButton(),
        timeSeconds: 60,
    }

    componentDidMount() {
        this.props.getTenantDetails();
    }

    componentWillUnmount() {
        clearInterval(interval);
    }

    createIntervalForButton = () => {
        interval = setInterval(() => {
            if ((this.state.timeSeconds - 1) > 0) {
                this.setState(prevState => ({
                    timeSeconds: prevState.timeSeconds - 1
                }))
            } else {
                clearInterval(interval)
                interval = undefined
                this.setState({
                    timeSeconds: 60
                })
            }
        }, 1000)
    }

    static getDerivedStateFromProps(nextProps, state, prevState) {
        if (nextProps.getTenantDetailsSuccess) {
            let allProjects = nextProps.getTenantDetailsSuccess.projects;
            allProjects[0].packages.map(packageObj => {
                let featureList = [];
                state.features.map(feature => {
                    let featureObj = packageObj.productList.filter(product => product.name === feature.name)[0];
                    featureObj.displayName = feature.displayName;
                    featureObj.showDescription = feature.showDescription;
                    if (feature.name === 'retention') {
                        featureObj.count = featureObj.count / 30 >= 12 ? `${featureObj.count / (30 * 12)} Year` : `${featureObj.count / (30)} Month(s)`
                    }
                    featureList.push(featureObj);
                })
                packageObj.featureList = featureList;
            })
            let carouselItemsArr = getCarouselArr(allProjects.length, 3)
            return {
                carouselItemsArr,
                allProjects,
                isFetching: false,
                selectedProject: allProjects.length > 0 ? allProjects[0] : {},
                enableActivationKey: nextProps.getTenantDetailsSuccess.enableActivationKey,
                enableOtp: nextProps.getTenantDetailsSuccess.enableOtp
            }
        }

        if (nextProps.onRegisterSuccess) {
            nextProps.onRegisterSuccess.isTrialUser ? nextProps.history.push("login?registration=true") : window.open(nextProps.onRegisterSuccess.url, "_self");
        }

        if (nextProps.getOtpSuccess) {
            let { message, otpCount } = nextProps.getOtpSuccess
            if (otpCount < 2)
                state.createTimeInterval();
            return {
                modalType: "success",
                isOpen: true,
                message2: message,
                isLoading: false,
                otpCount
            }
        }

        if (nextProps.getOtpFailure) {
            let otpCount = state.otpCount
            if (nextProps.getOtpFailure == "OTP Limit reached for this Phone Number") {
                otpCount = 3
            }
            return {
                isLoading: false,
                modalType: "error",
                isOpen: true,
                message2: nextProps.getOtpFailure,
                otpCount
            }
        }

        if (Object.keys(SELECTORS).filter(prop => prop.toLowerCase().includes("failure")).some(prop => nextProps[prop])) {
            let propName = Object.keys(SELECTORS).find(props => nextProps[props])
            return {
                ...prevState,
                isFetching: false,
                isOpen: true,
                message2: nextProps[propName],
                modalType: "error",
            }
        }
        return null
    }

    onSubmitHandler = (event) => {
        event.preventDefault();
        this.setState({
            isShowForm: false
        })
    }

    inputChangeHandler = ({ currentTarget }) => {
        let payload = cloneDeep(this.state.payload);
        if (currentTarget.id === 'firstName' || currentTarget.id === 'lastName') {
            if (/^[A-Za-z]+$/.test(currentTarget.value) || /^$/.test(currentTarget.value))
                payload[currentTarget.id] = currentTarget.value;
        }
        else
            payload[currentTarget.id] = currentTarget.value;
        this.setState({
            payload,
        });
    }

    componentDidUpdate(prevProps, prevState) {
        if (Object.keys(SELECTORS).some(prop => this.props[prop])) {
            this.props.resetToInitialState()
        }
    }

    packageSelectHandler = ({ id, subscriptionPrice }) => {
        let payload = cloneDeep(this.state.payload);
        payload.userProjectRequest = {
            projectId: this.state.selectedProject.id,
            packageId: id
        }
        payload.activationKey = '';
        payload.otp = '';
        let selectedPackage = this.state.selectedProject.packages.find(e => e.id == id);
        this.setState({ payload, totalAmount: subscriptionPrice, selectedPackage, paymentModal: true })
    }

    onRegisterSubmitHandler = () => {
        this.setState({
            isFetching: true,
            paymentModal: false
        }, () => {
            this.props.onRegisterSubmitHandler(this.state.payload)
        })
    }

    onCloseHandler = () => {
        this.setState({
            isOpen: false,
            message2: '',
            modalType: "",
        })
    }

    telChangeHandler = (value) => {
        let payload = cloneDeep(this.state.payload);
        payload.mobile = value;
        this.setState({
            payload,
            otpCount: 0
        });

    }

    isRegisterDisable = () => {
        if (this.state.enableActivationKey && this.state.enableOtp) {
            return !(this.state.payload.activationKey && this.state.payload.otp)
        }
        else if (this.state.enableActivationKey) {
            return !(this.state.payload.activationKey)
        }
        else if (this.state.enableOtp) {
            return !(this.state.payload.otp)
        }
        return false
    }

    inputHandler = (event) => {
        this.setState({
            userInput: event.target.value,
            isConfirm: this.state.givenCaptchaCode === event.target.value ? true : false,
        })
    }

    validateNextButton = () => {
        return (this.state.payload.firstName.length == 0 ||
            this.state.payload.mobile.length < 10 ||
            this.state.payload.email.length == 0 ||
            this.state.payload.lastName.length == 0)
    }

    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>iSignUp</title>
                    <meta name="description" content="M83-iSignUp" />
                </Helmet>

                {this.state.isFetching ?
                    <Loader />
                    :
                    <div className="login-outer-box" id="makerLogin">
                        <div className="login-image-content">
                            <div className="login-image-box">
                                <img src="https://content.iot83.com/m83/tenant/welcome.png" />
                            </div>
                            <h1>Welcome to iFlex83</h1>
                            <h5>Connect . Configure . Visualize</h5>
                            <p>A powerful and cost effective path to create dynamic IoT Applications. ​</p>
                            <p>Fast new and legacy device on-boarding.</p>
                            <p>Few clicks and your dashboards ready.</p>
                            <p>Not a single line of code.</p>
                            <p>Security, reliability and scalability assured.</p>
                        </div>

                        {this.state.isShowForm ?
                            <div className="login-form-wrapper">
                                <div className="login-logo">
                                    <img src="https://content.iot83.com/m83/misc/iFlexLogo.png" />
                                </div>

                                <div className="login-form-box">
                                    <form onSubmit={this.onSubmitHandler}>
                                        <div className="login-form-text">
                                            <h5>Personal Details</h5>
                                            <p>Give us some of your information to get access to IoT83 platform</p>
                                        </div>
                                        <div className="form-group">
                                            <input type="text" id="firstName" placeholder="First Name" className="form-control" required onChange={this.inputChangeHandler} maxLength={15} value={this.state.payload.firstName} />
                                            <span className="form-control-icon"><i className="fad fa-id-card-alt"></i></span>
                                        </div>
                                        <div className="form-group">
                                            <input type="text" id="lastName" placeholder="Last Name" className="form-control" required onChange={this.inputChangeHandler} maxLength={15} value={this.state.payload.lastName} />
                                            <span className="form-control-icon"><i className="fad fa-address-card"></i></span>
                                        </div>
                                        <div className="form-group">
                                            <input type="email" id="email" placeholder="Email" className="form-control" required onChange={this.inputChangeHandler} value={this.state.payload.email} />
                                            <span className="form-control-icon"><i className="fad fa-envelope"></i></span>
                                        </div>
                                        <div className="form-group">
                                            <div className="phone-input">
                                                <PhoneInput
                                                    className="form-control"
                                                    inputProps={{
                                                        name: 'phone',
                                                        required: true,
                                                    }}
                                                    placeholder="Mobile No."
                                                    country={'us'}
                                                    value={this.state.payload.mobile}
                                                    onChange={this.telChangeHandler}
                                                    required
                                                />
                                            </div>
                                        </div>
                                        <div className="form-group">
                                            <button type="submit" className="btn btn-primary" disabled={this.validateNextButton()}>Next</button>
                                            <a onClick={() => this.props.history.push("/login")}><i className="fas fa-angle-double-left mr-r-5"></i>Back</a>
                                        </div>
                                    </form>
                                </div>

                                <div className="sign-up-text">
                                    <h6>Already have account ?
                                        <button name="button" className="btn btn-link" onClick={() => this.props.history.push('/login')}>Login Here <i className="fas fa-angle-double-right"></i></button>
                                    </h6>
                                </div>
                            </div>
                            :
                            <div className="login-form-wrapper">
                                <div className="login-logo">
                                    <img src="https://content.iot83.com/m83/misc/iFlexLogo.png" />
                                </div>

                                <div className="package-wrapper" id="makerPackage">
                                    <div className="package-text text-center">
                                        <h6>Choose a plan that fits your requirement</h6>
                                    </div>

                                    {this.state.allProjects.length > 0 &&
                                        <ul className="list-style-none d-flex package-list package-list-trial">
                                            {this.state.selectedProject.packages.map((pack) => (
                                                <li key={pack.id}>
                                                    <div className={this.state.payload.userProjectRequest.packageId === pack.id ? "package-box active" : "package-box"}>
                                                        <div className="package-box-header">
                                                            <h5 className="text-truncate">{pack.name}</h5>
                                                        </div>
                                                        <div className="package-price-box"></div>
                                                        <div className="package-price-box-content">
                                                            <h1><span>$</span>{pack.subscriptionPrice}<small className="f-12 ml-1">USD</small></h1>
                                                            <span>{pack.numberOfDays === 30 ? 'Monthly' : `${pack.numberOfDays} Days`}</span>
                                                        </div>
                                                        <div className="package-box-body">
                                                            {pack.featureList.map(feature =>
                                                                <p key={feature.id}>
                                                                    <i className={feature.count === 0 ? "fad fa-times text-red" : "fad fa-check-circle"}></i>
                                                                    <span>{feature.count}</span>
                                                                    {feature.displayName}
                                                                    {feature.showDescription &&
                                                                        <i className="fad fa-info-circle text-primary mr-l-10 mr-r-0 cursor-pointer" data-tooltip data-tooltip-text={feature.description} data-tooltip-place="right"></i>
                                                                    }
                                                                </p>
                                                            )}
                                                            <button type="button" className={`btn btn-${pack.name == "SILVER" ? "primary" : pack.name == "BRONZE" ? "info" : pack.name == "GOLD" ? "warning" : pack.name == "PLATINUM" ? "success" : "primary"}`} onClick={() => this.packageSelectHandler(pack)}>
                                                                {this.state.payload.userProjectRequest.packageId === pack.id && <i className="far fa-check"></i>} Get Started
                                                        </button>
                                                        </div>
                                                    </div>
                                                </li>
                                            ))}
                                        </ul>
                                    }
                                    <div className="package-button-group">
                                        <button type="button" className="btn" onClick={() => this.setState({ isShowForm: true })}><i className="fas fa-angle-double-left mr-r-5"></i>Back</button>
                                    </div>
                                </div>

                            </div>
                        }
                    </div>
                }

                {/* payment modal */}
                {this.state.paymentModal &&
                    <div className="modal show animated slideInDown d-block" id="paymentModal">
                        <div className="modal-dialog modal-lg modal-dialog-centered">
                            <div className="modal-content">
                                <div className="modal-body">
                                    <div className="register-detail text-center">
                                        <h3 className="text-dark-theme fw-600">{this.state.payload.firstName + " "}{this.state.payload.lastName}</h3>
                                        <p><i className="fad fa-envelope"></i>{this.state.payload.email}</p>
                                        <p><i className="fad fa-phone"></i>{this.state.payload.mobile}</p>
                                        <div className="register-detail-wrapper">
                                            <div className="d-flex">
                                                <div className="flex-50 pd-r-10">
                                                    <div className="register-package-detail h-100">
                                                        <h5>{this.state.selectedPackage.name}</h5>
                                                        <ul className="list-style-none">
                                                            {this.state.selectedPackage.featureList.map(feature =>
                                                                <li key={feature.id}>
                                                                    <i className={feature.count === 0 ? "fad fa-times text-red" : "fad fa-check-circle text-success"}></i>
                                                                    <span>{feature.count}</span>
                                                                    {feature.displayName}
                                                                    {feature.showDescription &&
                                                                        <i className="fad fa-info-circle text-primary mr-l-10" data-tooltip data-tooltip-text={feature.description} data-tooltip-place="bottom"></i>
                                                                    }
                                                                </li>
                                                            )}
                                                        </ul>
                                                        <h4>{this.state.selectedPackage.subscriptionPrice === 0 ? 'Price' : 'Subscription Charges'} : <span className="text-green">$</span> <span className="text-green">{this.state.selectedPackage.subscriptionPrice}</span> <span className="text-green">USD</span> <small>/ {this.state.selectedPackage.numberOfDays === 30 ? 'Month' : `${this.state.selectedPackage.numberOfDays} Days`}</small></h4>
                                                    </div>
                                                </div>

                                                <div className="flex-50 pd-l-10">
                                                    <div className="register-package-detail p-4 h-100">
                                                        {this.state.enableActivationKey &&
                                                            <div className="form-group">
                                                                <input type="text" id="activationKey" placeholder="Activation Code" className="form-control" required onChange={this.inputChangeHandler} value={this.state.payload.activationKey} />
                                                            </div>
                                                        }

                                                        {this.state.enableActivationKey &&
                                                            <p className="text-center f-11">Do not have an activation code, please contact our sales team
                                                            <a href="https://iot83.com/contact-us" target="_blank" className="text-underline ml-1">Click Here <i className="fas fa-share f-11"></i></a>
                                                            </p>
                                                        }
                                                        {this.state.enableOtp &&
                                                            <React.Fragment>
                                                                <div className="form-group input-group">
                                                                    <input type="number" className="form-control" id="otp" value={this.state.payload.otp} required onChange={this.inputChangeHandler} placeholder="Enter OTP" />
                                                                    <div className="input-group-append">
                                                                        <button className="btn btn-transparent-green" type="button" disabled={this.state.isLoading || interval != undefined || this.state.otpCount >= 2} onClick={() => {
                                                                            let payload = { mobile: this.state.payload.mobile };
                                                                            this.setState({
                                                                                isLoading: true
                                                                            }, () => this.props.getOtp(payload))
                                                                        }}>{this.state.otpCount === 0 ? "Get OTP" : "Regenerate OTP"}</button>
                                                                    </div>
                                                                </div>
                                                                <div>
                                                                    {this.state.otpCount >= 2 ? <p className="text-cyan f-11">Note: You have exhausted the daily quota of OTP requests</p>
                                                                        : interval != undefined ? <p className="text-content f-11">{`You can regenerate OTP after ${this.state.timeSeconds} sec`}</p> :
                                                                            <p className="text-cyan f-11">Note: You can request for a maximum of 2 OTPs</p>
                                                                    }
                                                                </div>
                                                            </React.Fragment>
                                                        }

                                                        <div className="form-group captcha-box">
                                                            <ClientCaptcha captchaClassName="captcha-text" captchaCode={code => this.setState({ givenCaptchaCode: code, isConfirm: false, userInput: '' })} />
                                                            <input type="text" className="form-control" placeholder="Enter captcha" value={this.state.userInput} onChange={() => this.inputHandler(event)}></input>
                                                        </div>

                                                        <p className="text-center f-11">By signing up, I agree to the iFlex83 <span className="text-primary text-underline-hover" onClick={() => window.open(`/termsAndConditions`, '_blank')}>Terms & Conditions</span> and <span className="text-primary text-underline-hover" onClick={() => window.open(`/privacyPolicy`, '_blank')}>Privacy Policy</span></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-dark" data-dismiss="modal" onClick={() => this.setState({ paymentModal: false, userInput: '', isConfirm: false })}>Cancel</button>
                                    <button type="button" className="btn btn-success mr-r-15" disabled={this.isRegisterDisable() || !this.state.isConfirm} onClick={() => this.onRegisterSubmitHandler()}>{this.state.selectedPackage.subscriptionPrice === 0 ? 'Register' : 'Proceed to Payment'}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                }
                {/* end payment modal */}

                {this.state.isOpen &&
                    <NotificationModal
                        type={this.state.modalType}
                        message2={this.state.message2}
                        onCloseHandler={this.onCloseHandler}
                    />
                }
            </React.Fragment >
        );
    }
}

iSignUp.propTypes = {
    dispatch: PropTypes.func.isRequired
};


let allSelectors = {}
Object.entries(SELECTORS).map(([key, value]) => {
    allSelectors[key] = value()
})

const mapStateToProps = createStructuredSelector(allSelectors)

function mapDispatchToProps(dispatch) {
    let allActions = { dispatch }
    Object.entries(ACTIONS).map(([key, value]) => {
        allActions[key] = (...args) => dispatch(value(...args))
    })
    return allActions
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "registration", reducer });
const withSaga = injectSaga({ key: "registration", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(iSignUp);
