/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { takeEvery } from 'redux-saga';
import * as CONSTANTS from './constants';
import { apiCallHandler } from '../../api';


export function* getAllProjectsHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_TENANT_DETAILS_SUCCESS, CONSTANTS.GET_TENANT_DETAILS_FAILURE, 'getTenantDetails', false)];

}

export function* watcherGetAllProjectsRequest() {
  yield takeEvery(CONSTANTS.GET_TENANT_DETAILS, getAllProjectsHandlerAsync);
}

export function* onRegisterHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.ON_REGISTER_HANDLER_SUCCESS, CONSTANTS.ON_REGISTER_HANDLER_FAILURE, 'onUserRegistration', false)];

}

export function* watcherOnRegisterHandler() {
  yield takeEvery(CONSTANTS.ON_REGISTER_HANDLER, onRegisterHandlerAsync);
}

export function* getOtpHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_OTP_SUCCESS, CONSTANTS.GET_OTP_FAILURE, 'getOtp', false)];

}

export function* watcherGetOtpHandler() {
  yield takeEvery(CONSTANTS.GET_OTP, getOtpHandlerAsync);
}

export default function* rootSaga() {
  yield [
    watcherGetAllProjectsRequest(),
    watcherOnRegisterHandler(),
    watcherGetOtpHandler()
  ]
}

