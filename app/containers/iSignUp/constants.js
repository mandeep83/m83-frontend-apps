/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/*
 *
 * iSignUp constants
 *
 */

export const RESET_TO_INITIAL_STATE = "app/iSignUp/RESET_TO_INITIAL_STATE";

export const GET_TENANT_DETAILS = "app/iSignUp/GET_TENANT_DETAILS";
export const GET_TENANT_DETAILS_SUCCESS = "app/iSignUp/GET_TENANT_DETAILS_SUCCESS";
export const GET_TENANT_DETAILS_FAILURE = "app/iSignUp/GET_TENANT_DETAILS_FAILURE";

export const ON_REGISTER_HANDLER = "app/iSignUp/ON_REGISTER_HANDLER";
export const ON_REGISTER_HANDLER_SUCCESS = "app/iSignUp/ON_REGISTER_HANDLER_SUCCESS";
export const ON_REGISTER_HANDLER_FAILURE = "app/iSignUp/ON_REGISTER_HANDLER_FAILURE";

export const GET_OTP = "app/iSignUp/GET_OTP";
export const GET_OTP_SUCCESS = "app/iSignUp/GET_OTP_SUCCESS";
export const GET_OTP_FAILURE = "app/iSignUp/GET_OTP_FAILURE";
