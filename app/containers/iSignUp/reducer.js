/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/*
 *
 * iSignUp reducer
 *
 */

import { fromJS } from "immutable";
import * as CONSTANTS from './constants';

export const initialState = fromJS({});

function registrationReducer(state = initialState, action) {
  switch (action.type) {
    case CONSTANTS.RESET_TO_INITIAL_STATE:
      return initialState;
    case CONSTANTS.GET_TENANT_DETAILS_SUCCESS:
      return Object.assign({}, state, {
        getTenantDetailsSuccess: action.response
      });
    case CONSTANTS.GET_TENANT_DETAILS_FAILURE:
      return Object.assign({}, state, {
        getTenantDetailsFailure: action.error
      });
    case CONSTANTS.ON_REGISTER_HANDLER_SUCCESS:
      return Object.assign({}, state, {
        onRegisterSuccess: action.response
      });
    case CONSTANTS.ON_REGISTER_HANDLER_FAILURE:
      return Object.assign({}, state, {
        onRegisterFailure: action.error
      });
    case CONSTANTS.GET_OTP_SUCCESS:
      return Object.assign({}, state, {
        getOtpSuccess: action.response
      });
    case CONSTANTS.GET_OTP_FAILURE:
      return Object.assign({}, state, {
        getOtpFailure: action.error
      });
    default:
      return state;
  }
}

export default registrationReducer;
