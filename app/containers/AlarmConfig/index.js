/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * AlarmConfig
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import { allSelectors as SELECTORS } from "./selectors";
import { allActions as ACTIONS } from './actions'
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import reducer from "./reducer";
import saga from "./saga";
import { cloneDeep } from "lodash";
import AddNewButton from "../../components/AddNewButton/Loadable"
import ClientCaptcha from "react-client-captcha";
import {getTimeDifference, isNavAssigned} from "../../commonUtils";

/* eslint-disable react/prefer-stateless-function */
export class AlarmConfig extends React.Component {

    state = {
        deviceTypeList: [],
        selectedConnector: {},
        alarmConfigData: {},
        isFetching: true,
        isFetchingAttributes: true
    }

    componentDidMount() {
        this.props.deviceTypeList()
    }

    static getDerivedStateFromProps(nextProps, state) {
        if (nextProps.deviceTypeListSuccess) {
            let deviceTypeList = cloneDeep(state.deviceTypeList)
            deviceTypeList = nextProps.deviceTypeListSuccess.response
            let selectedConnector = deviceTypeList.length && deviceTypeList[0]
            if (deviceTypeList.length) {
                nextProps.getAlarmConfigData(selectedConnector.id)
            }
            return { deviceTypeList, selectedConnector, isFetching: deviceTypeList.length ? true : false, isFetchingAttributes: deviceTypeList.length ? true : false }
        }

        else if (nextProps.deviceTypeListFailure) {
            return { isFetching: false }
        }

        else if (nextProps.alarmConfigDataSuccess) {
            let alarmConfigData = nextProps.alarmConfigDataSuccess.response.data;
            alarmConfigData.metaInfo.map(info => {
                if (info.isConfigured === false) {
                    info.config.map(config => {
                        config.isConfigured = false;
                    })
                }
            });
            return {
                alarmConfigData: alarmConfigData,
                isFetchingAttributes: false,
                isFetching: false
            }
        }

        else if (nextProps.alarmConfigDataFailure) {
            return { isFetching: false, isFetchingAttributes: false }
        }

        else if (nextProps.updateAlarmConfigDataSuccess) {
            nextProps.getAlarmConfigData(state.selectedConnector.id)
        }

        else if (nextProps.updateAlarmConfigDataFailure) {
            return { isFetching: false }
        }

        else if (nextProps.resetAlarmSuccess) {
            return { isResetModal: false, isFetching: false }
        }

        else if (nextProps.resetAlarmFailure) {
            return { isFetching: false }
        }
        return null
    }

    componentDidUpdate(prevProps, prevState) {
        let newProp = Object.keys(SELECTORS).find(prop => this.props[prop])
        if (newProp) {
            if (newProp.includes("Failure") || newProp == "updateAlarmConfigDataSuccess") {
                let propData = this.props[newProp].error ? this.props[newProp].error : cloneDeep(this.props[newProp].response.message)
                let type = newProp.includes("Failure") ? "error" : "success"
                this.props.showNotification(type, propData, this.onCloseHandler)
            }
            this.props.resetToInitialStateProps(newProp)
        }
    }

    onChangeAlarmConfigHandler = (index, configIndex) => {
        let alarmConfigData = cloneDeep(this.state.alarmConfigData);
        if (event.target.id == "isConfigured") {
            alarmConfigData.metaInfo[index].isConfigured = event.target.checked;
        } else if (event.target.id == "isConfig") {
            alarmConfigData.metaInfo[index].config[configIndex].isConfigured = event.target.checked;
        } else {
            alarmConfigData.metaInfo[index].config[configIndex][event.target.id] = event.target.value ? Number(event.target.value) : event.target.value;
            if (event.target.id === 'warning') {
                if (alarmConfigData.metaInfo[index].config[configIndex].operator === 'LESS_THAN') {
                    alarmConfigData.metaInfo[index].config[configIndex]['minor'] = alarmConfigData.metaInfo[index].config[configIndex]['warning'] ? Number((alarmConfigData.metaInfo[index].config[configIndex]['warning'] < 0 ? alarmConfigData.metaInfo[index].config[configIndex]['warning'] * 1.15 : alarmConfigData.metaInfo[index].config[configIndex]['warning'] * 0.85).toFixed(2)) : alarmConfigData.metaInfo[index].config[configIndex]['warning'];
                    alarmConfigData.metaInfo[index].config[configIndex]['major'] = alarmConfigData.metaInfo[index].config[configIndex]['warning'] ? Number((alarmConfigData.metaInfo[index].config[configIndex]['warning'] < 0 ? alarmConfigData.metaInfo[index].config[configIndex]['warning'] * 1.30 : alarmConfigData.metaInfo[index].config[configIndex]['warning'] * 0.70).toFixed(2)) : alarmConfigData.metaInfo[index].config[configIndex]['warning'];
                    alarmConfigData.metaInfo[index].config[configIndex]['critical'] = alarmConfigData.metaInfo[index].config[configIndex]['warning'] ? Number((alarmConfigData.metaInfo[index].config[configIndex]['warning'] < 0 ? alarmConfigData.metaInfo[index].config[configIndex]['warning'] * 1.45 : alarmConfigData.metaInfo[index].config[configIndex]['warning'] * 0.55).toFixed(2)) : alarmConfigData.metaInfo[index].config[configIndex]['warning'];
                } else {
                    alarmConfigData.metaInfo[index].config[configIndex]['minor'] = alarmConfigData.metaInfo[index].config[configIndex]['warning'] ? Number((alarmConfigData.metaInfo[index].config[configIndex]['warning'] < 0 ? alarmConfigData.metaInfo[index].config[configIndex]['warning'] * 0.85 : alarmConfigData.metaInfo[index].config[configIndex]['warning'] * 1.15).toFixed(2)) : alarmConfigData.metaInfo[index].config[configIndex]['warning'];
                    alarmConfigData.metaInfo[index].config[configIndex]['major'] = alarmConfigData.metaInfo[index].config[configIndex]['warning'] ? Number((alarmConfigData.metaInfo[index].config[configIndex]['warning'] < 0 ? alarmConfigData.metaInfo[index].config[configIndex]['warning'] * 0.70 : alarmConfigData.metaInfo[index].config[configIndex]['warning'] * 1.30).toFixed(2)) : alarmConfigData.metaInfo[index].config[configIndex]['warning'];
                    alarmConfigData.metaInfo[index].config[configIndex]['critical'] = alarmConfigData.metaInfo[index].config[configIndex]['warning'] ? Number((alarmConfigData.metaInfo[index].config[configIndex]['warning'] < 0 ? alarmConfigData.metaInfo[index].config[configIndex]['warning'] * 0.55 : alarmConfigData.metaInfo[index].config[configIndex]['warning'] * 1.45).toFixed(2)) : alarmConfigData.metaInfo[index].config[configIndex]['warning'];
                }
            } else if (event.target.id === 'minor') {
                if (alarmConfigData.metaInfo[index].config[configIndex].operator === 'LESS_THAN') {
                    alarmConfigData.metaInfo[index].config[configIndex]['major'] = alarmConfigData.metaInfo[index].config[configIndex]['minor'] ? Number((alarmConfigData.metaInfo[index].config[configIndex]['minor'] < 0 ? alarmConfigData.metaInfo[index].config[configIndex]['minor'] * 1.15 : alarmConfigData.metaInfo[index].config[configIndex]['minor'] * 0.85).toFixed(2)) : alarmConfigData.metaInfo[index].config[configIndex]['minor'];
                    alarmConfigData.metaInfo[index].config[configIndex]['critical'] = alarmConfigData.metaInfo[index].config[configIndex]['minor'] ? Number((alarmConfigData.metaInfo[index].config[configIndex]['minor'] < 0 ? alarmConfigData.metaInfo[index].config[configIndex]['minor'] * 1.30 : alarmConfigData.metaInfo[index].config[configIndex]['minor'] * 0.75).toFixed(2)) : alarmConfigData.metaInfo[index].config[configIndex]['minor'];
                } else {
                    alarmConfigData.metaInfo[index].config[configIndex]['major'] = alarmConfigData.metaInfo[index].config[configIndex]['minor'] ? Number((alarmConfigData.metaInfo[index].config[configIndex]['minor'] < 0 ? alarmConfigData.metaInfo[index].config[configIndex]['minor'] * 0.85 : alarmConfigData.metaInfo[index].config[configIndex]['minor'] * 1.15).toFixed(2)) : alarmConfigData.metaInfo[index].config[configIndex]['minor'];
                    alarmConfigData.metaInfo[index].config[configIndex]['critical'] = alarmConfigData.metaInfo[index].config[configIndex]['minor'] ? Number((alarmConfigData.metaInfo[index].config[configIndex]['minor'] < 0 ? alarmConfigData.metaInfo[index].config[configIndex]['minor'] * 0.70 : alarmConfigData.metaInfo[index].config[configIndex]['minor'] * 1.30).toFixed(2)) : alarmConfigData.metaInfo[index].config[configIndex]['minor'];
                }
            } else if (event.target.id === 'major') {
                if (alarmConfigData.metaInfo[index].config[configIndex].operator === 'LESS_THAN') {
                    alarmConfigData.metaInfo[index].config[configIndex]['critical'] = alarmConfigData.metaInfo[index].config[configIndex]['major'] ? Number((alarmConfigData.metaInfo[index].config[configIndex]['major'] < 0 ? alarmConfigData.metaInfo[index].config[configIndex]['major'] * 1.15 : alarmConfigData.metaInfo[index].config[configIndex]['major'] * 0.85).toFixed(2)) : alarmConfigData.metaInfo[index].config[configIndex]['major'];
                } else {
                    alarmConfigData.metaInfo[index].config[configIndex]['critical'] = alarmConfigData.metaInfo[index].config[configIndex]['major'] ? Number((alarmConfigData.metaInfo[index].config[configIndex]['major'] < 0 ? alarmConfigData.metaInfo[index].config[configIndex]['major'] * 0.85 : alarmConfigData.metaInfo[index].config[configIndex]['major'] * 1.15).toFixed(2)) : alarmConfigData.metaInfo[index].config[configIndex]['major'];
                }
            }
        }
        this.setState({ alarmConfigData })
    }

    updateAlarmConfigHandler = () => {
        let alarmConfigData = cloneDeep(this.state.alarmConfigData),
            isError = false,
            errorMessage = '';

        alarmConfigData.metaInfo.map(attribute => {
            attribute.config.map(el => {
                if (el.isConfigured) {
                    if (el.warning === "" || el.minor === "" || el.major === "" || el.critical === "") {
                        isError = true;
                        errorMessage = "Alarm config values can't be empty !"
                    } else {
                        if (el.operator === "GREATER_THAN") {
                            if (el.minor <= el.warning) {
                                isError = true;
                                errorMessage = "Minor configuration value should be greater than warning configuration !"
                            } else if (el.major <= el.minor) {
                                isError = true;
                                errorMessage = "Major configuration value should be greater than minor configuration !"
                            } else if (el.critical <= el.major) {
                                isError = true;
                                errorMessage = "Critical configuration value should be greater than major configuration !"
                            } else if (attribute.config.filter(config => config.operator === "LESS_THAN" && config.isConfigured).length > 0) {
                                if (el.warning <= attribute.config.filter(config => config.operator === "LESS_THAN")[0].warning) {
                                    isError = true;
                                    errorMessage = "Warning value for > condition should be greater than warning value of < condition !";
                                }
                            }
                        } else if (el.operator === "LESS_THAN") {
                            if (el.minor >= el.warning) {
                                isError = true;
                                errorMessage = "Minor configuration value should be less than warning configuration !"
                            } else if (el.major >= el.minor) {
                                isError = true;
                                errorMessage = "Major configuration value should be less than minor configuration !"
                            } else if (el.critical >= el.major) {
                                isError = true;
                                errorMessage = "Critical configuration value should be less than major configuration !"
                            } else if (attribute.config.filter(config => config.operator === "GREATER_THAN" && config.isConfigured).length > 0) {
                                if (el.warning >= attribute.config.filter(config => config.operator === "GREATER_THAN")[0].warning) {
                                    isError = true;
                                    errorMessage = "Warning value for > condition should be greater than warning value of < condition !";
                                }
                            }
                        }
                    }
                }

            })

        });
        if (isError) {
            this.setState({
                isOpen: true,
                type: 'error',
                message2: errorMessage,
                isFetching: false
            })
        } else {
            alarmConfigData.metaInfo.map(el => {
                if (el.config.filter(config => config.isConfigured === true).length > 0) {
                    el.isConfigured = true;
                } else {
                    el.isConfigured = false;
                }
            });
            alarmConfigData.metaInfo = alarmConfigData.metaInfo.filter(el => el.isConfigured);

            alarmConfigData.metaInfo.map(el => {
                el.alarmCriteria = [];
                el.config.map(config => {
                    if (config.operator === "LESS_THAN" && config.isConfigured) {
                        el.alarmCriteria.push(
                            {
                                type: "critical",
                                operator: "LESS_THAN",
                                low: Number((config.critical - Math.abs(config.major - config.critical)).toFixed(2)),
                                high: Number((config.critical).toFixed(2)),
                            },
                            {
                                type: "major",
                                operator: "LESS_THAN",
                                low: Number((config.critical).toFixed(2)),
                                high: Number((config.major).toFixed(2)),
                            },
                            {
                                type: "minor",
                                operator: "LESS_THAN",
                                low: Number((config.major).toFixed(2)),
                                high: Number((config.minor).toFixed(2)),
                            },
                            {
                                type: "warning",
                                operator: "LESS_THAN",
                                low: Number((config.minor).toFixed(2)),
                                high: Number((config.warning).toFixed(2)),
                            }
                        )
                    } else if (config.operator === "GREATER_THAN" && config.isConfigured) {
                        el.alarmCriteria.push(
                            {
                                type: "warning",
                                operator: "GREATER_THAN",
                                low: Number((config.warning).toFixed(2)),
                                high: Number((config.minor).toFixed(2)),
                            }, {
                            type: "minor",
                            operator: "GREATER_THAN",
                            low: Number((config.minor).toFixed(2)),
                            high: Number((config.major).toFixed(2)),
                        }, {
                            type: "major",
                            operator: "GREATER_THAN",
                            low: Number((config.major).toFixed(2)),
                            high: Number((config.critical).toFixed(2)),
                        }, {
                            type: "critical",
                            operator: "GREATER_THAN",
                            low: Number((config.critical).toFixed(2)),
                            high: Number((config.critical + Math.abs(config.critical - config.major))),
                        }
                        )
                    }
                });
            });
            this.setState({
                isFetching: true
            }, () => this.props.updateAlarmConfigData(this.state.selectedConnector.id, alarmConfigData.metaInfo));
        }
    }

    deviceTypeChangeHandler = () => {
        let selectedConnector = this.state.deviceTypeList.find(el => el.id === event.target.value)
        this.setState({ selectedConnector, isFetchingAttributes: true }, () => this.props.getAlarmConfigData(selectedConnector.id))
    }

    onCloseHandler = () => {
        this.setState({
            isOpen: false,
            type: '',
            message2: ''
        })
    }

    inputHandler = (event) => {
        this.setState({
            userInput: event.target.value,
            isConfirm: this.state.givenCaptchaCode === event.target.value ? true : false,
        })
    }

    refreshComponent = () => {
        this.props.deviceTypeList()
        this.setState({
            isFetching: true
        })
    }

    render() {
        let getInputValue = (value) => {
            let currentValue = value.toString()
            let split = currentValue.split('.')
            if (split.length == 2 && split[1].length > 2) {
                currentValue = parseFloat(currentValue).toFixed(2)
            }
            else {
                currentValue = currentValue
            }
            return currentValue
        }
        return (
            <React.Fragment>
                <Helmet>
                    <title>Alarm Configuration</title>
                    <meta name="description" content="M83-Alarm Configuration" />
                </Helmet>


                <header className="content-header d-flex">
                    <div className="flex-60">
                        <h6>Alarm Configuration</h6>
                    </div>
                    <div className="flex-40 text-right">
                        <div className="content-header-group">
                            <button className="btn btn-light" disabled={this.state.isFetching} data-tooltip data-tooltip-text="Refresh" data-tooltip-place="bottom" onClick={() => this.refreshComponent()}>
                                <i className="far fa-sync-alt"></i>
                            </button>
                        </div>
                    </div>
                </header>


                <div className="content-body" id="data-attributes">
                    {this.state.isFetching ?
                        <Loader /> :
                        this.state.deviceTypeList.length > 0 ?
                            <React.Fragment>
                                <div className="form-content-wrapper">
                                    <div className="form-content-box">
                                        <div className="form-content-header">
                                            <p>Basic Information
                                                {this.state.alarmConfigData.id && <small>Last Updated : {getTimeDifference(this.state.alarmConfigData.updatedAt ? this.state.alarmConfigData.updatedAt : this.state.alarmConfigData.createdAt)}</small>}
                                            </p>
                                        </div>
                                        <div className="form-content-body">
                                            <div className="form-group">
                                                <label className="form-group-label">Device Type : <i className="fas fa-asterisk form-group-required"></i></label>
                                                <div className="input-group">

                                                    <div className="input-group-prepend"><strong className="input-group-text">{this.state.deviceTypeList.filter(device => device.id === this.state.selectedConnector.id)[0]['assetType'] ? this.state.deviceTypeList.filter(device => device.id === this.state.selectedConnector.id)[0]['assetType'] : 'N/A' }</strong></div>
                                                    <select className="form-control" onChange={this.deviceTypeChangeHandler} value={this.state.selectedConnector.id}>
                                                        {this.state.deviceTypeList.map(connector => {
                                                            return (
                                                                <option key={connector.id} value={connector.id}>{connector.name}</option>
                                                            )
                                                        })}
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="form-content-box">
                                        <div className="form-content-header">
                                            <p>Attributes</p>
                                        </div>
                                        <div className="form-content-body alarm-config-wrapper">
                                            {this.state.isFetchingAttributes ?
                                                <div className="inner-loader-wrapper" style={{ height: 320 }}>
                                                    <div className="inner-loader-content">
                                                        <i className="fad fa-sync-alt fa-spin"></i>
                                                    </div>
                                                </div> :
                                                this.state.alarmConfigData.metaInfo.length ?
                                                    <React.Fragment>
                                                        <div className="condition-line">
                                                            <ul>
                                                                <strong>Lower Threshold</strong>
                                                                <li><span className="bg-critical"></span>Critical</li>
                                                                <li><span className="bg-major"></span>Major</li>
                                                                <li><span className="bg-minor"></span>Minor</li>
                                                                <li><span className="bg-alarm-warning"></span>Warning</li>
                                                                <li><span className="bg-green"></span>Normal</li>
                                                                <li><span className="bg-alarm-warning"></span>Warning</li>
                                                                <li><span className="bg-minor"></span>Minor</li>
                                                                <li><span className="bg-major"></span>Major</li>
                                                                <li><span className="bg-critical"></span>Critical</li>
                                                                <strong>Upper Threshold</strong>
                                                            </ul>
                                                        </div>
                                                        {this.state.alarmConfigData.metaInfo.map((attribute, index) =>
                                                            <div className="form-group condition-box" key={index}>
                                                                <div className="condition-select">
                                                                    {/*<label className="check-box">
                                                                    <input
                                                                        type="checkbox"
                                                                        id="isConfigured"
                                                                        checked={attribute.isConfigured}
                                                                        onChange={(e) => {
                                                                            this.onChangeAlarmConfigHandler(index)
                                                                        }}/>
                                                                    <span className="check-mark"></span>
                                                                </label>*/}
                                                                    <input className="form-control" value={attribute.displayName ? attribute.displayName : attribute.attribute} disabled={true} />
                                                                    <input className="form-control" type="number" value={attribute.value && attribute.value.toFixed(2)} disabled={true} />
                                                                </div>
                                                                <div className="condition-box-list">
                                                                    <div className="d-flex">
                                                                        <label className="form-group-label flex-5"></label>
                                                                        <label className="form-group-label flex-15">Operator :</label>
                                                                        <label className="form-group-label flex-20 text-alarm-warning">Warning :</label>
                                                                        <label className="form-group-label flex-20 text-minor">Minor :</label>
                                                                        <label className="form-group-label flex-20 text-major">Major :</label>
                                                                        <label className="form-group-label flex-20 text-critical">Critical :</label>
                                                                    </div>
                                                                    {attribute.config.map((config, configIndex) =>
                                                                        <ul className="list-style-none d-flex" key={configIndex}>
                                                                            <li className="flex-5">
                                                                                <label className="check-box">
                                                                                    <input
                                                                                        type="checkbox"
                                                                                        id="isConfig"
                                                                                        checked={config.isConfigured}
                                                                                        onChange={(e) => {
                                                                                            this.onChangeAlarmConfigHandler(index, configIndex)
                                                                                        }} />
                                                                                    <span className="check-mark"></span>
                                                                                </label>
                                                                            </li>
                                                                            <li className="flex-15">
                                                                                <input className="form-control" value={config.operator === 'GREATER_THAN' ? '>' : '<'} readOnly />
                                                                            </li>
                                                                            <li className="flex-20">
                                                                                <input
                                                                                    type="number"
                                                                                    className="form-control"
                                                                                    id="warning"
                                                                                    disabled={!config.isConfigured}
                                                                                    value={config.warning ? getInputValue(config.warning) : ""}
                                                                                    onChange={() => this.onChangeAlarmConfigHandler(index, configIndex)} />
                                                                            </li>
                                                                            <li className="flex-20">
                                                                                <input
                                                                                    type="number"
                                                                                    className="form-control"
                                                                                    id="minor"
                                                                                    disabled={!config.isConfigured}
                                                                                    value={config.minor ? getInputValue(config.minor) : ""}
                                                                                    onChange={() => this.onChangeAlarmConfigHandler(index, configIndex)} />
                                                                            </li>
                                                                            <li className="flex-20">
                                                                                <input
                                                                                    type="number"
                                                                                    className="form-control"
                                                                                    id="major"
                                                                                    disabled={!config.isConfigured}
                                                                                    value={config.major ? getInputValue(config.major) : ""}
                                                                                    onChange={() => this.onChangeAlarmConfigHandler(index, configIndex)} />
                                                                            </li>
                                                                            <li className="flex-20">
                                                                                <input
                                                                                    type="number"
                                                                                    className="form-control"
                                                                                    id="critical"
                                                                                    disabled={!config.isConfigured}
                                                                                    value={config.critical ? getInputValue(config.critical) : ""}
                                                                                    onChange={() => this.onChangeAlarmConfigHandler(index, configIndex)} />
                                                                            </li>
                                                                        </ul>
                                                                    )}
                                                                </div>
                                                            </div>
                                                        )}
                                                    </React.Fragment>
                                                    :
                                                    <div className="attribute-empty">
                                                        <div>
                                                            <i className="fad fa-align-slash text-red"></i>
                                                            <p>No Device attributes available</p>
                                                        </div>
                                                    </div>
                                            }

                                            {/*{this.state.isFetchingAttributes ?
                                                <div className="inner-loader-wrapper" style={{ height: 320 }}>
                                                    <div className="inner-loader-content">
                                                        <i className="fad fa-sync-alt fa-spin"></i>
                                                    </div>
                                                </div>
                                                :
                                                this.state.alarmConfigData.metaInfo.length ?
                                                    <div className="form-group mb-0">
                                                        <div className="attribute-list">
                                                            <div className="attribute-list-header attribute-list-header-border d-flex">
                                                                <p className="flex-20">Attribute</p>
                                                                <p className="flex-10">Value</p>
                                                                <p className="flex-10">Operator</p>
                                                                {this.state.alarmConfigData.metaInfo && this.state.alarmConfigData.metaInfo.length && this.state.alarmConfigData.metaInfo[0].alarmCriteria.map((condition, index) =>
                                                                    <p className="flex-15 text-capitalize fw-600 text-center" key={index}>
                                                                        <span className={condition.type == "critical" ? "text-critical" : condition.type == "major" ? "text-major" : condition.type == "minor" ? "text-minor" : "text-alarm-warning"}>{condition.type}</span>
                                                                    </p>
                                                                )}
                                                                <p className="flex-30"></p>
                                                                <p className="flex-17_5 pt-0 text-center">
                                                                    <span className="d-flex d-block">
                                                                        <label className="form-group-label flex-50 mb-0"><i className="far fa-long-arrow-down mr-r-5"></i>Low</label>
                                                                        <label className="form-group-label flex-50 mb-0"><i className="far fa-long-arrow-up mr-r-5"></i>High</label>
                                                                    </span>
                                                                </p>
                                                                <p className="flex-17_5 pt-0 text-center">
                                                                    <span className="d-flex d-block">
                                                                        <label className="form-group-label flex-50 mb-0"><i className="far fa-long-arrow-down mr-r-5"></i>Low</label>
                                                                        <label className="form-group-label flex-50 mb-0"><i className="far fa-long-arrow-up mr-r-5"></i>High</label>
                                                                    </span>
                                                                </p>
                                                                <p className="flex-17_5 pt-0 text-center">
                                                                    <span className="d-flex d-block">
                                                                        <label className="form-group-label flex-50 mb-0"><i className="far fa-long-arrow-down mr-r-5"></i>Low</label>
                                                                        <label className="form-group-label flex-50 mb-0"><i className="far fa-long-arrow-up mr-r-5"></i>High</label>
                                                                    </span>
                                                                </p>
                                                                <p className="flex-17_5 pt-0 text-center">
                                                                    <span className="d-flex d-block">
                                                                        <label className="form-group-label flex-50 mb-0"><i className="far fa-long-arrow-down mr-r-5"></i>Low</label>
                                                                        <label className="form-group-label flex-50 mb-0"><i className="far fa-long-arrow-up mr-r-5"></i>High</label>
                                                                    </span>
                                                                </p>
                                                            </div>
                                                            <div className="attribute-list-body">
                                                                {this.state.alarmConfigData.metaInfo && this.state.alarmConfigData.metaInfo.sort((a, b) => a.attribute !== b.attribute ? a.attribute < b.attribute ? -1 : 1 : 0).map((attribute, index) =>
                                                                    <ul className="list-style-none d-flex align-items-center" key={index}>
                                                                       <li className="flex-20">
                                                                            <label className="check-box">
                                                                                <span className="check-text">{attribute.displayName ? attribute.displayName : attribute.attribute}</span>
                                                                                <input
                                                                                    type="checkbox"
                                                                                    id="isConfigured"
                                                                                    checked={attribute.isConfigured}
                                                                                    onChange={(e) => {
                                                                                        this.onChangeAlarmConfigHandler(index)
                                                                                    }}
                                                                                />
                                                                                <span className="check-mark"></span>
                                                                            </label>
                                                                        </li>
                                                                        <li className="flex-10">
                                                                            <input type="number" value={attribute.value && attribute.value.toFixed(2)} disabled={true} className="form-control" />
                                                                        </li>
                                                                        <div className="flex-10 pd-r-5">
                                                                            <select className="form-control" id="operator" disabled={!attribute.isConfigured} value={attribute.operator} onChange={(e) => {this.onChangeAlarmConfigHandler(index)}}>
                                                                                <option value="LESS_THAN">{"<"}</option>
                                                                                <option value="GREATER_THAN">{">"}</option>
                                                                            </select>
                                                                        </div>
                                                                        {attribute.alarmCriteria.map((temp, attributeIndex) =>
                                                                            <li className="flex-15" key={attributeIndex}>
                                                                                <input type="number" className="form-control" id={temp.type} disabled={!attribute.isConfigured}  value={attribute[temp.type] ? getInputValue(attribute[temp.type]) : ""} onChange={() => this.onChangeAlarmConfigHandler(index, attributeIndex)} />
                                                                                <div className="d-flex">
                                                                                    <div className="flex-50 pd-r-5">
                                                                                        <input type="number" className="form-control" id="low" disabled={!attribute.isConfigured || !(temp.type === "warning")} value={temp.low ? getInputValue(temp.low) : ""} onChange={() => this.onChangeAlarmConfigHandler(index, attributeIndex)} />
                                                                                    </div>
                                                                                    <div className="flex-60 pd-l-5">
                                                                                        <input type="number" className="form-control" id={temp.type} disabled={!attribute.isConfigured}  value={attribute[temp.type] ? getInputValue(attribute[temp.type]) : ""} onChange={() => this.onChangeAlarmConfigHandler(index, attributeIndex)} />
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                        )}
                                                                    </ul>
                                                                )}
                                                            </div>
                                                        </div>
                                                    </div>
                                                    :
                                                    <div className="attribute-empty">
                                                        <div>
                                                            <i className="fad fa-align-slash text-red"></i>
                                                            <p>No Device attributes available</p>
                                                        </div>
                                                    </div>
                                            }*/}
                                        </div>
                                    </div>
                                </div>

                                <div className="form-info-wrapper">
                                    <div className="form-info-body">
                                        <div className="form-info-icon">
                                            <img src="https://content.iot83.com/m83/misc/addAlarmConfig.png" />
                                        </div>
                                        <h5>Alarm Configuration</h5>
                                        <ul className="list-style-none form-info-list mb-4">
                                            <li><p>Configure the threshold for selected attributes in this section and get real time notifications on the portal for any attribute that has breached the threshold.</p></li>
                                            <li><p>Every Alarm Criteria allows you to configure the min and max range for each attribute.</p></li>
                                            <li><p>Marking an Attribute as checked will mean that the System will consider this attributes's configuration to raise alarms.</p></li>
                                            <li><p>System presents you with some default thresholds calculated on the basis of the most recent value. You can change the thresholds as per your requirement.</p></li>
                                        </ul>
                                    </div>
                                    <div className="form-info-footer">
                                        <button type="button" className="btn btn-primary" onClick={() => this.updateAlarmConfigHandler()} disabled={((this.state.alarmConfigData.metaInfo && this.state.alarmConfigData.metaInfo.length == 0) || this.state.isFetchingAttributes)}>{this.state.alarmConfigData.id ? 'Update' : 'Save'}</button>
                                    </div>
                                </div>
                            </React.Fragment>
                            :
                            <AddNewButton
                                text1="No alarm configuration available"
                                text2="You haven't created any device type(s) yet. Please create a device type first."
                                addButtonEnable={isNavAssigned("deviceTypes")}
                                createItemOnAddButtonClick={() => this.props.history.push(`/addOrEditDeviceType`)}
                                imageIcon="addAlarm.png"
                            />
                    }
                </div>

                {this.state.isResetModal &&
                    <div className="modal show d-block animated slideInDown" id="deleteModal">
                        <div className="modal-dialog modal-dialog-centered">
                            <div className="modal-content">
                                <div className="modal-body">
                                    <div className="delete-content">
                                        <div className="delete-icon">
                                            <i className="fad fa-retweet-alt text-primary"></i>
                                        </div>
                                        <h4 className="text-primary">Delete Alarm Config</h4>
                                        <h6>Are you sure you want to delete alarm configuration for <strong className="text-gray">{this.state.deviceTypeList.filter(device => device.id === this.state.selectedConnector.id)[0].name}</strong> ?</h6>
                                        <h6 className="text-red">This action can't be undone !</h6>
                                        <div className="form-group captcha-box">
                                            <ClientCaptcha captchaClassName="captcha-text" captchaCode={code => this.setState({ givenCaptchaCode: code, userInput: '', isConfirm: false })} />
                                            <input type="text" className="form-control" value={this.state.userInput} placeholder="Enter captcha" onChange={() => this.inputHandler(event)}></input>
                                        </div>
                                    </div>
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-dark" onClick={() => this.setState({ userInput: '', isConfirm: false, isResetModal: false })}>Cancel</button>
                                    <button type="button" className="btn btn-success" disabled={!this.state.isConfirm} onClick={() => this.setState({ isFetching: true, isResetModal: false }, () => this.props.resetAlarm(this.state.selectedConnector.id))}>Reset</button>
                                </div>
                            </div>

                        </div>
                    </div>
                }

                {this.state.isOpen &&
                    <NotificationModal
                        type={this.state.type}
                        message2={this.state.message2}
                        onCloseHandler={this.onCloseHandler}
                    />
                }

            </React.Fragment>
        );
    }
}

AlarmConfig.propTypes = {
    dispatch: PropTypes.func.isRequired
};

let allSelectors = {}
Object.entries(SELECTORS).map(([key, value]) => {
    allSelectors[key] = value()
})

const mapStateToProps = createStructuredSelector(allSelectors);


function mapDispatchToProps(dispatch) {
    let allActions = { dispatch }
    Object.entries(ACTIONS).map(([key, value]) => {
        allActions[key] = (...args) => dispatch(value(...args))
    })
    return allActions;
}


const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "alarmConfig", reducer });
const withSaga = injectSaga({ key: "alarmConfig", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(AlarmConfig);
