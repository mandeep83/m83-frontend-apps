/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/*
 *
 * AlarmConfig constants
 *
 */


export const RESET_TO_INITIAL_STATE = "app/AlarmConfig/RESET_TO_INITIAL_STATE";
export const RESET_TO_INITIAL_STATE_PROPS = "app/AlarmConfig/RESET_TO_INITIAL_STATE_PROPS";


export const GET_DEVICE_TYPE_LIST = {
  action: 'app/AlarmConfig/GET_DEVICE_TYPE_LIST',
  success: "app/AlarmConfig/GET_DEVICE_TYPE_LIST_SUCCESS",
  failure: "app/AlarmConfig/GET_DEVICE_TYPE_LIST_FAILURE",
  urlKey: "deviceTypeList",
  successKey: "deviceTypeListSuccess",
  failureKey: "deviceTypeListFailure",
  actionName: "deviceTypeList",
  actionArguments: []
}

export const GET_ALARM_CONFIG_DATA = {
  action: 'app/AlarmConfig/GET_ALARM_CONFIG_DATA',
  success: "app/AlarmConfig/GET_ALARM_CONFIG_DATA_SUCCESS",
  failure: "app/AlarmConfig/GET_ALARM_CONFIG_DATA_FAILURE",
  urlKey: "getAndUpdateAlarmConfigData",
  successKey: "alarmConfigDataSuccess",
  failureKey: "alarmConfigDataFailure",
  actionName: "getAlarmConfigData",
  actionArguments: ["id"]
}

export const UPDATE_ALARM_CONFIG_DATA = {
  action: 'app/AlarmConfig/UPDATE_ALARM_CONFIG_DATA',
  success: "app/AlarmConfig/UPDATE_ALARM_CONFIG_DATA_SUCCESS",
  failure: "app/AlarmConfig/UPDATE_ALARM_CONFIG_DATA_FAILURE",
  urlKey: "getAndUpdateAlarmConfigData",
  successKey: "updateAlarmConfigDataSuccess",
  failureKey: "updateAlarmConfigDataFailure",
  actionName: "updateAlarmConfigData",
  actionArguments: ["id", "payload"]
}

export const RESET_ALARM_CONFIG = {
  action: 'app/AlarmConfig/RESET_ALARM_CONFIG',
  success: "app/AlarmConfig/RESET_ALARM_CONFIG_SUCCESS",
  failure: "app/AlarmConfig/RESET_ALARM_CONFIG_FAILURE",
  urlKey: "resetAlarm",
  successKey: "resetAlarmSuccess",
  failureKey: "resetAlarmFailure",
  actionName: "resetAlarm",
  actionArguments: ["id"]
}