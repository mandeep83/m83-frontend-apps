/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/*
 *
 * AddOrEditConnector constants
 *
 */
export const RESET_TO_INITIAL_STATE = "app/AddOrEditConnector/RESET_TO_INITIAL_STATE";

export const SAVE_CONNECTOR = {
	action: 'app/AddOrEditConnector/SAVE_CONNECTOR',
	success: "app/AddOrEditConnector/SAVE_CONNECTOR_SUCCESS",
	failure: "app/AddOrEditConnector/SAVE_CONNECTOR_FAILURE",
	urlKey: "saveDataSource",
	successKey: "saveConnectorSuccess",
	failureKey: "saveConnectorFailure",
	actionName: "saveConnector",
	actionArguments: ["payload"]
}

export const GET_CONNECTOR_BY_ID = {
	action: 'app/AddOrEditConnector/GET_CONNECTOR_BY_ID',
	success: "app/AddOrEditConnector/GET_CONNECTOR_BY_ID_SUCCESS",
	failure: "app/AddOrEditConnector/GET_CONNECTOR_BY_ID_FAILURE",
	urlKey: "getDataSource",
	successKey: "getConnectorByIdSuccess",
	failureKey: "getConnectorByIdFailure",
	actionName: "getConnectorById",
	actionArguments: ["id"]
}


export const GET_DEVICE_TYPE_BY_ID = {
	action: 'app/AddOrEditConnector/GET_DEVICE_TYPE_BY_ID',
	success: "app/AddOrEditConnector/GET_DEVICE_TYPE_BY_ID_SUCCESS",
	failure: "app/AddOrEditConnector/GET_DEVICE_TYPE_BY_ID_FAILURE",
	urlKey: "getDeviceTypeById",
	successKey: "getDeviceTypeByIdSuccess",
	failureKey: "getDeviceTypeByIdFailure",
	actionName: "getDeviceTypeById",
	actionArguments: ["payload", "id"]
}

