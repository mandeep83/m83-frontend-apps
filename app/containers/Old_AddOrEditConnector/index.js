/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * AddOrEditConnector
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import { allSelectors as SELECTORS } from "./selectors";
import { allActions as ACTIONS } from './actions'
import reducer from "./reducer";
import NotificationModal from '../../components/NotificationModal/Loadable'
import saga from "./saga";

/* eslint-disable react/prefer-stateless-function */
export class AddOrEditConnector extends React.Component {

  state = {
    isFetching: false,
    payload: {}
  }

  componentWillMount() {
    if (this.props.match.params.id) {
      // if (localStorage.tenantType === "SAAS") {
      //   let payload = {
      //     max: 10,
      //     offset: 0
      //   }
      //   this.props.getDeviceTypeById(payload, this.props.match.params.id)
      // }
      // else
      this.props.getConnectorById(this.props.match.params.id)
      this.setState({
        isFetching: true
      })
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.getConnectorByIdSuccess) {
      this.setState({
        payload: nextProps.getConnectorByIdSuccess.response,
        isFetching: false
      })
      return;
    }

    if (nextProps.getConnectorByIdFailure) {
      this.setState({
        modalType: "error",
        isOpen: true,
        message2: nextProps.getConnectorByIdFailure.error,
        isFetching: false
      })
      return;
    }


    // if (nextProps.saveConnectorSuccess) {
    //   this.setState({
    //     modalType: "success",
    //     isOpen: true,
    //     message2: nextProps.saveConnectorSuccess.response,
    //     // saveConnectorSuccess: true
    //   })
    //   return;
    // }

    // if (nextProps.saveConnectorFailure) {
    //   this.setState({
    //     modalType: "error",
    //     isOpen: true,
    //     message2: nextProps.saveConnectorFailure.error,
    //   })
    //   return;
    // }
  }

  componentDidUpdate(prevProps, prevState) {
    if (Object.keys(SELECTORS).some(prop => this.props[prop])) {
      this.props.resetToInitialState()
    }
  }

  onCloseHandler = () => {
    this.setState({
      isOpen: false,
      message2: '',
      modalType: '',
    }, () => this.state.saveConnectorSuccess && this.props.history.push(`/deviceTypes`));
  }


  render() {
    return (
      <React.Fragment>
        {/* {this.props.match.path.includes("addOrEditDeviceType") ?
          <AddOrEditDeviceType {...this.props} isFetching={this.state.isFetching} />
          : null
        } */}
        {this.state.isOpen &&
          <NotificationModal
            type={this.state.modalType}
            message2={this.state.message2}
            onCloseHandler={this.onCloseHandler}
          />
        }
      </React.Fragment>
    );
  }
}

AddOrEditConnector.propTypes = {
  dispatch: PropTypes.func.isRequired
};

let allSelectors = {}
Object.entries(SELECTORS).map(([key, value]) => {
  allSelectors[key] = value()
})

const mapStateToProps = createStructuredSelector(allSelectors);


function mapDispatchToProps(dispatch) {
  let allActions = { dispatch }
  Object.entries(ACTIONS).map(([key, value]) => {
    allActions[key] = (...args) => dispatch(value(...args))
  })
  return allActions;
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

const withReducer = injectReducer({ key: "addOrEditConnector", reducer });
const withSaga = injectSaga({ key: "addOrEditConnector", saga });

export default compose(
  withReducer,
  withSaga,
  withConnect
)(AddOrEditConnector);
