/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

export const DEFAULT_ACTION = "app/TenantDetails/DEFAULT_ACTION";

export const GET_USER_DETAILS = "app/TenantDetails/GET_USER_DETAILS";
export const GET_USER_DETAILS_SUCCESS = "app/TenantDetails/GET_USER_DETAILS_SUCCESS";
export const GET_USER_DETAILS_FAILURE = "app/TenantDetails/GET_USER_DETAILS_FAILURE";

export const FETCH_TENANT_DETAILS = "app/TenantDetails/FETCH_TENANT_DETAILS";
export const FETCH_TENANT_DETAILS_SUCCESS = "app/TenantDetails/FETCH_TENANT_DETAILS_SUCCESS";
export const FETCH_TENANT_DETAILS_FAILURE = "app/TenantDetails/FETCH_TENANT_DETAILS_FAILURE";

export const FETCH_TENANT_SUMMARY = "app/TenantDetails/FETCH_TENANT_SUMMARY";
export const FETCH_TENANT_SUMMARY_SUCCESS = "app/TenantDetails/FETCH_TENANT_SUMMARY_SUCCESS";
export const FETCH_TENANT_SUMMARY_FAILURE = "app/TenantDetails/FETCH_TENANT_SUMMARY_FAILURE";


export const GET_ACTIVITY_LINE_CHART_DATA = "app/TenantDetails/GET_ACTIVITY_LINE_CHART_DATA";
export const GET_ACTIVITY_LINE_CHART_DATA_SUCCESS = "app/TenantDetails/GET_ACTIVITY_LINE_CHART_DATA_SUCCESS";
export const GET_ACTIVITY_LINE_CHART_DATA_FAILURE = "app/TenantDetails/GET_ACTIVITY_LINE_CHART_DATA_FAILURE";

export const GET_ACTIVITY_BAR_CHART_DATA = "app/TenantDetails/GET_ACTIVITY_BAR_CHART_DATA";
export const GET_ACTIVITY_BAR_CHART_DATA_SUCCESS = "app/TenantDetails/GET_ACTIVITY_BAR_CHART_DATA_SUCCESS";
export const GET_ACTIVITY_BAR_CHART_DATA_FAILURE = "app/TenantDetails/GET_ACTIVITY_BAR_CHART_DATA_FAILURE";
