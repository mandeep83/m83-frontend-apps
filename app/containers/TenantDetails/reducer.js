/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { fromJS } from "immutable";
import {
  DEFAULT_ACTION,
  FETCH_TENANT_DETAILS_SUCCESS,
  FETCH_TENANT_DETAILS_FAILURE,
  FETCH_TENANT_SUMMARY_SUCCESS,
  FETCH_TENANT_SUMMARY_FAILURE,
  GET_ACTIVITY_LINE_CHART_DATA_SUCCESS,
  GET_ACTIVITY_LINE_CHART_DATA_FAILURE,
  GET_ACTIVITY_BAR_CHART_DATA_SUCCESS,
  GET_ACTIVITY_BAR_CHART_DATA_FAILURE,
  GET_USER_DETAILS_SUCCESS,
  GET_USER_DETAILS_FAILURE,

} from "./constants";

export const initialState = fromJS({});

function tenantDetailsReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case FETCH_TENANT_DETAILS_SUCCESS:
      return Object.assign({}, state, {
        tenantDetails: action.response,
      });
    case FETCH_TENANT_DETAILS_FAILURE:
      return Object.assign({}, state, {
        tenantDetailsError: action.error
      });
    case FETCH_TENANT_SUMMARY_SUCCESS:
      return Object.assign({}, state, {
        tenantSummary: action.response,
      });
    case FETCH_TENANT_SUMMARY_FAILURE:
      return Object.assign({}, state, {
        tenantSummaryError: action.error
      });
    case GET_ACTIVITY_LINE_CHART_DATA_SUCCESS:
      return Object.assign({}, state, {
        lineChartData: action.response,
      });
    case GET_ACTIVITY_LINE_CHART_DATA_FAILURE:
      return Object.assign({}, state, {
        lineChartDataError: action.error,
      });
    case GET_ACTIVITY_BAR_CHART_DATA_SUCCESS:
      return Object.assign({}, state, {
        barChartData: action.response,
      });
    case GET_ACTIVITY_BAR_CHART_DATA_FAILURE:
      return Object.assign({}, state, {
        barChartDataError: action.error,
      });
    case GET_USER_DETAILS_SUCCESS:
      return Object.assign({}, state, {
        userDetails: action.response,
      });
    case GET_USER_DETAILS_FAILURE:
      return Object.assign({}, state, {
        userDetailsError: action.error,
      });
    default:
      return state;
  }
}

export default tenantDetailsReducer;
