/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { takeEvery } from 'redux-saga';

import {
  GET_USER_DETAILS,
  GET_USER_DETAILS_SUCCESS,
  GET_USER_DETAILS_FAILURE,
  FETCH_TENANT_DETAILS,
  FETCH_TENANT_DETAILS_SUCCESS,
  FETCH_TENANT_DETAILS_FAILURE,
  FETCH_TENANT_SUMMARY,
  FETCH_TENANT_SUMMARY_SUCCESS,
  FETCH_TENANT_SUMMARY_FAILURE,
  GET_ACTIVITY_LINE_CHART_DATA,
  GET_ACTIVITY_LINE_CHART_DATA_SUCCESS,
  GET_ACTIVITY_LINE_CHART_DATA_FAILURE,
  GET_ACTIVITY_BAR_CHART_DATA,
  GET_ACTIVITY_BAR_CHART_DATA_SUCCESS,
  GET_ACTIVITY_BAR_CHART_DATA_FAILURE,

} from './constants';
import { apiCallHandler } from "../../api";


export function* tenantDetailsApiHandlerAsync(action) {
  yield [apiCallHandler(action, FETCH_TENANT_DETAILS_SUCCESS, FETCH_TENANT_DETAILS_FAILURE, 'getTenantDetailsById')];
}

export function* getTenantSummaryHandlerAsync(action) {
  yield [apiCallHandler(action, FETCH_TENANT_SUMMARY_SUCCESS, FETCH_TENANT_SUMMARY_FAILURE, 'getAccountStats')];
}

export function* getActivityLineChartDataHandlerAsync(action) {
  yield [apiCallHandler(action, GET_ACTIVITY_LINE_CHART_DATA_SUCCESS, GET_ACTIVITY_LINE_CHART_DATA_FAILURE, 'getTenantActivityLineChartData')];
}

export function* getActivityBarChartDataHandlerAsync(action) {
  yield [apiCallHandler(action, GET_ACTIVITY_BAR_CHART_DATA_SUCCESS, GET_ACTIVITY_BAR_CHART_DATA_FAILURE, 'getTenantActivityBarChartData')];
}

export function* getUserDetailsHandlerAsync(action) {
  yield [apiCallHandler(action, GET_USER_DETAILS_SUCCESS, GET_USER_DETAILS_FAILURE, 'getUserDetails')];
}

export function* watcherTenantDetailsRequest() {
  yield takeEvery(FETCH_TENANT_DETAILS, tenantDetailsApiHandlerAsync);
}

export function* watcherTenantSummaryRequest() {
  yield takeEvery(FETCH_TENANT_SUMMARY, getTenantSummaryHandlerAsync);
}

export function* watcherGetActivityLineChartDataRequest() {
  yield takeEvery(GET_ACTIVITY_LINE_CHART_DATA, getActivityLineChartDataHandlerAsync);
}

export function* watcherGetActivityBarChartDataRequest() {
  yield takeEvery(GET_ACTIVITY_BAR_CHART_DATA, getActivityBarChartDataHandlerAsync);
}

export function* watcherGetUserDetails() {
  yield takeEvery(GET_USER_DETAILS, getUserDetailsHandlerAsync);
}

export default function* rootSaga() {
  yield [
    watcherTenantDetailsRequest(),
    watcherTenantSummaryRequest(),
    watcherGetActivityLineChartDataRequest(),
    watcherGetActivityBarChartDataRequest(),
    watcherGetUserDetails()
  ];
}
