/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import { CircularProgressbar, buildStyles } from 'react-circular-progressbar';

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import reducer from "./reducer";
import saga from "./saga";
import Loader from "../../components/Loader/Loadable";
import {
    getTenantDetailsSuccess,
    getTenantDetailsError,
    getTenantSummarySuccess,
    getTenantSummaryError,
    lineChartData,
    lineChartDataError,
    barChartData,
    barChartDataError,
    userDetails,
    userDetailsError,
} from './selectors'
import { getUserDetails, fetchTenantDetails, fetchTenantSummary, getActivityLineChartData, getActivityBarChartData } from './actions';
import RGL, { WidthProvider } from "react-grid-layout";
import ReactTooltip from "react-tooltip";
import AllChartsComponent from "../../components/AllChartsComponent";

const ReactGridLayout = WidthProvider(RGL);

/* eslint-disable react/prefer-stateless-function */
export class TenantDetails extends React.Component {
    state = {
        tenantSummary: [],
        tenantDetails: {},
        clusterInfo: [],
        counters: [],
        isFetching: true,
        isFetchingLineChartData: true,
        isFetchingBarChartData: true,
        lineChartData: [],
        barChartData: [],
        durationActivityLineChartData: 7,
        durationActivityBarChartData: 7,
        tenantList: []
    }

    componentDidMount() {
        if (this.props.match.params.id) {
            this.props.fetchTenantDetails(this.props.match.params.id);
            this.props.fetchTenantSummary(this.props.match.params.id);
            this.props.getActivityLineChartData(this.props.match.params.id, this.state.durationActivityLineChartData);
            this.props.getActivityBarChartData(this.props.match.params.id, this.state.durationActivityBarChartData);
        } else {
            this.props.getUserDetails();
        }
    }

    filterHandler = (event) => {
        const self = this;
        let tenantId = self.props.match.params.id ? self.props.match.params.id : this.state.userDetails.tenantId;
        if (event.target.id === 'activities') {
            self.setState({
                isFetchingLineChartData: true,
                durationActivityLineChartData: event.target.value
            }, () => { this.props.getActivityLineChartData(tenantId, self.state.durationActivityLineChartData) })
        } else {
            self.setState({
                isFetchingBarChartData: true,
                durationActivityBarChartData: event.target.value
            }, () => { this.props.getActivityBarChartData(tenantId, self.state.durationActivityBarChartData) });
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.userDetails && nextProps.userDetails !== this.props.userDetails) {
            this.setState({
                userDetails: nextProps.userDetails,
            }, () => {
                this.props.fetchTenantDetails(nextProps.userDetails.tenantId);
                this.props.fetchTenantSummary(nextProps.userDetails.tenantId);
                this.props.getActivityLineChartData(nextProps.userDetails.tenantId, this.state.durationActivityLineChartData);
                this.props.getActivityBarChartData(nextProps.userDetails.tenantId, this.state.durationActivityBarChartData);
            })
        }

        if (nextProps.tenantDetails && nextProps.tenantDetails !== this.props.tenantDetails) {
            this.setState({
                tenantDetails: nextProps.tenantDetails,
            })
        }

        if (nextProps.tenantDetailsError && nextProps.tenantDetailsError !== this.props.tenantDetailsError) {
            this.setState({
                modaltype: 'error',
                isOpen: true,
                message1: 'Oops !',
                isFetching: false,
                message2: nextProps.tenantDetailsError
            })
        }

        if (nextProps.tenantSummary && nextProps.tenantSummary !== this.props.tenantSummary) {
            let counters = JSON.parse(JSON.stringify(nextProps.tenantSummary.features));
            let clusterInfo = [
                {
                    name: "CPU Usage",
                    count: nextProps.tenantSummary.clusterInfo.cpuUsgae,
                    pathColor: 'rgba(0, 230, 115, 0.7)',
                },
                {
                    name: "Memory Usage",
                    count: nextProps.tenantSummary.clusterInfo.memoryUsage,
                    pathColor: 'rgba(255, 163, 26, 0.7)',
                },
                {
                    name: "Pods Usage",
                    count: nextProps.tenantSummary.clusterInfo.noOfPods,
                    pathColor: 'rgba(51, 102, 255, 0.5)',
                },
            ]

            this.setState({
                tenantSummary: nextProps.tenantSummary.features,
                isFetching: false,
                counters,
                clusterInfo
            })
        }

        if (nextProps.tenantSummaryError && nextProps.tenantSummaryError !== this.props.tenantSummaryError) {
            this.setState({
                modaltype: 'error',
                isOpen: true,
                message1: 'Oops !',
                isFetching: false,
                message2: nextProps.tenantSummaryError
            })
        }

        if (nextProps.lineChartData && nextProps.lineChartData !== this.props.lineChartData) {

            this.setState({
                lineChartData: nextProps.lineChartData,
                isFetchingLineChartData: false,
            })
        }

        if (nextProps.barChartData && nextProps.barChartData !== this.props.barChartData) {
            this.setState({
                barChartData: nextProps.barChartData,
                isFetchingBarChartData: false,
            })
        }
    }

    createChart = (data, id, chartType, chartSubType) => {
        let dataForChart = {
            availableAttributes: [],
            chartData: chartType == "lineChart" ? data.map(chartDataObj => {
                chartDataObj.category = new Date(chartDataObj.timestamp).toLocaleDateString()
                return chartDataObj;
            }) : data
        }

        Object.keys(data[0]).map((key, index) => {
            if (key !== 'category' && key !== 'timestamp') {
                dataForChart.availableAttributes.push(key)
            }
        })
        let config = {
            chartType: chartType,
            chartSubType: chartSubType,
            id: id,
            chartData: dataForChart
        };
        return config;
    }


    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>Tenant Details</title>
                    <meta name="description" content="M83-TenantDetail" />
                </Helmet>



                {/* {this.state.isFetching ?
                    <Loader /> :
                    <React.Fragment>
                        <div className="pageBreadcrumb">
                            <div className="row">
                                <div className="col-8">
                                    <p>
                                        <span className="active">{this.state.tenantDetails.companyName}</span>
                                    </p>
                                </div>

                                <div className="col-4 text-right">
                                    <div className="flex h-100 justify-content-end align-items-center">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="outerBox">
                            <div className="flex">
                                <div className="flex-item fx-b25 pd-r-7">
                                    <div className="profileBox">
                                        <div className="profilHeader">
                                            <h6>{this.state.tenantDetails.companyName}
                                                {this.state.tenantDetails.defaultAccount &&
                                                    <React.Fragment>
                                                        <button type="button" data-tip data-for="delete" className="btn btn-transparent btn-transparent-danger"><i className="fas fa-lock"></i></button>
                                                        <ReactTooltip id="delete" place="bottom" type="dark">
                                                            <div className="tooltipText">
                                                                <p>Delete Protected</p>
                                                            </div>
                                                        </ReactTooltip>
                                                    </React.Fragment>
                                                }</h6>
                                            <div className="profileStartBox">
                                                {this.state.tenantDetails.licenseType == 'TRIAL' ?
                                                    <p className="startBox">
                                                        <i className="fas fa-star"></i>
                                                    </p>
                                                    :
                                                    this.state.tenantDetails.licenseType == 'POC' ?
                                                        <p className="startBox">
                                                            <i className="fas fa-star"></i>
                                                        </p>
                                                        :
                                                        <p className="startBox">
                                                            <i className="fas fa-star"></i>
                                                            <i className="fas fa-star"></i>
                                                            <i className="fas fa-star"></i>
                                                        </p>
                                                }
                                                <p className={this.state.tenantDetails.licenseType == 'TRIAL' ? 'text-primary' : this.state.tenantDetails.licenseType == 'POC' ? 'text-orange' : 'text-lightGreen'}>{this.state.tenantDetails.licenseType}</p>
                                            </div>
                                        </div>
                                        <div className="profileContent">
                                            <div className="profileImageBox border-0">
                                                <img src={this.state.tenantDetails.imageUrl ? this.state.tenantDetails.imageUrl : "https://content.iot83.com/m83/tenant/tenantBuilding.png"} />
                                            </div>
                                            <p><strong><i className="fas fa-user-tag mr-r-7"></i> Tenant :</strong>{this.state.tenantDetails.tenantName}</p>
                                            <p><strong><i className="fas fa-tachometer-alt-slowest mr-r-7"></i> Type :</strong><span className={this.state.tenantDetails.type == "ENTERPRISE" ? "position-static alert alert-primary pageStage" : "position-static alert alert-info pageStage"}>{this.state.tenantDetails.type}</span></p>
                                            <p><strong><i className="fas fa-users mr-r-7"></i> Users :</strong><span data-tip data-for="user" className="badge badge-pill badge-primary">{this.state.tenantDetails.users ? this.state.tenantDetails.users.length : "N/A"}</span></p>
                                            <ReactTooltip id="user" place="bottom" type="dark">
                                                <div className="tooltipText">
                                                    <p>
                                                        <ul className="listStyleNone flex align-items-center">
                                                            {this.state.tenantDetails.users && this.state.tenantDetails.users.map((user, index) =>
                                                                <li className="mr-r-10" key={index}>
                                                                    <h6>{user.firstName} {user.lastName}</h6>
                                                                </li>
                                                            )}
                                                        </ul>
                                                    </p>
                                                </div>
                                            </ReactTooltip>
                                            <p className="text-truncate" data-tip data-for="description"><strong><i className="fas fa-tag mr-r-7"></i> Description :</strong>{this.state.tenantDetails.description ? this.state.tenantDetails.description : 'N/A'}</p>
                                            <ReactTooltip id="description" place="bottom" type="dark">
                                                <div className="tooltipText">
                                                    <p>{this.state.tenantDetails.description ? this.state.tenantDetails.description : 'N/A'}</p>
                                                </div>
                                            </ReactTooltip>
                                        </div>
                                    </div>
                                </div>
                                <div className="flex-item fx-b75">
                                    <ul className="counterList">
                                        {this.state.counters.map((data, index) => {
                                            return (
                                                <li key={index}>
                                                    <div className="counterCard landingCard">
                                                        <div className="counterContent">
                                                            <p>{data.displayName}</p>
                                                            <h4>{data.count}<span>Out of</span>{data.totalCount}</h4>
                                                        </div>
                                                        <div className="counterProgress">
                                                            <CircularProgressbar
                                                                value={parseInt((data.count / data.totalCount) * 100)}
                                                                text={`${parseInt((data.count / data.totalCount) * 100)}%`}
                                                                styles={buildStyles({
                                                                    trailColor: "#eee",
                                                                    pathColor: "#0099ff"
                                                                })}
                                                                className="circleProgressBar"
                                                            />
                                                        </div>
                                                    </div>
                                                </li>
                                            )
                                        })}
                                        <li>
                                            <div className="counterCard calenderIconCard">
                                                <div className="counterContent">
                                                    <p>Renewal Date</p>
                                                    <h4>{new Date(this.state.tenantDetails.licenseExpire).toLocaleDateString()}</h4>
                                                </div>
                                                <div className="counterIcon">
                                                    <i className="far fa-calendar-alt"></i>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div className="flex"> */}
                                {/*<div className="flex-item fx-b15 pd-r-7">*/}
                                {/*    <div className="profileUserBox tenantDetailContent">*/}
                                {/*        <div className="profilHeader">*/}
                                {/*            Users :*/}
                                {/*        </div>*/}
                                {/*            <div className="userList">*/}
                                {/*                <ul className="listStyleNone flex align-items-center">*/}
                                {/*                    {this.state.tenantDetails.users && this.state.tenantDetails.users.map((user, index) =>*/}
                                {/*                        <li key={index}>*/}
                                {/*                            <h6 data-tip data-for={'tooltip' + index}>{user.firstName} {user.lastName}*/}
                                {/*                                <span><i className="fas fa-info"></i></span>*/}
                                {/*                            </h6>*/}
                                {/*                            <ReactTooltip id={'tooltip' + index} place="bottom" type="dark">*/}
                                {/*                                <div className="tooltipText">*/}
                                {/*                                    <p><i className="fad fa-envelope"></i>{user.email}</p>*/}
                                {/*                                    <p><i className="fad fa-phone-alt"></i>{user.mobile}</p>*/}
                                {/*                                </div>*/}
                                {/*                            </ReactTooltip>*/}
                                {/*                        </li>*/}
                                {/*                    )}*/}
                                {/*                </ul>*/}
                                {/*            </div>*/}
                                {/*    </div>*/}
                                {/*</div>*/}
                                {/* <div className="flex-item fx-b100">
                                    <div className="card panel">
                                        <div className="flex usageList">
                                            {this.state.clusterInfo.map((info, index) =>
                                                <div key={index} className="flex-item fx-b33">
                                                    <div className="usageListContent">
                                                        <CircularProgressbar
                                                            value={info.count}
                                                            circleRatio={0.75}
                                                            styles={buildStyles({
                                                                rotation: 1 / 2 + 1 / 8,
                                                                pathColor: `${info.pathColor}`,
                                                                trailColor: "#eee"
                                                            })}
                                                            className="circleProgressBar"
                                                        />
                                                        <h1>{info.count}<span>%</span></h1>
                                                        <h5>{info.name}</h5>
                                                    </div>
                                                </div>
                                            )}
                                        </div>
                                    </div>
                                </div>
                                <div className="flex-item fx-b50 pd-r-7">
                                    <div className="card panel">
                                        <div className="card-header panelHeader"><h5>Activities</h5>
                                            <select id="activities" value={this.state.durationActivityLineChartData} className="form-control" onChange={this.filterHandler}>
                                                <option value={1}>Last 1 Day</option>
                                                <option value={2}>Last 2 Days</option>
                                                <option value={3}>Last 3 Days</option>
                                                <option value={5}>Last 5 Days</option>
                                                <option value={7}>Last 7 Days</option>
                                            </select>
                                        </div>
                                        <div className="card-body panelBody" style={{ height: "400px" }}>
                                            {this.state.isFetchingLineChartData ?
                                                <div className="panelLoader">
                                                    <i className="fal fa-sync-alt fa-spin"></i>
                                                </div> :
                                                this.state.lineChartData.length ?
                                                    <AllChartsComponent config={this.createChart(this.state.lineChartData, "lineChartDiv", "lineChart", "simpleLineChart")} /> :
                                                    <div className="panelMessage">
                                                        <p><i className="far fa-exclamation-triangle"></i>No Activity yet !</p>
                                                    </div>
                                            }
                                        </div>
                                    </div>
                                </div>

                                <div className="flex-item fx-b50 pd-l-7">
                                    <div className="card panel">
                                        <div className="card-header panelHeader"><h5>Top Tenant Activities</h5>
                                            <select id="topTenantActivities" value={this.state.durationActivityBarChartData} className="form-control" onChange={this.filterHandler}>
                                                <option value={1}>Last 1 Day</option>
                                                <option value={2}>Last 2 Days</option>
                                                <option value={3}>Last 3 Days</option>
                                                <option value={5}>Last 5 Days</option>
                                                <option value={7}>Last 7 Days</option>
                                            </select>
                                        </div>
                                        <div className="card-body panelBody" style={{ height: "400px" }}>
                                            {this.state.isFetchingBarChartData ?
                                                <div className="panelLoader">
                                                    <i className="fal fa-sync-alt fa-spin"></i>
                                                </div> :
                                                this.state.barChartData.length ?
                                                    <AllChartsComponent config={this.createChart(this.state.barChartData, "barChartDiv", "barChart", "simpleBarChart")} />
                                                    :
                                                    <div className="panelMessage">
                                                        <p><i className="far fa-exclamation-triangle"></i>No Activity yet !</p>
                                                    </div>
                                            }
                                        </div>
                                    </div>
                                </div>
                            </div>



                        </div>
                    </React.Fragment>
                } */}
                <header className="content-header d-flex">
                    <div className="flex-60">
                        <div className="d-flex">
                            <h6>Tenant Details
                           </h6>
                        </div>
                    </div>
                    
                    <div className="flex-40 text-right">
                        <div className="content-header-group">
                            <button className="btn btn-light"  data-tooltip data-tooltip-text="Back" data-tooltip-place="bottom" onClick={()=>this.props.history.push('/tenantList')}><i className="fas fa-angle-left"></i></button>
                        </div>
                    </div>
                </header>
                
                    <div className="content-body">
                        {/* <ul className="list-style-none d-flex tenant-counter-list">
                            {this.state.counters.map((data, index) => (
                                <li key={index} onClick={() => { data.displayName === 'Tenants' && this.props.history.push('/tenantList') }}>
                                    <div className="tenant-counter-box">
                                        <h4>{data.count}</h4>
                                        <p>{data.displayName}</p>
                                        <div className="tenant-counter-icon">
                                            <i className={data.icon}></i>
                                        </div>
                                    </div>
                                </li>
                            ))}
                        </ul> */}
                        <div className="tenant-detail-card">
                            <div className="tenant-detail-card-body">
                                <span className="tenant-icon">
                                    <i className="far fa-building"></i>
                                </span>
                                <h6 className="text-theme">SakshiFlex</h6>
                                <p><strong>Description : </strong><span>N/A</span></p>
                                <p><strong>Type :</strong> <span className="text-green">FLEX</span></p>
                                <p><strong>User :</strong> <span className=" text-theme ">sakshi.yadav@83incs.com</span></p>
                               
                                <div className="date-box">
                                    <p>Renew Date - <span className="fw-600i "> 15/10/2020 </span> <span className="calender-icon float-right"><i className="far fa-calendar-alt"></i></span></p>
                                </div>
                            </div>
                            <ul className="list-style-none d-flex tenant-content-list">
                                <li>
                                    <div className="tenant-content">
                                        <h6 className="bg-light-blue"> Connectors <span className="float-right"><i className="fal fa-chart-network"></i></span></h6>
                                        <span className="tenant-content-detail" >
                                            <h6>Total</h6>
                                            <span className="alert alert-primary count-badge">5</span>
                                        </span>
                                        <span className="tenant-content-detail ">
                                            <h6>Used</h6>
                                            <span className="alert alert-success count-badge">2</span>
                                        </span>
                                    </div>
                                </li>
                                <li>
                                    <div className="tenant-content">
                                        <h6 className="bg-light-blue"> Transformations <span className="float-right"><i className="fal fa-exchange"></i></span></h6>
                                        <span className="tenant-content-detail" >
                                            <h6>Total</h6>
                                            <span className="alert alert-primary count-badge">5</span>
                                        </span>
                                        <span className="tenant-content-detail ">
                                            <h6>Used</h6>
                                            <span className="alert alert-success count-badge">2</span>
                                        </span>
                                    </div>
                                </li>
                                <li>
                                    <div className="tenant-content">
                                        <h6 className="bg-light-blue"> Js Functions <span className="float-right"><i className="fal fa-function"></i></span></h6>
                                        <span className="tenant-content-detail" >
                                            <h6>Total</h6>
                                            <span className="alert alert-primary count-badge">5</span>
                                        </span>
                                        <span className="tenant-content-detail ">
                                            <h6>Used</h6>
                                            <span className="alert alert-success count-badge">2</span>
                                        </span>
                                    </div>
                                </li>
                                <li>
                                    <div className="tenant-content">
                                        <h6 className=" bg-light-blue"> Pages <span className="float-right"><i className="fal fa-window"></i></span></h6>
                                        <span className="tenant-content-detail" >
                                            <h6>Total</h6>
                                            <span className="alert alert-primary count-badge">5</span>
                                        </span>
                                        <span className="tenant-content-detail ">
                                            <h6>Used</h6>
                                            <span className="alert alert-success count-badge">2</span>
                                        </span>
                                    </div>
                                </li>
                                <li>
                                    <div className="tenant-content">
                                        <h6 className="t bg-light-blue"> APIs <span className="float-right"><i className="fal fa-network-wired"></i></span></h6>
                                        <span className="tenant-content-detail" >
                                            <h6>Total</h6>
                                            <span className="alert alert-primary count-badge">5</span>
                                        </span>
                                        <span className="tenant-content-detail ">
                                            <h6>Used</h6>
                                            <span className="alert alert-success count-badge">2</span>
                                        </span>
                                    </div>
                                </li>

                            </ul>
                        </div>

                        <div className="d-flex tenant-usage-list">
                            <div className="flex-33 pd-r-10">
                                <div className="tenant-usage-content">
                                    <CircularProgressbar
                                        value={"20"}
                                        circleRatio={0.75}
                                        styles={buildStyles({
                                            rotation: 1 / 2 + 1 / 8,
                                            pathColor: 'rgba(0, 230, 115, 0.7)',
                                            trailColor: "#eee"
                                        })}
                                        className="tenant-usage-progress"
                                    />
                                    <h1>{""}<span>20%</span></h1>
                                    <h6>CPU Usage</h6>
                                </div>
                            </div>
                            <div className="flex-33 pd-r-10">
                                <div className="tenant-usage-content">
                                    <CircularProgressbar
                                        value={"10"}
                                        circleRatio={0.75}
                                        styles={buildStyles({
                                            rotation: 1 / 2 + 1 / 8,
                                            pathColor: 'rgba(255, 163, 26, 0.7)',
                                            trailColor: "#eee"
                                        })}
                                        className="tenant-usage-progress"
                                    />
                                    <h1>{""}<span>10%</span></h1>
                                    <h6>CPU Usage</h6>
                                </div>
                            </div>
                            <div className="flex-33 pd-r-10">
                                <div className="tenant-usage-content">
                                    <CircularProgressbar
                                        value={"40"}
                                        circleRatio={0.75}
                                        styles={buildStyles({
                                            rotation: 1 / 2 + 1 / 8,
                                            pathColor: 'rgba(51, 102, 255, 0.5)',
                                            trailColor: "#eee"
                                        })}
                                        className="tenant-usage-progress"
                                    />
                                    <h1>{""}<span>40%</span></h1>
                                    <h6>CPU Usage</h6>
                                </div>
                            </div>
                        </div>
                        <div className="d-flex pd-l-5 pd-r-5">
                            <div className="flex-50 pd-r-10">
                                <div className="card">
                                    <div className="card-header"><h6>Activities</h6>
                                        <div className="form-group card-header-filter"  >
                                            <select id="activities" className="form-control">
                                                <option value={1}>Last 1 Day</option>
                                                <option value={2}>Last 2 Days</option>
                                                <option value={3}>Last 3 Days</option>
                                                <option value={5}>Last 5 Days</option>
                                                <option value={7}>Last 7 Days</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div className="card-body">
                                        <div className="card-message">
                                            <p>There is no data to display !</p>
                                        </div>        
                                    </div>
                                </div>
                            </div>
                            <div className="flex-50 pd-l-10">
                                <div className="card">
                                    <div className="card-header"><h6>Top Tenant Activities</h6>
                                        <div className="form-group card-header-filter">
                                            <select id="topTenantActivities" className="form-control" >
                                                <option value={1}>Last 1 Day</option>
                                                <option value={2}>Last 2 Days</option>
                                                <option value={3}>Last 3 Days</option>
                                                <option value={5}>Last 5 Days</option>
                                                <option value={7}>Last 7 Days</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div className="card-body">
                                        <div className="card-message">
                                            <p>There is no data to display !</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                }
            </React.Fragment>
        );
    }
}

TenantDetails.propTypes = {
    dispatch: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
    tenantDetails: getTenantDetailsSuccess(),
    tenantDetailsError: getTenantDetailsError(),
    tenantSummary: getTenantSummarySuccess(),
    tenantSummaryError: getTenantSummaryError(),
    lineChartDataError: lineChartDataError(),
    lineChartData: lineChartData(),
    barChartDataError: barChartDataError(),
    barChartData: barChartData(),
    userDetails: userDetails(),
    userDetailsError: userDetailsError()

});

function mapDispatchToProps(dispatch) {
    return {
        dispatch,
        getUserDetails: () => dispatch(getUserDetails()),
        fetchTenantDetails: (id) => dispatch(fetchTenantDetails(id)),
        fetchTenantSummary: (id) => dispatch(fetchTenantSummary(id)),
        getActivityLineChartData: (id, durationInDays) => dispatch(getActivityLineChartData(id, durationInDays)),
        getActivityBarChartData: (id, durationInDays) => dispatch(getActivityBarChartData(id, durationInDays)),
    };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "tenantDetails", reducer });
const withSaga = injectSaga({ key: "tenantDetails", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(TenantDetails);
