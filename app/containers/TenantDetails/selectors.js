/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
 * Direct selector to the tenantDetails state domain
 */

const selectTenantDetailsDomain = state =>
  state.get("tenantDetails", initialState);

export const getTenantDetailsSuccess = () => createSelector(selectTenantDetailsDomain, substate => substate.tenantDetails);
export const getTenantDetailsError = () => createSelector(selectTenantDetailsDomain, substate => substate.tenantDetailsError);

export const getTenantSummarySuccess = () => createSelector(selectTenantDetailsDomain, substate => substate.tenantSummary);
export const getTenantSummaryError = () => createSelector(selectTenantDetailsDomain, substate => substate.tenantSummaryError);


export const lineChartData = () => createSelector(selectTenantDetailsDomain, subState => subState.lineChartData);

export const lineChartDataError = () => createSelector(selectTenantDetailsDomain, subState => subState.lineChartDataError);

export const barChartData = () => createSelector(selectTenantDetailsDomain, subState => subState.barChartData);

export const barChartDataError = () => createSelector(selectTenantDetailsDomain, subState => subState.barChartDataError);

export const userDetails = () => createSelector(selectTenantDetailsDomain, subState => subState.userDetails);

export const userDetailsError = () => createSelector(selectTenantDetailsDomain, subState => subState.userDetailsError);

export const isFetchingState = state => state.get('loader');

export const getIsFetching = () =>
  createSelector(isFetchingState, substate => substate.get('isFetching'));