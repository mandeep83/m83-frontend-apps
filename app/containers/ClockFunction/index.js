/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import React from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {compose} from "redux";

/* eslint-disable react/prefer-stateless-function */
export class ClockFunction extends React.Component {
    state = {
        hours: {
            first: 0,
            second: 0,
        },
        minutes: {
            first: 0,
            second: 0,
        },
        seconds: {
            first: 0,
            second: 0,
        },
    }

    componentWillMount() {
        let self = this;
        setInterval(self.updateTime, 1000)
    }
    componentWillUnmount() {
        const _self = this
        clearInterval(_self.updateTime)
    }

    updateTime = () => {
        let self = this;
        let last = new Date(0)
        last.setUTCHours(-1)
        let now = new Date

        let lastHours = last.getHours().toString()
        let nowHours = now.getHours().toString()
        if (lastHours !== nowHours) {
            self.updateContainer("hours", nowHours)
        }

        let lastMinutes = last.getMinutes().toString()
        let nowMinutes = now.getMinutes().toString()
        if (lastMinutes !== nowMinutes) {
            self.updateContainer("minutes", nowMinutes)
        }

        let lastSeconds = last.getSeconds().toString()
        let nowSeconds = now.getSeconds().toString()
        if (lastSeconds !== nowSeconds) {
            self.updateContainer("seconds", nowSeconds)
        }
        last = now
    }

    updateContainer(stateVar, newTime) {
        let time = newTime.split('')
        if (time.length === 1) {
            time.unshift('0')
        }
        let temp = this.state[stateVar]
        this.setState({
            [stateVar]: {
                "first": temp.first != time[0] ? time[0] : temp.first,
                "second": temp.second != time[1] ? time[1] : temp.second,
            }
        })
    }

    render() {

        return (
            <div className="clockWidget">
                <ul>
                    <li><div><h4>{this.state.hours.first}{this.state.hours.second}</h4></div></li>
                    <li><div><h4>{this.state.minutes.first}{this.state.minutes.second}</h4></div></li>
                    <li><div><h4>{this.state.seconds.first}{this.state.seconds.second}</h4></div></li>
                </ul>
            </div>
        );
    }
}


ClockFunction.propTypes = {
    dispatch: PropTypes.func.isRequired
};

function mapDispatchToProps(dispatch) {
    return {
        dispatch
    };
}

const withConnect = connect(
    null,
    mapDispatchToProps
);

export default compose(withConnect)(ClockFunction);
