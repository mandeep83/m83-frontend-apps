/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import {apiCallHandler} from '../../api';
import { takeEvery } from 'redux-saga';
import { put } from 'redux-saga/effects';
import {
    DATA_LIST_REQUEST,
    DATA_LIST_SUCCESS,
    DATA_LIST_FAILURE,
    DELETE_DATA_CONNECTOR,
    DELETE_DATA_CONNECTOR_SUCCESS,
    DELETE_DATA_CONNECTOR_FAILURE,
    GET_DATA_CONNECTORS,
    GET_DATA_CONNECTORS_SUCCESS,
    GET_DATA_CONNECTORS_FAILURE,
    GET_ALL_DEVICE_GROUPS,
    GET_ALL_DEVICE_GROUPS_SUCCESS,
    GET_ALL_DEVICE_GROUPS_FAILURE,
    ADD_OR_UPDATE_GROUP,
    ADD_OR_UPDATE_GROUP_SUCCESS,
    ADD_OR_UPDATE_GROUP_FAILURE,
    DELETE_GROUP_INITIATED,
    DELETE_GROUP_SUCCESS,
    DELETE_GROUP_FAILURE,
    GET_DEVELOPER_QUOTA,
    GET_DEVELOPER_QUOTA_SUCCESS,
    GET_DEVELOPER_QUOTA_FAILURE,
} from './constants'


export function* getEtlDataSourceList(action) {
  yield [apiCallHandler(action, DATA_LIST_SUCCESS, DATA_LIST_FAILURE, 'getConnectorsByCategory')];
}

export function* apiDeleteEtlDataSourceList(action) {
  yield [apiCallHandler(action, DELETE_DATA_CONNECTOR_SUCCESS, DELETE_DATA_CONNECTOR_FAILURE, 'deleteConnector')];
}

export function* apiGetDataConnectors(action) {
  yield [apiCallHandler(action, GET_DATA_CONNECTORS_SUCCESS, GET_DATA_CONNECTORS_FAILURE, 'getDataConnectors')];
}

export function* GetAllDeviceGroupsByConnectorIdHandler(action) {
  yield [apiCallHandler(action, GET_ALL_DEVICE_GROUPS_SUCCESS, GET_ALL_DEVICE_GROUPS_FAILURE, 'getDeviceGroups')];
}

export function* addOrUpdateGroupHandler(action) {
  yield [apiCallHandler(action, ADD_OR_UPDATE_GROUP_SUCCESS, ADD_OR_UPDATE_GROUP_FAILURE, 'addOrUpdateGroup')];
}

export function* deleteGroupHandler(action) {
  yield [apiCallHandler(action, DELETE_GROUP_SUCCESS, DELETE_GROUP_FAILURE, 'deleteClusterGroup')];
}

export function* getDeveloperQuota(action) {
    yield [apiCallHandler(action, GET_DEVELOPER_QUOTA_SUCCESS, GET_DEVELOPER_QUOTA_FAILURE, 'getDeveloperQuota')]
}


export function* watcherGetEtlDataSourceList() {
  yield takeEvery(DATA_LIST_REQUEST, getEtlDataSourceList);
}

export function* watcherGetDataConnectors() {
  yield takeEvery(GET_DATA_CONNECTORS, apiGetDataConnectors);
}

export function* watcherDeleteEtlDataSourceList() {
  yield takeEvery(DELETE_DATA_CONNECTOR, apiDeleteEtlDataSourceList);
}

export function* watcherGetAllDeviceGroupsByConnectorId() {
  yield takeEvery(GET_ALL_DEVICE_GROUPS, GetAllDeviceGroupsByConnectorIdHandler);
}

export function* watcherAddOrUpdateGroup() {
  yield takeEvery(ADD_OR_UPDATE_GROUP, addOrUpdateGroupHandler);
}

export function* watcherDeleteGroup() {
  yield takeEvery(DELETE_GROUP_INITIATED, deleteGroupHandler);
}

export function* watcherGetDeveloperQuota() {
    yield takeEvery(GET_DEVELOPER_QUOTA, getDeveloperQuota);
}

export default function* rootSaga() {
  yield [watcherGetEtlDataSourceList(),
    watcherDeleteEtlDataSourceList(),
    watcherGetDataConnectors(),
    watcherGetAllDeviceGroupsByConnectorId(),
    watcherAddOrUpdateGroup(),
    watcherDeleteGroup(),
    watcherGetDeveloperQuota(),
  ];
}