/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
 * Direct selector to the manageEtlDataSource state domain
 */

const selectManageEtlDataSourceDomain = state => state.get("manageEtlDataSource", initialState);

export const getConnectorsListSuccess = () => createSelector(selectManageEtlDataSourceDomain, subState => subState.connectorsListSuccess );
export const getConnectorsListError = () => createSelector(selectManageEtlDataSourceDomain, subState => subState.connectorsListError);

export const deleteSuccess = () => createSelector(selectManageEtlDataSourceDomain, subState => subState.deleteSuccess);
export const deleteFailure = () => createSelector(selectManageEtlDataSourceDomain, subState => subState.deleteFailure);

export const getConnectorsCategorySuccess = () => createSelector(selectManageEtlDataSourceDomain, subState => subState.getConnectorsCategorySuccess);
export const getConnectorsCategoryFailure = () => createSelector(selectManageEtlDataSourceDomain, subState => subState.getConnectorsCategoryFailure);

export const deviceGroupsSuccess = () => createSelector(selectManageEtlDataSourceDomain, subState => subState.deviceGroupsSuccess);
export const deviceGroupsFailure = () => createSelector(selectManageEtlDataSourceDomain, subState => subState.deviceGroupsFailure);

export const addOrUpdateGroupSuccess = () => createSelector(selectManageEtlDataSourceDomain, subState => subState.addOrUpdateGroupSuccess);
export const addOrUpdateGroupFailure = () => createSelector(selectManageEtlDataSourceDomain, subState => subState.addOrUpdateGroupFailure);

export const deleteGroupSuccess = () => createSelector(selectManageEtlDataSourceDomain, subState => subState.deleteGroupSuccess);
export const deleteGroupFailure = () => createSelector(selectManageEtlDataSourceDomain, subState => subState.deleteGroupFailure);


export const getDeveloperQuotaSuccess = () => createSelector(selectManageEtlDataSourceDomain, subState => subState.developerQuotaSuccess);
export const getDeveloperQuotaFailure = () => createSelector(selectManageEtlDataSourceDomain, subState => subState.developerQuotaFailure);