/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import ReactTable from 'react-table';
import ReactTooltip from 'react-tooltip'
import cloneDeep from "lodash/cloneDeep";
import ReactJson from "react-json-view";
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import MQTT from 'mqtt';
import {
    getConnectorsListByCategory,
    deleteConnectorById,
    getConnectorsCategory,
    getAllDeviceGroupsByConnectorId,
    addOrUpdateGroup,
    deleteGroup,
    getDeveloperQuota,
} from './actions';
import {
    getConnectorsListSuccess,
    getConnectorsListError,
    deleteFailure,
    deleteSuccess,
    getConnectorsCategorySuccess,
    getConnectorsCategoryFailure,
    deviceGroupsSuccess,
    deviceGroupsFailure,
    addOrUpdateGroupSuccess,
    addOrUpdateGroupFailure,
    deleteGroupSuccess,
    deleteGroupFailure,
    getDeveloperQuotaFailure,
    getDeveloperQuotaSuccess,
} from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import Loader from "../../components/Loader";
import NotificationModal from '../../components/NotificationModal/Loadable';
import { getVMQCredentials, convertTimestampToDate } from "../../commonUtils";
import SlidingPane from "react-sliding-pane";
import ReactSelect from "react-select";
import ListingTable from "../../components/ListingTable/Loadable";
import AddNewButton from "../../components/AddNewButton/Loadable";
import NoDataFoundMessage from "../../components/NoDataFoundMessage/Loadable"
import ConfirmModel from "../../components/ConfirmModel";
import Skeleton from "react-loading-skeleton";
let client;

let CONECTOR_CATEGORIES = {
    "MONGODB": "nosql databases",
    "S3": "cloud storage",
}

/* eslint-disable react/prefer-stateless-function */
export class ManageEtlDataSource extends React.Component {
    state = {
        connectorsList: [],
        groups: [],
        allDevices: [],
        isFetching: true,
        showForm: false,
        selectedDataSource: '',
        dataConnectorType: '',
        connectorsCategory: [],
        selectedDataSourceItems: [],
        selectedConnector: {
            properties: {
                topics: []
            }
        },
        dataView: 'FILE',
        selectedConnectorId: null,
        showSniffingModal: false,
        showClustersView: false,
        isFetchingGroups: false,
        showGroupDetails: false,
        deleteGroupConfirmation: false,
        filteredDataList: [],
        searchTerm: '',
        remainingConnectorsCount: 0,
        usedConnectorsCount: 0,
        totalConnectorsCount: 0,
        toggleView: true
    }

    componentWillMount() {
        if (localStorage.tenantType === "SAAS") {
            this.props.getConnectorsListByCategory("MQTT");
        } else {
            this.props.getConnectorsCategory();
        }
        let payload = [
            {
                dependingProductId: null,
                productName: "connectors",
            }
        ];
        this.props.getDeveloperQuota(payload)
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.connectorsList && nextProps.connectorsList !== this.props.connectorsList) {
            let connectorsList = nextProps.connectorsList, filteredDataList;
            if (this.props.match.params.connector === 'MQTT') {
                connectorsList = connectorsList.filter(connector => connector.isExternal);
            }
            filteredDataList = connectorsList
            if (this.state.searchTerm != "") {
                filteredDataList = connectorsList.filter(item => item.name.toLowerCase().includes(this.state.searchTerm.toLowerCase()))
            }
            this.setState({
                connectorsList,
                filteredDataList,
                isFetching: false,
            })
        }
        if (nextProps.connectorsListError && nextProps.connectorsListError !== this.props.connectorsListError) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.connectorsListError,
                isFetching: false,
            })
        }
        if (nextProps.getConnectorsCategorySuccess && nextProps.getConnectorsCategorySuccess !== this.props.getConnectorsCategorySuccess) {
            if (this.props.match.params.connector) {
                this.props.getConnectorsListByCategory(this.props.match.params.connector);
            }
            this.setState({
                connectorsCategory: nextProps.getConnectorsCategorySuccess.map(item => {
                    item.connectors = item.connectors.sort((x, y) => y.available - x.available);
                    return item;
                }),
                isFetching: Boolean(this.props.match.params.connector),
            })
        }
        if (nextProps.getConnectorsCategoryFailure && nextProps.getConnectorsCategoryFailure !== this.props.getConnectorsCategoryFailure) {
            if (this.props.match.params.connector) {
                this.props.getConnectorsListByCategory(this.props.match.params.connector);
            }
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.getConnectorsCategoryFailure,
                isFetching: Boolean(this.props.match.params.connector),
            })
        }
        if (nextProps.deleteConnectorSuccess && nextProps.deleteConnectorSuccess !== this.props.deleteConnectorSuccess) {
            let etlDataSourceList = JSON.parse(JSON.stringify(this.state.connectorsList));
            etlDataSourceList = etlDataSourceList.filter(val => val.id !== nextProps.deleteConnectorSuccess.id)
            this.setState({
                connectorsList: etlDataSourceList,
                filteredDataList: etlDataSourceList,
                modalType: "success",
                isOpen: true,
                message2: nextProps.deleteConnectorSuccess.message,
                remainingConnectorsCount: this.state.remainingConnectorsCount + 1,
                usedConnectorsCount: this.state.usedConnectorsCount - 1,
            }, () => this.props.getConnectorsCategory())
        }
        if (nextProps.deleteConnectorFailure && nextProps.deleteConnectorFailure !== this.props.deleteConnectorFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.deleteConnectorFailure,
                isFetching: false,
            })
        }
        if (nextProps.deviceGroupsSuccess && nextProps.deviceGroupsSuccess !== this.props.deviceGroupsSuccess) {
            this.setState({
                groups: nextProps.deviceGroupsSuccess,
                showGroupDetails: false,
                isFetchingGroups: false,
            })
        }
        if (nextProps.deviceGroupsFailure && nextProps.deviceGroupsFailure !== this.props.deviceGroupsFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.deviceGroupsFailure,
                isFetchingGroups: false,
            })
        }
        if (nextProps.addOrUpdateGroupSuccess && nextProps.addOrUpdateGroupSuccess !== this.props.addOrUpdateGroupSuccess) {
            this.props.getAllDeviceGroupsByConnectorId(this.state.selectedConnectorId);
        }
        if (nextProps.addOrUpdateGroupFailure && nextProps.addOrUpdateGroupFailure !== this.props.addOrUpdateGroupFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.addOrUpdateGroupFailure,
                isFetchingGroups: false,
            })
        }
        if (nextProps.deleteGroupSuccess && nextProps.deleteGroupSuccess !== this.props.deleteGroupSuccess) {
            this.props.getAllDeviceGroupsByConnectorId(this.state.selectedConnectorId);
        }
        if (nextProps.deleteGroupFailure && nextProps.deleteGroupFailure !== this.props.deleteGroupFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.deleteGroupFailure,
                isFetchingGroups: false,
            })
        }
        if (nextProps.developerQuotaSuccess && nextProps.developerQuotaSuccess !== this.props.developerQuotaSuccess) {
            const findObject = nextProps.developerQuotaSuccess.find(product => product.productName === "connectors")
            let remainingConnectorsCount = findObject['remaining'],
                totalConnectorsCount = findObject['total'],
                usedConnectorsCount = findObject['used'];
            this.setState({
                remainingConnectorsCount,
                totalConnectorsCount,
                usedConnectorsCount
            });
        }

        if (nextProps.developerQuotaFailure && nextProps.developerQuotaFailure !== this.props.developerQuotaFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.developerQuotaFailure,
            })
        }
    }

    createMqttConnection = (selectedTopic) => {
        let brokerURL = window.location.origin === "http://localhost:3001" ? window.API_URL.split("//")[1].substring(0, window.API_URL.split("//")[1].length - 1) : window.location.host,
            count = 0,
            credentials = getVMQCredentials();
        const options = {
            host: brokerURL,
            protocol: 'wss',
            path: '/connectmqtt',
            username: credentials.userName,
            password: credentials.password,
        };
        client = MQTT.connect(options);
        client.on('connect', function () {
        });
        client.on('close', function (response) {
            if (response && count > 4) {
                client.end();
            }
        });
        client.on('reconnect', function () {
            count++;
        });
        this.subscribeMqttTopic(selectedTopic)
    }

    subscribeMqttTopic = (selectedTopic) => {
        this.state.previousTopic && client.unsubscribe(`${this.state.previousTopic}`);
        let _this = this,
            allTopics = JSON.parse(JSON.stringify(this.state.allTopics));
        client.subscribe(selectedTopic, function (err) {
            if (!err) {
                client.on('message', function (topic, message) {

                    allTopics.map(topic => {
                        if (topic.topicName === selectedTopic) {
                            topic.messages.unshift({
                                name: selectedTopic,
                                time: new Date().toLocaleString('en-US', { hour12: false, timeZone: localStorage.timeZone }),
                                message: JSON.stringify(JSON.parse(message.toString()), undefined, 2),
                            });
                            topic.messages.length == 60 ? topic.messages.splice(59) : ''
                        }
                    })
                    if (!_this.state.isHoldData) {
                        _this.setState({
                            allTopics,
                        })
                    }
                });
            }
        })
    }

    onCloseHandler = () => {
        this.setState({
            isOpen: false,
            message2: '',
            modalType: "",
        });
    }

    cancelClicked = () => {
        this.setState({
            deleteName: "",
            connectorToBeDeleted: null,
            confirmState: false
        })
    }

    showSourcesTable = (connectorName, connectorType) => {
        let selectedDataSourceItems = this.state.connectorsList.filter(source => source.sourceType === connectorType.toLowerCase());
        this.setState({ selectedDataSource: connectorName, showSourcesTable: true, selectedDataSourceItems, dataConnectorType: connectorType })
    }

    closeEtlWindow = (updatedSuccessfully) => {
        this.setState(
            { showAddOrEditDataSourceSection: false, isFetching: updatedSuccessfully ? true : false }, () => {
                if (updatedSuccessfully) {
                    this.props.getConnectorsListByCategory();
                    this.props.getConnectorsCategory()
                }
            })
    }

    showModal = (selectedConnector) => {
        let selectedTopic = `${selectedConnector.properties.topics[0].name}/+/report`,
            allTopics = [];

        selectedConnector.properties.topics.map(topic => {
            let reportTopic = {
                connectorName: selectedConnector.name,
                topicName: `${topic.name}/+/report`,
                messages: []
            },
                controlTopic = {
                    connectorName: selectedConnector.name,
                    topicName: `${topic.name}/+/control`,
                    messages: []
                };
            allTopics.push(reportTopic);
            allTopics.push(controlTopic);

        });

        if (client && client.connected) {
            client.end();
        }
        this.setState({
            selectedTopic,
            allTopics,
            showSniffingModal: true,
        }, () => {
            this.createMqttConnection(selectedTopic);
        });
    }

    copyToClipboard = (id) => {
        var copyText = document.getElementById(id);
        copyText.select();
        document.execCommand("copy");
        let element = $(`#Copied${id[id.length - 1]}`)[0]
        ReactTooltip.show(element)
        setTimeout(() => ReactTooltip.hide(element), 2500)
    }

    clearSniffingModal = () => {
        let allTopics = cloneDeep(this.state.allTopics);
        allTopics.map(topic => topic.messages = [])
        this.setState({
            allTopics
        }, () => this.subscribeMqttTopic(this.state.selectedTopic));
    }

    closeSniffingModal = () => {
        client.unsubscribe(this.state.selectedTopic);
        this.setState({
            allTopics: [],
            showSniffingModal: false,
            previousTopic: ''
        })
    }

    fetchConnectorListByCategory = (category) => {
        this.setState({
            isFetching: true,
            searchTerm: ""
        }, () => {
            this.props.history.push(`/connectors/${category}`);
            this.props.getConnectorsListByCategory(category);
        })
    }

    tabChangeHandler = (selectedTopic) => {
        let previousTopic = this.state.selectedTopic;
        this.setState({
            selectedTopic,
            previousTopic,
        }, () => this.subscribeMqttTopic(selectedTopic))
    }

    showClusterView = (connectorId) => {
        let allDevices = this.state.connectorsList.filter(connector => connector.id === connectorId)[0].properties.deviceIds;
        this.setState({
            allDevices,
            selectedConnectorId: connectorId,
            isFetchingGroups: true,
            showClustersView: true,
        }, () => { this.props.getAllDeviceGroupsByConnectorId(connectorId) })
    }

    getGroupsColumns = () => {
        let columns = [
            {
                Header: 'Group Name',
                accessor: "deviceGroupName",
            },
            {
                Header: 'No of Devices',
                Cell: row => {
                    return (
                        <span className="text-primary">
                            {row.original.attachedDevices.length}
                        </span>
                    )
                },
            },
            {
                Header: 'Actions',
                Cell: row => {
                    return (
                        <div className="button-group">
                            <button className="btn btn-transparent btn-transparent-success" data-tip data-for={"details" + row.original.deviceGroupId}
                                onClick={() => { this.showGroupDetails(row.original.deviceGroupId) }}>
                                <i className="fas fa-eye"></i>
                            </button>
                            <ReactTooltip id={"details" + row.original.deviceGroupId} place="bottom" type="dark">
                                <div className="tooltiptext"><p>Details</p></div>
                            </ReactTooltip>
                            <button className="btn btn-transparent btn-transparent-danger" data-tip data-for={"deleteGroup" + row.original.deviceGroupId}
                                onClick={() => { this.setState({ deleteGroupConfirmation: true, selectedDeviceGroupId: row.original.deviceGroupId }) }}>
                                <i className="fas fa-trash-alt"></i>
                            </button>
                            <ReactTooltip id={"deleteGroup" + row.original.deviceGroupId} place="bottom" type="dark">
                                <div className="tooltiptext"><p>Delete</p></div>
                            </ReactTooltip>
                        </div>
                    )
                },
            }
        ]
        return columns;
    }

    showGroupDetails = (selectedDeviceGroupId) => {
        let groups = cloneDeep(this.state.groups),
            selectedGroupDetails = groups.filter(group => group.deviceGroupId === selectedDeviceGroupId)[0];
        this.setState({
            selectedDeviceGroupId,
            selectedGroupDetails,
            showGroupDetails: true
        });
    }

    getDeviceColumns = () => {
        let columns = [
            {
                Header: 'Device Name/Id',
                Cell: row => {
                    return (
                        <span>{row.original.name ? row.original.name : row.original._uniqueDeviceId}</span>
                    )
                },
            },
            {
                Header: 'Tag Name',
                Cell: row => (row.original.isEdit ?
                    <input type="text" className="form-control" id="name" value={row.original.tag || ''} onChange={(e) => this.addTagHandler(e, row.original._uniqueDeviceId)}></input>
                    : row.original.tag || ''
                )
            },
            {
                Header: 'Actions',
                accessor: "name",
                Cell: row => (
                    <div className="button-group">
                        {row.original.isLoading ? "Loading..." : row.original.isEdit ?
                            <React.Fragment>
                                <button type="button" className="btn btn-transparent btn-transparent-success" data-tip
                                    data-for={"saveTag" + row.original._uniqueDeviceId}
                                    onClick={() => this.editDeviceTagCancel(row.original._uniqueDeviceId)}>
                                    <i className="fas fa-check"></i>
                                </button>
                                <ReactTooltip id={"saveTag" + row.original._uniqueDeviceId} place="bottom" type="dark">
                                    <div className="tooltiptext"><p>Save</p></div>
                                </ReactTooltip>
                                <button type="button" className="btn btn-transparent btn-transparent-dark" data-tip
                                    data-for={"cancelEdit" + row.original._uniqueDeviceId}
                                    onClick={() => this.editDeviceTagCancel(row.original._uniqueDeviceId)} >
                                    <i className="fas fa-times"></i>
                                </button>
                                <ReactTooltip id={"cancelEdit" + row.original._uniqueDeviceId} place="bottom" type="dark" >
                                    <div className="tooltiptext"><p>Cancel</p></div>
                                </ReactTooltip>
                            </React.Fragment> :
                            <React.Fragment>
                                <button type="button" className="btn btn-transparent btn-transparent-primary" data-tip
                                    data-for={"editTag" + row.original._uniqueDeviceId}
                                    onClick={() => this.editDeviceTag(row.original._uniqueDeviceId)}>
                                    <i className="fas fa-pencil"></i>
                                </button>
                                <ReactTooltip id={"editTag" + row.original._uniqueDeviceId} place="bottom" type="dark">
                                    <div className="tooltiptext"><p>Edit</p></div>
                                </ReactTooltip>
                                <button type="button" className="btn btn-transparent btn-transparent-danger" data-tip
                                    data-for={"deleteDevice" + row.original._uniqueDeviceId}
                                    onClick={() => this.deleteDeviceFromGroup(row.original._uniqueDeviceId)}>
                                    <i className="fas fa-trash-alt"></i>
                                </button>
                                <ReactTooltip id={"deleteDevice" + row.original._uniqueDeviceId} place="bottom"
                                    type="dark">
                                    <div className="tooltiptext"><p>Delete</p></div>
                                </ReactTooltip>
                            </React.Fragment>
                        }
                    </div>
                ),
            }
        ];
        return columns;
    }

    deleteDeviceFromGroup = (deviceId) => {
        let selectedGroupDetails = cloneDeep(this.state.selectedGroupDetails);
        selectedGroupDetails.attachedDevices = selectedGroupDetails.attachedDevices.filter(device => device._uniqueDeviceId !== deviceId);
        this.setState({
            selectedGroupDetails
        })
    }

    editDeviceTag = (deviceId) => {
        let selectedGroupDetails = cloneDeep(this.state.selectedGroupDetails);
        selectedGroupDetails.attachedDevices.filter(device => device._uniqueDeviceId === deviceId)[0].isEdit = true;
        this.setState({
            selectedGroupDetails
        })
    }

    editDeviceTagCancel = (deviceId) => {
        let selectedGroupDetails = cloneDeep(this.state.selectedGroupDetails);
        selectedGroupDetails.attachedDevices.filter(device => device._uniqueDeviceId === deviceId)[0].isEdit = false;
        this.setState({
            selectedGroupDetails
        })
    }
    
    addTagHandler = (event, deviceId) => {
        let selectedGroupDetails = cloneDeep(this.state.selectedGroupDetails);
        selectedGroupDetails.attachedDevices.filter(device => device._uniqueDeviceId === deviceId)[0].tag = event.target.value;
        this.setState({
            selectedGroupDetails
        })
    }

    groupDetailsChangeHandler = (event) => {
        let selectedGroupDetails = cloneDeep(this.state.selectedGroupDetails);
        selectedGroupDetails[event.target.name] = event.target.value;
        this.setState({
            selectedGroupDetails
        });
    }

    addNewGroupHandler = () => {
        let selectedGroupDetails = {
            deviceGroupName: '',
            description: '',
            connectorId: this.state.selectedConnectorId,
            attachedDevices: [],
            newlyAttachedDevices: [],
        }
        this.setState({
            showGroupDetails: true,
            selectedGroupDetails
        });
    }

    getAvailableDeviceOptions = () => {
        let allDevices = this.state.allDevices,
            attachedDevices = this.state.selectedGroupDetails.attachedDevices,
            options = [];
        allDevices.map(device => {
            if (attachedDevices.filter(attachedDevice => attachedDevice._uniqueDeviceId === device.deviceId).length == 0) {
                options.push({
                    label: device.name ? device.name : device.deviceId,
                    value: device.deviceId,
                })
            }
        })
        return options;
    }

    addDeviceToGroupHandler = (data) => {
        let selectedGroupDetails = cloneDeep(this.state.selectedGroupDetails);
        selectedGroupDetails.newlyAttachedDevices = data;
        this.setState({
            selectedGroupDetails
        });
    }

    addOrUpdateGroupDetails = () => {
        let payload = cloneDeep(this.state.selectedGroupDetails),
            deviceIds = [];
        payload.attachedDevices.map(device => {
            deviceIds.push({
                id: device._uniqueDeviceId,
                tag: device.tag,
            });
        })
        if (payload.newlyAttachedDevices && payload.newlyAttachedDevices.length > 0) {
            payload.newlyAttachedDevices.map(device => {
                deviceIds.push(
                    device.value);
            })
        }
        payload.deviceIds = deviceIds;
        delete payload.attachedDevices;
        delete payload.newlyAttachedDevices;

        this.setState({
            isFetchingGroups: true,
        }, () => { this.props.addOrUpdateGroup(payload.deviceGroupId, payload) })
    }

    onSearchHandler = (e) => {
        let filterList = JSON.parse(JSON.stringify(this.state.connectorsList))
        if (e.target.value.length > 0) {
            filterList = filterList.filter(item => item.name.toLowerCase().includes(e.target.value.toLowerCase()))
        }
        this.setState({
            filteredDataList: filterList,
            searchTerm: e.target.value
        })
    }

    emptySearchBox = () => {
        let filteredDataList = JSON.parse(JSON.stringify(this.state.connectorsList));
        this.setState({
            searchTerm: '',
            filteredDataList
        })
    }

    refreshList = () => {
        this.setState({
            isFetching: true
        }, () => {
            if (localStorage.tenantType === "SAAS") {
                this.props.getConnectorsListByCategory("MQTT");
            } else {
                this.props.getConnectorsCategory();
            }
        })
    }

    getColumns = () => {
        return [
            {
                Header: "Name", width: 30,
                cell: (row) => (
                    <React.Fragment>
                        <div className="list-view-icon">
                            {row.image ?
                                <img src={row.image} /> :
                                <i className="fad fa-router"></i>
                            }
                        </div>
                        <div className="list-view-icon-box">
                            <h6 className="text-theme fw-600">{row.name}</h6>
                            <p>{row.description}</p>
                        </div>
                    </React.Fragment>
                )
            },
            {
                Header: "Connector Type", width: 15,
                cell: (row) => (
                    row.source ? <h6 className="text-red">Source</h6> : <h6 className="text-green">Sink</h6>
                )
            },
            {
                Header: "Connection Type", width: 15,
                cell: (row) => (
                    row.isExternal ?
                        <h6><i className="far fa-arrow-to-top mr-r-10 text-primary"></i>External</h6> :
                        <h6><i className="far fa-arrow-to-bottom mr-r-10 text-success"></i>Internal</h6>
                )
            },
            { Header: "Created", width: 15, accessor: "createdAt" },
            { Header: "Updated", width: 15, accessor: "updatedAt" },
            {
                Header: "Actions", width: 10,
                cell: (row) => (
                    <div className="button-group">
                        <button className="btn-transparent btn-transparent-blue" data-tooltip="true" data-tooltip-text="Edit" data-tooltip-place="bottom" disabled={row.category == "MQTT" && !row.isExternal} onClick={() => this.navigateToAddorEditConnector(row.id)}>
                            <i className="far fa-pencil"></i>
                        </button>
                        <button className="btn-transparent btn-transparent-red" data-tooltip="true" data-tooltip-text="Delete" data-tooltip-place="bottom" disabled={row.category == "MQTT" && !row.isExternal} onClick={() => this.setState({
                            deleteName: row.name,
                            connectorToBeDeleted: row.id,
                            confirmState: true,
                            connectorName: row.name
                        })}><i className="far fa-trash-alt"></i>
                        </button>
                    </div>
                )
            },
        ]
    }

    navigateToAddorEditConnector = (connectorId) => {
        let selectedConnectorType = this.props.match.params.connector;
        let pathname = connectorId ? `/addOrEditConnector/${selectedConnectorType}/${connectorId}` : `/addOrEditConnector/${selectedConnectorType}`;
        let requiredConnectorDetails = this.state.connectorsCategory.find(category => category.type === CONECTOR_CATEGORIES[selectedConnectorType]).connectors.find(connector => connector.connector === selectedConnectorType);
        let state = {
            connectorImage: requiredConnectorDetails.imageName,
            connectorDescription: requiredConnectorDetails.description,
        }
        this.props.history.push({ pathname, state })
    }

    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>Connectors</title>
                    <meta name="description" content="Description of Connectors" />
                </Helmet>

                <header className="content-header d-flex">
                    <div className="flex-60">
                        {this.props.match.params.connector ?
                            <div className="d-flex">
                                <h6 className="previous" onClick={() => this.props.history.push('/connectors')}>Connectors</h6>
                                <h6 className="active">{this.props.match.params.connector}</h6>
                                <h6>
                                    <span className="content-header-badge-group">
                                        <span className="content-header-badge-item">Allocated <span className="badge badge-pill badge-primary">{this.state.totalConnectorsCount}</span></span>
                                        <span className="content-header-badge-item">Used <span className="badge badge-pill badge-success">{this.state.usedConnectorsCount}</span></span>
                                        <span className="content-header-badge-item">Remaining <span className="badge badge-pill badge-warning">{this.state.remainingConnectorsCount}</span></span>
                                    </span>
                                </h6>
                            </div> :
                            <h6>Connectors -
                            <span className="content-header-badge-group">
                                    <span className="content-header-badge-item">Allocated <span className="badge badge-pill badge-primary">{this.state.totalConnectorsCount}</span></span>
                                    <span className="content-header-badge-item">Used <span className="badge badge-pill badge-success">{this.state.usedConnectorsCount}</span></span>
                                    <span className="content-header-badge-item">Remaining <span className="badge badge-pill badge-warning">{this.state.remainingConnectorsCount}</span></span>
                                </span>
                            </h6>
                        }
                    </div>
                    <div className="flex-40 text-right">
                        {this.props.match.params.connector &&
                            <div className="content-header-group">
                                <div className="search-box">
                                    <span className="search-icon"><i className="far fa-search"></i></span>
                                    <input type="text" className="form-control" placeholder="Search..." value={this.state.searchTerm} onChange={this.onSearchHandler} disabled={this.state.connectorsList.length === 0 || this.state.isFetching} />
                                    {this.state.searchTerm.length > 0 &&
                                        <button className="search-button" onClick={() => this.emptySearchBox()}><i className="far fa-times"></i></button>
                                    }
                                </div>
                                <button className={this.state.toggleView == true ? "btn btn-light active" : "btn btn-light"} disabled={this.state.isFetching} data-tooltip="true" data-tooltip-text="List View" data-tooltip-place="bottom" onClick={() => this.setState({ toggleView: true })}><i className="fad fa-list-ul"></i></button>
                                <button className={this.state.toggleView == false ? "btn btn-light active" : "btn btn-light"} disabled={this.state.isFetching} data-tooltip="true" data-tooltip-text="Card View" data-tooltip-place="bottom" onClick={() => this.setState({ toggleView: false })}><i className="fad fa-table"></i></button>
                                {this.props.match.params.connector != "MQTT" &&
                                    <button className="btn btn-light" data-tooltip data-tooltip-text="Refresh" data-tooltip-place="bottom" disabled={this.state.isFetching} onClick={() => this.refreshList()} ><i className="far fa-sync-alt"></i></button>
                                }
                                {this.props.match.params.connector != "MQTT" &&
                                    <button type="button" className="btn btn-primary" data-tooltip data-tooltip-text={`Add ${this.props.match.params.connector} Connector`} data-tooltip-place="bottom" disabled={this.state.remainingConnectorsCount === 0 || this.state.isFetching} onClick={() => this.navigateToAddorEditConnector()}>
                                        <i className="far fa-plus"></i>
                                    </button>
                                }
                            </div>
                        }
                    </div>
                </header>
                

                <div className="content-body">
                    {this.state.isFetching ?
                        <React.Fragment>
                            {this.props.match.params.connector ?
                                < Skeleton count={9} className="skeleton-list-loader" />
                                :
                                < Skeleton count={27} className="skeleton-connector-loader" />
                            }
                        </React.Fragment>
                        :
                        <React.Fragment>
                            {this.props.match.params.connector ?
                                this.state.connectorsList.length > 0 ?
                                    this.state.filteredDataList.length > 0 ?
                                        this.state.toggleView ?
                                            <ListingTable
                                                columns={this.getColumns()}
                                                data={this.state.filteredDataList}
                                            />
                                            :
                                            <ul className="card-view-list">
                                                {this.state.connectorsList.map((item, index) =>
                                                    <li key={index}>
                                                        <div className="card-view-box">
                                                            <div className="card-view-header">
                                                                {item.source ?
                                                                    <span className="alert alert-danger mr-r-7"><i className="fad fa-plug mr-2"></i>Source</span> :
                                                                    <span className="alert alert-success mr-r-7"><i className="fad fa-plug mr-2"></i>Sink</span>
                                                                }
                                                                {item.isExternal ?
                                                                    <span className="alert alert-secondary text-primary bg-white"><i className="fas fa-arrow-to-top mr-2"></i>External</span> :
                                                                    <span className="alert alert-secondary text-success bg-white"><i className="fas fa-arrow-to-bottom mr-2"></i>Internal</span>
                                                                }
                                                                <div className="dropdown">
                                                                    <button className="btn-transparent btn-transparent-gray" type="button" data-toggle="dropdown">
                                                                        <i className="fas fa-ellipsis-v"></i>
                                                                    </button>
                                                                    <div className="dropdown-menu">
                                                                        <button className="dropdown-item" disabled={item.category == "MQTT" && !item.isExternal} onClick={() => this.navigateToAddorEditConnector(item.id)}>
                                                                            <i className="far fa-pencil"></i>Edit
                                                                        </button>
                                                                        <button className="dropdown-item" disabled={item.category == "MQTT" && !item.isExternal} onClick={() => this.setState({
                                                                            deleteName: item.name,
                                                                            connectorToBeDeleted: item.id,
                                                                            confirmState: true,
                                                                            connectorName: item.name
                                                                        })}><i className="far fa-trash-alt"></i>Delete
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="card-view-body">
                                                                <div className="card-view-icon">
                                                                    {item.image ?
                                                                        <img src={item.image} /> :
                                                                        <i className="fad fa-router"></i>
                                                                    }
                                                                </div>
                                                                <h6><i className="far fa-address-book"></i>{item.name ? item.name : "-"}</h6>
                                                                <p><i className="far fa-file-alt"></i>{item.description ? item.description : "-"}</p>
                                                            </div>
                                                            <div className="card-view-footer d-flex">
                                                                <div className="flex-50 p-3">
                                                                    <h6>Created</h6>
                                                                    <p><strong>By - </strong>{item.createdBy}</p>
                                                                    <p><strong>At - </strong>{new Date(item.createdAt).toLocaleString('en-US', { createdAt: localStorage.createdAt })}</p>
                                                                </div>
                                                                <div className="flex-50 p-3">
                                                                    <h6>Updated</h6>
                                                                    <p><strong>By - </strong>{item.updatedBy}</p>
                                                                    <p><strong>At - </strong>{new Date(item.updatedAt).toLocaleString('en-US', { createdAt: localStorage.createdAt })}</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                )}
                                            </ul>
                                        :
                                        <NoDataFoundMessage />
                                    :
                                    <AddNewButton
                                        text1="No connectors available"
                                        text2="You haven't created any connector yet"
                                        createItemOnAddButtonClick={() => this.navigateToAddorEditConnector()}
                                        imageIcon="addDeviceType.png"
                                        addButtonEnable={this.props.match.params.connector != "MQTT"}
                                    />
                                :
                                <div className="d-flex">
                                    {this.state.connectorsCategory.length > 0 && this.state.connectorsCategory.map((connector, index) => {
                                        return (
                                            <React.Fragment key={index}>
                                                {connector.connectors.length && connector.connectors.map((connectorType, id) => {
                                                    return (
                                                        <div className={`connector-card ${!connectorType.available && "disabled"}`} key={id} onClick={() => connectorType.available && this.fetchConnectorListByCategory(connectorType.connector)} >
                                                            <div className={`connector-card-image ${!connectorType.available && "disabled"}`}>
                                                                {!connectorType.available ? <div className="connector-card-image-overlay"></div> : ""}
                                                                <img src={`https://content.iot83.com/m83/dataConnectors/${connectorType.imageName}`} />
                                                            </div>
                                                            <h5 className="f-12 m-0">{connectorType.displayName}</h5>
                                                        </div>
                                                    )
                                                })}
                                            </React.Fragment>
                                        )
                                    })}
                                </div>
                            }
                        </React.Fragment>
                    }
                </div>

                <SlidingPane
                    className=''
                    overlayClassName='slidingFormOverlay'
                    closeIcon={<div></div>}
                    isOpen={this.state.showClustersView}
                    from='right'
                    width='550px'
                >
                    <div className="modal-content">
                        <div className="modal-header">
                            <h6 className="modal-title">Cluster Details
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={() => { this.setState({ showClustersView: false }) }}><i className="far fa-angle-right"></i></button>
                                <button type="button" className="btn btn-link pd-0 float-right" onClick={() => { this.addNewGroupHandler() }}><i className="far fa-plus"></i>Add New Cluster</button>
                            </h6>
                        </div>
                        {this.state.isFetchingGroups ?
                            <div className="modal-loader">
                                <i className="fad fa-sync-alt fa-spin text-primary"></i>
                            </div> :
                            <React.Fragment>
                                {this.state.showGroupDetails ?
                                    <React.Fragment>
                                        <div className="modal-body position-relative">
                                            <div className="form-group">
                                                <input type="text" id="deviceGroupName" name="deviceGroupName" className="form-control" placeholder="ParamName" className="form-control" value={this.state.selectedGroupDetails.deviceGroupName} onChange={this.groupDetailsChangeHandler} required />
                                                <label className="form-group-label">Name :
                                                <span className="requiredMark"><i className="fa fa-asterisk" /></span>
                                                </label>
                                            </div>
                                            <div className="form-group">
                                                <textarea rows="2" type="text" id="description" name="description" placeholder="description" className="form-control" value={this.state.selectedGroupDetails.description} onChange={this.groupDetailsChangeHandler} />
                                                <label className="form-group-label">Description :</label>
                                            </div>
                                            <div className="form-group">
                                                <label className="form-label">Add Devices :</label>
                                                <ReactSelect
                                                    className="form-control-multi-select"
                                                    isMulti={true}
                                                    options={this.getAvailableDeviceOptions()}
                                                    onChange={this.addDeviceToGroupHandler}
                                                    value={this.state.selectedGroupDetails.newlyAttachedDevices}
                                                >
                                                </ReactSelect>
                                            </div>
                                            <ReactTable
                                                columns={this.getDeviceColumns()}
                                                data={this.state.selectedGroupDetails.attachedDevices}
                                                className="customReactTable customReactTableBorder"
                                                defaultPageSize={5}
                                                sortable={true}
                                                PreviousComponent={(props) => <button type="button"{...props}><i className="fal fa-angle-left"></i></button>}
                                                NextComponent={(props) => <button type="button" {...props}><i className="fal fa-angle-right"></i></button>}
                                            />
                                        </div>
                                        <div className="modal-footer">
                                            <button className="btn btn-success" onClick={() => { this.addOrUpdateGroupDetails() }}>Save</button>
                                            <button type="button" onClick={() => this.setState({ showGroupDetails: false })} className="btn btn-dark">Back</button>
                                        </div>
                                    </React.Fragment> :
                                    <React.Fragment>
                                        <div className="modal-body position-relative">
                                            <ReactTable
                                                columns={this.getGroupsColumns()}
                                                data={this.state.groups}
                                                className="customReactTable customReactTableBorder"
                                                defaultPageSize={5}
                                                sortable={true}
                                                PreviousComponent={(props) => <button type="button"{...props}><i
                                                    className="fal fa-angle-left"></i></button>}
                                                NextComponent={(props) => <button type="button" {...props}><i
                                                    className="fal fa-angle-right"></i></button>}
                                            />
                                        </div>
                                        <div className="modal-footer">
                                            <button type="button" onClick={() => { this.setState({ showClustersView: false }) }} className="btn btn-dark">Cancel</button>
                                        </div>
                                    </React.Fragment>
                                }
                            </React.Fragment>
                        }
                    </div>
                </SlidingPane>

                {/* sniff modal*/}
                {this.state.showSniffingModal &&
                    <div className="modal d-block animated slideInDown" id="sniffingModal">
                        <div className="modal-dialog modal-lg modal-dialog-centered">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h6 className="modal-title">Connector: <span>{this.state.allTopics[0].connectorName}</span>
                                        <button type="button" className="close" onClick={this.closeSniffingModal} data-dismiss="modal">
                                            <i className="far fa-times"></i>
                                        </button>
                                    </h6>
                                    <ul className="modal-header-list list-style-none">
                                        <li className={`btn btn-light ${this.state.dataView === 'FILE' ? 'active' : ''}`} data-tip data-for="jsonView" onClick={() => this.setState({ dataView: 'FILE' })}>
                                            <i className="far fa-file-alt"></i>
                                            <ReactTooltip id="jsonView" place="bottom" type="dark">
                                                <div className="tooltiptext"><p>JSON View</p></div>
                                            </ReactTooltip>
                                        </li>
                                        <li className={`btn btn-light ${this.state.dataView === 'TREE' ? 'active' : ''}`} data-tip data-for="treeView" onClick={() => this.setState({ dataView: 'TREE' })}>
                                            <i className="far fa-folder-tree"></i>
                                            <ReactTooltip id="treeView" place="bottom" type="dark">
                                                <div className="tooltiptext"><p>Tree View</p></div>
                                            </ReactTooltip>
                                        </li>
                                        <li data-tip data-for="clear" className="btn btn-light" onClick={this.clearSniffingModal} >
                                            <i className="far fa-trash-alt"></i>
                                            <ReactTooltip id="clear" place="bottom" type="dark">
                                                <div className="tooltiptext"><p>Clear All</p></div>
                                            </ReactTooltip>
                                        </li>
                                    </ul>
                                </div>
                                <div className="modal-body">
                                    <ul className="nav nav-tabs modalNavTabs">
                                        {this.state.allTopics.map(topic => {
                                            return (
                                                <li key={topic.topicName} className="nav-item">
                                                    <a className={this.state.selectedTopic === topic.topicName ? "nav-link active" : "nav-link"} onClick={() => { this.tabChangeHandler(topic.topicName) }}>{`${topic.topicName}`}</a>
                                                </li>
                                            )
                                        })}
                                    </ul>
                                    <div className="sniff-wrapper">
                                        <ul className="list-style-none">
                                            {this.state.allTopics.map((topic, index) => {
                                                let html
                                                if (this.state.selectedTopic === topic.topicName) {
                                                    topic.messages.length ?
                                                        this.state.dataView === 'FILE' ?
                                                            html = topic.messages.map((messageObj, index) => {
                                                                this.state.dataView !== 'FILE' ? delete messageObj.time : ''
                                                                delete messageObj.name
                                                                return (
                                                                    <li key={index} onMouseEnter={() => this.setState({ isHoldData: true })} onMouseLeave={() => this.setState({ isHoldData: false })}>
                                                                        <input type="text" id={`copyText${index}`} value={messageObj.message} readOnly />
                                                                        {messageObj.message}
                                                                        <button className="btn-transparent btn-transparent-blue" onClick={() => this.copyToClipboard(`copyText${index}`)}><i className="far fa-copy"></i>
                                                                            <p className="d-inline-block" id={`Copied${index}`} data-tip="Copied"></p>
                                                                            <ReactTooltip className="copyMessage" place="left" />
                                                                        </button>
                                                                        <span>{messageObj.time}</span>
                                                                    </li>)
                                                            }) :
                                                            html = topic.messages.map((messageObj, index) => <ReactJson key={index} src={messageObj} />)
                                                        :
                                                        html = (
                                                            <div key={index} className="sniff-loader">
                                                                <img src="https://content.iot83.com/m83/misc/uploading.gif" />
                                                                <h6>Debugging...</h6>
                                                            </div>
                                                        )
                                                }
                                                return html;
                                            })}
                                        </ul>
                                    </div>
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-success" data-dismiss="modal" onClick={this.closeSniffingModal}>OK</button>
                                </div>
                            </div>
                        </div>
                    </div>
                }
                {/* end sniff modal */}

                {/* delete modal */}
                {this.state.confirmState &&
                    <ConfirmModel
                        status={"delete"}
                        deleteName={this.state.connectorName + " connector"}
                        confirmClicked={() => {
                            this.setState({
                                isFetching: true,
                                confirmState: false,
                                isOpen: false
                            }, () => { this.props.deleteConnectorById(this.state.connectorToBeDeleted) })
                        }}
                        cancelClicked={() => { this.cancelClicked() }}
                    />
                }
                {/* end delete modal */}

                {this.state.isOpen &&
                    <NotificationModal
                        type={this.state.modalType}
                        message2={this.state.message2}
                        onCloseHandler={this.onCloseHandler}
                    />
                }
            </React.Fragment>
        );
    }
}

ManageEtlDataSource.propTypes = {
    dispatch: PropTypes.func
};

const mapStateToProps = createStructuredSelector({
    connectorsList: getConnectorsListSuccess(),
    connectorsListError: getConnectorsListError(),
    deleteConnectorFailure: deleteFailure(),
    deleteConnectorSuccess: deleteSuccess(),
    getConnectorsCategorySuccess: getConnectorsCategorySuccess(),
    getConnectorsCategoryFailure: getConnectorsCategoryFailure(),
    deviceGroupsSuccess: deviceGroupsSuccess(),
    deviceGroupsFailure: deviceGroupsFailure(),
    addOrUpdateGroupSuccess: addOrUpdateGroupSuccess(),
    addOrUpdateGroupFailure: addOrUpdateGroupFailure(),
    deleteGroupSuccess: deleteGroupSuccess(),
    deleteGroupFailure: deleteGroupFailure(),
    developerQuotaSuccess: getDeveloperQuotaSuccess(),
    developerQuotaFailure: getDeveloperQuotaFailure(),
});

function mapDispatchToProps(dispatch) {
    return {
        dispatch,
        getConnectorsListByCategory: (category) => dispatch(getConnectorsListByCategory(category)),
        getConnectorsCategory: () => dispatch(getConnectorsCategory()),
        deleteConnectorById: (id) => dispatch(deleteConnectorById(id)),
        getAllDeviceGroupsByConnectorId: (id) => dispatch(getAllDeviceGroupsByConnectorId(id)),
        addOrUpdateGroup: (id, payload) => dispatch(addOrUpdateGroup(id, payload)),
        deleteGroup: (id) => dispatch(deleteGroup(id)),
        getDeveloperQuota: (payload) => dispatch(getDeveloperQuota(payload)),
    };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "manageEtlDataSource", reducer });
const withSaga = injectSaga({ key: "manageEtlDataSource", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(ManageEtlDataSource);