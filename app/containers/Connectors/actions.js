/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { DEFAULT_ACTION,
  DATA_LIST_REQUEST,
  DELETE_DATA_CONNECTOR,
  GET_DATA_CONNECTORS,
  GET_ALL_DEVICE_GROUPS,
  ADD_OR_UPDATE_GROUP,
  DELETE_GROUP_INITIATED,
  GET_DEVELOPER_QUOTA
} from "./constants";

export function defaultAction() {
  return {
    type: DEFAULT_ACTION
  };
}
export function getConnectorsListByCategory(category) {
  return {
    type: DATA_LIST_REQUEST,
    category,
  };
}
export function getConnectorsCategory() {
  return {
    type: GET_DATA_CONNECTORS,
  };
}
export function deleteConnectorById(id) {
  return {
    type: DELETE_DATA_CONNECTOR,
    id
  };
}
export function getAllDeviceGroupsByConnectorId(id) {
  return {
    type: GET_ALL_DEVICE_GROUPS,
    id
  };
}

export function addOrUpdateGroup(id, payload) {
  return {
    type: ADD_OR_UPDATE_GROUP,
    payload,
    id,
  };
}

export function deleteGroup(id) {
  return {
    type: DELETE_GROUP_INITIATED,
    id,
  };
}

export function getDeveloperQuota(payload) {
  return {
    type: GET_DEVELOPER_QUOTA,
    payload,
  }
}
