/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { fromJS } from "immutable";
import { DEFAULT_ACTION,
    DATA_LIST_SUCCESS,
    DATA_LIST_FAILURE,
    DELETE_DATA_CONNECTOR_SUCCESS,
    DELETE_DATA_CONNECTOR_FAILURE,
    GET_DATA_CONNECTORS_SUCCESS,
    GET_DATA_CONNECTORS_FAILURE,
    GET_ALL_DEVICE_GROUPS_SUCCESS,
    GET_ALL_DEVICE_GROUPS_FAILURE,
    ADD_OR_UPDATE_GROUP_SUCCESS,
    ADD_OR_UPDATE_GROUP_FAILURE,
    DELETE_GROUP_SUCCESS,
    DELETE_GROUP_FAILURE,
    GET_DEVELOPER_QUOTA_SUCCESS,
    GET_DEVELOPER_QUOTA_FAILURE,
} from "./constants";

export const initialState = fromJS({});

function manageEtlDataSourceReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
      
    case DATA_LIST_SUCCESS:
    return Object.assign({}, state, {
      'connectorsListSuccess': action.response
    });
    case DATA_LIST_FAILURE:
    return Object.assign({}, state, {
      'connectorsListError': action.error
    });
    case DELETE_DATA_CONNECTOR_SUCCESS: 
    return Object.assign({},state,{
      'deleteSuccess': action.response,
    })
  case DELETE_DATA_CONNECTOR_FAILURE: 
    return Object.assign({},state,{
      'deleteFailure': action.error,
    })
  case GET_DATA_CONNECTORS_SUCCESS: 
    return Object.assign({},state,{
      'getConnectorsCategorySuccess': action.response,
    })
  case GET_DATA_CONNECTORS_FAILURE: 
    return Object.assign({},state,{
      'getConnectorsCategoryFailure': action.error,
    })
  case GET_ALL_DEVICE_GROUPS_SUCCESS:
    return Object.assign({},state,{
      'deviceGroupsSuccess': action.response,
    })
  case GET_ALL_DEVICE_GROUPS_FAILURE:
    return Object.assign({},state,{
      'deviceGroupsFailure': action.error,
    })
  case ADD_OR_UPDATE_GROUP_SUCCESS:
    return Object.assign({},state,{
      'addOrUpdateGroupSuccess': action.response,
    })
  case ADD_OR_UPDATE_GROUP_SUCCESS:
    return Object.assign({},state,{
      'addOrUpdateGroupFailure': action.error,
    })
  case DELETE_GROUP_SUCCESS:
    return Object.assign({},state,{
      'deleteGroupSuccess': action.response,
    })
  case DELETE_GROUP_FAILURE:
    return Object.assign({},state,{
      'deleteGroupFailure': action.error,
    })
  case GET_DEVELOPER_QUOTA_SUCCESS:
      return Object.assign({}, state, {
        'developerQuotaSuccess': action.response
      });
  case GET_DEVELOPER_QUOTA_FAILURE:
      return Object.assign({}, state, {
        'developerQuotaFailure': action.error
      });
    default:
      return state;
  }
}

export default manageEtlDataSourceReducer;
