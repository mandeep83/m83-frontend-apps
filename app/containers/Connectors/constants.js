/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

export const DEFAULT_ACTION = "app/ManageEtlDataSource/DEFAULT_ACTION";

export const DATA_LIST_REQUEST = "app/ManageEtlDataSource/DATA_LIST_REQUEST";
export const DATA_LIST_SUCCESS = "app/ManageEtlDataSource/DATA_LIST_SUCCESS";
export const DATA_LIST_FAILURE = "app/ManageEtlDataSource/DATA_LIST_FAILURE";

export const DELETE_DATA_CONNECTOR = "app/ManageEtlDataSource/DELETE_DATA_CONNECTOR";
export const DELETE_DATA_CONNECTOR_SUCCESS = "app/ManageEtlDataSource/DELETE_DATA_CONNECTOR_SUCCESS";
export const DELETE_DATA_CONNECTOR_FAILURE = "app/ManageEtlDataSource/DELETE_DATA_CONNECTOR_FAILURE";

export const GET_DATA_CONNECTORS = "app/ManageEtlDataSource/GET_DATA_CONNECTORS";
export const GET_DATA_CONNECTORS_SUCCESS = "app/ManageEtlDataSource/GET_DATA_CONNECTORS_SUCCESS";
export const GET_DATA_CONNECTORS_FAILURE = "app/ManageEtlDataSource/GET_DATA_CONNECTORS_FAILURE";

export const GET_ALL_DEVICE_GROUPS = "app/ManageEtlDataSource/GET_ALL_DEVICE_GROUPS";
export const GET_ALL_DEVICE_GROUPS_SUCCESS = "app/ManageEtlDataSource/GET_ALL_DEVICE_GROUPS_SUCCESS";
export const GET_ALL_DEVICE_GROUPS_FAILURE = "app/ManageEtlDataSource/GET_ALL_DEVICE_GROUPS_FAILURE";

export const ADD_OR_UPDATE_GROUP = "app/ManageEtlDataSource/ADD_OR_UPDATE_GROUP";
export const ADD_OR_UPDATE_GROUP_SUCCESS = "app/ManageEtlDataSource/ADD_OR_UPDATE_GROUP_SUCCESS";
export const ADD_OR_UPDATE_GROUP_FAILURE = "app/ManageEtlDataSource/ADD_OR_UPDATE_GROUP_FAILURE";

export const DELETE_GROUP_INITIATED = "app/ManageEtlDataSource/DELETE_GROUP_INITIATED";
export const DELETE_GROUP_SUCCESS = "app/ManageEtlDataSource/DELETE_GROUP_SUCCESS";
export const DELETE_GROUP_FAILURE = "app/ManageEtlDataSource/DELETE_GROUP_FAILURE";

export const GET_DEVELOPER_QUOTA = "app/ManageEtlDataSource/GET_DEVELOPER_QUOTA";
export const GET_DEVELOPER_QUOTA_SUCCESS = "app/ManageEtlDataSource/GET_DEVELOPER_QUOTA_SUCCESS";
export const GET_DEVELOPER_QUOTA_FAILURE = "app/ManageEtlDataSource/GET_DEVELOPER_QUOTA_FAILURE";

