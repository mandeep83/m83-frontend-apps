/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { fromJS } from "immutable";
import * as CONSTANTS from './constants';

export const initialState = fromJS({});

function etlConfigListReducer(state = initialState, action) {
  switch (action.type) {
    case CONSTANTS.DEFAULT_ACTION:
      return state;
    case CONSTANTS.GET_ETL_LIST_SUCCESS:
      return Object.assign({}, state, {
        'allEltList': action.response,
      })
    case CONSTANTS.GET_ETL_LIST_FAILURE:
      return Object.assign({}, state, {
        'allEltListFailure': action.error,
      })
    case CONSTANTS.DELETE_ETL_SUCCESS:
      return Object.assign({}, state, {
        'deleteSuccess': action.response,
      })
    case CONSTANTS.DELETE_ETL_FAILURE:
      return Object.assign({}, state, {
        'deleteFailure': action.error,
      })
    case CONSTANTS.CHANGE_ETL_STATE_SUCCESS:
      return Object.assign({}, state, {
        'changeEtlStateSuccess': Math.random(),
        'changedState': action.response
      })
    case CONSTANTS.CHANGE_ETL_STATE_FAILURE:
      return Object.assign({},state,{
        'changeEtlStateFailure':{
          error:action.error,
          etlId:action.addOns.etlId
        }
      })
    case CONSTANTS.DEBUG_ETL_SUCCESS:
      return Object.assign({}, state, {
        'debugETLSuccess':action.response
      })
    case CONSTANTS.DELETE_ETL_FAILURE:
      return Object.assign({}, state, {
        'debugETLFailure': action.error,
      })
    case CONSTANTS.GET_MONITOR_DATA_SUCCESS:
      return Object.assign({},state,{
        'getMonitorDataSuccess':action.response
      })
    case CONSTANTS.GET_MONITOR_DATA_FAILURE:
      return Object.assign({},state,{
        'getMonitorDataFailure':action.error
      })
    case CONSTANTS.CLEAR_CHECKPOINT_SUCCESS:
      return Object.assign({},state,{
        'clearCheckpointSuccess': action.response
      })
    case CONSTANTS.CLEAR_CHECKPOINT_FAILURE:
      return Object.assign({},state,{
        'clearCheckpointFailure': action
      })    
    default:
      return initialState;
  }
}

export default etlConfigListReducer;
