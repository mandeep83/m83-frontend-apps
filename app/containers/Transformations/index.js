/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import * as ACTIONS from "./actions";
import * as SELECTORS from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import Loader from "../../components/Loader";
import NotificationModal from '../../components/NotificationModal/Loadable'
import ReactTooltip from "react-tooltip";
import MQTT from 'mqtt';
import ConfirmModel from "../../components/ConfirmModel";
import { cloneDeep } from "lodash";
import AllChartsComponent from "../../components/AllChartsComponent";
import { convertTimestampToDate } from '../../commonUtils';
import NoDataFoundMessage from "../../components/NoDataFoundMessage/Loadable"
import AddNewButton from "../../components/AddNewButton/Loadable"
import QuotaHeader from "../../components/QuotaHeader/Loadable";
import ListingTable from "../../components/ListingTable/Loadable"
import Skeleton from "react-loading-skeleton";

let client;
let clientException;

let chart = {};

/* eslint-disable react/prefer-stateless-function */
export class EtlConfigList extends React.Component {

    state = {
        etlList: [],
        messages: [],
        exception: [],
        isFetching: true,
        toggleView:false,
        closeMqtt: false,
        isImportModal: false,
        selectedEtl: null,
        specificLoader: false,
        filterList: [],
        searchTerm: '',
        confirmState: false,
        selectedViewIndex: '',
        selectedView: false,
        monitoringData: [],
        isFetchingMonitoringData: false
    }

    componentWillMount() {
        // this.getMqttConnection();
        // this.getMqttConnectionForException()
    }

    componentWillUnmount() {
        // client.end();
        // clientException.end();
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.allEltList && nextProps.allEltList !== this.props.allEltList) {
            let analytics = [], policy = [], report = [], etlList = [];
            nextProps.allEltList.map(val => val.isClearCheckPoint = false)
            nextProps.allEltList.length > 0 && nextProps.allEltList.map(val => {
                if (val.createdFor === "analytics")
                    analytics.push(val)
                else if (val.createdFor === "policy")
                    policy.push(val)
                else
                    report.push(val)
            })
            let self = this
            analytics.length > 0 && analytics.sort(function (a, b) { return self.alphabeticallySort(a, b) })
            policy.length > 0 && policy.sort(function (a, b) { return self.alphabeticallySort(a, b) })
            report.length > 0 && report.sort(function (a, b) { return self.alphabeticallySort(a, b) })
            etlList = [...analytics, ...policy, ...report]
            this.setState({
                etlList,
                filterList: etlList,
                isFetching: false
            })
        }

        if (nextProps.allEltListFailure && nextProps.allEltListFailure !== this.props.allEltListFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.allEltListFailure,
                isFetching: false
            })
        }

        if (nextProps.etlDelete && nextProps.etlDelete !== this.props.etlDelete) {
            let etlList = JSON.parse(JSON.stringify(this.state.etlList));
            etlList = etlList.filter(val => val.id !== nextProps.etlDelete.id)
            this.setState((previousState) => ({
                selectedView: false,
                etlList,
                modalType: "success",
                isOpen: true,
                message2: nextProps.etlDelete.message,
                isDeletedSuccess: !previousState.isDeletedSuccess
            }), () => {
                this.props.getEtlList();
                // this.getMqttConnection();
                // this.getMqttConnectionForException()
            })
        }

        if (nextProps.etlDeleteFailure && nextProps.etlDeleteFailure !== this.props.etlDeleteFailure) {
            this.setState({
                isFetching: false,
                modalType: "error",
                isOpen: true,
                message2: nextProps.etlDeleteFailure,
            })
        }

        if (nextProps.changeEtlStateSuccess && nextProps.changeEtlStateSuccess !== this.props.changeEtlStateSuccess) {
            let etlList = this.state.etlList.map((temp) => {
                if (temp.name === nextProps.changedState.name) {
                    temp.state = nextProps.changedState.state === "stop" ? "stop" : "running"
                    temp.isDebug = nextProps.changedState.state === "stop" ? false : temp.isDebug;
                }
                return temp
            })
            let filterList = this.state.filterList.map((temp) => {
                if (temp.name === nextProps.changedState.name) {
                    temp.state = nextProps.changedState.state === "stop" ? "stop" : "running"
                    temp.isDebug = nextProps.changedState.state === "stop" ? false : temp.isDebug;
                }
                return temp
            })
            delete filterList[nextProps.changedState.etlId.split('_')[1]].startStopLoader;
            delete filterList[nextProps.changedState.etlId.split('_')[1]].restartLoader;

            this.setState({
                specificLoader: false,
                etlList,
                filterList,
                modalType: "success",
                isOpen: true,
                message2: "Request Completed Successfully",
            })
        }

        if (nextProps.changeEtlStateFailure && nextProps.changeEtlStateFailure !== this.props.changeEtlStateFailure) {
            let filterList = JSON.parse(JSON.stringify(this.state.etlList))

            delete filterList[nextProps.changeEtlStateFailure.etlId.split('_')[1]].startStopLoader;
            delete filterList[nextProps.changeEtlStateFailure.etlId.split('_')[1]].restartLoader;

            this.setState({
                modalType: "error",
                filterList,
                specificLoader: false,
                isOpen: true,
                message2: nextProps.changeEtlStateFailure.error,
            })
        }

        if (nextProps.debugETLSuccess && nextProps.debugETLSuccess !== this.props.debugETLSuccess) {
            let etlList = this.state.etlList.map((el, key) => {
                if (el.name == nextProps.debugETLSuccess.etlName) {
                    el.isDebug = true;
                    el.state = "running"
                }
                return el;
            })
            this.setState({ etlList }, () => {
                this.getSubscribeTopicData(nextProps.debugETLSuccess.etlName)
            })
        }

        if (nextProps.debugETLFailure && nextProps.debugETLFailure !== this.props.debugETLFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.debugETLFailure,
            })
        }

        if (nextProps.getMonitorDataSuccess && nextProps.getMonitorDataSuccess !== this.props.getMonitorDataSuccess) {
            let monitoringData = nextProps.getMonitorDataSuccess.map(chartDataObj => {
                chartDataObj.category = new Date(chartDataObj.timestamp).toLocaleString();
                return chartDataObj;
            })
            this.setState({ monitoringData, isFetchingMonitoringData: false })
        }

        if (nextProps.getMonitorDataFailure && nextProps.getMonitorDataFailure !== this.props.getMonitorDataFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.getMonitorDataFailure,
            })
        }

        if (nextProps.clearCheckpointSuccess && nextProps.clearCheckpointSuccess !== this.props.clearCheckpointSuccess) {
            let filterList = cloneDeep(this.state.filterList);
            filterList.find(({ name }) => name === nextProps.clearCheckpointSuccess.etlName).isClearCheckPoint = false
            this.setState({
                filterList,
                modalType: "success",
                isOpen: true,
                message2: nextProps.clearCheckpointSuccess.message
            })
        }

        if (nextProps.clearCheckpointFailure && nextProps.clearCheckpointFailure !== this.props.clearCheckpointFailure) {
            let filterList = cloneDeep(this.state.filterList);
            filterList.find(({ name }) => name === nextProps.clearCheckpointFailure.addOns.etlName).isClearCheckPoint = false
            this.setState({
                filterList,
                modalType: "error",
                isOpen: true,
                message2: nextProps.clearCheckpointFailure.error,
            })
        }
    }

    alphabeticallySort = (a, b) => {
        if (a.name < b.name) { return -1; }
        if (a.name > b.name) { return 1; }
        return 0;
    }

    getSubscribeTopicData = (etlName, isMl, mlType) => {
        let topic = "DEBUG/" + localStorage.tenant + '/' + etlName
        let _this = this
        if (isMl)
            topic = localStorage.tenant + "_sfpData_" + mlType
        if (!client.connected) {
            this.getMqttConnection()
        }
        this.setState({
            messages: [],
            exception: []
        }, () => {
            let messages = [..._this.state.messages]
            client.subscribe(topic, function (err) {
                if (!err) {
                    client.on('message', function (topic, message) {
                        messages.unshift({
                            time: new Date().toLocaleString('en-US', { hour12: false, timeZone: localStorage.timeZone }),
                            message: JSON.stringify(JSON.parse(message.toString()), undefined, 2),
                        });
                        _this.setState({
                            messages, subscribedTopic: topic,
                        })
                    });
                }
            })
        })
    }

    closeMqttSubscribeConnection = () => {
        this.setState({
            showSubscribeModal: false
        })
        const _this = this;
        if (this.state.subscribedTopic) {
            client.unsubscribe(_this.state.subscribedTopic, function (err) {
                if (!err) {
                    client.end();
                    _this.setState({
                        messages: [],
                        subscribedTopic: null,
                    })
                }
            });
        }
    }

    getMqttConnection = () => {
        let brokerURL = window.location.origin === "http://localhost:3001" ? window.API_URL.split("//")[1].substring(0, window.API_URL.split("//")[1].length - 1) : window.location.host;
        let count = 0;
        const options = {
            host: brokerURL,
            protocol: 'wss',
            path: '/connectmqtt',
            username: '83incs',
            password: 'Hello@123',
        };

        client = MQTT.connect(options);

        client.on('connect', function () { });

        client.on('close', function (response) {
            if (response && count > 4) {
                client.end();
            }
        });

        client.on('reconnect', function () {
            count++;
        });
    }

    getMqttConnectionForException = () => {
        let brokerURL = window.location.origin === "http://localhost:3001" ? window.API_URL.split("//")[1].substring(0, window.API_URL.split("//")[1].length - 1) : window.location.host;
        let countException = 0;
        const options = {
            host: brokerURL,
            protocol: 'wss',
            path: '/connectmqtt',
            username: '83incs',
            password: 'Hello@123',

        };

        clientException = MQTT.connect(options);
        clientException.on('connect', function () { });

        let _this = this;
        let exception = [..._this.state.exception]

        clientException.subscribe(`DEBUG/${localStorage.tenant}`, function (err) {
            if (!err) {
                clientException.on('message', function (topic, message) {
                    exception.unshift({
                        time: new Date().toLocaleString('en-US', { hour12: false, timeZone: localStorage.timeZone }),
                        message: JSON.stringify(JSON.parse(message.toString()), undefined, 2),
                    });
                    _this.setState({
                        exception,
                    })
                });
            }
        })

        clientException.on('close', function (response) {
            if (response && response.type == 'error' && countException > 4) {
                clientException.end();
            }
        });

        clientException.on('reconnect', function () {
            countException++;
        });
    }

    onCloseHandler = () => {
        this.setState({
            isOpen: false,
            message2: '',
            modalType: "",
        });
    }

    selectedEtl = (event) => {
        this.closeMqttSubscribeConnection()
        this.setState({
            selectedEtl: event,
            closeMqtt: false
        })
    }

    onDebugRequestHandler = (etlName, isDebug, isMl, mlType) => {
        this.setState({
            closeMqtt: true
        })
        let payload = {
            etlName,
            isDebug: true,
            tenant: localStorage.tenant
        }
        this.props.debugEtl(payload)
    }

    onChangeEtlState = (e, stateName, name, etlType, id, loaderType) => {
        let etlList = [...this.state.etlList],
            filterList = cloneDeep(this.state.filterList),
            index = filterList.findIndex(val => val.id === id);
        if (e == "Restart") {
            filterList[index].restartLoader = true;
        } else {
            filterList[index].startStopLoader = true;
        }
        this.setState({ etlList, specificLoader: loaderType, filterList, isOpen: false }, () => this.props.changeEtlState(`${e == "Restart" ? "restart" : stateName.toUpperCase() === "RUNNING" ? "stop" : "resume"}`, name, etlType, `loader_${index}`))
    }

    filteredDataList = (value) => {
        let filterList = JSON.parse(JSON.stringify(this.state.etlList))
        if (value.length > 0) {
            filterList = filterList.filter(item => item.name.toLowerCase().includes(value.toLowerCase()))
        }
        this.setState({
            searchTerm: value,
            filterList
        })
    }

    emptySearchBox = () => {
        let etlList = JSON.parse(JSON.stringify(this.state.etlList))
        this.setState({
            searchTerm: "",
            filterList: etlList,
        })
    }

    deleteClient = (id, deleteTransformationName) => {
        this.setState({
            confirmState: true,
            clientToBeDeleted: id,
            deleteTransformationName
        })
    }

    createChartData = (id) => {
        let monitoringData = cloneDeep(this.state.monitoringData);
        let config = {
            id: id,
            chartType: "lineChart",
            chartSubType: "simpleLineChart",
            chartData: {
                chartData: [],
                availableAttributes: [id]
            }
        };
        monitoringData.map(
            el => config.chartData.chartData.push({
                timestamp: el.timestamp,
                category: el.category,
                [id]: el[id]
            })
        )
        return config
    }

    getColumns = () => {
        return [
            {
                Header: "Name", width: 17.5, accessor: "name",
                filterable: true,
                sortable: true,
                cell: (row) => (
                    <React.Fragment>
                        <h6 className="text-theme fw-600">{row.name}</h6>
                        <p>{`${row.description || "-"}`}</p>
                    </React.Fragment>
                )
            },
            {
                Header: "Device Type / Connector", width: 15, accessor: "connectorName", filterable: true, sortable: true,
                cell: (row) => (
                    <React.Fragment>
                        {/*<h6>{row.connectorName}</h6>*/}
                        <div className="button-group-link">
                            <button className="btn btn-link text-truncate" onClick={() => { this.props.history.push(`/addOrEditDeviceType/${row.connectorId}`) }}>{row.connectorName}</button>
                        </div>
                        {/*<p>{row.type}</p>*/}
                    </React.Fragment>
                )
            },
            {
                Header: "Type", width: 10, accessor: "type", filterable: true, sortable: true,
                cell: (row) => (
                    <h6 className={`text-capitalize ${row.etlType == "stream" ? "text-indigo" : "text-pink"}`}>{row.etlType}</h6>
                ),
                filter: ({ value, onChangeHandler, accessor }) =>
                    <select
                        className="form-control"
                        onChange={event => onChangeHandler(event, accessor)}
                        value={value ? value : 'all'}
                    >
                        <option value="">ALL</option>
                        <option value="Stream">Stream</option>
                    </select>
            },
            {
                Header: "Source / Sink", width: 10,
                cell: (row) => (
                    <React.Fragment>
                        <div className="list-view-icon">
                            <img src={`https://content.iot83.com/m83/dataConnectors/${row.connectorImageName}`} />
                        </div>
                        <div className="list-view-icon mr-l-50">
                            <img src={`https://content.iot83.com/m83/dataConnectors/${row.sink[0].url}`} />
                        </div>
                    </React.Fragment>
                )
            },
            { Header: "Created", width: 15, accessor: "createdAt", sortable: true, filterable: true },
            { Header: "Updated", width: 15, accessor: "updatedAt", sortable: true, filterable: true },
            {
                Header: "Actions", width: "17_5", cell: (row) => (
                    <div className="button-group">
                        {row.startStopLoader ?
                            <button type="button" className="btn-transparent btn-transparent-green">
                                <i className="far fa-cog fa-spin"></i>
                            </button> :
                            <button
                                className={`btn-transparent btn-transparent-${row.state.toUpperCase() === "RUNNING" ? 'gray' : 'green'}`}
                                id="startStop"
                                data-tooltip
                                data-tooltip-text={row.state.toUpperCase() === "RUNNING" ? 'Stop' : 'Start'}
                                data-tooltip-place="bottom"
                                onClick={(e) => {
                                    this.onChangeEtlState(row.state.toUpperCase() === "RUNNING" ? "Stop" : "Start", row.state, row.name, row.etlType, row.id, "startStop")
                                }}
                                id={row.state.toUpperCase() === "RUNNING" ? "Stop" : "Start"}>
                                <i className={`far fa-${row.state.toUpperCase() === "RUNNING" ? 'stop' : 'play'}`}></i>
                            </button>
                        }
                        {row.restartLoader ?
                            <button type="button" className="btn-transparent btn-transparent-green">
                                <i className="far fa-cog fa-spin"></i>
                            </button> :
                            <button
                                className="btn-transparent btn-transparent-yellow"
                                data-tooltip
                                data-tooltip-text="Restart"
                                data-tooltip-place="bottom"
                                id="restart"
                                disabled={row.state.toUpperCase() === "STOP"}
                                onClick={(e) => this.onChangeEtlState("Restart", row.state, row.name, row.etlType, row.id, "restart")}>
                                <i className="far fa-sync-alt"></i>
                            </button>
                        }
                        <button className="btn-transparent btn-transparent-red" data-tooltip data-tooltip-text="Clear Checkpoint" data-tooltip-place="bottom"
                            onClick={() => this.clearCheckpointHandler(row.name)}><i className={row.isClearCheckPoint ? 'far fa-cog fa-spin' : 'far fa-broom'}></i>
                        </button>
                        <button className="btn-transparent btn-transparent-cyan" data-tooltip data-tooltip-text="Info" data-tooltip-place="bottom"
                            onClick={() => this.setState({
                                selectedViewIndex: this.state.filterList.findIndex((item) => item.id === row.id),
                                selectedView: true,
                                isFetchingMonitoringData: true,
                                selectedMonitoringName: row.name
                            }, () => { this.props.getMointoringData(row.name) })} >
                            <i className="far fa-info"></i>
                        </button>
                        <button className="btn-transparent btn-transparent-blue" data-tooltip data-tooltip-text="Edit" data-tooltip-place="bottom"
                            onClick={() => this.props.history.push(`/addOrEditEtlPipeline/${row.createdFor}/${row.id}`)}>
                            <i className="far fa-pencil"></i>
                        </button>
                        <button className="btn-transparent btn-transparent-red" data-tooltip data-tooltip-text="Delete" data-tooltip-place="bottom"
                            onClick={() => this.deleteClient(row.id, row.name)}
                            disabled={(row.etlType && row.state.toUpperCase() === "RUNNING")}>
                            <i className="far fa-trash-alt"></i>
                        </button>
                    </div>
                )
            },
        ]
    }

    getStatusBarClass = (row) => {
        if (row.state === "running")
            return { className: "green", tooltipText: "Running" }
        if (row.state === "pause")
            return { className: "yellow", tooltipText: "Paused" }
        return { className: "red", tooltipText: "Stopped" }
    }

    refreshComponent = () => {
        this.setState({
            isFetching: true
        }, () => { this.props.getEtlList() })
    }

    clearCheckpointHandler = (etlName) => {
        let filterList = cloneDeep(this.state.filterList)
        filterList.find(({ name }) => name === etlName).isClearCheckPoint = true
        this.setState({
            filterList
        }, () => this.props.clearCheckpoint(etlName))
    }

    toggleView = () => {
        this.setState((prevState) => ({ toggleView: !prevState.toggleView }))
    }

    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>ETL Pipelines</title>
                    <meta name="description" content="M83-ETL Pipeline" />
                </Helmet>

                <QuotaHeader
                    showQuota
                    addButtonText="Add ETL Pipeline"
                    componentName="ETL Pipelines"
                    productName="etl"
                    onAddClick={() => this.props.history.push(`/addOrEditEtlPipeline`)}
                    // searchBoxDetails={{
                    //     value: this.state.searchTerm,
                    //     onChangeHandler: this.filteredDataList,
                    //     isDisabled: !this.state.etlList.length || (!this.state.specificLoader && this.state.isFetching)
                    // }}
                    refreshDisabled={this.state.isFetching}
                    addDisabled={this.state.isFetching}
                    refreshHandler={this.refreshComponent}
                    callBack={() => this.props.getEtlList()}
                    toggleButton={this.toggleView}
                    isDeletedSuccess={this.state.isDeletedSuccess}
                    {...this.props}
                />

                <div className="content-body">
                    {!this.state.specificLoader && this.state.isFetching ?
                        <Skeleton count={9} className="skeleton-list-loader" /> :
                        <React.Fragment>
                            {this.state.etlList.length ?
                                !this.state.toggleView ?
                                    <ListingTable
                                        data={this.state.filterList}
                                        columns={this.getColumns()}
                                        isLoading={!this.state.specificLoader && this.state.isFetching}
                                        statusBar={this.getStatusBarClass}
                                    />
                                    :
                                    <ul className="card-view-list">
                                        {this.state.filterList.map((item, index) =>
                                            <li key={index}>
                                                <div className="card-view-box">
                                                    <div className="card-view-header">
                                                        <span data-tooltip data-tooltip-text={item.state === "running" ? "Running" : item.state === "pause" ? "Paused" : "Stopped"} data-tooltip-place="bottom"
                                                              className={item.state === "running" ? "card-view-header-status bg-success" : item.state === "pause" ? "card-view-header-status bg-warning" : "card-view-header-status bg-danger"}>
                                                        </span>
                                                        <span className={item.etlType == "stream" ? "alert alert-primary text-capitalize" : "alert alert-danger text-capitalize"}>{item.etlType}</span>
                                                        <div className="dropdown">
                                                            <button className="btn-transparent btn-transparent-gray" type="button" data-toggle="dropdown">
                                                                <i className="fas fa-ellipsis-v"></i>
                                                            </button>
                                                            <div className="dropdown-menu">
                                                                {item.startStopLoader ?
                                                                    <button type="button" className="dropdown-item">
                                                                        <i className="far fa-cog fa-spin"></i>Loading
                                                                    </button> :
                                                                    <button className="dropdown-item"
                                                                        onClick={(e) => {
                                                                            this.onChangeEtlState(row.state.toUpperCase() === "RUNNING" ? "Stop" : "Start", item.state, item.name, item.etlType, item.id, "startStop")
                                                                        }}
                                                                        id={item.state.toUpperCase() === "RUNNING" ? "Stop" : "Start"}>
                                                                        <i className={`far fa-${item.state.toUpperCase() === "RUNNING" ? 'stop' : 'play'}`}></i>{item.state.toUpperCase() === "RUNNING" ? 'Stop' : 'Start'}
                                                                    </button>
                                                                }
                                                                {item.restartLoader ?
                                                                    <button type="button" className="dropdown-item"><i className="far fa-cog fa-spin"></i>Loading..</button> :
                                                                    <button className="dropdown-item" id="restart" disabled={item.state.toUpperCase() === "STOP"}
                                                                        onClick={(e) => this.onChangeEtlState("Restart", item.state, item.name, item.etlType, item.id, "restart")}>
                                                                        <i className="far fa-sync-alt"></i>Restart
                                                                    </button>
                                                                }
                                                                <button className="dropdown-item" onClick={() => this.clearCheckpointHandler(item.name)}>
                                                                    <i className={item.isClearCheckPoint ? 'far fa-cog fa-spin' : 'far fa-broom'}></i>Clear Checkpoint
                                                                </button>
                                                                <button className="dropdown-item"
                                                                    onClick={() => this.setState({
                                                                        selectedViewIndex: this.state.filterList.findIndex((item) => item.id === item.id),
                                                                        selectedView: true,
                                                                        isFetchingMonitoringData: true,
                                                                        selectedMonitoringName: item.name
                                                                    }, () => { this.props.getMointoringData(item.name) })} >
                                                                    <i className="far fa-info"></i>Info
                                                                </button>
                                                                <button className="dropdown-item" onClick={() => this.props.history.push(`/addOrEditEtlPipeline/${item.createdFor}/${item.id}`)}>
                                                                    <i className="far fa-pencil"></i>Edit
                                                                </button>
                                                                <button className="dropdown-item"
                                                                    onClick={() => this.deleteClient(item.id, item.name)}
                                                                    disabled={(item.etlType && item.state.toUpperCase() === "RUNNING")}>
                                                                    <i className="far fa-trash-alt"></i>Delete
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="card-view-body">
                                                        <div className="card-view-icon">
                                                            <img src={`https://content.iot83.com/m83/dataConnectors/${item.connectorImageName}`} />
                                                            <span><i className="fad fa-cloud-upload text-primary mr-2"></i>Source</span>
                                                        </div>
                                                        <div className="card-view-icon card-view-icon-right">
                                                            <img src={`https://content.iot83.com/m83/dataConnectors/${item.sink[0].url}`} />
                                                            <span>Sink<i className="fad fa-cloud-download text-green ml-2"></i></span>
                                                        </div>
                                                        <h6><i className="far fa-address-book"></i>{item.name ? item.name : "-"}</h6>
                                                        <p><i className="far fa-file-alt"></i>{item.description ? item.description : "-"}</p>
                                                        <div className="button-group-link">
                                                            <p>
                                                                <i className="far fa-microchip"></i>
                                                                <button className="btn btn-link text-truncate" onClick={() => { this.props.history.push(`/addOrEditDeviceType/${item.connectorId}`) }}>{item.connectorName}</button>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div className="card-view-footer d-flex">
                                                        <div className="flex-50 p-3">
                                                            <h6>Created</h6>
                                                            <p><strong>By - </strong>{item.createdBy}</p>
                                                            <p><strong>At - </strong>{new Date(item.createdAt).toLocaleString('en-US', { createdAt: localStorage.createdAt })}</p>
                                                        </div>
                                                        <div className="flex-50 p-3">
                                                            <h6>Updated</h6>
                                                            <p><strong>By - </strong>{item.updatedBy}</p>
                                                            <p><strong>At - </strong>{new Date(item.updatedAt).toLocaleString('en-US', { createdAt: localStorage.createdAt })}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        )}
                                    </ul>
                                :
                                <AddNewButton
                                    text1="No ETL Pipeline(s) available"
                                    text2="You haven't created any ETL pipelines yet. Please create a ETL pipeline first."
                                    imageIcon="addETL.png"
                                    createItemOnAddButtonClick={() => { this.props.history.push("/addOrEditEtlPipeline/analytics") }}
                                />
                            }
                        </React.Fragment>
                    }
                </div>

                {/* etl detail modal */}
                {this.state.selectedView &&
                    <div className="modal d-block animated slideInDown" role="dialog">
                        <div className="modal-dialog modal-xl modal-dialog-centered">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h6 className="modal-title">
                                        Name - <span>{this.state.filterList[this.state.selectedViewIndex].name}</span>
                                        <span className="ml-3 mr-3">|</span>
                                        Type - <span>{this.state.filterList[this.state.selectedViewIndex].etlType}</span>
                                        <button className="close" data-dismiss="modal" data-tooltip data-tooltip-text="Close" data-tooltip-place="bottom" onClick={() => this.setState({ selectedView: false, selectedViewIndex: '' })}>
                                            <i className="far fa-times"></i>
                                        </button>
                                    </h6>
                                </div>

                                <div className="modal-body etl-detail-wrapper">
                                    <div className="card">
                                        <div className="card-body d-flex">
                                            <div className="flex-30">
                                                <ul onClick={() => this.selectedEtl(val)}>
                                                    <li><img src={`https://content.iot83.com/m83/dataConnectors/${this.state.filterList[this.state.selectedViewIndex].connectorImageName}`} /></li>
                                                    <li><img src={`https://content.iot83.com/m83/dataConnectors/${this.state.filterList[this.state.selectedViewIndex].sink[0].url}`} /></li>
                                                </ul>
                                            </div>

                                            <div className="flex-45">
                                                <p><strong>Connector Name :</strong>{this.state.filterList[this.state.selectedViewIndex].connectorName}</p>
                                                {/*<p><strong>Collection Name :</strong>{this.state.filterList[this.state.selectedViewIndex].collection ? this.state.filterList[this.state.selectedViewIndex].collection : "N/A"}</p>*/}
                                                <p><strong>Last Updated At: </strong>{new Date(this.state.filterList[this.state.selectedViewIndex].updatedAt).toLocaleString('en-US', { timeZone: localStorage.timeZone })}</p>
                                                <p><strong>Description: </strong>{`${this.state.filterList[this.state.selectedViewIndex].description || 'N/A'}`}</p>
                                            </div>

                                            <div className="flex-25 text-right">
                                                <div className="button-group">
                                                    <button type="button" className="btn-transparent btn-transparent-cyan" data-tooltip data-tooltip-text="Refresh" data-tooltip-place="bottom"
                                                        onClick={() => this.setState({ isFetchingMonitoringData: true }, () => { this.props.getMointoringData(this.state.selectedMonitoringName) })}>
                                                        <i className="far fa-redo-alt"></i>
                                                    </button>
                                                    <button type="button" className={`btn-transparent btn-transparent-${this.state.filterList[this.state.selectedViewIndex].state.toUpperCase() === "RUNNING" ? "gray" : "green"}`}
                                                        data-tooltip data-tooltip-text={this.state.filterList[this.state.selectedViewIndex].state.toUpperCase() === "RUNNING" ? "Stop" : "Start"} data-tooltip-place="bottom" id="startStopInModal"
                                                        onClick={(e) => this.onChangeEtlState(this.state.filterList[this.state.selectedViewIndex].state.toUpperCase() === "RUNNING" ? "Stop" : "Start", this.state.filterList[this.state.selectedViewIndex].state, this.state.filterList[this.state.selectedViewIndex].name, this.state.filterList[this.state.selectedViewIndex].etlType, this.state.filterList[this.state.selectedViewIndex].id, "startStopInModal")} id={this.state.filterList[this.state.selectedViewIndex].state.toUpperCase() === "RUNNING" ? "Stop" : "Start"}>
                                                        <i className={this.state.specificLoader === "startStopInModal" ? "far fa-cog fa-spin" : this.state.filterList[this.state.selectedViewIndex].state.toUpperCase() === "RUNNING" ? "far fa-stop" : "far fa-play"}></i>
                                                    </button>
                                                    <button type="button" className="btn-transparent btn-transparent-yellow" data-tooltip data-tooltip-text="Restart" data-tooltip-place="bottom" id="restartInModal" disabled={this.state.filterList[this.state.selectedViewIndex].state.toUpperCase() === "STOP"}
                                                        onClick={(e) => this.onChangeEtlState("Restart", this.state.filterList[this.state.selectedViewIndex].state, this.state.filterList[this.state.selectedViewIndex].name, this.state.filterList[this.state.selectedViewIndex].etlType, this.state.filterList[this.state.selectedViewIndex].id, "restartInModal")} >
                                                        <i className={this.state.specificLoader === "restartInModal" ? "far fa-cog fa-spin" : "far fa-sync-alt"}></i>
                                                    </button>
                                                    <button className="btn-transparent btn-transparent-red" data-tooltip data-tooltip-text="Clear Checkpoint" data-tooltip-place="bottom"
                                                        onClick={() => this.clearCheckpointHandler(this.state.filterList[this.state.selectedViewIndex].name)}><i className={this.state.filterList[this.state.selectedViewIndex].isClearCheckPoint ? 'far fa-cog fa-spin' : 'far fa-broom'}></i>
                                                    </button>
                                                    <button type="button" className="btn-transparent btn-transparent-blue" data-tooltip data-tooltip-text="Edit" data-tooltip-place="bottom"
                                                        onClick={() => this.props.history.push(`/addOrEditEtlPipeline/${this.state.filterList[this.state.selectedViewIndex].createdFor}/${this.state.filterList[this.state.selectedViewIndex].id}`)}>
                                                        <i className="far fa-pen"></i>
                                                    </button>
                                                    <button type="button" className="btn-transparent btn-transparent-red" data-tooltip data-tooltip-text="Delete" data-tooltip-place="bottom"
                                                        onClick={() => this.deleteClient(this.state.filterList[this.state.selectedViewIndex].id, this.state.filterList[this.state.selectedViewIndex].name)}
                                                        disabled={this.state.filterList[this.state.selectedViewIndex].etlType && this.state.filterList[this.state.selectedViewIndex].state.toUpperCase() === "RUNNING" || (this.state.filterList[this.state.selectedViewIndex].createdByUserId !== localStorage["userId"] && localStorage.role !== "ACCOUNT_ADMIN")}>
                                                        <i className="far fa-trash-alt"></i>
                                                    </button>
                                                </div>
                                                {this.state.filterList[this.state.selectedViewIndex].state === 'running' ?
                                                    <span className="badge badge-pill badge-success"><i className="fad fa-spinner fa-spin mr-2"></i>Running</span> :
                                                    this.state.filterList[this.state.selectedViewIndex].state === 'pause' ?
                                                        <span className="badge badge-pill badge-warning"><i className="fad fa-pause mr-2"></i>Paused</span> :
                                                        this.state.filterList[this.state.selectedViewIndex].type === "Cloud/Files" ?
                                                            <span className="badge badge-pill badge-success"><i className="fad fa-check mr-2"></i>Completed</span> :
                                                            <span className="badge badge-pill badge-danger"><i className="fad fa-stop mr-2"></i>Stopped</span>
                                                }
                                            </div>
                                        </div>
                                    </div>

                                    <div className="d-flex">
                                        <div className="flex-50 pd-r-7">
                                            <div className="card">
                                                <div className="card-header"><h6>Input Rows</h6></div>
                                                <div className="card-body">
                                                    {this.state.isFetchingMonitoringData ?
                                                        <div className="card-loader">
                                                            <i className="fad fa-sync-alt fa-spin"></i>
                                                        </div> :
                                                        this.state.monitoringData.length > 0 ?
                                                            <div className="card-chart-box">
                                                                <AllChartsComponent timeZone={this.props.timeZone} config={this.createChartData("inputRowsPerSecond")} />
                                                            </div>
                                                            :
                                                            <div className="card-message">
                                                                <p>There is no data to display !</p>
                                                            </div>
                                                    }
                                                </div>
                                            </div>
                                        </div>

                                        <div className="flex-50 pd-l-7">
                                            <div className="card">
                                                <div className="card-header"><h6>Processed Rows</h6></div>
                                                <div className="card-body">
                                                    {this.state.isFetchingMonitoringData ?
                                                        <div className="card-loader">
                                                            <i className="fad fa-sync-alt fa-spin"></i>
                                                        </div> :
                                                        this.state.monitoringData.length > 0 ?
                                                            <div className="card-chart-box">
                                                                <AllChartsComponent timeZone={this.props.timeZone} config={this.createChartData("processedRowsPerSecond")} />
                                                            </div>
                                                            :
                                                            <div className="card-message">
                                                                <p>There is no data to display !</p>
                                                            </div>
                                                    }
                                                </div>
                                            </div>
                                        </div>

                                        <div className="flex-50 pd-r-7">
                                            <div className="card mb-0">
                                                <div className="card-header"><h6>Num Input Rows</h6></div>
                                                <div className="card-body">
                                                    {this.state.isFetchingMonitoringData ?
                                                        <div className="card-loader">
                                                            <i className="fad fa-sync-alt fa-spin"></i>
                                                        </div> :
                                                        this.state.monitoringData.length > 0 ?
                                                            <div className="card-chart-box">
                                                                <AllChartsComponent timeZone={this.props.timeZone} config={this.createChartData("numInputRows")} />
                                                            </div>
                                                            :
                                                            <div className="card-message">
                                                                <p>There is no data to display !</p>
                                                            </div>
                                                    }
                                                </div>
                                            </div>
                                        </div>

                                        <div className="flex-50 pd-l-7">
                                            <div className="card mb-0">
                                                <div className="card-header"><h6>Trigger Execution</h6></div>
                                                <div className="card-body">
                                                    {this.state.isFetchingMonitoringData ?
                                                        <div className="card-loader">
                                                            <i className="fad fa-sync-alt fa-spin"></i>
                                                        </div> :
                                                        this.state.monitoringData.length > 0 ?
                                                            <div className="card-chart-box">
                                                                <AllChartsComponent timeZone={this.props.timeZone} config={this.createChartData("triggerExecution")} />
                                                            </div>
                                                            :
                                                            <div className="card-message">
                                                                <p>There is no data to display !</p>
                                                            </div>
                                                    }
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                }
                {/* end detail modal */}

                {this.state.isOpen &&
                    <NotificationModal
                        type={this.state.modalType}
                        message2={this.state.message2}
                        onCloseHandler={this.onCloseHandler}
                    />
                }

                {this.state.confirmState &&
                    <ConfirmModel
                        deleteName={this.state.deleteTransformationName}
                        confirmClicked={() => {
                            this.props.deleteClient(this.state.clientToBeDeleted);

                            this.setState({
                                isFetching: true,
                                confirmState: false,
                                isOpen: false
                            })
                        }}
                        cancelClicked={() => {
                            this.setState({
                                confirmState: false,
                            })
                        }}
                    />
                }

            </React.Fragment >
        );
    }
}

EtlConfigList.propTypes = {
    dispatch: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
    allEltList: SELECTORS.allEltList(),
    allEltListFailure: SELECTORS.allEltListFailure(),
    etlDeleteFailure: SELECTORS.deleteFailure(),
    etlDelete: SELECTORS.deleteSuccess(),
    changeEtlStateSuccess: SELECTORS.changeEtlStateSuccess(),
    changeEtlStateFailure: SELECTORS.changeEtlStateFailure(),
    debugETLSuccess: SELECTORS.debugETLSuccess(),
    debugETLFailure: SELECTORS.debugETLFailure(),
    isFetching: SELECTORS.getIsFetching(),
    changedState: SELECTORS.changedState(),
    getMonitorDataSuccess: SELECTORS.getMonitorDataSuccess(),
    getMonitorDataFailure: SELECTORS.getMonitorDataFailure(),
    clearCheckpointSuccess: SELECTORS.clearCheckpointSuccess(),
    clearCheckpointFailure: SELECTORS.clearCheckpointFailure(),
});

function mapDispatchToProps(dispatch) {
    return {
        dispatch,
        getEtlList: () => dispatch(ACTIONS.getEtlList()),
        deleteClient: (id) => dispatch(ACTIONS.deleteClient(id)),
        changeEtlState: (state, etlName, etlType, etlId) => dispatch(ACTIONS.changeEtlState(state, etlName, etlType, etlId)),
        debugEtl: (payload) => dispatch(ACTIONS.debugEtl(payload)),
        getMointoringData: (etlName) => dispatch(ACTIONS.getMointoringData(etlName)),
        clearCheckpoint: (etlName) => dispatch(ACTIONS.clearCheckpoint(etlName))
    };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "etlConfigList", reducer });
const withSaga = injectSaga({ key: "etlConfigList", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(EtlConfigList);
