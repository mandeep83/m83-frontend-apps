/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { apiCallHandler } from '../../api';
import { takeEvery } from 'redux-saga';
// import { put } from 'redux-saga/effects';

import * as CONSTANTS from './constants';


export function* getEtlList(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_ETL_LIST_SUCCESS, CONSTANTS.GET_ETL_LIST_FAILURE, 'etlActions')];
}

export function* apiDeleteEtl(action) {
  yield [apiCallHandler(action, CONSTANTS.DELETE_ETL_SUCCESS, CONSTANTS.DELETE_ETL_FAILURE, 'etlDeleteHandle')];
}

export function* changeETLStateApiHandler(action) {
  yield [apiCallHandler(action,CONSTANTS.CHANGE_ETL_STATE_SUCCESS,CONSTANTS.CHANGE_ETL_STATE_FAILURE, 'changeETLState')];
}

export function* debugETLApiHandler(action) {
  yield [apiCallHandler(action,CONSTANTS.DEBUG_ETL_SUCCESS,CONSTANTS.DEBUG_ETL_FAILURE, 'debugETL')];
}

export function* getMonitorDataHandler(action) {
  yield [apiCallHandler(action,CONSTANTS.GET_MONITOR_DATA_SUCCESS,CONSTANTS.GET_MONITOR_DATA_FAILURE, 'getMonitorData')];
}

export function* clearCheckpoint(action) {
  yield [apiCallHandler(action,CONSTANTS.CLEAR_CHECKPOINT_SUCCESS,CONSTANTS.CLEAR_CHECKPOINT_FAILURE, 'clearCheckpoint')];
}

export function* watcherGetEtlList() {
  yield takeEvery(CONSTANTS.GET_ETL_LIST, getEtlList);
}

export function* watcherDeleteEtl() {
  yield takeEvery(CONSTANTS.DELETE_ETL, apiDeleteEtl);
}

export function* watcherChangeETLState() {
  yield takeEvery(CONSTANTS.CHANGE_ETL_STATE, changeETLStateApiHandler);
}

export function* watcherDebugEtlRequest() {
  yield takeEvery(CONSTANTS.DEBUG_ETL, debugETLApiHandler);
}


export function* watcherGetMonitorData() {
  yield takeEvery(CONSTANTS.GET_MONITOR_DATA, getMonitorDataHandler);
}

export function* watcherClearCheckpoint() {
  yield takeEvery(CONSTANTS.CLEAR_CHECKPOINT, clearCheckpoint);
}

export default function* rootSaga() {
  yield [watcherGetMonitorData(), watcherGetEtlList(), watcherDeleteEtl(), watcherChangeETLState(),watcherDebugEtlRequest(),watcherClearCheckpoint()];
}
