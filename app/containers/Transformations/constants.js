/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

export const DEFAULT_ACTION = "app/EtlConfigList/DEFAULT_ACTION";

export const GET_ETL_LIST = "app/EtlConfigList/GET_ETL_LIST";
export const GET_ETL_LIST_SUCCESS = "app/EtlConfigList/GET_ETL_LIST_SUCCESS";
export const GET_ETL_LIST_FAILURE = "app/EtlConfigList/GET_ETL_LIST_FAILURE";

export const GET_ETL_LIST_PLAY = "app/EtlConfigList/GET_ETL_LIST_PLAY";
export const GET_ETL_LIST_PLAY_SUCCESS = "app/EtlConfigList/GET_ETL_LIST_PLAY_SUCCESS";
export const GET_ETL_LIST_PLAY_FAILURE = "app/EtlConfigList/GET_ETL_LIST_PLAY_FAILURE";


export const DELETE_ETL = "app/EtlConfigList/DELETE_ETL";
export const DELETE_ETL_SUCCESS = "app/EtlConfigList/DELETE_ETL_SUCCESS";
export const DELETE_ETL_FAILURE = "app/EtlConfigList/DELETE_ETL_FAILURE";

export const CHANGE_ETL_STATE = "app/EtlConfigList/CHANGE_ETL_STATE";
export const CHANGE_ETL_STATE_SUCCESS = "app/EtlConfigList/CHANGE_ETL_STATE_SUCCESS";
export const CHANGE_ETL_STATE_FAILURE = "app/EtlConfigList/CHANGE_ETL_STATE_FAILURE";

export const DEBUG_ETL = "app/EtlConfigList/DEBUG_ETL";
export const DEBUG_ETL_SUCCESS = "app/EtlConfigList/DEBUG_ETL_SUCCESS";
export const DEBUG_ETL_FAILURE = "app/EtlConfigList/DEBUG_ETL_FAILURE";

export const GET_MONITOR_DATA = "app/EtlConfigList/GET_MONITOR_DATA";
export const GET_MONITOR_DATA_SUCCESS = "app/EtlConfigList/GET_MONITOR_DATA_SUCCESS";
export const GET_MONITOR_DATA_FAILURE = "app/EtlConfigList/GET_MONITOR_DATA_FAILURE";

export const CLEAR_CHECKPOINT = "app/EtlConfigList/CLEAR_CHECKPOINT";
export const CLEAR_CHECKPOINT_SUCCESS = "app/EtlConfigList/CLEAR_CHECKPOINT_SUCCESS";
export const CLEAR_CHECKPOINT_FAILURE = "app/EtlConfigList/CLEAR_CHECKPOINT_FAILURE";