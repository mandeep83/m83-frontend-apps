/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { DEFAULT_ACTION, GET_ETL_LIST, DELETE_ETL, CHANGE_ETL_STATE,DEBUG_ETL, GET_ETL_LIST_PLAY, GET_MONITOR_DATA, CLEAR_CHECKPOINT } from "./constants";

export function defaultAction() {
  return {
    type: DEFAULT_ACTION
  };
}

export function getEtlListPlay(name, type, ) {
  return {
    type: GET_ETL_LIST_PLAY
  };
}

export function getEtlList() {
  return {
    type: GET_ETL_LIST,
  };
}

export function deleteClient(id) {
  return {
    type: DELETE_ETL,
    id
  };
}

export function changeEtlState(state, etlName, etlType,etlId) {
  return {
    type: CHANGE_ETL_STATE,
    state,
    etlName,
    etlType,
    etlId
  };
}

export function getMointoringData(etlName){
  return{
    type: GET_MONITOR_DATA,
    etlName
  };
}

export function debugEtl(payload){
  return{
    type:DEBUG_ETL,
    payload
  }
}

export function clearCheckpoint(etlName){
  return{
    type: CLEAR_CHECKPOINT,
    etlName
  }
}