/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
 * Direct selector to the etlConfigList state domain
 */

const selectEtlConfigListDomain = state =>
  state.get("etlConfigList", initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by EtlConfigList
 */

export const allEltList = () => createSelector(selectEtlConfigListDomain, substate => substate.allEltList);
export const allEltListFailure = () => createSelector(selectEtlConfigListDomain, substate => substate.allEltListFailure);

export const deleteSuccess = () => createSelector(selectEtlConfigListDomain, substate => substate.deleteSuccess);
export const deleteFailure = () => createSelector(selectEtlConfigListDomain, substate => substate.deleteFailure);

export const changeEtlStateSuccess = () => createSelector(selectEtlConfigListDomain, substate => substate.changeEtlStateSuccess);
export const changeEtlStateFailure = () => createSelector(selectEtlConfigListDomain, substate => substate.changeEtlStateFailure);

export const debugETLSuccess = () => createSelector(selectEtlConfigListDomain, substate => substate.debugETLSuccess);
export const debugETLFailure = () => createSelector(selectEtlConfigListDomain, substate => substate.debugETLFailure);

export const changedState = () => createSelector(selectEtlConfigListDomain, substate => substate.changedState);

export const getMonitorDataSuccess = () => createSelector(selectEtlConfigListDomain, substate => substate.getMonitorDataSuccess);
export const getMonitorDataFailure = () => createSelector(selectEtlConfigListDomain, substate => substate.getMonitorDataFailure);

export const isFetchingState = state => state.get('loader');

export const getIsFetching = () => createSelector(isFetchingState, substate => substate.get('isFetching'));

export const clearCheckpointSuccess = () => createSelector(selectEtlConfigListDomain, substate => substate.clearCheckpointSuccess);
export const clearCheckpointFailure = () => createSelector(selectEtlConfigListDomain, substate => substate.clearCheckpointFailure); 