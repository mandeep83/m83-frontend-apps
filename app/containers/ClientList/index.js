/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * ClientList
 *
 */

import React from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {Helmet} from "react-helmet";
import {FormattedMessage} from "react-intl";
import {createStructuredSelector} from "reselect";
import {compose} from "redux";

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import makeSelectClientList from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import messages from "./messages";
import ListingTable from "../../components/ListingTable/Loadable";

/* eslint-disable react/prefer-stateless-function */
export class ClientList extends React.Component {
    state = {
        clientList: [
            {
                clientName: 'TestClient',
                description: 'This is test client',
                applicationCount: '2',
                userCount: '3',
                createdBy: "Vikas Shukla",
                updatedBy: "Vikas Shukla"
            }
        ]
    }
    getColumns = () => {
        return [
            {Header: "Logo", width: 10, accessor: "clientImage",
                cell: (row) => (
                    <div className="list-view-icon"><img src={require('../../assets//images/other/user.png')}></img></div>
                )
            },
            {Header: "Name", width: 20, accessor: "clientName",
                cell: (row) => (
                    <React.Fragment>
                        <h6>{row.clientName}</h6>
                        <p>{row.description}</p>
                    </React.Fragment>
                )
            },
            {Header: "Application(s)", width: 10, accessor: "applicationCount",
                cell: (row) => (
                    <div className="alert alert-primary list-view-badge">{row.applicationCount}</div>
                )
            },
            {Header: "User(s)", width: 15, accessor: "userCount",
                cell: (row) => (
                    <div className="user-initial-list">
                        <spa className="user-initial dynamic-colors">NT</spa>
                        <span className="user-initial dynamic-colors">JP</span>
                        <span className="user-initial dynamic-colors">VS</span>
                        <span className="user-initial dynamic-colors">SM</span>
                        <span className="user-initial user-initial-end">+5</span>
                    </div>
                )
            },
            {Header: "Created", width: 15, accessor: "createdBy" },
            {Header: "Updated", width: 15, accessor: "updatedBy" },
            {Header: "Actions", width: 15,
                cell: (row) => (
                    <div className="button-group">
                        <button className="btn-transparent btn-transparent-blue"><i className="far fa-pencil"></i></button>
                        <button className="btn-transparent btn-transparent-red"><i className="far fa-trash-alt"></i></button>
                    </div>
                )
            },
        ]
    }

    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>Client List</title>
                    <meta name="description" content="M83-Client List"/>
                </Helmet>

                <header className="content-header d-flex">
                    <div className="flex-60">
                        <h6>Clients <span className="text-green fw-600">(10)</span></h6>
                    </div>
                    <div className="flex-40 text-right">
                        <div className="content-header-group">
                            <div className="search-box">
                                <span className="search-icon"><i className="far fa-search"></i></span>
                                <input type="text" className="form-control" placeholder="Search..."/>
                                <button className="search-button"><i className="far fa-times"></i></button>
                            </div>
                            <button className="btn btn-light"><i className="fad fa-list-ul"></i></button>
                            <button className="btn btn-light"><i className="fad fa-table"></i></button>
                            <button className="btn btn-primary" onClick={() => this.props.history.push('/addOrEditClient')}><i className="far fa-plus"></i></button>
                        </div>
                    </div>
                </header>

                <div className="content-body" id="client-list">
                    <ListingTable
                        columns={this.getColumns()}
                        data={this.state.clientList}
                    />
                </div>

            </React.Fragment>
        );
    }
}

ClientList.propTypes = {
    dispatch: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
    clientlist: makeSelectClientList()
});

function mapDispatchToProps(dispatch) {
    return {
        dispatch
    };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({key: "clientList", reducer});
const withSaga = injectSaga({key: "clientList", saga});

export default compose(
    withReducer,
    withSaga,
    withConnect
)(ClientList);
