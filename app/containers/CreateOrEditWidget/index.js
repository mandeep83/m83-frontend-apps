/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import { allSelectors as SELECTORS } from "./selectors";
import { allActions as ACTIONS } from './actions'
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import reducer from "./reducer";
import saga from "./saga";
import CodePlayGround from '../CodePlayGround/Loadable';
import Loader from '../../components/Loader/Loadable';
import { Prompt } from 'react-router'
import { cloneDeep } from "lodash";
import produce from "immer"
import {CDN_LIST} from "../../utils/constants";

/* eslint-disable react/prefer-stateless-function */

export class CreateOrEditWidget extends React.Component {

    state = {
        payload: {
            name: "",
            description: "",
            settings: {
                js: {
                    preProcessor: "BABEL",
                    cdnList: CDN_LIST
                },
                css: {
                    preProcessor: "",
                    cdnList: []
                },
                html: {
                    preProcessor: "",
                    cdnList: []
                },
                image: {
                    preProcessor: "",
                }
            },
            code: {
                html: `<div id="root"></div>`,
                css: "",
                js: `class MyApp extends React.Component {\n\tstate={\n\t\tname: ""\n\t}\n\tcomponentWillMount(){\n\t\tthis.setState({\n\t\t\tname:"Developer"\n\t\t})\n\t}\n\trender() {\n\t\treturn <h1>Hello, {this.state.name}</h1>;\n\t}\n}\n\ReactDOM.render(<MyApp/>,document.getElementById("root"));`,
            }
        },
        activeTab: this.props.match.params.id ? "tab2" : "tab1",
        isFetching: Boolean(this.props.match.params.id),
        errorCode: 0,
        showWarningMessage: false,
        isStatusChecked: false
    }

    componentDidMount() {
        this.props.match.params.id && this.props.getPageById(this.props.match.params.id);
    }

    static getDerivedStateFromProps(nextProps, state) {
        let newProp = Object.keys(SELECTORS).find(prop => nextProps[prop])
        if (newProp) {
            let propData = nextProps[newProp].error ? nextProps[newProp].error : cloneDeep(nextProps[newProp].response)
            if (nextProps.createPageSuccess) {
                let callback = () => {
                    nextProps.history.push("/pages")
                }
                nextProps.showNotification('success', propData, callback)
                return {
                    showWarningMessage: false
                }
            }
            if (nextProps.getPageByIdSuccess) {
                let payload = produce(propData, payload => {
                    payload.code.html = decodeURIComponent(escape(atob(payload.code.html)));
                    payload.code.css = decodeURIComponent(escape(atob(payload.code.css)));
                    payload.code.js = decodeURIComponent(escape(atob(payload.code.js)));
                })
                return {
                    payload,
                    isFetching: false
                }
            }

            if (nextProps.getPageByIdFailure || nextProps.createPageFailure) {
                return {
                    isFetching: false,
                }
            }
        }
        return null
    }
    
    componentDidUpdate(prevProps, prevState) {
        let newProp = Object.keys(SELECTORS).find(prop => this.props[prop])
        if (newProp) {
            if (newProp.includes("Failure")) {
                let message = this.props[newProp].error
                this.props.showNotification("error", message)
            }
            this.props.resetToInitialState(newProp)
        }
        if (this.state.showWarningMessage) {
            window.onbeforeunload = () => true
        } else {
            window.onbeforeunload = undefined
        }
    }

    validateWidget() {
        if (this.state.payload.code.html) {
            return true;
        } else {
            this.props.showNotification("error", "Missing HTML content")
        }
    }

    inputChangeHandler = event => {
        let payload = cloneDeep(this.state.payload);
        payload[event.target.id] = event.target.value;
        this.setState({ payload });
    }

    codeChangeHandler = (html, css, js, settings) => {
        let payload = cloneDeep(this.state.payload);
        if (payload.code.html !== html) {
            this.setState({ htmlError: false })
        }
        payload.settings = settings;
        payload.code.html = html;
        payload.code.css = css;
        payload.code.js = js;
        this.setState({ payload, showWarningMessage: true })
    }

    addWidgetHandler = event => {
        event.preventDefault();
        if (this.validateWidget()) {
            let finalWidgetPayload = JSON.parse(JSON.stringify(this.state.payload));
            finalWidgetPayload.code.html = btoa(unescape(encodeURIComponent(finalWidgetPayload.code.html)));
            finalWidgetPayload.code.css = btoa(unescape(encodeURIComponent(finalWidgetPayload.code.css)));
            finalWidgetPayload.code.js = btoa(unescape(encodeURIComponent(finalWidgetPayload.code.js)));
            this.setState({ isFetching: true }, () => this.props.createPage(finalWidgetPayload, this.props.match.params.id))
        }
    }

    goBack = () => {
        this.props.history.push('/pages');
    }

    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>CreateOrEditPage</title>
                    <meta name="description" content="M83-AddOrEditDashboard" />
                </Helmet>

                <Prompt when={this.state.showWarningMessage} message='Leave site? Changes you made may not be saved.' />

                <header className="content-header d-flex">
                    <div className="flex-60">
                        <div className="d-flex">
                            <h6 className="previous" onClick={this.goBack}>Pages</h6>
                            <h6 className="active">{this.props.match.params.id ? this.state.payload.name : 'Add New'}</h6>
                        </div>
                    </div>
                    <div className="flex-40 text-right">
                        <div className="content-header-group">
                            <button className="btn btn-light" data-tooltip data-tooltip-text="Back" data-tooltip-place="bottom" onClick={this.goBack}><i className="fas fa-angle-left"></i></button>
                        </div>
                    </div>
                </header>

                {this.state.isFetching ?
                    <Loader /> :
                    <div className="content-body">
                        <div className="form-content-wrapper form-content-wrapper-left">
                            <div className="widget-box-wrapper">
                                <CodePlayGround
                                    css={this.state.payload.code.css}
                                    html={this.state.payload.code.html}
                                    js={this.state.payload.code.js}
                                    settings={this.state.payload.settings}
                                    playGroundChange={this.codeChangeHandler}
                                    errorCode={this.state.errorCode}
                                />
                            </div>
                        </div>

                        <form className="form-info-wrapper form-info-wrapper-left" onSubmit={this.addWidgetHandler}>
                            <div className="form-info-header">
                                <h5>Basic Information</h5>
                            </div>
                            <div className="form-info-body form-info-body-adjust">
                                <div className="form-group">
                                    <label className="form-group-label">Name : <i className="fas fa-asterisk form-group-required"></i></label>
                                    <input type="text" id="name" className="form-control" maxlength="25" required value={this.state.payload.name} onChange={this.inputChangeHandler} />
                                    {(this.state.errorCode === 1 || this.state.errorCode === 2) && <span className="form-group-error">{this.state.errorCode === 1 ? "Please fill the required field." : "Name should not be greater than 30 characters."}</span>}
                                </div>

                                <div className="form-group">
                                    <label className="form-group-label">Description :</label>
                                    <textarea rows="3" id="description" className="form-control"
                                        value={this.state.payload.description} onChange={this.inputChangeHandler}
                                    />
                                </div>

                            </div>
                            <div className="form-info-footer">
                                <button className="btn btn-light" type="button" onClick={this.goBack}>Cancel</button>
                                <button className="btn btn-primary" >{this.props.match.params.id ? "Update" : "Save"}</button>
                            </div>
                        </form>
                    </div>
                }
            </React.Fragment>
        );
    }
}

CreateOrEditWidget.propTypes = {
    dispatch: PropTypes.func.isRequired
};

let allSelectors = {}
Object.entries(SELECTORS).map(([key, value]) => {
    allSelectors[key] = value()
})

const mapStateToProps = createStructuredSelector(allSelectors)

function mapDispatchToProps(dispatch) {
    let allActions = { dispatch }
    Object.entries(ACTIONS).map(([key, value]) => {
        allActions[key] = (...args) => dispatch(value(...args))
    })
    return allActions
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "createOrEditWidget", reducer });
const withSaga = injectSaga({ key: "createOrEditWidget", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(CreateOrEditWidget);
