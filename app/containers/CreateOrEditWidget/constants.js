/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

export const RESET_TO_INITIAL_STATE = "app/ManageImages/RESET_TO_INITIAL_STATE";

export const CREATE_PAGE = {
    action : 'app/CreateOrEditWidget/CREATE_PAGE',
    success: "app/CreateOrEditWidget/CREATE_PAGE_SUCCESS",
    failure: "app/CreateOrEditWidget/CREATE_PAGE_FAILURE",
    urlKey: "createPage",
    successKey: "createPageSuccess",
    failureKey: "createPageFailure",
    actionName: "createPage",
    actionArguments : ["payload"]
}

export const GET_PAGE_BY_ID = {
    action : 'app/CreateOrEditWidget/GET_PAGE_BY_ID',
    success: "app/CreateOrEditWidget/GET_PAGE_BY_ID_SUCCESS",
    failure: "app/CreateOrEditWidget/GET_PAGE_BY_ID_FAILURE",
    urlKey: "getPageById",
    successKey: "getPageByIdSuccess",
    failureKey: "getPageByIdFailure",
    actionName: "getPageById",
    actionArguments : ["id"]
}