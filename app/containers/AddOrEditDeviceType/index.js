/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * AddOrEditDeviceType
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import reducer from "./reducer";
import saga from "./saga";
import { allSelectors as SELECTORS } from "./selectors";
import { allActions as ACTIONS } from './actions'
import { uIdGenerator, isNavAssigned } from "../../commonUtils";
import JSONInput from "react-json-editor-ajrm/dist";
import NotificationModal from '../../components/NotificationModal/Loadable'
import isPlainObject from 'lodash/isPlainObject'
import { cloneDeep, isEqual } from "lodash";
import range from 'lodash/range';
import ImageUploader from "../../components/ImageUploader";
import CommandConfigModal from "../../components/CommandConfigModal";
import * as yup from 'yup';
import TagsInput from 'react-tagsinput';

let COMMAND_OBJ = {
    controlName: "",
    controlCommand: {}
}

let client;

/* eslint-disable react/prefer-stateless-function */
export class AddOrEditDeviceType extends React.Component {
    getNewTopolgyLayer = () => {
        return {
            "imagePath": "",
            "connectedVia": "",
            "name": "",
            "id": uIdGenerator()
        }
    }
    state = {
        payload: {
            name: "",
            description: "",
            reportInterval: 10,
            isGateway: false,
            assetType: 'Fixed',
            category: "MQTT",
            source: true,
            deviceTopology: [],
            properties: {
                topics: [
                    {
                        name: 'Report (Publish)',
                        description: 'Send Device Telemetry Data on this Topic',
                        topicType: 'report',
                        topic: '<topicId>/<deviceId>/report',
                    }, {
                        name: 'Control (Subscribe)',
                        description: 'Subscribe to Control Commands from Cloud',
                        topicType: 'control',
                        topic: '<topicId>/<deviceId>/control',
                    }, {
                        name: 'Config (Publish)',
                        description: 'Send Device Config Data on this Topic',
                        topicType: 'config',
                        topic: '<topicId>/<deviceId>/config',
                    }, {
                        name: 'MetaData (Publish)',
                        description: 'Send Device Metadata on this Topic',
                        topicType: 'metaData',
                        topic: '<topicId>/<deviceId>/metaData',
                    }, {
                        name: 'Alarm (Publish)',
                        description: 'Send Device Alarm Data on this Topic',
                        topicType: 'alarm',
                        topic: '<topicId>/<deviceId>/alarm',
                    }, {
                        name: 'Miscellaneous (Publish)',
                        description: 'Send any other Device Data on this Topic',
                        topicType: 'miscellaneous/#',
                        topic: '<topicId>/miscellaneous/#',
                    }],
                deviceIds: []
            }
        },
        topologyEdit: false,
        selectedTopic: {
            name: '',
            schema: {}
        },
        isDeviceImageChanged: false,
        messages: [],
        sniffingTopic: '',
        dataView: 'FILE',
        dataConnectors: [],
        userJson: {},
        noOfClientIds: "1",
        topicSuffix: '',
        generatingClientIds: false,
        availableClientIds: 0,
        isDescriptionChanged: false,
        isLoader: false,
        saveMqttLoader: false,
        emailCredentialsInitiated: false,
        activeCommandIndex: -1,
        controlConfig: [],
        isFetching: true,
        topologyDetails: [],
        paginationPayload: {
            max: 10,
            offset: 0,
            connectorId: this.props.match.params.id,
            deviceGroupId: "ALL",
            tagId: "ALL"
        },
        activePageNumber: 1,
        totalDevices: 0,
        totalClientIds: 0,
        COMMAND_OBJ: {
            controlName: "",
            controlCommand: {}
        },
        listOfDeviceIds: [],
        customDeviceIds: [],
        isCustomDeviceIdEnabled: false
    }

    componentDidMount() {
        if (this.props.match.params.id) {
            let payload = {
                max: 10,
                offset: 0
            }
            this.props.getDeviceTypeById(payload, this.props.match.params.id)
            this.setState({
                isFetching: true
            })
        }
        else {
            this.props.getDeveloperQuota([
                {
                    dependingProductId: null,
                    productName: "no_of_devices",
                }
            ]);
        }
    }

    componentWillReceiveProps(nextProps) {
        let newProp = Object.keys(SELECTORS).find(prop => nextProps[prop])
        if (nextProps.getDeviceTypeByIdSuccess && nextProps.getDeviceTypeByIdSuccess !== this.props.getDeviceTypeByIdSuccess) {
            let _payload = cloneDeep(nextProps.getDeviceTypeByIdSuccess.response),
                topics = cloneDeep(this.state.payload.properties.topics),
                controlConfig = _payload.controlConfig.map(el => {
                    el.controlCommand = JSON.parse(el.controlCommand)
                    return el
                }),
                commandModalPayload = cloneDeep(controlConfig),
                topologyDetails = nextProps.getDeviceTypeByIdSuccess.response.deviceTopology;

            delete _payload.controlConfig;
            delete _payload.topologyDetails;

            if (topics[0].topic === '<topicId>/<deviceId>/report') {
                _payload.properties.topics && _payload.properties.topics.map((topicInfo, index) => {
                    if (topics[index]) {
                        topics[index]['topic'] = topicInfo.name.replace('+', '<deviceId>');
                    } else {
                        topics[index] = {
                            name: 'Miscellaneous (Publish)',
                            description: 'Custom Miscellaneous Topic',
                            topicType: 'miscellaneous/#',
                            topic: topicInfo.name.replace('+', '<deviceId>')
                        };
                    }
                })
                _payload.properties.topics = topics;
            }

            this.setState(prevState => ({
                payload: _payload,
                controlConfig,
                commandModalPayload,
                topologyConfigured: Boolean(topologyDetails.length),
                topologyDetails: topologyDetails.length ? topologyDetails : prevState.topologyDetails,
            }), () => this.props.getDeveloperQuota([{
                dependingProductId: null,
                productName: "no_of_devices",
            }]));
        }
        if (nextProps.getDeviceTypeByIdFailure) {
            return this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.getDeviceTypeByIdFailure.error,
                isFetching: false
            });
        }


        if (nextProps.getAllDevicesByConnectorIdSuccess) {
            let listOfDeviceIds = nextProps.getAllDevicesByConnectorIdSuccess.response.devices.map((el) => {
                return {
                    id: el._uniqueDeviceId,
                    name: el.name
                }
            });
            return this.setState({
                listOfDeviceIds,
                totalDevices: nextProps.getAllDevicesByConnectorIdSuccess.response.deviceCount,
                totalPages: Math.ceil(nextProps.getAllDevicesByConnectorIdSuccess.response.deviceCount / 10),
                generateDeviceLoader: false,
                isFetching: false
            });
        }

        if (nextProps.getAllDevicesByConnectorIdFailure) {
            return this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.getAllDevicesByConnectorIdFailure.error,
                generateDeviceLoader: false,
                isFetching: false
            });
        }

        if (nextProps.saveMqttConnectorSuccess) {
            let payload = cloneDeep(this.state.payload),
                paginationPayload = cloneDeep(this.state.paginationPayload);
            payload.id = nextProps.saveMqttConnectorSuccess.response.id
            paginationPayload.connectorId = nextProps.saveMqttConnectorSuccess.response.id // updating connectorId in paginationPayload
            return this.setState({
                payload,
                paginationPayload,
                saveMqttLoader: false,
                isDescriptionChanged: false
            }, () => this.props.history.push(`/addOrEditDeviceType/${payload.id}`));
        }

        if (nextProps.saveMqttConnectorFailure) {
            return this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.saveMqttConnectorFailure.error,
                saveMqttLoader: false,
                isDescriptionChanged: false
            });
        }

        if (nextProps.updateDeviceSuccess) {
            this.setState({
                modalType: "success",
                isOpen: true,
                message2: nextProps.updateDeviceSuccess.response,
                saveMqttLoader: false,
                isDeviceImageChanged: false,
                isDescriptionChanged: false
            })
            return;
        }

        if (nextProps.updateDeviceFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.updateDeviceFailure.error,
                saveMqttLoader: false
            })
            return;
        }

        if (nextProps.generateClientIdsSuccess) {
            let payload = cloneDeep(this.state.payload),
                availableClientIds = this.state.availableClientIds,
                totalDevices = this.state.totalDevices,
                listOfDeviceIds = cloneDeep(this.state.listOfDeviceIds),
                deviceIds = listOfDeviceIds.length ? listOfDeviceIds : [];
            totalDevices = Boolean(totalDevices) ?
                totalDevices + nextProps.generateClientIdsSuccess.response.deviceIds.length : nextProps.generateClientIdsSuccess.response.deviceIds.length
            nextProps.generateClientIdsSuccess.response.deviceIds.map(el => {
                deviceIds.push({
                    id: el,
                    name: null
                });
            });
            listOfDeviceIds = deviceIds.slice(0, 10);
            if (payload.properties.topics[0].topic === '<topicId>/<deviceId>/report') {
                payload.properties.topics.map(topicInfo => {
                    topicInfo.topic = topicInfo.topic.replace('<topicId>', nextProps.generateClientIdsSuccess.response.topic);
                })
            }
            availableClientIds = availableClientIds - (nextProps.generateClientIdsSuccess.response.deviceIds.length);
            return this.setState({
                listOfDeviceIds,
                modalType: "success",
                isOpen: true,
                message2: "Request Completed Successfully",
                payload,
                availableClientIds,
                isLoader: false,
                generateDeviceLoader: false,
                generatingClientIds: false,
                noOfClientIds: availableClientIds > 0 ? "1" : "0",
                totalDevices,
                totalPages: Math.ceil(totalDevices / 10),
                customDeviceIds: []
            }, () => {
                this.scrollToMyRef()
            });
        }

        if (nextProps.generateClientIdsFailure) {
            return this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.generateClientIdsFailure.error,
                isLoader: false,
                generatingClientIds: false,
                generateDeviceLoader: false
            });
        }

        if (nextProps.deleteDeviceSuccess) {
            let message2 = JSON.stringify(nextProps.deleteDeviceSuccess.response) === "{}" ? "Device deleted Successfully." : nextProps.deleteDeviceSuccess.response,
                availableClientIds = this.state.availableClientIds + 1;
            this.setState({
                modalType: "success",
                isOpen: true,
                message2,
                isLoader: false,
                availableClientIds,
                noOfClientIds: "1",
                activePageNumber: 1,
            }, () => {
                this.props.getAllDevicesByConnectorId({
                    max: 10,
                    offset: 0,
                    connectorId: this.props.match.params.id,
                    deviceGroupId: "ALL",
                    tagId: "ALL"
                })
            });
        }

        if (nextProps.deleteDeviceFailure) {
            return this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.deleteDeviceFailure.error,
                isLoader: false,
                generateDeviceLoader: false
            });
        }

        if (nextProps.emailCredentialsSuccess) {
            let payload = cloneDeep(this.state.payload);
            payload.mailRequest = payload.mailRequest ? payload.mailRequest + 1 : 1;
            return this.setState({
                modalType: "success",
                isOpen: true,
                message2: nextProps.emailCredentialsSuccess.response.message,
                emailCredentialsInitiated: false,
                payload
            });
        }

        if (nextProps.emailCredentialsFailure) {
            return this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.emailCredentialsFailure.error,
                emailCredentialsInitiated: false,
            });
        }

        if (nextProps.availableDeviceCountSuccess) {
            let availableClientIds = nextProps.availableDeviceCountSuccess.response.filter(product => product.productName === 'no_of_devices')[0]['remaining'],
                totalClientIds = nextProps.availableDeviceCountSuccess.response.filter(product => product.productName === 'no_of_devices')[0]['total'];
            this.props.match.params.id && this.props.getAllDevicesByConnectorId(this.state.paginationPayload);
            return this.setState({
                generatingClientIds: false,
                availableClientIds,
                totalClientIds,
                noOfClientIds: availableClientIds > 0 ? "1" : "0",
                generateDeviceLoader: false,
                isFetching: Boolean(this.props.match.params.id)
            });
        }

        if (nextProps.availableDeviceCountFailure) {
            return this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.availableDeviceCountFailure.error,
                generatingClientIds: false,
                generateDeviceLoader: false,
                isFetching: false

            });
        }

        if (nextProps.saveCommandsSuccess) {
            let commandModalPayload = cloneDeep(this.state.commandModalPayload);
            let data = nextProps.saveCommandsSuccess.response.data;
            if (nextProps.saveCommandsSuccess.response.isAddMode) {
                commandModalPayload.push({ ...this.state.COMMAND_OBJ, ...data });
            } else {
                let index = commandModalPayload.findIndex(temp => temp.id == nextProps.saveCommandsSuccess.response.data.id);
                let controlCommand = JSON.parse(data.controlCommand)
                commandModalPayload[index] = { ...this.state.COMMAND_OBJ, ...data, controlCommand }
            }
            let controlConfig = cloneDeep(commandModalPayload),
                COMMAND_OBJ = {
                    controlName: "",
                    controlCommand: {},
                    deviceTypeId: this.props.match.params.id
                }
            return this.setState({
                modalType: "success",
                isOpen: true,
                message2: nextProps.saveCommandsSuccess.response.message,
                activeCommandIndex: -1,
                commandModalLoader: false,
                deleteCommandLoader: false,
                controlConfig,
                COMMAND_OBJ,
                commandModalPayload,
                commandImage: "",
            });
        }

        if (nextProps.saveCommandsFailure) {
            return this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.saveCommandsFailure.error,
                commandModalLoader: false,
                deleteCommandLoader: false
            });
        }

        if (nextProps.deleteCommandsSuccess) {
            let commandModalPayload = cloneDeep(this.state.commandModalPayload);
            commandModalPayload = commandModalPayload.filter(temp => temp.id !== nextProps.deleteCommandsSuccess.response.id);
            let controlConfig = cloneDeep(commandModalPayload)
            return this.setState({
                modalType: "success",
                isOpen: true,
                message2: nextProps.deleteCommandsSuccess.response.message,
                deleteCommandLoader: false,
                controlConfig,
                commandModalPayload
            });
        }

        if (nextProps.deleteCommandsFailure) {
            return this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.deleteCommandsFailure.error,
                commandModalLoader: false,
                deleteCommandLoader: false
            });
        }

        if (nextProps.imageUploadSuccess) {
            let { secureUrl, addOns } = nextProps.imageUploadSuccess.response
            let payload = cloneDeep(this.state.payload);
            let topologyDetails = cloneDeep(this.state.topologyDetails);
            if (addOns && addOns.isFor === "topology") {
                let requiredLayer = topologyDetails.find(layer => layer.id === addOns.layerId)
                if (requiredLayer) {
                    requiredLayer.isUploadingImage = false
                    requiredLayer.imagePath = secureUrl
                }
            } else {
                payload.image = secureUrl + `?lastmod=${(new Date()).getTime()}`
            }
            return this.setState({
                payload,
                topologyDetails,
                isUploadingImage: false,
                isDescriptionChanged: true
            });
        }

        if (nextProps.imageUploadFailure) {
            return this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.imageUploadFailure.error,
                isUploadingImage: false
            });
        }
        if (["saveDeviceTopologySuccess", "saveDeviceTopologyFailure"].includes(newProp)) {
            let payload = cloneDeep(this.state.payload)
            if (newProp === "saveDeviceTopologySuccess") {
                payload.deviceTopology = this.state.topologyDetails
            }
            return this.setState(prevState => ({
                topologyLoader: false,
                payload,
                topologyConfigured: newProp === "saveDeviceTopologySuccess" ? true : prevState.topologyConfigured,
            }))
        }
    }

    componentDidUpdate(prevProps, prevState) {
        let newProp = Object.keys(SELECTORS).find(prop => this.props[prop])
        if (newProp) {
            if (["saveDeviceTopologySuccess"].includes(newProp) || ["saveDeviceTopologyFailure"].includes(newProp)) {
                let propData = this.props[newProp].error ? this.props[newProp].error : cloneDeep(this.props[newProp].response)
                let type = newProp.includes("saveDeviceTopologyFailure") ? "error" : "success"
                this.props.showNotification(type, propData, this.onCloseHandler)
            }
            this.props.resetToInitialState(newProp)
        }
    }

    uploadConnectorImage = (file) => {
        this.setState({
            isUploadingImage: true,
            isDeviceImageChanged: true
        }, () => this.props.imageUploadHandler({ file: file, fileType: "DEVICE_TYPE" }))
    }

    onChangeHandler = ({ currentTarget }) => {
        let payload = cloneDeep(this.state.payload),
            connectionTestedSuccessFully = this.state.connectionTestedSuccessFully,
            connectionTestFailed = this.state.connectionTestFailed,
            isTestingConnection = this.state.isTestingConnection,
            isDescriptionChanged = this.state.isDescriptionChanged,
            id = currentTarget.id,
            value = currentTarget.value,
            name = currentTarget.name;
        if (id) {
            if (id === "name") {
                if (/^[a-zA-Z]([\w]*[a-zA-Z0-9_])?$/.test(value) || /^$/.test(value)) {
                    payload[id] = value
                }
            } else if (id === "reportInterval") {
                if (/[0-9]+/.test(value) && parseInt(value) <= 18000)
                    payload[id] = parseInt(value)
                else if (/^$/.test(value)) {
                    payload[id] = value
                }
            } else if (id === 'isGateway') {
                payload[id] = currentTarget.checked;
            } else {
                payload[id] = value;
            }
        }

        if (id === "description" || id === "reportInterval") {
            isDescriptionChanged = true
        }
        this.setState({
            payload,
            connectionTestFailed,
            connectionTestedSuccessFully,
            isTestingConnection,
            isDescriptionChanged
        })
    }

    onCloseHandler = () => {
        this.setState({
            isOpen: false,
            message2: '',
            modalType: '',
        });
    }

    showCommandModal = () => {
        this.setState((prevState, props) => ({
            activeCommandIndex: prevState.controlConfig.length,
            commandModalPayload: prevState.controlConfig.length === 0 ? [] : prevState.controlConfig,
            isAddMode: true
        }))
    }

    addCommandHandler = () => {
        let commandModalPayload = cloneDeep(this.state.commandModalPayload)
        this.setState({
            commandModalPayload
        })
    }

    handleActiveCommand = (index) => {
        this.setState({
            activeCommandIndex: index
        })
    }

    deleteCommand = () => {
        let commandModalPayload = cloneDeep(this.state.commandModalPayload)
        this.setState({
            commandModalPayload,
            deleteCommandLoader: true,
            isOpenCommandModal: false
        }, () => this.props.deleteCommands(this.state.controlConfig[this.state.commandIndexToBeDeleted].id))
    }

    saveCommandsHandler = (COMMAND_OBJ, imageFile) => {
        let payload = {
            controlCommand: JSON.stringify(COMMAND_OBJ.controlCommand),
            controlName: COMMAND_OBJ.controlName.trim(),
            deviceTypeId: this.props.match.params.id,
            id: COMMAND_OBJ.id
        }

        this.setState({
            commandModalLoader: true,
            COMMAND_OBJ
        }, () => {
            this.props.saveCommands(payload, this.state.isAddMode, imageFile)
        })

    }

    closeCommandModal = () => {
        let COMMAND_OBJ = cloneDeep(this.state.COMMAND_OBJ)
        COMMAND_OBJ = {
            controlName: "",
            controlCommand: {},
            deviceTypeId: this.props.match.params.id
        }
        this.setState({
            activeCommandIndex: -1,
            COMMAND_OBJ,
            commandImage: "",
        })
    }

    deleteDeviceHandler = () => {
        let listOfDeviceIds = cloneDeep(this.state.listOfDeviceIds),
            deviceId = listOfDeviceIds[this.state.deviceToBeDeletedIndex].id,
            connectorId = this.props.match.params.id
        this.setState({
            confirmState: false,
            isLoader: true,
            deviceToBeDeletedIndex: null,
            deviceToBeDeletedName: null,
            generateDeviceLoader: true
        }, () => this.props.deleteDevice(deviceId, connectorId))
    }

    toggleConfirmModal = (deviceDetails) => {
        let { id, name } = deviceDetails || {},
            listOfDeviceIds = cloneDeep(this.state.listOfDeviceIds),
            deviceToBeDeletedIndex = id ? listOfDeviceIds.findIndex((obj => obj.id == id)) : null,
            confirmState = Boolean(id),
            deviceToBeDeletedName = id ? name || id : null
        this.setState({
            deviceToBeDeletedIndex,
            confirmState,
            deviceToBeDeletedName
        })
    }

    toggleDeleteCommandModal = (commandIndexToBeDeleted) => {
        this.setState({
            isOpenCommandModal: true,
            commandIndexToBeDeleted
        })
    }

    reviewPayload = () => {
        return yup.object().shape({
            "name": yup.string().required("Device Type name is Required").min(4, "Device Type name should be minimum 4 characters long").max(15, "Device Type name should be maximum 15 characters long"),
            "reportInterval": yup.string().required("Reporting Frequency is required"),
            "description": yup.string(),
        });
    }

    saveMqttConnector = async (e) => {
        e.preventDefault();
        let payload = cloneDeep(this.state.payload)
        payload.properties = {}
        let PAYLOAD_SCHEMA = this.reviewPayload()
        try {
            await PAYLOAD_SCHEMA.validate(payload);
            if (payload.image && (payload.image).includes("?")) {
                payload.image = payload.image.split("?")[0]
            }
            this.setState({
                saveMqttLoader: true,
                isDescriptionChanged: false
            }, () => this.props.saveMqttConnector(payload))
        }
        catch (error) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: error.message,
            })
        }
    }

    changeMqttConnectorDetails = async (e) => {
        e.preventDefault()
        let PAYLOAD_SCHEMA = this.reviewPayload(),
            payload = cloneDeep(this.state.payload);
        try {
            await PAYLOAD_SCHEMA.validate(payload);
            if (payload.image && (payload.image).includes("?")) {
                payload.image = payload.image.split("?")[0]
            }
            this.setState({
                saveMqttLoader: true
            }, () => this.props.updateDevice(payload, this.state.isDeviceImageChanged))
        }
        catch (error) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: error.message,
            })
        }
    }

    generateClientIdsForDeviceType = () => {
        this.setState({
            generatingClientIds: true,
            generateDeviceLoader: true,
            isLoader: true,
        }, () => { this.props.generateClientIds(this.state.noOfClientIds, this.props.match.params.id, this.state.customDeviceIds) })
    }

    scrollToMyRef = () => {
        this.myRef && this.myRef.scrollIntoView();
    }

    myRef = React.createRef()

    changeTopologyLayer = (action, index) => {
        let topologyDetails = cloneDeep(this.state.topologyDetails)
        if (action === "ADD") {
            topologyDetails.push(this.getNewTopolgyLayer())
        }
        else {
            topologyDetails.splice(index, 1)
            // topologyDetails[topologyDetails.length - 1].connectedViaError = false
            if (index) {
                topologyDetails[index - 1].connectedVia = ""
                topologyDetails[topologyDetails.length - 1].connectedViaError = false
            }
        }
        this.setState({
            topologyDetails
        })
    }

    topologyDetailsChangeHandler = (index, key, value) => {
        let topologyDetails = cloneDeep(this.state.topologyDetails)
        if (key === "name") {
            topologyDetails[index][key] = value.trim()
            topologyDetails[index][key + "Error"] = !(value.trim())
        }
        else {
            topologyDetails[index][key] = value
            topologyDetails[index][key + "Error"] = !value
        }
        this.setState({
            topologyDetails
        })
    }

    uploadLayerImage = (file, layerId) => {
        let topologyDetails = cloneDeep(this.state.topologyDetails)
        let requiredLayerIndex = topologyDetails.findIndex(layer => layer.id === layerId)
        topologyDetails[requiredLayerIndex].isUploadingImage = true
        let payload = { file, fileType: "TEMP" }
        let addOns = {
            isFor: "topology",
            layerId
        }
        this.setState({
            topologyDetails
        }, () => this.props.imageUploadHandler(payload, addOns))
    }

    saveDeviceTopology = () => {
        let topologyDetails = cloneDeep(this.state.topologyDetails)
        if (topologyDetails.every((layer, index) => layer.name && (layer.connectedVia || (index + 1 === topologyDetails.length)))) {
            topologyDetails.forEach((layer, index) => {
                layer.index = index
                layer.connectedVia = layer.connectedVia || null
                delete layer.connectedViaError
                delete layer.nameError
            })
            let payload = {
                deviceTopology: topologyDetails
            }
            this.setState({
                topologyLoader: true
            }, () => this.props.saveDeviceTopology(payload, this.state.payload.id, this.state.topologyConfigured))
        }
        else {
            topologyDetails.forEach((layer, index) => {
                if (!layer.name || !layer.connectedVia) {
                    layer.nameError = !layer.name
                    layer.connectedViaError = !layer.connectedVia && (index + 1 != topologyDetails.length)
                }
            })
            this.setState({
                topologyDetails
            })
        }
    }

    noOfClientIdsChangeHandler = ({ currentTarget }) => {
        if (/^[1-9]\d*$/.test(currentTarget.value) && currentTarget.value <= this.state.availableClientIds && currentTarget.value <= 500 || currentTarget.value === '') {
            this.setState({
                noOfClientIds: currentTarget.value,
            })
        }
    }

    pageChangeHandler = (activePageNumber) => {
        let paginationPayload = cloneDeep(this.state.paginationPayload);
        if (activePageNumber === "...") {
            activePageNumber = Math.ceil((this.getPageNumbers()[3] - 1) / 2)
        }

        paginationPayload = {
            offset: (activePageNumber - 1) * 10,
            max: 10,
            connectorId: this.state.payload.id,
            deviceGroupId: "ALL",
            tagId: "ALL",
        }
        this.setState({
            activePageNumber,
            paginationPayload,
            generateDeviceLoader: true
        }, () => this.props.getAllDevicesByConnectorId(paginationPayload))
    }

    getPageNumbers = () => {
        let totalPages = this.state.totalPages,
            activePageNumber = this.state.activePageNumber
        if (totalPages < 6)
            return range(1, totalPages + 1)
        else {
            let start = activePageNumber < 4 ? 1 : activePageNumber - 2
            let end = activePageNumber < 4 ? 6 : (activePageNumber + 3 > totalPages) ? totalPages + 1 : activePageNumber + 3
            if (end - start < 5)
                start = end - 5
            let arr = range(start, end)
            if (start !== 1)
                arr = [1, "...", ...arr]
            return arr
        }
    }

    isOpenRpcCommand = (index) => {
        let COMMAND_OBJ = cloneDeep(this.state.COMMAND_OBJ)
        let displayObj = this.state.commandModalPayload[index]
        COMMAND_OBJ.controlName = displayObj.controlName
        COMMAND_OBJ.controlCommand = displayObj.controlCommand
        COMMAND_OBJ.deviceTypeId = this.props.match.params.id
        COMMAND_OBJ.id = displayObj.id
        COMMAND_OBJ.isEdit = true

        this.setState({ COMMAND_OBJ, activeCommandIndex: index, isAddMode: false, commandImage: displayObj.imageUrl })
    }

    copyToClipboard = (topicId) => {
        navigator.clipboard.writeText(topicId)
        this.setState({
            modalType: "success",
            isOpen: true,
            message2: 'Copied to Clipboard successfully !',
        })
    }

    onDeleteRPCImage = () => {
        let commandModalPayload = cloneDeep(this.state.commandModalPayload);
        let requiredCommand = commandModalPayload.find(temp => temp.id === this.state.COMMAND_OBJ.id);
        requiredCommand.imageUrl = ""
        let controlConfig = cloneDeep(commandModalPayload)
        let COMMAND_OBJ = cloneDeep(this.state.COMMAND_OBJ)
        COMMAND_OBJ = {
            controlName: "",
            controlCommand: {},
            deviceTypeId: this.props.match.parampls.id
        }

        return this.setState({
            modalType: "success",
            isOpen: true,
            message2: "Command Image Reset Successfully",
            controlConfig,
            commandModalPayload,
            activeCommandIndex: -1,
            COMMAND_OBJ,
            commandImage: "",
        });
    }

    toggleDeviceIdType = () => {
        this.setState(prevState => ({
            customDeviceIds: [],
            isCustomDeviceIdEnabled: !prevState.isCustomDeviceIdEnabled
        }))
    }

    customIdTags = (tags) => {
        let lastTagAdded = tags[tags.length - 1];
        let isValidTag = tags.length == 0 || /^(?!-_)[a-zA-Z0-9-_]*[a-zA-Z0-9]$/.test(lastTagAdded);
        if(isValidTag){
            let customDeviceIds = cloneDeep(tags)
            this.setState({
                customDeviceIds
            })
        } else {
            this.props.showNotification("error", "Device ID should only contain only alphaNumeric characters")
        }
    }

    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>Add Or Edit Device Type</title>
                    <meta name="description" content="Description of AddOrEditDeviceType" />
                </Helmet>

                <header className="content-header d-flex">
                    <div className="flex-60">
                        <div className="d-flex">
                            <h6 className="previous" onClick={() => this.props.history.push(`/deviceTypes`)}>Device Types</h6>
                            <h6 className="active">{this.props.match.params.id ? this.state.payload.name : "Add New"}</h6>
                        </div>
                    </div>
                    <div className="flex-40 text-right">
                        <div className="content-header-group">
                            <button className="btn btn-light" data-tooltip data-tooltip-text="Back" data-tooltip-place="bottom" onClick={() => this.props.history.push(`/deviceTypes`)}><i className="fas fa-angle-left"></i></button>
                        </div>
                    </div>
                </header>

                {this.state.isFetching ?
                    <Loader /> :
                    <div className="content-body">
                        <div className="form-content-wrapper">
                            <div className="form-content-box">
                                <div className="form-content-header">
                                    <p>Basic Information</p>
                                </div>
                                <div className="form-content-body">
                                    <div className="d-flex">
                                        <div className="flex-50 pd-r-10">
                                            <div className="d-flex">
                                                <div className="flex-50 pd-r-10">
                                                    <div className="form-group">
                                                        <label className="form-group-label">Name : <i className="fas fa-asterisk form-group-required"></i></label>
                                                        <input type="text" placeholder="e.g. RaspberryPi or NodeMCU" id="name" className="form-control" value={this.state.payload.name} onChange={this.onChangeHandler} readOnly={this.props.match.params.id} />
                                                    </div>
                                                </div>
                                                <div className="flex-50 pd-l-10">
                                                    <div className="form-group">
                                                        <label className="form-group-label">Reporting Frequency : <i className="fas fa-asterisk form-group-required"></i></label>
                                                        <div className="input-group">
                                                            <input type="text" id="reportInterval" className="form-control" value={this.state.payload.reportInterval} onChange={this.onChangeHandler} />
                                                            <div className="input-group-append"><span className="input-group-text">secs</span></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                {/**/}
                                            </div>
                                            <div className="d-flex">
                                                <div className="flex-50 pd-r-10">
                                                    <div className="form-group">
                                                        <label className="form-group-label">Asset Type : <i className="fas fa-asterisk form-group-required"></i></label>
                                                        <div className="d-flex pd-t-5">
                                                            <div className="flex-50 pd-r-5">
                                                                <label className={`radio-button ${this.props.match.params.id && "radio-button-disabled"}`}>
                                                                    <span className="radio-button-text">Fixed</span>
                                                                    <input
                                                                        type="radio"
                                                                        value="Fixed"
                                                                        id="assetType"
                                                                        checked={this.state.payload.assetType === 'Fixed'}
                                                                        onChange={this.onChangeHandler}
                                                                        disabled={this.props.match.params.id}
                                                                    />
                                                                    <span className="radio-button-mark"></span>
                                                                </label>
                                                            </div>
                                                            <div className="flex-50 pd-l-5">
                                                                <label className={`radio-button ${this.props.match.params.id && "radio-button-disabled"}`}>
                                                                    <span className="radio-button-text">Movable</span>
                                                                    <input
                                                                        type="radio"
                                                                        value="Movable"
                                                                        id="assetType"
                                                                        checked={this.state.payload.assetType === 'Movable'}
                                                                        onChange={this.onChangeHandler}
                                                                        disabled={this.props.match.params.id}
                                                                    />
                                                                    <span className="radio-button-mark"></span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="flex-50 pd-l-10">
                                                    <div className="form-group">
                                                        <label className="form-group-label">Is Gateway :</label>
                                                        <div className={`toggle-switch-wrapper pd-t-10 ${typeof this.props.match.params.id === 'undefined' ? '' : 'toggle-switch-wrapper-disabled'}`}>
                                                            <label className="toggle-switch mr-l-0">
                                                                <input type="checkbox" id="isGateway" disabled={typeof this.props.match.params.id !== 'undefined'} checked={this.state.payload.isGateway} onChange={this.onChangeHandler} />
                                                                <span className="toggle-switch-slider"></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="flex-30 pd-l-5 pd-r-5">
                                            <div className="form-group">
                                                <label className="form-group-label">Description :</label>
                                                <textarea type="text" id="description" className="form-control" value={this.state.payload.description} onChange={this.onChangeHandler} style={{ height: 125 }} />
                                            </div>
                                        </div>

                                        <div className="flex-20 pd-l-10">
                                            <div className="form-group">
                                                <label className="form-group-label">Image :
                                                    <button type="button" className="btn btn-link float-right p-0 f-12" disabled={!this.state.payload.image} onClick={() => {
                                                        let payload = cloneDeep(this.state.payload)
                                                        payload.image = ''
                                                        this.setState({
                                                            payload,
                                                            isDescriptionChanged: true,
                                                            isDeviceImageChanged: true
                                                        })
                                                    }}>
                                                        <i className="far fa-redo-alt"></i>Reset
                                                    </button>
                                                </label>
                                                <div className="file-upload-box">
                                                    <ImageUploader
                                                        loader={this.state.isUploadingImage}
                                                        imagePath={this.state.payload.image ? this.state.payload.image + `?lastmod=${(new Date()).getTime()}` : ""}
                                                        uploadHandler={this.uploadConnectorImage}
                                                        classNames={{ blankUploader: "file-upload-form" }}
                                                        acceptFileType="image/x-png,image/gif,image/jpeg"
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    {this.state.saveMqttLoader &&
                                        <div className="form-content-body-overlay">
                                            <i className="fad fa-sync-alt fa-spin text-theme f-20"></i>
                                        </div>
                                    }
                                </div>
                                <div className="form-content-footer">
                                    {this.props.match.params.id ?
                                        <button
                                            // type='submit'
                                            className="btn btn-primary"
                                            disabled={!(this.props.match.params.id && this.state.isDescriptionChanged) || this.state.isUploadingImage}
                                            onClick={this.changeMqttConnectorDetails}
                                        >
                                            Update
                                        </button>
                                        :
                                        <button
                                            // type='submit'
                                            className="btn btn-success"
                                            disabled={this.props.match.params.id || (!this.state.payload.name) || this.state.isUploadingImage}
                                            onClick={this.saveMqttConnector}
                                        >
                                            Next
                                            <i className="far fa-angle-double-right mr-l-5"></i>
                                        </button>
                                    }
                                </div>
                            </div>

                            <div className="form-content-box">
                                <div className="form-content-header">
                                    <p>Definition
                                        {this.state.listOfDeviceIds.length > 0 &&
                                            <React.Fragment>
                                                <span className="allocation-badge-group float-right"><small className="float-left">Email Device Credentials Quota :</small>
                                                    <span className="allocation-badge-item pd-l-15">Allocated <span
                                                        className="badge badge-pill badge-primary">3</span></span>
                                                    <span className="allocation-badge-item">Used <span
                                                        className="badge badge-pill badge-success">{this.state.payload.mailRequest ? this.state.payload.mailRequest : 0}</span></span>
                                                    <span className="allocation-badge-item">Remaining <span
                                                        className="badge badge-pill badge-warning">{this.state.payload.mailRequest ? 3 - this.state.payload.mailRequest : 3}</span></span>
                                                </span>
                                            </React.Fragment>
                                        }
                                    </p>
                                </div>
                                <div className="form-content-body">
                                    <div className="form-group">
                                        <label className="form-group-label">
                                            Devices <small className="text-cyan fw-600">(Generate 500 devices at a time)</small>
                                            <span className="mr-r-15 mr-l-15">|</span>
                                                Used : <span className="badge badge-pill badge-success fw-600">{this.state.totalDevices ? this.state.totalDevices : "0"}</span>
                                            <span className="mr-r-15 mr-l-15">|</span>
                                                Remaining : <span className="badge badge-pill badge-warning fw-600">{this.state.availableClientIds}</span>
                                        </label>
                                        <div className="input-group">
                                            <input className="form-control" disabled={typeof this.props.match.params.id === 'undefined' || this.state.availableClientIds == 0} type="number" value={this.state.noOfClientIds} onChange={this.noOfClientIdsChangeHandler} />
                                            <div className={`toggle-switch-wrapper mr-r-20 ${typeof this.props.match.params.id === 'undefined' ? 'toggle-switch-wrapper-disabled' : ''}`}>
                                                <label className="toggle-switch">
                                                    <input type="checkbox" disabled={typeof this.props.match.params.id === 'undefined'} checked={this.state.isCustomDeviceIdEnabled} onChange={this.toggleDeviceIdType} />
                                                    <span className="toggle-switch-slider"></span>
                                                </label>
                                                <span className="text-primary">{`${this.state.payload.isGateway ? 'Custom Gateway Id(s)' : 'Custom Device Id(s)'}`}</span>
                                            </div>
                                            <div className="input-group-append">
                                                <button type="button" className={this.state.generatingClientIds ? "btn btn-transparent-green ml-2" : "btn btn-transparent-blue ml-2"}
                                                    disabled={(typeof this.props.match.params.id === 'undefined' || this.state.generatingClientIds || this.state.noOfClientIds === 0 || this.state.noOfClientIds === '') || (this.state.availableClientIds === 0)}
                                                    onClick={() => { this.generateClientIdsForDeviceType() }}>
                                                    <i className={`far fa-cog mr-r-5 ${this.state.generatingClientIds ? "fa-spin" : ""}`}></i>
                                                    {this.state.generatingClientIds ? 'Generating...' : `${this.state.payload.isGateway ? 'Generate Gateways' : 'Generate Client/Device Id(s)'}`}
                                                </button>
                                                <span className="ml-2 mr-2 f-24 text-light">|</span>
                                                {this.state.emailCredentialsInitiated ?
                                                    <button type="button" className="btn btn-transparent-blue cursor-default">
                                                        <i className="fas fa-cog fa-spin mr-r-5"></i>Sending Email...
                                                    </button> :
                                                    <button type="button" className="btn btn-transparent-blue"
                                                        disabled={this.state.listOfDeviceIds.length === 0 || this.state.payload.mailRequest && this.state.payload.mailRequest >= 3}
                                                        onClick={() => {
                                                            this.setState({
                                                                emailCredentialsInitiated: true
                                                            }, () => { this.props.emailCredentials(this.props.match.params.id) })
                                                        }}>
                                                        <i className="far fa-envelope mr-r-5"></i>Send MQTT Credentials
                                                    </button>
                                                }
                                            </div>
                                        </div>
                                    </div>
                                    {this.state.isCustomDeviceIdEnabled &&
                                        <div className="form-group">
                                            <label className="form-group-label">Custom Device Id(s) :
                                            <span className="text-cyan f-10 float-right"><strong>Note:</strong> Please press enter(&crarr;) to add Custom Device Ids.</span>
                                            </label>
                                            <TagsInput
                                                value={this.state.customDeviceIds}
                                                disabled={!this.state.isCustomDeviceIdEnabled}
                                                onlyUnique={true}
                                                inputProps={{ placeholder: 'Add a Device Id' }}
                                                maxTags={this.state.noOfClientIds}
                                                onChange={this.customIdTags}
                                                addOnBlur
                                            />
                                        </div>
                                    }
                                    <div className="form-group">
                                        <label className="form-group-label">MQTT Topic Information :</label>
                                        <div className="content-table">
                                            <table className="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th width="15%">Mode</th>
                                                        <th width="35%">Topic</th>
                                                        <th width="10%">Client Id</th>
                                                        <th width="7.5%">Type</th>
                                                        <th width="22.5%">Notes</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {this.state.payload.properties.topics.map((topicInfo, index) =>
                                                        <tr key={index}>
                                                            <td className={topicInfo.topicType === "control" ? "text-success" : "text-primary"}><span>{topicInfo.name}</span></td>
                                                            <td>
                                                                {!topicInfo.topic.startsWith('<topicId>') &&
                                                                    <button className="btn-transparent btn-transparent-green copy-button" data-tooltip data-tooltip-text="Copy" data-tooltip-place="bottom" onClick={() => this.copyToClipboard(topicInfo.topic)}>
                                                                        <i className="far fa-copy"></i>
                                                                    </button>
                                                                }
                                                                <span>{topicInfo.topic}</span>
                                                            </td>
                                                            <td>{'<deviceId>'}</td>
                                                            <td className={topicInfo.topic.includes('miscellaneous') ? "text-red" : "text-indigo"}>{topicInfo.topic.includes('miscellaneous') ? 'Wildcard' : 'Absolute'}</td>
                                                            <td>{topicInfo.description}</td>
                                                        </tr>
                                                    )}
                                                </tbody>
                                            </table>
                                        </div>
                                        <p className="text-content f-11">
                                            <strong className="text-gray"><sup className="text-red">*</sup>Report (Publish) :</strong> <span className="text-primary cursor-pointer text-underline-hover" onClick={() => { this.props.history.push('/deviceAttributes') }}>Device Attributes</span> (for Auto ETL) will be picked up from the data being reported on this <strong>'/report'</strong> topic. {localStorage.tenantType === "FLEX" && <React.Fragment> For subscribing to data being reported on other topics, please write <span className="text-primary cursor-pointer text-underline-hover" onClick={() => { this.props.history.push('/etlPipeline') }}>Custom ETLs.</span></React.Fragment>}
                                        </p>
                                        <p className="text-content f-11">
                                            <strong className="text-gray"><sup className="text-red">*</sup>Client Id :</strong> Please use the Device Id as the Client Id in <strong>MQTT</strong> Connection String.
                                        </p>
                                    </div>
                                    {/*{(this.props.match.params.id || this.state.saveMqttLoader) ? "" :
                                        <div className="form-content-body-overlay">
                                            <button className="btn btn-success" disabled={this.props.match.params.id || (!this.state.payload.name)} onClick={this.saveMqttConnector}>
                                                Proceed Next<i className="far fa-angle-double-right mr-l-5"></i>
                                            </button>
                                        </div>
                                    }*/}
                                </div>
                            </div>

                            <div className="form-content-box" ref={(ref) => this.myRef = ref}>
                                <div className="form-content-header">
                                    <p>Devices <strong className="text-indigo">({this.state.totalDevices ? this.state.totalDevices : "0"})</strong></p>
                                    <button className="btn-transparent btn-transparent-blue" data-tooltip data-tooltip-text="Edit" data-tooltip-place="bottom" onClick={() => (isNavAssigned('devices') && this.props.match.params.id) && this.props.history.push(`/devices/${this.props.match.params.id}`)}>
                                        <i className="far fa-pencil"></i>
                                    </button>
                                </div>
                                <div className="form-content-body">
                                    {this.state.generateDeviceLoader ?
                                        <div className="inner-loader-wrapper" style={{ height: 200 }}>
                                            <div className="inner-loader-content">
                                                <i className="fad fa-sync-alt fa-spin"></i>
                                            </div>
                                        </div>
                                        :
                                        this.state.listOfDeviceIds.length ?
                                            <React.Fragment>
                                                <ul className="list-style-none d-flex device-list">
                                                    {this.state.listOfDeviceIds.map((deviceObj, index) => {
                                                        return (
                                                            <li key={index}>
                                                                <div className="device-list-box">
                                                                    <p>{Boolean(deviceObj.name) ? deviceObj.name : deviceObj.id}</p>
                                                                    <div className="button-group">
                                                                        <button type="button" className="btn-transparent btn-transparent-red" data-tooltip data-tooltip-text="Delete" data-tooltip-place="bottom" data-tooltip data-tooltip-text="Delete" data-tooltip-place="bottom" onClick={() => this.toggleConfirmModal(deviceObj)}><i className="far fa-trash-alt"></i></button>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        )
                                                    })}
                                                </ul>
                                                <ul className="pagination list-style-none">
                                                    <li className="page-item" onClick={() => this.state.activePageNumber > 1 && this.pageChangeHandler(this.state.activePageNumber - 1)}>
                                                        <a className={`page-link ${this.state.activePageNumber > 1 ? "active" : ""}`}>
                                                            <i className="far fa-angle-double-left mr-2"></i>Previous
                                                        </a>
                                                    </li>
                                                    {this.getPageNumbers().map((pageNumber, index) => {
                                                        return (
                                                            <li key={index} className="page-item" onClick={() => pageNumber !== this.state.activePageNumber && this.pageChangeHandler(pageNumber)}>
                                                                <a className={`page-link ${this.state.activePageNumber === pageNumber ? "active" : ""}`}>{pageNumber}</a>
                                                            </li>
                                                        )
                                                    })}
                                                    <li className="page-item"
                                                        onClick={() => this.state.activePageNumber < this.state.totalPages && this.pageChangeHandler(this.state.activePageNumber + 1)}>
                                                        <a className={`page-link ${this.state.activePageNumber < this.state.totalPages ? "active" : ""}`}>
                                                            Next<i className="far fa-angle-double-right ml-2"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </React.Fragment> :
                                            <div className="inner-message-wrapper" style={{ height: 200 }}>
                                                <div className="inner-message-content">
                                                    <i className="fad fa-webcam"></i>
                                                    <h6>You haven't generated any device(s) yet.</h6>
                                                </div>
                                            </div>
                                    }
                                </div>
                            </div>

                            <div className="form-content-box">
                                <div className="form-content-header">
                                    <p>Device Controls</p>
                                </div>
                                {this.state.deleteCommandLoader ?
                                    <div className="inner-loader-wrapper" style={{ height: 200 }}>
                                        <div className="inner-loader-content">
                                            <i className="fad fa-sync-alt fa-spin"></i>
                                        </div>
                                    </div> :
                                    <div className="form-content-body">
                                        <ul className="list-style-none d-flex device-control-list">
                                            {this.state.controlConfig.map((command, i) => {
                                                return (
                                                    <li className="flex-20" key={i}>
                                                        <div className="device-control-list-box">
                                                            <div className="device-control-list-image">
                                                                <img src={command.imageUrl || "https://content.iot83.com/m83/misc/device-type-fallback.png"}></img>
                                                            </div>
                                                            <p>{command.controlName}</p>
                                                            <div className="button-group">
                                                                <button type="button" className="btn-transparent btn-transparent-blue" data-tooltip data-tooltip-text="Edit" data-tooltip-place="bottom" onClick={() => this.isOpenRpcCommand(i)}><i className="far fa-pencil"></i></button>
                                                                <button type="button" className="btn-transparent btn-transparent-red" data-tooltip data-tooltip-text="Delete" data-tooltip-place="bottom" onClick={() => this.toggleDeleteCommandModal(i)}><i className="far fa-trash-alt"></i></button>
                                                            </div>
                                                        </div>
                                                    </li>
                                                )
                                            })}
                                        </ul>
                                        <div className="pt-4 text-center">
                                            <button type="button" className="btn-link" disabled={typeof this.props.match.params.id === 'undefined' || this.state.controlConfig.length >= 5} data-toggle="modal" data-target="#myModal" onClick={this.showCommandModal}><i className="far fa-plus mr-r-7"></i>Add Control</button>
                                        </div>
                                    </div>
                                }
                            </div>

                            {this.state.payload.id &&
                                <div className="form-content-box">
                                    <div className="form-content-header">
                                        <p>Device Topology</p>
                                    </div>
                                    {this.state.topologyLoader ?
                                        <div className="inner-loader-wrapper" style={{ height: 300 }}>
                                            <div className="inner-loader-content">
                                                <i className="fad fa-sync-alt fa-spin"></i>
                                            </div>
                                        </div> :
                                        <React.Fragment>
                                            <div className="form-content-body">
                                                {this.state.topologyDetails.map((layer, index) => {
                                                    let { topologyDetails } = this.state
                                                    return (<div className="d-flex device-topology" key={layer.id}>
                                                        <div className="flex-30 pd-r-10">
                                                            <div className="form-group">
                                                                <label className="form-group-label">Component Image :
                                                                    <button type="button" className="btn btn-link float-right p-0 f-12" disabled={!layer.imagePath} onClick={() => {
                                                                        let topologyDetails = cloneDeep(this.state.topologyDetails)
                                                                        topologyDetails[index].imagePath = ''
                                                                        this.setState({
                                                                            topologyDetails
                                                                        })
                                                                    }}>
                                                                        <i className="far fa-redo-alt"></i>Reset
                                                                    </button>
                                                                </label>
                                                                <div className="file-upload-box">
                                                                    <ImageUploader
                                                                        loader={layer.isUploadingImage}
                                                                        imagePath={layer.imagePath}
                                                                        uploadHandler={this.uploadLayerImage}
                                                                        params={[layer.id]}
                                                                        acceptFileType="image/x-png,image/gif,image/jpeg"
                                                                    />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="flex-35 pd-l-5 pd-r-5">
                                                            <div className="form-group">
                                                                <label className="form-group-label">Component Name : <i className="fas fa-asterisk form-group-required"></i></label>
                                                                <input type="text" placeholder="e.g. Sensor or Gateway or Cloud" className="form-control" maxLength="25" pattern="^[a-zA-z0-9]{1,}$" value={layer.name}
                                                                    onChange={({ currentTarget }) => this.topologyDetailsChangeHandler(index, "name", currentTarget.value)} />
                                                                {layer.nameError && <span className="form-group-error">Please fill the required field.</span>}
                                                            </div>
                                                        </div>
                                                        <div className="flex-35 pd-l-10">
                                                            <div className="form-group">
                                                                <label className="form-group-label">Network Interface : {index + 1 != this.state.topologyDetails.length && <i className="fas fa-asterisk form-group-required"></i>}</label>
                                                                <input type="text" placeholder="e.g. WiFi or BLE or LoRa" className="form-control" pattern="^[a-zA-z0-9]{1,}$" value={layer.connectedVia || ""}
                                                                    disabled={(index + 1) == topologyDetails.length} onChange={({ currentTarget }) => this.topologyDetailsChangeHandler(index, "connectedVia", currentTarget.value)} />
                                                                {layer.connectedViaError && <span className="form-group-error">Please fill the required field.</span>}
                                                            </div>
                                                        </div>
                                                        <div className={`btn btn-light box-top-right-button ${topologyDetails.length >= 1 ? "" : "cursor-not-allowed"}`} data-tooltip data-tooltip-text="Remove" data-tooltip-place="bottom" onClick={() => topologyDetails.length >= 1 && this.changeTopologyLayer("REMOVE", index)}><i className="far fa-times"></i></div>
                                                    </div>)
                                                })}
                                                <div className="text-center">
                                                    <button type="button" className="btn btn-link" disabled={this.state.topologyDetails.length >= 5} onClick={() => this.changeTopologyLayer("ADD")}><i className="far fa-plus"></i>Add Component</button>
                                                </div>
                                            </div>
                                            {!isEqual(this.state.payload.deviceTopology, this.state.topologyDetails) && <div className="form-content-footer">
                                                <button type='button' className="btn btn-primary" disabled={this.state.topologyDetails.some(layer => layer.nameError || layer.connectedViaError || layer.isUploadingImage)} onClick={this.saveDeviceTopology}>Save</button>
                                            </div>}
                                        </React.Fragment>
                                    }
                                </div>
                            }
                        </div>

                        <div className="form-info-wrapper pd-b-60">
                            <div className="form-info-body">
                                <div className="form-info-icon">
                                    <img src="https://content.iot83.com/m83/misc/addDeviceType.png" />
                                </div>

                                <h5>Device Type</h5>
                                <ul className="list-style-none form-info-list">
                                    <li><p>Specify a name for your Device Type, that describes your device like a fire sensor, temp sensor etc.</p></li>
                                    <li><p>Based on your selected plan you are allocated a fixed number of devices.</p></li>
                                    <li><p>You can generate your Device Id's (same would be used a MQTT client Id for your connection from Device.</p></li>
                                    <li><p>Credentials will be mailed to you in form of a CSV that will have further details.</p></li>
                                    <li><p>You can also specify a topology for your Device by defining the layers involved like sensor connects to a gateway and the gateway connects to a cloud.</p></li>
                                    <li><p>Specify the Device Control commands for your device by using the Add Control Command section.</p></li>
                                    <li><p className="text-gray font-weight-bold">For details on how to connect various devices to {localStorage.tenantType === "MAKER" ? "iFlex83" : "Flex83"} Cloud, please refer on-boarding document received in the Welcome / Registration email.</p></li>
                                </ul>
                            </div>
                            <div className="form-info-footer">
                                <button type='button' className="btn btn-light" onClick={() => this.props.history.push(`/deviceTypes`)}>Cancel</button>
                            </div>
                        </div>
                    </div>
                }

                {/* command modal */}
                {this.state.activeCommandIndex >= 0 &&
                    <CommandConfigModal
                        COMMAND_OBJ={this.state.COMMAND_OBJ}
                        deviceTypeName={this.state.payload.name}
                        commandImage={this.state.commandImage}
                        saveCommandsHandler={this.saveCommandsHandler}
                        closeCommandModal={this.closeCommandModal}
                        commandModalLoader={this.state.commandModalLoader}
                        onDeleteRPCImage={this.onDeleteRPCImage}
                        showNotification={this.props.showNotification}
                    />
                }
                {/* end command modal */}

                {/* delete device modal */}
                {this.state.confirmState &&
                    <div className="modal show d-block animated slideInDown" id="deleteModal">
                        <div className="modal-dialog modal-dialog-centered">
                            <div className="modal-content">
                                <div className="modal-body">
                                    <div className="delete-content">
                                        <div className="delete-icon">
                                            <i className="fad fa-trash-alt text-red"></i>
                                        </div>
                                        <h4>Delete !</h4>
                                        <h6>Are you sure you want to delete device - <strong className="text-gray">{this.state.deviceToBeDeletedName}</strong> ?</h6>
                                    </div>
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-dark" data-dismiss="modal" onClick={() => { this.toggleConfirmModal() }}>Cancel</button>
                                    <button type="button" className="btn btn-success" data-dismiss="modal" onClick={() => { this.deleteDeviceHandler() }}>Delete</button>
                                </div>
                            </div>
                        </div>
                    </div>
                }
                {/* end delete modal */}

                {/* delete command modal */}
                {this.state.isOpenCommandModal &&
                    <div className="modal show d-block animated slideInDown" id="deleteCommandModal">
                        <div className="modal-dialog modal-dialog-centered">
                            <div className="modal-content">
                                <div className="modal-body">
                                    <div className="delete-content">
                                        <div className="delete-icon">
                                            <i className="fad fa-trash-alt text-red"></i>
                                        </div>
                                        <h4>Delete !</h4>
                                        <h6>Are you sure you want to delete command - <strong className="text-gray">{this.state.controlConfig[this.state.commandIndexToBeDeleted].controlName}</strong> ?</h6>
                                    </div>
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-dark" data-dismiss="modal" onClick={() => this.setState({ isOpenCommandModal: false })}>Cancel</button>
                                    <button type="button" className="btn btn-success" data-dismiss="modal" onClick={() => { this.deleteCommand() }}>Delete</button>
                                </div>
                            </div>
                        </div>
                    </div>
                }
                {/* end delete command modal */}

                {/* DeviceIds Input Moda; */}

                <div className="modal animated slideInDown" id="deviceIdsModal">
                    <div className="modal-dialog modal-xl modal-dialog-centered">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h4 className="modal-title">Add Devices
                                        <button type="button" className="close" data-dismiss="modal">
                                        <i className="far fa-times"></i>
                                    </button>
                                </h4>
                            </div>


                        </div>
                    </div>
                </div>

                {this.state.isOpen &&
                    <NotificationModal
                        type={this.state.modalType}
                        message2={this.state.message2}
                        onCloseHandler={this.onCloseHandler}
                    />
                }
            </React.Fragment >
        );
    }
}

AddOrEditDeviceType.propTypes = {
    dispatch: PropTypes.func.isRequired
};
let allSelectors = {}
Object.entries(SELECTORS).map(([key, value]) => {
    allSelectors[key] = value()
})

const mapStateToProps = createStructuredSelector(allSelectors);

function mapDispatchToProps(dispatch) {
    let allActions = { dispatch }
    Object.entries(ACTIONS).map(([key, value]) => {
        allActions[key] = (...args) => dispatch(value(...args))
    })
    return allActions;
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "addOrEditMqttConnector", reducer });
const withSaga = injectSaga({ key: "addOrEditMqttConnector", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(AddOrEditDeviceType);
