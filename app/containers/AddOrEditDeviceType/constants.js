/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/*
 *
 * AddOrEditDeviceType constants
 *
 */

export const RESET_TO_INITIAL_STATE = "app/AddOrEditDeviceType/RESET_TO_INITIAL_STATE";

export const GET_DEVICE_TYPE_BY_ID = {
	action: 'app/AddOrEditDeviceType/GET_DEVICE_TYPE_BY_ID',
	success: "app/AddOrEditDeviceType/GET_DEVICE_TYPE_BY_ID_SUCCESS",
	failure: "app/AddOrEditDeviceType/GET_DEVICE_TYPE_BY_ID_FAILURE",
	urlKey: "getDeviceTypeById",
	successKey: "getDeviceTypeByIdSuccess",
	failureKey: "getDeviceTypeByIdFailure",
	actionName: "getDeviceTypeById",
	actionArguments: ["payload", "id"]
}

export const SAVE_MQTT_CONNECTOR = {
	action: 'app/AddOrEditDeviceType/SAVE_MQTT_CONNECTOR',
	success: "app/AddOrEditDeviceType/SAVE_MQTT_CONNECTOR_SUCCESS",
	failure: "app/AddOrEditDeviceType/SAVE_MQTT_CONNECTOR_FAILURE",
	urlKey: "createDevice",
	successKey: "saveMqttConnectorSuccess",
	failureKey: "saveMqttConnectorFailure",
	actionName: "saveMqttConnector",
	actionArguments: ["payload"]
}

export const SAVE_COMMANDS = {
	action: 'app/AddOrEditDeviceType/SAVE_COMMANDS',
	success: "app/AddOrEditDeviceType/SAVE_COMMANDS_SUCCESS",
	failure: "app/AddOrEditDeviceType/SAVE_COMMANDS_FAILURE",
	urlKey: "saveCommands",
	successKey: "saveCommandsSuccess",
	failureKey: "saveCommandsFailure",
	actionName: "saveCommands",
	actionArguments: ["payload", "isAddMode", "commandImage"]
}

export const DELETE_RPC_COMMAND = {
	action: 'app/AddOrEditDeviceType/DELETE_RPC_COMMAND',
	success: "app/AddOrEditDeviceType/DELETE_RPC_COMMAND_SUCCESS",
	failure: "app/AddOrEditDeviceType/DELETE_RPC_COMMAND_FAILURE",
	urlKey: "deleteCommands",
	successKey: "deleteCommandsSuccess",
	failureKey: "deleteCommandsFailure",
	actionName: "deleteCommands",
	actionArguments: ["id"]
}

export const GENERATE_CLIENT_IDS = {
	action: 'app/AddOrEditDeviceType/GENERATE_CLIENT_IDS',
	success: "app/AddOrEditDeviceType/GENERATE_CLIENT_IDS_SUCCESS",
	failure: "app/AddOrEditDeviceType/GENERATE_CLIENT_IDS_FAILURE",
	urlKey: "generateClientIds",
	successKey: "generateClientIdsSuccess",
	failureKey: "generateClientIdsFailure",
	actionName: "generateClientIds",
	actionArguments: ["noOfDevices", "connectorId", "deviceIds"]
}

export const EMAIL_CREDENTIALS = {
	action: 'app/AddOrEditDeviceType/EMAIL_CREDENTIALS',
	success: "app/AddOrEditDeviceType/EMAIL_CREDENTIALS_SUCCESS",
	failure: "app/AddOrEditDeviceType/EMAIL_CREDENTIALS_FAILURE",
	urlKey: "emailCredentials",
	successKey: "emailCredentialsSuccess",
	failureKey: "emailCredentialsFailure",
	actionName: "emailCredentials",
	actionArguments: ["connectorId"]
}

export const EDIT_DEVICE = {
	action: 'app/AddOrEditDeviceType/EDIT_DEVICE',
	success: "app/AddOrEditDeviceType/EDIT_DEVICE_SUCCESS",
	failure: "app/AddOrEditDeviceType/EDIT_DEVICE_FAILURE",
	urlKey: "editDevice",
	successKey: "editDeviceSuccess",
	failureKey: "editDeviceFailure",
	actionName: "editDevice",
	actionArguments: ["payload", "deviceId"]
}

export const DELETE_DEVICE = {
	action: 'app/AddOrEditDeviceType/DELETE_DEVICE',
	success: "app/AddOrEditDeviceType/DELETE_DEVICE_SUCCESS",
	failure: "app/AddOrEditDeviceType/DELETE_DEVICE_FAILURE",
	urlKey: "deleteDevice",
	successKey: "deleteDeviceSuccess",
	failureKey: "deleteDeviceFailure",
	actionName: "deleteDevice",
	actionArguments: ["deviceId", "connectorId"]
}

export const AVAILABLE_DEVICE_COUNT = {
	action: 'app/AddOrEditDeviceType/AVAILABLE_DEVICE_COUNT',
	success: "app/AddOrEditDeviceType/AVAILABLE_DEVICE_COUNT_SUCCESS",
	failure: "app/AddOrEditDeviceType/AVAILABLE_DEVICE_COUNT_FAILURE",
	urlKey: "getDeveloperQuota",
	successKey: "availableDeviceCountSuccess",
	failureKey: "availableDeviceCountFailure",
	actionName: "getDeveloperQuota",
	actionArguments: ["payload"]
}

export const UPDATE_DEVICE_TYPE = {
	action: 'app/AddOrEditDeviceType/UPDATE_DEVICE_TYPE',
	success: "app/AddOrEditDeviceType/UPDATE_DEVICE_TYPE_SUCCESS",
	failure: "app/AddOrEditDeviceType/UPDATE_DEVICE_TYPE_FAILURE",
	urlKey: "updateDevice",
	successKey: "updateDeviceSuccess",
	failureKey: "updateDeviceFailure",
	actionName: "updateDevice",
	actionArguments: ["payload", "isDeviceImageChanged"]
}

export const IMAGE_UPLOAD_HANDLER = {
	action: 'app/AddOrEditDeviceType/IMAGE_UPLOAD_HANDLER',
	success: "app/AddOrEditDeviceType/IMAGE_UPLOAD_HANDLER_SUCCESS",
	failure: "app/AddOrEditDeviceType/IMAGE_UPLOAD_HANDLER_FAILURE",
	urlKey: "uploadFile",
	successKey: "imageUploadSuccess",
	failureKey: "imageUploadFailure",
	actionName: "imageUploadHandler",
	actionArguments: ["payload", "addOns"]
}

export const SAVE_TOPOLOGY_DETAILS = {
	action: 'app/AddOrEditDeviceType/SAVE_TOPOLOGY_DETAILS',
	success: "app/AddOrEditDeviceType/SAVE_TOPOLOGY_DETAILS_SUCCESS",
	failure: "app/AddOrEditDeviceType/SAVE_TOPOLOGY_DETAILS_FAILURE",
	urlKey: "saveDeviceTopology",
	successKey: "saveDeviceTopologySuccess",
	failureKey: "saveDeviceTopologyFailure",
	actionName: "saveDeviceTopology",
	actionArguments: ["payload", "connectorId", "isEdit"]
}

export const GET_ALL_DEVICES_BY_CONNECTOR_ID = {
	action: 'app/AddOrEditDeviceType/GET_ALL_DEVICES_BY_CONNECTOR_ID',
	success: "app/AddOrEditDeviceType/GET_ALL_DEVICES_BY_CONNECTOR_ID_SUCCESS",
	failure: "app/AddOrEditDeviceType/GET_ALL_DEVICES_BY_CONNECTOR_ID_FAILURE",
	urlKey: "getAllDevices",
	successKey: "getAllDevicesByConnectorIdSuccess",
	failureKey: "getAllDevicesByConnectorIdFailure",
	actionName: "getAllDevicesByConnectorId",
	actionArguments: ["payload"]
}


