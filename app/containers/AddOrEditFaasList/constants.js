/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

export const DEFAULT_ACTION = "app/AddOrEditFaasList/DEFAULT_ACTION";

export const ON_SUBMIT = "app/AddOrEditFaasList/ON_SUBMIT";
export const ON_SUBMIT_SUCCESS = "app/AddOrEditFaasList/ON_SUBMIT_SUCCESS";
export const ON_SUBMIT_FAILURE = "app/AddOrEditFaasList/ON_SUBMIT_FAILURE";

export const GET_FAAS_BY_ID = "app/AddOrEditFaasList/GET_FAAS_BY_ID";
export const GET_FAAS_BY_ID_SUCCESS = "app/AddOrEditFaasList/GET_FAAS_BY_ID_SUCCESS";
export const GET_FAAS_BY_ID_FAILURE = "app/AddOrEditFaasList/GET_FAAS_BY_ID_FAILURE";

export const FAAS_FILE_UPLOAD_SUCCESS = "app/AddOrEditFaasList/FILE_UPLOAD_SUCCESS";
export const FAAS_FILE_UPLOAD_REQUEST = "app/AddOrEditFaasList/FILE_UPLOAD_REQUEST";
export const FAAS_FILE_UPLOAD_FAILURE = "app/AddOrEditFaasList/FILE_UPLOAD_FAILURE";

export const GET_CONNECTION_STRING_SUCCESS = "app/AddOrEditFaasList/GET_CONNECTION_STRING_SUCCESS";
export const GET_CONNECTION_STRING_REQUEST = "app/AddOrEditFaasList/GET_CONNECTION_STRING_REQUEST";
export const GET_CONNECTION_STRING_FAILURE = "app/AddOrEditFaasList/GET_CONNECTION_STRING_FAILURE";

export const GET_FAAS_IMAGES = "app/AddOrEditFaasList/GET_FAAS_IMAGES"
export const GET_FAAS_IMAGES_SUCCESS = "app/AddOrEditFaasList/GET_FAAS_IMAGES_SUCCESS"
export const GET_FAAS_IMAGES_FAILURE = "app/AddOrEditFaasList/GET_FAAS_IMAGES_FAILURE"

export const GET_TEMPLATE_CODE_FAAS = "app/AddOrEditFaasList/GET_TEMPLATE_CODE_FAAS"
export const GET_TEMPLATE_CODE_FAAS_SUCCESS = "app/AddOrEditFaasList/GET_TEMPLATE_CODE_FAAS_SUCCESS"
export const GET_TEMPLATE_CODE_FAAS_FAILURE = "app/AddOrEditFaasList/GET_TEMPLATE_CODE_FAAS_FAILURE"

export const RESET_TO_INITIAL_STATE = "app/AddOrEditFaasList/RESET_TO_INITIAL_STATE";





