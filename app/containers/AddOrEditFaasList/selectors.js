/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
 * Direct selector to the addOrEditFaasList state domain
 */

const selectAddOrEditFaasListDomain = state =>
  state.get("addOrEditFaasList", initialState);

export const faasSubmitSuccess = () =>
  createSelector(selectAddOrEditFaasListDomain, subState => subState.FaasSubmitSuccess);

export const submitFaasFailure = () =>
  createSelector(selectAddOrEditFaasListDomain, subState => subState.FaasSubmitFailure);


export const getFassByIdSuccess = () =>
  createSelector(selectAddOrEditFaasListDomain, subState => subState.getFassByIdSuccess);

export const getFassByIdFailure = () =>
  createSelector(selectAddOrEditFaasListDomain, subState => subState.getFassByIdFailure);

export const getConnectionStringSuccess = () =>
  createSelector(selectAddOrEditFaasListDomain, subState => subState.getConnectionStringSuccess);

export const getConnectionStringFailure = () =>
  createSelector(selectAddOrEditFaasListDomain, subState => subState.getConnectionStringFailure);

export const fileUploadSuccess = () =>
  createSelector(selectAddOrEditFaasListDomain, subState => subState.fileUploadSuccess);

export const fileUploadFailure = () =>
  createSelector(selectAddOrEditFaasListDomain, subState => subState.fileUploadFailure);

const isFetchingState = state => state.get('loader');
export const getIsFetching = () => createSelector(isFetchingState, fetchingState => fetchingState.get('isFetching'));

export const getTemplateCodeFaasFailure =()=> createSelector(selectAddOrEditFaasListDomain, substate => substate.getTemplateCodeFaasFailure);
export const getTemplateCodeFaasSuccess =()=> createSelector(selectAddOrEditFaasListDomain, substate => substate.getTemplateCodeFaasSuccess);

export const getFaasImagesSuccess =()=> createSelector(selectAddOrEditFaasListDomain, substate => substate.getFaasImagesSuccess);
export const getFaasImagesFailure =()=> createSelector(selectAddOrEditFaasListDomain, substate => substate.getFaasImagesFailure);

const selectDataExpiryDomain = state => state.get("dataCollections", initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by DataExpiry
 */