/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { createStructuredSelector } from "reselect";
import Select from 'react-select';

import { compose } from "redux";
import AceEditor from "react-ace";
import 'brace/mode/python';
import 'brace/mode/json';
import 'brace/theme/monokai';
import 'brace/theme/github';
import 'brace/ext/searchbox'
import 'brace/ext/language_tools'
import * as Actions from "./actions";
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import * as Selectors from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import NotificationModal from '../../components/NotificationModal/Loadable'
import Loader from '../../components/Loader/Loadable'
import JSONInput from "react-json-editor-ajrm/dist";
import { Prompt } from 'react-router';
import SlidingPane from 'react-sliding-pane';
import Modal from 'react-modal';
import cloneDeep from 'lodash/cloneDeep';
import { isNavAssigned } from "../../commonUtils";
let reactSelectOptions =
    [{ value: "Boolean", label: "boolean" },
    { value: "int", label: "int" },
    { value: "float", label: "float" },
    { value: "char", label: "char" },
    { value: "String", label: "string" },
    { value: "map", label: "map" },],
    faasMemoryOptions = [
        { value: 128, label: "128" },
        { value: 256, label: "256" }
    ]
/* eslint-disable react/prefer-stateless-function */
export class AddOrEditFaasList extends React.Component {
    state = {
        file: null,
        code: "",
        filePreview: null,
        connectionString: {
            defaultDBConnectionString: "",
            mlDBConnectionString: ""
        },
        payload: {
            actionName: "",
            description: "",
            developmentMode: false,
            imageName: '',
            imageDescription: '',
            kind: "python:3",
            pathParameters: [],
            dockerImage: "existing",
            memory: "128",
            database: 'mongo',
            raw: false //send true in case of file upload
        },
        interpreters: [
            {
                value: "python:3",
                label: "Python 3",
                isDisable: false,
                fileType: "python",
            },
            {
                value: "nodejs:12",
                label: "NodeJS 12",
                isDisable: false,
                fileType: "javascript",
            },
            {
                value: "swift:4.2",
                label: "Swift 4.2",
                isDisable: false,
                fileType: "swift",
            },
            {
                value: "go:1.11",
                label: "Golang 1.11",
                isDisable: false,
                fileType: "golang",
            },
            {
                value: "ruby:2.5",
                label: "Ruby 2.5",
                isDisable: false,
                fileType: "ruby",
            },
            {
                value: "php:7.4",
                label: "PHP 7.4",
                isDisable: false,
                fileType: "php",
            }

        ],
        selectedFileType: "python",
        isOpen: false,
        isParamForm: false,
        selectedPathParam: {
            description: "",
            in: "query",
            name: "",
            required: true,
            type: "String",
            defaultValue: ""
        },
        faasImages: [],
        isFetching: true,
        isFetchingImages: true,
        editorModal: false,
        showWarningMessage: false,
        pullStatus: false,
        debugLogs: [],
        databases: [{
            name: 'mongo',
            displayName: 'MongoDb',
            imageName: 'mongodb',
            isEnabled: true,
        }, {
            name: 'timescale',
            displayName: 'TimescaleDb',
            imageName: 'timescaledb',
            isEnabled: true,
        }, {
            name: 'mysql',
            displayName: 'My SQL',
            imageName: 'mysql',
            isEnabled: false,
        }, {
            name: 'cassandra',
            displayName: 'Cassandra',
            imageName: 'cassandra',
            isEnabled: false,
        }, {
            name: 'elastic',
            displayName: 'Elastic',
            imageName: 'elastic',
            isEnabled: false,
        }]
    };

    componentDidUpdate = () => {
        if (this.state.showWarningMessage) {
            window.onbeforeunload = () => true
        } else {
            window.onbeforeunload = undefined
        }
    }

    componentWillMount() {
        this.props.getConnectionString();
        if (this.props.match.params.id) {
            this.props.getFaasById(this.props.match.params.id)
        } else {
            this.props.getFaasImages(this.state.payload.kind);
            this.props.getTemplateCodeForFaaS(this.state.payload.kind, this.state.payload.database)
        }
        Modal.setAppElement('body');
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.faasSubmitSuccess && nextProps.faasSubmitSuccess !== this.props.faasSubmitSuccess) {
            if (this.state.payload.developmentMode) {
                this.setState(prevState => ({
                    isFetching: false,
                    outputModal: true,
                    debugLogs: nextProps.faasSubmitSuccess.data.logs,
                    savedFaasId: nextProps.faasSubmitSuccess.data.id || prevState.savedFaasId
                }), () => this.props.resetToInitialState())
            }
            else {
                this.setState({
                    isOpen: true,
                    modalType: "success",
                    message2: nextProps.faasSubmitSuccess.message,
                    code: "",
                }, () => this.props.resetToInitialState());
            }
        }

        if (nextProps.submitFaasFailure && nextProps.submitFaasFailure != this.props.submitFaasFailure) {
            this.setState({
                isOpen: true,
                modalType: "error",
                message2: nextProps.submitFaasFailure.error,
                isFetching: false,
            })
            // errorHandling(["modalType", "isOpen", "message2", "isFetching"], nextProps.submitFaasFailure.error, this)
        }

        if (nextProps.getFassByIdSuccess && nextProps.getFassByIdSuccess !== this.props.getFassByIdSuccess) {
            let code = "", payload = nextProps.getFassByIdSuccess
            if (nextProps.getFassByIdSuccess.code) {
                code = nextProps.getFassByIdSuccess.code;
            }
            this.props.getFaasImages(payload.kind);
            let gitDiff = nextProps.getFassByIdSuccess.codeMismatch;
            let gitCode = nextProps.getFassByIdSuccess.gitCode;
            payload.dockerImage = "existing";
            this.setState({
                payload,
                code,
                isFetching: false,
                saveAsDraftOuter: payload.saveAsDraft,
                gitCode,
                gitDiff
            })
        }

        if (nextProps.getFassByIdFailure && nextProps.getFassByIdFailure != this.props.getFassByIdFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.getFassByIdFailure
            });
        }

        if (nextProps.connectionString) {
            this.setState({
                connectionString: nextProps.connectionString
            })
        }

        if (nextProps.connectionStringFailure && nextProps.connectionStringFailure !== this.props.connectionStringFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.connectionStringFailure,
            }, () => this.props.resetToInitialState())
        }

        if (nextProps.getDisplayCodeContentSuccess && nextProps.getDisplayCodeContentSuccess !== this.props.getDisplayCodeContentSuccess) {
            let displayCodeContent = nextProps.getDisplayCodeContentSuccess.data
            this.setState({
                displayCodeContent
            })
        }

        if (nextProps.getDisplayCodeContentFailure && nextProps.getDisplayCodeContentFailure !== this.props.getDisplayCodeContentFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.getDisplayCodeContentFailure,
            }, () => this.props.resetToInitialState())
        }

        if (nextProps.getFaasImagesSuccess && nextProps.getFaasImagesSuccess !== this.props.getFaasImagesSuccess) {
            let faasImages = nextProps.getFaasImagesSuccess.map((temp) => { return { value: temp.image, label: temp.image } })
            this.setState({
                faasImages,
                isFetchingImages: false,
            })
        }

        if (nextProps.getFaasImagesFailure && nextProps.getFaasImagesFailure !== this.props.getFaasImagesFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.getFaasImagesFailure,
                isFetchingImages: false,
            })
        }

        if (nextProps.templateCodeForFaaS !== undefined && nextProps.templateCodeForFaaS !== this.props.templateCodeForFaaS) {
            this.setState({
                code: nextProps.templateCodeForFaaS ? nextProps.templateCodeForFaaS.code : '',
                isFetching: false
            })
        }

        if (nextProps.templateCodeForFaaSFailure && nextProps.templateCodeForFaaSFailure !== this.props.templateCodeForFaaSFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.getFaasImagesFailure,
            })
        }
    }



    pathParamHandler = (index) => {
        let selectedPathParam;
        if (index >= 0) {
            selectedPathParam = this.state.payload.pathParameters[index]
            selectedPathParam.required = selectedPathParam.required.toString()

        } else {
            selectedPathParam = {
                description: "",
                in: "query",
                name: "",
                required: true,
                type: "String",
                defaultValue: ""
            }
        }
        this.setState((preState, props) => ({
            selectedParamIndex: index >= 0 ? index : -1,
            isParamForm: !preState.isParamForm,
            selectedPathParam
        }))
    }

    inputChangeHandler = event => {
        let selectedFileType = this.state.selectedFileType,
            payload = JSON.parse(JSON.stringify(this.state.payload)),
            code = this.state.code,
            faasImages = this.state.faasImages;

        if (event.target.id === "saveAsDraft") {
            payload.saveAsDraft = event.target.checked;
        }
        else if (event.target.id == "actionName") {
            let regex = /^[$A-Z_][0-9A-Z_$]*$/i;
            if (regex.test(event.target.value) || event.target.value == "") {

                payload[event.target.id] = event.target.value;
            }
        } else {
            payload[event.target.id] = event.target.value;
        }
        if (event.target.id === "dockerImage") {
            payload.requirements = [];
            payload.imageName = "";
        }


        this.setState({
            payload,
            selectedFileType,
            code,
            faasImages,
        });
    }

    reactSelectInputHandler = (valueObject, id) => {
        let selectedFileType = this.state.selectedFileType,
            payload = JSON.parse(JSON.stringify(this.state.payload)),
            code = this.state.code,
            faasImages = this.state.faasImages;
        if (id == "kind") {
            faasImages = [];
            payload.kind = valueObject.value;
            payload.imageName = ""
            this.props.getFaasImages(valueObject.value);
            this.props.getTemplateCodeForFaaS(valueObject.value, payload.database);
            code = '';
            selectedFileType = this.state.interpreters.find(interpreter => {
                if (valueObject.value === interpreter.value) {
                    return interpreter.fileType;
                }
            }).fileType
        }
        else if (id == "memory") {
            payload.memory = valueObject.value
        }
        else {
            payload.imageName = valueObject.value
        }
        this.setState({
            payload,
            selectedFileType,
            code,
            faasImages,
        });
    }

    formSubmitHandler = event => {
        event.preventDefault();
        let payload = { ...this.state.payload }
        payload.code = this.state.code
        if (!payload.imageName) {
            return this.setState({
                isOpen: true,
                modalType: "error",
                message2: "Please select Docker Image",
            })
        }
        if (this.state.payload.developmentMode) {
            this.setState({
                isFetching: true,
            }, () => this.props.formSubmitHandler(payload, this.props.match.params.id || this.state.savedFaasId))
            return;
        }
        delete payload.dockerImage;
        this.setState({
            isFetching: true,
            showWarningMessage: false,
        }, () => {
            this.props.formSubmitHandler(payload, this.props.match.params.id || this.state.savedFaasId);
        })
    };

    onCloseHandler = () => {
        let allowRedirection = this.state.modalType === "success" && !this.state.noRedirect;
        this.setState((previousState, props) => ({
            isOpen: false,
            isFetching: previousState.modalType && !this.state.noRedirect === "success",
            modalType: '',
            message2: '',
            noRedirect: false
        }), () => allowRedirection && this.navigateToCodeEngine())
    }
    reactSelectPathParams = (valueObject) => {

        let selectedPathParam = { ...this.state.selectedPathParam };
        selectedPathParam.type = valueObject.value
        if (valueObject.value === "Boolean") {
            selectedPathParam.defaultValue = "true";
        } if (valueObject.value === "map") {
            selectedPathParam.defaultValue = "{}";
        }
        else {
            selectedPathParam.defaultValue = "";
        }
        this.setState({
            selectedPathParam
        })
    }
    onChangeHandlerPathParams = (event) => {
        let selectedPathParam = { ...this.state.selectedPathParam };
        let errorMessageCode;
        if (event.target.name === "required") {
            selectedPathParam[event.target.name] = event.target.value === "true";
        } else {
            if (event.target.id === "name") {
                let regex = /^[$A-Z_][0-9A-Z_$]*$/i;
                if (regex.test(event.target.value) || event.target.value == "")
                    selectedPathParam[event.target.id] = event.target.value.trim();
            }
            else if (event.target.name === "defaultValue") {
                selectedPathParam.defaultValue = event.target.value
            }
            else {
                selectedPathParam[event.target.id] = event.target.id == "description" ? event.target.value : event.target.value.trim();
            }

            if (event.target.id === "defaultValue") {
                if (!isNaN(Number(event.target.value))) {
                    selectedPathParam.type = (event.target.value).includes(".") ? "float" : "int"
                }
                else if (event.target.value.trim().toLowerCase() === "true" || event.target.value.trim().toLowerCase() === "false") {
                    selectedPathParam.type = "Boolean"
                    selectedPathParam.defaultValue = event.target.value === "true"
                }
                else if (event.target.value.trim().length === 1) {
                    selectedPathParam.type = "char"
                }
                else {
                    selectedPathParam.type = "String"
                }
            }
            if (this.state.errorMessageCode) {
                if (this.validateType(selectedPathParam.defaultValue, selectedPathParam.type) && selectedPathParam.name) {
                    errorMessageCode = 0
                }
                else if (event.target.id === "type" && this.validateType(selectedPathParam.defaultValue, selectedPathParam.type)) {
                    errorMessageCode = 1
                }
                else if (event.target.id === "name" && this.state.errorMessageCode === 1 && event.target.value.trim()) {
                    errorMessageCode = 2
                }
            }
        }
        this.setState({
            errorMessageCode,
            selectedPathParam
        })
    }
    validateType = (value, type) => {
        if (type !== "map" && typeof value == "string" && value.trim() === "") {
            return true
        }
        else {
            if (type === "int" && !isNaN(Number(value)) && Number(value) % 1 === 0) return true;
            else if (type === "float" && !isNaN(Number(value)) && value.includes(".")) return true;
            else if (type === "Boolean" && typeof [true, false, "true", "false"].includes(value)) return true;
            else if (type === "char" && isNaN(Number(value)) && value.trim().length === 1) return true;
            else if (type === "String" && isNaN(Number(value)) && value.trim().length > 1) return true;
            else if (type === "map" && typeof value === "string") return true;
            else return false
        }
    }

    savePathParams = (event) => {
        event.preventDefault();
        let selectedPathParam = this.state.selectedPathParam
        let paramDefaultValue = selectedPathParam.defaultValue
        let validation = this.validateType(paramDefaultValue, selectedPathParam.type)
        if (selectedPathParam.name && selectedPathParam.in && selectedPathParam.type && validation) {
            let numberDataTypes = ["int", "float"]
            if (numberDataTypes.includes(selectedPathParam.type)) {
                selectedPathParam = { ...selectedPathParam, defaultValue: Number(paramDefaultValue) }
            }
            if (selectedPathParam.type === "Boolean") {
                selectedPathParam = { ...selectedPathParam, defaultValue: ['true', true].includes(paramDefaultValue) }
            }
            let payload = this.state.payload;
            if (this.state.selectedParamIndex >= 0) {
                payload.pathParameters[this.state.selectedParamIndex] = selectedPathParam
            } else {
                // selectedPathParam = cloneDeep(selectedPathParam)
                payload.pathParameters.push(selectedPathParam);
            }
            selectedPathParam = {
                description: "",
                in: "query",
                name: "",
                required: true,
                type: "String",
                defaultValue: ""
            }

            this.setState({ payload, selectedPathParam, isParamForm: false, selectedParamIndex: -1, errorMessageCode: 0 })
        }
        else {
            this.setState({ errorMessageCode: validation ? 1 : 2 })
        }
    }

    deleteParam = (index) => {
        let payload = { ...this.state.payload }
        payload.pathParameters.splice(index, 1)
        this.setState({ payload })
    }

    inputTagsChangeHandler = (tags) => {
        let payload = JSON.parse(JSON.stringify(this.state.payload));
        payload.requirements = tags
        this.setState({
            payload
        })
    }


    navigateToCodeEngine = () => {
        let state = JSON.parse(JSON.stringify(this.props.location.state || null));
        this.props.history.push({ state, pathname: `/faas` })
        /*if(event && event.target.id == "faas")
            this.props.history.push({ state, pathname: `/codeEngine`})
        else
        this.props.history.push({ state, pathname: `/codeEngine/${this.state.payload.category}`}) */
    }

    toggleDevMode = () => {
        let payload = cloneDeep(this.state.payload);
        payload.developmentMode = !payload.developmentMode
        this.setState({ payload })
    }

    handleDataBaseChange = (databaseName) => {
        let payload = JSON.parse(JSON.stringify(this.state.payload)),
            code = this.state.code;
        if (payload.database !== databaseName) {
            payload.database = databaseName;
            code = '';
            this.setState({
                payload,
                code
            }, () => { this.props.getTemplateCodeForFaaS(payload.kind, databaseName) });
        }
    }
    jsonValidation = (str) => {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }
    handleMapParamsInput = (e, isJsonInput = false) => {

        let selectedPathParam = cloneDeep(this.state.selectedPathParam);
        if (isJsonInput) {
            selectedPathParam.defaultValue = this.jsonValidation(JSON.stringify(e.jsObject)) ? JSON.stringify(e.jsObject) : e.jsObject
        }
        this.setState({ selectedPathParam })
    }

    render() {
        let imageNameValue = this.state.faasImages.find(temp => temp.value == this.state.payload.imageName)
        return (
            <React.Fragment>
                <Helmet>
                    <title>Add/Edit FaaS Function</title>
                    <meta name="description" content="M83-Add/EditFaaSFunction" />
                </Helmet>

                <Prompt when={this.state.showWarningMessage} message='Leave site? Changes you made may not be saved.' />
                {this.state.isFetching || this.state.isFetchingImages ? <Loader /> :
                    <React.Fragment>
                        <header className="content-header d-flex">
                            <div className="flex-60">
                                <div className="d-flex">
                                    <h6 className="previous" onClick={this.navigateToCodeEngine}>FaaS</h6>
                                    <h6 className="active">{this.props.match.params.id ? this.state.payload.actionName : 'Add New'}</h6>
                                </div>
                            </div>
                        </header>

                        <div className="content-body">
                            <form onSubmit={this.formSubmitHandler}>
                                <div className="form-content-wrapper form-content-wrapper-left">
                                    <div className="d-flex">
                                        <div className="flex-70 pd-r-6">
                                            <div className="form-content-box mb-0">
                                                <div className="form-content-header">
                                                    <p>Definition</p>
                                                    {!this.state.payload.id && <div className="form-group dropdown faas-db-list">
                                                        <label className="form-group-label d-inline-block">Get Sample Code For :</label>
                                                        <div className="d-inline-block ml-2">
                                                            <ul>
                                                                {this.state.databases.map((database, index) =>
                                                                    database.isEnabled ? <li key={index} className={this.state.payload.database === database.name ? "active" : ""} onClick={() => { this.handleDataBaseChange(database.name) }}><img src={`https://content.iot83.com/m83/dataConnectors/${database.imageName}.png`} data-tooltip data-tooltip-text={database.displayName} data-tooltip-place="bottom" /></li> :
                                                                        <li key={index} className="disabled"><img src={`https://content.iot83.com/m83/dataConnectors/${database.imageName}.png`} data-tooltip data-tooltip-text={database.displayName} data-tooltip-place="bottom" /></li>
                                                                )}
                                                                {/*<div className="dropdown-menu">
                                                                        <button type="button" className="btn-transparent btn-transparent-green" data-tooltip data-tooltip-text="Copy" data-tooltip-place="right"><i className="far fa-copy"></i></button>
                                                                        <div className="ace-editor h-100">
                                                                            <AceEditor
                                                                                placeholder="Type code here..."
                                                                                theme="monokai"
                                                                                name="ace_editor"
                                                                                height="100%"
                                                                                width="100%"
                                                                            />
                                                                        </div>
                                                                    </div>*/}
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    }
                                                </div>
                                                <div className="form-content-body p-2">
                                                    <div className="form-editor">
                                                        <div className="ace-editor h-100">
                                                            <AceEditor
                                                                placeholder="Type code here..."
                                                                mode={this.state.selectedFileType}
                                                                theme="monokai"
                                                                name="ace_editor"
                                                                onChange={(code) => this.setState({ code, showWarningMessage: true })}
                                                                fontSize={14}
                                                                value={this.state.code}
                                                                showPrintMargin={false}
                                                                showGutter={true}
                                                                highlightActiveLine={true}
                                                                setOptions={{
                                                                    enableBasicAutocompletion: true,
                                                                    enableLiveAutocompletion: true,
                                                                    enableSnippets: false,
                                                                    showLineNumbers: true,
                                                                    tabSize: 2,
                                                                }}
                                                                height="100%"
                                                                width="100%"
                                                            />
                                                        </div>
                                                        <button type="button" className="btn btn-light" onClick={() => { this.setState({ editorModal: true }) }}><i className="fas fa-expand"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="flex-30 pd-l-6">
                                            <div className="form-content-box mb-0">
                                                <div className="form-content-header">
                                                    <p>Parameters</p>
                                                    <button type="button" className="btn btn-link" onClick={() => this.pathParamHandler()}>
                                                        <i className="far fa-plus"></i>Add Param
                                                    </button>
                                                </div>
                                                <div className="form-content-body p-3">
                                                    <div className="param-list-wrapper">
                                                        {this.state.payload.pathParameters != 0 ?
                                                            <ul className="list-style-none">
                                                                {this.state.payload.pathParameters.map((param, index) => {
                                                                    return (
                                                                        <li key={index}>
                                                                            <div key={index} className="param-list-box">
                                                                                {(param.required === true || param.required === "true") ?
                                                                                    <span className="param-list-icon" data-tooltip data-tooltip-text="Required" data-tooltip-place="bottom">
                                                                                        <i className="fas fa-asterisk text-red f-9" />
                                                                                    </span> : ""
                                                                                }
                                                                                <h6>{param.name}</h6>
                                                                                <p><strong>Description :</strong>{param.description}</p>
                                                                                <p><strong>Value :</strong>{param.defaultValue.toString()}<span className="text-green float-right">({param.type})</span></p>
                                                                                <div className="button-group">
                                                                                    <button type="button" className="btn-transparent btn-transparent-blue" data-tooltip data-tooltip-text="Edit" data-tooltip-place="bottom" onClick={() => this.pathParamHandler(index)}>
                                                                                        <i className="far fa-pencil"></i>
                                                                                    </button>
                                                                                    <button type="button" className="btn-transparent btn-transparent-red" data-tooltip data-tooltip-text="Delete" data-tooltip-place="bottom" onClick={() => this.deleteParam(index)}>
                                                                                        <i className="far fa-trash-alt"></i>
                                                                                    </button>
                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                    )
                                                                })}
                                                            </ul> :
                                                            <div className="param-empty-list">
                                                                <p>No parameters added yet.</p>
                                                            </div>
                                                        }
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div className="form-info-wrapper form-info-wrapper-left">
                                    <div className="form-info-header">
                                        <h5>Configure</h5>
                                    </div>

                                    <div className="form-info-body form-info-body-adjust">
                                        {/* <div className="form-group">
                                            <label className="form-group-label">Development mode :
                                                <span className="toggle-switch-wrapper float-right">
                                                    <label className="toggle-switch">
                                                        <input type="checkbox" name="isCommand" checked={this.state.payload.developmentMode} onChange={this.toggleDevMode} />
                                                        <span className="toggle-switch-slider"></span>
                                                    </label>
                                                </span>
                                            </label>
                                        </div> */}

                                        <div className="form-group">
                                            <label className="form-group-label">Name : <i className="fas fa-asterisk form-group-required"></i></label>
                                            <input type="text" id="actionName" className="form-control" disabled={this.props.match.params.id || this.state.savedFaasId} value={this.state.payload.actionName} onChange={this.inputChangeHandler} required />
                                        </div>

                                        <div className="form-group">
                                            <label className="form-group-label">Description : </label>
                                            <textarea rows="3" id="description" className="form-control" value={this.state.payload.description} onChange={this.inputChangeHandler} />
                                        </div>

                                        <div className="form-group">
                                            <label className="form-group-label">Interpreter : <i className="fas fa-asterisk form-group-required"></i></label>
                                            <Select
                                                id="kind"
                                                className="form-control-multi-select"
                                                value={this.state.interpreters.find(temp => temp.value == this.state.payload.kind)}
                                                onChange={(valueObject) => this.reactSelectInputHandler(valueObject, "kind")}
                                                options={this.state.interpreters}
                                                isOptionDisabled={(option) => option.isdisabled}
                                                isDisabled={this.state.payload.id}
                                            />
                                        </div>

                                        <div className="form-group">
                                            <label className="form-group-label">Docker Image: <i className="fas fa-asterisk form-group-required"></i>
                                                {isNavAssigned('dockerImages') && <button type="button" className="btn btn-link" onClick={() => { this.props.history.push('/dockerImages') }}>
                                                    <i className="far fa-plus mr-r-5"></i>Add New
                                                </button>}
                                            </label>
                                            <Select
                                                id="imageName"
                                                className="form-control-multi-select"
                                                value={imageNameValue ? imageNameValue : null}
                                                onChange={(valueObject) => this.reactSelectInputHandler(valueObject, "imageName")}
                                                options={this.state.faasImages}
                                                placeholder={"Select Docker Image"}
                                            />

                                        </div>

                                        <div className="form-group">
                                            <label className="form-group-label">Memory (in MB) : <i className="fas fa-asterisk form-group-required"></i></label>
                                            <Select
                                                id="memory"
                                                className="form-control-multi-select"
                                                value={faasMemoryOptions.find(temp => temp.value == this.state.payload.memory)}
                                                onChange={(valueObject) => this.reactSelectInputHandler(valueObject, "memory")}
                                                options={faasMemoryOptions}
                                            />
                                        </div>

                                        {/* <div className="form-group">
                                            <label className="check-box">
                                                <span className="check-text">Do you want to save as draft ?</span>
                                                <input type="checkbox" id="saveAsDraft" checked={this.state.payload.saveAsDraft} onChange={this.inputChangeHandler} />
                                                <span className="check-mark" />
                                            </label>
                                        </div> */}
                                    </div>

                                    <div className="form-info-footer">
                                        <button type="button" className="btn btn-light" onClick={this.navigateToCodeEngine}>Cancel</button>

                                        {
                                            <button type="submit" className="btn btn-primary">
                                                {this.props.match.params.id ? "Update" : "Save"}
                                            </button>
                                        }
                                    </div>
                                </div>
                            </form>
                        </div>
                    </React.Fragment>
                }

                {/* add param modal */}
                <SlidingPane
                    className=''
                    overlayClassName='sliding-form'
                    closeIcon={<div></div>}
                    isOpen={this.state.isParamForm || false}
                    from='right'
                    width='500px'
                >
                    <div className="modal-content">
                        <div className="modal-header">
                            <h6 className="modal-title">{this.state.selectedParamIndex >= 0 ? "Edit Parameter" : "Add New Parameter"}
                                <button className="close" onClick={() => this.setState({ isParamForm: false, selectedParamIndex: -1, errorMessageCode: 0 })}><i className="fas fa-angle-right"></i></button>
                            </h6>
                        </div>

                        <form onSubmit={this.savePathParams}>
                            <div className="modal-body">
                                <div className="form-group">
                                    <label className="form-group-label">Name : <i className="fas fa-asterisk form-group-required"></i></label>
                                    <input type="text" id="name" className="form-control" pattern="^[a-zA-Z]([\w\$-]*[a-zA-Z0-9])?$"
                                        value={this.state.selectedPathParam.name} onChange={this.onChangeHandlerPathParams} required />
                                </div>

                                <div className="form-group">
                                    <label className="form-group-label">Description :</label>
                                    <textarea rows="3" type="text" id="description" className="form-control" value={this.state.selectedPathParam.description} onChange={this.onChangeHandlerPathParams} />
                                </div>

                                <div className="form-group">
                                    <label className="form-group-label">Data Type : <i className="fas fa-asterisk form-group-required"></i></label>
                                    <Select
                                        id="type"
                                        className="form-control-multi-select"
                                        isDisabled={this.state.selectedParamIndex >= 0}
                                        value={reactSelectOptions.find(temp => temp.value === this.state.selectedPathParam.type)}
                                        onChange={this.reactSelectPathParams}
                                        options={reactSelectOptions}
                                        isOptionDisabled={(option) => (option.value == "map" && this.state.payload.pathParameters.find(param => param.type === "map"))}
                                    />
                                    {this.state.selectedParamIndex == -1 && this.state.payload.pathParameters.find(param => param.type === "map") && <span className="form-group-error">map can be selected only once</span>}
                                </div>

                                <div className="form-group">
                                    <label className="form-group-label">Default Value <small>(Should be of <b className="text-orange">{this.state.selectedPathParam.type}</b> type)</small> : <i className="fas fa-asterisk form-group-required"></i></label>
                                    {this.state.selectedPathParam.type === "Boolean" ?
                                        <div className="d-flex">
                                            <div className="flex-25 pd-r-10">
                                                <label className="radio-button">
                                                    <span className="radio-button-text">True</span>
                                                    <input type="radio" name="defaultValue" checked={["true", true].includes(this.state.selectedPathParam.defaultValue)} value="true" onChange={this.onChangeHandlerPathParams} />
                                                    <span className="radio-button-mark"></span>
                                                </label>
                                            </div>
                                            <div className="flex-25 pd-r-10">
                                                <label className={"radio-button "}>
                                                    <span className="radio-button-text">False</span>
                                                    <input type="radio" name="defaultValue" checked={["false", false].includes(this.state.selectedPathParam.defaultValue)} value="false" onChange={this.onChangeHandlerPathParams} />
                                                    <span className="radio-button-mark"></span>
                                                </label>
                                            </div>
                                        </div>
                                        :
                                        this.state.selectedPathParam.type === "map" ?
                                            <div className="form-group">
                                                <div className="json-input-box">
                                                    <JSONInput
                                                        id="addMapDocument"
                                                        theme="light_mitsuketa_tribute"
                                                        placeholder={typeof this.state.selectedPathParam.defaultValue === "string" ? JSON.parse(this.state.selectedPathParam.defaultValue) : this.state.selectedPathParam.defaultValue}
                                                        colors={{
                                                            default: "#212F3D",
                                                            number: "#2ECC71",
                                                            string: "#ab210f",
                                                            keys: "#550fab",
                                                        }}
                                                        onChange={(e) => this.handleMapParamsInput(e, true)}
                                                    />
                                                </div>
                                            </div>
                                            : <input type="text" id="defaultValue" value={this.state.selectedPathParam.defaultValue} onChange={this.onChangeHandlerPathParams} className="form-control" required />}
                                    {this.state.errorMessageCode ? <span className="form-group-error">Value should be of {this.state.selectedPathParam.type} type </span> : ''}
                                </div>

                                <div className="form-group">
                                    <label className="form-group-label">Required : <i className="fas fa-asterisk form-group-required"></i></label>
                                    <div className="d-flex">
                                        <div className="flex-25 pd-r-10">
                                            <label className="radio-button">
                                                <span className="radio-button-text">True</span>
                                                <input type="radio" name="required" checked={this.state.selectedPathParam.required} value="true" onChange={this.onChangeHandlerPathParams} />
                                                <span className="radio-button-mark"></span>
                                            </label>
                                        </div>
                                        <div className="flex-25 pd-r-10">
                                            <label className={"radio-button " + (this.state.selectedPathParam.in === "path" ? " radioDisabled" : "")}>
                                                <span className="radio-button-text">False</span>
                                                <input type="radio" name="required" checked={!this.state.selectedPathParam.required} disabled={this.state.selectedPathParam.in === "path"} value="false" onChange={this.onChangeHandlerPathParams} />
                                                <span className="radio-button-mark"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="modal-footer justify-content-start">
                                <button className="btn btn-primary">{this.state.selectedParamIndex >= 0 ? "Update" : "Save"}</button>
                                <button type="button" className="btn btn-dark" onClick={() => this.setState({ isParamForm: false, selectedParamIndex: -1, errorMessageCode: 0 })}>Cancel</button>
                            </div>
                        </form>
                    </div>
                </SlidingPane>
                {/* end add param modal */}

                {/* response format modal */}
                <div className="modal fade animated slideInDown" role="dialog" id="responseModal">
                    <div className="modal-dialog modal-lg modal-dialog-centered">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h6 className="modal-title">Response Format
                                    <button className="close" data-dismiss="modal"><i className="far fa-times"></i></button>
                                </h6>
                            </div>
                            <div className="modal-body">
                                <div className="form-group">
                                    {this.state.payload.category !== "other" &&
                                        <div className="jsonInputBox">
                                            <JSONInput viewOnly={true} placeholder={this.state.payload.responseFormat ? this.state.payload.responseFormat : {}} />
                                        </div>
                                    }
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button className="btn btn-success" data-dismiss="modal">OK</button>
                            </div>
                        </div>
                    </div>
                </div>
                {/* end response format modal */}

                {/* full screen modal */}
                {this.state.editorModal &&
                    <div className="modal d-block animated slideInDown">
                        <div className="modal-dialog modal-full">
                            <div className="modal-content">
                                <div className="modal-body p-2">
                                    <div className="modal-full-editor">
                                        <div className="ace-editor h-100">
                                            <AceEditor
                                                placeholder="Type code here..."
                                                mode={this.state.selectedFileType}
                                                theme="monokai"
                                                name="ace_editor"
                                                onChange={(code) => this.setState({ code })}
                                                readOnly={((this.props.match.params.id || this.state.savedFaasId) && this.state.gitDiff) ? true : false}
                                                value={this.state.code}
                                                showPrintMargin={false}
                                                showGutter={true}
                                                highlightActiveLine={true}
                                                setOptions={{
                                                    enableBasicAutocompletion: true,
                                                    enableLiveAutocompletion: true,
                                                    enableSnippets: false,
                                                    showLineNumbers: true,
                                                    tabSize: 2,
                                                }}
                                                height="100%"
                                                width="100%"
                                            />
                                        </div>
                                        <button type="button" className="btn btn-light" onClick={() => { this.setState({ editorModal: false }) }}>
                                            <i className="fas fa-compress"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                }
                {/* end full screen modal */}


                {/* debug logs modal */}
                {this.state.outputModal &&
                    <div className="modal d-block animated slideInDown">
                        <div className="modal-dialog modal-lg modal-dialog-centered">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h6 className="modal-title">Debug Logs
                                    <button type="button" className="close" onClick={() => { this.setState({ outputModal: false, debugLogs: [] }) }}>
                                            <i className="far fa-times"></i>
                                        </button>
                                    </h6>
                                </div>
                                <div className="modal-body p-3">
                                    <div className="debugLogBox">
                                        {this.state.debugLogs.length ?
                                            this.state.debugLogs.map((log, logIndex) => (
                                                <pre key={logIndex}>
                                                    {log}
                                                </pre>
                                            )) : <pre> No logs found.</pre>
                                        }
                                    </div>
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-success" onClick={() => this.setState({ outputModal: false })}>OK</button>
                                </div>
                            </div>
                        </div>
                    </div>
                }
                {/* end debug logs modal */}

                {this.state.isOpen &&
                    <NotificationModal
                        type={this.state.modalType}
                        message2={this.state.message2}
                        onCloseHandler={this.onCloseHandler}
                    />
                }
            </React.Fragment>
        );
    }
}

AddOrEditFaasList.propTypes = {
    dispatch: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
    faasSubmitSuccess: Selectors.faasSubmitSuccess(),
    submitFaasFailure: Selectors.submitFaasFailure(),
    getFassByIdSuccess: Selectors.getFassByIdSuccess(),
    getFassByIdFailure: Selectors.getFassByIdFailure(),
    isFetching: Selectors.getIsFetching(),
    connectionString: Selectors.getConnectionStringSuccess(),
    connectionStringFailure: Selectors.getConnectionStringFailure(),
    getFaasImagesSuccess: Selectors.getFaasImagesSuccess(),
    getFaasImagesFailure: Selectors.getFaasImagesFailure(),
    templateCodeForFaaS: Selectors.getTemplateCodeFaasSuccess(),
    templateCodeForFaaSFailure: Selectors.getTemplateCodeFaasFailure(),
});

function mapDispatchToProps(dispatch) {
    return {
        dispatch,
        formSubmitHandler: (payload, id) => dispatch(Actions.formSubmitHandler(payload, id)),
        getFaasById: (id) => dispatch(Actions.getFaasById(id)),
        getConnectionString: () => dispatch(Actions.getConnectionString()),
        getFaasImages: (interpreter) => dispatch(Actions.getFaasImages(interpreter)),
        getTemplateCodeForFaaS: (interpreter, database) => dispatch(Actions.getTemplateCodeForFaaS(interpreter, database)),
        resetToInitialState: () => dispatch(Actions.resetToInitialState()),
    };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "addOrEditFaasList", reducer });
const withSaga = injectSaga({ key: "addOrEditFaasList", saga });
export default compose(
    withReducer,
    withSaga,
    withConnect
)(AddOrEditFaasList);

