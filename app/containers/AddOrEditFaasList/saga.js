/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { takeEvery } from 'redux-saga';
import * as CONSTANTS from './constants';
import { apiCallHandler } from '../../api';

export function* saveFaasHandler(action) {
    yield [apiCallHandler(action, CONSTANTS.ON_SUBMIT_SUCCESS, CONSTANTS.ON_SUBMIT_FAILURE, 'saveFaaS')];
}

export function* getFaasDetailsById(action) {
    yield [apiCallHandler(action, CONSTANTS.GET_FAAS_BY_ID_SUCCESS, CONSTANTS.GET_FAAS_BY_ID_FAILURE, 'getFaaSById')];
}

export function* getConnectionStringApiHandlerAsync(action) {
    yield [apiCallHandler(action, CONSTANTS.GET_CONNECTION_STRING_SUCCESS, CONSTANTS.GET_CONNECTION_STRING_FAILURE, 'getConnectionString', true)];
}

export function* faasImagesHandler(action) {
    yield [apiCallHandler(action,CONSTANTS.GET_FAAS_IMAGES_SUCCESS,CONSTANTS.GET_FAAS_IMAGES_FAILURE, 'getFaasImages')]
}

export function* getTemplateCodeFaaSHandler(action) {
    yield [apiCallHandler(action,CONSTANTS.GET_TEMPLATE_CODE_FAAS_SUCCESS,CONSTANTS.GET_TEMPLATE_CODE_FAAS_FAILURE, 'getTemplateCodeFaaS')]
}

export function* watcherFaasRequest() {
    yield takeEvery(CONSTANTS.ON_SUBMIT, saveFaasHandler);
}

export function* watcherGetFaasById() {
    yield takeEvery(CONSTANTS.GET_FAAS_BY_ID, getFaasDetailsById);
}

export function* watcherGetConnectionString() {
    yield takeEvery(CONSTANTS.GET_CONNECTION_STRING_REQUEST, getConnectionStringApiHandlerAsync);
}

export function* watcherGetFaasImages() {
    yield takeEvery(CONSTANTS.GET_FAAS_IMAGES, faasImagesHandler);
}

export function* watcherGetTemplateCodeFaas() {
    yield takeEvery(CONSTANTS.GET_TEMPLATE_CODE_FAAS, getTemplateCodeFaaSHandler);
}

export default function* rootSaga() {
    yield [
        watcherFaasRequest(),
        watcherGetFaasById(),
        watcherGetConnectionString(),
        watcherGetFaasImages(),
        watcherGetTemplateCodeFaas()
    ]
}

