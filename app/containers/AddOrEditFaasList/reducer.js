/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { fromJS } from "immutable";
import * as CONSTANTS from "./constants";

export const initialState = fromJS({});

function addOrEditFaasListReducer(state = initialState, action) {
  switch (action.type) {
    case CONSTANTS.RESET_TO_INITIAL_STATE:
      return initialState;
    case CONSTANTS.ON_SUBMIT_SUCCESS:
      return Object.assign({}, state, {
        'FaasSubmitSuccess': action.response,
      });
    case CONSTANTS.ON_SUBMIT_FAILURE:
      return Object.assign({}, state, {
        "FaasSubmitFailure": { error: action.error, timestamp: Date.now() },
      });
    case CONSTANTS.GET_FAAS_BY_ID_SUCCESS:
      return Object.assign({}, state, {
        'getFassByIdSuccess': action.response
      });
    case CONSTANTS.GET_FAAS_BY_ID_FAILURE:
      return Object.assign({}, state, {
        "getFassByIdFailure": action.error
      });
    case CONSTANTS.FAAS_FILE_UPLOAD_SUCCESS:
      return Object.assign({}, state, {
        fileUploadSuccess: action.response.secure_url
      });
    case CONSTANTS.FAAS_FILE_UPLOAD_FAILURE:
      return Object.assign({}, state, {
        fileUploadFailure: action.error
      });
    case CONSTANTS.GET_CONNECTION_STRING_SUCCESS:
      return Object.assign({}, state, {
        getConnectionStringSuccess: action.response
      });
    case CONSTANTS.GET_CONNECTION_STRING_FAILURE:
      return Object.assign({}, state, {
        getConnectionStringFailure: action.error
      });
    case CONSTANTS.GET_FAAS_IMAGES_SUCCESS:
      return Object.assign({}, state, {
        'getFaasImagesSuccess': action.response
      });
    case CONSTANTS.GET_FAAS_IMAGES_FAILURE:
      return Object.assign({}, state, {
        'getFaasImagesFailure': action.error
      })
    case CONSTANTS.GET_TEMPLATE_CODE_FAAS:
      return Object.assign({}, state, {
        'getTemplateCodeFaasSuccess': undefined
      });
    case CONSTANTS.GET_TEMPLATE_CODE_FAAS_SUCCESS:
      return Object.assign({}, state, {
        'getTemplateCodeFaasSuccess': action.response
      });
    case CONSTANTS.GET_TEMPLATE_CODE_FAAS_FAILURE:
      return Object.assign({}, state, {
        'getTemplateCodeFaasFailure': action.error
      });
    default:
      return state;
  }
}

export default addOrEditFaasListReducer;
