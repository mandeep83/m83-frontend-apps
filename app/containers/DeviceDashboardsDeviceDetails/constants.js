/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/*
 *
 * DeviceDashboardsDeviceDetails constants
 *
 */

export const RESET_TO_INITIAL_STATE = "app/DeviceDashboardsDeviceDetails/RESET_TO_INITIAL_STATE";

export const GET_DEVICE_INFO = {
    action : 'app/DeviceDashboardsDeviceDetails/GET_DEVICE_INFO',
    success: "app/DeviceDashboardsDeviceDetails/GET_DEVICE_INFO_SUCCESS",
    failure: "app/DeviceDashboardsDeviceDetails/GET_DEVICE_INFO_FAILURE",
    urlKey: "getDetailsPageData",
    subKey: "deviceInfo",
    successKey: "getDeviceInfoSuccess",
    failureKey: "getDeviceInfoFailure",
    actionName: "getDeviceInfo",
    actionArguments : ["connectorId", "deviceId"]
}

export const GET_DEVICE_TOPOLOGY = {
    action : 'app/DeviceDashboardsDeviceDetails/GET_DEVICE_TOPOLOGY',
    success: "app/DeviceDashboardsDeviceDetails/GET_DEVICE_TOPOLOGY_SUCCESS",
    failure: "app/DeviceDashboardsDeviceDetails/GET_DEVICE_TOPOLOGY_FAILURE",
    urlKey: "getDetailsPageData",
    subKey: "deviceTopology",
    successKey: "getDeviceTopologySuccess",
    failureKey: "getDeviceTopologyFailure",
    actionName: "getDeviceTopology",
    actionArguments : ["connectorId", "deviceId"]
}

export const GET_DEVICE_DETAILS = {
    action : 'app/DeviceDashboardsDeviceDetails/GET_DEVICE_DETAILS',
    success: "app/DeviceDashboardsDeviceDetails/GET_DEVICE_DETAILS_SUCCESS",
    failure: "app/DeviceDashboardsDeviceDetails/GET_DEVICE_DETAILS_FAILURE",
    urlKey: "getDetailsPageData",
    subKey: "deviceDetails",
    successKey: "getDeviceDetailsSuccess",
    failureKey: "getDeviceDetailsFailure",
    actionName: "getDeviceDetails",
    actionArguments : ["connectorId", "deviceId"]
}

export const GET_WEATHER_INFO = {
    action : 'app/DeviceDashboardsDeviceDetails/GET_WEATHER_INFO',
    success: "app/DeviceDashboardsDeviceDetails/GET_WEATHER_INFO_SUCCESS",
    failure: "app/DeviceDashboardsDeviceDetails/GET_WEATHER_INFO_FAILURE",
    urlKey: "getDetailsPageData",
    subKey: "weather",
    successKey: "getWeatherInfoSuccess",
    failureKey: "getWeatherInfoFailure",
    actionName: "getWeatherInfo",
    actionArguments : ["connectorId", "deviceId"]
}

export const GET_ALARMS_INFO = {
    action : 'app/DeviceDashboardsDeviceDetails/GET_ALARMS_INFO',
    success: "app/DeviceDashboardsDeviceDetails/GET_ALARMS_INFO_SUCCESS",
    failure: "app/DeviceDashboardsDeviceDetails/GET_ALARMS_INFO_FAILURE",
    urlKey: "getAlarmFilterData",
    successKey: "getAlarmsInfoSuccess",
    failureKey: "getAlarmsInfoFailure",
    actionName: "getAlarmsInfo",
    actionArguments : ["payload"]
}

export const GET_DASHBOARD_INFO = {
    action : 'app/DeviceDashboardsDeviceDetails/GET_DASHBOARD_INFO',
    success: "app/DeviceDashboardsDeviceDetails/GET_DASHBOARD_SUCCESS",
    failure: "app/DeviceDashboardsDeviceDetails/GET_DASHBOARD_FAILURE",
    urlKey: "dashboardConfig",
    successKey: "dashboardConfigSuccess",
    failureKey: "dashboardConfigFailure",
    actionName: "dashboardConfig",
    actionArguments : ["id"]
}

export const GET_CHART_INFO = {
    action : 'app/DeviceDashboardsDeviceDetails/GET_CHART_INFO',
    success: "app/DeviceDashboardsDeviceDetails/GET_CHART_SUCCESS",
    failure: "app/DeviceDashboardsDeviceDetails/GET_CHART_FAILURE",
    urlKey: "getMonitoringchartData",
    successKey: "getMonitoringchartSuccess",
    failureKey: "getMonitoringchartDataFailure",
    actionName: "getMonitoringchartData",
    actionArguments : ["id","chartType","payload"]
}

export const GET_LINE_CHART_INFO = {
    action : 'app/DeviceDashboardsDeviceDetails/GET_LINE_CHART_INFO',
    success: "app/DeviceDashboardsDeviceDetails/GET_LINE_CHART_SUCCESS",
    failure: "app/DeviceDashboardsDeviceDetails/GET_LINE_CHART_FAILURE",
    urlKey: "getLineMonitoringchartData",
    successKey: "getLineMonitoringchartSuccess",
    failureKey: "getLineMonitoringchartFailure",
    actionName: "getLineMonitoringchartData",
    actionArguments : ["id","payload"]
}

export const GET_CONFIG_INFO = {
    action : 'app/DeviceDashboardsDeviceDetails/GET_CONFIG_INFO',
    success: "app/DeviceDashboardsDeviceDetails/GET_CONFIG_SUCCESS",
    failure: "app/DeviceDashboardsDeviceDetails/GET_CONFIG_FAILURE",
    urlKey: "saveConfig",
    successKey: "saveConfigSuccess",
    failureKey: "saveConfigFailure",
    actionName: "saveConfig",
    actionArguments : ["id","payload"]
}

export const GET_ATTRIBUTES_INFO = {
    action : 'app/DeviceDashboardsDeviceDetails/GET_ATTRIBUTES_INFO',
    success: "app/DeviceDashboardsDeviceDetails/GET_ATTRIBUTES_SUCCESS",
    failure: "app/DeviceDashboardsDeviceDetails/GET_ATTRIBUTES_FAILURE",
    urlKey: "getAttributes",
    successKey: "getAttributesSuccess",
    failureKey: "getAttributesFailure",
    actionName: "getAttributes",
    actionArguments : ["id"]
}

export const UPDATE_ALARM_STATUS = {
    action : 'app/DeviceDashboardsDeviceDetails/UPDATE_ALARM_STATUS',
    success: "app/DeviceDashboardsDeviceDetails/UPDATE_ALARM_STATUS_SUCCESS",
    failure: "app/DeviceDashboardsDeviceDetails/UPDATE_ALARM_STATUS_FAILURE",
    urlKey: "updateAlarmStatus",
    successKey: "updateAlarmStatusSuccess",
    failureKey: "updateAlarmStatusFailure",
    actionName: "updateAlarmStatus",
    actionArguments : ["payload", 'alarmDetails']
}

export const GET_COMMAND_HISTORY_DATA = {
    action : 'app/DeviceDashboardsDeviceDetails/GET_COMMAND_HISTORY_DATA',
    success: "app/DeviceDashboardsDeviceDetails/GET_COMMAND_HISTORY_DATA_SUCCESS",
    failure: "app/DeviceDashboardsDeviceDetails/GET_COMMAND_HISTORY_DATA_FAILURE",
    urlKey: "getCommandHistory",
    successKey: "getCommandHistorySuccess",
    failureKey: "getCommandHistoryFailure",
    actionName: "getCommandHistory",
    actionArguments : ["payload"]
}

export const GET_ALL_RPC_LIST = {
	action: 'app/DeviceDashboardsDeviceDetails/GET_ALL_RPC_LIST',
	success: "app/DeviceDashboardsDeviceDetails/GET_ALL_RPC_LIST_SUCCESS",
	failure: "app/DeviceDashboardsDeviceDetails/GET_ALL_RPC_LIST_FAILURE",
	urlKey: "getAllRpc",
	successKey: "getAllRpcSuccess",
	failureKey: "getAllRpcFailure",
	actionName: "getAllRpc",
	actionArguments: ["id"]
}

export const RPC_COMMAND_PUBLISH = {
	action: 'app/DeviceDashboardsDeviceDetails/RPC_COMMAND_PUBLISH',
	success: "app/DeviceDashboardsDeviceDetails/RPC_COMMAND_PUBLISH_SUCCESS",
	failure: "app/DeviceDashboardsDeviceDetails/RPC_COMMAND_PUBLISH_FAILURE",
	urlKey: "rpcCommandPublish",
	successKey: "rpcCommandPublishSuccess",
	failureKey: "rpcCommandPublishFailure",
	actionName: "rpcCommandPublish",
	actionArguments: ["payload"]
}

export const SAVE_COMMANDS = {
	action: 'app/DeviceDashboardsDeviceDetails/SAVE_COMMANDS',
	success: "app/DeviceDashboardsDeviceDetails/SAVE_COMMANDS_SUCCESS",
	failure: "app/DeviceDashboardsDeviceDetails/SAVE_COMMANDS_FAILURE",
	urlKey: "saveCommands",
	successKey: "saveCommandsSuccess",
	failureKey: "saveCommandsFailure",
	actionName: "saveCommands",
	actionArguments: ["payload","isAddMode"]
}
