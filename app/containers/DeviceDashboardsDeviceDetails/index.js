/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * DeviceDashboardsDeviceDetails
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import { allSelectors as SELECTORS } from "./selectors";
import { allActions as ACTIONS } from './actions'
import reducer from "./reducer";
import saga from "./saga";
import { convertTimestampToDate, getTimeDifference, uIdGenerator, isNavAssigned, getBucketSizeOptions, DURATION_OPTIONS, getMiniLoader } from "../../commonUtils";
import WeatherWidget from "../../components/WeatherWidget/Loadable"
import { cloneDeep } from 'lodash';
import Loader from "../../components/Loader/Loadable";
import CreateChart from "../../components/CreateChart/Loadable";
import moment from 'moment-timezone';
import Datetime from "datetime-react-83incs";
import AceEditor from "react-ace";
import 'brace/mode/python';
import 'brace/mode/json';
import 'brace/theme/monokai';
import 'brace/theme/github';
import 'brace/ext/searchbox'
import 'brace/ext/language_tools'
import AddNewButton from "../../components/AddNewButton/Loadable";
import ConfirmModel from '../../components/ConfirmModel/Loadable'
import ListingTable from "../../components/ListingTable/Loadable";
import CommandConfigModal from "../../components/CommandConfigModal";
import ReactSelect from "react-select";
import { AgGridReact } from 'ag-grid-react';
import ConnectionHistoryChart from "../../components/ConnectionHistoryChart";
import MonitoringLineChart from "../../components/MonitoringLineChart";
import InsightsLineGraph from "../../components/InsightsLineGraph";
import produce from "immer";
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';


let getAttributeNamesFromArray = (attributesList) => {
    let attributeNames = "";
    attributesList.forEach((attr, index) => {
        if (index === 0) {
            attributeNames = attr.attribute;
        } else {
            attributeNames += `,${attr.attribute}`;
        }
    })

    return attributeNames;
}

/* eslint-disable react/prefer-stateless-function */
let ALARM_TYPE = {
    "critical": "fad fa-exclamation-triangle text-critical",
    "major": "fad fa-exclamation-square text-major",
    "minor": "fad fa-exclamation-circle text-minor",
    "warning": "fad fa-exclamation text-alarm-warning",
    "": "fad fa-check-circle text-green"
}

let PARENT_PAGE_TITLE = {
    "deviceDetails": "Device List",
    "deviceInfo": "Map & Geofencing "
}

let PARENT_PAGE_ROUTE = {
    "deviceDetails": "/deviceList",
    "deviceInfo": "/deviceMap"
}

let CURRENT_PAGE_TITLE = {
    "deviceDetails": "Device Details",
    "deviceInfo": "Device Map"
}

let interval = undefined

export class DeviceDashboardsDeviceDetails extends React.Component {
    state = {
        currentPathName: window.location.pathname.split('/')[1],
        currentTab: localStorage.currentTab ? localStorage.currentTab : "deviceOverview",
        deviceInfoLoader: true,
        deviceInfo: {},
        deviceDetailsLoader: true,
        deviceDetails: {},
        weatherInfoLoader: true,
        weatherInfo: {},
        deviceTopologyLoader: true,
        deviceTopology: [],
        deviceCommandLoader: true,
        rpcData: [],
        attributesList: [],
        attributeSuccess: false,
        deviceMonitoringLoader: true,
        dashboardConfigData: [],
        floatingButton: false,
        configureModal: false,
        chartCustomConfig: {},
        dateError: "",
        duration: 60,
        payload: {
            from: Date.now() - 3600000,
            to: Date.now(),
        },
        attrData: {
            operator: ">=",
            value: "0"
        },
        isCommandHistoryLoader: true,
        commandHistoryPayload: {
            from: Date.now() - 259200000,
            to: Date.now(),
            duration: 4320,
            deviceTypeId: this.props.match.params.deviceTypeId,
            uniqueId: this.props.match.params.deviceId
        },
        CommandHistoryDateError: "",
        commandHistoryData: [],
        isCommandPayload: false,
        commandPayload: "",

        alarmsInfoLoader: true,
        alarmsInfo: {},

        historyOperationDetails: {},
        historicViewModal: false,
        intervalTime: { value: 15000, label: '15 secs' },
        deviceCommandinProgress: "",
        setIntervalTime: false,
        intervalCreator: () => this.intervalCreator(),
        operations: [
            {
                label: "NONE",
                value: null,
                displayName: 'None',
            },
            {
                label: "RATE_OF_CHANGE",
                value: "rateOfChange",
                displayName: 'Rate_Of_Change',
            },
            {
                label: "AVG",
                value: "avg",
                displayName: 'Average',
            },
            {
                label: "COUNT",
                value: "count",
                displayName: 'Count',
            },
            {
                label: "MAX",
                value: "max",
                displayName: 'Max',
            },
            {
                label: "MIN",
                value: "min",
                displayName: 'Min',
            },
            {
                label: "SUM",
                value: "sum",
                displayName: 'Sum',
            },
            {
                label: "STDDEV",
                value: "stddev",
                displayName: 'Standard_Deviation',
            },
            {
                label: "STDDEV_POP",
                value: "stddev_pop",
                displayName: 'Standard_Deviation_POP',
            },
            {
                label: "STDDEV_SAMP",
                value: "stddev_samp",
                displayName: 'Standard_Deviation_SAMP',
            },
            {
                label: "VARIANCE",
                value: "variance",
                displayName: 'Variance',
            },
            {
                label: "VARIANCE_POP",
                value: "var_pop",
                displayName: 'Variance_POP',
            },
            {
                label: "VARIANCE_SAMP",
                value: "var_samp",
                displayName: 'Variance_SAMP',
            }
        ],
        COMMAND_OBJ: {
            controlName: "",
            controlCommand: {},
            deviceTypeId: this.props.match.params.deviceTypeId
        },
        bucketSizeOptions: [],
        insightsDuration: 60,
    }

    listRef = React.createRef();

    componentDidMount() {
        this.getSelectedTabData()
    }

    componentWillUnmount() {
        delete localStorage.currentTab
        clearInterval(interval)
    }

    static getDerivedStateFromProps(nextProps, state) {
        let newProp = Object.keys(SELECTORS).find(prop => nextProps[prop])
        let loaderKeys = {
            getDeviceInfoFailure: "deviceInfoLoader",
            getDeviceTopologyFailure: "deviceTopologyLoader",
            getDeviceDetailsFailure: "deviceDetailsLoader",
            getWeatherInfoFailure: "weatherInfoLoader",
            getAlarmsInfoFailure: "alarmsInfoLoader",
        }

        if (newProp) {
            let propData = nextProps[newProp].error ? nextProps[newProp].error : nextProps[newProp].response;

            if (nextProps.dashboardConfigSuccess) {
                let dashboardConfigData = propData.widgets;
                dashboardConfigData.map((val, index) => {
                    val.index = index;
                    val.operationDetails = {
                        operation: null,
                        bucketSize: state.bucketSizeOptions[0] && state.bucketSizeOptions[0]['value'],
                        attrData: {
                            operator: ">=",
                            value: 0
                        }
                    }

                    //Line Chart
                    let lineObj = {
                        deviceId: nextProps.match.params.deviceId,
                        deviceTypeId: nextProps.match.params.deviceTypeId,
                        attrCsv: val.lineChart.attributes.length > 0 ? val.lineChart.attributes.toString() : ["current"].toString(),
                        operation: null,
                    }
                    if (state.duration) {
                        lineObj.duration = Number(state.duration)
                    }
                    else {
                        lineObj.from = state.payload.from
                        lineObj.to = state.payload.to
                    }

                    //Gauge Chart
                    let gaugeObj = {
                        uniqueId: nextProps.match.params.deviceId,
                        connectorId: nextProps.match.params.deviceTypeId,
                        minVal: val.gaugeChart.min,
                        maxVal: val.gaugeChart.max,
                        attrName: val.gaugeChart.attributes.length > 0 ? val.gaugeChart.attributes.toString() : ["current"].toString()
                    }

                    nextProps.getLineMonitoringchartData(val.id, lineObj)
                    nextProps.getMonitoringchartData(val.id, "gaugeChart", gaugeObj)
                })
                return { dashboardConfigData, deviceMonitoringLoader: false }
            }

            else if (nextProps.getMonitoringchartSuccess) {
                let dashboardConfigData = state.dashboardConfigData, historyChartData = state.historyChartData ? cloneDeep(state.historyChartData) : {}, historyChartLoading = state.historyChartLoading;
                if (nextProps.getMonitoringchartSuccess.response.id === 'attrHistoricChart') {
                    historyChartData = {
                        data: nextProps.getMonitoringchartSuccess.response.data,
                        attributes: [state.historyChartAttributeName]
                    }
                    historyChartLoading = false
                } else {
                    dashboardConfigData = produce(state.dashboardConfigData, dashboardConfigData => {
                        dashboardConfigData.map(val => {
                            if (val.id === nextProps.getMonitoringchartSuccess.response.id) {
                                val[nextProps.getMonitoringchartSuccess.response.chartOf].data = nextProps.getMonitoringchartSuccess.response.data
                            }
                        })
                    })
                }
                if (interval) {
                    clearInterval(interval);
                    interval = undefined;
                }
                if (state.setIntervalTime) {
                    state.intervalCreator();
                }
                return { dashboardConfigData, historyChartData, historyChartLoading }
            }

            else if (nextProps.saveConfigSuccess) {
                state.dashboardConfigData.map((val, index) => {
                    if (val.gaugeChart.data === undefined) {
                        let obj = {
                            uniqueId: nextProps.match.params.deviceId,
                            connectorId: nextProps.match.params.deviceTypeId,
                            minVal: Number(val.gaugeChart.min),
                            maxVal: Number(val.gaugeChart.max),
                            attrName: val.attributes.toString()
                        }
                        nextProps.getMonitoringchartData(val.id, "gaugeChart", obj)
                    }
                    if (val.lineChart.data === undefined) {
                        let obj = {
                            deviceId: nextProps.match.params.deviceId,
                            deviceTypeId: nextProps.match.params.deviceTypeId,
                            attrCsv: val.attributes.toString(),
                            operation: null
                        }
                        if (state.duration === 0) {
                            obj.from = state.payload.from
                            obj.to = state.payload.to
                        } else
                            obj.duration = Number(state.duration)
                        nextProps.getLineMonitoringchartData(val.id, obj);
                    }
                })
                clearInterval(interval);
                interval = undefined;
                if (state.setIntervalTime && state.dashboardConfigData.length) {
                    state.intervalCreator()
                }

                return {
                    isOpen: true,
                    type: "success",
                    message2: "Request Completed Successfully"
                }
            }

            else if (nextProps.saveConfigFailure) {
                nextProps.dashboardConfig(nextProps.match.params.deviceTypeId);
                return {
                    isOpen: true,
                    type: "error",
                    message2: nextProps.saveConfigFailure.error,
                }
            }

            else if (nextProps.getLineMonitoringchartSuccess) {
                let dashboardConfigData = state.dashboardConfigData,
                    historyChartData = state.historyChartData ? cloneDeep(state.historyChartData) : {},
                    historyChartLoading = state.historyChartLoading;
                let responseOf = nextProps.getLineMonitoringchartSuccess.response.id;
                if (responseOf === "ALL") {
                    let insightsTableData = nextProps.getLineMonitoringchartSuccess.response.data
                    let insightsTableState = insightsTableData.chartData.length ? "loaded" : "emptyData";
                    insightsTableData.chartData.reverse();
                    return {
                        insightsTableState,
                        insightsTableData
                    }
                }

                if (responseOf === "insightsGraph") {
                    let insightsGraphData = nextProps.getLineMonitoringchartSuccess.response.data;
                    let insightsGraphState = insightsGraphData.chartData.length ? "loaded" : "emptyData";
                    return {
                        insightsGraphState,
                        insightsGraphData
                    }
                } else {
                    if (responseOf === 'attrHistoricChart') {
                        let appliedOperation = state.historyOperationDetails.operation;
                        if (nextProps.getLineMonitoringchartSuccess.response.dataof === "data") {
                            historyChartData[nextProps.getLineMonitoringchartSuccess.response.dataof] = nextProps.getLineMonitoringchartSuccess.response.data;
                            historyChartLoading = false
                            if (appliedOperation) {
                                let obj = {
                                    deviceId: nextProps.match.params.deviceId,
                                    deviceTypeId: nextProps.match.params.deviceTypeId,
                                    attrCsv: [state.historyChartAttributeName].toString(),
                                    duration: Number(state.historyChartDuration),
                                    operation: appliedOperation
                                };
                                if (appliedOperation == "rateOfChange") {
                                    obj.attrData = {
                                        [obj.attrCsv]: state.historyOperationDetails.attrData
                                    }
                                } else {
                                    obj.bucketSize = Number(state.historyOperationDetails.bucketSize)
                                }
                                historyChartLoading = true;
                                nextProps.getLineMonitoringchartData("attrHistoricChart", obj)
                            }
                        } else {
                            if (appliedOperation === "rateOfChange") {
                                historyChartData.operationData = nextProps.getLineMonitoringchartSuccess.response.data.chartData
                            }
                            else {
                                historyChartData.operationData = [...(historyChartData.data.chartData || []), ...(nextProps.getLineMonitoringchartSuccess.response.data.chartData.map(item => {
                                    item[appliedOperation] = item[nextProps.getLineMonitoringchartSuccess.response.data.availableAttributes[0].attr];
                                    item.timestamp = item.category;
                                    delete item.category;
                                    delete item[nextProps.getLineMonitoringchartSuccess.response.data.availableAttributes[0].attr];
                                    return item;
                                }))];
                            }
                            historyChartLoading = false;
                        }
                        historyChartData.attributes = [state.historyChartAttributeName];
                    } else {
                        dashboardConfigData = produce(state.dashboardConfigData, dashboardConfigData => {
                            dashboardConfigData.map((val, index) => {
                                if (val.id === responseOf) {
                                    let appliedOperation = val.operationDetails.operation;
                                    if (nextProps.getLineMonitoringchartSuccess.response.dataof === "data") {
                                        val.lineChart.data = nextProps.getLineMonitoringchartSuccess.response.data;
                                        if (appliedOperation) {
                                            let obj = {
                                                deviceId: nextProps.match.params.deviceId,
                                                deviceTypeId: nextProps.match.params.deviceTypeId,
                                                attrCsv: val.attributes.toString(),
                                                operation: appliedOperation,
                                            }
                                            if (state.duration === 0) {
                                                obj.from = state.payload.from
                                                obj.to = state.payload.to
                                            }
                                            else {
                                                obj.duration = Number(state.duration)
                                            }

                                            if (appliedOperation == "rateOfChange") {
                                                obj.attrData = {
                                                    [obj.attrCsv]: state.dashboardConfigData[index].operationDetails.attrData,
                                                }
                                            } else if (appliedOperation != "rateOfChange") {
                                                obj.bucketSize = Number(val.operationDetails.bucketSize)
                                            }
                                            nextProps.getLineMonitoringchartData(val.id, obj)
                                        }
                                    } else {
                                        let chartData = nextProps.getLineMonitoringchartSuccess.response.data.chartData;
                                        //** Changes needed below this */

                                        val.lineChart.operationData = chartData
                                        // if (appliedOperation === "rateOfChange") {
                                        //     val.lineChart.operationData = chartData
                                        // }
                                        // else {
                                        //     val.lineChart.operationData =  chartData
                                        // }
                                        // val.lineChart.operationData.sort((a,b) => a.category - b.category)
                                    }
                                }
                            });
                        })
                    }
                    if (interval) {
                        clearInterval(interval);
                        interval = undefined;
                    }
                    if (state.setIntervalTime) {
                        state.intervalCreator();
                    }
                }
                return { dashboardConfigData, historyChartData, historyChartLoading }
            }

            else if (nextProps.getLineMonitoringchartFailure) {
                let responseOf = nextProps.getLineMonitoringchartFailure.addOns.id
                let isForInsightsTable = responseOf === "ALL";
                if (isForInsightsTable) {
                    return {
                        insightsTableState: "error"
                    }
                }
                let isForInsightsGraph = responseOf === "insightsGraph";
                if (isForInsightsGraph) {
                    return {
                        insightsGraphState: "error"
                    }
                }
            }

            else if (nextProps.getAttributesSuccess) {
                let attributesList = propData.metaInfo;
                let insightsTableState = state.insightsTableState;
                let insightsAttributesStatus = attributesList.length ? "loaded" : "noAttributes";
                let insightsGraphState = state.insightsGraphState;
                let insightsGraphAttributes = state.insightsGraphAttributes;
                if (state.currentTab === "insights") {
                    if (attributesList.length) {
                        insightsTableState = "loading";
                        insightsGraphState = "loading";
                        let allAttributes = getAttributeNamesFromArray(attributesList);
                        let payload = {
                            deviceId: nextProps.match.params.deviceId,
                            deviceTypeId: nextProps.match.params.deviceTypeId,
                            operation: null,
                            duration: state.insightsDuration,
                        }
                        nextProps.getLineMonitoringchartData("ALL", {...payload, attrCsv: allAttributes});
                        let { attribute:value, displayName:label} = attributesList[0];
                        insightsGraphAttributes = [{label, value, attribute: value}];
                        nextProps.getLineMonitoringchartData("insightsGraph", {...payload, attrCsv: value});
                    }
                }
                return { attributesList, attributeSuccess: true, insightsAttributesStatus, insightsTableState, insightsGraphState, insightsGraphAttributes }
            }
            else if (nextProps.getAttributesFailure && state.currentTab === "insights") {
                return {
                    insightsAttributesStatus: "error"
                }
            }
            else if (nextProps.getDeviceInfoSuccess) {
                return {
                    deviceInfo: propData,
                    deviceInfoLoader: false,
                    bucketSizeOptions: getBucketSizeOptions(propData.reportInterval),
                }
            }
            else if (nextProps.getDeviceTopologySuccess) {
                return { deviceTopology: propData, deviceTopologyError: propData.length == 0 ? "No topology is defined for this Device Type" : false, deviceTopologyLoader: false }
            }
            else if (nextProps.getDeviceDetailsSuccess) {
                return { deviceDetails: propData.data, deviceDetailsLoader: false }
            }
            else if (nextProps.getWeatherInfoSuccess) {
                let weatherInfo = { ...propData };
                if (weatherInfo.isDataAvailable) {
                    weatherInfo.weatherData.selectedDay = propData.weatherData.currentDate
                    weatherInfo.weatherData.daily7DayForecast[0] = propData.weatherData.currentDate
                }
                return { weatherInfo, weatherInfoLoader: false }
            }
            else if (nextProps.getAlarmsInfoSuccess) {
                return { alarmsInfo: propData[0] || {}, alarmsInfoLoader: false }
            }
            else if (nextProps.updateAlarmStatusSuccess || nextProps.updateAlarmStatusFailure) {
                let { alarmType, alarmAttribute, actionType } = nextProps.updateAlarmStatusSuccess ? nextProps.updateAlarmStatusSuccess.alarmDetails : nextProps.updateAlarmStatusFailure.addOns.alarmDetails
                let alarmsInfo = cloneDeep(state.alarmsInfo)
                let requiredAlarmsArr = alarmsInfo.alarms[alarmType]
                let requiredAlarmIndex = requiredAlarmsArr.findIndex(alarm => alarm.attr === alarmAttribute)
                requiredAlarmsArr[requiredAlarmIndex].loaderType = null
                if (actionType === "acknowledge") {
                    requiredAlarmsArr[requiredAlarmIndex].acknowledged = Boolean(nextProps.updateAlarmStatusSuccess);
                    requiredAlarmsArr[requiredAlarmIndex].timeAcknowledged = new Date().getTime();
                }
                else if (nextProps.updateAlarmStatusSuccess) {
                    requiredAlarmsArr.splice(requiredAlarmIndex, 1);
                    if (requiredAlarmsArr.length === 0)
                        delete alarmsInfo.alarms[alarmType];
                }
                return {
                    alarmsInfo
                }
            }
            else if (Object.keys(loaderKeys).includes(newProp)) {
                let loaderKey = loaderKeys[newProp]
                return { [loaderKey]: false, [loaderKey.replace("Loader", "Error")]: propData }
            }
            else if (nextProps.getCommandHistorySuccess) {
                return { commandHistoryData: propData, isCommandHistoryLoader: false }
            }

            else if (nextProps.getCommandHistoryFailure) {
                return { isCommandHistoryLoader: false }
            }
            else if (nextProps.getAllRpcSuccess) {
                return { rpcData: propData, deviceCommandLoader: false }
            }
            else if (nextProps.rpcCommandPublishSuccess) {
                return {
                    isOpen: true,
                    type: "success",
                    message2: nextProps.rpcCommandPublishSuccess.response.message,
                    deviceCommandLoader: false,
                    deviceCommandinProgress: "",
                    isDisableDeviceCommandLoader: false
                }
            }
            else if (nextProps.rpcCommandPublishFailure) {
                return {
                    deviceCommandLoader: false,
                    deviceCommandinProgress: "",
                    isDisableDeviceCommandLoader: false,
                }
            }
        }
        if (nextProps.timeZone !== state.timeZone) {
            return {
                timeZone: nextProps.timeZone,
            }
        }
        if (nextProps.saveCommandsSuccess) {
            // let commandModalPayload = cloneDeep(state.commandModalPayload);
            // let data = nextProps.saveCommandsSuccess.response.data;
            // if (nextProps.saveCommandsSuccess.response.isAddMode) {
            //     commandModalPayload.push({ ...state.COMMAND_OBJ, ...data });
            // } else {
            //     let index = commandModalPayload.findIndex(temp => temp.id == nextProps.saveCommandsSuccess.response.data.id);
            //     commandModalPayload[index] = { ...state.COMMAND_OBJ, ...data }
            // }
            // let controlConfig = cloneDeep(commandModalPayload),
            //     COMMAND_OBJ = {
            //         controlName: "",
            //         controlCommand: {},
            //         deviceTypeId: nextProps.match.params.deviceTypeId
            //     }
            nextProps.getAllRpc(nextProps.match.params.deviceTypeId)
            return {
                type: "success",
                isOpen: true,
                message2: nextProps.saveCommandsSuccess.response.message,
                //     activeCommandIndex: -1,
                commandModalLoader: false,
                deviceCommandLoader: true,
                addCommandModal: false
                //     deleteCommandLoader: false,
                //     controlConfig,
                //     COMMAND_OBJ,
                //     commandModalPayload
            };
        }

        if (nextProps.saveCommandsFailure) {
            return {
                commandModalLoader: false,
                deleteCommandLoader: false
            };
        }
        return null
    }

    componentDidUpdate(prevProps, prevState) {
        let newProp = Object.keys(SELECTORS).find(prop => this.props[prop])
        if (newProp) {
            if (newProp.includes("Failure")) {
                let propData = this.props[newProp].error ? this.props[newProp].error : cloneDeep(this.props[newProp].response)
                this.props.showNotification("error", propData, this.onCloseHandler)
            }
            this.props.resetToInitialState(newProp)
        }
        if (prevState.dashboardConfigData.length > 0 && this.state.dashboardConfigData.length > 0 && this.listRef.current && prevState.dashboardConfigData.length < this.state.dashboardConfigData.length) {
            let element = document.getElementById("contentBody")
            element.scrollTo(0, this.listRef.current.offsetTop)
        }
        if (this.state.timeZone !== prevState.timeZone) {
            this.getDurationBlock(this.state.duration, this.state.payload.from, this.state.payload.to, this.state.dateError, this.dateChangeHandler)
        }
    }

    getMiniLoader = () => {
        return <div className="card-loader">
            <i className="fad fa-sync-alt fa-spin"></i>
        </div>
    }

    getErrorMessage = (errorMessage, isTopologyError) => {
        return (
            <div className="card-message">
                {Array.isArray(errorMessage) ? errorMessage.map((message, i) => (<p key={i}>{message}</p>)) :
                    <p>{errorMessage}</p>
                }
            </div>
        )
    }

    setSelectedDay = (dayInfo) => {
        let weatherInfo = { ...this.state.weatherInfo };
        weatherInfo.weatherData.selectedDay = dayInfo
        this.setState({
            weatherInfo
        })
    }

    onMonitor = () => {
        if (!this.state.dashboardConfigData)
            this.props.dashboardConfig(this.props.match.params.deviceTypeId);
    }

    dropCharts = () => {
        let dashboardConfigData = cloneDeep(this.state.dashboardConfigData);
        dashboardConfigData.push({
            id: uIdGenerator(),
            attributes: [],
            headerName: "Sample Header",
            lineChart: {
                "headerName": "Sample Header",
                "category": "lineChart",
                "attributes": [],
                "data": "",
                "operationData": "",
                "index": dashboardConfigData.length,
            },
            gaugeChart: {
                "headerName": "Sample Header",
                "category": "gaugeChart",
                "min": 0,
                "max": 1000,
                "attributes": [],
                "data": {},
                "index": dashboardConfigData.length + 1,
            },
            operationDetails: {
                operation: null,
                bucketSize: this.state.bucketSizeOptions[0]['value'],
                attrData: {
                    operator: ">=",
                    value: 0
                }
            }
        })
        this.setState({ dashboardConfigData })
    }

    configChangeHandler = (e) => {
        let chartCustomConfig = cloneDeep(this.state.chartCustomConfig);
        if (e.target.id === "attribute") {
            let attr = this.state.attributesList.find(val => val.attribute === e.target.value);
            chartCustomConfig.attributes[0] = e.target.value;
            chartCustomConfig.lineChart.attributes[0] = e.target.value;
            chartCustomConfig.gaugeChart.attributes[0] = e.target.value;
            chartCustomConfig.gaugeChart.min = attr.isConfigured ? Number((attr.alarmCriteria[0].low - Math.abs(attr.alarmCriteria[0].high - attr.alarmCriteria[0].low)).toFixed(2)) : attr.minValue ? attr.minValue : 0;
            chartCustomConfig.gaugeChart.max = attr.isConfigured ? Number((attr.alarmCriteria[attr.alarmCriteria.length - 1].high + Math.abs(attr.alarmCriteria[0].high - attr.alarmCriteria[0].low)).toFixed(2)) : attr.maxValue ? attr.maxValue : 1000;
            chartCustomConfig.headerName = `Charts For ${attr.displayName != "" ? attr.displayName : attr.attribute}`;
        } else if (e.target.id === "min" || e.target.id === "max") {
            chartCustomConfig.gaugeChart[e.target.id] = Number(e.target.value);
        } else
            chartCustomConfig[e.target.id] = e.target.value;
        this.setState({ chartCustomConfig })
    };

    saveChartConfig = () => {
        if (this.state.chartCustomConfig.gaugeChart.min < this.state.chartCustomConfig.gaugeChart.max) {
            let dashboardConfigData = cloneDeep(this.state.dashboardConfigData);
            dashboardConfigData.map((val, index) => {
                if (val.id === this.state.chartCustomConfig.id) {
                    if (Number(val.gaugeChart.min) != Number(this.state.chartCustomConfig.gaugeChart.min) || Number(val.gaugeChart.max) != Number(this.state.chartCustomConfig.gaugeChart.max) || val.attributes[0] != this.state.chartCustomConfig.attributes[0]) {
                        val.gaugeChart = cloneDeep(this.state.chartCustomConfig.gaugeChart)
                        val.gaugeChart.data = undefined
                    }
                    if (val.attributes[0] != this.state.chartCustomConfig.attributes[0]) {
                        val.lineChart = cloneDeep(this.state.chartCustomConfig.lineChart)
                        val.lineChart.data = undefined
                    }
                    val.attributes = cloneDeep(this.state.chartCustomConfig.attributes)
                    val.headerName = cloneDeep(this.state.chartCustomConfig.headerName)
                }
            })
            this.setState({ configureModal: false, dashboardConfigData }, () => this.props.saveConfig(this.props.match.params.deviceTypeId, dashboardConfigData))
        } else {
            this.setState({
                isOpen: true,
                type: "error",
                message2: "Max value should be greater than Min value."
            })
        }
    }

    saveConfig = () => {
        let dashboardConfigData = this.state.dashboardConfigData;
        this.setState({
            floatingButton: false,
            dashboardConfigData: [],
            deviceMonitoringLoader: true,
        }, () => {
            this.props.saveConfig(this.props.match.params.deviceTypeId, dashboardConfigData)
        })
    }

    onCloseHandler = () => {
        this.setState((previousState, props) => ({
            isOpen: false,
            type: '',
            message2: ''
        }))
    }

    callLineChartDataFunction = () => {
        this.state.dashboardConfigData.map((val, index) => {
            if (val.lineChart.data != "") {
                let obj = {
                    deviceId: this.props.match.params.deviceId,
                    deviceTypeId: this.props.match.params.deviceTypeId,
                    attrCsv: val.attributes.toString(),
                    operation: null,
                }
                if (this.state.duration === 0) {
                    obj.from = this.state.payload.from
                    obj.to = this.state.payload.to
                }
                else {
                    obj.duration = Number(this.state.duration)
                }
                this.props.getLineMonitoringchartData(val.id, obj);
            }
        })
    }

    deleteChartHandler = () => {
        clearInterval(interval)
        interval = undefined;
        let dashboardConfigData = produce(this.state.dashboardConfigData, dashboardConfigData => {
            dashboardConfigData.splice(this.state.deleteChartIndex, 1);
        });
        this.setState({
            dashboardConfigData,
            confirmState: false,
        }, () => this.props.saveConfig(this.props.match.params.deviceTypeId, dashboardConfigData))
    }

    updateAlarmStatus = (alarmAttribute, alarmIndex, alarmType, actionType) => {
        let alarmsInfo = cloneDeep(this.state.alarmsInfo)
        alarmsInfo.alarms[alarmType][alarmIndex].loaderType = actionType;
        let payload = {
            "deviceTypeId": alarmsInfo.deviceTypeId,
            "deviceId": alarmsInfo._uniqueDeviceId,
            alarmAttribute,
            alarmType,
        }
        if (actionType === "acknowledge") {
            payload.ackDuration = Date.now() + 600000
            payload.acknowledged = true
        }
        else {
            payload.addressed = true
        }
        let alarmDetails = {
            _uniqueDeviceId: alarmsInfo._uniqueDeviceId,
            alarmType,
            alarmAttribute,
            actionType
        }
        this.setState({
            alarmsInfo
        }, () => this.props.updateAlarmStatus(payload, alarmDetails))
    }

    dateChangeHandler = (event, type) => {
        let dashboardConfigData = cloneDeep(this.state.dashboardConfigData);
        if (type === "duration") {
            clearInterval(interval)
            dashboardConfigData.map(val => { if (val.lineChart.data != "") val.lineChart.data = undefined })
            let currentTime = Date.now();
            this.setState({
                dashboardConfigData,
                dateError: "",
                duration: event.value,
                payload: {
                    from: currentTime - ((event.value * 60) * 1000),
                    to: currentTime,
                }
            }, this.callLineChartDataFunction)
        }
        else {
            let payload = cloneDeep(this.state.payload),
                dateError = "";
            if (event._isValid) {
                // date is valid
                payload[type] = event.value.toDate().getTime()
            } else {
                // date is not valid
                dateError = type
            }
            if ((payload.to - payload.from) > 31536000000) {
                this.setState({
                    isOpen: true,
                    type: "error",
                    message2: "Date range cannot be more than a Year",
                })
                return
            }
            if (payload.from > payload.to || dateError) {
                this.setState({
                    isOpen: true,
                    type: "error",
                    message2: "Invalid Date range",
                    dateError,
                })
                return
            }
            dashboardConfigData.map(val => { if (val.lineChart.data != "") val.lineChart.data = undefined })
            dateError = ""
            this.setState({
                dashboardConfigData,
                payload,
                duration: 0,
                dateError
            }, this.callLineChartDataFunction);
        }
    }

    commandHistoryDateChangeHandler = (event, type) => {
        let commandHistoryPayload = cloneDeep(this.state.commandHistoryPayload)
        let CommandHistoryDateError = ""
        if (type == "duration") {
            commandHistoryPayload[type] = event.value
            commandHistoryPayload.from = Date.now() - ((event.value * 60) * 1000)
            commandHistoryPayload.to = Date.now()
        } else {
            CommandHistoryDateError = type
            commandHistoryPayload[type] = event._isValid ? event.value.toDate().getTime() : commandHistoryPayload[type]
            if (event._isValid && event.toDate().getTime() != undefined && commandHistoryPayload.from < commandHistoryPayload.to && (commandHistoryPayload.to - commandHistoryPayload.from) <= 31536000000) {
                CommandHistoryDateError = ""
                commandHistoryPayload.duration = 0;
            }
        }
        this.setState({
            commandHistoryPayload,
            CommandHistoryDateError,
            isCommandHistoryLoader: CommandHistoryDateError == ""
        }, () => {
            if (CommandHistoryDateError == "")
                this.props.getCommandHistory(commandHistoryPayload)
        })
    }

    rpcCommandHandler = (rpc) => {
        let rpcPayload = {
            commandName: rpc.controlName,
            deviceId: this.props.match.params.deviceId,
            deviceTypeId: this.props.match.params.deviceTypeId,
            payload: rpc.controlCommand
        }
        this.setState({ isDisableDeviceCommandLoader: true, deviceCommandinProgress: rpc.controlName }, () => this.props.rpcCommandPublish(rpcPayload))
    }

    commandPayloadModal = (payload) => {
        let commandPayload = cloneDeep(this.state.commandPayload)
        commandPayload = payload
        this.setState({ isCommandPayload: true, commandPayload })
    }

    copyToClipboard = (id) => {
        navigator.clipboard.writeText(id)
        this.setState({
            type: "success",
            isOpen: true,
            message2: 'Copied to Clipboard successfully !',
        })
    }

    getSelectedTabData = (isForRefresh) => {
        let deviceId = this.props.match.params.deviceId;
        let connectorId = this.props.match.params.deviceTypeId;
        let currentActiveTab = this.state.currentTab;
        if (interval) {
            clearInterval(interval)
        }
        this.props.getDeviceInfo(connectorId, deviceId);

        if (currentActiveTab === "deviceOverview") {
            this.setState({
                deviceInfoLoader: true,
                deviceInfo: {},
                deviceDetailsLoader: true,
                deviceDetails: {},
                weatherInfoLoader: true,
                weatherInfo: {},
                deviceTopologyLoader: true,
                deviceTopology: [],
                deviceCommandLoader: true,
                rpcData: [],
                attributesList: [],
                attributeSuccess: false,
                historicViewModal: false,
                deviceTopologyError: '',
                insightsAttributesStatus: "loading",
            }, () => {
                this.props.getDeviceTopology(connectorId, deviceId);
                this.props.getDeviceDetails(connectorId, deviceId);
                this.props.getWeatherInfo(connectorId, deviceId);
                this.props.getAllRpc(connectorId);
                this.props.getAttributes(connectorId);
            })
        }
        else if (currentActiveTab === "monitoring") {
            this.setState({
                deviceMonitoringLoader: true,
                dashboardConfigData: [],
                floatingButton: false,
                configureModal: false,
                chartCustomConfig: {},
                attributesList: [],
                attributeSuccess: false,
                dateError: "",
                duration: 60,
                payload: {
                    from: Date.now() - 3600000,
                    to: Date.now(),
                },
                insightsAttributesStatus: "loading",
            }, () => {
                // this.chartUpdate = setInterval(this.updateChartData,15000);
                this.props.getAttributes(connectorId);
                this.props.dashboardConfig(connectorId);
            })
        }
        else if (currentActiveTab === "insights") {
            if (isForRefresh) {
                this.insightsTableDurationChangeHandler(this.state.insightsDuration)
            } else if (this.state.insightsAttributesStatus !== "loading") {
                let insightsAttributesStatus = ["error", undefined].includes(this.state.insightsAttributesStatus) ? "loading" : this.state.insightsAttributesStatus;
                let loadInsightsTableData = this.state.attributesList.length && !this.state.insightsTableData;
                let insightsTableState = loadInsightsTableData ? "loading" : this.state.insightsTableState;
                let insightsGraphState = this.state.insightsGraphState;
                let insightsGraphAttributes = this.state.insightsGraphAttributes;
                if (this.state.attributesList.length && (!insightsGraphAttributes || !insightsGraphAttributes.length)) {
                    insightsGraphState = "loading";
                    let { attribute:value, displayName:label} = this.state.attributesList[0];
                    insightsGraphAttributes = [{label, value, attribute: value}];
                }
                this.setState({
                    insightsAttributesStatus,
                    insightsTableState,
                    insightsGraphState,
                    insightsGraphAttributes,
                }, () => {
                    if (insightsAttributesStatus === "loading") {
                        this.props.getAttributes(connectorId);
                        return;
                    }

                    if (insightsTableState === "loading") {
                        this.getInsightsData(this.state.attributesList, "ALL")
                    }

                    if (insightsGraphState === "loading") {
                        this.getInsightsData(this.state.insightsGraphAttributes, "insightsGraph")
                    }
                })
            }
        }
        else if (currentActiveTab === "commandHistory") {
            let currentTime = Date.now();
            this.setState({
                isCommandHistoryLoader: true,
                commandHistoryPayload: {
                    from: currentTime - 259200000,
                    to: currentTime,
                    duration: 4320,
                    deviceTypeId: connectorId,
                    uniqueId: deviceId
                },
                CommandHistoryDateError: "",
                commandHistoryData: [],
                isCommandPayload: false,
                commandPayload: ""
            }, () => {
                this.props.getCommandHistory(this.state.commandHistoryPayload)
            });
        }
        else if (currentActiveTab === "deviceAlarms") {
            let payload = {
                deviceTypeIds: [connectorId],
                uniqueDeviceId: deviceId,
            }
            this.setState({
                alarmsInfoLoader: true,
                alarmsInfo: {}
            }, () => {
                this.props.getAlarmsInfo(payload);
            });
        }
    }

    getInsightsData = (attributesList, type) => {
        let allAttributes = getAttributeNamesFromArray(attributesList)
        let payload = {
            deviceId: this.props.match.params.deviceId,
            deviceTypeId: this.props.match.params.deviceTypeId,
            attrCsv: allAttributes,
            operation: null,
            duration: this.state.insightsDuration,
        }
        this.props.getLineMonitoringchartData(type, payload);
    }

    getDurationBlock = (duration, from, to, error, changeHandler) => {
        return <div className="d-flex">
            <div className="flex-33 pd-r-10">
                <div className="form-group input-group">
                    <div className="input-group-prepend">
                        <span className="input-group-text">Duration :</span>
                    </div>
                    <div className="form-control p-0 border-none bg-transparent">
                        <ReactSelect
                            className="form-control-multi-select"
                            value={DURATION_OPTIONS.find(el => el.value == duration)}
                            onChange={(e) => changeHandler(e, "duration")}
                            options={DURATION_OPTIONS}
                        >
                        </ReactSelect>
                    </div>
                </div>
            </div>
            <div className="flex-33 pd-r-5 pd-l-5">
                <div className="form-group input-group">
                    <div className="input-group-prepend">
                        <span className="input-group-text">From :</span>
                    </div>
                    <Datetime
                        className={`${error === "from" ? "invalid-date" : ""}`}
                        closeOnSelect={true}
                        value={from}
                        isValidDate={(currentDate) => {
                            return new Date(currentDate._d).getTime() <= new Date().getTime();
                        }}
                        onChange={(e) => changeHandler(e, "from")}
                        displayTimeZone={this.props.timeZone}
                    />
                </div>
            </div>
            <div className="flex-33 pd-l-10">
                <div className="form-group input-group">
                    <div className="input-group-prepend">
                        <span className="input-group-text">To :</span>
                    </div>
                    <Datetime
                        className={`${error === "to" ? "invalid-date" : ""}`}
                        closeOnSelect={true}
                        value={to}
                        isValidDate={(currentDate) => {
                            return new Date(currentDate._d).getTime() <= new Date().getTime();
                        }}
                        onChange={(e) => changeHandler(e, "to")}
                        displayTimeZone={this.props.timeZone}
                    />
                </div>
            </div>
        </div>
    }

    getPercentAgeChange = (changedVal, origVal) => {
        return Math.abs(((Number(changedVal.toFixed(2)) - Number(origVal.toFixed(2))) / Number(origVal.toFixed(2)) * 100).toFixed(2));
    }

    getStatementForAlarm = (alarmValue, alarmLow, alarmHigh, unit, operator) => {
        let rateOfChange;
        let thresholdText;
        if (typeof operator != 'undefined' && operator === 'GREATER_THAN') {
            rateOfChange = alarmLow === 0 ? alarmValue : this.getPercentAgeChange(alarmValue, alarmLow);
            thresholdText = ' % ' + ' more than ' + alarmLow.toFixed(2) + ' ' + unit;
        } else {
            rateOfChange = alarmHigh === 0 ? alarmValue : this.getPercentAgeChange(alarmValue, alarmHigh);
            thresholdText = ' % ' + ' less than ' + alarmHigh.toFixed(2) + ' ' + unit;
        }
        return rateOfChange + thresholdText;
    }

    historyDataHandler = (name, displayName) => {
        this.setState({
            historicViewModal: true,
            historyChartAttributeName: name,
            historyChartAttributeDisplayName: displayName,
            historyChartLoading: true,
            historyOperationDetails: {
                operation: null,
                bucketSize: this.state.bucketSizeOptions[0]['value'],
                attrData: {
                    operator: ">=",
                    value: 0
                }
            }

        }, () => {
            this.getAttributeHistoryData(60)
        })
    }

    getAlarmConfigHTML = (alarmCriteria) => {
        let uniqueOperator = [...new Set(alarmCriteria.map(item => item.operator))];
        if (uniqueOperator.length === 2) {
            return (
                <ul className="custom-legend-wrapper mt-3">
                    <li>
                        <span className="custom-legend-icon bg-alarm-warning"></span>
                        <h6>Warning <strong>{`( < ${alarmCriteria.filter(alarm => alarm.type === "warning" && alarm.operator === "LESS_THAN")[0].high.toFixed(2)} & > ${alarmCriteria.filter(alarm => alarm.type === "warning" && alarm.operator === "GREATER_THAN")[0].low.toFixed(2)})`}</strong></h6>
                    </li>
                    <li>
                        <span className="custom-legend-icon bg-minor"></span>
                        <h6>Minor <strong>{`( < ${alarmCriteria.filter(alarm => alarm.type === "minor" && alarm.operator === "LESS_THAN")[0].high.toFixed(2)} & > ${alarmCriteria.filter(alarm => alarm.type === "minor" && alarm.operator === "GREATER_THAN")[0].low.toFixed(2)})`}</strong></h6>
                    </li>
                    <li>
                        <span className="custom-legend-icon bg-major"></span>
                        <h6>Major <strong>{`( < ${alarmCriteria.filter(alarm => alarm.type === "major" && alarm.operator === "LESS_THAN")[0].high.toFixed(2)} & > ${alarmCriteria.filter(alarm => alarm.type === "major" && alarm.operator === "GREATER_THAN")[0].low.toFixed(2)})`}</strong></h6>
                    </li>
                    <li>
                        <span className="custom-legend-icon bg-critical"></span>
                        <h6>Critical <strong>{`( < ${alarmCriteria.filter(alarm => alarm.type === "critical" && alarm.operator === "LESS_THAN")[0].high.toFixed(2)} & > ${alarmCriteria.filter(alarm => alarm.type === "critical" && alarm.operator === "GREATER_THAN")[0].low.toFixed(2)})`}</strong></h6>
                    </li>
                </ul>
            )
        } else {
            return (
                <ul className="custom-legend-wrapper mt-3">
                    {alarmCriteria.map((alarm, index) =>
                        <li key={"alarm" + index}>
                            {uniqueOperator[0] === 'LESS_THAN' ?
                                <React.Fragment>
                                    <span className={`custom-legend-icon ${alarmCriteria[alarmCriteria.length - (index + 1)].type === "warning" ? "bg-alarm-warning" : `bg-${alarmCriteria[alarmCriteria.length - (index + 1)].type}`}`}></span>
                                    <h6>
                                        {alarmCriteria[alarmCriteria.length - (index + 1)].type && alarmCriteria[alarmCriteria.length - (index + 1)].type.charAt(0).toUpperCase() + alarmCriteria[alarmCriteria.length - (index + 1)].type.slice(1)}
                                        <strong>{` ( < ${alarmCriteria[alarmCriteria.length - (index + 1)].high.toFixed(2)})`}</strong>
                                    </h6>
                                </React.Fragment> :
                                <React.Fragment>
                                    <span className={`custom-legend-icon ${alarm.type === "warning" ? "bg-alarm-warning" : `bg-${alarm.type}`}`}></span>
                                    <h6>
                                        {alarm.type && alarm.type.charAt(0).toUpperCase() + alarm.type.slice(1)}
                                        <strong>{` ( > ${alarm.low.toFixed(2)})`}</strong>
                                    </h6>
                                </React.Fragment>
                            }
                        </li>
                    )}
                </ul>
            )
        }
    }

    getAttributeHistoryData = (duration) => {
        this.setState({
            historyChartLoading: true,
            historyChartDuration: duration,
            historyChartData: ""
        }, () => {
            let lineObj = {
                deviceId: this.props.match.params.deviceId,
                deviceTypeId: this.props.match.params.deviceTypeId,
                attrCsv: [this.state.historyChartAttributeName].toString(),
                duration: Number(duration),
                operation: null
            }
            this.props.getLineMonitoringchartData("attrHistoricChart", lineObj);
        })
    }

    cancelClicked = () => {
        this.setState({
            confirmState: false,
            configActionIndexToBeDeleted: ""
        })
    }

    intervalCreator = () => {
        interval = setInterval(() => {
            this.state.dashboardConfigData.map((val, index) => {
                if (val.attributes.length) {
                    let obj = {
                        uniqueId: this.props.match.params.deviceId,
                        connectorId: this.props.match.params.deviceTypeId,
                        minVal: Number(val.gaugeChart.min),
                        maxVal: Number(val.gaugeChart.max),
                        attrName: val.attributes.toString()
                    }
                    this.props.getMonitoringchartData(val.id, "gaugeChart", obj);

                    obj = {
                        deviceId: this.props.match.params.deviceId,
                        deviceTypeId: this.props.match.params.deviceTypeId,
                        attrCsv: val.attributes.toString(),
                        operation: null
                    }
                    if (this.state.duration === 0) {
                        obj.from = this.state.payload.from
                        obj.to = this.state.payload.to
                    } else
                        obj.duration = Number(this.state.duration)
                    this.props.getLineMonitoringchartData(val.id, obj);
                }
            })
        }, this.state.intervalTime.value)
    }

    changeInInterval = (valueObject) => {
        clearInterval(interval);
        this.setState({
            intervalTime: valueObject
        }, () => this.intervalCreator())
    }

    autoRefreshHandler = () => {
        this.setState((prevState) => {
            return { setIntervalTime: !prevState.setIntervalTime }
        }, () => {
            if (this.state.setIntervalTime) {
                this.intervalCreator()
            } else {
                clearInterval(interval)
            }
        })
    }

    historyOperationChangeHandler = (event, type) => {
        let historyOperationDetails = cloneDeep(this.state.historyOperationDetails)
        let historyChartData = cloneDeep(this.state.historyChartData)
        historyChartData.operationData = null;
        if (type == "attrData")
            historyOperationDetails[type][event.target.id] = event.target.value
        else
            historyOperationDetails[type] = (type == "operation" || type == "bucketSize") ? event.value : event.target.value
        this.setState({
            historyChartLoading: true,
            historyChartData,
            historyOperationDetails
        }, () => {
            let lineObj = {
                deviceId: this.props.match.params.deviceId,
                deviceTypeId: this.props.match.params.deviceTypeId,
                attrCsv: [this.state.historyChartAttributeName].toString(),
                duration: Number(this.state.historyChartDuration),
                operation: null
            }
            if (this.state.historyOperationDetails.operation && this.state.historyOperationDetails.operation != "NONE") {
                let obj1 = cloneDeep(lineObj);
                obj1.operation = this.state.historyOperationDetails.operation
                if (this.state.historyOperationDetails.operation === "rateOfChange") {
                    obj1.attrData = {
                        [obj1.attrCsv]: this.state.historyOperationDetails.attrData
                    }
                }
                if (this.state.historyOperationDetails.operation && this.state.historyOperationDetails.operation !== "rateOfChange") {
                    obj1.bucketSize = Number(this.state.historyOperationDetails.bucketSize)
                }
                this.props.getLineMonitoringchartData("attrHistoricChart", obj1)
            } else {
                this.props.getLineMonitoringchartData("attrHistoricChart", lineObj);
            }
        })
    }

    operationChangeHandler = (event, type, index) => {
        clearInterval(interval);
        let dashboardConfigData = produce(this.state.dashboardConfigData, dashboardConfigData => {
            let requiredDashboard = dashboardConfigData[index];
            if (type === "operation" && event.value === null) {
                requiredDashboard.lineChart.data = null;
            }
            requiredDashboard.lineChart.operationData = null;


            if (type == "attrData") {
                dashboardConfigData[index]['operationDetails'][type][event.target.id] = event.target.value
            } else {
                dashboardConfigData[index]['operationDetails'][type] = (type == "operation" || type == "bucketSize") ? event.value : event.target.value
            }
        });

        this.setState({
            dashboardConfigData,
        }, () => {
            let obj = {
                deviceId: this.props.match.params.deviceId,
                deviceTypeId: this.props.match.params.deviceTypeId,
                attrCsv: this.state.dashboardConfigData[index].attributes.toString(),
                operation: dashboardConfigData[index]['operationDetails'].operation,
            }
            if (dashboardConfigData[index]['operationDetails'].operation && dashboardConfigData[index]['operationDetails'].operation === "rateOfChange") {
                obj.attrData = {
                    [obj.attrCsv]: dashboardConfigData[index]['operationDetails'].attrData
                }
            } else if (dashboardConfigData[index]['operationDetails'].operation && dashboardConfigData[index]['operationDetails'].operation != "rateOfChange") {
                obj.bucketSize = Number(dashboardConfigData[index]['operationDetails'].bucketSize)
            }

            if (this.state.duration === 0) {
                obj.from = this.state.payload.from
                obj.to = this.state.payload.to
            } else
                obj.duration = Number(this.state.duration)
            this.props.getLineMonitoringchartData(this.state.dashboardConfigData[index].id, obj)
        })
    }

    getColumns = () => {
        return [
            {
                Header: "Command Name", width: 25,
                cell: (row) => (
                    <h6 className="text-theme fw-600" >{row.commandName}</h6>
                )
            },
            {
                Header: "Triggered Type", width: 15,
                cell: (row) => (
                    <h6>{row.triggerType.toUpperCase()}</h6>
                )
            },
            {
                Header: "Triggered By", width: 20,
                cell: (row) => (
                    <h6>{row.triggeredBy}</h6>
                )
            },
            {
                Header: "Triggered At", width: 15,
                cell: (row) => (
                    <h6>{convertTimestampToDate(row.timestamp)}</h6>
                )
            },
            {
                Header: "Status", width: 15,
                cell: (row) => (
                    <h6 className={`text-${row.status ? "green" : "red"}`}>{row.status ? "Success" : "Error"}</h6>
                )
            }, {
                Header: "Payload", width: 10,
                cell: (row) => (
                    <button className="btn-transparent btn-transparent-blue" onClick={() => this.commandPayloadModal(row.payload)}><i className="far fa-brackets-curly"></i></button>
                )
            }
        ]
    }

    getStatusBarClass = (row) => {
        if (row.status)
            return { className: "green", tooltipText: "Success" }
        return { className: "red", tooltipText: "Failed" }
    }

    saveCommandsHandler = (COMMAND_OBJ) => {
        let payload = {
            controlCommand: JSON.stringify(COMMAND_OBJ.controlCommand),
            controlName: COMMAND_OBJ.controlName.trim(),
            deviceTypeId: this.props.match.params.deviceTypeId,
            id: COMMAND_OBJ.id
        }
        this.setState({
            commandModalLoader: true,
            COMMAND_OBJ
        }, () => {
            this.props.saveCommands(payload, this.state.isAddMode)
        })
    }

    closeCommandModal = () => {
        let COMMAND_OBJ = cloneDeep(this.state.COMMAND_OBJ)
        COMMAND_OBJ = {
            controlName: "",
            controlCommand: {},
            deviceTypeId: this.props.match.params.id
        }
        this.setState({
            activeCommandIndex: -1,
            COMMAND_OBJ,
            addCommandModal: false
        })
    }

    zoomChartHandler = (index, type, zoomedChartOperation = {}) => {
        this.setState({
            showZoomModel: true,
            zoomIndex: index,
            zoomType: type,
            zoomedChartOperation,
        })
    }

    getInsightsTableHTML = () => {
        return <div className="debug-table-wrapper">
            <div className="ag-theme-alpine">
                <AgGridReact
                    columnDefs={this.getColumnsForInsightsTable()}
                    rowData={this.state.insightsTableData.chartData}
                    animateRows={true}
                    pagination={true}
                    paginationPageSize={10}
                >
                </AgGridReact>
            </div>
        </div>
    }


    insightsTableDurationChangeHandler = (insightsDuration) => {
        if (this.state.attributeSuccess) {
            let insightsGraphState = this.state.insightsGraphAttributes && this.state.insightsGraphAttributes.length ? "loading" : "noAttributesSelected"
            this.setState({
                insightsDuration,
                insightsTableState: "loading",
                insightsGraphState,
            }, () => {
                this.getInsightsData(this.state.attributesList, "ALL");
                insightsGraphState === "loading" && this.getInsightsData(this.state.insightsGraphAttributes, "insightsGraph");
            })
        }
    }

    getInsightsHTML = () => {
        let { insightsGraphState, insightsTableState } = this.state;
        if (!this.state.insightsGraphAttributes || !this.state.insightsGraphAttributes.length) {
            insightsGraphState = "noAttributesSelected";
        }

        return <div className="insight-chart-group">
            <div className="card">
                <div className="card-header">
                    <h6>All Attributes Data</h6>
                    <div className="card-header-filter">
                        <label className="form-group-label d-inline-block mr-2">Duration :</label>
                        <ReactSelect
                            className="form-control-multi-select d-inline-block"
                            value={DURATION_OPTIONS.find(el => el.value == this.state.insightsDuration)}
                            onChange={(val) => this.insightsTableDurationChangeHandler(val.value)}
                            options={DURATION_OPTIONS}
                            isDisabled={this.state.insightsTableState === "loading"}
                        >
                        </ReactSelect>
                    </div>
                    {/*<div className="button-group">
                        <button className="btn-transparent btn-transparent-green" data-tooltip="true" data-tooltip-text="Add" data-tooltip-place="bottom" data-toggle="modal" data-target="#insightModal">
                            <i className="far fa-plus"></i>
                        </button>
                    </div>*/}
                </div>
                <div className="card-body">
                    {insightsTableState === "loading" && <div className="card-message">{getMiniLoader()}</div>}
                    {insightsTableState === "loaded" && this.getInsightsTableHTML()}
                    {insightsTableState === "error" && <div className="card-message"><p className="text-danger">Oops! we are unable to fetch attributes.</p></div>}
                    {insightsTableState === "emptyData" && <div className="card-message"><p>There is no data to display.</p></div>}
                </div>
            </div>

            <div className="card">
                <div className="card-body">
                    <div className="form-group">
                        <ReactSelect
                            className="form-control-multi-select"
                            value={this.state.insightsGraphAttributes}
                            onChange={this.insightsGraphAttributesChangeHandler}
                            options={this.state.attributesList.map(attr => ({ label: attr.displayName, value: attr.attribute, attribute: attr.attribute }))}
                            isMulti={true}
                            placeholder="Select Attribute(s)"
                            isDisabled={this.state.insightsGraphState === "loading"}
                        />
                    </div>
                    {insightsGraphState === "loading" && <div className="card-message">{getMiniLoader()}</div>}
                    {insightsGraphState === "loaded" && <InsightsLineGraph data={this.state.insightsGraphData} timeZone={this.props.timeZone} />}
                    {insightsGraphState === "error" && <div className="card-message"> <p className="text-danger">Oops! we are unable to fetch attributes.</p></div>}
                    {insightsGraphState === "emptyData" && <div className="card-message"><p>There is no data to display</p></div>}
                    {insightsGraphState === "noAttributesSelected" && <div className="card-message"><p>Please select attributes to view Graph.</p></div>}
                </div>
            </div>
        </div>
    }

    insightsGraphAttributesChangeHandler = (insightsGraphAttributes) => {
        if(insightsGraphAttributes && insightsGraphAttributes.length > 5){
            this.props.showNotification("error", "Cannot select more than 5 attributes at once.");
            return;
        }
        let insightsGraphState = insightsGraphAttributes ? "loading" : "noAttributesSelected";
        this.setState({
            insightsGraphAttributes: insightsGraphAttributes || [],
            insightsGraphState,
        }, () => {
            insightsGraphState === "loading" && this.getInsightsData(insightsGraphAttributes, "insightsGraph")
        })
    }

    getHeaderNameForInsightsTable = ({ attrDisplayName, unit }) => {
        return `${attrDisplayName} ${unit ? `(${unit})` : ""}`
    }

    getColumnsForInsightsTable = () => {
        let timestampColumn = {
            headerName: 'Time',
            field: 'category',
            filter: 'agTextColumnFilter',
            valueFormatter: params => {
                return convertTimestampToDate(params.data.category);
            }
        };
        let otherColumns = this.state.insightsTableData.availableAttributes.map(attr => ({ headerName: this.getHeaderNameForInsightsTable(attr), field: attr.attr }));
        return [timestampColumn, ...otherColumns];
    }

    render() {
        let { deviceDetails, deviceInfo, deviceTopology, weatherInfo, alarmsInfo } = this.state;
        let isRefreshBtnDisabled = alarmsInfo.alarms && Object.values(alarmsInfo.alarms).find(alarmByType => alarmByType.find(alarm => alarm.loaderType));
        return (
            <React.Fragment>
                <Helmet>
                    <title>{CURRENT_PAGE_TITLE[this.state.currentPathName]}</title>
                    <meta name="description" content="Description of M83-Device Details" />
                </Helmet>

                <header className="content-header d-flex">
                    <div className="flex-50">
                        <div className="d-flex">
                            <h6 className="previous" onClick={() => this.props.history.push(PARENT_PAGE_ROUTE[this.state.currentPathName])}>{PARENT_PAGE_TITLE[this.state.currentPathName]}</h6>
                            <h6 className="active">{deviceInfo.deviceName ? deviceInfo.deviceName : deviceInfo.id && ("..." + deviceInfo.id.substring(deviceInfo.id.length - 6))}</h6>
                        </div>
                    </div>
                    <div className="flex-50 text-right">
                        <div className="content-header-group">
                            {this.state.currentTab === "monitoring" &&
                                <div className="toggle-switch-wrapper mr-3">
                                    <span>Auto Refresh</span>
                                    <label className="toggle-switch">
                                        <input type="checkbox" onChange={this.autoRefreshHandler} checked={this.state.setIntervalTime} id="isAuthenticated" />
                                        <span className="toggle-switch-slider"></span>
                                    </label>
                                    <span>Every</span>
                                </div>
                            }
                            {this.state.currentTab === "monitoring" &&
                                <ReactSelect
                                    className="form-control-multi-select mr-3"
                                    value={this.state.intervalTime}
                                    onChange={this.changeInInterval}
                                    options={[{ value: 15000, label: '15 secs' },
                                    { value: 30000, label: '30 secs' },
                                    { value: 60000, label: '1 min' }]}
                                    isDisabled={!this.state.setIntervalTime}
                                >
                                </ReactSelect>
                            }
                            <button className="btn btn-light" data-tooltip data-tooltip-text="Refresh" data-tooltip-place="bottom" disabled={isRefreshBtnDisabled} onClick={() => this.getSelectedTabData("Refresh")}><i className="far fa-sync-alt"></i></button>
                            <button className="btn btn-light" data-tooltip data-tooltip-text="Back" data-tooltip-place="bottom" onClick={() => this.props.history.push(PARENT_PAGE_ROUTE[this.state.currentPathName])}><i className="fas fa-angle-left"></i></button>
                            {this.state.currentTab === "monitoring" &&
                                <button disabled={this.state.dashboardConfigData && this.state.dashboardConfigData.length >= this.state.attributesList.length} className="btn btn-primary" data-tooltip data-tooltip-text="Add Data Point" data-tooltip-place="bottom" onClick={() => this.dropCharts()}><i className="far fa-plus"></i></button>
                            }
                        </div>
                    </div>
                </header>

                <div className="content-body" id="contentBody">
                    <ul className="nav nav-tabs device-detail-tabs">
                        <li className="nav-item flex-20">
                            <a className={`nav-link ${this.state.currentTab === "deviceOverview" ? "active" : ""}`} onClick={() => {
                                clearInterval(interval)
                                localStorage.setItem("currentTab", 'deviceOverview');
                                this.setState({ currentTab: 'deviceOverview' }, () => {
                                    this.getSelectedTabData();
                                })
                            }}>
                                Overview
                            </a>
                        </li>
                        <li className="nav-item flex-20">
                            <a className={`nav-link ${this.state.currentTab === "insights" ? "active" : ""}`} onClick={() => {
                                clearInterval(interval)
                                localStorage.setItem("currentTab", 'insights');
                                this.setState({ currentTab: 'insights' }, () => {
                                    this.getSelectedTabData();
                                })
                            }}>
                                Insights
                            </a>
                        </li>
                        <li className="nav-item flex-20">
                            <a className={`nav-link ${this.state.currentTab === "monitoring" ? "active" : ""}`} onClick={() => {
                                clearInterval(interval)
                                localStorage.setItem("currentTab", 'monitoring');
                                this.setState({ currentTab: 'monitoring' }, () => {
                                    this.getSelectedTabData();
                                })
                            }}>
                                Monitoring
                            </a>
                        </li>
                        <li className="nav-item flex-20">
                            <a className={`nav-link ${this.state.currentTab === "commandHistory" ? "active" : ""}`} onClick={() => {
                                clearInterval(interval)
                                localStorage.setItem("currentTab", 'commandHistory');
                                this.setState({ currentTab: 'commandHistory' }, () => {
                                    this.getSelectedTabData();
                                })
                            }}>
                                Control history
                            </a>
                        </li>
                        <li className="nav-item flex-20">
                            <a className={`nav-link ${this.state.currentTab === "deviceAlarms" ? "active" : ""}`} onClick={() => {
                                clearInterval(interval)
                                localStorage.setItem("currentTab", 'deviceAlarms');
                                this.setState({ currentTab: 'deviceAlarms' }, () => {
                                    this.getSelectedTabData();
                                })
                            }}>
                                Alarms
                            </a>
                        </li>
                    </ul>

                    <div className="tab-content device-detail-tab-content">
                        {this.state.currentTab === "deviceOverview" ?
                            <div className="tab-pane fade show active">
                                <div className="d-flex">
                                    <div className="flex-40 pd-r-7">
                                        <div className="card">
                                            <div className="card-body">
                                                {this.state.deviceInfoLoader ? this.getMiniLoader() : this.state.deviceInfoError ? this.getErrorMessage(this.state.deviceInfoError) :
                                                    <div className="device-info-box">
                                                        <div className="device-info-status">
                                                            {deviceInfo.status == "not-connected" ?
                                                                <h6><i className="far fa-unlink mr-2"></i>Not connected
                                                                </h6> :
                                                                <h6>Last Reported :
                                                                    <span
                                                                        className={`ml-2 ${deviceInfo.status == "online" ? "text-green" : "text-red"}`}>{getTimeDifference(deviceInfo.lastReported)}</span>
                                                                </h6>
                                                            }
                                                            <span className="ml-2 mr-2 f-11 text-content">|</span>
                                                            <button className="btn btn-link p-0" data-tooltip
                                                                data-tooltip-text="Debug" data-tooltip-place="bottom"
                                                                onClick={() => isNavAssigned('deviceDiagnostics') && window.open(`/deviceDiagnostics/${this.props.match.params.deviceTypeId}/${deviceInfo.id}`, '_blank')}>
                                                                <i className="far fa-bug"></i>Debug
                                                            </button>
                                                        </div>
                                                        <div className="device-detail-image">
                                                            <img
                                                                src={deviceInfo.image ? deviceInfo.image : "https://content.iot83.com/m83/misc/device-type-fallback.png"} />
                                                            <span
                                                                className={`device-detail-status ${deviceInfo.status == "online" ? "bg-green" : deviceInfo.status == "offline" ? "bg-red" : "bg-gray"}`}></span>
                                                        </div>
                                                        {deviceInfo.deviceName ?
                                                            <h5>{deviceInfo.deviceName}
                                                                <button
                                                                    className="btn-transparent btn-transparent-green copy-button pt-1"
                                                                    data-tooltip data-tooltip-text="Copy"
                                                                    data-tooltip-place="bottom"
                                                                    onClick={() => this.copyToClipboard(`${deviceInfo.id}`)}>
                                                                    <i className="far fa-copy f-14"></i>
                                                                </button>
                                                                ({deviceInfo.id})
                                                            </h5> :
                                                            <h5>
                                                                <button
                                                                    className="btn-transparent btn-transparent-green copy-button"
                                                                    data-tooltip data-tooltip-text="Copy"
                                                                    data-tooltip-place="bottom"
                                                                    onClick={() => this.copyToClipboard(`${deviceInfo.id}`)}>
                                                                    <i className="far fa-copy"></i>
                                                                </button>
                                                                {deviceInfo.id}
                                                            </h5>
                                                        }
                                                        <p><strong><i className="fad fa-map-marker-alt"></i>Location :
                                                        </strong><a href="#" onClick={() => {
                                                                deviceInfo.deviceLocation && window.open(`https://www.google.com/maps/place/${deviceInfo.lat},${deviceInfo.lng}`)
                                                            }}>{deviceInfo.deviceLocation || "-"}</a></p>
                                                        <p><strong><i className="fad fa-desktop"></i>Device Type : </strong><span
                                                            onClick={() => isNavAssigned("deviceTypes") && this.props.history.push(`/addOrEditDeviceType/${this.props.match.params.deviceTypeId}`)}>{deviceInfo.deviceTypeName}</span>
                                                        </p>
                                                        <p><strong><i className="fad fa-layer-group"></i>Group :
                                                        </strong><span
                                                                onClick={() => isNavAssigned('deviceGroups') && this.props.history.push({
                                                                    pathname: `/deviceGroups/${this.props.match.params.deviceTypeId}/${deviceInfo.deviceGroupId}`,
                                                                    state: { childViewType: 'GROUP_DETAILS' }
                                                                })}>{deviceInfo.deviceGroupName}</span></p>
                                                        <p><strong><i className="fad fa-tags"></i>Tag : </strong><span
                                                            style={{ color: deviceInfo.tagColor }}
                                                            onClick={() => isNavAssigned('deviceGroups') && this.props.history.push({
                                                                pathname: `/deviceGroups/${this.props.match.params.deviceTypeId}/${deviceInfo.deviceGroupId}`,
                                                                state: { childViewType: 'GROUP_TAGVIEW' }
                                                            })}>{deviceInfo.tag || "-"}</span></p>
                                                        <p><strong><i className="fad fa-hourglass-half"></i>Reporting
                                                            interval : </strong>{deviceInfo.reportInterval} secs</p>
                                                        <p><strong><i className="fad fa-clock"></i>Last Reported :
                                                        </strong>{convertTimestampToDate(deviceInfo.lastReported) || "N/A"}
                                                        </p>
                                                    </div>
                                                }
                                            </div>
                                        </div>

                                        <div className="card">
                                            <div className="card-header"><h6>Device Details</h6></div>
                                            <div className="card-body">
                                                <div className="device-detail-box">
                                                    {this.state.deviceDetailsLoader ? this.getMiniLoader() : this.state.deviceDetailsError ? this.getErrorMessage(this.state.deviceDetailsError) :
                                                        <React.Fragment>
                                                            <div className="content-table device-detail-box-inner">
                                                                <table className="table table-bordered">
                                                                    <thead>
                                                                        <tr>
                                                                            <th width="10%"></th>
                                                                            <th width="40%">Attribute</th>
                                                                            <th width="40%">Value</th>
                                                                            <th width="10%"></th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        {deviceDetails.map(({ displayName, value, unit, name, alarmType }) =>
                                                                            <tr>
                                                                                <td className="f-15 text-center">
                                                                                    {typeof value === 'number' &&
                                                                                        <i className={ALARM_TYPE[alarmType]}
                                                                                            data-tooltip
                                                                                            data-tooltip-text={alarmType == "critical" ? "Critical" : alarmType == "major" ? "Major" : alarmType == "minor" ? "Minor" : alarmType == "warning" ? "Warning" : "Normal"}
                                                                                            data-tooltip-place="bottom"></i>}
                                                                                </td>
                                                                                <td className="text-gray">{displayName ? displayName : name}</td>
                                                                                <td className={`text-content ${name == "deviceId" && "text-truncate"}`}>

                                                                                    {typeof value === 'number' ? value.toFixed(2) : typeof value === 'boolean' ? `${value}` : value ? value : 'N/A'} {unit}
                                                                                </td>
                                                                                <td className="text-center">
                                                                                    {name == "deviceId" &&
                                                                                        <button
                                                                                            className="btn-transparent btn-transparent-green copy-button"
                                                                                            data-tooltip data-tooltip-text="Copy"
                                                                                            data-tooltip-place="bottom"
                                                                                            onClick={() => this.copyToClipboard(`${deviceInfo.id}`)}>
                                                                                            <i className="far fa-copy"></i>
                                                                                        </button>
                                                                                    }
                                                                                    {typeof value === 'number' &&
                                                                                        <button
                                                                                            className="btn-transparent btn-transparent-blue"
                                                                                            data-tooltip
                                                                                            data-tooltip-text="View Historical Data"
                                                                                            data-tooltip-place="bottom"
                                                                                            onClick={() => this.historyDataHandler(name, displayName)}>
                                                                                            <i className="far fa-history"></i>
                                                                                        </button>
                                                                                    }
                                                                                </td>
                                                                            </tr>
                                                                        )}
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                            <ul className="custom-legend-wrapper">
                                                                <li><span
                                                                    className="custom-legend-icon bg-green-legend"></span>
                                                                    <h6>Normal</h6></li>
                                                                <li><span
                                                                    className="custom-legend-icon bg-alarm-warning"></span>
                                                                    <h6>Warning</h6></li>
                                                                <li><span className="custom-legend-icon bg-minor"></span>
                                                                    <h6>Minor</h6></li>
                                                                <li><span className="custom-legend-icon bg-major"></span>
                                                                    <h6>Major</h6></li>
                                                                <li><span className="custom-legend-icon bg-critical"></span>
                                                                    <h6>Critical</h6></li>
                                                            </ul>
                                                        </React.Fragment>
                                                    }
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="flex-60 pd-l-7">
                                        <div className="card">
                                            <div className="card-header"><h6>Live Feed <a href="https://openweathermap.org/"
                                                target="_blank">(openweathermap.org)</a>
                                            </h6></div>
                                            <div className="card-body">
                                                <div className="device-weather-wrapper">
                                                    {this.state.weatherInfoLoader ?
                                                        this.getMiniLoader() :
                                                        (this.state.weatherInfoError || !weatherInfo.isDataAvailable) ?
                                                            this.getErrorMessage(!this.state.weatherInfoError && !weatherInfo.isDataAvailable ?
                                                                <React.Fragment>
                                                                    <p>You didn't configure the location for this
                                                                        device.</p>
                                                                    {isNavAssigned("devices") &&
                                                                        <div className="text-center">
                                                                            <button className="btn btn-link"
                                                                                onClick={() => this.props.history.push(`/devices/${this.props.match.params.deviceTypeId}`)}>
                                                                                <i className="far fa-plus"></i>Add Location
                                                                        </button>
                                                                        </div>
                                                                    }
                                                                </React.Fragment> : this.state.weatherInfoError) :
                                                            <WeatherWidget data={weatherInfo.weatherData}
                                                                setSelectedDay={this.setSelectedDay} />
                                                    }
                                                </div>
                                            </div>
                                        </div>

                                        <div className="card">
                                            <div className="card-header">
                                                <h6>Device Controls</h6>
                                            </div>
                                            <div className="card-body">
                                                <div className="device-command-wrapper">
                                                    <ul className="list-style-none d-flex">
                                                        {this.state.deviceCommandLoader ?
                                                            <li className="flex-100">{this.getMiniLoader()}</li> :
                                                            <React.Fragment>
                                                                {this.state.rpcData.length ?
                                                                    this.state.rpcData.map((rpc, index) => {
                                                                        return (
                                                                            <li className="flex-20" key={"rpc" + index}>
                                                                                {this.state.deviceCommandinProgress != "" && this.state.deviceCommandinProgress === rpc.controlName ?
                                                                                    <button type="button"
                                                                                        className="btn btn-success">
                                                                                        <i className="far fa-cog mr-r-5 fa-spin"></i>Executing
                                                                                        </button> :
                                                                                    <button
                                                                                        className="btn btn-primary device-command-wrapper-button"
                                                                                        key={index}
                                                                                        onClick={() => this.rpcCommandHandler(rpc)}
                                                                                        disabled={(!(deviceInfo.status == "online") || this.state.isDisableDeviceCommandLoader)}>
                                                                                        <span><img
                                                                                            src={rpc.imageUrl || "https://content.iot83.com/m83/misc/device-type-fallback.png"} /></span>
                                                                                        {rpc.controlName}
                                                                                    </button>
                                                                                }
                                                                            </li>
                                                                        )
                                                                    }
                                                                    ) :
                                                                    <li className="flex-100">
                                                                        <div className="card-message">
                                                                            <div>
                                                                                <p>No Control(s) defined for this Device
                                                                                    Type</p>
                                                                                <button className="btn btn-link"
                                                                                    onClick={() => this.setState({
                                                                                        addCommandModal: true,
                                                                                        COMMAND_OBJ: {
                                                                                            controlName: "",
                                                                                            controlCommand: {}
                                                                                        },
                                                                                        isAddMode: true
                                                                                    })}><i className="far fa-plus"></i>Add
                                                                                    Control
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                }
                                                            </React.Fragment>
                                                        }
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="card">
                                            <div className="card-header">
                                                <h6>Device Topology</h6>
                                            </div>
                                            <div className="card-body topology-wrapper">
                                                {this.state.deviceTopologyLoader ?
                                                    this.getMiniLoader() : this.state.deviceTopologyError ?
                                                        <div className="card-message">
                                                            <div>
                                                                <p>No topology is defined for this Device Type</p>
                                                                {isNavAssigned("deviceTypes") &&
                                                                    <button className="btn btn-link"
                                                                        onClick={() => this.props.history.push(`/addOrEditDeviceType/${this.props.match.params.deviceTypeId}`)}>
                                                                        <i className="far fa-plus"></i>Add Component</button>}
                                                            </div>
                                                        </div>
                                                        :
                                                        <ul className="topology-content-list d-flex">
                                                            {deviceTopology.map((item, i) => (
                                                                <li key={i}>
                                                                    <div className="topology-content-item">
                                                                        <div className="topology-image">
                                                                            <img
                                                                                src={item.imagePath || "https://image.flaticon.com/icons/svg/485/485884.svg"} />
                                                                        </div>
                                                                        <p>{item.name}</p>
                                                                        {((i + 1) != deviceTopology.length) &&
                                                                            <div className="topology-connection">
                                                                                <h6>{item.connectedVia}</h6>
                                                                                {item.status == "online" ?
                                                                                    <div className="topology-connection-link">
                                                                                        <img
                                                                                            src="https://content.iot83.com/m83/misc/connecting.gif" />
                                                                                    </div> :
                                                                                    item.status == "offline" ?
                                                                                        <div
                                                                                            className="topology-connection-link">
                                                                                            <span></span>
                                                                                            <i className="fad fa-times-circle text-red"></i>
                                                                                        </div> :
                                                                                        <div
                                                                                            className="topology-connection-link">
                                                                                            <span
                                                                                                className="topology-no-connection"></span>
                                                                                            <i className="fad fa-unlink text-light-gray"></i>
                                                                                        </div>
                                                                                }
                                                                            </div>
                                                                        }
                                                                    </div>
                                                                </li>
                                                            ))}
                                                        </ul>
                                                }
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div> :
                            this.state.currentTab === "monitoring" ?
                                <div className="tab-pane fade show active">
                                    <div className="device-chart-group">
                                        {this.getDurationBlock(this.state.duration, this.state.payload.from, this.state.payload.to, this.state.dateError, this.dateChangeHandler)}
                                        {this.state.deviceMonitoringLoader ?
                                            <div className="device-chart-message">{this.getMiniLoader()}</div> :
                                            this.state.dashboardConfigData.length > 0 ?
                                                this.state.dashboardConfigData.map((val, index) => (
                                                    <div className="card" key={val.id} ref={index === this.state.dashboardConfigData.length - 1 ? this.listRef : null}>
                                                        <div className="card-header">
                                                            <h6 className="pd-r-50">{val.headerName != "" ? val.headerName : "Sample Header"}</h6>
                                                            <div className="button-group">
                                                                <button type="button" className="btn-transparent-blue btn-transparent" data-tooltip data-tooltip-text="Configure" data-tooltip-place="bottom"
                                                                    onClick={() => this.setState({ configureModal: true, chartCustomConfig: val })} disabled={this.state.floatingButton || this.state.dateError != ""}>
                                                                    <i className="far fa-tools"></i>
                                                                </button>
                                                                <button type="button" className="btn-transparent btn-transparent-red" data-tooltip data-tooltip-text="Delete" data-tooltip-place="bottom"
                                                                    onClick={() => this.setState({ confirmState: true, deleteChartIndex: index })}>
                                                                    <i className="far fa-trash-alt"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <div className="card-body">
                                                            <div className="d-flex">
                                                                <div className="flex-15 pd-r-15 border-right">
                                                                    <div className="form-group">
                                                                        <label className="form-group-label">Operation :</label>
                                                                        <ReactSelect
                                                                            className="form-control-multi-select"
                                                                            value={this.state.operations.find(op => op.value === val.operationDetails.operation)}
                                                                            onChange={(e) => this.operationChangeHandler(e, "operation", index)}
                                                                            options={this.state.operations}
                                                                            isMulti={false}
                                                                            isDisabled={val.attributes.length == 0 || this.state.dateError != ""}
                                                                        >
                                                                        </ReactSelect>
                                                                    </div>
                                                                    <div className="form-group">
                                                                        <label className="form-group-label">Bucket Size :</label>
                                                                        <ReactSelect
                                                                            className="form-control-multi-select"
                                                                            value={this.state.bucketSizeOptions.find(option => option.value === val.operationDetails.bucketSize)}
                                                                            onChange={(e) => this.operationChangeHandler(e, "bucketSize", index)}
                                                                            options={this.state.bucketSizeOptions}
                                                                            isMulti={false}
                                                                            isDisabled={val.operationDetails.operation === "NONE" || val.operationDetails.operation === null || val.operationDetails.operation === "rateOfChange" || this.state.dateError != ""}
                                                                        >
                                                                        </ReactSelect>
                                                                    </div>
                                                                    <div className="form-group mb-0">
                                                                        <label className="form-group-label">Operator :</label>
                                                                        <div className="d-inline-block">
                                                                            <div className="input-group">
                                                                                <div className="input-group-prepend">
                                                                                    <span className="input-group-text w-auto">
                                                                                        <select id="operator" className="cursor-pointer" disabled={val.operationDetails.operation != "rateOfChange" || this.state.dateError != ""} value={val.operationDetails.attrData.operator} onChange={(e) => this.operationChangeHandler(e, "attrData", index)}>
                                                                                            <option value=">">{`>`}</option>
                                                                                            <option value=">=">{`>=`}</option>
                                                                                            <option value="<">{`<`}</option>
                                                                                            <option value="<=">{`<=`}</option>
                                                                                            <option value="=">{`=`}</option>
                                                                                        </select>
                                                                                    </span>
                                                                                </div>
                                                                                <input type="number" id="value" className="form-control" disabled={val.operationDetails.operation != "rateOfChange" || this.state.dateError != ""} onChange={(e) => this.operationChangeHandler(e, "attrData", index)} value={val.operationDetails.attrData.value} />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div className="flex-60 pd-l-10 pd-r-10 border-right">
                                                                    <div className="card-chart-box position-relative">
                                                                        {val.lineChart.data === "" && val.lineChart.operationData === "" ?
                                                                            <div className="card-message">
                                                                                <p>There is no data to display.</p>
                                                                            </div> :
                                                                            val.operationDetails.operation && val.operationDetails.operation != "NONE" ?
                                                                                (this.state.attributeSuccess && val.lineChart.data && val.lineChart.operationData ?
                                                                                    val.lineChart.data.chartData.length || (val.lineChart.operationData.chartData && val.lineChart.operationData.chartData.length) ?
                                                                                        <React.Fragment>
                                                                                            <button className="btn btn-light box-top-right-button" onClick={() => this.zoomChartHandler(index, "lineChart", this.state.operations.find(op => op.value === val.operationDetails.operation))}>
                                                                                                <i className="far fa-expand"></i>
                                                                                            </button>
                                                                                            <MonitoringLineChart timeZone={this.props.timeZone} data={val.lineChart} chartId={"lineChart" + val.id} alarmAttributes={this.state.attributesList} operationAttribute={this.state.operations.find(op => op.value === val.operationDetails.operation)} />
                                                                                        </React.Fragment> :
                                                                                        <div className="card-message">
                                                                                            <p>There is no data to display.</p>
                                                                                        </div> :
                                                                                    this.getMiniLoader()
                                                                                ) : (val.lineChart.data && this.state.attributeSuccess ?
                                                                                    val.lineChart.data.chartData.length ?
                                                                                        <React.Fragment>
                                                                                            <button className="btn btn-light box-top-right-button" onClick={() => this.zoomChartHandler(index, "lineChart")}>
                                                                                                <i className="far fa-expand"></i>
                                                                                            </button>
                                                                                            <MonitoringLineChart timeZone={this.props.timeZone} data={val.lineChart} chartId={"lineChart" + val.id} alarmAttributes={this.state.attributesList} operationAttribute={this.state.operations.find(op => op.value === val.operationDetails.operation)} />
                                                                                        </React.Fragment> :
                                                                                        <div className="card-message">
                                                                                            <p>There is no data to display.</p>
                                                                                        </div> :
                                                                                    this.getMiniLoader()
                                                                                )

                                                                        }
                                                                    </div>
                                                                </div>
                                                                <div className="flex-25 pd-l-10">
                                                                    <div className="card-chart-box position-relative pt-3" style={{ height: 250 }}>
                                                                        {(!val.gaugeChart.data || !this.state.attributeSuccess) ?
                                                                            this.getMiniLoader() :
                                                                            val.gaugeChart.data.hasOwnProperty("value") && val.gaugeChart.data.value != null ?
                                                                                <React.Fragment>
                                                                                    <button
                                                                                        className="btn btn-light box-top-right-button"
                                                                                        onClick={() => this.zoomChartHandler(index, "gaugeChart")}
                                                                                    >
                                                                                        <i className="far fa-expand"></i>
                                                                                    </button>
                                                                                    <CreateChart
                                                                                        data={val.gaugeChart}
                                                                                        timeZone={this.props.timeZone}
                                                                                        attributes={this.state.attributesList}
                                                                                        divId={"gaugeChart" + val.id}
                                                                                        type={"gaugeChart"}
                                                                                    />
                                                                                </React.Fragment> :
                                                                                <div className="card-message">
                                                                                    <p>There is no data to display.</p>
                                                                                </div>
                                                                        }
                                                                        {val.gaugeChart.data && val.gaugeChart.data.hasOwnProperty("value") && val.gaugeChart.data.timestamp !== 0 && this.state.attributeSuccess &&
                                                                            <div className="card-chart-box-status">
                                                                                <strong className="text-gray">Last Reported : </strong> <span className={`${deviceInfo.status == "online" ? "text-green" : "text-red"}`}>{getTimeDifference(val.gaugeChart.data.timestamp)}</span>
                                                                            </div>
                                                                        }
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            {val.attributes.length > 0 && this.state.attributesList.find(attr => attr.attribute === val.attributes[0]) && this.state.attributesList.find(attr => attr.attribute === val.attributes[0]).isConfigured &&
                                                                this.getAlarmConfigHTML(this.state.attributesList.find(attr => attr.attribute === val.attributes[0]).alarmCriteria)
                                                            }
                                                        </div>
                                                    </div>
                                                )) :
                                                <div className="device-chart-message">
                                                    <AddNewButton
                                                        text1="No monitoring data available"
                                                        text2="You haven't added any data point(s) yet. Please add a data point first."
                                                        imageIcon="addPage.png"
                                                        addButtonEnable={false}
                                                    />
                                                </div>
                                        }
                                    </div>
                                </div> :
                                this.state.currentTab === "commandHistory" ?
                                    <div className="tab-pane fade show active">
                                        <div className="device-chart-group">
                                            {this.getDurationBlock(this.state.commandHistoryPayload.duration, this.state.commandHistoryPayload.from, this.state.commandHistoryPayload.to, this.state.CommandHistoryDateError, this.commandHistoryDateChangeHandler)}
                                            {this.state.isCommandHistoryLoader ? <div className="device-chart-message">{this.getMiniLoader()}</div> :
                                                this.state.commandHistoryData.length > 0 ?
                                                    <ListingTable
                                                        columns={this.getColumns()}
                                                        data={this.state.commandHistoryData}
                                                        statusBar={this.getStatusBarClass}
                                                    />
                                                    :
                                                    <div className="device-chart-message">
                                                        <AddNewButton
                                                            text1="No control history available"
                                                            text2="No controls have been triggered"
                                                            imageIcon="noData.png"
                                                            addButtonEnable={false}
                                                        />
                                                    </div>
                                            }
                                        </div>
                                    </div> :
                                    this.state.currentTab === "deviceAlarms" ?
                                        <div className="tab-pane fade show active">
                                            <div className="device-chart-group">
                                                {this.state.alarmsInfoLoader ? <div className="device-chart-message">{this.getMiniLoader()}</div> : this.state.alarmsInfoError ? this.getErrorMessage(this.state.alarmsInfoError) :
                                                    this.state.alarmsInfo.alarms && Object.keys(this.state.alarmsInfo.alarms).length > 0 ? Object.entries(this.state.alarmsInfo.alarms).map(([alarmType, alarms], i) =>
                                                        <div className="card device-alarm-wrapper" key={i}>
                                                            <div className="card-header">
                                                                <span className={`device-alarm-icon text-${alarmType === "warning" ? "alarm-warning" : alarmType}`}>
                                                                    <i className={`fas ${alarmType === "critical" ? "fa-exclamation-triangle" : alarmType === "major" ? "fa-exclamation-square" : alarmType === "minor" ? "fa-exclamation-circle" : "fa-exclamation"}`}></i>
                                                                </span>
                                                                <h6 className="text-capitalize">{alarmType}</h6>
                                                                <div className="alert alert-primary">{alarms.length}</div>
                                                            </div>
                                                            <div className="card-body">
                                                                <ul className="device-alarm-list list-style-none">
                                                                    {Array.isArray(alarms) && alarms.map((alarm, alarmIndex) => {
                                                                        let alarmCount = alarm.count
                                                                        let tooltipVisibility = false
                                                                        if (Math.floor((alarm.count) / 1000) > 0) {
                                                                            if (alarm.count % 1000 == 0) {
                                                                                alarmCount = Math.floor(alarm.count / 1000) + "K"
                                                                            } else {
                                                                                alarmCount = Math.floor(alarm.count / 1000) + "K+"
                                                                                tooltipVisibility = true
                                                                            }
                                                                        }
                                                                        return (<li key={alarmIndex}>
                                                                            {
                                                                                alarm.count ?
                                                                                    <div className="device-alarm-count">
                                                                                        <i className="fad fa-bell text-orange"></i>
                                                                                        <span data-tooltip={tooltipVisibility} data-tooltip-text={alarm.count} data-tooltip-place="bottom" className="badge badge-danger">{alarmCount}</span>
                                                                                    </div> :
                                                                                    <div className="device-alarm-count">
                                                                                        <i className="fad fa-bell"></i>
                                                                                        <span className="badge badge-light">0</span>
                                                                                    </div>
                                                                            }
                                                                            <h6>Last Recorded value of <strong>{alarm.displayName !== "" ? alarm.displayName : alarm.attr}</strong> was <strong> {alarm.value % 1 ? alarm.value.toFixed(2) : alarm.value} {alarm.unit}.</strong> <span className="ml-1 mr-1">|</span> This was <strong>{this.getStatementForAlarm(alarm.value, alarm.low, alarm.high, alarm.unit, alarm.operator)}</strong> (Configured Threshold)</h6>
                                                                            <p>
                                                                                <strong>Last Reported : </strong>{convertTimestampToDate(alarm.timeRaised)} <span className="text-cyan">({getTimeDifference(alarm.timeRaised)})</span>
                                                                                {alarm.acknowledged && <React.Fragment><span className="mr-1 ml-1">|</span><strong>Snoozed At : </strong> {convertTimestampToDate(alarm.timeAcknowledged)} <span className="text-cyan">({getTimeDifference(alarm.timeAcknowledged)})</span></React.Fragment>}
                                                                            </p>
                                                                            <div className="button-group">
                                                                                <button type="button" disabled={alarm.loaderType || alarm.acknowledged} className="btn btn-outline-success" onClick={() => this.updateAlarmStatus(alarm.attr, alarmIndex, alarmType, "acknowledge")}>{alarm.acknowledged ? "Snoozed" : alarm.loaderType === "acknowledge" ? "Snoozing..." : "Snooze 10 mins"}</button>
                                                                                <button type="button" disabled={alarm.loaderType} className="btn btn-outline-primary" onClick={() => this.updateAlarmStatus(alarm.attr, alarmIndex, alarmType, "clear")} >{alarm.loaderType === "clear" ? "Clearing.." : "Clear"}</button>
                                                                            </div>
                                                                        </li>)
                                                                    }
                                                                    )}
                                                                </ul>
                                                            </div>
                                                        </div>) :
                                                        <div className="device-chart-group">
                                                            <div className="device-chart-message">
                                                                <AddNewButton
                                                                    text1="No alarms available"
                                                                    text2="There are no alarms for this device."
                                                                    imageIcon="noData.png"
                                                                    addButtonEnable={false}
                                                                />
                                                            </div>
                                                        </div>
                                                }
                                            </div>
                                        </div> :
                                        <div className="tab-pane fade show active">
                                            <div className="device-chart-group">
                                                {this.state.insightsAttributesStatus === "loading" && <div className="device-chart-message">{this.getMiniLoader()}</div>}
                                                {this.state.insightsAttributesStatus === "loaded" && this.getInsightsHTML()}
                                                {this.state.insightsAttributesStatus === "error" &&
                                                    <div className="device-chart-message">
                                                        <AddNewButton
                                                            text1="Oops! we are unable to fetch attributes."
                                                            text2="Please refresh the page."
                                                            imageIcon="noData.png"
                                                            addButtonEnable={false}
                                                        />
                                                    </div>
                                                }
                                                {this.state.insightsAttributesStatus === "noAttributes" &&
                                                    <div className="device-chart-message">
                                                        <AddNewButton
                                                            text1="No attributes found."
                                                            text2="There is no data to display."
                                                            imageIcon="noData.png"
                                                            addButtonEnable={false}
                                                        />
                                                    </div>
                                                }
                                            </div>
                                        </div>
                        }
                    </div>
                </div>

                {/* insights Modal */}
                <div className="modal animated slideInDown insight-modal" id="insightModal">
                    <div className="modal-dialog modal-dialog-centered">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h6 className="modal-title">Select Attributes
                                    <button type="button" className="close" data-tooltip data-tooltip-text="Close" data-tooltip-place="bottom" data-dismiss="modal">
                                        <i className="far fa-times"></i>
                                    </button>
                                </h6>
                            </div>
                            <div className="modal-body overflow-y-auto" style={{ height: 300 }}>
                                <div className="d-flex">
                                    <div className="flex-33">
                                        <div className="form-group p-2 mb-0">
                                            <label className="check-box">
                                                <input type="checkbox" />
                                                <span className="check-mark"></span>
                                                <span className="check-text">Attribute 1</span>
                                            </label>
                                        </div>
                                    </div>

                                    <div className="flex-33">
                                        <div className="form-group p-2 mb-0">
                                            <label className="check-box">
                                                <input type="checkbox" />
                                                <span className="check-mark"></span>
                                                <span className="check-text">Attribute 2</span>
                                            </label>
                                        </div>
                                    </div>

                                    <div className="flex-33">
                                        <div className="form-group p-2 mb-0">
                                            <label className="check-box">
                                                <input type="checkbox" />
                                                <span className="check-mark"></span>
                                                <span className="check-text">Attribute 3</span>
                                            </label>
                                        </div>
                                    </div>

                                    <div className="flex-33">
                                        <div className="form-group p-2 mb-0">
                                            <label className="check-box">
                                                <input type="checkbox" />
                                                <span className="check-mark"></span>
                                                <span className="check-text">Attribute 4</span>
                                            </label>
                                        </div>
                                    </div>

                                    <div className="flex-33">
                                        <div className="form-group p-2 mb-0">
                                            <label className="check-box">
                                                <input type="checkbox" />
                                                <span className="check-mark"></span>
                                                <span className="check-text">Attribute 5</span>
                                            </label>
                                        </div>
                                    </div>

                                    <div className="flex-33">
                                        <div className="form-group p-2 mb-0">
                                            <label className="check-box">
                                                <input type="checkbox" />
                                                <span className="check-mark"></span>
                                                <span className="check-text">Attribute 6</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div className="modal-footer">
                                <button type="button" className="btn btn-light" data-dismiss="modal">Cancel</button>
                                <button type="button" className="btn btn-primary" data-dismiss="modal">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
                {/* end insights Modal */}

                {/* graph full screen modal */}
                {this.state.showZoomModel &&
                    <div className="modal d-block animated slideInDown show" id="graphFullModal">
                        <div className="modal-dialog modal-full">
                            <div className="modal-content">
                                <div className="modal-body p-2">
                                    <div className="card modal-full-editor m-0 shadow-none">
                                        <div className="card-header border">
                                            <h6>{this.state.dashboardConfigData[this.state.zoomIndex].headerName}</h6>
                                            <button type="button" className="btn btn-light text-gray" onClick={() => this.setState({ showZoomModel: false })} data-dismiss="modal">
                                                <i className="fas fa-compress"></i>
                                            </button>
                                        </div>
                                        <div className="card-body border">
                                            {this.state.zoomType != "gaugeChart" ?
                                                <MonitoringLineChart
                                                    timeZone={this.props.timeZone}
                                                    data={this.state.dashboardConfigData[this.state.zoomIndex].lineChart}
                                                    alarmAttributes={this.state.attributesList}
                                                    operationAttribute={this.state.zoomedChartOperation}
                                                    chartId={`lineChart_${this.state.dashboardConfigData[this.state.zoomIndex].id}_Zoom`}
                                                /> :
                                                <React.Fragment>
                                                    <CreateChart
                                                        data={this.state.dashboardConfigData[this.state.zoomIndex].gaugeChart}
                                                        timeZone={this.props.timeZone}
                                                        attributes={this.state.attributesList}
                                                        divId={`gaugeChart_${this.state.dashboardConfigData[this.state.zoomIndex].id}_Zoom`}
                                                        type={"gaugeChart"}
                                                    />
                                                    {this.state.dashboardConfigData[this.state.zoomIndex].gaugeChart.data && this.state.dashboardConfigData[this.state.zoomIndex].gaugeChart.data.hasOwnProperty("value") && this.state.dashboardConfigData[this.state.zoomIndex].gaugeChart.data.timestamp !== 0 && this.state.attributeSuccess &&
                                                        <div className="card-chart-box-status">
                                                            <strong className="text-gray">Last Reported : </strong> <span className={`${deviceInfo.status == "online" ? "text-green" : "text-red"}`}>{getTimeDifference(this.state.dashboardConfigData[this.state.zoomIndex].gaugeChart.data.timestamp)}</span>
                                                        </div>
                                                    }
                                                </React.Fragment>
                                            }
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                }
                {/* end graph full screen modal */}

                {/* configure modal */}
                {this.state.configureModal &&
                    <div className="modal d-block animated slideInDown" id="configureModal">
                        <div className="modal-dialog modal-dialog-centered">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h6 className="modal-title">Select Data Points
                                        <button type="button" className="close" data-tooltip data-tooltip-text="Close" data-tooltip-place="bottom" onClick={() => this.setState({ configureModal: false })}>
                                            <i className="far fa-times"></i>
                                        </button>
                                    </h6>
                                </div>
                                <div className="modal-body">
                                    <div className="form-group">
                                        <label className="form-group-label">Header :</label>
                                        <input type="text" className="form-control" value={this.state.chartCustomConfig.headerName} onChange={this.configChangeHandler}
                                            name="header" id="headerName"
                                        />
                                    </div>
                                    <div className="d-flex">
                                        <div className="flex-50 pd-r-10">
                                            <div className="form-group">
                                                <label className="form-group-label">Min :</label>
                                                <input type="number" className="form-control" value={this.state.chartCustomConfig.gaugeChart.min}
                                                    onChange={this.configChangeHandler} name="min" id="min"
                                                />
                                            </div>
                                        </div>
                                        <div className="flex-50 pd-l-10">
                                            <div className="form-group">
                                                <label className="form-group-label">Max :</label>
                                                <input type="number" className="form-control" value={this.state.chartCustomConfig.gaugeChart.max}
                                                    onChange={this.configChangeHandler} name="max" id="max"
                                                />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label className="form-group-label">Select Attribute (showing only numeric attributes) :</label>
                                        <div className="d-flex">
                                            {this.state.attributesList.map((val, index) => (
                                                !this.state.dashboardConfigData.filter(subval => subval.id != this.state.chartCustomConfig.id).some(temp => temp.attributes.includes(val.attribute)) ?
                                                    <div className="flex-33 pd-r-10" key={index}>
                                                        <label className="radio-button mb-2">
                                                            <span className="radio-button-text-monitoring">{val.displayName != "" ? val.displayName : val.attribute}</span>
                                                            <input type="radio" value={val.attribute}
                                                                checked={this.state.chartCustomConfig.attributes.includes(val.attribute)}
                                                                onChange={this.configChangeHandler}
                                                                name="attribute" id="attribute"
                                                            />
                                                            <span className="radio-button-mark"></span>
                                                        </label>
                                                    </div> : null
                                            ))}
                                        </div>
                                    </div>
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-light" onClick={() => this.setState({ configureModal: false })}>Cancel</button>
                                    <button type="button" disabled={!this.state.chartCustomConfig.attributes.length > 0} className="btn btn-success" onClick={() => this.saveChartConfig()}>Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                }
                {/* end configure modal */}

                {/* command modal */}
                {this.state.addCommandModal &&
                    <CommandConfigModal COMMAND_OBJ={this.state.COMMAND_OBJ} deviceTypeName={deviceInfo.deviceTypeName} saveCommandsHandler={this.saveCommandsHandler} closeCommandModal={this.closeCommandModal} commandModalLoader={this.state.commandModalLoader} />
                }
                {/* end command modal */}

                {/* device control modal */}
                {this.state.isCommandPayload &&
                    <div className="modal d-block animated slideInDown">
                        <div className="modal-dialog">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h6 className="modal-title">Control payload
                                        <button type="button" className="close" data-tooltip data-tooltip-text="Close" data-tooltip-place="bottom" onClick={() => this.setState({ isCommandPayload: false, commandPayload: "" })}>
                                            <i className="far fa-times"></i>
                                        </button>
                                    </h6>
                                </div>
                                <div className="modal-body">
                                    <div className="ace-editor">
                                        <AceEditor
                                            placeholder="Paste your JSON here..."
                                            mode="json"
                                            name="ace_editor"
                                            theme="monokai"
                                            height="100%"
                                            width="100%"
                                            readOnly={true}
                                            value={this.state.commandPayload}
                                        />
                                    </div>
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-success" onClick={() => this.setState({ isCommandPayload: false, commandPayload: "" })}>OK</button>
                                </div>
                            </div>
                        </div>
                    </div>
                }
                {/* end device control modal */}

                {/* historical data modal */}
                {this.state.historicViewModal &&
                    <div className="modal animated slideInDown d-block">
                        <div className="modal-dialog modal-xl modal-dialog-centered">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h6 className="modal-title">Historical Data For - <span>{this.state.historyChartAttributeDisplayName}</span>
                                        <button type="button" className="close" data-tooltip data-tooltip-text="Close" data-tooltip-place="bottom"
                                            onClick={() => {
                                                this.setState({
                                                    historicViewModal: false,
                                                    historyChartData: undefined,
                                                    historyOperationDetails: {
                                                        operation: null,
                                                        bucketSize: this.state.bucketSizeOptions[0]['value'],
                                                        attrData: {
                                                            operator: ">=",
                                                            value: 0
                                                        }
                                                    }
                                                })
                                            }}
                                        >
                                            <i className="far fa-times"></i>
                                        </button>
                                    </h6>
                                    <ul className="modal-header-list list-style-none">
                                        <li className="btn btn-light" data-tooltip data-tooltip-text="Refresh" data-tooltip-place="bottom" onClick={() => this.getAttributeHistoryData(this.state.historyChartDuration)}>
                                            <i className="far fa-sync-alt"></i>
                                        </li>
                                    </ul>
                                </div>
                                <div className="modal-body card mb-0">
                                    <div className="form-group d-flex">
                                        <div className="flex-25 pd-r-5">
                                            <label className="form-group-label">Operation :</label>
                                            <ReactSelect
                                                className="form-control-multi-select"
                                                value={this.state.operations.find(operation => operation.value === this.state.historyOperationDetails.operation)}
                                                onChange={(event) => this.historyOperationChangeHandler(event, "operation")}
                                                options={this.state.operations}
                                                isMulti={false}
                                            >
                                            </ReactSelect>
                                        </div>
                                        <div className="flex-25 pd-r-5 pd-l-5">
                                            <label className="form-group-label">Bucket Size :</label>
                                            <ReactSelect
                                                className="form-control-multi-select"
                                                value={this.state.bucketSizeOptions.find(option => option.value === this.state.historyOperationDetails.bucketSize)}
                                                onChange={(e) => this.historyOperationChangeHandler(e, "bucketSize")}
                                                options={this.state.bucketSizeOptions}
                                                isMulti={false}
                                                isDisabled={this.state.historyOperationDetails.operation === "NONE" || this.state.historyOperationDetails.operation === null || this.state.historyOperationDetails.operation === "rateOfChange"}
                                            >
                                            </ReactSelect>
                                        </div>
                                        <div className="flex-25 pd-r-5 pd-l-5">
                                            <label className="form-group-label">Operator :</label>
                                            <div className="input-group">
                                                <div className="input-group-prepend">
                                                    <span className="input-group-text w-auto">
                                                        <select id="operator" className="cursor-pointer" disabled={this.state.historyOperationDetails.operation != "rateOfChange"} value={this.state.historyOperationDetails.attrData.operator} onChange={(e) => this.historyOperationChangeHandler(e, "attrData")} >
                                                            <option value=">">{`>`}</option>
                                                            <option value=">=">{`>=`}</option>
                                                            <option value="<">{`<`}</option>
                                                            <option value="<=">{`<=`}</option>
                                                            <option value="=">{`=`}</option>
                                                        </select>
                                                    </span>
                                                </div>
                                                <input type="number" id="value" className="form-control" disabled={this.state.historyOperationDetails.operation != "rateOfChange"} onChange={(e) => this.historyOperationChangeHandler(e, "attrData")} value={this.state.historyOperationDetails.attrData.value} />
                                            </div>
                                        </div>
                                        <div className="flex-25 pd-l-5">
                                            <label className="form-group-label">Duration :</label>
                                            <ReactSelect
                                                className="form-control pd-t-0 pd-b-0 pd-l-0 pd-r-0 border-none"
                                                value={DURATION_OPTIONS.find(el => el.value === this.state.historyChartDuration)}
                                                onChange={(val) => this.getAttributeHistoryData(val.value)}
                                                options={DURATION_OPTIONS}
                                                isMulti={false}
                                            >
                                            </ReactSelect>
                                        </div>
                                    </div>
                                    {this.state.attributesList.length > 0 && this.state.attributesList.filter(attr => attr.attribute === this.state.historyChartAttributeName && attr.isConfigured).length > 0 &&
                                        this.getAlarmConfigHTML(this.state.attributesList.find(attr => attr.attribute === this.state.historyChartAttributeName).alarmCriteria)
                                    }
                                    <div className="card-body p-0">
                                        {this.state.historyChartLoading ? this.getMiniLoader() :
                                            this.state.historyOperationDetails.operation && this.state.historyOperationDetails.operation != "NONE" ?
                                                (Boolean(this.state.historyChartData && this.state.historyChartData.data && this.state.historyChartData.operationData && this.state.historyChartData.operationData.length > 0 && this.state.historyChartData.data.chartData.length > 0 && this.state.attributeSuccess) ?
                                                    <div className="card-chart-box">
                                                        <MonitoringLineChart timeZone={this.props.timeZone} data={this.state.historyChartData} alarmAttributes={this.state.attributesList} chartId={"lineChartattrHistoricChart"} operationAttribute={this.state.operations.find(val => val.value === this.state.historyOperationDetails.operation)} />
                                                    </div>
                                                    :
                                                    <div className="card-message">
                                                        <p>There is no data to display.</p>
                                                    </div>)
                                                :
                                                Boolean(this.state.historyChartData && this.state.historyChartData.data && this.state.attributeSuccess && this.state.historyChartData.data.chartData.length > 0 && this.state.attributeSuccess) ?
                                                    <div className="card-chart-box">
                                                        <MonitoringLineChart timeZone={this.props.timeZone} data={this.state.historyChartData} alarmAttributes={this.state.attributesList} chartId={"lineChartattrHistoricChart"} />
                                                    </div>
                                                    :
                                                    <div className="card-message">
                                                        <p>There is no data to display.</p>
                                                    </div>
                                        }
                                    </div>
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-success" onClick={() => {
                                        this.setState({
                                            historicViewModal: false,
                                            historyChartData: undefined,
                                            historyOperationDetails: {
                                                operation: null,
                                                bucketSize: this.state.bucketSizeOptions[0]['value'],
                                                attrData: {
                                                    operator: ">=",
                                                    value: 0
                                                }
                                            }
                                        })
                                    }}>OK</button>
                                </div>
                            </div>
                        </div>
                    </div>
                }
                {/* end historical data modal */}

                {this.state.isOpen &&
                    <NotificationModal
                        type={this.state.type}
                        message2={this.state.message2}
                        onCloseHandler={this.onCloseHandler}
                    />
                }

                {this.state.confirmState &&
                    <ConfirmModel
                        status={"delete"}
                        delete={""}
                        message1={this.state.dashboardConfigData[this.state.deleteChartIndex].attributes.length ? "chart data for" : ""}
                        deleteName={this.state.dashboardConfigData[this.state.deleteChartIndex].attributes.length ? `${this.state.dashboardConfigData[this.state.deleteChartIndex].attributes[0]}` : ""}
                        confirmClicked={() => this.deleteChartHandler()}
                        cancelClicked={() => {
                            this.cancelClicked()
                        }}
                    />
                }

            </React.Fragment>
        );
    }
}

DeviceDashboardsDeviceDetails.propTypes = {
    dispatch: PropTypes.func.isRequired
};

let allSelectors = {}
Object.entries(SELECTORS).map(([key, value]) => {
    allSelectors[key] = value()
})

const mapStateToProps = createStructuredSelector(allSelectors)

function mapDispatchToProps(dispatch) {
    let allActions = { dispatch }
    Object.entries(ACTIONS).map(([key, value]) => {
        allActions[key] = (...args) => dispatch(value(...args))
    })
    return allActions
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({
    key: "deviceDashboardsDeviceDetails",
    reducer
});
const withSaga = injectSaga({ key: "deviceDashboardsDeviceDetails", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(DeviceDashboardsDeviceDetails);
