/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * PreviewPage
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import { allActions as ACTIONS } from './actions';
import { allSelectors as SELECTORS } from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import Loader from "../../components/Loader/Loadable";
import Iframe from "../../components/Iframe/Loadable";
import produce from "immer";

/* eslint-disable react/prefer-stateless-function */
export class PreviewPage extends React.Component {
    state = {
        isFetching: true,
        pageDetails: null,
    }

    componentDidMount() {
        this.props.getPageDetails(this.props.match.params.id);
    }

    componentDidUpdate(prevProps, prevState) {
        let newProp = Object.keys(SELECTORS).find(prop => this.props[prop])
        if (newProp) {
            if (newProp.includes("Failure")) {
                this.props.showNotification("error", this.props[newProp].error)
            }
            this.props.resetToInitialState(newProp)
        }
        if (this.props.match.params.id !== prevProps.match.params.id) {
            this.setState({
                isFetching: true,
            }, () => { this.props.getPageDetails(this.props.match.params.id) })
        }
    }

    static getDerivedStateFromProps(nextProps, state) {
        let newProp = Object.keys(SELECTORS).find(prop => nextProps[prop])
        if (newProp) {
            let propData = nextProps[newProp].error ? nextProps[newProp].error : nextProps[newProp].response;

            if (nextProps.pageDetailsFailure) {
                return {
                    isFetching: false,
                    type: 'error',
                    isOpen: true,
                    message2: nextProps.pageDetailsFailure.error,
                }
            }

            if (nextProps.pageDetailsSuccess) {
                let pageDetails = produce(propData, pageDetails => {
                    pageDetails.code.html = decodeURIComponent(escape(atob(pageDetails.code.html)));
                    pageDetails.code.css = decodeURIComponent(escape(atob(pageDetails.code.css)));
                    pageDetails.code.js = decodeURIComponent(escape(atob(pageDetails.code.js)));
                })
                return {
                    pageDetails,
                    isFetching: false
                }
            }

        }
        return null;
    }

    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>Dashboard</title>
                    <meta name="description" content="Preview Dashboard" />
                </Helmet>
                {this.state.isFetching ?
                    <Loader/> :
                    <React.Fragment>
                        <header className="content-header d-flex">
                            <div className="flex-60">
                                <div className="d-flex">
                                    <h6>Dashboard</h6>
                                    {this.state.pageDetails && <h6 className="active">{this.state.pageDetails.name}</h6>}
                                </div>
                            </div>
                        </header>
                        <div className="content-body">
                            {this.state.pageDetails && <div className="iframe-wrapper">
                                <Iframe
                                    css={this.state.pageDetails.code.css}
                                    js={this.state.pageDetails.code.js}
                                    setting={this.state.pageDetails.settings}
                                    html={this.state.pageDetails.code.html}
                                    id="iframe"
                                />
                            </div>}
                        </div>

                    </React.Fragment>
                }
            </React.Fragment>
        );
    }
}

PreviewPage.propTypes = {
  dispatch: PropTypes.func.isRequired
};
let allSelectors = {}
Object.entries(SELECTORS).map(([key, value]) => {
    allSelectors[key] = value()
})

const mapStateToProps = createStructuredSelector(allSelectors);

function mapDispatchToProps(dispatch) {
    let allActions = { dispatch }
    Object.entries(ACTIONS).map(([key, value]) => {
        allActions[key] = (...args) => dispatch(value(...args))
    })
    return allActions;
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

const withReducer = injectReducer({ key: "previewPage", reducer });
const withSaga = injectSaga({ key: "previewPage", saga });

export default compose(
  withReducer,
  withSaga,
  withConnect
)(PreviewPage);
