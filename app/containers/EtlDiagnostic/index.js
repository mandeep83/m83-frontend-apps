/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * EtlDiagnostic
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { FormattedMessage } from "react-intl";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import makeSelectEtlDiagnostic from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import messages from "./messages";
import Chart from "../../components/LineChart";

let config = {"headerName":"Sample Header","i":"dda6dd67-dbfe-4858-ab12-5a0b4c127b36","theme":"noTheme","widgetType":"barChart","widgetId":"bfc49154c9bc44b3b58985f6218e3640","widgetName":"3DColumnChart","dataSource":"DEFAULT","apiId":"","widgetData":{"chartData":[{"year2005":4.2,"category":"USA"},{"year2005":3.1,"category":"UK"},{"year2005":2.9,"category":"Canada"},{"year2005":2.3,"category":"Japan"},{"year2005":2.1,"category":"France"},{"year2005":4.9,"category":"Brazil"}],"availableAttributes":["year2005"]},"mqttDetails":{},"widgetDetails":{"imageUrl":"","buttonColor":"","icon":"","detailPage":"","selectedAttribute":"","isDetailPageRequired":false,"isSamePageRequired":false,"customValues":{"barWidth":{"value":80,"type":"number"},"legendPlacement":{"value":"bottom","type":"select","options":["bottom","top","left","right"]},"chartAngle":{"value":30,"type":"number"},"chartDepth":{"value":30,"type":"number"},"customColors":{"value":false,"type":"boolean"},"year2005":{"value":"#0ba6ef","type":"color","parent":"customColors"}}},"apiDetails":{},"customWidgetDetails":{},"preview":true,"showFullScreen":true,"w":6,"h":7,"x":0,"y":0}
let config2 = {"headerName":"Sample Header","i":"dda6dd67-dbfe-4858-ab12-5a0b24c127b36","theme":"noTheme","widgetType":"barChart","widgetId":"bfc49154c9bc44b3b58985f6218e3640","widgetName":"3DColumnChart","dataSource":"DEFAULT","apiId":"","widgetData":{"chartData":[{"year2005":4.2,"category":"USA"},{"year2005":3.1,"category":"UK"},{"year2005":2.9,"category":"Canada"},{"year2005":2.3,"category":"Japan"},{"year2005":2.1,"category":"France"},{"year2005":4.9,"category":"Brazil"}],"availableAttributes":["year2005"]},"mqttDetails":{},"widgetDetails":{"imageUrl":"","buttonColor":"","icon":"","detailPage":"","selectedAttribute":"","isDetailPageRequired":false,"isSamePageRequired":false,"customValues":{"barWidth":{"value":80,"type":"number"},"legendPlacement":{"value":"bottom","type":"select","options":["bottom","top","left","right"]},"chartAngle":{"value":30,"type":"number"},"chartDepth":{"value":30,"type":"number"},"customColors":{"value":false,"type":"boolean"},"year2005":{"value":"#0ba6ef","type":"color","parent":"customColors"}}},"apiDetails":{},"customWidgetDetails":{},"preview":true,"showFullScreen":true,"w":6,"h":7,"x":0,"y":0}
let config3 = {"headerName":"Sample Header","i":"dda6dd67-dbfe-4858-ab12-5a0b43c127b36","theme":"noTheme","widgetType":"barChart","widgetId":"bfc49154c9bc44b3b58985f6218e3640","widgetName":"3DColumnChart","dataSource":"DEFAULT","apiId":"","widgetData":{"chartData":[{"year2005":4.2,"category":"USA"},{"year2005":3.1,"category":"UK"},{"year2005":2.9,"category":"Canada"},{"year2005":2.3,"category":"Japan"},{"year2005":2.1,"category":"France"},{"year2005":4.9,"category":"Brazil"}],"availableAttributes":["year2005"]},"mqttDetails":{},"widgetDetails":{"imageUrl":"","buttonColor":"","icon":"","detailPage":"","selectedAttribute":"","isDetailPageRequired":false,"isSamePageRequired":false,"customValues":{"barWidth":{"value":80,"type":"number"},"legendPlacement":{"value":"bottom","type":"select","options":["bottom","top","left","right"]},"chartAngle":{"value":30,"type":"number"},"chartDepth":{"value":30,"type":"number"},"customColors":{"value":false,"type":"boolean"},"year2005":{"value":"#0ba6ef","type":"color","parent":"customColors"}}},"apiDetails":{},"customWidgetDetails":{},"preview":true,"showFullScreen":true,"w":6,"h":7,"x":0,"y":0}
let config4 = {"headerName":"Sample Header","i":"dda6dd67-dbfe-4858-ab12-5a0b4c3127b36","theme":"noTheme","widgetType":"barChart","widgetId":"bfc49154c9bc44b3b58985f6218e3640","widgetName":"3DColumnChart","dataSource":"DEFAULT","apiId":"","widgetData":{"chartData":[{"year2005":4.2,"category":"USA"},{"year2005":3.1,"category":"UK"},{"year2005":2.9,"category":"Canada"},{"year2005":2.3,"category":"Japan"},{"year2005":2.1,"category":"France"},{"year2005":4.9,"category":"Brazil"}],"availableAttributes":["year2005"]},"mqttDetails":{},"widgetDetails":{"imageUrl":"","buttonColor":"","icon":"","detailPage":"","selectedAttribute":"","isDetailPageRequired":false,"isSamePageRequired":false,"customValues":{"barWidth":{"value":80,"type":"number"},"legendPlacement":{"value":"bottom","type":"select","options":["bottom","top","left","right"]},"chartAngle":{"value":30,"type":"number"},"chartDepth":{"value":30,"type":"number"},"customColors":{"value":false,"type":"boolean"},"year2005":{"value":"#0ba6ef","type":"color","parent":"customColors"}}},"apiDetails":{},"customWidgetDetails":{},"preview":true,"showFullScreen":true,"w":6,"h":7,"x":0,"y":0}

/* eslint-disable react/prefer-stateless-function */
export class EtlDiagnostic extends React.Component {
  render() {
    return (
      <React.Fragment>
        <Helmet>
          <title>EtlDiagnostic</title>
          <meta name="description" content="Description of EtlDiagnostic" />
        </Helmet>
          <div className="pageBreadcrumb">
              <div className="row">
                  <div className="col-8">
                      <p><span>ETL Diagnostics (Last 20 minutes)</span></p>
                  </div>
                  <div className="col-4 text-right">
                      <div className="flex h-100 justify-content-end align-items-center">
                          <button className="btn btn-link pd-0 mr-r-7"><i className="fas fa-download"></i> Download Config</button>
                          <button className="btn btn-link pd-0"><i className="fas fa-sync-alt"></i> Refresh</button>
                      </div>
                  </div>
              </div>
          </div>
          <div className="outerBox">
            <div className="flex etlDiagnosticHeader">
                <div className="flex-item fx-b10">
                    <p>Status</p>
                </div>
                <div className="flex-item fx-b30">
                    <p>ETL Name</p>
                </div>
                <div className="flex-item fx-b10">
                    <p>State</p>
                </div>
                <div className="flex-item fx-b5">
                    <p>Max (s)</p>
                </div>
                <div className="flex-item fx-b5">
                    <p>Avg (s)</p>
                </div>
                <div className="flex-item fx-b10">
                    <p>Threshold (s)</p>
                </div>
                <div className="flex-item fx-b10">
                    <p>Processed Offset</p>
                </div>
                <div className="flex-item fx-b10">
                    <p>Max Offset</p>
                </div>
                <div className="flex-item fx-b10">
                    <p>Trigger Interval</p>
                </div>
            </div>
              <ul className="etlDiagnosticList">
                <li>
                    <div className="flex etlDiagnosticContent collapsed"  data-toggle="collapse" data-target="#demo">
                        <i className="fas fa-chevron-up"></i>
                        <div className="flex-item fx-b10">
                        <p className="text-lightGreen">
                            <i className="fas fa-check-circle"></i>
                            {/*<i className="fas fa-times-circle"></i>*/}
                        </p>
                    </div>
                    <div className="flex-item fx-b30">
                        <p>ETL_CONTAINER_INFO_PER_REPORTING_INTERVAL_V4_MONGODB</p>
                    </div>
                    <div className="flex-item fx-b10">
                        <p className="text-lightGreen">Running
                        {/*stopped*/}
                        </p>
                    </div>
                    <div className="flex-item fx-b5">
                        <p>0.03</p>
                    </div>
                    <div className="flex-item fx-b5">
                        <p className="text-lightGreen">0.04</p>
                    </div>
                    <div className="flex-item fx-b10">
                        <p>1</p>
                    </div>
                    <div className="flex-item fx-b10">
                        <p>2</p>
                    </div>
                    <div className="flex-item fx-b10">
                        <p>4</p>
                    </div>
                    <div className="flex-item fx-b10">
                        <p>5</p>
                    </div>
                    </div>
                    <div className="collapse" id="demo">
                        <div className="etlDiagnosticItem">
                            <div className="flex h-100 justify-content-end align-items-center mr-b-15">
                                <button className="btn btn-link"><i className="far fa-refresh mr-r-7"></i>Refresh</button>
                                <span className="mr-r-7 mr-l-7">|</span>
                                <button className="btn btn-link"><i className="far fa-recycle mr-r-7"></i>Clean Up and Restart</button>
                                <span className="mr-r-7 mr-l-7">|</span>
                                <button className="btn btn-link"><i className="far fa-undo mr-r-7"></i>Restart</button>
                                <span className="mr-r-7 mr-l-7">|</span>
                                <button className="btn btn-link"><i className="far fa-play mr-r-7"></i>Resume</button>
                                <span className="mr-r-7 mr-l-7">|</span>
                                <button className="btn btn-link"><i className="far fa-pause mr-r-7"></i>Pause</button>
                                <span className="mr-r-7 mr-l-7">|</span>
                                <button className="btn btn-link"><i className="far fa-file-text-o mr-r-7"></i>Change Config</button>
                            </div>
                            <div className="flex">
                                <div className="flex-item fx-b50 pd-7">
                                    <div className="card panel">
                                        <div className="card-body panelBody">
                                            <div className="etlDiagnosticChart">
                                                <Chart data={config}  />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="flex-item fx-b50 pd-7">
                                    <div className="card panel">
                                        <div className="card-body panelBody">
                                            <div className="etlDiagnosticChart">
                                                <Chart data={config2}  />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="flex-item fx-b50 pd-7">
                                    <div className="card panel">
                                        <div className="card-body panelBody">
                                            <div className="etlDiagnosticChart">
                                                <Chart data={config3}  />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="flex-item fx-b50 pd-7">
                                    <div className="card panel">
                                        <div className="card-body panelBody">
                                            <div className="etlDiagnosticChart">
                                                <Chart data={config4}  />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </li>
              </ul>
          </div>
      </React.Fragment>
    );
  }
}

EtlDiagnostic.propTypes = {
  dispatch: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
  etldiagnostic: makeSelectEtlDiagnostic()
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

const withReducer = injectReducer({ key: "etlDiagnostic", reducer });
const withSaga = injectSaga({ key: "etlDiagnostic", saga });

export default compose(
  withReducer,
  withSaga,
  withConnect
)(EtlDiagnostic);
