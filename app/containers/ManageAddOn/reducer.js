/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/*
 *
 * ManageAddOn reducer
 *
 */

import { fromJS } from "immutable";
import * as CONSTANTS from "./constants";

export const initialState = fromJS({});

let allCases = Object.values(CONSTANTS).filter(constant => typeof(constant) == "object")

let getValueObj = (action, index)=>{
  let isSuccessCase = action.type.includes("SUCCESS")
  let key =  isSuccessCase ? allCases[index].successKey : allCases[index].failureKey
  return {
    [key]: action
  }
}

function manageAddOnReducer(state = initialState, action) {
  if (action.type === CONSTANTS.RESET_TO_INITIAL_STATE) {
    return Object.assign({}, state, {
      [action.prop]: null,
    })
  }
  for(let i=0; i < allCases.length; i++){
    if(action.type === allCases[i].success || action.type === allCases[i].failure){
      return Object.assign({}, state, getValueObj(action, i));
    }
  }
  return state
}

export default manageAddOnReducer;
