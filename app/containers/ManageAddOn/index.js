/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * ManageAddOn
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import produce from "immer";

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import { allSelectors as SELECTORS } from "./selectors";
import { allActions as ACTIONS } from './actions'
import reducer from "./reducer";
import { cloneDeep } from "lodash";
import saga from "./saga";
import ListingTable from "../../components/ListingTable/Loadable";
import SearchBox from "../../components/SearchBox/Loadable";
import Loader from "../../components/Loader";
import ImageUploader from "../../components/ImageUploader";
import ConfirmModel from "../../components/ConfirmModel";
import ReactSelect from "react-select";
import {convertTimestampToDate} from "../../commonUtils";

let DEFAULT_PAYLOAD = {
    amount: 0,
    category: "",
    description: "",
    imageUrl: "",
    attributes: [],
    products: [],
    name: "",
    isPrivate: false,
    toggleView: false,
    isPublished: false,
    oneTimePurchase: false,
    tenantNames: []
}

let categoryOption = [
    {
        value: "Device Management",
        label: "Device Management"
    }, {
        value: "Operation",
        label: "Operation"
    }, {
        value: "Notification",
        label: "Notification"
    }, {
        value: "IAM",
        label: "IAM"
    }, {
        value: "Code",
        label: "Code"
    }, {
        value: "Visualization",
        label: "Visualization"
    }, {
        value: "Connector",
        label: "Connector"
    }, {
        value: "Analytics",
        label: "Analytics"
    }, {
        value: "Others",
        label: "Others"
    },
]

/* eslint-disable react/prefer-stateless-function */
export class ManageAddOn extends React.Component {
    state = {
        isAddOrEditAddOn: false,
        allAddOns: [],
        allProducts: [],
        payload: cloneDeep(DEFAULT_PAYLOAD),
        isFetchingAddons: true,
        isFetchingProducts: false,
        tenantsList: [],
        features: [
            {
                categoryName: "Device",
                products: [
                    {
                        name: "no_of_devices",
                        displayName: "Devices (Total)",
                        isBasePackageFeature: true,
                        showDescription: false,
                    },
                    {
                        name: "attributeKeys",
                        displayName: "Device Attribute(s)",
                        isBasePackageFeature: true,
                        showDescription: false,
                    },
                    {
                        name: "device_groups",
                        displayName: "Device Group(s) / Device Type",
                        isBasePackageFeature: true,
                        showDescription: false,
                    },
                    {
                        name: "device_types",
                        displayName: "Device Type",
                        isBasePackageFeature: true,
                        showDescription: false,
                    }

                ]
            },
            {
                categoryName: "Operation",
                products: [
                    {
                        name: "events",
                        displayName: "Event(s) / Device Type",
                        isBasePackageFeature: true,
                        showDescription: true,
                    },
                    {
                        name: "rules",
                        displayName: "Rule(s) / Event",
                        isBasePackageFeature: true,
                        showDescription: true,
                    },
                    {
                        name: "report",
                        displayName: "Report(s)",
                        isBasePackageFeature: true,
                        showDescription: false,
                    },
                    {
                        name: "schedules",
                        displayName: "Schedule(s)",
                        isBasePackageFeature: true,
                        showDescription: false,
                    }
                ]
            },
            {
                categoryName: "Notification",
                products: [
                    {
                        name: "rpc",
                        displayName: "RPC / mo",
                        isBasePackageFeature: true,
                        showDescription: true,
                    },
                    {
                        name: "webhooks",
                        displayName: "Webhooks / mo",
                        isBasePackageFeature: true,
                        showDescription: true,
                    },
                    {
                        name: "email",
                        displayName: "Emails / mo",
                        isBasePackageFeature: true,
                        showDescription: false,
                    },
                    {
                        name: "sms",
                        displayName: "SMS / mo",
                        isBasePackageFeature: true,
                        showDescription: false,
                    }
                ]
            },
            {
                categoryName: "IAM",
                products: [
                    {
                        name: "users",
                        displayName: "User(s)",
                        isBasePackageFeature: true,
                        showDescription: true,
                    },
                    {
                        name: "role",
                        displayName: "Role(s)",
                        isBasePackageFeature: true,
                        showDescription: true,
                    },
                    {
                        name: "groups",
                        displayName: "User Group(s)",
                        isBasePackageFeature: true,
                        showDescription: true,
                    }
                ]
            },
            {
                categoryName: "Code",
                products: [
                    {
                        name: "docker_images",
                        displayName: "Docker Images",
                        isBasePackageFeature: true,
                        showDescription: false,
                    },
                    {
                        name: "faas",
                        displayName: "FaaS(s)",
                        isBasePackageFeature: true,
                        showDescription: false,
                    },
                    {
                        name: "api",
                        displayName: "API(s)",
                        isBasePackageFeature: true,
                        showDescription: false,
                    }
                ]
            },
            {
                categoryName: "Visualization",
                products: [
                    {
                        name: "pages",
                        displayName: "Page(s)",
                        isBasePackageFeature: true,
                        showDescription: false,
                    }
                ]
            },
            {
                categoryName: "Connector",
                products: [
                    {
                        name: "connectors",
                        displayName: "Connector(s)",
                        isBasePackageFeature: true,
                        showDescription: false,
                    }
                ]
            },
            {
                categoryName: "Analytics",
                products: [
                    {
                        name: "notebook",
                        displayName: "ETL Notebook",
                        isBasePackageFeature: true,
                        showDescription: false,
                    },
                    {
                        name: "udf",
                        displayName: "ETL UDF",
                        isBasePackageFeature: true,
                        showDescription: false,
                    },
                    {
                        name: "etl",
                        displayName: "ETL- Data Transformation",
                        isBasePackageFeature: true,
                        showDescription: false,
                    }
                ]
            },
            {
                categoryName: "Storage",
                products: [
                    {
                        name: "retention",
                        displayName: "Data Storage (in months)",
                        isBasePackageFeature: true,
                        showDescription: true,
                    }
                ]
            }
        ],
    }


    componentDidMount() {
        this.props.getAllAddOns();
        this.props.getTenantList();
    }

    static getDerivedStateFromProps(nextProps, state) {
        let newProp = Object.keys(SELECTORS).find(prop => nextProps[prop])
        if (newProp) {
            let propData = nextProps[newProp].error ? nextProps[newProp].error : nextProps[newProp].response
            if (nextProps.getAllAddOnsSuccess) {
                return {
                    allAddOns: propData,
                    isFetchingAddons: false,
                }
            }
            if (nextProps.getAllAddOnsFailure) {
                return {
                    isFetchingAddons: false,
                }
            }
            if (nextProps.getAllProductsSuccess) {
                nextProps.getLicensesList();
                return {
                    allProducts: nextProps.getAllProductsSuccess.response,
                    isFetchingProducts: false,
                }
            }
            if (nextProps.tenantListSuccess) {
                return {
                    tenantsList: nextProps.tenantListSuccess.response.map(el => { return { value: el.tenantName, label: el.tenantName } }),
                }
            }
            if (nextProps.tenantListFailure) {
                nextProps.showNotification("error", propData)
                return {
                    tenantsList: [],
                }
            }
            if (nextProps.getAllProductsFailure) {
                return {
                    isFetchingAddons: false,
                }
            }
            if (nextProps.licensesListSuccess) {
                let license = propData.find(el => el.tenantCategory === "FLEX"),
                    allProducts = cloneDeep(state.allProducts),
                    features = cloneDeep(state.features);

                features.map(feature => {
                    feature.products.map(productObj => {
                        productObj.basePlanResourseCount = license[productObj.name];
                        productObj.id = allProducts.filter(product => product.name === productObj.name)[0].id;
                    })
                })
                allProducts.map(product => {
                    product.basePlanResourseCount = license[product.name];
                })
                return {
                    features,
                    allProducts,
                }

            }

            if (nextProps.imageUploadSuccess) {
                let { secureUrl } = nextProps.imageUploadSuccess.response
                let payload = produce(state.payload, draftState => {
                    draftState.imageUrl = secureUrl;
                })

                return {
                    payload,
                    isUploadingImage: false,
                };
            }
            if (nextProps.imageUploadFailure) {
                return {
                    isUploadingImage: false,
                }
            }
            if (nextProps.saveAddOnsSuccess) {
                nextProps.getAllAddOns();
                return {
                    isAddOrEditAddOn: false,
                    payload: cloneDeep(DEFAULT_PAYLOAD)
                }

            }
            if (nextProps.saveAddOnsFailure) {
                return {
                    isFetchingAddons: false,
                }
            }
            if (nextProps.deleteAddOnsSuccess) {
                let id = nextProps.deleteAddOnsSuccess.deletedAddonId;
                let allAddOns = produce(state.allAddOns, draftState => {
                    let index = draftState.findIndex(prd => prd.id === id);
                    draftState.splice(index, 1);
                })
                nextProps.showNotification("success", propData)
                return {
                    isFetchingAddons: false,
                    allAddOns,
                }
            }
            if (nextProps.deleteAddOnsFailure) {
                return {
                    isFetchingAddons: false,
                }
            }

        }
        return null;
    }

    componentDidUpdate(prevProps, prevState) {
        let newProp = Object.keys(SELECTORS).find(prop => this.props[prop])
        if (newProp) {
            if (newProp.includes("Failure")) {
                this.props.showNotification("error", this.props[newProp].error)
            }
            this.props.resetToInitialState(newProp)
        }
    }

    getColumns = () => {
        return [
            {
                Header: "Name", width: 15,
                filterable: true,
                sortable: true,
                accessor: "name",
                cell: (row) => (
                    <React.Fragment>
                        {/*<div className="list-view-icon">{row.imageUrl ? <img src={row.imageUrl} /> : <i className="fad fa-money-bill-wave text-green"></i>}</div>*/}
                        <h6 className="text-theme fw-600">{row.name}</h6>
                        <p>{`${row.description || "-"}`}</p>
                    </React.Fragment>
                )
            },
            {
                Header: "Category", width: 15,
                filterable: true,
                accessor: "category",
                sortable: true,
                filter: ({ value, onChangeHandler, accessor }) => (
                    <select
                        className="form-control"
                        onChange={event => onChangeHandler(event, accessor)}
                        value={value != "" ? value : ''}
                    >
                        <option value="">ALL</option>
                        {categoryOption.map((el, index) => <option key={index} value={el.value}>{el.label}</option>)}
                    </select>),
                cell: (row) => (
                    <h6>{row.category} </h6>
                )
            },
            {
                Header: "Published", width: 10,
                filterable: true,
                accessor: "isPublished",
                filter: ({ value, onChangeHandler, accessor }) => (
                    <select
                        className="form-control"
                        onChange={event => onChangeHandler(event, accessor)}
                        value={value != "" ? value : ''}
                    >
                        <option value="">ALL</option>
                        <option value={"true"}>Yes</option>
                        <option value={"false"}>No</option>
                    </select>),
                cell: (row) => (
                    row.isPublished ?
                        <h6 className="text-green"><i className="fad fa-check-circle mr-2"></i>Yes</h6> :
                        <h6 className="text-red"><i className="fad fa-times-circle mr-2"></i>No</h6>
                )
            },
            {
                Header: "Type", width: 10,
                filterable: true,
                accessor: "isPrivate",
                filter: ({ value, onChangeHandler, accessor }) => (
                    <select
                        className="form-control"
                        onChange={event => onChangeHandler(event, accessor)}
                        value={value ? value : 'all'}
                    >
                        <option value="">ALL</option>
                        <option value={"true"}>Private</option>
                        <option value={"false"}>Public</option>
                    </select>),
                cell: (row) =>
                    row.isPrivate ?
                        <h6 className="text-red"><i className="far fa-lock-alt mr-2"></i>Private</h6> :
                        <h6 className="text-indigo"><i className="far fa-globe mr-2"></i>Public</h6>
            },
            {
                Header: "Amount", width: 10,
                cell: (row) =>
                    <h6 className="text-dark-theme fw-600">${row.amount} <small className="text-content">USD</small></h6>
            },
            {
                Header: "Subscriptions", width: 10,
                cell: (row) => (
                    <span className="alert alert-warning list-view-badge">{row.noOfTenantsSubscribed} </span>
                )
            },
            {
                Header: "Products", width: 10,
                cell: (row) =>
                    <span className="alert alert-primary list-view-badge">{row.products.length}</span>
            },
            { Header: "Created At", width: 10,
                cell: row => (
                    <h6>{convertTimestampToDate(row.createdAt)}</h6>
                )
            },
            {
                Header: "Actions", width: 10, cell: (row) => (
                    <div className="button-group">
                        <button className="btn-transparent btn-transparent-blue" onClick={() => this.editAddOnHandler(row)} data-tooltip data-tooltip-text="Edit" data-tooltip-place="bottom">
                            <i className="far fa-pencil"></i>
                        </button>
                        <button className="btn-transparent btn-transparent-red" disabled={row.noOfTenantsSubscribed !== 0} data-tooltip data-tooltip-text="Delete" data-tooltip-place="bottom" onClick={() => this.setState({ confirmState: true, selectedAddOnBeDeleted: row.name, addOntobeDeleted: row.id })}>
                            <i className="far fa-trash-alt"></i>
                        </button>
                    </div>
                )
            },
        ]
    }

    getStatusBarClass = (row) => {
        if (row.isPublished)
            return { className: "green", tooltipText: "Published" }
        return { className: "red", tooltipText: "Not Published" }
    }

    addOnChangeHandler = ({ currentTarget }) => {
        let payload = produce(this.state.payload, draftState => {
            if (currentTarget.id === "isPublished") {
                draftState[currentTarget.id] = !draftState.isPublished
            } else if (currentTarget.id === "isPrivate") {
                draftState[currentTarget.id] = currentTarget.checked
                draftState.tenantNames = []
            }
            else if (currentTarget.id === "oneTimePurchase") {
                draftState[currentTarget.id] = currentTarget.checked
            }
            else {
                draftState[currentTarget.id] = currentTarget.value;
            }
        })
        this.setState({ payload })
    }

    productListHandler = ({ currentTarget }) => {
        let payload = produce(this.state.payload, draftState => {
            if (currentTarget.checked || currentTarget.id.includes("count")) {
                if (currentTarget.id.includes("count")) {
                    let id = currentTarget.id.split("_")[1],
                        productIndex = draftState.products.findIndex(prd => prd.id === id);
                    draftState.products[productIndex].count = currentTarget.value
                }
                else {
                    draftState.products.push(
                        {
                            id: currentTarget.id,
                            count: 10,
                        })
                }
            } else {
                draftState.products.splice(draftState.products.findIndex(prd => prd.id === currentTarget.id), 1)
            }
        })
        this.setState({ payload })
    }

    getMiniLoader = () => {
        return <div className="inner-loader-wrapper flex-100" style={{ height: 'calc(100vh - 194px)' }}>
            <div className="inner-loader-content">
                <i className="fad fa-sync-alt fa-spin"></i>
            </div>
        </div>
    }

    uploadAddOnImage = (file) => {
        this.setState({
            isUploadingImage: true,
        }, () => this.props.imageUploadHandler({ file: file, fileType: "LOGOS" }))
    }

    editAddOnHandler = (row) => {
        let payload = row
        if (payload && payload.isPrivate) {
            payload.tenantNames = payload.tenantNames.map(el => { return { value: el, label: el } })
        }
        this.setState(prevState => ({
            isAddOrEditAddOn: true,
            payload: payload || prevState.payload,
            isFetchingProducts: !this.state.allProducts.length
        }), () => {
            !this.state.allProducts.length && this.props.getAllProducts();
        })
    }

    cancelClicked = () => {
        this.setState({
            addOntobeDeleted: null,
            confirmState: false
        })
    }

    goBackToListingPage = () => {
        if (this.state.isAddOrEditAddOn) {
            this.setState({
                payload: cloneDeep(DEFAULT_PAYLOAD),
                isAddOrEditAddOn: false
            })
        }
    }

    submitHandler = () => {
        let payload = produce(this.state.payload, draftState => {
            draftState.tenantNames = draftState.tenantNames.map(el => el.value)
        })
        if (payload.isPrivate && !payload.tenantNames.length) {
            return this.props.showNotification("error", "Tenants mandatory for private packages")
        }
        if ((!payload.isPrivate) && !payload.amount) {
            return this.props.showNotification("error", "Amount cannot be zero for public packages")
        }
        this.setState({
            isFetchingAddons: true
        }, () => {
            this.props.saveAddOns(payload)
        })
    }

    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>Add-Ons</title>
                    <meta name="description" content="Description of Add-Ons" />
                </Helmet>

                {this.state.isFetchingAddons ? <Loader /> : this.state.isAddOrEditAddOn ?
                    <React.Fragment>
                        <header className="content-header">
                            <div className="d-flex">
                                <div className="flex-60">
                                    <div className="d-flex">
                                        <h6 className="previous" onClick={this.goBackToListingPage}>Add-Ons</h6>
                                        <h6 className="active">Add New</h6></div>
                                </div>
                                <div className="flex-40 text-right">
                                    <div className="content-header-group">
                                        <button className="btn btn-light" data-tooltip data-tooltip-text="Back" data-tooltip-place="bottom" onClick={this.goBackToListingPage}>
                                            <i className="fas fa-angle-left"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </header>

                        <div className="content-body">
                            <div className="form-content-wrapper form-content-wrapper-left">
                                <div className="form-content-box mb-0">
                                    <div className="form-content-header">
                                        <p>Package Products</p>
                                    </div>

                                    <div className="form-content-body">
                                        {this.state.isFetchingProducts ? this.getMiniLoader() :
                                            this.state.features.map((feature, index) =>
                                                <React.Fragment>
                                                    <h6 key={`header_${index}`} className="text-dark-theme fw-600 f-13">{feature.categoryName}</h6>
                                                    <ul key={`ul_${index}`} className="list-style-none d-flex custom-attribute-list">
                                                        {feature.products.map((product, subIndex) => {
                                                            let productIndex = this.state.payload.products.findIndex(prd => prd.id === product.id),
                                                                count = productIndex === -1 ? 0 : this.state.payload.products[productIndex].count;
                                                            return (
                                                                <li key={`${index}_${subIndex}`}>
                                                                    <div className="form-group">
                                                                        <label className={this.state.payload.noOfTenantsSubscribed ? "check-box cursor-not-allowed" : "check-box"}>
                                                                            <span className="check-text">{product.displayName}</span>
                                                                            <input type="checkbox" id={product.id} disabled={this.state.payload.noOfTenantsSubscribed} checked={productIndex !== -1} name="productCheckbox" onChange={this.productListHandler} />
                                                                            <span className="check-mark"></span>
                                                                        </label>
                                                                        <input type="number" className="form-control" id={"count" + "_" + product.id} onChange={productIndex === -1 ? null : this.productListHandler} value={count} disabled={this.state.payload.noOfTenantsSubscribed || productIndex === -1} />
                                                                        <p className="text-content mb-0">Base Package : <strong className="text-theme">{product.basePlanResourseCount ? product.basePlanResourseCount : 0}</strong></p>
                                                                    </div>
                                                                </li>
                                                            )
                                                        }
                                                        )}
                                                    </ul>
                                                </React.Fragment>
                                            )
                                        }
                                    </div>
                                </div>
                            </div>

                            <div className="form-info-wrapper form-info-wrapper-left">
                                <div className="form-info-header">
                                    <h5>Basic Information</h5>
                                </div>
                                <div className="form-info-body form-info-body-adjust">
                                    <div className="form-group">
                                        <label className="form-group-label">Category : <i className="fas fa-asterisk form-group-required"></i>
                                            <ReactSelect
                                                className="form-control-multi-select"
                                                value={categoryOption.find(el => el.value == this.state.payload.category)}
                                                onChange={(valueObj) => {
                                                    let payload = produce(this.state.payload, draftState => {
                                                        draftState.category = valueObj.value;
                                                    })
                                                    this.setState({ payload })
                                                }}
                                                options={categoryOption}
                                            />
                                        </label>
                                    </div>
                                    <div className="form-group">
                                        <label className="form-group-label">Name : <i className="fas fa-asterisk form-group-required"></i></label>
                                        <input type="text" id="name" name="name" className="form-control" value={this.state.payload.name} onChange={this.addOnChangeHandler} />
                                    </div>

                                    <div className="form-group">
                                        <label className="form-group-label">Description : </label>
                                        <input type="text" id="description" name="description" className="form-control" value={this.state.payload.description} onChange={this.addOnChangeHandler} />
                                    </div>

                                    <div className="form-group">
                                        <label className="form-group-label">Amount (In USD) : <i className="fas fa-asterisk form-group-required"></i></label>
                                        <input type="number" id="amount" name="amount" className="form-control" value={this.state.payload.amount} disabled={this.state.payload.noOfTenantsSubscribed} onChange={this.addOnChangeHandler} />
                                    </div>

                                    {/* <div className="form-group">
                                        <label className="form-group-label">Image :
                                            <button type="button" className="btn btn-link float-right p-0 f-12" disabled={!this.state.payload.imageUrl} onClick={() => {
                                                let payload = produce(this.state.payload, draftState => {
                                                    draftState.imageUrl = ""
                                                })
                                                this.setState({
                                                    payload
                                                })
                                            }}>
                                                <i className="far fa-redo-alt"></i>Reset
                                            </button>
                                        </label>
                                        <div className="file-upload-box">
                                            <ImageUploader
                                                loader={this.state.isUploadingImage}
                                                imagePath={this.state.payload.imageUrl}
                                                uploadHandler={this.uploadAddOnImage}
                                                classNames={{ blankUploader: "file-upload-form" }}
                                            />
                                        </div>

                                    </div> */}

                                    <div className="form-group">
                                        <label className="form-group-label">Publish :
                                            <div className={`toggle-switch-wrapper ${this.state.payload.id ? "toggle-switch-wrapper-disabled" : ''} float-right`}>
                                                <label className={this.state.payload.noOfTenantsSubscribed ? "toggle-switch cursor-not-allowed" : "toggle-switch"} >
                                                    <input type="checkbox" id="isPublished" checked={this.state.payload.isPublished} disabled={this.state.payload.noOfTenantsSubscribed} onChange={this.addOnChangeHandler} />
                                                    <span className="toggle-switch-slider"></span>
                                                </label>
                                            </div>
                                        </label>
                                    </div>
                                    <div className="form-group">
                                        <label className="form-group-label">is Private Package ?
                                            <div className={`toggle-switch-wrapper ${this.state.payload.id ? "toggle-switch-wrapper-disabled" : ''} float-right`}>
                                                <label className={this.state.payload.id ? "toggle-switch cursor-not-allowed" : "toggle-switch"} >
                                                    <input type="checkbox" id="isPrivate" checked={this.state.payload.isPrivate} disabled={this.state.payload.id} onChange={this.addOnChangeHandler} />
                                                    <span className="toggle-switch-slider"></span>
                                                </label>
                                            </div>
                                        </label>
                                    </div>
                                    {this.state.payload.isPrivate &&
                                        <div className="form-group">
                                            <label className="form-group-label">is One Time Purchase ?
                                            <div className={`toggle-switch-wrapper ${this.state.payload.id ? "toggle-switch-wrapper-disabled" : ''} float-right`}>
                                                    <label className={this.state.payload.id ? "toggle-switch cursor-not-allowed" : "toggle-switch"} >
                                                        <input type="checkbox" id="oneTimePurchase" checked={this.state.payload.oneTimePurchase} disabled={this.state.payload.id} onChange={this.addOnChangeHandler} />
                                                        <span className="toggle-switch-slider"></span>
                                                    </label>
                                                </div>
                                            </label>
                                        </div>
                                    }
                                    {this.state.payload.isPrivate &&
                                        <div className="form-group">
                                            <label className="form-group-label">Tenants :
                                            <ReactSelect
                                                    className="form-control-multi-select"
                                                    value={this.state.payload.tenantNames}
                                                    onChange={(value) => {
                                                        let payload = produce(this.state.payload, draftState => {
                                                            draftState.tenantNames = value;
                                                        })
                                                        this.setState({ payload })
                                                    }}
                                                    options={this.state.tenantsList}
                                                    isMulti={true}
                                                    menuPlacement={"top"}
                                                />
                                            </label>
                                        </div>}
                                </div>

                                <div className="form-info-footer">
                                    <button className="btn btn-light" onClick={this.goBackToListingPage}>Cancel</button>
                                    <button className="btn btn-primary" disabled={this.state.payload.name.trim() === "" || this.state.payload.amount.length === 0 || this.state.payload.category == ""} onClick={this.submitHandler}>{this.state.payload.id ? "Update" : "Save"}</button>
                                </div>
                            </div>
                        </div>
                    </React.Fragment>
                    :
                    <React.Fragment>
                        <header className="content-header">
                            <div className="d-flex">
                                <div className="flex-60">
                                    <h6>Add-Ons
                                        <span className="content-header-badge-group">
                                            <span className="content-header-badge-item">
                                                <span className="badge badge-pill badge-primary">{this.state.allAddOns.length}</span>
                                            </span>
                                        </span>
                                    </h6>
                                </div>
                                <div className="flex-40 text-right">
                                    <div className="content-header-group">
                                        <button className={this.state.toggleView === false ? "btn btn-light active" : "btn btn-light"} disabled={this.state.isFetching} data-tooltip="true" data-tooltip-text="List View" data-tooltip-place="bottom" onClick={() => this.setState({ toggleView: false })}><i className="fad fa-list-ul"></i></button>
                                        <button className={this.state.toggleView === true ? "btn btn-light active" : "btn btn-light"} disabled={this.state.isFetching} data-tooltip="true" data-tooltip-text="Card View" data-tooltip-place="bottom" onClick={() => this.setState({ toggleView: true })}><i className="fad fa-table"></i></button>
                                        <button className="btn btn-light" data-tooltip data-tooltip-text="Refresh" data-tooltip-place="bottom" onClick={() => this.setState({ isFetchingAddons: true }, () => this.props.getAllAddOns())}>
                                            <i className="far fa-sync-alt"></i>
                                        </button>
                                        <button className="btn btn-primary" data-tooltip data-tooltip-text="Add AddOn" data-tooltip-place="bottom" onClick={() => this.editAddOnHandler()}>
                                            <i className="far fa-plus"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </header>

                        <div className="content-body">
                            {!this.state.toggleView ?
                                <ListingTable
                                    data={this.state.allAddOns}
                                    columns={this.getColumns()}
                                    statusBar={this.getStatusBarClass}
                                />
                                :
                                <ul className="card-view-list" id="deviceListCard">
                                    {this.state.allAddOns.map((item, index) =>
                                        <li key={index}>
                                            <div className="card-view-box">
                                                <div className="card-view-header pd-l-10">
                                                    <span data-tooltip data-tooltip-text={item.isPublished ? "Published" : "Not-Published"} data-tooltip-place="bottom" className={item.isPublished ? "card-view-header-status bg-success" : "card-view-header-status bg-danger"}></span>
                                                    <span className={item.isPublished ? "alert alert-success" : "alert alert-danger"}>Published - {item.isPublished ? "Yes" : "No"}</span>
                                                    <span className="alert alert-info mr-l-10">Amount - <strong>${item.amount}</strong> <small className="text-content">USD</small></span>
                                                    <div className="dropdown">
                                                        <button className="btn-transparent btn-transparent-gray" type="button" data-toggle="dropdown">
                                                            <i className="fas fa-ellipsis-v"></i>
                                                        </button>
                                                        <div className="dropdown-menu">
                                                            <button className="dropdown-item" onClick={() => this.editAddOnHandler(item)} >
                                                                <i className="far fa-pencil"></i>Edit
                                                            </button>
                                                            <button className="dropdown-item" disabled={item.noOfTenantsSubscribed !== 0}  onClick={() => this.setState({ confirmState: true, selectedAddOnBeDeleted: item.name, addOntobeDeleted: item.id })}>
                                                                <i className="far fa-trash-alt"></i>Delete
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="card-view-body">
                                                    <div className="card-view-icon">
                                                        <i className="fad fa-cart-plus"></i>
                                                    </div>
                                                    <h6><i className="far fa-address-book"></i>{item.name ? item.name : "N/A"}</h6>
                                                    <p><i className="far fa-file-alt"></i>{item.description ? item.description : "N/A"}</p>
                                                    <p><i className="far fa-clipboard-list-check"></i>{item.category}</p>
                                                    <p>
                                                        <span className="badge badge-pill alert-warning mr-r-7">Subscriptions - {item.noOfTenantsSubscribed}</span>
                                                        <span className="badge badge-pill alert-primary">Products - {item.products.length}</span>
                                                    </p>
                                                </div>
                                                <div className="card-view-footer d-flex">
                                                    <div className="flex-50 p-3">
                                                        <h6>Created</h6>
                                                        <p><strong>At - </strong>{new Date(item.createdAt).toLocaleString('en-US', { createdAt: localStorage.createdAt })}</p>
                                                    </div>
                                                    <div className="flex-50 p-3">
                                                        <h6>Type</h6>
                                                        {item.isPrivate ?
                                                            <p className="text-red"><i className="far fa-lock-alt mr-2"></i>Private</p> :
                                                            <p className="text-indigo"><i className="far fa-globe mr-2"></i>Public</p>
                                                        }
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    )}
                                </ul>
                            }
                        </div>
                    </React.Fragment>
                }
                {
                    this.state.confirmState &&
                    <ConfirmModel
                        status={"delete"}
                        delete={""}
                        deleteName={this.state.selectedAddOnBeDeleted}
                        confirmClicked={() => {
                            this.setState({
                                isFetchingAddons: true,
                                confirmState: false,

                            }, () => { this.props.deleteAddOns(this.state.addOntobeDeleted) })
                        }}
                        cancelClicked={() => {
                            this.cancelClicked()
                        }}
                    />
                }
            </React.Fragment >
        );
    }
}

ManageAddOn.propTypes = {
    dispatch: PropTypes.func.isRequired
};


let allSelectors = {}
Object.entries(SELECTORS).map(([key, value]) => {
    allSelectors[key] = value()
})

const mapStateToProps = createStructuredSelector(allSelectors)

function mapDispatchToProps(dispatch) {
    let allActions = { dispatch }
    Object.entries(ACTIONS).map(([key, value]) => {
        allActions[key] = (...args) => dispatch(value(...args))
    })
    return allActions
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "manageAddOn", reducer });
const withSaga = injectSaga({ key: "manageAddOn", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(ManageAddOn);
