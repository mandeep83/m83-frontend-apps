/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/*
 *
 * ManageAddOn constants
 *
 */

export const RESET_TO_INITIAL_STATE = "app/ManageAddOn/RESET_TO_INITIAL_STATE";

export const GET_ALL_ADDONS = {
    action : 'app/ManageAddOn/GET_ALL_ADDONS',
    success: "app/ManageAddOn/GET_ALL_ADDONS_SUCCESS",
    failure: "app/ManageAddOn/GET_ALL_ADDONS_FAILURE",
    urlKey: "getAllAddOns",
    successKey: "getAllAddOnsSuccess",
    failureKey: "getAllAddOnsFailure",
    actionName: "getAllAddOns",
}

export const GET_ALL_PRODUCTS = {
    action : 'app/ManageAddOn/GET_ALL_PRODUCTS',
    success: "app/ManageAddOn/GET_ALL_PRODUCTS_SUCCESS",
    failure: "app/ManageAddOn/GET_ALL_PRODUCTS_FAILURE",
    urlKey: "getAllProducts",
    successKey: "getAllProductsSuccess",
    failureKey: "getAllProductsFailure",
    actionName: "getAllProducts",
}

export const IMAGE_UPLOAD_HANDLER = {
	action: 'app/ManageAddOn/IMAGE_UPLOAD_HANDLER',
	success: "app/ManageAddOn/IMAGE_UPLOAD_HANDLER_SUCCESS",
	failure: "app/ManageAddOn/IMAGE_UPLOAD_HANDLER_FAILURE",
	urlKey: "uploadFile",
	successKey: "imageUploadSuccess",
	failureKey: "imageUploadFailure",
	actionName: "imageUploadHandler",
	actionArguments: ["payload"]
}

export const SAVE_ADDONS={
    action: 'app/ManageAddOn/SAVE_ADDONS',
	success: "app/ManageAddOn/SAVE_ADDONS_SUCCESS",
	failure: "app/ManageAddOn/SAVE_ADDONS_FAILURE",
	urlKey: "saveAddOns",
	successKey: "saveAddOnsSuccess",
	failureKey: "saveAddOnsFailure",
	actionName: "saveAddOns",
	actionArguments: ["payload"]
}

export const DELETE_ADDONS = {
    action : 'app/ManageAddOn/DELETE_ADDONS',
    success: "app/ManageAddOn/DELETE_ADDONS_SUCCESS",
    failure: "app/ManageAddOn/DELETE_ADDONS_FAILURE",
    urlKey: "deleteAddOns",
    successKey: "deleteAddOnsSuccess",
    failureKey: "deleteAddOnsFailure",
    actionName: "deleteAddOns",
    actionArguments: ["id"]
}

export const GET_LICENSES_LIST = {
    action : 'app/ManageAddOn/GET_LICENSES_LIST',
    success: "app/ManageAddOn/GET_LICENSES_LIST_SUCCESS",
    failure: "app/ManageAddOn/GET_LICENSES_LIST_FAILURE",
    urlKey: "getAllLicenses",
    successKey: "licensesListSuccess",
    failureKey: "licensesListFailure",
    actionName: "getLicensesList",
    actionArguments : []
}

export const GET_TENANT_LIST = {
    action : 'app/ManageAddOn/GET_TENANT_LIST',
    success: "app/ManageAddOn/GET_TENANT_LIST_SUCCESS",
    failure: "app/ManageAddOn/GET_TENANT_LIST_FAILURE",
    urlKey: "getAccountList",
    successKey: "tenantListSuccess",
    failureKey: "tenantListFailure",
    actionName: "getTenantList",
    actionArguments : []
}