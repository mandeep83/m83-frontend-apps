/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * TermsAndConditions
 *
 */

import React from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {Helmet} from "react-helmet";
import {FormattedMessage} from "react-intl";
import {createStructuredSelector} from "reselect";
import {compose} from "redux";

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import makeSelectTermsAndConditions from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import messages from "./messages";

/* eslint-disable react/prefer-stateless-function */
export class TermsAndConditions extends React.Component {
    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>TermsAndConditions</title>
                    <meta
                        name="description"
                        content="Description of Terms And Conditions"
                    />
                </Helmet>
    
                <div className="content-wrapper">
                    <div className="content-item pl-0 pb-0">
                        <header className="content-header border-bottom d-flex" style={{left: 0}}>
                            <div className="flex-60">
                                <h6 className="text-gray">Terms and Conditions Agreement - <span className="text-primary">The {localStorage.tenantType == "MAKER" ? "iFlex83" : "Flex83"} Platform</span></h6>
                            </div>
                        </header>
    
                        {localStorage.tenantType == "MAKER" ?
                            <div className="content-body">
                                <div className="content-text-box">
                                    <p>
                                        SHOULD NO CUSTOM LICENSE AGREEMENT EXIST BETWEEN YOU (THE “LICENSEE”) AND IOT83 LTD. OR ITS AFFILIATES (THE “LICENSOR”) THIS IOT83 TERMS OF SERVICE(S) AGREEMENT (THE “AGREEMENT”) ESTABLISHES THE TERMS OF IOT83 SERVICE OR SERVICES (THE “SERVICES”) FOR USING THE iFLEX83 SAAS PLATFORM, AND ASSOICATED LICENSEE GROUPS AND COMMUNITIES. BY AND SIGNING UP FOR AND ESTABLISHING AN ACCOUNT, YOU (THE “LICENSEE”) AGREE TO THE TERMS AND CONDITIONS OF THIS AGREEMENT. IF YOU ARE USING THE SERVICES ON BEHALF OF A LEGAL ENTITY OTHER THAN YOURSELF IN YOUR PRIVATE CAPACITY, YOU AFFIRM THAT YOU HAVE THE RIGHTS TO BIND SUCH ENTITY OR ENTITIES TO THIS AGREEMENT, AND IN THIS CASE ALL REFERENCES TO “YOU”,”YOUR”, LICENSEE” OR SIMILAR REFERENCES APPLY TO THIS ENTITY.
                                    </p>
                                    <ul>
                                        <div className="content-text-bg"><img src="https://content.iot83.com/m83/misc/m83-logo.png" /></div>
                                        <li>
                                            <h6>Definitions</h6>
                                            <ul>
                                                <li>"IoT83" means IoT83 LTD, a Delaware corporation,  with its principal place of business located at 450 S. Abel St #361625, Milpitas, CA, 95036</li>
                                                <li>"iFlex83 Services" or “iFlex83” or “Product(s)” means the IoT Application Enablement Platform (AEP) offered as a SaaS product by IoT83. This Agreement establishes the terms and conditions between IoT83 and the Licensee (hereafter, the "Parties") for iFlex83 Subscription licensing.</li>
                                                <li>"iFlex83 Subscription(s)" means the iFlex83 subscription options that the Licensee purchases using the iFlex83 portal subscription management services or by other means, such as an executed quote, with IoT83, where such subscription is in good standing.</li>
                                                <li>"Licensee" or “You” or “Your” means the individual or company or other legal entity accepting the Agreement and using the Services either as defined in a separate custom Services addendum or as specified by the subscription to the Services.</li>
                                                <li>"Licensee Data" means electronic data submitted by Licensees or by devices owned or maintained by Licensees, Licensee Partners, or Licensee Customers (if any) transmitted to the iFlex83 Service and the applications created by Licensee in the use of the Services.</li>
                                                <li>"Licensee Applications" means any applications created using iFlex83 that Licensee may use on its own behalf or use to provide benefit to any third party.</li>
                                                <li>"Custom Services" means any services that IoT83 may provide to Licensee beyond the services outlined in this agreement.</li>
                                            </ul>
                                        </li>

                                        <li>
                                            <h6>Licensee Acceptance of Terms and Conditions for Services</h6>
                                            <ul>
                                                <li>By creating an account and using iFlex83 Services, Licensee affirm agreement to the terms and conditions (the "Terms of Service") outlined in this Agreement, as well as the iFlex83 Privacy Policy (the "Privacy Policy").</li>
                                                <li>The Terms of Service apply to Licensee’s utilization of the iFlex83 Services, any Licensee Applications created with iFlex83, and apply to any user groups made available, or other support that may be provided to Licensee.</li>
                                            </ul>
                                        </li>

                                        <li>
                                            <h6>License Grant and Rights of Use</h6>
                                            <ul>
                                                <li>License Grant. Subject to all limitations and restrictions contained herein and subject to the Licensee’s iFlex83 Subscription (including the number of connected Devices, AEP tools and resources licensed, and any Custom Services purchased or licensed by Licensee), licensor grants to Licensee: (i) Rights to use iFlex83 to collect, view, analyze, and manage Licensee Data and Connected Devices; (ii) Rights to use the iFlex83 to create Licensee Applications.</li>
                                                <li>Modifications and Enhancements. IoT83 may modify the iFlex83 in order to enhance or improve the operation of the iFlex83, and such modifications to Services may be performed with or without notice. IoT83 will make reasonable efforts to inform Licensee of any changes to the Services that would be expected to impact Licensee.</li>
                                                <li>Except as expressly provided for in this Agreement, Licensee has no rights to sublicense any of its rights under this Agreement to any Third Party, except with the prior written consent of IoT83, and Licensee may not offer to any Third Party rights or privileges to use the iFlex83 Services, or make any other use of the iFlex83 to any third party.</li>
                                                <li>Additional Restrictions. In no event will Licensee disassemble, decompile, or reverse engineer the iFlex83, IoT83 Intellectual Property, or Confidential Information (as defined herein) or permit others to do so. Disassembling, decompiling, and reverse engineering include, without limitation: (i) converting the Application from a machine-readable form into a human-readable form; (ii) disassembling or decompiling the Application by using any means or methods to translate machine-dependent or machine-independent object code into the original human-readable source code or any approximation thereof; (iii) examining the machine-readable object code that controls the Application's operation and creating the original source code or any approximation thereof by, for example, studying the Application's behavior in response to a variety of inputs; or (iv) performing any other activity related to the Application that could be construed to be reverse engineering, disassembling, or decompiling. To the extent any such activity may be permitted pursuant to written agreement, the results thereof will be deemed Confidential Information subject to the requirements of this Agreement. Licensee may use IoT83 Confidential Information solely in connection with the Application and pursuant to the terms of this Agreement.</li>
                                                <li>Licensee licenses and grants to  IoT83, to the extent needed for IoT83 to provide the Services outlined in this Agreement, the worldwide rights to use, copy, display, and modify Licensee Data, Licensee Applications, and Licensee’s Customer Data (if any) together, and as required to provide and use the Services.</li>
                                                <li>Third Party Software. The Services may contain third party software that requires notices and/or additional terms and conditions. Such required third party software notices and/or additional terms and conditions may be requested from IoT83 and are made a part of and incorporated by reference into this Agreement. By accepting this Agreement, Licensee is also accepting the additional terms and conditions, if any, set forth therein.</li>
                                                <li>The Products are licensed, not sold, to Licensee, notwithstanding the use herein of the words "sale," "sell," "sold," and the like.</li>
                                            </ul>
                                        </li>

                                        <div className="content-text-bg"><img src="https://content.iot83.com/m83/misc/m83-logo.png" /></div>
                                        <li>
                                            <h6>Payment</h6>
                                            <ul>
                                                <li>The fees for the iFlex83 Service are by subscription and are according to the Licensee’s iFlex83 Subscription details, and billing is managed on Licensee Portal. Enterprise, Commercial or Volume Licensees may contact IoT83 to arrange alternative payment methods to be arranged at IoT83’s discretion. Services are billed on a monthly basis and are non-refundable. Licensee is responsible for paying for Services according to the tier of Service subscribed to without exception until such Service is terminated. Upon termination of Services no charge will be billed on the next billing cycle, but any fees owed but not yet paid remain due to IoT83 from Licensee. No partial credit for unused Services will be provided to Licensee. Licensees desiring Custom Services (including but not limited to Application Development, Premium Development Operations (DevOps), and Premium Maintenance) may contact IoT83 to quote pricing and terms for delivery for the Custom Services.</li>
                                                <li>For business, corporate, or enterprise customers, the iFlex83 Services may be billed by invoice. Invoiced services are due to IoT83 Net 30 after receipt of the invoice. Failure of on-time payment may result in loss of Licensee Data and Licensee Applications. Any dispute over charges must be resolved during the Net 30 payment period, and any resolution to such a dispute may be accommodated in the next invoice cycle. Failure to pay on-time will result in a 2% per month finance charge. Should Licensee elect to pay for Services by credit card billing for subsequent periods will be automatic until Services are modified or canceled.</li>
                                                <li>IoT83 reserves the right to change the fees for iFlex83 Services or to create new iFlex83 Subscription tiers at any time. Should changes in fees increase for a Licensee, Licensee will be notified either by email or by notices on the Method83 portal and have the option to change Service tiers or Service Options, or to terminate Services, but it is the responsibility of Licensee to manage their selected tier of Service. Businesses, corporations, or enterprise customers may engage IoT83 directly to establish long term iFlex83 Subscription pricing.</li>
                                                <li>Licensees are responsible to pay any applicable taxes associated or incurred with their purchase of Services. IoT83 is responsible to pay taxes due based on IoT83 income.</li>
                                            </ul>
                                        </li>

                                        <li>
                                            <h6>Hosting</h6>
                                            <ul>
                                                <li>iFlex83 is cloud-hosted by IoT83 and Licensee has no rights or expectations to receive, manage, or otherwise possess the underlying iFlex83 in object code or source code form. Licensee Applications are run as a part of the Licensee’s license to iFlex83, and as such, are also cloud-hosted by IoT83.</li>
                                                <li>Service Availability. Service Provider will use reasonable efforts to achieve Service Provider's availability goals described in the 'Service Level Agreement for iFlex83.' Licensees desiring Custom Service Level Agreement (SLA) may contact IoT83 to quote pricing and terms for delivery for the Custom SLA.</li>
                                                <li>Support Services. Business, corporate, or enterprise customers may request IoT83 to provide custom support or maintenance which will be governed by an addendum to this Agreement.</li>
                                            </ul>
                                        </li>

                                        <li>
                                            <h6>Licensee Responsibility for Licensee Created Applications</h6>
                                            <ul>
                                                <li>Licensee takes full responsibility for the functionality, correctness, and maintainability of Licensee Applications. Licensee understands and agrees that iFlex83 Licensee Application performance and operational scale is dependent on the iFlex83 Subscription resources available for the Licensee Application to use in its operation. It is the Licensee's responsibility to assure that the level of services subscribed to aligns with Licensee's performance and availability demand and expectations.</li>
                                                <li>Licensees desiring Custom Services for Licensee Applications (including but not limited to Application Development, Premium Development Operations (DevOps), and Premium Maintenance) may contact IoT83 to quote pricing and terms for delivery for the Custom Services.</li>
                                            </ul>
                                        </li>

                                        <li>
                                            <h6>Intellectual Property and Software Ownership</h6>
                                            <ul>
                                                <li>IoT83 retains all Intellectual Property rights to the iFlex83 Services, the underlying technology, software, design, concepts, trade secrets, work-flows, services delivery systems, and platforms used to provide the iFlex83 Services and no grant or title to this Intellectual Property is provided to Licensee under this Agreement.</li>
                                                <li>For feature requests or suggestions that Licensees provide to IoT83 that may be incorporated into iFlex83, the Licensee grants to IoT83 an irrevocable, transferable, worldwide, perpetual, royalty-free license for such Intellectual Property to IoT83. Further, Licensee agrees that any such changes that Licensee requests and IoT83 provides, may incorporate customizations, inventions, or other Intellectual Property, and that by providing such customizations or changes, IoT83 is not conveying, relinquishing rights, or providing Licensee license to such underlying Intellectual Property.</li>
                                                <li>Insomuch as iFlex83 incorporates 3rd party software, no license to such software is conveyed in this Agreement, other than the iFlex83 Services rights conveyed to Licensee under this Agreement.</li>
                                                <li>Licensee owns any Intellectual Property related to the Licensee’s application itself that runs using iFlex83, but Licensee acknowledges that no conveyance of the iFlex83 Services or IoT83 Intellectual Property is provided or in any way conveyed to Licensee.</li>
                                            </ul>
                                        </li>

                                        <div className="content-text-bg"><img src="https://content.iot83.com/m83/misc/m83-logo.png" /></div>
                                        <li>
                                            <h6>Confidential Information</h6>
                                            <ul>
                                                <li>For the purposes of this Agreement, Confidential Information means all non-public proprietary or confidential information of Disclosing Party, in oral, visual, written, electronic or other tangible or intangible form, whether or not marked or designated as “confidential,” and all notes, analyses, summaries and other materials prepared by Recipient or any of its Representatives that contain, are based on or otherwise reflect, to any degree, any of the foregoing (“Notes”); provided, however, that Confidential Information does not include any information that: (a) is or becomes generally available to the public other than as a result of Recipient’s or its Representatives’ material breach of this Agreement; (b) is obtained by Recipient or its Representatives on a non-confidential basis from a third-party that, to Recipient’s knowledge, was not legally or contractually restricted from disclosing such information; (c) Recipient establishes by documentary evidence, was in Recipient’s or its Representatives’ possession prior to Disclosing Party’s disclosure hereunder; or (d) Recipient establishes by documentary evidence, was or is independently developed by Recipient or its Representatives without using any Confidential Information.</li>
                                                <li>Disclosing Party retains its entire right, title and interest in and to all Confidential Information, and no disclosure of Confidential Information hereunder will be construed as a license, assignment or other transfer of any such right, title and interest to Recipient or any other person.</li>
                                                <li>The Receiving Party will use the same degree of care in protecting Confidential Information received from the Receiving Party that it uses to protect its own Confidential Information, but in no event less that reasonable care and protection not to use any Confidential Information outside of the scope of the Agreement. Further, the Receiving Party shall limit access of the Disclosing Party Confidential Information to only employees, contractors, or other agents that require such assess to execute under the Agreement.</li>
                                                <li>The Parties acknowledge and agree that any breach of this the Confidential Information terms of this Agreement may cause injury to Disclosing Party for which money damages would be an inadequate remedy and that, in addition to remedies at law, Disclosing Party is entitled to equitable relief as a remedy for any such breach.</li>
                                                <li>Licensee shall not use any of IoT83's Confidential Information other than pursuant to and in accordance with the exercise of any of the benefits of Agreement. Without limiting the generality of the foregoing, Licensee shall not use IoT83's Confidential Information (i) for determining if any features, functions or processes disclosed by the IoT83 Confidential Information are covered by any patents or patent applications owned by Licensee; or (ii) for developing technology or products which avoid any of IoT83's Intellectual Property licensed hereunder; or (iii) as a reference for modifying existing patents or patent applications or creating any continuation, continuation in part, or extension of existing patents or patent applications or (iv) for generating data for publication or disclosure to third parties, which  compares  the  performance  or  functionality  of  the IoT83 Software or Products.</li>
                                                <li>Confidential Information Exceptions: If Recipient or any of its Representatives is required by a valid legal order to disclose any Confidential Information, Recipient shall notify Disclosing Party of such requirements so that Disclosing Party may seek, at Disclosing Party’s expense, a protective order or other remedy, and Recipient shall reasonably assist Disclosing Party therewith. If Recipient remains legally compelled to make such disclosure, it shall: (a) only disclose that portion of the Confidential Information that it is required to disclose; and (b) use reasonable efforts to ensure that such Confidential Information is afforded confidential treatment.</li>
                                            </ul>
                                        </li>

                                        <li>
                                            <h6>Warranties</h6>
                                            <ul>
                                                <li>Licensee Warranty.  It is the sole and exclusive responsibility of Licensee to determine the suitability of any and all iFlex83 Services or iFlex83 Subscriptions and Licensee Applications for Licensee's intended purposes.  The Licensee will be responsible for making all warranties to any consumer of Licensee’s applications using the iFlex83 Services.  Licensee represents and warrants that (i) it has the requisite power, authority and resources to enter into this Agreement and to perform its obligations pursuant to this Agreement; (ii) if a business, corporation, or enterprise, that it is duly organized, validly existing and in good standing under the laws of its jurisdiction of organization; (iii) it is not bound by any agreement or obligation (and will not enter into or assume any agreement or obligation) that could interfere with its obligations under this Agreement; and (iv) in connection with its obligations under this Agreement, it will comply with all applicable laws and regulations and has obtained all applicable permits, rights and licenses.</li>
                                                <li>IoT83 Warranty.  IoT83 warrants that the Products will perform substantially in accordance with the Documentation under regular use and will, in keeping with the service and maintenance commitments of the Support Agreement for iFlex83, remediate any Product failure according to the terms established there.  In the event of a breach of this warranty, as IoT83's sole obligation and Licensee's sole remedy, IoT83 will, at its option and expense, correct, repair or replace the non-conforming Product.</li>
                                                <li>Exclusions.  The warranties set forth in this Section and IoT83's support and maintenance obligations hereunder will not apply to any Products or services to the extent the defect or non-conformance is due to (i) use of the Product other than as specified by IoT83; (ii) incorrect implementation of the Product in the creation of a Licensee Application by Licensee; (iii) any error, act or omission by anyone other than IoT83 or IoT83's agents, employees, and subcontractors; or (iv) where written notice of the defect has not been given to IoT83 by Licensee within the applicable warranty period.</li>
                                                <li>IoT83 expressly makes no warranties of any kind for iFlex83, or for any 3rd Party component of the iFlex83 Services, if any, unless specified between IoT83 and Licensee in a separate signed and dated agreement. No implied statutory and other warranties are provided, including fitness for a particular purpose or application, or the suitability of the Services for any specific Licensee purposes. IoT83 does not provide any warranty as to continuous uptime, error free operation, security, latency, or timely response of the Services.</li>
                                                <li>Disclaimer. THE ABOVE WARRANTIES ARE THE ONLY WARRANTIES OF ANY KIND MADE BY IoT83 WITH RESPECT TO THE PRODUCTS. IoT83 HEREBY EXPRESSLY DISCLAIMS ALL OTHER WARRANTIES WITH RESPECT TO THE PRODUCTS, EITHER EXPRESS OR IMPLIED, INCLUDING WARRANTIES OF MERCHANTABILITY, FITNESS FOR ANY PARTICULAR PURPOSE, TITLE, NON-INFRINGEMENT, NON-INTERFERENCE WITH ENJOYMENT, ACCURACY, INTEGRATION, AS WELL AS ALL WARRANTIES ARISING BY USAGE OF TRADE, COURSE OF DEALING OR COURSE OF PERFORMANCE.  IoT83 DOES NOT WARRANT THAT (1) THE PRODUCTS WILL MEET LICENSEE'S REQUIREMENTS, (2) OPERATION OF THE PRODUCTS WILL BE UNINTERRUPTED OR ERROR-FREE, OR (3) DEFECTS WILL BE CORRECTED.  EXCEPT AS EXPRESSLY PROVIDED IN THIS SECTION 9, THE PRODUCTS ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND.</li>
                                            </ul>
                                        </li>

                                        <li>
                                            <h6>Indemnity</h6>
                                            <ul>
                                                <li>By accepting this Agreement, the Licensee undertakes to indemnify and hold harmless IoT83 as well as its directors, employees, managers, agents and any IoT83 subsidiary and /or parent company, from any prejudice resulting from the violation, by the Licensee, of the Terms and Conditions of this Agreement, the legal obligations or the rights of third parties.</li>
                                                <li>Further, each party will indemnify, defend and hold the other harmless against any claims, damages, and expenses by any third party resulting from any acts of omissions of Indemnifying party relating to its activities associated with this Agreement, their breach of this agreement, any distortion, fabrication or misrepresentation relating to the other party, the Services, or the Agreement itself.</li>
                                            </ul>
                                        </li>

                                        <div className="content-text-bg"><img src="https://content.iot83.com/m83/misc/m83-logo.png" /></div>
                                        <li>
                                            <h6>Limitations of Liability</h6>
                                            <ul>
                                                <li>Except for breaches of confidentiality or indemnification obligations, each party waives any and all claims against the other for claims for lost profit, revenues, data, interruptions of service, or consequential, incidental or any special damages, arising out of any services or work product associated with this Agreement.</li>
                                                <li>Further Licensee agrees that the maximum liability for IoT83 for any claim related to this Agreement is limited to the amount of any fee received by IoT83 from Licensee in the preceding six months.</li>
                                                <li>NOTWITHSTANDING ANYTHING TO THE CONTRARY CONTAINED IN THIS AGREEMENT, THE MAXIMUM LIABILITY OF IOT83 TO LICENSEE IN AGGREGATE FOR ALL CLAIMS MADE AGAINST IOT83 IN CONTRACT TORT OR OTHERWISE UNDER OR IN CONNECTION WITH THE SUBJECT MATTER OF THIS AGREEMENT SHALL NOT EXCEED THE SUMS PAID (IF ANY) BY LICENSEE TO IOT83 IN RESPECT OF THIS AGREEMENT.</li>
                                            </ul>
                                        </li>

                                        <li>
                                            <h6>Agreement Term and Termination</h6>
                                            <ul>
                                                <li>The Agreement between IoT83 and Licensee begins on the date Licensee accepts the Agreement and continues until terminated by either party in accordance with this Agreement. In the event Licensee terminates subscription to Services, the Agreement terminates, subject to other provisions of this Agreement. Subject to change in efforts to best manage Services, IoT83 will provide a grace period to retain Licensee Data, however beyond any grace period, if any, upon termination of Services Licensee Data and Licensee Applications will be deleted.</li>
                                                <li>Licensee may terminate use of the Services and this Agreement at any time for any and for no reason by using the online tools to terminate Licensee subscriptions by invoking the "Delete Account" (or similarly named profile management functions). IoT83 may also terminate Licensee account for any or no cause upon 30-day notice to user or upon Licensee failure to comply with the terms of this Agreement.</li>
                                                <li>In the event of termination any fees due to IoT83 are due Net30 from termination.</li>
                                                <li>Clauses in this Agreement covering IoT83 Property Rights and Intellectual Property rights, Warranties, Limitation of Liability, Indemnity and General Provisions survive any Agreement expiration or termination.</li>
                                            </ul>
                                        </li>

                                        <li>
                                            <h6>Licensee Obligations</h6>
                                            <ul>
                                                <li>Licensees of iFlex83: (i) Provide IoT83 with complete and accurate contact and billing information; and (ii) Notify IoT83 of any discovered unauthorized use of the Services and use all reasonable efforts to prevent such unauthorized use of the Services; and (iii) Be fully responsible for Licensee’s compliance with the Agreement; and (iv) Make no representation to any of Licensee’s business partners and / or customers that attempt to commit IoT83 or its Affiliates to Terms and Conditions outside this Agreement; and (v) Where Licensee enter into agreements with any Licensee’s Customers that rely on the Licensee’s use of iFlex83, Licensee will limit any representation of IoT83’s warranties and liabilities to IoT83 approved warranties and liabilities per this agreement; and (vi) Be fully responsible for the legality with which Licensee gains access to Licensee Data and Licensee Devices, and Licensee Application, including representations as to the accuracy, quality, and integrity of Licensee Data and Licensee Applications and the use of Licensee Data and Licensee Applications by any Third Parties.</li>
                                                <li>Licensees of iFlex83 shall not: (i) Transmit or store information in violation of a third parties rights; (ii) Use the Service for malicious uses including but not limited to fraudulent activities, transmission of malware or malicious code, or attack or attempt disrupt the Services themselves or the Data of another Licensee; (iii) Use or attempt to use the Services for malicious uses including but not limited to fraudulent activities, transmission of malware or malicious code, or attack or attempt disrupt the Services themselves or the Data of another Licensee; (iv) Attempt to re-sell, mirror, or copy the Services; (v) Attempt to gain unauthorized iFlex83 Services or attempt to access iFlex83 Subscriptions beyond Licensee’s authorized subscription; (vi) Attempt to use access as a means to disrupt the operation of the iFlex83 Services, the iFlex83 Services Data, or the iFlex83 Service Cloud Infrastructure.</li>
                                                <li>IoT83 does not prohibit minors from using the iFlex83 without parental consent. By accepting the Terms of Service, Licensee acknowledge that they have the authority to enter into the Agreement regardless of age.</li>
                                                <li>Before entering its data and information, the Licensee shall be obliged to check the same for viruses or other harmful components and to use state of the art anti-virus programs for this purpose. In addition, the Licensee itself shall be responsible for the entry and the maintenance of its data.</li>
                                                <li>Service Provider has the right (but not the obligation) to suspend access to the Application or remove any data or content transmitted via the Application without liability (i) if Service Provider reasonably believes that the Application is being used in violation of the terms of this Agreement or applicable law, (ii) if requested by a law enforcement or government agency or otherwise to comply with applicable law, provided that Licensor shall use commercially reasonable efforts to notify Licensee prior to suspending the access to the Products as permitted under these SaaS Terms, or (iii) as otherwise specified in this Agreement. Information on iFlex83 servers may be unavailable to Licensee during a suspension of access to the Software. IoT83 will use commercially reasonable efforts to give Licensee at least twelve (12) hours’ notice of a suspension unless IoT83 determines in its commercially reasonable judgment that a suspension on shorter or contemporaneous notice is necessary to protect IoT83 or its customers.</li>
                                            </ul>
                                        </li>

                                        <div className="content-text-bg"><img src="https://content.iot83.com/m83/misc/m83-logo.png" /></div>
                                        <li>
                                            <h6>Privacy Policy</h6>
                                            <ul>
                                                <li>
                                                    IoT83 respects the privacy of Licensees, Licensee Data. IoT83's use of any Licensee Personal Information, Licensee Data, or Licensee Applications is for the purposes of operating and managing the Services effectively. By using the Services, Licensee agree to the all subparts of this section of the Agreement:
                                                    <ul>
                                                        <li>Data collected by IoT83 is collected solely to communicate with Licensees and to operate the Services subscribed to by Licensees. Licensee personal information is only shared with Licensee consent unless such sharing of Licensee data is necessary to perform a Licensee request, it is legally required, it is necessary to execute the Terms of Service, IoT83 is using the data to detect fraud, bypass or compromise security of the Services, or other illegal activities, or to otherwise protect the rights of other Licensees, or IoT83 legal rights or IoT83 property.</li>
                                                        <li>Billing processing third party services, as selected by you in the sign-up and subscription management process, will have access to the billing information that you provide, solely to provide billing services as selected by you. IoT83 does not retain any Licensee credit card or billing information for subscribed services but may retain billing information for enterprise or corporate accounts as agreed to by the parties.</li>
                                                        <li>Third party marketing providers may have access to Your email address solely for promoting IoT83 and Method83 programs, but Licensees can opt-out of these programs.</li>
                                                        <li>IoT83 will collect Licensee names, addresses, telephone numbers, business names, email addresses, log-in credentials, information about tiers of Service selected, transactions made with us, customer service and support records, and any other information Licensees provide to IoT83 for the purposes of operating and managing the Services.</li>
                                                        <li>Licensee Data and Licensee Applications are retained privately to the Licensee and are accessible only through authenticated Licensee access to the Services.</li>
                                                        <li>IoT83 reserves the rights to amend the Privacy Policy at any time, and the use of information IoT83 receives from Licensees is subject to the Privacy Policy currently in effect. Should major changes be made to Privacy Policy, IoT will provide Licensees with email notification of such changes.</li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>

                                        <li>
                                            <h6>Miscellaneous</h6>
                                            <ul>
                                                <li>IoT and Licensee Relationship. Licensees are not authorized to make any sale, contract, representation, obligations, commitment, guarantee, or partnership on behalf of IoT83. IoT83 and Licensee are independent contractors and nothing in this Agreement creates any form of business, technology, or financial partnership. Neither of the Parties are granting any exclusivity to the other Party under this Agreement.</li>
                                                <li>IoT83 and Licensee Communications. Subject to change as needed to adequately provide the Services, IoT83 will contact Licensee using the iFlex83 portal capabilities, by email, or by other means necessary for the delivery and management of the Services. Subject to Licensee management of communication preferences with IoT83, IoT83 may provide promotional emails to Licensees. IoT83 may use software to monitor that emails have been received and opened in order to optimize delivery of Services. For business, corporate, or enterprise customers may request IoT83 specific service point of contact depending on iFlex83 Subscription levels.</li>
                                                <li>Waiver. The waiver by a party of a breach or a default of any provision of this Agreement by the other party will not be construed as a waiver of any succeeding breach of the same or any other provision, nor will any delay or omission on the part of such party to exercise or avail itself of any right, power or privilege that it has, or may have hereunder, operate as a waiver of any right, power or privilege by it.</li>
                                                <li>Foreign Asset Control (“OFAC”). Licensee agrees to abide by Office of Foreign Asset Control regulations, and acknowledge and commit that Licensee is not, nor will Licensee provide, sell, transfer, or otherwise convey Your access of the Services to any country subject to OFAC sanctions, namely Cuba, Iran, North Korea, Syria, and Ukraine's Crimea region.</li>
                                                <li>Export Compliance for Services. Each party to this Agreement will comply with the United States established export laws and regulations as well as other applicable laws in providing or using the Services.</li>
                                                <li>SDN List Control (“OFAC”). Licensee agrees to abide by Office of Foreign Asset Control regulations, and acknowledge and commit that Licensee is not, nor will Licensee provide, sell, transfer, or otherwise convey Your access of the Services to any entity or individuals listed on the Treasury Department's List of Specifically Designated Nationals List (herein, the "SDN List").</li>
                                                <li>Assignments of this Agreement. Neither party to this Agreement may assign any part of this Agreement or any of its rights or responsibilities to another party without written consent of the other party, except in the event of a merger, acquisition, or the sale of substantially all of its assets.</li>
                                                <li>Litigation and Waiver of Jury Trial. The laws of California govern this Agreement and each party agrees to such adjudication for any disputes that arise from this Agreement. Further, each party agrees to waive the right to jury trial in connection with any action or dispute arising or related to this Agreement.</li>
                                                <li>Entire Agreement. This Agreement constitutes the entire agreement between IoT83 and Licensee. The Agreement supersedes any prior discussions or agreements, either written or oral between the parties. Should any portion of this Agreement be deemed to be invalid, the remainder of this Agreement survives intact and remains in full forces and effect.</li>
                                                <li>Force Majeure. In no event will either party have any liability to the other for any delayed performance or nonperformance by a party which results, in whole or in part, directly or indirectly, from any cause beyond the reasonable control of such party.  Such causes will include (but will not be limited to) acts of God, wars, riots, civil disturbances, strikes, labor disputes, fires, storms, floods, earthquakes, natural disasters, inability to obtain or use raw or component materials or parts, labor, devices, facilities, or transportation, and acts of any government or agency thereof.</li>
                                                <li>Counterparts. This Agreement may be executed in multiple counterparts, each of which will be deemed to be an original, and all such counterparts will constitute one and the same instrument. This Agreement may be executed via validated digital signature transmission.</li>
                                                <li>Notices. Notices regarding to or arising out of this agreement may be made by email, certified mail, registered mail or corridor to the address, for the Licensee, as set forth in registration for Services or in any special Services Agreement, and for IoT83 as indicated in said special Services Agreement.</li>
                                                <li>Survival. Sections 2, 3, 6, 7, 8, 9, 10, and 13 shall survive the expiration or termination of this agreement.</li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>

                                <div className="content-text-box">
                                    <h5>Support Agreement - The iFlex83 Platform</h5>
                                    <ul>
                                        <div className="content-text-bg"><img src="https://content.iot83.com/m83/misc/m83-logo.png" /></div>
                                        <li>
                                            <h6>Standard Support and Maintenance</h6>
                                            <ul>
                                                <li>Error Corrections: LICENSOR will exercise commercially reasonable efforts to correct any error in LICENSOR Products as reported by LICENSEE in accordance with the priority level reasonably assigned to such error by LICENSOR. If a reported error has caused LICENSOR Products to be inoperable, or LICENSEE's notice to LICENSOR states that the reported error is substantial and material with respect to the LICENSEE's use of the Product, LICENSOR will expeditiously correct such error or provide a software patch or bypass around such error.   For purposes of this Exhibit an “error” is any failure of LICENSOR Products or LICENSOR Developed Application to operate in accordance with the Product Specifications and Documentation or specifications as identified and delivered as a part of the Agreement or any Accepted (Acceptance Criteria) Statement of Work.</li>
                                                <li>Software Updates: As required to optimize Product performance, LICENSOR will develop and deploy Product Updates and will provide LICENSEE with Documentation for all such Software Updates.</li>
                                                <li>Telephone Support and Electronic Mail Support: LICENSOR will provide telephone assistance 8x5 support during the business week and will provide electronic mail assistance 8x5 support during the business week with response and resolution outlined by error severity below. Should LICENSEE require 24x7x365 support, such support can be outlined and agreed on in a Statement of Work between the parties.</li>
                                                <li>On-Site Support: LICENSOR support Personnel will be available to visit LICENSEE for a duration of not more than two (2) weeks per year at LICENSEE cost and expense. These Personnel will assist in the integration, testing, and support of LICENSOR Products and Deliverables for LICENSEE. If LICENSEE wishes to acquire such support, it is available based on Personnel availability (which LICENSOR will make all commercially reasonable efforts to provide within 3 three weeks from the date of the request).</li>
                                            </ul>
                                        </li>

                                        <li>
                                            <h6>Service and Priority Levels of Errors</h6>
                                            <p>Maintenance and Support Services for resolution of different priority errors are defined herein:</p>
                                            <ul>
                                                <li>Priority I Errors
                                                    <ul>
                                                        <li>Description: Program errors that prevent some function or process of LICENSOR Products or Deliverables from substantially meeting the functional specification and which seriously affect the overall performance of the function or process as to any Product and no work around is known.</li>
                                                        <li>LICENSOR response and escalation procedures: LICENSOR will promptly initiate the following escalation procedures:
                                                            <ul>
                                                                <li>Assign LICENSOR support engineers to diagnose the errors and create a problem and diagnosis report, within 1 business day;</li>
                                                                <li>Assign senior LICENSOR engineers to correct the error or provide a work-around which resolves the severity of the error, within 2 business days.</li>
                                                                <li>Provide LICENSEE with periodic reports on the status of corrections, on a daily basis;</li>
                                                                <li>Commence work to provide LICENSEE with a work around 12x7x365 until final solution is available;</li>
                                                                <li>Provide final solution to LICENSEE as soon as it is available. Errors may be resolved either as a direct software correction or as a work-around that does not impact the services being offered by the LICENSEE.</li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li>Priority II Errors
                                                    <ul>
                                                        <li>Description: Program errors that prevent some function or process of LICENSOR Products or any Deliverable from substantially meeting functional specification, but may have a reasonable work around.</li>
                                                        <li>LICENSOR response and escalation procedure: LICENSOR will promptly initiate the following response and escalation procedures:
                                                            <ul>
                                                                <li>Assign LICENSOR support engineers to diagnose the error and create a problem and diagnosis report, within 3 business days;</li>
                                                                <li>Notify LICENSOR management that such errors have been reported and that steps are being taken to correct the error, within 1 business days;</li>
                                                                <li>Provide LICENSEE with periodic reports on the status of corrections, on a daily basis;</li>
                                                                <li>Commence work to provide LICENSEE with a work around until final solution is available;</li>
                                                                <li>Exercise commercially reasonable efforts to include the fix for the error in the next software maintenance release. Errors may be resolved either as a direct software correction or as a work-around that does not impact the services being offered by the LICENSEE.</li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <div className="content-text-bg"><img src="https://content.iot83.com/m83/misc/m83-logo.png" /></div>
                                                <li>Priority III Errors
                                                    <ul>
                                                        <li>Description: Program errors that prevent some portion of a function of LICENSOR Products or any Deliverable from substantially meeting functional specification but do not seriously affect the overall performance of the function.</li>
                                                        <li>LICENSOR response: LICENSOR will promptly initiate the following response and escalation procedure:
                                                            <ul>
                                                                <li>Assign LICENSOR support engineers to diagnose the error and create a problem and diagnosis report, within 5 business days;</li>
                                                                <li>Provide LICENSEE with periodic reports on the status of corrections, on a weekly basis;</li>
                                                                <li>Commence work to provide LICENSEE with a work around until final solution is available;</li>
                                                                <li>Exercise commercially reasonable efforts to include the fix for the error in the next major release of LICENSOR Products. Errors may be resolved either as a direct software correction or as a work-around that does not impact the services being offered by the LICENSEE.</li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>

                                        <li>
                                            <h6>Premium Maintenance and Support Services</h6>
                                            <ul>
                                                <li>LICENSOR is willing to offer LICENSEE premium and customized Maintenance and Support Services to meet the specific needs of LICENSEE.</li>
                                                <li>Such Premium Maintenance and Support Services can include:
                                                    <ul>
                                                        <li>Direct Support of LICENSEE Customers;</li>
                                                        <li>Extended hours of coverage, or escalated problem resolution timelines;</li>
                                                        <li>Or other support specific to the needs of the LICENSEE.</li>
                                                    </ul>
                                                </li>
                                                <li>Terms and Conditions for Such Premium Maintenance and Support Services will be established through a SOW developed and agreed to by the Parties.</li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div> :
                            <div className="content-body">
                                <div className="content-text-box">
                                    <p>
                                        SHOULD NO CUSTOM LICENSE AGREEMENT EXIST BETWEEN YOU (THE “LICENSEE”) AND IOT83 LTD. OR ITS AFFILIATES (THE “LICENSOR”) THIS IOT83 TERMS OF SERVICE(S) AGREEMENT (THE “AGREEMENT”) ESTABLISHES THE TERMS OF IOT83 SERVICE OR SERVICES (THE “SERVICES”) FOR USING THE FLEX83 SAAS PLATFORM, AND ASSOICATED LICENSEE GROUPS AND COMMUNITIES. BY AND SIGNING UP FOR AND ESTABLISHING AN ACCOUNT, YOU (THE “LICENSEE”) AGREE TO THE TERMS AND CONDITIONS OF THIS AGREEMENT. IF YOU ARE USING THE SERVICES ON BEHALF OF A LEGAL ENTITY OTHER THAN YOURSELF IN YOUR PRIVATE CAPACITY, YOU AFFIRM THAT YOU HAVE THE RIGHTS TO BIND SUCH ENTITY OR ENTITIES TO THIS AGREEMENT, AND IN THIS CASE ALL REFERENCES TO “YOU”,”YOUR”, LICENSEE” OR SIMILAR REFERENCES APPLY TO THIS ENTITY.
                                    </p>
                                    <ul>
                                        <div className="content-text-bg"><img src="https://content.iot83.com/m83/misc/m83-logo.png" /></div>
                                        <li>
                                            <h6>Definitions</h6>
                                            <ul>
                                                <li>"IoT83" means IoT83 LTD, a Delaware corporation,  with its principal place of business located at 450 S. Abel St #361625, Milpitas, CA, 95036.</li>
                                                <li>"Flex83 Services" or “Flex83” or “Product(s)” means the IoT Application Enablement Platform (AEP) offered as a SaaS product by IoT83. This Agreement establishes the terms and conditions between IoT83 and the Licensee (hereafter, the "Parties") for Flex83 Subscription licensing.</li>
                                                <li>"Flex83 Subscription(s)" means the Flex83 subscription options that the Licensee purchases using the Flex83 portal subscription management services or by other means, such as an executed quote, with IoT83, where such subscription is in good standing.</li>
                                                <li>"Licensee" or “You” or “Your” means the individual or company or other legal entity accepting the Agreement and using the Services either as defined in a separate custom Services addendum or as specified by the subscription to the Services.</li>
                                                <li>"Licensee Data" means electronic data submitted by Licensees or by devices owned or maintained by Licensees, Licensee Partners, or Licensee Customers (if any) transmitted to the Flex83 Service and the applications created by Licensee in the use of the Services.</li>
                                                <li>"Licensee Applications" means any applications created using Flex83 that Licensee may use on its own behalf or use to provide benefit to any third party.</li>
                                                <li>"Licensee’s Customer(s)" means customers of Licensee for Licensee Applications built with and / or hosted on Flex83.</li>
                                                <li>"Custom Services" means any services that IoT83 may provide to Licensee beyond the services outlined in this agreement.</li>
                                            </ul>
                                        </li>
                                        <li>
                                            <h6>Licensee Acceptance of Terms and Conditions for Services</h6>
                                            <ul>
                                                <li>By creating an account and using Flex83 Services, Licensee affirm agreement to the terms and conditions (the "Terms of Service") outlined in this Agreement, as well as the Flex83 Privacy Policy (the "Privacy Policy").</li>
                                                <li>The Terms of Service apply to Licensee’s utilization of the Flex83 Services, the Licensee Applications created with Flex83 and the utilization of Licensee Applications by Licensee’s Customers, and apply to any user groups made available, or other support that may be provided to Licensee.</li>
                                            </ul>
                                        </li>
                                        <li>
                                            <h6>License Grant and Rights of Use</h6>
                                            <ul>
                                                <li>License Grant. Subject to all limitations and restrictions contained herein and subject to the Licensee’s Flex83 Subscription (including the number of connected Devices, AEP tools and resources licensed, and any Custom Services purchased or licensed by Licensee), licensor grants to Licensee: (i) Rights to use Flex83 to collect, view, analyze, and manage Licensee Data and Connected Devices; (ii) Rights to use the Flex83 to create Licensee Applications; (iii) Rights to sublicense Licensee Applications to Licensee Customers.</li>
                                                <li>Modifications and Enhancements. IoT83 may modify the Flex83 in order to enhance or improve the operation of the Flex83, and such modifications to Services may be performed with or without notice. IoT83 will make reasonable efforts to inform Licensee of any changes to the Services that would be expected to impact Licensee.</li>
                                                <li>Except as expressly provided for in this Agreement, Licensee has no rights to sublicense any of its rights under this Agreement to any Third Party, except with the prior written consent of IoT83, and Licensee may not offer to any Third Party rights or privileges to use the Flex83 Services, or make any other use of the Flex83 to any third party.</li>
                                                <li>Additional Restrictions. In no event will Licensee disassemble, decompile, or reverse engineer the Flex83, IoT83 Intellectual Property, or Confidential Information (as defined herein) or permit others to do so. Disassembling, decompiling, and reverse engineering include, without limitation: (i) converting the Application from a machine-readable form into a human-readable form; (ii) disassembling or decompiling the Application by using any means or methods to translate machine-dependent or machine-independent object code into the original human-readable source code or any approximation thereof; (iii) examining the machine-readable object code that controls the Application's operation and creating the original source code or any approximation thereof by, for example, studying the Application's behavior in response to a variety of inputs; or (iv) performing any other activity related to the Application that could be construed to be reverse engineering, disassembling, or decompiling. To the extent any such activity may be permitted pursuant to written agreement, the results thereof will be deemed Confidential Information subject to the requirements of this Agreement. Licensee may use IoT83 Confidential Information solely in connection with the Application and pursuant to the terms of this Agreement.</li>
                                                <li>Licensee licenses and grants to  IoT83, to the extent needed for IoT83 to provide the Services outlined in this Agreement, the worldwide rights to use, copy, display, and modify Licensee Data, Licensee Applications, and Licensee’s Customer Data (if any) together, and as required to provide and use the Services.</li>
                                                <li>Third Party Software. The Services may contain third party software that requires notices and/or additional terms and conditions. Such required third party software notices and/or additional terms and conditions may be requested from IoT83 and are made a part of and incorporated by reference into this Agreement. By accepting this Agreement, Licensee is also accepting the additional terms and conditions, if any, set forth therein.</li>
                                                <li>The Products are licensed, not sold, to Licensee, notwithstanding the use herein of the words "sale," "sell," "sold," and the like.</li>
                                            </ul>
                                        </li>
                                        <div className="content-text-bg"><img src="https://content.iot83.com/m83/misc/m83-logo.png" /></div>
                                        <li>
                                            <h6>Payment</h6>
                                            <ul>
                                                <li>The fees for the Flex83 Service are by subscription and are according to the Licensee’s Flex83 Subscription details, and billing is managed on Licensee Portal or, for larger customers, through a quote and subscription payment process agreed to by the Parties. Services are billed on a monthly basis and are non-refundable. Licensee is responsible for paying for Services according to the tier of Service subscribed to without exception until such Service is terminated. Upon termination of Services no charge will be billed on the next billing cycle, but any fees owed but not yet paid remain due to IoT83 from Licensee. No partial credit for unused Services will be provided to Licensee. Licensees desiring Custom Services (including but not limited to Application Development, Premium Development Operations (DevOps), and Premium Maintenance) may contact IoT83 to quote pricing and terms for delivery for the Custom Services.</li>
                                                <li>Where Flex83 Services are be billed by invoice, invoiced services are due to IoT83 Net 30 after receipt of the invoice. Failure of on-time payment may result in loss of Licensee Data and Licensee Applications. Any dispute over charges must be resolved during the Net 30 payment period, and any resolution to such a dispute may be accommodated in the next invoice cycle. Failure to pay on-time will result in a 2% per month finance charge. Should Licensee elect to pay for Services by credit card billing for subsequent periods will be automatic until Services are modified or canceled.</li>
                                                <li>IoT83 reserves the right to change the fees for Flex83 Services or to create new Flex83 Subscription tiers at any time. Should changes in fees increase for a Licensee, Licensee will be notified either by email or by notices on the Flex83 portal and have the option to change Service tiers or Service Options, or to terminate Services, but it is the responsibility of Licensee to manage their selected tier of Service. Businesses, corporations, or enterprise customers may engage IoT83 directly to establish long term Flex83 Subscription pricing.</li>
                                                <li>Licensees are responsible to pay any applicable taxes associated or incurred with their purchase of Services. IoT83 is responsible to pay taxes due based on Iot83 income.</li>
                                            </ul>
                                        </li>

                                        <li>
                                            <h6>Hosting</h6>
                                            <ul>
                                                <li>Flex83 is cloud-hosted by IoT83 and Licensee has no rights or expectations to receive, manage, or otherwise possess the underlying Flex83 in object code or source code form. Licensee Applications are run as a part of the Licensee’s license to Flex83, and as such, are also cloud-hosted by IoT83. </li>
                                                <li>Larger customers may request to self-host Flex83. IoT83 has full and complete discretion in the granting of any such incremental terms and conditions, and additional terms and conditions will be required to assure the protection of IoT83 property.</li>
                                                <li>Service Availability. Service Provider will use reasonable efforts to achieve Service Provider's availability goals described in the 'Service Level Agreement for Flex83.' Licensees desiring Custom Service Level Agreement (SLA) may contact IoT83 to quote pricing and terms for delivery for the Custom SLA.</li>
                                                <li>Support Services. Business, corporate, or enterprise customers may request IoT83 to provide custom support or maintenance which will be governed by an addendum to this Agreement.</li>
                                            </ul>
                                        </li>

                                        <li>
                                            <h6>Licensee Responsibility for Licensee Created Applications</h6>
                                            <ul>
                                                <li>Licensee takes full responsibility for the functionality, correctness, and maintainability of Licensee Applications. Licensee understands and agrees that Flex83 Licensee Application performance and operational scale is dependent on the Flex83 Subscription resources available for the Licensee Application to use in its operation. It is the Licensee's responsibility to assure that the level of services subscribed to aligns with Licensee's performance and availability demand and expectations.</li>
                                                <li>Licensee shall provide Customer and End-Licensee support for Licensee Applications. IoT83 will have no obligations to directly support Licensee’s Customers unless Licensee enters into a separate support agreement with IoT83 for such support.</li>
                                                <li>Licensees desiring Custom Services for Licensee Applications (including but not limited to Application Development, Premium Development Operations (DevOps), and Premium Maintenance) may contact IoT83 to quote pricing and terms for delivery for the Custom Services.</li>
                                            </ul>
                                        </li>

                                        <li>
                                            <h6>Intellectual Property and Software Ownership</h6>
                                            <ul>
                                                <li>IoT83 retains all Intellectual Property rights to the Flex83 Services, the underlying technology, software, design, concepts, trade secrets, workflows, services delivery systems, and platforms used to provide the Flex83 Services and no grant or title to this Intellectual Property is provided to Licensee under this Agreement.</li>
                                                <li>For feature requests or suggestions that Licensees provide to IoT83 that may be incorporated into Flex83, the Licensee grants to IoT83 an irrevocable, transferable, worldwide, perpetual, royalty-free license for such Intellectual Property to IoT83. Further, Licensee agrees that any such changes that Licensee requests and IoT83 provides, may incorporate customizations, inventions, or other Intellectual Property, and that by providing such customizations or changes, IoT83 is not conveying, relinquishing rights, or providing Licensee license to such underlying Intellectual Property. </li>
                                                <li>Insomuch as Flex83 incorporates 3rd party software, no license to such software is conveyed in this Agreement, other than the Flex83 Services rights conveyed to Licensee under this Agreement.</li>
                                                <li>Licensee owns any Intellectual Property software, methods, user interfaces or other artifacts independently created by Licensee or on Licensee’s behalf in the creation of Licensee Applications. But as regards to Licensee Applications, Licensee acknowledges that no conveyance of the Flex83 Services or IoT83 Intellectual Property is provided to Licensee.</li>
                                            </ul>
                                        </li>

                                        <div className="content-text-bg"><img src="https://content.iot83.com/m83/misc/m83-logo.png" /></div>
                                        <li>
                                            <h6>Confidential Information</h6>
                                            <ul>
                                                <li>For the purposes of this Agreement, Confidential Information means all non-public proprietary or confidential information of Disclosing Party, in oral, visual, written, electronic or other tangible or intangible form, whether or not marked or designated as “confidential,” and all notes, analyses, summaries and other materials prepared by Recipient or any of its Representatives that contain, are based on or otherwise reflect, to any degree, any of the foregoing (“Notes”); provided, however, that Confidential Information does not include any information that: (a) is or becomes generally available to the public other than as a result of Recipient’s or its Representatives’ material breach of this Agreement; (b) is obtained by Recipient or its Representatives on a non-confidential basis from a third-party that, to Recipient’s knowledge, was not legally or contractually restricted from disclosing such information; (c) Recipient establishes by documentary evidence, was in Recipient’s or its Representatives’ possession prior to Disclosing Party’s disclosure hereunder; or (d) Recipient establishes by documentary evidence, was or is independently developed by Recipient or its Representatives without using any Confidential Information.</li>
                                                <li>Disclosing Party retains its entire right, title and interest in and to all Confidential Information, and no disclosure of Confidential Information hereunder will be construed as a license, assignment or other transfer of any such right, title and interest to Recipient or any other person.</li>
                                                <li>The Receiving Party will use the same degree of care in protecting Confidential Information received from the Receiving Party that it uses to protect its own Confidential Information, but in no event less that reasonable care and protection not to use any Confidential Information outside of the scope of the Agreement. Further, the Receiving Party shall limit access of the Disclosing Party Confidential Information to only employees, contractors, or other agents that require such assess to execute under the Agreement.</li>
                                                <li>The Parties acknowledge and agree that any breach of this the Confidential Information terms of this Agreement may cause injury to Disclosing Party for which money damages would be an inadequate remedy and that, in addition to remedies at law, Disclosing Party is entitled to equitable relief as a remedy for any such breach.</li>
                                                <li>Licensee shall not use any of IoT83's Confidential Information other than pursuant to and in accordance with the exercise of any of the benefits of Agreement. Without limiting the generality of the foregoing, Licensee shall not use IoT83's Confidential Information (i) for determining if any features, functions or processes disclosed by the IoT83 Confidential Information are covered by any patents or patent applications owned by Licensee; or (ii) for developing technology or products which avoid any of IoT83's Intellectual Property licensed hereunder; or (iii) as a reference for modifying existing patents or patent applications or creating any continuation, continuation in part, or extension of existing patents or patent applications or (iv) for generating data for publication or disclosure to third parties, which  compares  the  performance  or  functionality  of  the IoT83 Software or Products.</li>
                                                <li>Confidential Information Exceptions: If Recipient or any of its Representatives is required by a valid legal order to disclose any Confidential Information, Recipient shall notify Disclosing Party of such requirements so that Disclosing Party may seek, at Disclosing Party’s expense, a protective order or other remedy, and Recipient shall reasonably assist Disclosing Party therewith. If Recipient remains legally compelled to make such disclosure, it shall: (a) only disclose that portion of the Confidential Information that it is required to disclose; and (b) use reasonable efforts to ensure that such Confidential Information is afforded confidential treatment.</li>
                                            </ul>
                                        </li>

                                        <li>
                                            <h6>Warranties</h6>
                                            <ul>
                                                <li>Licensee Warranty.  It is the sole and exclusive responsibility of Licensee to determine the suitability of any and all Flex83 Services or Flex83 Subscriptions and Licensee Applications for Licensee's intended purposes.  The Licensee will be responsible for making all warranties to any consumer of Licensee’s Applications using the Flex83 Services.  Licensee represents and warrants that (i) it has the requisite power, authority and resources to enter into this Agreement and to perform its obligations pursuant to this Agreement; (ii) if a business, corporation, or enterprise, that it is duly organized, validly existing and in good standing under the laws of its jurisdiction of organization; (iii) it is not bound by any agreement or obligation (and will not enter into or assume any agreement or obligation) that could interfere with its obligations under this Agreement; and (iv) in connection with its obligations under this Agreement, it will comply with all applicable laws and regulations and has obtained all applicable permits, rights and licenses.</li>
                                                <li>IoT83 Warranty.  IoT83 warrants that the Products will perform substantially in accordance with the Documentation under regular use and will, in keeping with the service and maintenance commitments of the Support Agreement for Flex83, remediate any Product failure according to the terms established there.  In the event of a breach of this warranty, as IoT83's sole obligation and Licensee's sole remedy, IoT83 will, at its option and expense, correct, repair or replace the non-conforming Product, or refund affected services or functionality.</li>
                                                <li>Exclusions.  The warranties set forth in this Section and IoT83's support and maintenance obligations hereunder will not apply to any Products or services to the extent the defect or non-conformance is due to (i) use of the Product other than as specified by IoT83; (ii) incorrect implementation of the Product in the creation of a Licensee Application by Licensee; (iii) any error, act or omission by anyone other than IoT83 or IoT83's agents, employees, and subcontractors; or (iv) where written notice of the defect has not been given to IoT83 by Licensee within the applicable warranty period.</li>
                                                <li>IoT83 expressly makes no warranties of any kind for Flex83, or for any 3rd Party component of the Flex83 Services, if any, unless specified between IoT83 and Licensee in a separate signed and dated agreement. No implied statutory and other warranties are provided, including fitness for a particular purpose or application, or the suitability of the Services for any specific Licensee purposes. IoT83 does not provide any warranty as to continuous uptime, error free operation, security, latency, or timely response of the Services.</li>
                                                <li>Disclaimer. THE ABOVE WARRANTIES ARE THE ONLY WARRANTIES OF ANY KIND MADE BY IoT83 WITH RESPECT TO THE PRODUCTS. IoT83 HEREBY EXPRESSLY DISCLAIMS ALL OTHER WARRANTIES WITH RESPECT TO THE PRODUCTS, EITHER EXPRESS OR IMPLIED, INCLUDING WARRANTIES OF MERCHANTABILITY, FITNESS FOR ANY PARTICULAR PURPOSE, TITLE, NON-INFRINGEMENT, NON-INTERFERENCE WITH ENJOYMENT, ACCURACY, INTEGRATION, AS WELL AS ALL WARRANTIES ARISING BY USAGE OF TRADE, COURSE OF DEALING OR COURSE OF PERFORMANCE.  IoT83 DOES NOT WARRANT THAT (1) THE PRODUCTS WILL MEET LICENSEE'S OR LECENSEE’S CUSTOMER’S REQUIREMENTS, (2) OPERATION OF THE PRODUCTS WILL BE UNINTERRUPTED OR ERROR-FREE, OR (3) DEFECTS WILL BE CORRECTED.  EXCEPT AS EXPRESSLY PROVIDED IN THIS SECTION 9, THE PRODUCTS ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND.</li>
                                            </ul>
                                        </li>

                                        <li>
                                            <h6>Indemnity</h6>
                                            <ul>
                                                <li>By accepting this Agreement, the Licensee undertakes to indemnify and hold harmless IoT83 as well as its directors, employees, managers, agents and any IoT83 subsidiary and /or parent company, from any prejudice resulting from the violation, by the Licensee, of the Terms and Conditions of this Agreement, the legal obligations or the rights of third parties.</li>
                                                <li>Further, each party will indemnify, defend and hold the other harmless against any claims, damages, and expenses by any third party resulting from any acts of omissions of Indemnifying party relating to its activities associated with this Agreement, their breach of this agreement, any distortion, fabrication or misrepresentation relating to the other party, the Services, or the Agreement itself.</li>
                                            </ul>
                                        </li>

                                        <div className="content-text-bg"><img src="https://content.iot83.com/m83/misc/m83-logo.png" /></div>
                                        <li>
                                            <h6>Limitations of Liability</h6>
                                            <ul>
                                                <li>Except for breaches of confidentiality or indemnification obligations, each party waives any and all claims against the other for claims for lost profit, revenues, data, interruptions of service, or consequential, incidental or any special damages, arising out of any services or work product associated with this Agreement.</li>
                                                <li>Further Licensee agrees that the maximum liability for IoT83 for any claim related to this Agreement is limited to the amount of any fee received by IoT83 from Licensee in the preceding six months.</li>
                                                <li>NOTWITHSTANDING ANYTHING TO THE CONTRARY CONTAINED IN THIS AGREEMENT, THE MAXIMUM LIABILITY OF IOT83 TO LICENSEE IN AGGREGATE FOR ALL CLAIMS MADE AGAINST IOT83 IN CONTRACT TORT OR OTHERWISE UNDER OR IN CONNECTION WITH THE SUBJECT MATTER OF THIS AGREEMENT SHALL NOT EXCEED THE SUMS PAID (IF ANY) BY LICENSEE TO IOT83 IN RESPECT OF THIS AGREEMENT.</li>
                                            </ul>
                                        </li>

                                        <li>
                                            <h6>Agreement Term and Termination</h6>
                                            <ul>
                                                <li>The Agreement between IoT83 and Licensee begins on the date Licensee accepts the Agreement and continues until terminated by either party in accordance with this Agreement. In the event Licensee terminates subscription to Services, the Agreement terminates, subject to other provisions of this Agreement. Subject to change in efforts to best manage Services, IoT83 will provide a grace period to retain Licensee Data, however beyond any grace period, if any, upon termination of Services Licensee Data and Licensee Applications will be deleted.</li>
                                                <li>Licensee may terminate use of the Services and this Agreement at any time for any and for no reason by using the online tools to terminate Licensee subscriptions by invoking the "Delete Account" (or similarly named profile management functions). IoT83 may also terminate Licensee account for any or no cause upon 30-day notice to user or upon Licensee failure to comply with the terms of this Agreement.</li>
                                                <li>In the event of termination any fees due to IoT83 are due Net30 from termination.</li>
                                                <li>Clauses in this Agreement covering IoT83 Property Rights and Intellectual Property rights, Warranties, Limitation of Liability, Indemnity and General Provisions survive any Agreement expiration or termination.</li>
                                            </ul>
                                        </li>

                                        <li>
                                            <h6>Licensee Obligations</h6>
                                            <ul>
                                                <li>Licensees of Flex83: (i) Provide IoT83 with complete and accurate contact and billing information; and (ii) Notify IoT83 of any discovered unauthorized use of the Services and use all reasonable efforts to prevent such unauthorized use of the Services; and (iii) Be fully responsible for Licensee’s compliance with the Agreement; and (iv) Make no representation to any of Licensee’s business partners and / or customers that attempt to commit IoT83 or its Affiliates to Terms and Conditions outside this Agreement; and (v) Where Licensee enters into agreements with any Licensee’s Customers that rely on the Licensee’s use of Flex83, Licensee will limit any representation of IoT83’s warranties and liabilities to IoT83 approved warranties and liabilities per this agreement; and (vi) Be fully responsible for the legality with which Licensee gains access to Licensee Data and Licensee Devices, and any Data and Devices associated with Licensee Applications, including representations as to the accuracy, quality, and integrity of Licensee Data and Licensee Applications and the use of Licensee Data and Licensee Applications by any Third Parties.</li>
                                                <li>Licensees of Flex83 shall not: (i) Transmit or store information in violation of a third parties rights; (ii) Use the Service for malicious uses including but not limited to fraudulent activities, transmission of malware or malicious code, or attack or attempt disrupt the Services themselves or the Data of another Licensee; (iii) Use or attempt to use the Services for malicious uses including but not limited to fraudulent activities, transmission of malware or malicious code, or attack or attempt disrupt the Services themselves or the Data of another Licensee; (iv) Attempt to re-sell, mirror, or copy the Services; (v) Attempt to gain unauthorized Flex83 Services or attempt to access Flex83 Subscriptions beyond Licensee’s authorized subscription; (vi) Attempt to use access as a means to disrupt the operation of the Flex83 Services, the Flex83 Services Data, or the Flex83 Service Cloud Infrastructure.</li>
                                                <li>IoT83 does not prohibit minors from using the Flex83 without parental consent. By accepting the Terms of Service, Licensee acknowledge that they have the authority to enter into the Agreement regardless of age.</li>
                                                <li>Before entering its data and information, the Licensee shall be obliged to check the same for viruses or other harmful components and to use state of the art anti-virus programs for this purpose. In addition, the Licensee itself shall be responsible for the entry and the maintenance of its data.</li>
                                                <li>IoT83 has the right (but not the obligation) to suspend access to the Application or remove any data or content transmitted via the Application without liability (i) if IoT83 reasonably believes that the Application is being used in violation of the terms of this Agreement or applicable law, (ii) if requested by a law enforcement or government agency or otherwise to comply with applicable law, provided that Licensor shall use commercially reasonable efforts to notify Licensee prior to suspending the access to the Products as permitted under these SaaS Terms, or (iii) as otherwise specified in this Agreement. Information on Flex83 servers may be unavailable to Licensee during a suspension of access to the Software. IoT83 will use commercially reasonable efforts to give Licensee at least twelve (12) hours’ notice of a suspension unless IoT83 determines in its commercially reasonable judgment that a suspension on shorter or contemporaneous notice is necessary to protect IoT83 or its customers.</li>
                                            </ul>
                                        </li>

                                        <div className="content-text-bg"><img src="https://content.iot83.com/m83/misc/m83-logo.png" /></div>
                                        <li>
                                            <h6>Privacy Policy</h6>
                                            <ul>
                                                <li>
                                                    IoT83 respects the privacy of Licensees, Licensee Data. IoT83's use of any Licensee Personal Information, Licensee Data, or Licensee Applications is for the purposes of operating and managing the Services effectively. By using the Services, Licensee agree to the all subparts of this section of the Agreement:
                                                    <ul>
                                                        <li>Data collected by IoT83 is collected solely to communicate with Licensees and to operate the Services subscribed to by Licensees. Licensee personal information is only shared with Licensee consent unless such sharing of Licensee data is necessary to perform a Licensee request, it is legally required, it is necessary to execute the Terms of Service, IoT83 is using the data to detect fraud, bypass or compromise security of the Services, or other illegal activities, or to otherwise protect the rights of other Licensees, or IoT83 legal rights or IoT83 property.</li>
                                                        <li>Billing processing third party services, as selected by you in the sign-up and subscription management process, will have access to the billing information that you provide, solely to provide billing services as selected by you. IoT83 does not retain any Licensee credit card or billing information for subscribed services but may retain billing information for enterprise or corporate accounts as agreed to by the parties.</li>
                                                        <li>Third party marketing providers may have access to Your email address solely for promoting IoT83 and Flex83 programs, but Licensees can opt-out of these programs.</li>
                                                        <li>IoT83 will collect Licensee names, addresses, telephone numbers, business names, email addresses, log-in credentials, information about tiers of Service selected, transactions made with us, customer service and support records, and any other information Licensees provide to IoT83 for the purposes of operating and managing the Services.</li>
                                                        <li>Licensee Data and Licensee Applications are retained privately to the Licensee and are accessible only through authenticated Licensee access to the Services.</li>
                                                        <li>IoT83 reserves the rights to amend the Privacy Policy at any time, and the use of information IoT83 receives from Licensees is subject to the Privacy Policy currently in effect. Should major changes be made to Privacy Policy, IoT will provide Licensees with email notification of such changes.</li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>

                                        <li>
                                            <h6>Miscellaneous</h6>
                                            <ul>
                                                <li>IoT and Licensee Relationship. Licensees are not authorized to make any sale, contract, representation, obligations, commitment, guarantee, or partnership on behalf of IoT83. IoT83 and Licensee are independent contractors and nothing in this Agreement creates any form of business, technology, or financial partnership. Neither of the Parties are granting any exclusivity to the other Party under this Agreement.</li>
                                                <li>IoT83 and Licensee Communications. Subject to change as needed to adequately provide the Services, IoT83 will contact Licensee using the Flex83 portal capabilities, by email, or by other means necessary for the delivery and management of the Services. Subject to Licensee management of communication preferences with IoT83, IoT83 may provide promotional emails to Licensees. IoT83 may use software to monitor that emails have been received and opened in order to optimize delivery of Services. For business, corporate, or enterprise customers may request IoT83 specific service point of contact depending on Flex83 Subscription levels.</li>
                                                <li>Waiver. The waiver by a party of a breach or a default of any provision of this Agreement by the other party will not be construed as a waiver of any succeeding breach of the same or any other provision, nor will any delay or omission on the part of such party to exercise or avail itself of any right, power or privilege that it has, or may have hereunder, operate as a waiver of any right, power or privilege by it.</li>
                                                <li>Foreign Asset Control (“OFAC”). Licensee agrees to abide by Office of Foreign Asset Control regulations, and acknowledge and commit that Licensee is not, nor will Licensee provide, sell, transfer, or otherwise convey Your access of the Services to any country subject to OFAC sanctions, namely Cuba, Iran, North Korea, Syria, and Ukraine's Crimea region.</li>
                                                <li>Export Compliance for Services. Each party to this Agreement will comply with the United States established export laws and regulations as well as other applicable laws in providing or using the Services.</li>
                                                <li>SDN List Control (“OFAC”). Licensee agrees to abide by Office of Foreign Asset Control regulations, and acknowledge and commit that Licensee is not, nor will Licensee provide, sell, transfer, or otherwise convey Your access of the Services to any entity or individuals listed on the Treasury Department's List of Specifically Designated Nationals List (herein, the "SDN List").</li>
                                                <li>Assignments of this Agreement. Neither party to this Agreement may assign any part of this Agreement or any of its rights or responsibilities to another party without written consent of the other party, except in the event of a merger, acquisition, or the sale of substantially all of its assets.</li>
                                                <li>Litigation and Waiver of Jury Trial. The laws of California govern this Agreement and each party agrees to such adjudication for any disputes that arise from this Agreement. Further, each party agrees to waive the right to jury trial in connection with any action or dispute arising or related to this Agreement.</li>
                                                <li>Entire Agreement. This Agreement constitutes the entire agreement between IoT83 and Licensee. The Agreement supersedes any prior discussions or agreements, either written or oral between the parties. Should any portion of this Agreement be deemed to be invalid, the remainder of this Agreement survives intact and remains in full forces and effect.</li>
                                                <li>Force Majeure. In no event will either party have any liability to the other for any delayed performance or nonperformance by a party which results, in whole or in part, directly or indirectly, from any cause beyond the reasonable control of such party.  Such causes will include (but will not be limited to) acts of God, wars, riots, civil disturbances, strikes, labor disputes, fires, storms, floods, earthquakes, natural disasters, inability to obtain or use raw or component materials or parts, labor, devices, facilities, or transportation, and acts of any government or agency thereof. </li>
                                                <li>Counterparts. This Agreement may be executed in multiple counterparts, each of which will be deemed to be an original, and all such counterparts will constitute one and the same instrument. This Agreement may be executed via validated digital signature transmission.</li>
                                                <li>Notices. Notices regarding to or arising out of this agreement may be made by email, certified mail, registered mail or corridor to the address, for the Licensee, as set forth in registration for Services or in any special Services Agreement, and for IoT83 as indicated in said special Services Agreement.</li>
                                                <li>Survival. Sections 2, 3, 6, 7, 8, 9, 10, and 13 shall survive the expiration or termination of this agreement.</li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                                <div className="content-text-box">
                                    <h5>Support Agreement for Flex83</h5>
                                    <ul>
                                        <div className="content-text-bg"><img src="https://content.iot83.com/m83/misc/m83-logo.png" /></div>
                                        <li>
                                            <h6>Standard Support and Maintenance</h6>
                                            <ul>
                                                <li>Error Corrections: LICENSOR will exercise commercially reasonable efforts to correct any error in LICENSOR Products as reported by LICENSEE in accordance with the priority level reasonably assigned to such error by LICENSOR. If a reported error has caused LICENSOR Products to be inoperable, or LICENSEE's notice to LICENSOR states that the reported error is substantial and material with respect to the LICENSEE's use of the Product, LICENSOR will expeditiously correct such error or provide a software patch or bypass around such error.   For purposes of this Exhibit an “error” is any failure of LICENSOR Products or LICENSOR Developed Application to operate in accordance with the Product Specifications and Documentation or specifications as identified and delivered as a part of the Agreement or any Accepted (Acceptance Criteria) Statement of Work.</li>
                                                <li>Software Updates: As required to optimize Product performance, LICENSOR will develop and deploy Product Updates and will provide LICENSEE with Documentation for all such Software Updates.</li>
                                                <li>Telephone Support and Electronic Mail Support: LICENSOR will provide telephone assistance 8x5 support during the business week and will provide electronic mail assistance 8x5 support during the business week with response and resolution outlined by error severity below. Should LICENSEE require 24x7x365 support, such support can be outlined and agreed on in a Statement of Work between the parties.</li>
                                            </ul>
                                        </li>

                                        <li>
                                            <h6>Service and Priority Levels of Errors</h6>
                                            <p>Maintenance and Support Services for resolution of different priority errors are defined herein:</p>
                                            <ul>
                                                <li>Priority I Errors
                                                    <ul>
                                                        <li>Description: Program errors that prevent some function or process of LICENSOR Products or Deliverables from substantially meeting the functional specification and which seriously affect the overall performance of the function or process as to any Product and no work around is known.</li>
                                                        <li>LICENSOR response and escalation procedures: LICENSOR will promptly initiate the following escalation procedures:
                                                            <ul>
                                                                <li>Assign LICENSOR support engineers to diagnose the errors and create a problem and diagnosis report, within 1 business day;</li>
                                                                <li>Assign senior LICENSOR engineers to correct the error or provide a work-around which resolves the severity of the error, within 2 business days.</li>
                                                                <li>Provide LICENSEE with periodic reports on the status of corrections, on a daily basis;</li>
                                                                <li>Commence work to provide LICENSEE with a work around 12x7x365 until final solution is available; </li>
                                                                <li>Provide final solution to LICENSEE as soon as it is available. Errors may be resolved either as a direct software correction or as a work-around that does not impact the services being offered by the LICENSEE.</li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li>Priority II Errors
                                                    <ul>
                                                        <li>Description: Program errors that prevent some function or process of LICENSOR Products or any Deliverable from substantially meeting functional specification, but may have a reasonable work around.</li>
                                                        <li>LICENSOR response and escalation procedure: LICENSOR will promptly initiate the following response and escalation procedures:
                                                            <ul>
                                                                <li>Assign LICENSOR support engineers to diagnose the error and create a problem and diagnosis report, within 3 business days; </li>
                                                                <li>Notify LICENSOR management that such errors have been reported and that steps are being taken to correct the error, within 1 business days;</li>
                                                                <li>Provide LICENSEE with periodic reports on the status of corrections, on a daily basis; </li>
                                                                <li>Commence work to provide LICENSEE with a work around until final solution is available;</li>
                                                                <li>Exercise commercially reasonable efforts to include the fix for the error in the next software maintenance release. Errors may be resolved either as a direct software correction or as a work-around that does not impact the services being offered by the LICENSEE.</li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <div className="content-text-bg"><img src="https://content.iot83.com/m83/misc/m83-logo.png" /></div>
                                                <li>Priority III Errors
                                                    <ul>
                                                        <li>Description: Program errors that prevent some portion of a function of LICENSOR Products or any Deliverable from substantially meeting functional specification but do not seriously affect the overall performance of the function.</li>
                                                        <li>LICENSOR response: LICENSOR will promptly initiate the following response and escalation procedure:
                                                            <ul>
                                                                <li>Assign LICENSOR support engineers to diagnose the error and create a problem and diagnosis report, within 5 business days;</li>
                                                                <li>Provide LICENSEE with periodic reports on the status of corrections, on a weekly basis; </li>
                                                                <li>Commence work to provide LICENSEE with a work around until final solution is available;</li>
                                                                <li>Exercise commercially reasonable efforts to include the fix for the error in the next major release of LICENSOR Products. Errors may be resolved either as a direct software correction or as a work-around that does not impact the services being offered by the LICENSEE.</li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>

                                        <li>
                                            <h6>Premium Maintenance and Support Services</h6>
                                            <ul>
                                                <li>LICENSOR is willing to offer LICENSEE premium and customized Maintenance and Support Services to meet the specific needs of LICENSEE.</li>
                                                <li>Such Premium Maintenance and Support Services can include:
                                                    <ul>
                                                        <li>Direct Support of LICENSEE Customers;</li>
                                                        <li>Extended hours of coverage, or escalated problem resolution timelines;</li>
                                                        <li>Or other support specific to the needs of the LICENSEE.</li>
                                                    </ul>
                                                </li>
                                                <li>Terms and Conditions for Such Premium Maintenance and Support Services will be established through a SOW developed and agreed to by the Parties</li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        }
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

TermsAndConditions.propTypes = {
    dispatch: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
    termsandconditions: makeSelectTermsAndConditions()
});

function mapDispatchToProps(dispatch) {
    return {
        dispatch
    };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({key: "termsAndConditions", reducer});
const withSaga = injectSaga({key: "termsAndConditions", saga});

export default compose(
    withReducer,
    withSaga,
    withConnect
)(TermsAndConditions);
