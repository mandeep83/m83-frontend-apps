/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/*
 *
 * NewAddOrEditEtlConfig constants
 *
 */

export const RESET_TO_INITIAL_STATE = "app/NewAddOrEditEtlConfig/RESET_TO_INITIAL_STATE";

export const GET_ALL_DEVICE_TYPES = {
	action: 'app/NewAddOrEditEtlConfig/GET_ALL_DEVICE_TYPES',
	success: "app/NewAddOrEditEtlConfig/GET_ALL_DEVICE_TYPES_SUCCESS",
	failure: "app/NewAddOrEditEtlConfig/GET_ALL_DEVICE_TYPES_FAILURE",
	urlKey: "getAllDeviceTypeWithTopics",
	successKey: "getAllDeviceTypesSuccess",
	failureKey: "getAllDeviceTypesFailure",
	actionName: "getAllDeviceType",
}

export const GET_DATA_CONNECTORS = {
	action: 'app/NewAddOrEditEtlConfig/GET_DATA_CONNECTORS',
	success: "app/NewAddOrEditEtlConfig/GET_DATA_CONNECTORS_SUCCESS",
	failure: "app/NewAddOrEditEtlConfig/GET_DATA_CONNECTORS_FAILURE",
	urlKey: "getDataConnectors",
	successKey: "getDataConnectorsSuccess",
	failureKey: "getDataConnectorsFailure",
	actionName: "getDataConnectors",
}

export const GET_CONNECTORS_BY_CATEGORY = {
	action: 'app/NewAddOrEditEtlConfig/GET_CONNECTORS_BY_CATEGORY',
	success: "app/NewAddOrEditEtlConfig/GET_CONNECTORS_BY_CATEGORY_SUCCESS",
	failure: "app/NewAddOrEditEtlConfig/GET_CONNECTORS_BY_CATEGORY_FAILURE",
	urlKey: "getConnectorsByCategory",
	successKey: "getConnectorsByCategorySuccess",
	failureKey: "getConnectorsByCategoryFailure",
	actionName: "getConnectorsByCategory",
	actionArguments: ["category", "fromMount"],
}

export const GET_SPARK_FUNCTIONS = {
    action: "app/NewAddOrEditEtlConfig/GET_SPARK_FUNCTIONS",
    success: "app/NewAddOrEditEtlConfig/GET_SPARK_FUNCTIONS_SUCCESS",
	failure: "app/NewAddOrEditEtlConfig/GET_SPARK_FUNCTIONS_FAILURE",
	urlKey: "getSparkFunctions",
	successKey: "getSparkFunctionsSuccess",
	failureKey: "getSparkFunctionsFailure",
	actionName: "getSparkFunctions",
}

export const GENERATE_SCHEMA = {
	action: "app/NewAddOrEditEtlConfig/GENERATE_SCHEMA",
    success: "app/NewAddOrEditEtlConfig/GENERATE_SCHEMA_SUCCESS",
	failure: "app/NewAddOrEditEtlConfig/GENERATE_SCHEMA_FAILURE",
	urlKey: "hitSchema",
	successKey: "generateSchemaSuccess",
	failureKey: "generateSchemaFailure",
	actionName: "generateSchema",
	actionArguments: ["payload"],
}
export const SAVE_MISC_TOPIC = {
	action: "app/NewAddOrEditEtlConfig/SAVE_MISC_TOPIC",
    success: "app/NewAddOrEditEtlConfig/SAVE_MISC_TOPIC_SUCCESS",
	failure: "app/NewAddOrEditEtlConfig/SAVE_MISC_TOPIC_FAILURE",
	urlKey: "saveMiscTopic",
	successKey: "saveMiscTopicSuccess",
	failureKey: "saveMiscTopicFailure",
	actionName: "saveMiscTopic",
	actionArguments: ["payload"],
}

export const SAVE_ETL = {
	action: "app/NewAddOrEditEtlConfig/SAVE_ETL",
    success: "app/NewAddOrEditEtlConfig/SAVE_ETL_SUCCESS",
	failure: "app/NewAddOrEditEtlConfig/SAVE_ETL_FAILURE",
	urlKey: "saveETL",
	successKey: "saveETLSuccess",
	failureKey: "saveETLFailure",
	actionName: "saveETL",
	actionArguments: ["payload"],
}

export const GET_ETL_BY_ID = {
	action: "app/NewAddOrEditEtlConfig/GET_ETL_BY_ID",
    success: "app/NewAddOrEditEtlConfig/GET_ETL_BY_ID_SUCCESS",
	failure: "app/NewAddOrEditEtlConfig/GET_ETL_BY_ID_FAILURE",
	urlKey: "etlGetInfo",
	successKey: "getETLByIdSuccess",
	failureKey: "getETLByIdFailure",
	actionName: "getETLById",
	actionArguments: ["id"],
}

export const GET_TIMESCALE_CONNECTOR_TABLES = {
	action: "app/NewAddOrEditEtlConfig/GET_TIMESCALE_CONNECTOR_TABLES",
    success: "app/NewAddOrEditEtlConfig/GET_TIMESCALE_CONNECTOR_TABLES_SUCCESS",
	failure: "app/NewAddOrEditEtlConfig/GET_TIMESCALE_CONNECTOR_TABLES_FAILURE",
	urlKey: "getTimescaleConnectorTables",
	successKey: "getTimescaleConnectorTablesSuccess",
	failureKey: "getTimescaleConnectorTablesFailure",
	actionName: "getTimescaleConnectorTables",
	actionArguments: ["connectorId"],
}

export const CREATE_TOPIC_SCHEMA = {
	action: "app/NewAddOrEditEtlConfig/CREATE_TOPIC_SCHEMA",
    success: "app/NewAddOrEditEtlConfig/CREATE_TOPIC_SCHEMA_SUCCESS",
	failure: "app/NewAddOrEditEtlConfig/CREATE_TOPIC_SCHEMA_FAILURE",
	urlKey: "createTopicSchema",
	successKey: "createTopicSchemaSuccess",
	failureKey: "createTopicSchemaFailure",
	actionName: "createTopicSchema",
	actionArguments: ["payload"],
}

export const CLEAR_TOPIC_SCHEMA = {
	action: "app/NewAddOrEditEtlConfig/CLEAR_TOPIC_SCHEMA",
    success: "app/NewAddOrEditEtlConfig/CLEAR_TOPIC_SCHEMA_SUCCESS",
	failure: "app/NewAddOrEditEtlConfig/CLEAR_TOPIC_SCHEMA_FAILURE",
	urlKey: "clearTopicSchema",
	successKey: "clearTopicSchemaSuccess",
	failureKey: "clearTopicSchemaFailure",
	actionName: "clearTopicSchema",
	actionArguments: ["connectorId","topicId"],
}