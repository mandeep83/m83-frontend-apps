/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * NewAddOrEditEtlConfig
 *
 */

import React from "react";
import PropTypes, { object } from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { FormattedMessage } from "react-intl";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import { allActions as ACTIONS } from './actions';
import { allSelectors as SELECTORS } from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import messages from "./messages";
import TagsInput from 'react-tagsinput';
import 'react-tagsinput/react-tagsinput.css';
import AceEditor from "react-ace";
import produce from "immer"
import * as yup from 'yup';
import { getMqttConnection, closeMqttConnection, subscribeTopic, unsubscribeTopic } from "../../mqttConnection";
import { cloneDeep } from "lodash";
import Loader from "../../components/Loader";
import ReactJson from "react-json-view";
import JSONInput from "react-json-editor-ajrm/dist";
var flatten = require('flat');
import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';
import { allRegexPatterns, getMiniLoader } from "../../commonUtils";
let mongoIndexes = "";
let isAddNewIndexButtonDisabled = false;
const OPERATION_TYPES = ["select", "aggregate", "filter"];
const SOURCE_CONNECTORS = [
    {
        name: "MQTT",
        type: "Stream",
        imageName: "card-2"
    },
    {
        name: "MONGODB",
        type: "Batch",
        imageName: "card-7"
    },
    {
        name: "S3",
        type: "Batch",
        imageName: "card-5"
    },
];
let SOURCE_CONNECTOR_PAYLOADS = {
    "MQTT": {
        "childType": "mqttSource",
        "id": "",
        "properties": {
            "topicId": ""
        }
    },
    "MONGODB": {
        "id": "",
        "databaseId": "",
        "query": "",
        "collection": "",
        "childType": "mongoSource",
    },
    "S3": {
        "bucketId": "",
        "id": "",
        "format": "",
        "basePath": "",
        "childType": "s3Source",
    },
};
const DEFAULT_OPERATION = {
    "operations": [],
    "type": "select"
};
const DEFAULT_AGGREGATE_CONDITION = {
    type: "",
    field: "",
    alias: ""
};
let isSinkDetailAutofilled = false;
let isDefinitionAutofilled = false;

const MQTT_SOURCE_SCHEMA = yup.object().shape({
    "childType": yup.string().required("Connector Type is Required in Source"),
    "id": yup.string().required("Connector is Required in Source"),
    "properties": yup.object().shape({
        "topicId": yup.string().required("Topic is Required in Source")
    })
})

const S3_SOURCE_SCHEMA = yup.object().shape({
    "childType": yup.string().required("Connector Type is Required in Source"),
    "id": yup.string().required("Connector is Required in Source"),
    "bucketId": yup.string().required("Bucket Id is Required in Source"),
    "format": yup.string().required("Format is Required in Source"),
    "basePath": yup.string().required("Base Path is Required in Source"),
})

const MONGODB_SOURCE_SCHEMA = yup.object().shape({
    "childType": yup.string().required("Connector Type is Required in Source"),
    "id": yup.string().required("Connector is Required in Source"),
    "databaseId": yup.string().required("Database Id is Required in Source"),
    "query": yup.string().required("Query is Required in Source"),
    "collection": yup.string().required("Collection is Required in Source"),
})

const MONGODB_SCHEMA = yup.object().shape({
    "childType": yup.string(),
    "type": yup.string(),
    "collection": yup.string().required("In Sink Point, Collection Name is required").matches(/^(?!\s+$)/, 'In Sink Point, Collection name should not be blank'),
    "ttl": yup.number().typeError("In Sink Point, Time to Live must be an Integer ").positive().when("collection", {
        is: true,
        then: yup.number().required()
    }),
    "filterKeys": yup.array(),
    "index": yup.array().of(yup.array().of(
        yup.object().shape({
            "name": yup.string().required("In Sink Point, Index must have a valid value"),
            "value": yup.number()
        })
    )),
    "name": yup.string(),
    "databaseId": yup.string()
})

const MQTT_SCHEMA = yup.object().shape({
    "childType": yup.string(),
    "topic": yup.string().required("In Sink Connector, MQTT topic Name is required.")
})

const TIMESCALEDB_SCHEMA = yup.object().shape({
    "childType": yup.string(),
    "name": yup.string(),
    "databaseId": yup.string(),
    "table": yup.string().required("In Sink Point, Select Table From Dropdown"),
    "ttl": yup.number()
})

const S3_SCHEMA = yup.object().shape({
    "childType": yup.string(),
    "bucketId": yup.string().required("In Sink Point, Bucket Id is Required"),
    "format": yup.string().required("In Sink Point,Format is Required"),
    "dataDir": yup.string().test('len', 'In Sink Point, Directory Name is Required', val => val.length >= 2).matches(/^(?!\s+$)/, 'In Sink Point, Directory name should not be blank'),
    "partitionBy": yup.array().min(1, "In Sink Point, Partition must have atleast 1 Item")
})

const AGGREGATE_OPERATION_SCHEMA = yup.object().shape({
    type: yup.string(),
    groupBy: yup.array().min(1, "Atleast one value is required in Group By in the last Aggregate operation in Definition."),
    operations: yup.array().min(1).of(yup.object().shape({
        type: yup.string().required("Operation Type Missing in one of the conditions in the last Aggregate operation in Definition."),
        field: yup.string().when("type", {
            is: (val) => val !== "",
            then: yup.string().required("Field Missing in one of the conditions in the last Aggregate operation in Definition.")
        }),
        alias: yup.string().when(["type", "field"], {
            is: (val) => val !== "",
            then: yup.string().required("Alias Missing in one of the conditions in the last Aggregate operation in Definition.")
        })
    })),
    window: yup.object().shape({
        enable: yup.boolean().required(),
        windowColumn: yup.string(),
        windowInterval: yup.string(),
        slidingWindowInterval: yup.string()
    })
})

const SINK_CONNECTOR_PAYLOADS = {
    "MONGODB": {
        "childType": "mongoSink",
        "type": "insert",
        "collection": "",
        "ttl": 1,
        "filterKeys": [],
        "index": [
            [
                {
                    "name": "",
                    "value": -1
                }
            ]
        ],
        "name": "",
        "databaseId": "",
    },
    "TIMESCALEDB": {
        "childType": "timescaleSink",
        "name": "timescaleSinkConnector",
        "databaseId": "",
        "table": "",
        "ttl": 0
    },
    "S3": {
        "childType": "s3Sink",
        "bucketId": "",
        "format": "json",
        "dataDir": "",
        "partitionBy": []
    },
    "MQTT": {
        "childType": "mqttSink",
        "topic": ""
    },
}
let ETL_NAME_REGEX = allRegexPatterns.alphabeticStart_aplhanumericEnd_aplhanumericAndUndercoreInMiddle
/* eslint-disable react/prefer-stateless-function */

let isBasicInfoComplete = false;
let isSchemaFetchingPending = false;
export class NewAddOrEditEtlConfig extends React.Component {
    state = {
        functionModal: false,
        debugWindow: false,
        payload: {
            "type": "stream",
            "name": "",
            "description": "",
            "etlConfig": {
                "maxOffset": 1000,
                "triggerInterval": 5000,
                "threshold": 10
            },
            "sourceConnector": {
                "childType": "mqttSource",
                "id": "",
                "properties": {
                    "topicId": ""
                }
            },
            "definition": {
                "operations": [
                    {
                        "operations": [],
                        "type": "select",
                    }
                ],
                "connector": {
                    "type": "sink",
                    "id": "",
                    "name": "",
                    "sinkConnector": {},
                },
            },
            "attributeETL": false,
            "createdFor": "Analytics",
            "isDebug": false,
            "state": "stop",
            "timeSeries": {},
        },
        isFetchingSparkFunctions: true,
        sourceWindowVisibility: true,
        currentTab: "Attributes",
        sinkPointTypes: [],
        timescaleConnectortables: [],
        isFetchingSinkPointTypes: true,
        sinkType: "",
        lastFocussedOperationIndex: 0,
        isFetching: Boolean(this.props.match.params.id),
        topicSchema: {},
        createdSchemaTopicId: "",
        clearSchemaConfirmation: false,
        dataView: "TABLE",
        indexsuggestions: [],
        sourceConnectorType: "MQTT",
        connectorsListByCategory: {
            "MQTT": { "loader": true, "data": [] },
            "MONGODB": { "loader": true, "data": [] },
            "S3": { "loader": true, "data": [] }
        },
        sinkConnectorCategory: "",
        attributesArray: []
    };

    componentDidMount() {
        this.props.getDataConnectors();
        this.props.getSparkFunctions();
        getMqttConnection();
        this.props.getConnectorsByCategory("MQTT", "isFromMount");
    }

    componentWillUnmount() {
        closeMqttConnection();
    }

    static getDerivedStateFromProps(nextProps, state) {
        let newProp = Object.keys(SELECTORS).find(prop => nextProps[prop])
        if (newProp) {
            let propData = nextProps[newProp].error ? nextProps[newProp].error : nextProps[newProp].response;

            if (nextProps.getETLByIdSuccess) {
                let sinkConnectorCategory,
                    sinkType,
                    payload = propData,
                    sourceConnectorType = payload.sourceConnector.childType == "mqttSource" ? "MQTT" : payload.sourceConnector.childType == "mongoSource" ? "MONGODB" : "S3";
                payload.etlConfig.maxOffset = Number(payload.etlConfig.maxOffset)
                let requiredSourceConnector = state.connectorsListByCategory[sourceConnectorType].data.find(connector => connector.id === payload.sourceConnector.id),
                    requiredSourceTopic = requiredSourceConnector && requiredSourceConnector.properties.topics.find(topic => topic.topicId === payload.sourceConnector.properties.topicId),
                    defaultAttributes = requiredSourceTopic ? JSON.parse(requiredSourceTopic.schema) : "";
                if (!defaultAttributes && payload.sourceConnector.childType == "mqttSource") {
                    payload.definition.operations = [
                        {
                            "operations": [],
                            "type": "select",
                        }
                    ]
                }
                switch (propData.definition.connector.sinkConnector.childType) {
                    case "s3Sink":
                        sinkConnectorCategory = "S3";
                        sinkType = "cloud storage";
                        break;
                    case "timescaleSink":
                        sinkConnectorCategory = "TIMESCALEDB";
                        sinkType = "sql databases";
                        break;
                    case "mongoSink":
                        sinkConnectorCategory = "MONGODB";
                        sinkType = "nosql databases";
                        break;
                }
                nextProps.getConnectorsByCategory(sinkConnectorCategory);
                return {
                    payload,
                    defaultPayload: cloneDeep(payload),
                    sinkConnectorCategory,
                    sinkType,
                    isFetchingConnectorsByCategory: true,
                    defaultAttributes,
                    attributes: defaultAttributes,
                    attributesArray: [defaultAttributes],
                    isFetching: false,
                    sourceConnectorType
                }
            }
            if (nextProps.getETLByIdFailure) {
                return {
                    isFetching: false
                }
            }
            if (nextProps.getSparkFunctionsSuccess) {
                return {
                    sparkFunctions: propData,
                    isFetchingSparkFunctions: false,
                }
            }
            if (nextProps.getSparkFunctionsFailure) {
                return {
                    isFetchingSparkFunctions: false,
                }
            }
            if (nextProps.getTimescaleConnectorTablesSuccess) {
                return {
                    timescaleConnectortables: propData,
                    isFetchingTimescaleConnectorTables: false,
                }
            }
            if (nextProps.getTimescaleConnectorTablesFailure) {
                return {
                    isFetchingTimescaleConnectorTables: false,
                }
            }
            if (nextProps.generateSchemaFailure) {
                return {
                    isFetchingAttributes: false
                }
            }
            if (nextProps.getDataConnectorsSuccess) {
                let sinkPointTypes = propData;

                sinkPointTypes.map(sinkPointType => {
                    if (sinkPointType.connectors.filter(connector => connector.available && connector.displayName !== "MQTT").length > 0) {
                        sinkPointType.available = true;
                    } else {
                        sinkPointType.available = false;
                    }
                })

                /*let sinkPointTypes = propData.filter(sinkType => {
                    let availableCategory = sinkType.connectors.find(connector => connector.available)
                    return availableCategory && availableCategory.connector !== "MQTT"
                }).map(sinkPoint => {
                    sinkPoint.connectors = sinkPoint.connectors.filter(connector => connector.available);
                    return sinkPoint
                });*/
                let sinkType = "";
                let sinkConnectorCategory = "";
                let isFetchingConnectorsByCategory = false;
                let payload = state.payload;

                if (sinkPointTypes.length && !nextProps.match.params.id) {
                    sinkType = sinkPointTypes.filter(type => type.available)[0].type;
                    sinkConnectorCategory = sinkPointTypes.filter(type => type.available)[0].connectors.filter(type => type.available)[0].connector;
                    isFetchingConnectorsByCategory = true;
                    payload = produce(state.payload, payload => {
                        payload.definition.connector.sinkConnector = cloneDeep(SINK_CONNECTOR_PAYLOADS[sinkConnectorCategory])
                    })
                    nextProps.getConnectorsByCategory(sinkConnectorCategory)

                }

                return {
                    sinkPointTypes,
                    isFetchingSinkPointTypes: false,
                    sinkType,
                    payload,
                    sinkConnectorCategory,
                    isFetchingConnectorsByCategory
                }
            }
            if (nextProps.getDataConnectorsFailure) {
                return {
                    isFetchingSinkPointTypes: false,
                    isFetching: false,
                }
            }
            if (nextProps.getConnectorsByCategorySuccess) {
                let payload = state.payload,
                    responseCategory = nextProps.getConnectorsByCategorySuccess.category,
                    connectorsListByCategory = cloneDeep(state.connectorsListByCategory),
                    s3BucketName = state.s3BucketName,
                    s3BasePath = state.s3BasePath,
                    s3BucketRegion = state.s3BucketRegion,
                    timescaleConnectortables = state.timescaleConnectortables,
                    isFetchingTimescaleConnectorTables = state.isFetchingTimescaleConnectorTables;
                if (propData.length && !nextProps.getConnectorsByCategorySuccess.isFromMount) {
                    propData[0].category === "TIMESCALEDB" && (propData = propData.filter(connector => connector.name !== "IOT83_INTERNAL_TSDB"))
                    if (propData.length) {
                        payload = produce(payload, payload => {
                            let isSinkConnectorSelected = Boolean(payload.definition.connector.id),
                                sinkConnectorsList = propData.filter(el => el.sink)
                            let requiredSinkConnector = isSinkConnectorSelected ? sinkConnectorsList.find(connector => connector.id === payload.definition.connector.id) : sinkConnectorsList[0]
                            if (requiredSinkConnector) {
                                payload.definition.connector.name = requiredSinkConnector.name;
                                payload.definition.connector.id = requiredSinkConnector.id;
                            }
                            if (state.sinkConnectorCategory === "S3" && requiredSinkConnector) {
                                payload.definition.connector.sinkConnector.bucketId = requiredSinkConnector.properties.list[0].bucketId;
                                s3BucketName = requiredSinkConnector.properties.list[0].bucketName;
                                s3BasePath = requiredSinkConnector.properties.list[0].basePath;
                                s3BucketRegion = requiredSinkConnector.properties.list[0].region;
                            }
                            if (state.sinkConnectorCategory === "TIMESCALEDB" && requiredSinkConnector) {
                                payload.definition.connector.sinkConnector.name = requiredSinkConnector.name;
                                payload.definition.connector.sinkConnector.databaseId = requiredSinkConnector.properties.list[0].databaseId;
                                payload.definition.connector.sinkConnector.table = "";
                                timescaleConnectortables = [];
                                isFetchingTimescaleConnectorTables = true;
                                nextProps.getTimescaleConnectorTables(payload.definition.connector.id);
                            }
                            if (state.sinkConnectorCategory === "MONGODB" && requiredSinkConnector) {
                                payload.definition.connector.sinkConnector.name = requiredSinkConnector.name;
                                payload.definition.connector.sinkConnector.databaseId = requiredSinkConnector.properties.list[0].databaseId;
                            }
                        })
                    }
                }
                connectorsListByCategory[responseCategory].data = cloneDeep(propData);
                connectorsListByCategory[responseCategory].loader = false;
                if (nextProps.match.params.id && !state.payload.id) {
                    nextProps.getETLById(nextProps.match.params.id)
                }

                return {
                    isFetchingConnectorsByCategory: false,
                    payload,
                    connectorsListByCategory,
                    s3BucketName,
                    s3BasePath,
                    s3BucketRegion,
                    timescaleConnectortables,
                    isFetchingTimescaleConnectorTables,
                }
            }
            if (nextProps.getConnectorsByCategoryFailure) {
                return {
                    isFetchingConnectorsByCategory: false,
                }
            }
            if (nextProps.saveETLSuccess) {
                nextProps.showNotification("success", propData, () => nextProps.history.push('/etlPipeline'))
            }
            if (nextProps.saveETLFailure) {
                return {
                    isFetching: false,
                }
            }
            if (nextProps.createTopicSchemaSuccess) {
                let schema = propData.data;
                let connectorsListByCategory = cloneDeep(state.connectorsListByCategory);
                let requiredTopic = connectorsListByCategory["MQTT"].data.find(device => device.id === state.payload.sourceConnector.id).properties.topics.find(topic => topic.topicId === state.createdSchemaTopicId)
                requiredTopic.schema = schema;
                let defaultAttributes = requiredTopic ? JSON.parse(requiredTopic.schema) : "";
                nextProps.showNotification("success", propData.message)
                return {
                    isUpdatingAttributes: false,
                    connectorsListByCategory,
                    defaultAttributes: JSON.parse(schema),
                    attributes: JSON.parse(schema),
                    attributesArray: [defaultAttributes]
                }
            }
            if (nextProps.createTopicSchemaFailure) {
                return {
                    isCreatingSchema: false,
                    isUpdatingAttributes: false,
                }
            }

            if (nextProps.clearTopicSchemaSuccess) {
                nextProps.showNotification("success", propData)
                let connectorsListByCategory = cloneDeep(state.connectorsListByCategory)
                let requiredTopic = connectorsListByCategory["MQTT"].data.find(device => device.id === state.payload.sourceConnector.id).properties.topics.find(topic => topic.topicId === state.payload.sourceConnector.properties.topicId)
                requiredTopic.schema = "";
                let payload = produce(state.payload, payload => {
                    payload.definition.operations = [
                        {
                            "operations": [],
                            "type": "select",
                        }
                    ]
                })
                return {
                    isUpdatingAttributes: false,
                    defaultAttributes: "",
                    attributesArray: [],
                    attributes: "",
                    connectorsListByCategory,
                    payload
                }
            }

            if (nextProps.clearTopicSchemaFailure) {
                return {
                    isUpdatingAttributes: false
                }
            }

            if (nextProps.saveMiscTopicSuccess) {
                nextProps.showNotification("success", propData.message)
                let newTopic = {
                    topicId: propData.data,
                    name: state.miscTopicPrefix + state.miscTopicName,
                }
                let connectorsListByCategory = produce(state.connectorsListByCategory, draftState => {
                    let requiredConnector = draftState["MQTT"].data.find(connector => connector.id === state.miscTopicConnector);
                    requiredConnector.properties.topics.push(newTopic);
                })
                let payload = produce(state.payload, payload => {
                    payload.sourceConnector.properties.topicId = newTopic.topicId;
                    payload.sourceConnector.properties.topicName = newTopic.name;
                    payload.definition.operations = [
                        {
                            "operations": [],
                            "type": "select"
                        }
                    ]
                    if (state.sinkConnectorCategory === "MONGODB") {
                        payload.definition.connector.sinkConnector.index = [
                            [
                                {
                                    "name": "",
                                    "value": -1
                                }
                            ]
                        ]
                    }
                })
                return {
                    isSavingMiscTopic: false,
                    miscTopicModal: false,
                    miscTopicName: "",
                    miscTopicPrefix: "",
                    connectorsListByCategory,
                    payload,
                    attributes: "",
                    defaultAttributes: "",
                    attributesArray: [],
                    lastFocussedOperationIndex: 0,
                }
            }
            if (nextProps.saveMiscTopicFailure) {
                return {
                    isSavingMiscTopic: false
                }
            }
        }
        return null;
    }

    shouldComponentUpdate(nextProps, nextState) {
        let isNextBasicInfoComplete = this.validateBasicInfo(nextState);
        let isBasicInfoCheckToggled = isNextBasicInfoComplete !== isBasicInfoComplete;
        if (nextState.payload.sourceConnector.childType === "mqttSource") {
            isSchemaFetchingPending = false;
        } else if (isBasicInfoCheckToggled) {
            isSchemaFetchingPending = isNextBasicInfoComplete;
        }
        isBasicInfoComplete = isNextBasicInfoComplete;
        return true;
    }

    componentDidUpdate(prevProps, prevState) {
        let newProp = Object.keys(SELECTORS).find(prop => this.props[prop])
        if (newProp) {
            if (newProp.includes("Failure")) {
                this.props.showNotification("error", this.props[newProp].error)
            }
            this.props.resetToInitialState(newProp)
        }
        if (((prevState.payload.sourceConnector !== this.state.payload.sourceConnector) || (prevState.payload.definition.operations !== this.state.payload.definition.operations))) {
            isSinkDetailAutofilled = false;
        }
        if (((prevState.payload.sourceConnector !== this.state.payload.sourceConnector) || (prevState.payload.definition.operations !== this.state.payload.definition.operations)) && isDefinitionAutofilled && ((Date.now() - isDefinitionAutofilled) > 100)) {
            isDefinitionAutofilled = false;
        }
    }

    basicDetailsChangeHandler = ({ currentTarget: { id, value } }) => {
        let sourceConnectorType = this.state.sourceConnectorType,
            payload = cloneDeep(this.state.payload);
        payload[id] = value
        if (id == "type") {
            let connectorObj = SOURCE_CONNECTORS.find(sourceConnector => sourceConnector.type === (value.charAt(0).toUpperCase() + value.slice(1)))
            sourceConnectorType = connectorObj.name
            if (this.state.connectorsListByCategory[sourceConnectorType].loader) {
                this.props.getConnectorsByCategory(sourceConnectorType, "isFromMount")
            }
            payload.attributeETL = sourceConnectorType == "MQTT"
            payload.sourceConnector = cloneDeep(SOURCE_CONNECTOR_PAYLOADS[sourceConnectorType])
            this.setState({
                attributeView: "",
                attributes: "",
                attributesArray: [],
                defaultAttributes: "",
                isFetchingAttributes: false,
                lastFocussedOperationIndex: 0
            })
        }
        this.setState({
            payload,
            sourceConnectorType,
        })
    }

    etlConfigChangeHandler = ({ currentTarget }) => {
        let value = currentTarget.value === "" ? "" : Number(currentTarget.value)
        let isValidValue = value >= 0 || currentTarget.value === ""
        if (isValidValue) {
            let payload = produce(this.state.payload, payload => {
                payload[currentTarget.name][currentTarget.id] = value
            })
            this.setState({
                payload
            })
        }
        else {
            this.props.showNotification("error", "Input value should not be less than 0")
        }
    }

    sourceTopicChangeHandler = (sourceConnector, selectedTopicId) => {
        let requiredTopic = sourceConnector && sourceConnector.properties.topics.find((topic) => topic.topicId === selectedTopicId);
        let topicId = selectedTopicId;
        let topicName = sourceConnector && requiredTopic ? requiredTopic.name : selectedTopicId;
        if (topicName.endsWith("miscellaneous/#")) {
            this.setState({
                miscTopicConnector: sourceConnector.id,
                miscTopicModal: true,
                miscTopicPrefix: topicName.replace("#", ""),
                miscTopicName: "",
            })
            return;
        }
        let payload = produce(this.state.payload, payload => {
            payload.sourceConnector.properties.topicId = topicId;
            payload.sourceConnector.properties.topicName = topicName;
            payload.definition.operations = [
                {
                    "operations": [],
                    "type": "select"
                }
            ]
            if (requiredTopic && topicName === payload.definition.connector.sinkConnector.topic) {
                payload.definition.connector.sinkConnector.topic = ""
            }

            if (this.state.sinkConnectorCategory === "MONGODB") {
                payload.definition.connector.sinkConnector.index = [
                    [
                        {
                            "name": "",
                            "value": -1
                        }
                    ]
                ]
            }
        })
        let defaultAttributes = "";
        defaultAttributes = requiredTopic && requiredTopic.schema ? JSON.parse(requiredTopic.schema) : "";

        this.setState({
            payload,
            attributes: defaultAttributes,
            defaultAttributes,
            lastFocussedOperationIndex: 0,
            miscTopicModal: false,
            miscTopicName: "",
            miscTopicPrefix: "",
            attributesArray: [defaultAttributes],
        })
    }

    sourceDataBaseChangeHandler = (sourceConnector, currentTarget) => {
        let payload = produce(this.state.payload, payload => {
            payload.sourceConnector[currentTarget.id] = currentTarget.value;
            payload.definition.operations = [
                {
                    "operations": [],
                    "type": "select"
                }
            ]

            if (this.state.sinkConnectorCategory === "MONGODB") {
                payload.definition.connector.sinkConnector.index = [
                    [
                        {
                            "name": "",
                            "value": -1
                        }
                    ]
                ]
            }
        })

        this.setState({
            payload
        })
    }

    sourceBucketChangeHandler = (sourceConnector, currentTarget) => {
        let payload = produce(this.state.payload, payload => {
            payload.sourceConnector[currentTarget.id] = currentTarget.value;
            if (currentTarget.id == "bucketId") {
                payload.sourceConnector.basePath = sourceConnector.properties.list.find(el => el.bucketId == currentTarget.value).basePath
            }
            payload.definition.operations = [
                {
                    "operations": [],
                    "type": "select"
                }
            ]

            if (this.state.sinkConnectorCategory === "MONGODB") {
                payload.definition.connector.sinkConnector.index = [
                    [
                        {
                            "name": "",
                            "value": -1
                        }
                    ]
                ]
            }
        })

        this.setState({
            payload,
            attributes: "",
            defaultAttributes: "",
            lastFocussedOperationIndex: 0,
            attributesArray: [],
            isFetchingAttributes: false
        })
    }

    sourceConnectorChangeHandler = (isChecked, sourceConnector) => {
        let sourceConnectorId = sourceConnector.id;
        let payload = produce(this.state.payload, payload => {
            payload.sourceConnector.id = isChecked ? sourceConnectorId : ""
            switch (this.state.sourceConnectorType) {
                case "MQTT": {
                    payload.sourceConnector.properties.topicId = ""
                }
                case "S3": {
                    payload.sourceConnector.bucketId = ""
                    payload.sourceConnector.format = ""
                }
                case "MONGODB": {
                    payload.sourceConnector.databaseId = ""
                    payload.sourceConnector.query = ""
                    payload.sourceConnector.collection = ""
                }
            }
            payload.definition.operations = [
                {
                    "operations": [],
                    "type": "select"
                }
            ]
            if (this.state.sinkConnectorCategory === "MONGODB") {
                payload.definition.connector.sinkConnector.index = [
                    [
                        {
                            "name": "",
                            "value": -1
                        }
                    ]
                ]
            }
        })
        this.setState({
            payload,
            attributes: "",
            defaultAttributes: "",
            lastFocussedOperationIndex: 0,
            attributesArray: [],
            isFetchingAttributes: false
        })
    }

    operationsChangeHandler = (operations, index) => {
        let notAllowed = operations.slice(0, operations.length - 1).find(test => test.split(" ")[2] === operations[operations.length - 1].split(" ")[2])
        if (notAllowed) {
            this.props.showNotification("error", `Attribute already exists in this operation.`)
            return;
        }
        let payload = produce(this.state.payload, payload => {
            payload.definition.operations[index].operations = operations
            payload.definition.operations = payload.definition.operations.slice(0, index + 1)
            if (this.state.sinkConnectorCategory === "MONGODB") {
                payload.definition.connector.sinkConnector.index = [
                    [
                        {
                            "name": "",
                            "value": -1
                        }
                    ]
                ]
            }
        })
        this.setState({
            payload,
            attributesArray: []
        })
    }

    aggregateGroupByChangeHandler = (groupBy, index) => {
        let payload = produce(this.state.payload, payload => {
            payload.definition.operations[index].groupBy = groupBy
            payload.definition.operations = payload.definition.operations.slice(0, index + 1)
            if (this.state.sinkConnectorCategory === "MONGODB") {
                payload.definition.connector.sinkConnector.index = [
                    [
                        {
                            "name": "",
                            "value": -1
                        }
                    ]
                ]
            }
        })
        this.setState({
            payload,
            lastFocussedOperationIndex: index
        })
    }

    aggregateWindowDetailsChangeHandler = (key, index, value) => {
        let payload = produce(this.state.payload, payload => {
            payload.definition.operations[index].window[key] = value
            payload.definition.operations = payload.definition.operations.slice(0, index + 1)
            if (this.state.sinkConnectorCategory === "MONGODB") {
                payload.definition.connector.sinkConnector.index = [
                    [
                        {
                            "name": "",
                            "value": -1
                        }
                    ]
                ]
            }
        })
        this.setState({
            payload,
            lastFocussedOperationIndex: index,
        })
    }

    operationTypeChangeHandler = (currentTarget, index) => {
        let operationType = currentTarget.value
        let payload = produce(this.state.payload, payload => {
            if (operationType === "aggregate") {
                payload.definition.operations[index] = {
                    type: "aggregate",
                    groupBy: [],
                    operations: [{ ...DEFAULT_AGGREGATE_CONDITION }],
                    window: {
                        enable: false,
                        windowColumn: "",
                        windowInterval: "",
                        slidingWindowInterval: ""
                    }
                }
            }
            else {
                payload.definition.operations[index] = {
                    "operations": [],
                    "type": operationType,
                }
            }
            payload.definition.operations = payload.definition.operations.slice(0, index + 1)
            if (this.state.sinkConnectorCategory === "MONGODB") {
                payload.definition.connector.sinkConnector.index = [
                    [
                        {
                            "name": "",
                            "value": -1
                        }
                    ]
                ]
            }
        })
        this.setState({
            payload,
            lastFocussedOperationIndex: index,
        })
    }

    addNewOperation = async () => {
        let isNewOperationAllowed = await this.isNewOperationAllowed()
        if (isNewOperationAllowed) {
            let payload = produce(this.state.payload, payload => {
                payload.definition.operations.push(cloneDeep(DEFAULT_OPERATION))
            })
            this.setState({
                payload,
            })
        }
    }

    removeOperationHandler = ({ currentTarget }) => {
        let payload = produce(this.state.payload, payload => {
            payload.definition.operations = payload.definition.operations.slice(0, currentTarget.id)
            if (this.state.sinkConnectorCategory === "MONGODB") {
                payload.definition.connector.sinkConnector.index = [
                    [
                        {
                            "name": "",
                            "value": -1
                        }
                    ]
                ]
            }
        })
        let lastFocussedOperationIndex = this.state.lastFocussedOperationIndex == currentTarget.id ? this.state.lastFocussedOperationIndex - 1 : this.state.lastFocussedOperationIndex;
        let isFetchingAttributes = this.state.lastFocussedOperationIndex == currentTarget.id ? false : this.state.isFetchingAttributes;
        this.setState({
            payload,
            lastFocussedOperationIndex,
            isFetchingAttributes
        })
    }

    tabChangehandler = ({ currentTarget }) => {
        this.setState({ currentTab: currentTarget.id })
    }

    onCopyHandler = () => {
        this.props.showNotification("success", "Function copied to clipboard")
    }

    validateBasicInfo = (state) => {
        let { name, etlConfig: { maxOffset, triggerInterval, threshold }, sourceConnector } = state.payload;
        return Boolean(
            name &&
            ETL_NAME_REGEX.test(name) &&
            typeof (maxOffset) == "number" &&
            typeof (triggerInterval) == "number" &&
            typeof (threshold) == "number" &&
            sourceConnector.id &&
            this.isSourceDetailsFilled(sourceConnector)
        );
    }

    toggleWindowVisibility = ({ currentTarget }) => {
        this.setState((prevState => ({
            [currentTarget.id + "Visibility"]: !prevState[currentTarget.id + "Visibility"]
        })))
    }

    fetchSchema = (index) => {
        if (this.state.attributesArray[index]) {
            this.setState(prevState => ({
                isFetchingAttributes: false,
                currentTab: 'Attributes',
                attributes: prevState.attributesArray[index],
                lastFocussedOperationIndex: index,
            }));
        } else {
            if ((this.state.sourceConnectorType != "MQTT") || index) {
                if (this.state.sourceConnectorType != "MQTT" && !index) {
                    isSchemaFetchingPending = false;
                }
                let payloadSchema = {
                    "createdFor": "analytics",
                    "connector": this.state.payload.sourceConnector,
                    "definition": this.state.payload.definition.operations.slice(0, index + 1),
                    "description": this.state.payload.description,
                    "etlConfig": this.state.payload.etlConfig,
                    "isDebug": false,
                    "name": this.state.payload.name,
                    "state": "stop",
                    "type": this.state.payload.type,
                };

                let oldTopic = this.state.subscribedTopic;
                if (oldTopic) {
                    unsubscribeTopic(oldTopic, "doNotCloseConnection");
                }

                this.setState(prevState => ({
                    currentTab: 'Attributes',
                    attributes: "",
                    isFetchingAttributes: true,
                    subscribedTopic: `SCHEMA/${localStorage.getItem("tenant")}/${localStorage.getItem("selectedProjectId")}/${prevState.payload.name}`,
                    lastFocussedOperationIndex: index,
                }), () => {
                    let onClientConnect = () => {
                        this.props.generateSchema(payloadSchema)
                    }

                    let mqttMessagehandler = (message, topic) => {
                        if (message.error) {
                            this.props.showNotification("error", message.error)
                        }
                        let attributesArray = [...this.state.attributesArray];
                        attributesArray[index] = message.error ? "" : JSON.parse(message.schema);
                        this.setState({
                            attributes: message.error ? "" : JSON.parse(message.schema),
                            attributesArray,
                            isFetchingAttributes: false
                        })
                    }

                    subscribeTopic(this.state.subscribedTopic, mqttMessagehandler, onClientConnect)
                })
            }
            else {
                this.setState(prevState => ({
                    isFetchingAttributes: false,
                    currentTab: 'Attributes',
                    attributes: prevState.defaultAttributes,
                    attributesArray: [prevState.defaultAttributes],
                    lastFocussedOperationIndex: index,
                }));
            }
        }
    }

    sinkTypeChangehandler = ({ currentTarget }) => {
        let sinkConnectorCategory = this.state.sinkPointTypes.find(type => type.type === currentTarget.value).connectors.filter(type => type.available)[0].connector,
            isFetchingConnectorsByCategory = this.state.connectorsListByCategory[sinkConnectorCategory].loader;

        let s3BucketName = this.state.s3BucketName;
        let s3BasePath = this.state.s3BasePath;
        let s3BucketRegion = this.state.s3BucketRegion;
        let timescaleConnectortables = this.state.timescaleConnectortables;
        let isFetchingTimescaleConnectorTables = this.state.isFetchingTimescaleConnectorTables;

        let payload = produce(this.state.payload, payload => {
            payload.definition.connector.id = ""
            payload.definition.connector.name = ""
            payload.definition.connector.sinkConnector = cloneDeep(SINK_CONNECTOR_PAYLOADS[sinkConnectorCategory])
            if (!isFetchingConnectorsByCategory) {
                let requiredSinkConnector = this.state.connectorsListByCategory[sinkConnectorCategory].data.filter(el => el.sink)[0]
                payload.definition.connector.id = requiredSinkConnector ? requiredSinkConnector.id : ""
                payload.definition.connector.name = requiredSinkConnector ? requiredSinkConnector.id : ""
                if (sinkConnectorCategory === "S3") {
                    payload.definition.connector.sinkConnector.bucketId = requiredSinkConnector.properties.list[0].bucketId;
                    s3BucketName = requiredSinkConnector.properties.list[0].bucketName;
                    s3BasePath = requiredSinkConnector.properties.list[0].basePath;
                    s3BucketRegion = requiredSinkConnector.properties.list[0].region;
                }
                if (sinkConnectorCategory === "TIMESCALEDB") {
                    payload.definition.connector.sinkConnector.name = requiredSinkConnector.name;
                    payload.definition.connector.sinkConnector.databaseId = requiredSinkConnector.properties.list[0].databaseId;
                    payload.definition.connector.sinkConnector.table = "";
                    timescaleConnectortables = [];
                    isFetchingTimescaleConnectorTables = true;
                }
                if (sinkConnectorCategory === "MONGODB") {
                    payload.definition.connector.sinkConnector.name = requiredSinkConnector.name;
                    payload.definition.connector.sinkConnector.databaseId = requiredSinkConnector.properties.list[0].databaseId;
                }
            }
        })
        this.setState(prevState => ({
            sinkType: currentTarget.value,
            sinkConnectorCategory,
            isFetchingConnectorsByCategory,
            payload,
        }), () => this.state.isFetchingConnectorsByCategory && this.props.getConnectorsByCategory(this.state.sinkConnectorCategory))
    }

    sinkConnectorCategoryChangehandler = ({ currentTarget }) => {
        let sinkConnectorCategory = currentTarget.value,
            isFetchingConnectorsByCategory = Boolean(this.state.connectorsListByCategory[sinkConnectorCategory].loader);
        let s3BucketName = this.state.s3BucketName;
        let s3BasePath = this.state.s3BasePath;
        let s3BucketRegion = this.state.s3BucketRegion;
        let timescaleConnectortables = this.state.timescaleConnectortables;
        let isFetchingTimescaleConnectorTables = this.state.isFetchingTimescaleConnectorTables;

        let payload = produce(this.state.payload, payload => {
            payload.definition.connector.sinkConnector = cloneDeep(SINK_CONNECTOR_PAYLOADS[sinkConnectorCategory]);
            payload.definition.connector.id = ""
            payload.definition.connector.name = ""
            if (!isFetchingConnectorsByCategory) {
                let requiredSinkConnector = this.state.connectorsListByCategory[sinkConnectorCategory].data.filter(el => el.sink)[0]
                payload.definition.connector.id = requiredSinkConnector ? requiredSinkConnector.id : ""
                payload.definition.connector.name = requiredSinkConnector ? requiredSinkConnector.id : ""
                if (sinkConnectorCategory === "S3") {
                    payload.definition.connector.sinkConnector.bucketId = requiredSinkConnector.properties.list[0].bucketId;
                    s3BucketName = requiredSinkConnector.properties.list[0].bucketName;
                    s3BasePath = requiredSinkConnector.properties.list[0].basePath;
                    s3BucketRegion = requiredSinkConnector.properties.list[0].region;
                }
                if (sinkConnectorCategory === "TIMESCALEDB") {
                    payload.definition.connector.sinkConnector.name = requiredSinkConnector.name;
                    payload.definition.connector.sinkConnector.databaseId = requiredSinkConnector.properties.list[0].databaseId;
                    payload.definition.connector.sinkConnector.table = "";
                    timescaleConnectortables = [];
                    isFetchingTimescaleConnectorTables = true;
                }
                if (sinkConnectorCategory === "MONGODB") {
                    payload.definition.connector.sinkConnector.name = requiredSinkConnector.name;
                    payload.definition.connector.sinkConnector.databaseId = requiredSinkConnector.properties.list[0].databaseId;
                }
            }
        })
        this.setState(prevState => ({
            sinkConnectorCategory,
            isFetchingConnectorsByCategory,
            payload,
        }), () => this.state.isFetchingConnectorsByCategory && this.props.getConnectorsByCategory(sinkConnectorCategory))
    }

    sinkConnectorChangehandler = ({ target }) => {
        let s3BucketName = this.state.s3BucketName;
        let s3BasePath = this.state.s3BasePath;
        let s3BucketRegion = this.state.s3BucketRegion;
        let timescaleConnectortables = this.state.timescaleConnectortables;
        let isFetchingTimescaleConnectorTables = this.state.isFetchingTimescaleConnectorTables;
        let payload = produce(this.state.payload, payload => {
            payload.definition.connector.id = target.value
            payload.definition.connector.name = target.options[target.selectedIndex].getAttribute('name')
            let requiredSinkConnector = this.state.connectorsListByCategory[this.state.sinkConnectorCategory].data.find(connector => connector.id === target.value)
            if (this.state.sinkConnectorCategory === "S3") {
                payload.definition.connector.sinkConnector.bucketId = requiredSinkConnector.properties.list[0].bucketId;
                s3BucketName = requiredSinkConnector.properties.list[0].bucketName;
                s3BasePath = requiredSinkConnector.properties.list[0].basePath;
                s3BucketRegion = requiredSinkConnector.properties.list[0].region;
            }
            if (this.state.sinkConnectorCategory === "TIMESCALEDB") {
                payload.definition.connector.sinkConnector.name = requiredSinkConnector.name;
                payload.definition.connector.sinkConnector.databaseId = requiredSinkConnector.properties.list[0].databaseId;
                payload.definition.connector.sinkConnector.table = "";
                timescaleConnectortables = [];
                isFetchingTimescaleConnectorTables = true;
            }
            if (this.state.sinkConnectorCategory === "MONGODB") {
                payload.definition.connector.sinkConnector.name = requiredSinkConnector.name;
                payload.definition.connector.sinkConnector.databaseId = requiredSinkConnector.properties.list[0].databaseId;
            }
        })
        this.setState({
            payload,
            s3BucketName,
            s3BasePath,
            s3BucketRegion,
            timescaleConnectortables,
            isFetchingTimescaleConnectorTables,
        }, () => {
            this.state.isFetchingTimescaleConnectorTables && this.props.getTimescaleConnectorTables(this.state.payload.definition.connector.id);
        })
    }

    sinkDetailsChangeHandler = (key, value) => {
        if (key === "ttl") {
            value < 0 && this.props.showNotification("error", "Time To Live should not be less than 0")
            value = (value >= 0) ? value : ""
        }
        if (key === "collection" && !(/^[a-zA-Z0-9-_]+$/.test(value))) {
            return this.props.showNotification("error", "Only Alphanumeric characters, hyphen and underscore are allowed in Collection Name.")
        }
        let payload = produce(this.state.payload, payload => {
            payload.definition.connector.sinkConnector[key] = value;
        })
        this.setState({
            payload,
        })
    }

    addMongoParentIndex = () => {
        let payload = produce(this.state.payload, payload => {
            payload.definition.connector.sinkConnector.index.push([{
                "name": "",
                "value": -1
            }])
        })
        this.setState({
            payload,
        })
    }

    deleteMongoParentIndex = (parentIndex) => {
        let payload = produce(this.state.payload, payload => {
            payload.definition.connector.sinkConnector.index.splice(parentIndex, 1)
        })
        this.setState({
            payload,
        })
    }

    addMongoChildIndex = (parentIndex) => {
        let payload = produce(this.state.payload, payload => {
            payload.definition.connector.sinkConnector.index[parentIndex].push({
                "name": "",
                "value": -1
            })
        })
        this.setState({
            payload,
        })
    }

    deleteMongoChildIndex = (parentIndex, childIndex) => {
        let payload = produce(this.state.payload, payload => {
            payload.definition.connector.sinkConnector.index[parentIndex].splice(childIndex, 1)
        })
        this.setState({
            payload,
        })
    }

    mongoIndexDetailsChangeHandler = (key, parentIndex, childIndex, value) => {
        let payload = produce(this.state.payload, payload => {
            payload.definition.connector.sinkConnector.index[parentIndex][childIndex][key] = value
        })
        this.setState({
            payload,
        })
    }

    getIndexeOptionsForMongoSink = (selectedIndex) => {
        if (mongoIndexes) {
            let sinkDetails = this.state.payload.definition.connector.sinkConnector
            let filteredIndexes = mongoIndexes.filter((index) => (!sinkDetails.index.some((el) => el.some((child) => child.name === index)) || selectedIndex === index))
            if (filteredIndexes.length) {
                return <React.Fragment>
                    <option value=""> Select</option>
                    {filteredIndexes.map((index) => <option value={index} key={index}>{index}
                    </option>)}
                </React.Fragment>
            }
            else {
                return <option value="">No Valid Index Found</option>
            }
        }
        else {
            return <option value="disabled">Operations/Topic Schema missing</option>
        }
    }

    getMongoSinkHTML = () => {
        let sinkDetails = this.state.payload.definition.connector.sinkConnector
        let isEditModeOn = this.props.match.params.id
        return <React.Fragment>
            <div className="d-flex">
                <div className="flex-30 pd-r-10">
                    <div className="form-group" >
                        <label className="form-group-label">Operation Type :</label>
                        <div className="d-flex">
                            <div className="flex-50 pd-r-5">
                                <label className={`radio-button ${isEditModeOn ? "radio-button-disabled" : ""}`}>
                                    <span className="radio-button-text">Insert</span>
                                    <input id="type" type="radio" value="insert" name="connectionType" disabled={isEditModeOn} onChange={({ currentTarget }) => this.sinkDetailsChangeHandler("type", currentTarget.value)} checked={sinkDetails.type === "insert"} />
                                    <span className="radio-button-mark" />
                                </label>
                            </div>
                            <div className="flex-50 pd-l-5">
                                <label className={`radio-button ${isEditModeOn ? "radio-button-disabled" : ""}`}>
                                    <span className="radio-button-text">Update</span>
                                    <input id="type" type="radio" value="update" name="connectionType" disabled={isEditModeOn} onChange={({ currentTarget }) => this.sinkDetailsChangeHandler("type", currentTarget.value)} checked={sinkDetails.type === "update"} />
                                    <span className="radio-button-mark" />
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="flex-40 pd-l-5 pd-r-5">
                    <div className="form-group">
                        <label className="form-group-label">Collection Name : <i className="fas fa-asterisk form-group-required"></i></label>
                        <input type="text" id="name" className="form-control" disabled={isEditModeOn} value={sinkDetails.collection} onChange={({ currentTarget }) => this.sinkDetailsChangeHandler("collection", currentTarget.value)} required />
                    </div>
                </div>
                <div className="flex-30 pd-l-10">
                    <div className="form-group">
                        <label className="form-group-label">Time To Live <small>(in days)</small> : <i className="fas fa-asterisk form-group-required"></i></label>
                        <input type="number" id="ttl" name="ttl" className="form-control" disabled={isEditModeOn} value={sinkDetails.ttl} onChange={({ currentTarget }) => this.sinkDetailsChangeHandler("ttl", currentTarget.value)} />
                    </div>
                </div>
            </div>

            {sinkDetails.type === "update" ? (
                <div className="form-group">
                    <label className="form-group-label">Filter Keys :</label>
                    <TagsInput addOnBlur rows="3" className="form-control" id="filterKeys" value={sinkDetails.filterKeys} onChange={(tags) => this.sinkDetailsChangeHandler("filterKeys", tags)} />
                </div>) : null
            }

            <div className="connector-form-list form-group">
                <label className="form-group-label">
                    <button className="btn btn-primary" disabled={isAddNewIndexButtonDisabled} onClick={this.addMongoParentIndex} ><i className="far fa-plus"></i></button>Add Index(s) :
                </label>
                {sinkDetails.index.map((index, parentIndex) =>
                    <div className="connector-form-body" key={parentIndex}>
                        <div className="d-flex pd-r-30">
                            <label className="form-group-label flex-50">Name :</label>
                            <label className="form-group-label flex-50">Value :</label>
                        </div>
                        <button className="btn btn-light box-top-right-button" disabled={sinkDetails.index.length == 1} onClick={() => this.deleteMongoParentIndex(parentIndex)}><i className="far fa-times"></i></button>
                        {index.map((subIndex, childIndex) =>
                            <ul className="list-style-none d-flex" key={childIndex + "subIndex"}>
                                <li className="flex-50">
                                    <select value={subIndex.name} className="form-control" disabled={!this.state.payload.definition.operations[0].operations.length} onChange={({ currentTarget }) => this.mongoIndexDetailsChangeHandler("name", parentIndex, childIndex, currentTarget.value)} required>
                                        {this.getIndexeOptionsForMongoSink(subIndex.name)}
                                    </select>
                                </li>
                                <li className="flex-50">
                                    <select name="deviceType" value={subIndex.value} disabled={!mongoIndexes} className="form-control" onChange={({ currentTarget }) => this.mongoIndexDetailsChangeHandler("value", parentIndex, childIndex, currentTarget.value)} required>
                                        <option value={-1}>Descending</option>
                                        <option value={1}>Ascending</option>
                                    </select>
                                </li>
                                <div className="button-group">
                                    <button className="btn-transparent btn-transparent-red" disabled={index.length == 1} onClick={() => this.deleteMongoChildIndex(parentIndex, childIndex)}><i className="far fa-trash-alt"></i></button>
                                </div>
                            </ul>
                        )}
                        <div className="text-center">
                            <button className="btn btn-link" onClick={() => this.addMongoChildIndex(parentIndex)} disabled={isAddNewIndexButtonDisabled}><i className="far fa-plus"></i>Add New</button>
                        </div>
                    </div>
                )}
            </div>
        </React.Fragment>
    }

    getMQTTSinkHTML = () => {
        let sinkDetails = this.state.payload.definition.connector.sinkConnector
        return <div className="form-group">
            <label className="form-group-label">Topic Name (MQTT) : <i className="fas fa-asterisk form-group-required"></i></label>
            <select className="form-control" id="name" required value={this.state.payload.definition.connector.sinkConnector.topic} onChange={this.mongoSinkTopicChangeHanler}>
                <option value="">Select Topic Name</option>
                {this.state.connectorsListByCategory["MQTT"].data.find((device, index) => device.name === this.state.payload.definition.connector.name).properties.topics.map((topic, i) => {
                    if (this.state.payload.sourceConnector.properties.topicName !== topic.name)
                        return <option value={topic.name} key={i}>{topic.name}</option>
                })}
            </select>
        </div>
    }

    getS3SinkHTML = () => {
        let sinkDetails = this.state.payload.definition.connector.sinkConnector

        return <div className="d-flex">
            <div className="flex-50 pd-r-7">
                <div className="form-group">
                    <label className="form-group-label">Bucket Name : <i className="fas fa-asterisk form-group-required"></i></label>
                    <select className="form-control" id="bucketId" value={sinkDetails.bucketId} disabled>
                        <option value={sinkDetails.bucketId}>{this.state.s3BucketName}</option>
                    </select>
                </div>
            </div>

            <div className="flex-50 pd-l-7">
                <div className="form-group">
                    <label className="form-group-label">Base Path : <i className="fas fa-asterisk form-group-required"></i></label>
                    <input type="text" id="basePath" name="basePath" className="form-control" value={this.state.s3BasePath} disabled />
                </div>
            </div>

            <div className="flex-50 pd-r-7">
                <div className="form-group">
                    <label className="form-group-label">Region : <i className="fas fa-asterisk form-group-required"></i></label>
                    <input type="text" className="form-control" id="region" name="region" value={this.state.s3BucketRegion} disabled />
                </div>
            </div>

            <div className="flex-50 pd-l-7">
                <div className="form-group">
                    <label className="form-group-label">Directory Name : <i className="fas fa-asterisk form-group-required"></i></label>
                    <input type="text" className="form-control" id="name" name="dataDir" value={sinkDetails.dataDir || "/"} onChange={this.directoryNameChangeHandler} />
                </div>
            </div>

            <div className="flex-50 pd-r-7">
                <div className="form-group">
                    <label className="form-group-label">Format : <i className="fas fa-asterisk form-group-required"></i></label>
                    <select className="form-control" id="format" value={sinkDetails.format} onChange={({ currentTarget }) => this.sinkDetailsChangeHandler("format", currentTarget.value)} >
                        <option value="json">JSON</option>
                        <option value="csv">CSV</option>
                        <option value="parquet">Parquet</option>
                    </select>
                </div>
            </div>

            {/*<div className="flex-20 pd-r-10">
        <div className="form-group">
            <label className="form-group-label">Add Time Series ?</label>
            <label className="check-box">
                <input type="checkbox" name="timeSeries" value="timeSeries" id="timeSeries" />
                <span className="check-mark" />
            </label>
        </div>
    </div>

    <div className="flex-40 pd-l-5 pd-r-5">
        <div className="form-group">
            <label className="form-group-label">Unit : <i className="fas fa-asterisk form-group-required"></i></label>
            <select className="form-control" id="unit">
                <option value="milliseconds">MilliSeconds</option>
                <option value="seconds">Seconds</option>
            </select>
        </div>
    </div>

    <div className="flex-40 pd-l-10">
        <div className="form-group">
            <label className="form-group-label">Column : <i className="fas fa-asterisk form-group-required"></i></label>
            <select className="form-control" id="column">
                <option disabled>Select</option>
            </select>
        </div>
    </div>*/}

            <div className="flex-100">
                <div className="form-group">
                    <label className="form-group-label">Partition By : <i className="fas fa-asterisk form-group-required"></i></label>
                    <TagsInput addOnBlur value={sinkDetails.partitionBy} onChange={(tags) => this.sinkDetailsChangeHandler("partitionBy", tags)} id="tagInput" className="form-control" />
                </div>
            </div>
        </div>
    }

    timescaleTableChangeHandler = ({ currentTarget }) => {
        let payload = produce(this.state.payload, payload => {
            payload.definition.connector.sinkConnector.table = currentTarget.value
        })
        this.setState({
            payload,
        })
    }

    getTimescaleSinkHTML = () => {
        let sinkDetails = this.state.payload.definition.connector.sinkConnector
        return <div className="form-group">
            <label className="form-group-label">Table : <i className="fas fa-asterisk form-group-required"></i></label>
            <select className="form-control" id="bucketId" value={sinkDetails.table} disabled={this.state.isFetchingTimescaleConnectorTables || !this.state.timescaleConnectortables.length} onChange={this.timescaleTableChangeHandler}>
                {this.state.isFetchingTimescaleConnectorTables ? <option value="">Fetching Tables..</option> :
                    this.state.timescaleConnectortables.length ? <React.Fragment>
                        <option value="">Select</option>
                        {this.state.timescaleConnectortables.map(table => <option key={table} value={table}>{table}</option>)}
                    </React.Fragment> :
                        <option value="">No Tables Found</option>}
            </select>
        </div>
    }

    getSinkHTMLByCategory = () => {
        switch (this.state.sinkConnectorCategory) {
            case "MQTT":
                return this.getMQTTSinkHTML();
            case "S3":
                return this.getS3SinkHTML();
            case "MONGODB":
                return this.getMongoSinkHTML();
            case "TIMESCALEDB":
                return this.getTimescaleSinkHTML();
            default:
                return "";
        }
    }

    getPayloadSchema = () => {
        let sinkConnectorCategory = this.state.sinkConnectorCategory,
            sourceConnectorType = this.state.sourceConnectorType;
        return yup.object().shape({
            "type": yup.string().required("ETL Type is Required"),
            "name": yup.string().required("ETL Name is Required").matches(ETL_NAME_REGEX, 'ETL name can start only with an alphabet, end with an aplhanumeric character and can include only alphanumeric characters and underscore in between.'),
            "description": yup.string(),
            "etlConfig": yup.object().shape({
                "maxOffset": yup.number().typeError("Max Offset must be an integer").positive().integer("Max Offset must be an integer"),
                "triggerInterval": yup.number().typeError("Trigger Interval must be an integer").positive().integer("Trigger Interval must be an integer").when("maxOffset", {
                    is: true,
                    then: yup.number().required(),
                }),
                "threshold": yup.number().typeError("Threshold must be an integer").positive().integer("Threshold must be an integer").when(["maxOffset", "triggerInterval"], {
                    is: true,
                    then: yup.number().required()
                })
            }),
            "sourceConnector": sourceConnectorType === "MONGODB" ? MONGODB_SOURCE_SCHEMA : sourceConnectorType === "S3" ? S3_SOURCE_SCHEMA : sourceConnectorType === "MQTT" ? MQTT_SOURCE_SCHEMA : yup.object().required("In Source Connector, Source Connector Details are Missing"),
            "definition": yup.object().shape({
                "operations": yup.array().of(
                    yup.object().shape({
                        "operations": yup.array().min(1, "Operation field must have atleast 1 item in Definition").of(yup.string()),
                        "type": yup.string().required("Operation type is required")
                    })
                ),
                "connector": yup.object().when("operations[0].operations", {
                    is: (val) => val.length > 0,
                    then: yup.object().shape({
                        "id": yup.string().required("In Sink Point, Connector is Required"),
                        "name": yup.string().required("In Sink Point, Connector is Required"),
                        "sinkConnector": sinkConnectorCategory === "MONGODB" ? MONGODB_SCHEMA : sinkConnectorCategory === "TIMESCALEDB" ? TIMESCALEDB_SCHEMA : sinkConnectorCategory === "S3" ? S3_SCHEMA : sinkConnectorCategory === "MQTT" ? MQTT_SCHEMA : yup.object().required("In Sink Point, Sink Connector Details are Missing"),
                        "type": yup.string().required()
                    }),
                })
            }),
            "attributeETL": yup.boolean().required("ETL Attributes are required"),
            "createdFor": yup.string().required(),
            "isDebug": yup.boolean().required(),
            "state": yup.string().required(),
            "timeSeries": yup.object(),
        });
    }

    saveETL = async () => {
        let PAYLOAD_SCHEMA = this.getPayloadSchema()
        try {
            await PAYLOAD_SCHEMA.validate(this.state.payload);
            let operations = this.state.payload.definition.operations;
            let lastOperation = operations[operations.length - 1]

            if (lastOperation.type === "aggregate") {
                await AGGREGATE_OPERATION_SCHEMA.validate(lastOperation);
            }
            this.setState({
                isFetching: true
            }, () => this.props.saveETL(this.state.payload))
        }
        catch (error) {
            this.props.showNotification("error", error.message)
        }

    }

    checkingFields = (m, index, name) => {
        if (m.type && m.type === "struct") {
            return this.attributeTree(m.fields, index, name)
        } else if (m.type && m.type === "array") {
            return this.checkingFields(m.elementType, index, name)
        } else {
            return null
        }
    }

    attributeTree = (data, pos, name) => {
        return (
            <ul className="collapse show" id={`attribute${pos}`}>
                {data.map((m, index) => {
                    return (
                        <li key={index}>
                            <div className="etl-attribute-tree-box">
                                <p data-toggle="collapse" data-target={m.type.type ? `#attribute${pos}_${index}` : ""}>
                                    {m.name}
                                    <span>{m.type.type ? Array.isArray(m.type) ? "(array)" : "(object)" : `(${m.type})`}</span>
                                </p>
                                <div className="button-group">
                                    <button className="btn btn-link text-green" data-tooltip data-tooltip-text="Copy" data-tooltip-place="bottom" value={`${name}.${m.name} as ${m.name}`} onClick={this.pasteAttributeInDefinition}>
                                        <i className="far fa-copy"></i>
                                    </button>
                                </div>
                            </div>
                            {m.type.type && m.type.type === "struct" ? this.attributeTree(m.type.fields, `${pos}_${index}`, `${name}.${m.name}`) : m.type.type === "array" ?
                                this.checkingFields(m.type.elementType, `${pos}_${index}`, `${name}.${m.name}`) : null
                            }
                        </li>
                    )
                }
                )}
            </ul>
        );
    }

    getTragetOperationIndex = (isAggregate) => {
        let targetOperationIndex = this.state.lastFocussedOperationIndex
        if (isAggregate) {
            targetOperationIndex = this.state.payload.definition.operations.findIndex(operation => operation.type !== "aggregate")
        }
        return targetOperationIndex
    }

    pasteAttributeInDefinition = ({ currentTarget }) => {
        let payload = this.state.payload;
        let targetOperationIndex = this.state.lastFocussedOperationIndex;
        let isAggregate = payload.definition.operations[targetOperationIndex].type === "aggregate";
        if (isBasicInfoComplete) {
            if (isAggregate) {
                this.copyToClipboard(currentTarget.value.split(" ").reverse()[0]);
            } else {
                let value = (targetOperationIndex === 0 && this.state.payload.sourceConnector.childType === "mqttSource") ? `root.${currentTarget.value}` : currentTarget.value;
                let isAlreadyPresent = payload.definition.operations[targetOperationIndex].operations.find(test => test.split(" ")[2] === value.split(" ")[2]) || payload.definition.operations[targetOperationIndex].operations.includes(value)
                if (isAlreadyPresent) {
                    this.props.showNotification("error", `Attribute already exists in operation at position ${targetOperationIndex + 1}`)
                } else {
                    payload = produce(this.state.payload, payload => {
                        payload.definition.operations[targetOperationIndex].operations.push(value)
                        payload.definition.operations = payload.definition.operations.slice(0, targetOperationIndex + 1)
                        if (this.state.sinkConnectorCategory === "MONGODB") {
                            payload.definition.connector.sinkConnector.index = [
                                [
                                    {
                                        "name": "",
                                        "value": -1
                                    }
                                ]
                            ]
                        }
                    })
                    this.props.showNotification("success", `Attribute Added to operation at position ${targetOperationIndex + 1}`)
                    this.setState({
                        payload,
                    })
                }
            }
        }
    }

    attributesViewChangehandler = ({ currentTarget }) => {
        this.setState({
            attributeView: currentTarget.id
        })
    }

    handleDebugging = (event) => {
        event.stopPropagation()
        let payloadSchema = {
            "createdFor": "analytics",
            "connector": this.state.payload.sourceConnector,
            "definition": this.state.payload.definition.operations,
            "description": this.state.payload.description,
            "etlConfig": this.state.payload.etlConfig,
            "isDebug": true,
            "name": this.state.payload.name,
            "state": "stop",
            "type": "stream"
        };
        this.setState({
            debugWindow: true,
            isDebugging: true,
        }, () => {
            let onClientConnect = () => {
                this.props.generateSchema(payloadSchema)
            }
            subscribeTopic(`DEBUG_${this.state.payload.sourceConnector.id}`, (message, topic) => {
                let isError = message.error || message.status === "Failed";
                if (isError) {
                    this.props.showNotification("error", message.error || message.data[0])
                }
                this.setState(prevState => ({
                    debugData: isError ? "" : [...message.data, ...(prevState.debugData || [])],
                    isDebugging: false
                }))
            }, onClientConnect)
        })
    }

    stopDebugging = () => {
        unsubscribeTopic(`DEBUG/${this.state.payload.sourceConnector.id}`, "doNotCloseConnection")
        this.setState({
            debugData: "",
            debugWindow: false,
            isDebugging: false
        })
    }

    showCreateSchemaModal = (topicId, topicName) => {
        this.setState((prevState, props) => ({
            createdSchemaTopicId: topicId,
            createdSchemaTopicName: topicName,
            opencreateSchemaModal: true,
            topicSchema: {}
        }))
    }

    closeCreateSchemaModal = () => {
        this.setState({
            createdSchemaTopicId: "",
            createdSchemaTopicName: "",
            opencreateSchemaModal: false,
            topicSchema: {}
        })
    }

    topicSchemaChangeHandler = (topicSchema) => {
        this.setState({
            topicSchema: topicSchema
        })
    }

    saveCreateSchemaModal = () => {
        this.setState({
            isUpdatingAttributes: true,
            opencreateSchemaModal: false,
        }, () => this.props.createTopicSchema(
            {
                "deviceTypeId": this.state.payload.sourceConnector.id,
                "json": this.state.topicSchema,
                "topic": this.state.createdSchemaTopicName
            }))
    }

    clearSniffingModal = (topicId) => {
        this.setState({
            debugData: []
        })
    }

    copyToClipboard = (copiedData) => {
        navigator.clipboard.writeText(copiedData)
        this.props.showNotification("success", 'Copied to Clipboard successfully !')
    }

    onCloseHandler = () => {
        this.setState({
            isOpen: false,
            message2: '',
            modalType: '',
        });
    }

    mongoSinkTopicChangeHanler = ({ currentTarget }) => {
        let payload = produce(this.state.payload, payload => {
            payload.definition.connector.sinkConnector.topic = currentTarget.value
        })
        this.setState({
            payload,
        });
    }

    addNewConditionInAggregate = (index) => {
        let payload = produce(this.state.payload, payload => {
            payload.definition.operations[index].operations.push({ ...DEFAULT_AGGREGATE_CONDITION })
            payload.definition.operations = payload.definition.operations.slice(0, index + 1)
            if (this.state.sinkConnectorCategory === "MONGODB") {
                payload.definition.connector.sinkConnector.index = [
                    [
                        {
                            "name": "",
                            "value": -1
                        }
                    ]
                ]
            }
        })
        this.setState({
            payload,
            lastFocussedOperationIndex: index
        });
    }

    deleteConditionInAggregate = (index, conditionIndex) => {
        let payload = produce(this.state.payload, payload => {
            payload.definition.operations[index].operations.splice(conditionIndex, 1)
            payload.definition.operations = payload.definition.operations.slice(0, index + 1)
            if (this.state.sinkConnectorCategory === "MONGODB") {
                payload.definition.connector.sinkConnector.index = [
                    [
                        {
                            "name": "",
                            "value": -1
                        }
                    ]
                ]
            }
        })
        this.setState({
            payload,
            lastFocussedOperationIndex: index,
        });
    }

    aggregateConditionChangeHandler = (key, value, index, conditionIndex) => {
        let payload = produce(this.state.payload, payload => {
            payload.definition.operations[index].operations[conditionIndex][key] = value
            payload.definition.operations = payload.definition.operations.slice(0, index + 1)
            if (this.state.sinkConnectorCategory === "MONGODB") {
                payload.definition.connector.sinkConnector.index = [
                    [
                        {
                            "name": "",
                            "value": -1
                        }
                    ]
                ]
            }
        })
        this.setState({
            payload,
            lastFocussedOperationIndex: index,
        }, () => {
            key === "type" && this.fetchSchema(index)
        });
    }

    isNewOperationAllowed = async () => {
        let payload = this.state.payload
        let parent = payload.definition.operations[payload.definition.operations.length - 1]
        let isParentAggregate = parent.type === "aggregate"
        let isAllowed = false
        if (isParentAggregate) {
            try {
                await AGGREGATE_OPERATION_SCHEMA.validate(parent);
                isAllowed = true
            }
            catch (error) {
                isAllowed = false
                this.props.showNotification("error", error.message)
            }
        } else {
            isAllowed = Boolean(parent.operations.length)
            if (!isAllowed) {
                this.props.showNotification("error", `Please enter atleast one value in the last operation.`)
            }
        }
        return isAllowed
    }

    getIndexesForMongoSink = () => {
        let attributes = this.state.defaultAttributes;
        let totalOperationsLength = this.state.payload.definition.operations.length;
        let lastOperation = this.state.payload.definition.operations[totalOperationsLength - 1];
        let lastSelectOperation = lastOperation.type === "select" ? lastOperation : this.state.payload.definition.operations[totalOperationsLength - 2];
        let operations = [];
        let indexes = [];
        lastSelectOperation.operations.map((operation) => {
            if (operation.includes(" as ")) {
                let completeKey = operation.split(" as ")[0].split(".")
                let requiredKey = completeKey[completeKey.length - 1]
                operations.push(requiredKey)
            }
        })
        if (attributes) {
            let flattenedObj = flatten(attributes);
            Object.keys(flattenedObj).map((key) => {
                if (key.endsWith(".name") && operations.includes(flattenedObj[key]) && (flattenedObj[key.replace(".name", ".type")] || flattenedObj[key.replace(".name", ".type.type")]) !== "struct") {
                    !indexes.includes(flattenedObj[key]) && indexes.push(flattenedObj[key])
                }
            })

        }
        if (operations.includes("currentTimeMillis(0)")) {
            indexes.push("timestamp")
        }
        return indexes
    }

    closeMiscTopicModal = () => {
        this.setState({ miscTopicModal: false, miscTopicName: "", miscTopicPrefix: "" })
    }

    saveMiscTopic = () => {
        this.setState({
            isSavingMiscTopic: true
        }, () => {
            let payload = {
                connectorId: this.state.miscTopicConnector,
                topic: this.state.miscTopicName,
            }
            this.props.saveMiscTopic(payload)
        })
    }

    miscTopicNameChangeHandler = ({ currentTarget }) => {

        let value = currentTarget.value;
        const regexPattern = allRegexPatterns.alphanumeric_underscore_forwardSlash
        const patternTestPassed = (regexPattern.test(value) && value.length <= 20) || value === ""
        if (patternTestPassed) {
            this.setState({ miscTopicName: value })
        } else {
            this.props.showNotification("error", "Only Alphanumeric Characters, Underscore, Forward Slash and a Maximum of 20 characters are allowed in Topic Name.")
        }
    }

    isAddNewIndexButtonDisabled = () => {
        let noIndexExists = !mongoIndexes
        if (noIndexExists) {
            return true
        }
        let sinkDetails = this.state.payload.definition.connector.sinkConnector
        let noValidIndex = !(mongoIndexes.filter((index) => (!sinkDetails.index.some((el) => el.some((child) => child.name === index))))).length
        if (noValidIndex) {
            return true
        }
        let isAnyIndexEmpty = sinkDetails.index.some(parentIndex => parentIndex.some(childIndex => !childIndex.name))
        if (isAnyIndexEmpty) {
            return isAnyIndexEmpty
        }
    }

    getColumnForTableView = (data) => {
        if (!data) {
            return [];
        }
        let columnData = Object.keys(data).map(column => ({ headerName: column, field: column }))
        return columnData;
    }

    getTableData = (data) => {
        data = cloneDeep(data)
        data.map((column) => {
            Object.entries(column).map(([key, value]) => {
                if (typeof value == "object") {
                    column[key] = JSON.stringify(value)
                }
            })
        })
        return data;
    }

    getAllOperationsForAttributes = (attributes) => {

        let flattenRecursively = (obj, parent = this.state.sourceConnectorType === "MQTT" ? "root" : "") => {
            if (typeof obj.type == "object") {
                obj.type.fields.map(field => {
                    let parentName = parent ? `${parent}.${obj.name}` : obj.name
                    flattenRecursively(field, parentName);
                })
            } else {
                let operation = parent ? `${parent}.${obj.name} as ${obj.name}` : `${obj.name} as ${obj.name}`;
                allOperations.push(operation);
            }
        }

        let allOperations = [];
        if (attributes.fields) {
            attributes.fields.map(field => {
                flattenRecursively(field);
            })
        }
        else {
            allOperations = Object.keys(attributes).map(key => {
                let operation = this.state.sourceConnectorType === "MQTT" ? `root.${key} as ${key}` : `${key} as ${key}`;
                return operation;
            })
        }

        return allOperations

    }

    autofillOperationsInDefinition = () => {
        let operations = this.getAllOperationsForAttributes(this.state.attributesArray[0]);
        isDefinitionAutofilled = Date.now();
        this.operationsChangeHandler(operations, 0)
    }

    autofillSinkPointDetails = (event) => {
        event.stopPropagation();
        let payload = produce(this.state.payload, payload => {
            let sourceConnectorName = this.state.connectorsListByCategory["MQTT"].data.find(connector => connector.id === payload.sourceConnector.id).name
            payload.definition.connector.sinkConnector.collection = `${sourceConnectorName}Collection`
            if (payload.definition.operations[payload.definition.operations.length - 1].operations.some(operation => operation.includes("as timestamp"))) {
                payload.definition.connector.sinkConnector.index = [
                    [
                        {
                            "name": "timestamp",
                            "value": -1
                        }
                    ]
                ]
            }
        })
        isSinkDetailAutofilled = true
        this.setState({
            payload,
        });
    }

    etlNameInputRef = React.createRef();

    sourceConnectorTypeChangeHandler = ({ currentTarget }) => {
        let sourceConnectorType = currentTarget.id;
        let payload = produce(this.state.payload, payload => {
            payload.sourceConnector = cloneDeep(SOURCE_CONNECTOR_PAYLOADS[sourceConnectorType])
            payload.attributeETL = sourceConnectorType == "MQTT"
        });
        if (this.state.connectorsListByCategory[sourceConnectorType].loader) {
            this.props.getConnectorsByCategory(sourceConnectorType, "isFromMount")
        }

        this.setState({
            sourceConnectorType,
            payload,
            isFetchingAttributes: false,
        });
    }

    returnSourceConnectorHTMLByType = (sourceConnectorList, payload) => {
        switch (this.state.sourceConnectorType) {
            case "MQTT": {
                return (
                    <table className="table table-bordered m-0">
                        <thead>
                            <tr>
                                <th width="10%"></th>
                                <th width="40%">{"Device Type"}<i className="fas fa-asterisk text-red f-8"></i></th>
                                <th width="50%">{"Topics"}<i className="fas fa-asterisk text-red f-8"></i></th>
                            </tr>
                        </thead>
                        <tbody>
                            {sourceConnectorList.map(connector => {
                                return (<tr key={connector.id}>
                                    <td>
                                        <label className={"check-box " + (this.props.match.params.id ? "check-box-disabled" : "")}>
                                            <input type="checkbox"
                                                checked={payload.sourceConnector.id === connector.id}
                                                name="sourceConnectorId"
                                                disabled={this.props.match.params.id}
                                                onChange={({ currentTarget }) => this.sourceConnectorChangeHandler(currentTarget.checked, connector)} />
                                            <span className="check-mark"></span>
                                        </label>
                                    </td>
                                    <td>
                                        <h6 className="text-gray fw-600 f-12 mb-1">{connector.name}</h6>
                                        <p className="f-11 text-content m-0">{`${connector.description || "-"}`}</p>
                                    </td>
                                    <td>
                                        <div className="form-group mb-0">
                                            {<select className="form-control"
                                                disabled={!(payload.sourceConnector.id === connector.id) || (this.props.match.params.id)}
                                                value={payload.sourceConnector.properties.topicId}
                                                onChange={({ currentTarget }) => this.sourceTopicChangeHandler(connector, currentTarget.value)}>
                                                <option value="">Select Topic</option>
                                                {connector.properties.topics &&
                                                    <React.Fragment>{connector.properties.topics.map((topic, i) => !topic.name.endsWith("+/control") &&
                                                        <option key={i} value={topic.topicId}>{`....${topic.name.substr(25, topic.name.length)}`}</option>)}
                                                    </React.Fragment>}
                                            </select>}
                                        </div>
                                    </td>
                                </tr>)
                            })}

                        </tbody>
                    </table>)
            }
            case "MONGODB": {
                return (
                    <table className="table table-bordered m-0">
                        <thead>
                            <tr>
                                <th width="10%"></th>
                                <th width="20%">{"Connector Name"}<i className="fas fa-asterisk text-red f-8"></i></th>
                                <th width="25%">{"Databases"}<i className="fas fa-asterisk text-red f-8"></i></th>
                                <th width="20%">Collection<i className="fas fa-asterisk text-red f-8"></i></th>
                                <th width="25%">Query<i className="fas fa-asterisk text-red f-8"></i></th>
                            </tr>
                        </thead>
                        <tbody>
                            {sourceConnectorList.map(connector => {
                                return (<tr key={connector.id}>
                                    <td>
                                        <label className={"check-box " + (this.props.match.params.id ? "check-box-disabled" : "")}>
                                            <input type="checkbox"
                                                checked={payload.sourceConnector.id === connector.id}
                                                name="sourceConnectorId"
                                                onChange={({ currentTarget }) => this.sourceConnectorChangeHandler(currentTarget.checked, connector)} />
                                            <span className="check-mark"></span>
                                        </label>
                                    </td>
                                    <td>
                                        <h6 className="text-gray fw-600 f-12 mb-1">{connector.name}</h6>
                                        <p className="f-11 text-content m-0">{`${connector.description || "-"}`}</p>
                                    </td>
                                    <td>
                                        <div className="form-group mb-0">
                                            <select className="form-control"
                                                value={payload.sourceConnector.databaseId}
                                                disabled={!(payload.sourceConnector.id === connector.id)}
                                                id="databaseId"
                                                onChange={({ currentTarget }) => this.sourceDataBaseChangeHandler(connector, currentTarget)}>
                                                <option value="">Select Database</option>
                                                {connector.properties.list &&
                                                    <React.Fragment>{connector.properties.list.map((databaseObj, i) =>
                                                        <option key={i} value={databaseObj.databaseId}>{databaseObj.database}</option>)}
                                                    </React.Fragment>}
                                            </select>
                                        </div>
                                    </td>
                                    <td>
                                        <div className="d-flex">
                                            <input type="text" id="collection" disabled={payload.sourceConnector.id !== connector.id} className="form-control" value={payload.sourceConnector.id === connector.id ? payload.sourceConnector.collection : ""} onChange={({ currentTarget }) => this.sourceDataBaseChangeHandler(connector, currentTarget)} />
                                        </div>
                                    </td>
                                    <td>
                                        <div className="d-flex">
                                            <input type="text" id="query" disabled={payload.sourceConnector.id !== connector.id} className="form-control" value={payload.sourceConnector.id === connector.id ? payload.sourceConnector.query : ""} onChange={({ currentTarget }) => this.sourceDataBaseChangeHandler(connector, currentTarget)} />
                                        </div>
                                    </td>
                                </tr>)
                            })}

                        </tbody>
                    </table>)
            }
            case "S3": {
                return (<table className="table table-bordered m-0">
                    <thead>
                        <tr>
                            <th width="10%"></th>
                            <th width="20%">{"Connector Name"}<i className="fas fa-asterisk text-red f-8"></i></th>
                            <th width="40%">{"Buckets"}<i className="fas fa-asterisk text-red f-8"></i></th>
                            <th width="30%">{"Format"}<i className="fas fa-asterisk text-red f-8"></i></th>
                        </tr>
                    </thead>
                    <tbody>
                        {sourceConnectorList.map(connector => {
                            return (<tr key={connector.id}>
                                <td>
                                    <label className={"check-box " + (this.props.match.params.id ? "check-box-disabled" : "")}>
                                        <input type="checkbox"
                                            checked={payload.sourceConnector.id === connector.id}
                                            name="sourceConnectorId"
                                            disabled={this.props.match.params.id}
                                            onChange={({ currentTarget }) => this.sourceConnectorChangeHandler(currentTarget.checked, connector)} />
                                        <span className="check-mark"></span>
                                    </label>
                                </td>
                                <td>
                                    <h6 className="text-gray fw-600 f-12 mb-1">{connector.name}</h6>
                                    <p className="f-11 text-content m-0">{`${connector.description || "-"}`}</p>
                                </td>
                                <td>
                                    <div className="form-group mb-0">
                                        <select className="form-control"
                                            disabled={!(payload.sourceConnector.id === connector.id) || (this.props.match.params.id)}
                                            value={payload.sourceConnector.bucketId}
                                            id="bucketId"
                                            onChange={({ currentTarget }) => this.sourceBucketChangeHandler(connector, currentTarget)}>
                                            <option value="">Select Bucket</option>
                                            {connector.properties.list &&
                                                <React.Fragment>{connector.properties.list.map((bucket, i) =>
                                                    <option key={i} value={bucket.bucketId}>{bucket.bucketName}</option>)}
                                                </React.Fragment>}
                                        </select>
                                    </div>
                                </td>
                                <td>
                                    <div className="d-flex">
                                        <select className="form-control"
                                            id="format"
                                            disabled={!(payload.sourceConnector.id === connector.id) || (this.props.match.params.id)}
                                            value={payload.sourceConnector.id === connector.id ? payload.sourceConnector.format : ""}
                                            onChange={({ currentTarget }) => this.sourceBucketChangeHandler(connector, currentTarget)}>
                                            <option value="">Select Format</option>
                                            <option value="EXCEL">EXCEL</option>
                                            <option value="JSON">JSON</option>
                                            <option value="CSV">CSV</option>
                                            <option value="PARQUET">Parquet</option>
                                        </select>
                                    </div>
                                </td>
                            </tr>)
                        })}

                    </tbody>
                </table>)
            }
        }
    }

    getMissingDetailsMessageForSchema = () => {
        return <div className="inner-message-wrapper h-100">
            <div className="inner-message-content">
                <i className="fad fa-exclamation-triangle text-red"></i>
                <h6 className="text-red">Please enter the Basic Information and Source Connector Details first.</h6>
            </div>
        </div>
    }

    isSourceDetailsFilled = (sourceConnector) => {
        let isMQTTSource = sourceConnector.childType === "mqttSource";
        let isMongoSource = sourceConnector.childType === "mongoSource";
        if (isMQTTSource) {
            return sourceConnector.properties && sourceConnector.properties.topicId;
        }
        if (isMongoSource) {
            return sourceConnector.id && sourceConnector.databaseId && sourceConnector.query && sourceConnector.collection;
        }
        return sourceConnector.bucketId && sourceConnector.format
    }

    getSchemaHTML = () => {
        let isFetchingAttributes = this.state.isFetchingAttributes || this.state.isUpdatingAttributes;
        if (isFetchingAttributes) {
            return getMiniLoader();
        }

        let sourceConnector = this.state.payload.sourceConnector;
        let isMQTTSource = sourceConnector.childType === "mqttSource";

        if (isBasicInfoComplete) {
            if (this.state.attributes) {
                return <React.Fragment>
                    {this.state.attributeView === "rawView" && this.state.attributes ?
                        <div className="etl-attribute-tree">
                            <ReactJson name={null} src={this.state.attributes.fields} />
                        </div>
                        :
                        this.state.attributes && this.state.attributes.fields && this.state.attributes.fields.length > 0 &&
                        <div className="etl-attribute-tree">
                            <ul>
                                {this.state.attributes.fields.map((value, index) => {
                                    return (
                                        <li key={index}>
                                            <div className="etl-attribute-tree-box">
                                                <p data-toggle="collapse" data-target={value.type.type ? `#attribute${index}` : ""} key={index}>
                                                    {value.name}
                                                    <span>{value.type.type ? Array.isArray(value.type) ? "(array)" : "(object)" : `(${value.type})`}</span>
                                                </p>
                                                <div className="button-group">
                                                    <button className="btn btn-link text-green" data-tooltip data-tooltip-text="Copy" data-tooltip-place="bottom" value={`${value.name} as ${value.name}`} onClick={this.pasteAttributeInDefinition}>
                                                        <i className="far fa-copy"></i>
                                                    </button>
                                                </div>
                                            </div>
                                            {value.type.type && value.type.type === "struct" ?
                                                this.attributeTree(value.type.fields, index, value.name) :
                                                value.type.type === "array" ?
                                                    this.checkingFields(value.type.elementType, index, value.name) : null
                                            }
                                        </li>
                                    )
                                })}
                            </ul>
                        </div>
                    }
                </React.Fragment>
            } else {
                return <div className="inner-message-wrapper h-100">
                    <div className="inner-message-content">
                        <i className="fad fa-file-exclamation"></i>
                        <h6>{isSchemaFetchingPending ? "Scehma Not Fetched." : "No schema found."}</h6>
                        {isMQTTSource && <button type="button" className="btn btn-link" onClick={() => this.showCreateSchemaModal(sourceConnector.properties.topicId, sourceConnector.properties.topicName)}>
                            <i className="far fa-plus mr-2 f-13 text-primary"></i>Add Schema
                        </button>}
                        {isSchemaFetchingPending && <p className="text-primary">Click inside the Operations box in Definition to fetch schema.</p>}
                    </div>
                </div>
            }
        }
        return this.getMissingDetailsMessageForSchema()
    }

    isAutoFillDefinitionDisabled = () => {
        if (isDefinitionAutofilled) {
            return true;
        }

        if (this.state.sourceConnectorType === "MQTT" && !this.state.defaultAttributes) {
            return true;
        }

        return false
    }

    directoryNameChangeHandler = ({ currentTarget: { value } }) => {
        if (!value.startsWith("/")) {
            value = `/${value}`
        }
        let doubleSlashesArr = value.match(/(\/{2,3})/g);
        if (doubleSlashesArr) {
            doubleSlashesArr.map(multipleSlashes => {
                value = value.replace(multipleSlashes, "/");
            })
            this.props.showNotification("error", "Consecutive forward slashes are not allowed in Directory Name.")
        }

        let payload = produce(this.state.payload, payload => {
            payload.definition.connector.sinkConnector.dataDir = value;
        });
        this.setState({
            payload
        })
    }

    getETLConfigDetailsHTML = () => {
        return <React.Fragment>
            <div className="form-group">
                <label className="form-group-label">Max OffSet <small>(Records per batch)</small> : <i className="fas fa-asterisk form-group-required"></i></label>
                <input type="number" className="form-control" value={this.state.payload.etlConfig.maxOffset} name="etlConfig" id="maxOffset" onChange={this.etlConfigChangeHandler} />
            </div>

            <div className="form-group">
                <label className="form-group-label">Trigger Interval <small>(in milliseconds)</small> : <i className="fas fa-asterisk form-group-required"></i></label>
                <input type="number" className="form-control" value={this.state.payload.etlConfig.triggerInterval} name="etlConfig" id="triggerInterval" onChange={this.etlConfigChangeHandler} />
            </div>

            <div className="form-group">
                <label className="form-group-label">Thresholds <small>(in seconds)</small> : <i className="fas fa-asterisk form-group-required"></i></label>
                <input type="number" className="form-control" value={this.state.payload.etlConfig.threshold} name="etlConfig" id="threshold" onChange={this.etlConfigChangeHandler} />
            </div>
        </React.Fragment>
    }

    render() {
        let payload = this.state.payload,
            sourceConnectorLoader = this.state.connectorsListByCategory[this.state.sourceConnectorType].loader,
            sourceConnectorList = this.state.connectorsListByCategory[this.state.sourceConnectorType].data.filter(el => this.state.sourceConnectorType == "MQTT" || el.source),
            sinkConnectorsList = this.state.sinkConnectorCategory ? this.state.connectorsListByCategory[this.state.sinkConnectorCategory].data.filter(el => el.sink) : [];
        let isEditModeOn = this.props.match.params.id;
        let definitionDetailsVisibility = isBasicInfoComplete ? this.state.definitionDetailsVisibility : false;
        let sinkWindowCheck = isBasicInfoComplete && payload.definition.operations[0].operations.length;
        let sinkWindowVisibility = sinkWindowCheck && this.state.sinkWindowVisibility;
        mongoIndexes = this.state.sinkConnectorCategory === "MONGODB" && this.state.payload.definition.operations[0].operations.length ? this.getIndexesForMongoSink() : "";
        isAddNewIndexButtonDisabled = this.state.sinkConnectorCategory !== "MONGODB" || !isBasicInfoComplete || this.isAddNewIndexButtonDisabled();

        return (
            <React.Fragment>
                <Helmet>
                    <title>Add Or Edit ETL Config</title>
                    <meta name="description" content="Description of AddOrEditEtlConfig" />
                </Helmet>

                <header className="content-header d-flex">
                    <div className="flex-60">
                        <div className="d-flex">
                            <h6 className="previous" onClick={() => this.props.history.push('/etlPipeline')}>ETL Pipelines</h6>
                            <h6 className="active">Add New</h6>
                        </div>
                    </div>
                    <div className="flex-40 text-right">
                        <div className="content-header-group">
                            <button className="btn btn-light" data-tooltip data-tooltip-text="Back" data-tooltip-place="bottom" onClick={() => this.props.history.push('/etlPipeline')}><i className="fas fa-angle-left"></i></button>
                        </div>
                    </div>
                </header>

                {this.state.isFetching ?
                    <Loader /> :
                    <div className="content-body">
                        <div className="form-content-wrapper form-content-wrapper-both">
                            <div className="form-content-box collapse-wrapper">
                                <div className="form-content-header collapse-header">
                                    <h6>Source
                                        <i className={`fad fa-chevron-double-up collapse-header-toggle ${this.state.sourceWindowVisibility ? "" : "collapsed"}`} id="sourceWindow" onClick={this.toggleWindowVisibility}></i>
                                    </h6>
                                </div>

                                <div className={`collapse ${this.state.sourceWindowVisibility ? "show" : ""}`} id="sourceSelection">
                                    <div className="form-content-body d-flex">
                                        <div className="flex-25 pd-r-10">
                                            <ul className="list-style-none etl-source-list">
                                                {SOURCE_CONNECTORS.filter(sourceConnector => sourceConnector.type.toLowerCase() === this.state.payload.type).map((sourceConnector, index) => {
                                                    return (
                                                        <li key={index}>
                                                            <div id={sourceConnector.name} className={`etl-source-wrapper ${this.state.sourceConnectorType === sourceConnector.name ? "active" : ""}`} onClick={(e) => this.props.match.params.id ? false : this.sourceConnectorTypeChangeHandler(e)}>
                                                                <span><i className="fad fa-check-circle"></i></span>
                                                                <div className="etl-source-image">
                                                                    <img src={require(`../../assets/images/other/${sourceConnector.imageName}.png`)} />
                                                                </div>
                                                                <h6>{sourceConnector.name == "MONGODB" ? "Mongo DB" : sourceConnector.name}</h6>
                                                                <p>{sourceConnector.type}</p>
                                                            </div>
                                                        </li>)
                                                })}
                                            </ul>
                                        </div>

                                        <div className="flex-75">
                                            {sourceConnectorLoader ?
                                                <div className="inner-loader-wrapper etl-source-table-empty">
                                                    <div className="inner-loader-content">
                                                        <i className="fad fa-sync-alt fa-spin"></i>
                                                    </div>
                                                </div> :
                                                <React.Fragment>
                                                    {sourceConnectorList.length ?
                                                        <div className="content-table etl-source-table">
                                                            {this.returnSourceConnectorHTMLByType(sourceConnectorList, payload)}
                                                        </div> :
                                                        <div className="etl-source-table-empty">
                                                            <div>
                                                                <img src="https://dl1jn0gbol8q5.cloudfront.net/m83/misc/addETL.png" />
                                                                <p className="text-content mb-0">There is no data to display.</p>
                                                            </div>
                                                        </div>
                                                    }
                                                </React.Fragment>
                                            }
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="form-content-box collapse-wrapper etl-definition-wrapper">
                                <div className="form-content-header collapse-header">
                                    <h6>
                                        Definition {!isBasicInfoComplete && <span className="form-content-error">(Please fill the required fields to Configure Definition.)</span>}
                                        <i className={`fad fa-chevron-double-up collapse-header-toggle ${definitionDetailsVisibility ? "" : "collapsed"}`} id="definitionDetails" onClick={(e) => isBasicInfoComplete && this.toggleWindowVisibility(e)}></i>
                                    </h6>
                                    <div className="button-group">
                                        {/*<div className="toggle-switch-wrapper">
                                            <span>Basic</span>
                                            <label className="toggle-switch" onClick={(event) => event.stopPropagation()}>
                                                <input type="checkbox" disabled />
                                                <span className="toggle-switch-slider"></span>
                                            </label>
                                            <span>Advance</span>
                                        </div>*/}
                                        {isBasicInfoComplete && this.state.attributes &&
                                            <button className="btn-transparent btn-transparent-green" data-tooltip data-tooltip-text="Auto Fill Definition Data" data-tooltip-place="bottom" disabled={this.isAutoFillDefinitionDisabled()} onClick={this.autofillOperationsInDefinition}>
                                                <i className="far fa-file-signature"></i>
                                            </button>
                                        }
                                        <button className="btn-transparent btn-transparent-yellow" data-tooltip data-tooltip-text="Debug" data-tooltip-place="bottom" disabled={!(this.state.payload.sourceConnector.id && payload.definition.operations[0].operations.length)} onClick={this.handleDebugging}><i className="far fa-bug"></i></button>
                                        {this.props.match.params.id && <button className="btn-transparent btn-transparent-blue" data-tooltip data-tooltip-text="Reset To Original State" data-tooltip-place="bottom" onClick={() => this.setState({ payload: cloneDeep(this.state.defaultPayload) })}><i className="far fa-redo-alt"></i></button>}
                                    </div>
                                </div>

                                <div className={`collapse ${definitionDetailsVisibility ? "show" : ""}`}>
                                    <div className={`form-content-body`}>
                                        {payload.definition.operations.map((operation, index) => {
                                            return (<div key={index} className="etl-query-wrapper">
                                                <button className="btn btn-light box-top-right-button" disabled={index == 0 || this.state.isFetchingAttributes} data-tooltip data-tooltip-text="Close" data-tooltip-place="bottom" id={index} onClick={this.removeOperationHandler}><i className="far fa-times"></i></button>
                                                <div className="d-flex etl-query-type">
                                                    {OPERATION_TYPES.map((type, i) => {
                                                        let isOnlySelectOperationAllowed = (index == 0 && type !== "select") || (index && payload.definition.operations[index - 1].type !== "select" && type !== "select")
                                                        return <label key={i} className={"radio-button mr-r-10 " + (isOnlySelectOperationAllowed || this.state.isFetchingAttributes ? "radio-button-disabled" : "")}>
                                                            <span className="radio-button-text">{type.charAt(0) + type.slice(1)}</span>
                                                            <input type="checkbox" disabled={isOnlySelectOperationAllowed || this.state.isFetchingAttributes} checked={operation.type === type} value={type} id="type" onChange={({ currentTarget }) => this.operationTypeChangeHandler(currentTarget, index)} />
                                                            <span className="radio-button-mark"></span>
                                                        </label>
                                                    })}
                                                </div>

                                                <div className="etl-query-box">
                                                    {operation.type === "aggregate" ?
                                                        <div className="form-group">
                                                            <label className="form-group-label">Group By : <i className="fas fa-asterisk form-group-required"></i></label>
                                                            <TagsInput addOnBlur value={operation.groupBy} id="tag_select" onChange={(groupBy) => this.aggregateGroupByChangeHandler(groupBy, index)} placeholder="Expression" />
                                                        </div> :
                                                        <div className="form-group" onFocus={() => !this.state.isFetchingAttributes && this.fetchSchema(index)} >
                                                            <label className="form-group-label">Operations :</label>
                                                            <TagsInput addOnBlur value={operation.operations} id="tag_select" disabled={this.state.isFetchingAttributes} onChange={(operations) => !this.state.isFetchingAttributes && this.operationsChangeHandler(operations, index)} placeholder="Expression" />
                                                        </div>
                                                    }

                                                    {(operation.type === "filter" || operation.type === "select") &&
                                                        <button type="button" id="optional_select" className="btn btn-link" disabled={this.state.isFetchingAttributes || operation.operations.includes("currentTimeMillis(0) as timestamp")} onClick={() => this.operationsChangeHandler([...operation.operations, "currentTimeMillis(0) as timestamp"], index)}>
                                                            <i className="far fa-plus"></i>Add timestamp
                                                        </button>
                                                    }

                                                    {operation.type === "aggregate" &&
                                                        <React.Fragment>
                                                            <div className="d-flex">
                                                                <div className="flex-25 pd-r-10">
                                                                    <label className="form-group-label">Apply For Window Aggregation :</label>
                                                                    <div className="form-group">
                                                                        <label className="check-box">
                                                                            <input type="checkbox" checked={operation.window.enable} onChange={({ currentTarget }) => this.aggregateWindowDetailsChangeHandler("enable", index, currentTarget.checked)} />
                                                                            <span className="check-mark"></span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                {operation.window.enable && <React.Fragment>
                                                                    <div className="flex-25 pd-l-5 pd-r-5">
                                                                        <div className="form-group">
                                                                            <label className="form-group-label">Window Column :</label>
                                                                            <div className="input-group">
                                                                                <input type="text" className="form-control" value={operation.window.windowColumn} onChange={({ currentTarget }) => this.aggregateWindowDetailsChangeHandler("windowColumn", index, currentTarget.value)} />
                                                                                <div className="input-group-append">
                                                                                    <span className="input-group-text" data-tooltip data-tooltip-text="Column must be timestamp type" data-tooltip-place="bottom"><i className="far fa-info"></i></span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div className="flex-25 pd-l-5 pd-r-5">
                                                                        <div className="form-group">
                                                                            <label className="form-group-label">Window Interval :</label>
                                                                            <div className="input-group">
                                                                                <input type="text" className="form-control" value={operation.window.windowInterval} onChange={({ currentTarget }) => this.aggregateWindowDetailsChangeHandler("windowInterval", index, currentTarget.value)} />
                                                                                <div className="input-group-append">
                                                                                    <span className="input-group-text">mins</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div className="flex-25 pd-l-10">
                                                                        <div className="form-group">
                                                                            <label className="form-group-label">Sliding Window Interval :</label>
                                                                            <div className="input-group">
                                                                                <input type="text" className="form-control" value={operation.window.slidingWindowInterval} onChange={({ currentTarget }) => this.aggregateWindowDetailsChangeHandler("slidingWindowInterval", index, currentTarget.value)} />
                                                                                <div className="input-group-append">
                                                                                    <span className="input-group-text">mins</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </React.Fragment>}

                                                            </div>
                                                            <div className="connector-form-list form-group">
                                                                <label className="form-group-label d-inline-block">
                                                                    <button className="btn btn-primary" onClick={() => this.addNewConditionInAggregate(index)}><i className="far fa-plus"></i></button>Add Condition (s) :
                                                            </label>
                                                                <div className="connector-form-body">
                                                                    <div className="d-flex pd-r-30">
                                                                        <label className="form-group-label flex-33">Operation <i className="fas fa-asterisk form-group-required"></i></label>
                                                                        <label className="form-group-label flex-33">Field <i className="fas fa-asterisk form-group-required"></i></label>
                                                                        <label className="form-group-label flex-33">Alias <i className="fas fa-asterisk form-group-required"></i></label>
                                                                    </div>
                                                                    {operation.operations.map((condition, i) => <ul key={i} className="list-style-none d-flex">
                                                                        <li className="flex-33">
                                                                            <select className="form-control" value={condition.type} id="type" onChange={({ currentTarget }) => this.aggregateConditionChangeHandler("type", currentTarget.value, index, i)} required>
                                                                                <option value="">Select</option>
                                                                                <option value="sum">sum</option>
                                                                                <option value="min">min</option>
                                                                                <option value="max">max</option>
                                                                                <option value="count">count</option>
                                                                                <option value="avg">average</option>
                                                                                <option value="first">first</option>
                                                                                <option value="collect_list">collect list</option>
                                                                                <option value="collect_set">collect set</option>
                                                                            </select>
                                                                        </li>
                                                                        <li className="flex-33"><input type="text" id="unit" name="unit" className="form-control" id="field" onFocus={() => this.fetchSchema(index)} value={condition.field} onChange={({ currentTarget }) => this.aggregateConditionChangeHandler("field", currentTarget.value, index, i)} /></li>
                                                                        <li className="flex-33"><input type="text" id="repetition" name="repetition" className="form-control" id="alias" onFocus={() => this.fetchSchema(index)} value={condition.alias} onChange={({ currentTarget }) => this.aggregateConditionChangeHandler("alias", currentTarget.value, index, i)} /></li>
                                                                        <div className="button-group">
                                                                            <button className="btn-transparent btn-transparent-red" data-tooltip data-tooltip-text="Delete" data-tooltip-place="bottom" onClick={() => this.deleteConditionInAggregate(index, i)} disabled={operation.operations.length === 1}>
                                                                                <i className="far fa-trash-alt"></i>
                                                                            </button>
                                                                        </div>
                                                                    </ul>)}
                                                                </div>
                                                            </div>
                                                        </React.Fragment>
                                                    }
                                                </div>
                                            </div>)
                                        }
                                        )}
                                        <div className="text-center">
                                            <button className="btn btn-link" onClick={this.addNewOperation}><i className="far fa-plus"></i>Add New</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="form-content-box collapse-wrapper etl-definition-wrapper">
                                <div className="form-content-header collapse-header">
                                    <h6>Sink Point
                                        {sinkWindowCheck ? "" : <span className="form-content-error">(Please fill the required fields to Configure Sink Point.)</span>}
                                        <i className={`fad fa-chevron-double-up collapse-header-toggle ${sinkWindowVisibility ? "" : "collapsed"}`} id="sinkWindow" onClick={(e) => sinkWindowCheck && this.toggleWindowVisibility(e)}></i>
                                    </h6>
                                    <div className="button-group">
                                        {(this.state.sourceConnectorType === "MQTT" && this.state.sinkConnectorCategory === "MONGODB" && isBasicInfoComplete) &&
                                            <button className="btn-transparent btn-transparent-green" data-tooltip data-tooltip-text="Auto Fill Sink Details" data-tooltip-place="bottom" disabled={isSinkDetailAutofilled || !sinkWindowCheck} onClick={this.autofillSinkPointDetails}>
                                                <i className="far fa-file-signature"></i>
                                            </button>
                                        }
                                    </div>
                                </div>

                                <div className={`collapse ${sinkWindowVisibility ? "show" : ""}`} id="sinkPoint">
                                    <div className="form-content-body">
                                        <div className="etl-sink-wrapper">
                                            <div className="d-flex">
                                                <div className="flex-33 pd-r-10">
                                                    <div className="form-group">
                                                        <label className="form-group-label">Type :</label>
                                                        <select className="form-control" disabled={this.state.isFetchingSinkPointTypes || this.state.isFetchingConnectorsByCategory || isEditModeOn} value={this.state.sinkType} onChange={this.sinkTypeChangehandler}>
                                                            {this.state.isFetchingSinkPointTypes ? <option value="" disabled>Loading..</option> :
                                                                this.state.sinkPointTypes.map(type => <option key={type.type} disabled={!type.available} value={type.type}>{type.displayName}</option>)}
                                                        </select>
                                                    </div>
                                                </div>
                                                <div className="flex-33 pd-r-5 pd-l-5">
                                                    <div className="form-group">
                                                        <label className="form-group-label">Category :</label>
                                                        <select className="form-control" disabled={!this.state.sinkType || this.state.isFetchingConnectorsByCategory || isEditModeOn} value={this.state.sinkConnectorCategory} onChange={this.sinkConnectorCategoryChangehandler}>
                                                            <option disabled>Select {this.state.sinkType ? "" : "Type First"}</option>
                                                            {Boolean(this.state.sinkType) && this.state.sinkPointTypes.find(type => type.type === this.state.sinkType).connectors.map(category => <option key={category.connector} value={category.connector} disabled={!category.available}>{category.displayName}</option>)}
                                                        </select>
                                                    </div>
                                                </div>
                                                <div className="flex-33 pd-l-10">
                                                    <div className="form-group">
                                                        <label className="form-group-label">Connector :</label>
                                                        <select className="form-control" disabled={!this.state.sinkConnectorCategory || this.state.isFetchingConnectorsByCategory || (this.state.sinkConnectorCategory === "TIMESCALEDB" && this.state.isFetchingTimescaleConnectorTables) || isEditModeOn} value={payload.definition.connector.id} onChange={this.sinkConnectorChangehandler}>
                                                            {!this.state.sinkConnectorCategory ? <option value="">Select Category First</option> : this.state.isFetchingConnectorsByCategory ? <option value="">Loading..</option> : sinkConnectorsList.length > 0 ?
                                                                sinkConnectorsList.map(type => <option key={type.id} value={type.id} name={type.name}>{type.name}</option>) : <option value="">No Connectors Found</option>}
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            {Boolean(payload.definition.connector.id) && this.getSinkHTMLByCategory()}

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="form-info-wrapper form-info-wrapper-left">
                            <div className="form-info-header">
                                <h5>Basic Information</h5>
                            </div>

                            {/*<div className="action-tabs">
                                <ul className="nav nav-tabs list-style-none d-flex">
                                    <li className="nav-item active flex-50">
                                        <a className="nav-link">Information</a>
                                    </li>
                                    <li className="nav-item flex-50">
                                        <a className="nav-link">Root Node</a>
                                    </li>
                                </ul>
                            </div>*/}

                            {/*<div className="form-info-body form-info-body-adjust">
                                <div className="etl-attribute-tree">
                                    <ul>
                                        <li>
                                            <div className="form-group input-group">
                                                <input type="text" className="form-control" />
                                                <div className="input-group-append">
                                                    <button className="btn btn-transparent-blue" data-tooltip data-tooltip-text="Add Child" data-tooltip-place="bottom"><i className="far fa-plus"></i></button>
                                                    <button className="btn btn-transparent-red" data-tooltip data-tooltip-text="Delete" data-tooltip-place="bottom"><i className="far fa-trash-alt"></i></button>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="form-group input-group">
                                                <input type="text" className="form-control" />
                                                <div className="input-group-append">
                                                    <button className="btn btn-transparent-blue" data-tooltip data-tooltip-text="Add Child" data-tooltip-place="bottom"><i className="far fa-plus"></i></button>
                                                    <button className="btn btn-transparent-red" data-tooltip data-tooltip-text="Delete" data-tooltip-place="bottom"><i className="far fa-trash-alt"></i></button>
                                                </div>
                                            </div>
                                            <ul>
                                                <li>
                                                    <div className="form-group input-group">
                                                        <input type="text" className="form-control" />
                                                        <div className="input-group-append">
                                                            <button className="btn btn-transparent-blue" data-tooltip data-tooltip-text="Add Child" data-tooltip-place="bottom"><i className="far fa-plus"></i></button>
                                                            <button className="btn btn-transparent-green" data-tooltip data-tooltip-text="Add Sink Point" data-tooltip-place="bottom"><i className="far fa-database"></i></button>
                                                            <button className="btn btn-transparent-red" data-tooltip data-tooltip-text="Delete" data-tooltip-place="bottom"><i className="far fa-trash-alt"></i></button>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>*/}

                            <div className="form-info-body form-info-body-adjust">
                                <div className="form-group">
                                    <label className="form-group-label">Type : <i className="fas fa-asterisk form-group-required"></i></label>
                                    <select id="type" className="form-control" disabled={this.props.match.params.id} value={this.state.payload.type} onChange={this.basicDetailsChangeHandler}>
                                        <option value="stream">Stream</option>
                                        <option value="batch">Batch</option>
                                    </select>
                                </div>

                                <div className="form-group">
                                    <label className="form-group-label">Name : <i className="fas fa-asterisk form-group-required"></i></label>
                                    <input type="text" id="name" className="form-control" disabled={this.props.match.params.id} value={this.state.payload.name} ref={this.etlNameInputRef} id="name" onChange={this.basicDetailsChangeHandler} />
                                    {this.state.payload.name && !ETL_NAME_REGEX.test(this.state.payload.name) &&
                                        <span className="form-group-error">Please enter a valid ETL name</span>
                                    }
                                    {this.state.payload.name && !ETL_NAME_REGEX.test(this.state.payload.name) &&
                                        <span className="form-control-tooltip-icon text-red" onMouseEnter={() => this.etlNameInputRef.current.blur()} data-tooltip="true"
                                            data-tooltip-text="ETL name can start only with an alphabet, end with an alphanumeric character and can include only alphanumeric characters and underscore in between."
                                            data-tooltip-place="bottom">
                                            <i className="fad fa-exclamation-triangle"></i>
                                        </span>
                                    }
                                </div>

                                <div className="form-group">
                                    <label className="form-group-label">Description :</label>
                                    <textarea rows="3" id="description" className="form-control" value={this.state.payload.description} id="description" onChange={this.basicDetailsChangeHandler} />
                                </div>

                                {this.state.sourceConnectorType === "MQTT" && this.getETLConfigDetailsHTML()}
                            </div>

                            <div className="form-info-footer">
                                <button className="btn btn-light" onClick={() => this.props.history.push('/etlPipeline')}>Cancel</button>
                                <button className="btn btn-primary" onClick={this.saveETL}>{this.props.match.params.id ? "Update" : "Save"}</button>
                            </div>
                        </div>

                        <div className="form-info-wrapper pb-0">
                            <div className="action-tabs">
                                <ul className="nav nav-tabs list-style-none d-flex">
                                    {/* <li className="nav-item flex-33">
                                        <a className="nav-link">Function</a>
                                    </li>*/}
                                    <li className={`nav-item flex-50 ${this.state.currentTab === 'Attributes' ? 'active' : null}`}>
                                        <a className="nav-link" id="Attributes" onClick={this.tabChangehandler}>Attributes</a>
                                    </li>
                                    <li className={`nav-item flex-50 ${this.state.currentTab === 'Spark' ? 'active' : null}`}>
                                        <a className="nav-link" id="Spark" onClick={this.tabChangehandler}>Spark Fn()</a>
                                    </li>
                                </ul>
                            </div>

                            <div className="form-info-body form-info-body-adjust">
                                {this.state.currentTab === "Spark" ?
                                    (this.state.isFetchingSparkFunctions ?
                                        <div className="inner-loader-wrapper h-100">
                                            <div className="inner-loader-content">
                                                <i className="fad fa-sync-alt fa-spin"></i>
                                            </div>
                                        </div> :
                                        this.state.sparkFunctions.length > 0 ?
                                            <ul className="list-style-none form-info-list">
                                                {this.state.sparkFunctions && this.state.sparkFunctions.map((func, index) => (
                                                    <li className="mb-3" key={index}>
                                                        <h6 className="pr-4">
                                                            <button className="btn btn-link text-green" data-tooltip data-tooltip-text="Copy" data-tooltip-place="bottom" onClick={() => this.copyToClipboard(func.functionName)}>
                                                                <i className="far fa-copy f-12"></i>
                                                            </button>
                                                            {func.functionName}
                                                        </h6>
                                                        <p>{func.description}</p>
                                                    </li>
                                                ))
                                                }
                                            </ul> :
                                            <div className="inner-message-wrapper h-100">
                                                <div className="inner-message-content">
                                                    <i className="fad fa-file-exclamation"></i>
                                                    <h6>No Spark Functions Found</h6>
                                                </div>
                                            </div>
                                    ) :
                                    <React.Fragment>
                                        {this.state.attributes &&
                                            <div className="button-group mb-2">
                                                <button className={`btn-transparent btn-transparent-green ${this.state.attributeView === "rawView" ? "active" : ""}`} data-tooltip data-tooltip-text="JSON View" data-tooltip-place="bottom" id="rawView" onClick={this.attributesViewChangehandler}><i className="far fa-list"></i></button>
                                                <button className={`btn-transparent btn-transparent-yellow ${this.state.attributeView !== "rawView" ? "active" : ""}`} data-tooltip data-tooltip-text="Tree View" data-tooltip-place="bottom" id="nestedView" onClick={this.attributesViewChangehandler}><i className="far fa-folder-tree" ></i></button>
                                                <button className='btn-transparent btn-transparent-red' disabled={this.state.payload.sourceConnector.properties && this.state.payload.sourceConnector.properties.topicName.endsWith("+/report")} data-tooltip data-tooltip-text="Clear Schema" data-tooltip-place="bottom" onClick={() => this.setState({ clearSchemaConfirmation: true })}><i className="far fa-broom"></i></button>
                                            </div>
                                        }
                                        {this.getSchemaHTML()}
                                    </React.Fragment>
                                }
                            </div>
                        </div>
                    </div>
                }

                {/* debug window */}
                {this.state.debugWindow &&
                    <div className="modal show d-block" role="dialog">
                        <div className="debug-wrapper etl-debug-wrapper animated slideInUp" role="dialog">
                            <div className="debug-header">
                                <h6>Debug Window</h6>
                                <div className="button-group">
                                    <ul className="modal-header-list list-style-none">
                                        <li className={`btn btn-light ${this.state.dataView === 'FILE' ? 'active' : ''}`} data-tooltip data-tooltip-text="JSON View" data-tooltip-place="bottom" onClick={() => this.setState({ dataView: 'FILE' })}>
                                            <i className="far fa-folder"></i>
                                        </li>
                                        <li className={`btn btn-light ${this.state.dataView === 'TREE' ? 'active' : ''}`} data-tooltip data-tooltip-text="Tree View" data-tooltip-place="bottom" onClick={() => this.setState({ dataView: 'TREE' })}>
                                            <i className="far fa-folder-tree"></i>
                                        </li>
                                        <li className={`btn btn-light ${this.state.dataView === 'TABLE' ? 'active' : ''}`} data-tooltip data-tooltip-text="Table View" data-tooltip-place="bottom" onClick={() => this.setState({ dataView: 'TABLE' })}>
                                            <i className="far fa-table"></i>
                                        </li>
                                        <li className="btn btn-light" data-tooltip data-tooltip-text="Clear" data-tooltip-place="bottom" onClick={this.clearSniffingModal} >
                                            <i className="far fa-broom"></i>
                                        </li>
                                        <li type="button" className="btn btn-light" data-tooltip data-tooltip-text="Close" data-tooltip-place="bottom" onClick={this.stopDebugging}>
                                            <i className="far fa-times"></i>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div className="debug-body">
                                {this.state.isDebugging ?
                                    <div className="inner-loader-wrapper h-100">
                                        <div className="inner-loader-content">
                                            <h6>Debugging...</h6>
                                            <img src="https://content.iot83.com/m83/misc/uploading.gif" />
                                            <p className="text-gray">Please be patient. This may take few seconds.</p>
                                        </div>
                                    </div> :
                                    this.state.debugData ?
                                        this.state.dataView === 'FILE' ?
                                            <ul className="list-style-none overflow-y-auto h-100">
                                                {this.state.debugData.map((messageObj, index) => {
                                                    return (
                                                        <li className="text-content f-12 mb-2" key={index} onMouseEnter={() => this.setState({ isHoldData: true })} onMouseLeave={() => this.setState({ isHoldData: false })}>
                                                            <button className="btn-transparent btn-transparent-green copy-button" data-tooltip data-tooltip-text="Copy" data-tooltip-place="bottom" onClick={() => this.copyToClipboard(JSON.stringify(messageObj))}><i className="far fa-copy"></i></button>
                                                            {JSON.stringify(messageObj, undefined, 2)}
                                                            {messageObj.time}
                                                        </li>
                                                    )
                                                })}
                                            </ul> :
                                            this.state.dataView === 'TREE' ?
                                                <div className="h-100 overflow-y-auto">
                                                    {this.state.debugData.map((messageObj, index) => <ReactJson key={index} name={null} src={messageObj} />)}
                                                </div> :
                                                <div className="debug-table-wrapper h-100">
                                                    <div className="ag-theme-alpine">
                                                        <AgGridReact
                                                            columnDefs={this.getColumnForTableView(this.state.debugData[0])}
                                                            rowData={this.getTableData(this.state.debugData)}
                                                            animateRows={true}
                                                        />
                                                    </div>
                                                </div>
                                        :
                                        <div className="inner-message-wrapper h-100">
                                            <div className="inner-message-content">
                                                <i className="fad fa-file-exclamation"></i>
                                                <h6>There is no data to display.</h6>
                                            </div>
                                        </div>
                                }
                            </div>
                        </div>
                    </div>
                }
                {/* end debug window */}

                {/* function detail modal */}
                <div className="modal fade animated slideInDow" role="dialog" id="functionDetailModal" >
                    <div className="modal-dialog modal-lg modal-dialog-centered">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h6 className="modal-title">Function Name - <span>Test Function</span>
                                    <button className="close" data-dismiss="modal"><i className="far fa-times"></i></button>
                                </h6>
                            </div>
                            <div className="modal-body">
                                <div className="ace-editor"></div>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-success" data-dismiss="modal">OK</button>
                            </div>
                        </div>
                    </div>
                </div>
                {/* end function detail modal */}

                {/* schema modal */}
                {this.state.opencreateSchemaModal &&
                    <div className="modal d-block animated slideInDown" id="commandModal" role="dialog">
                        <div className="modal-dialog modal-lg modal-dialog-centered">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h6 className="modal-title">Create schema for - {this.state.createdSchemaTopicName}
                                        <button type="button" className="close" data-tooltip data-tooltip-text="Close" data-tooltip-place="bottom" onClick={this.closeCreateSchemaModal} data-dismiss="modal">
                                            <i className="far fa-times"></i>
                                        </button>
                                    </h6>
                                </div>
                                <div className="modal-body">
                                    <div className="json-input-box">
                                        <JSONInput
                                            theme="light_mitsuketa_tribute"
                                            value={this.state.topicSchema}
                                            onChange={(data) => this.topicSchemaChangeHandler(data.jsObject)}
                                            placeholder={this.state.topicSchema}
                                        />
                                    </div>
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-dark" onClick={this.closeCreateSchemaModal} data-dismiss="modal">Cancel</button>
                                    <button type="button" className="btn btn-success" onClick={this.saveCreateSchemaModal}>Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                }
                {/* end schema modal */}

                {/* create topic modal */}
                {this.state.miscTopicModal &&
                    <div className="modal d-block animated slideInDown" id="commandModal" role="dialog">
                        <div className="modal-dialog modal-dialog-centered">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h6 className="modal-title">Add Miscellaneous Topic Suffix
                                        <button type="button" className="close" data-tooltip data-tooltip-text="Close" data-tooltip-place="bottom" disabled={this.state.isSavingMiscTopic} onClick={this.closeMiscTopicModal} data-dismiss="modal">
                                            <i className="far fa-times"></i>
                                        </button>
                                    </h6>
                                </div>
                                <div className="modal-body">
                                    {this.state.isSavingMiscTopic ?
                                        <div className="inner-loader-wrapper" style={{ height: 179 }}>
                                            <div className="inner-loader-content">
                                                <i className="fad fa-sync-alt fa-spin"></i>
                                            </div>
                                        </div> :
                                        <div className="form-group mb-0">
                                            <div className="alert alert-info note-text">
                                                <p className="fw-600"><strong>Topic :</strong> {this.state.miscTopicPrefix}{this.state.miscTopicName}</p>
                                            </div>
                                            <textarea rows="7" id="type" type="text" name="miscTopicName" autoFocus className="form-control" placeholder="Add Suffix Here"
                                                value={this.state.miscTopicName} onChange={this.miscTopicNameChangeHandler}
                                            />
                                        </div>
                                    }
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-dark" disabled={this.state.isSavingMiscTopic} onClick={this.closeMiscTopicModal}>Cancel</button>
                                    <button type="button" className="btn btn-success" disabled={this.state.isSavingMiscTopic} onClick={this.saveMiscTopic}>Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                }
                {/* end create topic modal */}

                {/* delete modal */}
                {this.state.clearSchemaConfirmation &&
                    <div className="modal show d-block animated slideInDown" id="deleteModal">
                        <div className="modal-dialog modal-dialog-centered">
                            <div className="modal-content">
                                <div className="modal-body">
                                    <div className="delete-content">
                                        <div className="delete-icon">
                                            <i className="fad fa-broom text-red"></i>
                                        </div>
                                        <h4 className="text-red">Clear Schema</h4>
                                        <h6>Are you sure you want to clear schema for <strong>{this.state.payload.sourceConnector.properties.topicName}</strong> topic of Device Type <strong>{this.state.connectorsListByCategory["MQTT"].data.filter(deviceType => deviceType.id === this.state.payload.sourceConnector.id)[0]['name']}</strong> ?</h6>
                                    </div>
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-dark" onClick={() => { this.setState({ clearSchemaConfirmation: false }) }}>No</button>
                                    <button type="button" className="btn btn-success" onClick={() => {
                                        this.setState({
                                            isUpdatingAttributes: true,
                                            clearSchemaConfirmation: false,
                                        }, () => this.props.clearTopicSchema(this.state.payload.sourceConnector.id, this.state.payload.sourceConnector.properties.topicId))
                                    }}>Yes
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                }
                {/* end delete modal */}

                { this.state.isOpen &&
                    <NotificationModal
                        type={this.state.modalType}
                        message2={this.state.message2}
                        onCloseHandler={this.onCloseHandler}
                    />
                }
            </React.Fragment >

        );
    }
}

NewAddOrEditEtlConfig.propTypes = {
    dispatch: PropTypes.func.isRequired
};

let allSelectors = {}
Object.entries(SELECTORS).map(([key, value]) => {
    allSelectors[key] = value()
})

const mapStateToProps = createStructuredSelector(allSelectors);

function mapDispatchToProps(dispatch) {
    let allActions = { dispatch }
    Object.entries(ACTIONS).map(([key, value]) => {
        allActions[key] = (...args) => dispatch(value(...args))
    })
    return allActions;
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "newAddOrEditEtlConfig", reducer });
const withSaga = injectSaga({ key: "newAddOrEditEtlConfig", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(NewAddOrEditEtlConfig); 