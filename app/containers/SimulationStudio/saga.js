/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { takeEvery } from 'redux-saga';
import * as CONSTANTS from './constants';

import { apiCallHandler } from '../../api';


export function* getAllSimulationsApiHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_ALL_SIMULATIONS_SUCCESS, CONSTANTS.GET_ALL_SIMULATIONS_FAILURE, 'getAllSimulations')];
}

export function* startStopSimulationApiHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.START_STOP_SIMULATION_SUCCESS, CONSTANTS.START_STOP_SIMULATION_FAILURE, 'startStopSimulation')];
}

export function* deleteSimulationApiHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.DELETE_SIMULATION_SUCCESS, CONSTANTS.DELETE_SIMULATION_FAILURE, 'deleteSimulation')];
}

export function* cloneSimulationApiHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.CLONE_SIMULATION_SUCCESS, CONSTANTS.CLONE_SIMULATION_FAILURE, 'cloneSimulation')];
}

export function* getAllDeviceType(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_ALL_DEVICE_TYPE_SUCCESS, CONSTANTS.GET_ALL_DEVICE_TYPE_FAILURE, 'deviceTypeList')];
}

export function* getDeveloperQuotaHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_DEVELOPER_QUOTA_SUCCESS, CONSTANTS.GET_DEVELOPER_QUOTA_FAILURE, 'getDeveloperQuota')];
}
export function* watcherGetAllSimulations() {
  yield takeEvery(CONSTANTS.GET_ALL_SIMULATIONS, getAllSimulationsApiHandlerAsync);
}

export function* watcherStartStopSimulation() {
  yield takeEvery(CONSTANTS.START_STOP_SIMULATION, startStopSimulationApiHandlerAsync);
}

export function* watcherDeleteSimulation() {
  yield takeEvery(CONSTANTS.DELETE_SIMULATION, deleteSimulationApiHandlerAsync);
}

export function* watcherCloneSimulation() {
  yield takeEvery(CONSTANTS.CLONE_SIMULATION, cloneSimulationApiHandlerAsync);
}

export function* watcherGetAllDeviceType() {
  yield takeEvery(CONSTANTS.GET_ALL_DEVICE_TYPE, getAllDeviceType);
}

export function* watcherGetDeveloperQuota() {
  yield takeEvery(CONSTANTS.GET_DEVELOPER_QUOTA, getDeveloperQuotaHandlerAsync);
}


export default function* rootSaga() {
  yield [
    watcherGetAllSimulations(),
    watcherStartStopSimulation(),
    watcherDeleteSimulation(),
    watcherCloneSimulation(),
    watcherGetAllDeviceType(),
    watcherGetDeveloperQuota(),
  ];
}