/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import * as CONSTANTS from "./constants";

export function getAllSimulations() {
  return {
    type: CONSTANTS.GET_ALL_SIMULATIONS
  };
}

export function getAllDeviceType() {
  return {
    type: CONSTANTS.GET_ALL_DEVICE_TYPE
  };
}

export function startStopSimulation(id, state) {
  return {
    type: CONSTANTS.START_STOP_SIMULATION,
    id,
    state
  };
}

export function deleteSimulation(id) {
  return {
    type: CONSTANTS.DELETE_SIMULATION,
    id
  };
}

export function cloneSimulation(id) {
  return {
    type: CONSTANTS.CLONE_SIMULATION,
    id
  };
}

export function getDeveloperQuota(payload) {
  return {
    type: CONSTANTS.GET_DEVELOPER_QUOTA,
    payload,
  };
}

