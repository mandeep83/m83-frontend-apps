/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
 * Direct selector to the simulationStudio state domain
 */

const selectSimulationStudioDomain = state =>
  state.get("simulationStudio", initialState);


export const getAllSimulationsSuccess = () => createSelector(selectSimulationStudioDomain, substate => substate.getAllSimulationsSuccess);
export const getAllSimulationsFailure = () => createSelector(selectSimulationStudioDomain, substate => substate.getAllSimulationsFailure);

export const startStopSimulationSuccess = () => createSelector(selectSimulationStudioDomain, substate => substate.startStopSimulationSuccess);
export const startStopSimulationError = () => createSelector(selectSimulationStudioDomain, substate => substate.startStopSimulationError);

export const deleteSimulationSuccess = () => createSelector(selectSimulationStudioDomain, substate => substate.deleteSimulationSuccess);
export const deleteSimulationError = () => createSelector(selectSimulationStudioDomain, substate => substate.deleteSimulationError);

export const cloneSimulationSuccess = () => createSelector(selectSimulationStudioDomain, substate => substate.cloneSimulationSuccess);
export const cloneSimulationError = () => createSelector(selectSimulationStudioDomain, substate => substate.cloneSimulationError);

export const getAllDeviceTypeSuccess = () => createSelector(selectSimulationStudioDomain, substate => substate.getAllDeviceTypeSuccess);
export const getAllDeviceTypeFailure = () => createSelector(selectSimulationStudioDomain, substate => substate.getAllDeviceTypeFailure);


export const developerQuotaSuccess = () => createSelector(selectSimulationStudioDomain, subState => subState.developerQuotaSuccess);
export const developerQuotaFailure = () => createSelector(selectSimulationStudioDomain, subState => subState.developerQuotaFailure);

export { selectSimulationStudioDomain };
