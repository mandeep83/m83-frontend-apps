/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

export const GET_ALL_SIMULATIONS = "app/SimulationStudio/GET_ALL_SIMULATIONS";
export const GET_ALL_SIMULATIONS_SUCCESS = "app/SimulationStudio/GET_ALL_SIMULATIONS_SUCCESS";
export const GET_ALL_SIMULATIONS_FAILURE = "app/SimulationStudio/GET_ALL_SIMULATIONS_FAILURE";

export const START_STOP_SIMULATION = "app/SimulationStudio/START_STOP_SIMULATION";
export const START_STOP_SIMULATION_SUCCESS = "app/SimulationStudio/START_STOP_SIMULATION_SUCCESS";
export const START_STOP_SIMULATION_FAILURE = "app/SimulationStudio/START_STOP_SIMULATION_FAILURE";

export const DELETE_SIMULATION = "app/SimulationStudio/DELETE_SIMULATION";
export const DELETE_SIMULATION_SUCCESS = "app/SimulationStudio/DELETE_SIMULATION_SUCCESS";
export const DELETE_SIMULATION_FAILURE = "app/SimulationStudio/DELETE_SIMULATION_FAILURE";

export const CLONE_SIMULATION = "app/SimulationStudio/CLONE_SIMULATION";
export const CLONE_SIMULATION_SUCCESS = "app/SimulationStudio/CLONE_SIMULATION_SUCCESS";
export const CLONE_SIMULATION_FAILURE = "app/SimulationStudio/CLONE_SIMULATION_FAILURE";

export const GET_ALL_DEVICE_TYPE = "app/SimulationStudio/GET_ALL_DEVICE_TYPE";
export const GET_ALL_DEVICE_TYPE_SUCCESS = "app/SimulationStudio/GET_ALL_DEVICE_TYPE_SUCCESS";
export const GET_ALL_DEVICE_TYPE_FAILURE = "app/SimulationStudio/GET_ALL_DEVICE_TYPE_FAILURE";

export const GET_DEVELOPER_QUOTA = 'app/SimulationStudio/GET_DEVELOPER_QUOTA';
export const GET_DEVELOPER_QUOTA_SUCCESS = 'app/SimulationStudio/GET_DEVELOPER_QUOTA_SUCCESS';
export const GET_DEVELOPER_QUOTA_FAILURE = 'app/SimulationStudio/GET_DEVELOPER_QUOTA_FAILURE';
