/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import * as SELECTORS from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import * as ACTIONS from "./actions";
import Loader from "../../components/Loader";
import ConfirmModel from "../../components/ConfirmModel";
import NotificationModal from '../../components/NotificationModal/Loadable'
import ReactTooltip from "react-tooltip";
import MQTT from 'mqtt';
import ReactJson from "react-json-view";
import cloneDeep from "lodash/cloneDeep";
import { getVMQCredentials, convertTimestampToDate, isNavAssigned } from '../../commonUtils';
import TagsInput from 'react-tagsinput';
import ListingTable from "../../components/ListingTable/Loadable";
import NoDataFoundMessage from "../../components/NoDataFoundMessage/Loadable"
import AddNewButton from "../../components/AddNewButton/Loadable";
import isPlainObject from 'lodash/isPlainObject'
import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';
import Skeleton from "react-loading-skeleton";

let client;
/* eslint-disable react/prefer-stateless-function */
export class SimulationStudio extends React.Component {
    state = {
        allSimulations: [],
        allDeviceTypes: [],
        isFetching: true,
        toggleView: true,
        searchTerm: '',
        filteredList: [],
        showSniffingModal: false,
        selectedSimulationTopics: [],
        inputTags: [],
        dataView: "FILE",
        viewType: "TABLE",
        totalSimulationCount: 0,
    }

    refreshComponent = () => {
        this.setState({
            allSimulations: [],
            allDeviceTypes: [],
            isFetching: true,
            searchTerm: '',
            filteredList: [],
            showSniffingModal: false,
            selectedSimulationTopics: [],
            inputTags: [],
            dataView: "FILE",
            viewType: "TABLE",
            totalSimulationCount: 0,
        }, () => {
            let payload = [
                {
                    dependingProductId: null,
                    productName: "device_types",
                }
            ];
            this.props.getDeveloperQuota(payload);
            // this.props.getAllDeviceType();
            // this.props.getAllSimulations();
        })
    }

    componentDidMount() {
        let payload = [
            {
                dependingProductId: null,
                productName: "device_types",
            }
        ];
        this.props.getDeveloperQuota(payload);
        // this.props.getAllDeviceType();
        // this.props.getAllSimulations();
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.developerQuota && nextProps.developerQuota !== this.props.developerQuota) {
            let totalSimulationCount = nextProps.developerQuota.filter(product => product.productName === 'device_types')[0]['total'];
            this.setState({
                totalSimulationCount
            }, () => this.props.getAllDeviceType());
        }

        if (nextProps.developerQuotaFailure && nextProps.developerQuotaFailure !== this.props.developerQuotaFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.developerQuotaFailure
            }, () => this.props.getAllDeviceType());
        }
        if (nextProps.getAllDeviceTypeSuccess !== this.props.getAllDeviceTypeSuccess) {
            let allDeviceTypes = cloneDeep(this.state.allDeviceTypes)
            allDeviceTypes = nextProps.getAllDeviceTypeSuccess
            this.setState({
                allDeviceTypes
            }, () => this.props.getAllSimulations())
        }

        if (nextProps.getAllDeviceTypeFailure !== this.props.getAllDeviceTypeFailure) {
            this.props.getAllSimulations();
        }

        if (nextProps.getAllSimulationsSuccess !== this.props.getAllSimulationsSuccess) {
            let filteredList = nextProps.getAllSimulationsSuccess.map((e) => {
                e.LoadIntervalSecs = e.LoadIntervalSecs + " sec"
                e.LoadCount = e.LoadCount + " out of " + e.totalDevices
                return e
            })
            this.setState({
                isFetching: false,
                allSimulations: nextProps.getAllSimulationsSuccess,
                filteredList,
                isCloneSimulationLoader: false
            })
        }
        if (this.props.getAllSimulationsFailure && nextProps.getAllSimulationsFailure !== this.props.getAllSimulationsFailure) {
            this.setState({
                isOpen: true,
                modalType: "error",
                message2: nextProps.getAllSimulationsFailure,
                isFetching: false
            })
        }

        if (nextProps.startStopSimulationSuccess && nextProps.startStopSimulationSuccess !== this.props.startStopSimulationSuccess) {
            let filteredList = cloneDeep(this.state.filteredList),
                allSimulations = cloneDeep(this.state.allSimulations);
            filteredList.map((temp) => {
                if (temp.ID === nextProps.startStopSimulationSuccess.id) {
                    temp.state = nextProps.startStopSimulationSuccess.state
                    temp.startStopSimulationLoader = false
                }
            })
            allSimulations.map((temp) => {
                if (temp.ID === nextProps.startStopSimulationSuccess.id) {
                    temp.state = nextProps.startStopSimulationSuccess.state
                    temp.startStopSimulationLoader = false
                }
            })
            this.setState({
                filteredList,
                allSimulations,
                isOpen: true,
                modalType: "success",
                message2: nextProps.startStopSimulationSuccess.response,
            })
        }

        if (nextProps.startStopSimulationError !== this.props.startStopSimulationError) {
            let filteredList = cloneDeep(this.state.filteredList),
                allSimulations = cloneDeep(this.state.allSimulations);
            filteredList.map((temp) => {
                if (temp.ID === nextProps.startStopSimulationError.id) {
                    temp.startStopSimulationLoader = false
                }
            })
            allSimulations.map((temp) => {
                if (temp.ID === nextProps.startStopSimulationError.id) {
                    temp.startStopSimulationLoader = false
                }
            })
            this.setState({
                isOpen: true,
                filteredList,
                allSimulations,
                modalType: "error",
                message2: nextProps.startStopSimulationError.error,
            })
        }

        if (nextProps.deleteSimulationSuccess !== this.props.deleteSimulationSuccess) {
            let filteredList = cloneDeep(this.state.filteredList),
                allSimulations = cloneDeep(this.state.allSimulations);
            allSimulations = allSimulations.filter(el => el.ID !== nextProps.deleteSimulationSuccess.id);
            filteredList = filteredList.filter(el => el.ID !== nextProps.deleteSimulationSuccess.id);
            this.setState({
                selectedSimulationId: '',
                isOpen: true,
                isFetching: false,
                filteredList,
                allSimulations,
                modalType: "success",
                message2: nextProps.deleteSimulationSuccess.message,
            });
        }

        if (nextProps.deleteSimulationError && nextProps.deleteSimulationError !== this.props.deleteSimulationError) {
            this.setState({
                isOpen: true,
                modalType: "error",
                message2: nextProps.deleteSimulationError.error,
                isFetching: false
            })
        }

        if (nextProps.cloneSimulationSuccess !== this.props.cloneSimulationSuccess) {
            let filteredList = cloneDeep(this.state.filteredList)
            filteredList.map(simulation => {
                if (simulation.ID === nextProps.cloneSimulationSuccess.id) {
                    simulation.isDisabled = false
                }
            })
            this.setState({
                filteredList,
            }, () => this.props.getAllSimulations());
        }

        if (nextProps.cloneSimulationError && nextProps.cloneSimulationError !== this.props.cloneSimulationError) {
            let filteredList = cloneDeep(this.state.filteredList)
            filteredList.map(simulation => {
                if (simulation.ID === nextProps.cloneSimulationError.id) {
                    simulation.isDisabled = false
                }
            })
            this.setState({
                isOpen: true,
                modalType: "error",
                message2: nextProps.cloneSimulationError.error,
                cloneSimulationError: false
            })
        }
    }

    onCloseHandler = () => {
        this.setState((previousState, props) => ({
            isOpen: false,
            modalType: '',
            message2: ''
        }))
    }

    cancelClicked = () => {
        this.setState({
            selectedSimulationId: "",
            confirmState: false
        })
    }

    startStopSimulation = (id, state) => {
        let filteredList = cloneDeep(this.state.filteredList)
        filteredList.map((temp) => {
            if (temp.ID === id) {
                temp.startStopSimulationLoader = true
            }
        })
        this.setState({
            filteredList,
            isOpen: false,
        }, () => this.props.startStopSimulation(id, state))
    }

    filterDataList = (e) => {
        let filterList = cloneDeep(this.state.allSimulations)
        if (e.target.value.length > 0) {
            filterList = filterList.filter((item, index) => {
                return item.name.toLowerCase().includes(e.target.value.toLowerCase())
            })
        }
        this.setState({
            searchTerm: e.target.value,
            filteredList: filterList
        })
    }

    emptySearchBox = () => {
        let filterList = cloneDeep(this.state.allSimulations)
        this.setState({
            searchTerm: "",
            filteredList: filterList
        })
    }

    cloneSimulation = (id) => {
        let filteredList = cloneDeep(this.state.filteredList);
        filteredList.map(simulation => {
            if (simulation.ID === id) {
                simulation.isDisabled = true
            }
        })
        this.setState({ isCloneSimulationLoader: true, filteredList }, () => this.props.cloneSimulation(id));
    }

    showModal = (simulation) => {
        let selectedTopic = `${simulation.MqttData.topic}/+/report`,
            selectedSimulationTopics = [],
            reportTopic = {
                simulationName: simulation.name,
                topicName: `${simulation.MqttData.topic}/+/report`,
                messages: []
            },
            controlTopic = {
                simulationName: simulation.name,
                topicName: `${simulation.MqttData.topic}/+/control`,
                messages: []
            };
        selectedSimulationTopics.push(reportTopic);
        selectedSimulationTopics.push(controlTopic);
        if (client && client.connected) {
            client.end();
        }

        this.setState({
            selectedSimulationTopics,
            selectedTopic,
            showSniffingModal: true,
        }, () => {
            this.createMqttConnection();
        });
    }

    createMqttConnection = () => {
        let brokerURL = window.location.origin === "http://localhost:3001" ? window.API_URL.split("//")[1].substring(0, window.API_URL.split("//")[1].length - 1) : window.location.host,
            count = 0,
            credentials = getVMQCredentials();
        const options = {
            host: brokerURL,
            protocol: 'wss',
            path: '/connectmqtt',
            username: credentials.userName,
            password: credentials.password,
        };
        client = MQTT.connect(options);
        client.on('connect', function () {
        });
        client.on('close', function (response) {
            if (response && count > 4) {
                client.end();
            }
        });
        client.on('reconnect', function () {
            count++;
        });
        this.subscribeMqttTopic(this.state.selectedTopic)
    }

    subscribeMqttTopic = (selectedTopic) => {
        this.state.previousTopic && client.unsubscribe(`${this.state.previousTopic}`);
        let _this = this,
            selectedSimulationTopics = cloneDeep(this.state.selectedSimulationTopics);

        client.subscribe(selectedTopic, function (err) {
            if (!err) {
                client.on('message', function (topic, message) {
                    selectedSimulationTopics.map(topic => {
                        if (topic.topicName === selectedTopic) {
                            topic.messages.unshift({
                                name: selectedTopic,
                                time: new Date().toLocaleString('en-US', { hour12: false, timeZone: localStorage.timeZone }),
                                message: JSON.stringify(JSON.parse(message.toString()), undefined, 2),
                            });
                            topic.messages.length == 60 && topic.messages.splice(59);
                        }
                    })
                    if (!_this.state.isHoldData) {
                        _this.setState({
                            selectedSimulationTopics,
                        })
                    }
                });
            }
        })
    }

    tabChangeHandler = (selectedTopic) => {
        let previousTopic = this.state.selectedTopic;
        this.setState({
            selectedTopic,
            previousTopic,
        }, () => this.subscribeMqttTopic(selectedTopic))
    }

    closeSniffingModal = () => {
        client.unsubscribe(this.state.selectedTopic);
        this.setState({
            showSniffingModal: false,
            selectedSimulationTopics: [],
            dataView: "FILE"
        })
    }

    clearSniffingModal = () => {
        let selectedSimulationTopics = cloneDeep(this.state.selectedSimulationTopics);
        selectedSimulationTopics.map(topic => topic.messages = [])
        this.setState({
            selectedSimulationTopics
        }, () => this.subscribeMqttTopic(this.state.selectedTopic))
    }

    copyToClipboard = (id) => {
        let copyText = document.getElementById(id);
        copyText.select();
        document.execCommand("copy");
        let element = $(`#Copied${id[id.length - 1]}`)[0]
        this.setState({
            modalType: "success",
            isOpen: true,
            message2: 'Copied to Clipboard successfully !',
        })
    }

    inputTagsChangeHandler = (tags) => {
        this.setState({
            inputTags: tags
        })
    }

    getColumns = () => {
        return [
            {
                Header: "Name", width: 20,
                cell: (row) => (
                    <React.Fragment>
                        <div className="list-view-icon"><i className="fad fa-cube"></i></div>
                        <div className="list-view-icon-box">
                            <h6 className="text-theme fw-600 text-underline-hover text-truncate" onClick={() => { this.props.history.push(`/addOrEditDeviceType/${row.MqttData.selectedConnector}`) }}>{row.name}</h6>
                        </div>
                    </React.Fragment>
                )
            },
            {
                Header: "Device(s)", width: 15,
                cell: (row) => (
                    <div className="button-group-link">
                        <span className="alert alert-primary list-view-badge list-view-badge-link" onClick={() => { this.props.history.push(`/devices/${row.MqttData.selectedConnector}`) }}>{row.LoadCount}</span>
                    </div>
                )
            },
            { Header: "Reporting Frequency", width: 15, accessor: "LoadIntervalSecs" },
            { Header: "Created", width: 15, accessor: "createdAt" },
            { Header: "Updated", width: 15, accessor: "updatedAt" },
            {
                Header: "Actions", width: 20, cell: (row) => (
                    <div className="button-group" >
                        {row.startStopSimulationLoader ?
                            <button type="button" className="btn-transparent btn-transparent-green">
                                <i className="far fa-cog fa-spin"></i>
                            </button>
                            :
                            row.state === "stop" ?
                                <button className="btn-transparent btn-transparent-green" disabled={row.LoadCount === 0}
                                    onClick={() => { this.startStopSimulation(row.ID, "start") }}
                                    data-tooltip data-tooltip-text="Start" data-tooltip-place="bottom">
                                    <i className="far fa-play"></i>
                                </button> :
                                <button className="btn-transparent btn-transparent-gray" onClick={() => { this.startStopSimulation(row.ID, "stop") }}
                                    data-tooltip data-tooltip-text="Stop" data-tooltip-place="bottom">
                                    <i className="far fa-stop"></i>
                                </button>
                        }
                        <button className="btn-transparent btn-transparent-yellow" data-tooltip data-tooltip-text="Debug" data-tooltip-place="bottom"
                            disabled={row.state === "stop" || row.startStopSimulationLoader} onClick={() => isNavAssigned('deviceDiagnostics') && window.open(`/deviceDiagnostics/${row.MqttData.selectedConnector}`, '_blank')}>
                            <i className="far fa-bug"></i>
                        </button>
                        {/*<button className="btn-transparent btn-transparent-yellow" data-tooltip data-tooltip-text="Debug" data-tooltip-place="bottom"
                            disabled={row.state === "stop" || row.startStopSimulationLoader} onClick={() => { this.showModal(row) }}>
                            <i className="far fa-bug"></i>
                        </button>*/}
                        <button className="btn-transparent btn-transparent-cyan" data-tooltip data-tooltip-text="Preview" data-tooltip-place="bottom"
                            onClick={() => this.props.history.push(`/addOrEditSimulation/${row.ID}/true`)}>
                            <i className="far fa-info"></i>
                        </button>
                        <button className="btn-transparent btn-transparent-blue" data-tooltip data-tooltip-text="Edit" data-tooltip-place="bottom"
                            disabled={row.state !== "stop"} onClick={() => this.props.history.push(`/addOrEditSimulation/${row.ID}`)}>
                            <i className="far fa-pencil"></i>
                        </button>
                        <button className="btn-transparent btn-transparent-red" data-tooltip data-tooltip-text="Delete" data-tooltip-place="bottom"
                            disabled={row.state !== "stop"}
                            onClick={() => this.setState({ confirmState: true, selectedSimulationId: row.ID, deleteSimulationName: row.name })}>
                            <i className="far fa-trash-alt"></i>
                        </button>
                    </div >
                )
            },
        ]
    }

    getStatusBarClass = (row) => {
        if (row.state === "start")
            return { className: "green", tooltipText: "Running" }
        return { className: "red", tooltipText: "Stopped" }
    }

    goToaddOrEditSimulationPage = () => {
        this.props.history.push('/addOrEditSimulation')
    }

    checkNestedObject = (message) => {

        let columnData = Object.keys(JSON.parse(message[0].message));
        columnData = columnData.map((columnName) => ({ "headerName": columnName, "field": columnName }))
        return columnData;
    }

    getTableData = (data) => {
        data = JSON.parse(data.message)
        Object.entries(data).map(([key, value]) => {
            if (typeof value == "object") {
                data[key] = JSON.stringify(value)
            }
        })
        return data;
    }
    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>Simulations</title>
                    <meta name="description" content="M83-SimulationStudio" />
                </Helmet>

                <header className="content-header d-flex">
                    <div className="flex-60">
                        <h6>Simulations -
                            <span className="content-header-badge-group">
                                <span className="content-header-badge-item">Allocated <span className="badge badge-pill badge-primary">{this.state.totalSimulationCount}</span></span>
                                <span className="content-header-badge-item">Used <span className="badge badge-pill badge-success">{this.state.allSimulations.length}</span></span>
                                <span className="content-header-badge-item">Remaining <span className="badge badge-pill badge-warning">{this.state.totalSimulationCount > 0 ? this.state.totalSimulationCount - this.state.allSimulations.length : 0}</span></span>
                            </span>
                        </h6>
                    </div>

                    <div className="flex-40 text-right">
                        <div className="content-header-group">
                            <div className="search-box">
                                <span className="search-icon"><i className="far fa-search"></i></span>
                                <input type="text" className="form-control" disabled={this.state.allSimulations.length === 0 || !(this.state.allDeviceTypes.length > 0 && this.state.allSimulations.length > 0) || this.state.isFetching} value={this.state.searchTerm} onChange={this.filterDataList} placeholder="Search..." />
                                {this.state.searchTerm.length > 0 ? <button className="search-button" onClick={() => this.emptySearchBox()} ><i className="far fa-times"></i></button> : ''}
                            </div>
                            <button className={this.state.toggleView == true ? "btn btn-light active" : "btn btn-light"} disabled={this.state.isFetching} data-tooltip="true" data-tooltip-text="List View" data-tooltip-place="bottom" onClick={() => this.setState({ toggleView: true })}><i className="fad fa-list-ul"></i></button>
                            <button className={this.state.toggleView == false ? "btn btn-light active" : "btn btn-light"} disabled={this.state.isFetching} data-tooltip="true" data-tooltip-text="Card View" data-tooltip-place="bottom" onClick={() => this.setState({ toggleView: false })}><i className="fad fa-table"></i></button>
                            <button className="btn btn-light" disabled={this.state.isFetching} data-tooltip data-tooltip-text="Refresh" data-tooltip-place="bottom" onClick={() => this.refreshComponent()}><i className="far fa-sync-alt"></i></button>
                            <button type='button' className="btn btn-primary" data-tooltip data-tooltip-text="Add Simulation" data-tooltip-place="bottom" disabled={(this.state.totalSimulationCount - this.state.allSimulations.length) === 0 || this.state.isFetching} onClick={() => this.goToaddOrEditSimulationPage()}><i className="far fa-plus"></i></button>
                        </div>
                    </div>
                </header>

                <div className="content-body">
                    {this.state.isFetching ?
                        <Skeleton count={9} className="skeleton-list-loader" /> :
                        <React.Fragment>
                            {this.state.allDeviceTypes.length > 0 ?
                                this.state.allSimulations.length > 0 ?
                                    this.state.filteredList.length > 0 ?
                                        this.state.toggleView ?
                                            <ListingTable
                                                columns={this.getColumns()}
                                                data={this.state.filteredList}
                                                statusBar={this.getStatusBarClass}
                                            /> :
                                            <ul className="card-view-list">
                                                {this.state.filteredList.map((item, index) =>
                                                    <li key={index}>
                                                        <div className="card-view-box">
                                                            <div className="card-view-header">
                                                                <span data-tooltip data-tooltip-text={item.state === "start" ? "Running" : "Stopped"} data-tooltip-place="bottom" className={item.state === "start" ? "card-view-header-status bg-success" : "card-view-header-status bg-danger"}></span>
                                                                <span className="alert alert-primary mr-r-7" onClick={() => { this.props.history.push(`/devices/${item.MqttData.selectedConnector}`) }}>
                                                                    Devices - {item.LoadCount}
                                                                </span>
                                                                <span className="alert alert-success"><i className="fas fa-wave-triangle mr-r-5"></i>{item.LoadIntervalSecs}</span>
                                                                <div className="dropdown">
                                                                    <button className="btn-transparent btn-transparent-gray" type="button" data-toggle="dropdown">
                                                                        <i className="fas fa-ellipsis-v"></i>
                                                                    </button>
                                                                    <div className="dropdown-menu">
                                                                        {item.startStopSimulationLoader ?
                                                                            <button type="button" className="dropdown-item">
                                                                                <i className="far fa-cog fa-spin"></i>Loading...
                                                                            </button>
                                                                            :
                                                                            item.state === "stop" ?
                                                                                <button className="dropdown-item" disabled={item.LoadCount === 0}
                                                                                    onClick={() => { this.startStopSimulation(item.ID, "start") }}>
                                                                                    <i className="far fa-play"></i>Play
                                                                                </button> :
                                                                                <button className="dropdown-item" onClick={() => { this.startStopSimulation(item.ID, "stop") }}>
                                                                                    <i className="far fa-stop"></i>Stop
                                                                                </button>
                                                                        }
                                                                        <button className="dropdown-item"
                                                                            disabled={item.state === "stop" || item.startStopSimulationLoader} onClick={() => isNavAssigned('deviceDiagnostics') && window.open(`/deviceDiagnostics/${item.ID}`, '_blank')}>
                                                                            <i className="far fa-bug"></i>Debug
                                                                        </button>

                                                                        <button className="dropdown-item"
                                                                            onClick={() => this.props.history.push(`/addOrEditSimulation/${item.ID}/true`)}>
                                                                            <i className="far fa-info"></i>Preview
                                                                        </button>
                                                                        <button className="dropdown-item"
                                                                            disabled={item.state !== "stop"} onClick={() => this.props.history.push(`/addOrEditSimulation/${item.ID}`)}>
                                                                            <i className="far fa-pencil"></i>Edit
                                                                        </button>
                                                                        <button className="dropdown-item"
                                                                            disabled={item.state !== "stop"}
                                                                            onClick={() => this.setState({ confirmState: true, selectedSimulationId: item.ID, deleteSimulationName: item.name })}>
                                                                            <i className="far fa-trash-alt"></i>Delete
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="card-view-body">
                                                                <div className="card-view-icon">
                                                                    <i className="fad fa-cube"></i>
                                                                </div>
                                                                <h6><i className="far fa-address-book"></i>{item.name ? item.name : "N/A"}</h6>
                                                                <p><i className="far fa-file-alt"></i>{item.description ? item.description : "N/A"}</p>
                                                            </div>
                                                            <div className="card-view-footer d-flex">
                                                                <div className="flex-50 p-3">
                                                                    <h6>Created</h6>
                                                                    <p><strong>By - </strong>{item.createdBy}</p>
                                                                    <p><strong>At - </strong>{new Date(item.createdAt).toLocaleString('en-US', { createdAt: localStorage.createdAt })}</p>
                                                                </div>
                                                                <div className="flex-50 p-3">
                                                                    <h6>Updated</h6>
                                                                    <p><strong>By - </strong>{item.updatedBy}</p>
                                                                    <p><strong>At - </strong>{new Date(item.updatedAt).toLocaleString('en-US', { createdAt: localStorage.createdAt })}</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                )}
                                            </ul>
                                        :
                                        <NoDataFoundMessage />
                                    :
                                    <AddNewButton
                                        text1="No simulation(s) available"
                                        text2="You haven't created any simulation(s) yet"
                                        imageIcon="addSimulation.png"
                                        createItemOnAddButtonClick={this.goToaddOrEditSimulationPage}
                                    />
                                :
                                <AddNewButton
                                    text1="No simulation(s) available"
                                    text2="You haven't created any device type(s) yet. Please create a device type first."
                                    createItemOnAddButtonClick={() => this.props.history.push(`/addOrEditDeviceType`)}
                                    imageIcon="addSimulation.png"
                                />
                            }
                        </React.Fragment>
                    }
                </div>

                {/* sniff modal */}
                {this.state.showSniffingModal &&
                    <div className="modal d-block animated slideInDown" id="sniffingModal">
                        <div className="modal-dialog modal-lg modal-dialog-centered">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h6 className="modal-title">Debug for - <span>{this.state.selectedSimulationTopics[0].simulationName}</span>
                                        <button type="button" className="close" data-tooltip data-tooltip-text="Close" data-tooltip-place="bottom" onClick={this.closeSniffingModal} data-dismiss="modal">
                                            <i className="far fa-times"></i>
                                        </button>
                                    </h6>

                                    <ul className="modal-header-list list-style-none">
                                        <li className={`btn btn-light ${this.state.dataView === 'FILE' ? 'active' : ''}`} data-tooltip data-tooltip-text="JSON View" data-tooltip-place="bottom" onClick={() => this.setState({ dataView: 'FILE' })}>
                                            <i className="far fa-folder"></i>
                                        </li>
                                        <li className={`btn btn-light ${this.state.dataView === 'TREE' ? 'active' : ''}`} data-tooltip data-tooltip-text="Tree View" data-tooltip-place="bottom" onClick={() => this.setState({ dataView: 'TREE' })}>
                                            <i className="far fa-folder-tree"></i>
                                        </li>
                                        <li className={`btn btn-light ${this.state.dataView === 'TABLE' ? 'active' : ''}`} data-tooltip data-tooltip-text="Table View" data-tooltip-place="bottom" onClick={() => this.setState({ dataView: 'TABLE' })}>
                                            <i className="far fa-table"></i>
                                        </li>
                                        <li className="btn btn-light" data-tooltip data-tooltip-text="Clear" data-tooltip-place="bottom" onClick={this.clearSniffingModal} >
                                            <i className="far fa-broom"></i>
                                        </li>
                                    </ul>
                                </div>

                                <div className="modal-body">
                                    <ul className="nav nav-tabs">
                                        {this.state.selectedSimulationTopics.map(topic => {
                                            return (
                                                <li key={topic.topicName} className="nav-item">
                                                    <a className={this.state.selectedTopic === topic.topicName ? "nav-link active" : "nav-link"} onClick={() => { this.tabChangeHandler(topic.topicName) }}>{`${topic.topicName}`}</a>
                                                </li>
                                            )
                                        })}
                                    </ul>
                                    <div className="sniff-wrapper">
                                        <ul className="list-style-none">
                                            {this.state.selectedSimulationTopics.map((topic, index) => {
                                                let html
                                                if (this.state.selectedTopic === topic.topicName) {
                                                    topic.messages.length ?
                                                        this.state.dataView === 'FILE' ?
                                                            html = topic.messages.map((messageObj, index) => {
                                                                this.state.dataView !== 'FILE' ? delete messageObj.time : ''
                                                                delete messageObj.name
                                                                return (
                                                                    <li key={index} onMouseEnter={() => this.setState({ isHoldData: true })} onMouseLeave={() => this.setState({ isHoldData: false })}>
                                                                        <input type="text" id={`copyText${index}`} style={{ opacity: 0, position: 'absolute' }} value={messageObj.message} readOnly />
                                                                        {messageObj.message}
                                                                        <button className="btn-transparent btn-transparent-blue" onClick={() => this.copyToClipboard(`copyText${index}`)}>
                                                                            <i className="far fa-copy"></i>
                                                                        </button>
                                                                        <span>{messageObj.time}</span>
                                                                    </li>
                                                                )
                                                            }) :
                                                            this.state.dataView === 'TREE' ?
                                                                html = topic.messages.map((messageObj, index) => <ReactJson key={index} src={messageObj} />) :
                                                                html = <div className="debug-table-wrapper" style={{ height: 380 }}>
                                                                    <div className="ag-theme-alpine">
                                                                        <AgGridReact
                                                                            columnDefs={this.checkNestedObject(this.state.selectedSimulationTopics[index].messages)}
                                                                            rowData={topic.messages.map((messageObj, index) => this.getTableData(messageObj))}
                                                                            animateRows={true}
                                                                        />
                                                                    </div>
                                                                </div>
                                                        :
                                                        html = (
                                                            <div key={index} className="sniff-loader">
                                                                <div className="sniff-loader-content">
                                                                    <img src="https://content.iot83.com/m83/misc/uploading.gif" />
                                                                    <h6>Debugging...</h6>
                                                                </div>
                                                            </div>
                                                        )
                                                }
                                                return html;
                                            })}
                                        </ul>
                                    </div>
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-success" onClick={this.closeSniffingModal}>OK</button>
                                </div>
                            </div>
                        </div>
                    </div>
                }
                {/* end sniff modal */}

                {this.state.confirmState &&
                    <ConfirmModel
                        status={"delete"}
                        deleteName={this.state.deleteSimulationName}
                        confirmClicked={() => {
                            this.props.deleteSimulation(this.state.selectedSimulationId)
                            this.setState({
                                isFetching: true,
                                confirmState: false,
                                isOpen: false
                            })
                        }}
                        cancelClicked={() => {
                            this.cancelClicked()
                        }}
                    />
                }

                {
                    this.state.isOpen &&
                    <NotificationModal
                        type={this.state.modalType}
                        message2={this.state.message2}
                        onCloseHandler={this.onCloseHandler}
                    />
                }
            </React.Fragment >
        );
    }
}

SimulationStudio.propTypes = {
    dispatch: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
    getAllSimulationsSuccess: SELECTORS.getAllSimulationsSuccess(),
    getAllSimulationsFailure: SELECTORS.getAllSimulationsFailure(),
    startStopSimulationError: SELECTORS.startStopSimulationError(),
    startStopSimulationSuccess: SELECTORS.startStopSimulationSuccess(),
    deleteSimulationSuccess: SELECTORS.deleteSimulationSuccess(),
    deleteSimulationError: SELECTORS.deleteSimulationError(),
    cloneSimulationSuccess: SELECTORS.cloneSimulationSuccess(),
    cloneSimulationError: SELECTORS.cloneSimulationError(),
    getAllDeviceTypeSuccess: SELECTORS.getAllDeviceTypeSuccess(),
    getAllDeviceTypeFailure: SELECTORS.getAllDeviceTypeFailure(),
    developerQuota: SELECTORS.developerQuotaSuccess(),
    developerQuotaError: SELECTORS.developerQuotaFailure(),
});

function mapDispatchToProps(dispatch) {
    return {
        dispatch,
        getAllSimulations: () => dispatch(ACTIONS.getAllSimulations()),
        getAllDeviceType: () => dispatch(ACTIONS.getAllDeviceType()),
        startStopSimulation: (id, state) => dispatch(ACTIONS.startStopSimulation(id, state)),
        deleteSimulation: (id) => dispatch(ACTIONS.deleteSimulation(id)),
        cloneSimulation: (id) => dispatch(ACTIONS.cloneSimulation(id)),
        getDeveloperQuota: (payload) => dispatch(ACTIONS.getDeveloperQuota(payload)),
    };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "simulationStudio", reducer });
const withSaga = injectSaga({ key: "simulationStudio", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(SimulationStudio);
