/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

export const DEFAULT_ACTION = "app/AddorEditSimulation/DEFAULT_ACTION";

export const SAVE_SIMULATION = "app/AddorEditSimulation/SAVE_SIMULATION";
export const SAVE_SIMULATION_SUCCESS = "app/AddorEditSimulation/SAVE_SIMULATION_SUCCESS";
export const SAVE_SIMULATION_FAILURE = "app/AddorEditSimulation/SAVE_SIMULATION_FAILURE";


export const GET_SIMULATION_BY_ID = "app/AddorEditSimulation/GET_SIMULATION_BY_ID";
export const GET_SIMULATION_BY_ID_SUCCESS = "app/AddorEditSimulation/GET_SIMULATION_BY_ID_SUCCESS";
export const GET_SIMULATION_BY_ID_FAILURE = "app/AddorEditSimulation/GET_SIMULATION_BY_ID_FAILURE";

export const GET_ALL_SIMULATIONS = "app/AddorEditSimulation/GET_ALL_SIMULATIONS";
export const GET_ALL_SIMULATIONS_SUCCESS = "app/AddorEditSimulation/GET_ALL_SIMULATIONS_SUCCESS";
export const GET_ALL_SIMULATIONS_FAILURE = "app/AddorEditSimulation/GET_ALL_SIMULATIONS_FAILURE";

export const SAVE_LAMBDA_FUNCTION = "app/AddorEditSimulation/SAVE_LAMBDA_FUNCTION";
export const SAVE_LAMBDA_FUNCTION_SUCCESS = "app/AddorEditSimulation/SAVE_LAMBDA_FUNCTION_SUCCESS";
export const SAVE_LAMBDA_FUNCTION_FAILURE = "app/AddorEditSimulation/SAVE_LAMBDA_FUNCTION_FAILURE";

export const DELETE_LAMBDA_FUNCTION = "app/AddorEditSimulation/DELETE_LAMBDA_FUNCTION";
export const DELETE_LAMBDA_FUNCTION_SUCCESS = "app/AddorEditSimulation/DELETE_LAMBDA_FUNCTION_SUCCESS";
export const DELETE_LAMBDA_FUNCTION_FAILURE = "app/AddorEditSimulation/DELETE_LAMBDA_FUNCTION_FAILURE";

export const GET_LIST_OF_CONNECTORS = "app/AddorEditSimulation/GET_LIST_OF_CONNECTORS";
export const GET_LIST_OF_CONNECTORS_SUCCESS = "app/AddorEditSimulation/GET_LIST_OF_CONNECTORS_SUCCESS";
export const GET_LIST_OF_CONNECTORS_FAILURE = "app/AddorEditSimulation/GET_LIST_OF_CONNECTORS_FAILURE";

export const GET_FAAS_IMAGES = "app/AddorEditSimulation/GET_FAAS_IMAGES";
export const GET_FAAS_IMAGES_SUCCESS = "app/AddorEditSimulation/GET_FAAS_IMAGES_SUCCESS";
export const GET_FAAS_IMAGES_FAILURE = "app/AddorEditSimulation/GET_FAAS_IMAGES_FAILURE";
