/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { createSelector } from "reselect";
import { initialState } from "./reducer";


const selectAddorEditSimulationDomain = state =>
  state.get("addOrEditSimulation", initialState);


export const saveSimulationSuccess = () => createSelector(selectAddorEditSimulationDomain, substate => substate.saveSimulationSuccess);
export const saveSimulationError = () => createSelector(selectAddorEditSimulationDomain, substate => substate.saveSimulationError);

export const getSimulationByIdSuccess = () => createSelector(selectAddorEditSimulationDomain, substate => substate.getSimulationByIdSuccess);
export const getSimulationByIdError = () => createSelector(selectAddorEditSimulationDomain, substate => substate.getSimulationByIdError);


export const getAllSimulationsSuccess = () => createSelector(selectAddorEditSimulationDomain, substate => substate.getAllSimulationsSuccess);
export const getAllSimulationsFailure = () => createSelector(selectAddorEditSimulationDomain, substate => substate.getAllSimulationsFailure);

export const saveLambdaFunctionSuccess = () => createSelector(selectAddorEditSimulationDomain, substate => substate.saveLambdaFunctionSuccess);
export const saveLambdaFunctionFailure = () => createSelector(selectAddorEditSimulationDomain, substate => substate.saveLambdaFunctionFailure);

export const deleteLambdaFunctionSuccess = () => createSelector(selectAddorEditSimulationDomain, substate => substate.deleteLambdaFunctionSuccess);
export const deleteLambdaFunctionFailure = () => createSelector(selectAddorEditSimulationDomain, substate => substate.deleteLambdaFunctionFailure);

export const getListOfConnectorsSuccess = () => createSelector(selectAddorEditSimulationDomain, substate => substate.getListOfConnectorsSuccess);
export const getListOfConnectorsFailure = () => createSelector(selectAddorEditSimulationDomain, substate => substate.getListOfConnectorsFailure);

export const getFaasImagesSuccess = () => createSelector(selectAddorEditSimulationDomain, substate => substate.getFaasImagesSuccess);
export const getFaasImagesFailure = () => createSelector(selectAddorEditSimulationDomain, substate => substate.getFaasImagesFailure);