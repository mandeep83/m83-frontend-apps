/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import * as CONSTANTS from "./constants";

export function saveSimulation(payload) {
  return {
    type: CONSTANTS.SAVE_SIMULATION,
    payload
  };
}

export function saveLambdaFunction(payload) {
  return {
    type: CONSTANTS.SAVE_LAMBDA_FUNCTION,
    payload
  };
}

export function deleteLambdaFunction(payload) {
  return {
    type: CONSTANTS.DELETE_LAMBDA_FUNCTION,
    payload
  };
}

export function getSimulationById(id) {
  return {
    type: CONSTANTS.GET_SIMULATION_BY_ID,
    id
  };
}

export function getAllSimulations() {
  return {
    type: CONSTANTS.GET_ALL_SIMULATIONS,
  };
}

export function getListOfConnectors() {
  return {
    type: CONSTANTS.GET_LIST_OF_CONNECTORS
  }
}

export function getFaasImages(interpreter) {
  return {
    type: CONSTANTS.GET_FAAS_IMAGES,
    interpreter
  }
}
