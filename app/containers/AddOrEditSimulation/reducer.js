/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { fromJS } from "immutable";
import * as CONSTANTS from "./constants";

export const initialState = fromJS({});

function addorEditSimulationReducer(state = initialState, action) {
  switch (action.type) {
    case CONSTANTS.SAVE_SIMULATION_SUCCESS:
      return Object.assign({}, state, {
        saveSimulationSuccess: action.response,
      });
    case CONSTANTS.SAVE_SIMULATION_FAILURE:
      return Object.assign({}, state, {
        saveSimulationError: {
          error: action.error,
          timestamp: new Date()
        }
      });
    case CONSTANTS.GET_SIMULATION_BY_ID_SUCCESS:
      return Object.assign({}, state, {
        getSimulationByIdSuccess: action.response,
      });
    case CONSTANTS.GET_SIMULATION_BY_ID_FAILURE:
      return Object.assign({}, state, {
        getSimulationByIdError: action.error,
      });
    case CONSTANTS.GET_ALL_SIMULATIONS_SUCCESS:
      return Object.assign({}, state, {
        'getAllSimulationsSuccess': action.response,
      })
    case CONSTANTS.GET_ALL_SIMULATIONS_SUCCESS:
      return Object.assign({}, state, {
        'getAllSimulationsFailure': action.error,
      });
    case CONSTANTS.SAVE_LAMBDA_FUNCTION_SUCCESS:
      return Object.assign({}, state, {
        'saveLambdaFunctionSuccess': action.response
      });
    case CONSTANTS.SAVE_LAMBDA_FUNCTION_FAILURE:
      return Object.assign({}, state, {
        'saveLambdaFunctionFailure': action.error
      });
    case CONSTANTS.DELETE_LAMBDA_FUNCTION:
      return Object.assign({}, state, {
        'deleteLambdaFunctionSuccess': undefined,
        'deleteLambdaFunctionFailure': undefined
      });
    case CONSTANTS.DELETE_LAMBDA_FUNCTION_SUCCESS:
      return Object.assign({}, state, {
        'deleteLambdaFunctionSuccess': action.response
      });
    case CONSTANTS.DELETE_LAMBDA_FUNCTION_FAILURE:
      return Object.assign({}, state, {
        'deleteLambdaFunctionFailure': action.error
      });
    case CONSTANTS.GET_LIST_OF_CONNECTORS_SUCCESS:
      return Object.assign({}, state, {
        'getListOfConnectorsSuccess': action.response
      });
    case CONSTANTS.GET_LIST_OF_CONNECTORS_FAILURE:
      return Object.assign({}, state, {
        'getListOfConnectorsFailure': action.error
      });
    case CONSTANTS.GET_FAAS_IMAGES_SUCCESS:
      return Object.assign({}, state, {
        'getFaasImagesSuccess': action.response
      });
    case CONSTANTS.GET_FAAS_IMAGES_FAILURE:
      return Object.assign({}, state, {
        'getFaasImagesFailure': action.error
      });
    default:
      return state;
  }
}

export default addorEditSimulationReducer;
