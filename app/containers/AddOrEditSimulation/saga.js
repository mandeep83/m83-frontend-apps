/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { apiCallHandler } from '../../api';
import { takeEvery } from 'redux-saga';
import { put } from 'redux-saga/effects';
import * as CONSTANTS from './constants'
import { getConnectorsFailure } from './selectors';

export function* saveSimulationHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.SAVE_SIMULATION_SUCCESS, CONSTANTS.SAVE_SIMULATION_FAILURE, 'saveSimulation')];
}

export function* getSimulationByIdHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_SIMULATION_BY_ID_SUCCESS, CONSTANTS.GET_SIMULATION_BY_ID_FAILURE, 'getSimulationById')];
}

export function* apiGetAllSimulations(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_ALL_SIMULATIONS_SUCCESS, CONSTANTS.GET_ALL_SIMULATIONS_FAILURE, 'getAllSimulations')];
}

export function* saveLambdaFunctionHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.SAVE_LAMBDA_FUNCTION_SUCCESS, CONSTANTS.SAVE_LAMBDA_FUNCTION_FAILURE, 'saveLambdaFunctionSimulation')];
}

export function* deleteLambdaFunctionHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.DELETE_LAMBDA_FUNCTION_SUCCESS, CONSTANTS.DELETE_LAMBDA_FUNCTION_FAILURE, 'deleteLambdaFunctionSimulation')];
}

export function* getListOfConnectorsApiHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_LIST_OF_CONNECTORS_SUCCESS, CONSTANTS.GET_LIST_OF_CONNECTORS_FAILURE, 'getListOfConnectorsWithGroups')];
}

export function* faasImagesHandler(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_FAAS_IMAGES_SUCCESS, CONSTANTS.GET_FAAS_IMAGES_FAILURE, 'getFaasImages')]
}

export function* watcherSaveSimulation() {
  yield takeEvery(CONSTANTS.SAVE_SIMULATION, saveSimulationHandlerAsync);
}

export function* watcherSaveLambdaFunction() {
  yield takeEvery(CONSTANTS.SAVE_LAMBDA_FUNCTION, saveLambdaFunctionHandlerAsync);
}

export function* watcherDeleteLambdaFunction() {
  yield takeEvery(CONSTANTS.DELETE_LAMBDA_FUNCTION, deleteLambdaFunctionHandlerAsync);
}

export function* watcherGetAllSimulations() {
  yield takeEvery(CONSTANTS.GET_ALL_SIMULATIONS, apiGetAllSimulations);
}

export function* watcherGetSimulationById() {
  yield takeEvery(CONSTANTS.GET_SIMULATION_BY_ID, getSimulationByIdHandlerAsync);
}

export function* watcherGetListOfConnectors() {
  yield takeEvery(CONSTANTS.GET_LIST_OF_CONNECTORS, getListOfConnectorsApiHandlerAsync);
}

export function* watcherGetFaasImages() {
  yield takeEvery(CONSTANTS.GET_FAAS_IMAGES, faasImagesHandler);
}


export default function* rootSaga() {
  yield [
    watcherSaveSimulation(),
    watcherSaveLambdaFunction(),
    watcherDeleteLambdaFunction(),
    watcherGetSimulationById(),
    watcherGetAllSimulations(),
    watcherGetListOfConnectors(),
    watcherGetFaasImages(),
  ];
}
