/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import React from "react";
import PropTypes, { object, element } from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { FormattedMessage } from "react-intl";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import AceEditor from "react-ace";
import 'brace/mode/python';
import 'brace/mode/json';
import 'brace/theme/monokai';
import 'brace/theme/github';
import 'brace/ext/searchbox'
import 'brace/ext/language_tools'
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import reducer from "./reducer";
import saga from "./saga";
import Loader from "../../components/Loader";
import NotificationModal from '../../components/NotificationModal/Loadable'
import { Object } from "core-js";
import * as Actions from './actions';
import * as Selectors from './selectors';
import ConfirmModel from "../../components/ConfirmModel";
import Slider from 'react-input-slider';
var flatten = require('flat');
var unflatten = require('flat').unflatten
import TagsInput from 'react-tagsinput';
import ReactTooltip from "react-tooltip";
import ReactSelect from 'react-select';
import cloneDeep from "lodash/cloneDeep";
import AllChartsComponent from "../../components/AllChartsComponent";
import GoogleMap from "../../components/GoogleMap";
import { getAddressFromLatLng } from '../../commonUtils'
import MapSearchBox from "../../components/MapSearchBox"

let floatRegex = /^[+-]?((\.\d+)|(\d+(\.\d+)?))$/,
    intRegex = /^([-]?[1-9]\d*|0)$/;

let chart = {};

function countDecimals(text) {
    // let text = num.toString()
    if (text.indexOf('e-') > -1) {
        let [base, trail] = text.split('e-')
        let elen = parseInt(trail, 10)
        let idx = base.indexOf(".")
        return idx == -1 ? 0 + elen : (base.length - idx - 1) + elen
    }
    let index = text.indexOf(".")
    return index == -1 ? 0 : (text.length - index - 1)
}

/* eslint-disable react/prefer-stateless-function */
export class AddOrEditSimulation extends React.Component {
    state = {
        isFetching: true,
        payload: {
            deviceTypeId: '',
            connectorType: 'stream',
            connector: 'MQTT',
            loadIntervalSecs: 10,
            daysCount: 1,
            inputJson: '',
            masterConfig: [{
                StartTime: '00:00',
                EndTime: '24:00',
                ConfigData: {}
            }],
            templateData: '',
            isExternal: false,
            deviceGroupIds: [],
            mqttData: {
                topic: '',
                topicType: "ReportAndControl"
            },
        },
        flattenJson: {},
        errorMessage: '',
        variableModalConfigKey: '',
        variableModalCommandConfigKey: '',
        masterIndex: -1,
        modalPayload: {},
        editEquationForm: {
            "a": 0,
            "b": 0,
            "c": 0,
            "d": 0
        },
        modalErrorMessage: '',
        configErrorCode: 0,
        selectedMasterIndex: 0,
        selectedPathParam: {
            description: "",
            in: "query",
            name: "",
            required: "true",
            type: "Boolean",
            defaultValue: ""
        },
        controlForm: {
        },
        listOfDeviceGroupsForSelectedConnector: [],
        selectedJson: '',
        isViewOnly: this.props.match.params.isView,
        configErrorKeys: [],
        selectedDeviceCount: 0,
        locationModalPayload: {
            lat: "",
            lng: "",
        },
        addGeoLocation: false //set geoLocation false on code changes
    }

    componentDidMount() {
        this.generateHours();
        this.props.getListOfConnectors(); //get deviceTypes with groups and details
        this.props.getFaasImages("python:3");
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.saveSimulationSuccess && nextProps.saveSimulationSuccess !== this.props.saveSimulationSuccess) {
            this.setState({
                isOpen: true,
                modalType: "success",
                message2: nextProps.saveSimulationSuccess.message,
                submitSuccess: true
            });
        }

        if (nextProps.saveSimulationError && nextProps.saveSimulationError != this.props.saveSimulationError) {
            this.setState({
                isOpen: true,
                modalType: "error",
                message2: nextProps.saveSimulationError.error,
                isFetching: false,
            })
        }

        if (nextProps.saveLambdaFunctionSuccess && nextProps.saveLambdaFunctionSuccess !== this.props.saveLambdaFunctionSuccess) {
            let modalPayload = cloneDeep(this.state.modalPayload),
                payload = cloneDeep(this.state.payload);

            modalPayload.actionId = nextProps.saveLambdaFunctionSuccess.data.id;
            modalPayload.actionName = nextProps.saveLambdaFunctionSuccess.data.actionName;

            payload.masterConfig[this.state.masterIndex].ConfigData[this.state.functionModalConfigKey].commandConfig[this.state.functionModalCommandConfigKey].lambdaFunctionData = modalPayload

            this.setState({
                isOpen: true,
                modalType: "success",
                message2: nextProps.saveLambdaFunctionSuccess.message,
                modalPayload: {},
                masterIndex: -1,
                functionModalConfigKey: '',
                functionModalCommandConfigKey: '',
                payload
            }, () => $('#functionModal').modal('hide'));
        }

        if (nextProps.saveLambdaFunctionFailure && nextProps.saveLambdaFunctionFailure != this.props.saveLambdaFunctionFailure) {
            this.setState({
                isOpen: true,
                modalType: "error",
                message2: nextProps.saveLambdaFunctionFailure,
                isFetching: false
            })
        }

        if (nextProps.getSimulationByIdSuccess && nextProps.getSimulationByIdSuccess !== this.props.getSimulationByIdSuccess) {
            let startingHours = cloneDeep(this.state.startingHours),
                endingHours = cloneDeep(this.state.endingHours),
                payload = cloneDeep(nextProps.getSimulationByIdSuccess),
                selectedDeviceCount = 0,
                selectedConnector = this.state.connectorsList.find(el => el.id === payload.MqttData.selectedConnector);

            let listOfDeviceGroupsForSelectedConnector = selectedConnector.deviceGroups
            payload.deviceGroupIds = payload.deviceGroupIds.filter(id => listOfDeviceGroupsForSelectedConnector.some(group => group.deviceGroupId === id))
            payload.masterConfig = payload.MasterConfig;
            payload.templateData = payload.TemplateData;
            payload.loadIntervalSecs = payload.LoadIntervalSecs;
            payload.daysCount = payload.DaysCount;
            payload.connector = payload.Connector;
            payload.deviceTypeId = payload.deviceTypeId;

            payload.masterConfig.map((master, masterIndex) => {
                master.StartTime = master.StartTime < 10 ? `0${master.StartTime}:00` : `${master.StartTime}:00`
                master.EndTime = master.EndTime < 10 ? `0${master.EndTime}:00` : `${master.EndTime}:00`
                startingHours.map((hours, index) => {
                    if ((master.StartTime).split(':')[0] <= index && ((master.EndTime).split(':')[0] - 1) >= (index)) {
                        hours.currentSimulationIndex = masterIndex,
                            hours.selected = true
                    }
                })
                endingHours.map((hours, index) => {
                    if ((master.StartTime).split(':')[0] < (index + 1) && ((master.EndTime).split(':')[0]) > (index)) {
                        hours.currentSimulationIndex = masterIndex
                        hours.selected = true
                        if (masterIndex === this.state.selectedMasterIndex) {
                            hours.disabled = false
                        }
                    }
                })
                let configDataKeysArray = Object.keys(master.ConfigData)
                for (let j = 0; j < configDataKeysArray.length; j++) {
                    let configMapKey = configDataKeysArray[j],
                        commandConfigKeysArray;
                    commandConfigKeysArray = Object.keys(master.ConfigData[configMapKey].commandConfig)
                    for (let k = 0; k < commandConfigKeysArray.length; k++) {
                        let commandConfigKey = commandConfigKeysArray[k],
                            config = master.ConfigData[configMapKey].commandConfig[commandConfigKey];
                        if (config.valueType == "gps") {
                            getAddressFromLatLng(config.valueRange[0], config.valueRange[1], (location) => {
                                let payload = cloneDeep(this.state.payload)
                                payload.masterConfig[masterIndex].ConfigData[configMapKey].commandConfig[commandConfigKey].startLocation = location
                                this.setState({ payload })
                            })
                            getAddressFromLatLng(config.valueRange[2], config.valueRange[3], (location) => {
                                let payload = cloneDeep(this.state.payload)
                                payload.masterConfig[masterIndex].ConfigData[configMapKey].commandConfig[commandConfigKey].endLocation = location
                                this.setState({ payload })
                            })
                        }
                    }
                }
            })
            listOfDeviceGroupsForSelectedConnector.map(group => {
                if (payload.deviceGroupIds.includes(group.deviceGroupId)) {
                    selectedDeviceCount += group.deviceCount
                }
            })
            // if (payload.connector === "MQTT") {
            payload.mqttData = { ...payload.MqttData }
            payload.mqttData.topic = ""
            payload.mqttData.topic = selectedConnector.topic
            delete payload.MqttData
            // }
            delete payload.MasterConfig
            delete payload.LoadCount
            delete payload.TemplateData
            delete payload.LoadIntervalSecs
            delete payload.Connector
            delete payload.DaysCount
            this.setState({
                payload,
                isFetching: false,
                startingHours,
                endingHours,
                listOfDeviceGroupsForSelectedConnector,
                selectedDeviceCount,
                selectedConnector,
                isViewOnly: payload.state === "start" || this.props.match.params.isView,
                addGeoLocation: Boolean(Object.keys(payload.masterConfig[0].ConfigData).find(el => payload.masterConfig[0].ConfigData[el].commandConfig.default.valueType == "gps")) // check if geolocation is part of masterconfig
            })
        }

        if (nextProps.getSimulationByIdError && nextProps.getSimulationByIdError != this.props.getSimulationByIdError) {
            this.setState({
                isOpen: true,
                modalType: "error",
                message2: nextProps.getSimulationByIdError,
                isFetching: false
            })
        }

        if (nextProps.getAllSimulationsSuccess && nextProps.getAllSimulationsSuccess !== this.props.getAllSimulationsSuccess) {
            let unavailableMqttConnector = nextProps.getAllSimulationsSuccess.map(el => { return el.MqttData.selectedConnector }),
                connectorsList = cloneDeep(this.state.connectorsList)
            connectorsList = connectorsList.filter(el => !unavailableMqttConnector.includes(el.id))
            this.setState({
                connectorsList,
                isFetching: false
            })
        }

        if (nextProps.getAllSimulationsError && nextProps.getAllSimulationsError !== this.props.getAllSimulationsError) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.getAllSimulationsError,
                isFetching: false,
            })
        }

        if (nextProps.deleteLambdaFunctionSuccess && nextProps.deleteLambdaFunctionSuccess !== this.props.deleteLambdaFunctionSuccess) {
            let lambdaFunctionChangeIndex = cloneDeep(this.state.lambdaFunctionChangeIndex),
                payload = cloneDeep(this.state.payload),
                firstObj = lambdaFunctionChangeIndex[0],
                changeIndex;

            if (firstObj.isValueTypeChange) {
                changeIndex = payload.masterConfig[firstObj.masterIndex].ConfigData[firstObj.configKey].commandConfig["default"]
                changeIndex.valueType = firstObj.valueIfActionDeleted
                if (changeIndex.valueType === "time") {
                    changeIndex.rangeType === "linear"
                }
                else {
                    changeIndex.rangeType === "static"
                }
                payload.masterConfig[firstObj.masterIndex].ConfigData[firstObj.configKey].command = "default"
                payload.masterConfig[firstObj.masterIndex].ConfigData[firstObj.configKey].commandConfig = {
                    "default": changeIndex
                }
            }
            else if (firstObj.isValueTypeChange === false) {
                changeIndex = payload.masterConfig[firstObj.masterIndex].ConfigData[firstObj.configKey].commandConfig[firstObj.commandConfigKey]
                changeIndex.rangeType = firstObj.valueIfActionDeleted
            }

            if (changeIndex.valueType === "boolean") {
                if (changeIndex.rangeType === "static") {
                    changeIndex.valueRange = ["true"]
                }
                else if (changeIndex.rangeType !== "lambdaFunction") {
                    changeIndex.valueRange = ["true", "false"]
                }
            }
            this.setState({
                modalType: "success",
                isOpen: true,
                message2: nextProps.deleteLambdaFunctionSuccess,
                payload,
                lambdaFunctionChangeIndex: [],
                controlIndex: this.state.controlIndex === firstObj.configKey ? '' : this.state.controlIndex
            })
        }

        if (nextProps.deleteLambdaFunctionFailure && nextProps.deleteLambdaFunctionFailure !== this.props.deleteLambdaFunctionFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.deleteLambdaFunctionFailure,
            })
        }

        if (nextProps.getListOfConnectorsSuccess && nextProps.getListOfConnectorsSuccess !== this.props.getListOfConnectorsSuccess) {
            if (this.props.match.params.id) {
                this.props.getSimulationById(this.props.match.params.id)
            }
            else
                this.props.getAllSimulations();
            this.setState({
                connectorsList: nextProps.getListOfConnectorsSuccess
            })
        }

        if (nextProps.getListOfConnectorsFailure && nextProps.getListOfConnectorsFailure !== this.props.getListOfConnectorsFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.getListOfConnectorsFailure,
            })
        }
        if (nextProps.getFaasImagesSuccess && nextProps.getFaasImagesSuccess !== this.props.getFaasImagesSuccess) {
            this.setState({
                faasImages: nextProps.getFaasImagesSuccess
            })
        }

        if (nextProps.getFaasImagesFailure && nextProps.getFaasImagesFailure !== this.props.getFaasImagesFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.getFaasImagesFailure,
            })
        }
    }

    scrollToMyRef = () => {
        this.myRef.scrollIntoView();
    }

    myRef = React.createRef()

    onCloseHandler = () => {
        this.setState((previousState, props) => ({
            isOpen: false,
            isFetching: this.state.submitSuccess === true,
            modalType: '',
            message2: ''
        }), () => {
            if (this.state.submitSuccess === true) {
                this.props.history.push(`/simulation`)
            }
        })
    }

    changeView = (event) => {
        // event.preventDefault();
        let flattenJson = this.state.flattenJson,
            errorMessage;
        if (this.IsJsonString(this.state.payload.inputJson) && !Array.isArray(JSON.parse(this.state.payload.inputJson))) {
            Object.keys(JSON.parse(this.state.payload.inputJson)).length ?
                flattenJson = flatten(JSON.parse(this.state.payload.inputJson)) :
                errorMessage = "Empty JSON is not Allowed"

            let keysArray = Object.keys(JSON.parse(this.state.payload.inputJson));
            for (let i = 0; i < keysArray.length; i++) {
                if (/\s/g.test(keysArray[i])) {
                    errorMessage = "Spaces are not allowed in JSON keys. Please enter a valid JSON";
                    break;
                }
            }
        }
        else {
            errorMessage = "Please enter a valid JSON"
        }

        if (errorMessage) {
            this.setState({
                errorMessage
            })
        } else {
            this.createConfigurationObject(flattenJson)
        }
    }

    IsJsonString = (str) => {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }

    createConfigurationObject = (flattenJson) => {
        let payload = cloneDeep(this.state.payload),
            addGeoLocation = false;
        payload.masterConfig = [{
            StartTime: '00:00',
            EndTime: '24:00',
            ConfigData: {}
        }]
        Object.keys(flattenJson).map((att, index) => {
            let value = flattenJson[att].toString().toLowerCase() === "true" ? "true" : flattenJson[att].toString().toLowerCase() === "false" ? "false" : flattenJson[att].toString(),
                id = "Id" + index;
            payload.masterConfig[0].ConfigData[id] = {
                "command": "default",
                "commandConfig": {
                    "default": {
                        "id": id,
                        "name": att,
                        "currentValue": value,
                        "valueType": this.checkValueTypeOfKey(value, att),
                        "valueRange": [],
                        "rangeType": "static",
                        "valueDistribution": {},
                        "lambdaFunctionData": {}
                    }
                },
            }
            let defaultConfig = payload.masterConfig[0].ConfigData[id].commandConfig.default
            if (defaultConfig.valueType === "int") {
                defaultConfig.rangeType = "randomAlways"
                let from = (parseInt(0.8 * defaultConfig.currentValue)).toString(),
                    to = (parseInt(1.2 * defaultConfig.currentValue)).toString(),
                    step = "1";
                if (Math.sign(defaultConfig.currentValue) == -1) {
                    from = (parseInt(1.2 * defaultConfig.currentValue)).toString();
                    to = (parseInt(0.8 * defaultConfig.currentValue)).toString();
                    step = "1";
                }
                else if (defaultConfig.currentValue == 0) {
                    from = "0";
                    to = "1";
                    step = "1";
                }
                defaultConfig.valueRange = [from, to, step]
            }
            else if (defaultConfig.valueType === "float") {
                defaultConfig.rangeType = "randomAlways"
                let decimalCount = countDecimals(defaultConfig.currentValue)
                let from = ((parseFloat(0.8 * defaultConfig.currentValue)).toFixed(decimalCount)).toString(),
                    to = ((parseFloat(1.2 * defaultConfig.currentValue)).toFixed(decimalCount)).toString(),
                    step = (1 / Math.pow(10, decimalCount)).toFixed(decimalCount).toString();
                if (Math.sign(defaultConfig.currentValue) == -1) {
                    from = ((parseFloat(1.2 * defaultConfig.currentValue)).toFixed(decimalCount)).toString();
                    to = ((parseFloat(0.8 * defaultConfig.currentValue)).toFixed(decimalCount)).toString();
                    step = (1 / Math.pow(10, decimalCount)).toFixed(decimalCount).toString();
                }
                else if (defaultConfig.currentValue == 0) {
                    from = "0";
                    to = "1";
                    step = "0.1";
                }
                defaultConfig.valueRange = [from, to, step]

            }
            else if (defaultConfig.valueType === "time") {
                defaultConfig.rangeType = "linear"
            }
            else {
                payload.masterConfig[0].ConfigData[id].commandConfig.default.valueRange.push(value)
            }
        })
        if (this.state.selectedConnector && this.state.selectedConnector.assetType == "Movable") {
            let id = `Id${Object.keys(flattenJson).length}`
            payload.masterConfig[0].ConfigData[id] = {
                "command": "default",
                "commandConfig": {
                    "default": {
                        "id": id,
                        "name": "geoLocation",
                        "currentValue": "N/A",
                        "valueType": "gps",
                        "valueRange": ["", "", "", "", ""],
                        "rangeType": "linear",
                    }
                },
            }
            addGeoLocation = true
        }
        this.setState({
            payload,
            flattenJson,
            addGeoLocation //set geoLocation false on code changes
        }, () => {
            this.scrollToMyRef()
        })
    }

    checkValueTypeOfKey = (value, att) => {
        if (att.toLowerCase() === "timestamp") {
            return ("time");
        }

        if (!isNaN(Number(value))) {
            return (value).includes(".") ? "float" : "int"
        }
        else if (value.trim().toLowerCase() === "true" || value.trim().toLowerCase() === "false") {
            return ("boolean");
        }
        else {
            return ("string");
        }
    }

    changeHandlerAceEditor = (code) => {
        let payload = cloneDeep(this.state.payload);
        payload.inputJson = code
        payload.masterConfig = [{
            StartTime: '00:00',
            EndTime: '24:00',
            ConfigData: {}
        }]

        this.setState({
            payload,
            errorMessage: '',
            selectedJson: '',
            addGeoLocation: false //set geoLocation false on code changes
        })
    }

    simulationDataChangeHandler = (event, configKey, masterIndex, commandConfigKey) => {
        let payload = cloneDeep(this.state.payload),
            controlForm = cloneDeep(this.state.controlForm),
            changeIndex = payload.masterConfig[masterIndex].ConfigData[configKey].commandConfig[commandConfigKey];
        if (event.target.name === "valueRange" && event.target.type === "number") {
            if (event.target.id.split("_")[0] === "from") {
                if (changeIndex.valueType === "float") {
                    if (floatRegex.test(event.target.value) || /^$/.test(event.target.value)) {
                        changeIndex.valueRange[0] = event.target.value
                    }
                }
                else if (changeIndex.valueType === "int") {
                    if (intRegex.test(event.target.value) || /^$/.test(event.target.value)) {
                        changeIndex.valueRange[0] = event.target.value
                    }
                }
            }
            else if (event.target.id.split("_")[0] === "to") {
                if (changeIndex.valueType === "float") {
                    if (floatRegex.test(event.target.value) || /^$/.test(event.target.value)) {
                        changeIndex.valueRange[1] = event.target.value
                    }
                }
                else if (changeIndex.valueType === "int") {
                    if (intRegex.test(event.target.value) || /^$/.test(event.target.value)) {
                        changeIndex.valueRange[1] = event.target.value
                    }
                }
            }
            else if (event.target.id.split("_")[0] === "step") {
                if (changeIndex.valueType === "float") {
                    if (floatRegex.test(event.target.value) || /^$/.test(event.target.value)) {
                        changeIndex.valueRange[2] = event.target.value
                    }
                }
                else if (changeIndex.valueType === "int") {
                    if (intRegex.test(event.target.value) || /^$/.test(event.target.value)) {
                        changeIndex.valueRange[2] = event.target.value
                    }
                }
            }
            else if (event.target.id.split("_")[0] === "speed") {
                if (/^[+]?((\.\d+)|(\d+(\.\d+)?))$/.test(event.target.value) || /^$/.test(event.target.value)) {
                    changeIndex.valueRange[4] = event.target.value
                }
            }
        }
        else if (event.target.name === "valueRange" && (event.target.id.split("_")[0] === "prefix" || event.target.id.split("_")[0] === "range")) {
            if (event.target.id.split("_")[0] === "prefix") {
                changeIndex.valueRange[0] = event.target.value
            }
            else {
                changeIndex.valueRange[1] = event.target.value
            }
        }
        else if (event.target.name.split("_")[0] === "valueRange" && (event.target.id.split("_")[0] === "true" || event.target.id.split("_")[0] === "false")) {
            changeIndex.valueRange = []
            changeIndex.valueRange.push(event.target.value)
        }
        else if (event.target.name === "valueType") {
            if (this.checkValueTypeOfKey(changeIndex.currentValue, changeIndex.name) !== event.target.value && event.target.value !== "gps") {
                return this.setState({
                    isOpen: true,
                    modalType: "error",
                    message2: "Choose right data type"
                })
            }
            if (changeIndex.rangeType === "control") {
                let commandConfigKeysArray = Object.keys(payload.masterConfig[masterIndex].ConfigData[configKey].commandConfig),
                    lambdaFunctionChangeIndex = [];
                for (let k = 0; k < commandConfigKeysArray.length; k++) {
                    let commandConfigKey = commandConfigKeysArray[k],
                        config = payload.masterConfig[masterIndex].ConfigData[configMapKey].commandConfig[commandConfigKey];
                    if (config.rangeType === "lambdaFunction") {
                        lambdaFunctionChangeIndex.push({
                            masterIndex: masterIndex,
                            configKey: configKey,
                            commandConfigKey: "default",
                            actionId: config.lambdaFunctionData.actionId,
                            isValueTypeChange: true,
                            valueIfActionDeleted: event.currentTarget.value
                        })
                    }
                }
                if (lambdaFunctionChangeIndex.length) {
                    this.setState({
                        confirmState: true,
                        lambdaFunctionChangeIndex
                    })
                    return;
                }
            }

            if (changeIndex.rangeType === "lambdaFunction") {
                this.setState({
                    isOpen: true,
                    modalType: "error",
                    message2: "Please change value type from Lambda Function"
                })
                return;
            }

            for (let i = 0; i < payload.masterConfig.length; i++) {
                let master = payload.masterConfig[i],
                    configDataKeysArray = Object.keys(master.ConfigData);
                for (let j = 0; j < configDataKeysArray.length; j++) {
                    let configMapKey = configDataKeysArray[j],
                        commandConfigKeysArray = Object.keys(master.ConfigData[configMapKey].commandConfig)
                    if (configMapKey === configKey) {
                        for (let k = 0; k < commandConfigKeysArray.length; k++) {
                            let commandConfigKey = commandConfigKeysArray[k],
                                config = master.ConfigData[configMapKey].commandConfig[commandConfigKey];
                            config.valueType = event.target.value;
                            config.valueDistribution = {};
                            config.lambdaFunctionData = {};
                            if (config.valueType === "int" || config.valueType === "float") {
                                config.valueRange[0] = ""
                                config.valueRange[1] = ""
                                config.valueRange[2] = ""
                            }
                            else if (config.valueType === "time") {
                                config.valueRange = [""]
                                config.rangeType = "linear"
                            }
                            else {
                                config.valueRange = [""]
                            }
                        }
                    }
                }
            }
        }
        else if (event.target.name === "rangeType") {
            if (changeIndex.rangeType === "control") {
                for (let i = 0; i < payload.masterConfig.length; i++) {
                    let master = payload.masterConfig[i],
                        configDataKeysArray = Object.keys(master.ConfigData),
                        lambdaFunctionChangeIndex = [];
                    if (masterIndex === i) {
                        for (let j = 0; j < configDataKeysArray.length; j++) {
                            let configMapKey = configDataKeysArray[j],
                                commandConfigKeysArray = Object.keys(master.ConfigData[configMapKey].commandConfig)
                            if (configMapKey === configKey) {
                                for (let k = 0; k < commandConfigKeysArray.length; k++) {
                                    let commandConfigKey = commandConfigKeysArray[k],
                                        config = master.ConfigData[configMapKey].commandConfig[commandConfigKey];
                                    if (config.rangeType === "lambdaFunction" && config.lambdaFunctionData.actionId) {
                                        lambdaFunctionChangeIndex.push({
                                            masterIndex: masterIndex,
                                            configKey: configKey,
                                            commandConfigKey: "default",
                                            valueIfActionDeleted: event.currentTarget.value,
                                            isValueTypeChange: false,
                                            actionId: config.lambdaFunctionData.actionId
                                        })
                                    }
                                }
                                if (lambdaFunctionChangeIndex.length) {
                                    this.setState({
                                        confirmState: true,
                                        lambdaFunctionChangeIndex,
                                        controlIndex: this.state.controlIndex === configKey ? '' : this.state.controlIndex
                                    })
                                    return;
                                }
                                else {
                                    payload.masterConfig[masterIndex].ConfigData[configKey].commandConfig = {
                                        default: payload.masterConfig[masterIndex].ConfigData[configKey].commandConfig["default"]
                                    }
                                    payload.masterConfig[masterIndex].ConfigData[configKey].command = "default"
                                    this.setState({
                                        controlIndex: this.state.controlIndex === configKey ? '' : this.state.controlIndex
                                    })
                                }
                            }
                        }
                    }
                }
            }

            if (changeIndex.rangeType === "lambdaFunction" && changeIndex.lambdaFunctionData.actionId) {
                let lambdaFunctionChangeIndex = []
                lambdaFunctionChangeIndex.push({
                    masterIndex: masterIndex,
                    configKey: configKey,
                    commandConfigKey: commandConfigKey,
                    valueIfActionDeleted: event.currentTarget.value,
                    isValueTypeChange: false,
                    actionId: changeIndex.lambdaFunctionData.actionId
                })

                this.setState({
                    confirmState: true,
                    lambdaFunctionChangeIndex
                })
                return;
            }
            changeIndex[event.target.name] = event.target.value;
            changeIndex.valueRange = changeIndex.valueType === "string" ? [] : ["", "", ""];
            changeIndex.valueDistribution = {}
            if (changeIndex.valueType === "boolean") {
                if (event.target.value === "static") {
                    changeIndex.valueRange = ["true"]
                }
                else if (event.target.value !== "lambdaFunction") {
                    changeIndex.valueRange = ["true", "false"]
                }
            }
            else if (changeIndex.valueType === "int") {
                if (changeIndex.rangeType === "randomOnce" || changeIndex.rangeType === "randomAlways" || changeIndex.rangeType === "linear") {
                    let from = (parseInt(0.8 * changeIndex.currentValue)).toString(),
                        to = (parseInt(1.2 * changeIndex.currentValue)).toString(),
                        step = "1";
                    if (Math.sign(changeIndex.currentValue) == -1) {
                        from = (parseInt(1.2 * changeIndex.currentValue)).toString();
                        to = (parseInt(0.8 * changeIndex.currentValue)).toString();
                        step = "1";
                    }
                    else if (changeIndex.currentValue == 0) {
                        from = "0";
                        to = "1";
                        step = "1";
                    }
                    changeIndex.valueRange = [from, to, step]
                }
                else if (changeIndex.rangeType === "static") {
                    changeIndex.valueRange = [(changeIndex.currentValue).toString()]

                }
            }
            else if (changeIndex.valueType === "float") {
                let decimalCount = countDecimals(changeIndex.currentValue)
                if (changeIndex.rangeType === "randomOnce" || changeIndex.rangeType === "randomAlways" || changeIndex.rangeType === "linear") {
                    let from = ((parseFloat(0.8 * changeIndex.currentValue)).toFixed(decimalCount).toString()),
                        to = ((parseFloat(1.2 * changeIndex.currentValue)).toFixed(decimalCount).toString()),
                        step = (1 / Math.pow(10, decimalCount)).toFixed(decimalCount).toString();
                    if (Math.sign(changeIndex.currentValue) == -1) {
                        from = ((parseFloat(1.2 * changeIndex.currentValue)).toFixed(decimalCount).toString());
                        to = ((parseFloat(0.8 * changeIndex.currentValue)).toFixed(decimalCount).toString());
                        step = (1 / Math.pow(10, decimalCount)).toFixed(decimalCount).toString();
                    }
                    else if (changeIndex.currentValue == 0) {
                        from = "0";
                        to = "1";
                        step = "0.1";
                    }
                    changeIndex.valueRange = [from, to, step]
                }
                else if (changeIndex.rangeType === "static") {
                    changeIndex.valueRange = [(changeIndex.currentValue).toString()]
                }
            }

            if (event.target.value === "control") {
                controlForm[`name_${masterIndex}_${configKey}_${commandConfigKey}`] = ""
            }
        }
        else if (event.currentTarget.name === "isCommand") {
            if (event.currentTarget.checked) {
                payload.masterConfig[masterIndex].ConfigData[configKey].command = commandConfigKey
            }
            else {
                payload.masterConfig[masterIndex].ConfigData[configKey].command = "default"
            }
        }
        else
            changeIndex[event.target.name] = event.target.value;
        this.setState({
            payload,
            configErrorCode: 0,
            configErrorKeys: [],
            controlForm
        })
    }

    inputTagsChangeHandler = (masterIndex, configKey, commandConfigKey) => (tags) => {
        let payload = cloneDeep(this.state.payload);
        if ((payload.masterConfig[masterIndex].ConfigData[configKey].commandConfig[commandConfigKey].rangeType === "static") && tags.length > 1) {
            this.setState({
                isOpen: true,
                modalType: "error",
                message2: "only one tag allowed",
            })
        }
        else {
            payload.masterConfig[masterIndex].ConfigData[configKey].commandConfig[commandConfigKey].valueRange = tags.map(el => el.replace(/[\r\n	]+/gm, ""))
            this.setState({
                payload,
                configErrorCode: 0,
            })
        }
    }

    getEquationOutput = (data) => {
        let a = data.equationParams.a,
            b = data.equationParams.b,
            c = data.equationParams.c,
            d = data.equationParams.d,
            equation;

        let aString = (a !== 0 ? (a !== 1 ? a < 0 ? a === -1 ? "- " : `- ${Math.abs(a)}` : `${a}` : '') + "t<sup>3</sup>" : '');
        let bString = (b !== 0 ? (b !== 1 ? b < 0 ? b === -1 ? "- " : `- ${Math.abs(b)}` : `${a === 0 ? '' : " + "}${b}` : a === 0 ? '' : " + ") + "t<sup>2</sup>" : '');
        let cString = (c !== 0 ? (c !== 1 ? c < 0 ? c === -1 ? "- " : `- ${Math.abs(c)}` : `${a === 0 && b === 0 ? '' : " + "}${c}` : a === 0 && b === 0 ? '' : " + ") + "t" : '');
        let dString = d !== 0 ? d < 0 ? ` - ${Math.abs(d)}` : `${a === 0 && b === 0 && c === 0 ? '' : " + "}${d}` : ''
        a === 0 && b === 0 && c === 0 && d === 0 ? equation = "0" : equation = aString.concat(bString, cString, dString)
        data.equationType === "sineqn" ? equation = "f(t) = a * Sin(b * t + c) + d" : ''
        return { __html: equation };
    }

    formatSwitcher = (configKey, masterIndex, config, commandConfigKey) => {
        switch (config.valueType) {
            case "int":
                if (config.rangeType === "static") {
                    return (
                        <div className="position-relative">
                            <label className="form-group-label form-group-label-top">Value :</label>
                            <input type="number" id={`from_${masterIndex}_${configKey}_${commandConfigKey}`} name="valueRange" disabled={this.state.isViewOnly} required className="form-control" onChange={(e) => this.simulationDataChangeHandler(e, configKey, masterIndex, commandConfigKey)} value={config.valueRange[0]} />
                        </div>
                    );
                }
                else if (config.rangeType === "lambdaFunction") {
                    return (
                        <button type='button' className="btn-link" disabled={this.state.isViewOnly} onClick={(e) => this.openFunctionModal(configKey, masterIndex, commandConfigKey)}><span className="button-icon"><i className="far fa-plus"></i></span>{Object.keys(config.lambdaFunctionData).length ? "Update" : 'Add'} Function</button>
                    );
                }
                else if (config.rangeType === "control") {
                    return (
                        <button type="button" className="btn-link" disabled={this.state.isViewOnly} onClick={(e) => this.addEditControlConfigHandler(e, configKey, masterIndex, commandConfigKey, "add")}><span className="button-icon"><i className="far fa-plus"></i></span>Add Command</button>
                    );
                }
                else
                    return (
                        <div className="d-flex">
                            <div className="flex-33 pd-r-10">
                                <div className="position-relative">
                                    <label className="form-group-label form-group-label-top">From :</label>
                                    <input type="number" id={`from_${masterIndex}_${configKey}_${commandConfigKey}`} name="valueRange" required disabled={this.state.isViewOnly} className="form-control" onChange={(e) => this.simulationDataChangeHandler(e, configKey, masterIndex, commandConfigKey)} value={config.valueRange[0]} />
                                </div>
                            </div>
                            <div className="flex-33 pd-l-5 pd-r-5">
                                <div className="position-relative">
                                    <label className="form-group-label form-group-label-top">To :</label>
                                    <input type="number" id={`to_${masterIndex}_${configKey}_${commandConfigKey}`} name="valueRange" required className="form-control" disabled={this.state.isViewOnly} onChange={(e) => this.simulationDataChangeHandler(e, configKey, masterIndex, commandConfigKey)} value={config.valueRange[1]} />
                                </div>
                            </div>
                            <div className="flex-33 pd-l-10">
                                <div className="position-relative">
                                    <label className="form-group-label form-group-label-top">Step Size :</label>
                                    <input type="number" id={`step_${masterIndex}_${configKey}_${commandConfigKey}`} name="valueRange" required className="form-control" disabled={this.state.isViewOnly} onChange={(e) => this.simulationDataChangeHandler(e, configKey, masterIndex, commandConfigKey)} value={config.valueRange[2]} />
                                </div>
                            </div>
                        </div>
                    );
            case "float": {
                if (config.rangeType === "variable") {
                    let dataIndex = config.valueDistribution
                    if (Object.keys(dataIndex).length) {
                        return (
                            <p className="variableFunction" onClick={(e) => this.state.isViewOnly ? false : this.openEquationModalHandler(configKey, masterIndex, commandConfigKey)} dangerouslySetInnerHTML={this.getEquationOutput(dataIndex)}></p>
                        )
                    }
                    else {
                        return (
                            <button type='button' className="btn-link" disabled={this.state.isViewOnly} onClick={(e) => this.openEquationModalHandler(configKey, masterIndex, commandConfigKey)}><span className="button-icon"><i className="far fa-plus"></i></span>Add Equation</button>
                        );
                    }
                }
                else if (config.rangeType === "static") {
                    return (
                        <div className="position-relative">
                            <label className="form-group-label form-group-label-top">Value :</label>
                            <input type="number" id={`from_${masterIndex}_${configKey}_${commandConfigKey}`} name="valueRange" disabled={this.state.isViewOnly} required className="form-control" onChange={(e) => this.simulationDataChangeHandler(e, configKey, masterIndex, commandConfigKey)} value={config.valueRange[0]} />
                        </div>
                    );
                }
                else if (config.rangeType === "control") {
                    return (
                        <div className="d-flex">
                            <button type="button" className="btn-link" disabled={this.state.isViewOnly} onClick={(e) => this.addEditControlConfigHandler(e, configKey, masterIndex, commandConfigKey, "add")}><i className="far fa-plus"></i>Add Command</button>
                        </div>
                    );
                }
                else if (config.rangeType === "lambdaFunction") {
                    return (
                        <button type='button' className="btn-link" disabled={this.state.isViewOnly} onClick={(e) => this.openFunctionModal(configKey, masterIndex, commandConfigKey)}><i className="far fa-plus"></i>{Object.keys(config.lambdaFunctionData).length ? "Update" : 'Add'} Function</button>
                    );
                }
                else {
                    return (
                        <div className="d-flex">
                            <div className="flex-33 pd-r-10">
                                <div className="position-relative">
                                    <label className="form-group-label form-group-label-top">From :</label>
                                    <input type="number" id={`from_${masterIndex}_${configKey}_${commandConfigKey}`} disabled={this.state.isViewOnly} name="valueRange" required className="form-control" onChange={(e) => this.simulationDataChangeHandler(e, configKey, masterIndex, commandConfigKey)} value={config.valueRange[0]} />
                                </div>
                            </div>
                            <div className="flex-33 pd-l-5 pd-r-5">
                                <div className="position-relative">
                                    <label className="form-group-label form-group-label-top">To :</label>
                                    <input type="number" id={`to_${masterIndex}_${configKey}_${commandConfigKey}`} disabled={this.state.isViewOnly} name="valueRange" required className="form-control" onChange={(e) => this.simulationDataChangeHandler(e, configKey, masterIndex, commandConfigKey)} value={config.valueRange[1]} />
                                </div>
                            </div>
                            <div className="flex-33 pd-l-10">
                                <div className="position-relative">
                                    <label className="form-group-label form-group-label-top">Step Size :</label>
                                    <input type="number" id={`step_${masterIndex}_${configKey}_${commandConfigKey}`} disabled={this.state.isViewOnly} name="valueRange" required className="form-control" onChange={(e) => this.simulationDataChangeHandler(e, configKey, masterIndex, commandConfigKey)} value={config.valueRange[2]} />
                                </div>
                            </div>
                        </div>
                    );
                }
            }
            case "string":
                if (config.rangeType === "lambdaFunction") {
                    return (
                        <button type='button' className="btn-link" disabled={this.state.isViewOnly} onClick={(e) => this.openFunctionModal(configKey, masterIndex, commandConfigKey)}><i className="far fa-plus"></i>{Object.keys(config.lambdaFunctionData).length ? "Update" : 'Add'} Function</button>
                    );
                }
                else if (config.rangeType === "control") {
                    return (
                        <div className="d-flex">
                            <button type="button" className="btn-link" disabled={this.state.isViewOnly} onClick={(e) => this.addEditControlConfigHandler(e, configKey, masterIndex, commandConfigKey, "add")}><i className="far fa-plus"></i>Add Command</button>
                        </div>
                    );
                }
                else {
                    return (
                        <TagsInput value={config.valueRange} disabled={this.state.isViewOnly} inputProps={{ placeholder: config.rangeType === "static" ? '' : 'Add a tag' }} onChange={this.inputTagsChangeHandler(masterIndex, configKey, commandConfigKey)} />
                    );
                }
            case "time":
                return (
                    <div className="d-flex">
                        <input type="text" name="valueRange" placeholder="Time Stamp" className="form-control" value={"UTC"} onChange={(e) => this.simulationDataChangeHandler(e, configKey, masterIndex, commandConfigKey)} readOnly />
                    </div>
                );
            case "boolean":
                if (config.rangeType === "lambdaFunction") {
                    return (
                        <button type='button' className="btn-link" disabled={this.state.isViewOnly} onClick={(e) => this.openFunctionModal(configKey, masterIndex, commandConfigKey)}><i className="far fa-plus"></i>{Object.keys(config.lambdaFunctionData).length ? "Update" : 'Add'} Function</button>
                    );
                }
                else {
                    return (
                        <div className="d-flex">
                            <div className="flex-33 pd-r-10">
                                <label className={config.rangeType === "static" ? `radio-button ${this.state.isViewOnly ? 'radio-button-disabled' : ''}` : `check-box ${this.state.isViewOnly ? 'check-box-disabled' : ''}`}>
                                    <span className={config.rangeType === "static" ? "radio-button-text" : "check-text"}>True</span>
                                    <input type={config.rangeType === "static" ? "radio" : "checkbox"} id={`true_${masterIndex}_${configKey}_${commandConfigKey}`} value="true" name={`valueRange_${masterIndex}_${configKey}_${commandConfigKey}`} disabled={config.rangeType !== "static" || this.state.isViewOnly} checked={config.valueRange.includes("true")} onChange={(e) => this.simulationDataChangeHandler(e, configKey, masterIndex, commandConfigKey)} />
                                    <span className={config.rangeType === "static" ? "radio-button-mark" : "check-mark"}></span>
                                </label>
                            </div>
                            <div className="flex-33 pd-l-5 pd-r-5">
                                <label className={config.rangeType === "static" ? `radio-button ${this.state.isViewOnly ? 'radio-button-disabled' : ''}` : `check-box ${this.state.isViewOnly ? 'check-box-disabled' : ''}`}>
                                    <span className={config.rangeType === "static" ? "radio-button-text" : "check-text"}>False</span>
                                    <input type={config.rangeType === "static" ? "radio" : "checkbox"} id={`false_${masterIndex}_${configKey}_${commandConfigKey}`} value="false" name={`valueRange_${masterIndex}_${configKey}_${commandConfigKey}`} disabled={config.rangeType !== "static" || this.state.isViewOnly} checked={config.valueRange.includes("false")} onChange={(e) => this.simulationDataChangeHandler(e, configKey, masterIndex, commandConfigKey)} />
                                    <span className={config.rangeType === "static" ? "radio-button-mark" : "check-mark"}></span>
                                </label>
                            </div>
                        </div>
                    );
                }
            case "gps":
                return (
                    <div className="d-flex">
                        <div className="flex-33 pd-r-10">
                            <div className="position-relative">
                                <label className="form-group-label form-group-label-top">Start Location :</label>
                                {(config.valueRange[0] && config.valueRange[1]) ?
                                    <div className="list-view-icon list-view-icon-link" onClick={(e) => !this.state.isViewOnly && this.openLocationModalHandler(configKey, masterIndex, commandConfigKey, "startLocation")}>
                                        <img className="mr-t-10" src={`https://maps.googleapis.com/maps/api/staticmap?center=${config.startLocation}&zoom=4&size=128x40&key=AIzaSyDhe3XJyqFibPDk5tRdcAMeH09cv7Elc08`}></img>
                                    </div>
                                    :
                                    <button type='button' className="btn-link mr-t-10" disabled={this.state.isViewOnly} onClick={(e) => this.openLocationModalHandler(configKey, masterIndex, commandConfigKey, "startLocation")}>
                                        <span className="button-icon"><i className="far fa-plus"></i></span>Start Location
                                    </button>}
                            </div>
                        </div>
                        <div className="flex-33 pd-l-5 pd-r-5">
                            <div className="position-relative">
                                <label className="form-group-label form-group-label-top">End Location :</label>
                                {(config.valueRange[2] && config.valueRange[3]) ?
                                    <div className="list-view-icon list-view-icon-link" onClick={(e) => !this.state.isViewOnly && this.openLocationModalHandler(configKey, masterIndex, commandConfigKey, "endLocation")}>
                                        <img className="mr-t-10" src={`https://maps.googleapis.com/maps/api/staticmap?center=${config.endLocation}&zoom=4&size=128x40&key=AIzaSyDhe3XJyqFibPDk5tRdcAMeH09cv7Elc08`}></img>
                                    </div>
                                    :
                                    <button type='button' className="btn-link mr-t-10" disabled={this.state.isViewOnly} onClick={(e) => this.openLocationModalHandler(configKey, masterIndex, commandConfigKey, "endLocation")}>
                                        <span className="button-icon"><i className="far fa-plus"></i></span>End Location
                                    </button>}
                            </div>
                        </div>
                        <div className="flex-33 pd-l-10">
                            <div className="position-relative">
                                <label className="form-group-label form-group-label-top">Speed (km/hr) :</label>
                                <input type="number" id={`speed_${masterIndex}_${configKey}_${commandConfigKey}`} disabled={this.state.isViewOnly} name="valueRange" required className="form-control" onChange={(e) => this.simulationDataChangeHandler(e, configKey, masterIndex, commandConfigKey)} value={config.valueRange[4]} />
                            </div>
                        </div>
                    </div>
                );
            default:
                return '';
        }
    }

    onSubmitHandler = (event) => {
        event.preventDefault();
        let payload = cloneDeep(this.state.payload),
            configErrorCode = 0,
            configErrorKeys = [];
        if (this.state.selectedDeviceCount == 0) {
            return this.setState({
                isOpen: true,
                modalType: "error",
                message2: "Unable to create simulation with 0 devices.Please select Device Group with devices.",
            })
        }
        for (let i = 0; i < payload.masterConfig.length; i++) {
            let master = payload.masterConfig[i]
            if (master.StartTime === '' || master.EndTime === '') {
                configErrorCode = 4
                this.setState({
                    configErrorCode,
                    isOpen: true,
                    modalType: "error",
                    message2: "StartTime/EndTime Cannot be Empty",
                })
                break;
            }
            master.StartTime = parseInt(master.StartTime.split(':')[0])
            master.EndTime = parseInt(master.EndTime.split(':')[0])
            master.Alias = {}
            let configDataKeysArray = Object.keys(master.ConfigData)
            for (let j = 0; j < configDataKeysArray.length; j++) {
                let configMapKey = configDataKeysArray[j],
                    commandConfigKeysArray;
                if (master.ConfigData[configMapKey].commandConfig["default"].rangeType !== "control") {
                    master.ConfigData[configMapKey].commandConfig = {
                        default: master.ConfigData[configMapKey].commandConfig["default"]
                    }
                }
                commandConfigKeysArray = Object.keys(master.ConfigData[configMapKey].commandConfig)
                for (let k = 0; k < commandConfigKeysArray.length; k++) {
                    let commandConfigKey = commandConfigKeysArray[k],
                        config = master.ConfigData[configMapKey].commandConfig[commandConfigKey];
                    if (config.valueType === "time") {
                        let value = "UTC";
                        config.valueRange = [];
                        config.valueRange.push(value);
                    }
                    if (typeof config.valueRange == "string") {
                        let value = config.valueRange;
                        config.valueRange = [];
                        config.valueRange.push(value);
                    }
                    if (config.valueType !== "float") {
                        config.valueDistribution = {}
                    }
                    if (config.valueType == "gps") {
                        if (config.valueRange.some(el => el == "" || typeof el != "string")) {
                            configErrorCode = 7
                            configErrorKeys.push(configMapKey)
                            this.setState({
                                configErrorCode,
                                configErrorKeys,
                                isOpen: true,
                                modalType: "error",
                                message2: "Please enter Start/End Location Coordinates !",
                            })
                            break;
                        }
                    }
                    if (config.valueType == "int" && config.rangeType !== "control") {
                        if (parseInt(config.valueRange[1]) < parseInt(config.valueRange[0]) && parseInt(config.valueRange[2]) > 0) {
                            configErrorCode = 1
                            configErrorKeys.push(configMapKey)
                            this.setState({
                                configErrorCode,
                                configErrorKeys,
                                isOpen: true,
                                modalType: "error",
                                message2: "From Value is greater than To value and Step is positive",
                            })
                            break;
                        }
                        if (parseInt(config.valueRange[1]) > parseInt(config.valueRange[0]) && parseInt(config.valueRange[2]) < 0) {
                            configErrorCode = 1
                            configErrorKeys.push(configMapKey)
                            this.setState({
                                configErrorCode,
                                configErrorKeys,
                                isOpen: true,
                                modalType: "error",
                                message2: "From Value is less than To value and Step is negative",
                            })
                            break;
                        }
                    }
                    if (config.valueType == "float" && config.rangeType !== "control") {
                        if (parseFloat(config.valueRange[1]) < parseFloat(config.valueRange[0]) && parseInt(config.valueRange[2]) > 0) {
                            configErrorCode = 1
                            configErrorKeys.push(configMapKey)
                            this.setState({
                                configErrorCode,
                                configErrorKeys,
                                isOpen: true,
                                modalType: "error",
                                message2: "From Value is greater than To value and Step is positive",
                            })
                            break;
                        }
                        if (parseInt(config.valueRange[1]) > parseInt(config.valueRange[0]) && parseInt(config.valueRange[2]) < 0) {
                            configErrorCode = 1
                            configErrorKeys.push(configMapKey)
                            this.setState({
                                configErrorCode,
                                configErrorKeys,
                                isOpen: true,
                                modalType: "error",
                                message2: "From Value is less than To value and Step is negative",
                            })
                            break;
                        }
                    }
                    if (config.valueType === "int" && (config.rangeType === "linear" || config.rangeType === "randomAlways" || config.rangeType === "randomOnce")) {
                        if (config.valueRange[0] === config.valueRange[1]) {
                            configErrorCode = 1
                            configErrorKeys.push(configMapKey)
                            this.setState({
                                configErrorCode,
                                configErrorKeys,
                                isOpen: true,
                                modalType: "error",
                                message2: "From/To cannot have same value",
                            })
                            break;
                        }
                        if (config.valueRange[0] === '' || config.valueRange[1] === '' || config.valueRange === '') {
                            configErrorCode = 2
                            configErrorKeys.push(configMapKey)
                            this.setState({
                                configErrorCode,
                                configErrorKeys,
                                isOpen: true,
                                modalType: "error",
                                message2: "From/To/StepSize cannot be empty",
                            })
                            break;
                        }
                    }
                    if ((config.valueType === "int" || config.valueType === "float") && (config.rangeType === "static")) {
                        config.valueDistribution = {}
                        if (config.valueRange[0] === '') {
                            configErrorKeys.push(configMapKey)
                            configErrorCode = 2
                            this.setState({
                                configErrorCode,
                                configErrorKeys,
                                isOpen: true,
                                modalType: "error",
                                message2: "Value cannot be empty",
                            })
                            break;
                        }
                    }

                    if (config.valueType === "float" && (config.rangeType === "linear" || config.rangeType === "randomAlways" || config.rangeType === "randomOnce")) {
                        config.valueDistribution = {}
                        if (config.valueRange[0] === '' || config.valueRange[1] === '' || config.valueRange[2] === '') {
                            configErrorCode = 2
                            configErrorKeys.push(configMapKey)
                            this.setState({
                                configErrorCode,
                                configErrorKeys,
                                isOpen: true,
                                modalType: "error",
                                message2: "From/To/StepSize cannot be empty",
                            })
                            break;
                        }
                        if (config.valueRange[0] === config.valueRange[1]) {
                            configErrorCode = 1
                            configErrorKeys.push(configMapKey)
                            this.setState({
                                configErrorCode,
                                configErrorKeys,
                                isOpen: true,
                                modalType: "error",
                                message2: "From/To cannot have same value",
                            })
                            break;
                        }
                    }

                    if (config.valueType === "float" && config.rangeType === "variable") {
                        if (Object.keys(config.valueDistribution).length === 0) {
                            configErrorCode = 5
                            configErrorKeys.push(configMapKey)
                            this.setState({
                                configErrorCode,
                                configErrorKeys,
                                isOpen: true,
                                modalType: "error",
                                message2: "Equation cannot be empty",
                            })
                            break;
                        }
                    }
                    if ((config.valueType === "float" || config.valueType === "int" || config.valueType === "string") && config.rangeType === "lambdaFunction") {
                        if (Object.keys(config.lambdaFunctionData).length === 0) {
                            configErrorCode = 5
                            configErrorKeys.push(configMapKey)
                            this.setState({
                                configErrorCode,
                                configErrorKeys,
                                isOpen: true,
                                modalType: "error",
                                message2: "Function cannot be empty",
                            })
                            break;
                        }
                    }

                    if ((config.rangeType !== "variable" && config.rangeType !== "lambdaFunction" && config.rangeType !== "control") && config.valueRange.length === 0) {
                        configErrorCode = 6
                        configErrorKeys.push(configMapKey)
                        this.setState({
                            configErrorCode,
                            configErrorKeys,
                            isOpen: true,
                            modalType: "error",
                            message2: "Values cannot be empty",
                        })
                        break;
                    }

                    if (config.rangeType === "control") {
                        if (Object.keys(master.ConfigData[configMapKey].commandConfig).length < 2) {
                            configErrorCode = 3
                            configErrorKeys.push(configMapKey)
                            this.setState({
                                configErrorCode,
                                configErrorKeys,
                                isOpen: true,
                                modalType: "error",
                                message2: "Attribute with control type should have list of controls",
                            })
                            break;
                        }
                        if (master.ConfigData[configMapKey].command === "default") {
                            configErrorKeys.push(configMapKey)
                            configErrorCode = 3
                            this.setState({
                                configErrorCode,
                                configErrorKeys,
                                isOpen: true,
                                modalType: "error",
                                message2: "Please choose atleast one control ",
                            })
                            break;
                        }
                    }
                    config.isEditMode ? config.isEditMode = false : ''
                }
                master.Alias[btoa(master.ConfigData[configMapKey].commandConfig["default"].name)] = configMapKey
            }
        }

        if (configErrorCode === 0 && configErrorKeys.length == 0) {
            if (payload.isExternal === false) {
                delete payload.mqttData.user
                delete payload.mqttData.password
                delete payload.mqttData.host
                delete payload.mqttData.isAuthenticated
            }
            if (payload.mqttData.isAuthenticated === false) {
                delete payload.mqttData.user
                delete payload.mqttData.password
            }
            // payload.mqttData = payload.mqttData
            // delete payload.mqttData
            payload.mqttData.topic = payload.mqttData.topic.split('/')[0];
            this.setState({
                isFetching: true
            }, () => this.createTemplate(payload))
        }
    }

    createTemplate = (payload) => {
        let Json = flatten(JSON.parse(payload.inputJson));
        if (this.state.addGeoLocation) { //if geolocation is true add to json so that templateData can be formed
            Json.geoLocation = 12.1234
        }
        payload.templateData = cloneDeep(Json);
        let keysArray = Object.keys(payload.templateData);
        for (let i = 0; i < keysArray.length; i++) {
            let key = `Id${i}`;
            if (payload.masterConfig[0].ConfigData[key].commandConfig["default"].valueType == "float" ||
                payload.masterConfig[0].ConfigData[key].commandConfig["default"].valueType == "boolean" ||
                payload.masterConfig[0].ConfigData[key].commandConfig["default"].valueType == "time" ||
                payload.masterConfig[0].ConfigData[key].commandConfig["default"].valueType == "gps" ||
                payload.masterConfig[0].ConfigData[key].commandConfig["default"].valueType === "int") {
                payload.templateData[keysArray[i]] = `<< index .${key}>>`
            } else {
                payload.templateData[keysArray[i]] = `{{index .${key}}}`
            }
        }
        payload.templateData = unflatten(payload.templateData);
        payload.templateData = JSON.stringify(payload.templateData);
        payload.templateData = payload.templateData.replace(new RegExp("\"<<", 'g'), "{{");
        payload.templateData = payload.templateData.replace(new RegExp(">>\"", 'g'), "}}");
        payload.templateData = btoa(payload.templateData)
        this.props.saveSimulation(payload)
    }

    connectorDataChangeHandler = (event, index) => {
        let payload = cloneDeep(this.state.payload),
            listOfDeviceGroupsForSelectedConnector = cloneDeep(this.state.listOfDeviceGroupsForSelectedConnector),
            selectedConnector = this.state.connectorsList.find(el => el.id === event.currentTarget.value);
        payload.mqttData[event.currentTarget.id] = event.currentTarget.value
        payload.mqttData.topic = ""
        payload.deviceGroupIds = []
        payload.loadIntervalSecs = 10;
        payload.deviceTypeId = event.currentTarget.value;
        if (selectedConnector) {
            payload.loadIntervalSecs = 10;
            payload.mqttData.topic = selectedConnector.topic
            payload.loadIntervalSecs = selectedConnector.reportInterval;
            listOfDeviceGroupsForSelectedConnector = selectedConnector.deviceGroups
        }
        payload.masterConfig = [{
            StartTime: '00:00',
            EndTime: '24:00',
            ConfigData: {}
        }]
        payload.inputJson = "";
        this.setState({
            payload,
            selectedDeviceCount: 0,
            listOfDeviceGroupsForSelectedConnector,
            selectedConnector,
            selectedJson: '',
            errorMessage: '',
        })
    }

    handleDeviceGroupChange = (value) => {
        let payload = cloneDeep(this.state.payload),
            selectedDeviceCount = 0;
        payload.deviceGroupIds = value.map(el => el.value);
        this.state.listOfDeviceGroupsForSelectedConnector.map(group => {
            if (payload.deviceGroupIds.includes(group.deviceGroupId)) {
                selectedDeviceCount += group.deviceCount
            }
        })
        this.setState({
            payload,
            selectedDeviceCount
        })
    }

    openEquationModalHandler = (configKey, masterIndex, commandConfigKey) => {
        let payload = cloneDeep(this.state.payload),
            modalPayload = {
                "valueError": {
                    "nominalError": 1,
                    "abnormalError": 1
                },
                "equationParams": {
                    "a": 0,
                    "b": 0,
                    "c": 0,
                    "d": 0
                },
                "aMax": 10,
                "bMax": 10,
                "cMax": 10,
                "dMax": 10,
                "normalityProbabilityWeights": 0.1,
                "equationType": "cubiceqn"
            };

        if (Object.keys(payload.masterConfig[masterIndex].ConfigData[configKey].commandConfig[commandConfigKey].valueDistribution).length !== 0) {
            modalPayload = payload.masterConfig[masterIndex].ConfigData[configKey].commandConfig[commandConfigKey].valueDistribution
        }
        this.setState({
            payload,
            modalPayload,
            variableModalConfigKey: configKey,
            variableModalCommandConfigKey: commandConfigKey,
            masterIndex,
            editEquationMode: false
        }, () => $('#simulationModal').modal('show'))
    }

    variableDistributionEditHandler = (event) => {
        let modalPayload = JSON.parse(JSON.stringify(this.state.modalPayload)),
            editEquationMode = this.state.editEquationMode,
            modalErrorMessage = '';

        if (event.target.id === "nominalError" || event.target.id === "abnormalError") {
            modalPayload.valueError[event.target.id] = event.target.value
        }
        else if (event.target.id === "aMax" || event.target.id === "bMax" || event.target.id === "cMax" || event.target.id === "dMax") {
            modalPayload[event.target.id] = event.target.value ? parseInt(event.target.value) : 0
        }
        else if (event.target.name === "equationType") {
            modalPayload[event.target.name] = event.target.value
            modalPayload.equationParams = {
                "a": 0,
                "b": 0,
                "c": 0,
                "d": 0
            }
            editEquationMode = false
        }
        else {
            modalPayload[event.target.id] = event.target.value
        }
        this.setState({
            modalPayload,
            modalErrorMessage,
            editEquationMode
        })
    }

    valueDistributionSliderChangeHandler = (x, cubicEquationVariable) => {
        let modalPayload = JSON.parse(JSON.stringify(this.state.modalPayload)),
            modalErrorMessage = '';

        if (cubicEquationVariable === "a") {
            modalPayload.equationParams.a = x
        }
        else if (cubicEquationVariable === "b") {
            modalPayload.equationParams.b = x
        }
        else if (cubicEquationVariable === "c") {
            modalPayload.equationParams.c = x
        } else {
            modalPayload.equationParams.d = x
        }
        this.setState({
            modalPayload,
            modalErrorMessage
        })
    }

    createChartData = () => {
        let data = [];
        let constant = this.state.modalPayload.equationParams
        let timeIndex = this.state.payload.masterConfig[this.state.masterIndex]
        for (let i = parseInt(timeIndex.StartTime.split(':')[0]); i <= parseInt(timeIndex.EndTime.split(':')[0]); i++) {
            let dataObj = {
                x: i,
                y: this.solveCubicEquation(i, constant.a, constant.b, constant.c, constant.d)
            };
            data.push(dataObj);
        }
        data = data.map((el, index) => {
            el.category = el.x
            delete el.x;
            return el
        });
        let chartData = {
            chartData: data,
            availableAttributes: ["y"]
        }

        return chartData;
    }

    createChartConfig = () => {
        let config = {
            id: "lineChartDiv",
            chartType: "lineChart",
            chartSubType: "simpleLineChart",
            chartData: this.createChartData()
        }
        return config
    }

    solveCubicEquation = (x, a, b, c, d) => {
        let y = (a * (x * x * x)) + (b * (x * x)) + (c * x) + d;
        return y;
    }

    modalStateHandler = (mode, modalId) => {
        let payload = JSON.parse(JSON.stringify(this.state.payload)),
            modalPayload = JSON.parse(JSON.stringify(this.state.modalPayload)),
            modalErrorMessage = "";

        if (mode === "save") {
            if ((modalPayload.normalityProbabilityWeights < 0 || modalPayload.normalityProbabilityWeights > 1)) {
                modalErrorMessage = "Normal Probability Weight cannot be less than 0 or greater than 1"
                this.setState({ modalErrorMessage })
                return
            }
            payload.masterConfig[this.state.masterIndex].ConfigData[this.state.variableModalConfigKey].commandConfig[this.state.variableModalCommandConfigKey].valueDistribution = modalPayload
        }
        this.setState({
            payload,
            modalPayload: {},
            masterIndex: -1,
            variableModalConfigKey: '',
            variableModalCommandConfigKey: '',
            modalErrorMessage: '',
            functionModalConfigKey: '',
            functionModalCommandConfigKey: ''
        }, () => $(modalId).modal('hide'))
    }

    addDeleteSimulation = (event, mode, masterIndex) => {
        let payload = JSON.parse(JSON.stringify(this.state.payload)),
            selectedMasterIndex = this.state.selectedMasterIndex;
        event.stopPropagation();
        if (mode === "add") {
            payload.masterConfig.push({
                StartTime: '',
                EndTime: '',
                ConfigData: payload.masterConfig[0].ConfigData
            })
        }
        else {
            payload.masterConfig.splice(masterIndex, 1)
            selectedMasterIndex = selectedMasterIndex !== masterIndex ? selectedMasterIndex < payload.masterConfig.length ? selectedMasterIndex : payload.masterConfig.length - 1 : masterIndex === 0 ? 0 : masterIndex - 1
        }
        this.setState({
            payload,
            selectedMasterIndex
        })
    }

    generateHours = () => {
        let startingHours = [],
            endingHours = [];
        for (let i = 0; i <= 23; i++) {
            startingHours.push({
                time: `${i <= 9 ? '0' + i : i}:00`,
                selected: true,
                disabled: false,
                currentSimulationIndex: 0
            })
        }
        for (let i = 1; i <= 24; i++) {
            endingHours.push({
                time: `${i <= 9 ? '0' + i : i}:${'00'}`,
                selected: true,
                disabled: false,
                currentSimulationIndex: 0
            })
        }
        this.setState({
            startingHours,
            endingHours
        })
    }

    tabChangeHandler = (selectedMasterIndex) => {
        let endingHours = JSON.parse(JSON.stringify(this.state.endingHours)),
            payload = JSON.parse(JSON.stringify(this.state.payload));
        for (let i = 0; i < endingHours.length; i++) {
            endingHours[i].disabled = true
            if (endingHours[i].currentSimulationIndex === selectedMasterIndex) {
                endingHours[i].disabled = false
            }
            if (parseInt(endingHours[i].time.split(':')[0]) > parseInt(payload.masterConfig[selectedMasterIndex].StartTime)) {
                if (endingHours[i].currentSimulationIndex === -1) {
                    endingHours[i].disabled = false
                }
                if (endingHours[i].selected && endingHours[i].currentSimulationIndex !== selectedMasterIndex) {
                    break;
                }
            }
        }
        this.setState({
            selectedMasterIndex,
            endingHours
        })
    }

    timeChangeHandler = (e, masterIndex, type) => {
        let payload = JSON.parse(JSON.stringify(this.state.payload)),
            startingHours = JSON.parse(JSON.stringify(this.state.startingHours)),
            endingHours = JSON.parse(JSON.stringify(this.state.endingHours));
        if (type === "start") {
            payload.masterConfig[masterIndex].StartTime = event.target.value
            for (let i = 0; i < startingHours.length; i++) {
                if (startingHours[i].time === event.target.value) {
                    startingHours[i].selected = true
                    startingHours[i].currentSimulationIndex = masterIndex
                }
                if (startingHours[i].time !== event.target.value && startingHours[i].currentSimulationIndex === masterIndex) {
                    startingHours[i].selected = false
                    startingHours[i].currentSimulationIndex = -1
                }
            }
            for (let i = 0; i < endingHours.length; i++) {
                if (endingHours[i].currentSimulationIndex === masterIndex) {
                    endingHours[i].time = endingHours[i].time
                    endingHours[i].selected = false
                    endingHours[i].disabled = true
                    endingHours[i].currentSimulationIndex = -1
                }
                if (parseInt((payload.masterConfig[masterIndex].StartTime).split(':')[0]) < (i + 1)) {
                    if (endingHours[i].selected != true)
                        endingHours[i].disabled = false
                    else {
                        break;
                    }
                }
            }
            payload.masterConfig[masterIndex].EndTime = ''
        }
        else {
            payload.masterConfig[masterIndex].EndTime = event.target.value
            for (let i = 0; i < startingHours.length; i++) {
                if (parseInt((payload.masterConfig[masterIndex].StartTime).split(':')[0]) <= i &&
                    i < parseInt((payload.masterConfig[masterIndex].EndTime).split(':')[0])) {
                    startingHours[i].selected = true
                    startingHours[i].currentSimulationIndex = masterIndex
                }
                if (parseInt((payload.masterConfig[masterIndex].EndTime).split(':')[0]) - 1 < parseInt((startingHours[i].time).split(':')[0]) && startingHours[i].currentSimulationIndex === masterIndex) {
                    startingHours[i].selected = false
                    startingHours[i].currentSimulationIndex = -1
                }
            }
            for (let i = 0; i < endingHours.length; i++) {
                if (parseInt((payload.masterConfig[masterIndex].StartTime).split(':')[0]) < (i + 1) &&
                    (i + 1) <= parseInt((payload.masterConfig[masterIndex].EndTime).split(':')[0])) {
                    endingHours[i].selected = true
                    endingHours[i].currentSimulationIndex = masterIndex
                }
                if (parseInt((payload.masterConfig[masterIndex].EndTime).split(':')[0]) < parseInt((endingHours[i].time).split(':')[0]) && endingHours[i].currentSimulationIndex === masterIndex) {
                    endingHours[i].currentSimulationIndex = -1
                    endingHours[i].selected = false
                    endingHours[i].disabled = false
                }
            }
        }
        this.setState({
            startingHours,
            endingHours,
            payload
        })
    }

    openFunctionModal = (configKey, masterIndex, commandConfigKey) => {
        let payload = JSON.parse(JSON.stringify(this.state.payload)),
            modalPayload = {
                code: "import time \r\n\r\ndef main(args):                \r\n\tdata=time.time()               \r\n\treturn {'output':data}",
                kind: "python:3",
                imageName: "",
                requirement: [""],
                pathParameters: [],
                dependencies: []
            };
        if (Object.keys(payload.masterConfig[masterIndex].ConfigData[configKey].commandConfig[commandConfigKey].lambdaFunctionData).length !== 0) {
            modalPayload = payload.masterConfig[masterIndex].ConfigData[configKey].commandConfig[commandConfigKey].lambdaFunctionData
        }
        this.setState({
            payload,
            functionModalConfigKey: configKey,
            functionModalCommandConfigKey: commandConfigKey,
            masterIndex,
            modalPayload
        }, () => $('#functionModal').modal('show'))
    }

    functionModalCodeHandler = (code) => {
        let modalPayload = JSON.parse(JSON.stringify(this.state.modalPayload));
        modalPayload.code = code
        this.setState({
            modalPayload
        })
    }

    functionModalChangeHandler = (event) => {
        let modalPayload = JSON.parse(JSON.stringify(this.state.modalPayload));
        modalPayload[event.target.id] = event.target.value
        this.setState({
            modalPayload
        })
    }

    functionModalSaveHandler = (event) => {
        event.preventDefault();
        let modalPayload = JSON.parse(JSON.stringify(this.state.modalPayload));
        this.props.saveLambdaFunction(modalPayload)
    }

    pathParamHandler = (index) => {
        let modalPayload = JSON.parse(JSON.stringify(this.state.modalPayload));
        let selectedPathParam = JSON.parse(JSON.stringify(this.state.selectedPathParam));
        $('.paramFormBox').toggleClass('flipped');
        if (index >= 0) {
            selectedPathParam = this.state.modalPayload.pathParameters[index]
            selectedPathParam.required = selectedPathParam.required.toString()
        }
        else {
            selectedPathParam = {
                description: "",
                in: "query",
                name: "",
                required: "true",
                type: "Boolean",
                defaultValue: ""
            }
        }
        this.setState((preState, props) => ({
            selectedParamIndex: index >= 0 ? index : -1,
            isParamForm: !preState.isParamForm,
            selectedPathParam,
            modalPayload
        }))
    }

    onChangeHandlerPathParams = (event) => {
        let selectedPathParam = { ...this.state.selectedPathParam };
        let errorMessageCode;
        if (event.target.name === "required") {
            selectedPathParam[event.target.name] = event.target.value;
        } else {
            selectedPathParam[event.target.id] = event.target.value;
            if (event.target.id === "defaultValue") {
                if (!isNaN(Number(event.target.value))) {
                    selectedPathParam.type = (event.target.value).includes(".") ? "float" : "int"
                }
                else if (event.target.value.trim().toLowerCase() === "true" || event.target.value.trim().toLowerCase() === "false") {
                    selectedPathParam.type = "Boolean"
                }
                else if (event.target.value.trim().length === 1) {
                    selectedPathParam.type = "char"
                }
                else {
                    selectedPathParam.type = "String"
                }
            }
            if (this.state.errorMessageCode) {
                if (this.validateType(selectedPathParam.defaultValue, selectedPathParam.type) && selectedPathParam.name) {
                    errorMessageCode = 0
                }
                else if (event.target.id === "type" && this.validateType(selectedPathParam.defaultValue, selectedPathParam.type)) {
                    errorMessageCode = 1
                }
                else if (event.target.id === "name" && this.state.errorMessageCode === 1 && event.target.value) {
                    errorMessageCode = 2
                }
            }
        }
        this.setState({
            errorMessageCode,
            selectedPathParam
        })
    }

    validateType = (value, type) => {
        if (value.trim() === "") {
            return true
        }
        else {
            if (type === "int" && !isNaN(Number(value)) && Number(value) % 1 === 0) return true;
            else if (type === "float" && !isNaN(Number(value)) && value.includes(".")) return true;
            else if (type === "Boolean" && (value.trim().toLowerCase() === "true" || value.trim().toLowerCase() === "false")) return true;
            else if (type === "char" && isNaN(Number(value)) && value.trim().length === 1) return true;
            else if (type === "String" && isNaN(Number(value)) && value.trim().length > 1) return true;
            else return false
        }
    }

    flip = () => {
        $('.paramFormBox').toggleClass('flipped');
    }

    savePathParams = (event) => {
        event.preventDefault();
        let validation = this.validateType(this.state.selectedPathParam.defaultValue, this.state.selectedPathParam.type)
        if (this.state.selectedPathParam.name && this.state.selectedPathParam.in && this.state.selectedPathParam.type && validation) {
            let modalPayload = JSON.parse(JSON.stringify(this.state.modalPayload));
            if (this.state.selectedParamIndex >= 0) {
                modalPayload.pathParameters[this.state.selectedParamIndex] = this.state.selectedPathParam
            } else {
                modalPayload.pathParameters.push(this.state.selectedPathParam);
            }
            $('.paramFormBox').toggleClass('flipped');
            let selectedPathParam = {
                description: "",
                in: "query",
                name: "",
                required: "true",
                type: "",
                defaultValue: ""
            }
            this.setState({
                modalPayload,
                selectedPathParam,
                isParamForm: false,
                selectedParamIndex: -1,
                errorMessageCode: 0
            })
        }
        else {
            this.setState({ errorMessageCode: validation ? 1 : 2 })
        }
    }

    editEquationChangeHandler = (event) => {
        let editEquationForm = JSON.parse(JSON.stringify(this.state.editEquationForm)),
            modalPayload = JSON.parse(JSON.stringify(this.state.modalPayload));
        if (intRegex.test(event.target.value) || /^$/.test(event.target.value)) {
            editEquationForm[event.target.id] = parseInt(event.target.value) ? parseInt(event.target.value) : 0
        }
        if (modalPayload[`${event.target.id}Max`] < event.target.value || (- modalPayload[`${event.target.id}Max`] > event.target.value)) {
            modalPayload[`${event.target.id}Max`] = Math.abs(2 * event.target.value)
        }
        this.setState({
            modalPayload,
            editEquationForm
        })
    }

    cancelClicked = () => {
        this.setState({
            confirmState: false,
            lambdaFunctionChangeIndex: []
        })
    }

    controlFormHandler = (e, masterIndex, configKey, commandConfigKey) => {
        let controlForm = cloneDeep(this.state.controlForm)
        controlForm[`name_${masterIndex}_${configKey}_${commandConfigKey}`] = (e.currentTarget.value).replace(" ", "");
        this.setState({
            controlForm
        })
    }

    addEditControlConfigHandler = (e, configKey, masterIndex, commandConfigKey, type) => {
        let payload = cloneDeep(this.state.payload),
            controlForm = cloneDeep(this.state.controlForm),
            keyName = controlForm[`name_${masterIndex}_${configKey}_${commandConfigKey}`],
            defaultCommandConfig = payload.masterConfig[masterIndex].ConfigData[configKey].commandConfig["default"],
            keysArray = Object.keys(payload.masterConfig[masterIndex].ConfigData[configKey].commandConfig);

        if (type === "add") {
            keyName = `Default_Command`
            if (keysArray.find(el => el === keyName)) {
                this.setState({
                    isOpen: true,
                    modalType: "error",
                    message2: "Command name cannot be repeated. Please change the command name from Default_Command to desired name",
                })
                return;
            }
            payload.masterConfig[masterIndex].ConfigData[configKey].commandConfig[keyName] = cloneDeep(defaultCommandConfig)
            payload.masterConfig[masterIndex].ConfigData[configKey].commandConfig[keyName].rangeType = "static"
            payload.masterConfig[masterIndex].ConfigData[configKey].commandConfig[keyName].valueRange = []
            payload.masterConfig[masterIndex].ConfigData[configKey].commandConfig[keyName].valueRange.push(defaultCommandConfig.currentValue)
            if (Object.keys(payload.masterConfig[masterIndex].ConfigData[configKey].commandConfig).length === 2) {
                payload.masterConfig[masterIndex].ConfigData[configKey].command = keyName
            }
        } else {
            if ((!keyName) || keyName === "default") {
                this.setState({
                    isOpen: true,
                    modalType: "error",
                    message2: keyName === "default" ? "Command name cannot be default" : "Incorrect command name",
                })
                return;
            }
            if (keysArray.find(el => el === keyName) && keyName !== commandConfigKey) {
                this.setState({
                    isOpen: true,
                    modalType: "error",
                    message2: "Command name cannot be repeated",
                })
                return;
            }
            payload.masterConfig[masterIndex].ConfigData[configKey].commandConfig[keyName] = cloneDeep(payload.masterConfig[masterIndex].ConfigData[configKey].commandConfig[commandConfigKey])
            payload.masterConfig[masterIndex].ConfigData[configKey].commandConfig[keyName].isEditMode = false
            keyName === commandConfigKey ? null : delete payload.masterConfig[masterIndex].ConfigData[configKey].commandConfig[commandConfigKey];
            if (payload.masterConfig[masterIndex].ConfigData[configKey].command === commandConfigKey) {
                payload.masterConfig[masterIndex].ConfigData[configKey].command = keyName
            }
            controlForm[`name_${masterIndex}_${configKey}_${commandConfigKey}`] = ""
        }

        this.setState({
            controlForm,
            controlIndex: configKey,
            payload,
        })
    }

    addSampleJson = ({ currentTarget }) => {
        let payload = cloneDeep(this.state.payload),
            json = {};
        switch (currentTarget.value) {
            case 'energySensorJson':
                json = {
                    "sensorType": "demoSensor",
                    "timestamp": new Date().getTime(),
                    "readings": {
                        "current": 0.2,
                        "voltage": 230,
                        "resistance": 45
                    },
                    "status": false
                }
                break;
            case 'vechileObdJson':
                json = {
                    "engineRpm": 2000,
                    "vehicleSpeed": 120,
                    "runtimeSinceStart": 7200,
                    "noOfDtcs": 22,
                    "coolantTemperature": 45,
                    "averageFuelEconomy": 10,
                    "idlingDuration": 0,
                    "timestamp": new Date().getTime()
                }
                break;
            case 'waterSensorJson':
                json = {
                    "flowRate": 28,
                    "liquidTemperature": 23,
                    "timestamp": new Date().getTime()
                }
                break;
            case 'environmentalSensorJson':
                json = {
                    "sensors": {
                        "bme680": {
                            "temperature": 32.25,
                            "pressure": 972.57,
                            "humidity": 48.15,
                            "gas": 276.97
                        },
                        "css811": {
                            "eco2": 1001,
                            "tvoc": 91
                        },
                        "pir": {
                            "detected": false
                        }
                    },
                    "timestamp": new Date().getTime()
                }
                break;
            case 'smokeDetectorJson':
                json = {
                    "temperature": 50,
                    "smokeValue": 60,
                    "timestamp": new Date().getTime()
                }
                break;
            case 'smartParkingJson':
                json = {
                    "slotNo": "LG_150",
                    "isOccupied": true,
                    "carWeight": 2000,
                    "carMake": "Jeep",
                    "carModel": "Wrangler",
                    "carRegistration": "WX68 RUC",
                    "timestamp": new Date().getTime()
                }
                break;
            case 'oxymeterDeviceJson':
                json = {
                    "vitals": {
                        "spo2": 96,
                        "bloodPressure": {
                            "systolic": 120,
                            "diastolic": 80
                        },
                        "bpm": 72
                    },
                    "device": {
                        "battery": 86,
                        "ledBrightness": 57,
                        "chargingMode": false
                    },
                    "timestamp": new Date().getTime(),
                }
                break;
            case 'wifiRouterJson':
                json = {
                    "monitoring": {
                        "wifiPhy": 203,
                        "rssi": -98,
                        "ulBytes": 123,
                        "dlBytes": 345,
                        "cpu": 30,
                        "memory": 45
                    },
                    "info": {
                        "txBytes": 345667,
                        "rxBytes": 344232,
                        "mode": "8021 a,c",
                        "channel": 11
                    },
                    "mac": "AA:BB:CC:DD:EE",
                    "agentVersion": "Version_1",
                    "fwVersion": "FW-1234",
                    "ssid": {
                        "key": "MeetingRoom",
                        "pass": "password@123"
                    }
                }
                break;
            default:
                break;
        }
        payload.inputJson = JSON.stringify(json, null, 2);
        payload.masterConfig = [{
            StartTime: '00:00',
            EndTime: '24:00',
            ConfigData: {}
        }]
        this.setState({
            payload,
            errorMessage: '',
            selectedJson: currentTarget.value,
            addGeoLocation: false //set geoLocation false on code changes
        })
    }

    openLocationModalHandler = (configKey, masterIndex, commandConfigKey, locationType) => {
        let payload = cloneDeep(this.state.payload),
            locationModalPayload = {
                lat: "",
                lng: "",
            },
            editIndex = payload.masterConfig[masterIndex].ConfigData[configKey].commandConfig[commandConfigKey];
        if (locationType == "startLocation" && editIndex.valueRange[0] && editIndex.valueRange[1]) {
            locationModalPayload.lat = parseFloat(editIndex.valueRange[0])
            locationModalPayload.lng = parseFloat(editIndex.valueRange[1])
        }
        else if (locationType == "endLocation" && editIndex.valueRange[2] && editIndex.valueRange[3]) {
            locationModalPayload.lat = parseFloat(editIndex.valueRange[2])
            locationModalPayload.lng = parseFloat(editIndex.valueRange[3])
        }
        this.setState({
            payload,
            locationModalPayload,
            locationModalConfigKey: configKey,
            locationModalCommandConfigKey: commandConfigKey,
            locationType,
            location: '',
            masterIndex
        }, () => $('#locationModal').modal('show'))
    }


    isInvalidLatLng = () => {
        let { lat, lng } = this.state.locationModalPayload
        let inValidLat = !(parseFloat(lat) >= -90 && parseFloat(lat) <= 90)
        let inValidLng = !(parseFloat(lng) >= -180 && parseFloat(lng) <= 180)
        return inValidLat || inValidLng
    }

    tempLocationChangeHandler = ({ currentTarget }) => {
        let locationModalPayload = cloneDeep(this.state.locationModalPayload)
        locationModalPayload[currentTarget.id] = currentTarget.value ? Number(currentTarget.value) : ""
        this.setState({
            locationModalPayload,
            mapError: null,
        })
    }


    onFocus = () => {
        this.setState({
            isInvalidAddress: this.state.locationModalPayload && !this.state.locationModalPayload.lat,
        })
    }

    handleLatLngSearch = () => {
        if (this.state.locationModalPayload.lat && this.state.locationModalPayload.lng) {
            return getAddressFromLatLng(parseFloat(this.state.locationModalPayload.lat), parseFloat(this.state.locationModalPayload.lng), this.setLocation, this.handleMapError)
        }
        this.setState({
            mapError: "Missing or Inavalid Lat/Lng"
        })
    }

    handleMapError = (mapError) => {
        this.setState({
            mapError
        })
    }


    setLocation = (location, lat, lng) => {
        let locationModalPayload = cloneDeep(this.state.locationModalPayload)
        locationModalPayload.lat = lat
        locationModalPayload.lng = lng
        locationModalPayload.location = location
        this.setState({ locationModalPayload, mapError: null, isInvalidAddress: false })
    }

    locationChangeHandler = ({ currentTarget }) => {
        let locationModalPayload = cloneDeep(this.state.locationModalPayload)
        locationModalPayload.location = currentTarget.value
        locationModalPayload.lat = ""
        locationModalPayload.lng = ""
        this.setState({
            locationModalPayload,
            isInvalidAddress: false,
            location: currentTarget.value
        })
    }

    saveLocationHandler = () => {
        let payload = cloneDeep(this.state.payload),
            locationModalPayload = cloneDeep(this.state.locationModalPayload),
            changeIndex = payload.masterConfig[this.state.masterIndex].ConfigData[this.state.locationModalConfigKey].commandConfig[this.state.locationModalCommandConfigKey];
        if (this.state.locationType == "startLocation") {
            changeIndex.valueRange[0] = (locationModalPayload.lat).toString()
            changeIndex.valueRange[1] = (locationModalPayload.lng).toString()
            getAddressFromLatLng(changeIndex.valueRange[0], changeIndex.valueRange[1], (location) => {
                let payload = cloneDeep(this.state.payload)
                payload.masterConfig[this.state.masterIndex].ConfigData[this.state.locationModalConfigKey].commandConfig[this.state.locationModalCommandConfigKey].startLocation = location
                this.setState({ payload })
            })
        }
        else {
            changeIndex.valueRange[2] = (locationModalPayload.lat).toString()
            changeIndex.valueRange[3] = (locationModalPayload.lng).toString()
            getAddressFromLatLng(changeIndex.valueRange[2], changeIndex.valueRange[3], (location) => {
                let payload = cloneDeep(this.state.payload)
                payload.masterConfig[this.state.masterIndex].ConfigData[this.state.locationModalConfigKey].commandConfig[this.state.locationModalCommandConfigKey].endLocation = location
                this.setState({ payload })
            })
        }
        this.setState({
            locationType: "",
            payload,
            configErrorKeys: [],
            locationModalPayload: {
                lat: "",
                lng: ""
            }
        }, () => $('#locationModal').modal('hide'))
    }

    addGeoLocationColumn = ({ currentTarget }) => {
        let payload = cloneDeep(this.state.payload),
            id = `Id${Object.keys(payload.masterConfig[0].ConfigData).length}`;
        if (currentTarget.checked) { // adding goelocation // cannot edit name
            payload.masterConfig[0].ConfigData[id] = {
                "command": "default",
                "commandConfig": {
                    "default": {
                        "id": id,
                        "name": "geoLocation",
                        "currentValue": "N/A",
                        "valueType": "gps",
                        "valueRange": ["", "", "", "", ""],
                        "rangeType": "linear",
                    }
                },
            }
        }
        else { // removing geo location on uncheck
            let gpsObjectId = Object.keys(payload.masterConfig[0].ConfigData).find(el => payload.masterConfig[0].ConfigData[el].commandConfig.default.valueType == "gps")
            delete payload.masterConfig[0].ConfigData[gpsObjectId]
        }
        this.setState({
            addGeoLocation: currentTarget.checked,
            payload
        })
    }

    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>Add/Edit Simulation</title>
                    <meta name="description" content="M83-AddOrEditSimulation" />
                </Helmet>

                <header className="content-header d-flex">
                    <div className="flex-60">
                        <div className="d-flex">
                            <h6 className="previous" onClick={() => { this.props.history.push('/simulation') }}>Simulations</h6>
                            <h6 className="active">{this.props.match.params.id ? this.state.payload.name : 'Add New'}</h6>
                        </div>
                    </div>
                    <div className="flex-40 text-right">
                        <div className="content-header-group">
                            <button type='button' className="btn btn-light" data-tooltip data-tooltip-text="Back" data-tooltip-place="bottom" onClick={() => this.props.history.push('/simulation')}><i className="fas fa-angle-left"></i></button>
                        </div>
                    </div>
                </header>

                {this.state.isFetching ? <Loader /> :
                    <div className="content-body">
                        <div className="form-content-wrapper">
                            <form onSubmit={this.onSubmitHandler}>
                                <div className="form-content-box">
                                    <div className="form-content-header">
                                        <p>Configure</p>
                                    </div>
                                    <div className="form-content-body" id="data-attributes">
                                        <div className="d-flex">
                                            <div className="flex-50 pd-r-10">
                                                <div className="form-group">
                                                    <label className="form-group-label">Device Type :  <i className="fas fa-asterisk form-group-required"></i></label>
                                                    <select className="form-control" id="selectedConnector" required disabled={this.props.match.params.id || this.state.isViewOnly} value={this.state.payload.mqttData.selectedConnector} onChange={this.connectorDataChangeHandler}>
                                                        <option value=''>Select</option>
                                                        {this.state.connectorsList && this.state.connectorsList.map((connector, index) =>
                                                            <option key={index} disabled={connector.isDisabled} value={connector.id}>{connector.name}</option>
                                                        )}
                                                    </select>
                                                </div>
                                            </div>
                                            {/* <div className="flex-33 pd-l-10 pd-r-10">
                                                <div className="form-group">
                                                    <label className="form-group-label">Total Devices :</label>
                                                    <input className="form-control" type="number" id="totalDevices" disabled value={this.state.listOfDevicesForSelectedConnector.length} />
                                                </div>
                                            </div> */}
                                            <div className="flex-50 pd-l-10">
                                                <div className="form-group">
                                                    <label className="form-group-label">Reporting Frequency :</label>
                                                    <div className="input-group">
                                                        <input className="form-control" type="text" id="loadIntervalSecs" value={this.state.payload.loadIntervalSecs} readOnly />
                                                        <div className="input-group-append">
                                                            <span className="input-group-text">secs</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="flex-50 pd-r-10">
                                                <div className="form-group">
                                                    <label className="form-group-label">Device Group :  <i className="fas fa-asterisk form-group-required"></i></label>
                                                    <ReactSelect
                                                        className="form-control-multi-select"
                                                        value={this.state.payload.deviceGroupIds.map(el => {
                                                            let deviceGroup = this.state.listOfDeviceGroupsForSelectedConnector.find(list => list.deviceGroupId === el)
                                                            if (deviceGroup) {
                                                                return {
                                                                    value: el,
                                                                    label: deviceGroup.name
                                                                }
                                                            }
                                                        })}
                                                        onChange={(value) => this.handleDeviceGroupChange(value)}
                                                        options={this.state.listOfDeviceGroupsForSelectedConnector.map(el => {
                                                            return {
                                                                value: el.deviceGroupId,
                                                                label: el.name
                                                            }
                                                        })}
                                                        isMulti={true}
                                                        isDisabled={(!this.state.payload.mqttData.selectedConnector) || this.state.isViewOnly}
                                                    >
                                                    </ReactSelect>
                                                </div>
                                            </div>
                                            <div className="flex-50 pd-l-10">
                                                <div className="form-group">
                                                    <label className="form-group-label">Device Count :</label>
                                                    <div className="input-group">
                                                        <input className="form-control" type="text" id="selectedDeviceCount" value={this.state.selectedDeviceCount} readOnly />
                                                    </div>
                                                </div>
                                            </div>
                                            {/*<div className="flex-100">
                                                <div className="form-group">
                                                    <label className="form-group-label">Topic Info :</label>
                                                    <div className="content-table">
                                                        <table className="table table-bordered">
                                                            <thead>
                                                                <tr>
                                                                    <th width="20%">Topic</th>
                                                                    <th width="80%">Topic Id</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td><span className="text-orange">Report (Publish)</span></td>
                                                                    <td>{this.state.payload.mqttData.topic ? `${this.state.payload.mqttData.topic.split('/')[0]}/<deviceId>/report` : '<topicName>/<deviceId>/report'}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td><span className="text-success">Control (Subscribe)</span></td>
                                                                    <td>{this.state.payload.mqttData.topic ? `${this.state.payload.mqttData.topic.split('/')[0]}/<deviceId>/control` : '<topicName>/<deviceId>/control'}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td><span className="text-orange">Config (Publish)</span></td>
                                                                    <td>{this.state.payload.mqttData.topic ? `${this.state.payload.mqttData.topic.split('/')[0]}/<deviceId>/config` : '<topicName>/<deviceId>/config'}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td><span className="text-orange">MetaData (Publish)</span></td>
                                                                    <td>{this.state.payload.mqttData.topic ? `${this.state.payload.mqttData.topic.split('/')[0]}/<deviceId>/metaData` : '<topicName>/<deviceId>/metaData'}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td><span className="text-orange">Alarm (Publish)</span></td>
                                                                    <td>{this.state.payload.mqttData.topic ? `${this.state.payload.mqttData.topic.split('/')[0]}/<deviceId>/alarm` : '<topicName>/<deviceId>/alarm'}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td><span className="text-orange">Miscellaneous (Publish)</span></td>
                                                                    <td>{this.state.payload.mqttData.topic ? `${this.state.payload.mqttData.topic.split('/')[0]}/<deviceId>/miscellaneous/#` : '<topicName>/<deviceId>/miscellaneous/#'}</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>*/}
                                        </div>
                                    </div>
                                </div>

                                <div className="form-content-box">
                                    <div className="form-content-header">
                                        <p>Device JSON {this.state.errorMessage ? <span className="form-content-error">({this.state.errorMessage})</span> : ""}</p>
                                        <div className="form-group mb-0">
                                            <select className="form-control" name="selectJson" disabled={this.state.isViewOnly} value={this.state.selectedJson} onChange={this.addSampleJson} >
                                                <option value="">Select Sample JSON</option>
                                                <option value="energySensorJson">Energy Sensor JSON</option>
                                                <option value="vechileObdJson">Vehicle OBD JSON</option>
                                                <option value="waterSensorJson">Water Sensor JSON</option>
                                                <option value="environmentalSensorJson">Environmental Sensor JSON</option>
                                                <option value="smokeDetectorJson">Smoke Detector JSON</option>
                                                <option value="smartParkingJson">Smart Parking Sensor JSON</option>
                                                <option value="oxymeterDeviceJson">Oxymeter Device JSON</option>
                                                <option value="wifiRouterJson">WiFi Router JSON</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div className="form-content-body" id="data-attributes">
                                        <div className="ace-editor">
                                            <AceEditor
                                                placeholder="Paste your JSON here..."
                                                mode="json"
                                                id="inputJson"
                                                name="ace_editor"
                                                theme="monokai"
                                                height="100%"
                                                width="100%"
                                                value={this.state.payload.inputJson}
                                                onChange={(code) => this.changeHandlerAceEditor(code)}
                                                readOnly={this.state.isViewOnly}
                                            />
                                        </div>
                                        <div className="border-top text-right mt-3 pt-3">
                                            <button type="button" disabled={this.state.isViewOnly} className="btn btn-success" onClick={this.changeView}>Configure Attributes</button>
                                        </div>
                                    </div>
                                </div>

                                {Object.keys(this.state.payload.masterConfig[0].ConfigData).length ?
                                    <div className="form-content-box" ref={(ref) => this.myRef = ref}>
                                        <div className="form-content-header">
                                            <p>
                                                Device Attributes
                                                {/* {(this.state.selectedConnector && this.state.selectedConnector.assetType == "Movable") && // if asset is movable then add  geoLocationO
                                                    <div className="form-group">
                                                        <label className={this.state.payload.roleId ? "check-box check-box-disabled" : "check-box"}>
                                                            <span className="check-text">Simulate Geo Location</span>
                                                            <input type="checkbox" name="addGeoLocation" checked={this.state.addGeoLocation} onChange={this.addGeoLocationColumn} />
                                                            <span className="check-mark" />
                                                        </label>
                                                    </div>
                                                } */}
                                            </p>

                                        </div>
                                        <div className="form-content-body" id="data-attributes">
                                            <div className="form-group">
                                                <div className="attribute-list">
                                                    <div className="attribute-list-header d-flex">
                                                        <p className="flex-20">Attribute</p>
                                                        <p className="flex-10">Value</p>
                                                        <p className="flex-15">Data Type</p>
                                                        <p className="flex-20">Value Type</p>
                                                        <p className="flex-35">Format/Expression</p>
                                                    </div>
                                                    <div className="attribute-list-body pt-2">
                                                        {Object.keys(this.state.payload.masterConfig[0].ConfigData).map((configKey, configKeyIndex) => {
                                                            let html,
                                                                master = this.state.payload.masterConfig[0],
                                                                masterIndex = 0,
                                                                defaultCommandConfig = master.ConfigData[configKey].commandConfig["default"];
                                                            html = (
                                                                <React.Fragment key={configKeyIndex}>
                                                                    <ul className={`list-style-none d-flex align-items-center ${this.state.configErrorKeys.includes(configKey) ? "attribute-list-body-error" : ''}`}> {/* error class toggled here*/}
                                                                        <li className="flex-20">
                                                                            <p className="form-control text-truncate m-0">
                                                                                {Object.keys(master.ConfigData[configKey].commandConfig).length > 1 ?
                                                                                    <span className="attribute-list-collapse" onClick={() => { this.setState({ controlIndex: this.state.controlIndex === configKey ? '' : configKey }) }}>
                                                                                        <i className={this.state.controlIndex === configKey ? "fas fa-caret-up" : "fas fa-caret-down"}></i>
                                                                                    </span> : ''
                                                                                }{defaultCommandConfig.name}
                                                                            </p>
                                                                        </li>
                                                                        <li className="flex-10">
                                                                            <p className="form-control text-truncate m-0">{defaultCommandConfig.currentValue}</p>
                                                                        </li>
                                                                        <li className="flex-15">
                                                                            <select className="form-control" name="valueType" value={defaultCommandConfig.valueType}
                                                                                disabled={this.state.payload.state === "start" || masterIndex !== 0 || defaultCommandConfig.valueType === "time" || this.state.isViewOnly || defaultCommandConfig.valueType === "gps"} onChange={(e) => this.simulationDataChangeHandler(e, configKey, masterIndex, "default")} >
                                                                                <option disabled={defaultCommandConfig.valueType !== "boolean"} value="boolean">Boolean</option>
                                                                                <option disabled={defaultCommandConfig.valueType === "boolean" || defaultCommandConfig.valueType === "int" || defaultCommandConfig.valueType === "time"} value="float">Float</option>
                                                                                <option disabled={defaultCommandConfig.valueType === "float" || defaultCommandConfig.valueType === "boolean" || defaultCommandConfig.valueType === "string"} value="int">Integer</option>
                                                                                <option disabled={defaultCommandConfig.valueType === "float" || defaultCommandConfig.valueType === "boolean" || defaultCommandConfig.valueType === "int" || defaultCommandConfig.valueType === "time"} value="string">String</option>
                                                                                <option disabled={defaultCommandConfig.valueType !== "gps"} value="gps">Location</option>
                                                                                <option disabled={true} value="time">Timestamp</option>
                                                                            </select>
                                                                        </li>
                                                                        <li className="flex-20">
                                                                            <select className="form-control" name="rangeType" value={defaultCommandConfig.rangeType} disabled={this.state.payload.state === "start" || defaultCommandConfig.valueType === "time" || defaultCommandConfig.valueType === "gps" || this.state.isViewOnly} onChange={(e) => this.simulationDataChangeHandler(e, configKey, masterIndex, "default")} >
                                                                                <option value="static">Constant</option>
                                                                                <option value="linear">Linear</option>
                                                                                <option value="randomAlways">Random Always</option>
                                                                                <option value="randomOnce">Random Once</option>
                                                                                {defaultCommandConfig.valueType === "float" ? <option value="variable">Variable</option> : ''}
                                                                                {defaultCommandConfig.valueType === "float" || defaultCommandConfig.valueType === "int"
                                                                                    || defaultCommandConfig.valueType === "string" || defaultCommandConfig.valueType === "boolean" ?
                                                                                    <option disabled={localStorage.tenantType === "MAKER" || localStorage.tenantType === "FLEX"} value="lambdaFunction">Lambda Function</option> : ''}
                                                                                {(defaultCommandConfig.valueType === "float" || defaultCommandConfig.valueType === "int" || defaultCommandConfig.valueType === "string") && this.state.payload.mqttData.topicType === "ReportAndControl" ? <option value="control">Control</option> : ''}
                                                                            </select>
                                                                        </li>
                                                                        <li className="flex-35">
                                                                            {this.formatSwitcher(configKey, masterIndex, defaultCommandConfig, "default")}
                                                                        </li>
                                                                    </ul>
                                                                    {this.state.controlIndex === configKey ?
                                                                        <div className="form-group collapse show">
                                                                            <div className="attribute-list attribute-sub-list">
                                                                                <div className="alert alert-info note-text">
                                                                                    <p>
                                                                                        <span><strong>Topic</strong><i className="far fa-angle-double-right"></i>{`${this.state.payload.mqttData.topic}/<deviceId>/control`}</span>
                                                                                        <span className="mr-l-15"><strong>Command</strong><i className="far fa-angle-double-right"></i>{`{"${master.ConfigData[configKey].commandConfig["default"].name}" : "${master.ConfigData[configKey].command}"}`}</span>
                                                                                    </p>
                                                                                </div>
                                                                                <div className="attribute-list-header d-flex">
                                                                                    <p className="flex-10">Default</p>
                                                                                    <p className="flex-20">Command</p>
                                                                                    <p className="flex-20">Value Type</p>
                                                                                    <p className="flex-40">Format/Expression</p>
                                                                                    <p className="flex-10">Action</p>
                                                                                </div>
                                                                                <div className="attribute-list-body pt-2">
                                                                                    {Object.keys(master.ConfigData[configKey].commandConfig).map((commandConfigKey, commandConfigIndex) => {
                                                                                        let commandConfigByKey = master.ConfigData[configKey].commandConfig[commandConfigKey]
                                                                                        if (commandConfigKey !== "default") {
                                                                                            return (
                                                                                                <ul className="list-style-none d-flex align-items-center">
                                                                                                    <li className="flex-10">
                                                                                                        <div className="toggle-switch-wrapper">
                                                                                                            <label className="toggle-switch">
                                                                                                                <input type="checkbox" name="isCommand" onChange={(e) => this.simulationDataChangeHandler(e, configKey, masterIndex, commandConfigKey)} checked={master.ConfigData[configKey].command === commandConfigKey} />
                                                                                                                <span className="toggle-switch-slider"></span>
                                                                                                            </label>
                                                                                                        </div>
                                                                                                    </li>
                                                                                                    <li className="flex-20">
                                                                                                        <label className="form-group-label mr-b-0">
                                                                                                            {commandConfigByKey.isEditMode ?
                                                                                                                <div className="form-group m-0">
                                                                                                                    <input type="text" id={`name_${masterIndex}_${configKey}_${commandConfigKey}`} name="control" placeholder="From" className="form-control" onChange={(e) => this.controlFormHandler(e, masterIndex, configKey, commandConfigKey)} value={this.state.controlForm[`name_${masterIndex}_${configKey}_${commandConfigKey}`]} />
                                                                                                                </div> :
                                                                                                                <p className="form-control text-truncate m-0">{commandConfigKey}</p>
                                                                                                            }
                                                                                                        </label>
                                                                                                    </li>
                                                                                                    <li className="flex-20">
                                                                                                        <select className="form-control" name="rangeType" value={commandConfigByKey.rangeType} disabled={this.state.payload.state === "start" || defaultCommandConfig.valueType === "time" || this.state.isViewOnly} onChange={(e) => this.simulationDataChangeHandler(e, configKey, masterIndex, commandConfigKey)} >
                                                                                                            <option value="static">Constant</option>
                                                                                                            <option value="linear">Linear</option>
                                                                                                            <option value="randomAlways">Random Always</option>
                                                                                                            <option value="randomOnce">Random Once</option>
                                                                                                            {defaultCommandConfig.valueType === "float" ? <option value="variable">Variable</option> : ''}
                                                                                                            {defaultCommandConfig.valueType === "float" || defaultCommandConfig.valueType === "int"
                                                                                                                || defaultCommandConfig.valueType === "string" || defaultCommandConfig.valueType === "boolean" ?
                                                                                                                <option value="lambdaFunction" disabled={localStorage.tenantType === "MAKER" || localStorage.tenantType === "FLEX"}>Lambda Function</option> : ''}
                                                                                                        </select>
                                                                                                    </li>
                                                                                                    <li className="flex-40">
                                                                                                        {this.formatSwitcher(configKey, masterIndex, commandConfigByKey, commandConfigKey)}
                                                                                                    </li>
                                                                                                    <li className="flex-10">
                                                                                                        {!commandConfigByKey.isEditMode ?
                                                                                                            <div className="button-group">
                                                                                                                <button type="button" disabled={this.state.isViewOnly} onClick={() => {
                                                                                                                    let payload = cloneDeep(this.state.payload),
                                                                                                                        controlForm = cloneDeep(this.state.controlForm);
                                                                                                                    payload.masterConfig[masterIndex].ConfigData[configKey].commandConfig[commandConfigKey].isEditMode = true
                                                                                                                    controlForm[`name_${masterIndex}_${configKey}_${commandConfigKey}`] = commandConfigKey
                                                                                                                    this.setState({
                                                                                                                        payload,
                                                                                                                        controlForm
                                                                                                                    })
                                                                                                                }} className="btn-transparent btn-transparent-blue" data-tooltip data-tooltip-text="Edit" data-tooltip-place="bottom"><i className="far fa-pen"></i>
                                                                                                                </button>
                                                                                                                <button type="button" disabled={Object.keys(this.state.payload.masterConfig[masterIndex].ConfigData[configKey].commandConfig).length === 2 || this.state.payload.masterConfig[masterIndex].ConfigData[configKey].command === commandConfigKey || this.state.isViewOnly} onClick={() => {
                                                                                                                    let payload = cloneDeep(this.state.payload);
                                                                                                                    delete payload.masterConfig[masterIndex].ConfigData[configKey].commandConfig[commandConfigKey]
                                                                                                                    this.setState({
                                                                                                                        payload
                                                                                                                    })

                                                                                                                }} className="btn-transparent btn-transparent-red" data-tooltip data-tooltip-text="Delete" data-tooltip-place="bottom"><i className="far fa-trash-alt"></i>
                                                                                                                </button>
                                                                                                            </div> :
                                                                                                            <div className="button-group">
                                                                                                                <button type="button" disabled={this.state.isViewOnly} onClick={(e) => this.addEditControlConfigHandler(e, configKey, masterIndex, commandConfigKey, "edit")}
                                                                                                                    className="btn-transparent btn-transparent-green" data-tooltip data-tooltip-text="Save" data-tooltip-place="bottom">
                                                                                                                    <i className="far fa-check"></i>
                                                                                                                </button>
                                                                                                                <button type="button" disabled={this.state.isViewOnly} onClick={() => {
                                                                                                                    let payload = cloneDeep(this.state.payload),
                                                                                                                        controlForm = cloneDeep(this.state.controlForm);
                                                                                                                    payload.masterConfig[masterIndex].ConfigData[configKey].commandConfig[commandConfigKey].isEditMode = false
                                                                                                                    controlForm[`name_${masterIndex}_${configKey}_${commandConfigKey}`] = ''
                                                                                                                    this.setState({
                                                                                                                        payload,
                                                                                                                        controlForm
                                                                                                                    })
                                                                                                                }} className="btn-transparent btn-transparent-gray" data-tooltip data-tooltip-text="Cancel" data-tooltip-place="bottom">
                                                                                                                    <i className="far fa-times"></i>
                                                                                                                </button>
                                                                                                            </div>
                                                                                                        }
                                                                                                    </li>
                                                                                                </ul>
                                                                                            )
                                                                                        }
                                                                                    })}
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        : ''
                                                                    }
                                                                </React.Fragment>
                                                            )
                                                            return html
                                                        })
                                                        }
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    : ''
                                }
                                <div className="form-info-wrapper">
                                    <div className="form-info-body">
                                        <div className="form-info-icon">
                                            <img src="https://content.iot83.com/m83/misc/addSimulation.png" />
                                        </div>
                                        <h5>What is Simulation ?</h5>
                                        <ul className="list-style-none form-info-list mb-4">
                                            <li><p>Simulate your devices by using the sample JSON provided on this page or by specifying your own JSON that describes your Device Type.</p></li>
                                            <li><p>Once the simulation is started, the simulated or virtual device will start sending data to the designated MQTT topic.</p></li>
                                            <li><p>It will enable you to build the complete application even without having the actual hardware or device.</p></li>
                                            <li><p>Only one Simulation per Device Type permitted. Should you wish to change the JSON definition, please recreate the new simulation by deleting the current one.</p></li>
                                            <li><p>The Configure attributes section allows you to specify the simulation behaviour for any attribute. For example, if you have an attribute called temperature and you want that attribute to emit values within a range, then you can configure this behaviour accordingly.</p></li>
                                        </ul>
                                    </div>
                                    <div className="form-info-footer">
                                        <button type='button' className="btn btn-light" onClick={() => { this.props.history.push("/simulation"); }}>Cancel</button>
                                        <button type='submit' className="btn btn-primary" disabled={Object.keys(this.state.payload.masterConfig[0].ConfigData).length === 0 || !this.state.payload.deviceGroupIds.length || this.state.isViewOnly}>{this.props.match.params.id ? "Update" : "Save"}</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                }

                {/* equation modal */}
                <div className="modal fade animated slideInDown" id="simulationModal" role="dialog">
                    {this.state.variableModalConfigKey ?
                        <div className="modal-dialog modal-xl modal-dialog-centered">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h4 className="modal-title">Add/Edit Equation
                                        <button type="button" className="close" data-tooltip data-tooltip-text="Close" data-tooltip-place="bottom" onClick={() => this.modalStateHandler("clean", "#simulationModal")}>
                                            <i className="far fa-times"></i>
                                        </button>
                                    </h4>
                                </div>
                                <div className="modal-body">
                                    <div className="d-flex">
                                        <div className="flex-40 pd-r-10">
                                            {this.state.modalErrorMessage ? <p className="modal-body-error">{this.state.modalErrorMessage}</p> : ''}
                                            <div className="form-group">
                                                <label className="form-group-label">Normal Noise % :</label>
                                                <input type="number" id="nominalError" className="form-control" value={this.state.modalPayload.valueError.nominalError}
                                                    onChange={(e) => this.variableDistributionEditHandler(e)}
                                                />
                                            </div>
                                            <div className="form-group">
                                                <label className="form-group-label">Abnormal Noise % :</label>
                                                <input type="number" id="abnormalError" className="form-control" value={this.state.modalPayload.valueError.abnormalError}
                                                    onChange={(e) => this.variableDistributionEditHandler(e)}
                                                />
                                            </div>
                                            <div className="form-group">
                                                <label className="form-group-label">Normal Noise Probability % :</label>
                                                <input type="number" id="normalityProbabilityWeights" className="form-control" value={this.state.modalPayload.normalityProbabilityWeights}
                                                    onChange={(e) => this.variableDistributionEditHandler(e)}
                                                />
                                            </div>
                                            <div className="d-flex">
                                                <div className="flex-50 pd-r-7">
                                                    <div className="form-group">
                                                        <label className="form-group-label">Range A :</label>
                                                        <input type="number" type="number" id="aMax" className="form-control" value={this.state.modalPayload.aMax}
                                                            onChange={(e) => this.variableDistributionEditHandler(e)}
                                                        />
                                                    </div>
                                                </div>
                                                <div className="flex-50 pd-l-7">
                                                    <div className="form-group">
                                                        <label className="form-group-label">Range B :</label>
                                                        <input type="number" id="bMax" className="form-control" value={this.state.modalPayload.bMax}
                                                            onChange={(e) => this.variableDistributionEditHandler(e)}
                                                        />
                                                    </div>
                                                </div>
                                                <div className="flex-50 pd-r-7">
                                                    <div className="form-group">
                                                        <label className="form-group-label">Range C :</label>
                                                        <input type="number" id="cMax" className="form-control" value={this.state.modalPayload.cMax}
                                                            onChange={(e) => this.variableDistributionEditHandler(e)}
                                                        />
                                                    </div>
                                                </div>
                                                <div className="flex-50 pd-l-7">
                                                    <div className="form-group">
                                                        <label className="form-group-label">Range D :</label>
                                                        <input type="number" id="dMax" className="form-control" value={this.state.modalPayload.dMax}
                                                            onChange={(e) => this.variableDistributionEditHandler(e)}
                                                        />
                                                    </div>
                                                </div>
                                                <div className="flex-50 pd-r-7">
                                                    <label className="radio-button">
                                                        <span className="radio-button-text">Quadratic Equation</span>
                                                        <input type="radio" value="cubiceqn" name="equationType" checked={this.state.modalPayload.equationType === "cubiceqn"}
                                                            onChange={this.variableDistributionEditHandler}
                                                        />
                                                        <span className="radio-button-mark"></span>
                                                    </label>
                                                </div>
                                                <div className="flex-50 pd-l-7">
                                                    <label className="radio-button">
                                                        <span className="radio-button-text">Sinosudial Equation</span>
                                                        <input type="radio" value="sineqn" name="equationType" checked={this.state.modalPayload.equationType === "sineqn"}
                                                            onChange={this.variableDistributionEditHandler}
                                                        />
                                                        <span className="radio-button-mark"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="flex-60 pd-l-10">
                                            <div className="alert alert-info note-text">
                                                <p><strong>Note : </strong>Output is the function of time.</p>
                                            </div>
                                            {this.state.modalPayload.equationType === "cubiceqn" ?
                                                <div className="equation-wrapper">
                                                    {this.state.editEquationMode ?
                                                        <ul className="list-style-none d-flex form-group mr-b-10">
                                                            <li className="flex-10">
                                                                <div className="input-group">
                                                                    <div className="input-group-append border-right"><strong className="input-group-text">F(t) =</strong></div>
                                                                </div>
                                                            </li>
                                                            <li className="flex-22_5">
                                                                <div className="input-group">
                                                                    <div className="input-group-append"><strong className="input-group-text">A</strong></div>
                                                                    <input type="number" id="a" className="form-control" onChange={this.editEquationChangeHandler} value={this.state.editEquationForm.a} />
                                                                    <div className="input-group-prepend"><strong className="input-group-text">t<sup>3</sup></strong></div>
                                                                </div>

                                                            </li>
                                                            <li className="flex-22_5">
                                                                <div className="input-group">
                                                                    <div className="input-group-append"><strong className="input-group-text">B</strong></div>
                                                                    <input type="number" id="b" className="form-control" onChange={this.editEquationChangeHandler} value={this.state.editEquationForm.b} />
                                                                    <div className="input-group-prepend"><strong className="input-group-text">t<sup>2</sup></strong></div>
                                                                </div>
                                                            </li>
                                                            <li className="flex-22_5">
                                                                <div className="input-group">
                                                                    <div className="input-group-append"><strong className="input-group-text">C</strong></div>
                                                                    <input type="number" id="c" className="form-control" onChange={this.editEquationChangeHandler} value={this.state.editEquationForm.c} />
                                                                    <div className="input-group-prepend"><strong className="input-group-text">t</strong></div>
                                                                </div>
                                                            </li>
                                                            <li className="flex-22_5">
                                                                <div className="input-group">
                                                                    <div className="input-group-append"><strong className="input-group-text">D</strong></div>
                                                                    <input type="number" id="d" className="form-control" onChange={this.editEquationChangeHandler} value={this.state.editEquationForm.d} />
                                                                </div>
                                                            </li>
                                                            <div className="button-group">
                                                                <button type='button' className="btn-transparent btn-transparent-green" onClick={() => {
                                                                    let modalPayload = cloneDeep(this.state.modalPayload);
                                                                    modalPayload.equationParams = cloneDeep(this.state.editEquationForm)
                                                                    this.setState({
                                                                        modalPayload,
                                                                        editEquationMode: false
                                                                    })
                                                                }}><i className="far fa-check"></i></button>
                                                                <button type='button' className="btn-transparent btn-transparent-gray" onClick={() => this.setState({ editEquationMode: false })}>
                                                                    <i className="far fa-times"></i>
                                                                </button>
                                                            </div>
                                                        </ul> :
                                                        <div className="alert alert-warning note-text">
                                                            <p className="fw-600">f(t) = <span dangerouslySetInnerHTML={this.getEquationOutput(this.state.modalPayload)}></span></p>
                                                            <div className="button-group position-absolute">
                                                                <button type="button" className="btn-transparent btn-transparent-blue" onClick={() => {
                                                                    let editEquationForm;
                                                                    editEquationForm = cloneDeep(this.state.modalPayload.equationParams)
                                                                    this.setState({
                                                                        editEquationMode: true,
                                                                        editEquationForm
                                                                    })
                                                                }}>
                                                                    <i className="far fa-pen"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    }
                                                    <div className="d-flex">
                                                        <div className="flex-50 pd-r-6">
                                                            <div className="equation-slider">
                                                                <p>A ({this.state.modalPayload.equationParams.a})</p>
                                                                <Slider
                                                                    axis="x"
                                                                    xmin={-(this.state.modalPayload.aMax)}
                                                                    xmax={this.state.modalPayload.aMax}
                                                                    x={this.state.modalPayload.equationParams.a}
                                                                    onChange={({ x }) => {
                                                                        this.valueDistributionSliderChangeHandler(x, "a")
                                                                    }}
                                                                    className="w-100"
                                                                />
                                                            </div>
                                                        </div>
                                                        <div className="flex-50 pd-l-6">
                                                            <div className="equation-slider">
                                                                <p>B ({this.state.modalPayload.equationParams.b})</p>
                                                                <Slider
                                                                    axis="x"
                                                                    xmin={-(this.state.modalPayload.bMax)}
                                                                    xmax={this.state.modalPayload.bMax}
                                                                    x={this.state.modalPayload.equationParams.b}
                                                                    onChange={({ x }) => {
                                                                        this.valueDistributionSliderChangeHandler(x, "b")
                                                                    }}
                                                                    className="w-100"
                                                                />
                                                            </div>
                                                        </div>
                                                        <div className="flex-50 pd-r-6">
                                                            <div className="equation-slider">
                                                                <p>C ({this.state.modalPayload.equationParams.c})</p>
                                                                <Slider
                                                                    axis="x"
                                                                    xmin={-(this.state.modalPayload.cMax)}
                                                                    xmax={this.state.modalPayload.cMax}
                                                                    x={this.state.modalPayload.equationParams.c}
                                                                    onChange={({ x }) => {
                                                                        this.valueDistributionSliderChangeHandler(x, "c")
                                                                    }}
                                                                    className="w-100"
                                                                />
                                                            </div>
                                                        </div>
                                                        <div className="flex-50 pd-l-6">
                                                            <div className="equation-slider">
                                                                <p>D ({this.state.modalPayload.equationParams.d})</p>
                                                                <Slider
                                                                    axis="x"
                                                                    xmin={-(this.state.modalPayload.dMax)}
                                                                    xmax={this.state.modalPayload.dMax}
                                                                    x={this.state.modalPayload.equationParams.d}
                                                                    onChange={({ x }) => {
                                                                        this.valueDistributionSliderChangeHandler(x, "d")
                                                                    }}
                                                                    className="w-100"
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="equation-chart-box">
                                                        {this.state.payload.masterConfig[this.state.masterIndex].StartTime && this.state.payload.masterConfig[this.state.masterIndex].EndTime ?
                                                            <AllChartsComponent config={this.createChartConfig()} className="h-100 w-100" />
                                                            :
                                                            <div className="inner-message-wrapper">
                                                                <div className="inner-message-content">
                                                                    <i className="fad fa-exclamation-triangle"></i>
                                                                    <h6>Select time to see graph</h6>
                                                                </div>
                                                            </div>
                                                        }
                                                    </div>
                                                </div> :
                                                <div className="equation-wrapper">
                                                    <div className="alert alert-warning note-text">
                                                        <p className="fw-600">f(t) = a * Sin(b * t + c) + d</p>
                                                    </div>
                                                    <div className="equation-slider">
                                                        <p>A ({this.state.modalPayload.equationParams.a})</p>
                                                        <Slider
                                                            axis="x"
                                                            xmin={-(this.state.modalPayload.aMax)}
                                                            xmax={this.state.modalPayload.aMax}
                                                            x={this.state.modalPayload.equationParams.a}
                                                            onChange={({ x }) => {
                                                                this.valueDistributionSliderChangeHandler(x, "a")
                                                            }}
                                                            className="w-100"
                                                        />
                                                    </div>
                                                    <div className="equation-slider">
                                                        <p>B ({this.state.modalPayload.equationParams.b})</p>
                                                        <Slider
                                                            axis="x"
                                                            xmin={-(this.state.modalPayload.bMax)}
                                                            xmax={this.state.modalPayload.bMax}
                                                            x={this.state.modalPayload.equationParams.b}
                                                            onChange={({ x }) => {
                                                                this.valueDistributionSliderChangeHandler(x, "b")
                                                            }}
                                                            className="w-100"
                                                        />
                                                    </div>
                                                    <div className="equation-slider">
                                                        <p>C ({this.state.modalPayload.equationParams.c})</p>
                                                        <Slider
                                                            axis="x"
                                                            xmin={-(this.state.modalPayload.cMax)}
                                                            xmax={this.state.modalPayload.cMax}
                                                            x={this.state.modalPayload.equationParams.c}
                                                            onChange={({ x }) => {
                                                                this.valueDistributionSliderChangeHandler(x, "c")
                                                            }}
                                                            className="w-100"
                                                        />
                                                    </div>
                                                    <div className="equation-slider">
                                                        <p>D ({this.state.modalPayload.equationParams.d})</p>
                                                        <Slider
                                                            axis="x"
                                                            xmin={-(this.state.modalPayload.dMax)}
                                                            xmax={this.state.modalPayload.dMax}
                                                            x={this.state.modalPayload.equationParams.d}
                                                            onChange={({ x }) => {
                                                                this.valueDistributionSliderChangeHandler(x, "d")
                                                            }}
                                                            className="w-100"
                                                        />
                                                    </div>
                                                </div>
                                            }
                                        </div>
                                    </div>
                                </div>

                                <div className="modal-footer">
                                    <button type="button" className="btn btn-secondary" data-dismiss="modal" onClick={() => this.modalStateHandler("clean", "#simulationModal")}>Cancel</button>
                                    <button type="button" className="btn btn-success" onClick={() => this.modalStateHandler("save", "#simulationModal")}>{this.props.match.params.id ? "Update" : "Save"}</button>
                                </div>
                            </div>
                        </div>
                        : ''
                    }
                </div>
                {/* end equation modal */}

                {/* function modal */}
                <div className="modal animated slideInDown" id="functionModal" role="dialog">
                    {this.state.functionModalConfigKey ?
                        <div className="modal-dialog modal-xl modal-dialog-centered">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <button type="button" className="close" data-tooltip data-tooltip-text="Close" data-tooltip-place="bottom" onClick={() => this.modalStateHandler("clean", "#functionModal")}>&times;</button>
                                    <h4 className="modal-title"><i className="fas fa-pi"></i> Add/Edit Function</h4>
                                </div>
                                <div className="modal-body simulation-modal-body">
                                    <div className="note-text">
                                        <strong>Note : </strong>
                                        <span><i className="fas fa-location-arrow"></i>Updating function will affect real time results on Running simulation</span>
                                        <span><i className="fas fa-location-arrow"></i>Please do not change the name of the return variable - "output"</span>
                                    </div>
                                    <div className="d-flex">
                                        <div className="flex-50 pd-r-7">
                                            <div className="form-group">
                                                <label className="form-group-label">Kind</label>
                                                <input type="text" id="kind" disabled={true} placeholder="kind" className="form-control" defaultValue={"python:3"} required />
                                            </div>
                                        </div>
                                        <div className="flex-50 pd-l-7">
                                            <div className="form-group">
                                                <label className="form-group-label">Type</label>
                                                <select id="imageName" className="form-control" required value={this.state.modalPayload.imageName} onChange={this.functionModalChangeHandler}>
                                                    <option value="">Select</option>
                                                    {this.state.faasImages && this.state.faasImages.map((options, index) => <option key={index} value={options.image}>{options.image}</option>)}
                                                </select>
                                            </div>
                                        </div>
                                        <div className="flex-50 pd-r-7">
                                            <label className="form-group-label">Add Definition</label>
                                            <div className="ace-editor">
                                                <AceEditor
                                                    width="100%"
                                                    placeholder="Type code here..."
                                                    mode="python"
                                                    theme="monokai"
                                                    name="ace_editor"
                                                    onChange={(code) => this.functionModalCodeHandler(code)}
                                                    fontSize={14}
                                                    value={this.state.modalPayload.code}
                                                    showPrintMargin={false}
                                                    showGutter={true}
                                                    highlightActiveLine={true}
                                                    setOptions={{
                                                        enableBasicAutocompletion: true,
                                                        enableLiveAutocompletion: true,
                                                        enableSnippets: false,
                                                        showLineNumbers: true,
                                                        tabSize: 2,
                                                    }}
                                                    height="100%"
                                                />
                                            </div>
                                        </div>
                                        <div className="flex-50 pd-l-7">
                                            <label className="form-group-label position-relative">Add Parameter <button className="btn-link-add" type="button" onClick={() => this.pathParamHandler()}><span className="btn-icon"><i className="far fa-plus"></i></span></button></label>
                                            <div className="modal-form">
                                                <div className="parameter-box">
                                                    <form onSubmit={this.savePathParams}>
                                                        <div className="d-flex">
                                                            <div className="flex-50 pd-r-7">
                                                                <div className="form-group">
                                                                    <label className="form-group-label">Name</label>
                                                                    <input type="text" id="name" required placeholder="Name"
                                                                        value={this.state.selectedPathParam.name}
                                                                        onChange={this.onChangeHandlerPathParams} className="form-control" required />
                                                                </div>
                                                            </div>
                                                            <div className="flex-50 pd-l-7">
                                                                <div className="form-group">
                                                                    <label className="form-group-label">Data Type</label>
                                                                    <select id="type" className="form-control" required value={this.state.selectedPathParam.type} onChange={this.onChangeHandlerPathParams}>
                                                                        <option value="Boolean">Boolean</option>
                                                                        <option value="int">int</option>
                                                                        <option value="float">float</option>
                                                                        <option value="char">char</option>
                                                                        <option value="String">String</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div className="flex-100">
                                                                <div className="form-group">
                                                                    <label className="form-group-label">Description</label>
                                                                    <input type="text" className="form-control" id="description" value={this.state.selectedPathParam.description} onChange={this.onChangeHandlerPathParams} placeholder="description" />
                                                                </div>
                                                            </div>
                                                            <div className="flex-50 pd-r-7">
                                                                <div className="form-group">
                                                                    <label className="form-group-label">Default Value</label>
                                                                    <input type="text" id="defaultValue" value={this.state.selectedPathParam.defaultValue} onChange={this.onChangeHandlerPathParams} placeholder="DefaultValue"
                                                                        className="form-control" required />
                                                                </div>
                                                            </div>
                                                            <div className="flex-50 pd-l-7">
                                                                <div className="form-group">
                                                                    <label className="form-group-label">Required</label>
                                                                    <div className="d-flex">
                                                                        <div className="flex-50">
                                                                            <label className="radio-button border-0 bg-transparent">
                                                                                <span className="radio-button-text">True</span>
                                                                                <input type="radio" checked={this.state.selectedPathParam.required == "true"}
                                                                                    onChange={this.onChangeHandlerPathParams} value="true" />
                                                                                <span className="radio-button-mark"></span>
                                                                            </label>
                                                                        </div>
                                                                        <div className="flex-50">
                                                                            <label className="radio-button border-0 bg-transparent">
                                                                                <span className="radio-button-text">False</span>
                                                                                <input type="radio" name="required" checked={this.state.selectedPathParam.required == "false"}
                                                                                    onChange={this.onChangeHandlerPathParams} value="false" />
                                                                                <span className="radio-button-mark"></span>
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="flex-100">
                                                                <div className="form-group text-right">
                                                                    <button className="btn-transparent btn-transparent-green d-inline-block" type="submit"><i className="far fa-check"></i></button>
                                                                    <button className="btn-transparent btn-transparent-red d-inline-block" type="button" onClick={() => { $('.paramFormBox').toggleClass('flipped'); }}><i className="far fa-trash-alt"></i></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-secondary" data-dismiss="modal" onClick={() => this.modalStateHandler("clean", "#functionModal")}>Cancel</button>
                                    <button type="button" className="btn btn-success" onClick={this.functionModalSaveHandler}  >{this.state.modalPayload.actionId ? "Update" : "Save"}</button>
                                </div>
                            </div>
                        </div>
                        : ''}
                </div>


                <div className="modal animated slideInDown" id="functionModalOld">
                    {this.state.functionModalConfigKey ?
                        <div className="modal-dialog modal-xl modal-dialog-centered">
                            <div className="modal-content">
                                <div className="modal-body position-relative">
                                    <div className="flex">
                                        <div className="flex-item fx-b40 contentFormDetail">
                                            <div className="contentFormHeading">
                                                <p>Parameters</p>
                                                <button type="button" className="btn btn-link float-right fw500" onClick={() => this.pathParamHandler()} >
                                                    <i className="far fa-plus mr-r-5"></i>Add Param
                                            </button>
                                            </div>
                                            <div className="paramFormBox">
                                                <div className="OuterAddParamBox">
                                                    <div className="addParamContent">
                                                        <h4>Add Parameter</h4>
                                                        <form onSubmit={this.savePathParams}>
                                                            <div className="modal-body">
                                                                <p className="noteText pd-l-7 mr-t-5 mr-b-10">
                                                                    <strong>Note :</strong>Default Value should be of</p>
                                                                <div className="form-group flex">
                                                                    <div className="form-group-left-label fx-b30">
                                                                        <label className="form-group-label">isRequired :<span className="requiredMark"><i className="fa fa-asterisk" /></span></label>
                                                                    </div>
                                                                    <div className="form-group-right-label fx-b70">
                                                                        <div className="flex">
                                                                            <div className="flex-item fx-b50">
                                                                                <label className="radioLabel">True
                                                                            <input type="radio" name="required" checked={this.state.selectedPathParam.required == "true"} value="true" onChange={this.onChangeHandlerPathParams} value="true" />
                                                                                    <span className="radioMark" />
                                                                                </label>
                                                                            </div>
                                                                            <div className="flex-item fx-b50">
                                                                                <label className="radioLabel">False
                                                                            <input type="radio" name="required" checked={this.state.selectedPathParam.required == "false"} onChange={this.onChangeHandlerPathParams} value="false" />
                                                                                    <span className="radioMark" />
                                                                                </label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="modal-footer">
                                                                <button type='submit' className="btn btn-success">Add</button>
                                                                <button type="button" onClick={() => { $('.paramFormBox').toggleClass('flipped'); }} className="btn btn-dark">Cancel</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                                <div className="paramBox">
                                                    {this.state.modalPayload.pathParameters != 0 ?
                                                        <ul className="paramList">
                                                            {this.state.modalPayload.pathParameters.map((param, index) => {
                                                                return (
                                                                    <li key={index}>
                                                                        <div key={index} className="paramCard">
                                                                            <button type="button" className="btn btn-transparent btn-transparent-primary" onClick={() => this.pathParamHandler(index)}>
                                                                                <i className="fas fa-pencil"></i>
                                                                            </button>
                                                                            <h5>{param.name}<span className="paramRequiredMark"><i className={(param.required === true || param.required === "true") ? "fa fa-asterisk" : ""} /></span></h5>
                                                                            <p><strong>Description :</strong>{param.description}</p>
                                                                            <p><strong>Value :</strong>{param.defaultValue}<span className="text-primary">({param.type})</span></p>
                                                                            <button type="button" className="btn btn-transparent btn-transparent-danger" onClick={() => this.deleteParam(index)}>
                                                                                <i className="fas fa-times"></i>
                                                                            </button>
                                                                        </div>
                                                                    </li>)
                                                            })}
                                                        </ul> :
                                                        <div className="contentTableEmpty">
                                                            <h6>No parameters added yet.</h6>
                                                        </div>
                                                    }
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> : ""
                    }
                </div>
                {/* end function modal */}

                {/* map modal */}
                <div className="modal animated slideInDown" id="locationModal">
                    <div className="modal-dialog modal-lg modal-dialog-centered">
                        <div className="modal-content">
                            <div className="modal-header position-relative">
                                <h6 className="modal-title">{this.state.locationType == "startLocation" ? "Start Location" : "End Location"}<span></span>
                                    <button type="button" className="close" data-tooltip data-tooltip-text="Close" data-tooltip-place="bottom" data-dismiss="modal">
                                        <i className="far fa-times"></i>
                                    </button>
                                </h6>
                            </div>
                            <div className="modal-body location-map-wrapper">
                                <React.Fragment>
                                    <div className="d-flex">
                                        <div className="flex-40 pd-r-10">
                                            <div className="form-group">
                                                <label className="form-group-label">Location :</label>
                                                <div className="auto-complete-box">
                                                    <MapSearchBox onFocus={this.onFocus} onSelect={this.setLocation} value={this.state.location || ""} onChange={this.locationChangeHandler} />
                                                    {this.state.isInvalidAddress && <span className="form-group-error">No Results Found.</span>}
                                                </div>
                                            </div>
                                            <p>Enter your location OR search using lat / lng.</p>
                                            <div className="form-group">
                                                <label className="form-group-label">Latitude :</label>
                                                <input type="number" id="lat" className="form-control" value={this.state.locationModalPayload.lat} onChange={this.tempLocationChangeHandler} />
                                                {(this.isInvalidLatLng() || this.state.mapError) && <span className="form-group-error">{this.isInvalidLatLng() ? "Invalid Lat/Lang" : this.state.mapError}. Device Location will not be Saved.</span>}
                                            </div>
                                            <div className="form-group">
                                                <label className="form-group-label">Longitude :</label>
                                                <input type="number" id="lng" className="form-control" value={this.state.locationModalPayload.lng} onChange={this.tempLocationChangeHandler} />
                                            </div>
                                            <div className="form-group mb-0 text-right">
                                                <button className="btn btn-primary w-100" type="button" disabled={this.isInvalidLatLng() || this.state.mapError} onClick={this.handleLatLngSearch}>Search</button>
                                            </div>
                                        </div>
                                        <div className="flex-60 pd-l-10">
                                            <div className="location-map">
                                                <GoogleMap
                                                    markers={[{ lat: this.state.locationModalPayload.lat, lng: this.state.locationModalPayload.lng }]}
                                                    setLocation={this.setLocation}
                                                    zoom={this.state.locationModalPayload.zoomLevel}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </React.Fragment>
                            </div>
                            <div className="modal-footer">
                                <button className="btn btn-dark" type="button" data-dismiss="modal">Cancel</button>
                                <button className="btn btn-success" type="button" disabled={(!this.state.locationModalPayload.lat) || !this.state.locationModalPayload.lng} onClick={this.saveLocationHandler}>Save</button>
                            </div>
                        </div>
                    </div>
                </div>
                {/* end map modal */}

                {this.state.confirmState &&
                    <ConfirmModel
                        status={"delete"}
                        deleteName={"Lambda Function"}
                        confirmClicked={() => {
                            let payloadForApi = {
                                id: []
                            },
                                lambdaFunctionChangeIndex = cloneDeep(this.state.lambdaFunctionChangeIndex);
                            lambdaFunctionChangeIndex.map(el => {
                                let id = el.actionId;
                                payloadForApi.id.push(id)
                            })
                            this.props.deleteLambdaFunction(payloadForApi)
                            this.setState({
                                confirmState: false,
                            })
                        }}
                        cancelClicked={() => {
                            this.cancelClicked()
                        }}
                    />
                }

                {this.state.isOpen &&
                    <NotificationModal
                        type={this.state.modalType}
                        message2={this.state.message2}
                        onCloseHandler={this.onCloseHandler}
                    />
                }
            </React.Fragment>
        );
    }
}

AddOrEditSimulation.propTypes = {
    dispatch: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
    saveSimulationError: Selectors.saveSimulationError(),
    saveSimulationSuccess: Selectors.saveSimulationSuccess(),
    getSimulationByIdSuccess: Selectors.getSimulationByIdSuccess(),
    getSimulationByIdError: Selectors.getSimulationByIdError(),
    getAllSimulationsSuccess: Selectors.getAllSimulationsSuccess(),
    getAllSimulationsFailure: Selectors.getAllSimulationsFailure(),
    saveLambdaFunctionSuccess: Selectors.saveLambdaFunctionSuccess(),
    saveLambdaFunctionFailure: Selectors.saveLambdaFunctionFailure(),
    deleteLambdaFunctionSuccess: Selectors.deleteLambdaFunctionSuccess(),
    deleteLambdaFunctionFailure: Selectors.deleteLambdaFunctionFailure(),
    getListOfConnectorsSuccess: Selectors.getListOfConnectorsSuccess(),
    getListOfConnectorsFailure: Selectors.getListOfConnectorsFailure(),
    getFaasImagesSuccess: Selectors.getFaasImagesSuccess(),
    getFaasImagesFailure: Selectors.getFaasImagesFailure(),

});

function mapDispatchToProps(dispatch) {
    return {
        dispatch,
        saveSimulation: (payload) => dispatch(Actions.saveSimulation(payload)),
        getSimulationById: id => dispatch(Actions.getSimulationById(id)),
        getAllSimulations: () => dispatch(Actions.getAllSimulations()),
        saveLambdaFunction: (payload) => dispatch(Actions.saveLambdaFunction(payload)),
        deleteLambdaFunction: (payload) => dispatch(Actions.deleteLambdaFunction(payload)),
        getListOfConnectors: () => dispatch(Actions.getListOfConnectors()),
        getFaasImages: (interpreter) => dispatch(Actions.getFaasImages(interpreter)),
    };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "addOrEditSimulation", reducer });
const withSaga = injectSaga({ key: "addOrEditSimulation", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(AddOrEditSimulation);