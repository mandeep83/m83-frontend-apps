/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/*
 *
 * DeviceAttributes actions
 *
 */

import {
  DEFAULT_ACTION,
  FETCH_DEVICES,
  FETCH_DEVICE_STATUS,
  CREATE_FAST_APP,
  REFRESH_SCHEMA,
  VERIFY_SCHEMA,
  GET_ETL_FUNCTIONS,
  DEBUG_DEVICE_ATTRIBUTES,
  GET_DEVELOPER_QUOTA,
  RESET_DEVICE_ATTRIBUTES,
  GET_STATS_DATA
} from "./constants";

export function defaultAction() {
  return {
    type: DEFAULT_ACTION
  };
}


export function fetchDevices() {
  return {
    type: FETCH_DEVICES,
    category: 'MQTT'
  };
}


export function fetchDeviceStatus(id) {
  return {
    type: FETCH_DEVICE_STATUS,
    id,
  };
}

export function createFastApp(id, payload) {
  return {
    type: CREATE_FAST_APP,
    id,
    payload,
  };
}

export function refreshSchema(id, userConsentToken) {
  return {
    type: REFRESH_SCHEMA,
    id,
    userConsentToken,
  };
}

export function verifySchema(id) {
  return {
    type: VERIFY_SCHEMA,
    id,
  };
}

export function resetDeviceAttributes(id) {
  return {
    type: RESET_DEVICE_ATTRIBUTES,
    id,
  };
}

export function getETLMathFunctions() {
  return {
    type: GET_ETL_FUNCTIONS,
  };
}

export function debugDeviceAttributes(payload) {
  return {
    type: DEBUG_DEVICE_ATTRIBUTES,
    payload,
  };
}

export function getDeveloperQuota(payload) {
  return {
    type: GET_DEVELOPER_QUOTA,
    payload,
  };
}

export function etEtlStatsData(id,duration) {
  return {
    type: GET_STATS_DATA,
    id,duration
  };
}
