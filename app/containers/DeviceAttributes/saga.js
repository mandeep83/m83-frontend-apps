/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/


import { apiCallHandler } from '../../api';
import { takeEvery } from 'redux-saga';
import * as CONSTANTS from "./constants";


export function* getDevicesListHandlerAsync(action) {
    yield [apiCallHandler(action, CONSTANTS.FETCH_DEVICES_SUCCESS, CONSTANTS.FETCH_DEVICES_FAILURE, 'getDeviceTypes')];
}
export function* getDeviceStatusHandlerAsync(action) {
    yield [apiCallHandler(action, CONSTANTS.FETCH_DEVICE_STATUS_SUCCESS, CONSTANTS.FETCH_DEVICE_STATUS_FAILURE, 'getConnectorStatus')];
}

export function* createFastAppHandlerAsync(action) {
    yield [apiCallHandler(action, CONSTANTS.CREATE_FAST_APP_SUCCESS, CONSTANTS.CREATE_FAST_APP_FAILURE, 'createFastApp')];
}

export function* refreshSchemaHandlerAsync(action) {
    yield [apiCallHandler(action, CONSTANTS.REFRESH_SCHEMA_SUCCESS, CONSTANTS.REFRESH_SCHEMA_FAILURE, 'refreshSchema')];
}

export function* verifySchemaHandlerAsync(action) {
    yield [apiCallHandler(action, CONSTANTS.VERIFY_SCHEMA_SUCCESS, CONSTANTS.VERIFY_SCHEMA_FAILURE, 'verifySchema')];
}

export function* refreshDeviceAttributesHandlerAsync(action) {
    yield [apiCallHandler(action, CONSTANTS.RESET_DEVICE_ATTRIBUTES_SUCCESS, CONSTANTS.RESET_DEVICE_ATTRIBUTES_FAILURE, 'resetDeviceAttributes')];
}

export function* getETLMathFunctionsHandlerAsync(action) {
    yield [apiCallHandler(action, CONSTANTS.GET_ETL_FUNCTIONS_SUCCESS, CONSTANTS.GET_ETL_FUNCTIONS_FAILURE, 'getETLMathFunctions')];
}

export function* debugDeviceAttributeHandlerAsync(action) {
    yield [apiCallHandler(action, CONSTANTS.DEBUG_DEVICE_ATTRIBUTES_SUCCESS, CONSTANTS.DEBUG_DEVICE_ATTRIBUTES_FAILURE, 'debugDeviceAttributes')];
}

export function* getDeveloperQuotaHandlerAsync(action) {
    yield [apiCallHandler(action, CONSTANTS.GET_DEVELOPER_QUOTA_SUCCESS, CONSTANTS.GET_DEVELOPER_QUOTA_FAILURE, 'getDeveloperQuota')];
}

export function* getStatsData(action) {
    yield [apiCallHandler(action, CONSTANTS.GET_STATS_DATA_SUCCESS, CONSTANTS.GET_STATS_DATA_FAILURE, 'getStatsData')];
}

export function* watcherGetDevices() {
    yield takeEvery(CONSTANTS.FETCH_DEVICES, getDevicesListHandlerAsync);
}
export function* watcherGetDeviceStatus() {
    yield takeEvery(CONSTANTS.FETCH_DEVICE_STATUS, getDeviceStatusHandlerAsync);
}
export function* watcherCreateFastApp() {
    yield takeEvery(CONSTANTS.CREATE_FAST_APP, createFastAppHandlerAsync);
}
export function* watcherRefreshSchema() {
    yield takeEvery(CONSTANTS.REFRESH_SCHEMA, refreshSchemaHandlerAsync);
}
export function* watcherVerifySchema() {
    yield takeEvery(CONSTANTS.VERIFY_SCHEMA, verifySchemaHandlerAsync);
}
export function* watcherDeviceAttributes() {
    yield takeEvery(CONSTANTS.RESET_DEVICE_ATTRIBUTES, refreshDeviceAttributesHandlerAsync);
}

export function* watcherGetETLMathFunctions() {
    yield takeEvery(CONSTANTS.GET_ETL_FUNCTIONS, getETLMathFunctionsHandlerAsync);
}

export function* watcherDebugDeviceAttributes() {
    yield takeEvery(CONSTANTS.DEBUG_DEVICE_ATTRIBUTES, debugDeviceAttributeHandlerAsync);
}

export function* watcherGetDeveloperQuota() {
    yield takeEvery(CONSTANTS.GET_DEVELOPER_QUOTA, getDeveloperQuotaHandlerAsync);
}

export function* watcherStatsData() {
    yield takeEvery(CONSTANTS.GET_STATS_DATA, getStatsData);
}

export default function* rootSaga() {
    yield [
        watcherGetDevices(),
        watcherGetDeviceStatus(),
        watcherCreateFastApp(),
        watcherRefreshSchema(),
        watcherVerifySchema(),
        watcherDeviceAttributes(),
        watcherGetETLMathFunctions(),
        watcherDebugDeviceAttributes(),
        watcherGetDeveloperQuota(),
        watcherStatsData(),
    ];
}
