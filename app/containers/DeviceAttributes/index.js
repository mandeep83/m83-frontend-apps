/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * DeviceAttributes
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import ClientCaptcha from "react-client-captcha";
import ReactTextareaAutocomplete from "@webscopeio/react-textarea-autocomplete";
import cloneDeep from "lodash/cloneDeep";
import ReactJson from "react-json-view";
import ReactSelect from "react-select";
import ReactTooltip from 'react-tooltip';
import { getStatsSuccess, getStatsFailure, getDevicesSuccess, getDevicesFailure, deviceStatusSuccess, deviceStatusFailure, fastAppSuccess, fastAppFailure, refreshSchemaSuccess, refreshSchemaFailure, verifySchemaSuccess, verifySchemaFailure, etlMathFunctionsSuccess, etlMathFunctionsError, debugSuccess, debugError, developerQuotaSuccess, developerQuotaFailure, resetDeviceAttributesSuccess, resetDeviceAttributesFailure } from "./selectors";
import { fetchDevices, fetchDeviceStatus, createFastApp, refreshSchema, verifySchema, resetDeviceAttributes, getETLMathFunctions, debugDeviceAttributes, getDeveloperQuota, etEtlStatsData } from './actions';
import reducer from "./reducer";
import saga from "./saga";
import { getTimeDifference, isNavAssigned } from "../../commonUtils";
import NotificationModal from "../../components/NotificationModal/Loadable";
import Loader from '../../components/Loader/Loadable';
import AddNewButton from "../../components/AddNewButton/Loadable";
import "@webscopeio/react-textarea-autocomplete/style.css";
import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';
import StatsChart from "./Charts/StatsChart";
import { closeMqttConnection, subscribeTopic, unsubscribeTopic } from "../../mqttConnection";

/* eslint-disable react/prefer-stateless-function */
export class DeviceAttributes extends React.Component {
    state = {
        payload: {
            attributes: [],
            etlConfig: { maxOffset: "1000", triggerInterval: 5000, threshold: 10 },
        },
        devices: [],
        operations: [],
        ETLFunctions: [],
        customAttributes: [],
        selectedDeviceId: null,
        isFetching: true,
        refreshSchemaConfirmationModal: false,
        showDebugModal: false,
        debugData: [],
        debugInitiated: false,
        debugState: false,
        isFetchingDeviceDetails: false,
        totalAttributesCount: 0,
        remainingAttributesCount: 0,
        usedAttributesCount: 0,
        isSchemaGenerated: false,
        isDebugBeforeSave: false,
        etlData: undefined,
        eltDuration: 5,
        dataView: 'TABLE',
        schemaVerificationInProgress: false,
        userConsentToken: null,
        assetType: null,
        isGateway: false,
        isTimestampExist: false,
    }

    refreshComponent = () => {
        this.setState({
            payload: {
                attributes: [],
            },
            devices: [],
            operations: [],
            ETLFunctions: [],
            customAttributes: [],
            isFetching: true,
            refreshSchemaConfirmationModal: false,
            showDebugModal: false,
            debugData: [],
            debugInitiated: false,
            debugState: false,
            isFetchingDeviceDetails: false,
            totalAttributesCount: 0,
            remainingAttributesCount: 0,
            usedAttributesCount: 0,
        }, () => {
            let payload = [
                {
                    dependingProductId: null,
                    productName: "attributeKeys"
                }
            ];
            this.props.getDeveloperQuota(payload);
            this.props.getETLMathFunctions();
        })
    }

    componentWillMount() {
        let payload = [
            {
                dependingProductId: null,
                productName: "attributeKeys"
            }
        ];
        this.props.getDeveloperQuota(payload);
        this.props.getETLMathFunctions();
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.getStatsSuccess && nextProps.getStatsSuccess !== this.props.getStatsSuccess) {
            this.setState({
                etlData: nextProps.getStatsSuccess
            })
        }
        if (nextProps.getStatsFailure && nextProps.getStatsFailure !== this.props.getStatsFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.getStatsFailure,
                isFetching: false
            });
        }
        if (nextProps.deviceList && nextProps.deviceList !== this.props.deviceList) {
            let selectedDeviceId = nextProps.deviceList.length > 0 ?
                this.props.match.params.connectorId ?
                    this.props.match.params.connectorId : this.state.selectedDeviceId ?
                        this.state.selectedDeviceId : nextProps.deviceList[0].id : null;
            this.setState({
                isFetching: false,
                isFetchingDeviceDetails: nextProps.deviceList.length > 0 ? true : false,
                devices: nextProps.deviceList,
                refreshBtnDisabled: true,
                selectedDeviceId,
                assetType: selectedDeviceId ? nextProps.deviceList.filter(deviceType => deviceType.id === selectedDeviceId)[0]['assetType'] : null,
                isGateway: selectedDeviceId ? nextProps.deviceList.filter(deviceType => deviceType.id === selectedDeviceId)[0]['isGateway'] ? true : false : false,
            }, () => {
                nextProps.deviceList.length > 0 && this.props.fetchDeviceStatus(selectedDeviceId);
            })
        }
        if (nextProps.deviceListError && nextProps.deviceListError !== this.props.deviceListError) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.deviceListError,
                isFetching: false
            });
        }
        if (nextProps.deviceStatus && nextProps.deviceStatus !== this.props.deviceStatus) {
            let payload = cloneDeep(this.state.payload),
                customAttributes = [],
                usedAttributesCount,
                remainingAttributesCount,
                isTimestampExist = this.state.isTimestampExist;

            if (nextProps.deviceStatus.id) {
                payload.attributes = nextProps.deviceStatus.attributes;
                payload.operations = nextProps.deviceStatus.operations;
                payload.id = nextProps.deviceStatus.id;
                payload.createdAt = nextProps.deviceStatus.createdAt;
                payload.updatedAt = nextProps.deviceStatus.updatedAt;
                payload.attributes.map(attribute => {
                    attribute.isDisabled = attribute.checked ? true : false;
                    if (attribute.isCustom) {
                        attribute.isDebug = true;
                        customAttributes.push(attribute);
                    }
                });
            } else if (!nextProps.deviceStatus.isSchemaGenerated) {
                delete payload.operations;
                payload.attributes = [];
                if (nextProps.deviceStatus.error) {
                    payload.error = nextProps.deviceStatus.error;
                }
            } else if (nextProps.deviceStatus.attributes) {
                delete payload.operations;
                payload.attributes = nextProps.deviceStatus.attributes;
                payload.attributes.map(attribute => {
                    if (attribute.path === 'timestamp') {
                        attribute.isDisabled = true;
                        attribute.checked = true;
                        attribute.displayName = 'timestamp';
                        attribute.attribute = 'timestamp';
                        attribute.isTimestamp = true;

                        isTimestampExist = true;
                    }
                });
            }
            usedAttributesCount = payload.attributes.filter(attr => attr.checked === true && !attr.isCustom).length + customAttributes.length;
            remainingAttributesCount = this.state.totalAttributesCount > 0 ? this.state.totalAttributesCount - usedAttributesCount : 0;
            this.setState({
                refreshBtnDisabled: false,
                isSchemaGenerated: nextProps.deviceStatus.isSchemaGenerated,
                isFetchingDeviceDetails: false,
                isFetching: false,
                isTimestampExist,
                payload,
                customAttributes,
                usedAttributesCount,
                remainingAttributesCount,
            })
        }

        if (nextProps.deviceStatusError && nextProps.deviceStatusError !== this.props.deviceStatusError) {
            this.setState({
                modalType: "error",
                refreshBtnDisabled: false,
                isOpen: true,
                message2: nextProps.deviceStatusError.error,
                isFetching: false,
                isFetchingDeviceDetails: false,
            });
        }
        if (nextProps.fastAppSuccess && nextProps.fastAppSuccess !== this.props.fastAppSuccess) {
            this.setState({
                modalType: "success",
                isOpen: true,
                showWarningMessage: false,
                message2: nextProps.fastAppSuccess.message,
            });
        }
        if (nextProps.fastAppFailure && nextProps.fastAppFailure !== this.props.fastAppFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.fastAppFailure.error,
                isFetching: false
            });
        }
        if (nextProps.refreshSchemaSuccess && nextProps.refreshSchemaSuccess !== this.props.refreshSchemaSuccess) {
            let payload = {
                attributes: [],
            }
            this.setState({
                payload,
                modalType: "success",
                isOpen: true,
                message2: nextProps.refreshSchemaSuccess.message,
                showWarningMessage: false,
            }, () => { this.props.fetchDeviceStatus(this.state.selectedDeviceId) });
        }
        if (nextProps.refreshSchemaFailure && nextProps.refreshSchemaFailure !== this.props.refreshSchemaFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.refreshSchemaFailure,
                isFetching: false
            });
        }

        if (nextProps.verifySchemaSuccess && nextProps.verifySchemaSuccess !== this.props.verifySchemaSuccess) {
            let isSubset = nextProps.verifySchemaSuccess.data.isSubset;
            if (isSubset) {
                this.setState({
                    schemaVerificationInProgress: false,
                    isFetchingDeviceDetails: true
                }, () => { this.props.fetchDeviceStatus(this.state.selectedDeviceId) });
            } else {
                this.setState({
                    schemaVerificationInProgress: false,
                    refreshSchemaConfirmationModal: true,
                    userConsentToken: nextProps.verifySchemaSuccess.data.token,
                })
            }
        }
        if (nextProps.verifySchemaFailure && nextProps.verifySchemaFailure !== this.props.verifySchemaFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.verifySchemaFailure.error,
                schemaVerificationInProgress: false
            });
        }
        if (nextProps.resetDeviceAttributesSuccess && nextProps.resetDeviceAttributesSuccess !== this.props.resetDeviceAttributesSuccess) {
            this.handleRefreshAttributesTimer();
            this.props.fetchDeviceStatus(this.state.selectedDeviceId);
        }
        if (nextProps.resetDeviceAttributesError && nextProps.resetDeviceAttributesError !== this.props.resetDeviceAttributesError) {
            this.setState({
                refreshBtnDisabled: false,
                refreshAttributesBtnDisabled: false,
                modalType: "error",
                isOpen: true,
                message2: nextProps.resetDeviceAttributesError,
                isFetchingDeviceDetails: false,
            });
        }

        if (nextProps.etlMathFunctions && nextProps.etlMathFunctions !== this.props.etlMathFunctions) {
            this.setState({
                ETLFunctions: nextProps.etlMathFunctions,
            }, () => this.props.fetchDevices())
        }
        if (nextProps.etlMathFunctionsError && nextProps.etlMathFunctionsError !== this.props.etlMathFunctionsError) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.etlMathFunctionsError
            }, () => this.props.fetchDevices());
        }
        if (nextProps.debugSuccess && nextProps.debugSuccess !== this.props.debugSuccess) {
            if (this.state.isDebugBeforeSave) {
                let payload = this.getPayload();
                this.setState({
                    isDebugBeforeSave: false,
                }, () => { this.props.createFastApp(payload.id, payload) });
            } else {
                if (this.state.debugState) {
                    this.setState({
                        debugInitiated: false,
                        debugState: !this.state.debugState,
                        showDebugModal: false,
                    }, () => closeMqttConnection());
                } else {
                    this.setState({
                        debugInitiated: false,
                        debugState: !this.state.debugState,
                    });
                }
            }
        }
        if (nextProps.debugError && nextProps.debugError !== this.props.debugError) {
            if (this.state.isDebugBeforeSave) {
                this.setState({
                    isDebugBeforeSave: false,
                    isFetching: false,
                    modalType: "error",
                    isOpen: true,
                    message2: nextProps.debugError.error,
                });
            } else {
                this.setState({
                    modalType: "error",
                    isOpen: true,
                    message2: nextProps.debugError.error,
                    debugInitiated: false,
                    showDebugModal: false,
                }, () => { unsubscribeTopic(`DEBUG_${this.state.selectedDeviceId}`, "doNotCloseConnection") });
            }

        }

        if (nextProps.developerQuota && nextProps.developerQuota !== this.props.developerQuota) {
            let totalAttributesCount = nextProps.developerQuota.filter(product => product.productName === 'attributeKeys')[0]['total'];
            this.setState({
                totalAttributesCount
            });
        }

        if (nextProps.developerQuotaFailure && nextProps.developerQuotaFailure !== this.props.developerQuotaFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.developerQuotaFailure
            });
        }
    }

    customAttrETLFnChangeHandler = (valueObj, type, index) => {
        let customAttributes = cloneDeep(this.state.customAttributes);
        customAttributes[index][type.name] = valueObj.value;
        customAttributes[index]['params'] = this.state.ETLFunctions.filter(fn => fn.declaration === valueObj.value)[0]['params'];
        this.setState({
            customAttributes
        })
    }

    attributeSelectionChangeHandler = (valueObj, type, index, paramIndex) => {
        let customAttributes = cloneDeep(this.state.customAttributes);
        customAttributes[index]['params'][paramIndex]['value'] = valueObj.value;
        this.setState({
            customAttributes
        })
    }

    customAttrChangeHandler = (event, index, paramIndex) => {
        let customAttributes = cloneDeep(this.state.customAttributes),
            payload = cloneDeep(this.state.payload);

        if (event.target.name === 'displayName' || event.target.name === 'attribute' || event.target.name === 'unit' || event.target.name === 'expression') {
            if (event.target.name === 'attribute') {
                let regex = /^[$A-Z_][A-Z_$]*$/i;
                if (regex.test(event.target.value) || event.target.value == '') {
                    customAttributes[index][event.target.name] = event.target.value;
                }

            } else if (event.target.name === 'displayName' || event.target.name === 'unit') {
                if (customAttributes[index].isDisabled) {
                    payload.attributes.filter(attr => attr.attribute === customAttributes[index].attribute)[0][event.target.name] = event.target.value;
                }
                customAttributes[index][event.target.name] = event.target.value;

            } else {
                customAttributes[index][event.target.name] = event.target.value;
            }
        } else if (event.target.name === 'isExpression') {
            customAttributes[index][event.target.name] = event.target.checked;
        } else {
            customAttributes[index]['params'][paramIndex]['value'] = event.target.value;
        }
        this.setState({
            customAttributes,
            payload
        })

    }

    addCustomAttribute = () => {
        let customAttributes = cloneDeep(this.state.customAttributes),
            usedAttributesCount,
            remainingAttributesCount;
        customAttributes.push({
            attribute: '',
            displayName: '',
            unit: '',
            functionId: null,
            isExpression: false,
            expression: '',
            params: [],
            isDebug: false,
        });
        usedAttributesCount = this.state.payload.attributes.filter(attr => attr.checked === true && !attr.isCustom).length + customAttributes.length;
        remainingAttributesCount = this.state.totalAttributesCount - usedAttributesCount;
        this.setState({
            usedAttributesCount,
            remainingAttributesCount,
            customAttributes
        })
    }

    deleteCustomAttribute = (index) => {
        let customAttributes = cloneDeep(this.state.customAttributes),
            usedAttributesCount,
            remainingAttributesCount;
        customAttributes.splice(index, 1);
        usedAttributesCount = this.state.payload.attributes.filter(attr => attr.checked === true && !attr.isCustom).length + customAttributes.length;
        remainingAttributesCount = this.state.totalAttributesCount - usedAttributesCount;
        this.setState({
            customAttributes,
            usedAttributesCount,
            remainingAttributesCount
        });
    }

    handleDeviceChange = () => {
        let selectedDeviceId = event.target.value,
            payload = cloneDeep(this.state.payload);
        payload.id = '';
        payload.createdAt = null;
        payload.updatedAt = null;
        this.setState({
            remainingAttributesCount: 0,
            usedAttributesCount: 0,
            isFetchingDeviceDetails: true,
            debugData: [],
            etlData: undefined,
            eltDuration: 5,
            selectedDeviceId,
            payload,
            isTimestampExist: false,
            assetType: this.state.devices.filter(deviceType => deviceType.id === selectedDeviceId)[0]['assetType'],
            isGateway: this.state.devices.filter(deviceType => deviceType.id === selectedDeviceId)[0]['isGateway'] ? true : false,
        }, () => {
            this.props.fetchDeviceStatus(selectedDeviceId);
            this.props.match.params.connectorId && this.props.history.push('/deviceAttributes');
        });
    }

    handleAttributeChange = (event, index) => {
        let payload = JSON.parse(JSON.stringify(this.state.payload)),
            usedAttributesCount,
            remainingAttributesCount;
        if (event.target.name === "selectAttr") {
            if (event.target.checked === false || this.state.remainingAttributesCount != 0) {
                let attr = payload.attributes[index].path.split('.');
                payload.attributes[index].checked = event.target.checked;
                payload.attributes[index]['isLatitude'] = false;
                payload.attributes[index]['isLongitude'] = false;
                payload.attributes[index]['isUnique'] = false;
                payload.attributes[index]['isName'] = false;
                payload.attributes[index].attribute = event.target.checked ? attr.length > 1 ? attr[attr.length - 1] : attr[0] : '';
                payload.attributes[index].displayName = event.target.checked ? attr.length > 1 ? attr[attr.length - 1] : attr[0] : '';
            } else {
                this.setState({
                    modalType: "error",
                    isOpen: true,
                    message2: 'Device attributes quota exceeded !',
                });
            }
        } else if (event.target.name === "displayName" || event.target.name === "unit" || event.target.name === "attribute") {
            if (event.target.name === "attribute") {
                let regex = /^[$A-Z_][A-Z_$]*$/i;
                if (regex.test(event.target.value) || event.target.value == '') {
                    payload.attributes[index][event.target.name] = event.target.value;
                }
            }
            else if (event.target.name === "displayName") {
                payload.attributes[index][event.target.name] = event.target.value.trimStart();

            }

            else {
                payload.attributes[index][event.target.name] = event.target.value;
            }
        }
        usedAttributesCount = payload.attributes.filter(attr => attr.checked === true && !attr.isCustom).length + this.state.customAttributes.length;
        remainingAttributesCount = this.state.totalAttributesCount - usedAttributesCount;
        this.setState({
            payload,
            usedAttributesCount,
            remainingAttributesCount,
        })
    }

    handleAttributeDecorateChange = (valueObj, index) => {
        let payload = JSON.parse(JSON.stringify(this.state.payload)),
            value = valueObj.value;

        payload.attributes[index]['isLatitude'] = false;
        payload.attributes[index]['isLongitude'] = false;
        payload.attributes[index]['isTimestamp'] = false;
        payload.attributes[index]['isUnique'] = false;

        payload.attributes.map(attribute => {
            if (typeof attribute[value] !== 'undefined' && attribute[value]) {
                let attr = attribute.path.split('.');
                attribute[value] = false;
                attribute['attribute'] = attr.length > 1 ? attr[attr.length - 1] : attr[0];
                attribute['displayName'] = attr.length > 1 ? attr[attr.length - 1] : attr[0];
            }
            payload.attributes[index][value] = true;
        });
        if (value === "isLatitude") {
            payload.attributes[index]['attribute'] = 'lat';
            payload.attributes[index]['displayName'] = 'Latitude';
        } else if (value === "isLongitude") {
            payload.attributes[index]['attribute'] = 'lng';
            payload.attributes[index]['displayName'] = 'Longitude';
        } else if (value === "isTimestamp") {
            payload.attributes[index]['attribute'] = 'timestamp';
            payload.attributes[index]['displayName'] = 'timestamp';
        } else if (value === "isUnique") {
            payload.attributes[index]['attribute'] = '_uniqueDeviceId';
        }
        this.setState({
            payload
        });
    }

    getSelectedDecorateOption = (attribute) => {
        if (attribute.isUnique) {
            return {
                label: 'Set as Unique',
                value: 'isUnique',
            }
        } else if (attribute.isLatitude) {
            return {
                label: 'Set as Latitude',
                value: 'isLatitude',
            }
        } else if (attribute.isLongitude) {
            return {
                label: 'Set as Longitude',
                value: 'isLongitude',
            }
        } else if (attribute.isName) {
            return {
                label: 'Set as Name',
                value: 'isName',
            }
        } else if (attribute.isTimestamp) {
            return {
                label: 'Set as Timestamp',
                value: 'isTimestamp',
            }
        } else {
            return null
        }
    }

    getDecorateDataOptions = (attribute) => {
        let options = [];

        if (this.state.isGateway) {
            options.push({
                label: 'Set as Unique',
                value: 'isUnique',
            });
        }
        if (typeof attribute.value === 'string') {
            options.push({
                label: 'Set as Name',
                value: 'isName',
            })
        } else if (typeof attribute.value === 'number') {
            options.push({
                label: 'Set as Latitude',
                value: 'isLatitude',
            }, {
                label: 'Set as Longitude',
                value: 'isLongitude',
            });
            if (!this.state.isTimestampExist) {
                options.push({
                    label: 'Set as Timestamp',
                    value: 'isTimestamp',
                });
            }
        }
        return options;
    }


    onCloseHandler = () => {
        if (this.state.modalType === "success") {
            this.props.fetchDeviceStatus(this.state.selectedDeviceId);
        }
        this.setState({
            isOpen: false,
            message2: '',
            modalType: "",
        })
    }

    inputHandler = (event) => {
        this.setState({
            userInput: event.target.value,
            isConfirm: this.state.givenCaptchaCode === event.target.value ? true : false,
        })
    }

    validateUniqueSelectedAttrName = (rawAttrs, customAttr) => {
        let uniqAttrSet = new Set();
        rawAttrs.forEach(a => {
            if (a.checked && a.attribute) {
                if (a.attribute.length === 0) {
                    return false;
                }
                if (!a.isCustom) {
                    uniqAttrSet.add(a.attribute.toUpperCase())
                }

            }
        });
        customAttr.forEach(a => {
            if (a.attribute.length === 0) {
                return false;
            }
            uniqAttrSet.add(a.attribute.toUpperCase())
        });
        return customAttr.length + rawAttrs.filter(f => (f.checked && !f.isCustom)).length == uniqAttrSet.size
    }

    getPayload = () => {
        let payload = JSON.parse(JSON.stringify(this.state.payload)),
            operations = payload.operations ? payload.operations : [],
            isUniqueAttributesSelected = false;

        payload.connectorId = this.state.selectedDeviceId;
        payload.metaInfo = {};

        payload.attributes.map(attr => {
            if (attr.checked === true) {
                if (attr.attribute === 'timestamp' && !payload.operations) {
                    operations.push(`cast(root.${attr.path}/1000 as timestamp) as timestamp`);
                    payload.metaInfo.timestamp = attr.attribute;
                } else if (attr.isUnique && !payload.operations) {
                    if (typeof attr.value === 'number') {
                        operations.push(`root.${attr.path} as ${attr.attribute}`);
                    } else {
                        operations.push(`root.${attr.path} as ${attr.attribute}`)
                    }
                    operations.push(`split(topic,'/')[1] as gatewayId`);
                    isUniqueAttributesSelected = true;
                } else {
                    if (!attr.isDisabled) {
                        if (/\s/.test(attr.path)) {
                            let pathArr = attr.path.split('.'),
                                newPath = [];
                            pathArr.map(str => {
                                if (/\s/.test(str)) {
                                    let newStr = "`" + str + "`";
                                    newPath.push(newStr);
                                } else {
                                    newPath.push(str);
                                }
                            });
                            operations.push(`root.${newPath.join(".")} as ${attr.attribute.replace(" ", "")}`)
                        } else {
                            operations.push(`root.${attr.path} as ${attr.attribute}`)
                        }

                    }
                }
                if (/\s/.test(attr.attribute)) {
                    attr.attribute = attr.attribute.replace(" ", "");
                }
                if (attr.isLatitude) {
                    payload.metaInfo.lat = attr.attribute;
                }
                if (attr.isLongitude) {
                    payload.metaInfo.lng = attr.attribute;
                }
                if (attr.isName) {
                    payload.metaInfo.name = attr.attribute;
                }
            }
            delete attr.isDisabled
        });

        this.state.customAttributes.map(customAttr => {
            if (!customAttr.isDisabled) {
                let operation;
                if (customAttr.isExpression) {
                    operation = customAttr.expression;
                } else {
                    operation = `${this.state.ETLFunctions.filter(fn => fn.declaration === customAttr.functionId)[0].name}(`;
                    customAttr.params.map((param, index) => {
                        if (index === 0) {
                            operation += `${param.name === 'columnName' ? `root.${param.value}` : param.value}`
                        } else {
                            operation += `, ${param.name === 'columnName' ? `root.${param.value}` : param.value}`
                        }
                    })
                    operation += `)`;
                }
                operations.push(`${operation} as ${customAttr.attribute}`);
                payload.attributes.push({
                    attribute: customAttr.attribute,
                    checked: true,
                    displayName: customAttr.displayName,
                    unit: customAttr.unit,
                    isCustom: true,
                    isExpression: customAttr.isExpression,
                    functionId: customAttr.functionId,
                    expression: customAttr.expression,
                    params: customAttr.params,
                })
            }
        })

        if (!payload.operations && !isUniqueAttributesSelected) {
            operations.push(`split(topic,'/')[1] as _uniqueDeviceId`);
        }
        payload.operations = operations;

        return payload;
    }

    validateCustomAttributes = () => {
        let valid = true;
        this.state.customAttributes.map(attr => {
            if (attr.isExpression) {
                if (attr.expression === '') {
                    valid = false;
                }
            } else {
                if (!attr.functionId) {
                    valid = false;
                } else {
                    attr.params.map(param => {
                        if (typeof param.value === 'undefined' || param.value === '') {
                            valid = false;
                        }
                    })
                }
            }
        })
        return valid;
    }

    handleCreateFastApp = () => {
        if(this.state.isGateway && !this.state.payload.attributes.filter(attr => attr.checked).some(attr => attr.isUnique)){
            this.props.showNotification("error", "Please make atleast one attribute unique.");
            return;
        }
        if (!this.validateUniqueSelectedAttrName(this.state.payload.attributes, this.state.customAttributes)) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: 'Attributes names cannot be empty or Non Unique !',
            });
        } else if (this.state.customAttributes.length > 0 && !this.validateCustomAttributes()) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: 'Please provide all the required details !',
            });
        } else if (this.state.customAttributes.length > 0 && this.state.customAttributes.filter(customAttr => customAttr.isDebug === false).length > 0) {
            let debugPayload = this.getDebugPayload(true);
            this.setState({
                isFetching: true,
                isDebugBeforeSave: true,
            }, () => { this.props.debugDeviceAttributes(debugPayload) });
        } else {
            let payload = this.getPayload();
            this.setState({
                isFetching: true
            }, () => { this.props.createFastApp(payload.id, payload) });
        }
    }

    getDebugPayload = (debugState) => {
        let payload = cloneDeep(this.state.payload),
            operationsDebug = [],
            attributes = [],
            isUniqueAttributesSelected = false;

        payload.attributes.map(attr => {
            if (attr.checked === true && !attr.isCustom) {
                if (attr.isUnique) {
                    if (typeof attr.value === 'number') {
                        operationsDebug.push(`root.${attr.path} as ${attr.attribute}`);
                    } else {
                        operationsDebug.push(`root.${attr.path} as ${attr.attribute}`)
                    }
                    operationsDebug.push(`split(topic,'/')[1] as gatewayId`);
                    isUniqueAttributesSelected = true;
                } else if (attr.attribute === 'timestamp') {
                    operationsDebug.push(`cast(root.${attr.path}/1000 as timestamp) as timestamp`)
                } else {
                    if (/\s/.test(attr.path)) {
                        let pathArr = attr.path.split('.'),
                            newPath = [];

                        pathArr.map(str => {
                            if (/\s/.test(str)) {
                                let newStr = "`" + str + "`";
                                newPath.push(newStr);
                            } else {
                                newPath.push(str);
                            }
                        })
                        operationsDebug.push(`root.${newPath.join(".")} as ${attr.attribute.replace(" ", "")}`)
                    } else {
                        operationsDebug.push(`root.${attr.path} as ${attr.attribute}`)
                    }
                }
                if (/\s/.test(attr.attribute)) {
                    attr.attribute = attr.attribute.replace(" ", "");
                }
                attributes.push(attr);
            }
        });

        this.state.customAttributes.map(customAttr => {
            let operation;
            if (customAttr.isExpression && customAttr.expression && customAttr.attribute) {
                operation = customAttr.expression;
                operationsDebug.push(`${operation} as ${customAttr.attribute}`);
            } else if (customAttr.functionId && customAttr.attribute) {
                let validation = true;
                operation = `${this.state.ETLFunctions.filter(fn => fn.declaration === customAttr.functionId)[0].name}(`;
                customAttr.params.map((param, index) => {
                    validation = param.value ? true : false;
                    if (index === 0) {
                        operation += `${param.name === 'columnName' ? `root.${param.value}` : param.value}`
                    } else {
                        operation += `, ${param.name === 'columnName' ? `root.${param.value}` : param.value}`
                    }
                })
                operation += `)`;
                if (validation) {
                    operationsDebug.push(`${operation} as ${customAttr.attribute}`);
                }
            }
            attributes.push({
                attribute: customAttr.attribute,
                checked: true,
                displayName: customAttr.displayName,
                unit: customAttr.unit,
                isCustom: true,
                isExpression: customAttr.isExpression,
                functionId: customAttr.functionId,
                expression: customAttr.expression,
                params: customAttr.params,
            })
        })
        if (!isUniqueAttributesSelected) {
            operationsDebug.push(`split(topic,'/')[1] as _uniqueDeviceId`);
        }


        let debugPayload = {
            connector: {
                id: this.state.selectedDeviceId,
                properties: {
                    topicId: this.state.devices.filter(device => device.id === this.state.selectedDeviceId)[0].properties.topics[0].topicId,
                }
            },
            definition: [
                {
                    operations: operationsDebug,
                    type: "select"
                }
            ],
            createdFor: "analytics",
            etlConfig: {
                maxOffset: "1000",
                triggerInterval: 5000,
                threshold: 10
            },
            debugState: debugState ? debugState : !this.state.debugState,
            name: `ETL_${this.state.devices.filter(device => device.id === this.state.selectedDeviceId)[0].name}`,
            type: "stream",
            attributes,
        }

        return debugPayload;


    }

    debugData = () => {
        if (!this.validateUniqueSelectedAttrName(this.state.payload.attributes, this.state.customAttributes)) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: 'Attributes names cannot be empty or Non Unique !',
            });
        } else {
            let debugPayload = this.getDebugPayload();
            this.setState({
                debugData: [],
                debugInitiated: true,
                showDebugModal: !this.state.showDebugModal,
            }, () => {
                debugPayload.debugState ? subscribeTopic(`DEBUG_${this.state.selectedDeviceId}`, this.mqttMessageHandler, null) : unsubscribeTopic(`DEBUG_${this.state.selectedDeviceId}`, "doNotCloseConnection");
                this.props.debugDeviceAttributes(debugPayload);
            });
        }
    }

    closeSniffingModal = () => {
        this.debugData();
    }

    mqttMessageHandler = (message) => {
        try {
            let debugData = this.state.debugData,
                customAttributes = this.state.customAttributes;
            if (message.data.length > 0) {
                if (debugData.length === 0 && customAttributes.length > 0) {
                    customAttributes.map(attributeObj => {
                        if (message.data[0].hasOwnProperty(attributeObj.attribute)) {
                            attributeObj.isDebug = true;
                        }
                    })
                }
                debugData.unshift(message.data[0]);
                debugData.length == 60 && debugData.splice(59);
                this.setState({
                    debugData,
                    customAttributes,
                    debugStatus: message.status
                })
            }
        } catch (e) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: 'The debug data is not a valid JSON data format.',
            })
        }
    }

    copyToClipboard = (id) => {
        let copyText = document.getElementById(id);
        copyText.select();
        document.execCommand("copy");
        let element = $(`#Copied${id[id.length - 1]}`)[0];
        ReactTooltip.show(element)
        setTimeout(() => ReactTooltip.hide(element), 2500)
    }

    isEnableDebugAttributes = () => {
        let enableDebug;
        enableDebug = this.state.payload.attributes.filter(attr => attr.checked).length > 0 ? true : false;
        return enableDebug;
    }

    getTableData = () => {
        let debugData = this.state.debugData,
            data = [];
        debugData.map(dataObj => {
            let obj = {};
            Object.keys(dataObj).map(key => {
                if (typeof dataObj[key] === 'object') {
                    Object.keys(dataObj[key]).map(subKey => {
                        obj[subKey] = dataObj[key][subKey];
                    })
                } else {
                    obj[key] = key === 'timestamp' ? new Date(dataObj[key]).toLocaleString('en-US', { timeZone: localStorage.timeZone }) : dataObj[key];
                }

            });
            data.push(obj);
        })
        return data;
    }

    getColumns = () => {
        let dataObj = this.state.debugData[0],
            columns = [];
        Object.keys(dataObj).map(key => {
            if (typeof dataObj[key] === 'object') {
                Object.keys(dataObj[key]).map(subKey => {
                    columns.push({
                        headerName: subKey,
                        field: subKey,
                    })
                })
            } else {
                columns.push({
                    headerName: key,
                    field: key,
                })
            }
        });
        return columns;
    }

    getETLFunctionsDropdownOptions = () => {
        let eTLFunctions = this.state.ETLFunctions,
            options = [];
        eTLFunctions.map(fn => {
            options.push({
                label: fn.declaration,
                value: fn.declaration,
            })
        });
        return options;
    }

    getAttributeOptions = (param) => {
        let options = [];
        this.state.payload.attributes.map(attribute => {
            if (!attribute.isCustom) {
                if (param.type === 'number' || param.type === 'double' || param.type === 'long' || param.type === 'int' || param.type === 'integer') {
                    if (typeof attribute.value === 'number' && attribute.displayName !== 'timestamp') {
                        options.push({
                            label: attribute.path,
                            value: attribute.path,
                        })
                    }
                } else if (param.type === 'timestamp') {
                    if (attribute.displayName === 'timestamp') {
                        options.push({
                            label: attribute.path,
                            value: attribute.path,
                        })
                    }

                } else if (param.type === 'string') {
                    if (typeof attribute.value === 'string') {
                        options.push({
                            label: attribute.path,
                            value: attribute.path,
                        })
                    }
                } else {
                    options.push({
                        label: attribute.path,
                        value: attribute.path,
                    })
                }

            }
        });
        return options;
    }

    resetDeviceAttributes = () => {
        this.setState({
            isFetchingDeviceDetails: true,
            refreshAttributesBtnDisabled: true,
            refreshBtnDisabled: true
        }, () => {
            this.props.resetDeviceAttributes(this.state.selectedDeviceId);
        });

    }

    refreshDeviceAttributes = () => {
        this.setState({
            isFetchingDeviceDetails: true,
        }, () => { this.props.fetchDeviceStatus(this.state.selectedDeviceId) });
    }

    deviceTypeStats = () => {
        if (this.state.etlData === undefined && this.state.selectedDeviceId) {
            this.props.etEtlStatsData(this.state.selectedDeviceId, 5);
        }
    }

    verifySchema = () => {
        this.setState({
            schemaVerificationInProgress: true
        }, () => { this.props.verifySchema(this.state.selectedDeviceId) });
    }

    handleRefreshAttributesTimer = () => {
        let startTime = 0;
        let _this = this;
        let disableTimer = setInterval(function () {
            if (startTime === 15) {
                clearInterval(disableTimer);
                _this.setState({
                    refreshAttributesBtnDisabled: false
                }, () => { _this.refreshDeviceAttributes() });
                return;
            }
            document.getElementById("timer").innerText = 'Reset attributes will be enabled after: ' + (15 - startTime) + ' seconds';
            startTime += 1;
        }, 1000);
    }

    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>Device Attributes</title>
                    <meta name="description" content="M83-Device Attributes" />
                </Helmet>

                <header className="content-header d-flex">
                    <div className="flex-60">
                        <h6>Device Attributes -
                            <span className="content-header-badge-group">
                                <span className="content-header-badge-item">Allocated <span
                                    className="badge badge-pill badge-primary">{this.state.totalAttributesCount} </span></span>
                                <span className="content-header-badge-item">Used <span
                                    className="badge badge-pill badge-success">{this.state.usedAttributesCount} </span></span>
                                <span className="content-header-badge-item">Remaining <span
                                    className="badge badge-pill badge-warning">{this.state.remainingAttributesCount} </span></span>
                            </span>
                        </h6>
                    </div>
                    <div className="flex-40 text-right">
                        <div className="content-header-group">
                            <button className="btn btn-light" disabled={this.state.isFetching || this.state.refreshBtnDisabled || this.state.debugInitiated} data-tooltip data-tooltip-text="Refresh" data-tooltip-place="bottom" onClick={this.refreshComponent}><i className="far fa-sync-alt"></i></button>
                        </div>
                    </div>
                </header>

                {this.state.isFetching ? <Loader /> :
                    this.state.devices.length > 0 ?
                        <div className="content-body" id="data-attributes">
                            <div className="form-content-wrapper">
                                <div className="form-content-box">
                                    <div className="form-content-header">
                                        <p>Basic Information
                                            {this.state.payload.createdAt && <small>Last Updated : {getTimeDifference(this.state.payload.updatedAt ? this.state.payload.updatedAt : this.state.payload.createdAt)}</small>}
                                        </p>
                                    </div>

                                    <div className="form-content-body">
                                        <div className="form-group">
                                            <label className="form-group-label">Device Type : <i className="fas fa-asterisk form-group-required"></i></label>
                                            <div className="input-group">
                                                <div className="input-group-prepend">
                                                    {this.state.devices.filter(device => device.id === this.state.selectedDeviceId)[0]['assetType'] ?
                                                        <React.Fragment>
                                                            {this.state.devices.filter(device => device.id === this.state.selectedDeviceId)[0]['assetType'] === "Fixed" ?
                                                                <span className="input-group-text text-orange fw-600 bg-white" style={{minWidth: 100}}>
                                                                    <i className="fas fa-map-marker-alt mr-2"></i>
                                                                    {this.state.devices.filter(device => device.id === this.state.selectedDeviceId)[0]['assetType']}
                                                                </span>:
                                                                <span className="input-group-text text-info fw-600 bg-white" style={{minWidth: 100}}>
                                                                    <i className="fas fa-route mr-2"></i>
                                                                    {this.state.devices.filter(device => device.id === this.state.selectedDeviceId)[0]['assetType']}
                                                                </span>
                                                            }
                                                        </React.Fragment> : 'N/A'
                                                    }
                                                    
                                                </div>
                                                <select className="form-control" value={this.state.selectedDeviceId} onChange={this.handleDeviceChange}>
                                                    {this.state.devices.map(device =>
                                                        <option key={device.id} value={device.id}>{device.name}</option>
                                                    )}
                                                </select>
                                                <div className="input-group-append">
                                                    {this.state.schemaVerificationInProgress ?
                                                        <button className="btn btn-transparent-blue" data-tooltip data-tooltip-text="Verifing Schema" data-tooltip-place="bottom" disabled={true}>
                                                            <i className="far fa-cog fa-spin"></i>
                                                        </button> :
                                                        <button className="btn btn-transparent-blue" data-tooltip data-tooltip-text="Refresh Schema/JSON " data-tooltip-place="bottom" disabled={typeof this.state.payload.id === 'undefined' || this.state.payload.id === '' || this.state.payload.attributes.length == 0 || this.state.showDebugModal} onClick={this.verifySchema}>
                                                            <i className="far fa-redo"></i>
                                                        </button>
                                                    }
                                                    <button className="btn btn-transparent-green" disabled={!this.state.payload.id} onClick={this.deviceTypeStats} data-tooltip data-tooltip-text="Statistics" data-tooltip-place="bottom" data-toggle="modal" data-target="#graphModal">
                                                        <i className="far fa-chart-bar"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="form-content-box">
                                    <div className="form-content-header">
                                        <p>Raw Attributes <strong className="text-indigo">({this.state.payload.attributes.length})</strong>
                                            <small className="float-none ml-3"><sup>*</sup>Array Datatype Not Supported. </small>
                                            {!this.state.refreshAttributesBtnDisabled ?
                                                <button className="btn btn-link" style={{right: 90 }} disabled={this.state.payload.id || this.state.debugInitiated || this.state.debugState || this.state.isFetchingDeviceDetails} onClick={this.resetDeviceAttributes}>
                                                    <i className="far fa-redo-alt"></i>Reset
                                                </button>
                                                :
                                                <span id="timer" className="form-content-error" style={{right: 120 }}></span>
                                            }
                                            <button className="btn btn-link" disabled={this.state.payload.id || this.state.debugInitiated || this.state.debugState || this.state.isFetchingDeviceDetails} onClick={this.refreshDeviceAttributes}>
                                                <i className="far fa-sync-alt"></i>Refresh
                                            </button>
                                        </p>
                                    </div>
                                    <div className="form-content-body">
                                        {this.state.isFetchingDeviceDetails ?
                                            <div className="inner-loader-wrapper" style={{ height: 320 }}>
                                                <div className="inner-loader-content">
                                                    <i className="fad fa-sync-alt fa-spin f-20"></i>
                                                </div>
                                            </div> :
                                            this.state.payload.attributes.length > 0 ?
                                                <div className="form-group mb-0">
                                                    <div className="attribute-list">
                                                        <div className="attribute-list-header d-flex">
                                                            <p className="flex-20">Attribute Path</p>
                                                            <p className="flex-10">Data Type</p>
                                                            <p className="flex-10">Value</p>
                                                            <p className="flex-17_5">Attribute Name</p>
                                                            <p className="flex-17_5">Display Name</p>
                                                            <p className="flex-10">Unit</p>
                                                            <p className="flex-15">Decorate Data</p>

                                                        </div>
                                                        <div className="attribute-list-body">
                                                            {this.state.payload.attributes.sort((a, b) => a.path !== b.path ? a.path < b.path ? -1 : 1 : 0).map((attribute, index) =>
                                                                !attribute.isCustom &&
                                                                <ul className="list-style-none d-flex align-items-center"
                                                                    key={index}>
                                                                    <li className="flex-20">
                                                                        <label className={`check-box ${attribute.isDisabled && 'check-box-disabled'}`}>
                                                                            <span className="check-text">{attribute.path}</span>
                                                                            <input
                                                                                type="checkbox"
                                                                                name="selectAttr"
                                                                                disabled={attribute.isDisabled}
                                                                                checked={attribute.checked}
                                                                                onChange={(e) => {
                                                                                    this.handleAttributeChange(e, index)
                                                                                }} />
                                                                            <span className="check-mark"></span>
                                                                        </label>
                                                                    </li>
                                                                    <li className="flex-10">
                                                                        <input
                                                                            type="text"
                                                                            name="dataType"
                                                                            className="form-control"
                                                                            disabled={true}
                                                                            value={typeof attribute.value === 'number' ? 'Numeric' : (typeof attribute.value === 'string' ? 'String' : 'Boolean')}
                                                                        />
                                                                    </li>
                                                                    <li className="flex-10">
                                                                        <input
                                                                            type="text"
                                                                            name="dataValue"
                                                                            className="form-control"
                                                                            disabled={true}
                                                                            value={attribute.value}
                                                                        />
                                                                    </li>
                                                                    <li className="flex-17_5">
                                                                        <input
                                                                            type="text"
                                                                            name="attribute"
                                                                            className="form-control"
                                                                            disabled={attribute.attribute === "timestamp" || attribute.isDisabled || !attribute.checked || attribute.isLongitude || attribute.isLatitude || attribute.isUnique}
                                                                            value={attribute.attribute}
                                                                            onChange={(e) => {
                                                                                this.handleAttributeChange(e, index)
                                                                            }}
                                                                        />
                                                                    </li>
                                                                    <li className="flex-17_5">
                                                                        <input
                                                                            type="text"
                                                                            name="displayName"
                                                                            className="form-control"
                                                                            maxLength="30"
                                                                            //disabled={attribute.isDisabled || !attribute.checked}
                                                                            disabled={attribute.displayName === "timestamp"}
                                                                            value={attribute.displayName}
                                                                            onChange={(e) => {
                                                                                this.handleAttributeChange(e, index)
                                                                            }}
                                                                        />
                                                                    </li>
                                                                    <li className="flex-10">
                                                                        <input
                                                                            placeholder={attribute.displayName === 'timestamp' ? "" : "e.g. Volt"}
                                                                            type="text"
                                                                            name="unit"
                                                                            className="form-control"
                                                                            //disabled={attribute.isDisabled || !attribute.checked}
                                                                            disabled={attribute.displayName === "timestamp"}
                                                                            value={attribute.displayName === 'timestamp' ? '' : attribute.unit}
                                                                            onChange={(e) => {
                                                                                this.handleAttributeChange(e, index)
                                                                            }}
                                                                        />
                                                                    </li>
                                                                    <li className="flex-15">
                                                                        <ReactSelect
                                                                            className="form-control-multi-select"
                                                                            name="decorateData"
                                                                            options={this.getDecorateDataOptions(attribute)}
                                                                            isMulti={false}
                                                                            isDisabled={attribute.checked === false || attribute.displayName === "timestamp" || this.state.payload.id}
                                                                            value={this.getSelectedDecorateOption(attribute)}
                                                                            onChange={(value) => this.handleAttributeDecorateChange(value, index)}
                                                                        >
                                                                        </ReactSelect>
                                                                    </li>
                                                                </ul>
                                                            )}
                                                        </div>
                                                    </div>
                                                </div> :
                                                this.state.payload && this.state.payload.error ?
                                                    <div className="attribute-empty">
                                                        <div>
                                                            <i className="fad fa-align-slash text-red"></i>
                                                            <p>{this.state.payload.error}</p>
                                                        </div>
                                                    </div> :
                                                    <div className="attribute-empty">
                                                        <div>
                                                            <i className="fad fa-webcam-slash text-red"></i>
                                                            <p>Your Device is currently Offline/Not Reporting.</p>
                                                            <p>In case your device has started reporting, then wait for the first packet from your device to reach the system.</p>
                                                            <p>You may also use Refresh Device Attributes button in case you feel that device is reporting and system is not picking.</p>
                                                            <p>This depends on the Reporting Interval of your Device.</p>
                                                        </div>
                                                    </div>
                                        }
                                    </div>
                                </div>

                                {this.state.payload.attributes.length > 0 ?
                                    <div className="form-content-box">
                                        <div className="form-content-header">
                                            <p>Synthetic Attributes</p>
                                        </div>
                                        <div className="form-content-body">
                                            {this.state.customAttributes.length > 0 ?
                                                <React.Fragment>
                                                    {this.state.customAttributes.map((customAttr, index) =>
                                                        <ul className="list-style-none d-flex add-attribute-box" key={index}>
                                                            <h6>Attribute {index + 1} :</h6>
                                                            <li className="flex-40">
                                                                <div className="form-group">
                                                                    <label className="form-group-label">Name : <i className="fas fa-asterisk form-group-required"></i></label>
                                                                    <input
                                                                        placeholder="e.g. power"
                                                                        type="text"
                                                                        name="attribute"
                                                                        className="form-control"
                                                                        disabled={customAttr.isDisabled}
                                                                        value={customAttr.attribute}
                                                                        onChange={(e) => this.customAttrChangeHandler(e, index)}
                                                                    />
                                                                </div>
                                                            </li>
                                                            <li className="flex-40">
                                                                <div className="form-group">
                                                                    <label className="form-group-label">Display Name :</label>
                                                                    <input
                                                                        placeholder="e.g. Power"
                                                                        type="text"
                                                                        name="displayName"
                                                                        className="form-control"
                                                                        //disabled={customAttr.isDisabled}
                                                                        value={customAttr.displayName}
                                                                        onChange={(e) => this.customAttrChangeHandler(e, index)}
                                                                    />
                                                                </div>
                                                            </li>
                                                            <li className="flex-20">
                                                                <div className="form-group">
                                                                    <label className="form-group-label">Unit :</label>
                                                                    <input
                                                                        placeholder="e.g. Watt"
                                                                        type="text"
                                                                        name="unit"
                                                                        className="form-control"
                                                                        //disabled={customAttr.isDisabled}
                                                                        value={customAttr.unit}
                                                                        onChange={(e) => this.customAttrChangeHandler(e, index)}
                                                                    />
                                                                </div>
                                                            </li>
                                                            <li className="flex-5">
                                                                <div className="form-group mb-0">
                                                                    <label className="form-group-label">Exp ?</label>
                                                                    <label className={`check-box mt-2 ${customAttr.isDisabled && 'check-box-disabled'}`}>
                                                                        <input
                                                                            type="checkbox"
                                                                            name="isExpression"
                                                                            className="form-control"
                                                                            disabled={customAttr.isDisabled}
                                                                            checked={customAttr.isExpression}
                                                                            onChange={(e) => this.customAttrChangeHandler(e, index)}
                                                                        />
                                                                        <span className="check-mark"></span>
                                                                    </label>
                                                                </div>
                                                            </li>
                                                            {customAttr.isExpression ?
                                                                <li className="flex-25">
                                                                    <div className="form-group mb-0">
                                                                        <label className="form-group-label">Function : <i className="fas fa-asterisk form-group-required"></i></label>
                                                                        <input type="text" name="functionId" disabled value="expression" className="form-control" />
                                                                    </div>
                                                                </li> :
                                                                <li className="flex-40">
                                                                    <div className="form-group mb-0">
                                                                        <label className="form-group-label">Function : <i className="fas fa-asterisk form-group-required"></i></label>
                                                                        <ReactSelect
                                                                            className="form-control-multi-select"
                                                                            name="functionId"
                                                                            value={customAttr.functionId ? {
                                                                                label: customAttr.functionId,
                                                                                value: customAttr.functionId
                                                                            } : {}}
                                                                            onChange={(value, type) => this.customAttrETLFnChangeHandler(value, type, index)}
                                                                            options={this.getETLFunctionsDropdownOptions()}
                                                                            isMulti={false}
                                                                            isDisabled={customAttr.isDisabled}
                                                                        >
                                                                        </ReactSelect>
                                                                    </div>
                                                                </li>
                                                            }
                                                            {customAttr.isExpression ?
                                                                <li className="flex-70">
                                                                    <div className="form-group mb-0">
                                                                        <label className="form-group-label">Expression : <i className="fas fa-asterisk form-group-required"></i>
                                                                            <a href="https://spark.apache.org/docs/2.4.6/api/sql/index.html#window" target="_blank" data-tooltip data-tooltip-text="View Spark Functions" data-tooltip-place="bottom">
                                                                                <i className="far fa-info-circle ml-2"></i>
                                                                            </a>
                                                                            <span className="float-right f-10 text-cyan">*Do not use "select" as it is already provided by the system</span>
                                                                        </label>
                                                                        <ReactTextareaAutocomplete
                                                                            className="form-control"
                                                                            name="expression"
                                                                            placeholder="Type @a for Attributes and :f for Functions. eg: power is (i * i * r) so here: (readings.current * readings.current * readings.resistance)"
                                                                            value={customAttr.expression}
                                                                            disabled={customAttr.isDisabled}
                                                                            onChange={e => this.customAttrChangeHandler(e, index)}
                                                                            loadingComponent={() => <span>Loading</span>}
                                                                            trigger={{
                                                                                "@": {
                                                                                    dataProvider: (token) => { return this.state.payload.attributes },
                                                                                    component: ({ entity: { path } }) => <div>{path}</div>,
                                                                                    output: (item, trigger) => `root.${item.path}`
                                                                                },
                                                                                ":": {
                                                                                    dataProvider: (token) => { return this.state.ETLFunctions },
                                                                                    component: ({ entity: { name } }) => <div>{name}</div>,
                                                                                    output: (item, trigger) => `${item.declaration}`
                                                                                }
                                                                            }}
                                                                        />
                                                                    </div>
                                                                </li> :
                                                                <React.Fragment>
                                                                    {customAttr.params.length > 0 ? customAttr.params.map((param, paramIndex) =>
                                                                        <li key={paramIndex} className={customAttr.params.length === 1 ? 'flex-55' : customAttr.params.length === 2 ? 'flex-27_5' : 'flex-18_3'}>
                                                                            <div className="form-group mb-0">
                                                                                <label className="form-group-label">Argument {paramIndex + 1} : <i className="fas fa-asterisk form-group-required"></i></label>
                                                                                {param.name === 'columnName' ?
                                                                                    <ReactSelect
                                                                                        className="form-control-multi-select"
                                                                                        value={param.value ? { label: param.value, value: param.value } : {}}
                                                                                        onChange={(value, type) => this.attributeSelectionChangeHandler(value, type, index, paramIndex)}
                                                                                        options={this.getAttributeOptions(param)}
                                                                                        isMulti={false}
                                                                                        isDisabled={customAttr.isDisabled}
                                                                                    >
                                                                                    </ReactSelect> :
                                                                                    <input
                                                                                        type={customAttr.params[paramIndex].type === 'double' ? 'number' : 'text'}
                                                                                        className="form-control"
                                                                                        placeholder={customAttr.params[paramIndex].type === 'double' ? customAttr.params[paramIndex].name + ' (Numeric Value)' : customAttr.params[paramIndex].name + ' (String Value)'}
                                                                                        disabled={customAttr.isDisabled}
                                                                                        value={param.value}
                                                                                        onChange={(e) => this.customAttrChangeHandler(e, index, paramIndex)}
                                                                                    />
                                                                                }
                                                                            </div>
                                                                        </li>
                                                                    ) :
                                                                        <li className="flex-55">
                                                                            <div className="form-group mb-0">
                                                                                <label className="form-group-label">Function Arguments : <i className="fas fa-asterisk form-group-required"></i></label>
                                                                                <input type="text" className="form-control" disabled />
                                                                            </div>
                                                                        </li>
                                                                    }
                                                                </React.Fragment>
                                                            }
                                                            <button className="btn btn-light box-top-right-button" data-tooltip data-tooltip-text="Remove" data-tooltip-place="bottom" disabled={customAttr.isDisabled} onClick={() => { this.deleteCustomAttribute(index) }}><i className="far fa-times"></i></button>
                                                        </ul>
                                                    )}
                                                    <div className="text-center">
                                                        <button className="btn btn-link" disabled={this.state.isFetchingDeviceDetails || this.state.remainingAttributesCount == 0} onClick={() => { this.addCustomAttribute() }}>
                                                            <i className="far fa-plus"></i>Add Attribute
                                                        </button>
                                                    </div>
                                                </React.Fragment> :
                                                <div className="text-center p-4">
                                                    <button className="btn-link" disabled={this.state.isFetchingDeviceDetails || this.state.remainingAttributesCount == 0} onClick={() => { this.addCustomAttribute() }}>
                                                        <i className="far fa-plus mr-r-5"></i>Add Attribute
                                                    </button>
                                                </div>
                                            }
                                        </div>
                                    </div>
                                    : ""
                                }
                            </div>

                            <div className="form-info-wrapper">
                                <div className="form-info-body">
                                    <div className="form-info-icon">
                                        <img src="https://content.iot83.com/m83/dataConnectors/mqtt.png" />
                                    </div>
                                    <div className="alert alert-info note-text">
                                        <p><strong>Note :</strong> Data is being picked from the Telemetry Topic <strong>"/report"</strong></p>
                                    </div>
                                    <h5>Raw Attributes</h5>
                                    <ul className="list-style-none form-info-list mb-4">
                                        <li><p>Attributes reported in the device JSON that are available for selection so that they can be viewed as time series variables</p></li>
                                    </ul>
                                    <h5>Synthetic Attributes</h5>
                                    <ul className="list-style-none form-info-list mb-4">
                                        <li><p>Attributes that are derived by applying mathematical or statistical formulae on the device variables.</p></li>
                                        <li><p>For example, if a device is reporting current (I) and voltage (V), then power (W) which is current (I) * voltage (V) is a custom attribute.</p></li>
                                    </ul>
                                    <h5>Refresh Schema/JSON</h5>
                                    <ul className="list-style-none form-info-list mb-4">
                                        <li><p>In case you want to change the Definition of your Device Type by making changes to your JSON, you can easily do so.</p></li>
                                        <li><p>1. If your old JSON is a subset of the new JSON, all your old data will be retained and you will be presented with the newly added attributes in your device definition.</p></li>
                                        <li><p>2. If your old JSON is NOT a subset of the new JSON, and contains lesser attributes or changed names, then all your old data will be deleted and you will have to reconfigure from scratch.</p></li>
                                    </ul>
                                    <h5>Refresh Attributes</h5>
                                    <ul className="list-style-none form-info-list mb-4">
                                        <li><p>This button can be used in case you feel that the device is reporting and the system is not showing your attributes. Please be informed that the time for showing attributes depends on the Reporting interval of the Device.</p></li>
                                    </ul>
                                    {this.state.customAttributes.filter(attr => attr.isExpression === false).length > 0 &&
                                        <React.Fragment>
                                            <h5>Selected Function(s)</h5>
                                            <ul className="list-style-none form-info-list mb-4">
                                                {this.state.customAttributes.map((attribute, index) =>
                                                    attribute.functionId &&
                                                    <li className="form-info-list-selected" key={index}>
                                                        <p><strong className="text-theme fw-600 mr-r-5">{attribute.functionId} :</strong></p>
                                                        <p>{this.state.ETLFunctions.filter(fn => fn.declaration === attribute.functionId)[0].description}</p>
                                                    </li>
                                                )}
                                            </ul>
                                        </React.Fragment>
                                    }

                                </div>
                                <div className="form-info-footer">
                                    {this.isEnableDebugAttributes() ?
                                        <button className="btn btn-warning" disabled={this.state.debugInitiated} onClick={() => { this.debugData() }}>
                                            {this.state.debugInitiated ? !this.state.debugState ? 'Debugging...' : 'Stopping...' :
                                                <React.Fragment>{this.state.debugState ? <i className="fad fa-stop mr-r-5"></i> : <i className="fad fa-play mr-r-5"></i>} Debug</React.Fragment>
                                            }
                                        </button> :
                                        <button type="button" className="btn btn-warning" disabled={true}><i className="fad fa-bug mr-r-5"></i>Debug</button>
                                    }
                                    <button className="btn btn-primary" disabled={this.state.debugInitiated || this.state.debugState || this.state.payload.attributes.length === 0} onClick={this.handleCreateFastApp}>{this.state.payload.id ? 'Update' : 'Save'}</button>
                                </div>
                            </div>

                        </div>
                        :
                        <div className="content-body">
                            <AddNewButton
                                text1="No device attribute(s) available"
                                text2="You haven't created any device type(s) yet. Please create a device type first."
                                addButtonEnable={isNavAssigned("deviceTypes")}
                                createItemOnAddButtonClick={() => {
                                    this.props.history.push(`/addOrEditDeviceType`);
                                }}
                                imageIcon="addDeviceAttribute.png"
                            />
                        </div>
                }

                {/* debug window*/}
                {this.state.showDebugModal &&
                    <div className="debug-wrapper animated slideInUp">
                        <div className="debug-header">
                            <h6>Debug</h6>
                            <div className="button-group">
                                {this.state.debugData.length > 0 && this.state.debugStatus !== 'Failed' &&
                                    <React.Fragment>
                                        <button className={`btn btn-light ${this.state.dataView === 'TABLE' ? 'active' : ''}`} data-tooltip data-tooltip-text="Table View" data-tooltip-place="bottom" onClick={() => this.setState({ dataView: 'TABLE' })}>
                                            <i className="far fa-file-alt"></i>
                                        </button>
                                        <button className={`btn btn-light ${this.state.dataView === 'TREE' ? 'active' : ''}`} data-tooltip data-tooltip-text="Tree View" data-tooltip-place="bottom" onClick={() => this.setState({ dataView: 'TREE' })}>
                                            <i className="far fa-folder-tree"></i>
                                        </button>
                                        <button className="btn btn-light" data-tooltip data-tooltip-text="Clear" data-tooltip-place="bottom" onClick={() => { this.setState({ debugData: [] }) }}>
                                            <i className="far fa-broom"></i>
                                        </button>
                                    </React.Fragment>
                                }
                                <button className="btn btn-light" data-tooltip data-tooltip-text="Close" data-tooltip-place="bottom" onClick={() => { this.debugData() }}>
                                    <i className="far fa-times"></i>
                                </button>
                            </div>
                        </div>

                        <div className="debug-body">
                            {this.state.debugData.length === 0 ?
                                <div className="inner-loader-wrapper h-100">
                                    <div className="inner-loader-content">
                                        <h6>Debugging...</h6>
                                        <img src="https://content.iot83.com/m83/misc/uploading.gif" />
                                        <p className="text-gray">Please be patient. This may take few seconds.</p>
                                    </div>
                                </div> :
                                <div className="debug-table-wrapper">
                                    {this.state.debugStatus !== 'Failed' ?
                                        this.state.dataView === 'TABLE' ?
                                            <div className="ag-theme-alpine" style={{ height: 300 }}>
                                                <AgGridReact
                                                    columnDefs={this.getColumns()}
                                                    rowData={this.getTableData()}
                                                    animateRows={true}
                                                    pagination={true}
                                                    paginationPageSize={5}
                                                >
                                                </AgGridReact>
                                            </div> :
                                            <div className="overflow-y-auto" style={{ height: 300 }}>
                                                {this.state.debugData.map((messageObj, index) =>
                                                    <ReactJson key={index} src={messageObj} />)
                                                }
                                            </div> :
                                        <div className="text-center mt-5">
                                            <h5 className="text-red fw-600">ERROR !</h5>
                                            <h6 className="text-red">{this.state.debugData} !</h6>
                                        </div>
                                    }
                                </div>
                            }
                        </div>
                    </div>
                }
                {/* end debug window */}

                {/* refresh schema modal */}
                {this.state.refreshSchemaConfirmationModal &&
                    <div className="modal show d-block animated slideInDown" id="deleteModal">
                        <div className="modal-dialog modal-dialog-centered">
                            <div className="modal-content">
                                <div className="modal-body">
                                    <div className="delete-content">
                                        <div className="delete-icon">
                                            <i className="fad fa-retweet-alt text-primary"></i>
                                        </div>
                                        <h4 className="text-primary">Refresh Schema !</h4>
                                        <h6>Are you sure you want to refresh schema for <strong className="text-gray">{this.state.devices.filter(device => device.id === this.state.selectedDeviceId)[0].name} </strong> ?</h6>
                                        <h6>All dashboards, events and applications associated with this device will be deleted !</h6>
                                        <h6 className="text-red">It will delete all your existing data, This action can't be undone !</h6>
                                        <div className="form-group captcha-box">
                                            <ClientCaptcha captchaClassName="captcha-text" captchaCode={code => this.setState({ givenCaptchaCode: code, userInput: '', isConfirm: false })} />
                                            <input type="text" className="form-control" value={this.state.userInput} placeholder="Enter captcha" onChange={() => this.inputHandler(event)}></input>
                                        </div>
                                    </div>
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-dark" data-dismiss="modal" onClick={() => { this.setState({ userInput: '', isConfirm: false, refreshSchemaConfirmationModal: false, userConsentToken: null }) }}>Cancel</button>
                                    <button type="button" className="btn btn-success" data-dismiss="modal" disabled={!this.state.isConfirm} onClick={() => { this.setState({ isFetching: true, refreshSchemaConfirmationModal: false, debugData: [] }, () => { this.props.refreshSchema(this.state.selectedDeviceId, this.state.userConsentToken) }) }}>Refresh</button>
                                </div>
                            </div>
                        </div>
                    </div>
                }
                {/* end refresh schema modal */}

                {/* etl graph modal */}
                <div className="modal animated slideInDown" id="graphModal" data-backdrop="static">
                    <div className="modal-dialog modal-xl">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h6 className="modal-title">Stats for - <span>{this.state.devices.find(val => val.id === this.state.selectedDeviceId) ? this.state.devices.find(val => val.id === this.state.selectedDeviceId).name : "NA"}</span>
                                    <button type="button" className="close" data-dismiss="modal" data-tooltip data-tooltip-text="Close" data-tooltip-place="bottom"><i className="far fa-times"></i></button>
                                </h6>
                                <div className="form-group mb-0 mr-r-45">
                                    <select disabled={this.state.etlData === undefined} className="form-control" value={this.state.eltDuration} onChange={(e) => {
                                        this.setState({
                                            eltDuration: e.target.value,
                                            etlData: undefined
                                        }, () => this.props.etEtlStatsData(this.state.selectedDeviceId, this.state.eltDuration))
                                    }}>
                                        <option value={5}>Last 5 mins</option>
                                        <option value={15}>Last 15 mins</option>
                                        <option value={30}>Last 30 mins</option>
                                        <option value={60}>Last 1 hour</option>
                                        <option value={180}>Last 3 hours</option>
                                    </select>
                                </div>
                                <ul className="modal-header-list list-style-none">
                                    <li className="btn btn-light" disabled={this.state.etlData === undefined} onClick={() => {
                                        this.setState({
                                            etlData: undefined
                                        }, () => this.props.etEtlStatsData(this.state.selectedDeviceId, this.state.eltDuration))
                                    }} data-tooltip data-tooltip-text="Refresh" data-tooltip-place="bottom">
                                        <i className="far fa-sync-alt"></i>
                                    </li>
                                </ul>
                            </div>
                            <div className="modal-body p-0">
                                <ul className="list-style-none d-flex stats-chart-list">
                                    <li className="flex-50">
                                        <div className="card">
                                            <div className="card-header">
                                                <h6>Input Rows/sec</h6>
                                            </div>
                                            <div className="card-body">
                                                {this.state.etlData ? this.state.etlData.length ?
                                                    <div className="card-chart-box">
                                                        <StatsChart timeZone={this.props.timeZone} name="Input Rows/sec" divId="inputRowsPerSecond" data={this.state.etlData} />
                                                    </div> :
                                                    <div className="card-message">
                                                        <p>There is no data to display !</p>
                                                    </div> :
                                                    <div className="card-loader">
                                                        <i className="fad fa-sync-alt fa-spin"></i>
                                                    </div>
                                                }
                                            </div>
                                        </div>
                                    </li>
                                    <li className="flex-50">
                                        <div className="card">
                                            <div className="card-header">
                                                <h6>Processed Rows/sec</h6>
                                            </div>
                                            <div className="card-body">
                                                {this.state.etlData ? this.state.etlData.length ?
                                                    <div className="card-chart-box">
                                                        <StatsChart timeZone={this.props.timeZone} name="Processed Rows/sec" divId="processedRowsPerSecond" data={this.state.etlData} />
                                                    </div> :
                                                    <div className="card-message">
                                                        <p>There is no data to display !</p>
                                                    </div> :
                                                    <div className="card-loader">
                                                        <i className="fad fa-sync-alt fa-spin"></i>
                                                    </div>
                                                }
                                            </div>
                                        </div>
                                    </li>
                                    <li className="flex-50">
                                        <div className="card">
                                            <div className="card-header">
                                                <h6>No. of Input Rows</h6>
                                            </div>
                                            <div className="card-body">
                                                {this.state.etlData ? this.state.etlData.length ?
                                                    <div className="card-chart-box">
                                                        <StatsChart timeZone={this.props.timeZone} name="No. of Input Rows" divId="numInputRows" data={this.state.etlData} />
                                                    </div> :
                                                    <div className="card-message">
                                                        <p>There is no data to display !</p>
                                                    </div> :
                                                    <div className="card-loader">
                                                        <i className="fad fa-sync-alt fa-spin"></i>
                                                    </div>
                                                }
                                            </div>
                                        </div>
                                    </li>
                                    <li className="flex-50">
                                        <div className="card">
                                            <div className="card-header">
                                                <h6>Trigger Execution (in ms)</h6>
                                            </div>
                                            <div className="card-body">
                                                {this.state.etlData ? this.state.etlData.length ?
                                                    <div className="card-chart-box">
                                                        <StatsChart timeZone={this.props.timeZone} name="Trigger Execution (in ms)" divId="triggerExecution" data={this.state.etlData} />
                                                    </div> :
                                                    <div className="card-message">
                                                        <p>There is no data to display !</p>
                                                    </div> :
                                                    <div className="card-loader">
                                                        <i className="fad fa-sync-alt fa-spin"></i>
                                                    </div>
                                                }
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                {/* end etl graph modal */}

                {this.state.isOpen &&
                    <NotificationModal
                        type={this.state.modalType}
                        message2={this.state.message2}
                        onCloseHandler={this.onCloseHandler}
                    />
                }
            </React.Fragment>
        );
    }
}

DeviceAttributes.propTypes = {
    dispatch: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
    deviceList: getDevicesSuccess(),
    deviceListError: getDevicesFailure(),
    deviceStatus: deviceStatusSuccess(),
    deviceStatusError: deviceStatusFailure(),
    fastAppSuccess: fastAppSuccess(),
    fastAppFailure: fastAppFailure(),
    refreshSchemaSuccess: refreshSchemaSuccess(),
    refreshSchemaFailure: refreshSchemaFailure(),
    verifySchemaSuccess: verifySchemaSuccess(),
    verifySchemaFailure: verifySchemaFailure(),
    resetDeviceAttributesSuccess: resetDeviceAttributesSuccess(),
    resetDeviceAttributesError: resetDeviceAttributesFailure(),
    etlMathFunctions: etlMathFunctionsSuccess(),
    etlMathFunctionsError: etlMathFunctionsError(),
    debugSuccess: debugSuccess(),
    debugError: debugError(),
    developerQuota: developerQuotaSuccess(),
    developerQuotaError: developerQuotaFailure(),
    getStatsSuccess: getStatsSuccess(),
    getStatsFailure: getStatsFailure()
});

function mapDispatchToProps(dispatch) {
    return {
        dispatch,
        fetchDevices: (payload) => dispatch(fetchDevices(payload)),
        fetchDeviceStatus: (id) => dispatch(fetchDeviceStatus(id)),
        createFastApp: (id, payload) => dispatch(createFastApp(id, payload)),
        refreshSchema: (id, userConsentToken) => dispatch(refreshSchema(id, userConsentToken)),
        verifySchema: (id) => dispatch(verifySchema(id)),
        resetDeviceAttributes: (id) => dispatch(resetDeviceAttributes(id)),
        getETLMathFunctions: () => dispatch(getETLMathFunctions()),
        debugDeviceAttributes: (paylaod) => dispatch(debugDeviceAttributes(paylaod)),
        getDeveloperQuota: (payload) => dispatch(getDeveloperQuota(payload)),
        etEtlStatsData: (id, duration) => dispatch(etEtlStatsData(id, duration)),
    };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "deviceAttributes", reducer });
const withSaga = injectSaga({ key: "deviceAttributes", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(DeviceAttributes);
