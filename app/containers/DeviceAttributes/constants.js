/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/*
 *
 * DeviceAttributes constants
 *
 */

export const DEFAULT_ACTION = "app/DeviceAttributes/DEFAULT_ACTION";

export const FETCH_DEVICES = "app/DeviceAttributes/FETCH_DEVICES";
export const FETCH_DEVICES_SUCCESS = "app/DeviceAttributes/FETCH_DEVICES_SUCCESS";
export const FETCH_DEVICES_FAILURE = "app/DeviceAttributes/FETCH_DEVICES_FAILURE";

export const FETCH_DEVICE_STATUS = "app/DeviceAttributes/FETCH_DEVICE_STATUS";
export const FETCH_DEVICE_STATUS_SUCCESS = "app/DeviceAttributes/FETCH_DEVICE_STATUS_SUCCESS";
export const FETCH_DEVICE_STATUS_FAILURE = "app/DeviceAttributes/FETCH_DEVICE_STATUS_FAILURE";

export const CREATE_FAST_APP = "app/DeviceAttributes/CREATE_FAST_APP";
export const CREATE_FAST_APP_SUCCESS = "app/DeviceAttributes/CREATE_FAST_APP_SUCCESS";
export const CREATE_FAST_APP_FAILURE = "app/DeviceAttributes/CREATE_FAST_APP_FAILURE";

export const REFRESH_SCHEMA = "app/DeviceAttributes/REFRESH_SCHEMA";
export const REFRESH_SCHEMA_SUCCESS = "app/DeviceAttributes/REFRESH_SCHEMA_SUCCESS";
export const REFRESH_SCHEMA_FAILURE = "app/DeviceAttributes/REFRESH_SCHEMA_FAILURE";

export const VERIFY_SCHEMA = "app/DeviceAttributes/VERIFY_SCHEMA";
export const VERIFY_SCHEMA_SUCCESS = "app/DeviceAttributes/VERIFY_SCHEMA_SUCCESS";
export const VERIFY_SCHEMA_FAILURE = "app/DeviceAttributes/VERIFY_SCHEMA_FAILURE";

export const RESET_DEVICE_ATTRIBUTES = "app/DeviceAttributes/RESET_DEVICE_ATTRIBUTES";
export const RESET_DEVICE_ATTRIBUTES_SUCCESS = "app/DeviceAttributes/RESET_DEVICE_ATTRIBUTES_SUCCESS";
export const RESET_DEVICE_ATTRIBUTES_FAILURE = "app/DeviceAttributes/RESET_DEVICE_ATTRIBUTES_FAILURE";

export const GET_ETL_FUNCTIONS = 'app/DeviceAttributes/GET_ETL_FUNCTIONS';
export const GET_ETL_FUNCTIONS_SUCCESS = 'app/DeviceAttributes/GET_ETL_FUNCTIONS_SUCCESS';
export const GET_ETL_FUNCTIONS_FAILURE = 'app/DeviceAttributes/GET_ETL_FUNCTIONS_FAILURE';

export const DEBUG_DEVICE_ATTRIBUTES = 'app/DeviceAttributes/DEBUG_DEVICE_ATTRIBUTES';
export const DEBUG_DEVICE_ATTRIBUTES_SUCCESS = 'app/DeviceAttributes/DEBUG_DEVICE_ATTRIBUTES_SUCCESS';
export const DEBUG_DEVICE_ATTRIBUTES_FAILURE = 'app/DeviceAttributes/DEBUG_DEVICE_ATTRIBUTES_FAILURE';

export const GET_DEVELOPER_QUOTA = 'app/DeviceAttributes/GET_DEVELOPER_QUOTA';
export const GET_DEVELOPER_QUOTA_SUCCESS = 'app/DeviceAttributes/GET_DEVELOPER_QUOTA_SUCCESS';
export const GET_DEVELOPER_QUOTA_FAILURE = 'app/DeviceAttributes/GET_DEVELOPER_QUOTA_FAILURE';

export const GET_STATS_DATA = 'app/DeviceAttributes/GET_STATS_DATA';
export const GET_STATS_DATA_SUCCESS = 'app/DeviceAttributes/GET_STATS_DATA_SUCCESS';
export const GET_STATS_DATA_FAILURE = 'app/DeviceAttributes/GET_STATS_DATA_FAILURE';
