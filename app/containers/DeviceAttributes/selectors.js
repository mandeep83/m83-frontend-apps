/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */


import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
 * Direct selector to the applicationCreation state domain
 */

const selectDeviceAttributesDomain = state => state.get("deviceAttributes", initialState);

export const makeSelectDeviceAttributes = () => createSelector(selectDeviceAttributesDomain, subState => subState.toJS());


export const getDevicesSuccess = () => createSelector(selectDeviceAttributesDomain, subState => subState.getDevicesSuccess);
export const getDevicesFailure = () => createSelector(selectDeviceAttributesDomain, subState => subState.getDevicesFailure);

export const deviceStatusSuccess = () => createSelector(selectDeviceAttributesDomain, subState => subState.deviceStatusSuccess);
export const deviceStatusFailure = () => createSelector(selectDeviceAttributesDomain, subState => subState.deviceStatusFailure);

export const fastAppSuccess = () => createSelector(selectDeviceAttributesDomain, subState => subState.fastAppSuccess);
export const fastAppFailure = () => createSelector(selectDeviceAttributesDomain, subState => subState.fastAppFailure);

export const refreshSchemaSuccess = () => createSelector(selectDeviceAttributesDomain, subState => subState.refreshSchemaSuccess);
export const refreshSchemaFailure = () => createSelector(selectDeviceAttributesDomain, subState => subState.refreshSchemaFailure);

export const verifySchemaSuccess = () => createSelector(selectDeviceAttributesDomain, subState => subState.verifySchemaSuccess);
export const verifySchemaFailure = () => createSelector(selectDeviceAttributesDomain, subState => subState.verifySchemaFailure);

export const resetDeviceAttributesSuccess = () => createSelector(selectDeviceAttributesDomain, subState => subState.resetDeviceAttributesSuccess);
export const resetDeviceAttributesFailure = () => createSelector(selectDeviceAttributesDomain, subState => subState.resetDeviceAttributesFailure);

export const etlMathFunctionsSuccess = () => createSelector(selectDeviceAttributesDomain, subState => subState.etlMathFunctionsSuccess);
export const etlMathFunctionsError = () => createSelector(selectDeviceAttributesDomain, subState => subState.etlMathFunctionsError);

export const debugSuccess = () => createSelector(selectDeviceAttributesDomain, subState => subState.debugSuccess);
export const debugError = () => createSelector(selectDeviceAttributesDomain, subState => subState.debugError);

export const developerQuotaSuccess = () => createSelector(selectDeviceAttributesDomain, subState => subState.developerQuotaSuccess);
export const developerQuotaFailure = () => createSelector(selectDeviceAttributesDomain, subState => subState.developerQuotaFailure);

export const getStatsSuccess = () => createSelector(selectDeviceAttributesDomain, subState => subState.getStatsSuccess);
export const getStatsFailure = () => createSelector(selectDeviceAttributesDomain, subState => subState.getStatsFailure);
