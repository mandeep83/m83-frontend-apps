import React, { Component } from "react";
// import PropTypes from 'prop-types';
// import styled from 'styled-components';
import * as am4charts from "@amcharts/amcharts4/charts";
import * as am4core from "@amcharts/amcharts4/core";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";

let chart = {}



class StatsChart extends Component {

    state = {
        color: {
            inputRowsPerSecond: "#b3e6b3",
            processedRowsPerSecond: "#647bce",
            numInputRows: "#ffe066",
            triggerExecution: "#7ccee9"
        },
    }

    componentDidMount() {
        this.createChart();
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.timeZone !== this.props.timeZone) {
            chart[this.props.divId].dispose()
            this.createChart();
        }
    }

    createChart() {
        let _this = this;
        let config = {}
        config.data = _this.props.data.map(val => {
            delete val.name
            return val;
        })
        config.cursor = {}
        config.series = [{
            id: `Series1`,
            name: _this.props.name,
            stroke: _this.state.color[_this.props.divId],
            strokeWidth: 1,
            type: "LineSeries",
            minBulletDistance: 50,
            bullets: [{
                children: [{
                    "type": "Circle",
                    "fill": am4core.color("#fff"),
                    "cursorOverStyle": am4core.MouseCursorStyle.pointer,
                    "resizable": true,
                    "width": 8,
                    "height": 8,
                    "states": {
                        "hover": {
                            "properties": {
                                "scale": 1.7
                            }
                        }
                    },
                    "strokeWidth": 3,
                    "filters": [{
                        "type": "DropShadowFilter",
                        "color": "#B8B8B8",
                        "dx": 3,
                        "dy": 3
                    }]
                }],
            }],
            fill: _this.state.color[_this.props.divId],
            filters: [{
                "type": "DropShadowFilter",
                "color": "#e6e6e6",
                "dx": 6,
                "dy": 6
            }],
            fillOpacity: 0.2,
            dataFields: {
                valueY: _this.props.divId,
                dateX: "timestamp",
            },
            tooltipText: `${_this.props.name} : [bold]{valueY} ${_this.props.unit ? " " + _this.props.unit : ""}`,
            tooltip: {
                getFillFromObject: false,
                background: {
                    fill: "#211D21",
                    fillOpacity: 1,
                },
                label: {
                    fill: "#fff",
                }
            },
            "segments": {
                "fillModifier": {
                    "type": "LinearGradientModifier",
                    "opacities": [1, 0],
                    "gradient": {
                        "rotation": 90
                    }
                }
            },
        }]
        config.xAxes = [{
            "startLocation": 0.5,
            "endLocation": 0.7,
            "type": "DateAxis",
            "cursorTooltipEnabled": true,
            "tooltipDateFormat": "dd MMM yyyy HH:mm",
            "dateFormats": {
                "day": "dd MMM",
                "hour": "dd MMM HH:mm",
                "minute": "dd MMM HH:mm",
                "second": "HH:mm:ss",
                "millisecond": "HH:mm:ss"
            },
            dateFormatter: { timezone: localStorage.timeZone },
            "renderer": {
                line: {
                    strokeOpacity: 0.1,
                    strokeWidth: 1,
                },
                "labels": {
                    "disabled": false,
                    "fill": "#808080"
                },
            },
            "fontSize": "10px",
        }]
        config.yAxes = [{
            type: "ValueAxis",
            cursorTooltipEnabled: false,
            renderer: {
                line: {
                    strokeOpacity: 0.2,
                    strokeWidth: 1,
                },
                labels: {
                    fill: "#808080"
                }
            },
            // baseValue: -1000,
            // title: {
            //     text: _this.props.data.data.availableAttributes.length > 0 ? (_this.props.data.data.availableAttributes[0].attrDisplayName ? _this.props.data.data.availableAttributes[0].attrDisplayName + " "+(_this.props.data.data.availableAttributes[0].unit? "("+_this.props.data.data.availableAttributes[0].unit+")" : "") : _this.props.data.data.availableAttributes[0].attr + " "+ (_this.props.data.data.availableAttributes[0].unit ? "("+_this.props.data.data.availableAttributes[0].unit+")": "")) : "",
            //     fill: "#4a4a4a",
            //     strokeWidth: 1,
            //     fontWeight: "bold",
            //     fontSize: 12
            // },
            maxPrecision: 0,
            fontSize: 10,
        }]
        // config.legend = {
        //     useDefaultMarker: true,
        //     markers: {
        //         width: 15,
        //         height: 15
        //     }
        // }
        // config.scrollbarX = {
        //     "type": "XYChartScrollbar",
        //     series:["Series1"],
        //     minHeight : 20,
        //     dy: 0
        // }
        config.marginTop = 10
        config.paddingRight = 10
        config.paddingLeft = 0
        config.paddingBottom = 0
        chart[_this.props.divId] = am4core.createFromConfig(config, _this.props.divId, am4charts.XYChart);
    }

    render() {
        return <React.Fragment>
            <div id={this.props.divId} className="h-100"></div>
        </React.Fragment>
    }

    componentWillUnmount() {
        chart[this.props.divId].dispose()
    }
}

export default StatsChart;