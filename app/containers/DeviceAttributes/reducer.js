/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/*
 *
 * DeviceAttributes reducer
 *
 */

import { fromJS } from "immutable";
import {
  DEFAULT_ACTION,
  FETCH_DEVICES_SUCCESS,
  FETCH_DEVICES_FAILURE,
  FETCH_DEVICE_STATUS_SUCCESS,
  FETCH_DEVICE_STATUS_FAILURE,
  CREATE_FAST_APP_SUCCESS,
  CREATE_FAST_APP_FAILURE,
  REFRESH_SCHEMA_SUCCESS,
  REFRESH_SCHEMA_FAILURE,
  VERIFY_SCHEMA_SUCCESS,
  VERIFY_SCHEMA_FAILURE,
  GET_ETL_FUNCTIONS_SUCCESS,
  GET_ETL_FUNCTIONS_FAILURE,
  DEBUG_DEVICE_ATTRIBUTES_SUCCESS,
  DEBUG_DEVICE_ATTRIBUTES_FAILURE,
  GET_DEVELOPER_QUOTA_SUCCESS,
  GET_DEVELOPER_QUOTA_FAILURE,
  RESET_DEVICE_ATTRIBUTES_SUCCESS,
  RESET_DEVICE_ATTRIBUTES_FAILURE,
  GET_STATS_DATA_SUCCESS,
  GET_STATS_DATA_FAILURE
} from "./constants";

export const initialState = fromJS({});

function deviceAttributesReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case GET_STATS_DATA_SUCCESS:
      return Object.assign({}, state, {
        'getStatsSuccess': action.response,
      })
    case GET_STATS_DATA_FAILURE:
      return Object.assign({}, state, {
        'getStatsFailure': action.error,
      })
    case FETCH_DEVICES_SUCCESS:
      return Object.assign({}, state, {
        'getDevicesSuccess': action.response,
      })
    case FETCH_DEVICES_FAILURE:
      return Object.assign({}, state, {
        'getDevicesFailure': action.error,
      })
    case FETCH_DEVICE_STATUS_SUCCESS:
      return Object.assign({}, state, {
        'deviceStatusSuccess': action.response,
      })
    case FETCH_DEVICE_STATUS_FAILURE:
      return Object.assign({}, state, {
        'deviceStatusFailure': {
          error: action.error,
          timestamp: new Date()
        },
      })
    case CREATE_FAST_APP_SUCCESS:
      return Object.assign({}, state, {
        'fastAppSuccess': action.response,
      })
    case CREATE_FAST_APP_FAILURE:
      return Object.assign({}, state, {
        'fastAppFailure': {
          error: action.error,
          timestamp: new Date()
        },
      })
    case REFRESH_SCHEMA_SUCCESS:
      return Object.assign({}, state, {
        'refreshSchemaSuccess': action.response,
      })
    case REFRESH_SCHEMA_FAILURE:
      return Object.assign({}, state, {
        'refreshSchemaFailure': action.error,
      })
    case VERIFY_SCHEMA_SUCCESS:
      return Object.assign({}, state, {
        'verifySchemaSuccess': action.response,
      })
    case VERIFY_SCHEMA_FAILURE:
      return Object.assign({}, state, {
        'verifySchemaFailure': {
          error: action.error,
          timestamp: new Date()
        },
      })
    case RESET_DEVICE_ATTRIBUTES_SUCCESS:
      return Object.assign({}, state, {
        'resetDeviceAttributesSuccess': action.response,
      })
    case RESET_DEVICE_ATTRIBUTES_FAILURE:
      return Object.assign({}, state, {
        'resetDeviceAttributesFailure': action.error,
      })
    case GET_ETL_FUNCTIONS_SUCCESS:
      return Object.assign({}, state, {
        etlMathFunctionsSuccess: action.response
      });
    case GET_ETL_FUNCTIONS_FAILURE:
      return Object.assign({}, state, {
        etlMathFunctionsError: action.error
      });
    case DEBUG_DEVICE_ATTRIBUTES_SUCCESS:
      return Object.assign({}, state, {
        debugSuccess: action.response
      });
    case DEBUG_DEVICE_ATTRIBUTES_FAILURE:
      return Object.assign({}, state, {
        debugError: {
          error: action.error,
          timestamp: new Date()
        }
      });
    case GET_DEVELOPER_QUOTA_SUCCESS:
      return Object.assign({}, state, {
        developerQuotaSuccess: action.response
      });
    case GET_DEVELOPER_QUOTA_FAILURE:
      return Object.assign({}, state, {
        developerQuotaFailure: action.error
      });
    default:
      return state;
  }
}

export default deviceAttributesReducer;
