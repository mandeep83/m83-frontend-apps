/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

export const DEFAULT_ACTION = "app/TicketList/DEFAULT_ACTION";

export const GET_TICKET_LIST = 'app/TicketList/GET_TICKET_LIST';
export const GET_TICKET_LIST_FAILURE = 'app/TicketList/GET_TICKET_LIST_FAILURE';
export const GET_TICKET_LIST_SUCCESS = 'app/TicketList/GET_TICKET_LIST_SUCCESS';
export const GET_OPEN_TICKET_LIST_SUCCESS = 'app/TicketList/GET_OPEN_TICKET_LIST_SUCCESS';
export const GET_OPEN_TICKET_LIST_FAILURE = 'app/TicketList/GET_OPEN_TICKET_LIST_FAILURE';
export const GET_CLOSED_TICKET_LIST_SUCCESS = 'app/TicketList/GET_CLOSED_TICKET_LIST_SUCCESS';
export const GET_CLOSED_TICKET_LIST_FAILURE = 'app/TicketList/GET_CLOSED_TICKET_LIST_FAILURE';
export const GET_REOPENED_TICKET_LIST_SUCCESS = 'app/TicketList/GET_REOPENED_TICKET_LIST_SUCCESS';
export const GET_REOPENED_TICKET_LIST_FAILURE = 'app/TicketList/GET_REOPENED_TICKET_LIST_FAILURE';
export const GET_RESOLVED_TICKET_LIST_SUCCESS = 'app/TicketList/GET_RESOLVED_TICKET_LIST_SUCCESS';
export const GET_RESOLVED_TICKET_LIST_FAILURE = 'app/TicketList/GET_RESOLVED_TICKET_LIST_FAILURE';
export const GET_INPROGESS_TICKET_LIST_SUCCESS = 'app/TicketList/GET_INPROGESS_TICKET_LIST_SUCCESS';
export const GET_INPROGESS_TICKET_LIST_FAILURE = 'app/TicketList/GET_INPROGESS_TICKET_LIST_FAILURE';

export const GET_USERS = 'app/TicketList/GET_USERS_LIST';
export const GET_USERS_SUCCESS = 'app/TicketList/GET_USERS_SUCCESS';
export const GET_USERS_FAILURE = 'app/TicketList/GET_USERS_FAILURE';

export const FETCH_TICKET_DETAILS = "app/TicketList/FETCH_TICKET_DETAILS";
export const TICKET_DETAILS_SUCCESS = "app/TicketList/TICKET_DETAILS_SUCCESS";
export const TICKET_DETAILS_ERROR = "app/TicketList/TICKET_DETAILS_ERROR";

export const DELETE_TICKETS = "app/TicketList/DELETE_TICKETS";
export const DELETE_TICKETS_SUCCESS = "app/TicketList/DELETE_TICKETS_SUCCESS";
export const DELETE_TICKETS_ERROR = "app/TicketList/DELETE_TICKETS_ERROR";

export const FETCH_TICKET_COMMENTS = "app/TicketList/FETCH_TICKET_COMMENTS";
export const TICKET_COMMENTS_SUCCESS = "app/TicketList/TICKET_COMMENTS_SUCCESS";
export const TICKET_COMMENTS_ERROR = "app/TicketList/TICKET_COMMENTS_ERROR";

export const FETCH_TICKET_LOGS = "app/TicketList/FETCH_TICKET_LOGS";
export const TICKET_LOGS_SUCCESS = "app/TicketList/TICKET_LOGS_SUCCESS";
export const TICKET_LOGS_ERROR = "app/TicketList/TICKET_LOGS_ERROR";

export const ADD_COMMENT_REQUEST = "app/TicketList/ADD_COMMENT_REQUEST";
export const ADD_COMMENTS_SUCCESS = "app/TicketList/ADD_COMMENTS_SUCCESS";
export const ADD_COMMENTS_ERROR = "app/TicketList/ADD_COMMENTS_ERROR";

export const ADD_TICKET_REQUEST = "app/TicketList/ADD_TICKET_REQUEST";
export const ADD_TICKET_SUCCESS = "app/TicketList/ADD_TICKET_SUCCESS";
export const ADD_TICKET_ERROR = "app/TicketList/ADD_TICKET_ERROR";

export const GET_JIRA_OAUTH_STATUS_REQUEST = "app/TicketList/GET_JIRA_OAUTH_STATUS_REQUEST";
export const GET_JIRA_OAUTH_STATUS_SUCCESS = "app/TicketList/GET_JIRA_OAUTH_STATUS_SUCCESS";
export const GET_JIRA_OAUTH_STATUS_ERROR = "app/TicketList/GET_JIRA_OAUTH_STATUS_ERROR";

export const CREATE_JIRA_PROJECT__REQUEST = "app/TicketList/CREATE_JIRA_PROJECT__REQUEST";
export const CREATE_JIRA_PROJECT__SUCCESS = "app/TicketList/CREATE_JIRA_PROJECT__SUCCESS";
export const CREATE_JIRA_PROJECT__ERROR = "app/TicketList/CREATE_JIRA_PROJECT__ERROR";

//     'GET_OPEN_TICKET_LIST_FAILURE',
//     'GET_OPEN_TICKET_LIST_SUCCESS',
// 'GET_CLOSED_TICKET_LIST_FAILURE',
// 'GET_CLOSED_TICKET_LIST_SUCCESS',
// 'GET_TICKET_CLOSED_LIST_SUCCESS',
// 'GET_RESOLVED_TICKET_LIST_FAILURE',
// 'GET_RESOLVED_TICKET_LIST_RESOLVED_SUCCESS',
// 'GET_TICKET_CLOSED_LIST_FAILURE',
// 'GET_RESOLVED_TICKET_LIST_SUCCESS',
// 'GET_RESOLVED_TICKET_LIST_RESOLVED_FAILURE',
// }