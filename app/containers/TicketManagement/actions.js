/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/
import * as CONSTANTS from "./constants";
let constantsForTicketList = [{
  success: CONSTANTS.GET_OPEN_TICKET_LIST_SUCCESS,
  failure: CONSTANTS.GET_OPEN_TICKET_LIST_FAILURE,
  status: "OPEN"
}, {
  success: CONSTANTS.GET_REOPENED_TICKET_LIST_SUCCESS,
  failure: CONSTANTS.GET_REOPENED_TICKET_LIST_FAILURE,
  status: "REOPEN"
}, {
  success: CONSTANTS.GET_RESOLVED_TICKET_LIST_SUCCESS,
  failure: CONSTANTS.GET_RESOLVED_TICKET_LIST_FAILURE,
  status: "RESOLVED"
}, {
  success: CONSTANTS.GET_CLOSED_TICKET_LIST_SUCCESS,
  failure: CONSTANTS.GET_CLOSED_TICKET_LIST_FAILURE,
  status: "CLOSED"

}, {
  success: CONSTANTS.GET_INPROGESS_TICKET_LIST_SUCCESS,
  failure: CONSTANTS.GET_INPROGESS_TICKET_LIST_FAILURE,
  status: "INPROGRESS"
},
]

export function getTicketList(type, currentPageNumber) {
  return {
    type: CONSTANTS.GET_TICKET_LIST,
    constants: type === "ALL" ? constantsForTicketList : constantsForTicketList.filter(el => el.status === type),
    currentPageNumber,
  };
}

export function getAllUsers() {
  return {
    type: CONSTANTS.GET_USERS,
  };
}

export function getTicketDetails(id) {
  return {
    type: CONSTANTS.FETCH_TICKET_DETAILS,
    id
  };
}

export function getTicketComments(id) {
  return {
    type: CONSTANTS.FETCH_TICKET_COMMENTS,
    id
  };
}

export function getTicketLogs(id) {
  return {
    type: CONSTANTS.FETCH_TICKET_LOGS,
    id
  };
}

export function addCommentHandler(id, payload) {
  return {
    type: CONSTANTS.ADD_COMMENT_REQUEST,
    id,
    payload
  };
}

export function createOrEditTicket(actualPayload, noModal, previousStatus) {
  let payload = { ...actualPayload };
  if (payload.status)
    payload.status = payload.status.toUpperCase();
  return {
    type: CONSTANTS.ADD_TICKET_REQUEST,
    payload,
    noModal,
    previousStatus
  };
}

export function getJiraOauthStatus() {
  return {
    type: CONSTANTS.GET_JIRA_OAUTH_STATUS_REQUEST,
  };
}

export function createJiraProject() {
  return {
    type: CONSTANTS.CREATE_JIRA_PROJECT__REQUEST,
  };
}