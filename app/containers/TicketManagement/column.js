/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import React from 'react';
import styled from 'styled-components';
import { Droppable, Draggable } from 'react-beautiful-dnd';
import Ticket from './ticket';

const Title = styled.p`
`;

const TicketList = styled.div`
	height: calc(100vh - 310px);
	overflow: auto;
`;

const Container = styled.div`
    flex: 0 0 20%;
    max-width: 20%;
`;

class InnerList extends React.Component {
	shouldComponentUpdate(nextProps) {
		if (nextProps.tickets === this.props.tickets) {
			return false;
		}
		return true;
	}
	render() {
			return(this.props.tickets.map((ticket, index) => (
					<Ticket key={ticket.id} ticket={ticket} index={index} ticketEditHandler={this.props.ticketEditHandler}/>)))
	}
}

export default class Column extends React.Component {
	setColumnClassName = (title)=>{
		switch(title){
			case "Open" :
				return "badge badge-pill badge-primary"
			case "In Progress" :
				return "badge badge-pill badge-warning"
			case "Resolved" :
				return "badge badge-pill badge-success"
			case "Closed" :
				return "badge badge-pill badge-info"
			case "Reopen" :
				return "badge badge-pill badge-danger"
		}
	}
	render() {
		return (
			<Draggable draggableId={this.props.column.id} index={this.props.index}>
				{provided => (
					<Container {...provided.draggableProps} innerRef={provided.innerRef}>
						<div className="ticketList">
							<Title>
								<div className="ticketListHeader flex">
									<p className="flex-item fx-b50 pd-r-7 ">{this.props.column.title}</p>
									{this.props.column.loader ? <div className="panelLoader flex-item fx-b50 mr-t-0 text-right">
										<i className="fal fa-sync-alt fa-spin"></i>
									</div> : 
									<span className={this.setColumnClassName(this.props.column.title)}>{this.props.column.totalCount}</span>}
								</div>
							</Title>
							{this.props.column.loader ?<div className="ticketLoadingBox">
								<div className="loadSpinner">
									<div className="cube1"></div>
									<div className="cube2"></div>
								</div>
							</div> : <Droppable droppableId={this.props.column.id} isDropDisabled={this.props.column.isDropDisabled}  type="ticket">
								{(provided, snapshot) => (
									<TicketList
										innerRef={provided.innerRef}
										{...provided.droppableProps}
										isDraggingOver={snapshot.isDraggingOver}
									>
										<InnerList tickets={this.props.column.tickets} ticketEditHandler={this.props.ticketEditHandler}/>
										{this.props.column.totalPages > 1 &&
										<div className="text-center">
											{<button className = {`btn btn-link float-left mr-l-7`} disabled={this.props.column.activePageNumber === 1} onClick = {() =>this.props.changePage("previous")} ><i
												className="fas fa-chevron-circle-left"></i>Previous</button>}
											<span className="mr-t-10 d-inline-block"> {this.props.column.activePageNumber}/{this.props.column.totalPages} </span>
											{<button onClick = {() =>this.props.changePage("next")} className = {`btn btn-link float-right mr-r-7`} disabled={this.props.column.activePageNumber === this.props.column.totalPages}>Next<i
												className="fas fa-chevron-circle-right mr-r-0 mr-l-5"></i></button>}
										</div>}
									</TicketList>
								)}
							</Droppable>}
						</div>
					 </Container>
				)}
			 </Draggable>
		);
	}
}