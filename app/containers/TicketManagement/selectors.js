/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
 * Direct selector to the ticketList state domain
 */

const selectTicketListDomain = state => state.get("ticketList", initialState);

export const getTicketListSuccess = () =>
    createSelector(selectTicketListDomain, subState => subState.ticketList);
export const getTicketListError = () =>
    createSelector(selectTicketListDomain, subState => subState.ticketListError);

export const getUsersListSuccess = () =>
    createSelector(selectTicketListDomain, subState => subState.usersList);
export const getUsersListError = () =>
    createSelector(selectTicketListDomain, subState => subState.usersListError);

export const getInstanceSuccess = () =>
    createSelector(selectTicketListDomain, subState => subState.instanceList);
export const getInstanceFailure = () =>
    createSelector(selectTicketListDomain, subState => subState.instanceListError);

export const getTicketDeatilsSuccess = () =>
    createSelector(selectTicketListDomain, subState => subState.ticketDetails);
export const getTicketDeatilsError = () =>
    createSelector(selectTicketListDomain, subState => subState.ticketDetailsError);

export const getTicketCommentsSuccess = () =>
    createSelector(selectTicketListDomain, subState => subState.ticketComments);
export const getTicketCommentsError = () =>
    createSelector(selectTicketListDomain, subState => subState.ticketCommentsError);

export const addCommentsSuccess = () =>
    createSelector(selectTicketListDomain, subState => subState.addCommentSuccess);
export const addCommentsError = () =>
    createSelector(selectTicketListDomain, subState => subState.addCommentError);

export const addTicketSuccess = () =>
    createSelector(selectTicketListDomain, subState => subState.addTicketSuccess);
export const addTicketError = () =>
    createSelector(selectTicketListDomain, subState => subState.addTicketError);

export const openTicketList = () =>
    createSelector(selectTicketListDomain, subState => subState.openTicketList);
export const openTicketListError = () =>
    createSelector(selectTicketListDomain, subState => subState.openTicketListError);

export const closedTicketList = () =>
    createSelector(selectTicketListDomain, subState => subState.closedTicketList);
export const closedTicketListError = () =>
    createSelector(selectTicketListDomain, subState => subState.closedTicketListError);

export const reOpenedTicketList = () =>
    createSelector(selectTicketListDomain, subState => subState.reOpenedTicketList);
export const reOpenedTicketListError = () =>
    createSelector(selectTicketListDomain, subState => subState.reOpenedTicketListError);

export const resolvedTicketList = () =>
    createSelector(selectTicketListDomain, subState => subState.resolvedTicketList);
export const resolvedTicketListError = () =>
    createSelector(selectTicketListDomain, subState => subState.resolvedTicketListError);

export const inProgressTicketList = () =>
    createSelector(selectTicketListDomain, subState => subState.inProgressTicketList);
export const inProgressTicketListError = () =>
    createSelector(selectTicketListDomain, subState => subState.inProgressTicketListError);

export const jiraOauthStatusSuccess = () =>
createSelector(selectTicketListDomain, subState => subState.jiraOauthStatusSuccess);
export const jiraOauthStatusError = () =>
createSelector(selectTicketListDomain, subState => subState.jiraOauthStatusError);

export const createJiraProjectSuccess = () =>
createSelector(selectTicketListDomain, subState => subState.createJiraProjectSuccess);
export const createJiraProjectError = () =>
createSelector(selectTicketListDomain, subState => subState.createJiraProjectError);


export const ticketLogs = () =>
    createSelector(selectTicketListDomain, subState => subState.ticketLogs);
export const ticketLogsError = () =>
    createSelector(selectTicketListDomain, subState => subState.ticketLogsError);

export const isFetchingState = state => state.get('loader');

export const getIsFetching = () =>
    createSelector(isFetchingState, substate => substate.get('isFetching'));