/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import ReactSelect from "react-select";
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import * as SELECTORS from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import * as ACTIONS from "./actions";
import Loader from "../../components/Loader";
import NotificationModal from '../../components/NotificationModal/Loadable'
import Column from './column';
// import "@atlaskit/css-reset";
import styled from "styled-components";
import { DragDropContext, Droppable } from "react-beautiful-dnd";
import SlidingPane from "react-sliding-pane";
import jwt_decode from "jwt-decode";
import { getInitials } from '../../commonUtils'

/* eslint-disable react/prefer-stateless-function */

const Container = styled.div`
        display: flex;
        flex-wrap: wrap;
        flex-direction: row;   
`;
class InnerList extends React.PureComponent {
    render() {
        const { column, index, isDropDisabled } = this.props
        return <Column isDropDisabled={isDropDisabled} key={column.id} column={column} index={index} ticketEditHandler={this.props.ticketEditHandler} changePage={this.props.changePage} />;
    }
}

let jiraStatus = false

export class TicketList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            tickets: {
                open: {},
                inProgress: {},
                resolved: {},
                closed: {},
                reopen: {},
            },
            showTicketDetailsModal: false,
            isOpen: false,
            emptyTicketDetails: {
                currentAssigneeId: "",
                description: "",
                priority: "",
                status: "OPEN",
                title: "",
                type: "",
            },
            ticketDetails: {
                currentAssigneeId: "",
                description: "",
                priority: "",
                status: "OPEN",
                title: "",
                type: "",
            },
            ticketComments: [],
            ticketLogs: [],
            userList: [],
            deviceTypes: [],
            instanceList: [],
            deviceId: '',
            commentText: '',
            emptyComment: false,
            ticketList: [],
            className: "",
            ticketType: [{
                key: "Operational",
                value: "OPERATIONAL"
            }, {
                key: "Setup",
                value: "SETUP"
            }, {
                key: "Technical",
                value: "TECHNICAL"
            }],
            selectedFilters: {
                priority: { value: "ALL", label: "All" },
                currentAssigneeId: { value: "ALL", label: "All" },
                type: { value: "ALL", label: "All" }
            },
            modalLoader: false,
            isFetching: true,
            isFetchingTicketDetails: false,
            ticketStatusTypes: ["OPEN", "INPROGRESS", "CLOSED", "REOPEN", "RESOLVED"],
            statusKeys: [
                {
                    type: "OPEN",
                    value: "OPEN",
                    displayName: "Open",
                    accessKey: "open"
                },
                {
                    type: "INPROGRESS",
                    value: "INPROGRESS",
                    displayName: "In Progress",
                    accessKey: "inProgress"
                },
                {
                    type: "RESOLVED",
                    value: "RESOLVED",
                    displayName: "Resolved",
                    accessKey: "resolved"
                },
                {
                    type: "CLOSED",
                    value: "CLOSED",
                    displayName: "Closed",
                    accessKey: "closed"
                },
                {
                    type: "REOPEN",
                    value: "REOPEN",
                    displayName: "Reopen",
                    accessKey: "reopen"
                },
            ],
            ticketTypes: [{ value: "ALL", label: "All" }, { value: "OPERATIONAL", label: "Operational" }, { value: "SETUP", label: "Setup" }, { value: "TECHNICAL", label: "Technical" }, { value: "OTHER", label: "Others" },],
            priorityOptions: [{ value: "ALL", label: "All" }, { value: "HIGH", label: "High", className: "far fa-arrow-up text-danger mr-r-5" }, { value: "MEDIUM", label: "Medium", className: "far fa-arrows-v text-orange mr-r-5" }, { value: "LOW", label: "Low", className: "far fa-arrow-down text-success mr-r-5" }],
            isFetchingJiraStatus: true
        }
        this.handleChange = this.handleChange.bind(this);
        this.onEnterPress = this.onEnterPress.bind(this);
    }

    getAlltickets = () => {
        return [].concat.apply([], (Object.keys(this.state.tickets)).map(key => this.state.tickets[key].ticketList));
    }

    openTicketModalHandler = (event, ticketId, showDetailsModal) => {
        let ticketDetails;
        if (ticketId) {
            let allTickets = this.getAlltickets()
            ticketDetails = JSON.parse(JSON.stringify(allTickets.find(el => el.id === ticketId)))
        }
        else
            ticketDetails = JSON.parse(JSON.stringify(this.state.emptyTicketDetails));
        showDetailsModal = Boolean(showDetailsModal)
        this.setState({
            ticketDetails,
            showTicketDetailsModal: showDetailsModal,
            isFetchingTicketDetails: showDetailsModal,
            addTicketModal: !showDetailsModal,
            isCommentsTabActive: true
        }, () => showDetailsModal && this.props.getTicketComments(ticketDetails.id))
    }


    toTitleCase(str) {
        return str.toLowerCase().split(' ')
            .map((s) => s.charAt(0).toUpperCase() + s.substring(1))
            .join(' ');
    }

    componentWillMount() {
        jiraStatus = false
        if (this.props.isFetchedValidity && !jiraStatus) {
            jiraStatus = true
            this.props.getJiraOauthStatus();
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.isFetchedValidity && !jiraStatus) {
            jiraStatus = true
            this.props.getJiraOauthStatus();
        }

        if (nextProps.userList && nextProps.userList !== this.props.userList) {
            let userList = nextProps.userList.map(user => {
                return {
                    name: this.toTitleCase(user.name),
                    skills: user.skills,
                    label: this.toTitleCase(user.name),
                    value: user.id,
                }
            })
            userList.unshift({ value: "ALL", label: "All", name: "All", skills: "All" })
            this.setState({
                isFetching: false,
                userList,
            })
        }

        if (nextProps.userListError && nextProps.userListError !== this.props.userListError) {
            this.setState(prevState => ({
                isFetching: false,
                message2: nextProps.userListError.error,
                isOpen: true,
                type: 'error'
            }))
        }

        if (nextProps.ticketDetails && nextProps.ticketDetails !== this.props.ticketDetails) {
            this.setState({ ticketDetails: nextProps.ticketDetails, isFetchingTicketDetails: false })
        }

        if (nextProps.ticketDetailsError && nextProps.ticketDetailsError !== this.props.ticketDetailsError) {
            this.setState({
                message2: nextProps.ticketDetailsError,
                isOpen: true,
                type: 'error'
            })
        }

        if (nextProps.addTicketSuccess && nextProps.addTicketSuccess !== this.props.addTicketSuccess) {
            let tickets = JSON.parse(JSON.stringify(this.state.tickets)),
                emptyTicketDetails = JSON.parse(JSON.stringify(this.state.emptyTicketDetails)),
                ticketDetails = JSON.parse(JSON.stringify(this.state.ticketDetails)),
                addedFromAddTicketModal = this.state.addTicketModal,
                addedTicketStatus;
            if (addedFromAddTicketModal) {
                let ticketStatuskey = this.state.statusKeys.find(el => el.type.toUpperCase() === ticketDetails.status.toUpperCase()).accessKey;
                tickets[ticketStatuskey].loader = true;
                addedTicketStatus = ticketDetails.status;
                ticketDetails = emptyTicketDetails;
            }
            this.setState(prevState => ({
                tickets,
                ticketDetails,
                addTicketModal: false,
                message2: nextProps.addTicketSuccess.message,
                modalLoader: false,
                isOpen: !nextProps.addTicketSuccess.noModal,
                type: 'success',
                logBoxLoader: prevState.showTicketDetailsModal
            }), () => {
                addedFromAddTicketModal && this.props.getTicketList(addedTicketStatus.toUpperCase())
                this.state.logBoxLoader && this.props.getTicketLogs(this.state.ticketDetails.id);
            })
        }

        if (nextProps.addTicketError && nextProps.addTicketError !== this.props.addTicketError) {
            let tickets = JSON.parse(JSON.stringify(this.state.tickets)),
                ticketDetails = JSON.parse(JSON.stringify(this.state.ticketDetails)),
                previousStatus = nextProps.addTicketError.previousStatus;
            if (previousStatus) {
                if (previousStatus.includes("detailsModal") && this.state.showTicketDetailsModal) {
                    let keyName = previousStatus.split("_")[0],
                        previousValue = previousStatus.split("_")[2];
                    ticketDetails[keyName] = previousValue
                }
                else {
                    let allTickets = this.getAlltickets()
                    let updatedTicketStatus = allTickets.find(ticket => ticket.id === nextProps.addTicketError.ticket.id).status,
                        updatedTicketStatusKey = this.state.statusKeys.find(status => status.type === updatedTicketStatus).accessKey
                    tickets[updatedTicketStatusKey].ticketList = ((JSON.parse(JSON.stringify(this.state.tickets))[updatedTicketStatusKey]).ticketList.filter(ticket => ticket.id !== nextProps.addTicketError.ticket.id));
                    tickets[updatedTicketStatusKey].totalCount--;
                    nextProps.addTicketError.ticket.status = this.state.statusKeys.find(el => el.accessKey === nextProps.addTicketError.previousStatus).type
                    tickets[nextProps.addTicketError.previousStatus].ticketList.push(nextProps.addTicketError.ticket);
                    tickets[nextProps.addTicketError.previousStatus].totalCount++;
                }
            }
            this.setState({
                ticketDetails,
                tickets,
                modalLoader: false,
                message2: nextProps.addTicketError.error || "The request could not be completed.",
                isOpen: true,
                type: 'error'
            })
        }

        if (nextProps.openTicketList && nextProps.openTicketList !== this.props.openTicketList) {
            let tickets = JSON.parse(JSON.stringify(this.state.tickets));
            nextProps.openTicketList.tickets.tickets.map(ticket => {
                ticket.currentAssigneeId = ticket.assigneeId
            })
            let ticketList = nextProps.openTicketList.currentPageNumber ? (tickets.open.ticketList).concat(nextProps.openTicketList.tickets.tickets) : nextProps.openTicketList.tickets.tickets,
                activePageNumber = nextProps.openTicketList.currentPageNumber ? (nextProps.openTicketList.currentPageNumber + 1) : 1;
            tickets.open = {
                ticketList,
                totalCount: nextProps.openTicketList.tickets.totalCount,
                totalPages: Math.ceil((nextProps.openTicketList.tickets.totalCount) / 10),
                activePageNumber,
                loader: false,
            }
            this.setState({
                tickets,
            })
        }

        if (nextProps.openTicketListError && nextProps.openTicketListError !== this.props.openTicketListError) {
            let tickets = JSON.parse(JSON.stringify(this.state.tickets));
            tickets.open.loader = false;
            this.setState({
                tickets,
                message2: nextProps.openTicketListError.error,
                isOpen: true,
                type: 'error',
            })
        }

        if (nextProps.closedTicketList && nextProps.closedTicketList !== this.props.closedTicketList) {
            let tickets = JSON.parse(JSON.stringify(this.state.tickets))
            nextProps.closedTicketList.tickets.tickets.map(ticket => {
                ticket.currentAssigneeId = ticket.assigneeId
            })
            let ticketList = nextProps.closedTicketList.currentPageNumber ? (tickets.open.ticketList).concat(nextProps.closedTicketList.tickets.tickets) : nextProps.closedTicketList.tickets.tickets,
                activePageNumber = nextProps.closedTicketList.currentPageNumber ? (nextProps.closedTicketList.currentPageNumber + 1) : 1;
            tickets.closed = {
                ticketList,
                totalCount: nextProps.closedTicketList.tickets.totalCount,
                totalPages: Math.ceil((nextProps.closedTicketList.tickets.totalCount) / 10),
                activePageNumber,
                loader: false,
            }
            this.setState({
                tickets,
            })

        }

        if (nextProps.closedTicketListError && nextProps.closedTicketListError !== this.props.closedTicketListError) {
            let tickets = JSON.parse(JSON.stringify(this.state.tickets));
            tickets.closed.loader = false;
            this.setState({
                tickets,
                message2: nextProps.closedTicketListError.error,
                isOpen: true,
                type: 'error'
            })
        }

        if (nextProps.resolvedTicketList && nextProps.resolvedTicketList !== this.props.resolvedTicketList) {
            let tickets = JSON.parse(JSON.stringify(this.state.tickets))
            nextProps.resolvedTicketList.tickets.tickets.map(ticket => {
                ticket.currentAssigneeId = ticket.assigneeId
            })
            let ticketList = nextProps.resolvedTicketList.currentPageNumber ? (tickets.open.ticketList).concat(nextProps.resolvedTicketList.tickets.tickets) : nextProps.resolvedTicketList.tickets.tickets,
                activePageNumber = nextProps.resolvedTicketList.currentPageNumber ? (nextProps.resolvedTicketList.currentPageNumber + 1) : 1;
            tickets.resolved = {
                ticketList,
                totalCount: nextProps.resolvedTicketList.tickets.totalCount,
                activePageNumber,
                totalPages: Math.ceil((nextProps.resolvedTicketList.tickets.totalCount) / 10),
                loader: false,
            };
            this.setState({
                tickets,
            })

        }

        if (nextProps.resolvedTicketListError && nextProps.resolvedTicketListError !== this.props.resolvedTicketListError) {
            let tickets = JSON.parse(JSON.stringify(this.state.tickets));
            tickets.resolved.loader = false;
            this.setState({
                tickets,
                message2: nextProps.resolvedTicketListError.error,
                isOpen: true,
                type: 'error'
            })
        }

        if (nextProps.reOpenedTicketList && nextProps.reOpenedTicketList !== this.props.reOpenedTicketList) {
            let tickets = JSON.parse(JSON.stringify(this.state.tickets))
            nextProps.reOpenedTicketList.tickets.tickets.map(ticket => {
                ticket.currentAssigneeId = ticket.assigneeId
            })
            let ticketList = nextProps.reOpenedTicketList.currentPageNumber ? (tickets.open.ticketList).concat(nextProps.reOpenedTicketList.tickets.tickets) : nextProps.reOpenedTicketList.tickets.tickets,
                activePageNumber = nextProps.reOpenedTicketList.currentPageNumber ? (nextProps.reOpenedTicketList.currentPageNumber + 1) : 1;
            tickets.reopen = {
                ticketList,
                totalCount: nextProps.reOpenedTicketList.tickets.totalCount,
                totalPages: Math.ceil((nextProps.reOpenedTicketList.tickets.totalCount) / 10),
                activePageNumber,
                loader: false,
            };
            this.setState({
                tickets,
            })
        }

        if (nextProps.reOpenedTicketListError && nextProps.reOpenedTicketListError !== this.props.reOpenedTicketListError) {
            let tickets = JSON.parse(JSON.stringify(this.state.tickets));
            tickets.reopen.loader = false;
            this.setState({
                tickets,
                message2: nextProps.reOpenedTicketListError.error,
                isOpen: true,
                type: 'error'
            })
        }
        if (nextProps.inProgressTicketList && nextProps.inProgressTicketList !== this.props.inProgressTicketList) {
            let tickets = JSON.parse(JSON.stringify(this.state.tickets))
            nextProps.inProgressTicketList.tickets.tickets.map(ticket => {
                ticket.currentAssigneeId = ticket.assigneeId
            })
            let ticketList = nextProps.inProgressTicketList.currentPageNumber ? (tickets.open.ticketList).concat(nextProps.inProgressTicketList.tickets.tickets) : nextProps.inProgressTicketList.tickets.tickets,
                activePageNumber = nextProps.inProgressTicketList.currentPageNumber ? (nextProps.inProgressTicketList.currentPageNumber + 1) : 1;
            tickets.inProgress = {
                ticketList,
                totalCount: nextProps.inProgressTicketList.tickets.totalCount,
                totalPages: Math.ceil((nextProps.inProgressTicketList.tickets.totalCount) / 10),
                activePageNumber,
                loader: false,
            };
            this.setState({
                tickets,
            })

        }

        if (nextProps.inProgressTicketListError && nextProps.inProgressTicketListError !== this.props.inProgressTicketListError) {
            let tickets = JSON.parse(JSON.stringify(this.state.tickets));
            tickets.inProgress.loader = false;
            this.setState({
                tickets,
                message2: nextProps.inProgressTicketListError.error,
                isOpen: true,
                type: 'error',
            })
        }

        if (nextProps.ticketComments && nextProps.ticketComments !== this.props.ticketComments) {
            this.setState({
                ticketComments: nextProps.ticketComments,
                isFetchingTicketDetails: false,
                commentBoxLoader: false
            })
        }

        if (nextProps.ticketCommentsError && nextProps.ticketCommentsError !== this.props.ticketCommentsError) {
            this.setState({
                message2: nextProps.ticketCommentsError,
                isOpen: true,
                type: 'error',
                commentBoxLoader: false,
            })
        }

        if (nextProps.addCommentSuccess && nextProps.addCommentSuccess !== this.props.addCommentSuccess) {
            this.setState({
                commentId: '',
                commentText: '',
                isEditing: false,
                logBoxLoader: true
            });
            this.props.getTicketComments(this.state.ticketDetails.id);
            this.props.getTicketLogs(this.state.ticketDetails.id);
        }

        if (nextProps.addCommentError && nextProps.addCommentError !== this.props.addCommentError) {
            this.setState({
                commentId: '',
                commentText: '',
                isEditing: false,
                message2: nextProps.addCommentError,
                isOpen: true,
                type: 'error',
                commentBoxLoader: false,
            })
        }

        if (nextProps.ticketLogs && nextProps.ticketLogs !== this.props.ticketLogs) {
            this.setState({
                ticketLogs: nextProps.ticketLogs,
                isFetchingTicketDetails: false,
                logBoxLoader: false
            })
        }

        if (nextProps.ticketLogsError && nextProps.ticketLogsError !== this.props.ticketLogsError) {
            this.setState({
                message2: nextProps.ticketLogsError.error,
                isOpen: true,
                type: 'error',
                logBoxLoader: false,
            })
        }

        if (nextProps.jiraOauthStatusSuccess && nextProps.jiraOauthStatusSuccess !== this.props.jiraOauthStatusSuccess) {
            let tickets = JSON.parse(JSON.stringify(this.state.tickets));
            Object.keys(tickets).map(key => {
                tickets[key].loader = true
            })
            this.setState({
                jiraStatus: nextProps.jiraOauthStatusSuccess,
                isFetchingJiraStatus: (nextProps.jiraOauthStatusSuccess.jiraAvailable && nextProps.jiraOauthStatusSuccess.jiraConfigured && localStorage.isJiraLoggedIn === "true"),
                tickets,
                isFetching: true
            }, () => {
                if (!nextProps.jiraOauthStatusSuccess.jiraAvailable) {
                    this.props.getAllUsers();
                    this.props.getTicketList("ALL");
                } else if ((nextProps.jiraOauthStatusSuccess.jiraAvailable && nextProps.jiraOauthStatusSuccess.jiraConfigured && localStorage.isJiraLoggedIn === "true")) {
                    this.props.createJiraProject()
                }
            })
        }

        if (nextProps.jiraOauthStatusError && nextProps.jiraOauthStatusError !== this.props.jiraOauthStatusError) {
            this.setState({
                message2: nextProps.jiraOauthStatusError.error,
                isOpen: true,
                type: 'error',
                logBoxLoader: false,
            })
        }

        if (nextProps.createJiraProjectSuccess && nextProps.createJiraProjectSuccess !== this.props.createJiraProjectSuccess) {
            this.setState({
                isFetchingJiraStatus: false,
            }, () => {
                this.props.getAllUsers();
                this.props.getTicketList("ALL");
            })
        }

        if (nextProps.jiraOauthStatusError && nextProps.jiraOauthStatusError !== this.props.jiraOauthStatusError) {
            this.setState({
                message2: nextProps.jiraOauthStatusError.error,
                isOpen: true,
                type: 'error',
                logBoxLoader: false,
            })
        }
    }

    getPriorityArrowIcon(priority) {
        if (priority === "HIGH") {
            return <span className="text-danger">
                <i className="far fa-arrow-up text-danger"></i>
            </span>
        } else if (priority === "MEDIUM") {
            return <span className="text-warning">
                <i className="far fa-arrows-v text-orange"></i>
            </span>
        } else if (priority === "LOW") {
            return <span className="text-success">
                <i className="far fa-arrow-down text-success"></i>
            </span>
        }
    }

    onCloseHandler = () => {
        this.setState({
            message2: '',
            isOpen: false,
            type: ''
        })
    }

    onEnterPress(e) {
        if (e.key === 'Enter' && this.state.commentText === '' && e.shiftKey == false) {
            e.preventDefault();
            this.setState({
                emptyComment: true,
                send_message: this.state.commentText,
                commentText: '',
            });
        } else if (e.key === 'Enter' && e.shiftKey == false && this.state.commentText !== '') {
            e.preventDefault();
            this.payLoadGenerator();
        } else if (e.key === 'Enter' && this.state.commentText === '' && e.shiftKey == true) {
            e.preventDefault();
            this.setState({
                emptyComment: true,
                send_message: this.state.commentText,
                commentText: '',
            });
        } else {
            this.setState({
                emptyComment: false,
            });
        }
    }

    onSubmitComment = (event) => {
        event.preventDefault()
        this.payLoadGenerator()
    }

    payLoadGenerator() {
        let text = this.state.commentText
        let payload = {
            "description": text
        }
        if (this.state.commentId) {
            this.setState({ isEditing: true })
            payload = [...this.state.ticketComments].find((comment) => {
                return comment.commentId == this.state.commentId
            })
            payload.description = text
        }
        else {
            this.setState({ commentBoxLoader: true })
        }
        $('#commentForm')[0].reset();
        this.props.addCommentHandler(this.state.ticketDetails.id, payload);
    }

    ticketInputChangeHandler = (event, options) => {
        let ticketDetails = JSON.parse(JSON.stringify(this.state.ticketDetails)),
            errorCode = this.state.errorCode;
        if (options)
            ticketDetails[options.name] = event.value
        else
            ticketDetails[event.target.id] = event.target.value;
        this.setState({ ticketDetails, errorCode: 0 })
    }

    createOrEditTicket = (event) => {
        event.preventDefault();
        let ticketDetails = JSON.parse(JSON.stringify(this.state.ticketDetails));
        ticketDetails.description = ticketDetails.description.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
        ticketDetails.title = ticketDetails.title.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
        let missingValues = !(ticketDetails.priority && ticketDetails.type && ticketDetails.currentAssigneeId && ticketDetails.description && ticketDetails.title);
        if (missingValues) {
            this.setState({ errorCode: 1 })
            return;
        }
        delete ticketDetails.createdBy;
        delete ticketDetails.assignedTo;
        this.setState({
            modalLoader: true
        }, () => this.props.createOrEditTicket(ticketDetails))
    }

    handleChange(event) {
        this.setState({ commentText: event.target.value });
    }

    clearUpdateComment = (event) => {
        this.setState({
            commentId: '',
            commentText: ''
        })
    }

    updateTicketChangeHandler(selection, options) {
        let key = options.name;
        let ticketDetails = { ...this.state.ticketDetails }
        let previousValue = ticketDetails[key];
        ticketDetails[key] = selection.value;
        if (key === "currentAssigneeId")
            ticketDetails.assignedTo = selection.label;
        this.setState({ ticketDetails, changedStatus: key === "status" }, () => {
            this.props.createOrEditTicket(this.state.ticketDetails, "noSuccessModal", key === "status" ? `${key}_detailsModal_${previousValue}` : null);
        })
    }

    filterHandler = (value, options) => {
        let selectedFilters = JSON.parse(JSON.stringify(this.state.selectedFilters))
        selectedFilters[options.name] = value
        this.setState({
            selectedFilters,
        }
        )
    }

    newTicketInstances = () => {
        let options = JSON.parse(JSON.stringify(this.state.instanceList)).map(instance => {
            return {
                value: instance,
                label: instance
            }
        })
        this.state.instanceList.length ? options.unshift({
            value: "ALL",
            label: "ALL"
        }) : ''
        return options;
    }

    changeTicketOrder = (ticketsArray, oldIndex, newIndex) => {
        ticketsArray.splice(newIndex, 0, ticketsArray.splice(oldIndex, 1)[0]);
        return ticketsArray;
    };

    onDragEnd = (result, provided) => {
        const { destination, source, draggableId, type } = result;
        let tickets = JSON.parse(JSON.stringify(this.state.tickets));
        if (destination.droppableId === source.droppableId && destination.index !== source.index) {
            tickets[source.droppableId].ticketList = this.changeTicketOrder(tickets[source.droppableId].ticketList, source.index, destination.index)
            this.setState({ tickets })
            return;
        }
        let noDestination = !destination,
            sameSourceAndDestination = destination.droppableId === source.droppableId && destination.index === source.index,
            isResolvedAndNotDraggable = source.droppableId === "resolved" && (destination.droppableId !== "closed"),
            isClosedAndNotDraggable = source.droppableId === "closed" && destination.droppableId !== "reopen",
            isReOpenedAndNotDraggable = source.droppableId === "reopen" && destination.droppableId === "open",
            isOpenAndNotDraggable = source.droppableId === "open" && destination.droppableId === "reopen";
        if (noDestination || sameSourceAndDestination || isResolvedAndNotDraggable || isClosedAndNotDraggable || isReOpenedAndNotDraggable || isOpenAndNotDraggable) {
            return;
        }
        let draggedTicket = (JSON.parse(JSON.stringify(tickets))[source.droppableId]).ticketList.find(ticket => ticket.id === draggableId);
        tickets[source.droppableId].ticketList = JSON.parse(JSON.stringify(tickets))[source.droppableId].ticketList.filter(ticket => ticket.id !== draggableId);
        tickets[source.droppableId].totalCount--;
        draggedTicket.status = this.state.statusKeys.find(el => el.accessKey === destination.droppableId).type
        tickets[destination.droppableId].ticketList.push(draggedTicket);
        tickets[destination.droppableId].totalCount++;
        this.setState({ tickets }, () => this.props.createOrEditTicket(draggedTicket, "noSuccessModal", source.droppableId));
    }

    onDragStart = (result, provided) => {
        const { destination, source, draggableId, type } = result;
        let tickets = JSON.parse(JSON.stringify(this.state.tickets));

        let disableCondition = {
            open: ["REOPEN"],
            inProgress: ["REOPEN", "OPEN"],
            resolved: ["OPEN", "INPROGRESS", "REOPEN"],
            closed: ["OPEN", "INPROGRESS", "RESOLVED"],
            reopen: ["OPEN"],
        };

        Object.keys(tickets).map((temp) => {
            tickets[temp].isDisabled = disableCondition[source.droppableId].some((dropContainer) => dropContainer.toUpperCase() === temp.toUpperCase());
            return temp;
        })

        this.setState({ tickets });

    }

    closeModalHandler = () => {
        let ticketDetails = JSON.parse(JSON.stringify(this.state.emptyTicketDetails))
        this.setState({
            addTicketModal: false,
            ticketDetails,
            errorCode: 0,
        })
    }

    getBorderClassForModal = () => {
        switch (this.state.ticketDetails.status.toUpperCase()) {
            case "OPEN":
                return "borderOpen"
            case "INPROGRESS":
                return "borderInProgress"
            case "CLOSED":
                return "borderClosed"
            case "RESOLVED":
                return "borderResolved"
            case "REOPEN":
                return "borderReOpen"
        }
    }

    getFilteredTicketStatusList = () => {
        let statusList = this.state.statusKeys.filter((type) => {
            let conditionForOpen = this.state.ticketDetails.status === "OPEN" && type.value === "REOPEN",
                conditionForInprogress = this.state.ticketDetails.status === "INPROGRESS" && (type.value === "REOPEN" || type.value === "OPEN"),
                conditionForReopen = this.state.ticketDetails.status === "REOPEN" && (type.value === "OPEN"),
                conditionForClosed = this.state.ticketDetails.status === "CLOSED" && (type.value === "OPEN" || type.value === "INPROGRESS" || type.value === "RESOLVED"),
                conditionForResolved = this.state.ticketDetails.status === "RESOLVED" && (type.value === "OPEN" || type.value === "INPROGRESS" || type.value === "REOPEN");
            if (conditionForOpen || conditionForInprogress || conditionForReopen || conditionForClosed || conditionForResolved) {
                return false
            }
            return true
        })
        return statusList
    }

    closeTicketDetailsModal = () => {
        let tickets = JSON.parse(JSON.stringify(this.state.tickets)),
            allTickets = [].concat.apply([], (Object.keys(this.state.tickets)).map(key => this.state.tickets[key].ticketList)),
            previousStatus = JSON.parse(JSON.stringify(allTickets.find(ticket => ticket.id === this.state.ticketDetails.id))).status,
            currentStatusKey = this.state.statusKeys.find(el => el.type.toUpperCase() === previousStatus.toUpperCase()).accessKey;
        if (this.state.changedStatus) {
            tickets[currentStatusKey].ticketList = tickets[currentStatusKey].ticketList.filter(ticket => ticket.id !== this.state.ticketDetails.id);
            let newStatusKey = this.state.statusKeys.find(el => el.type === this.state.ticketDetails.status).accessKey;
            tickets[newStatusKey].ticketList.push(this.state.ticketDetails);
        }
        else {
            let ticketIndex = tickets[currentStatusKey].ticketList.findIndex(ticket => ticket.id === this.state.ticketDetails.id);
            tickets[currentStatusKey].ticketList[ticketIndex] = this.state.ticketDetails;
        }
        this.setState({
            tickets,
            showTicketDetailsModal: false,
            ticketDetails: JSON.parse(JSON.stringify(this.state.emptyTicketDetails)),
            changedStatus: false,
            ticketComments: [],
            ticketLogs: [],
        })
    }

    changePage = (type, status, statusKey, fetchedTicketsCount, currentPageNumber) => {
        let tickets = JSON.parse(JSON.stringify(this.state.tickets)),
            ticketsDoNotNeedToBeFetched = (fetchedTicketsCount / 10) < (currentPageNumber);
        if (type === "previous") {
            tickets[statusKey].activePageNumber -= 1
        }
        else {
            if (ticketsDoNotNeedToBeFetched)
                tickets[statusKey].activePageNumber += 1;
            else
                tickets[statusKey].loader = true;
        }
        this.setState({ tickets }, () => !ticketsDoNotNeedToBeFetched && this.props.getTicketList(status, currentPageNumber))
    }

    getFilteredTicketList = () => {
        let filteredTicketList = {
            open: {},
            inProgress: {},
            resolved: {},
            closed: {},
            reopen: {},
        };
        let isFiltered = Object.values(this.state.selectedFilters).some(filter => filter.value !== "ALL")
        Object.entries(this.state.tickets).map(([type, ticketData]) => {
            filteredTicketList[type].ticketList = ticketData.ticketList ? (ticketData.ticketList.filter(ticket => {
                return (((ticket.type === this.state.selectedFilters.type.value) || (this.state.selectedFilters.type.value === "ALL")) && ((ticket.priority === this.state.selectedFilters.priority.value) || (this.state.selectedFilters.priority.value === "ALL")) && ((ticket.currentAssigneeId === this.state.selectedFilters.currentAssigneeId.value) || (this.state.selectedFilters.currentAssigneeId.value === "ALL")))
            })) : [];
            filteredTicketList[type].totalPages = isFiltered ? Math.ceil(filteredTicketList[type].ticketList.length / 10) : ticketData.totalPages || 1;
            filteredTicketList[type].activePageNumber = ticketData.activePageNumber || 1;
            filteredTicketList[type].loader = ticketData.loader;
            filteredTicketList[type].totalCount = isFiltered ? filteredTicketList[type].ticketList.length : ticketData.totalCount;
            filteredTicketList[type].isDisabled = ticketData.isDisabled;
        })
        return filteredTicketList;
    }

    switchTabs = () => {
        let noComments = !this.state.ticketComments.length,
            noLogs = !this.state.ticketLogs.length,
            isCommentsTabActive = !this.state.isCommentsTabActive;
        this.setState({
            isCommentsTabActive,
            commentBoxLoader: (isCommentsTabActive && noComments),
            logBoxLoader: (!isCommentsTabActive && noLogs)
        }, () => {
            if (this.state.commentBoxLoader)
                this.props.getTicketComments(this.state.ticketDetails.id);
            else if (this.state.logBoxLoader)
                this.props.getTicketLogs(this.state.ticketDetails.id);
        })
    }

    formatOptionLabel = ({ value, label, className }) => {
        return (<div >
            <i className={className}></i>{label}
        </div>)
    };

    formatOptionsForStatus = ({ displayName }) => {
        return <div >{displayName} </div>
    };

    getReactSelectValue = (value, data) => {
        return data.find(el => el.value.toUpperCase() === value.toUpperCase())
    }

    render() {
        let filteredTicketList = this.getFilteredTicketList()
        return (
            <React.Fragment>
                <Helmet>
                    <title>Tickets</title>
                    <meta name="description" content="M83-TicketManagement" />
                </Helmet>

                <div className="pageBreadcrumb">
                    <div className="row">
                        <div className="col-8">
                            <p><span>Tickets</span></p>
                        </div>
                        <div className="col-4 text-right">
                            {this.state.jiraStatus && (this.state.jiraStatus.jiraAvailable && this.state.jiraStatus.jiraConfigured && localStorage.isJiraLoggedIn === "true" || !this.state.jiraStatus.jiraAvailable) && <div className="flex h-100 justify-content-end align-items-center">
                                <button className="btn btn-primary" data-toggle="modal" onClick={() => this.setState({ addTicketModal: true })} >
                                    <span className="btn-primary-icon"><i className="far fa-plus" /></span>
                                </button>
                            </div>
                            }
                        </div>
                    </div>
                </div>

                {this.state.isFetchingJiraStatus ?
                    <Loader />
                    :
                    this.state.jiraStatus.jiraAvailable && this.state.jiraStatus.jiraConfigured && localStorage.isJiraLoggedIn === "true" || !this.state.jiraStatus.jiraAvailable ?
                        <div className="outerBox pb-0">
                            <div className="flex">
                                <div className="flex-item fx-b33 pd-l-7">
                                    <div className="form-group">
                                        <label className="form-label">Assignee :</label>
                                        <ReactSelect
                                            name="currentAssigneeId"
                                            options={this.state.userList}
                                            value={this.state.selectedFilters.currentAssigneeId}
                                            onChange={this.filterHandler}
                                            isMulti={false}
                                            className="form-control-multi-select assigneeReactSelect" >
                                        </ReactSelect>
                                    </div>
                                </div>

                                <div className="flex-item fx-b33 pd-l-7">
                                    <div className="form-group">
                                        <label className="form-label">Type :</label>
                                        <ReactSelect
                                            name="type"
                                            options={this.state.ticketTypes}
                                            value={this.state.selectedFilters.type}
                                            onChange={this.filterHandler}
                                            isMulti={false}
                                            className="form-control-multi-select" >
                                        </ReactSelect>
                                    </div>
                                </div>

                                <div className="flex-item fx-b33 pd-r-7 pd-l-7">
                                    <div className="form-group">
                                        <label className="form-label">Priority :</label>
                                        <ReactSelect
                                            name="priority"
                                            options={this.state.priorityOptions}
                                            value={this.state.selectedFilters.priority}
                                            onChange={this.filterHandler}
                                            isMulti={false}
                                            className="form-control-multi-select" >
                                        </ReactSelect>
                                    </div>
                                </div>
                                <div className="flex-item fx-b40">

                                </div>

                            </div>
                            {this.state.isFetching > 0 ? <Loader /> :
                                <DragDropContext onDragEnd={this.onDragEnd} onDragStart={this.onDragStart}>
                                    <Droppable droppableId="all-columns" direction="horizontal" type="column">
                                        {provided => (
                                            <Container{...provided.droppableProps} innerRef={provided.innerRef}>
                                                {/*this.state.columnOrder.map((columnId, index) => {
                                                const column = this.state.columns[columnId];
                                                return (
                                                    <InnerList
                                                        key={column.id}
                                                        column={column}
                                                        ticketMap={this.state.tickets}
                                                        index={index}
                                                    />
                                                );
                                            })*/}
                                                {Object.entries(filteredTicketList).length && Object.entries(filteredTicketList).map(([ticketType, ticketsData], index) => {
                                                    const status = this.state.statusKeys.find(el => el.accessKey === ticketType).type,
                                                        activePageNumber = ticketsData.activePageNumber,
                                                        endIndex = ticketsData.activePageNumber * 10,
                                                        tickets = ticketsData.ticketList.slice(endIndex - 10, endIndex),
                                                        column = {
                                                            id: ticketType,
                                                            title: this.state.statusKeys.find(el => el.accessKey === ticketType).displayName,
                                                            tickets,
                                                            totalCount: ticketsData.totalCount,
                                                            totalPages: ticketsData.totalPages,
                                                            activePageNumber,
                                                            loader: ticketsData.loader,
                                                            isDropDisabled: ticketsData.isDisabled
                                                        };
                                                    return (
                                                        <InnerList
                                                            key={column.id}
                                                            column={column}
                                                            isDropDisabled={column.isDropDisabled}
                                                            index={index}
                                                            ticketEditHandler={(e, ticketId, showDetailsModal) => this.openTicketModalHandler(e, ticketId, showDetailsModal)}
                                                            addOrEditTicketModal={this.state.addTicketModal}
                                                            changePage={(type) => this.changePage(type, status, ticketType, ticketsData.ticketList.length, activePageNumber)}
                                                        />
                                                    );
                                                })}
                                                {provided.placeholder}
                                            </Container>
                                        )}
                                    </Droppable>
                                </DragDropContext>
                            }
                        </div>
                        :
                        <div className="contentAddBox">
                            <div className="contentAddBoxDetail">
                                <div className="contentAddBoxImage">
                                    <img src="https://content.iot83.com/m83/icons/jira.png" />
                                </div>

                                {this.state.jiraStatus.jiraConfigured && localStorage.isJiraLoggedIn !== "true" ?
                                    <h6>You must be connected to the JIRA. Please connect and try again.</h6> :
                                    localStorage.role === "ACCOUNT_ADMIN" ?
                                        <React.Fragment>
                                            <h6>JIRA is not configured. Please configure and connect.</h6>
                                            <button type="button" onClick={() => this.props.history.push("/oAuthConfigurations")} className="btn btn-primary" ><i className="fal fa-tools"></i>Configure</button>
                                        </React.Fragment> :
                                        <h6>JIRA is not configured. Please connect your administrator.</h6>
                                }
                            </div>
                        </div>
                }


                {this.state.showTicketDetailsModal &&
                    <div className="modal fade show d-block" tabIndex="-1" data-backdrop="false">
                        <div className="modal-dialog modal-lg animated slideInDown" role="document">
                            <div className="modal-content">
                                {this.state.isFetchingTicketDetails ?
                                    <div className="ticketLoadingBox">
                                        <div className="loadSpinner">
                                            <div className="cube1"></div>
                                            <div className="cube2"></div>
                                        </div>
                                    </div> :
                                    <div className={'ticketDetailCard ' + this.getBorderClassForModal()}>
                                        <button className="btn close" onClick={this.closeTicketDetailsModal}><i className="far fa-times-circle"></i></button>
                                        <div className="ticketDetails">
                                            <h5>{this.state.ticketDetails.ticketNo}</h5>
                                            <ul>
                                                <li className="pd-r-5">
                                                    <div className="form-group">
                                                        <label className="form-label">Status :</label>
                                                        <ReactSelect
                                                            name="status"
                                                            options={this.getFilteredTicketStatusList()}
                                                            formatOptionLabel={this.formatOptionsForStatus}
                                                            value={this.getReactSelectValue(this.state.ticketDetails.status, this.getFilteredTicketStatusList())}
                                                            onChange={(value, options) => this.updateTicketChangeHandler(value, options)}
                                                            isMulti={false}
                                                            className="form-control-multi-select" >
                                                        </ReactSelect>
                                                    </div>
                                                </li>

                                                <li className="pd-l-5 pd-r-5 mr-0">
                                                    <div className="form-group">
                                                        <label className="form-label">Priority :</label>
                                                        <ReactSelect
                                                            name="priority"
                                                            options={this.state.priorityOptions.slice(1, this.state.priorityOptions.length)}
                                                            isDisabled={this.state.ticketDetails.status.toUpperCase() === "CLOSED"}
                                                            formatOptionLabel={this.formatOptionLabel}
                                                            value={this.getReactSelectValue(this.state.ticketDetails.priority, this.state.priorityOptions)}
                                                            onChange={(value, options) => this.updateTicketChangeHandler(value, options)}
                                                            isMulti={false}
                                                            className="form-control-multi-select" >
                                                        </ReactSelect>
                                                    </div>
                                                </li>
                                                <li className="pd-l-5 mr-0">
                                                    <div className="form-group">
                                                        <label className="form-label">Assignee :</label>
                                                        <ReactSelect
                                                            name="currentAssigneeId"
                                                            isDisabled={this.state.ticketDetails.status.toUpperCase() === "CLOSED"}
                                                            options={this.state.userList.slice(1, this.state.userList.length)}
                                                            value={this.getReactSelectValue(this.state.ticketDetails.currentAssigneeId, this.state.userList)}
                                                            onChange={(value, options) => this.updateTicketChangeHandler(value, options)}
                                                            isMulti={false}
                                                            className="form-control-multi-select" >
                                                        </ReactSelect>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                        <div className="ticketDescription">
                                            <h4><strong>Title :</strong>{this.state.ticketDetails.title}</h4>
                                            <p><strong>Description :</strong> {this.state.ticketDetails.description}</p>
                                            <div className="ticketIconGroup">
                                                <button className="btn btn-transparent btn-transparent-primary" disabled><i className="far fa-paperclip"></i></button>
                                                <button className="btn btn-transparent btn-transparent-danger" disabled><i className="far fa-images"></i></button>
                                                <button className="btn btn-transparent btn-transparent-success" disabled><i className="far fa-share-alt"></i></button>
                                                <button className="btn btn-transparent btn-transparent-warning" disabled><i className="far fa-file-download"></i></button>
                                            </div>
                                        </div>
                                        <div className="ticketComments">
                                            <ul className="nav nav-tabs">
                                                <li className="nav-item">
                                                    <a className={this.state.isCommentsTabActive ? "nav-link active" : "nav-link"} onClick={this.switchTabs}>Comments</a>
                                                </li>
                                                <li className="nav-item">
                                                    <a className={!this.state.isCommentsTabActive ? "nav-link active" : "nav-link"} onClick={this.switchTabs}>Logs</a>
                                                </li>
                                            </ul>
                                            <div className="tab-content" id="myTabContent">
                                                {this.state.isCommentsTabActive ? <div className="tab-pane d-block">
                                                    <form id="commentForm" className="commentForm" onSubmit={this.onSubmitComment}>
                                                        <div className="input-group">
                                                            <span className="initialsImage inputInitialsImage">
                                                                {getInitials(localStorage.Username)}
                                                            </span>
                                                            <textarea rows="1" onKeyDown={this.onEnterPress} value={this.state.commentText}
                                                                onChange={this.handleChange}
                                                                ref="comment" required
                                                                className={this.state.emptyComment ? "form-control form-control-error" : "form-control"}
                                                                placeholder="Add comment....."
                                                            />
                                                            {this.state.commentId ?
                                                                <React.Fragment>
                                                                    <button className="btn btn-primary"><i className="fal fa-paper-plane"></i>Update</button>
                                                                    <button type="button" className="btn btn-secondary" onClick={this.clearUpdateComment}>
                                                                        <i className="far fa-times"></i>Cancel
                                                                        </button>
                                                                </React.Fragment> :
                                                                <button className="btn btn-success"><i className="fal fa-paper-plane"></i>Post</button>
                                                            }
                                                        </div>
                                                    </form>
                                                    <div className="commentsOuterBox">
                                                        <ul className="commentsList">
                                                            {this.state.commentBoxLoader &&
                                                                <li>
                                                                    <div className="commentBox commentBoxLoader" id="loaderdiv">
                                                                        <span className="initialsImage inputInitialsImage">
                                                                            <i className="far fa-user"></i>
                                                                        </span>
                                                                        <div className="commentDetail">
                                                                            <span className="commentBoxLoaderTime"></span>
                                                                            <span className="commentBoxLoaderDesc"></span>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            }
                                                            {this.state.ticketComments.map((comment) => {
                                                                if (comment.commentId === this.state.commentId && this.state.isEditing) {
                                                                    return <li key={comment.commentId}>
                                                                        <div className="commentBox commentBoxLoader">
                                                                            <span className="initialsImage inputInitialsImage">
                                                                                <i className="far fa-user"></i>
                                                                            </span>
                                                                            <div className="commentDetail">
                                                                                <span className="commentBoxLoaderTime"></span>
                                                                                <span className="commentBoxLoaderDesc"></span>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                }
                                                                else {
                                                                    return <li key={comment.commentId}>
                                                                        <div className="commentBox">
                                                                            <span className="initialsImage inputInitialsImage">
                                                                                {getInitials(comment.postedBy)}
                                                                            </span>
                                                                            <div className="commentDetail">
                                                                                <small>{new Date(Number(comment.updatedOn)).toLocaleString('en-US', { timeZone: localStorage.timeZone })}</small>
                                                                                <p>{comment.description}</p>
                                                                                {comment.commentId !== this.state.commentId && <button className={`btn btn-transparent ${comment.postedBy !== localStorage["Username"] ? "btn-disabled" : ""}`} disabled={comment.postedBy !== localStorage["Username"]} onClick={() => { this.setState({ commentText: comment.description, commentId: comment.commentId }) }}>
                                                                                    <i className="far fa-pen text-primary" />
                                                                                </button>}
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                }
                                                            })}
                                                        </ul>
                                                    </div>
                                                </div> :
                                                    <div className="tab-pane d-block">
                                                        <div className="commentsOuterBox">
                                                            <ul className="commentsList">
                                                                {this.state.logBoxLoader &&
                                                                    <li>
                                                                        <div className="commentBox commentBoxLoader" id="loaderdiv">
                                                                            <span className="initialsImage inputInitialsImage">
                                                                                <i className="far fa-user"></i>
                                                                            </span>
                                                                            <div className="commentDetail">
                                                                                <span className="commentBoxLoaderTime"></span>
                                                                                <span className="commentBoxLoaderDesc"></span>
                                                                            </div>
                                                                        </div>
                                                                    </li>}

                                                                {this.state.ticketLogs.map((log, index) =>
                                                                    <li key={index}>
                                                                        <div className="commentBox">
                                                                            <span className="initialsImage inputInitialsImage">
                                                                                {log.updatedBy ? getInitials(log.updatedBy) : "N/A"}
                                                                            </span>
                                                                            <div className="commentDetail">
                                                                                <small>{log.updatedAt ? new Date(Number(log.updatedAt)).toLocaleString('en-US', { timeZone: localStorage.timeZone }) : "N/A"}</small>
                                                                                <p>{`${log.updatedBy} ${log.description}`}</p>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                )
                                                                }
                                                            </ul>
                                                        </div>
                                                    </div>
                                                }
                                            </div>
                                        </div>
                                    </div>
                                }
                            </div>
                        </div>
                    </div>
                }

                {/* add new ticket */}
                <SlidingPane
                    className=''
                    overlayClassName='slidingFormOverlay'
                    closeIcon={<div></div>}
                    isOpen={this.state.addTicketModal || false}
                    from='right'
                    width='550px'
                >
                    <div className="modal-content">
                        <div className="modal-header">
                            <h6 className="modal-title">{this.state.ticketDetails.id ? "Update Ticket" : "Add new Ticket"}
                                <button className="close" onClick={this.closeModalHandler}><i className="far fa-angle-right"></i></button>
                            </h6>
                        </div>

                        {this.state.modalLoader ?
                            <div className="modal-loader">
                                <i className="fad fa-sync-alt fa-spin text-primary"></i>
                            </div> :

                            <form onSubmit={this.createOrEditTicket}>
                                <div className="modal-body">
                                    {this.state.errorCode === 1 &&
                                        <p className="modalErrorText"><i className="fas fa-exclamation-triangle mr-r-7"></i>Please fill all the mandatory fields(marked with asterisk)</p>
                                    }

                                    <div className="form-group">
                                        <input type="text" name="ticketTitle" placeholder="Ticket Title" disabled={this.state.ticketDetails.status.toUpperCase() === "CLOSED"} autoFocus required className="form-control" id="title"
                                            value={this.state.ticketDetails.title} onChange={this.ticketInputChangeHandler} />
                                        <label className="form-group-label" htmlFor="ticketTitle">Ticket Title :
                                                <span className="requiredMark"><i className="fa fa-asterisk" /></span>
                                        </label>
                                    </div>

                                    <div className="form-group">
                                        <textarea rows="4" name="ticketDesc" className="form-control" placeholder="Description"
                                            id="description" value={this.state.ticketDetails.description} disabled={this.state.ticketDetails.status.toUpperCase() === "CLOSED"} onChange={this.ticketInputChangeHandler} >
                                        </textarea>
                                        <label className="form-group-label" htmlFor="ticketDesc">Description :
                                                <span className="requiredMark"><i className="fa fa-asterisk" /></span>
                                        </label>
                                    </div>

                                    <div className="form-group">
                                        <label className="form-label">Assignee : <span className="requiredMark"><i className="fa fa-asterisk" /></span></label>
                                        <ReactSelect
                                            name="currentAssigneeId"
                                            isDisabled={this.state.ticketDetails.status.toUpperCase() === "CLOSED"}
                                            options={this.state.userList.slice(1, this.state.userList.length)}
                                            value={this.getReactSelectValue(this.state.ticketDetails.currentAssigneeId, this.state.userList)}
                                            onChange={this.ticketInputChangeHandler}
                                            isMulti={false}
                                            className="form-control-multi-select" >
                                        </ReactSelect>
                                    </div>

                                    <div className="form-group">
                                        <label className="form-label">Type : <span className="requiredMark"><i className="fa fa-asterisk" /></span></label>
                                        <ReactSelect
                                            name="type"
                                            isDisabled={this.state.ticketDetails.status.toUpperCase() === "CLOSED"}
                                            options={this.state.ticketTypes.slice(1, this.state.ticketTypes.length)}
                                            value={this.getReactSelectValue(this.state.ticketDetails.type, this.state.ticketTypes)}
                                            onChange={this.ticketInputChangeHandler}
                                            isMulti={false}
                                            className="form-control-multi-select" >
                                        </ReactSelect>
                                    </div>

                                    <div className="form-group">
                                        <label className="form-label">Priority : <span className="requiredMark"><i className="fa fa-asterisk" /></span></label>
                                        <ReactSelect
                                            name="priority"
                                            isDisabled={this.state.ticketDetails.status.toUpperCase() === "CLOSED"}
                                            options={this.state.priorityOptions.slice(1, this.state.priorityOptions.length)}
                                            value={this.getReactSelectValue(this.state.ticketDetails.priority, this.state.priorityOptions)}
                                            onChange={this.ticketInputChangeHandler}
                                            isMulti={false}
                                            className="form-control-multi-select" >
                                        </ReactSelect>
                                    </div>
                                </div>

                                <div className="modal-footer">
                                    <button className="btn btn-success" disabled={this.state.errorCode || this.state.ticketDetails.status.toUpperCase() === "CLOSED"}>{this.state.ticketDetails.id ? 'Update' : 'Save'}</button>
                                    <button type="button" className="btn btn-dark" onClick={this.closeModalHandler}>Cancel</button>
                                </div>
                            </form>
                        }
                    </div>
                </SlidingPane>
                {/* end add new ticket */}

                {this.state.isOpen &&
                    <NotificationModal
                        type={this.state.type}
                        message2={this.state.message2}
                        onCloseHandler={this.onCloseHandler}
                    />
                }
            </React.Fragment>
        );
    }
}

TicketList.propTypes = {
    dispatch: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
    userList: SELECTORS.getUsersListSuccess(),
    userListError: SELECTORS.getUsersListError(),
    ticketDetails: SELECTORS.getTicketDeatilsSuccess(),
    ticketDetailsError: SELECTORS.getTicketDeatilsError(),
    ticketComments: SELECTORS.getTicketCommentsSuccess(),
    ticketCommentsError: SELECTORS.getTicketCommentsError(),
    addCommentSuccess: SELECTORS.addCommentsSuccess(),
    addCommentError: SELECTORS.addCommentsError(),
    addTicketSuccess: SELECTORS.addTicketSuccess(),
    addTicketError: SELECTORS.addTicketError(),
    openTicketList: SELECTORS.openTicketList(),
    openTicketListError: SELECTORS.openTicketListError(),
    closedTicketList: SELECTORS.closedTicketList(),
    closedTicketListError: SELECTORS.closedTicketListError(),
    reOpenedTicketList: SELECTORS.reOpenedTicketList(),
    reOpenedTicketListError: SELECTORS.reOpenedTicketListError(),
    resolvedTicketList: SELECTORS.resolvedTicketList(),
    resolvedTicketListError: SELECTORS.resolvedTicketListError(),
    inProgressTicketList: SELECTORS.inProgressTicketList(),
    inProgressTicketListError: SELECTORS.inProgressTicketListError(),
    ticketLogs: SELECTORS.ticketLogs(),
    ticketLogsError: SELECTORS.ticketLogsError(),
    jiraOauthStatusError: SELECTORS.jiraOauthStatusError(),
    jiraOauthStatusSuccess: SELECTORS.jiraOauthStatusSuccess(),
    createJiraProjectError: SELECTORS.createJiraProjectError(),
    createJiraProjectSuccess: SELECTORS.createJiraProjectSuccess(),
});

function mapDispatchToProps(dispatch) {
    return {
        dispatch,
        getTicketList: (type, currentPageNumber) => dispatch(ACTIONS.getTicketList(type, currentPageNumber)),
        getAllUsers: () => dispatch(ACTIONS.getAllUsers()),
        getTicketDetails: (id) => dispatch(ACTIONS.getTicketDetails(id)),
        getTicketComments: (id) => dispatch(ACTIONS.getTicketComments(id)),
        getTicketLogs: (id) => dispatch(ACTIONS.getTicketLogs(id)),
        addCommentHandler: (id, payload) => dispatch(ACTIONS.addCommentHandler(id, payload)),
        createOrEditTicket: (payload, noModal, previousStatus) => dispatch(ACTIONS.createOrEditTicket(payload, noModal, previousStatus)),
        getJiraOauthStatus: () => dispatch(ACTIONS.getJiraOauthStatus()),
        createJiraProject: () => dispatch(ACTIONS.createJiraProject()),
    };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "ticketList", reducer });
const withSaga = injectSaga({ key: "ticketList", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(TicketList);
