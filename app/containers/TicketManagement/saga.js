/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { takeEvery } from 'redux-saga';
import * as CONSTANTS from "./constants";
import { apiCallHandler } from '../../api';

export function* getListApiHandlerAsync(action) {
    let constants = JSON.parse(JSON.stringify(action.constants))
    for (let i = 0; i < constants.length; i++) {
        let payload = {
            "max": 10,
            "offset": action.currentPageNumber ? (action.currentPageNumber * 10) : 0,
            "status": constants[i].status
        }
        action.payload = payload;
        yield [apiCallHandler(action, constants[i].success, constants[i].failure, 'getTicketList')];
    }
}

export function* getUsersListApiHandlerAsync(action) {
    yield [apiCallHandler(action, CONSTANTS.GET_USERS_SUCCESS, CONSTANTS.GET_USERS_FAILURE, JSON.parse(localStorage.isJiraLoggedIn) ? "getJiraUsers" : 'getUsersWithoutPagination')];
}

export function* getTicketDetailsApiHandlerAsync(action) {
    yield [apiCallHandler(action, CONSTANTS.TICKET_DETAILS_SUCCESS, CONSTANTS.TICKET_DETAILS_ERROR, 'getTicketDetails')];
}

export function* getTicketCommentsApiHandlerAsync(action) {
    yield [apiCallHandler(action, CONSTANTS.TICKET_COMMENTS_SUCCESS, CONSTANTS.TICKET_COMMENTS_ERROR, 'getTicketComments', false)];
}

export function* addCommentApiHandlerAsync(action) {
    yield [apiCallHandler(action, CONSTANTS.ADD_COMMENTS_SUCCESS, CONSTANTS.ADD_COMMENTS_ERROR, 'saveTicketComment', false)];
}

export function* addTicketApiHandlerAsync(action) {
    yield [apiCallHandler(action, CONSTANTS.ADD_TICKET_SUCCESS, CONSTANTS.ADD_TICKET_ERROR, 'saveTicketDetails')];
}

export function* deleteTicketsApiHandlerAsync(action) {
    yield [apiCallHandler(action, CONSTANTS.DELETE_TICKETS_SUCCESS, CONSTANTS.DELETE_TICKETS_ERROR, 'deleteTickets')];
}
export function* getJiraOauthStatusApiHandlerAsync(action) {
    yield [apiCallHandler(action, CONSTANTS.GET_JIRA_OAUTH_STATUS_SUCCESS, CONSTANTS.GET_JIRA_OAUTH_STATUS_ERROR, 'getJiraOauthStatus', false)];
}

export function* getJiraProjectStatusApiHandlerAsync(action) {
    yield [apiCallHandler(action, CONSTANTS.CREATE_JIRA_PROJECT__SUCCESS, CONSTANTS.CREATE_JIRA_PROJECT__ERROR, 'createJiraProject', false)];
}

export function* getTicketLogsApiHandlerAsync(action) {
    yield [apiCallHandler(action, CONSTANTS.TICKET_LOGS_SUCCESS, CONSTANTS.TICKET_LOGS_ERROR, 'fetchTicketLogs', false)];
}

export function* watcherGetListRequest() {
    yield takeEvery(CONSTANTS.GET_TICKET_LIST, getListApiHandlerAsync);
}

export function* watcherGetUsersListRequest() {
    yield takeEvery(CONSTANTS.GET_USERS, getUsersListApiHandlerAsync);
}

export function* watcherGetTicketDetailsRequest() {
    yield takeEvery(CONSTANTS.FETCH_TICKET_DETAILS, getTicketDetailsApiHandlerAsync);
}

export function* watcherGetTicketCommentsRequest() {
    yield takeEvery(CONSTANTS.FETCH_TICKET_COMMENTS, getTicketCommentsApiHandlerAsync);
}

export function* watcherAddCommentRequest() {
    yield takeEvery(CONSTANTS.ADD_COMMENT_REQUEST, addCommentApiHandlerAsync);
}

export function* watcherAddTicketRequest() {
    yield takeEvery(CONSTANTS.ADD_TICKET_REQUEST, addTicketApiHandlerAsync);
}

export function* watcherDeleteTickets() {
    yield takeEvery(CONSTANTS.DELETE_TICKETS, deleteTicketsApiHandlerAsync);
}

export function* watcherGetTicketLogs() {
    yield takeEvery(CONSTANTS.FETCH_TICKET_LOGS, getTicketLogsApiHandlerAsync);
}

export function* watcherGetJiraOauthStatus() {
    yield takeEvery(CONSTANTS.GET_JIRA_OAUTH_STATUS_REQUEST, getJiraOauthStatusApiHandlerAsync);
}

export function* watcherGetJiraProjectStatus() {
    yield takeEvery(CONSTANTS.CREATE_JIRA_PROJECT__REQUEST, getJiraProjectStatusApiHandlerAsync);
}

export default function* rootSaga() {
    yield [
        watcherGetListRequest(),
        watcherGetUsersListRequest(),
        watcherGetTicketDetailsRequest(),
        watcherGetTicketCommentsRequest(),
        watcherAddCommentRequest(),
        watcherAddTicketRequest(),
        watcherDeleteTickets(),
        watcherGetTicketLogs(),
        watcherGetJiraOauthStatus(),
        watcherGetJiraProjectStatus()
    ];
}
