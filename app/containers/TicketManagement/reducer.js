/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { fromJS } from "immutable";
import * as CONSTANTS from "./constants";

export const initialState = fromJS({});

function ticketListReducer(state = initialState, action) {
    switch (action.type) {
        case CONSTANTS.GET_OPEN_TICKET_LIST_SUCCESS:
            return Object.assign({}, state, {
                openTicketList: { tickets: action.response, timestamp: Date.now(), currentPageNumber: action.currentPageNumber }
            });
        case CONSTANTS.GET_OPEN_TICKET_LIST_FAILURE:
            return Object.assign({}, state, {
                openTicketListError: { error: action.error, timestamp: Date.now() }
            });
        case CONSTANTS.GET_CLOSED_TICKET_LIST_SUCCESS:
            return Object.assign({}, state, {
                closedTicketList: { tickets: action.response, timestamp: Date.now(), currentPageNumber: action.currentPageNumber }
            });
        case CONSTANTS.GET_CLOSED_TICKET_LIST_FAILURE:
            return Object.assign({}, state, {
                closedTicketListError: { error: action.error, timestamp: Date.now() }
            });
        case CONSTANTS.GET_INPROGESS_TICKET_LIST_SUCCESS:
            return Object.assign({}, state, {
                inProgressTicketList: { tickets: action.response, timestamp: Date.now(), currentPageNumber: action.currentPageNumber }
            });
        case CONSTANTS.GET_INPROGESS_TICKET_LIST_FAILURE:
            return Object.assign({}, state, {
                inProgressTicketListError: { error: action.error, timestamp: Date.now() }
            });
        case CONSTANTS.GET_RESOLVED_TICKET_LIST_SUCCESS:
            return Object.assign({}, state, {
                resolvedTicketList: { tickets: action.response, timestamp: Date.now(), currentPageNumber: action.currentPageNumber }
            });
        case CONSTANTS.GET_RESOLVED_TICKET_LIST_FAILURE:
            return Object.assign({}, state, {
                resolvedTicketListError: { error: action.error, timestamp: Date.now() }
            });
        case CONSTANTS.GET_REOPENED_TICKET_LIST_SUCCESS:
            return Object.assign({}, state, {
                reOpenedTicketList: { tickets: action.response, timestamp: Date.now(), currentPageNumber: action.currentPageNumber }
            });
        case CONSTANTS.GET_REOPENED_TICKET_LIST_FAILURE:
            return Object.assign({}, state, {
                reOpenedTicketListError: { error: action.error, timestamp: Date.now() }
            });
        case CONSTANTS.GET_USERS_SUCCESS:
            return Object.assign({}, state, {
                usersList: action.response
            });
        case CONSTANTS.GET_USERS_FAILURE:
            return Object.assign({}, state, {
                usersListError: { error: action.error, timestamp: Date.now() }
            });
        case CONSTANTS.ADD_TICKET_SUCCESS:
            return Object.assign({}, state, {
                addTicketSuccess: { message: action.response, timestamp: Date.now(), noModal: action.noModal },

            });
        case CONSTANTS.ADD_TICKET_ERROR:
            return Object.assign({}, state, {
                addTicketError: { error: action.error, timestamp: Date.now(), previousStatus: action.addOns.previousStatus, ticket: action.addOns.payload }
            });
        case CONSTANTS.TICKET_DETAILS_SUCCESS:
            return Object.assign({}, state, {
                ticketDetails: action.response
            });
        case CONSTANTS.TICKET_DETAILS_ERROR:
            return Object.assign({}, state, {
                ticketDetailsError: action.error
            });
        case CONSTANTS.TICKET_COMMENTS_SUCCESS:
            return Object.assign({}, state, {
                ticketComments: action.response
            });
        case CONSTANTS.TICKET_COMMENTS_ERROR:
            return Object.assign({}, state, {
                ticketCommentsError: action.error
            });
        case CONSTANTS.ADD_COMMENTS_SUCCESS:
            return Object.assign({}, state, {
                addCommentSuccess: action.response
            });
        case CONSTANTS.ADD_COMMENTS_ERROR:
            return Object.assign({}, state, {
                addCommentError: action.error
            });
        case CONSTANTS.TICKET_LOGS_SUCCESS:
            return Object.assign({}, state, {
                ticketLogs: action.response
            });
        case CONSTANTS.TICKET_LOGS_ERROR:
            return Object.assign({}, state, {
                ticketLogsError: { error: action.error, timestamp: Date.now() }
            });
        case CONSTANTS.GET_JIRA_OAUTH_STATUS_SUCCESS:
            return Object.assign({}, state, {
                jiraOauthStatusSuccess: action.response
            });
        case CONSTANTS.GET_JIRA_OAUTH_STATUS_ERROR:
            return Object.assign({}, state, {
                jiraOauthStatusError: { error: action.error, timestamp: Date.now() }
            });
        case CONSTANTS.CREATE_JIRA_PROJECT__SUCCESS:
            return Object.assign({}, state, {
                createJiraProjectSuccess: action.response
            });
        case CONSTANTS.CREATE_JIRA_PROJECT__ERROR:
            return Object.assign({}, state, {
                createJiraProjectError: { error: action.error, timestamp: Date.now() }
            });
        default:
            return state;
    }
}

export default ticketListReducer;
