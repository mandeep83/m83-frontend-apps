/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import React from 'react';
import styled from 'styled-components';
import { Draggable } from 'react-beautiful-dnd';
import { getInitials } from '../../commonUtils'

const Container = styled.div`

`;

export default class Ticket extends React.Component {
	render() {
		let classNameForSpan, classNameForI;
		switch (this.props.ticket.priority) {
			case "HIGH":
				classNameForSpan = "text-danger"
				classNameForI = "far fa-arrow-up text-danger"
			case "MEDIUM":
				classNameForSpan = "text-warning"
				classNameForI = "far fa-arrows-v text-orange"
			case "LOW":
				classNameForSpan = "text-success"
				classNameForI = "far fa-arrow-down text-success"
		}
		return (
			<Draggable draggableId={this.props.ticket.id} index={this.props.index}>
				{(provided, snapshot) => (
					<Container
						{...provided.draggableProps}
						{...provided.dragHandleProps}
						innerRef={provided.innerRef}
						isDragging={snapshot.isDragging}
						aria-roledescription="Press space bar to lift the ticket"
					>
						<div className="ticketContainer">
							<p>
								<span className={classNameForSpan}>
									<i className={classNameForI}></i>
								</span>
								Created On: {new Date(this.props.ticket.createdAt).toLocaleDateString()}
								<button type="button" onClick={(e) => this.props.ticketEditHandler(e, this.props.ticket.id)}>
									<i className="fas fa-pencil"></i>
								</button>
							</p>
							<span onClick={(e) => this.props.ticketEditHandler(e, this.props.ticket.id, "showDetailsModal")}>
								<h4>{this.props.ticket.ticketNo} | {this.props.ticket.title}</h4>
								<h5>{this.props.ticket.description}</h5>
								<div className="initialsBox">
									<span className="initialsImage">{getInitials(this.props.ticket.assignedTo)}</span>
									<p className="text-capitalize">{this.props.ticket.assignedTo}</p>
								</div>
								<span className="ticketSlot"></span>
							</span>
						</div>
					</Container>
				)}
			</Draggable>
		);
	}
}