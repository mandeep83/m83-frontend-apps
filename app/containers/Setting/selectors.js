/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
 * Direct selector to the setting state domain
 */

const selectSettingDomain = state => state.get("setting", initialState);

export const getSubmitFormSuccess = () => createSelector(selectSettingDomain, subState => subState.submitFormSuccess);
export const getSubmitFormFailure = () => createSelector(selectSettingDomain, subState => subState.submitFormFailure);

export const getAccountSettingSuccess = () => createSelector(selectSettingDomain, subState => subState.getAccountSettingSuccess);
export const getAccountSettingFailure = () => createSelector(selectSettingDomain, subState => subState.getAccountSettingFailure);

export const getImageUploadSuccess = () => createSelector(selectSettingDomain, subState => subState.imageUploadSuccess);
export const getImageUploadFailure = () => createSelector(selectSettingDomain, subState => subState.imageUploadFailure);

export const saveDefaultThemeSuccess = () => createSelector(selectSettingDomain, substate => substate.saveDefaultThemeSuccess);
export const saveDefaultThemeError = () => createSelector(selectSettingDomain, substate => substate.saveDefaultThemeError);

export const getThemeSuccess = () => createSelector(selectSettingDomain, substate => substate.getThemeSuccess);
export const getThemeFailure = () => createSelector(selectSettingDomain, substate => substate.getThemeFailure);