/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/*
 *
 * Setting constants
 *
 */


export const DEFAULT_ACTION = "app/Setting/DEFAULT_ACTION";

export const SUBMIT_FORM = "app/Setting/SUBMIT_FORM";
export const SUBMIT_FORM_FAILURE = "app/Setting/SUBMIT_FORM_FAILURE";
export const SUBMIT_FORM_SUCCESS = "app/Setting/SUBMIT_FORM_SUCCESS";

export const IMAGE_UPLOAD_REQUEST = "app/Setting/IMAGE_UPLOAD_REQUEST";
export const IMAGE_UPLOAD_SUCCESS = "app/Setting/IMAGE_UPLOAD_SUCCESS";
export const IMAGE_UPLOAD_FAILURE = "app/Setting/IMAGE_UPLOAD_FAILURE";

export const GET_ACCOUNT_SETTINGS_REQUEST = "app/Setting/GET_ACCOUNT_SETTINGS_REQUEST";
export const GET_ACCOUNT_SETTINGS_SUCCESS = "app/Setting/GET_ACCOUNT_SETTINGS_SUCCESS";
export const GET_ACCOUNT_SETTINGS_FAILURE = "app/Setting/GET_ACCOUNT_SETTINGS_FAILURE";

export const SAVE_DEFAULT_THEME = 'app/Setting/SAVE_DEFAULT_THEME';
export const SAVE_DEFAULT_THEME_SUCCESS = 'app/Setting/SAVE_DEFAULT_THEME_SUCCESS';
export const SAVE_DEFAULT_THEME_ERROR ="app/Setting/SAVE_DEFAULT_THEME_ERROR";

export const GET_THEMES = 'app/Setting/GET_THEMES';
export const GET_THEMES_SUCCESS = 'app/Setting/GET_THEMES_SUCCESS';
export const GET_THEMES_ERROR ="app/Setting/GET_THEMES_ERROR";
