/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * Setting
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { FormattedMessage } from "react-intl";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import reducer from "./reducer";
import saga from "./saga";
import messages from "./messages";
import Loader from '../../components/Loader/Loadable';
import * as ACTIONS from './actions'
import * as SELECTORS from './selectors'
import NotificationModal from '../../components/NotificationModal/Loadable'

let style = {
    "themeSideBarTextColor": "",
    "themeSideBar": "",
    "themeHeaderTextColor": "",
    "themeHeader": ""
}

let payload = {
    "brandingText": "",
    "message": "",
    "brandingImageUrl": "",
    "logoImage": "",
    "style": {
        "themeSideBarTextColor": "",
        "themeSideBar": "",
        "themeHeaderTextColor": "",
        "themeHeader": ""
    }
}
/* eslint-disable react/prefer-stateless-function */
export class Setting extends React.Component {
    state = {
        solidColors: ["bg-danger backgroundImage", "bgBlack backgroundImage", "bgWhite backgroundImage", "bg-success backgroundImage", "bg-warning backgroundImage", "bg-info backgroundImage", "bg-primary backgroundImage", "bg-secondary backgroundImage", "bg-dark backgroundImage", "bg-solid-warning backgroundImage", "bg-solid-brown backgroundImage", "bg-solid-green backgroundImage", "bg-solid-blue backgroundImage"],
        gradientColors: ["bg-midnight-bloom", "bg-vicious-stance", "bg-night-sky", "bg-slick-carbon", "bg-asteroid", "bg-plum-plate", "bg-happy-fisher", "bg-desert-hump", "bg-jungle-day", "bg-aqua-splash", "bg-arielle-smile"],
        textColors: { bgLight: "text-light", bgDanger: "text-danger", bgPrimary: "text-primary", bgWarning: "text-warning", bgDark: "text-dark", bgSuccess: "text-success", bgInfo: "text-info", bgMuted: "text-muted", bgWhite: "text-white" },
        isTenantLogoUploading: false,
        isTenantBrandingImageUploading: false,
        payload: payload,
        isFetching: 2,
        isSettingChanged: false,
        themes: [],
        showThemeModal: false
    }


    componentDidMount() {
        this.props.getAccountSettingsDetails();
        this.props.getThemes();
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.getThemeSuccess && nextProps.getThemeSuccess !== this.props.getThemeSuccess) {
            let themes = JSON.parse(JSON.stringify(nextProps.getThemeSuccess));
            themes.map(theme => {
                theme.isSelecting = false;
                return theme;
            })
            this.setState((prevState) => ({
                themes,
                isFetching: prevState.isFetching - 1
            }))

            if (nextProps.getThemeFailure && nextProps.getThemeFailure != this.props.getThemeFailure) {
                this.setState((prevState) => ({
                    modalType: "error",
                    isOpen: true,
                    message2: nextProps.getThemeFailure,
                    isFetching: prevState.isFetching - 1
                }));
            }

        }

        if (nextProps.submitSuccess && nextProps.submitSuccess !== this.props.submitSuccess) {
            let payload = JSON.parse(JSON.stringify(this.state.payload));
            if (nextProps.submitSuccess.isReset) {
                payload.logoImage = "";
                payload.brandingImageUrl = "";
            }
            if (localStorage.tenantLogoURL !== payload.logoImage) {
                localStorage.tenantLogoURL = payload.logoImage;
                nextProps.fetchSideNav();
            }
            this.setState({
                modalType: "success",
                isOpen: !nextProps.submitSuccess.isReset,
                message2: nextProps.submitSuccess.message,
                isFetching: false,
                payload
            });
        }

        if (nextProps.submitFailure && nextProps.submitFailure != this.props.submitFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.submitFailure,
                isFetching: false
            });
        }

        if (nextProps.getAccountSettingSuccess && nextProps.getAccountSettingSuccess !== this.props.getAccountSettingSuccess) {
            let payload = JSON.parse(JSON.stringify(nextProps.getAccountSettingSuccess))
            // this.props.setTheme(payload.style)
            this.setState((prevState) => ({
                payload,
                isFetching: prevState.isFetching - 1
            }))
        }

        if (nextProps.getAccountSettingFailure && nextProps.getAccountSettingFailure != this.props.getAccountSettingFailure) {
            this.setState((prevState) => ({
                modalType: "error",
                isOpen: true,
                message2: nextProps.getAccountSettingFailure,
                isFetching: prevState.isFetching - 1
            }));
        }

        if (nextProps.saveDefaultThemeSuccess && nextProps.saveDefaultThemeSuccess !== this.props.saveDefaultThemeSuccess) {
            let { id, message } = nextProps.saveDefaultThemeSuccess
            let themes = JSON.parse(JSON.stringify(this.state.themes));
            if (document.getElementById("themeCSS")) {
                setTimeout(() => {
                    let appliedLinkTag = document.getElementById("themeCSS")
                    document.getElementsByTagName('head')[0].removeChild(appliedLinkTag);
                }, 2000);
            }
            var link = document.createElement('link');
            link.id = "themeCSS"
            link.rel = "stylesheet"
            link.href = window.API_URL + "api/unauth/user/theme.css" + `?userId=${localStorage.getItem("userId")}`;
            document.getElementsByTagName('head')[0].appendChild(link);

            themes.map(theme => {
                theme.isTenantDefaultTheme = false
                theme.isSelecting = false;
                if (theme.id == id) {
                    theme.isTenantDefaultTheme = true
                }
                return theme;
            })
            this.setState({
                isOpen: true,
                themes,
                message2: message,
                modalType: "success"
            })
        }

        if (nextProps.saveDefaultThemeError && nextProps.saveDefaultThemeError !== this.props.saveDefaultThemeError) {
            this.setState({
                isOpen: true,
                isFetching: false,
                message2: nextProps.saveDefaultThemeError.error,
                modalType: "error"
            })
        }

        if (nextProps.imageUploadSuccess && nextProps.imageUploadSuccess !== this.props.imageUploadSuccess) {
            let payload = JSON.parse(JSON.stringify(this.state.payload));
            let isTenantLogoUploading = this.state.isTenantLogoUploading;
            let isTenantBrandingImageUploading = this.state.isTenantBrandingImageUploading;
            if (nextProps.imageUploadSuccess.imageType === "tenantLogo") {
                payload.logoImage = nextProps.imageUploadSuccess.secure_url + `?lastmod=${new Date()}`;
                isTenantLogoUploading = false;
            } else {
                payload.brandingImageUrl = nextProps.imageUploadSuccess.secure_url + `?lastmod=${new Date()}`;
                isTenantBrandingImageUploading = false;
            }
            this.setState({
                payload,
                isTenantLogoUploading,
                isTenantBrandingImageUploading,
                isSettingChanged: true
            });
        }

        if (nextProps.imageUploadFailure && nextProps.imageUploadFailure !== this.props.imageUploadFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.imageUploadFailure
            });
        }
    }

    fileUploadHandler = event => {
        let isTenantLogoUploading = this.state.isTenantLogoUploading;
        let isTenantBrandingImageUploading = this.state.isTenantBrandingImageUploading;
        if (event.target.name === "tenantLogo") {
            isTenantLogoUploading = true;
        } else {
            isTenantBrandingImageUploading = true;
        }
        this.setState({ isTenantLogoUploading, isTenantBrandingImageUploading });
        this.props.uploadImageHandler(event.target.files[0], event.target.name);
    };

    inputChangeHandler = (event, themeColor) => {
        let payload = JSON.parse(JSON.stringify(this.state.payload));
        if (event.target.id.includes("theme"))
            payload.style[event.target.id] = themeColor
        else
            payload[event.target.id] = event.target.value;
        this.setState({
            payload,
            isSettingChanged: true
        }, () => {
            // this.props.setTheme(payload.style)
        });
    };

    componentWillUnmount() {
        // this.props.setTheme(style)
    }

    onSubmitHandler = (event) => {
        event.preventDefault();
        let payload = JSON.parse(JSON.stringify(this.state.payload));
        if (payload.brandingText.length > 255 || payload.message.length > 255) {
            let text = payload.brandingText.length > 255 ? "Message" : "Text"
            this.props.showNotification("error", `Cannot input more than 255 characters in Branding ${text}.`)
        }
        else {
            if ((payload.logoImage).includes("?")) {
                payload.logoImage = payload.logoImage.split("?")[0]
            }
            if ((payload.brandingImageUrl).includes("?")) {
                payload.brandingImageUrl = payload.brandingImageUrl.split("?")[0]
            }
            this.setState({
                isFetching: true
            }, () => {
                this.props.onSubmitHandler(payload, false);
            })
        }
    }

    onCloseHandler = () => {
        this.setState({
            isOpen: false,
            message2: "",
        });
    };

    resetToDefault = () => {
        this.setState({
            isFetching: true,
            payload
        }, () => {
            this.props.onSubmitHandler(payload, true);
        })
        // this.setState({ payload }, () => this.props.setTheme(payload.style))
    }

    handleChange = ({ currentTarget }) => {
        let payload = JSON.parse(JSON.stringify(this.state.payload));
        if (currentTarget.id === "message") {
            payload.message = currentTarget.textContent
        }
        else {
            payload.brandingText = currentTarget.textContent
        }
        this.setState({
            payload,
            isSettingChanged: true
        })
    }

    changeDefaultTheme = (id) => {
        let themes = JSON.parse(JSON.stringify(this.state.themes));
        themes.map(theme => {
            if (theme.id == id)
                theme.isSelecting = true;
            return theme;
        })
        this.setState({
            themes
        });
        this.props.saveDefaultTheme(localStorage.tenant, id)
    }

    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>Settings</title>
                    <meta name="description" content="M83-Setting" />
                </Helmet>

                <header className="content-header d-flex">
                    <div className="flex-60">
                        <div className="d-flex">
                            <h6>Settings</h6>
                        </div>
                    </div>
                    <div className="flex-40 text-right">
                        <div className="content-header-group"></div>
                    </div>
                </header>

                {this.state.isFetching ?
                    <Loader />
                    :
                    <div className="content-body">
                        <form className="brand-wrapper" onSubmit={this.onSubmitHandler}>
                            <div className="login-outer-box" id="flexLogin">
                                <div className="d-flex h-100">
                                    <div className="flex-65 h-100">
                                        <div className="login-outer-image">
                                            {this.state.isTenantBrandingImageUploading ?
                                                <i className="fad fa-spinner fa-spin"></i> :
                                                this.state.payload.brandingImageUrl ?
                                                    <img src={this.state.payload.brandingImageUrl} /> :
                                                    <img src="https://content.iot83.com/m83/misc/flexLoginImage_new.png" />
                                            }
                                        </div>
                                    </div>

                                    <div className="flex-35 bg-white position-relative">
                                        <div className="login-logo">
                                            {this.state.isTenantLogoUploading ?
                                                <i className="fad fa-spinner text-theme fa-spin f-18"></i> :
                                                this.state.payload.logoImage ?
                                                    <React.Fragment>
                                                        <img src={this.state.payload.logoImage} />
                                                        <label className="btn-transparent btn-transparent-blue btn-upload" data-tooltip="true" data-tooltip-text="Edit Logo" data-tooltip-place="bottom">
                                                            <i className="far fa-pencil cursor-pointer"></i>
                                                            <input type="file" name="tenantLogo" onChange={this.fileUploadHandler} />
                                                        </label>
                                                    </React.Fragment> :
                                                    <React.Fragment>
                                                        <img src="https://content.iot83.com/m83/misc/flexLogo.png" />
                                                        <label className="btn-transparent btn-transparent-blue btn-upload" data-tooltip="true" data-tooltip-text="Edit Logo" data-tooltip-place="bottom">
                                                            <i className="far fa-pencil cursor-pointer"></i>
                                                            <input type="file" name="tenantLogo" onChange={this.fileUploadHandler} />
                                                        </label>
                                                    </React.Fragment>
                                            }
                                        </div>
                                        <div className="login-form-box">
                                            <div className="login-form-text">
                                                <h1 id="message" contentEditable="true" suppressContentEditableWarning={true} onBlur={this.handleChange}>{this.state.payload.message ? this.state.payload.message : "Welcome to Flex83"}</h1>
                                                <h6 id="brandingText" contentEditable="true" suppressContentEditableWarning={true} onBlur={this.handleChange}>{this.state.payload.brandingText ? this.state.payload.brandingText : "Connect. Configure. Visualize. Operate. Extend"}</h6>
                                            </div>
                                            <div className="form-group">
                                                <input type="email" id="email" placeholder="Username" className="form-control" readOnly />
                                                <span className="form-control-icon"><i className="fad fa-envelope"></i></span>
                                            </div>
                                            <div className="form-group">
                                                <input type="password" id="password" placeholder="Password" className="form-control" readOnly />
                                                <span className="form-control-icon"><i className="fad fa-lock"></i></span>
                                            </div>
                                            <div className="form-group">
                                                <button name="button" className="btn btn-primary" disabled>Login</button>
                                                <a>Forgot Password ?</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="button-group text-right">
                                <div className="brand-theme-list">
                                    <label>Set Default Theme:</label>
                                    <ul className="list-style-none d-flex">
                                        {this.state.themes.map((val, index) =>
                                            <li key={index} onClick={() => this.changeDefaultTheme(val.id)}>
                                                <div className={`brand-theme-list-box ${val.isTenantDefaultTheme ? "active" : ""}`} style={{ backgroundColor: val.darkThemeColor }}>
                                                    <div className={`brand-theme-list-border ${val.isSelecting ? "blink-animation" : ""}`} style={{ backgroundColor: val.themeColor }}></div>
                                                </div>
                                            </li>
                                        )}
                                    </ul>
                                </div>
                                <label className="btn btn-warning btn-upload mr-r-10">
                                    <i className="far fa-edit mr-r-10"></i>Edit Image
                                    <input type="file" name="brandingLogo" onChange={this.fileUploadHandler} />
                                </label>
                                <button type="submit" className="btn btn-success mr-r-10" disabled={!this.state.isSettingChanged}><i className="far fa-check mr-r-10"></i>Apply</button>
                                <button type="button" className="btn btn-light" onClick={this.resetToDefault}><i className="far fa-redo mr-r-10"></i>Reset</button>
                            </div>
                        </form>
                    </div>
                }

                {this.state.isOpen &&
                    <NotificationModal
                        type={this.state.modalType}
                        message2={this.state.message2}
                        onCloseHandler={this.onCloseHandler}
                    />
                }
            </React.Fragment>
        );
    }
}

Setting.propTypes = {
    dispatch: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
    imageUploadSuccess: SELECTORS.getImageUploadSuccess(),
    imageUploadFailure: SELECTORS.getImageUploadFailure(),
    submitSuccess: SELECTORS.getSubmitFormSuccess(),
    submitFailure: SELECTORS.getSubmitFormFailure(),
    getAccountSettingFailure: SELECTORS.getAccountSettingFailure(),
    getAccountSettingSuccess: SELECTORS.getAccountSettingSuccess(),
    saveDefaultThemeSuccess: SELECTORS.saveDefaultThemeSuccess(),
    saveDefaultThemeError: SELECTORS.saveDefaultThemeError(),
    getThemeSuccess: SELECTORS.getThemeSuccess(),
    getThemeFailure: SELECTORS.getThemeFailure(),
});

function mapDispatchToProps(dispatch) {
    return {
        dispatch,
        uploadImageHandler: (payload, imageType) => dispatch(ACTIONS.uploadImageHandler(payload, imageType)),
        onSubmitHandler: (payload, isReset) => dispatch(ACTIONS.onSubmitHandler(payload, isReset)),
        getAccountSettingsDetails: (payload) => dispatch(ACTIONS.getAccountSettingsDetails(payload)),
        saveDefaultTheme: (tenantName, id) => dispatch(ACTIONS.saveDefaultTheme(tenantName, id)),
        getThemes: () => dispatch(ACTIONS.getThemes())

    };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "setting", reducer });
const withSaga = injectSaga({ key: "setting", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(Setting);
