/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/*
 *
 * Setting reducer
 *
 */

import { fromJS } from "immutable";
import * as CONSTANTS from "./constants";

export const initialState = fromJS({});

function settingReducer(state = initialState, action) {
  switch (action.type) {
    case CONSTANTS.DEFAULT_ACTION:
      return state;
    case CONSTANTS.SUBMIT_FORM_SUCCESS:
      return Object.assign({}, state, {
        submitFormSuccess: {
          message: action.response.message,
          isReset: action.response.isReset,
          timestamp: new Date()
        }
      });
    case CONSTANTS.SUBMIT_FORM_FAILURE:
      return Object.assign({}, state, {
        submitFormFailure: action.error,
      });
    case CONSTANTS.IMAGE_UPLOAD_SUCCESS:
      return Object.assign({}, state, {
        imageUploadSuccess: action.response
      });
    case CONSTANTS.IMAGE_UPLOAD_FAILURE:
      return Object.assign({}, state, {
        imageUploadFailure: action.error
      });
    case CONSTANTS.GET_ACCOUNT_SETTINGS_SUCCESS:
      return Object.assign({}, state, {
        getAccountSettingSuccess: action.response
      });
    case CONSTANTS.GET_ACCOUNT_SETTINGS_FAILURE:
      return Object.assign({}, state, {
        getAccountSettingFailure: action.error
      });
    case CONSTANTS.SAVE_DEFAULT_THEME_SUCCESS:
      return Object.assign({}, state, {
        saveDefaultThemeSuccess: action.response
      })
    case CONSTANTS.SAVE_DEFAULT_THEME_ERROR:
      return Object.assign({}, state, {
        saveDefaultThemeError: action.error
      })

    case CONSTANTS.GET_THEMES_SUCCESS:
      return Object.assign({}, state, {
        getThemeSuccess: action.response
      })
    case CONSTANTS.GET_THEMES_ERROR:
      return Object.assign({}, state, {
        getThemeFailure: action.error
      })
    default:
      return state;
  }
}

export default settingReducer;

