
/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/*
 *
 * Setting actions
 *
 */

import * as CONSTANTS from "./constants";

export function defaultAction() {
  return {
    type: CONSTANTS.DEFAULT_ACTION
  };
}

export function uploadImageHandler(payload, imageType) {
  return {
    type: CONSTANTS.IMAGE_UPLOAD_REQUEST,
    payload,
    imageType,
  };
}


export function onSubmitHandler(payload, isReset) {
  return {
    type: CONSTANTS.SUBMIT_FORM,
    payload,
    isReset
  };
}


export function getAccountSettingsDetails() {
  return {
    type: CONSTANTS.GET_ACCOUNT_SETTINGS_REQUEST,
  };
}

export function saveDefaultTheme(tenantName, id) {
  return {
    type: CONSTANTS.SAVE_DEFAULT_THEME,
    tenantName,
    id
  }
}

export function getThemes() {
  return {
    type: CONSTANTS.GET_THEMES
  }
}
