/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { takeEvery } from 'redux-saga';
import * as CONSTANTS from './constants';
import { apiCallHandler } from '../../api';

export function* addOrEditAccountApiHandler(action) {
  yield [apiCallHandler(action, CONSTANTS.SUBMIT_FORM_SUCCESS, CONSTANTS.SUBMIT_FORM_FAILURE, 'saveAccountSettings')];
}

export function* getAccountSettingsApiHandler(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_ACCOUNT_SETTINGS_SUCCESS, CONSTANTS.GET_ACCOUNT_SETTINGS_FAILURE, 'getAccountSettings')];
}

export function* imageUploadApiHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.IMAGE_UPLOAD_SUCCESS, CONSTANTS.IMAGE_UPLOAD_FAILURE, 'fileUploadRequest', false)];
}

export function* watcherGetAccountSettings() {
  yield takeEvery(CONSTANTS.GET_ACCOUNT_SETTINGS_REQUEST, getAccountSettingsApiHandler);
}

export function* saveDefaultTheme(action) {
  yield [apiCallHandler(action, CONSTANTS.SAVE_DEFAULT_THEME_SUCCESS, CONSTANTS.SAVE_DEFAULT_THEME_ERROR, 'saveDefaultTheme')]
}
export function* getThemes(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_THEMES_SUCCESS, CONSTANTS.GET_THEMES_ERROR, 'getCssThemes')]
}

export function* watcherGetThemes() {
  yield takeEvery(CONSTANTS.GET_THEMES, getThemes);
}


export function* watcherAddOrEditAccountRequest() {
  yield takeEvery(CONSTANTS.SUBMIT_FORM, addOrEditAccountApiHandler);
}

export function* watcherSaveDefaultTheme() {
  yield takeEvery(CONSTANTS.SAVE_DEFAULT_THEME, saveDefaultTheme)
}

export function* watcherImageUploadRequest() {
  yield takeEvery(CONSTANTS.IMAGE_UPLOAD_REQUEST, imageUploadApiHandlerAsync);
}

export default function* rootSaga() {
  yield [
    watcherAddOrEditAccountRequest(),
    watcherImageUploadRequest(),
    watcherGetAccountSettings(),
    watcherSaveDefaultTheme(),
    watcherGetThemes(),
  ]
}

