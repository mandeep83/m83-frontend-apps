/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { fromJS } from "immutable";
import * as CONSTANTS from "./constants";


export const initialState = fromJS({});

function addOrEditMlReducer(state = initialState, action) {
  switch (action.type) {
    case CONSTANTS.RESET_TO_INITIAL_STATE:
      return initialState;
    case CONSTANTS.GET_CONNECTORS_BY_CATEGORY_SUCCESS:
      return Object.assign({}, state, {
        getConnectorsSuccess: action.response
      });
    case CONSTANTS.GET_CONNECTORS_BY_CATEGORY_FAILURE:
      return Object.assign({}, state, {
        getConnectorsFailure: action.error
      });
    case CONSTANTS.SAVE_EXPERIMENT_SUCCESS:
      return Object.assign({}, state, {
        saveExperimentSuccess: action.response
      });
    case CONSTANTS.SAVE_EXPERIMENT_FAILURE:
      return Object.assign({}, state, {
        saveExperimentFailure: action.error
      });
    case CONSTANTS.GET_EXPERIMENT_BY_ID_SUCCESS:
      return Object.assign({}, state, {
        getExperimentByIdSuccess: action.response
      });
    case CONSTANTS.GET_EXPERIMENT_BY_ID_FAILURE:
      return Object.assign({}, state, {
        getExperimentByIdFailure: action.error
      });
    case CONSTANTS.GET_COLUMNS_SUCCESS:
      return Object.assign({}, state, {
        getColumnsSuccess: action.response
      });
    case CONSTANTS.GET_COLUMNS_FAILURE:
      return Object.assign({}, state, {
        getColumnsFailure: action.error
      });
    case CONSTANTS.GET_DATA_CONNECTORS_LIST_SUCCESS:
      return Object.assign({}, state, {
        getDataConnectorsListSuccess: action.response
      });
    case CONSTANTS.GET_DATA_CONNECTORS_LIST_FAILURE:
      return Object.assign({}, state, {
        getDataConnectorsListFailure: action.error
      });
    case CONSTANTS.GET_STATISTICS_SUCCESS:
      return Object.assign({}, state, {
        getStatisticsSuccess: {
          message: action.response
        },
      });
    case CONSTANTS.GET_STATISTICS_FAILURE:
      return Object.assign({}, state, {
        getStatisticsFailure: {
          error: action.error,
          loader: action.addOns.loader
        },
      });
    default:
      return state;
  }
}

export default addOrEditMlReducer;
