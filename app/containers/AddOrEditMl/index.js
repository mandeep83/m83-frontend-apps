/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { FormattedMessage } from "react-intl";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import makeSelectAddOrEditMl from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import messages from "./messages";
import * as Actions from './actions';
import * as Selectors from './selectors';
import NotificationModal from '../../components/NotificationModal/Loadable';
import Loader from "../../components/Loader";
import ReactSelect from 'react-select';
import jwt_decode from "jwt-decode";
import MQTT from 'mqtt';
import Slider from 'react-input-slider';
import cloneDeep from 'lodash/cloneDeep';
import Chart from "../../components/LineChart";
import ReactTooltip from "react-tooltip";

let client;

/* eslint-disable react/prefer-stateless-function */
export class AddOrEditMl extends React.Component {
    state = {
        payload: {
            name: '',
            description: '',
            dataSource: {},
            cleaningAndTransformConfig: {
                operations: [
                    {
                        colName: "",
                        actions: [
                        ]
                    }]
            },
            featureSelectionConfig: {
                techniques: [

                ]
            },
            algorithm: {
                trainingJobName: ''
            },
            customModel: false
        },
        connectorsList: [],
        columnsList: [],
        tools: [
            {
                displayName: 'Trimming',
                name: 'trimming',
                url: 'https://content.iot83.com/m83/mlStudio/trimming.png',
                type: ["String"]
            }, {
                displayName: 'Padding',
                name: 'padding',
                url: 'https://content.iot83.com/m83/mlStudio/padding.png',
                type: ["String", "Numeric"]
            }, {
                displayName: 'Fill Null Value',
                name: 'fillNullValues',
                url: 'https://content.iot83.com/m83/mlStudio/nullValue.png',
                type: ["String", "Numeric"]
            }, {
                displayName: 'Unique Column',
                name: 'uniqueColumn',
                url: 'https://content.iot83.com/m83/mlStudio/unique.png',
                type: ["String", "Numeric"]
            }, {
                displayName: 'Handling All N/A',
                name: 'treatAllMissingValues',
                url: 'https://content.iot83.com/m83/mlStudio/nullValue.png',
                type: ["String", "Numeric"]
            }, {
                displayName: 'Drop Column',
                name: 'dropColumn',
                url: 'https://content.iot83.com/m83/mlStudio/dropDuplicate.png',
                type: ["String", "Numeric"]
            }, {
                displayName: 'Drop Duplicate',
                name: 'dropDuplicates',
                url: 'https://content.iot83.com/m83/mlStudio/dropDuplicate.png',
                type: ["String", "Numeric"]
            }, {
                displayName: 'Regrex Replace',
                name: 'regrexReplace',
                url: 'https://content.iot83.com/m83/mlStudio/replace.png',
                type: ["String", "Numeric"]
            }, {
                displayName: 'Quassi Constant',
                name: 'quasiConstant',
                url: 'https://content.iot83.com/m83/mlStudio/constantcolumn.png',
                type: ["String", "Numeric"]
            }, {
                displayName: 'Encoding',
                name: 'encoding',
                url: 'https://content.iot83.com/m83/mlStudio/encode.png',
                type: ["String", "Numeric"]
            }, {
                displayName: 'Transformation',
                name: 'transformation',
                url: 'https://content.iot83.com/m83/mlStudio/encode.png',
                type: ["String", "Numeric"]
            }, {
                displayName: 'Select Multiple Columns',
                name: 'selectColumn',
                url: 'https://content.iot83.com/m83/mlStudio/encode.png',
                type: ["String", "Numeric"]
            },],
        isFetching: true,
        algorithmsList: [
            {
                name: 'Regression',
                children: [
                    { name: 'XgBoost', value: "XgBoost", url: 'https://content.iot83.com/m83/mlStudio/regression.png' },
                    { name: 'Linear-Learner', value: "linear-learner", url: 'https://content.iot83.com/m83/mlStudio/regression.png' },
                ],
                toolTipText: "Technique for prediction of continous variables"
            }, {
                name: ' Classification',
                children: [
                    { name: 'XgBoost', value: "XgBoost", url: 'https://content.iot83.com/m83/mlStudio/regression.png' },
                    { name: 'Linear-Learner', value: "linear-learner", url: 'https://content.iot83.com/m83/mlStudio/regression.png' }
                ],
                toolTipText: "Technique for prediction of discrette variables"
            }, {
                name: 'Anomaly',
                children: [
                    { name: 'RandomCutForest', value: "RandomCutForest", url: 'https://content.iot83.com/m83/mlStudio/regression.png' }
                ],
                toolTipText: "Technique for predicting unusual behaviour in data points"
            }, {
                name: 'Clustering',
                children: [
                    { name: 'K-Means', value: "K-Means", url: 'https://content.iot83.com/m83/mlStudio/regression.png' }
                ],
                toolTipText: "Quantization technique to identify data points in the same subgroup (clusters)"
            }],
        optionsForEncoder: [
            { value: 'countVectorize', type: ['Numeric'], label: 'Count Vectorize' },
            { value: 'oneHotEncoder', type: ['Numeric'], label: 'One Hot Encoder' },
            { value: 'binaries', type: ['Numeric'], label: 'Binaries' },
            { value: 'stopWordsRemover', type: ['String'], label: 'Stop Words Remover' },
            { value: 'vectorAssemblers', type: ['Numeric'], label: 'Vector Assemblers' },
            { value: 'stringIndexer', type: ['String'], label: 'String Indexer' }],
        featureList: [{
            displayName: "Constant Removal",
            value: "constantRemoval",
            imageName: "vectorSlicing"
        }, {
            displayName: "Correlation Removal",
            value: "correlationRemoval",
            imageName: "rFormula"
        }, {
            displayName: "Information Gain",
            value: "informationGain",
            imageName: "vectorSlicing"
        }, {
            displayName: "Chi-Sq Test",
            value: "chiSqTest",
            imageName: "rFormula"
        }],
        dataConnectorsList: [],
        dataCorrelationData: {},
        dataCorrelationDataWithCleaningAndTransform: {},
        statisticalReportData: {},
        statisticalReportDataWithCleaningAndTransform: {},
        showMissingValuesData: {},
        showMissingValuesDataWithCleaningAndTransform: {},
        showSampleData: {},
        showSampleDataWithCleaningAndTransform: {},
        featureImportanceData: {},
        informationGainData: {}
    }

    componentDidMount() {
        this.props.match.params.id && this.props.getMLExperimentById(this.props.match.params.id)
        this.props.getDataConnectorsList();
        this.createMqttConnection();
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.getConnectorsSuccess && nextProps.getConnectorsSuccess !== this.props.getConnectorsSuccess) {
            this.setState({
                connectorsList: nextProps.getConnectorsSuccess,
                isFetching: false
            })
        }

        if (nextProps.getConnectorsFailure && nextProps.getConnectorsFailure !== this.props.getConnectorsFailure) {
            this.setState({
                isOpen: true,
                modalType: "error",
                message2: nextProps.getConnectorsFailure,
            }, () => this.props.resetToInitialState())
        }

        if (nextProps.getDataConnectorsListSuccess && nextProps.getDataConnectorsListSuccess !== this.props.getDataConnectorsListSuccess) {
            let dataConnectorsList = nextProps.getDataConnectorsListSuccess.find(category => category.type === "cloud storage").connectors.filter(type => type.connector === "S3")
            let isFetching = this.props.match.params.id ? this.state.isFetching : false
            this.setState({
                dataConnectorsList,
                isFetching
            })
        }

        if (nextProps.getDataConnectorsListFailure && nextProps.getDataConnectorsListFailure !== this.props.getDataConnectorsListFailure) {
            this.setState({
                isOpen: true,
                modalType: "error",
                message2: nextProps.getDataConnectorsListFailure,
            }, () => this.props.resetToInitialState())
        }

        if (nextProps.saveExperimentSuccess && nextProps.saveExperimentSuccess !== this.props.saveExperimentSuccess) {
            this.setState({
                isOpen: true,
                modalType: "success",
                message2: nextProps.saveExperimentSuccess,
                saveSuccess: true
            })
        }

        if (nextProps.saveExperimentFailure && nextProps.saveExperimentFailure !== this.props.saveExperimentFailure) {
            this.setState({
                isOpen: true,
                modalType: "error",
                message2: nextProps.saveExperimentFailure,
                isFetching: false
            }, () => this.props.resetToInitialState())
        }

        if (nextProps.getExperimentByIdSuccess && nextProps.getExperimentByIdSuccess !== this.props.getExperimentByIdSuccess) {
            this.props.getColumns(nextProps.getExperimentByIdSuccess.dataSource)
            this.setState({
                payload: nextProps.getExperimentByIdSuccess
            }, () => this.props.getConnectorsByCategory(nextProps.getExperimentByIdSuccess.dataSource.connectorCategory))
        }

        if (nextProps.getExperimentByIdFailure && nextProps.getExperimentByIdFailure !== this.props.getExperimentByIdFailure) {
            this.setState({
                isOpen: true,
                modalType: "error",
                message2: nextProps.getExperimentByIdFailure,
            }, () => this.props.resetToInitialState())
        }
        if (nextProps.getColumnsSuccess && nextProps.getColumnsSuccess !== this.props.getColumnsSuccess) {
            this.setState({
                columnsList: nextProps.getColumnsSuccess
            })
        }

        if (nextProps.getColumnsFailure && nextProps.getColumnsFailure !== this.props.getColumnsFailure) {
            this.setState({
                isOpen: true,
                modalType: "error",
                message2: nextProps.getColumnsFailure,
            }, () => this.props.resetToInitialState())
        }

        if (nextProps.getStatisticsSuccess && nextProps.getStatisticsSuccess !== this.props.getStatisticsSuccess) {
            this.setState({
                isOpen: true,
                modalType: "success",
                message2: "Preparing Data",
            }, () => this.props.resetToInitialState())
        }

        if (nextProps.getStatisticsFailure && nextProps.getStatisticsFailure !== this.props.getStatisticsFailure) {
            this.setState({
                isOpen: true,
                modalType: "error",
                message2: nextProps.getStatisticsFailure.error,
                [nextProps.getStatisticsFailure.loader]: false
            }, () => this.props.resetToInitialState())
        }
    }

    onCloseHandler = () => {
        this.setState({
            isOpen: false,
            modalType: '',
            message2: ''
        }, () => {
            if (this.state.saveSuccess) {
                this.props.history.push('/mlStudio')
            }
        })
    }

    formChangeHandler = (event) => {
        let payload = cloneDeep(this.state.payload)
        payload[event.target.id] = event.target.value
        this.setState({
            payload
        })
    }

    getConnectorsByCategory = (category) => {
        let payload = cloneDeep(this.state.payload);
        this.props.match.params.id ? '' : payload.dataSource = {} // changes needed 
        payload.dataSource.connectorCategory = category
        this.emptyModalPayload();
        this.emptyModalPayloadWithCleaningAndTransform();
        this.setState({
            connectorsList: [],
            payload
        }, () => this.props.getConnectorsByCategory(category));

    }

    dataSourceChangeHandler = (type, id) => {
        let payload = cloneDeep(this.state.payload);
        if (type === "connector") {
            payload.dataSource.connectorId = id
            payload.dataSource.dirId = ''
            payload.dataSource.bucketId = ''

        }
        else if (type === "bucket") {
            payload.dataSource.bucketId = id
            payload.dataSource.dirId = ''
        }
        else if (type === "dir") {
            payload.dataSource.dirId = id
            this.props.getColumns(payload.dataSource)
        }
        this.emptyModalPayload();
        this.emptyModalPayloadWithCleaningAndTransform();
        this.setState({
            payload
        })
    }

    addRemoveColumns = (type, operationIndex) => {
        let payload = cloneDeep(this.state.payload);
        if (type === "add") {
            payload.cleaningAndTransformConfig.operations.push({
                colName: "",
                actions: []
            })
        }
        else {
            payload.cleaningAndTransformConfig.operations.splice(operationIndex, 1)
            if (this.state.operationIndex === operationIndex)
                this.setState({
                    operationIndex: '',
                    actionIndex: '',
                    selectedAction: ''
                })
        }
        this.emptyModalPayloadWithCleaningAndTransform();
        this.setState({
            payload
        })
    }

    columnChangeHandler = (event, operationIndex) => {
        let payload = cloneDeep(this.state.payload),
            selectedColumn;
        payload.cleaningAndTransformConfig.operations[operationIndex][event.target.id] = event.target.value
        if (event.target.value === "ForAll") {
            payload.cleaningAndTransformConfig.operations[operationIndex].colType = "String Numeric"
        } else {
            selectedColumn = this.state.columnsList.find(col => col.keys === event.target.value)
            payload.cleaningAndTransformConfig.operations[operationIndex].colType = selectedColumn ? selectedColumn.type : ''
        }
        payload.cleaningAndTransformConfig.operations[operationIndex].actions = []
        this.emptyModalPayloadWithCleaningAndTransform();
        this.setState({
            payload,
            selectedAction: '',
            actionIndex: '',
            operationIndex: ''
        })
    }

    onToolDrag = (selectedToolName) => {
        this.setState({
            selectedToolName
        })
    }

    allowDrop = (event) => {
        event.stopPropagation();
        event.preventDefault();
    }

    onToolDrop = (event, operationIndex) => {
        let payload = cloneDeep(this.state.payload),
            column = payload.cleaningAndTransformConfig.operations[operationIndex];
        if (column.colName) {
            if (this.state.tools.find(tool => this.state.selectedToolName !== "selectColumn" && this.state.selectedToolName === tool.name && tool.type.includes(column.colType))) {
                if (column.actions.find(action => action.actionName === this.state.selectedToolName)) {
                    this.setState({
                        isOpen: true,
                        modalType: "error",
                        message2: "Error Action is being repeated"
                    })
                }
                else {
                    column.actions.push({
                        actionName: this.state.selectedToolName,
                        isCompleted: this.state.selectedToolName === 'treatAllMissingValues' || this.state.selectedToolName === 'dropColumn' || this.state.selectedToolName === 'dropDuplicates' ? true : false
                    })
                    this.emptyModalPayloadWithCleaningAndTransform();
                }
            }
            else if (column.colName === "ForAll" && this.state.selectedToolName === "selectColumn") {
                if (!column.actions.find(action => action.actionName === this.state.selectedToolName)) {
                    column.actions.push({
                        actionName: this.state.selectedToolName,
                        isCompleted: false
                    })
                    this.emptyModalPayloadWithCleaningAndTransform();
                }
                else {
                    this.setState({
                        isOpen: true,
                        modalType: "error",
                        message2: "Error ! Error Action is being repeated "
                    })
                }
            }
            else {
                this.setState({
                    isOpen: true,
                    modalType: "error",
                    message2: `Error! This column doesn't Support the selected tool`
                })
            }
        }
        else {
            this.setState({
                isOpen: true,
                modalType: "error",
                message2: "Error Please Select Column First"
            })
        }
        this.setState({
            payload,
            selectedToolName: ''
        })
    }

    deleteAction = (e, deleteOperationIndex, deleteActionIndex) => {
        let payload = cloneDeep(this.state.payload);
        (payload.cleaningAndTransformConfig.operations[deleteOperationIndex].actions).splice(deleteActionIndex, 1)
        this.emptyModalPayloadWithCleaningAndTransform();
        this.setState({
            payload,
            operationIndex: '',
            actionIndex: '',
            selectedAction: ''
        })
    }

    onActionSelect = (e, operationIndex, actionIndex, selectedAction) => {
        let formPayload = {},
            payload = cloneDeep(this.state.payload),
            selectedPayload = payload.cleaningAndTransformConfig.operations[operationIndex].actions[actionIndex],
            selectedEncoder, selectedInputCols;
        formPayload = Object.keys(selectedPayload).length > 1 ? selectedPayload : {}
        switch (selectedAction) {
            case "trimming":
                formPayload.type = selectedPayload.type ? selectedPayload.type : "all";
                break;
            case "uniqueColumn":
                formPayload.tolerance = selectedPayload.tolerance ? selectedPayload.tolerance : "0";
                break;
            case "encoding":
                selectedEncoder = selectedPayload.encoder ? this.state.optionsForEncoder.find(encoder => encoder.value === selectedPayload.encoder) : {};
                if (selectedPayload.inputCols || selectedPayload.inputCol) {
                    if (selectedPayload.encoder === "vectorAssemblers") {
                        selectedInputCols = selectedPayload.inputCols.map(col => {
                            return ({
                                value: col,
                                label: col
                            })
                        })
                    }
                    else {
                        selectedInputCols = {
                            value: selectedPayload.inputCol,
                            label: selectedPayload.inputCol
                        }
                    }
                }
                break;
            case "selectColumn":
                if (selectedPayload.columnList) {
                    selectedInputCols = selectedPayload.columnList.map(col => {
                        return ({
                            value: col,
                            label: col
                        })
                    })
                }
                break;
        }
        this.setState({
            operationIndex,
            actionIndex,
            selectedAction,
            formPayload,
            selectedEncoder,
            selectedInputCols
        })
    }

    transformationFormChangeHandler = (event) => {
        let formPayload = cloneDeep(this.state.formPayload);
        if (event.target.name === "type") {
            formPayload.type = event.target.value
        }
        else if (event.target.name === "dropColumn") {
            formPayload.dropColumn = event.target.value
        }
        else if (event.target.id === "encoder") {
            formPayload = {}
            formPayload[event.target.id] = event.target.value
        }
        else if (event.target.id === "length" || event.target.id === "padWith" || event.target.id === "vocabSize" || event.target.id === "minDf") {
            if (/[0-9]+/.test(event.target.value) || /^$/.test(event.target.value)) {
                formPayload[event.target.id] = event.target.value === '' ? '' : parseInt(event.target.value)
            }
        }
        else if (event.target.id === "transform") {
            if (event.target.value === "splitData") {
                formPayload.percent = 0
            }
            formPayload[event.target.id] = event.target.value
        }
        else {
            formPayload[event.target.id] = (event.target.value)
        }
        this.setState({
            formPayload
        })
    }

    actionSaveHandler = (event) => {
        event.preventDefault();
        let formPayload = cloneDeep(this.state.formPayload),
            payload = cloneDeep(this.state.payload)
        let keysArray = Object.keys(formPayload),
            changeIndex = payload.cleaningAndTransformConfig.operations[this.state.operationIndex].actions[this.state.actionIndex]
        for (let i = 0; i < keysArray.length; i++) {
            keysArray[i] !== "actionName" ? changeIndex[keysArray[i]] = formPayload[keysArray[i]] : ''
        }
        changeIndex.isCompleted = true
        this.emptyModalPayloadWithCleaningAndTransform();
        this.setState({
            formPayload: {},
            payload,
            operationIndex: '',
            actionIndex: '',
            selectedAction: ''
        })
    }

    SliderChangeHandler = (x, key) => {
        let formPayload = cloneDeep(this.state.formPayload);
        if (key === "percent") {
            formPayload.percent = x.toFixed(2);
        }
        else if (key === "threshold") {
            formPayload.threshold = x.toFixed(2);
        }
        else {
            formPayload.tolerance = x.toFixed(2);
        }
        this.setState({
            formPayload
        })
    }

    onFeatureDrag = (draggedFeature) => {
        this.setState({
            draggedFeature
        })
    }

    onFeatureDrop = () => {
        let payload = cloneDeep(this.state.payload);
        if (payload.featureSelectionConfig.techniques.find(action => action.actionName === this.state.draggedFeature)) {
            this.setState({
                isOpen: true,
                draggedFeature: '',
                message2: 'Error! Feature is being repeated!',
                modalType: "error",
            })
            return;
        }
        else {
            payload.featureSelectionConfig.techniques.push({
                actionName: this.state.draggedFeature,
                isCompleted: this.state.draggedFeature === "constantRemoval" ? true : false
            })
            this.setState({
                payload,
                draggedFeature: ''
            })
        }
    }

    deleteFeature = (e, featureIndex) => {
        let payload = cloneDeep(this.state.payload);
        payload.featureSelectionConfig.techniques.splice(featureIndex, 1)
        this.setState({
            payload,
            featureIndex: '',
            selectedFeature: ''
        })
    }

    onFeatureSelect = (e, featureIndex, selectedFeature) => {
        let payload = cloneDeep(this.state.payload),
            featurePayload = {},
            selectedFeaturePayload = payload.featureSelectionConfig.techniques[featureIndex];
        featurePayload = Object.keys(selectedFeaturePayload).length > 1 ? selectedFeaturePayload : {}
        switch (selectedFeature) {
            case "correlationRemoval":
                featurePayload.threshold = selectedFeaturePayload.threshold ? selectedFeaturePayload.threshold : 0.1;
                featurePayload.targetColumn = selectedFeaturePayload.targetColumn ? selectedFeaturePayload.targetColumn : '';
                break;
            case "chiSqTest":
                featurePayload.k = selectedFeaturePayload.k ? selectedFeaturePayload.k : 1;
                break;
            case "informationGain":
                featurePayload.targetColumn = selectedFeaturePayload.targetColumn ? selectedFeaturePayload.targetColumn : '';
                featurePayload.targetColumnType = selectedFeaturePayload.targetColumnType ? selectedFeaturePayload.targetColumnType : 'regressionType';
                break;

        }
        this.setState({
            payload,
            featureIndex,
            selectedFeature,
            featurePayload
        })
    }

    SliderChangeHandlerForFeatureSelection = (x, key) => {
        let featurePayload = cloneDeep(this.state.featurePayload);
        if (key === "threshold") {
            featurePayload.threshold = x.toFixed(2);
        } else {
            featurePayload[key] = x
        }
        this.setState({
            featurePayload
        })
    }

    featureFormChangeHandler = (event) => {
        let featurePayload = cloneDeep(this.state.featurePayload),
            informationGainData = cloneDeep(this.state.informationGainData);
        if (event.target.id === "targetColumn" || event.target.id === "targetColumnType") {
            informationGainData = {}
        }
        featurePayload[event.target.id] = event.target.value
        this.setState({
            featurePayload,
            informationGainData
        })
    }

    featureSaveHandler = (event) => {
        event.preventDefault();
        let featurePayload = cloneDeep(this.state.featurePayload),
            payload = cloneDeep(this.state.payload)
        let keysArray = Object.keys(featurePayload),
            changeIndex = payload.featureSelectionConfig.techniques[this.state.featureIndex]
        for (let i = 0; i < keysArray.length; i++) {
            keysArray[i] !== "actionName" ? changeIndex[keysArray[i]] = featurePayload[keysArray[i]] : ''
        }
        changeIndex.isCompleted = true
        this.setState({
            featurePayload: {},
            payload,
            featureIndex: '',
            selectedFeature: '',
        })
    }

    selectAlgorithm = (property, value) => {
        let payload = cloneDeep(this.state.payload);
        if (property === "name") {
            payload.algorithm.algorithmName = value
            payload.algorithm.algorithmType = '';
            payload.algorithm.hyperparams = {}
        }
        else {
            payload.algorithm.algorithmType = value
            switch (value) {
                case "XgBoost": {
                    payload.algorithm.hyperparams = {
                        objective: payload.algorithm.algorithmName === "Regression" ? 'reg:linear' : 'binary:logistics',
                        eta: 0.1,
                        subsample: 1,
                        num_round: 100,
                        trainSplitValue: 0.7,
                        testSplitValue: 0.3
                    }
                    break;
                }
                case "linear-learner": {
                    payload.algorithm.hyperparams = {
                        feature_dim: 1,
                        predictor_type: payload.algorithm.algorithmName === "Regression" ? 'regressor' : 'binary_classifier',
                        trainSplitValue: 0.7,
                        testSplitValue: 0.3
                    }
                    break;
                }
                case "RandomCutForest": {
                    payload.algorithm.hyperparams = {
                        feature_dim: 1,
                        num_samples_per_tree: 256,
                        num_trees: 50,
                        train_instance_count: 1
                    }
                    break;
                }
                case "K-Means": {
                    payload.algorithm.hyperparams = {
                        feature_dim: 1,
                        k: 5
                    }
                    break;
                }
            }
        }
        this.setState({
            payload
        })
    }

    algorithmFormHandler = (event) => {
        let payload = cloneDeep(this.state.payload)
        if (event.target.id === 'trainingJobName') {
            if (/^[A-Za-z0-9]+$/.test(event.target.value) || /^$/.test(event.target.value)) {
                payload.algorithm.trainingJobName = event.target.value
            }
        }
        else if (event.target.id === 'max_depth') {
            if ((/^[0-9]+$/.test(event.target.value) && (parseInt(event.target.value) >= 0 && parseInt(event.target.value) <= 1000)) || /^$/.test(event.target.value)) {
                payload.algorithm.hyperparams.max_depth = event.target.value
            }
        }
        else if (event.target.id === 'subsample') {
            if (((parseInt(event.target.value) >= 0 && parseInt(event.target.value) <= 1)) || /^$/.test(event.target.value)) {
                payload.algorithm.hyperparams.subsample = event.target.value
            }
        }
        else if (event.target.id === 'feature_dim') {
            if (/^[0-9]\d*$/.test(event.target.value) || /^$/.test(event.target.value)) {
                payload.algorithm.hyperparams.feature_dim = event.target.value
            }
        }
        else {
            payload.algorithm.hyperparams[event.target.id] = event.target.value
        }
        this.setState({
            payload
        })
    }

    saveExperiment = (e, isDraft) => {
        let payload = cloneDeep(this.state.payload)
        payload.isDraft = isDraft
        if (payload.name === "" || (!payload.dataSource.dirId)) {
            this.setState({
                isOpen: true,
                modalType: "error",
                message2: "Experiment Name or DataSource cannot be empty",
            })
            return;
        }
        else {
            this.props.saveExperiment(payload);
            this.setState({
                isFetching: true
            })
        }
    }

    handleChangeForReactSelect = (value) => {
        let formPayload = cloneDeep(this.state.formPayload),
            selectedEncoder;
        formPayload.encoder = value.value;
        selectedEncoder = value
        this.setState({
            formPayload,
            selectedEncoder
        })
    }

    createOptionsForMultiColumns = () => {
        let columnsList = cloneDeep(this.state.columnsList),
            options = [];
        for (let i = 0; i < columnsList.length; i++) {
            options.push({
                value: columnsList[i].keys,
                label: columnsList[i].keys
            })
        }
        return options;
    }

    createOptionsForReactSelect = () => {
        let columnsList = cloneDeep(this.state.columnsList),
            options = [];
        for (let i = 0; i < columnsList.length; i++) {
            if (columnsList[i].type === this.state.payload.cleaningAndTransformConfig.operations[this.state.operationIndex].colType) {
                options.push({
                    value: columnsList[i].keys,
                    label: columnsList[i].keys
                })
            }
        }
        return options;
    }

    handleMultipleColumnsToolChange = value => {
        let formPayload = cloneDeep(this.state.formPayload),
            selectedInputCols;
        selectedInputCols = value
        formPayload.columnList = []
        formPayload.columnList = value.map(val => { return val.value })
        this.setState({
            formPayload,
            selectedInputCols
        })
    }

    handleInputColumnChange = (value, type, isMulti) => {
        let formPayload = cloneDeep(this.state.formPayload),
            selectedInputCols;
        selectedInputCols = value
        if (isMulti) {
            formPayload.inputCols = []
            formPayload.inputCols = value.map(val => { return val.value })
        }
        else {
            formPayload.inputCol = ''
            formPayload.inputCol = value.value
        }
        this.setState({
            formPayload,
            selectedInputCols
        })
    }

    getStatistics = (event, isCleaningAndTransform) => {
        event.stopPropagation();
        let payload = {
            action: {
                type: "showStatistics",
                actionName: event.target.name
            },
            dataSource: this.state.payload.dataSource
        },
            showMissingValuesData = cloneDeep(this.state.showMissingValuesData),
            statisticalReportData = cloneDeep(this.state.statisticalReportData),
            showSampleData = cloneDeep(this.state.showSampleData),
            dataCorrelationData = cloneDeep(this.state.dataCorrelationData),
            statisticalReportDataWithCleaningAndTransform = cloneDeep(this.state.statisticalReportDataWithCleaningAndTransform),
            dataCorrelationDataWithCleaningAndTransform = cloneDeep(this.state.dataCorrelationDataWithCleaningAndTransform),
            showMissingValuesDataWithCleaningAndTransform = cloneDeep(this.state.showMissingValuesDataWithCleaningAndTransform),
            showSampleDataWithCleaningAndTransform = cloneDeep(this.state.showSampleDataWithCleaningAndTransform);
        if (isCleaningAndTransform) {
            if ((event.target.name === "dataCorrelationWithCleaningAndTransform" && dataCorrelationDataWithCleaningAndTransform.userId)
                || (event.target.name === "showStatisticalReportWithCleaningAndTransform" && statisticalReportDataWithCleaningAndTransform.userId)
                || (event.target.name === "showMissingValuesWithCleaningAndTransform" && showMissingValuesDataWithCleaningAndTransform.userId)
                || (event.target.name === "showSampleDataWithCleaningAndTransform" && showSampleDataWithCleaningAndTransform.userId)) {
                $(`#${event.target.name}Modal`).modal()
            }
            else {
                payload.cleaningAndTransformConfig = this.state.payload.cleaningAndTransformConfig
                if (event.target.name === "dataCorrelationWithCleaningAndTransform") {
                    dataCorrelationDataWithCleaningAndTransform = {}
                    payload.action.actionName = "dataCorrelation"
                } else if (event.target.name === "showStatisticalReportWithCleaningAndTransform") {
                    statisticalReportDataWithCleaningAndTransform = {}
                    payload.action.actionName = "showStatisticalReport"
                } else if (event.target.name === "showMissingValuesWithCleaningAndTransform") {
                    showMissingValuesDataWithCleaningAndTransform = {}
                    payload.action.actionName = "showMissingValues"
                }
                else if (event.target.name === "showSampleDataWithCleaningAndTransform") {
                    showSampleDataWithCleaningAndTransform = {}
                    payload.action.actionName = "showSampleData"
                }
                this.props.getStatistics(payload, `${event.target.name}Loader`);

                this.setState({
                    statisticalReportDataWithCleaningAndTransform,
                    dataCorrelationDataWithCleaningAndTransform,
                    showMissingValuesDataWithCleaningAndTransform,
                    showSampleDataWithCleaningAndTransform,
                    [`${event.target.name}Loader`]: true
                })
            }
        }
        else {
            if ((event.target.name === "dataCorrelation" && dataCorrelationData.userId)
                || (event.target.name === "showStatisticalReport" && statisticalReportData.userId)
                || (event.target.name === "showMissingValues" && showMissingValuesData.userId)
                || (event.target.name === "showSampleData" && showSampleData.userId)) {
                $(`#${event.target.name}Modal`).modal()
            }
            else {
                if (event.target.name === "dataCorrelation") {
                    dataCorrelationData = {}
                } else if (event.target.name === "showStatisticalReport") {
                    statisticalReportData = {}
                } else if (event.target.name === "showMissingValues") {
                    showMissingValuesData = {}
                }
                else if (event.target.name === "showSampleData") {
                    showSampleData = {}
                }
                this.props.getStatistics(payload, `${event.target.name}Loader`);

                this.setState({
                    statisticalReportData,
                    dataCorrelationData,
                    showMissingValuesData,
                    showSampleData,
                    [`${event.target.name}Loader`]: true
                })
            }
        }
        let id = event.target.name
        setTimeout(() => this.resetLoaders(id), 300000);
    }

    resetLoaders = (id,) => {
        this.setState({
            [`${id}Loader`]: false
        })
    }

    createMqttConnection = () => {
        let brokerURL = window.location.origin === "http://localhost:3001" ? window.API_URL.split("//")[1].substring(0, window.API_URL.split("//")[1].length - 1) : window.location.host,
            count = 0,
            mlTopic = [`MLStudio/${window.API_URL.split("//")[1].split(".")[0]}`, `MLStudio/${window.API_URL.split("//")[1].split(".")[0]}/informationgain`];
        const options = {
            host: brokerURL,
            protocol: 'wss',
            path: '/connectmqtt',
            username: '83incs',
            password: 'Hello@123',
        };
        client = MQTT.connect(options)
        let _this = this;
        client.on('connect', function () {
        });
        client.on('close', function (response) {
            if (response && count > 4) {
                client.end();
            }
        });
        client.on('reconnect', function () {
            count++;
        });
        client.subscribe(mlTopic, function (err) {
            if (!err) {
                client.on('message', function (topic, message) {
                    let dataMessage = JSON.parse(message.toString()),
                        statisticalReportData = cloneDeep(_this.state.statisticalReportData),
                        dataCorrelationData = cloneDeep(_this.state.dataCorrelationData),
                        showMissingValuesData = cloneDeep(_this.state.showMissingValuesData),
                        showSampleData = cloneDeep(_this.state.showSampleData),
                        statisticalReportDataWithCleaningAndTransform = cloneDeep(_this.state.statisticalReportDataWithCleaningAndTransform),
                        dataCorrelationDataWithCleaningAndTransform = cloneDeep(_this.state.dataCorrelationDataWithCleaningAndTransform),
                        showMissingValuesDataWithCleaningAndTransform = cloneDeep(_this.state.showMissingValuesDataWithCleaningAndTransform),
                        showSampleDataWithCleaningAndTransform = cloneDeep(_this.state.showSampleDataWithCleaningAndTransform),
                        featureImportanceData = cloneDeep(_this.state.featureImportanceData),
                        message2;
                    if (topic === `MLStudio/${window.API_URL.split("//")[1].split(".")[0]}/informationgain`) {
                        let informationGainData = JSON.parse(message.toString()),
                            featurePayload = cloneDeep(_this.state.featurePayload);
                        informationGainData.dataKeys = informationGainData.data && Object.keys(informationGainData.data).length ? Object.keys(informationGainData.data) : []
                        let masterList = [];
                        informationGainData.dataKeys.map(el => {
                            masterList.push({
                                key: el, val: informationGainData.data[el]
                            })
                        })
                        masterList = masterList.sort((a, b) => b.val - a.val);
                        informationGainData.data = {}
                        masterList.map(el => informationGainData.data[el.key] = el.val)
                        informationGainData.dataKeys = Object.keys(informationGainData.data)
                        featurePayload.k = 1;
                        _this.setState({
                            informationGainData,
                            informationGainLoader: false,
                            message2: "Data Prepared , click on the Show Information Gain Data button to view the data.",
                            isOpen: true,
                            modalType: "success",
                            featurePayload
                        })
                        return;
                    }
                    if (jwt_decode(localStorage["token"]).jti === dataMessage.userId && _this.state.payload.dataSource.dirId === dataMessage.dataSource.dirId) {
                        if (dataMessage.cleaningAndTransformConfig) {
                            if (dataMessage.type === "dataCorrelation") {
                                dataCorrelationDataWithCleaningAndTransform = dataMessage
                                dataCorrelationDataWithCleaningAndTransform.dataKeys = dataMessage.data.length ? Object.keys(dataMessage.data[0]) : []
                                message2 = "Data Prepared , click on the Data Correlation button to view the data.";
                            }
                            else if (dataMessage.type === "showStatisticalReport") {
                                statisticalReportDataWithCleaningAndTransform = dataMessage
                                statisticalReportDataWithCleaningAndTransform.dataKeys = dataMessage.variables ? Object.keys(dataMessage.variables) : []
                                message2 = "Data Prepared , click on the Show Statistical Report button to view the data.";
                            }
                            else if (dataMessage.type === "showMissingValues") {
                                showMissingValuesDataWithCleaningAndTransform = dataMessage
                                showMissingValuesDataWithCleaningAndTransform.dataKeys = dataMessage.data ? Object.keys(dataMessage.data[0]) : []
                                message2 = "Data Prepared , click on the Show All N/A button to view the data.";
                            }
                            else if (dataMessage.type === "showSampleData") {
                                showSampleDataWithCleaningAndTransform = dataMessage
                                showSampleDataWithCleaningAndTransform.dataKeys = dataMessage.sample ? Object.keys(dataMessage.sample[0]) : []
                                message2 = "Data Prepared , click on the Show Sample Data button to view the data.";
                            }
                            _this.setState({
                                statisticalReportDataWithCleaningAndTransform,
                                dataCorrelationDataWithCleaningAndTransform,
                                showMissingValuesDataWithCleaningAndTransform,
                                showSampleDataWithCleaningAndTransform,
                                isOpen: true,
                                modalType: "success",
                                message2,
                                [`${dataMessage.type}WithCleaningAndTransformLoader`]: false
                            })
                        }
                        else {
                            if (dataMessage.type === "dataCorrelation") {
                                dataCorrelationData = dataMessage
                                dataCorrelationData.dataKeys = dataMessage.data.length ? Object.keys(dataMessage.data[0]) : []
                                message2 = "Data Prepared , click on the Data Correlation button to view the data.";
                            }
                            else if (dataMessage.type === "showStatisticalReport") {
                                statisticalReportData = dataMessage
                                statisticalReportData.dataKeys = dataMessage.variables ? Object.keys(dataMessage.variables) : []
                                message2 = "Data Prepared , click on the Show Statistical Report button to view the data.";
                            }
                            else if (dataMessage.type === "showMissingValues") {
                                showMissingValuesData = dataMessage
                                showMissingValuesData.dataKeys = dataMessage.data ? Object.keys(dataMessage.data[0]) : []
                                message2 = "Data Prepared , click on the Show All N/A button to view the data.";
                            }
                            else if (dataMessage.type === "showSampleData") {
                                showSampleData = dataMessage
                                showSampleData.dataKeys = dataMessage.sample ? Object.keys(dataMessage.sample[0]) : []
                                message2 = "Data Prepared , click on the Show Sample Data button to view the data.";
                            }
                            else if (dataMessage.type === "featureImportance") {
                                featureImportanceData = dataMessage
                                featureImportanceData.dataKeys = dataMessage.data ? Object.keys(dataMessage.data).sort() : []
                                let masterList = [];
                                featureImportanceData.dataKeys.map(el => {
                                    masterList.push({
                                        key: el, val: featureImportanceData.data[el]
                                    })
                                })
                                masterList = masterList.sort((a, b) => b.val - a.val);
                                featureImportanceData.data = {}
                                masterList.map(el => featureImportanceData.data[el.key] = el.val)
                                featureImportanceData.dataKeys = Object.keys(featureImportanceData.data)
                                message2 = "Data Prepared , click on the Feature Importance button to view the data.";
                            }
                            _this.setState({
                                statisticalReportData,
                                dataCorrelationData,
                                showMissingValuesData,
                                showSampleData,
                                featureImportanceData,
                                isOpen: true,
                                modalType: "success",
                                message2,
                                [`${dataMessage.type}Loader`]: false
                            })
                        }
                    }
                });
            }
        })
    }

    checkIfKeyIsValid = (key) => {
        if (typeof key === "object") {
            return (key && key.__int64__ ? key.__int64__ : "Error")
        }
        else if (!key) {
            return "Error"
        }
        else
            return key
    }

    emptyModalPayload = () => {
        this.setState({
            statisticalReportData: {},
            dataCorrelationData: {},
            showMissingValuesData: {},
            showSampleData: {},
            featureImportanceData: {},
            informationGainData: {},
        })
    }

    emptyModalPayloadWithCleaningAndTransform = () => {
        this.setState({
            statisticalReportDataWithCleaningAndTransform: {},
            dataCorrelationDataWithCleaningAndTransform: {},
            showMissingValuesDataWithCleaningAndTransform: {},
            showSampleDataWithCleaningAndTransform: {},
        })
    }

    createChart = (data, el, chartType, chartSubType) => {
        let dataForChartJson = data.variables[el].histogram_data.__Series__ ? JSON.parse(data.variables[el].histogram_data.__Series__) : null,
            dataForChart = [];
        if (dataForChartJson) {
            for (let i = 0; i < 100; i++) {
                dataForChart.push({
                    category: i,
                    [el]: dataForChartJson[i]
                })
            }
            dataForChart = {
                availableAttributes: [el],
                chartData: dataForChart
            }
            let chartData = {
                theme: "kelly's",
                widgetType: chartType,
                widgetName: chartSubType,
                i: data.cleaningAndTransformConfig ? `chartData_${chartType}_${el}_WithCleaningAndTransform` : `chartData_${chartType}_${el}`,
                widgetData: dataForChart,
                widgetDetails: {
                    "customValues": chartType === "lineChartForMl" ?
                        {
                            "lineThickness": { "value": 2, "type": "number" },
                            "legendPlacement": {
                                "value": "bottom", "type": "select",
                                "options": ["bottom", "top", "left", "right"]
                            },
                            "bulletVisibility": { "value": true, "type": "boolean" }
                            , "bulletSize": { "value": 5, "type": "number", "parent": "bulletVisibility" },
                            "customColors": { "value": false, "type": "boolean" },
                            "aqi": { "value": "#FF0000", "type": "color", "parent": "customColors" }
                        } : {
                            "barWidth": { "value": 80, "type": "number" },
                            "legendPlacement": { "value": "bottom", "type": "select", "options": ["bottom", "top", "left", "right"] },
                            "chartAngle": { "value": 30, "type": "number" }, "chartDepth": { "value": 30, "type": "number" },
                            "customColors": { "value": false, "type": "boolean" }, "year2005": { "value": "#0ba6ef", "type": "color", "parent": "customColors" },
                            "columnTopRadius": { "value": 10 }
                        }
                }
            };
            return (<Chart data={chartData} deleteLegend={chartType === "lineChartForMl" ? false : true} />);
        }
        else {
            return ("Error creating Charts");
        }
    }

    createChartForFeatureImportanceData = (data, chartType, chartSubType) => {
        let dataForChart = [];
        if (data.dataKeys.length) {
            for (let i = 0; i < data.dataKeys.length; i++) {
                dataForChart.push({
                    category: data.dataKeys[i],
                    columnValue: data.data[data.dataKeys[i]]
                })
            }
            dataForChart = {
                availableAttributes: ["columnValue"],
                chartData: dataForChart
            }
            let chartData = {
                widgetType: chartType,
                widgetName: chartSubType,
                i: `chartData_${chartType}`,
                widgetData: dataForChart,
                widgetDetails: {
                    "customValues": {
                        "barWidth": { "value": 80, "type": "number" },
                        "legendPlacement": { "value": "bottom", "type": "select", "options": ["bottom", "top", "left", "right"] },
                        "chartAngle": { "value": 30, "type": "number" }, "chartDepth": { "value": 30, "type": "number" },
                        "customColors": { "value": false, "type": "boolean" }, "year2005": { "value": "#0ba6ef", "type": "color", "parent": "customColors" }
                    }
                }
            };
            return (<Chart data={chartData} deleteLegend={true} />);
        }
        else {
            return ("Error creating Charts");
        }
    }

    SliderChangeHandlerForAlgorithm = (x, key) => {
        let payload = cloneDeep(this.state.payload)
        let value = parseFloat(x.toFixed(1)),
            adjustedValue = parseFloat((1 - value).toFixed(1));
        if (key === "trainSplitValue") {
            payload.algorithm.hyperparams.trainSplitValue = value
            payload.algorithm.hyperparams.testSplitValue = adjustedValue
        } else {
            payload.algorithm.hyperparams.testSplitValue = value
            payload.algorithm.hyperparams.trainSplitValue = adjustedValue
        }
        this.setState({
            payload
        })
    }

    render() {
        var directoryList = [];
        this.state.connectorsList.map(connector => {
            if (connector.id === this.state.payload.dataSource.connectorId) {
                connector.properties.list.map(bucket => {
                    if (bucket.bucketId === this.state.payload.dataSource.bucketId) {
                        bucket.dataDirs.map((dir, index) => {
                            if (dir.dirId === this.state.payload.dataSource.dirId) {
                                directoryList.push({
                                    connector: connector.name,
                                    bucket: bucket.name,
                                    name: dir.name
                                })
                            }
                        })
                    }
                })
            }
        })
        return (
            <div>
                <Helmet>
                    <title>AddOrEditMl</title>
                    <meta name="description" content="Description of AddOrEditMl" />
                </Helmet>
                {
                    this.state.isFetching ? <Loader /> :
                        <React.Fragment>
                            <header className="content-header d-flex">
                                <div className="flex-60">
                                    <div className="d-flex">
                                        <h6 className="previous" onClick={() => { this.props.history.push("/mlExperiments"); }}>ML Experiments</h6>
                                        <h6 className="active">Add Experiment</h6>
                                    </div>
                                </div>
                                <div className="flex-40 text-right">
                                    <div className="content-header-group">
                                        <div className="search-box">
                                            <span className="search-icon"><i className="far fa-search"></i></span>
                                            <input type="text" className="form-control" placeholder="Search..." />
                                            <button className="search-button"><i className="far fa-times"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </header>
                            
                            <div className="content-body">
                                <div className="d-flex pd-b-40">
                                    <div className="flex-50 pd-r-10">
                                        <div className="form-group">
                                            <label className="form-group-label">Experiment Name :<i className="fas fa-asterisk form-group-required"></i></label>
                                            <input type="text" id="name" name="name" className="form-control" value={this.state.payload.name} onChange={this.formChangeHandler} />
                                        </div>
                                    </div>
                                    <div className="flex-50 pd-l-10">
                                        <div className="form-group">
                                            <label className="form-group-label">Description :<i className="fas fa-asterisk form-group-required"></i></label>
                                            <input type="text" id="description" className="form-control" value={this.state.payload.description} onChange={this.formChangeHandler} />
                                        </div>
                                    </div>
                                    <div className="flex-100">
                                        <div className="ml-item-box" data-toggle="collapse" data-target="#item1" onClick={() => { $("#item1").slideToggle() }}>
                                            <h6 className="m-0">Data Configuration</h6>
                                            <div className="btn-group">
                                                {this.state.dataCorrelationLoader ?
                                                    <span className="dataCorrelationLoader"></span> :
                                                    <React.Fragment>
                                                        <button data-tip data-for="dataCorrelation" className="btn btn-link" name="dataCorrelation" disabled={!this.state.payload.dataSource.dirId} onClick={this.getStatistics}>Data Correlation</button>
                                                        <ReactTooltip id="dataCorrelation" place="left" type="dark">
                                                            <div className="tooltipText"><p>Statistical technique to show how strongly pairs of variables are related</p></div>
                                                        </ReactTooltip>
                                                    </React.Fragment>}
                                                {this.state.showStatisticalReportLoader ?
                                                    <span className="showStatisticalReportLoader"></span> :
                                                    <React.Fragment>
                                                        <button className="btn btn-link" data-tip data-for="showStatisticalReport" name="showStatisticalReport" disabled={!this.state.payload.dataSource.dirId} onClick={this.getStatistics}>Show Statistical Report</button>
                                                        <ReactTooltip id="showStatisticalReport" place="left" type="dark">
                                                            <div className="tooltipText"><p>Genrates report for Exploratory data analysis</p></div>
                                                        </ReactTooltip></React.Fragment>}
                                                {this.state.showMissingValuesLoader ?
                                                    <span className="showMissingValuesLoader"></span> :
                                                    <React.Fragment>
                                                        <button className="btn btn-link" data-tip data-for="showMissingValues" name="showMissingValues" disabled={!this.state.payload.dataSource.dirId} onClick={this.getStatistics}>Show All N/a</button>
                                                        <ReactTooltip id="showMissingValues" place="left" type="dark">
                                                            <div className="tooltipText"><p>Missing values in entire dataset</p></div>
                                                        </ReactTooltip>
                                                    </React.Fragment>}
                                                {this.state.showSampleDataLoader ?
                                                    <span className="showSampleDataLoader"></span> :
                                                    <React.Fragment>
                                                        <button className="btn btn-link" data-tip data-for="showSampleData" name="showSampleData" disabled={!this.state.payload.dataSource.dirId} onClick={this.getStatistics}>Show Sample Data</button>
                                                        <ReactTooltip id="showSampleData" place="left" type="dark">
                                                            <div className="tooltipText"><p>Display Sample Data</p></div>
                                                        </ReactTooltip></React.Fragment>}
                                                <button className="btn btn-link" data-tip data-for="featureImportance" name="featureImportance" disabled={!this.state.payload.dataSource.dirId} onClick={(event) => {
                                                    event.stopPropagation();
                                                    this.setState({
                                                        featureImportanceLoader: false
                                                    })
                                                    $(`#${event.target.name}Modal`).modal()
                                                }}>Feature Importance</button>
                                                <ReactTooltip id="featureImportance" place="left" type="dark">
                                                    <div className="tooltipText"><p>Feature importance refers to techniques that assign a score to input features based on how useful they are at predicting a target variable.</p></div>
                                                </ReactTooltip>
                                            </div>
                                        </div>
                                        <div className="ml-collapse-item-box collapse show" id="item1">
                                            <div>
                                                <div className="d-flex">
                                                    <div className="flex-40">
                                                        <div className="form-section">
                                                            <div className="form-section-header">
                                                                <p className="p-0 m-0">Select Source File</p>
                                                            </div>
                                                            <div className="form-section-body">
                                                                <ul className="list-style-none">
                                                                    {this.state.dataConnectorsList.map((dataConnector, index) => {
                                                                        return (
                                                                            <li className={this.props.match.params.id ? 'disabledList' : ''} key={index}>
                                                                                <p className="level-one" data-toggle="collapse" data-target="#child1" onClick={() => this.getConnectorsByCategory(dataConnector.connector)}> <i className="fas fa-caret-right"></i><img src={`https://content.iot83.com/m83/dataConnectors/${dataConnector.imageName}`} />{dataConnector.displayName}</p>
                                                                                <ul className={this.state.payload.dataSource.connectorCategory === dataConnector.connector ? 'collapse show' : "collapse"} id="child1" >
                                                                                    {this.state.connectorsList.map((connector, connectorIndex) => {
                                                                                        return (
                                                                                            <li key={connectorIndex}>
                                                                                                <p className="level-two collapsed" data-toggle="collapse" onClick={() => this.dataSourceChangeHandler("connector", connector.id)}><i className="fas fa-caret-right"></i>{connector.name}</p>
                                                                                                <ul className={this.state.payload.dataSource.connectorId === connector.id ? "collapse show" : "collapse "} id="child2" >
                                                                                                    {connector.properties.list.map((bucket, bucketIndex) => {
                                                                                                        return (
                                                                                                            <li key={bucketIndex}>
                                                                                                                <p className="level-three collapsed" onClick={() => this.dataSourceChangeHandler("bucket", bucket.bucketId)} data-toggle="collapse"><i className="fas fa-caret-right"></i>{bucket.name}</p>
                                                                                                                <ul className={this.state.payload.dataSource.bucketId === bucket.bucketId ? "collapse show" : "collapse "} id="child3" >
                                                                                                                    {/* {bucket.dataDirs.map((dir, dirIndex) => {
                                                                                                                        return (
                                                                                                                            <p key={dirIndex} className="level-three pd-l-80" onClick={() => this.dataSourceChangeHandler("dir", dir.dirId)}><i className="fas fa-circle"></i>{dir.name}</p>
                                                                                                                        )
                                                                                                                    })} */}
                                                                                                                </ul>
                                                                                                            </li>)
                                                                                                    })}
                                                                                                </ul>
                                                                                            </li>)
                                                                                    })}
                                                                                </ul>
                                                                            </li>)
                                                                    })}
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="flex-60">
                                                        {!this.state.payload.dataSource.dirId ?
                                                            <div className="drop-zone-box">
                                                                <div className="drop-zone">
                                                                    <h5 className="text-color f-13">
                                                                        <div className="drop-zone-icon">
                                                                            <img src="https://content.iot83.com/m83/mlStudio/feature.png" />
                                                                        </div>
                                                                            No Source File Selected
                                                                        </h5>
                                                                </div>
                                                            </div> :
                                                            <div className="selected-item-box">
                                                                <div className="selected-item ">
                                                                    <i className="fas fa-times-circle cancel text-content"></i>
                                                                    <div className="d-flex selected-info-box">
                                                                        <img src="https://content.iot83.com/m83/mlStudio/algo.png" />
                                                                        <div className="d-flex selected-info-name">
                                                                            <div className="flex-50 selected-info-image">
                                                                                <img src="https://content.iot83.com/m83/mlStudio/amazon.png" />
                                                                            </div>
                                                                            {directoryList.map((dir, index) => <div className="flex-50" key={index}>
                                                                                <p className="m-0"><strong>{this.state.payload.dataSource.connectorCategory}</strong></p>
                                                                                <p className="m-0">{dir.connector}</p>
                                                                                <p className="m-0">{dir.bucket}</p>
                                                                                <p className="m-0">{dir.name}</p>
                                                                            </div>
                                                                            )}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="flex-100">
                                        <div className="ml-item-box" data-toggle="collapse" data-target="#item2" onClick={() => { $("#item2").slideToggle() }}>
                                            <h6 className="m-0">Cleaning & Transformation</h6>
                                            <div className="btn-group">
                                                {this.state.dataCorrelationWithCleaningAndTransformLoader ?
                                                    <span className="dataCorrelationLoader"></span> :
                                                    <React.Fragment>
                                                        <button className="btn btn-link" data-tip data-for="dataCorrelationWithCleaningAndTransform" name="dataCorrelationWithCleaningAndTransform" disabled={!this.state.payload.cleaningAndTransformConfig.operations[0].actions.length} onClick={(e) => this.getStatistics(e, true)}>Data Correlation</button>
                                                        <ReactTooltip id="dataCorrelationWithCleaningAndTransform" place="left" type="dark">
                                                            <div className="tooltipText"><p>Statistical technique to show how strongly pairs of variables are related</p></div>
                                                        </ReactTooltip></React.Fragment>}
                                                {this.state.showStatisticalReportWithCleaningAndTransformLoader ?
                                                    <span className="showStatisticalReportLoader"></span> :
                                                    <React.Fragment>
                                                        <button className="btn btn-link" data-tip data-for="showStatisticalReportWithCleaningAndTransform" name="showStatisticalReportWithCleaningAndTransform" disabled={!this.state.payload.cleaningAndTransformConfig.operations[0].actions.length} onClick={(e) => this.getStatistics(e, true)}>Show Statistical Report</button>
                                                        <ReactTooltip id="showStatisticalReportWithCleaningAndTransform" place="left" type="dark">
                                                            <div className="tooltipText"><p>Genrates report for Exploratory data analysis</p></div>
                                                        </ReactTooltip></React.Fragment>}
                                                {this.state.showMissingValuesWithCleaningAndTransformLoader ?
                                                    <span className="showMissingValuesLoader"></span> :
                                                    <React.Fragment>
                                                        <button className="btn btn-link" data-tip data-for="showMissingValuesWithCleaningAndTransform" name="showMissingValuesWithCleaningAndTransform" disabled={!this.state.payload.cleaningAndTransformConfig.operations[0].actions.length} onClick={(e) => this.getStatistics(e, true)}>Show All N/a</button>
                                                        <ReactTooltip id="showMissingValuesWithCleaningAndTransform" place="left" type="dark">
                                                            <div className="tooltipText"><p>Missing values in entire dataset</p></div>
                                                        </ReactTooltip></React.Fragment>}
                                                {this.state.showSampleDataWithCleaningAndTransformLoader ?
                                                    <span className="showSampleDataLoader"></span> :
                                                    <React.Fragment>
                                                        <button className="btn btn-link" data-tip data-for="showSampleData" name="showSampleDataWithCleaningAndTransform" disabled={!this.state.payload.cleaningAndTransformConfig.operations[0].actions.length} onClick={(e) => this.getStatistics(e, true)}>Show Sample Data</button>
                                                        <ReactTooltip id="showSampleDataWithCleaningAndTransform" place="left" type="dark">
                                                            <div className="tooltipText"><p>Display Sample Data</p></div>
                                                        </ReactTooltip></React.Fragment>}
                                            </div>
                                        </div>
                                        <div className=" ml-item-collapse-box collapse" id="item2">
                                            <div>
                                                <div className="d-flex">
                                                    <div className="flex-33">
                                                        <div className="form-section">
                                                            <div className="form-section-header">
                                                                <p className="p-0 m-0">Select Tools</p>
                                                            </div>
                                                            <div className="form-section-body">
                                                                 <div className="searchBox input-group mb-3">
                                                            <input type="search" className="form-control" placeholder="&#128269; Search...." />
                                                        </div>
                                                                <ul className="list-style-none d-flex feature-list">
                                                                    {this.state.tools.map((tool, index) => {
                                                                        return (
                                                                            <li key={index} className="flex-50" draggable onDragStart={() => this.onToolDrag(tool.name)}><img src={tool.url} />{tool.displayName}</li>)
                                                                    })}
                                                                </ul>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div className="flex-33">
                                                        <div className="form-section">
                                                            <div className="form-section-header d-flex">
                                                                <p className="p-0 m-0 flex-50">Add Columns</p>
                                                                <div className="flex-50 text-right">
                                                                    <button type="button" className="btn-transparent btn-transparent-blue float-right" onClick={() => this.addRemoveColumns("add", null)}>
                                                                        <i className="fas fa-plus"></i>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                            <div className="form-section-body">
                                                                {this.state.payload.cleaningAndTransformConfig.operations.map((operation, operationIndex) => {
                                                                    return (
                                                                        <div className="column-box" key={operationIndex}>
                                                                            <button className={this.state.payload.cleaningAndTransformConfig.operations.length === 1 ? "btn-transparent btn-transparent-red btn-disabled" : "btn-transparent btn-transparent-red"} disabled={this.state.payload.cleaningAndTransformConfig.operations.length === 1} onClick={() => this.addRemoveColumns("delete", operationIndex)}><i className="far fa-trash-alt"></i></button>
                                                                            <div className="form-group">
                                                                                <label className="form-group-label">Column :</label>
                                                                                <select className="form-control" id="colName" required value={operation.colName} onChange={(e) => this.columnChangeHandler(e, operationIndex)}>
                                                                                    <option value=''>Select</option>
                                                                                    {this.state.columnsList.map((column, columnIndex) => {
                                                                                        return (<option key={columnIndex} value={column.keys}>{column.keys}</option>)
                                                                                    }
                                                                                    )}
                                                                                    {this.state.columnsList.length && <option value='ForAll'>Select Multi Columns</option>}
                                                                                </select>
                                                                            </div>
                                                                            <div>
                                                                                <ul className="list-style-none">
                                                                                    <React.Fragment>
                                                                                        {operation.actions.map((action, actionIndex) => {
                                                                                            let url = this.state.tools.find(tool => tool.name === action.actionName).url
                                                                                            let displayName = this.state.tools.find(tool => tool.name === action.actionName).displayName
                                                                                            return (
                                                                                                <li key={actionIndex} className={action.isCompleted ? this.state.actionIndex === actionIndex && this.state.operationIndex === operationIndex ? "active completed" : "unactive completed" : this.state.actionIndex === actionIndex && this.state.operationIndex === operationIndex ? "active" : "unactive"}>
                                                                                                    <p onClick={(e) => { this.onActionSelect(e, operationIndex, actionIndex, action.actionName) }}> <img src={url} />{displayName}</p><button className="btn-transparent btn-transparent-red" onClick={(e) => this.deleteAction(e, operationIndex, actionIndex)}><i className="fas fa-trash-alt"></i></button>
                                                                                                </li>
                                                                                            )
                                                                                        })}
                                                                                        <li className="drop-zone-box pd-r-10" onDrop={(e) => this.onToolDrop(e, operationIndex)} onDragOver={(event) => this.allowDrop(event)} >
                                                                                            <div className="drop-zone">
                                                                                                <h6>Drag and Drop Feature Here</h6>
                                                                                            </div>
                                                                                        </li>
                                                                                    </React.Fragment>
                                                                                </ul>
                                                                            </div>
                                                                        </div>)
                                                                })}
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="flex-33">
                                                        <div className="form-section">
                                                            {this.state.selectedAction ?
                                                                <div className="form-section-header">
                                                                    <span className="fw-600">Properties <i className="fas fa-greater-than mr-r-10 mr-l-10 f-12"></i></span><span className="fw-600">Transformation</span><i className="fas fa-greater-than mr-r-10 mr-l-10 f-12"></i><span className="fw-600">{this.state.tools.find(tool => tool.name === this.state.selectedAction).displayName}</span>
                                                                </div> :
                                                                <div className="form-section-header">
                                                                    <span className="fw-600">Properties <i className="fas fa-greater-than mr-r-10 mr-l-10 f-12"></i></span><span className="fw-600">Transformation</span>
                                                                </div>}
                                                            <div className="form-section-body">
                                                                {
                                                                    this.state.selectedAction === '' &&
                                                                    <h5 className="f-13">Select Atleast One Tool</h5>
                                                                }
                                                                 ________________ Trimming ________________
                                                                {this.state.selectedAction === "trimming" &&
                                                                    <form className="contentForm" onSubmit={this.actionSaveHandler}>
                                                                        <div className="form-group">
                                                                            <div className="d-flex">
                                                                                <label className="flex-30 pd-l-10">Column Name:</label>
                                                                                <div className="flex-25">
                                                                                    <label className="radioLabel">Left
                                                                                <input name="type" type="radio" value="left" checked={this.state.formPayload.type === "left"} onChange={this.transformationFormChangeHandler} />
                                                                                        <span className="radioMark" />
                                                                                    </label>
                                                                                </div>
                                                                                <div className="flex-25">
                                                                                    <label className="radioLabel">Right
                                                                                <input name="type" type="radio" value="right" checked={this.state.formPayload.type === "right"} onChange={this.transformationFormChangeHandler} />
                                                                                        <span className="radioMark" />
                                                                                    </label>
                                                                                </div>
                                                                                <div className="flex-20">
                                                                                    <label className="radioLabel">All
                                                                                <input name="type" type="radio" value="all" checked={this.state.formPayload.type === "all"} onChange={this.transformationFormChangeHandler} />
                                                                                        <span className="radioMark" />
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div className="bottom-button text-right">
                                                                            <button type="submit" className="btn btn-success m-0">Save</button>
                                                                        </div>
                                                                    </form>}


                                                                 ________________ Padding ________________
                                                                {this.state.selectedAction === "padding" &&
                                                                    <form className="contentForm" onSubmit={this.actionSaveHandler}  >
                                                                        <div className="form-group">
                                                                            <input className="form-control" id="length" value={this.state.formPayload.length} type="number" onChange={this.transformationFormChangeHandler} placeholder="indexes" />
                                                                            <label className="form-group-label">Length :<i className="fas fa-asterisk form-group-required"></i> </label>
                                                                        </div>
                                                                        <div className="form-group">
                                                                            <div className="d-flex">
                                                                                <label className="flex-40 pd-l-10">Type:</label>
                                                                                <div className="flex-30">
                                                                                    <label className="radioLabel">Left
                                                                                            <input name="type" type="radio" value="left" checked={this.state.formPayload.type === "left"} onChange={this.transformationFormChangeHandler} />
                                                                                        <span className="radioMark" />
                                                                                    </label>
                                                                                </div>
                                                                                <div className="flex-30">
                                                                                    <label className="radioLabel">Right
                                                                                            <input name="type" type="radio" value="right" checked={this.state.formPayload.type === "right"} onChange={this.transformationFormChangeHandler} />
                                                                                        <span className="radioMark" />
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div className="form-group">
                                                                            <input className="form-control" id="padWith" value={this.state.formPayload.padWith} type="number" onChange={this.transformationFormChangeHandler} placeholder="indexes" />
                                                                            <label className="form-group-label">Bandwidth :<i className="fas fa-asterisk form-group-required"></i>
                                                                            </label>
                                                                        </div>
                                                                        <div className="bottom-button text-right">
                                                                            <button type="submit" className="btn btn-success m-0">Save</button>
                                                                        </div>
                                                                    </form>}

                                                                 ________________ Fill Null value ________________
                                                                {this.state.selectedAction === "fillNullValues" &&
                                                                    <form className="contentForm" onSubmit={this.actionSaveHandler}>
                                                                        <div className="d-flex">
                                                                            <label className="flex-40 pd-l-10">Type:</label>
                                                                            <div className="flex-30">
                                                                                <label className={this.state.payload.cleaningAndTransformConfig.operations[this.state.operationIndex].colType === "String" ? "radioLabel radioDisabled" : "radioLabel"}>Mean
                                                                                        <input name="type" type="radio" value="mean" disabled={this.state.payload.cleaningAndTransformConfig.operations[this.state.operationIndex].colType === "String"} checked={this.state.formPayload.type === "mean"} onChange={this.transformationFormChangeHandler} />
                                                                                    <span className="radioMark" />
                                                                                </label>
                                                                            </div>
                                                                            <div className="flex-30">
                                                                                <label className="radioLabel">Mode
                                                                                        <input name="type" type="radio" value="mode" checked={this.state.formPayload.type === "mode"} onChange={this.transformationFormChangeHandler} />
                                                                                    <span className="radioMark" />
                                                                                </label>
                                                                            </div>
                                                                        </div>
                                                                        <div className="bottom-button text-right">
                                                                            <button type="submit" className="btn btn-success m-0">Save</button>
                                                                        </div>
                                                                    </form>}

                                                                 ________________ Handling All N/A ________________
                                                                {this.state.selectedAction === "treatAllMissingValues" &&
                                                                    <p className='handlingAll'>Handling All doesn't require any parameter</p>
                                                                }

                                                                 ________________ Unique Column ________________   ________________ Quassi Constant  ________________
                                                                {(this.state.selectedAction === "uniqueColumn" || this.state.selectedAction === "quasiConstant") &&
                                                                    <form className="contentForm" onSubmit={this.actionSaveHandler}>
                                                                        <div className="form-group">
                                                                            <div className="d-flex">
                                                                                <label className="flex-20 pd-l-10">Tolerance:</label>
                                                                                <p className="flex-20 fw-700 pd-t-2 text-theme">{this.state.formPayload.tolerance}</p>
                                                                                <div className="flex-60">
                                                                                    <label className="radioLabel pd-0 mr-r-16">0</label>
                                                                                    <Slider
                                                                                        axis="x"
                                                                                        xmin={0}
                                                                                        xmax={1}
                                                                                        xstep={0.01}
                                                                                        x={this.state.formPayload.tolerance}
                                                                                        onChange={({ x }) => { this.SliderChangeHandler(x) }}
                                                                                    />
                                                                                    <label className="radioLabel pd-0 mr-l-16">1</label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div className="bottom-button text-right">
                                                                            <button type="submit" className="btn btn-success m-0">Save</button>
                                                                        </div>
                                                                    </form>}

                                                                 ________________ Drop Column  ________________
                                                                {this.state.selectedAction === "dropColumn" &&
                                                                    <p className='handlingAll'>Drop Column doesn't require any parameter</p>
                                                                }

                                                                 ________________ Drop Duplicate  ________________
                                                                {this.state.selectedAction === "dropDuplicates" &&
                                                                    <p className='handlingAll'>Drop Duplicate doesn't require any parameter</p>
                                                                }

                                                                 ________________ Regrex Replace  ________________
                                                                {this.state.selectedAction === "regrexReplace" &&
                                                                    <form className="contentForm" onSubmit={this.actionSaveHandler}>
                                                                        <div className="form-group">
                                                                            <input className="form-control" type="text" required id="replaceExcept" value={this.state.formPayload.replaceExcept} onChange={this.transformationFormChangeHandler} placeholder="indexes" />
                                                                            <label className="form-group-label">Replace Except :<i className="fas fa-asterisk form-group-required"></i></label>
                                                                        </div>
                                                                        <div className="form-group">
                                                                            <input className="form-control" type="text" required id="replaceWith" value={this.state.formPayload.replaceWith} onChange={this.transformationFormChangeHandler} placeholder="indexes" />
                                                                            <label className="form-group-label">Replace With :<i className="fas fa-asterisk form-group-required"></i></label>
                                                                        </div>
                                                                        <div className="bottom-button text-right">
                                                                            <button type="submit" className="btn btn-success m-0">Save</button>
                                                                        </div>
                                                                    </form>}

                                                                 ________________ Select Multiple Columns  ________________
                                                                {this.state.selectedAction === "selectColumn" &&
                                                                    <form className="contentForm" onSubmit={this.actionSaveHandler}>
                                                                        <div className="form-group inheritedForm">
                                                                            <label className="form-label">Select Column :</label>
                                                                            <ReactSelect
                                                                                value={this.state.selectedInputCols}
                                                                                onChange={this.handleMultipleColumnsToolChange}
                                                                                options={this.createOptionsForMultiColumns()}
                                                                                isMulti={true}
                                                                            />
                                                                        </div>
                                                                        <div className="bottom-button text-right">
                                                                            <button type="submit" className="btn btn-success m-0">Save</button>
                                                                        </div>
                                                                    </form>}

                                                                 ________________ Encoding  ________________
                                                                {this.state.selectedAction === "encoding" &&
                                                                    <form className="contentForm" onSubmit={this.actionSaveHandler}>
                                                                        <div className="inheritedForm">
                                                                            <label className="form-label">Select Encoding :</label>
                                                                            <ReactSelect
                                                                                value={this.state.selectedEncoder}
                                                                                onChange={this.handleChangeForReactSelect}
                                                                                options={this.state.optionsForEncoder}
                                                                                isMulti={false}
                                                                                isOptionDisabled={(option) => { return !option.type.includes(this.state.payload.cleaningAndTransformConfig.operations[this.state.operationIndex].colType) }}
                                                                            />
                                                                        </div>


                                                                         (1)_______________Count vectorize________________
                                                                        {this.state.formPayload.encoder === "countVectorize" &&
                                                                            <React.Fragment>
                                                                                <div className="form-group">
                                                                                    <input className="form-control" type="number" id="minDf" value={this.state.formPayload.minDf} onChange={this.transformationFormChangeHandler} placeholder="indexes" />
                                                                                    <label className="form-group-label">Min DF :<i className="fas fa-asterisk form-group-required"></i></label>
                                                                                </div>
                                                                                <div className="form-group">
                                                                                    <input className="form-control" type="number" id="vocabSize" value={this.state.formPayload.vocabSize} onChange={this.transformationFormChangeHandler} placeholder="indexes" />
                                                                                    <label className="form-group-label">Vocab Size :<i className="fas fa-asterisk form-group-required"></i> </label>
                                                                                </div>
                                                                                <div className="form-group">
                                                                                    <input className="form-control" type="text" id="outputCol" value={this.state.formPayload.outputCol} onChange={this.transformationFormChangeHandler} placeholder="indexes" />
                                                                                    <label className="form-group-label">Output Column :<i className="fas fa-asterisk form-group-required"></i> </label>
                                                                                </div>
                                                                                <div className="form-group">
                                                                                    <div className="d-flex">
                                                                                        <label className="flex-40 pd-l-10">Drop Original Column:</label>
                                                                                        <div className="flex-30">
                                                                                            <label className="radioLabel">True
                                                                                                    <input name="dropColumn" type="radio" value={"true"} checked={this.state.formPayload.dropColumn === "true"} onChange={this.transformationFormChangeHandler} />
                                                                                                <span className="radioMark" />
                                                                                            </label>
                                                                                        </div>
                                                                                        <div className="flex-30">
                                                                                            <label className="radioLabel">False
                                                                                                    <input name="dropColumn" type="radio" value={"false"} checked={this.state.formPayload.dropColumn === "false"} onChange={this.transformationFormChangeHandler} />
                                                                                                <span className="radioMark" />
                                                                                            </label>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="bottom-button text-right">
                                                                                    <button type="submit" className="btn btn-success m-0">Save</button>
                                                                                </div>
                                                                            </React.Fragment>}


                                                                         (2)_________________Onehotencoder________________
                                                                        {this.state.formPayload.encoder === "oneHotEncoder" &&
                                                                            <React.Fragment>
                                                                                <div className="form-group inheritedForm">
                                                                                    <label className="form-label">Select Column :</label>
                                                                                    <ReactSelect
                                                                                        value={this.state.selectedInputCols}
                                                                                        onChange={(value, type) => this.handleInputColumnChange(value, type, false)}
                                                                                        options={this.createOptionsForReactSelect()}
                                                                                        isMulti={false}
                                                                                    />
                                                                                </div>
                                                                                <div className="form-group">
                                                                                    <input className="form-control" type="text" id="outputCols" value={this.state.formPayload.outputCols} onChange={this.transformationFormChangeHandler} placeholder="indexes" />
                                                                                    <label className="form-group-label">Output column :<i className="fas fa-asterisk form-group-required"></i> </label>                                                                                   </div>
                                                                                <div className="form-group">
                                                                                    <div className="d-flex">
                                                                                        <label className="flex-40 pd-l-10">Drop Original Column :</label>
                                                                                        <div className="flex-30">
                                                                                            <label className="radioLabel">True
                                                                                                    <input name="dropColumn" type="radio" value={"true"} checked={this.state.formPayload.dropColumn === "true"} onChange={this.transformationFormChangeHandler} />
                                                                                                <span className="radioMark" />
                                                                                            </label>
                                                                                        </div>
                                                                                        <div className="flex-30">
                                                                                            <label className="radioLabel">False
                                                                                                    <input name="dropColumn" type="radio" value={"false"} checked={this.state.formPayload.dropColumn === "false"} onChange={this.transformationFormChangeHandler} />
                                                                                                <span className="radioMark" />
                                                                                            </label>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="bottom-button text-right">
                                                                                    <button type="submit" className="btn btn-success m-0">Save</button>
                                                                                </div>
                                                                            </React.Fragment>}


                                                                         (3)_________________Vector Assemblers________________
                                                                        {this.state.formPayload.encoder === "vectorAssemblers" &&
                                                                            <React.Fragment>
                                                                                <div className="form-group inheritedForm">
                                                                                    <label className="form-label">Select Column :</label>
                                                                                    <ReactSelect
                                                                                        value={this.state.selectedInputCols}
                                                                                        onChange={(value, type) => this.handleInputColumnChange(value, type, true)}
                                                                                        options={this.createOptionsForReactSelect()}
                                                                                        isMulti={true}
                                                                                    />
                                                                                </div>
                                                                                <div className="form-group">
                                                                                    <input className="form-control" type="text" id="outputCol" value={this.state.formPayload.outputCol} onChange={this.transformationFormChangeHandler} placeholder="indexes" />
                                                                                    <label className="form-group-label">Output column :<i className="fas fa-asterisk form-group-required"></i> </label>
                                                                                </div>
                                                                                <div className="form-group">
                                                                                    <div className="d-flex">
                                                                                        <label className="flex-40 pd-l-10">Drop Original Column:</label>
                                                                                        <div className="flex-30">
                                                                                            <label className="radioLabel">True
                                                                                                    <input name="dropColumn" type="radio" value={"true"} checked={this.state.formPayload.dropColumn === "true"} onChange={this.transformationFormChangeHandler} />
                                                                                                <span className="radioMark" />
                                                                                            </label>
                                                                                        </div>
                                                                                        <div className="flex-30">
                                                                                            <label className="radioLabel">False
                                                                                                    <input name="dropColumn" type="radio" value={"false"} checked={this.state.formPayload.dropColumn === "false"} onChange={this.transformationFormChangeHandler} />
                                                                                                <span className="radioMark" />
                                                                                            </label>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="bottom-button text-right">
                                                                                    <button type="submit" className="btn btn-success m-0">Save</button>
                                                                                </div>
                                                                            </React.Fragment>}


                                                                         (4)_________________ Binaries ________________
                                                                        {this.state.formPayload.encoder === "binaries" &&
                                                                            <React.Fragment>
                                                                                <div className="form-group inheritedForm">
                                                                                    <label className="form-label">Select Column :</label>
                                                                                    <ReactSelect
                                                                                        value={this.state.selectedInputCols}
                                                                                        onChange={(value, type) => this.handleInputColumnChange(value, type, false)}
                                                                                        options={this.createOptionsForReactSelect()}
                                                                                        isMulti={true}
                                                                                    />
                                                                                </div>
                                                                                <div className="form-group">
                                                                                    <div className="d-flex">
                                                                                        <label className="flex-20 pd-l-10">Threshold:</label>
                                                                                        <p className="flex-20 fw-700 pd-t-2 text-theme">{this.state.formPayload.threshold}</p>
                                                                                        <div className="flex-60">
                                                                                            <label className="radioLabel pd-0 mr-r-16">0</label>
                                                                                            <Slider
                                                                                                axis="x"
                                                                                                xmin={0}
                                                                                                xmax={1}
                                                                                                xstep={0.01}
                                                                                                x={this.state.formPayload.threshold}
                                                                                                onChange={({ x }) => { this.SliderChangeHandler(x, "threshold") }}
                                                                                            />
                                                                                            <label className="radioLabel pd-0 mr-l-16">1</label>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="form-group">
                                                                                    <div className="d-flex">
                                                                                        <label className="flex-40 pd-l-10">Drop Original Column:</label>
                                                                                        <div className="flex-30">
                                                                                            <label className="radioLabel">True
                                                                                                    <input name="dropColumn" type="radio" value={"true"} checked={this.state.formPayload.dropColumn === "true"} onChange={this.transformationFormChangeHandler} />
                                                                                                <span className="radioMark" />
                                                                                            </label>
                                                                                        </div>
                                                                                        <div className="flex-30">
                                                                                            <label className="radioLabel">False
                                                                                                    <input name="dropColumn" type="radio" value={"false"} checked={this.state.formPayload.dropColumn === "false"} onChange={this.transformationFormChangeHandler} />
                                                                                                <span className="radioMark" />
                                                                                            </label>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="bottom-button text-right">
                                                                                    <button type="submit" className="btn btn-success m-0">Save</button>
                                                                                </div>
                                                                            </React.Fragment>
                                                                        }

                                                                         (5)_________________Stop Words Remover________________
                                                                        {this.state.formPayload.encoder === "stopWordsRemover" &&
                                                                            <React.Fragment>
                                                                                <div className="form-group inheritedForm">
                                                                                    <label className="form-label">Select Column :</label>
                                                                                    <ReactSelect
                                                                                        value={this.state.selectedInputCols}
                                                                                        onChange={(value, type) => this.handleInputColumnChange(value, type, false)}
                                                                                        options={this.createOptionsForReactSelect()}
                                                                                        isMulti={false}
                                                                                    />
                                                                                </div>
                                                                                <div className="form-group">
                                                                                    <input className="form-control" type="text" id="outputCol" value={this.state.formPayload.outputCol} onChange={this.transformationFormChangeHandler} placeholder="indexes" />
                                                                                    <label className="form-group-label">Output Column : <i className="fas fa-asterisk form-group-required"></i> </label>
                                                                                </div>
                                                                                <div className="form-group">
                                                                                    <div className="d-flex">
                                                                                        <label className="flex-40 pd-l-10">Drop Original Column:</label>
                                                                                        <div className="flex-30">
                                                                                            <label className="radioLabel">True
                                                                                                    <input name="dropColumn" type="radio" value={"true"} checked={this.state.formPayload.dropColumn === "true"} onChange={this.transformationFormChangeHandler} />
                                                                                                <span className="radioMark" />
                                                                                            </label>
                                                                                        </div>
                                                                                        <div className="flex-30">
                                                                                            <label className="radioLabel">False
                                                                                                    <input name="dropColumn" type="radio" value={"false"} checked={this.state.formPayload.dropColumn === "false"} onChange={this.transformationFormChangeHandler} />
                                                                                                <span className="radioMark" />
                                                                                            </label>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="bottom-button text-right">
                                                                                    <button type="submit" className="btn btn-success m-0">Save</button>
                                                                                </div>
                                                                            </React.Fragment>
                                                                        }

                                                                         (6)_________________String Indexer________________
                                                                        {this.state.formPayload.encoder === "stringIndexer" &&
                                                                            <React.Fragment>
                                                                                <div className="form-group inheritedForm">
                                                                                    <label className="form-label">Select Column :</label>
                                                                                    <ReactSelect
                                                                                        value={this.state.selectedInputCols}
                                                                                        onChange={(value, type) => this.handleInputColumnChange(value, type, false)}
                                                                                        options={this.createOptionsForReactSelect()}
                                                                                        isMulti={false}
                                                                                    />
                                                                                </div>
                                                                                <div className="form-group">
                                                                                    <input className="form-control" type="text" id="outputCol" value={this.state.formPayload.outputCol} onChange={this.transformationFormChangeHandler} placeholder="indexes" />
                                                                                    <label className="form-group-label">Output Column :<i className="fas fa-asterisk form-group-required"></i> </label>
                                                                                </div>
                                                                                <div className="form-group">
                                                                                    <div className="d-flex">
                                                                                        <label className="flex-40 pd-l-10">Drop Original Column:</label>
                                                                                        <div className="flex-30">
                                                                                            <label className="radioLabel">True
                                                                                                    <input name="dropColumn" type="radio" value={"true"} checked={this.state.formPayload.dropColumn === "true"} onChange={this.transformationFormChangeHandler} />
                                                                                                <span className="radioMark" />
                                                                                            </label>
                                                                                        </div>
                                                                                        <div className="flex-30">
                                                                                            <label className="radioLabel">False
                                                                                                    <input name="dropColumn" type="radio" value={"false"} checked={this.state.formPayload.dropColumn === "false"} onChange={this.transformationFormChangeHandler} />
                                                                                                <span className="radioMark" />
                                                                                            </label>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="bottom-button text-right">
                                                                                    <button type="submit" className="btn btn-success m-0">Save</button>
                                                                                </div>
                                                                            </React.Fragment>
                                                                        }
                                                                    </form>}

                                                                {this.state.selectedAction === "transformation" &&
                                                                    <form className="contentForm" onSubmit={this.actionSaveHandler}>
                                                                        <div className="form-group">
                                                                            <select className="form-control" id="transform" required value={this.state.formPayload.transform} onChange={this.transformationFormChangeHandler}>
                                                                                <option value=''>Select</option>
                                                                                <option value='renameColumn'>Rename Column</option>
                                                                                <option value='targetColumn'>Target Column</option>
                                                                                <option value='splitData'>Split Data</option>
                                                                            </select>
                                                                            <label className="form-group-label">Column :<i className="fas fa-asterisk form-group-required"></i> </label>
                                                                        </div>
                                                                        {this.state.formPayload.transform === 'renameColumn' &&
                                                                            <React.Fragment>
                                                                                <div className="form-group">
                                                                                    <input className="form-control" type="text" id="newColumnName" value={this.state.formPayload.newColumnName} onChange={this.transformationFormChangeHandler} placeholder="indexes" />
                                                                                    <label className="form-group-label">New Column Name :<i className="fas fa-asterisk form-group-required"></i> </label>
                                                                                </div>
                                                                                <div className="bottom-button text-right">
                                                                                    <button type="submit" className="btn btn-success m-0">Save</button>
                                                                                </div>
                                                                            </React.Fragment>
                                                                        }
                                                                        {this.state.formPayload.transform === 'targetColumn' &&
                                                                            <React.Fragment>
                                                                                <div className="form-group">
                                                                                    <p className='handlingAll'>Target Column doesn't require any parameter</p>
                                                                                </div>
                                                                                <div className="bottom-button text-right">
                                                                                    <button type="submit" className="btn btn-success m-0">Save</button>
                                                                                </div>
                                                                            </React.Fragment>
                                                                        }
                                                                        {this.state.formPayload.transform === 'splitData' &&
                                                                            <React.Fragment>
                                                                                <div className="d-flex">
                                                                                    <label className="flex-20 pd-l-10">Percent:</label>
                                                                                    <p className="flex-20 fw-700 pd-t-2 text-theme">{this.state.formPayload.percent}</p>
                                                                                    <div className="flex-60">
                                                                                        <label className="radioLabel pd-0 mr-r-16">0</label>
                                                                                        <Slider
                                                                                            axis="x"
                                                                                            xmin={0}
                                                                                            xmax={1}
                                                                                            xstep={0.01}
                                                                                            x={this.state.formPayload.percent}
                                                                                            onChange={({ x }) => { this.SliderChangeHandler(x, "percent") }}
                                                                                        />
                                                                                        <label className="radioLabel p-0 mr-l-16">1</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="bottom-button text-right">
                                                                                    <button type="submit" className="btn btn-success m-0">Save</button>
                                                                                </div>
                                                                            </React.Fragment>
                                                                        }
                                                                    </form>}

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="flex-100">
                                        <div className="ml-item-box" data-toggle="collapse" data-target="#item3">
                                            <h6 className="m-0">Feature Selection</h6>
                                        </div>
                                        <div className="ml-item-collapse-box collapse" id="item3">
                                            <div>
                                                <div className="d-flex">
                                                    <div className="flex-33">
                                                        <div className="form-section">
                                                            <div className="form-section-header">
                                                                <p className="p-0 m-0">Select Features</p>
                                                            </div>
                                                            <div className="form-section-body">
                                                                <ul className="list-style-none feature-list">
                                                                    {this.state.featureList.map((feature, index) => {
                                                                        return (
                                                                            <li key={index} draggable onDragStart={() => this.onFeatureDrag(feature.value)}> <img src={`https://content.iot83.com/m83/mlStudio/${feature.imageName}.png`} />{feature.displayName}</li>
                                                                        )
                                                                    })}
                                                                </ul>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div className="flex-33">
                                                        <div className="form-section addFeature">
                                                            <div className="form-section-header">
                                                                <p className="p-0 m-0">Selected Features</p>
                                                            </div>
                                                            <div className="form-section-body">
                                                                <ul className="list-style-none add-feature-list">
                                                                    <React.Fragment>
                                                                        {this.state.payload.featureSelectionConfig.techniques.map((feature, featureIndex) => {
                                                                            let displayName = this.state.featureList.find(el => el.value === feature.actionName).displayName
                                                                            return (
                                                                                <li key={featureIndex} className={feature.isCompleted ? this.state.featureIndex === featureIndex ? "active completed" : "unactive completed" : this.state.featureIndex === featureIndex ? "active" : "unactive"}>
                                                                                    <p onClick={(e) => { this.onFeatureSelect(e, featureIndex, feature.actionName) }}> <img src="https://content.iot83.com/m83/mlStudio/vectorSlicing.png" />{displayName}<button className="btn-transparent btn-transparent-blue"><i className="fas fa-question"></i></button></p><button className="btn-transparent btn-transparent-red" onClick={(e) => this.deleteFeature(e, featureIndex)}><i className="fas fa-trash-alt"></i></button>
                                                                                </li>
                                                                            )
                                                                        })}
                                                                        <li className="drop-zone-box pd-r-10" onDrop={(e) => this.onFeatureDrop(e)} onDragOver={(event) => this.allowDrop(event)} >
                                                                            <div className="drop-zone active">
                                                                                <h6>Drag and Drop Feature Here</h6>
                                                                            </div>
                                                                        </li>
                                                                    </React.Fragment>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="flex-33">
                                                        <div className="form-section">

                                                            {this.state.selectedFeature ?
                                                                <div className="form-section-header">
                                                                    <span className="fw-600">Properties<i className="fas fa-greater-than mr-r-10 mr-l-10 f-12"></i></span><span className="fw-600">{this.state.featureList.find(el => el.value === this.state.selectedFeature).displayName}</span>
                                                                </div> :
                                                                <div className="form-section-header">
                                                                    <span className="fw-600">Properties</span>
                                                                </div>}
                                                            <div className="form-section-body">
                                                                {
                                                                    !this.state.selectedFeature &&
                                                                    <h5 className="f-13">Select Atleast One Feature</h5>
                                                                }
                                                                {this.state.selectedFeature === "correlationRemoval" &&
                                                                    <form className="contentForm" onSubmit={this.featureSaveHandler}>
                                                                        <div className="d-flex">
                                                                            <label className="flex-20 pd-l-10">Percent:</label>
                                                                            <p className="flex-20 pd-t-2 fw-700 text-theme">{this.state.featurePayload.threshold}</p>
                                                                            <div className="flex-60">
                                                                                <label className="radioLabel pd-0 mr-r-16">0.1</label>
                                                                                <Slider
                                                                                    axis="x"
                                                                                    xmin={0.1}
                                                                                    xmax={0.7}
                                                                                    xstep={0.01}
                                                                                    x={this.state.featurePayload.threshold}
                                                                                    onChange={({ x }) => { this.SliderChangeHandlerForFeatureSelection(x, "threshold") }}
                                                                                />
                                                                                <label className="radioLabel pd-0 mr-l-16">0.7</label>
                                                                            </div>
                                                                        </div>
                                                                        <div className="form-group">
                                                                            <label className="form-group-label">Target Column: <i className="fas fa-asterisk form-group-required"></i></label>
                                                                            <select className="form-control" id="targetColumn" required value={this.state.featurePayload.targetColumn} onChange={this.featureFormChangeHandler}>
                                                                                <option value=''>Select</option>
                                                                                {this.state.columnsList.map((column, columnIndex) => {
                                                                                    return (<option key={columnIndex} value={column.keys}>{column.keys}</option>)
                                                                                }
                                                                                )}
                                                                            </select>
                                                                        </div>
                                                                        <div className="bottom-button text-right">
                                                                            <button type="submit" className="btn btn-success m-0">Save</button>
                                                                        </div>
                                                                    </form>}

                                                                {this.state.selectedFeature === "informationGain" &&
                                                                    <form className="contentForm" onSubmit={this.featureSaveHandler}>
                                                                        <div className="form-group">
                                                                            <label className="form-group-label">Target Column: <i className="fas fa-asterisk form-group-required"></i></label>
                                                                            <select className="form-control" id="targetColumn" disabled={this.state.informationGainLoader} required value={this.state.featurePayload.targetColumn} onChange={this.featureFormChangeHandler}>
                                                                                <option value=''>Select</option>
                                                                                {this.state.columnsList.map((column, columnIndex) => {
                                                                                    return (<option key={columnIndex} value={column.keys}>{column.keys}</option>)
                                                                                }
                                                                                )}
                                                                            </select>
                                                                        </div>
                                                                        <div className="form-group">
                                                                            <label className="form-group-label">Target Column Type :<i className="fas fa-asterisk form-group-required"></i></label>
                                                                            <select className="form-control" id="targetColumnType" disabled={this.state.informationGainLoader} required value={this.state.featurePayload.targetColumnType} onChange={this.featureFormChangeHandler}>
                                                                                <option value='categoricalType'>Categorical Type</option>
                                                                                <option value='regressionType'>Regression Type</option>
                                                                            </select>                                                                        </div>
                                                                        {Object.keys(this.state.informationGainData).length ?
                                                                            <div className="d-flex">
                                                                                <label className="flex-20 pd-l-10">Percent:</label>
                                                                                <p className="flex-20 pd-t-2 fw-700 text-theme">{this.state.featurePayload.k}</p>
                                                                                <div className="flex-60">
                                                                                    <label className="radioLabel pd-0 mr-r-16">1</label>
                                                                                    <Slider
                                                                                        axis="x"
                                                                                        xmin={1}
                                                                                        xmax={this.state.informationGainData.dataKeys.length}
                                                                                        xstep={1}
                                                                                        x={this.state.featurePayload.k}
                                                                                        onChange={({ x }) => { this.SliderChangeHandlerForFeatureSelection(x, "k") }}
                                                                                    />
                                                                                    <label className="radioLabel pd-0 mr-l-16">{this.state.columnsList.length}</label>
                                                                                </div>
                                                                            </div> : null}
                                                                        {Object.keys(this.state.informationGainData).length ? <button type="button" className="btn btn-link" data-toggle="modal" data-target="#informationGainModal" ><i className="far fa-plus"></i>Show Information Gain Data</button> : null}
                                                                        <div className="text-right">
                                                                            <button type="button" disabled={(!this.state.featurePayload.targetColumn || this.state.informationGainLoader)} onClick={() => {
                                                                                let payload = {
                                                                                    dataSource: cloneDeep(this.state.payload.dataSource),
                                                                                    action: {
                                                                                        type: "showStatistics",
                                                                                        actionName: "informationGain",
                                                                                        targetColumn: this.state.featurePayload.targetColumn,
                                                                                        targetColumnType: this.state.featurePayload.targetColumnType
                                                                                    }
                                                                                };
                                                                                this.props.getStatistics(payload, "informationGainLoader");
                                                                                this.setState({
                                                                                    informationGainLoader: true
                                                                                })
                                                                            }} className="btn btn-success m-0">Get Dependant Columns Number</button>
                                                                        </div>
                                                                        <div className="bottom-button text-right">
                                                                            <button type="submit" disabled={!this.state.featurePayload.k || this.state.informationGainLoader} className="btn btn-success m-0">Save</button>
                                                                        </div>
                                                                    </form>}

                                                                {this.state.selectedFeature === "chiSqTest" &&
                                                                    <form className="contentForm" onSubmit={this.featureSaveHandler}>
                                                                        <div className="d-flex">
                                                                            <label className="flex-20 pd-l-10">Percent:</label>
                                                                            <p className="flex-20 fw-700 pd-t-2 text-theme">{this.state.featurePayload.k}</p>
                                                                            <div className="flex-60">
                                                                                <label className="radioLabel pd-0 mr-r-16">1</label>
                                                                                <Slider
                                                                                    axis="x"
                                                                                    xmin={1}
                                                                                    xmax={this.state.columnsList.length}
                                                                                    xstep={1}
                                                                                    x={this.state.featurePayload.k}
                                                                                    onChange={({ x }) => { this.SliderChangeHandlerForFeatureSelection(x, "k") }}
                                                                                />
                                                                                <label className="radioLabel pd-0 mr-l-16">{this.state.columnsList.length}</label>
                                                                            </div>
                                                                        </div>
                                                                        <div className="bottom-button text-right">
                                                                            <button type="submit" className="btn btn-success m-0">Save</button>
                                                                        </div>
                                                                    </form>}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="flex-100">
                                        <div className="ml-item-box" data-toggle="collapse" data-target="#item4">
                                            <h6 className="m-0">Model Building</h6>
                                        </div>
                                        <div className="collapse" id="item4">
                                            <div  >
                                                <div className="d-flex">
                                                    <div className="flex-33">
                                                        <div className="form-section">
                                                            <div className="form-section-header">
                                                                <p className="p-0 m-0">Select Algorithm</p>
                                                            </div>
                                                            <div className="form-section-body">
                                                                <ul className="list-style-none">
                                                                    {this.state.algorithmsList.map((algorithm, index) => {
                                                                        return (
                                                                            <li key={index}>
                                                                                <p data-tip data-for={`alogo${index}`} className={`pd-l-8 ${this.state.payload.algorithm.algorithmName === algorithm.name ? '' : "collapsed"}`} data-toggle="collapse" onClick={() => this.selectAlgorithm("name", algorithm.name)} data-target={`#child${index}`}> <i className="fas fa-caret-right mr-r-5"></i>{algorithm.name}</p>
                                                                                <ReactTooltip id={`alogo${index}`} place="right" type="dark">
                                                                                    <div className="tooltipText">
                                                                                        <p>{algorithm.toolTipText}</p>
                                                                                    </div>
                                                                                </ReactTooltip>
                                                                                <ul className={`list-style-none pd-l-20 ${this.state.payload.algorithm.algorithmName === algorithm.name ? "collapse show" : "collapse"}`} id={`child${index}`} >
                                                                                    {algorithm.children.map((child, childIndex) => {
                                                                                        return (<li key={childIndex}>
                                                                                            <p className="level-one" onClick={() => this.selectAlgorithm("type", child.value)}><img src={child.url} />{child.name}</p>
                                                                                        </li>)
                                                                                    })}
                                                                                </ul>
                                                                            </li>
                                                                        )
                                                                    })}
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="flex-33">
                                                        <div className="selected-item-box">
                                                            <div className="selected-item d-flex">
                                                                <i className="fas fa-times-circle cancel text-content"></i>
                                                                <div className="d-flex selected-info-box">
                                                                    <img src="https://content.iot83.com/m83/mlStudio/algo.png" />
                                                                    <div className=" d-flex selected-info-name">
                                                                        <div className="fx-b50 selected-info-image">
                                                                            <img src="https://content.iot83.com/m83/mlStudio/XGBoostLogo.png" />
                                                                        </div>
                                                                        <div className="flex-50">
                                                                            <p className="m-0 pd-l-10"><strong className="fw-700">{this.state.payload.algorithm.algorithmName}</strong></p>
                                                                            <p className="m-0 pd-l-10 fw-600">{this.state.payload.algorithm.algorithmType}</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="flex-33">
                                                        <div className="form-section">
                                                            {this.state.payload.algorithm.algorithmType ?
                                                                <div className="form-section-header">
                                                                    <span className="fw-600">Parameters <i className="fas fa-greater-than mr-r-10 mr-l-10 f-12"></i></span><span className="fw-600">{this.state.payload.algorithm.algorithmName}</span><i className="fas fa-greater-than mr-r-10 mr-l-10 f-12"></i><span className="fw-600">{this.state.payload.algorithm.algorithmType === "linear-learner" ? "Linear-Learner" : this.state.payload.algorithm.algorithmType}</span>
                                                                </div> :
                                                                <div className="form-section-header">
                                                                    <span className="fw-600">Parameters</span>
                                                                </div>}
                                                             _____________X-Boost__________
                                                            <div className="form-section-body">
                                                                {
                                                                    !this.state.payload.algorithm.algorithmType &&
                                                                    <h5 className="text-center f-13">Select Atleast One Algorithm</h5>
                                                                }

                                                                {this.state.payload.algorithm.algorithmType === "XgBoost" &&
                                                                    <form className="contentForm">
                                                                        <div className="form-group">
                                                                            <label className="form-group-label">Training Job Name : <i className="fas fa-asterisk form-group-required"></i></label>
                                                                            <input className="form-control" type="text" id="trainingJobName" value={this.state.payload.algorithm.trainingJobName} onChange={this.algorithmFormHandler} placeholder="indexes" />
                                                                        </div>
                                                                        <p className="text-primary pd-l-10"><strong>HyperParameters</strong></p>
                                                                        <div className="form-group">
                                                                            <label className="form-group-label">Objective : <i className="fas fa-asterisk form-group-required"></i></label>
                                                                            <input type="text" id="objective" className="form-control" defaultValue={this.state.payload.algorithm.hyperparams.objective} readOnly={true} placeholder="indexes" />
                                                                        </div>
                                                                        <div className="form-group">
                                                                            <label className="form-group-label">ETA : <i className="fas fa-asterisk form-group-required"></i></label>
                                                                            <input type="number" id="eta" className="form-control" value={this.state.payload.algorithm.hyperparams.eta} onChange={this.algorithmFormHandler} placeholder="indexes" />
                                                                        </div>
                                                                        <div className="form-group">
                                                                            <label className="form-group-label">subsample :</label>
                                                                            <input type="number" id="subsample" className="form-control" value={this.state.payload.algorithm.hyperparams.subsample} onChange={this.algorithmFormHandler} placeholder="indexes" />
                                                                        </div>
                                                                        <div className="form-group">
                                                                            <label className="form-group-label">num_round : <i className="fas fa-asterisk form-group-required"></i></label>
                                                                            <input type="text" id="num_round" className="form-control" value={this.state.payload.algorithm.hyperparams.num_round} onChange={this.algorithmFormHandler} placeholder="indexes" />
                                                                        </div>
                                                                        <div className="d-flex">
                                                                            <label className="flex-20 pd-l-10">Train Value:</label>
                                                                            <p className="flex-20 fw-700 pd-t-2 text-theme">{this.state.payload.algorithm.hyperparams.trainSplitValue}</p>
                                                                            <div className="flex-60">
                                                                                <label className="radioLabel pd-0 mr-r-16">0</label>
                                                                                <Slider
                                                                                    axis="x"
                                                                                    xmin={0}
                                                                                    xmax={1}
                                                                                    xstep={0.1}
                                                                                    x={this.state.payload.algorithm.hyperparams.trainSplitValue}
                                                                                    onChange={({ x }) => { this.SliderChangeHandlerForAlgorithm(x, "trainSplitValue") }}
                                                                                />
                                                                                <label className="radioLabel pd-0 mr-l-16">1</label>
                                                                            </div>
                                                                        </div>
                                                                        <div className="d-flex">
                                                                            <label className="flex-20 pd-l-10">Test Value:</label>
                                                                            <p className="flex-20 fw-700 pd-t-2 text-theme">{this.state.payload.algorithm.hyperparams.testSplitValue}</p>
                                                                            <div className="flex-60">
                                                                                <label className="radioLabel pd-0 mr-r-16">0</label>
                                                                                <Slider
                                                                                    axis="x"
                                                                                    xmin={0}
                                                                                    xmax={1}
                                                                                    xstep={0.1}
                                                                                    x={this.state.payload.algorithm.hyperparams.testSplitValue}
                                                                                    onChange={({ x }) => { this.SliderChangeHandlerForAlgorithm(x, "testSplitValue") }}
                                                                                />
                                                                                <label className="radioLabel pd-0 mr-l-16">1</label>
                                                                            </div>
                                                                        </div>
                                                                    </form>}

                                                                 _____________Linear Learner__________
                                                                {this.state.payload.algorithm.algorithmType === "linear-learner" &&
                                                                    <form className="contentForm">
                                                                        <div className="form-group">
                                                                            <label className="form-group-label">Training Job Name : <i className="fas fa-asterisk form-group-required"></i></label>
                                                                            <input type="text" id="trainingJobName" className="form-control" value={this.state.payload.algorithm.trainingJobName} onChange={this.algorithmFormHandler} placeholder="indexes" />
                                                                        </div>
                                                                        <p className="text-primary pd-l-10"><strong>HyperParameters</strong></p>
                                                                        <div className="form-group">
                                                                            <label className="form-group-label">Feature_Dim : <i className="fas fa-asterisk form-group-required"></i></label>
                                                                            <input type="text" id="feature_dim" className="form-control" value={this.state.payload.algorithm.hyperparams.feature_dim} onChange={this.algorithmFormHandler} placeholder="indexes" />
                                                                        </div>
                                                                        <div className="form-group">
                                                                            <label className="form-group-label">predictor_type : <i className="fas fa-asterisk form-group-required"></i></label>
                                                                            <select className="form-control" id="predictor_type" value={this.state.payload.algorithm.hyperparams.predictor_type} onChange={this.algorithmFormHandler}>
                                                                                <option value='binary_classifier'>Binary Classifier</option>
                                                                                <option value='multiclass_classifier'>Multi Class Classifier</option>
                                                                            </select>
                                                                        </div>
                                                                        <div className="form-group">
                                                                            <label className="form-group-label">Target Column : <i className="fas fa-asterisk form-group-required"></i></label>
                                                                            <input className="form-control" type="text" id="target" value={this.state.payload.algorithm.hyperparams.target} onChange={this.algorithmFormHandler} placeholder="indexes" />
                                                                        </div>
                                                                        <div className="d-flex">
                                                                            <label className="flex-20 pd-l-10">Train Value:</label>
                                                                            <p className="flex-20 fw-700 pd-t-2 text-theme">{this.state.payload.algorithm.hyperparams.trainSplitValue}</p>
                                                                            <div className="flex-60">
                                                                                <label className="radioLabel pd-0 mr-r-16">0</label>
                                                                                <Slider
                                                                                    axis="x"
                                                                                    xmin={0}
                                                                                    xmax={1}
                                                                                    xstep={0.1}
                                                                                    x={this.state.payload.algorithm.hyperparams.trainSplitValue}
                                                                                    onChange={({ x }) => { this.SliderChangeHandlerForAlgorithm(x, "testSplitValue") }}
                                                                                />
                                                                                <label className="radioLabel pd-0 mr-l-16">1</label>
                                                                            </div>
                                                                        </div>
                                                                        <div className="d-flex">
                                                                            <label className="flex-20 pd-l-10">Test Value:</label>
                                                                            <p className="flex-20 fw-700 pd-t-2 text-theme">{this.state.payload.algorithm.hyperparams.testSplitValue}</p>
                                                                            <div className="flex-60">
                                                                                <label className="radioLabel pd-0 mr-r-16">0</label>
                                                                                <Slider
                                                                                    axis="x"
                                                                                    xmin={0}
                                                                                    xmax={1}
                                                                                    xstep={0.1}
                                                                                    x={this.state.payload.algorithm.hyperparams.testSplitValue}
                                                                                    onChange={({ x }) => { this.SliderChangeHandlerForAlgorithm(x, "testSplitValue") }}
                                                                                />
                                                                                <label className="radioLabel pd-0 mr-l-16">1</label>
                                                                            </div>
                                                                        </div>
                                                                    </form>}

                                                                 _____________K-means__________
                                                                {this.state.payload.algorithm.algorithmType === "K-Means" &&
                                                                    <form className="contentForm">
                                                                        <div className="form-group">
                                                                            <label className="form-group-label">Training Job Name : <i className="fas fa-asterisk form-group-required"></i></label>
                                                                            <input type="text" id="trainingJobName" className="form-control" value={this.state.payload.algorithm.trainingJobName} onChange={this.algorithmFormHandler} placeholder="indexes" />
                                                                        </div>
                                                                        <p className="text-primary pd-l-10"><strong>HyperParameters</strong></p>
                                                                        <div className="form-group">
                                                                            <label className="form-group-label">Feature_Dim :</label>
                                                                            <input type="text" id="feature_dim" className="form-control" value={this.state.payload.algorithm.hyperparams.feature_dim} onChange={this.algorithmFormHandler} placeholder="indexes" />
                                                                        </div>
                                                                        <div className="form-group">
                                                                            <label className="form-group-label">K : <i className="fas fa-asterisk form-group-required"></i></label>
                                                                            <input type="text" id="k" className="form-control" value={this.state.payload.algorithm.hyperparams.k} onChange={this.algorithmFormHandler} placeholder="indexes" />
                                                                        </div>
                                                                    </form>}

                                                                 _____________RCF__________
                                                                {this.state.payload.algorithm.algorithmType === "RandomCutForest" &&
                                                                    <form className="contentForm">
                                                                        <div className="form-group">
                                                                            <label className="form-group-label">Training Job Name : <i className="fas fa-asterisk form-group-required"></i></label>
                                                                            <input type="text" id="trainingJobName" className="form-control" value={this.state.payload.algorithm.trainingJobName} onChange={this.algorithmFormHandler} placeholder="indexes" />
                                                                        </div>
                                                                        <p className="text-primary pd-l-10"><strong>HyperParameters</strong></p>
                                                                        <div className="form-group">
                                                                            <label className="form-group-label">Feature_Dim :</label>
                                                                            <input type="text" id="feature_dim" className="form-control" value={this.state.payload.algorithm.hyperparams.feature_dim} onChange={this.algorithmFormHandler} placeholder="indexes" />
                                                                        </div>
                                                                        <div className="form-group">
                                                                            <label className="form-group-label">NUM_SAMPLES_PER_TREE :</label>
                                                                            <input type="text" id="num_samples_per_tree" className="form-control" value={this.state.payload.algorithm.hyperparams.num_samples_per_tree} onChange={this.algorithmFormHandler} placeholder="indexes" />
                                                                        </div>
                                                                        <div className="form-group">
                                                                            <label className="form-group-label">NUM_TREE : <i className="fas fa-asterisk form-group-required"></i></label>
                                                                            <input type="text" id="num_trees" className="form-control" value={this.state.payload.algorithm.hyperparams.num_trees} onChange={this.algorithmFormHandler} placeholder="indexes" />
                                                                        </div>
                                                                        <div className="form-group">
                                                                            <label className="form-group-label">TRAIN_INSTANCE_COUNT : <i className="fas fa-asterisk form-group-required"></i></label>
                                                                            <input type="text" id="train_instance_count" className="form-control" defaultValue={this.state.payload.algorithm.hyperparams.train_instance_count} placeholder="indexes" />
                                                                        </div>
                                                                    </form>}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            {/* <div className="pageBreadcrumb">
                                <div className="row">
                                    <div className="col-8">
                                        <p>
                                            <span className="previousPage" onClick={() => { this.props.history.push("/mlStudio"); }}>ML Experiments</span>
                                            <span className="active">Add Experiment</span>
                                        </p>
                                    </div>
                                    <div className="col-4 text-right">
                                        <div className="flex h-100 justify-content-end align-items-center">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="outerBox">
                                <div className="mlStudioBox">
                                    <div className="mlStudioFormHeader flex">
                                        <div className="flex-item fx-b40 pd-r-5">
                                            <div className="form-group">
                                                <input className="form-control" id="name" type="text" value={this.state.payload.name} onChange={this.formChangeHandler} placeholder="label column" />
                                                <label className="form-group-label">Experiment Name<span className="requiredMark"><i className="fas fa-star-of-life"></i></span></label>
                                            </div>
                                        </div>
                                        <div className="flex-item fx-b60 pd-l-5 pd-r-5">
                                            <div className="form-group">
                                                <input className="form-control" id="description" type="text" value={this.state.payload.description} onChange={this.formChangeHandler} placeholder="label column" />
                                                <label className="form-group-label">Description<span className="requiredMark"><i className="fas fa-star-of-life"></i></span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="mlStudioForm flex"> */}
                            {/* _____________Data Config_____________ */}
                            {/* <div className="flex-item fx-b100 mlStudioFormContent">
                                            <div className="mlStudioItemBox" onClick={() => { $("#item1").slideToggle() }} >
                                                <p>Data Configuration</p>
                                                <div className="btn-group">
                                                    {this.state.dataCorrelationLoader ?
                                                        <span className="dataCorrelationLoader"></span> :
                                                        <React.Fragment>
                                                            <button data-tip data-for="dataCorrelation" className="btn btn-link" name="dataCorrelation" disabled={!this.state.payload.dataSource.dirId} onClick={this.getStatistics}>Data Correlation</button>
                                                            <ReactTooltip id="dataCorrelation" place="left" type="dark">
                                                                <div className="tooltipText"><p>Statistical technique to show how strongly pairs of variables are related</p></div>
                                                            </ReactTooltip>
                                                        </React.Fragment>}
                                                    {this.state.showStatisticalReportLoader ?
                                                        <span className="showStatisticalReportLoader"></span> :
                                                        <React.Fragment>
                                                            <button className="btn btn-link" data-tip data-for="showStatisticalReport" name="showStatisticalReport" disabled={!this.state.payload.dataSource.dirId} onClick={this.getStatistics}>Show Statistical Report</button>
                                                            <ReactTooltip id="showStatisticalReport" place="left" type="dark">
                                                                <div className="tooltipText"><p>Genrates report for Exploratory data analysis</p></div>
                                                            </ReactTooltip></React.Fragment>}
                                                    {this.state.showMissingValuesLoader ?
                                                        <span className="showMissingValuesLoader"></span> :
                                                        <React.Fragment>
                                                            <button className="btn btn-link" data-tip data-for="showMissingValues" name="showMissingValues" disabled={!this.state.payload.dataSource.dirId} onClick={this.getStatistics}>Show All N/a</button>
                                                            <ReactTooltip id="showMissingValues" place="left" type="dark">
                                                                <div className="tooltipText"><p>Missing values in entire dataset</p></div>
                                                            </ReactTooltip>
                                                        </React.Fragment>}
                                                    {this.state.showSampleDataLoader ?
                                                        <span className="showSampleDataLoader"></span> :
                                                        <React.Fragment>
                                                            <button className="btn btn-link" data-tip data-for="showSampleData" name="showSampleData" disabled={!this.state.payload.dataSource.dirId} onClick={this.getStatistics}>Show Sample Data</button>
                                                            <ReactTooltip id="showSampleData" place="left" type="dark">
                                                                <div className="tooltipText"><p>Display Sample Data</p></div>
                                                            </ReactTooltip></React.Fragment>}
                                                    <button className="btn btn-link" data-tip data-for="featureImportance" name="featureImportance" disabled={!this.state.payload.dataSource.dirId} onClick={(event) => {
                                                        event.stopPropagation();
                                                        this.setState({
                                                            featureImportanceLoader: false
                                                        })
                                                        $(`#${event.target.name}Modal`).modal()
                                                    }}>Feature Importance</button>
                                                    <ReactTooltip id="featureImportance" place="left" type="dark">
                                                        <div className="tooltipText"><p>Feature importance refers to techniques that assign a score to input features based on how useful they are at predicting a target variable.</p></div>
                                                    </ReactTooltip>
                                                </div> */}
                            {/*<div className="btn-group">*/}
                            {/*    <span className="dataCorrelationLoader"></span>*/}
                            {/*    <span className="showStatisticalReportLoader"></span>*/}
                            {/*    <span className="showMissingValuesLoader"></span>*/}
                            {/*    <span className="showSampleDataLoader"></span>*/}
                            {/*</div>*/}
                            {/* </div>
                                            <div className="mlStudioItemBoxCollapsed collapse show" id="item1">
                                                <div>
                                                    <div className="flex">
                                                        <div className="flex-item fx-b40">
                                                            <div className="formSection">
                                                                <div className="formSectionHeader">
                                                                    <p className="p-0 m-0">Select Source File</p>
                                                                </div>
                                                                <div className="formSectionBody">
                                                                    <ul className="connectorsList hierarchyList">
                                                                        {this.state.dataConnectorsList.map((dataConnector, index) => {
                                                                            return (
                                                                                <li className={this.props.match.params.id ? 'disabledList' : ''} key={index}>
                                                                                    <p className="level1 collapsed" data-toggle="collapse" data-target="#child1" onClick={() => this.getConnectorsByCategory(dataConnector.connector)}> <i className="fas fa-caret-right"></i>{dataConnector.displayName}{/*<img src={`https://content.iot83.com/m83/dataConnectors/${dataConnector.imageName}`} /></p>
                                                                                    <ul className={this.state.payload.dataSource.connectorCategory === dataConnector.connector ? 'collapse show' : "collapse"} id="child1" >
                                                                                        {this.state.connectorsList.map((connector, connectorIndex) => {
                                                                                            return (
                                                                                                <li key={connectorIndex}>
                                                                                                    <p className="level2 collapsed" data-toggle="collapse" onClick={() => this.dataSourceChangeHandler("connector", connector.id)}><i className="fas fa-caret-right"></i>{connector.name}</p>
                                                                                                    <ul className={this.state.payload.dataSource.connectorId === connector.id ? "collapse show" : "collapse "} id="child2" >
                                                                                                        {connector.properties.list.map((bucket, bucketIndex) => {
                                                                                                            return (
                                                                                                                <li key={bucketIndex}>
                                                                                                                    <p className="level3 collapsed" onClick={() => this.dataSourceChangeHandler("bucket", bucket.bucketId)} data-toggle="collapse"><i className="fas fa-caret-right"></i>{bucket.name}</p>
                                                                                                                    <ul className={this.state.payload.dataSource.bucketId === bucket.bucketId ? "collapse show" : "collapse "} id="child3" >
                                                                                                                        {bucket.dataDirs.map((dir, dirIndex) => {
                                                                                                                            return (
                                                                                                                                <p key={dirIndex} className="level3 pd-l-80" onClick={() => this.dataSourceChangeHandler("dir", dir.dirId)}><i className="fas fa-circle"></i>{dir.name}</p>
                                                                                                                            )
                                                                                                                        })}
                                                                                                                    </ul>
                                                                                                                </li>)
                                                                                                        })}
                                                                                                    </ul>
                                                                                                </li>)
                                                                                        })}
                                                                                    </ul>
                                                                                </li>)
                                                                        })}
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="flex-item fx-b60">
                                                            {!this.state.payload.dataSource.dirId ?
                                                                <div className="dropzoneBox">
                                                                    <div className="dropzone">
                                                                        <h5>No Source File Selected
                                                                            <div className="icon">
                                                                                <img src="https://content.iot83.com/m83/mlStudio/feature.png" />
                                                                            </div>
                                                                        </h5>
                                                                    </div>
                                                                </div> :
                                                                <div className="selectedItemBox">
                                                                    <div className="selectedItem ">
                                                                        <i className="fas fa-times-circle cancel"></i>
                                                                        <div className="flex info">
                                                                            <img src="https://content.iot83.com/m83/mlStudio/algo.png" />
                                                                            <div className="fx-b50 selectedItemName">
                                                                                <img src="https://content.iot83.com/m83/mlStudio/amazon.png" />
                                                                            </div>
                                                                            {directoryList.map((dir, index) => <div className="fx-b50" key={index}>
                                                                                <p className="m-0"><strong>{this.state.payload.dataSource.connectorCategory}</strong></p>
                                                                                <p className="m-0">{dir.connector}</p>
                                                                                <p className="m-0">{dir.bucket}</p>
                                                                                <p className="m-0">{dir.name}</p>
                                                                            </div>
                                                                            )}
                                                                        </div>
                                                                    </div>
                                                                </div>}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> */}

                            {/* _____________cleaning and transformation_____________ */}
                            {/* <div className="flex-item fx-b100 mlStudioFormContent">
                                            <div className="mlStudioItemBox" onClick={() => { $("#item2").slideToggle() }}>
                                                <p>Cleaning & Transformation</p>
                                                <div className="btn-group">
                                                    {this.state.dataCorrelationWithCleaningAndTransformLoader ?
                                                        <span className="dataCorrelationLoader"></span> :
                                                        <React.Fragment>
                                                            <button className="btn btn-link" data-tip data-for="dataCorrelationWithCleaningAndTransform" name="dataCorrelationWithCleaningAndTransform" disabled={!this.state.payload.cleaningAndTransformConfig.operations[0].actions.length} onClick={(e) => this.getStatistics(e, true)}>Data Correlation</button>
                                                            <ReactTooltip id="dataCorrelationWithCleaningAndTransform" place="left" type="dark">
                                                                <div className="tooltipText"><p>Statistical technique to show how strongly pairs of variables are related</p></div>
                                                            </ReactTooltip></React.Fragment>}
                                                    {this.state.showStatisticalReportWithCleaningAndTransformLoader ?
                                                        <span className="showStatisticalReportLoader"></span> :
                                                        <React.Fragment>
                                                            <button className="btn btn-link" data-tip data-for="showStatisticalReportWithCleaningAndTransform" name="showStatisticalReportWithCleaningAndTransform" disabled={!this.state.payload.cleaningAndTransformConfig.operations[0].actions.length} onClick={(e) => this.getStatistics(e, true)}>Show Statistical Report</button>
                                                            <ReactTooltip id="showStatisticalReportWithCleaningAndTransform" place="left" type="dark">
                                                                <div className="tooltipText"><p>Genrates report for Exploratory data analysis</p></div>
                                                            </ReactTooltip></React.Fragment>}
                                                    {this.state.showMissingValuesWithCleaningAndTransformLoader ?
                                                        <span className="showMissingValuesLoader"></span> :
                                                        <React.Fragment>
                                                            <button className="btn btn-link" data-tip data-for="showMissingValuesWithCleaningAndTransform" name="showMissingValuesWithCleaningAndTransform" disabled={!this.state.payload.cleaningAndTransformConfig.operations[0].actions.length} onClick={(e) => this.getStatistics(e, true)}>Show All N/a</button>
                                                            <ReactTooltip id="showMissingValuesWithCleaningAndTransform" place="left" type="dark">
                                                                <div className="tooltipText"><p>Missing values in entire dataset</p></div>
                                                            </ReactTooltip></React.Fragment>}
                                                    {this.state.showSampleDataWithCleaningAndTransformLoader ?
                                                        <span className="showSampleDataLoader"></span> :
                                                        <React.Fragment>
                                                            <button className="btn btn-link" data-tip data-for="showSampleData" name="showSampleDataWithCleaningAndTransform" disabled={!this.state.payload.cleaningAndTransformConfig.operations[0].actions.length} onClick={(e) => this.getStatistics(e, true)}>Show Sample Data</button>
                                                            <ReactTooltip id="showSampleDataWithCleaningAndTransform" place="left" type="dark">
                                                                <div className="tooltipText"><p>Display Sample Data</p></div>
                                                            </ReactTooltip></React.Fragment>}
                                                </div>
                                            </div>
                                            <div className=" mlStudioItemBoxCollapsed collapse" id="item2">
                                                <div  >
                                                    <div className="flex">
                                                        <div className="flex-item fx-b33">
                                                            <div className="formSection">
                                                                <div className="formSectionHeader">
                                                                    <p className="p-0 m-0">Select Tools</p>
                                                                </div>
                                                                <div className="formSectionBody"> */}
                            {/* <div className="searchBox input-group mb-3">
                                                            <input type="search" className="form-control" placeholder="&#128269; Search...." />
                                                        </div> */}
                            {/* <ul className="featureList flex">
                                                                        {this.state.tools.map((tool, index) => {
                                                                            return (
                                                                                <li key={index} className="fx-b50" draggable onDragStart={() => this.onToolDrag(tool.name)}><img src={tool.url} />{tool.displayName}</li>)
                                                                        })}
                                                                    </ul>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div className="flex-item fx-b33">
                                                            <div className="formSection addFeature">
                                                                <div className="formSectionHeader position-relative">
                                                                    <p className="p-0 m-0">Add Columns</p>
                                                                    <button type="button" className="btn btn-primary" onClick={() => this.addRemoveColumns("add", null)}>
                                                                        <i className="fas fa-plus"></i>
                                                                    </button>
                                                                </div>
                                                                <div className="formSectionBody">
                                                                    {this.state.payload.cleaningAndTransformConfig.operations.map((operation, operationIndex) => {
                                                                        return (
                                                                            <div className="columnBox" key={operationIndex}>
                                                                                <button className={this.state.payload.cleaningAndTransformConfig.operations.length === 1 ? "btn btn-transparent btn-transparent-danger btn-disabled" : "btn btn-transparent btn-transparent-danger"} disabled={this.state.payload.cleaningAndTransformConfig.operations.length === 1} onClick={() => this.addRemoveColumns("delete", operationIndex)}><i className="far fa-trash-alt"></i></button>
                                                                                <div className="form-group">
                                                                                    <select className="form-control" id="colName" required value={operation.colName} onChange={(e) => this.columnChangeHandler(e, operationIndex)}>
                                                                                        <option value=''>Select</option>
                                                                                        {this.state.columnsList.map((column, columnIndex) => {
                                                                                            return (<option key={columnIndex} value={column.keys}>{column.keys}</option>)
                                                                                        }
                                                                                        )}
                                                                                        {this.state.columnsList.length && <option value='ForAll'>Select Multi Columns</option>}
                                                                                    </select>
                                                                                    <label className="form-group-label">Column :<span className="requiredMark"><i
                                                                                        className="fas fa-star-of-life"></i></span>
                                                                                    </label>
                                                                                </div>
                                                                                <ul className="addFeatureList">
                                                                                    <React.Fragment>
                                                                                        {operation.actions.map((action, actionIndex) => {
                                                                                            let url = this.state.tools.find(tool => tool.name === action.actionName).url
                                                                                            let displayName = this.state.tools.find(tool => tool.name === action.actionName).displayName
                                                                                            return (
                                                                                                <li key={actionIndex} className={action.isCompleted ? this.state.actionIndex === actionIndex && this.state.operationIndex === operationIndex ? "active completed" : "unactive completed" : this.state.actionIndex === actionIndex && this.state.operationIndex === operationIndex ? "active" : "unactive"}>
                                                                                                    <p onClick={(e) => { this.onActionSelect(e, operationIndex, actionIndex, action.actionName) }}> <img src={url} />{displayName}</p><button className="btn btn-transparent btn-transparent-danger" onClick={(e) => this.deleteAction(e, operationIndex, actionIndex)}><i className="fas fa-trash-alt"></i></button>
                                                                                                </li>
                                                                                            )
                                                                                        })}
                                                                                        <li className="dropzoneBox pd-r-10" onDrop={(e) => this.onToolDrop(e, operationIndex)} onDragOver={(event) => this.allowDrop(event)} >
                                                                                            <div className="dropzone activeDropzone">
                                                                                                <h5 className="p-0">Drag and Drop Feature Here</h5>
                                                                                            </div>
                                                                                        </li>
                                                                                    </React.Fragment>
                                                                                </ul>
                                                                            </div>)
                                                                    })}
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="flex-item fx-b33">
                                                            <div className="formSection">
                                                                {this.state.selectedAction ?
                                                                    <div className="formSectionHeader">
                                                                        <span>Properties <i className="fas fa-greater-than mr-r-10 mr-l-10"></i></span><span>Transformation</span><i className="fas fa-greater-than mr-r-10 mr-l-10"></i><span>{this.state.tools.find(tool => tool.name === this.state.selectedAction).displayName}</span>
                                                                    </div> :
                                                                    <div className="formSectionHeader">
                                                                        <span>Properties <i className="fas fa-greater-than mr-r-10 mr-l-10"></i></span><span>Transformation</span>
                                                                    </div>}
                                                                <div className="formSectionBody">
                                                                    {
                                                                        this.state.selectedAction === '' &&
                                                                        <h5 className="selectMessage">Select Atleast One Tool</h5>
                                                                    } */}
                            {/* ________________ Trimming ________________ */}
                            {/* {this.state.selectedAction === "trimming" &&
                                                                        <form className="contentForm" onSubmit={this.actionSaveHandler}>
                                                                            <div className="form-group">
                                                                                <div className="flex">
                                                                                    <label className="flex-item fx-b30 pd-l-10">Column Name:</label>
                                                                                    <div className="flex-item fx-b25">
                                                                                        <label className="radioLabel">Left
                                                                                <input name="type" type="radio" value="left" checked={this.state.formPayload.type === "left"} onChange={this.transformationFormChangeHandler} />
                                                                                            <span className="radioMark" />
                                                                                        </label>
                                                                                    </div>
                                                                                    <div className="flex-item fx-b25">
                                                                                        <label className="radioLabel">Right
                                                                                <input name="type" type="radio" value="right" checked={this.state.formPayload.type === "right"} onChange={this.transformationFormChangeHandler} />
                                                                                            <span className="radioMark" />
                                                                                        </label>
                                                                                    </div>
                                                                                    <div className="flex-item fx-b20">
                                                                                        <label className="radioLabel">All
                                                                                <input name="type" type="radio" value="all" checked={this.state.formPayload.type === "all"} onChange={this.transformationFormChangeHandler} />
                                                                                            <span className="radioMark" />
                                                                                        </label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div className="bottomButton text-right">
                                                                                <button type="submit" className="btn btn-success m-0">Save</button>
                                                                            </div>
                                                                        </form>} */}


                            {/* ________________ Padding ________________ */}
                            {/* {this.state.selectedAction === "padding" &&
                                                                        <form className="contentForm" onSubmit={this.actionSaveHandler}  >
                                                                            <div className="form-group">
                                                                                <input className="form-control" id="length" value={this.state.formPayload.length} type="number" onChange={this.transformationFormChangeHandler} placeholder="indexes" />
                                                                                <label className="form-group-label">Length :<span className="requiredMark"><i
                                                                                    className="fas fa-star-of-life"></i></span></label>
                                                                            </div>
                                                                            <div className="form-group">
                                                                                <div className="flex">
                                                                                    <label className="flex-item fx-b40 pd-l-10">Type:</label>
                                                                                    <div className="flex-item fx-b30">
                                                                                        <label className="radioLabel">Left
                                                                                            <input name="type" type="radio" value="left" checked={this.state.formPayload.type === "left"} onChange={this.transformationFormChangeHandler} />
                                                                                            <span className="radioMark" />
                                                                                        </label>
                                                                                    </div>
                                                                                    <div className="flex-item fx-b30">
                                                                                        <label className="radioLabel">Right
                                                                                            <input name="type" type="radio" value="right" checked={this.state.formPayload.type === "right"} onChange={this.transformationFormChangeHandler} />
                                                                                            <span className="radioMark" />
                                                                                        </label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div className="form-group">
                                                                                <input className="form-control" id="padWith" value={this.state.formPayload.padWith} type="number" onChange={this.transformationFormChangeHandler} placeholder="indexes" />
                                                                                <label className="form-group-label">Bandwidth :<span className="requiredMark"><i
                                                                                    className="fas fa-star-of-life"></i></span></label>
                                                                            </div>
                                                                            <div className="bottomButton text-right">
                                                                                <button type="submit" className="btn btn-success m-0">Save</button>
                                                                            </div> */}
                            {/* </form>} */}

                            {/* ________________ Fill Null value ________________ */}
                            {/* {this.state.selectedAction === "fillNullValues" &&
                                                                        <form className="contentForm" onSubmit={this.actionSaveHandler}>
                                                                            <div className="flex">
                                                                                <label className="flex-item fx-b40 pd-l-10">Type:</label>
                                                                                <div className="flex-item fx-b30">
                                                                                    <label className={this.state.payload.cleaningAndTransformConfig.operations[this.state.operationIndex].colType === "String" ? "radioLabel radioDisabled" : "radioLabel"}>Mean
                                                                                        <input name="type" type="radio" value="mean" disabled={this.state.payload.cleaningAndTransformConfig.operations[this.state.operationIndex].colType === "String"} checked={this.state.formPayload.type === "mean"} onChange={this.transformationFormChangeHandler} />
                                                                                        <span className="radioMark" />
                                                                                    </label>
                                                                                </div>
                                                                                <div className="flex-item fx-b30">
                                                                                    <label className="radioLabel">Mode
                                                                                        <input name="type" type="radio" value="mode" checked={this.state.formPayload.type === "mode"} onChange={this.transformationFormChangeHandler} />
                                                                                        <span className="radioMark" />
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                            <div className="bottomButton text-right">
                                                                                <button type="submit" className="btn btn-success m-0">Save</button>
                                                                            </div>
                                                                        </form>}

                                                                    {/* ________________ Handling All N/A ________________ */}
                            {/* {this.state.selectedAction === "treatAllMissingValues" &&
                                <p className='handlingAll'>Handling All doesn't require any parameter</p>
                            } */}

                            {/* ________________ Unique Column ________________ */} {/* ________________ Quassi Constant  ________________ */}
                            {/* {(this.state.selectedAction === "uniqueColumn" || this.state.selectedAction === "quasiConstant") &&
                                                                        <form className="contentForm" onSubmit={this.actionSaveHandler}>
                                                                            <div className="form-group">
                                                                                <div className="flex rangeSliderBox">
                                                                                    <label className="flex-item fx-b20 pd-l-10">Tolerance:</label>
                                                                                    <p className="flex-item fx-b20">{this.state.formPayload.tolerance}</p>
                                                                                    <div className="flex-item fx-b60">
                                                                                        <label className="radioLabel pd-0 mr-r-16">0</label>
                                                                                        <Slider
                                                                                            axis="x"
                                                                                            xmin={0}
                                                                                            xmax={1}
                                                                                            xstep={0.01}
                                                                                            x={this.state.formPayload.tolerance}
                                                                                            onChange={({ x }) => { this.SliderChangeHandler(x) }}
                                                                                        />
                                                                                        <label className="radioLabel pd-0 mr-l-16">1</label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div className="bottomButton text-right">
                                                                                <button type="submit" className="btn btn-success m-0">Save</button>
                                                                            </div>
                                                                        </form>}  */}

                            {/* ________________ Drop Column  ________________ */}
                            {/* {this.state.selectedAction === "dropColumn" &&
                                                                        <p className='handlingAll'>Drop Column doesn't require any parameter</p>
                                                                    } */}

                            {/* ________________ Drop Duplicate  ________________ */}
                            {/* {this.state.selectedAction === "dropDuplicates" &&
                                                                        <p className='handlingAll'>Drop Duplicate doesn't require any parameter</p>
                                                                    } */}

                            {/* ________________ Regrex Replace  ________________ */}
                            {/* {this.state.selectedAction === "regrexReplace" &&
                                                                        <form className="contentForm" onSubmit={this.actionSaveHandler}>
                                                                            <div className="form-group">
                                                                                <input className="form-control" type="text" required id="replaceExcept" value={this.state.formPayload.replaceExcept} onChange={this.transformationFormChangeHandler} placeholder="indexes" />
                                                                                <label className="form-group-label">Replace Except :<span className="requiredMark"><i
                                                                                    className="fas fa-star-of-life"></i></span></label>
                                                                            </div>
                                                                            <div className="form-group">
                                                                                <input className="form-control" type="text" required id="replaceWith" value={this.state.formPayload.replaceWith} onChange={this.transformationFormChangeHandler} placeholder="indexes" />
                                                                                <label className="form-group-label">Replace With :<span className="requiredMark"><i
                                                                                    className="fas fa-star-of-life"></i></span></label>
                                                                            </div>
                                                                            <div className="bottomButton text-right">
                                                                                <button type="submit" className="btn btn-success m-0">Save</button>
                                                                            </div>
                                                                        </form>} */}

                            {/* ________________ Select Multiple Columns  ________________ */}
                            {/* {this.state.selectedAction === "selectColumn" &&
                                                                        <form className="contentForm" onSubmit={this.actionSaveHandler}>
                                                                            <div className="form-group inheritedForm">
                                                                                <label className="form-label">Select Column :</label>
                                                                                <ReactSelect
                                                                                    value={this.state.selectedInputCols}
                                                                                    onChange={this.handleMultipleColumnsToolChange}
                                                                                    options={this.createOptionsForMultiColumns()}
                                                                                    isMulti={true}
                                                                                />
                                                                            </div>
                                                                            <div className="bottomButton text-right">
                                                                                <button type="submit" className="btn btn-success m-0">Save</button>
                                                                            </div>
                                                                        </form>} */}

                            {/* ________________ Encoding  ________________ */}
                            {/* {this.state.selectedAction === "encoding" &&
                                                                        <form className="contentForm" onSubmit={this.actionSaveHandler}>
                                                                            <div className="inheritedForm">
                                                                                <label className="form-label">Select Encoding :</label>
                                                                                <ReactSelect
                                                                                    value={this.state.selectedEncoder}
                                                                                    onChange={this.handleChangeForReactSelect}
                                                                                    options={this.state.optionsForEncoder}
                                                                                    isMulti={false}
                                                                                    isOptionDisabled={(option) => { return !option.type.includes(this.state.payload.cleaningAndTransformConfig.operations[this.state.operationIndex].colType) }}
                                                                                />
                                                                            </div> */}


                            {/* (1)_______________Count vectorize________________ */}
                            {/* {this.state.formPayload.encoder === "countVectorize" &&
                                                                                <React.Fragment>
                                                                                    <div className="form-group">
                                                                                        <input className="form-control" type="number" id="minDf" value={this.state.formPayload.minDf} onChange={this.transformationFormChangeHandler} placeholder="indexes" />
                                                                                        <label className="form-group-label">Min DF :<span className="requiredMark"><i
                                                                                            className="fas fa-star-of-life"></i></span></label>
                                                                                    </div>
                                                                                    <div className="form-group">
                                                                                        <input className="form-control" type="number" id="vocabSize" value={this.state.formPayload.vocabSize} onChange={this.transformationFormChangeHandler} placeholder="indexes" />
                                                                                        <label className="form-group-label">Vocab Size :<span className="requiredMark"><i
                                                                                            className="fas fa-star-of-life"></i></span></label>
                                                                                    </div>
                                                                                    <div className="form-group">
                                                                                        <input className="form-control" type="text" id="outputCol" value={this.state.formPayload.outputCol} onChange={this.transformationFormChangeHandler} placeholder="indexes" />
                                                                                        <label className="form-group-label">Output Column :<span className="requiredMark"><i
                                                                                            className="fas fa-star-of-life"></i></span></label>
                                                                                    </div>
                                                                                    <div className="form-group">
                                                                                        <div className="flex">
                                                                                            <label className="flex-item fx-b40 pd-l-10">Drop Original Column:</label>
                                                                                            <div className="flex-item fx-b30">
                                                                                                <label className="radioLabel">True
                                                                                                    <input name="dropColumn" type="radio" value={"true"} checked={this.state.formPayload.dropColumn === "true"} onChange={this.transformationFormChangeHandler} />
                                                                                                    <span className="radioMark" />
                                                                                                </label>
                                                                                            </div>
                                                                                            <div className="flex-item fx-b30">
                                                                                                <label className="radioLabel">False
                                                                                                    <input name="dropColumn" type="radio" value={"false"} checked={this.state.formPayload.dropColumn === "false"} onChange={this.transformationFormChangeHandler} />
                                                                                                    <span className="radioMark" />
                                                                                                </label>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div className="bottomButton text-right">
                                                                                        <button type="submit" className="btn btn-success m-0">Save</button>
                                                                                    </div>
                                                                                </React.Fragment>} */}


                            {/* (2)_________________Onehotencoder________________ */}
                            {/* {this.state.formPayload.encoder === "oneHotEncoder" &&
                                                                                <React.Fragment>
                                                                                    <div className="form-group inheritedForm">
                                                                                        <label className="form-label">Select Column :</label>
                                                                                        <ReactSelect
                                                                                            value={this.state.selectedInputCols}
                                                                                            onChange={(value, type) => this.handleInputColumnChange(value, type, false)}
                                                                                            options={this.createOptionsForReactSelect()}
                                                                                            isMulti={false}
                                                                                        />
                                                                                    </div>
                                                                                    <div className="form-group">
                                                                                        <input className="form-control" type="text" id="outputCols" value={this.state.formPayload.outputCols} onChange={this.transformationFormChangeHandler} placeholder="indexes" />
                                                                                        <label className="form-group-label">Output column :<span className="requiredMark"><i
                                                                                            className="fas fa-star-of-life"></i></span></label>
                                                                                    </div>
                                                                                    <div className="form-group">
                                                                                        <div className="flex">
                                                                                            <label className="flex-item fx-b40 pd-l-10">Drop Original Column:</label>
                                                                                            <div className="flex-item fx-b30">
                                                                                                <label className="radioLabel">True
                                                                                                    <input name="dropColumn" type="radio" value={"true"} checked={this.state.formPayload.dropColumn === "true"} onChange={this.transformationFormChangeHandler} />
                                                                                                    <span className="radioMark" />
                                                                                                </label>
                                                                                            </div>
                                                                                            <div className="flex-item fx-b30">
                                                                                                <label className="radioLabel">False
                                                                                                    <input name="dropColumn" type="radio" value={"false"} checked={this.state.formPayload.dropColumn === "false"} onChange={this.transformationFormChangeHandler} />
                                                                                                    <span className="radioMark" />
                                                                                                </label>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div className="bottomButton text-right">
                                                                                        <button type="submit" className="btn btn-success m-0">Save</button>
                                                                                    </div>
                                                                                </React.Fragment>} */}


                            {/* (3)_________________Vector Assemblers________________ */}
                            {/* {this.state.formPayload.encoder === "vectorAssemblers" &&
                                                                                <React.Fragment>
                                                                                    <div className="form-group inheritedForm">
                                                                                        <label className="form-label">Select Column :</label>
                                                                                        <ReactSelect
                                                                                            value={this.state.selectedInputCols}
                                                                                            onChange={(value, type) => this.handleInputColumnChange(value, type, true)}
                                                                                            options={this.createOptionsForReactSelect()}
                                                                                            isMulti={true}
                                                                                        />
                                                                                    </div>
                                                                                    <div className="form-group">
                                                                                        <input className="form-control" type="text" id="outputCol" value={this.state.formPayload.outputCol} onChange={this.transformationFormChangeHandler} placeholder="indexes" />
                                                                                        <label className="form-group-label">Output column :<span className="requiredMark"><i
                                                                                            className="fas fa-star-of-life"></i></span></label>
                                                                                    </div>
                                                                                    <div className="form-group">
                                                                                        <div className="flex">
                                                                                            <label className="flex-item fx-b40 pd-l-10">Drop Original Column:</label>
                                                                                            <div className="flex-item fx-b30">
                                                                                                <label className="radioLabel">True
                                                                                                    <input name="dropColumn" type="radio" value={"true"} checked={this.state.formPayload.dropColumn === "true"} onChange={this.transformationFormChangeHandler} />
                                                                                                    <span className="radioMark" />
                                                                                                </label>
                                                                                            </div>
                                                                                            <div className="flex-item fx-b30">
                                                                                                <label className="radioLabel">False
                                                                                                    <input name="dropColumn" type="radio" value={"false"} checked={this.state.formPayload.dropColumn === "false"} onChange={this.transformationFormChangeHandler} />
                                                                                                    <span className="radioMark" />
                                                                                                </label>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div className="bottomButton text-right">
                                                                                        <button type="submit" className="btn btn-success m-0">Save</button>
                                                                                    </div>
                                                                                </React.Fragment>} */}


                            {/* (4)_________________ Binaries ________________ */}
                            {/* {this.state.formPayload.encoder === "binaries" &&
                                                                                <React.Fragment>
                                                                                    <div className="form-group inheritedForm">
                                                                                        <label className="form-label">Select Column :</label>
                                                                                        <ReactSelect
                                                                                            value={this.state.selectedInputCols}
                                                                                            onChange={(value, type) => this.handleInputColumnChange(value, type, false)}
                                                                                            options={this.createOptionsForReactSelect()}
                                                                                            isMulti={true}
                                                                                        />
                                                                                    </div>
                                                                                    <div className="form-group">
                                                                                        <div className="flex rangeSliderBox">
                                                                                            <label className="flex-item fx-b20 pd-l-10">Threshold:</label>
                                                                                            <p className="flex-item fx-b20">{this.state.formPayload.threshold}</p>
                                                                                            <div className="flex-item fx-b60">
                                                                                                <label className="radioLabel pd-0 mr-r-16">0</label>
                                                                                                <Slider
                                                                                                    axis="x"
                                                                                                    xmin={0}
                                                                                                    xmax={1}
                                                                                                    xstep={0.01}
                                                                                                    x={this.state.formPayload.threshold}
                                                                                                    onChange={({ x }) => { this.SliderChangeHandler(x, "threshold") }}
                                                                                                />
                                                                                                <label className="radioLabel pd-0 mr-l-16">1</label>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div className="form-group">
                                                                                        <div className="flex">
                                                                                            <label className="flex-item fx-b40 pd-l-10">Drop Original Column:</label>
                                                                                            <div className="flex-item fx-b30">
                                                                                                <label className="radioLabel">True
                                                                                                    <input name="dropColumn" type="radio" value={"true"} checked={this.state.formPayload.dropColumn === "true"} onChange={this.transformationFormChangeHandler} />
                                                                                                    <span className="radioMark" />
                                                                                                </label>
                                                                                            </div>
                                                                                            <div className="flex-item fx-b30">
                                                                                                <label className="radioLabel">False
                                                                                                    <input name="dropColumn" type="radio" value={"false"} checked={this.state.formPayload.dropColumn === "false"} onChange={this.transformationFormChangeHandler} />
                                                                                                    <span className="radioMark" />
                                                                                                </label>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div className="bottomButton text-right">
                                                                                        <button type="submit" className="btn btn-success m-0">Save</button>
                                                                                    </div>
                                                                                </React.Fragment>
                                                                            } */}

                            {/* (5)_________________Stop Words Remover________________ */}
                            {/* {this.state.formPayload.encoder === "stopWordsRemover" &&
                                                                                <React.Fragment>
                                                                                    <div className="form-group inheritedForm">
                                                                                        <label className="form-label">Select Column :</label>
                                                                                        <ReactSelect
                                                                                            value={this.state.selectedInputCols}
                                                                                            onChange={(value, type) => this.handleInputColumnChange(value, type, false)}
                                                                                            options={this.createOptionsForReactSelect()}
                                                                                            isMulti={false}
                                                                                        />
                                                                                    </div>
                                                                                    <div className="form-group">
                                                                                        <input className="form-control" type="text" id="outputCol" value={this.state.formPayload.outputCol} onChange={this.transformationFormChangeHandler} placeholder="indexes" />
                                                                                        <label className="form-group-label">Output Column :<span className="requiredMark"><i
                                                                                            className="fas fa-star-of-life"></i></span></label>
                                                                                    </div>
                                                                                    <div className="form-group">
                                                                                        <div className="flex">
                                                                                            <label className="flex-item fx-b40 pd-l-10">Drop Original Column:</label>
                                                                                            <div className="flex-item fx-b30">
                                                                                                <label className="radioLabel">True
                                                                                                    <input name="dropColumn" type="radio" value={"true"} checked={this.state.formPayload.dropColumn === "true"} onChange={this.transformationFormChangeHandler} />
                                                                                                    <span className="radioMark" />
                                                                                                </label>
                                                                                            </div>
                                                                                            <div className="flex-item fx-b30">
                                                                                                <label className="radioLabel">False
                                                                                                    <input name="dropColumn" type="radio" value={"false"} checked={this.state.formPayload.dropColumn === "false"} onChange={this.transformationFormChangeHandler} />
                                                                                                    <span className="radioMark" />
                                                                                                </label>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div className="bottomButton text-right">
                                                                                        <button type="submit" className="btn btn-success m-0">Save</button>
                                                                                    </div>
                                                                                </React.Fragment> */
                            }

                            {/* (6)_________________String Indexer________________ */}
                            {/* {this.state.formPayload.encoder === "stringIndexer" &&
                                                                                <React.Fragment>
                                                                                    <div className="form-group inheritedForm">
                                                                                        <label className="form-label">Select Column :</label>
                                                                                        <ReactSelect
                                                                                            value={this.state.selectedInputCols}
                                                                                            onChange={(value, type) => this.handleInputColumnChange(value, type, false)}
                                                                                            options={this.createOptionsForReactSelect()}
                                                                                            isMulti={false}
                                                                                        />
                                                                                    </div>
                                                                                    <div className="form-group">
                                                                                        <input className="form-control" type="text" id="outputCol" value={this.state.formPayload.outputCol} onChange={this.transformationFormChangeHandler} placeholder="indexes" />
                                                                                        <label className="form-group-label">Output Column :<span className="requiredMark"><i
                                                                                            className="fas fa-star-of-life"></i></span></label>
                                                                                    </div>
                                                                                    <div className="form-group">
                                                                                        <div className="flex">
                                                                                            <label className="flex-item fx-b40 pd-l-10">Drop Original Column:</label>
                                                                                            <div className="flex-item fx-b30">
                                                                                                <label className="radioLabel">True
                                                                                                    <input name="dropColumn" type="radio" value={"true"} checked={this.state.formPayload.dropColumn === "true"} onChange={this.transformationFormChangeHandler} />
                                                                                                    <span className="radioMark" />
                                                                                                </label>
                                                                                            </div>
                                                                                            <div className="flex-item fx-b30">
                                                                                                <label className="radioLabel">False
                                                                                                    <input name="dropColumn" type="radio" value={"false"} checked={this.state.formPayload.dropColumn === "false"} onChange={this.transformationFormChangeHandler} />
                                                                                                    <span className="radioMark" />
                                                                                                </label>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div className="bottomButton text-right">
                                                                                        <button type="submit" className="btn btn-success m-0">Save</button>
                                                                                    </div>
                                                                                </React.Fragment>
                                                                            }
                                                                        </form>}

                                                                    {this.state.selectedAction === "transformation" &&
                                                                        <form className="contentForm" onSubmit={this.actionSaveHandler}>
                                                                            <div className="form-group">
                                                                                <select className="form-control" id="transform" required value={this.state.formPayload.transform} onChange={this.transformationFormChangeHandler}>
                                                                                    <option value=''>Select</option>
                                                                                    <option value='renameColumn'>Rename Column</option>
                                                                                    <option value='targetColumn'>Target Column</option>
                                                                                    <option value='splitData'>Split Data</option>
                                                                                </select>
                                                                                <label className="form-group-label">Column :<span className="requiredMark"><i
                                                                                    className="fas fa-star-of-life"></i></span></label>
                                                                            </div>
                                                                            {this.state.formPayload.transform === 'renameColumn' &&
                                                                                <React.Fragment>
                                                                                    <div className="form-group">
                                                                                        <input className="form-control" type="text" id="newColumnName" value={this.state.formPayload.newColumnName} onChange={this.transformationFormChangeHandler} placeholder="indexes" />
                                                                                        <label className="form-group-label">New Column Name :<span className="requiredMark"><i
                                                                                            className="fas fa-star-of-life"></i></span></label>
                                                                                    </div>
                                                                                    <div className="bottomButton text-right">
                                                                                        <button type="submit" className="btn btn-success m-0">Save</button>
                                                                                    </div>
                                                                                </React.Fragment>
                                                                            }
                                                                            {this.state.formPayload.transform === 'targetColumn' &&
                                                                                <React.Fragment>
                                                                                    <div className="form-group">
                                                                                        <p className='handlingAll'>Target Column doesn't require any parameter</p>
                                                                                    </div>
                                                                                    <div className="bottomButton text-right">
                                                                                        <button type="submit" className="btn btn-success m-0">Save</button>
                                                                                    </div>
                                                                                </React.Fragment>
                                                                            }
                                                                            {this.state.formPayload.transform === 'splitData' &&
                                                                                <React.Fragment>
                                                                                    <div className="flex rangeSliderBox">
                                                                                        <label className="flex-item fx-b20 pd-l-10">Percent:</label>
                                                                                        <p className="flex-item fx-b20">{this.state.formPayload.percent}</p>
                                                                                        <div className="flex-item fx-b60">
                                                                                            <label className="radioLabel pd-0 mr-r-16">0</label>
                                                                                            <Slider
                                                                                                axis="x"
                                                                                                xmin={0}
                                                                                                xmax={1}
                                                                                                xstep={0.01}
                                                                                                x={this.state.formPayload.percent}
                                                                                                onChange={({ x }) => { this.SliderChangeHandler(x, "percent") }}
                                                                                            />
                                                                                            <label className="radioLabel pd-0 mr-l-16">1</label>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div className="bottomButton text-right">
                                                                                        <button type="submit" className="btn btn-success m-0">Save</button>
                                                                                    </div>
                                                                                </React.Fragment>
                                                                            }
                                                                        </form>}

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> */}

                            {/* _____________Feature selection_____________ */}
                            {/* <div className="flex-item fx-b100 mlStudioFormContent">
                                            <div className="mlStudioItemBox" data-toggle="collapse" data-target="#item3">
                                                <p>Feature Selection</p>
                                            </div>
                                            <div className=" mlStudioItemBoxCollapsed collapse" id="item3">
                                                <div>
                                                    <div className="flex">
                                                        <div className="flex-item fx-b33">
                                                            <div className="formSection">
                                                                <div className="formSectionHeader">
                                                                    <p className="p-0 m-0">Select Features</p>
                                                                </div>
                                                                <div className="formSectionBody">
                                                                    <ul className="featureList">
                                                                        {this.state.featureList.map((feature, index) => {
                                                                            return (
                                                                                <li key={index} draggable onDragStart={() => this.onFeatureDrag(feature.value)}> <img src={`https://content.iot83.com/m83/mlStudio/${feature.imageName}.png`} />{feature.displayName}</li>
                                                                            )
                                                                        })}
                                                                    </ul>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div className="flex-item fx-b33">
                                                            <div className="formSection addFeature">
                                                                <div className="formSectionHeader">
                                                                    <p className="p-0 m-0">Selected Features</p>
                                                                </div>
                                                                <div className="formSectionBody">
                                                                    <ul className="addFeatureList">
                                                                        <React.Fragment>
                                                                            {this.state.payload.featureSelectionConfig.techniques.map((feature, featureIndex) => {
                                                                                let displayName = this.state.featureList.find(el => el.value === feature.actionName).displayName
                                                                                return (
                                                                                    <li key={featureIndex} className={feature.isCompleted ? this.state.featureIndex === featureIndex ? "active completed" : "unactive completed" : this.state.featureIndex === featureIndex ? "active" : "unactive"}>
                                                                                        <p onClick={(e) => { this.onFeatureSelect(e, featureIndex, feature.actionName) }}> <img src="https://content.iot83.com/m83/mlStudio/vectorSlicing.png" />{displayName}</p><button className="btn btn-transparent btn-transparent-danger" onClick={(e) => this.deleteFeature(e, featureIndex)}><i className="fas fa-trash-alt"></i></button>
                                                                                    </li>
                                                                                )
                                                                            })}
                                                                            <li className="dropzoneBox pd-r-10" onDrop={(e) => this.onFeatureDrop(e)} onDragOver={(event) => this.allowDrop(event)} >
                                                                                <div className="dropzone activeDropzone">
                                                                                    <h5 className="p-0">Drag and Drop Feature Here</h5>
                                                                                </div>
                                                                            </li>
                                                                        </React.Fragment>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="flex-item fx-b33">
                                                            <div className="formSection">

                                                                {this.state.selectedFeature ?
                                                                    <div className="formSectionHeader">
                                                                        <span>Properties<i className="fas fa-greater-than mr-r-10 mr-l-10"></i></span><span>{this.state.featureList.find(el => el.value === this.state.selectedFeature).displayName}</span>
                                                                    </div> :
                                                                    <div className="formSectionHeader">
                                                                        <span>Properties</span>
                                                                    </div>}
                                                                <div className="formSectionBody">
                                                                    {
                                                                        !this.state.selectedFeature &&
                                                                        <h5 className="selectMessage">Select Atleast One Feature</h5>
                                                                    }
                                                                    {this.state.selectedFeature === "correlationRemoval" &&
                                                                        <form className="contentForm" onSubmit={this.featureSaveHandler}>
                                                                            <div className="flex rangeSliderBox">
                                                                                <label className="flex-item fx-b20 pd-l-10">Percent:</label>
                                                                                <p className="flex-item fx-b20">{this.state.featurePayload.threshold}</p>
                                                                                <div className="flex-item fx-b60">
                                                                                    <label className="radioLabel pd-0 mr-r-16">0.1</label>
                                                                                    <Slider
                                                                                        axis="x"
                                                                                        xmin={0.1}
                                                                                        xmax={0.7}
                                                                                        xstep={0.01}
                                                                                        x={this.state.featurePayload.threshold}
                                                                                        onChange={({ x }) => { this.SliderChangeHandlerForFeatureSelection(x, "threshold") }}
                                                                                    />
                                                                                    <label className="radioLabel pd-0 mr-l-16">0.7</label>
                                                                                </div>
                                                                            </div>
                                                                            <div className="form-group">
                                                                                <select className="form-control" id="targetColumn" required value={this.state.featurePayload.targetColumn} onChange={this.featureFormChangeHandler}>
                                                                                    <option value=''>Select</option>
                                                                                    {this.state.columnsList.map((column, columnIndex) => {
                                                                                        return (<option key={columnIndex} value={column.keys}>{column.keys}</option>)
                                                                                    }
                                                                                    )}
                                                                                </select>
                                                                                <label className="form-group-label">Target Column: <span className="requiredMark"><i className="fas fa-star-of-life"></i></span></label>
                                                                            </div>
                                                                            <div className="bottomButton text-right">
                                                                                <button type="submit" className="btn btn-success m-0">Save</button>
                                                                            </div>
                                                                        </form>}

                                                                    {this.state.selectedFeature === "informationGain" &&
                                                                        <form className="contentForm" onSubmit={this.featureSaveHandler}>
                                                                            <div className="form-group">
                                                                                <select className="form-control" id="targetColumn" disabled={this.state.informationGainLoader} required value={this.state.featurePayload.targetColumn} onChange={this.featureFormChangeHandler}>
                                                                                    <option value=''>Select</option>
                                                                                    {this.state.columnsList.map((column, columnIndex) => {
                                                                                        return (<option key={columnIndex} value={column.keys}>{column.keys}</option>)
                                                                                    }
                                                                                    )}
                                                                                </select>
                                                                                <label className="form-group-label">Target Column: <span className="requiredMark"><i className="fas fa-star-of-life"></i></span></label>
                                                                            </div>
                                                                            <div className="form-group">
                                                                                <select className="form-control" id="targetColumnType" disabled={this.state.informationGainLoader} required value={this.state.featurePayload.targetColumnType} onChange={this.featureFormChangeHandler}>
                                                                                    <option value='categoricalType'>Categorical Type</option>
                                                                                    <option value='regressionType'>Regression Type</option>
                                                                                </select>
                                                                                <label className="form-group-label">Target Column Type : <span className="requiredMark"><i className="fas fa-star-of-life"></i></span></label>
                                                                            </div>
                                                                            {Object.keys(this.state.informationGainData).length ?
                                                                                <div className="flex rangeSliderBox">
                                                                                    <label className="flex-item fx-b20 pd-l-10">Percent:</label>
                                                                                    <p className="flex-item fx-b20">{this.state.featurePayload.k}</p>
                                                                                    <div className="flex-item fx-b60">
                                                                                        <label className="radioLabel pd-0 mr-r-16">1</label>
                                                                                        <Slider
                                                                                            axis="x"
                                                                                            xmin={1}
                                                                                            xmax={this.state.informationGainData.dataKeys.length}
                                                                                            xstep={1}
                                                                                            x={this.state.featurePayload.k}
                                                                                            onChange={({ x }) => { this.SliderChangeHandlerForFeatureSelection(x, "k") }}
                                                                                        />
                                                                                        <label className="radioLabel pd-0 mr-l-16">{this.state.columnsList.length}</label>
                                                                                    </div>
                                                                                </div> : null}
                                                                            {Object.keys(this.state.informationGainData).length ? <button type="button" className="btn btn-link" data-toggle="modal" data-target="#informationGainModal" ><i className="far fa-plus"></i>Show Information Gain Data</button> : null}
                                                                            <div className="bottomButton text-right">
                                                                                <button type="button" disabled={(!this.state.featurePayload.targetColumn || this.state.informationGainLoader)} onClick={() => {
                                                                                    let payload = {
                                                                                        dataSource: cloneDeep(this.state.payload.dataSource),
                                                                                        action: {
                                                                                            type: "showStatistics",
                                                                                            actionName: "informationGain",
                                                                                            targetColumn: this.state.featurePayload.targetColumn,
                                                                                            targetColumnType: this.state.featurePayload.targetColumnType
                                                                                        }
                                                                                    };
                                                                                    this.props.getStatistics(payload, "informationGainLoader");
                                                                                    this.setState({
                                                                                        informationGainLoader: true
                                                                                    })
                                                                                }} className="btn btn-success m-0">Get Dependant Columns Number</button>
                                                                            </div>
                                                                            <div className="bottomButton text-right">
                                                                                <button type="submit" disabled={!this.state.featurePayload.k || this.state.informationGainLoader} className="btn btn-success m-0">Save</button>
                                                                            </div>
                                                                        </form>}

                                                                    {this.state.selectedFeature === "chiSqTest" &&
                                                                        <form className="contentForm" onSubmit={this.featureSaveHandler}>
                                                                            <div className="flex rangeSliderBox">
                                                                                <label className="flex-item fx-b20 pd-l-10">Percent:</label>
                                                                                <p className="flex-item fx-b20">{this.state.featurePayload.k}</p>
                                                                                <div className="flex-item fx-b60">
                                                                                    <label className="radioLabel pd-0 mr-r-16">1</label>
                                                                                    <Slider
                                                                                        axis="x"
                                                                                        xmin={1}
                                                                                        xmax={this.state.columnsList.length}
                                                                                        xstep={1}
                                                                                        x={this.state.featurePayload.k}
                                                                                        onChange={({ x }) => { this.SliderChangeHandlerForFeatureSelection(x, "k") }}
                                                                                    />
                                                                                    <label className="radioLabel pd-0 mr-l-16">{this.state.columnsList.length}</label>
                                                                                </div>
                                                                            </div>
                                                                            <div className="bottomButton text-right">
                                                                                <button type="submit" className="btn btn-success m-0">Save</button>
                                                                            </div>
                                                                        </form>}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        {/* _____________Model Building_____________ */}
                            {/* <div className="flex-item fx-b100 mlStudioFormContent m-0">
                                            <div className="mlStudioItemBox" data-toggle="collapse" data-target="#item4">
                                                <p>Model Building</p>
                                            </div>
                                            <div className=" mlStudioItemBoxCollapsed collapse" id="item4">
                                                <div  >
                                                    <div className="flex">
                                                        <div className="flex-item fx-b33">
                                                            <div className="formSection">
                                                                <div className="formSectionHeader">
                                                                    <p className="p-0 m-0">Select Algorithm</p>
                                                                </div>
                                                                <div className="formSectionBody">
                                                                    <ul className="connectorsList hierarchyList">
                                                                        {this.state.algorithmsList.map((algorithm, index) => {
                                                                            return (
                                                                                <li key={index}>
                                                                                    <p data-tip data-for={`alogo${index}`} className={`level2 pd-l-8 ${this.state.payload.algorithm.algorithmName === algorithm.name ? '' : "collapsed"}`} data-toggle="collapse" onClick={() => this.selectAlgorithm("name", algorithm.name)} data-target={`#child${index}`}> <i className="fas fa-caret-right"></i>{algorithm.name}</p>
                                                                                    <ReactTooltip id={`alogo${index}`} place="right" type="dark">
                                                                                        <div className="tooltipText">
                                                                                            <p>{algorithm.toolTipText}</p>
                                                                                        </div>
                                                                                    </ReactTooltip>
                                                                                    <ul className={this.state.payload.algorithm.algorithmName === algorithm.name ? "collapse show" : "collapse"} id={`child${index}`} >
                                                                                        {algorithm.children.map((child, childIndex) => {
                                                                                            return (<li key={childIndex}>
                                                                                                <p className="level1" onClick={() => this.selectAlgorithm("type", child.value)}>{child.name}<img src={child.url} /></p>
                                                                                            </li>)
                                                                                        })}
                                                                                    </ul>
                                                                                </li>
                                                                            )
                                                                        })}
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="flex-item fx-b33">
                                                            <div className="selectedItemBox">
                                                                <div className="selectedItem ">
                                                                    <i className="fas fa-times-circle cancel"></i>
                                                                    <div className="flex info">
                                                                        <img src="https://content.iot83.com/m83/mlStudio/algo.png" />
                                                                        <div className="fx-b50 selectedItemName">
                                                                            <img src="https://content.iot83.com/m83/mlStudio/XGBoostLogo.png" />
                                                                        </div>
                                                                        <div className="fx-b50">
                                                                            <p className="m-0"><strong>{this.state.payload.algorithm.algorithmName}</strong></p>
                                                                            <p className="m-0">{this.state.payload.algorithm.algorithmType}</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="flex-item fx-b33">
                                                            <div className="formSection">
                                                                {this.state.payload.algorithm.algorithmType ?
                                                                    <div className="formSectionHeader">
                                                                        <span>Parameters <i className="fas fa-greater-than mr-r-10 mr-l-10"></i></span><span>{this.state.payload.algorithm.algorithmName}</span><i className="fas fa-greater-than mr-r-10 mr-l-10"></i><span>{this.state.payload.algorithm.algorithmType === "linear-learner" ? "Linear-Learner" : this.state.payload.algorithm.algorithmType}</span>
                                                                    </div> :
                                                                    <div className="formSectionHeader">
                                                                        <span>Parameters</span>
                                                                    </div>} */}
                            {/* _____________X-Boost__________ */}
                            {/* <div className="formSectionBody">
                                                                    {
                                                                        !this.state.payload.algorithm.algorithmType &&
                                                                        <h5 className="selectMessage">Select Atleast One Algorithm</h5>
                                                                    }

                                                                    {this.state.payload.algorithm.algorithmType === "XgBoost" &&
                                                                        <form className="contentForm">
                                                                            <div className="form-group">
                                                                                <input className="form-control" type="text" id="trainingJobName" value={this.state.payload.algorithm.trainingJobName} onChange={this.algorithmFormHandler} placeholder="indexes" />
                                                                                <label className="form-group-label">Training Job Name : <span className="requiredMark"><i className="fas fa-star-of-life"></i></span></label>
                                                                            </div>
                                                                            <p className="text-primary pd-l-10"><strong>HyperParameters</strong></p>
                                                                            <div className="form-group">
                                                                                <input className="form-control" id="objective" defaultValue={this.state.payload.algorithm.hyperparams.objective} readOnly={true} type="text" placeholder="indexes" />
                                                                                <label className="form-group-label">Objective : <span className="requiredMark"><i className="fas fa-star-of-life"></i></span></label>
                                                                            </div>
                                                                            <div className="form-group">
                                                                                <input className="form-control" id="eta" value={this.state.payload.algorithm.hyperparams.eta} onChange={this.algorithmFormHandler} type="number" placeholder="indexes" />
                                                                                <label className="form-group-label">ETA : <span className="requiredMark"><i className="fas fa-star-of-life"></i></span></label>
                                                                            </div>
                                                                            <div className="form-group">
                                                                                <input className="form-control" id="subsample" value={this.state.payload.algorithm.hyperparams.subsample} onChange={this.algorithmFormHandler} type="number" placeholder="indexes" />
                                                                                <label className="form-group-label">subsample :</label>
                                                                            </div>
                                                                            <div className="form-group">
                                                                                <input className="form-control" id="num_round" value={this.state.payload.algorithm.hyperparams.num_round} onChange={this.algorithmFormHandler} type="text" placeholder="indexes" />
                                                                                <label className="form-group-label">num_round : <span className="requiredMark"><i className="fas fa-star-of-life"></i></span></label>
                                                                            </div>
                                                                            <div className="flex rangeSliderBox">
                                                                                <label className="flex-item fx-b20 pd-l-10">Train Value:</label>
                                                                                <p className="flex-item fx-b20">{this.state.payload.algorithm.hyperparams.trainSplitValue}</p>
                                                                                <div className="flex-item fx-b60">
                                                                                    <label className="radioLabel pd-0 mr-r-16">0</label>
                                                                                    <Slider
                                                                                        axis="x"
                                                                                        xmin={0}
                                                                                        xmax={1}
                                                                                        xstep={0.1}
                                                                                        x={this.state.payload.algorithm.hyperparams.trainSplitValue}
                                                                                        onChange={({ x }) => { this.SliderChangeHandlerForAlgorithm(x, "trainSplitValue") }}
                                                                                    />
                                                                                    <label className="radioLabel pd-0 mr-l-16">1</label>
                                                                                </div>
                                                                            </div>
                                                                            <div className="flex rangeSliderBox">
                                                                                <label className="flex-item fx-b20 pd-l-10">Test Value:</label>
                                                                                <p className="flex-item fx-b20">{this.state.payload.algorithm.hyperparams.testSplitValue}</p>
                                                                                <div className="flex-item fx-b60">
                                                                                    <label className="radioLabel pd-0 mr-r-16">0</label>
                                                                                    <Slider
                                                                                        axis="x"
                                                                                        xmin={0}
                                                                                        xmax={1}
                                                                                        xstep={0.1}
                                                                                        x={this.state.payload.algorithm.hyperparams.testSplitValue}
                                                                                        onChange={({ x }) => { this.SliderChangeHandlerForAlgorithm(x, "testSplitValue") }}
                                                                                    />
                                                                                    <label className="radioLabel pd-0 mr-l-16">1</label>
                                                                                </div>
                                                                            </div>
                                                                        </form>} */}


                            {/* _____________Linear Learner__________ */}
                            {/* {this.state.payload.algorithm.algorithmType === "linear-learner" &&
                                                                        <form className="contentForm">
                                                                            <div className="form-group">
                                                                                <input className="form-control" type="text" id="trainingJobName" value={this.state.payload.algorithm.trainingJobName} onChange={this.algorithmFormHandler} placeholder="indexes" />
                                                                                <label className="form-group-label">Training Job Name : <span className="requiredMark"><i className="fas fa-star-of-life"></i></span></label>
                                                                            </div>
                                                                            <p className="text-primary pd-l-10"><strong>HyperParameters</strong></p>
                                                                            <div className="form-group">
                                                                                <input className="form-control" type="text" id="feature_dim" value={this.state.payload.algorithm.hyperparams.feature_dim} onChange={this.algorithmFormHandler} placeholder="indexes" />
                                                                                <label className="form-group-label">Feature_Dim : <span className="requiredMark"><i className="fas fa-star-of-life"></i></span></label>
                                                                            </div>
                                                                            <div className="form-group">
                                                                                <select className="form-control" id="predictor_type" value={this.state.payload.algorithm.hyperparams.predictor_type} onChange={this.algorithmFormHandler}>
                                                                                    <option value='binary_classifier'>Binary Classifier</option>
                                                                                    <option value='multiclass_classifier'>Multi Class Classifier</option>
                                                                                </select>
                                                                                <label className="form-group-label">predictor_type : <span className="requiredMark"><i className="fas fa-star-of-life"></i></span></label>
                                                                            </div>
                                                                            <div className="form-group">
                                                                                <input className="form-control" type="text" id="target" value={this.state.payload.algorithm.hyperparams.target} onChange={this.algorithmFormHandler} placeholder="indexes" />
                                                                                <label className="form-group-label">Target Column : <span className="requiredMark"><i className="fas fa-star-of-life"></i></span></label>
                                                                            </div>
                                                                            <div className="flex rangeSliderBox">
                                                                                <label className="flex-item fx-b20 pd-l-10">Train Value:</label>
                                                                                <p className="flex-item fx-b20">{this.state.payload.algorithm.hyperparams.trainSplitValue}</p>
                                                                                <div className="flex-item fx-b60">
                                                                                    <label className="radioLabel pd-0 mr-r-16">0</label>
                                                                                    <Slider
                                                                                        axis="x"
                                                                                        xmin={0}
                                                                                        xmax={1}
                                                                                        xstep={0.1}
                                                                                        x={this.state.payload.algorithm.hyperparams.trainSplitValue}
                                                                                        onChange={({ x }) => { this.SliderChangeHandlerForAlgorithm(x, "testSplitValue") }}
                                                                                    />
                                                                                    <label className="radioLabel pd-0 mr-l-16">1</label>
                                                                                </div>
                                                                            </div>
                                                                            <div className="flex rangeSliderBox">
                                                                                <label className="flex-item fx-b20 pd-l-10">Test Value:</label>
                                                                                <p className="flex-item fx-b20">{this.state.payload.algorithm.hyperparams.testSplitValue}</p>
                                                                                <div className="flex-item fx-b60">
                                                                                    <label className="radioLabel pd-0 mr-r-16">0</label>
                                                                                    <Slider
                                                                                        axis="x"
                                                                                        xmin={0}
                                                                                        xmax={1}
                                                                                        xstep={0.1}
                                                                                        x={this.state.payload.algorithm.hyperparams.testSplitValue}
                                                                                        onChange={({ x }) => { this.SliderChangeHandlerForAlgorithm(x, "testSplitValue") }}
                                                                                    />
                                                                                    <label className="radioLabel pd-0 mr-l-16">1</label>
                                                                                </div>
                                                                            </div>
                                                                        </form>} */}

                            {/* _____________K-means__________ */}
                            {/* {this.state.payload.algorithm.algorithmType === "K-Means" &&
                                                                        <form className="contentForm">
                                                                            <div className="form-group">
                                                                                <input className="form-control" type="text" id="trainingJobName" value={this.state.payload.algorithm.trainingJobName} onChange={this.algorithmFormHandler} placeholder="indexes" />
                                                                                <label className="form-group-label">Training Job Name : <span className="requiredMark"><i className="fas fa-star-of-life"></i></span></label>
                                                                            </div>
                                                                            <p className="text-primary pd-l-10"><strong>HyperParameters</strong></p>
                                                                            <div className="form-group">
                                                                                <input className="form-control" type="text" id="feature_dim" value={this.state.payload.algorithm.hyperparams.feature_dim} onChange={this.algorithmFormHandler} placeholder="indexes" />
                                                                                <label className="form-group-label">Feature_Dim :</label>
                                                                            </div>
                                                                            <div className="form-group">
                                                                                <input className="form-control" type="text" id="k" value={this.state.payload.algorithm.hyperparams.k} onChange={this.algorithmFormHandler} placeholder="indexes" />
                                                                                <label className="form-group-label">K : <span className="requiredMark"><i className="fas fa-star-of-life"></i></span></label>
                                                                            </div>
                                                                        </form>} */}

                            {/* _____________RCF__________ */}
                            {/* {this.state.payload.algorithm.algorithmType === "RandomCutForest" &&
                                                                        <form className="contentForm">
                                                                            <div className="form-group">
                                                                                <input className="form-control" type="text" id="trainingJobName" value={this.state.payload.algorithm.trainingJobName} onChange={this.algorithmFormHandler} placeholder="indexes" />
                                                                                <label className="form-group-label">Training Job Name : <span className="requiredMark"><i className="fas fa-star-of-life"></i></span></label>
                                                                            </div>
                                                                            <p className="text-primary pd-l-10"><strong>HyperParameters</strong></p>
                                                                            <div className="form-group">
                                                                                <input className="form-control" type="text" id="feature_dim" value={this.state.payload.algorithm.hyperparams.feature_dim} onChange={this.algorithmFormHandler} placeholder="indexes" />
                                                                                <label className="form-group-label">Feature_Dim :</label>
                                                                            </div>
                                                                            <div className="form-group">
                                                                                <input className="form-control" id="num_samples_per_tree" value={this.state.payload.algorithm.hyperparams.num_samples_per_tree} onChange={this.algorithmFormHandler} type="text" placeholder="indexes" />
                                                                                <label className="form-group-label">NUM_SAMPLES_PER_TREE :</label>
                                                                            </div>
                                                                            <div className="form-group">
                                                                                <input className="form-control" id="num_trees" value={this.state.payload.algorithm.hyperparams.num_trees} onChange={this.algorithmFormHandler} type="text" placeholder="indexes" />
                                                                                <label className="form-group-label">NUM_TREE : <span className="requiredMark"><i className="fas fa-star-of-life"></i></span></label>
                                                                            </div>
                                                                            <div className="form-group">
                                                                                <input className="form-control" id="train_instance_count" defaultValue={this.state.payload.algorithm.hyperparams.train_instance_count} type="text" placeholder="indexes" />
                                                                                <label className="form-group-label">TRAIN_INSTANCE_COUNT : <span className="requiredMark"><i className="fas fa-star-of-life"></i></span></label>
                                                                            </div>
                                                                        </form>}
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="mlStudioFormFooter flex">
                                    <div className="bottomButton text-right">
                                        <button type="button" onClick={(e) => this.saveExperiment(e, true)} className="btn btn-primary mr-r-10">Save as Draft </button>
                                        <button type="button" onClick={(e) => this.saveExperiment(e, false)} className="btn btn-success">Save</button>
                                        <button type="button" onClick={() => { this.props.history.push("/mlStudio"); }} className="btn btn-secondary">Cancel</button>
                                    </div>
                                </div>
                            </div> */}
                        </React.Fragment>
                }

                <div className="modal animated slideInDown" id="showStatisticalReportModal">
                    <div className="modal-dialog themeModal modal-xl modal-dialog-scrollable">
                        {this.state.statisticalReportData.userId &&
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h6 className="modal-title text-center">Transformation
                                    <button type="button" className="close" data-dismiss="modal"><i className="far fa-times"></i></button>
                                    </h6>
                                </div>
                                <div className="modal-body">
                                    <div className="reportBox">
                                        <ul className="nav nav-tabs">
                                            <li className="nav-item">
                                                <a className="nav-link active" data-toggle="tab" href="#home">OverView</a>
                                            </li>
                                            <li className="nav-item">
                                                <a className="nav-link" data-toggle="tab" href="#menu1">Variable</a>
                                            </li>
                                        </ul>

                                        <div className="tab-content">
                                            <div className="tab-pane active" id="home">
                                                <div className="reportBoxContent">
                                                    <div className="reportBoxItem">
                                                        <p>Overview</p>
                                                    </div>
                                                    <div className="flex">
                                                        <div className="flex-item fx-b33">
                                                            <div className="contentTableBox">
                                                                <table className="table">
                                                                    <thead>
                                                                        <tr>
                                                                            <th colSpan="2">
                                                                                Dataset info
                                                                            </th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <tr>
                                                                            <td><strong>Number of variables</strong></td>
                                                                            <td>{this.state.statisticalReportData.table.n_var}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><strong>Number of observations</strong></td>
                                                                            <td>{this.state.statisticalReportData.table.n}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><strong>Total Missing (%)</strong></td>
                                                                            <td>{this.state.statisticalReportData.table.p_cells_missing}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><strong>Total size in memory</strong></td>
                                                                            <td>{this.state.statisticalReportData.table.memory_size ? `${this.state.statisticalReportData.table.memory_size.__int64__} B` : ''}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><strong>Average record size in memory</strong></td>
                                                                            <td>{this.state.statisticalReportData.table.record_size ? `${this.state.statisticalReportData.table.record_size} B` : ''}</td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                        <div className="flex-item fx-b33">
                                                            <div className="contentTableBox">
                                                                <table className="table">
                                                                    <thead>
                                                                        <tr>
                                                                            <th colSpan="2">
                                                                                Variables types
                                                                            </th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <tr>
                                                                            <td><strong>Numeric</strong></td>
                                                                            <td>{this.state.statisticalReportData.table.types && this.state.statisticalReportData.table.types.NUM ? `${this.state.statisticalReportData.table.types.NUM.__int64__} %` : ''}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><strong>Categorical</strong></td>
                                                                            <td>{this.state.statisticalReportData.table.types && this.state.statisticalReportData.table.types.NUM ? `${this.state.statisticalReportData.table.types.NUM.__int64__} %` : ''}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><strong>Date</strong></td>
                                                                            <td>{this.state.statisticalReportData.table.types && this.state.statisticalReportData.table.types.NUM ? `${this.state.statisticalReportData.table.types.NUM.__int64__} %` : ''}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><strong>Text (Unique)</strong></td>
                                                                            <td>{this.state.statisticalReportData.table.types && this.state.statisticalReportData.table.types.NUM ? `${this.state.statisticalReportData.table.types.NUM.__int64__} %` : ''}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><strong>Rejected</strong></td>
                                                                            <td>{this.state.statisticalReportData.table.types && this.state.statisticalReportData.table.types.NUM ? `${this.state.statisticalReportData.table.types.NUM.__int64__} %` : ''}</td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                        <div className="flex-item fx-b33">
                                                            <div className="contentTableBox">
                                                                <table className="table">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>
                                                                                Variables types
                                                                    </th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        {this.state.statisticalReportData.dataKeys.map((el, index) => {
                                                                            return (
                                                                                <React.Fragment key={index}>
                                                                                    <tr>
                                                                                        <td><span className="text-red">{el}</span>has {this.state.statisticalReportData.variables[el].n_missing.__int64__} / {this.state.statisticalReportData.variables[el].p_missing} % missing values <span className="badge  badge-secondary">missing</span></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td><span className="text-red">{el}</span>has {this.state.statisticalReportData.variables[el].n_zeros} / {this.state.statisticalReportData.variables[el].p_zeros} % zeros</td>
                                                                                    </tr>
                                                                                </React.Fragment>
                                                                            )
                                                                        })}
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="tab-pane fade" id="menu1">
                                                {this.state.statisticalReportData.dataKeys.map((el, index) => {
                                                    return (
                                                        <div className="reportBoxContent" key={index}>
                                                            <div className="reportBoxItem">
                                                                <p>{`${el}(${(this.state.statisticalReportData.variables[el].type.__Variable__).split("_")[1]})`}</p>
                                                            </div>
                                                            <div className="flex">
                                                                <div className="flex-item fx-b35">
                                                                    <div className="contentTableBox">
                                                                        <table className="table">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td><strong>Distinct count</strong></td>
                                                                                    <td>{this.state.statisticalReportData.variables[el].distinct_count.__int64__}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td><strong>Unique (%)</strong></td>
                                                                                    <td>{this.state.statisticalReportData.variables[el].p_unique} %</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td><strong>Total Missing (%)</strong></td>
                                                                                    <td>{this.state.statisticalReportData.table.p_cells_missing} %</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td><strong>Missing (%)	</strong></td>
                                                                                    <td>{this.state.statisticalReportData.variables[el].p_missing} %</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td><strong> Missing (n)</strong></td>
                                                                                    <td>{this.state.statisticalReportData.variables[el].n_missing.__int64__}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td><strong>Infinite (%)</strong></td>
                                                                                    <td>{this.state.statisticalReportData.variables[el].p_infinite} %</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td><strong>Infinite (n)</strong></td>
                                                                                    <td>{this.state.statisticalReportData.variables[el].n_infinite.__int64__}</td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                                <div className="flex-item fx-b35">
                                                                    <div className="contentTableBox">
                                                                        <table className="table">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td><strong> Mean</strong></td>
                                                                                    <td>{this.checkIfKeyIsValid(this.state.statisticalReportData.variables[el].mean)}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td><strong> Minimum</strong></td>
                                                                                    <td>{this.checkIfKeyIsValid(this.state.statisticalReportData.variables[el].min)}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td><strong>Maximum</strong></td>
                                                                                    <td>{this.checkIfKeyIsValid(this.state.statisticalReportData.variables[el].max)}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td><strong>Zeros (%)</strong></td>
                                                                                    <td>{this.checkIfKeyIsValid(this.state.statisticalReportData.variables[el].p_zeros)} %</td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                                <div className="flex-item fx-b30">
                                                                    <div className="staticBox position-relative">
                                                                        <div className="chatImage">
                                                                            {this.createChart(this.state.statisticalReportData, el, "barChartForMl", "basicBarChart")}
                                                                        </div>
                                                                        <button className="btn btn-link" data-target={`#details_${index}`} data-toggle="collapse">Details</button>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <div className="flex-item fx-b100">
                                                                <div className="collapse mr-t-10" id={`details_${index}`}>
                                                                    <div className="flex">
                                                                        <div className="flex-item fx-b20">
                                                                            <label className="form-label font-weight-bold f-18 mr-l-10">Quantile statistics :</label>
                                                                            <ul className="staticList">
                                                                                <li><strong>Minimum</strong>{this.checkIfKeyIsValid(this.state.statisticalReportData.variables[el].min)}</li>
                                                                                <li><strong>5-th percentile</strong>{this.state.statisticalReportData.variables[el]["5%"]}</li>
                                                                                <li><strong>Q1</strong>{this.state.statisticalReportData.variables[el]["25%"]}</li>
                                                                                <li><strong>Mode</strong>{this.checkIfKeyIsValid(this.state.statisticalReportData.variables[el].mode)}</li>
                                                                                <li><strong>Q3</strong>{this.state.statisticalReportData.variables[el]["75%"]}</li>
                                                                                <li><strong>95-th percentile</strong>{this.state.statisticalReportData.variables[el]["95%"]}</li>
                                                                                <li><strong>Maximum</strong>{this.checkIfKeyIsValid(this.state.statisticalReportData.variables[el].max)}</li>
                                                                                <li><strong>Range</strong>{this.checkIfKeyIsValid(this.state.statisticalReportData.variables[el].range)}</li>
                                                                                <li><strong>Interquartile range</strong>{this.state.statisticalReportData.variables[el].iqr}</li>
                                                                            </ul>
                                                                        </div>
                                                                        <div className="flex-item fx-b20">
                                                                            <label className="form-label font-weight-bold f-18 mr-l-10">Descriptive statistics :</label>
                                                                            <ul className="staticList">
                                                                                <li><strong>Standard deviation</strong>{this.state.statisticalReportData.variables[el].std}</li>
                                                                                <li><strong>Coef of variation</strong>{this.state.statisticalReportData.variables[el].cv}</li>
                                                                                <li><strong>Kurtosis</strong>{this.state.statisticalReportData.variables[el].kurtosis}</li>
                                                                                <li><strong>Mean</strong>{this.checkIfKeyIsValid(this.state.statisticalReportData.variables[el].mean)}</li>
                                                                                <li><strong>MAD</strong>{this.state.statisticalReportData.variables[el].mad}</li>
                                                                                <li><strong>Skewness</strong>{this.state.statisticalReportData.variables[el].skewness}</li>
                                                                                <li><strong>Sum</strong>{this.checkIfKeyIsValid(this.state.statisticalReportData.variables[el].sum)}</li>
                                                                                <li><strong>Variance</strong>{this.state.statisticalReportData.variables[el].variance}</li>
                                                                                <li><strong>Memory size</strong>{this.state.statisticalReportData.variables[el].memory_size} B</li>
                                                                            </ul>
                                                                        </div>
                                                                        <div className="flex-item fx-b60">
                                                                            <div className="chatImage">
                                                                                {this.createChart(this.state.statisticalReportData, el, "lineChartForMl", "simpleLineChart")}
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>)
                                                })}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>}
                    </div>
                </div>

                <div className="modal animated slideInDown" id="showStatisticalReportWithCleaningAndTransformModal">
                    <div className="modal-dialog themeModal modal-xl modal-dialog-scrollable">
                        {this.state.statisticalReportDataWithCleaningAndTransform.userId &&
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h6 className="modal-title text-center">Transformation
                                    <button type="button" className="close" data-dismiss="modal"><i className="far fa-times"></i></button>
                                    </h6>
                                </div>
                                <div className="modal-body">
                                    <div className="reportBox">
                                        <ul className="nav nav-tabs">
                                            <li className="nav-item">
                                                <a className="nav-link active" data-toggle="tab" href="#home">OverView</a>
                                            </li>
                                            <li className="nav-item">
                                                <a className="nav-link" data-toggle="tab" href="#menu1">Variable</a>
                                            </li>
                                        </ul>

                                        <div className="tab-content">
                                            <div className="tab-pane active" id="home">
                                                <div className="reportBoxContent">
                                                    <div className="reportBoxItem">
                                                        <p>Overview</p>
                                                    </div>
                                                    <div className="flex">
                                                        <div className="flex-item fx-b33">
                                                            <div className="contentTableBox">
                                                                <table className="table">
                                                                    <thead>
                                                                        <tr>
                                                                            <th colSpan="2">
                                                                                Dataset info
                                                                            </th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <tr>
                                                                            <td><strong>Number of variables</strong></td>
                                                                            <td>{this.state.statisticalReportDataWithCleaningAndTransform.table.n_var}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><strong>Number of observations</strong></td>
                                                                            <td>{this.state.statisticalReportDataWithCleaningAndTransform.table.n}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><strong>Total Missing (%)</strong></td>
                                                                            <td>{this.state.statisticalReportDataWithCleaningAndTransform.table.p_cells_missing}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><strong>Total size in memory</strong></td>
                                                                            <td>{this.state.statisticalReportDataWithCleaningAndTransform.table.memory_size ? `${this.state.statisticalReportDataWithCleaningAndTransform.table.memory_size.__int64__} B` : ''}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><strong>Average record size in memory</strong></td>
                                                                            <td>{this.state.statisticalReportDataWithCleaningAndTransform.table.record_size ? `${this.state.statisticalReportDataWithCleaningAndTransform.table.record_size} B` : ''}</td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                        <div className="flex-item fx-b33">
                                                            <div className="contentTableBox">
                                                                <table className="table">
                                                                    <thead>
                                                                        <tr>
                                                                            <th colSpan="2">
                                                                                Variables types
                                                                            </th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <tr>
                                                                            <td><strong>Numeric</strong></td>
                                                                            <td>{this.state.statisticalReportDataWithCleaningAndTransform.table.types && this.state.statisticalReportDataWithCleaningAndTransform.table.types.NUM ? `${this.state.statisticalReportDataWithCleaningAndTransform.table.types.NUM.__int64__} %` : ''}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><strong>Categorical</strong></td>
                                                                            <td>{this.state.statisticalReportDataWithCleaningAndTransform.table.types && this.state.statisticalReportDataWithCleaningAndTransform.table.types.NUM ? `${this.state.statisticalReportDataWithCleaningAndTransform.table.types.NUM.__int64__} %` : ''}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><strong>Date</strong></td>
                                                                            <td>{this.state.statisticalReportDataWithCleaningAndTransform.table.types && this.state.statisticalReportDataWithCleaningAndTransform.table.types.NUM ? `${this.state.statisticalReportDataWithCleaningAndTransform.table.types.NUM.__int64__} %` : ''}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><strong>Text (Unique)</strong></td>
                                                                            <td>{this.state.statisticalReportDataWithCleaningAndTransform.table.types && this.state.statisticalReportDataWithCleaningAndTransform.table.types.NUM ? `${this.state.statisticalReportDataWithCleaningAndTransform.table.types.NUM.__int64__} %` : ''}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><strong>Rejected</strong></td>
                                                                            <td>{this.state.statisticalReportDataWithCleaningAndTransform.table.types && this.state.statisticalReportDataWithCleaningAndTransform.table.types.NUM ? `${this.state.statisticalReportDataWithCleaningAndTransform.table.types.NUM.__int64__} %` : ''}</td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                        <div className="flex-item fx-b33">
                                                            <div className="contentTableBox">
                                                                <table className="table">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>
                                                                                Variables types
                                                                    </th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        {this.state.statisticalReportDataWithCleaningAndTransform.dataKeys.map((el, index) => {
                                                                            return (
                                                                                <React.Fragment key={index}>
                                                                                    <tr>
                                                                                        <td><span className="text-red">{el}</span>has {this.state.statisticalReportDataWithCleaningAndTransform.variables[el].n_missing.__int64__} / {this.state.statisticalReportDataWithCleaningAndTransform.variables[el].p_missing} % missing values <span className="badge  badge-secondary">missing</span></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td><span className="text-red">{el}</span>has {this.state.statisticalReportDataWithCleaningAndTransform.variables[el].n_zeros} / {this.state.statisticalReportDataWithCleaningAndTransform.variables[el].p_zeros} % zeros</td>
                                                                                    </tr>
                                                                                </React.Fragment>
                                                                            )
                                                                        })}
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="tab-pane fade" id="menu1">
                                                {this.state.statisticalReportDataWithCleaningAndTransform.dataKeys.map((el, index) => {
                                                    return (
                                                        <div className="reportBoxContent" key={index}>
                                                            <div className="reportBoxItem">
                                                                <p>{`${el}(${(this.state.statisticalReportDataWithCleaningAndTransform.variables[el].type.__Variable__).split("_")[1]})`}</p>
                                                            </div>
                                                            <div className="flex">
                                                                <div className="flex-item fx-b35">
                                                                    <div className="contentTableBox">
                                                                        <table className="table">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td><strong>Distinct count</strong></td>
                                                                                    <td>{this.state.statisticalReportDataWithCleaningAndTransform.variables[el].distinct_count.__int64__}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td><strong>Unique (%)</strong></td>
                                                                                    <td>{this.state.statisticalReportDataWithCleaningAndTransform.variables[el].p_unique} %</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td><strong>Total Missing (%)</strong></td>
                                                                                    <td>{this.state.statisticalReportDataWithCleaningAndTransform.table.p_cells_missing} %</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td><strong>Missing (%)	</strong></td>
                                                                                    <td>{this.state.statisticalReportDataWithCleaningAndTransform.variables[el].p_missing} %</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td><strong> Missing (n)</strong></td>
                                                                                    <td>{this.state.statisticalReportDataWithCleaningAndTransform.variables[el].n_missing.__int64__}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td><strong>Infinite (%)</strong></td>
                                                                                    <td>{this.state.statisticalReportDataWithCleaningAndTransform.variables[el].p_infinite} %</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td><strong>Infinite (n)</strong></td>
                                                                                    <td>{this.state.statisticalReportDataWithCleaningAndTransform.variables[el].n_infinite.__int64__}</td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                                <div className="flex-item fx-b35">
                                                                    <div className="contentTableBox">
                                                                        <table className="table">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td><strong> Mean</strong></td>
                                                                                    <td>{this.checkIfKeyIsValid(this.state.statisticalReportDataWithCleaningAndTransform.variables[el].mean)}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td><strong> Minimum</strong></td>
                                                                                    <td>{this.checkIfKeyIsValid(this.state.statisticalReportDataWithCleaningAndTransform.variables[el].min)}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td><strong>Maximum</strong></td>
                                                                                    <td>{this.checkIfKeyIsValid(this.state.statisticalReportDataWithCleaningAndTransform.variables[el].max)}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td><strong>Zeros (%)</strong></td>
                                                                                    <td>{this.checkIfKeyIsValid(this.state.statisticalReportDataWithCleaningAndTransform.variables[el].p_zeros)} %</td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                                <div className="flex-item fx-b30">
                                                                    <div className="staticBox position-relative">
                                                                        <div className="chatImage">
                                                                            {this.createChart(this.state.statisticalReportDataWithCleaningAndTransform, el, "barChartForMl", "basicBarChart")}
                                                                        </div>
                                                                        <button className="btn btn-link" data-target={`#details_${index}`} data-toggle="collapse">Details</button>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <div className="flex-item fx-b100">
                                                                <div className="collapse mr-t-10" id={`details_${index}`}>
                                                                    <div className="flex">
                                                                        <div className="flex-item fx-b20">
                                                                            <label className="form-label font-weight-bold f-18 mr-l-10">Quantile statistics :</label>
                                                                            <ul className="staticList">
                                                                                <li><strong>Minimum</strong>{this.checkIfKeyIsValid(this.state.statisticalReportDataWithCleaningAndTransform.variables[el].min)}</li>
                                                                                <li><strong>5-th percentile</strong>{this.state.statisticalReportDataWithCleaningAndTransform.variables[el]["5%"]}</li>
                                                                                <li><strong>Q1</strong>{this.state.statisticalReportDataWithCleaningAndTransform.variables[el]["25%"]}</li>
                                                                                <li><strong>Mode</strong>{this.checkIfKeyIsValid(this.state.statisticalReportDataWithCleaningAndTransform.variables[el].mode)}</li>
                                                                                <li><strong>Q3</strong>{this.state.statisticalReportDataWithCleaningAndTransform.variables[el]["75%"]}</li>
                                                                                <li><strong>95-th percentile</strong>{this.state.statisticalReportDataWithCleaningAndTransform.variables[el]["95%"]}</li>
                                                                                <li><strong>Maximum</strong>{this.checkIfKeyIsValid(this.state.statisticalReportDataWithCleaningAndTransform.variables[el].max)}</li>
                                                                                <li><strong>Range</strong>{this.checkIfKeyIsValid(this.state.statisticalReportDataWithCleaningAndTransform.variables[el].range)}</li>
                                                                                <li><strong>Interquartile range</strong>{this.state.statisticalReportDataWithCleaningAndTransform.variables[el].iqr}</li>
                                                                            </ul>
                                                                        </div>
                                                                        <div className="flex-item fx-b20">
                                                                            <label className="form-label font-weight-bold f-18 mr-l-10">Descriptive statistics :</label>
                                                                            <ul className="staticList">
                                                                                <li><strong>Standard deviation</strong>{this.state.statisticalReportDataWithCleaningAndTransform.variables[el].std}</li>
                                                                                <li><strong>Coef of variation</strong>{this.state.statisticalReportDataWithCleaningAndTransform.variables[el].cv}</li>
                                                                                <li><strong>Kurtosis</strong>{this.state.statisticalReportDataWithCleaningAndTransform.variables[el].kurtosis}</li>
                                                                                <li><strong>Mean</strong>{this.checkIfKeyIsValid(this.state.statisticalReportDataWithCleaningAndTransform.variables[el].mean)}</li>
                                                                                <li><strong>MAD</strong>{this.state.statisticalReportDataWithCleaningAndTransform.variables[el].mad}</li>
                                                                                <li><strong>Skewness</strong>{this.state.statisticalReportDataWithCleaningAndTransform.variables[el].skewness}</li>
                                                                                <li><strong>Sum</strong>{this.checkIfKeyIsValid(this.state.statisticalReportDataWithCleaningAndTransform.variables[el].sum)}</li>
                                                                                <li><strong>Variance</strong>{this.state.statisticalReportDataWithCleaningAndTransform.variables[el].variance}</li>
                                                                                <li><strong>Memory size</strong>{this.state.statisticalReportDataWithCleaningAndTransform.variables[el].memory_size} B</li>
                                                                            </ul>
                                                                        </div>
                                                                        <div className="flex-item fx-b60">
                                                                            <div className="chatImage">
                                                                                {this.createChart(this.state.statisticalReportDataWithCleaningAndTransform, el, "lineChartForMl", "simpleLineChart")}
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>)
                                                })}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>}
                    </div>
                </div>

                <div className="modal animated slideInDown" id="featureImportanceModal">
                    <div className="modal-dialog modal-xl modal-dialog-scrollable">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h4 className="modal-title text-center">Feature Importance</h4>
                                <button type="button" className="close" data-dismiss="modal"><i className="far fa-times"></i></button>
                            </div>
                            <div className="modal-body">
                                <div className="form-group input-group">
                                    <select className="form-control" disabled={this.state.featureImportanceLoader} id="featureImportanceColName" required value={this.state.featureImportanceColName} onChange={(e) => {
                                        let featureImportanceColName = e.currentTarget.value
                                        this.setState({
                                            featureImportanceColName
                                        })
                                    }}>
                                        <option value=''>Select</option>
                                        {this.state.columnsList.map((column, columnIndex) => {
                                            return (<option key={columnIndex} value={column.keys}>{column.keys}</option>)
                                        }
                                        )}
                                    </select>
                                    <div className="input-group-append">
                                        <button className="btn btn-transparent-success input-group-text" disabled={this.state.featureImportanceLoader} onClick={(e) => {
                                            let payload = {
                                                action: {
                                                    type: "showStatistics",
                                                    actionName: "featureImportance",
                                                    target: this.state.featureImportanceColName
                                                },
                                                dataSource: this.state.payload.dataSource
                                            }
                                            this.setState({
                                                featureImportanceLoader: true
                                            })
                                            this.props.getStatistics(payload, `featureImportanceLoader`);
                                        }} >Get Importance</button>
                                    </div>
                                    <label className="form-group-label">Target Column :<i className="fas fa-asterisk form-group-required"></i></label>
                                </div>
                                {/*<button type="button"  className="btn btn-success"></button>*/}
                                <div className="featureModalBox">
                                    {this.state.featureImportanceLoader ?
                                        <div className="fileUploadLoader">
                                            <img src="https://content.iot83.com/m83/misc/uploading.gif" />
                                            <h6>Loading...</h6>
                                        </div> : Object.keys(this.state.featureImportanceData).length ?
                                            <div className="reportBox">
                                                <ul className="nav nav-tabs">
                                                    <li className="nav-item">
                                                        <a className="nav-link active" data-toggle="tab" href="#home">Chart Data</a>
                                                    </li>
                                                    <li className="nav-item">
                                                        <a className="nav-link" data-toggle="tab" href="#menu1">Table</a>
                                                    </li>
                                                </ul>

                                                <div className="tab-content">
                                                    <div className="tab-pane active" id="home">
                                                        <div className="chartBox">
                                                            {this.createChartForFeatureImportanceData(this.state.featureImportanceData, "barChartForMl", "basicBarChart")}
                                                        </div>
                                                    </div>
                                                    <div className="tab-pane fade" id="menu1">
                                                        <div className="contentTableBox mr-0 mr-t-10 table-responsive">
                                                            <table className="table">
                                                                <thead>
                                                                    <tr>
                                                                        {this.state.featureImportanceData.dataKeys.map((el, index) => {
                                                                            return (<th className="bg-transparent text-dark" key={index}>{el}</th>)
                                                                        })}
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        {this.state.featureImportanceData.dataKeys.map((el, elIndex) => {
                                                                            return (
                                                                                <td key={elIndex}>{this.state.featureImportanceData.data[el]}</td>
                                                                            )
                                                                        })}
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> : <p className="noteText">'Click Get Importance button to get the data'
                                            </p>}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="modal animated slideInDown" id="informationGainModal">
                    <div className="modal-dialog modal-xl modal-dialog-scrollable">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h4 className="modal-title text-center">Information Gain</h4>
                                <button type="button" className="close" data-dismiss="modal"><i className="far fa-times"></i></button>
                            </div>
                            <div className="modal-body">
                                <div className="featureModalBox">
                                    {Object.keys(this.state.informationGainData).length ?
                                        <div className="reportBox">
                                            <div className="tab-content">
                                                <div className="contentTableBox mr-0 mr-t-10 table-responsive">
                                                    <table className="table">
                                                        <thead>
                                                            <tr>
                                                                {this.state.informationGainData.dataKeys.map((el, index) => {
                                                                    return (<th className="bg-transparent text-dark" key={index}>{el}</th>)
                                                                })}
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                {this.state.informationGainData.dataKeys.map((el, elIndex) => {
                                                                    return (
                                                                        <td key={elIndex}>{this.state.informationGainData.data[el]}</td>
                                                                    )
                                                                })}
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div> : <p className="noteText">'Click Get Importance button to get the data'
                                            </p>}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="modal animated slideInDown " id="dataCorrelationModal">
                    <div className="modal-dialog modal-xl modal-dialog-scrollable">
                        {this.state.dataCorrelationData.userId &&
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h6 className="modal-title text-center">Data Correlation (Method='Pearson')
                                    <button type="button" className="close" data-dismiss="modal"><i className="far fa-times"></i></button>
                                    </h6>
                                </div>
                                <div className="modal-body">
                                    <div className="reportBox">
                                        <div className="flex">
                                            <div className="flex-item fx-b20">
                                                <div className="contentTableBox mr-0">
                                                    <table className="table">
                                                        <thead>
                                                            <tr>
                                                                <th className="border-r-0"><span className="visibilityHidden">Header</span></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            {this.state.dataCorrelationData.data.map((data, index) => {
                                                                let html = <tr key={index}>
                                                                    {index < this.state.dataCorrelationData.dataKeys.length ?
                                                                        <td className="font-weight-bold text-dark border-r-0">{this.state.dataCorrelationData.dataKeys[index]}</td>
                                                                        : ''}
                                                                </tr>
                                                                return html;
                                                            }
                                                            )}
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div className="flex-item fx-b80">
                                                <div className="contentTableBox mr-0 table-responsive">
                                                    <table className="table">
                                                        <thead>
                                                            <tr>
                                                                {/*<th className="bg-transparent text-dark"></th>*/}
                                                                {this.state.dataCorrelationData.dataKeys.map((el, index) => {
                                                                    return (<th className="bg-transparent text-dark" key={index}>{el}</th>)
                                                                })}
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            {this.state.dataCorrelationData.data.map((data, index) => {
                                                                let html = <tr key={index}>
                                                                    {this.state.dataCorrelationData.dataKeys.map((el, elIndex) => {
                                                                        return (
                                                                            <React.Fragment key={elIndex}>
                                                                                <td>{data[el]}</td>
                                                                            </React.Fragment>
                                                                        )
                                                                    })
                                                                    }
                                                                </tr>
                                                                return html;
                                                            })}
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <p className="noteText pd-l-7 mr-b-20"><strong>Note: </strong>String type and object type column have been dropped, which are - {this.state.dataCorrelationData.droppedColumnList.map((el, index) => { return (`${el}${this.state.dataCorrelationData.droppedColumnList.length - 1 === index ? '.' : ', '}`) })}</p>
                                    </div>
                                </div>
                            </div>}
                    </div>
                </div>

                <div className="modal animated slideInDown" id="dataCorrelationWithCleaningAndTransformModal">
                    <div className="modal-dialog modal-xl modal-dialog-scrollable">
                        {this.state.dataCorrelationDataWithCleaningAndTransform.userId &&
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h6 className="modal-title text-center">Data Correlation (Method='Pearson')
                                    <button type="button" className="close" data-dismiss="modal"><i className="far fa-times"></i></button>
                                    </h6>
                                </div>
                                <div className="modal-body">
                                    <div className="reportBox">
                                        <div className="flex">
                                            <div className="flex-item fx-b20">
                                                <div className="contentTableBox mr-0">
                                                    <table className="table">
                                                        <thead>
                                                            <tr>
                                                                <th className="border-r-0"><span className="visibilityHidden">Header</span></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            {this.state.dataCorrelationDataWithCleaningAndTransform.data.map((data, index) => {
                                                                let html = <tr key={index}>
                                                                    {index < this.state.dataCorrelationDataWithCleaningAndTransform.dataKeys.length ?
                                                                        <td className="font-weight-bold text-dark">{this.state.dataCorrelationDataWithCleaningAndTransform.dataKeys[index]}</td>
                                                                        : ''}
                                                                </tr>
                                                                return html;
                                                            }
                                                            )}
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div className="flex-item fx-b80">
                                                <div className="contentTableBox mr-0 table-responsive">
                                                    <table className="table">
                                                        <thead>
                                                            <tr>
                                                                {/*<th className="bg-transparent text-dark"></th>*/}
                                                                {this.state.dataCorrelationDataWithCleaningAndTransform.dataKeys.map((el, index) => {
                                                                    return (<th className="bg-transparent text-dark" key={index}>{el}</th>)
                                                                })}
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            {this.state.dataCorrelationDataWithCleaningAndTransform.data.map((data, index) => {
                                                                let html = <tr key={index}>
                                                                    {this.state.dataCorrelationDataWithCleaningAndTransform.dataKeys.map((el, elIndex) => {
                                                                        return (
                                                                            <React.Fragment key={elIndex}>
                                                                                <td>{data[el]}</td>
                                                                            </React.Fragment>
                                                                        )
                                                                    })
                                                                    }
                                                                </tr>
                                                                return html;
                                                            })}
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <p className="noteText pd-l-7 mr-b-20"><strong>Note: </strong>String type and object type column have been dropped, which are - {this.state.dataCorrelationData.droppedColumnList.map((el, index) => { return (`${el}${this.state.dataCorrelationData.droppedColumnList.length - 1 === index ? '.' : ', '}`) })}</p>
                                    </div>
                                </div>
                            </div>}
                    </div>
                </div>

                <div className="modal animated slideInDown" id="showMissingValuesModal">
                    <div className="modal-dialog modal-xl modal-dialog-scrollable">
                        {this.state.showMissingValuesData.userId &&
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h6 className="modal-title text-center">Show Missing Values
                                    <button type="button" className="close" data-dismiss="modal"><i className="far fa-times"></i></button>
                                    </h6>
                                </div>
                                <div className="modal-body">
                                    <div className="reportBox">
                                        <div className="contentTableBox mr-0 table-responsive">
                                            <table className="table">
                                                <thead>
                                                    <tr>
                                                        {this.state.showMissingValuesData.dataKeys.map((el, index) => {
                                                            return (<th className="bg-transparent text-dark" key={index}>{el}</th>)
                                                        })}
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {this.state.showMissingValuesData.data.map((data, index) => {
                                                        let html = <tr key={index}>
                                                            {this.state.showMissingValuesData.dataKeys.map((el, elIndex) => {
                                                                return (
                                                                    <td key={elIndex}>{data[el]}</td>
                                                                )
                                                            })}
                                                        </tr>
                                                        return html;
                                                    })}
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>}
                    </div>
                </div>

                <div className="modal animated slideInDown" id="showMissingValuesWithCleaningAndTransformModal">
                    <div className="modal-dialog modal-xl modal-dialog-scrollable">
                        {this.state.showMissingValuesDataWithCleaningAndTransform.userId &&
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h6 className="modal-title text-center">Show Missing Values
                                    <button type="button" className="close" data-dismiss="modal"><i className="far fa-times"></i></button>
                                    </h6>
                                </div>
                                <div className="modal-body">
                                    <div className="reportBox">
                                        <div className="contentTableBox mr-0 table-responsive">
                                            <table className="table">
                                                <thead>
                                                    <tr>
                                                        {this.state.showMissingValuesDataWithCleaningAndTransform.dataKeys.map((el, index) => {
                                                            return (<th className="bg-transparent text-dark" key={index}>{el}</th>)
                                                        })}
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {this.state.showMissingValuesDataWithCleaningAndTransform.data.map((data, index) => {
                                                        let html = <tr key={index}>
                                                            {this.state.showMissingValuesDataWithCleaningAndTransform.dataKeys.map((el, elIndex) => {
                                                                return (
                                                                    <td key={elIndex}>{data[el]}</td>
                                                                )
                                                            })}
                                                        </tr>
                                                        return html;
                                                    })}
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>}
                    </div>
                </div>

                <div className="modal animated slideInDown" id="showSampleDataModal">
                    <div className="modal-dialog modal-xl modal-dialog-scrollable">
                        {this.state.showSampleData.userId &&
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h6 className="modal-title text-center">Sample Data
                                    <button type="button" className="close" data-dismiss="modal"><i className="far fa-times"></i></button>
                                    </h6>
                                </div>
                                <div className="modal-body">
                                    <div className="reportBox">
                                        <div className="contentTableBox mr-0 table-responsive">
                                            <table className="table">
                                                <thead>
                                                    <tr>
                                                        {this.state.showSampleData.dataKeys.map((el, index) => {
                                                            return (<th className="bg-transparent text-dark" key={index}>{el}</th>)
                                                        })}
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {this.state.showSampleData.sample.map((data, index) => {
                                                        let html = <tr key={index}>
                                                            {this.state.showSampleData.dataKeys.map((el, elIndex) => {
                                                                return (
                                                                    <td key={elIndex}>{data[el]}</td>
                                                                )
                                                            })}
                                                        </tr>
                                                        return html;
                                                    })}
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>}
                    </div>
                </div>

                <div className="modal animated slideInDown" id="showSampleDataWithCleaningAndTransformModal">
                    <div className="modal-dialog modal-xl modal-dialog-scrollable">
                        {this.state.showSampleDataWithCleaningAndTransform.userId &&
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h6 className="modal-title text-center">Sample Data
                                    <button type="button" className="close" data-dismiss="modal"><i className="far fa-times"></i></button>
                                    </h6>
                                </div>
                                <div className="modal-body">
                                    <div className="reportBox">
                                        <div className="contentTableBox mr-0 table-responsive">
                                            <table className="table">
                                                <thead>
                                                    <tr>
                                                        {this.state.showSampleDataWithCleaningAndTransform.dataKeys.map((el, index) => {
                                                            return (<th className="bg-transparent text-dark" key={index}>{el}</th>)
                                                        })}
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {this.state.showSampleDataWithCleaningAndTransform.sample.map((data, index) => {
                                                        let html = <tr key={index}>
                                                            {this.state.showSampleDataWithCleaningAndTransform.dataKeys.map((el, elIndex) => {
                                                                return (
                                                                    <td key={elIndex}>{data[el]}</td>
                                                                )
                                                            })}
                                                        </tr>
                                                        return html;
                                                    })}
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>}
                    </div>
                </div>

                {this.state.isOpen &&
                    <NotificationModal
                        type={this.state.modalType}
                        message2={this.state.message2}
                        onCloseHandler={this.onCloseHandler}
                    />
                }
            </div >

        );
    }
}

AddOrEditMl.propTypes = {
    dispatch: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
    getConnectorsSuccess: Selectors.getConnectorsSuccess(),
    getConnectorsFailure: Selectors.getConnectorsFailure(),
    saveExperimentSuccess: Selectors.saveExperimentSuccess(),
    saveExperimentFailure: Selectors.saveExperimentFailure(),
    getExperimentByIdSuccess: Selectors.getExperimentByIdSuccess(),
    getExperimentByIdFailure: Selectors.getExperimentByIdFailure(),
    getColumnsSuccess: Selectors.getColumnsSuccess(),
    getColumnsFailure: Selectors.getColumnsFailure(),
    getDataConnectorsListSuccess: Selectors.getDataConnectorsListSuccess(),
    getDataConnectorsListFailure: Selectors.getDataConnectorsListFailure(),
    getStatisticsSuccess: Selectors.getStatisticsSuccess(),
    getStatisticsFailure: Selectors.getStatisticsFailure(),
});

function mapDispatchToProps(dispatch) {
    return {
        dispatch,
        getConnectorsByCategory: (category) => dispatch(Actions.getConnectorsByCategory(category)),
        getColumns: (payload) => dispatch(Actions.getColumns(payload)),
        saveExperiment: (payload) => dispatch(Actions.saveExperiment(payload)),
        getMLExperimentById: (id) => dispatch(Actions.getMLExperimentById(id)),
        getDataConnectorsList: () => dispatch(Actions.getDataConnectorsList()),
        getStatistics: (payload, loader) => dispatch(Actions.getStatistics(payload, loader)),
        resetToInitialState: () => dispatch(Actions.resetToInitialState()),
    };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "addOrEditMl", reducer });
const withSaga = injectSaga({ key: "addOrEditMl", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(AddOrEditMl);
