/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { apiCallHandler } from '../../api';
import { takeEvery } from 'redux-saga';
import { put } from 'redux-saga/effects';
import * as CONSTANTS from './constants'




export function* getConnectorsByCategoryHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_CONNECTORS_BY_CATEGORY_SUCCESS, CONSTANTS.GET_CONNECTORS_BY_CATEGORY_FAILURE, 'getConnectorsByCategory')];
}

export function* saveExperimentApiHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.SAVE_EXPERIMENT_SUCCESS, CONSTANTS.SAVE_EXPERIMENT_FAILURE, 'saveExperiment')];
}

export function* getExperimentByIdHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_EXPERIMENT_BY_ID_SUCCESS, CONSTANTS.GET_EXPERIMENT_BY_ID_FAILURE, 'getExperimentById')];
}

export function* getColumnsApiHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_COLUMNS_SUCCESS, CONSTANTS.GET_COLUMNS_FAILURE, 'getColumnsForMLStudio')];
}

export function* apiGetDataConnectors(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_DATA_CONNECTORS_LIST_SUCCESS, CONSTANTS.GET_DATA_CONNECTORS_LIST_FAILURE, 'getDataConnectors')];
}

export function* apiGetStatistics(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_STATISTICS_SUCCESS, CONSTANTS.GET_STATISTICS_FAILURE, 'getStatisticsMlExperiment')];
}

export function* watcherGetConnectorsByCategory() {
  yield takeEvery(CONSTANTS.GET_CONNECTORS_BY_CATEGORY, getConnectorsByCategoryHandlerAsync);
}

export function* watcherSaveExperiment() {
  yield takeEvery(CONSTANTS.SAVE_EXPERIMENT, saveExperimentApiHandlerAsync);
}

export function* watcherGetExperimentById() {
  yield takeEvery(CONSTANTS.GET_EXPERIMENT_BY_ID, getExperimentByIdHandlerAsync);
}

export function* watcherGetColumns() {
  yield takeEvery(CONSTANTS.GET_COLUMNS, getColumnsApiHandlerAsync);
}

export function* watcherGetDataConnectors() {
  yield takeEvery(CONSTANTS.GET_DATA_CONNECTORS_LIST, apiGetDataConnectors);
}

export function* watcherGetStatistics() {
  yield takeEvery(CONSTANTS.GET_STATISTICS, apiGetStatistics);
}

export default function* rootSaga() {
  yield [
    watcherGetConnectorsByCategory(),
    watcherSaveExperiment(),
    watcherGetExperimentById(),
    watcherGetColumns(),
    watcherGetDataConnectors(),
    watcherGetStatistics(),
  ];
}