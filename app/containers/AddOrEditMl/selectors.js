/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { createSelector } from "reselect";
import { initialState } from "./reducer";


const selectAddOrEditMlDomain = state => state.get("addOrEditMl", initialState);

export const getConnectorsSuccess = () => createSelector(selectAddOrEditMlDomain, substate => substate.getConnectorsSuccess);
export const getConnectorsFailure = () => createSelector(selectAddOrEditMlDomain, substate => substate.getConnectorsFailure);

export const saveExperimentSuccess = () => createSelector(selectAddOrEditMlDomain, substate => substate.saveExperimentSuccess);
export const saveExperimentFailure = () => createSelector(selectAddOrEditMlDomain, substate => substate.saveExperimentFailure);

export const getExperimentByIdSuccess = () => createSelector(selectAddOrEditMlDomain, substate => substate.getExperimentByIdSuccess);
export const getExperimentByIdFailure = () => createSelector(selectAddOrEditMlDomain, substate => substate.getExperimentByIdFailure);

export const getColumnsSuccess = () => createSelector(selectAddOrEditMlDomain, substate => substate.getColumnsSuccess);
export const getColumnsFailure = () => createSelector(selectAddOrEditMlDomain, substate => substate.getColumnsFailure);

export const getDataConnectorsListSuccess = () => createSelector(selectAddOrEditMlDomain, substate => substate.getDataConnectorsListSuccess);
export const getDataConnectorsListFailure = () => createSelector(selectAddOrEditMlDomain, substate => substate.getDataConnectorsListFailure);

export const getStatisticsSuccess = () => createSelector(selectAddOrEditMlDomain, substate => substate.getStatisticsSuccess);
export const getStatisticsFailure = () => createSelector(selectAddOrEditMlDomain, substate => substate.getStatisticsFailure);

