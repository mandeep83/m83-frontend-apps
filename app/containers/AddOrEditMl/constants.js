/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

export const RESET_TO_INITIAL_STATE = "app/AddOrEditMl/RESET_TO_INITIAL_STATE";


export const GET_CONNECTORS_BY_CATEGORY = "app/AddOrEditMl/GET_CONNECTORS_BY_CATEGORY";
export const GET_CONNECTORS_BY_CATEGORY_SUCCESS = "app/AddOrEditMl/GET_CONNECTORS_BY_CATEGORY_SUCCESS";
export const GET_CONNECTORS_BY_CATEGORY_FAILURE = "app/AddOrEditMl/GET_CONNECTORS_BY_CATEGORY_FAILURE";

export const SAVE_EXPERIMENT = "app/AddOrEditMl/SAVE_EXPERIMENT";
export const SAVE_EXPERIMENT_SUCCESS = "app/AddOrEditMl/SAVE_EXPERIMENT_SUCCESS";
export const SAVE_EXPERIMENT_FAILURE = "app/AddOrEditMl/SAVE_EXPERIMENT_FAILURE";

export const GET_EXPERIMENT_BY_ID = "app/AddOrEditMl/GET_EXPERIMENT_BY_ID";
export const GET_EXPERIMENT_BY_ID_SUCCESS = "app/AddOrEditMl/GET_EXPERIMENT_BY_ID_SUCCESS";
export const GET_EXPERIMENT_BY_ID_FAILURE = "app/AddOrEditMl/GET_EXPERIMENT_BY_ID_FAILURE";

export const GET_COLUMNS = "app/AddOrEditMl/GET_COLUMNS";
export const GET_COLUMNS_SUCCESS = "app/AddOrEditMl/GET_COLUMNS_SUCCESS";
export const GET_COLUMNS_FAILURE = "app/AddOrEditMl/GET_COLUMNS_FAILURE";


export const GET_DATA_CONNECTORS_LIST = "app/AddOrEditMl/GET_DATA_CONNECTORS";
export const GET_DATA_CONNECTORS_LIST_SUCCESS = "app/AddOrEditMl/GET_DATA_CONNECTORS_SUCCESS";
export const GET_DATA_CONNECTORS_LIST_FAILURE = "app/AddOrEditMl/GET_DATA_CONNECTORS_FAILURE";


export const GET_STATISTICS = "app/AddOrEditMl/GET_STATISTICS";
export const GET_STATISTICS_SUCCESS = "app/AddOrEditMl/GET_STATISTICS_SUCCESS";
export const GET_STATISTICS_FAILURE = "app/AddOrEditMl/GET_STATISTICS_FAILURE";
