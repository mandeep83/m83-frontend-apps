/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * DeviceDashboardsDeviceMap
 *
 */

import React, { Fragment } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import ReactSelect from "react-select";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import { allSelectors as SELECTORS } from "./selectors";
import { allActions as ACTIONS } from './actions'
import reducer from "./reducer";
import saga from "./saga";
import { isEqual, cloneDeep } from 'lodash'
import GoogleMap, { getMapZoom } from "../../components/GoogleMap";
import AddNewButton from "../../components/AddNewButton/Loadable"
import Loader from "../../components/Loader/Loadable";
import {getAddressFromLatLng, isNavAssigned} from "../../commonUtils"
import Slider from 'react-input-slider';
import MapSearchBox from "../../components/MapSearchBox";
import SearchBarWithSuggestion from "../../components/SearchBarWithSuggestion";
import CollapseOrShowAllFilters from "../../components/CollapseOrShowAllFilters";

let DEFAULT_FILTERS = {
    keyword: "",
    groupId: [],
    tagId: [],
    alarmCategories: [],
}

const PAGE_TITLE = {
    "/deviceList": "Device List",
    "/deviceMap": "Map & Geofencing "
}

/* eslint-disable react/prefer-stateless-function */
export const ALARM_CATEGORIES = [
    {
        name: "critical",
        displayName: "Critical",
        className: "bg-critical",
        icon: "far fa-exclamation-triangle",
    },
    {
        name: "major",
        displayName: "Major",
        className: "bg-major",
        icon: "far fa-exclamation-square",
    },
    {
        name: "minor",
        displayName: "Minor",
        className: "bg-minor",
        icon: "far fa-exclamation-circle",
    },
    {
        name: "warning",
        displayName: "Warning",
        className: "bg-alarm-warning",
        icon: "far fa-exclamation",
    },
]

let STATUS_TYPES = [
    {
        type: "online",
        displayName: "Online",
    },
    {
        type: "offline",
        displayName: "Offline",
    },
    {
        type: "not-connected",
        displayName: "Not Connected",
    },
]

let DEFAULT_GEOFENCING = {
    status: false,
    radius: 140,
    lat: "",
    lng: "",
    locationName: ""
}

export class DeviceDashboardsDeviceMap extends React.Component {
    state = {
        filterExpand: true,
        deviceTypeList: [],
        isFetching: true,
        deviceGroups: [],
        filters: cloneDeep(DEFAULT_FILTERS),
        deviceTags: [],
        devicesMetaData: {},
        allDevices: [],
        selectedDeviceTypes: [],
        geofencing: { ...DEFAULT_GEOFENCING },
        movableDeviceTypes: [],
    }

    shouldComponentUpdate(nextProps, nextState) {
        return !isEqual(this.props.data, nextProps.data) || !isEqual(nextState, this.state) || !isEqual(nextProps.widgetProperties, this.props.widgetProperties)
    }

    componentDidMount() {
        this.props.deviceTypeList()
    }
    
    static getDerivedStateFromProps(nextProps, state) {
        let newProp = Object.keys(SELECTORS).find(prop => nextProps[prop])
        if (newProp) {
            let propData = nextProps[newProp].error ? nextProps[newProp].error : nextProps[newProp].response
            if (nextProps.deviceTypeListSuccess) {
                let deviceTypeList = propData
                let selectedDeviceTypes = []
                let deviceGroups = []
                let deviceTags = []
                let allDeviceGroups = {}
                let allDeviceTags = {}
                let payload = {
                    deviceTypeIds: [],
                };

                let movableDeviceTypes = []

                deviceTypeList.forEach(deviceType => {
                    selectedDeviceTypes.push({
                        label: deviceType.name,
                        value: deviceType.id,
                        zoomLevel: deviceType.zoomLevel
                    });

                    let isMovable = deviceType.assetType === "Movable"
                    if(isMovable){
                        movableDeviceTypes.push(deviceType.id)
                    }

                    Array.prototype.push.apply(deviceGroups, deviceType.deviceGroups);
                    Array.prototype.push.apply(deviceTags, deviceType.tags);

                    allDeviceGroups[deviceType.id] = deviceType.deviceGroups
                    allDeviceTags[deviceType.id] = deviceType.tags

                    payload.deviceTypeIds.push(deviceType.id);
                })
                selectedDeviceTypes.length > 0 && nextProps.getAllDevices(payload)
                return {
                    isFetching: false,
                    deviceTypeList,
                    selectedDeviceTypes,
                    allDeviceGroups,
                    allDeviceTags,
                    deviceGroups,
                    deviceTags,
                    defaultZoomLevel: deviceTypeList.length === 1 && deviceTypeList[0].zoomLevel,
                    movableDeviceTypes,
                }
            }
            else if (nextProps.deviceTypeListFailure) {
                return { isFetching: false }
            }
            else if (nextProps.getDeviceTagsSuccess) {
                return {
                    isFetchingTags: false,
                    deviceTags: propData,
                }
            }
            else if (nextProps.getAllDevicesSuccess) {
                let devicesMetaData = {}
                let isFilteredList = nextProps.getAllDevicesSuccess.deviceTypeIds.length !== state.deviceTypeList.length
                if (isFilteredList) {
                    devicesMetaData = cloneDeep(state.devicesMetaData)
                    nextProps.getAllDevicesSuccess.deviceTypeIds.map(deviceTypeId => {
                        devicesMetaData[deviceTypeId] = undefined
                    })
                }
                propData.map(device => {
                    if (!devicesMetaData[device.deviceTypeId]) {
                        devicesMetaData[device.deviceTypeId] = [device]
                    }
                    else {
                        devicesMetaData[device.deviceTypeId].push(device)
                    }
                })
                let allDevices = propData.length > 0 ? Object.values(devicesMetaData).reduce((acc, devices) => [...acc, ...devices]) : [];

                return {
                    isFetchingDevices: false,
                    isFetching: false,
                    devicesMetaData,
                    allDevices
                }
            }
            else if (nextProps.getAllDevicesFailure) {
                nextProps.showNotification("error", propData, () => {
                })
                return {
                    isFetchingDevices: false,
                    isFetching: false,
                }
            }
            else if (nextProps.getDeviceTagsFailure) {
                return {
                    isFetchingTags: false,
                }
            }
            else if (nextProps.saveMapZoomLevelSuccess) {
                nextProps.showNotification("success", "Zoom Level Saved Successfully")
                let deviceTypeList = cloneDeep(state.deviceTypeList)
                let requiredDeviceType = deviceTypeList.find(deviceType => deviceType.id === nextProps[newProp].deviceTypeDetails.deviceTypeId)
                if (requiredDeviceType) {
                    requiredDeviceType.zoomLevel = nextProps[newProp].deviceTypeDetails.zoomLevel
                }
                return { savingZoomLevel: false, deviceTypeList }
            }
            else if (nextProps.saveMapZoomLevelFailure) {
                nextProps.showNotification("error", propData)
                return { savingZoomLevel: false }
            }
            else if (newProp.includes("Failure")) {
                nextProps.showNotification("error", propData)
            }
        }
        return null
    }
    
    componentDidUpdate(prevProps, prevState) {
        let newProp = Object.keys(SELECTORS).find(prop => this.props[prop])
        if (newProp) {
            this.props.resetToInitialStateProps(newProp)
        }
    }

    filterChangeHandler(key, value) {
        let filters = cloneDeep(this.state.filters)
        let previouslyCheckedIndex = filters[key].indexOf(value)
        if (Array.isArray(value)) {
            filters[key] = []
        } else {
            if (previouslyCheckedIndex != -1) {
                filters[key].splice(previouslyCheckedIndex, 1)
            } else {
                filters[key].push(value)
            }
        }
        this.setState({
            filters
        })
    }

    statusFilterChangeHandler = (statusType) => {
        this.setState(prevState => ({
            status: statusType === prevState.status ? "" : statusType
        }))
    }
    
    getFilteredItems = () => {
        let filteredItems = this.state.allDevices.filter(device => {
            let deviceTypeValidation = this.state.selectedDeviceTypes.length === this.state.deviceTypeList.length || this.state.selectedDeviceTypes.some(deviceType => deviceType.value === device.deviceTypeId)
            let groupIdValidation = !this.state.filters.groupId.length || this.state.filters.groupId.includes(device.groupId)
            let tagIdValidation = !this.state.filters.tagId.length || this.state.filters.tagId.includes(device.tagId)
            let keywordValidation = !this.state.filters.keyword || (([(device.name || "").toLowerCase(), device.id.toLowerCase()].includes(this.state.filters.keyword.toLowerCase()) || device.id.toLowerCase().includes(this.state.filters.keyword.toLowerCase())))
            let alarmCategoryValidation = !this.state.filters.alarmCategories.length || this.state.filters.alarmCategories.some(category => device.alarmsCount[category])
            let geolocationValidation = this.state.geofencing.status && this.state.geofencing.lat ? this.isInsideGeofence(device.lat, device.lng) : true
            return deviceTypeValidation && groupIdValidation && tagIdValidation && geolocationValidation && keywordValidation && alarmCategoryValidation
        })
        return filteredItems
    }

    refreshDeviceList = () => {
        let payload = {
            deviceTypeIds: [],
        };

        payload.deviceTypeIds = this.state.selectedDeviceTypes.map(deviceType => deviceType.value)

        this.setState({
            isFetchingDevices: true,
            isFetching: true,
        }, () => this.props.getAllDevices(payload));
    }

    isInsideGeofence(deviceLat, deviceLng) {
        var ky = 40000 / 360;
        var kx = Math.cos(Math.PI * this.state.geofencing.lat / 180.0) * ky;
        var dx = Math.abs(this.state.geofencing.lng - deviceLng) * kx;
        var dy = Math.abs(this.state.geofencing.lat - deviceLat) * ky;
        return Math.sqrt(dx * dx + dy * dy) <= this.state.geofencing.radius;
    }

    setLocation = (locationName, lat, lng) => {
        let geofencing = cloneDeep(this.state.geofencing)
        geofencing.lat = lat
        geofencing.lng = lng
        geofencing.locationName = locationName
        this.setState({
            geofencing,
        })
    }

    toggleGeofencing = () => {
        let geofencing = cloneDeep(this.state.geofencing)
        geofencing.status = !geofencing.status
        if (geofencing.status && !geofencing.lat && navigator.geolocation) {
            navigator.geolocation.getCurrentPosition((position) => {
                getAddressFromLatLng(position.coords.latitude, position.coords.longitude, this.setLocation)
            });
        }
        this.setState({ geofencing })
    }

    locationChangeHandler = ({ currentTarget }) => {
        let geofencing = cloneDeep(this.state.geofencing)
        geofencing.locationName = currentTarget.value
        this.setState({
            geofencing
        })
    }
    getActiveClass = (key, value) => {
        return this.state.filters[key].includes(value) ? "active" : ""
    }

    searchHandler = (value) => {
        let filters = cloneDeep(this.state.filters)
        filters.keyword = value
        this.setState({ filters })
    }

    saveMapZoomLevel = () => {
        let payload = {
            "deviceTypeId": this.state.selectedDeviceTypes[0].value,
            "zoomLevel": getMapZoom()
        }
        this.setState({
            savingZoomLevel: true
        }, () => this.props.saveMapZoomLevel(payload))
    }

    getDeviceTypes = () => {
        let deviceTypeList = this.state.deviceTypeList,
            list = [];

        deviceTypeList.map(deviceType => {
            list.push({
                label: deviceType.name,
                value: deviceType.id,
            })
        })
        return list;
    }

    deviceTypeChangeHandler = (selectedDeviceTypes) => {
        let deviceGroups = []
        let deviceTags = []
        selectedDeviceTypes.map(deviceType => {
            Array.prototype.push.apply(deviceGroups, this.state.allDeviceGroups[deviceType.value]);
            Array.prototype.push.apply(deviceTags, this.state.allDeviceTags[deviceType.value]);
        })

        this.setState({
            selectedDeviceTypes,
            defaultZoomLevel: selectedDeviceTypes.length === 1 && this.state.deviceTypeList.find(deviceType => deviceType.id === selectedDeviceTypes[0].value).zoomLevel,
            deviceGroups,
            deviceTags,
            filters: cloneDeep(DEFAULT_FILTERS),
            geofencing: { ...DEFAULT_GEOFENCING },
        })
    }

    filterExpandCollapseHandler = () => {
        this.setState({
            filterExpand: this.state.filterExpand ? false : true
        })
    }

    getDataForSuggestions = () => {
        let suggestionsData = []
        Object.entries(this.state.devicesMetaData).map(([deviceType, devices]) => {
            if (this.state.selectedDeviceTypes.some(el => el.value === deviceType)) {
                suggestionsData.push(...devices)
            }
        })
        return suggestionsData
    }

    render() {
        let filteredDevices = this.getFilteredItems()
        let onlineDevices = 0
        let offlineDevices = 0
        let disconnectedDevices = 0
        filteredDevices.map(device => {
            if (device.status === "online") onlineDevices++;
            else if (device.status === "offline") offlineDevices++;
            else disconnectedDevices++
        })
        
        return (
            <React.Fragment>
                <Helmet>
                    <title>{PAGE_TITLE[window.location.pathname]}</title>
                    <meta name="description" content="M83-Device Map" />
                </Helmet>

                <header className="content-header d-flex">
                    <div className="flex-60">
                        <h6>{PAGE_TITLE[window.location.pathname]}</h6>
                    </div>
                    <div className="flex-40 text-right">
                        <div className="content-header-group">
                            <button className="btn btn-light" data-tooltip data-tooltip-text="Refresh" data-tooltip-place="bottom" disabled={this.state.isFetchingDevices || this.state.isFetching} onClick={this.refreshDeviceList} ><i className="far fa-sync-alt"></i></button>
                            <button className={`btn btn-light ${this.state.filterExpand && "active"}`} data-tooltip data-tooltip-text="Filters" data-tooltip-place="bottom" disabled={this.state.isFetchingDevices || this.state.isFetching || !Boolean(this.state.deviceTypeList.length)} onClick={() => this.filterExpandCollapseHandler()} ><i className="far fa-filter"></i></button>
                            {/*{this.state.selectedDeviceTypes.length == 1 &&
                                <button className="btn btn-primary" disabled={this.state.savingZoomLevel} onClick={this.saveMapZoomLevel}>
                                    {this.state.savingZoomLevel ? <i className="far fa-cog fa-spin mr-r-5"></i> : <i className="far fa-map-marker-alt mr-r-5"></i>}
                                    {this.state.savingZoomLevel ? "Saving" : "Set zoom level"}
                                </button>
                            }*/}
                        </div>
                    </div>
                </header>

                {this.state.isFetching ?
                    <Loader /> :
                    <React.Fragment>
                        {this.state.deviceTypeList.length ?
                            <React.Fragment>
                                {this.state.filterExpand &&
                                    <div className="device-filter-wrapper">
                                        <div className="device-filter-box">
                                            <h5>Filters <CollapseOrShowAllFilters /></h5>
                                            <h6>Specify Your Search Criteria</h6>

                                            {/*<div className="form-group mb-0">
                                                <div className="search-wrapper">
                                                    <SearchBarWithSuggestion value={this.state.filters.keyword} suggestionKeys={["name", "id"]} onChange={this.searchHandler} data={this.getDataForSuggestions()} />
                                                </div>
                                            </div>*/}
                                        </div>
                                        <div className="device-filter-box">
                                            <p>Device Type <i className="fas fa-angle-down device-filter-collapse" data-toggle="collapse" data-target="#deviceType__filter__"></i></p>
                                            <div className="collapse show" id="deviceType__filter__">
                                                <div className="device-filter-body">
                                                    <div className="form-group mb-0">
                                                        <ReactSelect
                                                            id="deviceTypes"
                                                            options={this.getDeviceTypes()}
                                                            value={this.state.selectedDeviceTypes}
                                                            onChange={this.deviceTypeChangeHandler}
                                                            isMulti={true}
                                                            isDisabled={this.state.isFetchingDevices}
                                                            className="form-control-multi-select">
                                                        </ReactSelect>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="device-filter-box">
                                            <p>Groups <i className="fas fa-angle-down device-filter-collapse" data-toggle="collapse" data-target="#groups__filter__"></i>
                                                {this.state.filters.groupId.length > 0 &&
                                                    <button className="btn btn-link" onClick={() => this.filterChangeHandler("groupId", [])}>Reset</button>
                                                }
                                            </p>
                                            <div className="collapse show" id="groups__filter__">
                                                <div className="device-filter-body">
                                                    <ul className="d-flex list-style-none device-filter-group">
                                                        {this.state.deviceGroups.length ?
                                                            this.state.deviceGroups.map(deviceGroup =>
                                                                <li key={deviceGroup.deviceGroupId} className={this.getActiveClass("groupId", deviceGroup.deviceGroupId)} onClick={() => this.filterChangeHandler("groupId", deviceGroup.deviceGroupId)}>{deviceGroup.name}</li>) :
                                                            <li><p>No Device Groups Found !</p></li>
                                                        }
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="device-filter-box">
                                            <p>Tags <i className="fas fa-angle-down device-filter-collapse" data-toggle="collapse" data-target="#tags__filter__"></i>
                                                {this.state.filters.tagId.length > 0 &&
                                                    <button className="btn btn-link" onClick={() => this.filterChangeHandler("tagId", [])}>Reset</button>
                                                }
                                            </p>
                                            <div className="collapse show" id="tags__filter__">
                                                <div className="device-filter-body">
                                                    <ul className="d-flex list-style-none device-filter-group">
                                                        {this.state.isFetchingTags ?
                                                            <div className="text-center flex-100"><i className="fad fa-sync-alt fa-spin f-13 text-theme"></i></div>
                                                            :
                                                            this.state.deviceTags.length ? this.state.deviceTags.map(deviceTag =>
                                                                <li key={deviceTag.id}
                                                                    className={this.getActiveClass("tagId", deviceTag.id)}
                                                                    onClick={() => this.filterChangeHandler("tagId", deviceTag.id)}>{deviceTag.tag}</li>
                                                            ) : <li><p>No Tags found</p></li>
                                                        }
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="device-filter-box">
                                            <p>Alarms <i className="fas fa-angle-down device-filter-collapse" data-toggle="collapse" data-target="#alarms__filter__"></i>
                                                {this.state.filters.alarmCategories.length > 0 &&
                                                    <button className="btn btn-link" onClick={() => this.state.filters.alarmCategories.length && this.filterChangeHandler("alarmCategories", [])}>Reset</button>
                                                }
                                            </p>
                                            <div className="collapse show"  id="alarms__filter__">
                                                <div className="device-filter-body">
                                                    <ul className="d-flex list-style-none device-filter-alarms">
                                                        {ALARM_CATEGORIES.map(category =>
                                                            <li key={category.name} className={this.getActiveClass("alarmCategories", category.name)} onClick={() => this.filterChangeHandler("alarmCategories", category.name)}><span className={category.className}><i className={category.icon}></i></span> <p>{category.displayName}</p></li>
                                                        )}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="device-filter-box">
                                            <p>Status <i className="fas fa-angle-down device-filter-collapse" data-toggle="collapse" data-target="#status__filter__"></i>
                                                {this.state.status &&
                                                    <button className="btn btn-link" onClick={() => this.setState({ status: "" })}>Reset</button>
                                                }
                                            </p>
                                            <div className="collapse show" id="status__filter__">
                                                <div className="device-filter-body">
                                                    {STATUS_TYPES.map((status, i) =>
                                                        <label className="radio-button" key={i}>
                                                            <span className="radio-button-text">{status.displayName}</span>
                                                            <input type="radio" checked={status.type === this.state.status} onChange={() => this.statusFilterChangeHandler(status.type)} />
                                                            <span className="radio-button-mark"></span>
                                                        </label>
                                                    )}
                                                </div>
                                            </div>
                                        </div>
                                        <div className="device-filter-box">
                                            <p>Geo-Fencing <i className="fas fa-angle-down device-filter-collapse" data-toggle="collapse" data-target="#geolocation__filter__"></i></p>
                                            <div className="collapse" id="geolocation__filter__">
                                                <div className="device-filter-body">
                                                    <div className="form-group">
                                                        <label className="form-group-label">Geo-Fencing :
                                                            <div className="toggle-switch-wrapper">
                                                                <span>OFF</span>
                                                                <label className="toggle-switch">
                                                                    <input type="checkbox" checked={this.state.geofencing.status} onChange={this.toggleGeofencing} />
                                                                    <span className="toggle-switch-slider"></span>
                                                                </label>
                                                                <span>ON</span>
                                                            </div>
                                                        </label>
                                                    </div>
                                                    <div className="form-group">
                                                        <label className="form-group-label">Geo-Fencing Center Location :</label>
                                                        <div className="search-wrapper">
                                                            <MapSearchBox disabled={!this.state.geofencing.status} value={this.state.geofencing.locationName} onChange={this.locationChangeHandler} onSelect={this.setLocation} />
                                                        </div>
                                                    </div>
                                                    <div className="form-group">
                                                        <label className="form-group-label">Geo-Fencing Radius :</label>
                                                        <div className="range-slider-wrapper">
                                                            <Slider
                                                                disabled={!this.state.geofencing.status}
                                                                axis="x"
                                                                xmin={0}
                                                                xmax={2000}
                                                                x={this.state.geofencing.radius}
                                                                onChange={({ x }) => {
                                                                    let geofencing = cloneDeep(this.state.geofencing)
                                                                    geofencing.radius = x
                                                                    this.setState({ geofencing })
                                                                }}
                                                            />
                                                            <div className="form-control range-slider-value">{this.state.geofencing.radius}</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                }

                                <div className="content-body device-filter-content" style={{paddingRight: this.state.filterExpand ? 270 : 12 }}>
                                    <ul className="list-style-none d-flex device-counter-list">
                                        <li className="flex-25">
                                            <div className="device-counter-card">
                                                <span className="bg-primary"></span>
                                                <h4>{filteredDevices.length}</h4>
                                                <p>All devices</p>
                                                <div className="device-counter-icon">
                                                    <img src="https://content.iot83.com/m83/misc/icons/totalDevice.png" />
                                                    <div className="device-counter-status bg-primary"></div>
                                                </div>
                                            </div>
                                        </li>
                                        <li className="flex-25">
                                            <div className="device-counter-card">
                                                <span className="bg-green"></span>
                                                <h4>{onlineDevices}</h4>
                                                <p>Online devices</p>
                                                <div className="device-counter-icon">
                                                    <img src="https://content.iot83.com/m83/misc/icons/onlineDevice.png" />
                                                    <div className="device-counter-status bg-green"></div>
                                                </div>
                                            </div>
                                        </li>
                                        <li className="flex-25">
                                            <div className="device-counter-card">
                                                <span className="bg-red"></span>
                                                <h4>{offlineDevices}</h4>
                                                <p>Offline devices</p>
                                                <div className="device-counter-icon">
                                                    <img src="https://content.iot83.com/m83/misc/icons/offlineDevice.png" />
                                                    <div className="device-counter-status bg-red"></div>
                                                </div>
                                            </div>
                                        </li>
                                        <li className="flex-25">
                                            <div className="device-counter-card">
                                                <span className="bg-gray"></span>
                                                <h4>{disconnectedDevices}</h4>
                                                <p>Not connected devices</p>
                                                <div className="device-counter-icon">
                                                    <img src="https://content.iot83.com/m83/misc/icons/notConnectedDevice.png" />
                                                    <div className="device-counter-status bg-gray"></div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>

                                    <div className="device-map-wrapper">
                                        <GoogleMap
                                            isFor="deviceMap"
                                            geofencing={this.state.geofencing}
                                            markers={filteredDevices.filter(device => device.lat && (!this.state.status || device.status === this.state.status))}
                                            history={this.props.history}
                                            zoom={this.state.defaultZoomLevel}
                                            movableDeviceTypes={this.state.movableDeviceTypes}
                                        />
                                        <div className="alert alert-info note-text">
                                            <p>
                                                <strong>Note :</strong> Devices will not be available on Map until their location is configured. Don't see all your devices ?
                                                <button className="btn btn-link p-0 f-12 ml-1" onClick={() => this.props.history.push(`devices`)}>Click here</button> to configure.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </React.Fragment>
                            :
                            <div className="content-body">
                                <AddNewButton
                                    text1="No device(s) available"
                                    text2="You haven't created any device type(s) yet. Please create a device type first."
                                    addButtonEnable={isNavAssigned("deviceTypes")}
                                    createItemOnAddButtonClick={() => this.props.history.push(`/addOrEditDeviceType`)}
                                    imageIcon="addDeviceMap.png"
                                />
                            </div>
                        }
                    </React.Fragment>
                }
            </React.Fragment>
        )

    }
}

DeviceDashboardsDeviceMap.propTypes = {
    dispatch: PropTypes.func.isRequired
};

let allSelectors = {}
Object.entries(SELECTORS).map(([key, value]) => {
    allSelectors[key] = value()
})

const mapStateToProps = createStructuredSelector(allSelectors)

function mapDispatchToProps(dispatch) {
    let allActions = { dispatch }
    Object.entries(ACTIONS).map(([key, value]) => {
        allActions[key] = (...args) => dispatch(value(...args))
    })
    return allActions
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({
    key: "deviceDashboardsDeviceMap",
    reducer
});
const withSaga = injectSaga({ key: "deviceDashboardsDeviceMap", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(DeviceDashboardsDeviceMap);
