/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/*
 *
 * PagePreview constants
 *
 */

export const RESET_TO_INITIAL_STATE = "app/PagePreview/RESET_TO_INITIAL_STATE";
export const RESET_TO_INITIAL_STATE_PROPS = "app/PagePreview/RESET_TO_INITIAL_STATE_PROPS";

export const GET_PAGE_BY_ID = {
    action : 'app/PagePreview/GET_PAGE_BY_ID',
    success: "app/PagePreview/GET_PAGE_BY_ID_SUCCESS",
    failure: "app/PagePreview/GET_PAGE_BY_ID_FAILURE",
    urlKey: "getPageById",
    successKey: "getPageByidSuccess",
    failureKey: "getPageByidFailure",
    actionName: "getPageById",
    actionArguments : ["id"]
}

export const SAVE_PAGE = {
    action : 'app/PagePreview/SAVE_PAGE',
    success: "app/PagePreview/SAVE_PAGE_SUCCESS",
    failure: "app/PagePreview/SAVE_PAGE_FAILURE",
    urlKey: "savePage",
    successKey: "savePageSuccess",
    failureKey: "savePageFailure",
    actionName: "savePage",
    actionArguments : ["payload", "id"]
}

export const INVOKE_API = {
    action : 'app/PagePreview/INVOKE_API',
    success: "app/PagePreview/INVOKE_API_SUCCESS",
    failure: "app/PagePreview/INVOKE_API_FAILURE",
    urlKey: "invokeApi",
    successKey: "invokeApiSuccess",
    failureKey: "invokeApiFailure",
    actionName: "invokeApi",
    actionArguments : ["uri", "index", "method"]
}

export const GET_ALL_GATEWAY_APIS = {
    action : 'app/PagePreview/GET_ALL_GATEWAY_APIS',
    success: "app/PagePreview/GET_ALL_GATEWAY_APIS_SUCCESS",
    failure: "app/PagePreview/GET_ALL_GATEWAY_APIS_FAILURE",
    urlKey: "getAllGatewayApis",
    successKey: "getAllGatewayApisSuccess", 
    failureKey: "getAllGatewayApisFailure",
    actionName: "getAllGatewayApis",
}

export const UPLOAD_IMAGE = {
    action : 'app/PagePreview/UPLOAD_IMAGE',
    success: "app/PagePreview/UPLOAD_IMAGE_SUCCESS",
    failure: "app/PagePreview/UPLOAD_IMAGE_FAILURE",
    urlKey: "fileUploadRequest",
    successKey: "uploadImageSuccess",
    failureKey: "uploadImageFailure",
    actionName: "uploadImage",
    actionArguments : ["payload", "imageType"]
}

export const GET_ALL_PAGES = { //changed
    action : 'app/PagePreview/GET_ALL_PAGES',
    success: "app/PagePreview/GET_ALL_PAGES_SUCCESS",
    failure: "app/PagePreview/GET_ALL_PAGES_FAILURE",
    urlKey: "getAllPages", //changed
    successKey: "getAllPagesSuccess",
    failureKey: "getAllPagesFailure",
    actionName: "getAllPages",
}

export const GET_ALL_CUSTOM_WIDGETS = { //changed //check if it can be combined with GET_ALL_WIDGETS
    action : 'app/PagePreview/GET_ALL_CUSTOM_WIDGETS',
    success: "app/PagePreview/GET_ALL_CUSTOM_WIDGETS_SUCCESS",
    failure: "app/PagePreview/GET_ALL_CUSTOM_WIDGETS_FAILURE",
    urlKey: "getAllWidgets",
    successKey: "getAllCustomWidgetsSuccess",
    failureKey: "getAllCustomWidgetsFailure",
    actionName: "getAllCustomWidgets", //changed
}

export const GET_ALL_WIDGETS = {
    action : 'app/PagePreview/GET_ALL_WIDGETS',
    success: "app/PagePreview/GET_ALL_WIDGETS_SUCCESS",
    failure: "app/PagePreview/GET_ALL_WIDGETS_FAILURE",
    urlKey: "getAllWidgets",
    successKey: "getAllWidgetsSuccess",
    failureKey: "getAllWidgetsFailure",
    actionName: "getAllWidgets",
}

export const GET_ALL_SIMULATION_TOPICS = {
    action : 'app/PagePreview/GET_ALL_SIMULATION_TOPICS',
    success: "app/PagePreview/GET_ALL_SIMULATION_TOPICS_SUCCESS",
    failure: "app/PagePreview/GET_ALL_SIMULATION_TOPICS_FAILURE",
    urlKey: "getAllSimulationTopics",
    successKey: "getAllSimulationTopicsSuccess",
    failureKey: "getAllSimulationTopicsFailure",
    actionName: "getAllSimulationTopics", //changed
}

export const GET_COLLECTION_DETAILS = {
    action : 'app/PagePreview/GET_COLLECTION_DETAILS',
    success: "app/PagePreview/GET_COLLECTION_DETAILS_SUCCESS",
    failure: "app/PagePreview/GET_COLLECTION_DETAILS_FAILURE",
    urlKey: "getCollectionDetails",
    successKey: "getCollectionDetailsSuccess",
    failureKey: "getCollectionDetailsFailure",
    actionName: "getCollectionDetails",
    actionArguments : ["collectionName"]
}

export const GET_DURATION_LIST = {
    action : 'app/PagePreview/GET_DURATION_LIST',
    success: "app/PagePreview/GET_DURATION_LIST_SUCCESS",
    failure: "app/PagePreview/GET_DURATION_LIST_FAILURE",
    urlKey: "getDurationList",
    successKey: "getDurationListSuccess",
    failureKey: "getDurationListFailure",
    actionName: "getDurationList",
}

export const GET_CONNECTORS_LIST = { //mulitple values changed
    action : 'app/PagePreview/GET_CONNECTORS_LIST',
    success: "app/PagePreview/GET_CONNECTORS_LIST_SUCCESS",
    failure: "app/PagePreview/GET_CONNECTORS_LIST_FAILURE",
    urlKey: "getConnectorsByCategory",
    successKey: "getConnectorsListSuccess",
    failureKey: "getConnectorsListFailure",
    actionName: "getConnectorsByCategory",
    actionArguments : ["category"]
}

export const GET_DEVICE_GROUPS = { 
    action : 'app/PagePreview/GET_DEVICE_GROUPS',
    success: "app/PagePreview/GET_DEVICE_GROUPS_SUCCESS",
    failure: "app/PagePreview/GET_DEVICE_GROUPS_FAILURE",
    urlKey: "getDeviceGroups",
    successKey: "getDeviceGroupsSuccess",
    failureKey: "getDeviceGroupsFailure",
    actionName: "getDeviceGroups", //changed
    actionArguments : ["id"]
}

export const GET_TAGS_FOR_DEVICE_GROUP = { 
    action : 'app/PagePreview/GET_TAGS_FOR_DEVICE_GROUP',
    success: "app/PagePreview/GET_TAGS_FOR_DEVICE_GROUP_SUCCESS",
    failure: "app/PagePreview/GET_TAGS_FOR_DEVICE_GROUP_FAILURE",
    urlKey: "getTagsForDevice",
    successKey: "getTagsForDeviceGroupSuccess",
    failureKey: "getTagsForDeviceGroupFailure",
    actionName: "getTagsForDeviceGroup", 
    actionArguments : ["groupId", "connectorId"]
}
