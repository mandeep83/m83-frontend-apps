/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * PagePreview
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { FormattedMessage } from "react-intl";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import { allSelectors as SELECTORS } from "./selectors";
import { allActions as ACTIONS } from './actions'
import GridLayout from '../../components/GridLayout';
import { handleApiData, invokeApiHandler, mqttDetailsValidation, getCustomValues, updateWidgetData, 
    unsubscribeAllTopics, commonParamChangeHandler, validateVisibility, isParentFalse, getOptionsArr, getReactSelectValue, flattenTopologyData } from "../CreateOrEditPage/pageStudioUtils";
import { subscribeTopic } from "../../mqttConnection"
import cloneDeep from "lodash/cloneDeep"
import startCase from "lodash/startCase"
import reducer from "./reducer";
import saga from "./saga";
import Loader from "../../components/Loader";
import ReactSelect from 'react-select';
import SlidingPane from "react-sliding-pane";
import * as pageStudioConstants from "../../containers/CreateOrEditPage/commonConstants"
let { API_COLLECTION_WIDGETS, CUSTOMIZATION_WIDGETS, AUTO_REFRESH_OPTIONS, LAST_AVAILABLE_LIST, COUNTER_TYPES} = pageStudioConstants
let intervalIds = {},
    client, publishClient;

class PagePreview extends React.Component {

    state = {
        dashboardData: {},
        addedWidgets: [],
        dashboardName: "",
        dashboardDesc: "",
        isFetchingPreviewData: 1,
        messages: {},
        selectedTopic: "",
        customizationWidgets: ["counter", "pieChart", "gaugeChart", "barChart", "lineChart", "header", "label", "image", "map"],
        autoRefreshingSpan: 0,
        fullScreenWidgetIndex: -1,
        dataCollections: [],
        selectedAttr: [],
        identifiers: [],
        attributes: [],
        IDs: [],
        lastAvailableList: LAST_AVAILABLE_LIST,
        counterTypes: COUNTER_TYPES,
        rawDurationList: [],
        durationList: {},
        connectorsList: [],
        subscribedTopics: [],
        deviceGroups: [],
        selectedDeviceGroup: "",
        selectedTags: [],
        connectorId: "",
        deviceTags: [],
        selectedDuration: 60,
        widgetMetaData: [],
        controlCommands: []
    }

    componentDidMount() {
        this.handleMounting(true)
    }

    componentWillUnmount() {
        unsubscribeAllTopics(this.state.subscribedTopics)
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.match.params.id !== prevProps.match.params.id) {
            this.handleMounting()
        }
        if (this.state.deviceGroups !== prevState.deviceGroups && this.state.deviceGroups && this.state.deviceGroups.length && this.state.pageType === "aggregate" && this.state.connectorId) {
            this.props.getTagsForDeviceGroup(this.state.deviceGroups[0].deviceGroupId, this.state.connectorId)
        }
        if (prevState.dashboardName !== this.state.dashboardName) {
            this.getAllWidgetsData(true)
            if (this.state.connectorId) {
                this.props.getDeviceGroups(this.state.connectorId)
                this.state.dataSourceValue && this.props.getCollectionDetails(this.state.dataSourceValue)
            }
        }
        let newProp = Object.keys(SELECTORS).find(prop => this.props[prop])
        if (newProp) {
            if (newProp.includes("Failure")) {
                let propData = this.props[newProp].error ? this.props[newProp].error : cloneDeep(this.props[newProp].response)
                let type = "error"
                this.props.showNotification(type, propData, this.onCloseHandler)
            }
            this.props.resetToInitialStateProps(newProp)
        }
    }

    mqttMessageHandler = (message, topic) => {
        let messages = cloneDeep(this.state.messages)
        let addedWidgets = cloneDeep(this.state.addedWidgets);
        if (!messages[topic] || messages[topic].every(msg => JSON.stringify(msg) !== JSON.stringify(message))) {
            if (messages[topic])
                messages[topic].push(message);
            else
                messages = { ...messages, [topic]: [message] }
            addedWidgets.map((widget) => {
                if (mqttDetailsValidation(widget) && widget.mqttDetails.topic.name === topic) {
                    widget = updateWidgetData(widget, messages, this.state.connectorsList)
                }
                return widget
            })
            let isSniffing = this.state.isSniffing === topic ? false : this.state.isSniffing
            this.setState({
                // chartData,
                messages,
                addedWidgets,
                isSniffing
            })
        }
    }

    getAllWidgetsData = (isInitialInvoking) => {
        this.clearIntervals()
        let addedWidgets = cloneDeep(this.state.addedWidgets)
        let subscribedTopics = [...this.state.subscribedTopics]
        addedWidgets.map((widget, index) => {
            let isApiConfigured = widget.dataSource === "API" && widget.apiId
            let ismqttConfigured = mqttDetailsValidation(widget)
            widget.isLoadingData = isInitialInvoking && Boolean(isApiConfigured || ismqttConfigured)
            if (isApiConfigured) {
                invokeApiHandler(widget, false, this.props.invokeApi, this.state)
                let isAutoRefreshEnabled = widget.widgetDetails.autoRefreshing && this.state.autoRefreshingSpan
                if (isAutoRefreshEnabled)
                    intervalIds[widget.i] = setInterval(() => invokeApiHandler(widget, false, this.props.invokeApi, this.state), Number(this.state.autoRefreshingSpan))
            }
            else if (ismqttConfigured && isInitialInvoking) {
                let topic = widget.mqttDetails.topic.name
                if (!subscribedTopics.includes(topic)) {
                    subscribedTopics.push(topic)
                    subscribeTopic(topic, this.mqttMessageHandler)
                }
            }
        })
        this.setState({
            subscribedTopics,
            addedWidgets
        })
    }

    getParamValue = (value, paramName, isFromConfiguration) => {
        let valuesArray = window.location.pathname.split('/').filter((path, index) => index > 2)
        if (["id", "idCsv"].includes(paramName) && !isFromConfiguration)
            return valuesArray.join() || value || ""
        return value || ""
    }

    handleMounting = (isInitialMount) => {
        this.clearIntervals();
        this.setState({
            isFetchingPreviewData: 1,
            pageId: this.props.match.params.id,
            addedWidgets: [],
            selectedDeviceGroup: "",
            selectedTags: [],
            deviceTags: [],
            deviceGroups: [],
            subscribedTopics: []
        }, () => {
            if (isInitialMount) {
                this.props.getConnectorsByCategory("MQTT");
                this.props.getDurationList();
                this.props.getAllGatewayApis();
                if (localStorage.applications) {
                    let _applications = JSON.parse(localStorage.applications)
                    _applications.map(application => {
                        application.page.map(pageObj => {
                            if (pageObj.pageId === this.props.match.params.id) {
                                this.props.setTheme(application.style)
                            }
                        })
                    })
                }
            }
            else {
                this.props.getPageById(this.props.match.params.id)
            }
        })
    }

    clearIntervals = () => {
        Object.keys(intervalIds).map(key => {
            clearInterval(intervalIds[key]);
        })
        intervalIds = {}
    }

    static getDerivedStateFromProps(nextProps, state) {
        let newProp = Object.keys(SELECTORS).find(prop => nextProps[prop])
        if (newProp) {
            let propData = nextProps[newProp].error ? nextProps[newProp].error : nextProps[newProp].response
            switch (newProp) {
                case "getAllGatewayApisSuccess":
                    return {
                        allGatewayApis: propData
                    }
                case "getPageByidSuccess": {
                    // let addedWidgets = propData.widgets.map(widget => {
                    //   if (widget === "customWidgets" && !widget.widgetProperties.widgetSettings) {
                    //     widget.widgetProperties.widgetSettings = {
                    //       js: {
                    //         preProcessor: "BABEL",
                    //         cdnList: CDN_LIST
                    //       },
                    //       css: {
                    //         preProcessor: "",
                    //         cdnList: []
                    //       },
                    //       html: {
                    //         preProcessor: "",
                    //         cdnList: []
                    //       }
                    //     }
                    //     delete widget.javascriptPreProcessor
                    //   }
                    //   return widget
                    // })
                    let subscribedTopics = [...state.subscribedTopics]
                    let controlCommands = []
                    let dataSourceValue = ""
                    if (propData.connectorId) {
                        let requiredConnector = state.connectorsList.find(connector => connector.value === propData.connectorId),
                            controlCommands = requiredConnector.controlConfig ? requiredConnector.controlConfig.map(config => {
                                config.label = config.controlName;
                                config.value = config.controlName;
                                return config
                            }) : controlCommands,
                            userId = localStorage.userId,
                            projectId = localStorage.selectedProjectId,
                            userIdKey = userId.substring(0, 3) + userId.substring(userId.length - 5),
                            projectKey = projectId.substring(0, 3) + projectId.substring(projectId.length - 5)
                        dataSourceValue = `ETL_analytics_${requiredConnector.label}_${userIdKey}_${projectKey}`
                    }
                    propData.widgets.map((widget) => {
                        let needsCustomValues = CUSTOMIZATION_WIDGETS.includes(widget.widgetType)
                        if (needsCustomValues && (widget.dataSource === "DEFAULT" || !["barChart", "lineChart", "lineChartMultiAxes"].includes(widget.widgetType)))
                            widget.widgetDetails.customValues = getCustomValues(widget.widgetType, widget.widgetName, widget.widgetData, widget.widgetDetails.customValues)
                        let drillDownDeviceId = window.location.pathname.split('/').filter((path, index) => index > 2).join()
                        drillDownDeviceId && widget.apiId && Array.isArray(widget.apiDetails.pathParameters) && widget.apiDetails.pathParameters.map(param => {
                            if (["id", "idCsv"].includes(param.name))
                                param.value = drillDownDeviceId
                        })
                        widget.isLoadingData = widget.apiId || (widget.mqttDetails.topic && widget.mqttDetails.topic.name)
                        if(widget.widgetType === "topology" && widget.dataSource === "DEFAULT")
                            widget.widgetData = flattenTopologyData(widget.widgetData)
                    })
                    return {
                        dashboardName: propData.name,
                        dashboardDesc: propData.description,
                        connectorId: propData.connectorId,
                        addedWidgets: propData.widgets,
                        isFetchingPreviewData: state.isFetchingPreviewData - 1,
                        pageSettings: propData.pageSettings,
                        pageType: propData.pageType,
                        selectedDuration: propData.duration || 60,
                        subscribedTopics,
                        controlCommands,
                        dataSourceValue
                    }
                }
                case "getAllWidgetsSuccess":
                    return {
                        // allWidgets: propData.filter(widget => widget.pageTypes.includes(state.pageType)),
                        allWidgets: propData,
                        consistentWidgetData: propData,
                        widgetMetaData: propData,
                        // isLoading: state.isLoading - 1,
                    }
                case "getConnectorsListSuccess":
                    propData.forEach(connector => { connector.label = connector.name; connector.value = connector.id })
                    nextProps.getPageById(nextProps.match.params.id)
                    return {
                        connectorsList: propData,
                    }
                case "getDurationListSuccess":
                    let durationList = {
                        hourly: propData.filter(duration => duration.label.toLowerCase().includes("hour")),
                        daily: propData.filter(duration => duration.label.toLowerCase().includes("day")),
                        weekly: propData.filter(duration => duration.label.toLowerCase().includes("week")),
                        monthly: propData.filter(duration => duration.label.toLowerCase().includes("month")),
                        yearly: propData.filter(duration => duration.label.toLowerCase().includes("year")),
                    }
                    Object.entries(durationList).map(([key, value]) => {
                        !value.length && delete durationList[key]
                    })
                    return {
                        durationList,
                        rawDurationList: propData,
                    }
                case "invokeApiSuccess":
                    let addedWidgets = handleApiData(state.addedWidgets, nextProps[newProp])
                    return {
                        addedWidgets
                    }
                case "getDeviceGroupsSuccess": {
                    propData.map(deviceGroup => {
                        deviceGroup.label = deviceGroup.deviceGroupName
                        deviceGroup.value = deviceGroup.deviceGroupId

                    })
                    return {
                        deviceGroups: propData,
                        selectedDeviceGroup: state.pageType === "aggregate" && propData.length ? propData[0].deviceGroupId : ""
                    }
                }
                case "getConnectorsListFailure":
                case "getPageByidFailure":
                    return {
                        isFetchingPreviewData: prevState.isFetchingPreviewData - 1
                    }
                case "getCollectionDetailsSuccess":
                    let attributes = propData.map(el => {
                        el.label = el.displayName;
                        el.value = el.attribute || el.name;
                        return el
                    }),
                        uniqueAttr = attributes.find(attr => attr.isUnique),
                        devices = uniqueAttr ? uniqueAttr.uniqueValues.map(val => ({ label: val.name || val.deviceId, value: val.deviceId })) : state.devices,
                        identifier = uniqueAttr ? uniqueAttr.name : ""
                    attributes = attributes.filter(attr => !attr.isUnique)
                    return {
                        attributes,
                        devices,
                        identifier
                    }
            }
        }
        return null
    }

    refreshingChangeHandler = ({ currentTarget }) => {
        let isNotTurnedOff = currentTarget.value !== "0"
        this.setState({
            autoRefreshingSpan: currentTarget.value
        }, () => {
            isNotTurnedOff && this.getAllWidgetsData()
        })
    }

    deviceGroupChangeHandler = ({ currentTarget }) => {
        this.setState({
            selectedDeviceGroup: currentTarget.value,
            selectedTags: []
        }, () => {
            this.state.selectedDeviceGroup && this.props.getTagsForDeviceGroup(currentTarget.value, this.state.connectorId);
            this.getAllWidgetsData()
        })
    }

    deviceTagsChangeHandler = (selectedTags) => {
        this.setState({
            selectedTags,
        }, () => this.getAllWidgetsData())
    }

    onCloseHandler = () => {
        this.setState({
            isOpen: false,
            message2: ''
        })
    }

    validateApiCall = () => {
        event.preventDefault()
        let addedWidgets = cloneDeep(this.state.addedWidgets)
        let configWidget = addedWidgets[this.state.configWidgetIndex]
        let errorMessage = false
        let hasParameters = (configWidget.apiDetails.pathParameters && configWidget.apiDetails.pathParameters.length)
        if (hasParameters) {
            configWidget.apiDetails.pathParameters.map((param, index) => {
                if (!param.value && param.required) {
                    param.errorMessage = true
                    errorMessage = true
                }
            })
        }
        if (errorMessage) {
            this.setState({
                addedWidgets
            })
        }
        else {
            configWidget.isLoadingData = true
            invokeApiHandler(configWidget, true, this.props.invokeApi, this.state)
            this.setState({
                addedWidgets
            })
        }
    }

    setStateFromChild = (newState, callback) => {
        if (typeof (newState) == "function") {
            this.setState(prevState => (newState(prevState)), callback)
        }
        else
            this.setState({
                ...newState
            }, callback)
    }

    getSelectedDuration = () => {
        let durationObj = this.state.rawDurationList.find(duration => duration.value === this.state.selectedDuration)
        return durationObj ? durationObj.label : "Last 1 hour"
    }

    durationChangeHandler = (selectedDuration) => {
        let addedWidgets = cloneDeep(this.state.addedWidgets)
        addedWidgets.map(widget => {
            if (["lineChart", "lineChartMultiAxes", "gaugeChart"].includes(widget.widgetType) && widget.dataSource === "API" && widget.apiId && !intervalIds[widget.i]) {
                invokeApiHandler(widget, false, this.props.invokeApi, this.state)
            }
        })
        this.setState({
            addedWidgets,
            selectedDuration
        })
    }

    closeModalHandler = () => {
        let addedWidgets = cloneDeep(this.state.addedWidgets)
        addedWidgets[this.state.configWidgetIndex] = this.state.tempWidgetData
        this.setState({
            addedWidgets,
            configWidgetIndex: null,
            tempWidgetData: null,
        })
    }

    saveWidgetConfiguration = ({ value, name, optionsKey }) => {
        this.setState({
            widgetMenu: true,
            configWidgetIndex: null,
            tempWidgetData: null,
        })
    }

    paramChangeHandler = (event, paramIndex, isReactSelect) => {
        let addedWidgets = commonParamChangeHandler(event, paramIndex, isReactSelect, this.state)
        this.setState({
            addedWidgets
        })
    }

    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>PagePreview</title>
                    <meta name="description" content="Description of PagePreview" />
                </Helmet>
                
                <header className="content-header d-flex">
                    {!localStorage.applications &&
                    <div className="flex-60">
                        <div className="d-flex">
                            <h6 className="previous" onClick={() => this.props.history.push("/dashboards")}>Dashboards</h6>
                            <h6>Preview</h6>
                            <h6 className="active">{this.state.dashboardName}</h6>
                        </div>
                    </div>
                    }
                    <div className="flex-40 text-right">
                        <div className="content-header-group">
                            {!localStorage.applications &&
                            <button className="btn btn-light" onClick={() => this.props.history.push('/dashboards')}><i className="fas fa-angle-left"></i></button>
                            }
                        </div>
                    </div>
                </header>
                
                {this.state.isFetchingPreviewData ? <Loader /> :
                    <React.Fragment>
                        <div className="d-flex content-filter mb-0">
                            {Boolean(this.state.pageType) && this.state.pageType !== "deviceDetail" &&
                            <React.Fragment>
                                <div className="form-group flex-25 pd-r-10">
                                    <div className="input-group">
                                        <div className="input-group-prepend">
                                            <span className="input-group-text">Device Group :</span>
                                        </div>
                                        <select name="selectWidget" className="form-control" value={this.state.selectedDeviceGroup} id="selectedDeviceGroup" onChange={this.deviceGroupChangeHandler} >
                                            {this.state.pageType === "listing" && <option value="">All</option>}
                                            {this.state.deviceGroups.map((option, index) => <option value={option.deviceGroupId} key={index}>{option.deviceGroupName}</option>)}
                                        </select>
                                    </div>
                                </div>
                                <div className="form-group flex-25 pd-l-5 pd-r-10">
                                    <div className="input-group">
                                        <div className="input-group-prepend">
                                            <span className="input-group-text">Device Tags :</span>
                                        </div>
                                        <ReactSelect
                                            className="form-control-multi-select"
                                            isMulti
                                            isDisabled={!this.state.selectedDeviceGroup || this.state.deviceTags.length == 0}
                                            options={this.state.deviceTags}
                                            onChange={this.deviceTagsChangeHandler}
                                            value={this.state.selectedTags}
                                        />
                                    </div>
                                </div>
                            </React.Fragment>
                            }
                            {this.state.pageType !== "listing" &&
                            <div className="form-group flex-30 pd-l-5 pd-r-10">
                                <div className="input-group">
                                    <div className="input-group-prepend">
                                        <span className="input-group-text">Duration :</span>
                                    </div>
                                    <select className="form-control">
                                        <option>Last Hourly</option>
                                        <option>Daily</option>
                                        <option>Weekly</option>
                                        <option>Monthly</option>
                                    </select>
                                    {/*{Object.entries(this.state.durationList).map(([durationLabel, values]) =>*/}
                                    {/*    <div key={durationLabel} className="card">*/}
                                    {/*        <div className="card-header">*/}
                                    {/*            <a className="card-link" data-toggle="collapse" href={`#${durationLabel}`}>{startCase(durationLabel)}</a>*/}
                                    {/*        </div>*/}
                                    {/*        <div id={durationLabel} className="dropdown-menu" data-parent="#accordion">*/}
                                    {/*            <div className="card-body">*/}
                                    {/*                {values.map(({ label, value }, index) => <p key={index} onClick={() => this.durationChangeHandler(value)} >{label}</p>)}*/}
                                    {/*            </div>*/}
                                    {/*        </div>*/}
                                    {/*    </div>*/}
                                    {/*)}*/}
                                </div>
                            </div>
                            }
                            <div className="form-group flex-20 pd-l-10">
                                <div className="input-group">
                                    <div className="input-group-prepend">
                                        <span className="input-group-text">Auto Refresh :</span>
                                        <select name="selectWidget" className="form-control" value={this.state.autoRefreshingSpan} onChange={this.refreshingChangeHandler} >
                                            {AUTO_REFRESH_OPTIONS.map((option, index) => <option value={option.value} key={index}>{option.label}</option>)}
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="content-body page-preview-wrapper">
                            <GridLayout
                                setState={this.setStateFromChild}
                                state={this.state}
                                onCloseHandler={this.onCloseHandler}
                                {...this.props}
                            />
                        </div>
                    </React.Fragment>
                }
                
                {/* widget config modal */}
                {(typeof (this.state.configWidgetIndex) == "number") &&
                <SlidingPane
                    className=''
                    overlayClassName='slidingFormOverlay'
                    closeIcon={<div></div>}
                    isOpen={(typeof (this.state.configWidgetIndex) == "number")}
                    from='right'
                    width='475px'
                >
                    <div className="modal-content">
                        <div className="modal-header">
                            <h6 className="modal-title">Widget Configuration
                                <button type="button" className="btn close" onClick={this.closeModalHandler}>
                                    <i className="far fa-angle-right"></i>
                                </button>
                            </h6>
                        </div>

                        {this.state.configLoader ?
                            <div className="modal-loader">
                                <i className="fad fa-sync-alt fa-spin text-primary"></i>
                            </div>
                            :
                            <form onSubmit={this.saveWidgetConfiguration}>
                                <div className="modal-body position-relative">
                                    {<React.Fragment>
                                        <div className="flex">
                                            <div className="form-group input-group">
                                                {API_COLLECTION_WIDGETS.includes(this.state.configWidgetType) ?
                                                    <div className="configTable">
                                                        {this.state.addedWidgets[this.state.configWidgetIndex].apiDetails.pathParameters.map((parameter, paramIndex) => {
                                                            if (validateVisibility(parameter, this.state.addedWidgets[this.state.configWidgetIndex])) {
                                                                return (
                                                                    <div className="form-group " key={paramIndex}>
                                                                        <label className="form-group-label">{parameter.displayName || parameter.name}</label>
                                                                        {/* {Boolean(parameter.required) && <i className="fa fa-asterisk f-8 ml-1 text-red"></i>} */}
                                                                        {parameter.type === "Boolean" ?
                                                                            <div className="flex">
                                                                                <div className="fx-b50 mb-2">
                                                                                    <label className="radioLabel">True
                                                                                        <input type="radio" id="required" value="true" checked={parameter.value === "true"} onChange={(e) => this.paramChangeHandler(e, paramIndex)} />
                                                                                        <span className="radioMark" />
                                                                                    </label>
                                                                                </div>
                                                                                <div className="fx-b50 mb-2">
                                                                                    <label className="radioLabel">False
                                                                                        <input type="radio" id="required" value="false" checked={parameter.value === "false"} onChange={(e) => this.paramChangeHandler(e, paramIndex)} />
                                                                                        <span className="radioMark" />
                                                                                    </label>
                                                                                </div>
                                                                                {parameter.errorMessage &&
                                                                                    <div className="fx-b100"><p className="errorMessage text-right">Please fill out this field.</p></div>
                                                                                }
                                                                            </div> :
                                                                            parameter.type === "select" ? parameter.isLoading ?
                                                                                <div className="text-center">
                                                                                    <i className="fad fa-sync-alt fa-spin text-primary f-14"></i>
                                                                                </div> :
                                                                                <React.Fragment>
                                                                                    <ReactSelect
                                                                                        className="form-control-multi-select"
                                                                                        isMulti={parameter.name.includes("Csv")}
                                                                                        isDisabled={parameter.isDisabled || parameter.parent && isParentFalse(parameter, this.state.addedWidgets[this.state.configWidgetIndex])}
                                                                                        options={getOptionsArr(parameter, this.state.addedWidgets[this.state.configWidgetIndex], this.state[parameter.optionsKey])}
                                                                                        onChange={(value) => this.paramChangeHandler({ value, id: parameter.name }, paramIndex, true)}
                                                                                        value={getReactSelectValue(parameter, this.state[parameter.optionsKey])}
                                                                                    />
                                                                                    {parameter.errorMessage && <p className="errorMessage text-right">Please fill out this field.</p>}
                                                                                </React.Fragment> :
                                                                                <React.Fragment>
                                                                                    <input disabled={parameter.isDisabled || (parameter.parent && isParentFalse(parameter, this.state.addedWidgets[this.state.configWidgetIndex]))} className="form-control" onChange={(e) => this.paramChangeHandler(e, paramIndex)} required={parameter.required} value={parameter.value} />
                                                                                    {parameter.errorMessage && <p className="errorMessage text-right">Please fill out this field.</p>}
                                                                                </React.Fragment>
                                                                        }
                                                                    </div>
                                                                )
                                                            }
                                                        })
                                                        }
                                                    </div>
                                                    :
                                                    <React.Fragment>
                                                        <select className="form-control mr-r-8" disabled={API_COLLECTION_WIDGETS.includes(this.state.configWidgetType)} value={this.state.apiId} id="apiId" onChange={() => null} >
                                                            <option value="" >Select</option>
                                                            {this.state.allGatewayApis.map((temp, index) => {
                                                                return <option value={temp.id} key={index}>{temp.apiPath}</option>
                                                            })}
                                                        </select>
                                                        <label className="form-group-label">API URL :
                                                    <span className="requiredMark"><i className="fa fa-asterisk" /></span>
                                                        </label>
                                                    </React.Fragment>
                                                }
                                                <div className="input-group-prepend">
                                                    <button className="btn btn-primary rounded" onClick={this.validateApiCall}>Plot</button>
                                                </div>
                                            </div>
                                        </div>

                                    </React.Fragment>
                                    }
                                </div>
                            </form>
                        }
                    </div>
                </SlidingPane>}
                {/* end widget config modal */}
            </React.Fragment>
        )
    }
}

PagePreview.propTypes = {
    dispatch: PropTypes.func.isRequired
};


let allSelectors = {}
Object.entries(SELECTORS).map(([key, value]) => {
    allSelectors[key] = value()
})

const mapStateToProps = createStructuredSelector(allSelectors)

function mapDispatchToProps(dispatch) {
    let allActions = { dispatch }
    Object.entries(ACTIONS).map(([key, value]) => {
        allActions[key] = (...args) => dispatch(value(...args))
    })
    return allActions
}
const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "pagepreview", reducer });
const withSaga = injectSaga({ key: "pagepreview", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(PagePreview);
