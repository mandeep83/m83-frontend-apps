/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

// export const DEFAULT_ACTION = 'app/PolicyList/DEFAULT_ACTION';

export const GET_POLICY_DATA = 'app/PolicyList/GET_POLICY_DATA';
export const GET_POLICY_DATA_SUCCESS = 'app/PolicyList/GET_POLICY_DATA_SUCCESS';
export const GET_POLICY_ERROR ="app/PolicyList/GET_POLICY_ERROR";

export const DELETE_POLICY_ITEM = 'app/PolicyList/DELETE_POLICY_ITEM';
export const DELETE_POLICY_ITEM_SUCCESS = 'app/PolicyList/DELETE_POLICY_ITEM_SUCCESS';
export const DELETE_POLICY_ITEM_ERROR ="app/PolicyList/DELETE_POLICY_ITEM_ERROR";

export const CHANGE_POLICY_STATUS = 'app/PolicyList/CHANGE_POLICY_STATUS';
export const CHANGE_POLICY_STATUS_SUCCESS = 'app/PolicyList/CHANGE_POLICY_STATUS_SUCCESS';
export const CHANGE_POLICY_STATUS_ERROR ="app/PolicyList/CHANGE_POLICY_STATUS_ERROR";

export const RESET_TO_INITIAL_STATE = 'app/PolicyList/RESET_TO_INITIAL_STATE';