/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/
import { apiCallHandler } from '../../api';
import {takeEvery} from 'redux-saga';
import * as CONSTANTS from './constants';


export function* getPolicyData(action){
  yield[apiCallHandler(action, CONSTANTS.GET_POLICY_DATA_SUCCESS, CONSTANTS.GET_POLICY_ERROR, 'getAllPolicyData' )]
}

export function* watcherGetPolicyData(){
  yield takeEvery(CONSTANTS.GET_POLICY_DATA, getPolicyData)
}

export function* deletePolicyItem(action){
  yield[apiCallHandler(action, CONSTANTS.DELETE_POLICY_ITEM_SUCCESS, CONSTANTS.DELETE_POLICY_ITEM_ERROR, 'deletePolicyItem' )]
}

export function* watcherDeletePolicyItem(){
  yield takeEvery(CONSTANTS.DELETE_POLICY_ITEM, deletePolicyItem)
}

export function* changePolicyStatus(action){
  yield[apiCallHandler(action, CONSTANTS.CHANGE_POLICY_STATUS_SUCCESS, CONSTANTS.CHANGE_POLICY_STATUS_ERROR, 'changePolicyStatus' )]
}

export function* watcherChangePolicyStatus(){
  yield takeEvery(CONSTANTS.CHANGE_POLICY_STATUS, changePolicyStatus)
}


export default function* rootSaga(){
  yield [
      watcherGetPolicyData(),
      watcherDeletePolicyItem(),
      watcherChangePolicyStatus()
  ]
}