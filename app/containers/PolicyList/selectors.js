/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the policyList state domain
 */

const selectPolicyListDomain = state => state.get('policyList', initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by PolicyList
 */

export const isFetchingState = state => state.get('loader');

 const getPolicyData = () => createSelector(selectPolicyListDomain, substate => substate.getPolicyData);
 const getPolicyDataError = () => createSelector(selectPolicyListDomain, substate => substate.getPolicyDataError);

 export const deletePolicyItemSuccess = () => createSelector(selectPolicyListDomain, substate => substate.deletePolicyItemSuccess);
 export const deletePolicyItemError = () => createSelector(selectPolicyListDomain, substate => substate.deletePolicyItemError);


 export const changePolicyStatusSuccess = () => createSelector(selectPolicyListDomain, substate => substate.changePolicyStatusSuccess);
 export const changePolicyStatusError = () => createSelector(selectPolicyListDomain, substate => substate.changePolicyStatusError);

export { getPolicyData,  getPolicyDataError};
