/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import ReactTooltip from "react-tooltip";
import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import * as SELECTORS from './selectors';
import reducer from './reducer';
import saga from './saga';
import { getAllPolicyData, deletePolicyItem, changePolicyStatus, resetToInitialState } from './actions'
import Loader from '../../components/Loader/Loadable';
import NotificationModal from '../../components/NotificationModal/Loadable'
import messages from './messages';
import ConfirmModel from '../../components/ConfirmModel';
import ReactTable from 'react-table';
import NoDataFoundMessage from "../../components/NoDataFoundMessage/Loadable"
import AddNewButton from "../../components/AddNewButton/Loadable"


/* eslint-disable react/prefer-stateless-function */


export class PolicyList extends React.Component {

    state = {
        showModal: false,
        selectedId: '',
        policyToBeDeleted: '',
        policyToBeDeployUndeploy: '',
        active: '',
        selectedDeviceId: "",
        apiListType: true,
        weekDays: ["SUNDAY", "MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY"],
        isFetching: true,
        policyData: []
    }

    componentDidMount() {
        this.props.getAllPolicyData()
    }


    componentWillReceiveProps(nextProps) {
        if (nextProps.policyData && nextProps.policyData !== this.props.policyData) {
            this.setState({
                policyData: nextProps.policyData,
                isFetching: false
            }, () => this.props.resetToInitialState())
        }

        if (nextProps.policyDataError && nextProps.policyDataError !== this.props.policyDataError) {
            this.setState({
                isOpen: true,
                isFetching: false,
                message2: nextProps.policyDataError,
                modalType: "error"
            }, () => this.props.resetToInitialState())
        }

        if (nextProps.deletePolicyItemSuccess && nextProps.deletePolicyItemSuccess !== this.props.deletePolicyItemSuccess) {
            let policyData = this.state.policyData.filter((item) => {
                return item.id !== nextProps.deletePolicyItemSuccess.id
            })
            this.setState({
                policyData,
                isOpen: true,
                isFetching: false,
                message2: nextProps.deletePolicyItemSuccess.message,
                modalType: "success"
            }, () => this.props.resetToInitialState())
        }

        if (nextProps.deletePolicyItemError && nextProps.deletePolicyItemError !== this.props.deletePolicyItemError) {
            this.setState({
                isOpen: true,
                isFetching: false,
                message2: nextProps.deletePolicyItemError,
                modalType: "error"
            }, () => this.props.resetToInitialState())
        }


        if (nextProps.changePolicyStatusSuccess && nextProps.changePolicyStatusSuccess !== this.props.changePolicyStatusSuccess) {
            let policyData = [...this.state.policyData]
            policyData = policyData.map(temp => {
                if (temp.id === nextProps.changePolicyStatusSuccess.id) {
                    temp.active = !temp.active;
                }
                return temp;
            })
            this.setState({
                changingStatusFor: null,
                policyData,
                isOpen: true,
                isFetching: false,
                message2: nextProps.changePolicyStatusSuccess.message,
                modalType: "success"
            }, () => this.props.resetToInitialState())
        }

        if (nextProps.changePolicyStatusError && nextProps.changePolicyStatusError !== this.props.changePolicyStatusError) {
            this.setState({
                changingStatusFor: null,
                isOpen: true,
                isFetching: false,
                message2: nextProps.changePolicyStatusError,
                modalType: "error"
            }, () => this.props.resetToInitialState())
        }
    }

    cancelClicked = () => {
        this.setState({
            showModal: false
        })
    }

    onCloseHandler = () => {
        this.setState({
            isOpen: false,
            message2: "",
            modalType: ""
        });
    }

    confirmDelete = () => {
        this.setState({
            showModal: false,
            isFetching: true,
            isOpen: false,
        }, () => this.props.deletePolicyItem(this.state.selectedId))
    }

    changePolicyStatus = (id, status) => {
        this.setState({
            changingStatusFor: id
        }, () => this.props.changePolicyStatus(id, status))
    }

    getPropValue = ({ createdByUserId, status, active }) => {
        let isNotCreator = createdByUserId !== localStorage["userId"],
            isNotAdmin = localStorage.role !== "ACCOUNT_ADMIN",
            policyIsRunning = status !== "STOP",
            policyIsActive = active,
            validation = (isNotCreator && isNotAdmin) || policyIsRunning || policyIsActive;
        return validation
    }

    goToAddOrEditPolicyPage = () => {
        this.props.history.push('/policyAddEdit')
    }

    render() {

        return (
            <React.Fragment>
                <Helmet>
                    <title>Policies</title>
                    <meta name="description" content="M83-Policy" />
                </Helmet>
                {this.state.isFetching ? <Loader /> :
                    <React.Fragment>
                        <div className="pageBreadcrumb">
                            <div className="row">
                                <div className="col-8">
                                    <p><span>Policies {this.state.policyData.length > 0 && '(' + this.state.policyData.length + ')'}</span></p>
                                </div>
                                <div className="col-4 text-right">
                                    <div className="flex h-100 justify-content-end align-items-center">
                                        {this.state.policyData && this.state.policyData.length > 0 &&
                                            <button type="button" name="button" className="btn btn-primary" onClick={() => this.goToAddOrEditPolicyPage()}>
                                                <span className="btn-primary-icon"><i className="far fa-plus"></i></span>
                                            </button>
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>

                        {/*<div className="outerBox pd-b-0">*/}
                        {/*    <ul className="pageList">*/}
                        {/*        <li>*/}
                        {/*            <div className="pageListBox position-relative">*/}
                        {/*                <div className="pageListBoxImage cursor-pointer">*/}
                        {/*                    <img src="https://content.iot83.com/m83/other/calendar.png" />*/}
                        {/*                    <span className="btn btn-transparent btn-transparent-success"><i className="fal fa-clock"></i></span>*/}
                        {/*                </div>*/}
                        {/*                <div className="pageListBoxContent cursor-pointer">*/}
                        {/*                    <h5>SFP</h5>*/}
                        {/*                    <h6><strong>Description :</strong>N/A</h6>*/}
                        {/*                    <h6>*/}
                        {/*                        <strong>ETL :</strong>*/}
                        {/*                        <span className="alert alert-info pageListPill">KafkaEtl</span>*/}
                        {/*                        <span className="alert alert-info pageListPill">PolicyEtl</span>*/}
                        {/*                    </h6>*/}
                        {/*                    <h6>*/}
                        {/*                        <strong>Created By :</strong>*/}
                        {/*                        <span className="pageListBoxInitial" data-tip data-for="createdBy">SC</span>*/}
                        {/*                        <ReactTooltip id="createdBy" place="bottom" type="dark">*/}
                        {/*                            <div className="tooltipText"><p>Shakher chauhan</p></div>*/}
                        {/*                        </ReactTooltip>*/}
                        {/*                        <span className="mr-r-10 mr-l-10">|</span>*/}
                        {/*                        <strong>Updated By :</strong>*/}
                        {/*                        <span className="pageListBoxInitial" data-tip data-for="updatedBy">SC</span>*/}
                        {/*                        <ReactTooltip id="updatedBy" place="bottom" type="dark">*/}
                        {/*                            <div className="tooltipText"><p>Shakher chauhan</p></div>*/}
                        {/*                        </ReactTooltip>*/}
                        {/*                    </h6>*/}
                        {/*                </div>*/}
                        {/*                <div className="pageListBoxFooter"><p><strong>Created At:</strong>24/03/2020, 15:39:51</p><p>*/}
                        {/*                    <strong>Updated At:</strong>24/03/2020, 15:39:51</p>*/}
                        {/*                    <div className="dropdown pageListDropdown">*/}
                        {/*                        <button type="button" className="btn dropdown-toggle" data-toggle="dropdown">*/}
                        {/*                            <i className="far fa-ellipsis-v"></i>*/}
                        {/*                        </button>*/}
                        {/*                        <ul className="dropdown-menu pageListDropdownMenu">*/}
                        {/*                            <li><i className="far fa-play"></i>Start</li>*/}
                        {/*                            <li><i className="far fa-pen"></i>Edit</li>*/}
                        {/*                            <li><i className="far fa-trash-alt"></i>Delete</li>*/}
                        {/*                        </ul>*/}
                        {/*                    </div>*/}
                        {/*                </div>*/}
                        {/*            </div>*/}
                        {/*        </li>*/}
                        {/*    </ul>*/}
                        {/*</div>*/}
                        {this.state.policyData && this.state.policyData.length > 0 ?
                            <div className="outerBox">
                                <div className="contentTableBox customReactTableBox animated slideInUp">
                                    <ReactTable
                                        columns={[
                                            { columns: [{ id: "name", Header: "Name", accessor: "name", sortable: false, filterable: true, }] },
                                            { columns: [{ id: "description", Header: "Description", Cell: row => <span>{row.original.description || "N/A"}</span>, sortable: false, filterable: false, }] },
                                            { columns: [{ id: "createdBy", Header: "Created By", accessor: "createdBy", sortable: false, filterable: true, }] },
                                            { columns: [{ id: "timestamp", Header: "Created At", Cell: row => <span>{new Date(row.original.createdAt).toLocaleString('en-US', { timeZone: localStorage.timeZone })}</span>, sortable: false, filterable: false, }] },
                                            { columns: [{ id: "updatedBy", Header: "Updated By", accessor: "updatedBy", sortable: false, filterable: true, }] },
                                            { columns: [{ id: "updatedAt", Header: "Updated At", Cell: row => <span>{new Date(row.original.updatedAt).toLocaleString('en-US', { timeZone: localStorage.timeZone })}</span>, sortable: false, filterable: false, }] },
                                            {
                                                columns: [{
                                                    id: "actions", Header: "Actions", sortable: false, filterable: false,
                                                    Cell: row => {
                                                        return (
                                                            <div className="">
                                                                <button type="button" disabled={row.original.active} className=" btn btn-transparent btn-transparent-primary" onClick={() => { this.props.history.push('policyAddEdit/' + row.original.id) }} data-tip data-for={"edit" + row.original.id}>
                                                                    <i className="fas fa-pencil"></i>
                                                                </button>
                                                                <ReactTooltip id={"edit" + row.original.id} place="bottom" type="dark">
                                                                    <div className="tooltipText"><p>Edit</p></div>
                                                                </ReactTooltip>
                                                                <span className="mr-r-10" data-tip data-for={"startStop" + row.original.id}>
                                                                    {this.state.changingStatusFor === row.original.id ?
                                                                        <span className="panelLoader">
                                                                            <i className="fal fa-sync-alt fa-spin "></i>
                                                                        </span> :
                                                                        <button type="button"
                                                                            onClick={() => this.changePolicyStatus(row.original.id, row.original.active)}
                                                                            className={row.original.active ? "btn btn-transparent btn-transparent-warning" : "btn btn-transparent btn-transparent-success"}>
                                                                            <i className={row.original.active ? "fas fa-pause" : "fas fa-play"}></i>
                                                                        </button>}
                                                                </span>
                                                                <ReactTooltip id={"startStop" + row.original.id} place="bottom" type="dark">
                                                                    <div className="tooltipText"><p>{row.original.active ? "Stop" : "Start"}</p></div>
                                                                </ReactTooltip>
                                                                <button type="button" disabled={this.getPropValue(row.original)} className="btn btn-transparent btn-transparent-danger" data-tip data-for={"delete" + row.original.id} onClick={() => { this.setState({ showModal: true, selectedId: row.original.id }) }}>
                                                                    <i className="fas fa-trash-alt"></i>
                                                                </button>
                                                                {!this.getPropValue(row.original) && <ReactTooltip id={"delete" + row.original.id} place="bottom" type="dark">
                                                                    <div className="tooltipText"><p>Delete</p></div>
                                                                </ReactTooltip>}
                                                            </div>
                                                        )
                                                    },
                                                }]
                                            },
                                        ]}
                                        noDataText="There is no data to display."
                                        PreviousComponent={(props) => <button type="button"{...props}><i className="fal fa-angle-left"></i></button>}
                                        NextComponent={(props) => <button type="button" {...props}><i className="fal fa-angle-right"></i></button>}
                                        data={this.state.policyData}
                                        loading={this.state.isFetching}
                                        pages={Math.ceil(this.state.policyData.length / 10)}
                                        defaultFilterMethod={this.filterMethod}
                                        defaultPageSize={10}
                                        className="customReactTable"
                                    />
                                </div>
                            </div>
                            :


                            <AddNewButton
                                text1="You have not created any policies yet."
                                createItemOnAddButtonClick={this.goToAddOrEditPolicyPage}
                                imageIcon="addDeviceType.png"
                            />
                        }
                    </React.Fragment>
                }

                {this.state.showModal &&
                    <ConfirmModel
                        deleteName="the selected policy"
                        confirmClicked={this.confirmDelete}
                        cancelClicked={this.cancelClicked} />
                }

                {this.state.isOpen &&
                    <NotificationModal
                        type={this.state.modalType}
                        message2={this.state.message2}
                        onCloseHandler={this.onCloseHandler}
                    />
                }
            </React.Fragment >
        );
    }
}

PolicyList.propTypes = {
    dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
    policyData: SELECTORS.getPolicyData(),
    policyDataError: SELECTORS.getPolicyDataError(),
    deletePolicyItemError: SELECTORS.deletePolicyItemError(),
    deletePolicyItemSuccess: SELECTORS.deletePolicyItemSuccess(),
    changePolicyStatusSuccess: SELECTORS.changePolicyStatusSuccess(),
    changePolicyStatusError: SELECTORS.changePolicyStatusError(),
});

function mapDispatchToProps(dispatch) {
    return {
        dispatch,
        getAllPolicyData: () => dispatch(getAllPolicyData()),
        deletePolicyItem: (id) => dispatch(deletePolicyItem(id)),
        changePolicyStatus: (id, status) => dispatch(changePolicyStatus(id, status)),
        resetToInitialState: () => dispatch(resetToInitialState()),
    };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'policyList', reducer });
const withSaga = injectSaga({ key: 'policyList', saga });

export default compose(
    withReducer,
    withSaga,
    withConnect,
)(PolicyList);