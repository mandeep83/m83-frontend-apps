/**
 *
 * ManageReports
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import reducer from "./reducer";
import saga from "./saga";
import Loader from '../../components/Loader/Loadable';
import ReactSelect from "react-select";
import SlidingPane from 'react-sliding-pane';
import { allSelectors as SELECTORS } from "./selectors";
import { allActions as ACTIONS } from './actions'
import NotificationModal from '../../components/NotificationModal/Loadable';
import ConfirmModel from "../../components/ConfirmModel/Loadable";
import { isNavAssigned, getTimeDifference } from '../../commonUtils';
import ListingTable from "../../components/ListingTable/Loadable";
import NoDataFoundMessage from "../../components/NoDataFoundMessage/Loadable"
import AddNewButton from "../../components/AddNewButton/Loadable";
import TagsWithSuggestions from "../../components/TagsWithSuggestions/Loadable";
import produce from "immer";
import cloneDeep from "lodash/cloneDeep";
import ReactAce from "react-ace";
import Skeleton from "react-loading-skeleton";

/* eslint-disable react/prefer-stateless-function */
export class ManageReports extends React.Component {
    state = {
        isFetching: true,
        toggleView: true,
        reportsList: [],
        payload: {
            "name": "",
            "reportTemplate": "Device Uptime Report",
            "description": "",
            "deviceTypeId": "",
            "selectedGroupId": "",
            "duration": 1,  	//duration in hours
            "emails": [],
            "format": "CSV",
            "isScheduled": true,
            "hour": 0,
            "minutes": 1,
            "daysOfWeek": [],
            "timeZone": this.props.timeZone,
        },
        searchTerm: '',
        addOrEditReportModal: false,
        durationList: [
            {
                value: 1,
                label: "Last 1 Hour"
            }, {
                value: 3,
                label: "Last 3 Hours"
            },
            {
                value: 6,
                label: "Last 6 Hours"
            },
            {
                value: 9,
                label: "Last 9 Hours"
            },
            {
                value: 12,
                label: "Last 12 Hours"
            },
            {
                value: 24,
                label: "Last 1 Day"
            },
            {
                value: 72,
                label: "Last 3 Days"
            },
            {
                value: 168,
                label: "Last 7 Days"
            }
        ],
        isFetchingReportHistory: false,
        reportData: [],
        daysList: ["MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY", "SUNDAY"],
        reportTemplates: [
            {
                label: "Device Uptime Report",
                value: "Device Uptime Report",
            }, {
                label: "Device Alarm Aggregate Report",
                value: "Device Alarm Aggregate Report",
            }, {
                label: "Device Event History Report",
                value: "Device Event History Report",
            }
        ],
        deviceTypesList: [],
        listOfGroupsForSelectedDeviceType: [],
        emailSuggestions: [],
        remainingReportsCount: 0,
        usedReportsCount: 0,
        totalReportsCount: 0,
    }

    componentDidMount() {
        let payload = [
            {
                dependingProductId: null,
                productName: "report",
            }
        ];
        this.props.getDeveloperQuota(payload)
        this.props.getAllDeviceTypes();
    }

    componentWillReceiveProps = (nextProps) => {
        if (nextProps.timeZone !== this.props.timeZone) {
            let payload = produce(this.state.payload, draftState => {
                draftState.timeZone = nextProps.timeZone
            })
            this.setState({
                payload
            })
        }

        if (nextProps.getAllReportsSuccess) {
            this.setState({
                reportsList: nextProps.getAllReportsSuccess.response.map(el => {
                    let requiredDeviceType = this.state.deviceTypesList.find(device => device.value === el.deviceTypeId)
                    el.deviceTypeName = requiredDeviceType ? requiredDeviceType.label : "N/A"
                    return el;
                }),
                isFetching: false
            })
        }

        if (nextProps.getDeveloperQuotaSuccess) {
            const findObject = nextProps.getDeveloperQuotaSuccess.response.find(product => product.productName === 'report')
            let remainingReportsCount = findObject['remaining'],
                totalReportsCount = findObject['total'],
                usedReportsCount = findObject['used'];
            this.setState({
                remainingReportsCount,
                usedReportsCount,
                totalReportsCount
            })
        }

        if (nextProps.getEmailSuggestionsSuccess) {
            this.setState({
                emailSuggestions: nextProps.getEmailSuggestionsSuccess.response.users.map(user => user.email),
                isFetchingSuggestions: false
            })
        }

        if (nextProps.getAllDeviceTypesSuccess) {
            this.setState({
                deviceTypesList: nextProps.getAllDeviceTypesSuccess.response.map(el => {
                    return {
                        value: el.id,
                        label: el.name,
                        deviceGroups: el.deviceGroups.map(group => {
                            return {
                                value: group.deviceGroupId,
                                label: group.name
                            }
                        })
                    }
                })
            }, () => { this.props.getAllReports() })
        }

        if (nextProps.saveReportSuccess) {
            this.setState((prevState) => ({
                modalLoader: false,
                addOrEditReportModal: false,
                isFetching: true,
                remainingReportsCount: prevState.payload.id ? prevState.remainingReportsCount : prevState.remainingReportsCount - 1,
                usedReportsCount: prevState.payload.id ? prevState.usedReportsCount : prevState.usedReportsCount + 1,
            }), () => this.props.getAllReports())
        }

        if (nextProps.getReportByIdSuccess) {
            let payload = { ...nextProps.getReportByIdSuccess.response },
                listOfGroupsForSelectedDeviceType = this.state.deviceTypesList.find(el => el.value === payload.deviceTypeId).deviceGroups;
            this.setState({
                modalLoader: false,
                payload,
                listOfGroupsForSelectedDeviceType
            })
        }

        if (nextProps.deleteReportSuccess) {
            let reportsList = this.state.reportsList.filter(el => el.id != nextProps.deleteReportSuccess.id)
            this.setState((prevState) => ({
                isFetching: false,
                modalType: "success",
                isOpen: true,
                message2: nextProps.deleteReportSuccess.response,
                reportsList,
                remainingReportsCount: prevState.remainingReportsCount + 1,
                usedReportsCount: prevState.usedReportsCount - 1,
            }))
        }


        if (nextProps.getReportHistorySuccess) {
            this.setState({
                isFetchingReportHistory: false,
                reportHistory: nextProps.getReportHistorySuccess.response
            })
        }

        if (nextProps.getReportHistoryFailure) {
            this.setState({
                isFetchingReportHistory: false,
                modalType: "error",
                isOpen: true,
                message2: nextProps.getReportHistoryFailure.error,
                reportHistory: []
            })
        }

        if (nextProps.generateReportSuccess) {
            let anchorTag = document.createElement('a'),
                response = { ...nextProps.generateReportSuccess.response };
            anchorTag.href = response.url;
            anchorTag.click();
            let reportsList = cloneDeep(this.state.reportsList)
            reportsList.map((temp) => {
                if (temp.id === response.id) {
                    temp.generateReportLoader = false
                }
            })
            this.setState({
                reportsList,
                modalType: "success",
                isOpen: true,
                message2: "Report generated successfully",
            })
        }

        if (nextProps.generateReportFailure) {
            let reportsList = cloneDeep(this.state.reportsList),
                response = { ...nextProps.generateReportFailure }
            reportsList.map((temp) => {
                if (temp.id === response.addOns.id) {
                    temp.generateReportLoader = false
                }
            })
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: response.error,
                reportsList
            })
        }

        if (nextProps.deleteReportFailure) {
            this.setState({
                isFetching: false,
                modalType: "error",
                isOpen: true,
                message2: nextProps.deleteReportFailure.error
            })
        }

        if (nextProps.saveReportFailure) {
            this.setState({
                modalLoader: false,
                modalType: "error",
                isOpen: true,
                message2: nextProps.saveReportFailure.error,
            })
        }

        if (nextProps.getAllDeviceTypesFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.getAllDeviceTypesFailure.error,
            })
        }

        if (nextProps.getReportByIdFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.getReportByIdFailure.error,
                modalLoader: false,
                addOrEditReportModal: false,
            })
        }
    }

    componentDidUpdate(prevProps, prevState) {
        if (Object.keys(SELECTORS).some(prop => this.props[prop])) {
            this.props.resetToInitialState()
        }
    }

    inputChangeHandler = ({ currentTarget }) => {
        let payload = produce(this.state.payload, draftState => {
            if (currentTarget.id == "isScheduled") {
                draftState.isScheduled = currentTarget.checked
                draftState.hour = 0
                draftState.minutes = 0
                draftState.daysOfWeek = []
            }
            else if (currentTarget.name == "format") {
                draftState.format = currentTarget.value
            }
            else if (currentTarget.id === "name") {
                if (/^[a-zA-Z]([\w\s]*[a-zA-Z0-9 _])?$/.test(currentTarget.value) || /^$/.test(currentTarget.value)) {
                    draftState[currentTarget.id] = (currentTarget.value).replace(/  +/g, ' ');
                }
            }
            else
                draftState[currentTarget.id] = currentTarget.value
        })
        this.setState({
            payload,
            errorMessage: false
        });
    }

    onCloseHandler = () => {
        this.setState({
            isOpen: false,
            modalType: "",
            message2: "",
        })
    }

    closeAddEditReportModal = () => {
        let payload = {
            "name": "",
            "reportTemplate": "Device Uptime Report",
            "description": "",
            "deviceTypeId": "",
            "selectedGroupId": "",
            "duration": 1,  	//duration in hours
            "emails": [],
            "format": "CSV",
            "isScheduled": true,
            "hour": 0,
            "minutes": 1,
            "daysOfWeek": [],
            "timeZone": "",
        };
        this.setState({
            addOrEditReportModal: false,
            payload,
            errorMessage: null,
            selectedReportId: "",
            allAttributes: [],
        })
    }

    saveReport = (event) => {
        event.preventDefault()
        let payload = JSON.parse(JSON.stringify(this.state.payload)),
            errorMessage;
        if (!payload.name) {
            errorMessage = "Please enter Name."
            return this.setState({
                errorMessage
            })
        }
        if (!payload.deviceTypeId) {
            errorMessage = "Please select atleast one Device Type."
            return this.setState({
                errorMessage
            })
        }
        // if (payload.isScheduled && (payload.hour == 0) && (payload.minutes == 0)) {
        //     errorMessage = "Schedule time cannot be empty."
        //     return this.setState({
        //         errorMessage
        //     })
        // }
        if (payload.isScheduled && payload.daysOfWeek.length == 0) {
            errorMessage = "Please select atleast one day."
            return this.setState({
                errorMessage
            })
        }

        payload.hour = parseInt(payload.hour)
        payload.minutes = parseInt(payload.minutes)
        this.setState({
            modalLoader: true,
            errorMessage: null
        }, () => this.props.saveReport(payload))
    }

    deleteHandler = (id) => {
        this.setState({
            confirmState: true,
            reportToBeDeleted: id
        })
    }

    cancelClicked = () => {
        this.setState({
            reportToBeDeleted: "",
            confirmState: false,
        })
    }

    editReport = (id) => {
        this.setState({
            addOrEditReportModal: true,
            modalLoader: true,
            selectedReportId: id,
        }, () => {
            this.props.getReportById(id)
        })
    }

    generateReport = (id) => {
        let reportsList = cloneDeep(this.state.reportsList)
        reportsList.map((temp) => {
            if (temp.id === id) {
                temp.generateReportLoader = true
            }
        })
        this.setState({
            reportsList
        }, () => this.props.generateReport(id))


    }

    openAddOrEditModal = () => {
        this.setState({
            modalLoader: false,
            addOrEditReportModal: true
        })
    }

    showHistoryModal = ({ name, id }) => {
        this.setState({
            historyModal: true,
            reportName: name,
            isFetchingReportHistory: true
        }, () => this.props.getReportHistory(id))
    }

    getColumns = () => {
        return [
            {
                Header: "Name", width: 17.5,
                filterable: true,
                sortable: true,
                accessor: "name",
                cell: (row) => (
                    <React.Fragment>
                        <div className="list-view-icon"><img src={`https://content.iot83.com/m83/icons/${row.format.toLowerCase()}.png`} /></div>
                        <div className="list-view-icon-box">
                            <h6 className="text-theme fw-600">{row.name}</h6>
                            <p>{`${row.description || "-"}`}</p>
                        </div>
                        {/*{Boolean(row.scheduleIds && row.scheduleIds.length) && <span className="btn btn-transparent btn-transparent-success"><i className="fal fa-clock"></i></span>}*/}
                    </React.Fragment>
                )
            },
            {
                Header: "Type", width: 15,
                cell: (row) => <h6 className="fw-400">{row.reportTemplate}</h6>,
                accessor: "reportTemplate",
                filterable: true,
                sortable: true,
                filter: ({ value, onChangeHandler, accessor }) =>
                    <select
                        className="form-control"
                        onChange={event => onChangeHandler(event, accessor)}
                        value={value ? value : 'All'}
                    >
                        <option value="">All</option>
                        {this.state.reportTemplates.map((el, index) => <option key={index} value={el.value}>{el.label}</option>)}
                    </select>
            },
            {
                Header: "Duration", width: 10,
                cell: (row) => (
                    <span className="alert alert-primary list-view-badge">Last {row.duration > 24 ? row.duration / 24 : row.duration} {row.duration == 1 ? "hour" : row.duration > 24 ? "days" : "hours"}</span>
                )
            },
            {
                Header: "Device Type", width: 12.5,
                filterable: true,
                sortable: true,
                accessor: "deviceTypeName",
                cell: (row) => (
                    <div className="button-group-link">
                        <button className="btn btn-link text-truncate" onClick={() => { isNavAssigned('deviceTypes') && this.props.history.push(`/addOrEditDeviceType/${row.deviceTypeId}`) }}>{row.deviceTypeName}</button>
                    </div>
                )
            },
            { Header: "Created", width: 15, accessor: "createdAt", filterable: true, sortable: true },
            { Header: "Updated", width: 15, accessor: "updatedAt", filterable: true, sortable: true },
            {
                Header: "Actions", width: 15, cell: (row) => (
                    <div className="button-group">
                        {row.generateReportLoader ?
                            <button type="button" className="btn-transparent btn-transparent-green" data-tooltip data-tooltip-text="Loading" data-tooltip-place="bottom">
                                <i className="far fa-cog fa-spin"></i>
                            </button> :
                            <button className="btn-transparent btn-transparent-green" data-tooltip data-tooltip-text="Generate" data-tooltip-place="bottom" onClick={() => this.generateReport(row.id)}>
                                <i className="far fa-cogs"></i>
                            </button>
                        }
                        <button className="btn-transparent btn-transparent-yellow" data-tooltip data-tooltip-text="History" data-tooltip-place="bottom" onClick={() => this.showHistoryModal(row)}>
                            <i className="fas fa-history"></i>
                        </button>
                        <button className="btn-transparent btn-transparent-blue" data-tooltip data-tooltip-text="Edit" data-tooltip-place="bottom" onClick={() => this.editReport(row.id)}>
                            <i className="far fa-pencil"></i>
                        </button>
                        <button className="btn-transparent btn-transparent-red" data-tooltip data-tooltip-text="Delete" data-tooltip-place="bottom" onClick={() => this.deleteHandler(row.id)}>
                            <i className="far fa-trash-alt"></i>
                        </button>
                    </div>
                )
            },
        ]
    }

    addRemoveDays = (day) => {
        let payload = produce(this.state.payload, draftState => {
            if (draftState.daysOfWeek.includes(day)) {
                let removeIndex = draftState.daysOfWeek.findIndex(repeatDay => repeatDay === day)
                draftState.daysOfWeek.splice(removeIndex, 1)
            }
            else {
                draftState.daysOfWeek.push(day)
            }
        })
        this.setState({
            payload,
            errorMessage: null
        })
    }


    fetchEmailSuggestions = (value) => {
        clearTimeout(this.suggestionsTimeout)
        this.setState({
            emailSuggestions: [],
            isFetchingSuggestions: Boolean(value)
        }, () => {
            if (value)
                this.suggestionsTimeout = setTimeout(() => this.props.getEmailSuggestions({ email: value }), 1000)
        })
    }


    actionMetaDataChangeHandler = (value, key) => {
        let payload = produce(this.state.payload, draftState => {
            draftState.emails = value
        })
        this.setState({ payload })
    }

    refreshComponent = () => {
        this.props.getAllDeviceTypes();
        this.setState({
            isFetching: true,
            reportsList: [],
            payload: {
                "name": "",
                "reportTemplate": "Device Uptime Report",
                "description": "",
                "deviceTypeId": "",
                "selectedGroupId": "",
                "duration": 1,  	//duration in hours
                "emails": [],
                "format": "CSV",
                "isScheduled": true,
                "hour": 0,
                "minutes": 1,
                "daysOfWeek": [],
                "timeZone": this.props.timeZone,
            },
            isFetchingReportHistory: false,
            reportData: [],
            deviceTypesList: [],
            listOfGroupsForSelectedDeviceType: [],
            emailSuggestions: []
        })
    }

    addOrEditReport = () => {
        this.setState({
            addOrEditReportModal: true,
            payload: {
                "name": "",
                "reportTemplate": "Device Uptime Report",
                "description": "",
                "deviceTypeId": "",
                "selectedGroupId": "",
                "duration": 1,  	//duration in hours
                "emails": [],
                "format": "CSV",
                "isScheduled": true,
                "hour": 0,
                "minutes": 1,
                "daysOfWeek": [],
                "timeZone": this.props.timeZone,
            },
            errorMessage: null
        })
    }

    changeHandlerForReactSelect = (value, key) => {
        let payload = cloneDeep(this.state.payload),
            listOfGroupsForSelectedDeviceType = [...this.state.listOfGroupsForSelectedDeviceType]
        payload[key] = value.value
        if (key == "deviceTypeId") {
            listOfGroupsForSelectedDeviceType = value.deviceGroups
            payload.selectedGroupId = listOfGroupsForSelectedDeviceType.length ? listOfGroupsForSelectedDeviceType[0].value : ""
        }
        this.setState({
            payload,
            listOfGroupsForSelectedDeviceType
        })
    }

    getStatusBarClass = (row) => {
        if (row.isScheduled) {
            return { className: "green", tooltipText: "Scheduled" }
        }
        else {
            return { className: "gray", tooltipText: "Not-Scheduled" }
        }
    }

    render() {
        let filteredReportsList = (this.state.searchTerm ? this.state.reportsList.filter(el => (el.name.toLowerCase()).includes(this.state.searchTerm.toLowerCase())) : this.state.reportsList);
        return (
            <React.Fragment>
                <Helmet>
                    <title>Reports</title>
                    <meta name="description" content="Description of ManageReports" />
                </Helmet>

                <header className="content-header d-flex">
                    <div className="flex-60">
                        <h6>Reports -
                            <span className="content-header-badge-group">
                                <span className="content-header-badge-item">Allocated <span className="badge badge-pill badge-primary">{this.state.totalReportsCount}</span></span>
                                <span className="content-header-badge-item">Used <span className="badge badge-pill badge-success">{this.state.usedReportsCount}</span></span>
                                <span className="content-header-badge-item">Remaining <span className="badge badge-pill badge-warning">{this.state.remainingReportsCount}</span></span>
                            </span>
                        </h6>
                    </div>

                    <div className="flex-40 text-right">
                        <div className="content-header-group">
                            {/*<div className="search-box">
                                <span className="search-icon"><i className="far fa-search"></i></span>
                                <input type="text" className="form-control" disabled={this.state.reportsList.length === 0 || this.state.isFetching} value={this.state.searchTerm} onChange={({ currentTarget }) => {
                                    this.setState({
                                        searchTerm: currentTarget.value,
                                    })
                                }} placeholder="Search..." />
                                {this.state.searchTerm.length > 0 ? <button className="search-button" onClick={() => {
                                    this.setState({
                                        searchTerm: "",
                                    })
                                }}><i className="far fa-times"></i></button> : ""}
                            </div>*/}
                            <button className={this.state.toggleView == true ? "btn btn-light active" : "btn btn-light"} disabled={this.state.isFetching} data-tooltip="true" data-tooltip-text="List View" data-tooltip-place="bottom" onClick={() => this.setState({ toggleView: true })}><i className="fad fa-list-ul"></i></button>
                            <button className={this.state.toggleView == false ? "btn btn-light active" : "btn btn-light"} disabled={this.state.isFetching} data-tooltip="true" data-tooltip-text="Card View" data-tooltip-place="bottom" onClick={() => this.setState({ toggleView: false })}><i className="fad fa-table"></i></button>
                            <button className="btn btn-light" disabled={this.state.isFetching} data-tooltip="true" data-tooltip-text="Refresh" onClick={this.refreshComponent} data-tooltip-place="bottom"><i className="far fa-sync-alt"></i></button>
                            <button className="btn btn-primary" disabled={this.state.remainingReportsCount === 0 || this.state.isFetching} data-tooltip="true" data-tooltip-text="Add Report" data-tooltip-place="bottom" onClick={this.addOrEditReport}><i className="far fa-plus"></i></button>
                        </div>
                    </div>
                </header>

                <div className="content-body">
                    {this.state.isFetching ?
                        <Skeleton count={9} className="skeleton-list-loader" /> :
                        <React.Fragment>
                            {this.state.reportsList.length > 0 ?
                                filteredReportsList.length > 0 ?
                                    this.state.toggleView ?
                                        <ListingTable
                                            columns={this.getColumns()}
                                            statusBar={this.getStatusBarClass}
                                            data={filteredReportsList}
                                        /> :
                                        <ul className="card-view-list">
                                            {this.state.reportsList.map((item, index) =>
                                                <li key={index}>
                                                    <div className="card-view-box">
                                                        <div className="card-view-header">
                                                            <span data-tooltip data-tooltip-text={item.isScheduled ? "Scheduled" : "Not-Scheduled"} data-tooltip-place="bottom" className={item.isScheduled ? "card-view-header-status bg-success" : "card-view-header-status bg-danger"}></span>
                                                            <span className="alert alert-primary"><i className="fas fa-history mr-1"></i>Last {item.duration > 24 ? item.duration / 24 : item.duration} {item.duration == 1 ? "hour" : item.duration > 24 ? "days" : "hours"}</span>
                                                            <div className="dropdown">
                                                                <button className="btn-transparent btn-transparent-gray" type="button" data-toggle="dropdown">
                                                                    <i className="fas fa-ellipsis-v"></i>
                                                                </button>
                                                                <div className="dropdown-menu">
                                                                    {item.generateReportLoader ?
                                                                        <button type="button" className="dropdown-item"><i className="far fa-cog fa-spin"></i> Loading</button> :
                                                                        <button className="dropdown-item" onClick={() => this.generateReport(item.id)}>
                                                                            <i className="far fa-cogs"></i>Generate
                                                                        </button>
                                                                    }
                                                                    <button className="dropdown-item" onClick={() => this.showHistoryModal(item)}>
                                                                        <i className="fas fa-history"></i>History
                                                                    </button>
                                                                    <button className="dropdown-item" onClick={() => this.editReport(item.id)}>
                                                                        <i className="far fa-pencil"></i>Edit
                                                                    </button>
                                                                    <button className="dropdown-item" onClick={() => this.deleteHandler(item.id)}>
                                                                        <i className="far fa-trash-alt"></i>Delete
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="card-view-body">
                                                            <div className="card-view-icon">
                                                                <img src={`https://content.iot83.com/m83/icons/${item.format.toLowerCase()}.png`} />
                                                            </div>
                                                            <h6><i className="far fa-address-book"></i>{item.name ? item.name : "N/A"}</h6>
                                                            <p><i className="far fa-file-alt"></i>{item.description ? item.description : "N/A"}</p>
                                                            <div className="button-group-link">
                                                                <p>
                                                                    <i className="far fa-microchip"></i>
                                                                    <button className="btn btn-link" onClick={() => { isNavAssigned('deviceTypes') && this.props.history.push(`/addOrEditDeviceType/${item.deviceTypeId}`) }}>{item.deviceTypeName}</button>
                                                                </p>
                                                            </div>
                                                            <p><span className="badge badge-pill badge-light">{item.reportTemplate}</span></p>
                                                        </div>
                                                        <div className="card-view-footer d-flex">
                                                            <div className="flex-50 p-3">
                                                                <h6>Created</h6>
                                                                <p><strong>By - </strong>{item.createdBy}</p>
                                                                <p><strong>At - </strong>{new Date(item.createdAt).toLocaleString('en-US', { createdAt: localStorage.createdAt })}</p>
                                                            </div>
                                                            <div className="flex-50 p-3">
                                                                <h6>Updated</h6>
                                                                <p><strong>By - </strong>{item.updatedBy}</p>
                                                                <p><strong>At - </strong>{new Date(item.updatedAt).toLocaleString('en-US', { createdAt: localStorage.createdAt })}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            )}
                                        </ul>
                                    :
                                    <NoDataFoundMessage />
                                :
                                <AddNewButton
                                    text1="No Reports available"
                                    text2="You haven't created any reports yet. Please create a report first."
                                    imageIcon="addReport.png"
                                    createItemOnAddButtonClick={this.openAddOrEditModal}
                                />
                            }
                        </React.Fragment>
                    }
                </div>

                {/* add new report */}
                <SlidingPane
                    className=''
                    overlayClassName='sliding-form'
                    closeIcon={<div></div>}
                    isOpen={this.state.addOrEditReportModal || false}
                    from='right'
                    width='500px'
                >
                    <div className="modal-content">
                        <div className="modal-header">
                            <h6 className="modal-title">{this.state.selectedReportId ? "Edit" : "Add New"} Report
                                <button type="button" className="close" onClick={this.closeAddEditReportModal}><i className="fas fa-angle-right"></i></button>
                            </h6>
                        </div>

                        {this.state.modalLoader ?
                            <div className="modal-loader">
                                <i className="fad fa-sync-alt fa-spin"></i>
                            </div> :
                            <React.Fragment>
                                <div className="modal-body">
                                    {this.state.errorMessage &&
                                        <React.Fragment>
                                            {Array.isArray(this.state.errorMessage) ?
                                                this.state.errorMessage.map((val, index) => (
                                                    <p className="modal-body-error" key={index}>{val}</p>
                                                )) :
                                                <p className="modal-body-error">{this.state.errorMessage}</p>
                                            }
                                        </React.Fragment>
                                    }

                                    <div className="form-group">
                                        <label className="form-group-label">Name : <i className="fas fa-asterisk form-group-required"></i></label>
                                        <input type="text" className="form-control" id="name" value={this.state.payload.name} required onChange={this.inputChangeHandler} autoFocus />
                                    </div>

                                    <div className="form-group">
                                        <label className="form-group-label">Description :</label>
                                        <textarea rows="3" type="text" id="description" className="form-control" value={this.state.payload.description} onChange={this.inputChangeHandler} />
                                    </div>

                                    <div className="form-group">
                                        <label className="form-group-label">Report Template : <i className="fas fa-asterisk form-group-required"></i></label>
                                        <ReactSelect
                                            name="dataSet"
                                            options={this.state.reportTemplates}
                                            isMulti={false}
                                            value={this.state.reportTemplates.find(el => el.value === this.state.payload.reportTemplate)}
                                            onChange={(value) => this.changeHandlerForReactSelect(value, "reportTemplate")}
                                            className="form-control-multi-select" >
                                        </ReactSelect>
                                    </div>

                                    <div className="form-group">
                                        <label className="form-group-label">Device Types : <i className="fas fa-asterisk form-group-required"></i></label>
                                        <ReactSelect
                                            name="dataSet"
                                            options={this.state.deviceTypesList}
                                            isMulti={false}
                                            className="form-control-multi-select"
                                            value={this.state.deviceTypesList.find(el => el.value === this.state.payload.deviceTypeId)}
                                            onChange={(value) => this.changeHandlerForReactSelect(value, "deviceTypeId")} >

                                        </ReactSelect>
                                    </div>

                                    {this.state.payload.reportTemplate == "Device Alarm Aggregate Report" &&
                                        <div className="form-group">
                                            <label className="form-group-label">Device Groups : <i className="fas fa-asterisk form-group-required"></i></label>
                                            <ReactSelect
                                                name="dataSet"
                                                options={this.state.listOfGroupsForSelectedDeviceType}
                                                isMulti={false}
                                                className="form-control-multi-select"
                                                value={this.state.listOfGroupsForSelectedDeviceType.find(el => el.value === this.state.payload.selectedGroupId)}
                                                onChange={(value) => this.changeHandlerForReactSelect(value, "selectedGroupId")} >
                                            </ReactSelect>
                                        </div>
                                    }

                                    <div className="form-group">
                                        <label className="form-group-label">Duration : <i className="fas fa-asterisk form-group-required"></i></label>
                                        <ReactSelect
                                            className="form-control-multi-select"
                                            value={this.state.durationList.find(el => el.value == this.state.payload.duration)}
                                            onChange={(value) => this.changeHandlerForReactSelect(value, "duration")}
                                            options={this.state.durationList}
                                            isMulti={false}
                                        />
                                    </div>

                                    <div className="form-group">
                                        <label className="check-box" >
                                            <span className="check-text">Do you want to schedule the report ?</span>
                                            <input type="checkbox" id="isScheduled" checked={this.state.payload.isScheduled} onChange={this.inputChangeHandler} />
                                            <span className="check-mark" />
                                        </label>
                                    </div>

                                    {this.state.payload.isScheduled &&
                                        <React.Fragment>
                                            <div className="d-flex">
                                                <div className="form-group flex-50 pd-r-10">
                                                    <label className="form-group-label">Hours :<i className="fas fa-asterisk form-group-required"></i></label>
                                                    <ReactSelect
                                                        className="form-control-multi-select"
                                                        value={{ label: `${this.state.payload.hour <= 9 ? '0' + this.state.payload.hour : this.state.payload.hour}`, value: this.state.payload.hour }}
                                                        onChange={(value) => this.changeHandlerForReactSelect(value, "hour")}
                                                        options={Array.from(Array(24).keys()).map((el, i) => {
                                                            return {
                                                                value: i,
                                                                label: `${i <= 9 ? '0' + i : i}`
                                                            }
                                                        })}
                                                        isMulti={false}
                                                    />
                                                </div>
                                                <div className="form-group flex-50 pd-l-10">
                                                    <label className="form-group-label">Minutes :<i className="fas fa-asterisk form-group-required"></i></label>
                                                    <ReactSelect
                                                        className="form-control-multi-select"
                                                        value={{ label: `${this.state.payload.minutes <= 9 ? '0' + this.state.payload.minutes : this.state.payload.minutes}`, value: this.state.payload.minutes }}
                                                        onChange={(value) => this.changeHandlerForReactSelect(value, "minutes")}
                                                        options={Array.from(Array(60).keys()).map((el, i) => {
                                                            return {
                                                                value: i,
                                                                label: `${i <= 9 ? '0' + i : i}`
                                                            }
                                                        })}
                                                        // isDisabled={false}
                                                        isMulti={false}
                                                    />
                                                </div>
                                            </div>

                                            <div className="form-group">
                                                <label className="form-group-label">Days : <i className="fas fa-asterisk form-group-required"></i></label>
                                                <ul className="day-list d-flex">
                                                    {this.state.daysList.map((day, index) => {
                                                        return (
                                                            <li key={index} onClick={() => this.addRemoveDays(day)} className={this.state.payload.daysOfWeek.includes(day) ? "active" : ''}>
                                                                {day.slice(0, 1)}
                                                            </li>
                                                        )
                                                    })}
                                                </ul>
                                            </div>
                                        </React.Fragment>
                                    }

                                    <div className="form-group">
                                        <label className="form-group-label">Recipient Emails :
                                            <span className="text-cyan f-10 float-right"><strong>Note:</strong> Please press enter(&crarr;) to add email.</span>
                                        </label>
                                        <div className="search-wrapper">
                                            <TagsWithSuggestions
                                                selectedTags={this.state.payload.emails || []}
                                                suggestionData={this.state.emailSuggestions}
                                                fetchSuggestions={this.fetchEmailSuggestions}
                                                isLoading={this.state.isFetchingSuggestions}
                                                onAddition={(tags) => this.actionMetaDataChangeHandler(tags, "emails")}
                                                validationRegex={{
                                                    regex: /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/,
                                                    validationMessage: "Invalid Email"
                                                }}
                                                allowNewEntries
                                                allowOnlyUnique
                                                showNotification={this.props.showNotification}
                                                fetchSuggestionsFromServer
                                                autoFocus={false}
                                            />
                                        </div>
                                    </div>

                                    <div className="form-group">
                                        <label className="form-group-label">Format : <i className="fas fa-asterisk form-group-required"></i></label>
                                        <div className="d-flex">
                                            <div className="flex-25 pd-r-10">
                                                <label className="radio-button">
                                                    <span className="radio-button-text">CSV</span>
                                                    <input type="radio" name="format" value="CSV" checked={this.state.payload.format === "CSV"} onChange={this.inputChangeHandler} />
                                                    <span className="radio-button-mark"></span>
                                                </label>
                                            </div>
                                            <div className="flex-25 pd-r-10">
                                                <label className="radio-button">
                                                    <span className="radio-button-text">PDF</span>
                                                    <input type="radio" name="format" value="PDF" checked={this.state.payload.format === "PDF"} onChange={this.inputChangeHandler} />
                                                    <span className="radio-button-mark"></span>
                                                </label>
                                            </div>
                                            <div className="flex-25 pd-r-10">
                                                <label className="radio-button">
                                                    <span className="radio-button-text">EXCEL</span>
                                                    <input type="radio" name="format" value="EXCEL" checked={this.state.payload.format === "EXCEL"} onChange={this.inputChangeHandler} />
                                                    <span className="radio-button-mark"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="modal-footer justify-content-start">
                                    <button className="btn btn-primary" onClick={this.saveReport}>{this.state.selectedReportId ? "Update" : "Save"}</button>
                                    <button type="button" className="btn btn-dark" onClick={this.closeAddEditReportModal}>Cancel</button>
                                </div>
                            </React.Fragment>
                        }
                    </div>
                </SlidingPane>
                {/* end add new report */}

                {/* report history modal */}
                {this.state.historyModal &&
                    <div className="modal d-block animated slideInDown" role="dialog" id="gitHistoryModal">
                        <div className="modal-dialog modal-lg">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h6 className="modal-title">Report - <span>{this.state.reportName.charAt(0).toUpperCase() + this.state.reportName.slice(1)}</span>
                                        <button type="button" className="close" data-dismiss="modal" data-tooltip="true" data-tooltip-text="Close" data-tooltip-place="bottom" onClick={() => this.setState({ historyModal: false })}><i className="far fa-times"></i></button>
                                    </h6>
                                </div>

                                {this.state.isFetchingReportHistory ?
                                    <div className="modal-loader" style={{ height: "490px" }}>
                                        <i className="fad fa-sync-alt fa-spin"></i>
                                    </div>
                                    :
                                    <div className="modal-body">
                                        <div className="history-timeline-wrapper">
                                            {this.state.reportHistory.length > 0 ?
                                                <ul className="list-style-none">
                                                    {this.state.reportHistory.map((report, index) => {
                                                        return (
                                                            <li key={index}>
                                                                <div className="history-timeline-box">
                                                                    <div className={`history-timeline-state bg-green`}></div>
                                                                    <div className="history-timeline-image">
                                                                        <img src={`https://content.iot83.com/m83/icons/${report.format.toLowerCase()}.png`} />
                                                                    </div>
                                                                    <div className="pd-l-50">
                                                                        <span className={`badge badge-pill badge-primary cursor-pointer`} onClick={() => window.open(report.s3path)}><i className="far fa-download mr-2"></i>Download</span>
                                                                        <h6>{new Date(report.timestamp).toLocaleString('en-US', { timeZone: localStorage.timeZone })}</h6>
                                                                        <p>generated at {getTimeDifference(report.timestamp)}</p>
                                                                    </div>

                                                                </div>
                                                            </li>
                                                        )
                                                    })}
                                                </ul> :
                                                <div className="inner-message-wrapper h-100">
                                                    <div className="inner-message-content">
                                                        <i className="fad fa-file-exclamation"></i>
                                                        <h6>There is no data to display</h6>
                                                    </div>
                                                </div>
                                            }
                                        </div>

                                    </div>
                                }
                            </div>
                        </div>
                    </div>
                }
                {/* end report history modal */}

                {this.state.isOpen &&
                    <NotificationModal
                        type={this.state.modalType}
                        message2={this.state.message2}
                        onCloseHandler={this.onCloseHandler}
                    />
                }

                {
                    this.state.confirmState &&
                    <ConfirmModel
                        status={"delete"}
                        deleteName="the selected report"
                        confirmClicked={() => {
                            this.setState({
                                confirmState: false,
                                isFetching: true
                            }, () => this.props.deleteReport(this.state.reportToBeDeleted))
                        }}
                        cancelClicked={() => {
                            this.cancelClicked()
                        }}
                    />
                }
            </React.Fragment >

        );
    }
}

ManageReports.propTypes = {
    dispatch: PropTypes.func.isRequired
};

let allSelectors = {}
Object.entries(SELECTORS).map(([key, value]) => {
    allSelectors[key] = value()
})

const mapStateToProps = createStructuredSelector(allSelectors);

function mapDispatchToProps(dispatch) {
    let allActions = { dispatch }
    Object.entries(ACTIONS).map(([key, value]) => {
        allActions[key] = (...args) => dispatch(value(...args))
    })
    return allActions;
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "manageReports", reducer });
const withSaga = injectSaga({ key: "manageReports", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(ManageReports);
