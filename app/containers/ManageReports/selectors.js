import { createSelector } from "reselect";
import { initialState } from "./reducer";
import * as CONSTANTS from "./constants"

const selectManageReportsDomain = state => state.get("manageReports", initialState);

let allCases = Object.values(CONSTANTS).filter(constant => typeof (constant) == "object");
export const allSelectors = {}
allCases.map(constant => {
    allSelectors[constant.successKey] = () => createSelector(selectManageReportsDomain, subState => subState[constant.successKey]);
    allSelectors[constant.failureKey] = () => createSelector(selectManageReportsDomain, subState => subState[constant.failureKey]);
})