/*
 *
 * ManageReports constants
 *
 */
export const RESET_TO_INITIAL_STATE = "app/DeviceTypes/RESET_TO_INITIAL_STATE";

export const SAVE_REPORT = {
	action: "app/ManageReports/SAVE_REPORT",
	success: "app/ManageReports/SAVE_REPORT_SUCCESS",
	failure: "app/ManageReports/SAVE_REPORT_FAILURE",
	urlKey: "saveReport",
	successKey: "saveReportSuccess",
	failureKey: "saveReportFailure",
	actionName: "saveReport",
	actionArguments: ["payload"]
}

export const GET_ALL_REPORTS = {
	action: "app/ManageReports/GET_ALL_REPORTS",
	success: "app/ManageReports/GET_ALL_REPORTS_SUCCESS",
	failure: "app/ManageReports/GET_ALL_REPORTS_FAILURE",
	urlKey: "getAllReports",
	successKey: "getAllReportsSuccess",
	failureKey: "getAllReportsFailure",
	actionName: "getAllReports",
	actionArguments: []
}

export const DELETE_REPORT = {
	action: "app/ManageReports/DELETE_REPORT",
	success: "app/ManageReports/DELETE_REPORT_SUCCESS",
	failure: "app/ManageReports/DELETE_REPORT_FAILURE",
	urlKey: "deleteReport",
	successKey: "deleteReportSuccess",
	failureKey: "deleteReportFailure",
	actionName: "deleteReport",
	actionArguments: ["id"]
}

export const GET_REPORT_BY_ID = {
	action: "app/ManageReports/GET_REPORT_BY_ID",
	success: "app/ManageReports/GET_REPORT_BY_ID_SUCCESS",
	failure: "app/ManageReports/GET_REPORT_BY_ID_FAILURE",
	urlKey: "getReportById",
	successKey: "getReportByIdSuccess",
	failureKey: "getReportByIdFailure",
	actionName: "getReportById",
	actionArguments: ["id"]
}

export const GET_ALL_DEVICE_TYPES = {
	action: "app/ManageReports/GET_ALL_DEVICE_TYPES",
	success: "app/ManageReports/GET_ALL_DEVICE_TYPES_SUCCESS",
	failure: "app/ManageReports/GET_ALL_DEVICE_TYPES_FAILURE",
	urlKey: "deviceTypeList",
	successKey: "getAllDeviceTypesSuccess",
	failureKey: "getAllDeviceTypesFailure",
	actionName: "getAllDeviceTypes",
	actionArguments: []
}

export const GET_USERS_EMAILS = {
	action: 'app/ManageReports/GET_USERS_EMAILS',
	success: "app/ManageReports/GET_USERS_EMAILS_SUCCESS",
	failure: "app/ManageReports/GET_USERS_EMAILS_FAILURE",
	urlKey: "fetchUsersWithPagination",
	failureKey: "getEmailSuggestionsFailure",
	successKey: "getEmailSuggestionsSuccess",
	actionName: "getEmailSuggestions",
	actionArguments: ["payload"]
}

export const GET_DEVELOPER_QUOTA = {
	action: 'app/ManageReports/GET_DEVELOPER_QUOTA',
	success: "app/ManageReports/GET_DEVELOPER_QUOTA_SUCCESS",
	failure: "app/ManageReports/GET_DEVELOPER_QUOTA_FAILURE",
	urlKey: "getDeveloperQuota",
	failureKey: "getDeveloperQuotaFailure",
	successKey: "getDeveloperQuotaSuccess",
	actionName: "getDeveloperQuota",
	actionArguments: ["payload"]
}

export const GET_REPORT_HISTORY = {
	action: 'app/ManageReports/GET_REPORT_HISTORY',
	success: "app/ManageReports/GET_REPORT_HISTORY_SUCCESS",
	failure: "app/ManageReports/GET_REPORT_HISTORY_FAILURE",
	urlKey: "getReportHistory",
	failureKey: "getReportHistoryFailure",
	successKey: "getReportHistorySuccess",
	actionName: "getReportHistory",
	actionArguments: ["id"]
}

export const GENERATE_REPORT = {
	action: 'app/ManageReports/GENERATE_REPORT',
	success: "app/ManageReports/GENERATE_REPORT_SUCCESS",
	failure: "app/ManageReports/GENERATE_REPORT_FAILURE",
	urlKey: "generateReport",
	failureKey: "generateReportFailure",
	successKey: "generateReportSuccess",
	actionName: "generateReport",
	actionArguments: ["id"]
}