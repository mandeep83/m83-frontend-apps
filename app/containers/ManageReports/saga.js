import { takeEvery } from 'redux-saga';
import * as CONSTANTS from './constants';

import { apiCallHandler } from '../../api';


let allApiCalls = Object.values(CONSTANTS).filter(constant => typeof (constant) == "object")

allApiCalls.map(apiCall => {
  apiCall.apiCallHandler = function* (action) {
    yield [apiCallHandler(action, apiCall.success, apiCall.failure, apiCall.urlKey)]
  }
  apiCall.watcher = function* () {
    yield takeEvery(apiCall.action, apiCall.apiCallHandler);
  }
})

export default function* rootSaga() {
  yield allApiCalls.map(apiCall => apiCall.watcher());
}
