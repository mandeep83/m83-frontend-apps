/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { takeEvery } from 'redux-saga';

import {
    DELETE_ACCOUNT, DELETE_ACCOUNT_FAILURE, DELETE_ACCOUNT_SUCCESS,GET_ACCOUNT_LIST, GET_ACCOUNT_LIST_FAILURE, GET_ACCOUNT_LIST_SUCCESS,
} from './constants';
import {apiCallHandler} from "../../api";


export function* apiGetAccountListHandlerAsync(action) {
    yield[apiCallHandler(action, GET_ACCOUNT_LIST_SUCCESS, GET_ACCOUNT_LIST_FAILURE, 'getAccountList')];
}
export function* deleteAccountApiHandlerAsync(action) {
    yield[apiCallHandler(action, DELETE_ACCOUNT_SUCCESS, DELETE_ACCOUNT_FAILURE, 'deleteAccountById')];
}


export function* watcherGetAccountList() {
    yield takeEvery(GET_ACCOUNT_LIST, apiGetAccountListHandlerAsync);
}

export function* watcherDeleteAcoountRequest() {
    yield takeEvery(DELETE_ACCOUNT, deleteAccountApiHandlerAsync);
}
export default function* rootSaga() {
    yield [
        watcherGetAccountList(),watcherDeleteAcoountRequest()
    ];
}
