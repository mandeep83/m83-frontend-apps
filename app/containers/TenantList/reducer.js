/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import {fromJS} from "immutable";
import {
    DEFAULT_ACTION, DELETE_ACCOUNT_FAILURE, DELETE_ACCOUNT_SUCCESS, GET_ACCOUNT_LIST_FAILURE,
    GET_ACCOUNT_LIST_SUCCESS
} from "./constants";

export const initialState = fromJS({});

function adminClientListReducer(state = initialState, action) {
    switch (action.type) {
        case DEFAULT_ACTION:
            return state;
        case GET_ACCOUNT_LIST_SUCCESS:
            return Object.assign({}, state, {
                accountList: action.response,
            });
        case GET_ACCOUNT_LIST_FAILURE:
            return Object.assign({}, state, {
                accountListError: {
                    error:action.error,
                    date:new Date()
                }
            });
        case DELETE_ACCOUNT_SUCCESS:
            let temp = Object.assign({}, state, {
                deleteSuccess: {timestamp : Date.now() , response: action.response}
            })
            temp.accountList = temp.accountList.filter(function(account) {
                if(account.id != action.response.id)
                    return account;
            })
            return temp;
        case DELETE_ACCOUNT_FAILURE:
            return Object.assign({}, state, {
                deleteError: {response : action.error, timestamp : Date.now()},
            });
        default:
            return state;
    }
}

export default adminClientListReducer;
