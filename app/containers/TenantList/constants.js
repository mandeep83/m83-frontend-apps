/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

export const DEFAULT_ACTION = "app/AdminClientList/DEFAULT_ACTION";

export const GET_ACCOUNT_LIST = 'app/clientDashboard/GET_ACCOUNT_LIST';
export const GET_ACCOUNT_LIST_SUCCESS = 'app/clientDashboard/GET_ACCOUNT_LIST_SUCCESS';
export const GET_ACCOUNT_LIST_FAILURE = 'app/clientDashboard/GET_ACCOUNT_LIST_FAILURE';

export const DELETE_ACCOUNT = 'app/clientDashboard/DELETE_ACCOUNT';
export const DELETE_ACCOUNT_SUCCESS = 'app/clientDashboard/DELETE_ACCOUNT_SUCCESS';
export const DELETE_ACCOUNT_FAILURE = 'app/clientDashboard/DELETE_ACCOUNT_FAILURE';

