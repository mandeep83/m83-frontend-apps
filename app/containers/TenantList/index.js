/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import ClientCaptcha from "react-client-captcha";

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import {
    getAccountListError,
    getAccountListSuccess,
    getDeleteError,
    getDeleteSuccess,
    getIsFetching
} from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import { accountDeleteHandler, getAccountList } from "./actions";
import Loader from "../../components/Loader/index";
import NotificationModal from '../../components/NotificationModal/Loadable'
import ReactTooltip from 'react-tooltip';
import {convertTimestampToDate, getInitials} from '../../commonUtils';
import ListingTable from "../../components/ListingTable/Loadable";
import NoDataFoundMessage from "../../components/NoDataFoundMessage/Loadable";
import AddNewButton from "../../components/AddNewButton/Loadable";

/* eslint-disable react/prefer-stateless-function */
export class TenantList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            openModal: false,
            accountToBeDeleted: '',
            isListView: false,
            accountList: [],
            filteredList: [],
            searchTerm: '',
            isFetching: true,
            givenCaptchaCode: '',
            userInput: '',
            isConfirm: false,
            showModal: false,
            selectedTenantId: null,
            showOwnersList: false,
        }
    }

    componentDidMount() {
        this.props.getAccountList();
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.accountList && nextProps.accountList !== this.state.accountList) {
            let accountList = nextProps.accountList;
            accountList.map(account => {
                if (account.subscriptionInfo.isPostPaymentEnabled) {
                    if (!account.licenseExpire) {
                        account.subscriptionStatus = "N/A"
                    } else if (account.licenseExpire < new Date().getTime()) {
                        account.subscriptionStatus = 'EXPIRED';
                        account.subscriptionColorClass = 'alert-danger';
                    } else {
                        account.subscriptionStatus = 'TRIAL';
                        account.subscriptionColorClass = 'alert-primary';
                    }
                } else {
                    if (account.subscriptionInfo.isSubscriptionCancelled) {
                        account.subscriptionStatus = 'CANCELLED';
                        account.subscriptionColorClass = 'alert-danger';
                    } else if (account.subscriptionInfo.isTrial === false && account.subscriptionInfo.paymentPending) {
                        account.subscriptionStatus = 'PAYMENT PENDING';
                        account.subscriptionColorClass = 'alert-warning';
                    } else {
                        account.subscriptionStatus = 'ACTIVE';
                        account.subscriptionColorClass = 'alert-success';
                    }
                }
            })
            this.setState({
                accountList,
                filteredList: accountList,
                isFetching: false,
            })
        }
        
        if (nextProps.accountListError && nextProps.accountListError !== this.props.accountListError) {
            this.setState({
                modalType: "error",
                openModal: true,
                message2: nextProps.accountListError.error
            })
        }
        
        if (nextProps.deleteErrorMessage && nextProps.deleteErrorMessage != this.props.deleteErrorMessage) {
            this.setState({
                modalType: "error",
                openModal: true,
                message2: nextProps.deleteErrorMessage.response,
                isFetching: false,
            })
        }
        
        if (nextProps.deleteSuccessMessage && nextProps.deleteSuccessMessage !== this.props.deleteSuccessMessage) {
            this.setState({
                modalType: "success",
                openModal: true,
                message2: nextProps.deleteSuccessMessage.response.message,
                isFetching: false,
            })
        }
    }

    confirmModalHandler = (id, tenantName) => {
        this.setState({
            accountToBeDeleted: id,
            tenantToBeDeleted: tenantName,
            showModal: true
        }, () => $('#confirmModal').modal('show'))

    }
    
    onCloseHandler = () => {
        this.setState({
            openModal: false,
            message2: ''
        })
    }

    onSearchHandler = (e) => {
        let filteredList = JSON.parse(JSON.stringify(this.state.accountList));
        if (e.target.value.length > 0) {
            filteredList = filteredList.filter((item) => item.tenantName.toLowerCase().includes(`${e.target.value.toLowerCase()}`)
            );
        }
        this.setState({
            filteredList,
            searchTerm: e.target.value
        })

    }

    emptySearchBox = () => {
        let tenantList = JSON.parse(JSON.stringify(this.state.accountList))
        this.setState({
            searchTerm: "",
            filteredList: tenantList
        })
    }

    refreshComponent = () => {
        this.setState({
            isFetching: true
        }, () => {
            this.props.getAccountList();
        })
    }

    inputHandler = (event) => {
        this.setState({
            userInput: event.target.value,
            isConfirm: this.state.givenCaptchaCode === event.target.value ? true : false,
        })
    }

    verificationHandler = () => {
        this.setState({
            isFetching: true,
            showModal: false
        }, () => {
            this.props.accountDeleteHandler(this.state.accountToBeDeleted)
            $('#confirmModal').modal('hide')
        })
    }
    
    getColumns = () => {
        return [
            {
                Header: "Tenant Name", width: 20, accessor: "tenantName", bold: true, sortable: true, filterable: true,
                cell: (row) => (
                    <React.Fragment>
                        <div className="list-view-icon">
                            <img src="https://content.iot83.com/m83/misc/addTenant.png" />
                        </div>
                        <div className="list-view-icon-box">
                            <h6 className="text-theme fw-600">{row.tenantName}</h6>
                            <p>{`${row.description || "-"}`}</p>
                        </div>
                    </React.Fragment>
                )
            },
            {
                Header: "URL", width: 20, accessor: "URL",
                cell: row => {
                    let baseUrl = window.API_URL.split('.')[1],
                        url;
                    url = `https://${row.tenantName}.${baseUrl}.com`
                    return (
                        <div className="button-group-link">
                            <button className="btn btn-link w-100 text-truncate text-initial f-13" onClick={() => { window.open(url, '_blank'); }}>{url}</button>
                        </div>
                    )
                }
            },
            {
                Header: "Type",
                width: 10,
                accessor: "type",
                cell: row => <span className={`badge badge-pill list-view-pill ${row.type == "MAKER" ? "alert-warning" : "alert-info"}`}>{row.type}</span>,
                filterable: true,
                sortable: true,
                filter: ({ value, onChangeHandler, accessor }) =>
                    <select
                        className="form-control"
                        onChange={event => onChangeHandler(event, accessor)}
                        value={value ? value : 'ALL'}
                    >
                        <option value="">ALL</option>
                        <option value="FLEX">FLEX</option>
                        <option value="MAKER">MAKER</option>
                    </select>
            },
            {
                Header: "Subscription Status",
                width: 15,
                accessor: "subscriptionStatus",
                cell: row => <span className={`badge badge-pill list-view-pill ${row.subscriptionColorClass}`}>{row.subscriptionStatus}</span>,
                filterable: true,
                sortable: true,
            },
            {
                Header: "Owner(s)",
                width: 10,
                accessor: "type",
                cell: row =>
                    <div className="list-view-alarm-count">
                        <i className="far fa-users"></i>
                        <span className="badge badge-warning cursor-pointer text-underline-hover"  onClick={() => { row.defaultAccountAdminList.length > 0 && this.showTenantOwners(row.id) }}>{row.defaultAccountAdminList.length}</span>
                    </div>
            },
            { Header: "Created At", width: "12_5",
                cell: row => (
                    <h6>{convertTimestampToDate(row.createdAt)}</h6>
                )
            },
            { Header: "Expiring On", width: "12_5",
                cell: row =>
                    <h6 className={row.licenseExpire && row.licenseExpire < new Date().getTime() ? 'text-red' : ''}>{row.licenseExpire ? convertTimestampToDate(row.licenseExpire) : "N/A"}</h6>
            },
        ]
    }

    showTenantOwners = (tenantId) => {
        this.setState({
            showOwnersList: true,
            selectedTenantId: tenantId,
        });
    }

    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>Manage Tenants</title>
                    <meta name="description" content="M83-ManageTenants" />
                </Helmet>

                {this.state.isFetching ? <Loader /> :
                    <React.Fragment>
                        <header className="content-header d-flex">
                            <div className="flex-60">
                                <h6>Tenants
                                    <span className="content-header-badge-group">
                                        <span className="content-header-badge-item">
                                            <span className="badge badge-pill badge-primary">{this.state.filteredList.length > 0 && this.state.filteredList.length}</span>
                                        </span>
                                    </span>
                                </h6>
                            </div>
                            <div className="flex-40 text-right">
                                <div className="content-header-group">
                                    {!this.state.isListView &&
                                        <div className="search-box">
                                            <span className="search-icon"><i className="far fa-search"></i></span>
                                            <input type="text" className="form-control" placeholder="Search..."
                                                   value={this.state.searchTerm} onChange={this.onSearchHandler}
                                                   disabled={this.state.filteredList.length == 0}/>
                                            {this.state.searchTerm.length > 0 &&
                                            <button className="search-button" onClick={() => this.emptySearchBox()}><i
                                                className="far fa-times"></i></button>}
                                        </div>
                                    }
                                    <button className={`btn btn-light ${!this.state.isListView ? "active": ""}`} onClick={()=> this.setState({isListView: false})}><i className="fad fa-table"></i></button>
                                    <button className={`btn btn-light ${this.state.isListView ? "active": ""}`} onClick={()=> this.setState({isListView: true})}><i className="fad fa-list-ul"></i></button>
                                    <button className="btn btn-light" data-tooltip data-tooltip-text="Refresh" data-tooltip-place="bottom" onClick={() => this.refreshComponent()}>
                                        <i className="far fa-sync-alt"></i>
                                    </button>
                                    <button className="btn btn-primary" data-tooltip data-tooltip-text="Add Tenant" data-tooltip-place="bottom" onClick={() => { this.props.history.push('/addOrEditTenant') }}><i className="far fa-plus"></i>
                                    </button>
                                </div>
                            </div>
                        </header>

                        <div className="content-body">
                            {this.state.accountList.length > 0 ?
                                this.state.filteredList.length > 0 ?
                                    this.state.isListView ?
                                        <ListingTable
                                            columns={this.getColumns()}
                                            data={this.state.filteredList}
                                        /> :
                                        <ul className="card-view-list"  id="deviceListCard">
                                            {this.state.filteredList.map((tenant, index) =>
                                                <li key={index}>
                                                    <div className="card-view-box">
                                                        <div className="card-view-header pd-l-10">
                                                            <span className={`mr-r-7 alert ${tenant.type == "MAKER" ? "alert-warning" : "alert-info"}`}>{tenant.type}</span>
                                                            {tenant.type !== 'MAKER' && <span className={`mr-r-7 alert ${tenant.subscriptionColorClass}`}>{tenant.subscriptionStatus}</span>}
                                                            <span className="alert alert-secondary bg-white text-underline-hover text-orange" onClick={() => { tenant.defaultAccountAdminList.length > 0 && this.showTenantOwners(tenant.id) }}>
                                                                <i className="fas fa-users mr-2"></i>Users - {tenant.defaultAccountAdminList.length}
                                                            </span>
                                                            {/*<div className="button-group">
                                                                <button className="btn-transparent btn-transparent-cyan" data-tooltip data-tooltip-text="View" data-tooltip-place="bottom" disabled={true} onClick={() => { this.props.history.push(`/tenantDetails/${tenant.tenantName}/${tenant.id}`) }}><i className="far fa-info"></i></button>
                                                                <button className="btn-transparent btn-transparent-blue" data-tooltip data-tooltip-text="Edit" data-tooltip-place="bottom" disabled={tenant.type === 'FLEX'} onClick={() => { this.props.history.push(`/addOrEditTenant/${tenant.tenantName}/${tenant.id}`) }}><i className="far fa-pencil"></i></button>
                                                                <button className="btn-transparent btn-transparent-red" data-tooltip data-tooltip-text="Delete" data-tooltip-place="bottom" disabled={tenant.defaultAccount || tenant.type === 'FLEX'} onClick={() => { this.confirmModalHandler(tenant.id, tenant.tenantName) }}><i className="far fa-trash-alt"></i></button>
                                                            </div>*/}
                                                        </div>
                                                        <div className="card-view-body">
                                                            <div className="card-view-icon">
                                                                <img src="https://content.iot83.com/m83/misc/addTenant.png" />
                                                            </div>
                                                            <h6><i className="far fa-address-book"></i>{tenant.tenantName}</h6>
                                                            <p><i className="far fa-file-alt"></i>{`${tenant.description || "-"}`}</p>
                                                            <p><i className="far fa-building"></i>{`${tenant.companyName || "-"}`}</p>
                                                            <p className="text-primary text-underline-hover" onClick={() => { window.open(`https://${tenant.tenantName}.${window.API_URL.split('.')[1]}.com`, '_blank'); }}>
                                                                <i className="far fa-link"></i>
                                                                {`https://${tenant.tenantName}.${window.API_URL.split('.')[1]}.com`}
                                                            </p>
                                                        </div>
                                                        <div className="card-view-footer d-flex">
                                                            <div className="flex-50 p-3">
                                                                <h6>Created At</h6>
                                                                <p>{convertTimestampToDate(tenant.createdAt)}</p>
                                                            </div>
                                                            <div className="flex-50 p-3">
                                                                <h6>{tenant.licenseExpire < new Date().getTime() ? 'Expired On ' : 'Expiring On '}</h6>
                                                                <p className={tenant.licenseExpire && tenant.licenseExpire < new Date().getTime() ? 'text-red' : ''}>{tenant.licenseExpire ? convertTimestampToDate(tenant.licenseExpire) : "-"}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            )}
                                        </ul>
                                    :
                                    <NoDataFoundMessage />
                                :
                                <AddNewButton
                                    text1="No tenants available"
                                    text2="You haven't created any tenants yet"
                                    imageIcon="addTenant.png"
                                    createItemOnAddButtonClick={() => { this.props.history.push('/addOrEditTenant') }}
                                />
                            }
                        </div>
                    </React.Fragment>
                }

                {this.state.showOwnersList &&
                    <div className="modal animated slideInDown show d-block">
                        <div className="modal-dialog modal-dialog-centered">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h6 className="modal-title">Owners for <span>{this.state.filteredList.filter(tenant => tenant.id === this.state.selectedTenantId)[0].tenantName}</span> Tenant
                                        <button type="button" className="close" data-tooltip data-tooltip-text="Close" data-tooltip-place="bottom" onClick={() => { this.setState({ showOwnersList: false }) }}>
                                            <i className="far fa-times"></i>
                                        </button>
                                    </h6>
                                </div>
                                <div className="modal-body overflow-y-auto" style={{ height: 400 }}>
                                    <div className="content-table">
                                        <table className="table table-bordered">
                                            <thead>
                                            <tr>
                                                <th width="60%">Email</th>
                                                <th width="40%">Contact No</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {this.state.filteredList.filter(tenant => tenant.id === this.state.selectedTenantId)[0]['defaultAccountAdminList'].map((owner, index) =>
                                                <tr key={index}>
                                                    <td><p>{owner.email}</p></td>
                                                    <td><p>{owner.phoneNumber ? owner.phoneNumber : '-'}</p></td>
                                                </tr>
                                            )}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div className="modal-footer">
                                    <button className="btn btn-success" onClick={() => { this.setState({ showOwnersList: false }) }}>OK</button>
                                </div>
                            </div>
                        </div>
                    </div>
                }

                {/* delete modal */}
                <div className="modal fade animated slideInDown" id="confirmModal">
                    {this.state.showModal &&
                        <div className="modal-dialog modal-dialog-centered">
                            <div className="modal-content">
                                <div className="modal-body">
                                    <div className="delete-content">
                                        <div className="delete-icon">
                                            <i className="fad fa-trash-alt text-red"></i>
                                        </div>
                                        <h4>Delete!</h4>
                                        <h6>Are you sure you want to delete Tenant {this.state.tenantToBeDeleted} ?</h6>
                                        <div className="form-group captcha-box">
                                            <ClientCaptcha captchaClassName="captcha-text" captchaCode={code => this.setState({ givenCaptchaCode: code, userInput: '', isConfirm: false })} />
                                            <input type="text" className="form-control" placeholder="Enter captcha" value={this.state.userInput} onChange={() => this.inputHandler(event)}></input>
                                        </div>
                                    </div>
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-dark" data-dismiss="modal" onClick={() => { this.setState({ accountToBeDeleted: '', userInput: '', isConfirm: false, showModal: false }, () => $('#confirmModal').modal('hide')) }}>No</button>
                                    <button type="button" className="btn btn-success" data-dismiss="modal" disabled={!this.state.isConfirm} onClick={() => this.verificationHandler()}>Delete</button>
                                </div>
                            </div>
                        </div>
                    }
                </div>
                {/* end delete modal */}

                {this.state.openModal &&
                    <NotificationModal
                        type={this.state.modalType}
                        message2={this.state.message2}
                        onCloseHandler={this.onCloseHandler}
                    />
                }

            </React.Fragment>
        );
    }
}

TenantList.propTypes = {
    dispatch: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
    accountList: getAccountListSuccess(),
    accountListError: getAccountListError(),
    deleteSuccessMessage: getDeleteSuccess(),
    deleteErrorMessage: getDeleteError(),
    isFetching: getIsFetching()
});

function mapDispatchToProps(dispatch) {
    return {
        dispatch,
        getAccountList: () => dispatch(getAccountList()),
        accountDeleteHandler: (id) => dispatch(accountDeleteHandler(id))
    };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "tenantList", reducer });
const withSaga = injectSaga({ key: "tenantList", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(TenantList);
