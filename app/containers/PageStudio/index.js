/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import * as Selectors from './selectors';
import reducer from './reducer';
import saga from './saga';
import * as Actions from './actions'
import Loader from '../../components/Loader/Loadable';
import NotificationModal from '../../components/NotificationModal/Loadable'
import ConfirmModel from '../../components/ConfirmModel/Loadable'
import ReactTooltip from "react-tooltip";
import { getInitials, convertTimestampToDate, getFilteredItems } from '../../commonUtils';
import ReactTable from 'react-table';
import TagsInput from 'react-tagsinput';
import { cloneDeep } from 'lodash';
import startCase from 'lodash/startCase';
import ListingTable from "../../components/ListingTable/Loadable";
import NoDataFoundMessage from "../../components/NoDataFoundMessage/Loadable"
import AddNewButton from "../../components/AddNewButton/Loadable"
import { PAGE_TYPE_OPTIONS } from '../CreateOrEditPage/commonConstants';


/* eslint-disable react/prefer-stateless-function */
export class DashboardStudio extends React.Component {

    state = {
        dashboards: [],
        dashboardToBeDeleted: '',
        isFetching: true,
        isClonePageLoader: false,
        filteredDataList: [],
        inputTags: [],
        viewType: "TABLE",
        connectorsList: [],
        filters: {
            pageType: "",
            connectorId: this.props.location.state ? this.props.location.state.selectedConnectorId : "",
            name: ""
        }
    }

    componentDidMount() {
        this.props.getConnectorsListByCategory();
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.dashboardList && (nextProps.dashboardList != this.props.dashboardList)) {
            let dashboards = localStorage.tenantType === "SAAS" ? nextProps.dashboardList.filter(el => el.resourceOwner) : nextProps.dashboardList
            this.setState({
                dashboards,
                isFetching: false,
                filteredDataList: dashboards,
                isClonePageLoader: false,
            })
        }

        if (nextProps.dashboardListError && nextProps.dashboardListError !== this.props.dashboardListError) {
            this.setState({
                modalType: 'error',
                isOpen: true,
                message2: nextProps.dashboardListError,
                isFetching: false,
            })
        }

        if (nextProps.connectorListSuccess && nextProps.connectorListSuccess !== this.props.connectorListSuccess) {
            let filters = cloneDeep(this.state.filters)
            filters.connectorId = nextProps.connectorListSuccess.length ? nextProps.connectorListSuccess[0].id : filters.connectorId
            this.setState({
                filters,
                connectorsList: nextProps.connectorListSuccess,
                isFetching: Boolean(nextProps.connectorListSuccess.length)
            },() => nextProps.connectorListSuccess.length && this.props.getDashboards(this.props.match.params.type, this.props.match.params.model))
        }
        if (nextProps.connectorListFailure && nextProps.connectorListFailure !== this.props.connectorListFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.connectorListFailure,
                isFetching: false,
            })
        }

        if (nextProps.deleteDashboardSuccess && nextProps.deleteDashboardSuccess != this.props.deleteDashboardSuccess) {
            this.setState({
                isOpen: true,
                message2: nextProps.deleteDashboardSuccess,
                modalType: 'success',
                isFetching: false,
            })
        }

        if (nextProps.deleteDashboardError && nextProps.deleteDashboardError !== this.props.deleteDashboardError) {
            this.setState({
                modalType: 'error',
                isOpen: true,
                message2: nextProps.deleteDashboardError,
                isFetching: false,
            })
        }

        if (nextProps.clonePageSuccess && (nextProps.clonePageSuccess != this.props.clonePageSuccess)) {
            this.setState({
                isOpen: true,
                message2: nextProps.clonePageSuccess.response,
                modalType: 'success',
            }, () => this.props.getDashboards(this.props.match.params.type, this.props.match.params.model));
        }

        if (nextProps.clonePageError && nextProps.clonePageError != this.props.clonePageError) {
            this.setState({
                modalType: 'error',
                isOpen: true,
                message2: nextProps.clonePageError.response,
                isClonePageLoader: false,
                isFetching: false,
            })
        }
    }

    onCloseHandler = () => {
        this.setState({
            isOpen: false,
            message2: '',
            modalType: '',
        })
    }

    confirmModalHandler = (id, deleteName) => {
        this.setState({
            dashboardToBeDeleted: id,
            confirmState: true,
            deleteName
        });
    }

    clonePage = (id) => {
        this.setState({ isFetching: true }, () => this.props.clonePage(id));
    }


    inputTagsChangeHandler = (tags) => {
        this.setState({
            inputTags: tags
        })
    }

    goToAddorEditPage = () => {
        this.props.history.push({
            pathname: "/addOrEditDashboard",
            state: {
                selectedConnector: this.state.filters.connectorId,
                pageType: this.state.filters.pageType,
            }
        })
        // this.props.history.push(this.props.match.params.type && this.props.match.params.model ? '/addOrEditDashboard/' + this.props.match.params.type + "/" + this.props.match.params.model : '/addOrEditDashboard');
    }

    getColumns = () => {
        return [
            {
                Header: "Name", width: 25,
                cell: (row) => (
                    <React.Fragment>
                        <div className="list-view-icon"><img src="https://content.iot83.com/m83/misc/dashbord-fallback.png" /></div>
                        <div className="list-view-icon-box">
                            <h6 className="text-theme fw-600">{row.name}</h6>
                            <p>{row.description}</p>
                        </div>
                    </React.Fragment>
                )
            },
            {
                Header: "Page Type", width: 15, cell: (row) => (
                    <h6 className={`text-capitalize ${row.pageType == "listing" ? "text-green" : row.pageType == "deviceDetail" ? "text-primary" : row.pageType == "aggregate" ? "text-orange" : row.pageType == "eventHistory" ? "text-pink" : "text-indigo"}`}>
                        <i className={`f-12 mr-r-7 fad ${row.pageType == "listing" ? "fa-list-ul" : row.pageType == "deviceDetail" ? "fa-info" : row.pageType == "aggregate" ? "fa-analytics" : row.pageType == "eventHistory" ? "fa-calendar-alt" : "fa-chart-area"}`}></i>
                        {row.pageType}
                    </h6>
                )
            },
            {
                Header: "Device Type", width: 15,
                cell: row => (
                    <div className="button-group button-group-link">
                        <button className="btn btn-link" onClick={() => { this.props.history.push(`/addOrEditDeviceType/${row.connectorId}`) }}>{row.connectorName}</button>
                    </div>
                )
            },
            { Header: "Created", width: 15, accessor: "createdAt" },
            { Header: "Updated", width: 15, accessor: "updatedAt" },
            {
                Header: "Actions", width: 15, cell: (row) => (
                    <div className="button-group">
                        <button className="btn-transparent btn-transparent-cyan" onClick={() => { this.props.history.push(`/previewPage/${row.id}`) }} data-tip data-for={"view" + row.id}><i className="far fa-info"></i></button>
                        <ReactTooltip id={"view" + row.id} place="bottom" type="dark">
                            <div className="tooltipText"><p>View</p></div>
                        </ReactTooltip>

                        {/*<button className="btn-transparent btn-transparent-green" onClick={() => this.clonePage(row.id)} data-tip data-for={"clone" + row.id}><i className="far fa-copy"></i></button>
                        <ReactTooltip id={"clone" + row.id} place="bottom" type="dark">
                            <div className="tooltipText"><p>Clone</p></div>
                        </ReactTooltip>*/}

                        <button className="btn-transparent btn-transparent-blue" onClick={() => { this.props.history.push(`/addOrEditDashboard/${row.id}`) }} data-tip data-for={"edit" + row.id}><i className="far fa-pencil"></i></button>

                        <ReactTooltip id={"edit" + row.id} place="bottom" type="dark">
                            <div className="tooltipText"><p>Edit</p></div>
                        </ReactTooltip>

                        <button className="btn-transparent btn-transparent-red" onClick={() => this.confirmModalHandler(row.id, row.name)} data-tip data-for={"delete" + row.id}><i className="far fa-trash-alt"></i></button>

                        <ReactTooltip id={"delete" + row.id} place="bottom" type="dark">
                            <div className="tooltipText"><p>Delete</p></div>
                        </ReactTooltip>
                    </div>
                )
            },
        ]
    }

    filterChangeHandler = ({ currentTarget }) => {
        let { id, value } = currentTarget
        let filters = cloneDeep(this.state.filters)
        filters[id] = value
        this.setState({ filters })
    }

    getAddNewButton = () => {
        if (!this.state.connectorsList.length) {
            return <AddNewButton
                text1="No dashboard(s) available"
                text2="You haven't created any device type(s) yet. Please create a device type first."
                createItemOnAddButtonClick={() => this.props.history.push(`/addOrEditDeviceType`)}
                imageIcon="addDevice.png"
            />
        }
        let { connectorId, pageType } = this.state.filters
        let isFiltered = connectorId || pageType
        let text2 = "You haven't created any dashboard(s) yet"
        if (isFiltered) {
            let bothFilters = connectorId && pageType
            let text = bothFilters ? "of this Page Type and Device Type" : connectorId ? "for this Device Type" : "with this Page Type"
            text2 = "You haven't created any dashboard(s) " + text
        }
        return (
            <AddNewButton
                text1="No dashboard(s) available"
                text2={text2}
                imageIcon="addPage.png"
                createItemOnAddButtonClick={this.goToAddorEditPage}
            />
        )
    }

    render() {
        let filteredDashboardsList = getFilteredItems(this.state.dashboards, this.state.filters)
        return (
            <React.Fragment>
                <Helmet>
                    <title>Dashboards</title>
                    <meta name="description" content="M83-Dashboards" />
                </Helmet>

                <header className="content-header d-flex">
                    <div className="flex-60">
                        <h6>Dashboards{/* -*/}
                            {/*<span className="content-header-badge-group">
                                <span className="content-header-badge-item">Used <span className="badge badge-pill badge-success">{this.state.dashboards.length > 0 ? this.state.dashboards.length : 0}</span></span>
                                <span className="content-header-badge-item">Allocated <span className="badge badge-pill badge-primary">2</span></span>
                                <span className="content-header-badge-item">Remaining <span className="badge badge-pill badge-warning">2</span></span>
                            </span>*/}
                        </h6>
                    </div>
                    {this.state.dashboards.length > 0 &&
                        <div className="flex-40 text-right">
                            <div className="content-header-group">
                                <div className="search-box">
                                    <span className="search-icon"><i className="far fa-search"></i></span>
                                    <input type="text" className="form-control" placeholder="Search..." id="name" value={this.state.filters.name} onChange={this.filterChangeHandler} disabled={!this.state.dashboards.length} />
                                    {Boolean(this.state.filters.name) && <button className="search-button" onClick={() => this.filterChangeHandler({ currentTarget: { id: "name", value: "" } })}><i className="far fa-times"></i></button>}
                                </div>
                                {/* <button className="btn btn-light"><i className="fad fa-list-ul"></i></button>
                            <button className="btn btn-light"><i className="fad fa-table"></i></button> */}
                                <button className="btn btn-primary" onClick={() => {
                                    let url = this.props.match.params.type && this.props.match.params.model ? '/addOrEditDashboard/' + this.props.match.params.type + "/" + this.props.match.params.model : '/addOrEditDashboard';
                                    this.props.history.push(url)
                                }}><i className="far fa-plus"></i></button>
                            </div>
                        </div>}
                </header>

                {this.state.isFetching ?
                    <Loader /> :

                    <React.Fragment>
                        {/*{this.state.dashboards.length > 0 &&*/}
                        {(this.state.connectorsList.length > 0 && this.state.dashboards.length > 0) &&
                            <div className="d-flex content-filter">
                                <div className="flex-50 form-group pd-r-10">
                                    <div className="input-group">
                                        <div className="input-group-prepend">
                                            <span className="input-group-text">Device Type :</span>
                                        </div>
                                        <select id='connectorId' className="form-control" value={this.state.filters.connectorId} onChange={this.filterChangeHandler}>
                                            {this.state.connectorsList.map(connector => {
                                                return (
                                                    <option key={connector.id} value={connector.id}>{connector.name}</option>
                                                )
                                            })}
                                        </select>
                                    </div>
                                </div>
                                <div className="flex-50 form-group pd-l-5 pd-r-5">
                                    <div className="input-group">
                                        <div className="input-group-prepend">
                                            <span className="input-group-text">Page Type :</span>
                                        </div>
                                        <select id='pageType' className="form-control" value={this.state.filters.pageType} onChange={this.filterChangeHandler}>
                                            <option value="">All</option>
                                            {Object.entries(PAGE_TYPE_OPTIONS).map(([pageType, displayName]) => (
                                                <option key={pageType} value={pageType}>{displayName}</option>
                                            ))}
                                        </select>
                                    </div>
                                </div>
                            </div>
                        }
                        {/*}*/}
                        <div className={`content-body ${this.state.dashboards.length > 0 && "content-filter-body"}`}>
                            {
                                this.state.dashboards.length > 0 ?
                                    filteredDashboardsList.length > 0 ?
                                        <ListingTable
                                            columns={this.getColumns()}
                                            data={filteredDashboardsList}
                                        />
                                        :
                                        this.state.filters.name ?
                                            <NoDataFoundMessage /> : this.getAddNewButton()
                                    :
                                    this.getAddNewButton()
                            }
                        </div>
                    </React.Fragment>
                }
                {
                    this.state.confirmState &&

                    <ConfirmModel
                        status={"delete"}
                        deleteName={this.state.deleteName}
                        confirmClicked={() => {
                            this.setState(
                                {
                                    isFetching: true,
                                    confirmState: false,
                                    isOpen: false
                                },
                                () => this.props.deleteDashboard(this.state.dashboardToBeDeleted));
                        }}
                        cancelClicked={() => { this.setState({ dashboardToBeDeleted: '', confirmState: false }) }}
                    />
                }

                {
                    this.state.isOpen &&
                    <NotificationModal
                        type={this.state.modalType}
                        message2={this.state.message2}
                        onCloseHandler={this.onCloseHandler}
                    />
                }
            </React.Fragment >
        )
    }
}

DashboardStudio.propTypes = {
    dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
    dashboardList: Selectors.getDashboardList(),
    dashboardListError: Selectors.getDashboardListError(),
    deleteDashboardSuccess: Selectors.getDeleteDashboardSuccess(),
    deleteDashboardError: Selectors.getDeleteDashboardError(),
    clonePageSuccess: Selectors.getClonePageSuccess(),
    clonePageError: Selectors.getClonePageError(),
    connectorListSuccess: Selectors.connectorListSuccess(),
    connectorListFailure: Selectors.connectorListFailure()
});

function mapDispatchToProps(dispatch) {
    return {
        dispatch,
        getDashboards: (type, model) => dispatch(Actions.getDashboards(type, model)),
        deleteDashboard: (id) => dispatch(Actions.deleteDashboard(id)),
        clonePage: (id) => dispatch(Actions.clonePage(id)),
        getConnectorsListByCategory: () => dispatch(Actions.getConnectorsListByCategory())
    };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'dashboardStudio', reducer });
const withSaga = injectSaga({ key: 'dashboardStudio', saga });

export default compose(
    withReducer,
    withSaga,
    withConnect,
)(DashboardStudio);
