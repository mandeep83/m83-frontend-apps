/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

export const DEFAULT_ACTION = 'app/DashboardStudio/DEFAULT_ACTION';

export const GET_DASHBOARD_LIST = 'app/DashboardStudio/GET_DASHBOARD_LIST';
export const DASHBOARD_LIST_SUCCESS = 'app/DashboardStudio/DASHBOARD_LIST_SUCCESS';
export const DASHBOARD_LIST_ERROR = 'app/DashboardStudio/DASHBOARD_LIST_ERROR';

export const DELETE_DASHBOARD_REQUEST = 'app/DashboardStudio/DELETE_DASHBOARD_REQUEST';
export const DELETE_DASHBOARD_SUCCESS = 'app/DashboardStudio/DELETE_DASHBOARD_SUCCESS';
export const DELETE_DASHBOARD_ERROR = 'app/DashboardStudio/DELETE_DASHBOARD_ERROR';

export const CLONE_PAGE = 'app/DashboardStudio/CLONE_PAGE';
export const CLONE_PAGE_SUCCESS = 'app/DashboardStudio/CLONE_PAGE_SUCCESS';
export const CLONE_PAGE_FAILURE = 'app/DashboardStudio/CLONE_PAGE_FAILURE';

export const GET_CONNECTORS_BY_CATEGORY = 'app/DashboardStudio/GET_CONNECTORS_BY_CATEGORY';
export const GET_CONNECTORS_BY_CATEGORY_SUCCESS = 'app/DashboardStudio/GET_CONNECTORS_BY_CATEGORY_SUCCESS';
export const GET_CONNECTORS_BY_CATEGORY_FAILURE = 'app/DashboardStudio/GET_CONNECTORS_BY_CATEGORY_FAILURE';