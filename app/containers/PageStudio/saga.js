/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { takeEvery } from 'redux-saga';
import * as CONSTANTS from "./constants";
import { apiCallHandler } from '../../api';

export function* getDashboardListApiHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.DASHBOARD_LIST_SUCCESS, CONSTANTS.DASHBOARD_LIST_ERROR, 'getDashboardList')];
}

export function* deleteDashboardApiHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.DELETE_DASHBOARD_SUCCESS, CONSTANTS.DELETE_DASHBOARD_ERROR, 'deleteDashboardById')];
}


export function* clonePageApiHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.CLONE_PAGE_SUCCESS, CONSTANTS.CLONE_PAGE_FAILURE, 'clonePage')];
}


export function* getConnectorsListByCategoryApiHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_CONNECTORS_BY_CATEGORY_SUCCESS, CONSTANTS.GET_CONNECTORS_BY_CATEGORY_FAILURE, 'deviceTypeList')];
}

export function* watcherClonePageRequest() {
  yield takeEvery(CONSTANTS.CLONE_PAGE, clonePageApiHandlerAsync);
}


export function* watcherGetDashboardListRequest() {
  yield takeEvery(CONSTANTS.GET_DASHBOARD_LIST, getDashboardListApiHandlerAsync);
}

export function* watcherDeleteDashboardRequest() {
  yield takeEvery(CONSTANTS.DELETE_DASHBOARD_REQUEST, deleteDashboardApiHandlerAsync);
}

export function* watcherGetConnectorsListByCategory() {
  yield takeEvery(CONSTANTS.GET_CONNECTORS_BY_CATEGORY, getConnectorsListByCategoryApiHandlerAsync);
}

export default function* rootSaga() {
  yield [
    watcherGetDashboardListRequest(),
    watcherDeleteDashboardRequest(),
    watcherClonePageRequest(),
    watcherGetConnectorsListByCategory()
  ];
}
