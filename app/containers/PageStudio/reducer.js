/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { fromJS } from 'immutable';
import * as CONSTANTS from "./constants";
export const initialState = fromJS({});

function dashboardStudioReducer(state = initialState, action) {
  switch (action.type) {
    case CONSTANTS.DEFAULT_ACTION:
      return state;
    case CONSTANTS.DASHBOARD_LIST_SUCCESS:
      return Object.assign({}, state, {
        dashboardList: action.response,
      });
    case CONSTANTS.DASHBOARD_LIST_ERROR:
      return Object.assign({}, state, {
        dashboardListError: action.error,
      });
    case CONSTANTS.DELETE_DASHBOARD_REQUEST:
      return Object.assign({}, state, {
        deleteDashboardSuccess: undefined,
        deleteDashboardError: undefined
      });
    case CONSTANTS.DELETE_DASHBOARD_SUCCESS:
      let tempList = Object.assign({}, state).dashboardList;
      tempList = tempList.filter((temp) => (
        temp.id != action.response.id
      ))
      return Object.assign({}, state, {
        dashboardList: tempList,
        deleteDashboardSuccess: action.response.message
      });
    case CONSTANTS.DELETE_DASHBOARD_ERROR:
      return Object.assign({}, state, {
        deleteDashboardError: action.error,
      });
    case CONSTANTS.CLONE_PAGE_SUCCESS:
      return Object.assign({}, state, {
        clonePageSuccess: {
          response: action.response.message,
          id: action.response.id,
          random: Math.random()
        },
      });
    case CONSTANTS.CLONE_PAGE_FAILURE:
      return Object.assign({}, state, {
        clonePageError: {
          response: action.error,
          id: action.addOns.id,
          random: Math.random()
        },
      });
    case CONSTANTS.GET_CONNECTORS_BY_CATEGORY_SUCCESS:
      return Object.assign({}, state, {
        connectorListSuccess: action.response,
      });
    case CONSTANTS.GET_CONNECTORS_BY_CATEGORY_FAILURE:
      return Object.assign({}, state, {
        connectorListFailure: action.error,
      });
    default:
      return state;
  }
}

export default dashboardStudioReducer;

