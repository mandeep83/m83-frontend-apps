/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/*
 *
 * MyPlan constants
 *
 */

export const RESET_TO_INITIAL_STATE = "app/MyPlan/RESET_TO_INITIAL_STATE";
export const RESET_TO_INITIAL_STATE_PROPS = "app/MyPlan/RESET_TO_INITIAL_STATE_PROPS";


export const GET_ADD_ONS = {
    action : 'app/MyPlan/GET_ADD_ONS',
    success: "app/MyPlan/GET_ADD_ONS_SUCCESS",
    failure: "app/MyPlan/GET_ADD_ONS_FAILURE",
    urlKey: "getAddOns",
    successKey: "getAddOnsSuccess",
    failureKey: "getAddOnsFailure",
    actionName: "getAddOns",
    actionArguments : []
}

export const GET_PROFILE_DETAILS = {
    action : 'app/MyPlan/GET_PROFILE_DETAILS',
    success: "app/MyPlan/GET_PROFILE_DETAILS_SUCCESS",
    failure: "app/MyPlan/GET_PROFILE_DETAILS_FAILURE",
    urlKey: "getProfileDetails",
    successKey: "getProfileDetailsSuccess",
    failureKey: "getProfileDetailsFailure",
    actionName: "getProfileDetails",
    actionArguments : ["payload"]
}

export const GET_TENANT_DASHBOARD_DETAILS = {
    action : 'app/MyPlan/GET_TENANT_DASHBOARD_DETAILS',
    success: "app/MyPlan/GET_TENANT_DASHBOARD_DETAILS_SUCCESS",
    failure: "app/MyPlan/GET_TENANT_DASHBOARD_DETAILS_FAILURE",
    urlKey: "getTenantDashboardDetails",
    successKey: "getTenantDashboardDetailsSuccess",
    failureKey: "getTenantDashboardDetailsFailure",
    actionName: "getTenantDashboardDetails",
}

export const GET_TRANSACTION_DETAILS = {
    action : 'app/MyPlan/GET_TRANSACTION_DETAILS',
    success: "app/MyPlan/GET_TRANSACTION_DETAILS_SUCCESS",
    failure: "app/MyPlan/GET_TRANSACTION_DETAILS_FAILURE",
    urlKey: "getTenantTransactionDetails",
    successKey: "getTenantTransactionDetailsSuccess",
    failureKey: "getTenantTransactionDetailsFailure",
    actionName: "getTenantTransactionDetails",
    actionArguments : ["payload"]
}

export const PAYMENT_OF_TENANT_ADD_ONS = {
    action : 'app/MyPlan/PAYMENT_OF_TENANT_ADD_ONS',
    success: "app/MyPlan/PAYMENT_OF_TENANT_ADD_ONS_SUCCESS",
    failure: "app/MyPlan/PAYMENT_OF_TENANT_ADD_ONS_FAILURE",
    urlKey: "paymentOfTenantAddOns",
    successKey: "paymentOfTenantAddOnsSuccess",
    failureKey: "paymentOfTenantAddOnsFailure",
    actionName: "paymentOfTenantAddOns",
    actionArguments : ["payload", "alarmDetails"]
}

export const GET_LICENSES_LIST = {
    action : 'app/MyPlan/GET_LICENSES_LIST',
    success: "app/MyPlan/GET_LICENSES_LIST_SUCCESS",
    failure: "app/MyPlan/GET_LICENSES_LIST_FAILURE",
    urlKey: "getAllLicenses",
    successKey: "licensesListSuccess",
    failureKey: "licensesListFailure",
    actionName: "getLicensesList",
    actionArguments : []
}

export const CANCEL_SUBSCRIPTION = {
    action : 'app/MyPlan/CANCEL_SUBSCRIPTION',
    success: "app/MyPlan/CANCEL_SUBSCRIPTION_SUCCESS",
    failure: "app/MyPlan/CANCEL_SUBSCRIPTION_FAILURE",
    urlKey: "cancelSubscriptionTenant",
    successKey: "cancelSubscriptionSuccess",
    failureKey: "cancelSubscriptionFailure",
    actionName: "cancelSubscription",
    actionArguments : ["reason"]
}

export const RENEW_SUBSCRIPTION = {
    action : 'app/MyPlan/RENEW_SUBSCRIPTION',
    success: "app/MyPlan/RENEW_SUBSCRIPTION_SUCCESS",
    failure: "app/MyPlan/RENEW_SUBSCRIPTION_FAILURE",
    urlKey: "renewSubscriptionFlex",
    successKey: "renewSubscriptionSuccess",
    failureKey: "renewSubscriptionFailure",
    actionName: "renewSubscription",
    actionArguments : []
}

export const PURCHASE_SUBSCRIPTION = {
    action : 'app/MyPlan/PURCHASE_SUBSCRIPTION',
    success: "app/MyPlan/PURCHASE_SUBSCRIPTION_SUCCESS",
    failure: "app/MyPlan/PURCHASE_SUBSCRIPTION_FAILURE",
    urlKey: "purchaseSubscriptionFlex",
    successKey: "purchaseSubscriptionSuccess",
    failureKey: "purchaseSubscriptionFailure",
    actionName: "purchaseSubscription",
    actionArguments : []
}

export const QUOTA_INFORMATION = {
    action : 'app/MyPlan/QUOTA_INFORMATION',
    success: "app/MyPlan/QUOTA_INFORMATION_SUCCESS",
    failure: "app/MyPlan/QUOTA_INFORMATION_FAILURE",
    urlKey: "getDeveloperQuota",
    successKey: "quotaInformationSuccess",
    failureKey: "quotaInformationFailure",
    actionName: "getQuotaInformation",
    actionArguments : ["payload"]
}

export const GET_USER_DETAILS_BY_ID = {
    action : 'app/MyPlan/GET_USER_DETAILS_BY_ID',
    success: "app/MyPlan/GET_USER_DETAILS_BY_ID_SUCCESS",
    failure: "app/MyPlan/GET_USER_DETAILS_BY_ID_FAILURE",
    urlKey: "getUserDetailsById",
    successKey: "getUserDetailsByIdSuccess",
    failureKey: "getUserDetailsByIdFailure",
    actionName: "getUserDetailsById",
    actionArguments : ["id"]
}

export const UPLOAD_PROFILE_PICTURE = {
    action : 'app/MyPlan/UPLOAD_PROFILE_PICTURE',
    success: "app/MyPlan/UPLOAD_PROFILE_PICTURE_SUCCESS",
    failure: "app/MyPlan/UPLOAD_PROFILE_PICTURE_FAILURE",
    urlKey: "uploadProfilePicture",
    successKey: "uploadProfilePictureSuccess",
    failureKey: "uploadProfilePictureFailure",
    actionName: "uploadProfilePicture",
    actionArguments : ["payload"]
}

export const UPDATE_USER_DETAILS = {
    action : 'app/MyPlan/UPDATE_USER_DETAILS',
    success: "app/MyPlan/UPDATE_USER_DETAILS_SUCCESS",
    failure: "app/MyPlan/UPDATE_USER_DETAILS_FAILURE",
    urlKey: "saveUserDetails",
    successKey: "updateUserDetailsSuccess",
    failureKey: "updateUserDetailsFailure",
    actionName: "updateUserDetails",
    actionArguments : ["payload"]
}
