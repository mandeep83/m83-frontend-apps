/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * MyPlan
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import reducer from "./reducer";
import saga from "./saga";
import { getTimeDifference } from "../../commonUtils";
import { allSelectors as SELECTORS } from "./selectors";
import { getUserDetailsSuccess, getUserDetailsError, isLoadingUserDetails } from "../Header/selectors";
import { allActions as ACTIONS } from './actions'
import Loader from "../../components/Loader/Loadable";
import ListingTable from "../../components/ListingTable/Loadable";
import jwt_decode from "jwt-decode";
import PhoneInput from "react-phone-input-2";
import 'react-phone-input-2/lib/style.css';
import ImageUploader from "../../components/ImageUploader";
import { cloneDeep } from "lodash"

/* eslint-disable react/prefer-stateless-function */
let ALL_SELECTORS = { ...SELECTORS, getUserDetailsSuccess, getUserDetailsError, isLoadingUserDetails }
export class MyPlan extends React.Component {

    state = {
        listOfAddOns: [],
        profileDetails: {},
        isFetching: 4,
        selectedTab: "subscription",
        tenantTransactionDetail: [],
        features: [
            {
                categoryName: "Device",
                products: [
                    {
                        name: "no_of_devices",
                        displayName: "Devices (Total)",
                        isBasePackageFeature: true,
                        showDescription: false,
                    },
                    {
                        name: "attributeKeys",
                        displayName: "Device Attribute(s)",
                        isBasePackageFeature: true,
                        showDescription: false,
                    },
                    {
                        name: "device_groups",
                        displayName: "Device Group(s) / Device Type",
                        isBasePackageFeature: true,
                        showDescription: false,
                    },
                    {
                        name: "device_types",
                        displayName: "Device Type",
                        isBasePackageFeature: true,
                        showDescription: false,
                    }

                ]
            },
            {
                categoryName: "Operation",
                products: [
                    {
                        name: "events",
                        displayName: "Event(s) / Device Type",
                        isBasePackageFeature: true,
                        showDescription: true,
                    },
                    {
                        name: "rules",
                        displayName: "Rule(s) / Event",
                        isBasePackageFeature: true,
                        showDescription: true,
                    },
                    {
                        name: "report",
                        displayName: "Report(s)",
                        isBasePackageFeature: true,
                        showDescription: false,
                    },
                    {
                        name: "schedules",
                        displayName: "Schedule(s)",
                        isBasePackageFeature: true,
                        showDescription: false,
                    }
                ]
            },
            {
                categoryName: "Notification",
                products: [
                    {
                        name: "rpc",
                        displayName: "RPC / mo",
                        isBasePackageFeature: true,
                        showDescription: true,
                    },
                    {
                        name: "webhooks",
                        displayName: "Webhooks / mo",
                        isBasePackageFeature: true,
                        showDescription: true,
                    },
                    {
                        name: "email",
                        displayName: "Emails / mo",
                        isBasePackageFeature: true,
                        showDescription: false,
                    },
                    {
                        name: "sms",
                        displayName: "SMS / mo",
                        isBasePackageFeature: true,
                        showDescription: false,
                    }
                ]
            },
            {
                categoryName: "IAM",
                products: [
                    {
                        name: "users",
                        displayName: "User(s)",
                        isBasePackageFeature: true,
                        showDescription: true,
                    },
                    {
                        name: "role",
                        displayName: "Role(s)",
                        isBasePackageFeature: true,
                        showDescription: true,
                    },
                    {
                        name: "groups",
                        displayName: "User Group(s)",
                        isBasePackageFeature: true,
                        showDescription: true,
                    }
                ]
            },
            {
                categoryName: "Code",
                products: [
                    {
                        name: "docker_images",
                        displayName: "Docker Images",
                        isBasePackageFeature: true,
                        showDescription: false,
                    },
                    {
                        name: "faas",
                        displayName: "FaaS(s)",
                        isBasePackageFeature: true,
                        showDescription: false,
                    },
                    {
                        name: "api",
                        displayName: "API(s)",
                        isBasePackageFeature: true,
                        showDescription: false,
                    }
                ]
            },
            {
                categoryName: "Visualization",
                products: [
                    {
                        name: "pages",
                        displayName: "Page(s)",
                        isBasePackageFeature: true,
                        showDescription: false,
                    }
                ]
            },
            {
                categoryName: "Connector",
                products: [
                    {
                        name: "connectors",
                        displayName: "Connector(s)",
                        isBasePackageFeature: true,
                        showDescription: false,
                    }
                ]
            },
            {
                categoryName: "Analytics",
                products: [
                    {
                        name: "notebook",
                        displayName: "ETL Notebook",
                        isBasePackageFeature: true,
                        showDescription: false,
                    },
                    {
                        name: "udf",
                        displayName: "ETL UDF",
                        isBasePackageFeature: true,
                        showDescription: false,
                    },
                    {
                        name: "etl",
                        displayName: "ETL- Data Transformation",
                        isBasePackageFeature: true,
                        showDescription: false,
                    }
                ]
            },
            {
                categoryName: "Storage",
                products: [
                    {
                        name: "retention",
                        displayName: "Data Storage (in months)",
                        isBasePackageFeature: true,
                        showDescription: true,
                    }
                ]
            }
        ],
        baseLicenseDetails: [],
        quotaDetails: [],
        isCancelSubscription: false,
        reloadBillingInfo: false,
        reloadAddOnsInfo: false,
        reloadSummaryInfo: false,
        reloadQuotaInfo: true,
        addOnPurchaseInProgress: false,
        resumeSubscriptionInProgress: false,
        tenantDashboardDetail: {
            features: []
        },
    }

    componentDidMount() {
        const upgrade = new URLSearchParams(this.props.location.search).get("upgrade")
        if (upgrade) {
            if (upgrade === "true")
                this.props.showNotification("success", "Transaction completed Successfully");
            else
                this.props.showNotification("error", "Add-on Purchase Failed, lease try after some time")
        }
        let payload = this.getQuotaPayload();
        this.props.getLicensesList();
        this.props.getAddOns();
        this.props.getProfileDetails();
        this.props.getTenantTransactionDetails();
        this.props.getQuotaInformation(payload);
    }

    getQuotaPayload = () => {
        let payload = [];
        this.state.features.map(feature => {
            feature.products.map(product => {
                let obj = {
                    productName: product.name
                }
                payload.push(obj);
            })
        })
        return payload;
    }

    static getDerivedStateFromProps(nextProps, state) {
        let newProp = Object.keys(ALL_SELECTORS).find(prop => nextProps[prop])
        if (newProp) {
            let propData = nextProps[newProp].error ? nextProps[newProp].error : nextProps[newProp].response
            if (nextProps.getAddOnsSuccess) {
                if (state.reloadAddOnsInfo) {
                    return {
                        ...state,
                        listOfAddOns: propData.map(item => {
                            item.upgradeQty = 0
                            return item
                        }),
                        reloadAddOnsInfo: false
                    }
                } else {
                    return {
                        ...state,
                        listOfAddOns: propData.map(item => {
                            item.upgradeQty = 0
                            return item
                        }),
                        isFetching: state.isFetching - 1
                    }
                }
            }

            if (nextProps.getProfileDetailsSuccess) {
                return {
                    ...state,
                    profileDetails: propData,
                    isFetching: state.isFetching - 1
                }
            }

            if (nextProps.getTenantDashboardDetailsSuccess) {
                let tenantDashboardDetail = propData;
                tenantDashboardDetail.features = state.features;
                tenantDashboardDetail.features.map(feature => {
                    feature.products.map(product => {
                        let productDetails = tenantDashboardDetail.productList.filter(productObj => productObj.name === product.name)[0];
                        if (productDetails) {
                            product.totalCount = product.name === 'retention' ? productDetails['count'] / 30 >= 12 ? `${productDetails['count'] / (365)}` : `${productDetails['count'] / (30)}` : productDetails['count'];
                            product.addOnCount = product.name === 'retention' ? '-' : product.totalCount - product.baseCount;
                            product.description = productDetails['description'];
                        }
                    })
                })
                if (state.reloadSummaryInfo) {
                    return {
                        tenantDashboardDetail,
                        reloadSummaryInfo: false
                    }
                } else {
                    return {
                        tenantDashboardDetail,
                        isFetching: state.isFetching - 1
                    }
                }
            }

            if (nextProps.getTenantTransactionDetailsSuccess) {
                if (state.reloadBillingInfo) {
                    return {
                        tenantTransactionDetail: propData,
                        reloadBillingInfo: false
                    }
                } else {
                    return {
                        tenantTransactionDetail: propData,
                        isFetching: state.isFetching - 1
                    }
                }

            }

            if (nextProps.renewSubscriptionSuccess) {
                window.open(propData, "_self")
            }

            if (nextProps.purchaseSubscriptionSuccess) {
                window.open(propData, "_self")
            }

            if (nextProps.paymentOfTenantAddOnsSuccess) {
                if (state.totalPayableAmountThisBillingCycle === 0) {
                    nextProps.getAddOns();
                    nextProps.showNotification("success", "Request Completed Successfully !")
                    return {
                        reloadAddOnsInfo: true,
                        addOnPurchaseInProgress: false
                    }

                } else {
                    window.open(propData, "_self");
                }
            }

            if (nextProps.paymentOfTenantAddOnsFailure) {
                return {
                    addOnPurchaseInProgress: false
                }
            }

            if (nextProps.licensesListSuccess) {
                let licenses = propData.find(el => el.tenantCategory === "FLEX"),
                    baseLicenseDetails = [],
                    features = state.features;
                features.map(feature => {
                    feature.products.map(product => {
                        let obj = {
                            isBasePackageFeature: product.isBasePackageFeature,
                            name: product.displayName,
                            count: product.name === 'retention' ? licenses[product.name] / 30 >= 12 ? `${licenses[product.name] / (365)}` : `${licenses[product.name] / (30)}` : licenses[product.name],
                        }
                        product.baseCount = product.name === 'retention' ? licenses[product.name] / 30 >= 12 ? `${licenses[product.name] / (365)}` : `${licenses[product.name] / (30)}` : licenses[product.name];
                        baseLicenseDetails.push(obj);
                    })
                });
                nextProps.getTenantDashboardDetails();
                return {
                    baseLicenseDetails
                }
            }

            if (nextProps.cancelSubscriptionSuccess) {
                let tenantDashboardDetail = { ...state.tenantDashboardDetail }
                tenantDashboardDetail.isCancelEnabled = false;
                nextProps.showNotification("success", propData.message, () => {
                    window.location.reload()
                })
                return {
                    tenantDashboardDetail,
                }
            }
            if (nextProps.quotaInformationSuccess) {
                let quotaDetails = [];
                state.features.map(feature => {
                    feature.products.map(product => {
                        if (product.name !== "device_groups" && product.name !== "events" && product.name !== "rules" && product.name !== "retention" && product.name !== "attributeKeys") {
                            let quotaObj = nextProps.quotaInformationSuccess.response.filter(productObj => productObj.productName === product.name)[0];
                            quotaObj.usedPercentage = Math.round((quotaObj.used / quotaObj.total) * 100);
                            quotaObj.displayName = product.displayName;
                            quotaDetails.push(quotaObj);

                        }

                    })
                });
                quotaDetails = quotaDetails.sort((x, y) => y.usedPercentage - x.usedPercentage);
                return {
                    quotaDetails,
                    reloadQuotaInfo: false
                }
            }

            if (nextProps.uploadProfilePictureSuccess) {
                return {
                    isUploadingImage: false,
                }
            }

            if (nextProps.uploadProfilePictureFailure) {
                return {
                    isUploadingImage: false,
                }
            }
            // if (nextProps.updateUserDetailsSuccess) {
            //     let userDetails = cloneDeep(state.updatedUserDetails);
            //     return {
            //         userDetails,
            //         isUpdatingUserDetails: false,
            //         editUserDetailsModal: false,
            //     }
            // }

            // if (nextProps.updateUserDetailsFailure) {
            //     return {
            //         isUpdatingUserDetails: false,
            //     }
            // }
        }
        return null
    }

    onCloseHandler = () => {
        this.setState({
            isFetching: 0
        })
    }

    componentDidUpdate(prevProps, prevState) {
        let newProp = Object.keys(ALL_SELECTORS).find(prop => this.props[prop])
        if (newProp) {
            if (newProp.includes("Failure")) {
                this.props.showNotification("error", this.props[newProp].error, this.onCloseHandler)
            }
            this.props.resetToInitialStateProps(newProp)
        }
    }

    onIncrementOrDecrementChange = (item, operation) => {
        if ((operation === "decrement" && item.upgradeQty == 0) || !item.isPublished || (this.state.tenantDashboardDetail.isCancelEnabled === false && this.state.tenantDashboardDetail.isPostPaymentEnabled === false && item.amount > 0)) {
            return
        } else if (operation === "increment" && item.isPrivate && item.upgradeQty == 1) {
            return
        }
        let listOfAddOns = JSON.parse(JSON.stringify(this.state.listOfAddOns));
        let index = listOfAddOns.findIndex(temp => temp.id === item.id)
        listOfAddOns[index].upgradeQty = (operation === "increment" ? listOfAddOns[index].upgradeQty + 1 : listOfAddOns[index].upgradeQty - 1);
        this.setState({
            listOfAddOns
        })
    }

    onChangeHandler = id => event => {
        let value = parseInt(event.target.value)
        if (value < 0) {
            return
        }
        let listOfAddOns = JSON.parse(JSON.stringify(this.state.listOfAddOns));
        let index = listOfAddOns.findIndex(temp => temp.id === id)
        listOfAddOns[index].upgradeQty = value >= listOfAddOns[index].qty ? value : listOfAddOns[index].upgradeQty;
        this.setState({
            listOfAddOns
        })
    }

    tabChangeHandler = selectedTab => {
        this.setState({
            selectedTab
        })
    }

    getStringWithAnd = ({ products }) => {
        if (products.length === 1) {
            return products[0].name
        } else if (products.length > 1) {
            let str = ""
            products.map((product, index) => {
                if (index === products.length - 1) {
                    str += " and " + product.name
                } else {
                    str += index == 0 ? product.name : ", " + product.name
                }
            })
            return str
        }
    }

    proceedToPayment = () => {
        let { listOfUpgradedAddOns, totalPayableAmountThisBillingCycle } = this.getListOfUpgradeAddOns()
        let payload = {
            "addOnsList": listOfUpgradedAddOns.map(({ id, upgradeQty }) => ({ id, qty: upgradeQty }))
        };
        this.setState({
            addOnPurchaseInProgress: true,
            totalPayableAmountThisBillingCycle,
        }, () => this.props.paymentOfTenantAddOns(payload));
    }

    getListOfUpgradeAddOns = () => {
        let totalPurchasedPackage = 0;
        let totalPayableAmountBillingCycle = this.state.tenantDashboardDetail ? this.state.tenantDashboardDetail.subscriptionPrice : 0;
        const licenseExpiry = this.state.tenantDashboardDetail ? this.state.tenantDashboardDetail.licenseExpiry : 0;
        let totalPayableAmountThisBillingCycle = 0
        const currentDate = new Date();

        const diffTime = Math.abs(licenseExpiry - currentDate);
        const remainingDaysInThisCycle = parseInt(diffTime / (1000 * 60 * 60 * 24));

        let listOfUpgradedAddOns = [...this.state.listOfAddOns].filter(item => {
            if (item.upgradeQty) {
                totalPayableAmountBillingCycle += item.amount > 0 ? (item.upgradeQty * item.amount) : 0;
                totalPurchasedPackage += item.upgradeQty;
                totalPayableAmountThisBillingCycle += item.amount > 0 ? parseFloat(((item.amount / 30) * remainingDaysInThisCycle).toFixed(2)) * item.upgradeQty : 0;
                return item
            }
        })

        return {
            totalPurchasedPackage,
            listOfUpgradedAddOns,
            totalPayableAmountBillingCycle,
            totalPayableAmountThisBillingCycle,
            remainingDaysInThisCycle
        }
    }

    getPriceForAddons = () => {
        let _total = 0
        this.state.listOfAddOns.filter(item => item.qty).forEach(i => {
            _total += (i.qty * i.amount)
        })
        return _total;
    }

    confirmModalHandler = (isCancelSubscription) => {
        this.setState({ isCancelSubscription }, () => { $('#confirmModal').modal('show') });
    };

    initiateSubscriptionHandler = () => {
        $('#subscriptionModal').modal('show');
    };

    getAddOns = (category) => {
        return Object.entries(category).sort().map(([key, value]) =>
            <div className="addon-list-box" key={key}>
                <div className="addon-list-header">
                    <h6>{key}</h6>
                </div>
                <ul className="d-flex list-style-none addon-list">
                    {value.map(category =>
                    (!(category.isPrivate && category.qty > 0) &&
                        <li className="flex-50" key={category.id}>
                            <div className={`addon-box ${category.upgradeQty > 0 ? "active" : 0}`}>
                                {category.isPublished ? (this.state.tenantDashboardDetail.isSubscriptionCancelled || (this.state.tenantDashboardDetail.isPostPaymentEnabled && category.amount > 0) || (this.state.tenantDashboardDetail.isPostPaymentEnabled === false && this.state.tenantDashboardDetail.paymentPending === true && category.amount > 0)) ?
                                    <div className="addon-box-overlay"></div> :
                                    null :
                                    <div className="addon-box-overlay">
                                        <a href="mailto:support@flex83.com">
                                            <img src={require('../../assets/images/other/contactUs.png')} />
                                        </a>
                                    </div>
                                }
                                <div className="addon-box-header">
                                    <div className="addon-image">
                                        {category.isPrivate ?
                                            <i className="fad fa-lock-alt text-red" data-tooltip data-tooltip-text="Private Package" data-tooltip-place="bottom"></i> :
                                            <i className="far fa-globe text-primary" data-tooltip data-tooltip-text="Public Package" data-tooltip-place="bottom"></i>
                                        }
                                    </div>
                                    {category.oneTimePurchase &&
                                        <div className="addon-purchase-image">
                                            <img src={require('../../assets/images/purchaseIcon.png')} />
                                            <p>One <br /> time <br /> purchase</p>
                                        </div>
                                    }
                                    <h6 className="text-dark-theme fw-600 f-13">{category.name}</h6>
                                    <p className="text-content m-0 f-10">{category.description}</p>
                                    {Boolean(category.qty) && <span className="badge badge-success addon-badge">Purchased ({category.qty})</span>}
                                </div>
                                <div className="addon-body">
                                    {category.products.map(product => (
                                        product.name !== "Databases" && <span className="badge badge-pill badge-primary" key={product.id}><strong>{category.upgradeQty > 1 ? (product.count * category.upgradeQty) : product.count}</strong>{product.name}</span>
                                    ))}
                                </div>
                                <div className="addon-footer">
                                    {category.oneTimePurchase ?
                                        <div>
                                            <button className="btn btn-transparent-green pt-1 pb-1 mr-2" disabled={category.upgradeQty > 0} onClick={() => this.onIncrementOrDecrementChange(category, "increment")}>Buy</button>
                                            <button className="btn btn-transparent-red pt-1 pb-1" disabled={category.upgradeQty < 1} onClick={() => this.onIncrementOrDecrementChange(category, "decrement")}>Cancel</button>
                                        </div> :
                                        <div className="form-group input-group mb-0">
                                            <div className="input-group-prepend" onClick={() => this.onIncrementOrDecrementChange(category, "decrement")}>
                                                <button className="btn btn-light"><i className="far fa-minus"></i></button>
                                            </div>
                                            <input type="number" className="form-control" disabled={!category.isPublished || this.state.tenantDashboardDetail.isSubscriptionCancelled} value={category.upgradeQty} onChange={this.onChangeHandler(category.id)} />
                                            <div className="input-group-append" onClick={() => this.onIncrementOrDecrementChange(category, "increment")}>
                                                <button className="btn btn-light ml-0"><i className="far fa-plus"></i></button>
                                            </div>
                                        </div>
                                    }
                                    <h5>${category.upgradeQty > 1 ? (category.amount * category.upgradeQty) : category.amount}
                                        <small className="text-content f-10 ml-1">USD / mo</small>
                                    </h5>
                                </div>
                            </div>
                        </li>
                    ))
                    }
                </ul>
            </div>
        )
    }

    cancelOrResumeSubscription = () => {
        if (this.state.isCancelSubscription) {
            this.setState({
                isFetching: 1,
            }, () => { this.props.cancelSubscription("userChoice") })
        } else {
            this.setState({
                resumeSubscriptionInProgress: true,
            }, () => { this.props.renewSubscription() })
        }
    }

    purchaseSubscription = () => {
        this.setState({
            purchaseSubscriptionInProgress: true,
        }, () => { this.props.purchaseSubscription() })

    }

    getDateDifference = (licenseExpiry) => {
        let differenceInTime = licenseExpiry - (new Date).getTime(),
            differenceInDays = Math.floor(differenceInTime / (1000 * 3600 * 24));
        if (differenceInDays > 0) {
            return `[${differenceInDays} ${differenceInDays > 1 ? 'days' : 'day'} remaining]`
        }
        else {
            return ''
        }
    }

    getCurrentBilling = (licenseExpiry) => {
        let billingDate = licenseExpiry - (30 * 24 * 60 * 60 * 1000);
        return `${new Date(billingDate).toLocaleDateString("en-US")} - ${new Date(licenseExpiry).toLocaleDateString("en-US")}`;
    }

    getColumns = () => {
        return [
            {
                Header: "Transaction Type", width: 50, cell: row => (
                    <React.Fragment>
                        <div className="list-view-icon">
                            <img src={require('../../assets/images/other/paypal-logo.png')} />
                        </div>
                        <div className="list-view-icon-box">
                            <h6 className="text-theme fw-600">{row.type}</h6>
                            <p>Transaction Id : <strong className="text-gray">{row.txnId ? row.txnId : 'N/A'}</strong></p>
                        </div>
                    </React.Fragment>
                )
            },
            {
                Header: "Amount", width: 20, cell: row => (
                    <React.Fragment>
                        <h6 className="text-dark-theme fw-700">$ {(row.amount).toFixed(2)}</h6>
                        <p>{getTimeDifference(row.txnDate)}</p>
                    </React.Fragment>
                )
            },
            {
                Header: "Status", width: 15, cell: row => (
                    <div className="">
                        {row.status === 'SUCCESS' || row.status === 'PAID' ?
                            <span className="badge badge-pill alert-success list-view-pill"><i className="fad fa-check-circle mr-r-5"></i>{row.status}</span> :
                            row.status === 'CANCELLED' ?
                                <span className="badge badge-pill alert-danger list-view-pill"><i className="fad fa-times-circle mr-r-5"></i>{row.status}</span> :
                                <span className="badge badge-pill alert-warning list-view-pill"><i className="fad fa-exclamation-triangle mr-r-5"></i>{row.status}</span>
                        }
                    </div>
                )
            },
            {
                Header: "Invoice", width: 10, cell: row => (
                    <div className="button-group">
                        <button className="btn-transparent btn-transparent-blue" onClick={() => { window.open(row.invoiceUrl, '_blank') }} disabled={!row.invoiceUrl}>
                            <i className="far fa-file-invoice-dollar"></i>
                        </button>
                    </div>
                )
            },
        ]
    }

    refreshTabHandler = () => {
        if (this.state.selectedTab === "billing") {
            this.setState({
                reloadBillingInfo: true
            }, () => {
                this.props.getTenantTransactionDetails();
            })
        } else if (this.state.selectedTab === "addons") {
            this.setState({
                reloadAddOnsInfo: true
            }, () => {
                this.props.getAddOns();
            })
        } else if (this.state.selectedTab === "quotaInfo") {
            this.setState({
                reloadQuotaInfo: true
            }, () => {
                this.props.getQuotaInformation(this.getQuotaPayload());
            })
        } else {
            this.setState({
                reloadSummaryInfo: true
            }, () => {
                this.props.getTenantDashboardDetails();
            });
        }

    }

    getMiniLoader = () => {
        return (
            <div className="inner-loader-wrapper h-100">
                <div className="inner-loader-content">
                    <i className="fad fa-sync-alt fa-spin"></i>
                </div>
            </div>
        );
    }

    uploadProfilePicture = (file) => {
        this.setState({
            isUploadingImage: true,
        }, () => this.props.uploadProfilePicture({ file, fileType: "DEVICE_TYPE" }))
    }

    userDetailsChangeHandler = ({ currentTarget }) => {
        let key = currentTarget.id;
        let value = currentTarget.value;
        let updatedUserDetails = cloneDeep(this.state.updatedUserDetails);
        updatedUserDetails[key] = value
        this.setState({
            updatedUserDetails,
        })
    }

    openEditUserDetailsModal = () => {
        let updatedUserDetails = {
            firstName: "",
            lastName: "",
            email: "",
            mobile: "",
            id: localStorage.getItem("userId")
        }
        this.setState({
            editUserDetailsModal: true,
            updatedUserDetails
        })
    }

    openEditProfilePictureModal = () => {
        this.setState({
            editProfilePictureModal: true,
        })
    }

    updateUserDetails = () => {
        this.setState({
            isUpdatingUserDetails: true,
        }, () => this.props.updateUserDetails(this.state.updatedUserDetails))
    }

    closeEditUserDetailsModal = () => {
        this.setState({
            editUserDetailsModal: false,
            editProfilePictureModal: false,
        })
    }

    render() {
        let userDetails = this.props.getUserDetailsSuccess || {}
        const tenantURL = window.location.origin === "http://localhost:3001" ? window.API_URL.substring(0, window.API_URL.length - 1) : window.location.origin;
        if (this.state.selectedTab === "addons") {
            var { totalPurchasedPackage, listOfUpgradedAddOns, totalPayableAmountBillingCycle, totalPayableAmountThisBillingCycle, remainingDaysInThisCycle } = this.getListOfUpgradeAddOns()
        }
        let isPublishedCategory = {}
        let isUnpublishedCategory = {}
        this.state.listOfAddOns.map(i => {
            if (i.isPublished) {
                if (isPublishedCategory[i.category])
                    isPublishedCategory[i.category].push(i)
                else
                    isPublishedCategory[i.category] = [i]
            } else {
                if (isUnpublishedCategory[i.category])
                    isUnpublishedCategory[i.category].push(i)
                else
                    isUnpublishedCategory[i.category] = [i]
            }
        });

        return (
            <React.Fragment>
                <Helmet>
                    <title>My Plan Details</title>
                    <meta name="description" content="M83-MyAccount" />
                </Helmet>

                <header className="content-header d-flex">
                    <div className="flex-90">
                        <h6>My Plan Details</h6>
                    </div>
                    {!this.state.isFetching &&
                        <div className="flex-10 text-right">
                            <div className="content-header-group">
                                <button className="btn btn-light" data-tooltip="true" data-tooltip-text="Refresh"
                                    data-tooltip-place="bottom" onClick={this.refreshTabHandler}><i className="far fa-sync-alt"></i>
                                </button>
                            </div>
                        </div>
                    }
                </header>

                <div className="content-body" id="flexProfile">
                    {this.state.isFetching ?
                        <Loader />
                        :
                        <div className="d-flex">
                            <div className="flex-35 pd-r-7">
                                <div className="card">
                                    <div className="profile-image-box">
                                        <div className="profile-image-content">
                                            <h6>Welcome,</h6>
                                            <h4>{this.state.profileDetails.name}</h4>
                                           {/*<button className="btn-transparent btn-transparent-green" onClick={this.openEditUserDetailsModal}><i className="fas fa-pencil"></i></button>*/}
                                        </div>
                                        <div className="profile-image-bg">
                                            <img src="https://content.iot83.com/m83/misc/icons/userProfileBg.jpg" />
                                        </div>
                                        <div className="profile-image">
                                            <div className="profile-image-overlay">
                                                <button className="btn btn-link text-white p-0" onClick={this.openEditProfilePictureModal}><i className="fas fa-pencil f-16 m-0"></i></button>
                                            </div>
                                            {this.props.isLoadingUserDetails ? this.getMiniLoader() : <img src={userDetails.imageUrl || "https://content.iot83.com/m83/misc/icons/userProfile.png"} />}
                                        </div>
                                    </div>
                                    <div className="profile-body">
                                        <p><i className="fad fa-user-crown"></i>{tenantURL}</p>
                                        <p><i className="fad fa-building"></i>{this.state.profileDetails.companyName}</p>
                                        <p><i className="fad fa-envelope"></i>{this.state.profileDetails.email}</p>
                                        {this.state.profileDetails.mobile && <div className="list-phone-input"><i className="fad fa-phone-alt f-12 text-green mr-r-10"></i>
                                            <PhoneInput
                                                inputProps={{
                                                    name: 'phone',
                                                }}
                                                disabled={true}
                                                disableDropdown={true}
                                                value={this.state.profileDetails.phone}
                                                onChange={(value) => null}
                                            />
                                        </div>}
                                    </div>
                                </div>

                                <div className="card current-plan-wrapper mb-0">
                                    <div className="current-plan-header">
                                        <h6>Current Monthly Subscription</h6>
                                        <h5>
                                            {this.state.tenantDashboardDetail.isSubscriptionCancelled === false && this.state.tenantDashboardDetail.isPostPaymentEnabled === false && <span>{`$${this.state.tenantDashboardDetail.subscriptionPrice}`}<small className="f-10 ml-1">USD</small></span>}
                                        </h5>
                                    </div>
                                    <div className="current-plan-body">
                                        {this.state.tenantDashboardDetail.features.map((feature, index) => (
                                            <div className="content-table" key={index}>
                                                <table className="table table-bordered m-0">
                                                    <thead>
                                                        <tr>
                                                            <th width="55%">{feature.categoryName}</th>
                                                            <th width="15%">Base</th>
                                                            <th width="15%">Buy</th>
                                                            <th width="15%">Total</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        {feature.products.map((product, subIndex) => (
                                                            <tr key={subIndex}>
                                                                <td>
                                                                    <i className="fas fa-check mr-3 text-green"></i>{product.displayName}
                                                                    {product.showDescription &&
                                                                        <span className="ml-2" data-tooltip data-tooltip-text={product.description} data-tooltip-place="right">
                                                                            <i className="fad fa-info-circle text-theme"></i>
                                                                        </span>
                                                                    }
                                                                </td>
                                                                <td><strong className="text-theme fw-600">{product.baseCount}</strong></td>
                                                                <td><strong className="text-theme fw-600">{product.addOnCount}</strong></td>
                                                                <td>{product.name === "retention" ? <strong className="text-theme fw-600">-</strong> : <strong className="text-theme fw-600">{product.baseCount + product.addOnCount}</strong>}</td>
                                                            </tr>
                                                        ))}
                                                    </tbody>
                                                </table>
                                            </div>
                                        ))}

                                    </div>
                                </div>
                            </div>

                            <div className="flex-65 pd-l-7">
                                <div className="action-tabs mr-b-15">
                                    <ul className="nav nav-tabs">
                                        <li className={this.state.selectedTab === "subscription" ? "nav-item active" : "nav-item"} onClick={() => this.tabChangeHandler("subscription")}>
                                            <a className="nav-link">Summary</a>
                                        </li>
                                        <li className={this.state.selectedTab === "addons" ? "nav-item active" : "nav-item"} onClick={() => this.tabChangeHandler("addons")}>
                                            <a className="nav-link">Add-Ons</a>
                                        </li>
                                        <li className={this.state.selectedTab === "billing" ? "nav-item active" : "nav-item"} onClick={() => this.tabChangeHandler("billing")}>
                                            <a className="nav-link">Billing</a>
                                        </li>
                                        <li className={this.state.selectedTab === "quotaInfo" ? "nav-item active" : "nav-item"} onClick={() => this.tabChangeHandler("quotaInfo")}>
                                            <a className="nav-link">Usage</a>
                                        </li>
                                    </ul>
                                </div>

                                <div className="tab-content">
                                    {this.state.selectedTab === "addons" ?
                                        <div className="tab-pane fade show active position-relative" id="addons">
                                            {this.state.reloadAddOnsInfo ?
                                                <div className="addon-list-wrapper">
                                                    {this.getMiniLoader()}
                                                </div> :
                                                <React.Fragment>
                                                    <div className="addon-list-wrapper" style={{ paddingBottom: Boolean(totalPurchasedPackage) && 160 }}>
                                                        {this.state.tenantDashboardDetail.isPostPaymentEnabled === true ?
                                                            <div className="alert alert-danger note-text">
                                                                <p>Please <strong>“Buy”</strong> the subscription before proceeding with <strong>"Add On"</strong> purchase.</p>
                                                            </div> :
                                                            this.state.tenantDashboardDetail.paymentPending ?
                                                                <div className="alert alert-danger note-text">
                                                                    <p>You can purchase <strong>"Add Ons"</strong>, once payment is processed.</p>
                                                                </div> :
                                                                null
                                                        }
                                                        {this.getAddOns(isPublishedCategory)}
                                                        {this.getAddOns(isUnpublishedCategory)}
                                                    </div>

                                                    {Boolean(totalPurchasedPackage) &&
                                                        <div className="addon-price-box animated slideInUp">
                                                            <p>{totalPurchasedPackage} Add-On(s) Selected</p>
                                                            {listOfUpgradedAddOns.map(item => (
                                                                <h6 key={item.id}><strong>{item.upgradeQty}</strong>{item.name}</h6>
                                                            ))}
                                                            <div className="addon-price-detail">
                                                                <h5><span>Amount for current billing cycle (pro-rated)</span><strong>${totalPayableAmountThisBillingCycle.toFixed(2)} <small>USD <span className="text-orange">( For remaining {remainingDaysInThisCycle} days )</span></small></strong></h5>
                                                                <h5><span>Amount from next billing cycle</span>{this.state.tenantDashboardDetail.isPostPaymentEnabled === true ? <span className="text-orange">N/A</span> : <strong>${totalPayableAmountBillingCycle} <small>USD / mo</small> <small><span className="text-orange">( Includes {this.state.tenantDashboardDetail.name} price of ${this.state.tenantDashboardDetail.subscriptionPrice - this.getPriceForAddons()} USD / mo )</span></small></strong>}</h5>
                                                                <button type="button" className="btn btn-primary" onClick={() => this.proceedToPayment()} disabled={jwt_decode(localStorage["token"]).defaultAccountAdmin === false}>{totalPayableAmountThisBillingCycle > 0 ? `Pay ${totalPayableAmountThisBillingCycle.toFixed(2)} USD` : 'Proceed'}<i className="far fa-angle-double-right ml-2"></i></button>
                                                            </div>
                                                        </div>
                                                    }
                                                </React.Fragment>
                                            }
                                        </div>
                                        :
                                        this.state.selectedTab === "billing" ?
                                            <div className="tab-pane fade show active" id="billing">
                                                {this.state.reloadBillingInfo ?
                                                    <div className="addon-list-wrapper">
                                                        {this.getMiniLoader()}
                                                    </div> :
                                                    <div className="addon-list-wrapper">
                                                        <ListingTable
                                                            columns={this.getColumns()}
                                                            data={this.state.tenantTransactionDetail}
                                                            className="list-style-none payment-list"
                                                        />
                                                    </div>
                                                }
                                            </div>
                                            :
                                            this.state.selectedTab === "quotaInfo" ?
                                                <div className="tab-pane fade show active" id="quotaInfo">
                                                    {this.state.reloadQuotaInfo ?
                                                        <div className="addon-list-wrapper">
                                                            {this.getMiniLoader()}
                                                        </div> :
                                                        <ul className="list-style-none d-flex profile-analysis">
                                                            {this.state.quotaDetails.map((quota, index) =>
                                                                <li className="flex-33" key={index}>
                                                                    <div className="card">
                                                                        <div className="card-body">
                                                                            {/*<div className="card-icon">
                                                                                <img src={require('../../assets/images/other/iamPackage.png')} />
                                                                            </div>*/}
                                                                            <h5>{quota.used} <small>of</small> {quota.total} <small>used</small></h5>
                                                                            <p>{quota.displayName}</p>
                                                                        </div>
                                                                        <div className="card-footer">
                                                                            <div className="profile-progress">
                                                                                <div className="progress">
                                                                                    <div className={`progress-bar ${quota.usedPercentage > 80 ? "bg-red" : quota.usedPercentage > 60 ? "bg-yellow" : "bg-success"}`} role="progressbar" style={{ width: `${quota.usedPercentage}%` }} aria-valuenow={quota.usedPercentage} aria-valuemin="0" aria-valuemax="100" data-tooltip data-tooltip-text={`${quota.usedPercentage} %`} data-tooltip-place="bottom"></div>
                                                                                </div>
                                                                                <div className="progress-count">
                                                                                    {quota.usedPercentage > 80 ?
                                                                                        <button type="button" className="btn btn-link" onClick={() => { this.setState({ selectedTab: 'addons' }) }}>Buy Now</button> :
                                                                                        <p>({quota.usedPercentage}%)</p>
                                                                                    }
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            )}
                                                        </ul>
                                                    }
                                                </div>

                                                :
                                                <div className="tab-pane fade show active" id="subscription">
                                                    {this.state.reloadSummaryInfo ?
                                                        <div className="summary-wrapper">
                                                            {this.getMiniLoader()}
                                                        </div> :
                                                        <div className="summary-wrapper">
                                                            {new Date().getTime() - this.state.tenantDashboardDetail.licenseExpiry > 0 &&
                                                                <div className="alert alert-danger note-text">
                                                                    <p>
                                                                        {new Date().getTime() - this.state.tenantDashboardDetail.licenseExpiry < 172800000 ?
                                                                            `Your account will be suspended and access will be revoked after ${new Date(this.state.tenantDashboardDetail.licenseExpiry + 172800000).toLocaleString("en-US", { timeZone: localStorage.timeZone })}, Please pay to avoid this.` :
                                                                            'Your account is suspended, Please pay now.'
                                                                        }
                                                                    </p>
                                                                </div>
                                                            }
                                                            <div className="summary-header">
                                                                {this.state.tenantDashboardDetail.isPostPaymentEnabled ?
                                                                    <h6>
                                                                        Purchase Subscription
                                                                    <button
                                                                            className="btn btn-success"
                                                                            disabled={jwt_decode(localStorage["token"]).defaultAccountAdmin === false}
                                                                            onClick={() => this.initiateSubscriptionHandler()}>
                                                                            Buy Now
                                                                    </button>
                                                                    </h6> : <h6>Subscription Summary
                                                                    <small className="ml-1 text-content">(You can cancel your subscription any time but not on the billing date)</small>
                                                                        {this.state.tenantDashboardDetail.isSubscriptionCancelled ?
                                                                            <button
                                                                                className="btn btn-primary"
                                                                                disabled={jwt_decode(localStorage["token"]).defaultAccountAdmin === false}
                                                                                onClick={() => {
                                                                                    this.confirmModalHandler(false)
                                                                                }}>
                                                                                Resume Subscription
                                                                        </button> :
                                                                            <button
                                                                                className="btn btn-danger"
                                                                                disabled={jwt_decode(localStorage["token"]).defaultAccountAdmin === false || !this.state.tenantDashboardDetail.isCancelEnabled}
                                                                                onClick={() => {
                                                                                    this.confirmModalHandler(true)
                                                                                }}>
                                                                                Cancel Subscription
                                                                        </button>
                                                                        }
                                                                    </h6>
                                                                }
                                                            </div>
                                                            <div className="summary-box">
                                                                <h6>
                                                                    <label>Base Package <i className="far fa-angle-double-right"></i></label>
                                                                    <span className="text-theme fw-600">{this.state.tenantDashboardDetail.name}</span>
                                                                    {!this.state.tenantDashboardDetail.isSubscriptionCancelled &&
                                                                        <strong>
                                                                            {`$ ${this.state.tenantDashboardDetail.isPostPaymentEnabled ? 0 : this.state.tenantDashboardDetail.subscriptionPrice - this.getPriceForAddons()} `}<small>USD / mo</small>
                                                                        </strong>
                                                                    }
                                                                </h6>
                                                                <h6>
                                                                    <label>Add-on Package(s) <i className="far fa-angle-double-right"></i></label>
                                                                    {this.state.listOfAddOns.some(item => item.qty > 0) ? this.state.listOfAddOns.filter(item => item.qty).map(item => (
                                                                        <span className="badge badge-pill badge-success" key={item.id}>{item.name}<b>{item.qty}</b></span>)) : "N/A"
                                                                    }
                                                                    {!this.state.tenantDashboardDetail.isSubscriptionCancelled &&
                                                                        <strong>{`$${this.getPriceForAddons()} `}<small>USD / mo</small></strong>}
                                                                </h6>
                                                                {/*Next Billing Date Information */}
                                                                <h6>
                                                                    {this.state.tenantDashboardDetail.isPostPaymentEnabled ?
                                                                        <React.Fragment>
                                                                            <label>Next Billing Date
                                                                            <i className="far fa-angle-double-right"></i>
                                                                            </label>
                                                                            <span className="text-orange">Will be updated after payment of subscription.</span>
                                                                        </React.Fragment> :
                                                                        (this.state.tenantDashboardDetail.isTrial === false && this.state.tenantDashboardDetail.paymentPending === true) ?
                                                                            <React.Fragment>
                                                                                <label>Next Billing Date
                                                                                <i className="far fa-angle-double-right"></i>
                                                                                </label>
                                                                                <span className="text-orange">Will be updated when payment is processed</span>
                                                                            </React.Fragment> :
                                                                            <React.Fragment>
                                                                                <label>{this.state.tenantDashboardDetail.isTrial ? 'Expiring On' : this.state.tenantDashboardDetail.isSubscriptionCancelled ? this.state.tenantDashboardDetail.licenseExpiry > new Date().getTime() ? 'Expiring On' : 'Expired On' : 'Next Billing Date'}
                                                                                    <i className="far fa-angle-double-right"></i>
                                                                                </label>
                                                                                <span
                                                                                    className="text-orange">{this.state.tenantDashboardDetail.isTrial === false && this.state.tenantDashboardDetail.isSubscriptionCancelled === false ? `approximately ${new Date(this.state.tenantDashboardDetail.licenseExpiry).toLocaleDateString("en-US")}` : new Date(this.state.tenantDashboardDetail.licenseExpiry).toLocaleDateString("en-US")}</span>
                                                                            </React.Fragment>
                                                                    }
                                                                    {this.state.tenantDashboardDetail.isSubscriptionCancelled === false &&
                                                                        <strong>
                                                                            <small className="mr-2">Total Amount: </small>{`$${this.state.tenantDashboardDetail.isPostPaymentEnabled ? this.getPriceForAddons() : this.state.tenantDashboardDetail.subscriptionPrice} `}<small>USD / mo</small>
                                                                        </strong>
                                                                    }
                                                                </h6>
                                                                {/*Current Billing Cycle Information */}
                                                                {(this.state.tenantDashboardDetail.isTrial === false && this.state.tenantDashboardDetail.paymentPending === false && this.state.tenantDashboardDetail.isSubscriptionCancelled === false) &&
                                                                    <h6>
                                                                        <label>Current Billing Cycle
                                                                            <i className="far fa-angle-double-right"></i>
                                                                        </label>
                                                                        <span className="text-green">{new Date(this.state.tenantDashboardDetail.lastBillingDate).toLocaleDateString("en-US")} - {new Date(this.state.tenantDashboardDetail.licenseExpiry).toLocaleDateString("en-US")}</span>
                                                                    </h6>
                                                                }
                                                                {/*Trial Information */}
                                                                {(this.state.tenantDashboardDetail.isPostPaymentEnabled || this.state.tenantDashboardDetail.isTrial) &&
                                                                    <h6>
                                                                        <label>Trial Active<i className="far fa-angle-double-right"></i></label>
                                                                        {(this.state.tenantDashboardDetail.licenseExpiry < new Date().getTime()) ? <span className="text-red">No (Expired)</span> : <span className="text-green">Yes (Ongoing) {this.getDateDifference(this.state.tenantDashboardDetail.licenseExpiry)}</span>}
                                                                    </h6>
                                                                }
                                                                {/*Subscription Status Active Info */}
                                                                {!this.state.tenantDashboardDetail.isPostPaymentEnabled &&
                                                                    <h6>
                                                                        <label>Subscription Status <i className="far fa-angle-double-right"></i></label>
                                                                        { this.state.tenantDashboardDetail.isSubscriptionCancelled ?
                                                                            <span className="text-red">Cancelled</span>:
                                                                            (this.state.tenantDashboardDetail.isTrial === false && this.state.tenantDashboardDetail.paymentPending) ?
                                                                                <span className="text-orange">Payment Pending</span> :
                                                                                <span className="text-green">Active</span>
                                                                        }
                                                                    </h6>
                                                                }
                                                            </div>

                                                            <div className="summary-header">
                                                                <h6>Base Package Details</h6>
                                                            </div>
                                                            <ul className="d-flex list-style-none addon-list">
                                                                <li className="flex-100 p-0">
                                                                    <div className="addon-box">
                                                                        <div className="addon-box-header">
                                                                            <div className="addon-image">
                                                                                <img src="https://content.iot83.com/m83/misc/icons/flexPackageImage.png" />
                                                                            </div>
                                                                            <h6 className="text-dark-theme fw-600 f-13">{this.state.tenantDashboardDetail.name}</h6>
                                                                            <p className="text-content m-0 f-11">Package Info</p>
                                                                        </div>
                                                                        <div className="addon-body">
                                                                            {this.state.baseLicenseDetails.map((feature, index) => (
                                                                                feature.isBasePackageFeature &&
                                                                                <h6 key={index}><strong
                                                                                    className="text-theme mr-2">{feature.count}</strong>{feature.name}
                                                                                </h6>
                                                                            ))}
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            </ul>

                                                            {this.state.listOfAddOns.filter(item => item.qty).length ?
                                                                <React.Fragment>
                                                                    <div className="summary-header">
                                                                        <h6>Add-on Package(s) Details</h6>
                                                                    </div>
                                                                    <ul className="d-flex list-style-none addon-list">
                                                                        {this.state.listOfAddOns.filter(item => item.qty).map(item => (
                                                                            <li className="flex-50" key={item.id}>
                                                                                <div className="addon-box">
                                                                                    <div className="addon-box-header">
                                                                                        <div className="addon-image">
                                                                                            {/*<img src={item.imageUrl ? item.imageUrl : require('../../assets/images/other/iamPackage.png')} />*/}
                                                                                            {item.isPrivate ?
                                                                                                <i className="fad fa-lock-alt text-red" data-tooltip data-tooltip-text="Private Package" data-tooltip-place="bottom"></i> :
                                                                                                <i className="far fa-globe text-primary" data-tooltip data-tooltip-text="Public Package" data-tooltip-place="bottom"></i>
                                                                                            }
                                                                                        </div>
                                                                                        <h6 className="text-dark-theme fw-600 f-13">{item.name}</h6>
                                                                                        <p className="text-content m-0 f-11">{item.description}</p>
                                                                                        <span className="badge badge-success addon-badge">{item.qty}</span>
                                                                                    </div>
                                                                                    <div className="addon-body">
                                                                                        {item.products.map(m =>
                                                                                            m.name !== "Databases" &&
                                                                                            <span className="badge badge-pill badge-primary" key={m.id}><strong>{m.count * item.qty}</strong>{m.name}</span>
                                                                                        )}
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                        ))}
                                                                    </ul>
                                                                </React.Fragment>
                                                                :
                                                                <div className="addon-list-empty">
                                                                    <div>
                                                                        <img src="https://content.iot83.com/m83/account/bill.png" />
                                                                        <p className="text-content mb-0">You haven't purchased any add-on(s) yet.</p>
                                                                        <button className="btn btn-link" onClick={() => this.tabChangeHandler("addons")}
                                                                            disabled={this.state.tenantDashboardDetail.isSubscriptionCancelled}
                                                                        >Purchase Add-ons<i className="far fa-angle-double-right ml-2"></i>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            }
                                                        </div>
                                                    }
                                                </div>
                                    }
                                </div>
                            </div>
                        </div>
                    }
                </div>

                {/* edit info modal */}
                {this.state.editUserDetailsModal &&
                    <div className="modal d-block animated slideInDown" id="editInfo" role="dialog">
                        <div className="modal-dialog modal-dialog-centered">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h6 className="modal-title">Edit Profile Info
                                    <button type="button" className="close" data-tooltip data-tooltip-text="Close" data-tooltip-place="bottom" onClick={this.closeEditUserDetailsModal}>
                                            <i className="far fa-times"></i>
                                        </button>
                                    </h6>
                                </div>
                                {this.state.isUpdatingUserDetails ? this.getMiniLoader() : <form onSubmit={this.updateUserDetails}>
                                    <div className="modal-body">
                                        <div className="form-group">
                                            <label className="form-group-label">First Name : <i className="fas fa-asterisk form-group-required"></i></label>
                                            <input type="text" id="firstName" required value={this.state.updatedUserDetails.firstName} onChange={this.userDetailsChangeHandler} className="form-control" placeholder="First Name" />
                                        </div>
                                        <div className="form-group">
                                            <label className="form-group-label">Last Name : <i className="fas fa-asterisk form-group-required"></i></label>
                                            <input type="text" id="lastName" required value={this.state.updatedUserDetails.lastName} onChange={this.userDetailsChangeHandler} className="form-control" placeholder="Last Name" />
                                        </div>
                                        <div className="form-group">
                                            <label className="form-group-label">Email : <i className="fas fa-asterisk form-group-required"></i></label>
                                            <input type="text" name="email" required value={this.state.updatedUserDetails.email} onChange={this.userDetailsChangeHandler} className="form-control" placeholder="Email" readOnly />
                                        </div>
                                        <div className="form-group">
                                            <label className="form-group-label">Mobile No : </label>
                                            <input type="text" name="mobile" value={this.state.updatedUserDetails.mobile} onChange={this.userDetailsChangeHandler} className="form-control" placeholder="Mobile" maxLength="10" />
                                        </div>
                                    </div>
                                    <div className="modal-footer">
                                        <button type="button" className="btn btn-dark" onClick={this.closeEditUserDetailsModal}>Cancel</button>
                                        <button className="btn btn-success" data-dismiss="modal">Update</button>
                                    </div>
                                </form>}
                            </div>
                        </div>
                    </div>}
                {/* end edit info modal */}

                {this.state.editProfilePictureModal && <div className="modal d-block animated slideInDown" id="editDP" role="dialog">
                    <div className="modal-dialog modal-dialog-centered">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h6 className="modal-title">View/Edit Profile Picture
                            <button type="button" className="close" data-tooltip data-tooltip-text="Close" data-tooltip-place="bottom" onClick={this.closeEditUserDetailsModal}>
                                        <i className="far fa-times"></i>
                                    </button>
                                </h6>
                            </div>
                            <div className="modal-body">
                                <div className="form-group">
                                    <div className="file-upload-box">
                                        <ImageUploader
                                            loader={this.state.isUploadingImage}
                                            imagePath={userDetails.imageUrl || ""}
                                            uploadHandler={this.uploadProfilePicture}
                                            classNames={{ blankUploader: "file-upload-form" }}
                                            acceptFileType="image/x-png,image/gif,image/jpeg"
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>}
                {/* subscription modal */}
                <div className="modal fade animated slideInDown" id="confirmModal">
                    <div className="modal-dialog modal-dialog-centered">
                        <div className="modal-content">
                            <div className="modal-body">
                                <div className="delete-content">
                                    <div className="delete-icon">
                                        <i className={`fad fa-money-bill-wave ${this.state.isCancelSubscription ? "text-red" : "text-green"}`}></i>
                                    </div>
                                    <h4 className={this.state.isCancelSubscription ? "text-red" : "text-green"}>{this.state.isCancelSubscription ? 'Cancel Subscription' : 'Resume Subscription'}</h4>
                                    <h6>{this.state.isCancelSubscription ? 'Are you sure you want to cancel your subscription ?' : 'Are you sure you want to resume your subscription ?'}</h6>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-dark" data-dismiss="modal" >No</button>
                                <button type="button" className="btn btn-success" data-dismiss="modal" onClick={this.cancelOrResumeSubscription}>Yes</button>
                            </div>
                        </div>
                    </div>
                </div>
                {/* end subscription modal */}

                {/* continue subscription modal */}
                <div className="modal fade animated slideInDown" id="subscriptionModal">
                    <div className="modal-dialog modal-dialog-centered">
                        <div className="modal-content">
                            <div className="modal-body">
                                <div className="delete-content">
                                    <div className="delete-icon">
                                        <i className="fad fa-credit-card text-cyan"></i>
                                    </div>
                                    <div className="text-left mr-t-20">
                                        <p><i className="fas fa-circle mr-2 f-6"></i><strong className="text-cyan">Cancel your subscription</strong> at any time to prevent monthly charges.</p>
                                        <p><i className="fas fa-circle mr-2 f-6 mr-t-20"></i>On Clicking <strong className="text-cyan">"Continue"</strong>, you will be redirected to Paypal for Payment.</p>
                                        <p><i className="fas fa-circle mr-2 f-6"></i><strong className="text-dark-theme">IoT83</strong> retains no user financial data from this <img src={require('../../assets/images/other/paypal-logo.png')} width="40" /> transaction.</p>
                                    </div>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-dark" data-dismiss="modal" >Cancel</button>
                                <button type="button" className="btn btn-success" data-dismiss="modal" onClick={this.purchaseSubscription}>Continue</button>
                            </div>
                        </div>
                    </div>
                </div>
                {/* end continue subscription modal */}

                {/* payment processing modal */}
                {(this.state.addOnPurchaseInProgress || this.state.resumeSubscriptionInProgress || this.state.purchaseSubscriptionInProgress) &&
                    <div className="modal d-block d-block animated slideInDown">
                        <div className="modal-dialog modal-dialog-centered">
                            <div className="modal-content">
                                <div className="modal-body">
                                    <div className="delete-content">
                                        <div className="delete-icon">
                                            <i className="fad fa-credit-card text-cyan"></i>
                                        </div>
                                        <div className="delete-content-loader">
                                            <img src="https://content.iot83.com/m83/misc/uploading.gif" />
                                            <p className="text-theme">Processing...</p>
                                        </div>
                                        <h6>Please wait while the system processes your request.</h6>
                                        <h6 className="fw-600">{!(this.state.addOnPurchaseInProgress && totalPayableAmountThisBillingCycle === 0) && "You will be redirected to third-party payment gateway."}</h6>
                                        <h6>Please do not  hit refresh or back button or close this window.</h6>
                                        <h6>It may take upto 60 seconds.</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                }
                {/* end payment processing modal */}

            </React.Fragment>
        );
    }
}

MyPlan.propTypes = {
    dispatch: PropTypes.func.isRequired
};

let allSelectors = {}
Object.entries(ALL_SELECTORS).map(([key, value]) => {
    allSelectors[key] = value()
})

const mapStateToProps = createStructuredSelector(allSelectors);

function mapDispatchToProps(dispatch) {
    let allActions = { dispatch }
    Object.entries(ACTIONS).map(([key, value]) => {
        allActions[key] = (...args) => dispatch(value(...args))
    })
    return allActions
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "myPlan", reducer });
const withSaga = injectSaga({ key: "myPlan", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(MyPlan);
