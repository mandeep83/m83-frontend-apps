/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * ManageEtlSchedules
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { FormattedMessage } from "react-intl";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import makeSelectManageEtlSchedules from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import { allActions as ACTIONS } from './actions'
import { allSelectors as SELECTORS } from "./selectors";
import cloneDeep from 'lodash/cloneDeep';
import messages from "./messages";
import ListingTable from "../../components/ListingTable/Loadable";
import SlidingPane from 'react-sliding-pane';
import ReactSelect from "react-select";
import Skeleton from "react-loading-skeleton";
import NoDataFoundMessage from "../../components/NoDataFoundMessage/Loadable"
import AddNewButton from "../../components/AddNewButton/Loadable";
import QuotaHeader from "../../components/QuotaHeader/Loadable";
import { convertTimestampToDate } from "../../commonUtils";

/* eslint-disable react/prefer-stateless-function */
export class ManageEtlSchedules extends React.Component {

    state = {
        addOrEditEtlSchedules: false,
        isFetchingSchedule: false,
        saveScheduleInProgress: false,
        errorMessage: null,
        isOpen: false,
        isFetching: true,
        editScheduleId: null,
        isActionFetching: false,
        isEtlChanging: false,
        searchTerm: ' ',
        payload: {
            name: '',
            description: '',
            cron: { type: 'Hourly', expr: '' },
            etlName: '',
            etlId: null,
            etlCategory: 'Batch',
            isCronEnabled: false,
            defaultParam: [],
        },
        allEtl: [],
        filteredList: [],
        scheduleList: [],
        toggleView: false,
    };

    qoutaCallback = () => {
        this.props.getAllEtlScheduler();
    }

    componentWillReceiveProps(nextProps) {

        if (nextProps.allEtlSchedulerSuccess && nextProps.allEtlSchedulerSuccess !== this.props.allEtlSchedulerSuccess) {

            let filteredList = cloneDeep(nextProps.allEtlSchedulerSuccess.response.data)
            filteredList.map(list => {
                list.cronType = list.cron.type
            })
            this.setState({
                scheduleList: filteredList,
                isFetching: false,
                filteredList,
            })
        }

        if (nextProps.allEtlSchedulerFailure && nextProps.allEtlSchedulerFailure !== this.props.allEtlSchedulerFailure) {

            this.props.showNotification('error', nextProps.allEtlSchedulerFailure.error)
            this.setState({
                isFetching: false
            }, () => this.props.resetToInitialState())
        }

        if (nextProps.createScheduleSuccess && nextProps.createScheduleSuccess !== this.props.createScheduleSuccess) {

            this.setState({
                saveScheduleInProgress: false,
                addOrEditEtlSchedules: false,
                isFetching: true
            }, () => { this.props.getAllEtlScheduler(); this.props.resetToInitialState() })
        }

        if (nextProps.createScheduleFailure && nextProps.createScheduleFailure !== this.props.createScheduleFailure) {

            this.props.showNotification('error', nextProps.createScheduleFailure.error)
            this.setState({
                isFetching: false,
                saveScheduleInProgress: false,
                isFetchingSchedule: false,
            }, () => this.props.resetToInitialState())
        }

        if (nextProps.updateSchedulerSuccess && nextProps.updateSchedulerSuccess !== this.props.updateSchedulerSuccess) {

            this.setState({
                editScheduleId: null,
                saveScheduleInProgress: false,
                addOrEditEtlSchedules: false,
                isFetching: true
            }, () => { this.props.getAllEtlScheduler(); this.props.resetToInitialState() })
        }

        if (nextProps.updateSchedulerFailure && nextProps.updateSchedulerFailure !== this.props.updateSchedulerFailure) {

            this.props.showNotification('error', nextProps.updateSchedulerFailure.error)
            this.setState({
                isFetching: false,
                saveScheduleInProgress: false,
                addOrEditEtlSchedules: false,
                isFetchingSchedule: false,
            }, () => this.props.resetToInitialState())
        }

        if (nextProps.getAllEtlSuccess && nextProps.getAllEtlSuccess !== this.props.getAllEtlSuccess) {
            let allEtl = nextProps.getAllEtlSuccess.response.data.map(action => {
                return ({
                    id: action.id,
                    value: action.id,
                    label: action.name
                })
            })

            this.setState ({
                allEtl,
                isFetchingSchedule: false,
            }, () => { this.props.resetToInitialState() })
        }

        if (nextProps.getAllEtlFailure && nextProps.getAllEtlFailure !== this.props.getAllEtlFailure) {

            this.props.showNotification('error', nextProps.getAllEtlFailure.error)
            this.setState ({
                addOrEditEtlSchedules: false,
                isFetchingSchedule: false,
            }, () => this.props.resetToInitialState())
        }

        if (nextProps.getEtlByIdSuccess && nextProps.getEtlByIdSuccess !== this.props.getEtlByIdSuccess) {

            let data = nextProps.getEtlByIdSuccess.response.data
            let payload = cloneDeep(this.state.payload)
            payload.name = data.name
            payload.description = data.description
            payload.etlId = data.etlId
            payload.etlName = data.etlName
            payload.cron.type = data.cron.type
            payload.cron.expr = ''
            payload.etlCategory = data.etlCategory
            payload.isCronEnabled = false
            payload.defaultParam = []
            this.setState({
                payload,
                isFetchingSchedule: false,
            })
        }

        if (nextProps.getEtlByIdFailure && nextProps.getEtlByIdFailure !== this.props.getEtlByIdFailure) {

            this.props.showNotification('error', nextProps.getEtlByIdFailure.error)
            this.setState({
                isFetching: false,
                addOrEditEtlSchedules: false
            }, () => this.props.resetToInitialState())
        }

        if (nextProps.deleteSchedulerSuccess && nextProps.deleteSchedulerSuccess !== this.props.deleteSchedulerSuccess) {

            this.props.showNotification('success', nextProps.deleteSchedulerSuccess.response)
            let scheduleList = JSON.parse(JSON.stringify(this.state.scheduleList))
            scheduleList = scheduleList.filter(list => list.schedulerId !== this.state.selectedScheduleId)
            this.setState((prevState) => ({
                scheduleList,
                filteredList: scheduleList,
                selectedScheduleId: '',
                isFetching: false,
                isDeletedSuccess: !prevState.isDeletedSuccess
            }), () => this.props.resetToInitialState())
        }

        if (nextProps.deleteSchedulerFailure && nextProps.deleteSchedulerFailure !== this.props.deleteSchedulerFailure) {

            this.props.showNotification('error', nextProps.deleteSchedulerFailure.error)
            this.setState({
                isFetching: false
            }, () => this.props.resetToInitialState())
        }

        if (nextProps.actionEtlScheduleSuccess && nextProps.actionEtlScheduleSuccess !== this.props.actionEtlScheduleSuccess) {

            this.props.showNotification('success', nextProps.actionEtlScheduleSuccess.response)
            let name = nextProps.actionEtlScheduleSuccess.name
            let status = nextProps.actionEtlScheduleSuccess.status
            let filteredList = JSON.parse(JSON.stringify(this.state.filteredList))

            filteredList.map(list => {
                if (list.name === name) {
                    if (status === "resume") {
                        list.isCronEnabled = true
                    }
                    else if (status === "pause") {
                        list.isCronEnabled = false
                    }
                    list.isActionFetching = false
                }
            })

            this.setState({
                filteredList,
                isEtlChanging: false
            }, () => this.props.resetToInitialState())
        }

        if (nextProps.actionEtlScheduleFailure && nextProps.actionEtlScheduleFailure !== this.props.actionEtlScheduleFailure) {

            filteredList.map(list => {
                list.isActionFetching = false
            })

            this.props.showNotification('error', nextProps.actionEtlScheduleFailure.error)
            this.setState({
                filteredList,
                isEtlChanging: false
            }, () => this.props.resetToInitialState())
        }
    }

    onChangeHandler = ({ currentTarget }) => {

        let payload = cloneDeep(this.state.payload);

        if (currentTarget.id === "name") {

            if (/^[a-zA-Z0-9]+(?:_[a-zA-Z0-9]+)*$/.test(currentTarget.value) || /^[a-zA-Z_0-9]*$/.test(currentTarget.value))
                payload.name = currentTarget.value
        }

        else if (currentTarget.id === 'cron') {
            payload.cron.type = currentTarget.value
        }

        else
            payload[currentTarget.id] = currentTarget.value

        this.setState({ payload })
    }

    createOrEditScheduleHandler = (event) => {

        event.preventDefault();
        let payload = cloneDeep(this.state.payload);

        if (!this.state.editScheduleId) {

            if (!payload.etlId) {
                this.setState({
                    errorMessage: "Please choose ETL before creating schedule."
                })
            }

            else if (payload.cron.type === "") {
                this.setState({
                    errorMessage: "Please choose cron."
                })
            }

            else {
                this.setState({
                    errorMessage: null,
                    saveScheduleInProgress: true,
                }, () => this.props.createEtlScheduler(payload))
            }
        }

        else {

            if (!payload.etlId) {
                this.setState({
                    errorMessage: "Please choose ETL before creating schedule."
                })
            }

            else if (payload.cron.type === "") {
                this.setState({
                    errorMessage: "Please choose cron."
                })
            }

            else {
                payload.isCronEnabled = true
                this.setState({
                    errorMessage: null,
                    saveScheduleInProgress: true,
                }, () => this.props.updateEtlScheduler(this.state.editScheduleId, payload))

            }
        }

    }

    addNewGroupHandler = () => {

        let payload = {
            name: '',
            description: '',
            cron: { type: 'Hourly', expr: '' },
            defaultParam: [],
            etlName: '',
            etlId: null,
            etlCategory: 'Batch',
            isCronEnabled: false,
        }

        this.setState({
            addOrEditEtlSchedules: true,
            isFetchingSchedule: true,
            payload,
            errorMessage: null,
        }, () => this.props.getAllEtl());
    }

    onCloseHandler = () => {

        this.setState({
            isOpen: false,
            type: "",
            message2: "",
        })
    }

    cancelClicked = () => {

        this.setState({
            selectedScheduleId: "",
            confirmState: false,
            selectedScheduleName: ''
        })
    }

    getSelectedEtlSchedule = (payload) => {

        let obj = {};

        if (payload) {
            obj = {
                label: payload.etlName,
                value: payload.etlId,
            }
        }
        return obj;
    }

    reactSelectChangeHandler = (list) => {

        let payload = cloneDeep(this.state.payload);
        payload.etlId = list.id
        payload.etlName = list.label
        this.setState({ payload });
    }

    openModal = (el) => {

        this.setState({
            addOrEditEtlSchedules: true,
            isFetchingSchedule: true,
            editScheduleId: el.schedulerId,
        }, () => {
            el && this.props.getEtlSchedulerById(el.schedulerId);
            this.props.getAllEtl()
        })
    }

    etlActionHandler = (row) => {

        let status = ""
        let filteredList = JSON.parse(JSON.stringify(this.state.filteredList))

        if (row.isCronEnabled) {
            status = "pause"
        }

        else if (!row.isCronEnabled) {
            status = "resume"
        }

        filteredList.map(list => {

            if (list.schedulerId === row.schedulerId) {
                list.isActionFetching = true
            }

        })

        this.setState({
            filteredList,
            isEtlChanging: true,
        }, () => this.props.etlSchedulerAction(row.name, status))

    }

    getColumns = () => {
        let column = [
            {
                Header: "Name", width: "17_5",
                filterable: true,
                sortable: true,
                accessor: "name",
                cell: (row) => (
                    <React.Fragment>
                        <div className="list-view-icon">
                            <i className="fad fa-calendar-alt text-dark-theme"></i>
                        </div>
                        <div className="list-view-icon-box">
                            <h6 className="text-theme fw-600">{row.name}</h6>
                            <p>{`${row.description || "-"}`}</p>
                        </div>
                    </React.Fragment>
                )
            },
            {
                Header: "ETL", width: 15,
                filterable: true,
                sortable: true,
                accessor: "etlName",
                cell: (row) => <h6 className="fw-400">{row.etlName}</h6>,
            },
            {
                Header: "Cron", width: 10,
                filterable: true,
                sortable: true,
                accessor: "cronType",
                cell: (row) => <span className="alert alert-primary list-view-badge">{row.cron.type}</span>,
            },
            {
                Header: "Last Executed", width: "12_5",
                cell: (row) => (
                    <h6 data-tooltip data-tooltip-text={convertTimestampToDate(new Date(row.lastExecution).getTime())} data-tooltip-place="bottom">{convertTimestampToDate(new Date(row.lastExecution).getTime())}</h6>
                )
            },
            { Header: "Created", width: 15, accessor: "createdAt" },
            { Header: "Updated", width: 15, accessor: "updatedAt" },
            {
                Header: "Actions", width: 15, cell: (row) => (
                    <div className="button-group">
                        {row.isActionFetching ?
                            <button type="button" className="btn-transparent btn-transparent-green"><i className="far fa-cog fa-spin"></i></button>
                            :
                            <div className="toggle-switch-wrapper d-inline-block">
                                <label className="toggle-switch ml-0" data-tooltip data-tooltip-text={row.isCronEnabled ? "Stop" : "Start"} data-tooltip-place="bottom">
                                    <input type="checkbox"
                                        disabled={this.state.isEtlChanging}
                                        onChange={() => this.etlActionHandler(row)} checked={row.isCronEnabled} />
                                    <span className="toggle-switch-slider"></span>
                                </label>
                            </div>
                        }
                        <button className="btn-transparent btn-transparent-blue" data-tooltip data-tooltip-text="Edit" data-tooltip-place="bottom"
                            onClick={() => { this.openModal(row) }}
                            disabled={row.isCronEnabled}
                        >
                            <i className="far fa-pencil"></i>
                        </button>
                        <button className="btn-transparent btn-transparent-red" data-tooltip data-tooltip-text="Delete" data-tooltip-place="bottom"
                            disabled={row.isCronEnabled}
                            onClick={() => { this.setState({ confirmState: true, selectedScheduleId: row.schedulerId, selectedScheduleName: row.name }) }}>
                            <i className="far fa-trash-alt"></i>
                        </button>
                    </div>
                )
            },
        ];
        return column;
    }

    getStatusBarClass = (row) => {
        if (row.isCronEnabled)
            return { className: "green", tooltipText: "Running" }
        return { className: "red", tooltipText: "Stopped" }

    }

    refreshComponent = () => {

        this.setState({
            isFetching: true,
        }, () =>
            this.props.getAllEtlScheduler()
        )

    }

    filteredDataList = (value) => {

        let filterList = cloneDeep(this.state.scheduleList);

        if (value) {
            filterList = filterList.filter(item => item.name.toLowerCase().includes(value.toLowerCase()))
        }

        this.setState({
            searchTerm: value,
            filteredList: filterList
        })

    }

    emptySearchBox = () => {

        let filteredList = JSON.parse(JSON.stringify(this.state.scheduleList))
        this.setState({
            searchTerm: '',
            filteredList
        })
    }

    toggleView = () => {
        this.setState((prevState) => ({ toggleView: !prevState.toggleView }))
    }

    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>ManageEtlSchedules</title>
                    <meta
                        name="description"
                        content="Description of ManageEtlSchedules"
                    />
                </Helmet>
                
                <QuotaHeader
                    showQuota
                    addButtonText="Add Etl Schedule"
                    componentName="ETL Schedules"
                    productName="schedules"
                    onAddClick={this.addNewGroupHandler}
                    searchBoxDetails={{
                        value: this.state.searchTerm,
                        onChangeHandler: this.filteredDataList,
                        isDisabled: !this.state.scheduleList.length || this.state.isFetching
                    }}
                    refreshDisabled={this.state.isFetching}
                    addDisabled={this.state.isFetching}
                    refreshHandler={this.refreshComponent}
                    callBack={this.qoutaCallback}
                    toggleButton={this.toggleView}
                    isAddSuccess={this.state.isAddSuccess}
                    isDeletedSuccess={this.state.isDeletedSuccess}
                    {...this.props}
                />

                <div className="content-body">
                    {this.state.isFetching ?
                        <Skeleton count={9} className="skeleton-list-loader" /> :
                        <React.Fragment>
                            {this.state.scheduleList.length > 0 ?
                                this.state.filteredList.length > 0 ?
                                    !this.state.toggleView ?
                                        <ListingTable
                                            columns={this.getColumns()}
                                            data={this.state.filteredList}
                                            statusBar={this.getStatusBarClass}
                                        /> :
                                        <ul className="card-view-list">
                                            {this.state.filteredList.map((item, index) =>
                                                <li key={index}>
                                                    <div className="card-view-box">
                                                        <div className="card-view-header">
                                                            <span data-tooltip data-tooltip-text={item.isCronEnabled ? "Running" : "Stopped"} data-tooltip-place="bottom" className={item.isCronEnabled ? "card-view-header-status bg-success" : "card-view-header-status bg-danger"}></span>
                                                            <span className="alert alert-primary mr-r-7">{item.cron.type}</span>
                                                            {item.lastExecution ?
                                                                <span className="alert alert-success"><i className="fas fa-history mr-1"></i>{convertTimestampToDate(new Date(item.lastExecution).getTime())}</span>: ""
                                                            }
                                                            <div className="dropdown">
                                                                <button className="btn-transparent btn-transparent-gray" type="button" data-toggle="dropdown">
                                                                    <i className="fas fa-ellipsis-v"></i>
                                                                </button>
                                                                <div className="dropdown-menu">
                                                                    {item.isActionFetching ?
                                                                        <button type="button" className="dropdown-item"><i className="far fa-cog fa-spin"></i>Loading...</button>
                                                                        :
                                                                        <div className="toggle-switch-wrapper d-inline-block">
                                                                            <label className="toggle-switch ml-0" >
                                                                                <input type="checkbox"
                                                                                    disabled={this.state.isEtlChanging}
                                                                                    onChange={() => this.etlActionHandler(item)} checked={item.isCronEnabled} />
                                                                                <span className="toggle-switch-slider"></span>
                                                                            </label>
                                                                            <span>{item.isCronEnabled ? "Stop" : "Start"}</span>
                                                                        </div>
                                                                    }
                                                                    <button className="dropdown-item" onClick={() => { this.openModal(item) }} disabled={item.isCronEnabled}>
                                                                        <i className="far fa-pencil"></i>Edit
                                                                    </button>
                                                                    <button className="dropdown-item" disabled={item.isCronEnabled}
                                                                        onClick={() => { this.setState({ confirmState: true, selectedScheduleId: item.schedulerId, selectedScheduleName: item.name }) }}>
                                                                        <i className="far fa-trash-alt"></i>Delete
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="card-view-body">
                                                            <div className="card-view-icon">
                                                                <i className="fad fa-calendar-alt"></i>
                                                            </div>
                                                            <h6><i className="far fa-address-book"></i>{item.name ? item.name : "-"}</h6>
                                                            <p><i className="far fa-file-alt"></i>{item.description ? item.description : "-"}</p>
                                                            <p><i className="far fa-exchange"></i>{item.etlName}</p>
                                                        </div>
                                                        <div className="card-view-footer d-flex">
                                                            <div className="flex-50 p-3">
                                                                <h6>Created</h6>
                                                                <p><strong>By - </strong>{item.createdBy}</p>
                                                                <p><strong>At - </strong>{new Date(item.createdAt).toLocaleString('en-US', { createdAt: localStorage.createdAt })}</p>
                                                            </div>
                                                            <div className="flex-50 p-3">
                                                                <h6>Updated</h6>
                                                                <p><strong>By - </strong>{item.updatedBy}</p>
                                                                <p><strong>At - </strong>{new Date(item.updatedAt).toLocaleString('en-US', { createdAt: localStorage.createdAt })}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            )}
                                        </ul>
                                    :
                                    <NoDataFoundMessage /> :
                                <AddNewButton
                                    text1="No ETL Schedules available"
                                    text2="You haven't created any ETL Schedule yet. Please create a ETL Schedule first."
                                    imageIcon="addSchedule.png"
                                    addButtonEnable={true}
                                    createItemOnAddButtonClick={this.addNewGroupHandler}
                                />
                            }
                        </React.Fragment>
                    }
                </div>

                <SlidingPane
                    className=''
                    overlayClassName='sliding-form'
                    closeIcon={<div></div>}
                    isOpen={this.state.addOrEditEtlSchedules || false}
                    from='right'
                    width='500px'
                >
                    <div className="modal-content">
                        <div className="modal-header">
                            <h6 className="modal-title">{this.state.editScheduleId ? "Update ETL Schedules" : "Add New ETL Schedules"}
                                <button type="button" className="btn btn-light close" onClick={() => { this.setState({ addOrEditEtlSchedules: false, editScheduleId: null, errorMessage: '', }) }}>
                                    <i className="fas fa-angle-right"></i>
                                </button>
                            </h6>
                        </div>

                        {this.state.saveScheduleInProgress || this.state.isFetchingSchedule ?
                            <div className="modal-loader">
                                <i className="fad fa-sync-alt fa-spin"></i>
                            </div> :
                            <form onSubmit={this.createOrEditScheduleHandler} >
                                <div className="modal-body">
                                    {this.state.errorMessage &&
                                        <p className="modal-body-error">{this.state.errorMessage}</p>
                                    }

                                    <div className="form-group">
                                        <label className="form-group-label">Name : <i className="fas fa-asterisk form-group-required"></i></label>
                                        <input type="text" className="form-control  text-capitalize" id="name"
                                            disabled={this.state.editScheduleId}
                                            value={this.state.payload.name}
                                            onChange={this.onChangeHandler}
                                            required autoFocus />
                                    </div>

                                    <div className="form-group">
                                        <label className="form-group-label">Description :</label>
                                        <textarea rows="3" type="text" id="description" className="form-control" value={this.state.payload.description}
                                            onChange={this.onChangeHandler}
                                        />
                                    </div>

                                    <div className="form-group">
                                        <label className="form-group-label">CRON : <i className="fas fa-asterisk form-group-required"></i></label>
                                        <select className="form-control" id="cron" onChange={this.onChangeHandler} value={this.state.payload.cron.type}>
                                            <option>Hourly</option>
                                            <option>Daily</option>
                                            {/* <option>Custom</option> */}
                                        </select>
                                    </div>

                                    <div className="form-group">
                                        <label className="form-group-label">ETL : <i className="fas fa-asterisk form-group-required"></i></label>

                                        <ReactSelect
                                            options={this.state.allEtl}
                                            value={this.getSelectedEtlSchedule(this.state.payload)}
                                            onChange={this.reactSelectChangeHandler}
                                            className="form-control-multi-select"
                                            isMulti={false}
                                            name={this.state.payload.etlName}
                                        >
                                        </ReactSelect>
                                    </div>
                                </div>

                                <div className="modal-footer justify-content-start">
                                    <button className="btn btn-primary" >{this.state.editScheduleId ? "Update" : "Save"}</button>
                                    <button type="button" className="btn btn-dark" onClick={() => { this.setState({ addOrEditEtlSchedules: false, editScheduleId: null }) }}>Cancel</button>
                                </div>

                            </form>
                        }
                    </div>
                </SlidingPane>

                {this.state.isOpen &&
                    <NotificationModal
                        type={this.state.modalType}
                        message2={this.state.message2}
                        onCloseHandler={this.onCloseHandler}
                    />
                }

                {this.state.confirmState &&
                    <ConfirmModel
                        status={"delete"}
                        deleteName={this.state.selectedScheduleName}
                        confirmClicked={() => {
                            this.props.deleteEtlScheduler(this.state.selectedScheduleId)
                            this.setState({
                                isFetching: true,
                                confirmState: false,
                                isOpen: false,
                                selectedScheduleName: ''
                            })
                        }}
                        cancelClicked={() => {
                            this.cancelClicked()
                        }}
                    />
                }
            </React.Fragment>
        );
    }
}

ManageEtlSchedules.propTypes = {
    dispatch: PropTypes.func.isRequired
};

let allSelectors = {}
Object.entries(SELECTORS).map(([key, value]) => {
    allSelectors[key] = value()
})

const mapStateToProps = createStructuredSelector(allSelectors);

function mapDispatchToProps(dispatch) {
    let allActions = { dispatch }
    Object.entries(ACTIONS).map(([key, value]) => {
        allActions[key] = (...args) => dispatch(value(...args))
    })
    return allActions;
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "manageEtlSchedules", reducer });
const withSaga = injectSaga({ key: "manageEtlSchedules", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(ManageEtlSchedules);
