/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/*
 *
 * ManageEtlSchedules constants
 *
 */

export const RESET_TO_INITIAL_STATE = "app/ManageEtlSchedules/DEFAULT_ACTION";

export const CREATE_ETL_SCHEDULER = {
	action: 'app/ManageEtlSchedules/CREATE_ETL_SCHEDULER',
	success: "app/ManageEtlSchedules/CREATE_ETL_SCHEDULER_SUCCESS",
	failure: "app/ManageEtlSchedules/CREATE_ETL_SCHEDULER_FAILURE",
	urlKey: "createEtlScheduler",
	successKey: "createScheduleSuccess",
	failureKey: "createScheduleFailure",
	actionName: "createEtlScheduler",
	actionArguments: ["payload"]
}

export const ACTION_ETL_SCHEDULER = {
	action: 'app/ManageEtlSchedules/ACTION_ETL_SCHEDULER',
	success: "app/ManageEtlSchedules/ACTION_ETL_SCHEDULER_SUCCESS",
	failure: "app/ManageEtlSchedules/ACTION_ETL_SCHEDULER_FAILURE",
	urlKey: "etlSchedulerAction",
	successKey: "actionEtlScheduleSuccess",
	failureKey: "actionEtlScheduleFailure",
	actionName: "etlSchedulerAction",
	actionArguments: ["name", "status"]
}

export const GET_ALL_ETL = {
	action: 'app/ManageEtlSchedules/GET_ALL_ETL',
	success: "app/ManageEtlSchedules/GET_ALL_ETL_SUCCESS",
	failure: "app/ManageEtlSchedules/GET_ALL_ETL_FAILURE",
	urlKey: "getAllEtl",
	successKey: "getAllEtlSuccess",
	failureKey: "getAllEtlFailure",
	actionName: "getAllEtl",
}

export const UPDATE_ETL_SCHEDULER = {
	action: 'app/ManageEtlSchedules/UPDATE_ETL_SCHEDULER',
	success: "app/ManageEtlSchedules/UPDATE_ETL_SCHEDULER_SUCCESS",
	failure: "app/ManageEtlSchedules/UPDATE_ETL_SCHEDULER_FAILURE",
	urlKey: "updateEtlScheduler",
	successKey: "updateSchedulerSuccess",
	failureKey: "updateSchedulerFailure",
	actionName: "updateEtlScheduler",
	actionArguments: ["id","payload"]
}

export const DELETE_ETL_SCHEDULER = {
	action: 'app/ManageEtlSchedules/DELETE_ETL_SCHEDULER',
	success: "app/ManageEtlSchedules/DELETE_ETL_SCHEDULER_SUCCESS",
	failure: "app/ManageEtlSchedules/DELETE_ETL_SCHEDULER_FAILURE",
	urlKey: "deleteEtlScheduler",
	successKey: "deleteSchedulerSuccess",
	failureKey: "deleteSchedulerFailure",
	actionName: "deleteEtlScheduler",
	actionArguments: ["id"]
}

export const GET_ETL_SCHEDULER = {
	action: 'app/ManageEtlSchedules/GET_ETL_SCHEDULER',
	success: "app/ManageEtlSchedules/GET_ETL_SCHEDULER_SUCCESS",
	failure: "app/ManageEtlSchedules/GET_ETL_SCHEDULER_FAILURE",
	urlKey: "getEtlSchedulerById",
	successKey: "getEtlByIdSuccess",
	failureKey: "getEtlByIdFailure",
	actionName: "getEtlSchedulerById",
	actionArguments: ["id"]
}

export const GET_ALL_SCHEDULER = {
	action: 'app/ManageEtlSchedules/GET_ALL_SCHEDULER',
	success: "app/ManageEtlSchedules/GET_ALL_SCHEDULER_SUCCESS",
	failure: "app/ManageEtlSchedules/GET_ALL_SCHEDULER_FAILURE",
	urlKey: "getAllEtlScheduler",
	successKey: "allEtlSchedulerSuccess",
	failureKey: "allEtlSchedulerFailure",
	actionName: "getAllEtlScheduler",
}
