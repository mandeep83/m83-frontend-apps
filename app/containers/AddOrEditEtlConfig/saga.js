/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { apiCallHandler } from '../../api';
import { takeEvery } from 'redux-saga';
import {
  GET_ETL_INFO, GET_ETL_INFO_SUCCESS, GET_ETL_INFO_FAILURE,
  SAVE_ETL, SAVE_ETL_SUCCESS, SAVE_ETL_FAILURE,
  DATA_LIST_REQUEST, DATA_LIST_SUCCESS, DATA_LIST_FAILURE, GET_ALL_SPARKS_METHODS, GET_ALL_SPARKS_METHODS_SUCCESS, GET_ALL_SPARKS_METHODS_FAILURE,
  GET_SOURCE_SUCCESS,GET_SOURCE_FAILURE,GET_SOURCE, 
  GET_CATEGORY, GET_CATEGORY_SUCCESS,GET_CATEGORY_FAILURE,
  GET_TOPICS,GET_TOPICS_SUCCESS,GET_TOPICS_FAILURE,
  GET_FUNCTIONS,GET_FUNCTIONS_SUCCESS,GET_FUNCTIONS_FAILURE,
  GET_CONNECTORS,GET_CONNECTORS_SUCCESS,GET_CONNECTORS_FAILURE, DO_TRANSFORMATION,
  DO_TRANSFORMATION_SUCCESS,DO_TRANSFORMATION_FAILURE,
  GET_BUCKETS,GET_BUCKETS_SUCCESS,GET_BUCKETS_FAILURE,
  GENERATE_SCHEMA, GENERATE_SCHEMA_SUCCESS, GENERATE_SCHEMA_FAILURE,
  GET_ETL_INFO_LIST, GET_ETL_INFO_LIST_SUCCESS, GET_ETL_INFO_LIST_SUCCESS_FAILURE,
  GET_DATA_CONNECTORS,GET_DATA_CONNECTORS_SUCCESS,GET_DATA_CONNECTORS_FAILURE,
  GET_CATEGORY_CONNECTOR, GET_CATEGORY_CONNECTOR_SUCCESS, GET_CATEGORY_CONNECTOR_FAILURE,
  GENERATE_TIME_SERIES_SCHEMA, GENERATE_TIME_SERIES_SCHEMA_SUCCESS, GENERATE_TIME_SERIES_SCHEMA_FAILURE,
  GET_CONNECTORS_FOR_CATEGORY, GET_CONNECTORS_FOR_CATEGORY_SUCCESS, GET_CONNECTORS_FOR_CATEGORY_FAILURE
} from './constants';
import { func } from 'prop-types';

export function* getEtlInfo(action) {
  yield [apiCallHandler(action, GET_ETL_INFO_SUCCESS, GET_ETL_INFO_FAILURE, 'etlGetInfo')];
}

export function* saveUpdateEtl(action) {
  yield [apiCallHandler(action, SAVE_ETL_SUCCESS, SAVE_ETL_FAILURE, 'etlHandler')];
}

export function* watcherGetEtlInfo() {
  yield takeEvery(GET_ETL_INFO, getEtlInfo);
}

export function* watcherSaveUpdate() {
  yield takeEvery(SAVE_ETL, saveUpdateEtl);
}

export function* apiGetDataListRequest(action) {
  yield [apiCallHandler(action, DATA_LIST_SUCCESS, DATA_LIST_FAILURE, 'getDataSourceList')];
}

export function* watcherGetDataList() {
  yield takeEvery(DATA_LIST_REQUEST, apiGetDataListRequest);
}


export function* apiGetSparksMethodRequest(action) {
  yield [apiCallHandler(action, GET_ALL_SPARKS_METHODS_SUCCESS, GET_ALL_SPARKS_METHODS_FAILURE, 'getAllSparksMethod')];
}

export function* apiGetSource(action) {
  yield [apiCallHandler(action, GET_SOURCE_SUCCESS, GET_SOURCE_FAILURE, 'getDataConnectors')];
}

export function* apiGetCategory(action) {
  yield [apiCallHandler(action, GET_CATEGORY_SUCCESS, GET_CATEGORY_FAILURE, 'getCategoryData')];
}

export function* apiGetTopics(action) {
  yield [apiCallHandler(action, GET_TOPICS_SUCCESS, GET_TOPICS_FAILURE, 'getTopicsData',false)];
}

export function* apiGetFunctions(action) {
  yield [apiCallHandler(action, GET_FUNCTIONS_SUCCESS, GET_FUNCTIONS_FAILURE, 'getFunction',false)];
}

export function* apiGetConnectors(action) {
  yield [apiCallHandler(action, GET_CONNECTORS_SUCCESS, GET_CONNECTORS_FAILURE, 'getConnectors')];
}

export function* apiDoTransformation(action) {
  yield [apiCallHandler(action, DO_TRANSFORMATION_SUCCESS, DO_TRANSFORMATION_FAILURE, 'doTransformation')];
}

export function* apiGetBuckets(action) {
  yield [apiCallHandler(action, GET_BUCKETS_SUCCESS, GET_BUCKETS_FAILURE, 'getBucket')];
}

export function* apiGenerateSchema(action) {
  yield [apiCallHandler(action, GENERATE_SCHEMA_SUCCESS, GENERATE_SCHEMA_FAILURE, 'hitSchema',false)];
}

export function* apiGenerateTimeSeriesSchema(action){
  yield [apiCallHandler(action, GENERATE_TIME_SERIES_SCHEMA_SUCCESS, GENERATE_TIME_SERIES_SCHEMA_FAILURE, 'hitTimeSeriesSchema', false)];
}

export function* getEtlInfoList(action) {
  yield [apiCallHandler(action, GET_ETL_INFO_LIST_SUCCESS ,GET_ETL_INFO_LIST_SUCCESS_FAILURE, 'etlActions')]
}

export function* apiGetDataConnectors(action){
  yield [apiCallHandler(action, GET_DATA_CONNECTORS_SUCCESS, GET_DATA_CONNECTORS_FAILURE, 'getDataConnectors')];
}

export function* apiGetConnectorForCategory(action){
  yield[apiCallHandler(action, GET_CONNECTORS_FOR_CATEGORY_SUCCESS, GET_CONNECTORS_FOR_CATEGORY_FAILURE, 'getConnectorsForCategory')]
}


export function* apiGetConnectorByCategory(action){
  yield[apiCallHandler(action, GET_CATEGORY_CONNECTOR_SUCCESS, GET_CATEGORY_CONNECTOR_FAILURE, 'getConnectorsByCategory')]
}

export function* watcherGetAllSparksMethods() {
  yield takeEvery(GET_ALL_SPARKS_METHODS, apiGetSparksMethodRequest);
}

export function* watcherGetSourceList() {
  yield takeEvery(GET_SOURCE, apiGetSource);
}

export function* watcherGetCategory() {
  yield takeEvery(GET_CATEGORY, apiGetCategory);
}

export function* watcherGetTopics() {
  yield takeEvery(GET_TOPICS, apiGetTopics);
}

export function* watcherGetFunctions() {
  yield takeEvery(GET_FUNCTIONS, apiGetFunctions);
}

export function* watcherGetConnectors() {
  yield takeEvery(GET_CONNECTORS, apiGetConnectors);
}

export function* watcherDoTransformation() {
  yield takeEvery(DO_TRANSFORMATION, apiDoTransformation);
}

export function* watcherGetBuckets() {
  yield takeEvery(GET_BUCKETS, apiGetBuckets);
}

export function* watcherGenerateSchema() {
  yield takeEvery(GENERATE_SCHEMA, apiGenerateSchema);
}

export function* watchetrGenerateTimeSeriesSchema(){
  yield takeEvery(GENERATE_TIME_SERIES_SCHEMA, apiGenerateTimeSeriesSchema);
}

export function* watcherGetEtlInfoList() {
  yield takeEvery(GET_ETL_INFO_LIST, getEtlInfoList);
}

export function* watcherGetDataConnectors() {
  yield takeEvery(GET_DATA_CONNECTORS, apiGetDataConnectors)
}

export function* watcherGetConnectorByCategory() {
  yield takeEvery(GET_CATEGORY_CONNECTOR, apiGetConnectorByCategory)
}

export function* watcherGetConnectorForCategory() {
  yield takeEvery(GET_CONNECTORS_FOR_CATEGORY, apiGetConnectorForCategory)
}

export default function* rootSaga() {
  yield [
    watcherGetEtlInfo(),
    watcherSaveUpdate(),
    watcherGetDataList(),
    watcherGetAllSparksMethods(),
    watcherGetSourceList(),
    watcherGetCategory(),
    watcherGetTopics(),
    watcherGetFunctions(),
    watcherGetConnectors(),
    watcherDoTransformation(),
    watcherGetBuckets(),
    watcherGenerateSchema(),
    watcherGetEtlInfoList(),
    watcherGetDataConnectors(),
    watcherGetConnectorByCategory(),
    watchetrGenerateTimeSeriesSchema(),
    watcherGetConnectorForCategory()
  ]
}

