/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { DEFAULT_ACTION, SAVE_ETL, GET_ETL_INFO, DATA_LIST_REQUEST, GET_ALL_SPARKS_METHODS,GET_SOURCE,GET_CATEGORY,GET_TOPICS,GET_FUNCTIONS,GET_CONNECTORS,DO_TRANSFORMATION,GET_BUCKETS,GENERATE_SCHEMA,GET_ETL_INFO_LIST,GET_DATA_CONNECTORS, GET_CATEGORY_CONNECTOR, GENERATE_TIME_SERIES_SCHEMA, GET_CONNECTORS_FOR_CATEGORY} from "./constants";

export function defaultAction() {
  return {
    type: DEFAULT_ACTION
  };
}

export function getEtlInfoList() {
  return {
  type: GET_ETL_INFO_LIST
  }
}

export function getEtlInfo(id) {
  return {
    type: GET_ETL_INFO,
    id,
  };
}

export function saveUpdateEtl(payload, id) {
  return {
    type: SAVE_ETL,
    payload,
    id,
  };
}

export function getDataSetList() {
  return {
    type: DATA_LIST_REQUEST
  };
}

export function getDataConnectors(){
  return{
    type: GET_DATA_CONNECTORS
  };
}

export function getAllSparksMethod() {
  return {
    type: GET_ALL_SPARKS_METHODS
  };
}

export function getSourceList(){
  return {
    type: GET_SOURCE
  };
}

export function getCategory(category){
  return {
    type: GET_CATEGORY,
    category
  }
}

export function getTopicsIds(id){
  return {
    type: GET_TOPICS,
    id
  }
}

export function getFunctions(functionType){
  return {
    type: GET_FUNCTIONS,
    functionType
  }
}

export function getConnectors(){
  return {
    type: GET_CONNECTORS,
  }
}

export function getConnectorsForCategory(){
  return {
    type: GET_CONNECTORS_FOR_CATEGORY,
  }
}

export function transformation(payload,id){
  return {
    type: DO_TRANSFORMATION,
    payload,
    id
  }
}

export function getBuckets(){
  return {
    type: GET_BUCKETS,
  }
}

export function generateSchema(topic,payload){
  return {
    topic,
    payload,
    type: GENERATE_SCHEMA,
  }
}

export function getCategoryByConnectors(category){
  return{
    type:GET_CATEGORY_CONNECTOR,
    category
  }
}

export function generateTimeSeriesSchema(topic, payload){
  return{
    topic,
    payload,
    type: GENERATE_TIME_SERIES_SCHEMA
  }
}