/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import AceEditor from "react-ace";

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import * as SELECTORS from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import * as ACTIONS from './actions';
import Loader from "../../components/Loader";
import NotificationModal from '../../components/NotificationModal/Loadable'
import ReactSelect from "react-select";
import TagsInput from 'react-tagsinput';
import { setCharAt } from '../../commonUtils';
import MQTT from 'mqtt';
import JSONInput from "react-json-editor-ajrm/dist";
import 'brace/theme/github';
import ReactTooltip from "react-tooltip";
import ReactJson from "react-json-view";
import { Prompt } from 'react-router';
//import CopyToClipboard from 'react-copy-to-clipboard';
//const { jsonToTableHtmlString } = require('json-table-converter')
let client;

/* eslint-disable react/prefer-stateless-function */
export class AddOrEditEtlConfig extends React.Component {
    state = {
        saveSuccess: false,
        payload: {

            createdFor: this.props.match.params.createdFor,
            connector: null,
            definition: [
                {
                    operations: [],
                    type: "select"
                }
            ],
            description: "",
            etlConfig: {
                maxOffset: "1000",
                triggerInterval: 1000,
                threshold: 10
            },
            isDebug: false,
            name: "",
            state: "stop",
            type: ""
        },
        activeTab: this.props.match.params.id ? "define" : "source",
        // activeTab: "define",
        allSparksMethods: [],
        paths: [],
        sourceList: [],
        categoryList: [],
        topicList: [],
        functionsList: [],
        connectorList: [],
        functionType: "v3/transformations/sparkFunctions",
        activeConfig: "select",
        nodeDefinition: [{ type: "select", operations: [] }],
        nodeName: "Root",
        activeNodeId: "root",
        rootNodeValue: "Root",
        sinkPayload: {
            type: "sink",
            connector: {
                id: "",
                properties: {},
            },
        },
        bucketList: [],
        connectorName: "",
        exception: [],
        transformationList: [],
        connectorLoader: false,
        debugFlag: false,
        mqttConnectStatus: false,
        debugJson: {},
        schemaData: null,
        functionData: '',
        functionName: '',
        showFunctionModal: false,
        defaultList: [],
        defaultCategoryList: [],
        defaultConnectorList: [],
        defaultType: '',
        defaultCategory: '',
        prevDefaultType: '',
        prevDefaultCategory: '',
        prevDefaultCategoryList: [],

        etlInfo: {},
        attributeView: "JSONview",
        defaultConnectorFlag: false,
        newSinkData: false,
        collectionNameFill: true,
        connectorPropertiesFill: true,
        bucketIdFill: true,
        directoryNameFill: true,
        basePathFill: true,
        name: true,
        bucketDataDirs: [],
        timeSeriesFlag: false,
        timeSeriesSchema: [],
        schemaCallFlag: false,
        timeSeriesSchemaFlag: true,
        showWarningMessage: false,
        functionLoader: true,
        etlFunctionModal: true,
        selectedDefinitionIndex : 0,
        selectedDefinitionType : "select",
        selectedDefinitionValues : [],
        initialSchemaStatus : false,
        branchSerialIndex : 'root',
        sinkCollectionId : ''
    }

    componentWillMount() {
        let sinkCollectionId =   localStorage.getItem('myProjects') && JSON.parse(localStorage.getItem('myProjects'))[0].id
        sinkCollectionId = `_${sinkCollectionId.slice(0,3)}${sinkCollectionId.slice(sinkCollectionId.length - 5, sinkCollectionId.length )}`
        this.setState({sinkCollectionId})
        this.props.getSourceList();
        this.props.getEtlInfoList();
        // this.props.getDataSetList();
        this.props.getConnectors();

        this.props.getFunctions(this.state.functionType);
        if (this.props.match.params.id) {
            this.props.getEtlInfo(this.props.match.params.id)
        }
        else {
            this.getMqttConnection();
        }
        //this.getMqttConnection();
        this.props.getDataConnectors();
    }

    componentDidUpdate = () => {
        if (this.state.showWarningMessage) {
            window.onbeforeunload = () => true
        } else {
            window.onbeforeunload = undefined
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.fetchBranchesSuccess && nextProps.fetchBranchesSuccess !== this.props.fetchBranchesSuccess) {
            this.setState({
                bucketList: nextProps.fetchBranchesSuccess
            })
        }

        if (nextProps.EtlListSuccess && nextProps.EtlListSuccess !== this.props.EtlListSuccess) {
            this.setState({
                transformationList: nextProps.EtlListSuccess
            })
        }

        if (nextProps.fetchBranchesError && nextProps.fetchBranchesError !== this.props.fetchBranchesError) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.fetchBranchesError,
            })
        }

        if (nextProps.transformationSuccess && nextProps.transformationSuccess !== this.props.transformationSuccess) {
            this.setState({
                modalType: "success",
                isOpen: true,
                message2: nextProps.transformationSuccess,
                showWarningMessage: false,
            }, () => this.props.history.push('/etlPipeline'))
        }

        if (nextProps.transformationError && nextProps.transformationError !== this.props.transformationError) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.transformationError,
            })
        }

        if (nextProps.fetchedConnectors && nextProps.fetchedConnectors !== this.props.fetchedConnectors) {
            this.setState({
                connectorList: nextProps.fetchedConnectors,
            })
        }

        if (nextProps.fetchedConnectorsError && nextProps.fetchedConnectorsError !== this.props.fetchedConnectorsError) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.fetchedConnectorsError,
            })
        }

        if (nextProps.fetchedConnectorsForCategory && nextProps.fetchedConnectorsForCategory !== this.props.fetchedConnectorsForCategory) {
            this.setState({
                connectorList: nextProps.fetchedConnectorsForCategory,
            }, () => this.props.getCategoryByConnectors(this.state.defaultCategory))
        }

        if (nextProps.fetchedConnectorsForCategoryError && nextProps.fetchedConnectorsForCategoryError !== this.props.fetchedConnectorsForCategoryError) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.fetchedConnectorsForCategoryError,
            })
        }

        if (nextProps.fetchedFunction && nextProps.fetchedFunction !== this.props.fetchedFunction) {
            this.setState({
                functionLoader: false,
                functionsList: nextProps.fetchedFunction
            })
        }

        if (nextProps.fetchedFunctionError && nextProps.fetchedFunctionError !== this.props.fetchedFunctionError) {
            this.setState({
                functionLoader: false,
                modalType: "error",
                isOpen: true,
                message2: nextProps.fetchedFunctionError,
            })
        }

        if (nextProps.fetchTopics && nextProps.fetchTopics !== this.props.fetchTopics) {
            this.setState({
                topicList: nextProps.fetchTopics,
                connectorLoader: false
            })
        }

        if (nextProps.fetchTopicsError && nextProps.fetchTopicsError !== this.props.fetchTopicsError) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.fetchTopicsError,
            })
        }

        if (nextProps.fetchCategory && nextProps.fetchCategory !== this.props.fetchCategory) {
            let payload = JSON.parse(JSON.stringify(this.state.payload))

            let topicList = [], bucketDataDirs = []
            if (nextProps.fetchCategory.length > 0) {
                if (this.state.payload.type === "stream") {
                    payload.connector = {
                        id: nextProps.fetchCategory[0].id,
                        properties: {
                            topicId: nextProps.fetchCategory[0].properties.topics[0].topicId
                        }
                    }
                    topicList = nextProps.fetchCategory[0].properties.topics
                } else {
                    if (nextProps.fetchCategory[0].category === "EXCEL") {
                        payload.connector = {
                            id: nextProps.fetchCategory[0].id,
                            properties: {
                                fileId: nextProps.fetchCategory[0].properties.fileList[0].fileId
                            }
                        }
                    }
                    else if (nextProps.fetchCategory[0].category === "S3") {
                        let defaultS3Source = nextProps.fetchCategory.find(s => s.sink === false)
                        if (defaultS3Source) {
                            payload.connector = {
                                id: defaultS3Source.id,
                                properties: {
                                    bucketId: defaultS3Source.properties.buckets[0].bucketId,
                                    dirId: defaultS3Source.properties.buckets[0].dataDirs[0].dirId
                                }
                            }
                            bucketDataDirs = defaultS3Source.properties.buckets[0].dataDirs
                        } else {
                            payload.connector = {
                                id: nextProps.fetchCategory[0].id,
                                properties: {}
                            }
                        }
                    }
                    else {
                        payload.connector = {
                            id: nextProps.fetchCategory[0].id,
                            properties: {}
                        }
                    }
                }
            }
            if (this.state.payload.type === "stream") {
                this.setState({
                    categoryList: nextProps.fetchCategory,
                    payload,
                    topicList,
                    bucketDataDirs,
                    schemaData: nextProps.fetchCategory.length > 0 && nextProps.fetchCategory[0].properties.topics.length > 0 ? JSON.parse(nextProps.fetchCategory[0].properties.topics[0].schema) : null
                })
            }
            else {
                this.setState({
                    categoryList: nextProps.fetchCategory,
                    payload,
                    topicList,
                    bucketDataDirs
                })
            }

            // if (nextProps.fetchCategory === "EXCEL") {
            //     this.setState({
            //         categoryList: nextProps.fetchCategory,
            //         payload,
            //         topicList
            //     })
            // }
            // else {
            //     this.setState({
            //         categoryList: nextProps.fetchCategory,
            //         payload,
            //         topicList
            //     })
            // }

        }

        if (nextProps.fetchCategoryError && nextProps.fetchCategoryError !== this.props.fetchCategoryError) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.fetchCategoryError,
            })
        }

        if (nextProps.fetchSourceList && nextProps.fetchSourceList !== this.props.fetchSourceList) {
            this.setState({
                sourceList: nextProps.fetchSourceList.map(item => {
                    item.connectors = item.connectors.sort((x, y) => y.available - x.available);
                    return item;
                })
            })
        }

        if (nextProps.fetchSourceListError && nextProps.fetchSourceListError !== this.props.fetchSourceListError) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.fetchSourceListError,
            })
        }

        if (nextProps.etlInfo && nextProps.etlInfo !== this.props.etlInfo) {
            let selectedDefinitionValues = nextProps.etlInfo.definition.length > 0  && nextProps.etlInfo.definition[0].type === "agg" ? nextProps.etlInfo.definition[0].groupBy : nextProps.etlInfo.definition[0].operations
            let selectedDefinitionType = nextProps.etlInfo.definition[0].type
            if (!Object.keys(nextProps.etlInfo.definition[1]).includes("window"))
                nextProps.etlInfo.definition[1]["window"] = { enable: false }
            this.setState({
                payload: nextProps.etlInfo,
                nodeDefinition: nextProps.etlInfo.definition,
                etlInfo: nextProps.etlInfo,
                selectedDefinitionValues,
                selectedDefinitionType
            }, () => this.getMqttConnection())
        }

        if (nextProps.etlError && nextProps.etlError !== this.props.etlError) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.etlError,
            })
        }

        if (nextProps.saveEtlSuccess && nextProps.saveEtlSuccess !== this.props.saveEtlSuccess) {
            this.setState({
                modalType: "success",
                isOpen: true,
                message2: nextProps.saveEtlSuccess,
                saveSuccess: true,
            })
        }

        if (nextProps.saveEtlError && nextProps.saveEtlError !== this.props.saveEtlError) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.saveEtlError,
            })
        }

        if (nextProps.allSparksMethodsSuccess && nextProps.allSparksMethodsSuccess !== this.props.allSparksMethodsSuccess) {
            let allSparksMethods = nextProps.allSparksMethodsSuccess.map((temp) => {
                return { ...temp, alias: null, path: null }
            })
            this.setState({
                allSparksMethods
            })
        }

        if (nextProps.allSparksMethodsFailure && nextProps.allSparksMethodsFailure !== this.props.allSparksMethodsFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.allSparksMethodsFailure,
            })
        }

        if (nextProps.schemaApi && nextProps.schemaApi !== this.props.schemaApi) {
            // let pubnubKeys = localStorage.getItem('pubnubKeys')
            // if (localStorage.getItem('pubnubKeys'))
            //     pubnubKeys = JSON.parse(localStorage.getItem('pubnubKeys'))

            // let schemaApi = JSON.parse(JSON.stringify(nextProps.schemaApi))
            // let self = this
            // if ((schemaApi === "SCHEMA") && pubnubKeys) {
            //     let pubnubclient = new PubNub({
            //         subscribeKey: window.atob(pubnubKeys.subscribe),
            //         authKey: window.atob(pubnubKeys.auth)
            //     })
            //     let projectId =   localStorage.getItem('selectedProjectId') 
            //     pubnubclient.subscribe({ channels: [`${schemaApi}_${localStorage.tenant}_${projectId}_${self.state.payload.name}`] });
            //     pubnubclient.addListener({
            //         message: (payload) => {
            //             let messageData = JSON.parse(JSON.stringify(payload.message))
            //             if (messageData.error != null) {
            //                 self.setState({
            //                     schemaData: JSON.parse(messageData.schema),
            //                     modalType: "error",
            //                     isOpen: true,
            //                     message2: [messageData.error],
            //                     initialSchemaStatus : true
            //                 })
            //             }
            //             else {
            //                 self.setState({
            //                     schemaData: JSON.parse(messageData.schema),
            //                     initialSchemaStatus : true
            //                 })
            //             }

            //         }
            //     });
            // }
            // else if (this.state.mqttConnectStatus) {
                
            //     let projectId =   localStorage.getItem('selectedProjectId') 
            //     // client.subscribe(`${schemaApi}/${localStorage.tenant}/${self.state.payload.name}`, function (err) {
            //     //     if (!err) {
            //     //         client.on('message', function (schemaApi, message) {
            //     //                 self.setState({
            //     //                     debugJson: JSON.parse(message.toString())
            //     //                 })
            //     //         })
            //     //     }
            //     // })
            //     client.subscribe(`DEBUG_${localStorage.tenant}_${projectId}_${self.state.payload.name}`, function (err) {
            //         if (!err) {
            //             client.on('message', function (schemaApi, message) {
            //                 self.setState({
            //                     debugJson: JSON.parse(message.toString())
            //                 })
            //             })
            //         }
            //     })
            // }
            // else if (this.state.mqttConnectStatus) {
            //     let self = this;
            //     client.subscribe(`${schemaApi}/${localStorage.tenant}/${self.state.payload.name}`, function (err) {
            //         if (!err) {
            //             client.on('message', function (schemaApi, message) {
            //                 if (schemaApi.includes("SCHEMA")) {
            //                     let messageData = JSON.parse(message.toString())
            //                     if (messageData.error != null) {
            //                         self.setState({
            //                             schemaData: JSON.parse(messageData.schema),
            //                             modalType: "error",
            //                             isOpen: true,
            //                             message2: [messageData.error],
            //                         })
            //                     } else {
            //                         self.setState({
            //                             schemaData: JSON.parse(messageData.schema)
            //                         })
            //                     }
            //                 }

            //                 else {
            //                     self.setState({
            //                         debugJson: JSON.parse(message.toString())
            //                     })
            //                 }
            //             })
            //         }
            //     })
            // }
        }

        if (nextProps.schemaApiError && nextProps.schemaApiError !== this.props.schemaApiError) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.schemaApiError,
            })
        }

        if (nextProps.timeSeriesSchemaSuccess && nextProps.timeSeriesSchemaSuccess !== this.props.timeSeriesSchemaFailure) {
            // let schemaApi = JSON.parse(JSON.stringify(nextProps.timeSeriesSchemaSuccess))
            // let pubnubKeys = JSON.parse(localStorage.getItem('pubnubKeys'))
            // let self = this
            // if (schemaApi === "timeSeriesColumn") {
            //     client = new PubNub({
            //         subscribeKey: window.atob(pubnubKeys.subscribe),
            //         authKey: window.atob(pubnubKeys.auth)
            //     })
            //     let projectId =   localStorage.getItem('selectedProjectId')
            //     client.subscribe({ channels: [`TIMESERIES_${localStorage.tenant}_${projectId}_${self.state.payload.name}`] });
            //     client.addListener({
            //         message: (payload) => {
            //             let messageData = JSON.parse(JSON.stringify(payload.message))
            //             let sinkPayload = JSON.parse(JSON.stringify(self.state.sinkPayload))
            //             sinkPayload.timeSeries.column = messageData.schema[0]
            //             self.setState({
            //                 timeSeriesSchema: messageData.schema,
            //                 sinkPayload,
            //                 timeSeriesSchemaFlag: false
            //             })
            //         }
            //     });
            // }
            // if (this.state.mqttConnectStatus) {
            //     let self = this;
            //     client.subscribe(`${schemaApi}/${localStorage.tenant}/${self.state.payload.name}`, function (err) {
            //         if (!err) {
            //             client.on('message', function (schemaApi, message) {
            //                 let sinkPayload = JSON.parse(JSON.stringify(self.state.sinkPayload))
            //                 if(schemaApi.includes('timeSeriesColumn'))
            //                 {
            //                     let messageData = JSON.parse(message.toString())
            //                     sinkPayload.timeSeries.column = messageData.schema[0]
            //                     self.setState({timeSeriesSchema:messageData.schema, sinkPayload, timeSeriesSchemaFlag:false},()=>self.state.timeSeriesSchema)
            //                 }
            //             })
            //         }
            //     })
            // }
        }

        if (nextProps.timeSeriesSchemaFailure && nextProps.timeSeriesSchemaFailure !== this.props.timeSeriesSchemaFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.timeSeriesSchemaFailure,
            })
        }

        if (nextProps.getConnectorsSuccess && nextProps.getConnectorsSuccess !== this.props.getConnectorsSuccess) {
            let defaultCategoryList = [];
            nextProps.getConnectorsSuccess.find((val) => val.type === nextProps.getConnectorsSuccess[0].type).connectors.map((con) => {
                defaultCategoryList.push({ connector: con.connector, available: con.available })
            })
            this.setState({
                defaultList: nextProps.getConnectorsSuccess,
                defaultType: nextProps.getConnectorsSuccess[0].type,
                prevDefaultType: nextProps.getConnectorsSuccess[0].type,
                defaultCategoryList,
                prevDefaultCategoryList: defaultCategoryList,
                defaultCategory: defaultCategoryList[0].connector,
                prevDefaultCategory: defaultCategoryList[0].connector
            }, () => defaultCategoryList[0].available ? this.props.getCategoryByConnectors(this.state.defaultCategory) : '')
        }

        if (nextProps.getConnectorsFailure && nextProps.getConnectorsFailure !== this.props.getConnectorsFailure) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.getConnectorsFailure,
            })
        }

        if (nextProps.getConnectorByCategorySuccess && nextProps.getConnectorByCategorySuccess !== this.props.getConnectorByCategorySuccess) {
            let defaultConnectorList = nextProps.getConnectorByCategorySuccess.map((val) => {
                return {
                    name: val.name,
                    id: val.id,
                    sink: val.sink
                }
            })
            this.setState({
                defaultConnectorList,
                defaultConnectorFlag: false
            })
        }

        if (nextProps.getConnectorByCategoryFailure && nextProps.getConnectorByCategoryFailure !== this.props.getConnectorByCategoryFailure) {
            this.setState({
                defaultConnectorFlag: false,
                modalType: "error",
                isOpen: true,
                message2: nextProps.getConnectorByCategoryFailure,
            })
        }

    }

    getMqttConnection = () => {
        let brokerURL = window.location.origin === "http://localhost:3001" ? window.API_URL.split("//")[1].substring(0, window.API_URL.split("//")[1].length - 1) : window.location.host;
        let count = 0;
        const options = {
            host: brokerURL,
            protocol: 'wss',
            path: '/connectmqtt',
            username: '83incs',
            password: 'Hello@123',

        };

        client = MQTT.connect(options);
        let self = this;

        client.on('connect', function () {
            self.setState({
                mqttConnectStatus: true
            }, () => {
                let payload = JSON.parse(JSON.stringify(self.state.payload))
                payload.definition = payload.definition.filter(val => val.type !== "sink")
                if (self.state.payload.type === "batch") {
                    payload.definition[0].operations = []
                }
                if (self.props.match.params.id) {
                    self.props.generateSchema("SCHEMA", payload)
                }
            })
        });

        client.on('close', function (response) {
            if (response && count > 4) {
                client.end();
            }
        });

        client.on('reconnect', function () {
            count++;
        });
    }

    sourceDataChangeHandler = (event, source, connect) => {
        let payload = JSON.parse(JSON.stringify(this.state.payload))
        if (event === "type") {
            payload[event] = source === "stream" ? "stream" : "batch"
            payload.etlConfig = {
                maxOffset: "1000",
                triggerInterval: 1000,
                threshold: 10
            }
            payload.connector = {}
            this.setState({
                payload,
                connectorName: connect.displayName,
                selectedConnector: connect.connector
            }, () => { this.props.getCategory(connect.connector) })
        } else if (event === "connector") {
            let fileId = this.state.categoryList.find((val) => val.id === source)
            payload.connector = {
                id: source,
                properties: this.state.payload.type === "stream" ? {
                    topicId: ""
                } : {}
            }
            this.setState({
                payload,
                topicList: this.state.payload.type === "stream" ? this.state.categoryList.find(val => val.id === source).properties.topics : [],
                bucketDataDirs: []
                // connectorLoader: true,
            })
        } else if (event === "topicId") {
            payload.connector.properties.topicId = this.state.topicList[source].topicId
            this.setState({
                payload,
                schemaData: JSON.parse(this.state.topicList[source].schema)
            })
        } else if (event === "file") {
            payload.connector.properties.fileId = source
            this.setState({
                payload
            })
        } else if (event.includes("bucket")) {
            if (event.includes("dir")) {
                payload.connector.properties.dirId = source.dirId
                payload.connector.properties.inputPartitionKeys = JSON.parse(JSON.stringify(source.partitionBy))
                this.setState({ payload })
            }
            else {
                payload.connector.properties.bucketId = source.bucketId
                delete payload.connector.properties.dirId
                let bucketDataDirs = source.dataDirs
                payload.connector.id = connect
                this.setState({
                    payload,
                    bucketDataDirs
                })
            }
        }
    }

    addBranches = (e, id) => {
        let payload = JSON.parse(JSON.stringify(this.state.payload))
        if (id === "root") {
            let branchIndex = payload.definition.findIndex(val => val.type === "branches")
            if (branchIndex >= 0) {
                payload.definition[branchIndex].branches.push({
                    demoId: id + "_" + payload.definition[branchIndex].branches.length,
                    definition: [
                        {
                            operations: [],
                            type: "select"
                        }
                    ]
                })
                if (payload.definition.some(val => val.type === "sink"))
                    payload.definition.splice(payload.definition.findIndex(val => val.type === "sink"), 1)
            } else {
                payload.definition.push({
                    type: "branches",
                    branches: [
                        {
                            demoId: "root_0",
                            definition: [
                                {
                                    operations: [],
                                    type: "select"
                                }
                            ]
                        }
                    ]
                })
            }
        } else {
            let path = id.split("_");
            let reachedObject = payload.definition[payload.definition.findIndex(val => val.type === "branches")].branches
            let matchPath = ""
            path.map((pathVal, index) => {
                if (index > 0) {
                    matchPath += "_" + pathVal
                    reachedObject = reachedObject[reachedObject.findIndex(val => val.demoId === matchPath)]
                    if (index !== path.length - 1) {
                        reachedObject = reachedObject.definition[reachedObject.definition.findIndex(val => val.type === "branches")].branches
                    }
                } else {
                    matchPath = "root"
                }
            })
            let branchIndex = reachedObject.definition.findIndex(val => val.type === "branches")
            if (branchIndex >= 0) {
                reachedObject.definition[branchIndex].branches.push({
                    demoId: id + "_" + reachedObject.definition[branchIndex].branches.length,
                    definition: [
                        {
                            operations: [],
                            type: "select"
                        }
                    ]
                })
            } else {
                reachedObject.definition.push({
                    type: "branches",
                    branches: [
                        {
                            demoId: id + "_" + 0,
                            definition: [
                                {
                                    operations: [],
                                    type: "select"
                                }
                            ]
                        }
                    ]
                })
            }
            if (reachedObject.definition.some(val => val.type === "sink"))
                reachedObject.definition.splice(reachedObject.definition.findIndex(val => val.type === "sink"), 1)
        }

        this.setState({
            payload
        })
    }

    createTreeDefination(obj) {
        if (obj.some(val => val.type === "branches")) {
            obj[obj.findIndex(sub => sub.type === "branches")].branches.map((data, index) => {
                let dub = data.demoId.split("_")
                let replaceIndex = dub.length === 2 ? dub.length + 0 : dub.length + (dub.length) / 2
                data.demoId = setCharAt(data.demoId, 3 + parseInt(replaceIndex), index)
                if (data.definition.some(val => val.type === "branches")) {
                    data.definition[data.definition.findIndex(val => val.type === "branches")].branches.length > 0 ?
                        this.createTreeDefination(data.definition) :
                        data.definition.splice(data.definition.findIndex(val => val.type === "branches"), 1)
                }
            })
            return obj;
        }
    }

    deleteBranches = (e, id) => {
        let payload = JSON.parse(JSON.stringify(this.state.payload))
        let branchSerialIndex =  this.state.branchSerialIndex
        let selectedDefinitionValues = this.state.selectedDefinitionValues.length >= 0 ? JSON.parse(JSON.stringify(this.state.selectedDefinitionValues)) : []
        let selectedDefinitionType = this.state.selectedDefinitionType
        let selectedDefinitionIndex = this.state.selectedDefinitionIndex
        let activeNodeId = this.state.activeNodeId

        if (id === "root") {
            payload.definition.splice(payload.definition.findIndex(val => val.type === "branches"), 1)
            branchSerialIndex = "root"
            activeNodeId = "root"
            selectedDefinitionValues = payload.definition.length > 0 && payload.definition[0].type === "agg" ? payload.definition[0].groupBy : payload.definition[0].operations
            selectedDefinitionType = payload.definition[0].type
            selectedDefinitionIndex = 0

        } else {
            branchSerialIndex = ''
            let path = id.split("_")
            let matchId = ""
            let obj = payload
            path.map((val, index) => {
                if (index > 0) {
                    matchId += "_" + val
                    obj = obj.definition[obj.definition.findIndex(val => val.type === "branches")].branches
                    if (index !== path.length - 1) {
                        obj = obj[obj.findIndex(val => val.demoId === matchId)]
                    } else {
                        obj.splice(obj.findIndex(val => val.demoId === matchId), 1)
                    }
                } else {
                    matchId = "root"
                }
            })
            payload.definition = this.createTreeDefination(payload.definition);
        }
        if ((id !== "root") && (payload.definition[payload.definition.findIndex(val => val.type === "branches")].branches.length === 0)) {
            payload.definition.splice(payload.definition.findIndex(val => val.type === "branches"), 1)
        }
        this.setState({
            payload,
            branchSerialIndex,
            selectedDefinitionIndex,
            selectedDefinitionType,
            selectedDefinitionValues,
            activeNodeId
        })
    }

    nodeValueChangeHandler = (e) => {
        let payload = JSON.parse(JSON.stringify(this.state.payload))
        let nodeDefinition = JSON.parse(JSON.stringify(this.state.nodeDefinition))
        let sinkPayload = JSON.parse(JSON.stringify(this.state.sinkPayload))
        let path = e.target.id.split("_")
        let matchId = ""
        let obj = payload
        path.map((val, index) => {
            if (index > 0) {
                matchId += "_" + val
                obj = obj.definition[obj.definition.findIndex(val => val.type === "branches")].branches

                if (index !== path.length - 1) {
                    obj = obj[obj.findIndex(val => val.demoId === matchId)]
                } else {
                    nodeDefinition = obj.find(val => val.demoId === matchId).definition
                    if (e.target.value === "")
                        delete obj[obj.findIndex(val => val.demoId === matchId)].name
                    else
                        obj = obj[obj.findIndex(val => val.demoId === matchId)].name = e.target.value
                }
            } else {
                matchId = "root"
            }
        })

        nodeDefinition.map((val) => {
            if (val.type === "sink") {
                sinkPayload = val
            }
        })

        this.setState({
            payload,
            activeNodeId: e.target.id,
            nodeDefinition,
            nodeName: e.target.value,
            sinkPayload,
            branchSerialIndex : e.target.id
        })
    }

    myComponent = (data) => {
        return (
            <ul id="collapse1" className="listStyleNone hierarchyBranch collapse show">
                {data.map((m, index) => {
                    return <li className="nodeBox" key={index}>
                        <div className="nodeInputBox">
                            <input type="text" id={m.demoId} value={m.name ? m.name : ""} onFocus={this.nodeValueChangeHandler} onChange={this.nodeValueChangeHandler} className="form-control" placeholder="Branch Name" required />
                            <div className="button-group">
                                <button type="button" onClick={(e) => this.addBranches(e, m.demoId)} className="btn btn-transparent btn-transparent-primary">
                                    <i className="far fa-plus"></i>
                                </button>
                                {!m.definition.some(val => val.type === "branches") && <button type="button" disabled={this.sinkDisableHandler(m)} onClick={() => this.sinkData(m)} className="btn btn-transparent btn-transparent-success" data-toggle="modal" data-target="#sinkModal">
                                    <i className="far fa-link"></i>
                                </button>}
                                <button type="button" onClick={(e) => this.deleteBranches(e, m.demoId)} className="btn btn-transparent btn-transparent-danger">
                                    <i className="far fa-trash-alt"></i>
                                </button>
                            </div>
                        </div>
                        {m.definition.some(subVal => subVal.type === "branches") && this.myComponent(m.definition[m.definition.findIndex(valFind => valFind.type === "branches")].branches)}
                    </li>
                }
                )}
            </ul>
        );
    }

    checkingFields = (m, index, name) => {
        if (m.type && m.type === "struct") {
            return this.attributeTree(m.fields, index, name)
        } else if (m.type && m.type === "array") {
            return this.checkingFields(m.elementType, index, name)
        } else {
            return null
        }
    }

    attributeTree = (data, pos, name) => {
        return (
            <div className="collapse show" id={`attribute${pos}`}>
                <ul className="listStyleNone hierarchyBranch">
                    {data.map((m, index) => {
                        return (
                            <li className="nodeBox" key={index}>
                                <div className="nodeInputBox" data-toggle="collapse" data-target={m.type.type ? `#attribute${pos}_${index}` : ""}>
                                    <div className={m.type.type ? "nodeInputText pd-l-25" : 'nodeInputText pd-l-10'}>{m.name}
                                        <strong>{m.type.type ? Array.isArray(m.type) ? "(array)" : "(object)" : `(${m.type})`}</strong>
                                        {m.type.type &&
                                            <span className="nodeIconCollapse"><i className="fas fa-chevron-down"></i></span>
                                        }
                                        {/*{m.type.type ?*/}
                                        {/*    <span className="nodeIcon"><i className="fas fa-brackets-curly"></i></span> :*/}
                                        {/*    <span className="nodeIcon"><i className="fas fa-brackets"></i></span>*/}
                                        {/*}*/}
                                    </div> { this.state.initialSchemaStatus && <button type="button" className="btn btn-transparent btn-transparent-primary" value={`${name}.${m.name} as ${m.name}`} onClick={(e)=>this.useButtonHandler(e)}><i className="far fa-copy"></i></button>}
                                </div> 
                                {m.type.type && m.type.type === "struct" ? this.attributeTree(m.type.fields, `${pos}_${index}`, `${name}.${m.name}`) : m.type.type === "array" ?
                                    this.checkingFields(m.type.elementType, `${pos}_${index}`, `${name}.${m.name}`) : null}
                            </li>
                        )
                    }
                    )}
                </ul>
            </div>
        );
    }

    
    useButtonHandler=(e)=>{
        if(this.state.payload.type === "stream"){
            let selectedDefinitionValues = JSON.parse(JSON.stringify(this.state.selectedDefinitionValues))
            selectedDefinitionValues.push(e.currentTarget.value)
            this.setState({selectedDefinitionValues}, () =>{
                if(this.state.selectedDefinitionType === "select" || this.state.selectedDefinitionType === "filter")
                    this.configChangeHandler(this.state.selectedDefinitionValues, this.state.selectedDefinitionType, this.state.selectedDefinitionIndex)
                else
                    this.configChangeHandler(this.state.selectedDefinitionValues, this.state.selectedDefinitionType, "groupBy", this.state.selectedDefinitionIndex)
            })
        } else{
            if(this.state.branchSerialIndex.trim() !== ''){
                let selectedDefinitionValues = JSON.parse(JSON.stringify(this.state.selectedDefinitionValues))
                selectedDefinitionValues.push(e.currentTarget.value)
                this.setState({selectedDefinitionValues}, () =>{
                    if(this.state.selectedDefinitionType === "select" || this.state.selectedDefinitionType === "filter")
                        this.configChangeHandler(this.state.selectedDefinitionValues, this.state.selectedDefinitionType, this.state.selectedDefinitionIndex)
                    else
                        this.configChangeHandler(this.state.selectedDefinitionValues, this.state.selectedDefinitionType, "groupBy", this.state.selectedDefinitionIndex)
                })
            }
        }
    }

    selectValuesCheck = (operations, index, type) => {
        this.setState({selectedDefinitionIndex:index, selectedDefinitionType:type})
    }
    configChangeHandler = (e, id, agg, aggindex, index) => {
        let targetId
        if (!Array.isArray(e) && (id !== "optional")) {
            targetId = e.target.id
        }
        let payload = JSON.parse(JSON.stringify(this.state.payload));
        let schemaPayload = JSON.parse(JSON.stringify(this.state.payload));
        let nodeDefinition = JSON.parse(JSON.stringify(this.state.nodeDefinition));
        let selectedDefinitionValues = JSON.parse(JSON.stringify(this.state.selectedDefinitionValues))
        

        if (schemaPayload.definition.length > 1) {
            schemaPayload.definition = schemaPayload.definition.filter((val) => {
                if (val.type) {
                    if (val.type !== "sink") {
                        return val
                    }
                }

            })
        }
        let path = this.state.activeNodeId.split("_")
        let matchId = ""
        let obj = payload, schemaObj = schemaPayload;
        if (path.length === 1) {
            switch (id) {
                case "agg":
                    if (["window"].includes(agg)) {
                        payload.definition[aggindex].window.enable = !payload.definition[aggindex].window.enable
                        nodeDefinition[aggindex].window.enable = payload.definition[aggindex].window.enable
                        schemaPayload.definition[aggindex].window.enable = payload.definition[aggindex].window.enable
                        if (payload.definition[aggindex].window.enable) {
                            payload.definition[aggindex].window = { enable: true, windowColumn: "", windowInterval: null, slidingWindowInterval: null }
                            schemaPayload.definition[aggindex].window = { enable: true, windowColumn: "", windowInterval: null, slidingWindowInterval: null }
                            nodeDefinition[aggindex].window = { enable: true, windowColumn: "", windowInterval: null, slidingWindowInterval: null }
                        }
                        else {
                            payload.definition[aggindex].window = { enable: false }
                            schemaPayload.definition[aggindex].window = { enable: false }
                            nodeDefinition[aggindex].window = { enable: false }
                        }

                    }
                    else if (["windowColumn", "windowInterval", "slidingWindowInterval"].includes(agg)) {
                        let regex = /^([0-5]?[0-9]|60)$/i
                        if ((agg == "windowInterval") || (agg == "slidingWindowInterval")) {
                            if ((regex.test(e.target.value)) || (e.target.value == '')) {
                                payload.definition[aggindex].window[agg] = e.target.value
                                nodeDefinition[aggindex].window[agg] = e.target.value
                                schemaPayload.definition[aggindex].window[agg] = e.target.value
                            }
                        }
                        else {
                            payload.definition[aggindex].window[agg] = e.target.value
                            nodeDefinition[aggindex].window[agg] = e.target.value
                            schemaPayload.definition[aggindex].window[agg] = e.target.value
                        }
                    }
                    else if (["type", "field", "alias"].includes(agg)) {
                        nodeDefinition[index].operations[aggindex][agg] = e.target.value
                        payload.definition[index].operations[aggindex][agg] = e.target.value
                        schemaPayload.definition[index].operations[aggindex][agg] = e.target.value
                        schemaPayload.definition.splice(index + 1);
                    } else if (agg === "delete") {
                        nodeDefinition[index].operations.splice(aggindex, 1)
                        payload.definition[index].operations.splice(aggindex, 1)
                        schemaPayload.definition[index].operations.splice(aggindex, 1)
                        schemaPayload.definition.splice(index + 1);
                    } else if (agg === "add") {
                        let operationLength = payload.definition[aggindex].operations.length - 1
                        if ((payload.definition[aggindex].operations[operationLength].type.trim() !== '') && (payload.definition[aggindex].operations[operationLength].field.trim() !== '') && (payload.definition[aggindex].operations[operationLength].alias.trim() !== '')) {
                            nodeDefinition[aggindex].operations.push({ type: "", field: "", alias: "" })
                            payload.definition[aggindex].operations.push({ type: "", field: "", alias: "" })
                            schemaPayload.definition[aggindex].operations.push({ type: "", field: "", alias: "" })
                            schemaPayload.definition.splice(aggindex + 1);
                        }
                        else
                            return
                    } else {
                        selectedDefinitionValues = e
                        nodeDefinition[aggindex].groupBy = e
                        payload.definition[aggindex].groupBy = e
                        schemaPayload.definition[aggindex].groupBy = e;
                        schemaPayload.definition.splice(aggindex + 1);
                    }
                    break;
                default:
                    if (id === "optional") {
                        payload.definition[agg].operations.push(e);
                        nodeDefinition[agg].operations.push(e);
                        schemaPayload.definition[agg].operations.push(e);
                    }
                    else {
                        selectedDefinitionValues = e
                        payload.definition[agg].operations = e;
                        nodeDefinition[agg].operations = e
                        schemaPayload.definition[agg].operations = e;
                    }

                    break;
            }
        } else {
            path.map((val, subindex) => {
                if (subindex > 0) {
                    matchId += "_" + val
                    obj = obj.definition[obj.definition.findIndex(val => val.type === "branches")].branches
                    schemaObj = schemaObj.definition[schemaObj.definition.findIndex(val => val.type === "branches")]
                    schemaObj.branches = JSON.parse(JSON.stringify([schemaObj.branches.find(val => val.demoId === matchId)]))
                    schemaObj = schemaObj.branches[0]
                    if (subindex !== path.length - 1) {
                        obj = obj[obj.findIndex(val => val.demoId === matchId)]
                    } else {
                        switch (id) {
                            case "agg":
                                obj = obj[obj.findIndex(val => val.demoId === matchId)].definition
                                schemaObj = schemaObj.definition
                                if (["window"].includes(agg)) {
                                    obj.definition[aggindex].window.enable = !obj.definition[aggindex].window.enable
                                    nodeDefinition[aggindex].window.enable = obj.definition[aggindex].window.enable
                                    schemaPayload.definition[aggindex].window.enable = obj.definition[aggindex].window.enable
                                    if (obj.definition[aggindex].window.enable) {
                                        obj.definition[aggindex].window = { enable: true, windowColumn: "", windowInterval: null, slidingWindowInterval: null }
                                        schemaPayload.definition[aggindex].window = { enable: true, windowColumn: "", windowInterval: null, slidingWindowInterval: null }
                                        nodeDefinition[aggindex].window = { enable: true, windowColumn: "", windowInterval: null, slidingWindowInterval: null }
                                    }
                                    else {
                                        obj.definition[aggindex].window = { enable: false }
                                        schemaPayload.definition[aggindex].window = { enable: false }
                                        nodeDefinition[aggindex].window = { enable: false }
                                    }

                                }
                                else if (["windowColumn", "windowInterval", "slidingWindowInterval"].includes(agg)) {
                                    let regex = /^([0-5]?[0-9]|60)$/i
                                    if ((agg == "windowInterval") || (agg == "slidingWindowInterval")) {
                                        if ((regex.test(e.target.value)) || (e.target.value == '')) {
                                            obj.definition[aggindex].window[agg] = e.target.value
                                            nodeDefinition[aggindex].window[agg] = e.target.value
                                            schemaPayload.definition[aggindex].window[agg] = e.target.value
                                        }
                                    }
                                    else {
                                        obj.definition[aggindex].window[agg] = e.target.value
                                        nodeDefinition[aggindex].window[agg] = e.target.value
                                        schemaPayload.definition[aggindex].window[agg] = e.target.value
                                    }

                                }
                                else if (["type", "field", "alias"].includes(agg)) {
                                    nodeDefinition[index].operations[aggindex][agg] = e.target.value
                                    obj[index].operations[aggindex][agg] = e.target.value
                                    schemaObj[index].operations[aggindex][agg] = e.target.value
                                    schemaObj.splice(index + 1);
                                } else if (agg === "delete") {
                                    nodeDefinition[index].operations.splice(aggindex, 1)
                                    obj[index].operations.splice(aggindex, 1)
                                    schemaObj[index].operations.splice(aggindex, 1)
                                    schemaObj.splice(index + 1);
                                } else if (agg === "add") {
                                    nodeDefinition[aggindex].operations.push({ type: "", field: "", alias: "" })
                                    obj[aggindex].operations.push({ type: "", field: "", alias: "" })
                                    schemaObj[aggindex].operations.push({ type: "", field: "", alias: "" })
                                    schemaObj.splice(aggindex + 1)
                                } else {
                                    selectedDefinitionValues = e
                                    nodeDefinition[aggindex].groupBy = e
                                    obj[aggindex].groupBy = e
                                    schemaObj[aggindex].groupBy = e
                                    schemaObj.splice(aggindex + 1)
                                }
                                break;
                            default:
                                if (id === "optional") {
                                    obj[obj.findIndex(val => val.demoId === matchId)].definition[agg].operations.push(e)
                                    nodeDefinition[agg].operations.push(e)
                                    schemaObj.definition[agg].operations.push(e)
                                    schemaObj.definition.splice(agg + 1)
                                }
                                else {
                                    selectedDefinitionValues = e
                                    obj[obj.findIndex(val => val.demoId === matchId)].definition[agg].operations = e
                                    nodeDefinition[agg].operations = e
                                    schemaObj.definition[agg].operations = e
                                    schemaObj.definition.splice(agg + 1)
                                }
                                break;
                        }
                    }
                } else {
                    matchId = "root"
                }
            })
        }

        // schemaPayload.definition = schemaPayload.definition.map((val)=>{
        //     if((val.type==="select")||(val.select==="filter")){
        //         if(val.window)
        //             delete val.window
        //     }
        //     return val
        // })
        if (id === "agg") {
            if ((agg === "type") || (agg === "field") || (agg === "alias")) {
                let deleteRange = schemaPayload.definition.length - index + 1
                let pos = index + 1
                schemaPayload.definition.splice(pos, deleteRange)
            } else {
                let deleteRange = schemaPayload.definition.length - aggindex + 1
                let pos = aggindex + 1
                schemaPayload.definition.splice(pos, deleteRange)
            }
        } else {
            let deleteRange = schemaPayload.definition.length - agg + 1
            let pos = agg + 1
            schemaPayload.definition.splice(pos, deleteRange)
        }
        if (schemaPayload.type === "batch") {
            schemaPayload.connector.properties.fileId = this.state.payload.connector.properties.fileId
        }

        this.setState({
            selectedDefinitionValues,
            payload,
            nodeDefinition,
            schemaPayload,
            showWarningMessage: true,
        }, () => {
            if ((Array.isArray(e)) || (targetId == "type")) {
                this.props.generateSchema("SCHEMA", schemaPayload)
            }
        })
    }

    schemaCall = () => {
        this.props.generateSchema("SCHEMA", this.state.schemaPayload)
    }

    timeSeriesSchemaCall = () => {
        let sinkPayload = JSON.parse(JSON.stringify(this.state.sinkPayload))
        let payload = JSON.parse(JSON.stringify(this.state.payload))
        if (payload.definition.length > 1) {
            payload.definition = payload.definition.filter((val) => {
                if (val.type) {
                    if (val.type !== "sink") {
                        return val
                    }
                }
            })
        }
        if (!this.state.timeSeriesFlag) {
            this.sinkDataChangeHandler(["date", "hour", ...this.state.sinkPayload.connector.properties.partitionBy], "partitionBy")
            this.props.generateTimeSeriesSchema("timeSeriesColumn", payload)
            //this.setState({timeSeriesFlag:true}, () => this.sinkDataChangeHandler(["date", "hour", ...this.state.sinkPayload.connector.properties.partitionBy], "partitionBy"))
        }
        else {
            let partitionBy = this.state.sinkPayload.connector.properties.partitionBy
            partitionBy.splice(0, 2)
            this.sinkDataChangeHandler(partitionBy, "partitionBy")
            //this.setState({timeSeriesFlag : false}, () => this.sinkDataChangeHandler(partitionBy, "partitionBy"))
        }
        //this.setState({ timeSeriesFlag: !this.state.timeSeriesFlag, sinkPayload}, () => this.state.timeSeriesFlag ? this.props.generateTimeSeriesSchema("timeSeriesColumn", payload) : '')
    }
    sinkData = (Obj) => {
        let sinkPayload = JSON.parse(JSON.stringify(this.state.sinkPayload))
        let timeSeriesFlag = false
        let payload = JSON.parse(JSON.stringify(this.state.payload))
        let defaultCategory, defaultCategoryList = [], defaultType, defaultConnectorList = []

        if (Obj === "root") {
            sinkPayload = payload.definition.some(val => val.type === "sink") ?
                payload.definition.find(val => val.type === "sink") : {
                    type: "sink",
                    connector: {
                        id: "",
                        properties: {},
                    },
                }
        } else {
            let path = Obj.demoId.split("_")
            let matchPath = "root"
            let dataObj
            path.map((val, index) => {
                if (index > 0 && index != path.length - 1) {
                    matchPath = matchPath + "_" + val
                    dataObj = dataObj.find(value => value.demoId === matchPath).definition.find(subValue => subValue.type === "branches").branches
                } else if (index === path.length - 1) {
                    matchPath = matchPath + "_" + val
                    dataObj = dataObj.find(value => value.demoId === matchPath).definition
                    dataObj = dataObj.some(subValue => subValue.type === "sink") ? dataObj.find(subValue => subValue.type === "sink") : {
                        type: "sink",
                        connector: {
                            id: "",
                            properties: {},
                        },
                    }
                    sinkPayload = dataObj
                }
                else {
                    dataObj = payload.definition.find(value => value.type === "branches").branches
                }
            })
        }


        if (this.state.payload.type === "stream") {
            if (this.props.match.params.id && !this.state.newSinkData) {
                let sinkObj = this.state.connectorList.find((val) => val.id === this.state.etlInfo.definition.find((etl) => etl.type === "sink").connector.id)
                defaultCategory = sinkObj.category
                sinkPayload.connector.id = sinkObj.id

                this.state.defaultList.find((value) => value.connectors.find(val => {
                    if (val.connector === defaultCategory) {
                        defaultType = value.type
                        value.connectors.map((con) => defaultCategoryList.push({ connector: con.connector, available: con.available }))
                        return val
                    }
                })
                )
                this.state.connectorList.filter((val) => val.category === defaultCategory).map((cat) => {
                    if (cat.sink)
                        defaultConnectorList.push({ id: cat.id, name: cat.name, sink: cat.sink })
                    return
                })
            }
            else if (sinkPayload.connector.id.trim() !== '') {
                defaultType = this.state.defaultType
                defaultCategory = this.state.defaultCategory
                defaultCategoryList = this.state.defaultCategoryList
                defaultConnectorList = this.state.defaultConnectorList
            }
            else {
                defaultType = this.state.defaultType
                this.state.defaultList.find((val) => val.type === this.state.defaultType).connectors.map((con) => {
                    defaultCategoryList.push({ connector: con.connector, available: con.available })
                })
                defaultCategory = defaultCategoryList[0].connector
                //this.props.getCategoryByConnectors(defaultCategory)
                this.setState({defaultCategory}, () => this.props.getConnectorsForCategory())
                // this.state.connectorList.filter((val)=>val.category === defaultCategory).map((cat)=>{
                //     if(cat.sink)
                //         defaultConnectorList.push({id:cat.id,name:cat.name, sink:cat.sink})
                //     return
                // })
            }
        }
        else {
            if ((this.props.match.params.id) && (sinkPayload.connector.id.trim() !== '')) {
                let sinkObj = this.state.connectorList.find((val) => val.id === sinkPayload.connector.id)
                defaultCategory = sinkObj.category
                sinkPayload.connector.id = sinkObj.id

                this.state.defaultList.find((value) => value.connectors.find(val => {
                    if (val.connector === defaultCategory) {
                        defaultType = value.type
                        value.connectors.map((con) => defaultCategoryList.push({ connector: con.connector, available: con.available }))
                        return val
                    }
                })
                )
                this.state.connectorList.filter((val) => val.category === defaultCategory).map((cat) => {
                    if (cat.sink)
                        defaultConnectorList.push({ id: cat.id, name: cat.name, sink: cat.sink })
                    return
                })
            }
            else if (sinkPayload.connector.id.trim() !== '') {
                let sinkObj = this.state.connectorList.find((val) => val.id === sinkPayload.connector.id)
                defaultCategory = sinkObj.category
                sinkPayload.connector.id = sinkObj.id

                this.state.defaultList.find((value) => value.connectors.find(val => {
                    if (val.connector === defaultCategory) {
                        defaultType = value.type
                        value.connectors.map((con) => defaultCategoryList.push({ connector: con.connector, available: con.available }))
                        return val
                    }
                })
                )
                this.state.connectorList.filter((val) => val.category === defaultCategory).map((cat) => {
                    if (cat.sink)
                        defaultConnectorList.push({ id: cat.id, name: cat.name, sink: cat.sink })
                    return
                })
            }
            else {
                defaultType = this.state.defaultType
                this.state.defaultList.find((val) => val.type === this.state.defaultType).connectors.map((con) => {
                    defaultCategoryList.push({ connector: con.connector, available: con.available })
                })
                defaultCategory = defaultCategoryList[0].connector
                //this.props.getCategoryByConnectors(defaultCategory)
                this.setState({defaultCategory}, () => this.props.getConnectorsForCategory())
                // this.state.connectorList.filter((val)=>val.category === defaultCategory).map((cat)=>{
                //     if(cat.sink)
                //         defaultConnectorList.push({id:cat.id,name:cat.name, sink:cat.sink})
                //     return
                // })
            }
        }

        if (sinkPayload.connector.properties.partitionBy && sinkPayload.connector.properties.partitionBy.includes("hour")) {
            if (payload.definition.length > 1) {
                payload.definition = payload.definition.filter((val) => {
                    if (val.type) {
                        if (val.type !== "sink") {
                            return val
                        }
                    }
                })
            }
            timeSeriesFlag = true
            this.props.generateTimeSeriesSchema("timeSeriesColumn", payload)
        }

        if (this.props.match.params.id) {
            this.setState({
                activeNodeId: Obj === "root" ? "root" : Obj.demoId,
                sinkPayload,
                timeSeriesFlag,
                isOpenSink: true,
                defaultType,
                defaultCategory,
                defaultCategoryList,
                defaultConnectorList
            })
        }
        else {
            this.setState({
                activeNodeId: Obj === "root" ? "root" : Obj.demoId,
                sinkPayload,
                isOpenSink: true,
                defaultType,
                timeSeriesFlag,
                defaultCategory,
                defaultCategoryList,
                defaultConnectorList
            })
        }

    }

    sinkTypeCategoryHandler = (e) => {
        let defaultType = this.state.defaultType
        if (e.target.id === "type") {
            let defaultCategoryList = []
            defaultType = e.target.value
            this.state.defaultList.find((val) => val.type === e.target.value).connectors.map((con) => {
                defaultCategoryList.push({ connector: con.connector, available: con.available })
            })
            let sinkPayload = {
                type: "sink",
                connector: {
                    id: "",
                    properties: {},
                },
            }
            this.setState({ timeSeriesSchemaFlag: true, timeSeriesFlag: false, defaultCategoryList, defaultConnectorList: [], sinkPayload, defaultType: e.target.value, defaultCategory: defaultCategoryList[0].connector }, () =>{ this.props.getConnectorsForCategory()})
        } else if (e.target.id === "category") {
            let sinkPayload = {
                type: "sink",
                connector: {
                    id: "",
                    properties: {},
                },
            }
            let defaultConnectorList = []
            this.state.connectorList.filter((val) => val.category === e.target.value).map((cat) => {
                if (cat.sink)
                    defaultConnectorList.push({ id: cat.id, name: cat.name, sink: cat.sink })
                return
            })

            this.setState({
                defaultType,
                defaultConnectorList,
                defaultCategory: e.target.value,
                sinkPayload,
                collectionNameFill: true,
                connectorPropertiesFill: true,
                basePathFill: true,
                bucketIdFill: true,
                directoryNameFill: true,
                name: true
            })
        }


    }

    sinkDataChangeHandler = (e, id) => {
        let sinkPayload = JSON.parse(JSON.stringify(this.state.sinkPayload))
        //let payload = JSON.parse(JSON.stringify(this.state.payload))
        let timeSeriesFlag = this.state.timeSeriesFlag
        let regex = /^\S*$/i
        if (!e.target && !Array.isArray(e)) {
            let splitId = e.split("_")
            sinkPayload.connector.properties.index[splitId[2]].splice(splitId[4], 1)
        }
        else if (e.target && e.target.id === "id") {
            sinkPayload.connector.id = e.target.value
            let category = this.state.connectorList.find(val => val.id === e.target.value).category
            if (["KAFKA", "MQTT"].includes(category)) {
                sinkPayload.connector.properties = { name: "" }
            } else if (["S3", "HDFS"].includes(category)) {
                sinkPayload.connector.properties = {
                    name: "",
                    partitionBy: [],
                    basePath: "",
                    bucketId: "",
                    format: "json"
                }
            } else {
                sinkPayload.connector.properties = {
                    collection: "",
                    type: "insert",
                    index: [[{ "name": "", "value": -1 }]],
                    ttl: 1
                }
            }
        } else {
            if (e.target && e.target.id.includes("index")) {

                let splitId = e.target.id.split("_")

                if ((splitId[1] === "name") || (splitId[1] === "value")) {
                    sinkPayload.connector.properties.index[splitId[2]][splitId[3]][splitId[1]] = splitId[1] === "value" ? Number(e.target.value) : regex.test(e.target.value) ? e.target.value : sinkPayload.connector.properties.index[splitId[2]][splitId[3]][splitId[1]]
                }
                else {
                    // if (splitId[3] === "del"){
                    //     sinkPayload.connector.properties.index[splitId[2]].splice(splitId[4], 1)
                    // }
                    if (splitId[3] === "del") {
                        sinkPayload.connector.properties.index[splitId[2]].splice(splitId[4], 1)
                        sinkPayload.connector.properties.index.splice(splitId[2], 1)
                    }
                    else if (splitId[1] === "subList") {
                        //sinkPayload.connector.properties.index.push({ name: "", value: -1 })
                        sinkPayload.connector.properties.index[splitId[2]].push({ name: "", value: -1 })
                    }
                    else {
                        sinkPayload.connector.properties.index.push([{ name: "", value: -1 }])
                    }
                }
            }
            else if (e.target && e.target.id.includes("unit")) {
                sinkPayload.timeSeries.unit = e.target.value
            }
            else if (e.target && e.target.id.includes("column")) {
                sinkPayload.timeSeries.column = e.target.value
            }
            else {
                if (e.target && e.target.id === "type" && e.target.value === "insert")
                    delete sinkPayload.connector.properties.filterKeys
                else if (e.target && e.target.id === "type" && e.target.value === "update")
                    sinkPayload.connector.properties.filterKeys = []
                sinkPayload.connector.properties[id ? id : e.target.id] = id ? e : e.target.id === "ttl" ? Number(e.target.value) : regex.test(e.target.value) ? e.target.value : sinkPayload.connector.properties[e.target.id]
                if ((sinkPayload.connector.properties.partitionBy && (sinkPayload.connector.properties.partitionBy.includes("date") && sinkPayload.connector.properties.partitionBy.includes("hour")))) {
                    // if (payload.definition.length > 1) {
                    //     payload.definition = payload.definition.filter((val) => {
                    //         if (val.type) {
                    //             if (val.type !== "sink") {
                    //                 return val
                    //             }
                    //         }
                    //     })
                    // }
                    if (!this.state.sinkPayload.timeSeries) {
                        sinkPayload.timeSeries = {
                            "isTimeSeries": true,
                            "column": "",
                            "unit": "milliseconds",
                        }
                    }

                    timeSeriesFlag = true
                } else if ((sinkPayload.connector.properties.partitionBy && (!sinkPayload.connector.properties.partitionBy.includes("date") && !sinkPayload.connector.properties.partitionBy.includes("hour")))) {
                    timeSeriesFlag = false
                    delete sinkPayload.timeSeries
                }
            }
        }
        this.setState({
            sinkPayload,
            timeSeriesFlag,
            collectionNameFill: true,
            connectorPropertiesFill: true,
            basePathFill: true,
            bucketIdFill: true,
            directoryNameFill: true,
            name: true,
            showWarningMessage: true,
        })
    }

    submitClicked = () => {
        let payload = JSON.parse(JSON.stringify(this.state.payload))
        let nodeDefinition = JSON.parse(JSON.stringify(this.state.nodeDefinition))

        let path = this.state.activeNodeId.split("_")
        let matchId = ""
        let obj = payload

        if (path.length === 1) {
            if (payload.definition.some(val => val.type === "sink")) {
                payload.definition.splice(payload.definition.findIndex(val => val.type === "sink"), 1)
            }
            payload.definition.push(this.state.sinkPayload)
            nodeDefinition = payload.definition
        }
        else {
            path.map((val, index) => {
                if (index > 0) {
                    matchId += "_" + val
                    obj = obj.definition[obj.definition.findIndex(val => val.type === "branches")].branches
                    if (index !== path.length - 1) {
                        obj = obj[obj.findIndex(val => val.demoId === matchId)]
                    } else {
                        if (obj[obj.findIndex(val => val.demoId === matchId)].definition.some(val => val.type === "sink")) {
                            obj[obj.findIndex(val => val.demoId === matchId)].definition.splice(
                                obj[obj.findIndex(val => val.demoId === matchId)].definition.findIndex(val => val.type === "sink"), 1)
                        }

                        obj[obj.findIndex(val => val.demoId === matchId)].definition.push(this.state.sinkPayload)
                        nodeDefinition = obj[obj.findIndex(val => val.demoId === matchId)].definition
                    }
                } else {
                    matchId = "root"
                }
            })
        }

        if (nodeDefinition[nodeDefinition.length - 1].connector.properties.index) {
            if ((nodeDefinition[nodeDefinition.length - 1].connector.properties.index.map((value) => value.every((val) => val.name.trim() !== '')).every(val => val === true)) && (nodeDefinition[nodeDefinition.length - 1].connector.properties.collection.trim() !== '')) {
                this.setState({
                    payload,
                    nodeDefinition,
                    newSinkData: true,
                    collectionNameFill: true,
                    connectorPropertiesFill: true,
                    basePathFill: true,
                    bucketIdFill: true,
                    directoryNameFill: true,
                    name: true,
                    prevDefaultCategory: this.state.defaultCategory,
                    prevDefaultCategoryList: this.state.defaultCategoryList,
                    prevDefaultType: this.state.defaultType
                    // sinkPayload: {
                    //     type: "sink",
                    //     connector: {
                    //         id: "",
                    //         properties: {},
                    //     },
                    // }
                }, () => { $('#sinkModal').modal('hide'); })
            }
            else {
                let collectionNameFill = true, connectorPropertiesFill = true
                if ((nodeDefinition[nodeDefinition.length - 1].connector.properties.collection.trim() === '')) {
                    collectionNameFill = false
                }
                nodeDefinition.find((val) => val.type === "sink").connector.properties.index.map(val => {
                    val.map(obj => {
                        if (obj.name.trim() === '')
                            connectorPropertiesFill = false
                    })
                })

                this.setState({ collectionNameFill, connectorPropertiesFill })
            }
        }
        else {
            let basePathFill = true, bucketIdFill = true, directoryNameFill = true, namey = true, properties = nodeDefinition.find(val => val.type === "sink").connector.properties
            if ((properties.basePath !== undefined) && (properties.basePath.trim() === ''))
                basePathFill = false
            else {
                if (properties.basePath !== undefined) {
                    let splitValue = properties.basePath.split('/')
                    if (splitValue[splitValue.length - 1] !== this.state.payload.name) {
                        nodeDefinition.find(val => val.type === "sink").connector.properties.basePath = `${properties.basePath}/${this.state.payload.name}`
                    }
                }
            }
            if ((properties.bucketId !== undefined) && (properties.bucketId.trim() === ''))
                bucketIdFill = false

            if ((properties.name !== undefined) && (properties.name.trim() === '')) {
                directoryNameFill = false
                namey = false
            }

            if (basePathFill === true && bucketIdFill === true && directoryNameFill === true && namey === true)
                this.setState({
                    payload,
                    nodeDefinition,
                    newSinkData: true,
                    collectionNameFill: true,
                    connectorPropertiesFill: true,
                    basePathFill: true,
                    bucketIdFill: true,
                    directoryNameFill: true,
                    name: true,
                    prevDefaultCategory: this.state.defaultCategory,
                    prevDefaultCategoryList: this.state.defaultCategoryList,
                    prevDefaultType: this.state.defaultType

                    // sinkPayload: {
                    //     type: "sink",
                    //     connector: {
                    //         id: "",
                    //         properties: {},
                    //     },
                    // }
                }, () => { $('#sinkModal').modal('hide'); })
            else
                this.setState({
                    basePathFill,
                    bucketIdFill,
                    directoryNameFill,
                    name: namey
                })
        }
    }

    addDeletePayload = (status, id, index) => {
        let payload = JSON.parse(JSON.stringify(this.state.payload))
        let nodeDefinition = JSON.parse(JSON.stringify(this.state.nodeDefinition))

        let selectedDefinitionIndex =  this.state.selectedDefinitionIndex
        let selectedDefinitionType  = this.state.selectedDefinitionType
        let selectedDefinitionValues  = JSON.parse(JSON.stringify(this.state.selectedDefinitionType))

        let path = id.split("_")
        let matchId = ""
        let obj = payload
        if (path.length === 1) {
            if (status === "add") {
                payload.definition.push({ type: "select", operations: [] })
                nodeDefinition = payload.definition
                if (payload.definition.some(val => val.type === "sink")) {
                    payload.definition.push(payload.definition.find(val => val.type === "sink"))
                    payload.definition.splice(payload.definition.findIndex(val => val.type === "sink"), 1)
                }
            } else {
                payload.definition.splice(index, 1)
                nodeDefinition = payload.definition
                let tempPayload = payload.definition.filter(val => val.type !== "sink" && val.type !== "branches" )
                selectedDefinitionIndex = tempPayload.length - 1
                selectedDefinitionType = tempPayload[tempPayload.length - 1].type
                selectedDefinitionValues = selectedDefinitionType === "agg" ? tempPayload[tempPayload.length - 1].groupBy : tempPayload[tempPayload.length - 1].operations 
            }
        } else {
            path.map((val, subIndex) => {
                if (subIndex > 0) {
                    matchId += "_" + val
                    obj = obj.definition[obj.definition.findIndex(val => val.type === "branches")].branches
                    if (subIndex !== path.length - 1) {
                        obj = obj[obj.findIndex(val => val.demoId === matchId)]
                    } else {
                        if (status === "add") {
                            obj[obj.findIndex(val => val.demoId === matchId)].definition.push({ type: "select", operations: [] })
                            nodeDefinition = obj[obj.findIndex(val => val.demoId === matchId)].definition
                            if (nodeDefinition.some(val => val.type === "sink")) {
                                nodeDefinition.push(nodeDefinition.find(val => val.type === "sink"))
                                nodeDefinition.splice(nodeDefinition.findIndex(val => val.type === "sink"), 1)
                            }
                        } else {
                            obj[obj.findIndex(val => val.demoId === matchId)].definition.splice(index, 1)
                            nodeDefinition = obj[obj.findIndex(val => val.demoId === matchId)].definition
                            let tempNodeDefinition = nodeDefinition.filter(val => val.type !== "sink")
                            selectedDefinitionIndex = tempNodeDefinition.length - 1
                            selectedDefinitionType = tempNodeDefinition[selectedDefinitionIndex].type
                            selectedDefinitionValues = selectedDefinitionType === "agg" ? tempNodeDefinition[selectedDefinitionIndex].groupBy : tempNodeDefinition[selectedDefinitionIndex].operations
                        }
                    }
                } else {
                    matchId = "root"
                }
            })
        }
        // payload.definition = payload.definition.map((val)=>{
        //     if((val.type==="select")||(val.select==="filter")){
        //         if(val.window)
        //             delete val.window
        //     }
        //     return val
        // })
        // nodeDefinition = nodeDefinition.map((val)=>{
        //     if((val.type==="select")||(val.select==="filter")){
        //         if(val.window)
        //             delete val.window
        //     }
        //     return val
        // })
        this.setState({
            payload,
            nodeDefinition,
            selectedDefinitionIndex,
            selectedDefinitionType,
            selectedDefinitionValues
        })
    }

    onCloseHandler = (index) => {
        if (index !== undefined) {
            let message2 = [...this.state.message2]
            message2.splice(index, 1);
            this.setState({
                message2
            })
        } else {
            this.setState({
                isOpen: false,
                message2: '',
                modalType: "",
            }, () => {
                this.state.saveSuccess ? this.props.history.push('/etlPipeline') : null
            });
        }
    }

    etlConfigSubmitHandler = (event) => {
        event.preventDefault();
        switch (this.state.activeTab) {
            case "source":
                this.setState({ activeTab: "config" });
                break;
            case "config":
                this.setState({ activeTab: "define", functionLoader: true }, () => {
                    this.props.getFunctions(this.state.functionType)
                });
                break;
            case "define":
                let breakFlag = false
                let payload = JSON.parse(JSON.stringify(this.state.payload))
                if (this.state.payload.type === "batch") {
                    payload.definition.map((value) => {
                        if (((value.type === "select") || (value.type === "filter")) && (value.operations.length === 0)) {
                            breakFlag = true;
                        } else if ((value.type === "agg") && ((value.groupBy.length == 0) || (value.operations[value.operations.length - 1].type.trim() === '') || (value.operations[value.operations.length - 1].field.trim() === '') || (value.operations[value.operations.length - 1].alias.trim() === ''))) {
                            breakFlag = true;
                        }
                        else if (value.type === "branches") {
                            let flagValue = this.branchCheck(value.branches)
                            breakFlag = flagValue
                        }
                    })
                }
                else {
                    payload.definition.map((value) => {
                        if ((value.type === "select" || value.type === "filter") && value.operations.length === 0)
                            breakFlag = true
                        else if (value.type === "agg" && (value.groupBy.length === 0 || value.operations[value.operations.length - 1].type.trim() == '' || value.operations[value.operations.length - 1].field.trim() == '' || value.operations[value.operations.length - 1].alias.trim() == ''))
                            breakFlag = true
                    })
                }
                if (!breakFlag)
                    this.props.transformation(this.state.payload, this.props.match.params.id)
                else {
                    this.setState({
                        modalType: "error",
                        isOpen: true,
                        message2: "Please fill all the definition"
                    })
                }
                break;
        }
    }

    branchCheck = (def) => {
        let breakFlag = false
        def.map((value) => {
            value.definition.map((val) => {
                if (((val.type === "select") || (val.type === "filter")) && (val.operations.length === 0)) {
                    breakFlag = true;
                } else if ((val.type === "agg") && ((val.groupBy.length === 0) || (val.operations[val.operations.length - 1].type.trim() === '') || (val.operations[val.operations.length - 1].field.trim() === '') || (val.operations[val.operations.length - 1].alias.trim() === ''))) {
                    breakFlag = true;
                } else if (val.type === "branches") {
                    breakFlag = this.branchCheck(val.branches)
                }
            })
        })
        return breakFlag
    }

    nameChangeHandler = (event) => {
        let payload = JSON.parse(JSON.stringify(this.state.payload))
        let patch = event.target.id.split('_');
        if (patch[1]) {
            payload[patch[0]][patch[1]] = patch[1] === "maxOffset" ? event.target.value : event.target.value !== "" ? Number(event.target.value) : null
        } else {
            payload[patch[0]] = patch[0] === "name" ? event.target.value != "" ? event.target.value.match(/^[a-zA-Z_$][a-zA-Z_$0-9]*$/g) ? event.target.value : payload[patch[0]] : "" : event.target.value
        }
        this.setState({
            payload,
        })
    }

    changeInDefinition = (index, type) => {
        let payload = JSON.parse(JSON.stringify(this.state.payload))
        let nodeDefinition = JSON.parse(JSON.stringify(this.state.nodeDefinition))
        let path = this.state.activeNodeId.split("_")
        let matchId = ""
        let obj = payload
        let selectedDefinitionIndex = this.state.selectedDefinitionIndex
        let selectedDefinitionType = this.state.selectedDefinitionType
        let selectedDefinitionValues = JSON.parse(JSON.stringify(this.state.selectedDefinitionValues))
        if (path.length === 1) {
            if (type === "agg") {
                selectedDefinitionIndex = index
                selectedDefinitionType = "agg"
                selectedDefinitionValues = []
                payload.definition[index] = { type: "agg", operations: [{ type: "", field: "", alias: "" }], window: { enable: false }, groupBy: [] }
                nodeDefinition[index] = { type: "agg", operations: [{ type: "", field: "", alias: "" }], window: { enable: false }, groupBy: [] }
            } else {
                selectedDefinitionIndex = index
                selectedDefinitionType = type === "select" ? "select" : "filter"
                selectedDefinitionValues = []
                payload.definition[index] = { type: type === "select" ? "select" : "filter", operations: [] }
                nodeDefinition[index] = { type: type === "select" ? "select" : "filter", operations: [] }
            }
        } else {
            path.map((val, subindex) => {
                if (subindex > 0) {
                    matchId += "_" + val
                    obj = obj.definition[obj.definition.findIndex(val => val.type === "branches")].branches
                    if (subindex !== path.length - 1) {
                        obj = obj[obj.findIndex(val => val.demoId === matchId)]
                    } else {
                        if (type === "select") {
                            obj[obj.findIndex(val => val.demoId === matchId)].definition[index] = { type: "select", operations: [] }
                        } else if (type === "filter") {
                            obj[obj.findIndex(val => val.demoId === matchId)].definition[index] = { type: "filter", operations: [] }
                        } else {
                            obj[obj.findIndex(val => val.demoId === matchId)].definition[index] = { type: "agg", operations: [{ type: "", field: "", alias: "" }], window: { enable: false }, groupBy: [] }
                        }
                        nodeDefinition = obj[obj.findIndex(val => val.demoId === matchId)].definition
                        selectedDefinitionIndex = index
                        selectedDefinitionType = nodeDefinition[index].type
                        selectedDefinitionValues = nodeDefinition[index].type === "agg" ? nodeDefinition[index].groupBy : nodeDefinition[index].operations
                    }
                } else {
                    matchId = "root"
                }
            })
        }
        this.setState({
            nodeDefinition,
            payload,
            selectedDefinitionIndex,
            selectedDefinitionType,
            selectedDefinitionValues
        })
    }

    sinkValidator = (obj) => {
        for (var val = 0; val < obj.length; val++) {
            if (!obj[val].definition.some(subVal => subVal.type === "sink")) {
                return true;
            } else {
                if (obj[val].definition.some(subVal => subVal.type === "branches")) {
                    this.sinkValidator(obj[val].definition.find(subVal => subVal.type === "branches").branches)
                } else {
                    return false;
                }
            }
        }
    }

    changeMade = (e, index, key) => {
        let nodeDefination = JSON.parse(JSON.stringify(this.state.nodeDefinition))
        nodeDefination[index].operations[key] = e.target.value
        this.setState({
            nodeDefination
        })
    }

    definitionDisableCheck = () => {
        let operationLength
        let definitionLength
        if ((this.props.match.params.id) || (this.state.payload.definition[this.state.payload.definition.length - 1].type == "sink") || (this.state.payload.definition[this.state.payload.definition.length - 1].type == "branches")) {
            operationLength = this.state.payload.definition[this.state.payload.definition.length - 2].operations.length - 1
            definitionLength = this.state.payload.definition[this.state.payload.definition.length - 2]
        }
        else {
            operationLength = this.state.payload.definition[this.state.payload.definition.length - 1].operations.length - 1
            definitionLength = this.state.payload.definition[this.state.payload.definition.length - 1]
        }
        if ((definitionLength.groupBy.length !== 0) && (definitionLength.operations[operationLength].type.trim() !== "") && (definitionLength.operations[operationLength].field.trim() !== "") && (definitionLength.operations[operationLength].alias.trim() !== ""))
            return false
        else
            return true
    }


    disabledCheck = () => {
        if (this.state.payload.type === "stream") {
            return this.props.match.params.id ?
                this.state.payload.definition[this.state.payload.definition.length - 2].type == "agg"
                    ? this.definitionDisableCheck() : this.state.payload.definition[this.state.payload.definition.length - 2].operations.length === 0 ? true : false
                : this.state.payload.definition[this.state.payload.definition.length - 1].type == "sink" ?
                    this.state.payload.definition[this.state.payload.definition.length - 2].type == "agg" ? this.definitionDisableCheck() : this.state.payload.definition[this.state.payload.definition.length - 2].operations.length === 0 ? true : false
                    : this.state.payload.definition[this.state.payload.definition.length - 1].type == "agg" ? this.definitionDisableCheck() : this.state.payload.definition[this.state.payload.definition.length - 1].operations.length === 0 ? true : false
        } else {
            let batchPayload = this.state.nodeDefinition.filter((value) => {
                if ((value.type !== "branches") && (value.type !== "sink"))
                    return value
            })
            if ((batchPayload[batchPayload.length - 1].type === "select") || batchPayload[batchPayload.length - 1].type === "filter") {
                return batchPayload[batchPayload.length - 1].operations.length > 0 ? false : true
            }
            else {
                let aggPayload = batchPayload[batchPayload.length - 1]
                if ((aggPayload.groupBy.length > 0) && (aggPayload.operations[aggPayload.operations.length - 1].type.trim() !== '') && (aggPayload.operations[aggPayload.operations.length - 1].field.trim() !== '') && (aggPayload.operations[aggPayload.operations.length - 1].alias.trim() !== ''))
                    return false
                else
                    return true
            }
        }
    }

    definitionDeleteBox = () => {
        if (this.state.payload.type === "stream") {
            return this.props.match.params.id ? this.state.nodeDefinition.length - 1 === 1 ? true : false
                : this.state.nodeDefinition[this.state.nodeDefinition.length - 1].type === "sink"
                    ? this.state.nodeDefinition.length - 1 === 1 ? true : false : this.state.nodeDefinition.length === 1 ? true : false
        }
        else {
            return this.state.nodeDefinition.filter((value) => {
                if ((value.type !== "branches") && (value.type !== "sink"))
                    return value
            }).length === 1 ? true : false
        }
    }

    batchSchemaCall = () => {
        let payload = JSON.parse(JSON.stringify(this.state.payload))
        payload.definition = payload.definition.filter(val => val.type !== "sink")
        payload.definition[0].operations = []
        this.props.generateSchema("SCHEMA", payload)
    }

    categoryChangeHandler = (e) => {
        let sinkPayload = {
            type: "sink",
            connector: {
                id: "",
                properties: {},
            },
        }
        this.setState({ defaultCategory: e.target.value, sinkPayload, defaultConnectorFlag: true, timeSeriesFlag: false }, () => this.props.getConnectorsForCategory() )
    }

    typeDisableHandler = () => {
        // return this.state.categoryList.length>0 ? this.state.payload.type === "stream" ?
        // this.state.payload.connector === null || (this.state.payload.connector && this.state.payload.connector.properties && this.state.payload.connector.properties.topicId === "")
        // : this.state.payload.connector === null || (this.state.payload.connector && this.state.payload.connector.properties && !this.state.payload.connector.properties.fileId )
        // : true
        if (this.state.categoryList.length === 0)
            return true
        else if (this.state.payload.type === "stream") {
            if (this.state.payload.connector === null || (this.state.payload.connector && this.state.payload.connector.properties && this.state.payload.connector.properties.topicId === ""))
                return true
            else
                return false
        }
        else if (this.state.payload.type === "batch") {
            if ((this.state.selectedConnector === "EXCEL") || (this.state.selectedConnector === "CSV"))
                if (this.state.payload.connector === null || (this.state.payload.connector && this.state.payload.connector.properties && !this.state.payload.connector.properties.fileId))
                    return true
                else
                    return false
            else if (this.state.selectedConnector === "S3") {
                if (this.state.payload.connector === null || this.state.payload.connector.id.trim() === '' || !this.state.payload.connector.properties.bucketId || !this.state.payload.connector.properties.dirId)
                    return true
                else {

                    return false
                }
            }
            else if (this.state.payload.connector === null)
                return true
            else
                return false

        }
    }


    sinkCancelHandler = () => {
        this.setState({
            timeSeriesFlag: false,
            timeSeriesSchemaFlag: true,
            sinkPayload: this.state.sinkPayload,
            defaultConnectorList: this.state.defaultConnectorList,
            collectionNameFill: true,
            connectorPropertiesFill: true,
            basePathFill: true,
            bucketIdFill: true,
            directoryNameFill: true,
            defaultCategory: this.state.prevDefaultCategory,
            defaultType: this.state.prevDefaultType,
            defaultCategoryList: this.state.prevDefaultCategoryList
            // defaultCategory:'',
            // defaultType:''
        }, () => this.props.getCategoryByConnectors(this.state.defaultCategory))

        // this.setState({
        //     sinkPayload: this.state.sinkPayload,
        //     // sinkPayload: {
        //     //     type: "sink",
        //     //     connector: {
        //     //         id: "",
        //     //         properties: {}
        //     //     },
        //     // },
        //     defaultCategory: this.state.prevDefaultCategory,
        //     defaultType: this.state.prevDefaultType,
        //     defaultConnectorList: this.state.defaultConnectorList,
        //     collectionNameFill: true,
        //     connectorPropertiesFill: true,
        //     basePathFill: true,
        //     bucketIdFill: true,
        //     directoryNameFill: true,
        //     defaultCategoryList: this.state.prevDefaultCategoryList
        //     // defaultCategory:'',
        //     // defaultType:''
        // }, () => this.props.getCategoryByConnectors(this.state.defaultCategory))
    }

    onCopyHandler = () => {
        this.setState({
            modalType: "success",
            isOpen: true,
            message2: "Function copied to clipboard",
        })
    }

    debugBranchCheck = (def) => {
        let breakFlag = true
        def.every((value) => {
            value.definition.every((val, index) => {
                if ( ( ( val.type === "select" || val.type === "filter" )  && val.operations.length !== 0) || ( val.type==="agg" && val.groupBy.length !== 0) ) {
                    breakFlag = false;
                    return false
                }else if (val.type === "branches") {
                    breakFlag = this.debugBranchCheck(val.branches)
                }
                else
                    return true
            })
            return true
        })
        return breakFlag
    }

    debugStateCheck = () => {
        let debugFlag = true
        if(this.state.payload.type === "stream"){
            let nodeDefination =  JSON.parse(JSON.stringify(this.state.nodeDefinition))
            nodeDefination.length > 0 && nodeDefination.map((val)=>{
                if( ( (val.type==="select" || val.type==="filter" ) && (val.operations.length !== 0) ) || (val.type === "agg" && val.groupBy.length !== 0) ){
                    debugFlag = false
                    nodeDefination = []
                }
            })
            return debugFlag
        }
        else{
            let payload =  JSON.parse(JSON.stringify(this.state.payload))
            payload.definition.every((value, index) => {
                if (( value.type === "select" || value.type === "filter" || value.type === "agg" ) && (value.operations.length !== 0)) {
                    debugFlag = false;
                    return false
                } 
                else if (value.type === "branches") {
                    debugFlag = this.debugBranchCheck(value.branches)
                }
                return true
            })
            return debugFlag
        }
    }

    sinkDisableHandler = (m) =>{
        let debugFlag = true
        if(this.state.payload.type === "stream"){
            let nodeDefination =  JSON.parse(JSON.stringify(this.state.nodeDefinition))
            nodeDefination.length > 0 && nodeDefination.map((val)=>{
                if( ( (val.type==="select" || val.type==="filter" ) && (val.operations.length !== 0) ) || (val.type === "agg" && val.groupBy.length !== 0) ){
                    debugFlag = false
                }
            })
        } else{
            let nodeDefination =  Array.isArray(m) ? m.length > 0 && JSON.parse(JSON.stringify(m)) : m.definition.length > 0 &&  JSON.parse(JSON.stringify(m.definition))
            nodeDefination.length > 0 && nodeDefination.map((val)=>{
                if( ( (val.type==="select" || val.type==="filter" ) && (val.operations.length !== 0) ) || (val.type === "agg" && val.groupBy.length !== 0) ){
                    debugFlag = false
                }
            })
        }
        return debugFlag
    }

    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>Add/Edit ETL Pipeline</title>
                    <meta name="description" content="M83-AddOrEditTransformations" />
                </Helmet>
                <Prompt when={this.state.showWarningMessage} message='Leave site? Changes you made may not be saved.' />
                <div className="pageBreadcrumb">
                    <div className="row">
                        <div className="col-8">
                            <p>
                                <span className="previousPage" onClick={() => { this.props.history.push('/etlPipeline') }}>ETL Pipelines {this.state.transformationList.length > 0 ? '(' + this.state.transformationList.length + ')' : ''}</span>
                                <span className="active">{this.props.match.params.id ? this.state.payload.name : 'Add New'}</span>
                            </p>
                        </div>
                        <div className="col-4 text-right">
                            <div className="flex h-100 justify-content-end align-items-center">
                            </div>
                        </div>
                    </div>
                </div>

                {/*========= new add form ========= */}
                {this.props.isFetching ? <Loader /> :
                    <div className="outerBox pd-b-0 pd-t-90">
                        <ul className="nav nav-tabs stepWizardTop">
                            <li className={`nav-item ${this.state.activeTab === "source" ? "active" : ""}`}>
                                <a className="nav-link">
                                    <span className="stepWizardNumber">1</span>Source
                                </a>
                            </li>
                            <li className={`nav-item ${this.state.activeTab === "config" ? "active" : ""}`}>
                                <a className="nav-link">
                                    <span className="stepWizardNumber">2</span>Configuration
                                </a>
                            </li>
                            <li className={`nav-item ${this.state.activeTab === "define" ? "active" : ""}`}>
                                <a className="nav-link">
                                    <span className="stepWizardNumber">3</span>Definition
                                </a>
                            </li>
                        </ul>

                        <div className="tab-content pl-1 pr-1">
                            {this.state.activeTab === "config" ?
                                <div className="tab-pane fade show active" id="config">
                                    <form className="contentForm etlDefineForm" onSubmit={this.etlConfigSubmitHandler}>
                                        <div className="contentFormDetail border-r-0">
                                            <div className="contentFormHeading"><p>Configuration</p></div>
                                            <div className="flex">
                                                <div className="flex-item fx-b50 pd-r-10">
                                                    <div className="form-group">
                                                        <input type="text" name="etlName" id="name" placeholder="Name" className="form-control" required
                                                            value={this.state.payload.name} pattern="^[a-zA-Z0-9_]*$"
                                                            onChange={this.nameChangeHandler} disabled={this.props.match.params.id}
                                                        />
                                                        <label className="form-group-label">Name :<span className="requiredMark"><i className="fa fa-asterisk"></i></span></label>
                                                    </div>
                                                    <div className="form-group">
                                                        <select id="type" disabled className="form-control" required value={this.state.payload.type}>
                                                            <option value="stream">Stream</option>
                                                            <option value="batch">Batch</option>
                                                        </select>
                                                        <label className="form-group-label">Type:</label>
                                                    </div>
                                                </div>

                                                <div className="flex-item fx-b50 pd-l-10">
                                                    <div className="form-group">
                                                        <textarea type="text" rows="4" name="desc" id="description" placeholder="Description" className="form-control"
                                                            value={this.state.payload.description} onChange={this.nameChangeHandler} disabled={this.state.payload.isMl} />
                                                        <label className="form-group-label">Description :</label>
                                                    </div>
                                                </div>
                                            </div>

                                            {this.state.payload.type === "stream" &&
                                                <div className="flex">
                                                    <div className=" flex-item fx-b33 pd-r-10">
                                                        <div className="form-group ">
                                                            <input type="number" name="max-offset" id="etlConfig_maxOffset" placeholder="MaxOffset" disabled={this.state.payload.isMl}
                                                                className="form-control" required value={this.state.payload.etlConfig.maxOffset} onChange={this.nameChangeHandler}
                                                            />
                                                            <label className="form-group-label">Max Offset : (Records to be processed per batch)
                                                            <span className="requiredMark"><i className="fa fa-asterisk"></i></span>
                                                            </label>
                                                        </div>
                                                    </div>

                                                    <div className=" flex-item fx-b33 pd-l-5 pd-r-5">
                                                        <div className="form-group">
                                                            <input type="number" name="trigger-interval" id="etlConfig_triggerInterval" placeholder="TriggerInterval" disabled={this.state.payload.isMl}
                                                                className="form-control" required value={this.state.payload.etlConfig.triggerInterval} onChange={this.nameChangeHandler}
                                                            />
                                                            <label className="form-group-label">Trigger Interval : (In milliseconds)
                                                            <span className="requiredMark"><i className="fa fa-asterisk"></i></span>
                                                            </label>
                                                        </div>
                                                    </div>

                                                    <div className=" flex-item fx-b33 pd-l-10">
                                                        <div className="form-group">
                                                            <input type="number" name="threshold" id="etlConfig_threshold" placeholder="Threshold" disabled={this.state.payload.isMl}
                                                                className="form-control" required value={this.state.payload.etlConfig.threshold} onChange={this.nameChangeHandler}
                                                            />
                                                            <label className="form-group-label">Threshold : (In Seconds)
                                                            <span className="requiredMark"><i className="fa fa-asterisk"></i></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            }
                                        </div>

                                        <div className="bottomButton">
                                            {!this.props.match.params.id && <button type="button" name="button" className="btn btn-info float-left" onClick={() => this.setState({ activeTab: "source" })}>Back</button>}
                                            <button type="submit" name="button" className="btn btn-success" onClick={() => { !this.props.match.params.id && this.state.payload.type === "batch" ? this.batchSchemaCall() : '' }}>Next</button>
                                            <button type="button" name="button" onClick={() => this.props.history.push('/etlPipeline')} className="btn btn-dark">Cancel</button>
                                        </div>
                                    </form>
                                </div> :
                                this.state.activeTab === "define" ?
                                    <div className="tab-pane fade show active" id="define">
                                        <div className={this.state.etlFunctionModal ? "contentForm etlDefineForm definationCollapsed" : "contentForm etlDefineForm"}>
                                            <div className="flex">
                                                <div className={this.state.debugFlag ? "flex-item fx-b100 contentFormDetail definationDebugCollapsed" : "flex-item fx-b100 contentFormDetail"}>
                                                    <div className="definitionOuterBox flex">
                                                        {this.state.payload.type === "batch" &&
                                                            <div className="fx-b45 card panel h-100 m-0">
                                                                <div className="branchBox">
                                                       N             <ul className="hierarchyRoot listStyleNone">
                                                                        <li className="nodeBox">
                                                                            <div className="nodeInputBox">
                                                                                <input
                                                                                    type="text" value={this.state.rootNodeValue}
                                                                                    onChange={() => this.setState({
                                                                                        rootNodeValue: "Root"
                                                                                    })}
                                                                                    onFocus={() => this.setState({
                                                                                        activeNodeId: "root",
                                                                                        nodeName: "Root",
                                                                                        nodeDefinition: this.state.payload.definition,
                                                                                        branchSerialIndex : "root"
                                                                                    })} className="form-control" placeholder="Root" required />
                                                                                <div className="button-group">
                                                                                    <button type="button" data-tip data-for="childNode" onClick={(e) => this.addBranches(e, "root")} className="btn btn-transparent btn-transparent-primary">
                                                                                        <i className="far fa-plus"></i>
                                                                                    </button>
                                                                                    <ReactTooltip id="childNode" place="bottom" type="dark">
                                                                                        <div className="tooltipText"><span>Add Child Node</span></div>
                                                                                    </ReactTooltip>
                                                                                    {!this.state.payload.definition.some(val => val.type === "branches") &&
                                                                                        <React.Fragment>
                                                                                            <button type="button" disabled={this.sinkDisableHandler(this.state.nodeDefinition)} className="btn btn-transparent btn-transparent-success" data-tip data-for="sinkDatabase" onClick={() => this.sinkData("root")} data-toggle="modal" data-target="#sinkModal">
                                                                                                <i className="far fa-database"></i>
                                                                                            </button>
                                                                                            <ReactTooltip id="sinkDatabase" place="bottom" type="dark">
                                                                                                <div className="tooltipText"><span>Add Sink Point</span></div>
                                                                                            </ReactTooltip>
                                                                                        </React.Fragment>
                                                                                    }

                                                                                    {this.state.payload.definition.some(val => val.type === "branches") &&
                                                                                        <button type="button" onClick={(e) => this.deleteBranches(e, "root")} className="btn btn-transparent btn-transparent-danger">
                                                                                            <i className="far fa-trash-alt"></i>
                                                                                        </button>
                                                                                    }
                                                                                </div>
                                                                            </div>
                                                                            {this.state.payload.definition.some(val => val.type === "branches") && this.state.payload.definition[this.state.payload.definition.findIndex(val => val.type === "branches")].branches.length > 0 &&
                                                                                this.myComponent(this.state.payload.definition[this.state.payload.definition.findIndex(val => val.type === "branches")].branches)
                                                                            }
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>}

                                                        <div className={this.state.payload.type === "batch" ? "fx-b55 h-100 pd-l-15" : "fx-b100 h-100 pd-l-5"}>
                                                            <div className="definitionBody">
                                                                <div className="definitionHeader">Name :<strong>{this.state.nodeName} (Default node for each data Set)</strong>
                                                                    <div className="button-group">
                                                                        {this.state.payload.type === "stream" &&
                                                                            <React.Fragment>
                                                                                <button data-tip data-for="sinkPoint" disabled={this.sinkDisableHandler()} type="button" onClick={() => this.sinkData("root")} className="btn btn-transparent btn-transparent-success" data-toggle="modal" data-target="#sinkModal">
                                                                                    <i className="far fa-database"></i>
                                                                                </button>
                                                                                <ReactTooltip id="sinkPoint" place="left" type="dark">
                                                                                    <div className="tooltipText"><span>Add Sink Point</span></div>
                                                                                </ReactTooltip>
                                                                            </React.Fragment>
                                                                            }
                                                                            <button type="button"
                                                                                    onClick={() => this.setState({ debugFlag: true }, () => {
                                                                                        let payload = JSON.parse(JSON.stringify(this.state.payload))
                                                                                        payload.isDebug = true
                                                                                        this.props.generateSchema("DEBUG", payload)
                                                                                    })}
                                                                                    disabled={this.debugStateCheck()}
                                                                                    data-tip data-for="debugWindow"
                                                                                    className="btn btn-transparent btn-transparent-warning" data-toggle="collapse" data-target="#debugWindow">
                                                                                <i className="far fa-bug"></i>
                                                                            </button>
                                                                            <ReactTooltip id="debugWindow" place="left" type="dark">
                                                                                <div className="tooltipText"><spanleft>Debug Window</spanleft></div>
                                                                            </ReactTooltip>
                                                                            <button onClick={()=> this.setState(prevState => ({  etlFunctionModal: !prevState.etlFunctionModal }))} type="button"
                                                                                    data-tip data-for="utilities"
                                                                                    className="btn btn-transparent btn-transparent-primary">
                                                                                <i className="far fa-function"></i>
                                                                            </button>
                                                                            <ReactTooltip id="utilities" place="left" type="dark">
                                                                                <div className="tooltipText"><span>Utilities</span></div>
                                                                            </ReactTooltip>
                                                                        </div>
                                                                </div>

                                                                {this.state.nodeDefinition.map((val, index) => {
                                                                    if (["select", "filter", "agg"].includes(val.type)) {
                                                                        return <div key={index} className="definitionBox">
                                                                            <div className="button-group definitionButtonGroup">
                                                                                <button type="button" disabled={this.definitionDeleteBox()}
                                                                                    onClick={() => this.addDeletePayload("del", this.state.activeNodeId, index) } className="btn btn-transparent btn-transparent-danger">
                                                                                    <i className="fas fa-times"></i>
                                                                                </button>
                                                                            </div>
                                                                            <ul className="listStyleNone flex definitionItem">
                                                                                <li>
                                                                                    <div className="form-group m-0">
                                                                                        <label className="form-label">Query Type:</label>
                                                                                    </div>
                                                                                </li>
                                                                                <li>
                                                                                    <label className="radioLabel">
                                                                                        <span className="radioText">Select</span>
                                                                                        <input checked={val.type === "select"} name={"test" + index} onChange={() => this.changeInDefinition(index, "select")} type="radio" />
                                                                                        <span className="radioMark"></span>
                                                                                    </label>
                                                                                </li>
                                                                                <li>
                                                                                    <label className="radioLabel">
                                                                                        <span className="radioText">Aggregate</span>
                                                                                        <input checked={val.type === "agg"} name={"test" + index} onChange={() => this.changeInDefinition(index, "agg")} type="radio" />
                                                                                        <span className="radioMark"></span>
                                                                                    </label>
                                                                                </li>
                                                                                <li>
                                                                                    <label className="radioLabel">
                                                                                        <span className="radioText">Filter</span>
                                                                                        <input checked={val.type === "filter"} name={"test" + index} onChange={() => this.changeInDefinition(index, "filter")} type="radio" />
                                                                                        <span className="radioMark"></span>
                                                                                    </label>
                                                                                </li>
                                                                            </ul>
                                                                            {(val.type === "filter" || val.type === "select") && <button type="button" id="optional_select" disabled={val.operations.includes("currentTimeMillis(0) as timestamp")} onClick={() => this.configChangeHandler("currentTimeMillis(0) as timestamp", "optional", index)} className="btn btn-link timpStampBtn"><i className="far fa-plus mr-r-4"></i> Add timestamp</button>}
                                                                            {val.type === "select" && <div className="definitionSelection" onClick={ () => {  this.state.branchSerialIndex.trim()!=='' && val.operations.length === 0 ? this.setState({selectedDefinitionIndex:index, selectedDefinitionType:"select", selectedDefinitionValues : [] }, () => this.configChangeHandler([], "select", index)) :  this.selectValuesCheck(val.operations, index, "select") } }>
                                                                                <TagsInput value={val.operations} id="tag_select" onChange={(info) =>{this.configChangeHandler(info, "select", index)}} placeholder="Expression" />
                                                                            </div>}
                                                                            {val.type === "agg" &&
                                                                                <div className="definitionAggregation">
                                                                                    <div className="flex">
                                                                                        <div className="flex-item">
                                                                                            <div className="form-group" onClick = { () => this.state.branchSerialIndex.trim()!=='' && this.selectValuesCheck(val.groupBy, index, "agg") }>
                                                                                                <TagsInput value={val.groupBy} id="tag_agg" onChange={(info) => this.configChangeHandler(info, "agg", "groupBy", index)} placeholder="Group By :" />
                                                                                            </div>
                                                                                        </div>
                                                                                        <div className="flex-item fx-b100 mr-b-20">
                                                                                            <label className="checkboxLabel f-12">Apply for Window Aggregation
                                                                                            <input type="checkbox" name="window" value="window" id="window" onChange={(e) => this.configChangeHandler(e, "agg", "window", index)} checked={val.window.enable} />
                                                                                                <span className="checkmark" />
                                                                                            </label>
                                                                                        </div>
                                                                                    </div>
                                                                                    {val.window.enable && <div className="flex windowBox mr-b-20">
                                                                                        <div className="flex-item fx-b33 pd-r-7">
                                                                                            <div className="input-group">
                                                                                                <input type="text" className="form-control" placeholder="Window Column" name="column" id="windowColumn" value={this.state.payload.definition[index].window.windowColumn} onChange={(e) => this.configChangeHandler(e, "agg", "windowColumn", index)} required />
                                                                                                <div className="input-group-append">
                                                                                                    <span className="input-group-text" data-tip data-for="Column"><i className="far fa-info text-primary"></i>
                                                                                                        <ReactTooltip id="Column" place="bottom" type="dark">
                                                                                                            <div className="tooltipText"><p>Column should be timestamp type</p></div>
                                                                                                        </ReactTooltip>
                                                                                                    </span>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div className="flex-item fx-b33 pd-l-7 pd-r-7">
                                                                                            <div className="input-group" >
                                                                                                <input type="text" className="form-control" placeholder="Window Interval" name="windowInterval" id="windowInterval" value={this.state.payload.definition[index].window.windowInterval} onChange={(e) => this.configChangeHandler(e, "agg", "windowInterval", index)} required />
                                                                                                <div className="input-group-append">
                                                                                                    <span className="input-group-text">minutes</span>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div className="flex-item fx-b33 pd-l-7">
                                                                                            <div className="input-group">
                                                                                                <input type="text" className="form-control" placeholder="Sliding Window Interval" name="slidingInterval" id="slidingInterval" value={this.state.payload.definition[index].window.slidingWindowInterval} onChange={(e) => this.configChangeHandler(e, "agg", "slidingWindowInterval", index)} />
                                                                                                <div className="input-group-append">
                                                                                                    <span className="input-group-text">minutes</span>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>}
                                                                                    {val.operations.map((subVal, subindex) => (
                                                                                        <div className="flex" key={subindex}>
                                                                                            <div className="flex-item fx-b30">
                                                                                                <div className="form-group">
                                                                                                    <select className="form-control" value={subVal.type} id="type" onChange={(info) => this.configChangeHandler(info, "agg", "type", subindex, index)} required>
                                                                                                        <option value="">Select</option>
                                                                                                        <option value="sum">sum</option>
                                                                                                        <option value="min">min</option>
                                                                                                        <option value="max">max</option>
                                                                                                        <option value="count">count</option>
                                                                                                        <option value="avg">average</option>
                                                                                                        <option value="first">first</option>
                                                                                                        <option value="collect_list">collect list</option>
                                                                                                        <option value="collect_set">collect set</option>
                                                                                                    </select>
                                                                                                    <label className="form-group-label">Operation</label>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div className="flex-item fx-b30">
                                                                                                <div className="form-group">
                                                                                                    <input type="text" value={subVal.field} onChange={(info) => this.configChangeHandler(info, "agg", "field", subindex, index)} name="field" id="field" placeholder="Field" className="form-control" onBlur={() => this.schemaCall()} required />
                                                                                                    <label className="form-group-label">Field</label>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div className="flex-item fx-b30">
                                                                                                <div className="form-group">
                                                                                                    <input type="text" value={subVal.alias} onChange={(info) => this.configChangeHandler(info, "agg", "alias", subindex, index)} name="alias" id="alias" placeholder="Alias" className="form-control" onBlur={() => this.schemaCall()} required />
                                                                                                    <label className="form-group-label">Alias</label>
                                                                                                </div>
                                                                                            </div>
                                                                                            {val.operations.length > 1 &&
                                                                                                <div className="flex-item fx-b10">
                                                                                                    <div className="text-center">
                                                                                                        <button type="button" onClick={(info) => this.configChangeHandler(info, "agg", "delete", subindex, index)} className="btn btn-transparent btn-transparent-danger mt-2">
                                                                                                            <i className="far fa-trash-alt" />
                                                                                                        </button>
                                                                                                    </div>
                                                                                                </div>
                                                                                            }
                                                                                        </div>))}
                                                                                    <div className="text-center">
                                                                                        <button type="button" onClick={(info) => this.configChangeHandler(info, "agg", "add", index)} className="btn btn-link"><i className="far fa-plus"></i>Add</button>
                                                                                    </div>
                                                                                </div>}

                                                                            {val.type === "filter" && <div className="definitionSelection" onClick={() =>{ this.state.branchSerialIndex.trim()!=='' && val.operations.length === 0 ? this.setState({selectedDefinitionIndex:index, selectedDefinitionType:"filter", selectedDefinitionValues : []}, () => this.configChangeHandler([], "select", index))  :  this.selectValuesCheck(val.operations, index, "filter") }  }>
                                                                                <TagsInput value={val.operations} id="filter" onChange={(info) => this.configChangeHandler(info, "filter", index)} placeholder="Expression" />
                                                                            </div>}

                                                                        </div>
                                                                    }
                                                                })}
                                                                <div className="form-group text-right">
                                                                    <button type="button" disabled={this.disabledCheck()} onClick={() => { this.addDeletePayload("add", this.state.activeNodeId) }} className="btn btn-link"><i className="far fa-plus"></i>Add Definition</button>
                                                                </div>
                                                                {this.state.etlFunctionModal &&
                                                                    <div className="utilitiesModal">
                                                                        <div className="modal-content position-relative">
                                                                            <button className="closeButton" onClick={() => this.setState({ etlFunctionModal: false })}><i className=" fas fa-chevron-right"></i></button>
                                                                            <div className="modal-body">
                                                                                <div className="etlFunctionBox collapsed" data-toggle="collapse" data-target="#function">
                                                                                    functions   <i className=" fas fa-chevron-down"></i>
                                                                                </div>
                                                                                <div className="etlFunctionBoxItem collapse" id="function">
                                                                                    <div className="functionListOuterBox">
                                                                                        <div className="card panel m-0 h-100">
                                                                                            <div className="card-body panelBody">
                                                                                                <ul className="nav nav-tabs">
                                                                                                    <li className={`nav-link ${this.state.functionType == "v3/transformations/sparkFunctions" ? "active show" : ""}`}>
                                                                                                        <a className="nav-item" data-toggle="tab" onClick={() => this.setState({ functionType: "v3/transformations/sparkFunctions", functionLoader: true }, () => this.props.getFunctions("v3/transformations/sparkFunctions"))}>
                                                                                                            Spark Function
                                                                                                        </a>
                                                                                                    </li>
                                                                                                    <li className={`nav-link ${this.state.functionType == "v2/dynamicEtl/dynamicJSTemplate" ? "active" : ""}`}>
                                                                                                        <a onClick={() => this.setState({ functionType: "v2/dynamicEtl/dynamicJSTemplate", functionLoader: true }, () => this.props.getFunctions("v2/dynamicEtl/dynamicJSTemplate"))} className="nav-item" data-toggle="tab">
                                                                                                            Function
                                                                                                        </a>
                                                                                                    </li>
                                                                                                </ul>
                                                                                                <div className="tab-content">
                                                                                                    <div className="tab-pane fade show active">
                                                                                                        {this.state.functionLoader ?
                                                                                                            <div className="panelLoader ">
                                                                                                                <i className="fal fa-sync-alt fa-spin"></i>
                                                                                                            </div> : this.state.functionsList.length > 0 ?
                                                                                                                <ul className="functionList listStyleNone">
                                                                                                                    {/*{this.state.functionsList.map((val, index) => {
                                                                                                                        return (
                                                                                                                            <CopyToClipboard text={val.functionName} onCopy={this.onCopyHandler}>
                                                                                                                                <li key={index} data-toggle="modal" onClick={() => this.state.functionType == "v2/dynamicEtl/dynamicJSTemplate" ? this.setState({ functionData: val.originalFunction, functionName: val.functionName, showFunctionModal: true }) : ''} data-target={this.state.functionType == "v2/dynamicEtl/dynamicJSTemplate" ? "#functionModal" : ''}>
                                                                                                                                    <h5>{val.functionName}</h5><p>{val.description}</p>
                                                                                                                                    {this.state.functionType === "v3/transformations/sparkFunctions" &&
                                                                                                                                    <button className="btn btn-transparent btn-transparent-primary">
                                                                                                                                        <i className="far fa-copy"></i>
                                                                                                                                    </button>
                                                                                                                                    }
                                                                                                                                </li>
                                                                                                                            </CopyToClipboard>
                                                                                                                        )
                                                                                                                    })}*/}
                                                                                                                </ul>
                                                                                                                :
                                                                                                                <div className="contentTableEmpty h-100">
                                                                                                                    <h6>There is no data to display.</h6>
                                                                                                                    <button className="btn btn-primary" onClick={() => { this.props.history.push("/addOrEditUdf"); }}>
                                                                                                                        <i className="far fa-plus"></i>Add new
                                                                                                                    </button>
                                                                                                                </div>
                                                                                                        }
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="etlFunctionBox collapsed" data-toggle="collapse" data-target="#function1">
                                                                                    Available attributes  <i className=" fas fa-chevron-down"></i>
                                                                                <ol className="toggleView listStyleNone">
                                                                                    <li><a className={this.state.attributeView == "JSONview" ? "active" : ''} onClick={() => this.setState({ attributeView: "JSONview" })}><i className="fas fa-folder-tree"></i></a></li>
                                                                                    <li><a className={this.state.attributeView == "rawView" ? "active" : ''} onClick={() => this.setState({ attributeView: "rawView" })}><i className="fas fa-list"></i></a></li>
                                                                                </ol>
                                                                                </div>
                                                                                <div className="etlFunctionBoxItem collapse" id="function1">
                                                                                    <div className="functionListOuterBox">
                                                                                        <div className="card panel m-0 h-100">
                                                                                            {/*<div className="card-header panelHeader">*/}
                                                                                            {/*    <h5>*/}
                                                                                            {/*        */}
                                                                                            {/*    </h5>*/}
                                                                                            {/*    /!*<h5 data-toggle="modal" data-target="#functionModal" onClick={()=>this.setState({attributeView:"rawView"})}>RawView</h5>*!/*/}
                                                                                            {/*</div>*/}
                                                                                            <div className="card-body panelBody pd-t-0">
                                                                                                {this.state.attributeView === "rawView" && this.state.schemaData ?
                                                                                                    this.state.initialSchemaStatus ?<div className="branchBox">
                                                                                                        <ReactJson src={this.state.schemaData.fields} />
                                                                                                    </div> : ''
                                                                                                    :
                                                                                                    this.state.initialSchemaStatus && this.state.schemaData && this.state.schemaData.fields && this.state.schemaData.fields.length > 0 &&
                                                                                                    <div className="branchBox">
                                                                                                        <ul className="hierarchyRoot listStyleNone m-0">
                                                                                                            {this.state.schemaData.fields.map((value, index) => {
                                                                                                                    return <li className="nodeBox" key={index}>
                                                                                                                        <div className="nodeInputBox position-relative" >
                                                                                                                            <div className={value.type.type ? "nodeInputText pd-l-25" : 'nodeInputText pd-l-10'} data-toggle="collapse" data-target={value.type.type ? `#attribute${index}` : ""}>{value.name}
                                                                                                                                <strong>{value.type.type ? Array.isArray(value.type) ? "(array)" : "(object)" : `(${value.type})`}</strong>
                                                                                                                                {value.type.type &&
                                                                                                                                <span className="nodeIconCollapse"><i className="fas fa-chevron-down"></i></span>
                                                                                                                                }
                                                                                                                            </div> {this.state.initialSchemaStatus && <button type="button" className="btn btn-transparent btn-transparent-primary" value={`${value.name} as ${value.name}`} onClick={(e)=>this.useButtonHandler(e)} ><i className="far fa-copy"></i></button>}
                                                                                                                        </div>
                                                                                                                        {value.type.type && value.type.type === "struct" ? this.attributeTree(value.type.fields, index, value.name) : value.type.type === "array" ? this.checkingFields(value.type.elementType, index, value.name) : null}
                                                                                                                    </li>
                                                                                                                }
                                                                                                            )}
                                                                                                        </ul>
                                                                                                    </div>
                                                                                                }
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                               
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                }

                                                            </div>

                                                        </div>

                                                    </div>
                                                    {this.state.debugFlag &&
                                                        <div className={this.state.etlFunctionModal ? "debugBlock" : "debugBlock debugBlockExpand"}>
                                                            <div className="debugWindow">
                                                                <button type="button" className="btn btn-transparent"
                                                                    onClick={() => this.setState({
                                                                        debugFlag: false,
                                                                        debugJson: {}
                                                                    })}><i className="far fa-times"></i></button>
                                                                <div className="h-100 overflow-y-auto">
                                                                    {Object.keys(this.state.debugJson).length > 0 ?
                                                                        /*<div className="table-responsive" dangerouslySetInnerHTML={{ __html: jsonToTableHtmlString(this.state.debugJson.data)}}/>*/
                                                                        null
                                                                         :
                                                                        <div className="panelLoader">
                                                                            <i className="fal fa-sync-alt fa-spin"></i>
                                                                        </div>
                                                                    }
                                                                </div>
                                                            </div>
                                                        </div>
                                                    }
                                                </div>

                                            </div>

                                            <div className="bottomButton">
                                                <button type="button" className="btn btn-info float-left" onClick={() => this.setState({ activeTab: "config" })}>Back</button>
                                                <button type="button" onClick={(e) => this.etlConfigSubmitHandler(e)} className="btn btn-success">{this.props.match.params.id ? "update" : "Save"}</button>
                                                <button type="button" className="btn btn-dark" onClick={() => this.props.history.push('/etlPipeline')}>Cancel</button>
                                            </div>
                                        </div>
                                    </div>
                                    : this.state.activeTab === "source" ?
                                        <div className="tab-pane fade show active" id="source">
                                            <form onSubmit={this.etlConfigSubmitHandler}>
                                                <div className="flex sourceBox">
                                                    {this.state.payload.type === "" && this.state.sourceList.map((source, index) =>
                                                        <div className="flex-item fx-b20" key={index}>
                                                            <p className="sourceHeader">{source.displayName}</p>
                                                            <div className="sourceBody">
                                                                <ul className="sourceList listStyleNone">
                                                                    {source.connectors.map((connect, index) =>
                                                                        connect.connector === "HTTP" ? null :
                                                                            (<li key={index} id="type" onClick={() => this.sourceDataChangeHandler("type", source.type, connect)} style={!connect.available ? { pointerEvents: "none" } : {}}>
                                                                                <div className="sourceListImage">
                                                                                    {/*<img src={`https://content.iot83.com/m83/dataConnectors/${connect.imageName}`} />*/}
                                                                                </div>
                                                                                <p>{connect.displayName}</p>
                                                                                {!connect.available &&
                                                                                    <div className="comingSoonBox">
                                                                                    </div>
                                                                                }
                                                                            </li>)
                                                                    )}
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    )}
                                                </div>

                                                {this.state.payload.type != "" ?
                                                    <React.Fragment>
                                                        <div className="sourceBox">
                                                            <p className="sourceHeader">
                                                                <span className="previousPage" onClick={() => {
                                                                    let payload = JSON.parse(JSON.stringify(this.state.payload))
                                                                    payload.type = ""
                                                                    this.setState({
                                                                        payload,
                                                                        topicList: []
                                                                    })
                                                                }}>Stream</span>
                                                                <span className="active" >{this.state.connectorName}</span>
                                                            </p>
                                                            <div className="sourceBody">
                                                                <div className="contentTableBox w-100 m-0">
                                                                    <table className="table">
                                                                        <thead>
                                                                            <tr>
                                                                                <th width="5%"></th>
                                                                                <th width="25%">Connector Name</th>
                                                                                <th width="20%">Connector Type</th>
                                                                                <th width="50%">Description</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            {this.state.categoryList.length > 0 ?
                                                                                this.state.categoryList.filter(temp => temp.source || (temp.source && temp.sink)).map((val, index) =>
                                                                                    <React.Fragment key={index}>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <label
                                                                                                    className="radioLabel">
                                                                                                    <input type="radio"
                                                                                                        checked={this.state.payload.connector && this.state.payload.connector.id === val.id}
                                                                                                        onChange={() => this.sourceDataChangeHandler("connector", val.id)}
                                                                                                        name="connector" />
                                                                                                    <span className="radioMark" />
                                                                                                </label>
                                                                                            </td>
                                                                                            <td>{val.name}</td>
                                                                                            <td className="text-orange">{val.source && 'Source'}</td>
                                                                                            <td>{val.description}</td>
                                                                                        </tr>
                                                                                        {this.state.payload.connector.id === val.id && this.state.topicList.length > 0 &&
                                                                                            <tr>
                                                                                                <td colSpan="4">
                                                                                                    <div className="contentTableBox m-0 p-2">
                                                                                                        {this.state.topicList.map((val, index) =>
                                                                                                            <div className="etlTopicList" key={index}>
                                                                                                                <label className="radioLabel m-0">
                                                                                                                    <input
                                                                                                                        type="radio" id="topicId" name="typeId"
                                                                                                                        checked={this.state.payload.connector && this.state.payload.connector.properties.topicId === val.topicId}
                                                                                                                        onChange={() => this.sourceDataChangeHandler("topicId", index)}
                                                                                                                    />
                                                                                                                    <span className="radioMark" />
                                                                                                                </label>
                                                                                                                <p><strong>Topic Name :</strong> {val.name}</p>
                                                                                                            </div>
                                                                                                        )}
                                                                                                    </div>
                                                                                                </td>
                                                                                            </tr>
                                                                                        }
                                                                                        {
                                                                                            val.category === "S3" ? val.properties.buckets &&
                                                                                                <tr>
                                                                                                    <td colSpan="4">
                                                                                                        <div className="contentTableBox m-0 p-2">
                                                                                                            <table className="table">
                                                                                                                <thead>
                                                                                                                    <tr>
                                                                                                                        <th width="5%"></th>
                                                                                                                        <th width="25%">Bucket Name</th>
                                                                                                                        <th width="20%">Bucket Id</th>
                                                                                                                    </tr>
                                                                                                                </thead>
                                                                                                                <tbody>
                                                                                                                    {val.properties.buckets.length > 0 && val.properties.buckets.map((bck) =>
                                                                                                                        <React.Fragment>
                                                                                                                            <tr>
                                                                                                                                <td>
                                                                                                                                    <label className="radioLabel">
                                                                                                                                        <input type="radio" name="bucket"
                                                                                                                                            onChange={() => this.sourceDataChangeHandler("bucket", bck, val.id)}
                                                                                                                                            checked={this.state.payload.connector.properties.bucketId === bck.bucketId}
                                                                                                                                        />
                                                                                                                                        <span className="radioMark" />
                                                                                                                                    </label></td>
                                                                                                                                <td>{bck.name}</td>
                                                                                                                                <td>{bck.bucketId}</td>
                                                                                                                            </tr>
                                                                                                                            {this.state.payload.connector.properties.bucketId === bck.bucketId &&
                                                                                                                                <tr>
                                                                                                                                    <td colSpan="4">
                                                                                                                                        <div className="contentTableBox m-0 p-2">
                                                                                                                                            <table className="table">
                                                                                                                                                <thead>
                                                                                                                                                    <tr>
                                                                                                                                                        <th width="5%"></th>
                                                                                                                                                        <th width="25%">Name</th>
                                                                                                                                                        <th width="20%">Base Path</th>
                                                                                                                                                        <th width="20%">Format</th>
                                                                                                                                                        <th width="20%">DirId</th>
                                                                                                                                                    </tr>
                                                                                                                                                </thead>
                                                                                                                                                <tbody>
                                                                                                                                                    {this.state.bucketDataDirs.length > 0 && this.state.bucketDataDirs.map((bck) =>
                                                                                                                                                        <tr>
                                                                                                                                                            <td>
                                                                                                                                                                <label className="radioLabel">
                                                                                                                                                                    <input type="radio" name="bucket_dir"
                                                                                                                                                                        onChange={() => this.sourceDataChangeHandler("bucket_dir", bck)}
                                                                                                                                                                        checked={this.state.payload.connector.properties.dirId === bck.dirId}
                                                                                                                                                                    />
                                                                                                                                                                    <span className="radioMark" />
                                                                                                                                                                </label>
                                                                                                                                                            </td>
                                                                                                                                                            <td>{bck.name}</td>
                                                                                                                                                            <td>{bck.basePath}</td>
                                                                                                                                                            <td>{bck.format}</td>
                                                                                                                                                            <td>{bck.dirId}</td>
                                                                                                                                                        </tr>
                                                                                                                                                    )}
                                                                                                                                                </tbody>
                                                                                                                                            </table>
                                                                                                                                        </div>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            }
                                                                                                                        </React.Fragment>
                                                                                                                    )}
                                                                                                                </tbody>
                                                                                                            </table>
                                                                                                        </div>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                :
                                                                                                val.properties.fileList && this.state.payload.connector.id === val.id &&
                                                                                                <tr>
                                                                                                    <td colSpan="4">
                                                                                                        <div className="contentTableBox m-0 p-2">
                                                                                                            <table className="table">
                                                                                                                <thead>
                                                                                                                    <tr>
                                                                                                                        <th width="5%"></th>
                                                                                                                        <th width="25%">File Name</th>
                                                                                                                        <th width="20%">Sheet Name</th>
                                                                                                                        {/* <th width="50%">File Path</th> */}
                                                                                                                    </tr>
                                                                                                                </thead>
                                                                                                                <tbody>
                                                                                                                    {
                                                                                                                        val.properties.fileList.length > 0 && val.properties.fileList.map((fil) =>
                                                                                                                            <tr>
                                                                                                                                <td>
                                                                                                                                    <label className="radioLabel">
                                                                                                                                        <input type="radio" name="file"
                                                                                                                                            checked={this.state.selectedConnector === "CSV" ?
                                                                                                                                                this.state.payload.connector.properties.fileId && this.state.payload.connector.properties.fileId === fil.filePath
                                                                                                                                                : this.state.payload.connector.properties.fileId && this.state.payload.connector.properties.fileId === fil.fileId
                                                                                                                                            }
                                                                                                                                            onChange={() => this.sourceDataChangeHandler("file", this.state.selectedConnector === "CSV" ? fil.filePath : fil.fileId)}
                                                                                                                                        />
                                                                                                                                        <span className="radioMark" />
                                                                                                                                    </label></td>
                                                                                                                                <td>{fil.fileName}</td>
                                                                                                                                <td>{fil.sheetName}</td>
                                                                                                                                {/* <td>{fil.filePath}</td> */}
                                                                                                                            </tr>)
                                                                                                                    }
                                                                                                                </tbody>
                                                                                                            </table>
                                                                                                        </div>
                                                                                                    </td>
                                                                                                </tr>
                                                                                        }
                                                                                        {/* {this.state.bucketDataDirs.length > 0 ?
                                                                                            <tr>
                                                                                                <td colSpan="4">
                                                                                                    <div className="contentTableBox m-0 p-2">
                                                                                                        <table className="table">
                                                                                                            <thead>
                                                                                                                <tr>
                                                                                                                    <th width="5%"></th>
                                                                                                                    <th width="25%">Name</th>
                                                                                                                    <th width="20%">Base Path</th>
                                                                                                                    <th width="20%">Format</th>
                                                                                                                    <th width="20%">DirId</th>
                                                                                                                </tr>
                                                                                                            </thead>
                                                                                                            <tbody>
                                                                                                                {this.state.bucketDataDirs.length > 0 && this.state.bucketDataDirs.map((bck) =>
                                                                                                                    <tr>
                                                                                                                        <td>
                                                                                                                            <label className="radioLabel">
                                                                                                                                <input type="radio" name="bucket_dir"
                                                                                                                                    onChange={() => this.sourceDataChangeHandler("bucket_dir", bck)}
                                                                                                                                />
                                                                                                                                <span className="radioMark" />
                                                                                                                            </label>
                                                                                                                        </td>
                                                                                                                        <td>{bck.name}</td>
                                                                                                                        <td>{bck.basePath}</td>
                                                                                                                        <td>{bck.format}</td>
                                                                                                                        <td>{bck.dirId}</td>
                                                                                                                    </tr>
                                                                                                                )}
                                                                                                            </tbody>
                                                                                                        </table>
                                                                                                    </div>
                                                                                                </td>
                                                                                            </tr>
                                                                                            : ''} */}
                                                                                    </React.Fragment>
                                                                                ) :
                                                                                <tr>
                                                                                    <td colSpan="4">
                                                                                        <div className="contentTableEmpty">
                                                                                            <h6>There is no data to display.</h6>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            }
                                                                        </tbody>
                                                                    </table>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </React.Fragment>
                                                    : null
                                                }

                                                <div className="bottomButton">
                                                    {this.state.payload.type != "" && <button type="submit" disabled={this.typeDisableHandler()} className="btn btn-success">Next</button>}
                                                    <button type="button" className="btn btn-dark" onClick={() => this.props.history.push('/etlPipeline')}>Cancel</button>
                                                </div>
                                            </form>
                                        </div> :
                                        null
                            }
                        </div>
                    </div>
                }

                {/* sink form */}
                <div className="modal fade sinkModalBox" role="dialog" id="sinkModal">
                    <div className="modal-dialog modal-lg modal-dialog-centered">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h6 className="modal-title">Add Sink Point
                                    <button className="close" onClick={() => this.sinkCancelHandler()} data-dismiss="modal"><i className="far fa-times"></i></button>
                                </h6>
                            </div>
                            <div className="modal-body">
                                <div className="contentForm">
                                    <div className="flex">
                                        <div className="form-group flex-item fx-b33 pd-r-10">
                                            <select className="form-control text-capitalize" id="type" required value={this.state.defaultType} onChange={(e) => this.sinkTypeCategoryHandler(e)} >
                                                <option value="" disabled>Select</option>
                                                {this.state.defaultList.map((val, index) =>
                                                    <option key={index} id={index} value={val.type}>{val.displayName}</option>
                                                )}
                                            </select>
                                            <label className="form-group-label">Type :</label>
                                        </div>
                                        <div className="form-group flex-item fx-b33 pd-l-5 pd-r-5">
                                            <select className="form-control text-capitalize" id="category" required value={this.state.defaultCategory} onChange={(e) => this.categoryChangeHandler(e)}>
                                                <option value="" disabled>Select</option>
                                                {this.state.defaultCategoryList.map((val, index) => <option key={index} id={index} value={val.connector} disabled={!val.available}>{val.connector}</option>
                                                )}
                                            </select>
                                            <label className="form-group-label">Category :</label>
                                        </div>
                                        <div className="form-group flex-item fx-b33 pd-l-10">
                                            {
                                                !this.state.defaultConnectorFlag ? <select className="form-control" id="id" onChange={this.sinkDataChangeHandler} value={this.state.sinkPayload.connector.id} required >
                                                    <option value="" disabled>Select</option>
                                                    {this.state.defaultConnectorList.map((val, index) => <option value={val.id} key={index} disabled={!val.sink}>{val.name}</option>
                                                    )}
                                                </select> :
                                                    <div className="panelLoader">
                                                        <i className="fal fa-sync-alt fa-spin"></i>
                                                    </div>
                                            }
                                            <label className="form-group-label">Connector :<span className="requiredMark"><i className="fa fa-asterisk"></i></span></label>
                                        </div>
                                    </div>

                                    {/* MQTT FORM */}
                                    {this.state.sinkPayload.connector.id !== "" && ["KAFKA", "MQTT"].includes(this.state.connectorList.find(val => val.id === this.state.sinkPayload.connector.id).category) &&
                                        <div className="form-group">
                                            <input type="text" id="name" className="form-control" placeholder="TopicName" required onChange={this.sinkDataChangeHandler} value={this.state.sinkPayload.connector.properties.name} />
                                            <label className="form-group-label">Topic Name :<span className="requiredMark"><i className="fa fa-asterisk"></i></span></label>
                                            {!this.state.name && this.state.sinkPayload.connector.properties.name.trim() === '' ? <p className="formErrorMessage"><i className="fas fa-exclamation-triangle"></i>Please fill the name.</p> : ''}
                                        </div>
                                    }
                                    {/* END MQTT FORM */}

                                    {/* MONGODB FORM */}
                                    {this.state.sinkPayload.connector.id !== "" && this.state.connectorList.find(val => val.id === this.state.sinkPayload.connector.id).category === "MONGODB" &&
                                        <React.Fragment>
                                            <div className="form-group">
                                                <input type="text" id="collection" name="collectionName" className="form-control" placeholde="CollectionName" onChange={this.sinkDataChangeHandler} value={this.state.sinkPayload.connector.properties.collection} disabled = {this.props.match.params.id && this.state.sinkPayload.connector.properties.collection.includes(this.state.sinkCollectionId) } />
                                                <label className="form-group-label">Collection Name :<span className="requiredMark"><i className="fa fa-asterisk"></i></span></label>
                                                {!this.state.collectionNameFill && this.state.sinkPayload.connector.properties.collection.trim() === '' ? <p className="formErrorMessage"><i className="fas fa-exclamation-triangle"></i>Please fill the collection name.</p> : ''}
                                            </div>

                                            <div className="form-group">
                                                <div className="flex">
                                                    <div className="form-group-left-label">
                                                        <label className="form-label">Operation Type:</label>
                                                    </div>
                                                    <div className="form-group-right-label">
                                                        <div className="flex">
                                                            <div className="flex-item fx-b50">
                                                                <label className="radioLabel">Insert
                                                                    <input
                                                                        checked={this.state.sinkPayload.connector.properties.type === "insert"}
                                                                        onChange={this.sinkDataChangeHandler}
                                                                        id="type"
                                                                        type="radio"
                                                                        value="insert"
                                                                        name="connectionType"
                                                                    />
                                                                    <span className="radioMark" />
                                                                </label>
                                                            </div>
                                                            <div className="flex-item fx-b50">
                                                                <label className="radioLabel m-0">Update
                                                                    <input
                                                                        checked={this.state.sinkPayload.connector.properties.type === "update"}
                                                                        onChange={this.sinkDataChangeHandler}
                                                                        id="type"
                                                                        type="radio"
                                                                        value="update"
                                                                        name="connectionType"
                                                                    />
                                                                    <span className="radioMark" />
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            {this.state.sinkPayload.connector.id !== "" && this.state.sinkPayload.connector.properties.type === "update" && this.state.connectorList.find(val => val.id === this.state.sinkPayload.connector.id).category === "MONGODB" &&
                                                <div className="form-group fx-b100">
                                                    <TagsInput value={this.state.sinkPayload.connector.properties.filterKeys} id="filterKeys" onChange={(e) => this.sinkDataChangeHandler(e, "filterKeys")} inputProps={{ "placeholder": 'Filter Keys' }} />
                                                </div>
                                            }

                                            <div className="form-group flex-item fx-b100">
                                                <input type="number" id="ttl" name="ttl" className="form-control" placeholder="TTL" value={this.state.sinkPayload.connector.properties.ttl} onChange={this.sinkDataChangeHandler} />
                                                <label className="form-group-label">Time To Live : (in days)<span className="requiredMark"><i className="fa fa-asterisk"></i></span></label>
                                            </div>
                                            <p className={!this.state.connectorPropertiesFill ? 'mongoIndexListErrorMsg' : `mongoIndexListErrorMsg visibilityHidden`}><i className="fas fa-exclamation-triangle"></i> Please fill all the properties.</p>
                                            <label className="form-group-label">Index for Collection<span className="requiredMark"></span></label>
                                            {this.state.sinkPayload.connector.properties.index.map((val, index) => (
                                                <div className="mongoIndexList" key={index}>
                                                    <button className="close" id={"index_list_" + index + "_del_"} disabled={this.state.sinkPayload.connector.properties.index.length === 1 ? true : false} onClick={(e) => this.sinkDataChangeHandler(e)}>&times;</button>
                                                    {val.map((value, subIndex) => {
                                                        return <div className="flex" key={subIndex}>
                                                            <div className="form-group flex-item fx-b45 pd-r-10">
                                                                <input type="text" id={`index_name_${index}_${subIndex}`} name="name" className="form-control" placeholder="Name" onChange={this.sinkDataChangeHandler} value={value.name} />
                                                                <label className="form-group-label">Name :<span className="requiredMark"><i className="fa fa-asterisk"></i></span></label>
                                                            </div>
                                                            <div className="form-group flex-item fx-b45 pd-l-5 pd-r-5">
                                                                {/* <input type="number"  name="value" className="form-control" placeholder="Value" onChange={this.sinkDataChangeHandler} value={value.value} /> */}

                                                                <select name="deviceType" id={`index_value_${index}_${subIndex}`} className="form-control" required value={value.value} onChange={this.sinkDataChangeHandler}>
                                                                    <option value={-1}>Descending</option>
                                                                    <option value={1}>Ascending</option>
                                                                </select>

                                                                <label className="form-group-label">Value :<span className="requiredMark"><i className="fa fa-asterisk"></i></span></label>



                                                            </div>
                                                            <div className="form-group flex-item fx-b5 pd-t-7 pd-l-10">
                                                                <button type="button" className="btn btn-transparent btn-transparent-danger" id={"index_list_" + index + "_del_" + subIndex} onClick={(e) => this.sinkDataChangeHandler(`index_list_${index}_del_${subIndex}`)} disabled={val.length > 1 ? false : true} >
                                                                    <i className="far fa-trash-alt" ></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    }
                                                    )
                                                    }
                                                    <div className="text-center">
                                                        <button className="btn btn-link mt-1" id={`index_subList_${index}_add`} onClick={(e) => this.sinkDataChangeHandler(e)} ><i className="far fa-plus mr-r-5" ></i>Add More</button>
                                                    </div>
                                                </div>
                                            ))}

                                            <div className="text-right">
                                                <button id={"index_list_0_add"} onClick={(e) => this.sinkDataChangeHandler(e)} className="btn btn-link"> <i className="far fa-plus mr-r-5"></i>Add new</button>
                                            </div>
                                        </React.Fragment>
                                    }
                                    {/* END MONGODB FORM */}

                                    {/* S3 and HDFS FORM */}
                                    {this.state.sinkPayload.connector.id !== "" && ["S3", "HDFS"].includes(this.state.connectorList.find(val => val.id === this.state.sinkPayload.connector.id).category) &&
                                        <React.Fragment>
                                            <div className="flex">
                                                <div className="form-group flex-item fx-b50 pd-r-7">
                                                    <select className="form-control" id="bucketId" onChange={this.sinkDataChangeHandler} value={this.state.sinkPayload.connector.properties.bucketId}>
                                                        <option value="">Select</option>
                                                        {this.state.connectorList.find(val => val.category === "S3").properties.buckets.map((val, index) =>
                                                            <option value={val.bucketId} key={index}>{val.name}</option>
                                                        )}
                                                    </select>
                                                    <label className="form-group-label">Bucket Name :<span className="requiredMark"><i className="fa fa-asterisk"></i></span></label>
                                                    {!this.state.bucketIdFill && this.state.sinkPayload.connector.properties.bucketId.trim() === '' ? <p className="formErrorMessage"><i className="fas fa-exclamation-triangle"></i>Please fill the bucket name.</p> : ''}
                                                </div>

                                                <div className="form-group flex-item fx-b50 pd-l-7">
                                                    <input type="text" className="form-control" id="name" name="dirName" placeholder="DirName" onChange={this.sinkDataChangeHandler} value={this.state.sinkPayload.connector.properties.name} />
                                                    <label className="form-group-label">Directory Name :<span className="requiredMark"><i className="fa fa-asterisk"></i></span></label>
                                                    {!this.state.directoryNameFill && this.state.sinkPayload.connector.properties.name.trim() === '' ? <p className="formErrorMessage"><i className="fas fa-exclamation-triangle"></i>Please fill the directory name.</p> : ''}
                                                </div>

                                                <div className="form-group flex-item fx-b50 pd-r-7">
                                                    <input type="text" id="basePath" name="basePath" className="form-control" placeholder="BasePath" onChange={this.sinkDataChangeHandler} value={this.state.sinkPayload.connector.properties.basePath} />
                                                    <label className="form-group-label">Base Path :<span className="requiredMark"><i className="fa fa-asterisk"></i></span></label>
                                                    {!this.state.basePathFill && this.state.sinkPayload.connector.properties.basePath.trim() === '' ? <p className="formErrorMessage"><i className="fas fa-exclamation-triangle"></i>Please fill the base path.</p> : ''}
                                                </div>

                                                <div className="form-group flex-item fx-b50 pd-l-7">
                                                    <select className="form-control" id="format" onChange={this.sinkDataChangeHandler} value={this.state.sinkPayload.connector.properties.format}>
                                                        <option value="json">JSON</option>
                                                        <option value="csv">CSV</option>
                                                        <option value="parquet">Parquet</option>
                                                    </select>
                                                    <label className="form-group-label">Format :<span className="requiredMark"><i className="fa fa-asterisk"></i></span></label>
                                                </div>
                                                <div className="flex-item fx-b100 mr-b-20">
                                                    <label className="checkboxLabel f-12">Add Time Series
                                                        <input type="checkbox" checked={this.state.timeSeriesFlag} name="timeSeries" value="timeSeries" id="timeSeries" onClick={() => this.timeSeriesSchemaCall()} />
                                                        <span className="checkmark" />
                                                    </label>
                                                </div>
                                                {
                                                    this.state.timeSeriesFlag ?
                                                        <React.Fragment>
                                                            <div className="form-group flex-item fx-b50 pd-l-7">
                                                                <select className="form-control" id="unit" onChange={this.sinkDataChangeHandler} value={this.state.sinkPayload.timeSeries.unit}>
                                                                    <option value="milliseconds">MilliSeconds</option>
                                                                    <option value="seconds">Seconds</option>
                                                                </select>
                                                                <label className="form-group-label">Unit :<span className="requiredMark"><i className="fa fa-asterisk"></i></span></label>
                                                            </div>
                                                            <div className="form-group flex-item fx-b50 pd-l-7">
                                                                {!this.state.timeSeriesSchemaFlag ?
                                                                    <select className="form-control" id="column" onChange={this.sinkDataChangeHandler} value={this.state.sinkPayload.timeSeries.column}>
                                                                        <option disabled>Select</option>
                                                                        {Array.isArray(this.state.timeSeriesSchema) && this.state.timeSeriesSchema.map(val => <option value={val}>{val}</option>)}
                                                                    </select> :
                                                                    <div className="panelLoader">
                                                                        <i className="fal fa-sync-alt fa-spin"></i>
                                                                    </div>
                                                                }
                                                                <label className="form-group-label">Column :<span className="requiredMark"><i className="fa fa-asterisk"></i></span></label>
                                                            </div>
                                                        </React.Fragment> : ''
                                                }
                                                <div className="form-group flex-item fx-b100">
                                                    <TagsInput value={this.state.sinkPayload.connector.properties.partitionBy} id="partitionBy" onChange={(e) => this.sinkDataChangeHandler(e, "partitionBy")} inputProps={{ "placeholder": 'Partition By' }} />
                                                </div>
                                            </div>
                                        </React.Fragment>
                                    }
                                    {/* S3 and HDFS FORM */}
                                </div>

                                <div className="bottomButton text-right">
                                    <button type="button" onClick={this.submitClicked} className="btn btn-success" disabled={this.state.sinkPayload.connector.id.trim() === '' || this.state.defaultCategory.trim() === '' || this.state.defaultType.trim() === ''} >Save</button>
                                    <button type="button"
                                        onClick={() => this.sinkCancelHandler()} className="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/* end sink form */}

                {/* function detail modal */}
                {
                    this.state.showFunctionModal &&
                    <div className="modal fade show d-block" role="dialog" id="functionModal" >
                        <div className="modal-dialog modal-lg modal-dialog-centered">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h6 className="modal-title"><span>Function Name:</span> {this.state.functionName}
                                        <button className="close" data-dismiss="modal" onClick={() => this.setState({ showFunctionModal: false })}><i className="far fa-times"></i></button>
                                    </h6>
                                </div>
                                <div className="modal-body">
                                    <div className="functionDetailBox">
                                        <AceEditor
                                            focus={true}
                                            theme="github"
                                            name="ace_editor"
                                            value={this.state.functionData}
                                            readOnly={true}
                                            fontSize={14}
                                            height="100%"
                                            width="100%"
                                        />
                                    </div>

                                    <div className="bottomButton">
                                        <button type="button" className="btn btn-success" data-dismiss="modal" onClick={() => this.setState({ showFunctionModal: false })}>OK</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                }

                {/* end function detail modal */}

                {
                    this.state.isOpen &&
                    <NotificationModal
                        type={this.state.modalType}
                        message2={this.state.message2}
                        onCloseHandler={this.onCloseHandler}
                    />
                }

            </React.Fragment >
        );
    }
}

AddOrEditEtlConfig.propTypes = {
    dispatch: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
    etlError: SELECTORS.etlError(),
    etlInfo: SELECTORS.etlInfo(),
    saveEtlError: SELECTORS.saveEtlError(),
    saveEtlSuccess: SELECTORS.saveEtlSuccess(),
    // dataSourceList: SELECTORS.getDataSourceSuccess(),
    // dataSourceError: SELECTORS.getDataSourceError(),
    isFetching: SELECTORS.getIsFetching(),
    allSparksMethodsSuccess: SELECTORS.getAllSparksMethodsSuccess(),
    allSparksMethodsFailure: SELECTORS.getAllSparksMethodsError(),
    fetchSourceList: SELECTORS.fetchSourceList(),
    fetchSourceListError: SELECTORS.fetchSourceListError(),
    fetchCategory: SELECTORS.fetchCategory(),
    fetchCategoryError: SELECTORS.fetchCategoryError(),
    fetchTopics: SELECTORS.fetchTopics(),
    fetchTopicsError: SELECTORS.fetchTopicsError(),
    fetchedFunction: SELECTORS.fetchedFunction(),
    fetchedFunctionError: SELECTORS.fetchedFunctionError(),
    fetchedConnectors: SELECTORS.fetchedConnectors(),
    fetchedConnectorsError: SELECTORS.fetchedConnectorsError(),
    fetchedConnectorsForCategory : SELECTORS.fetchedConnectorsForCategory(),
    fetchedConnectorsForCategoryError : SELECTORS.fetchedConnectorsForCategoryError(),
    transformationSuccess: SELECTORS.transformationSuccess(),
    transformationError: SELECTORS.transformationError(),
    fetchBranchesSuccess: SELECTORS.fetchBranchesSuccess(),
    fetchBranchesError: SELECTORS.fetchBranchesError(),
    schemaApi: SELECTORS.schemaApi(),
    schemaApiError: SELECTORS.schemaApiError(),
    EtlListSuccess: SELECTORS.EtlListSuccess(),
    EtlListFailure: SELECTORS.EtlListFailure(),
    getConnectorsSuccess: SELECTORS.getConnectorsSuccess(),
    getConnectorsFailure: SELECTORS.getConnectorsFailure(),
    getConnectorByCategorySuccess: SELECTORS.getConnectorByCategorySuccess(),
    getConnectorByCategoryFailure: SELECTORS.getConnectorByCategoryFailure(),
    timeSeriesSchemaSuccess: SELECTORS.timeSeriesSchemaSuccess(),
    timeSeriesSchemaFailure: SELECTORS.timeSeriesSchemaFailure()
})
function mapDispatchToProps(dispatch) {
    return {
        dispatch,
        getDataConnectors: () => dispatch(ACTIONS.getDataConnectors()),
        getDataSetList: () => dispatch(ACTIONS.getDataSetList()),
        getEtlInfo: (id) => dispatch(ACTIONS.getEtlInfo(id)),
        saveUpdateEtl: (payload, id) => dispatch(ACTIONS.saveUpdateEtl(payload, id)),
        getAllSparksMethod: () => dispatch(ACTIONS.getAllSparksMethod()),
        getSourceList: () => dispatch(ACTIONS.getSourceList()),
        getCategory: (source) => dispatch(ACTIONS.getCategory(source)),
        getTopicsIds: (id) => dispatch(ACTIONS.getTopicsIds(id)),
        getFunctions: (functionType) => dispatch(ACTIONS.getFunctions(functionType)),
        getConnectors: () => dispatch(ACTIONS.getConnectors()),
        transformation: (payload, id) => dispatch(ACTIONS.transformation(payload, id)),
        getBuckets: () => dispatch(ACTIONS.getBuckets()),
        generateSchema: (topic, payload) => dispatch(ACTIONS.generateSchema(topic, payload)),
        getEtlInfoList: () => dispatch(ACTIONS.getEtlInfoList()),
        getCategoryByConnectors: (category) => dispatch(ACTIONS.getCategoryByConnectors(category)),
        getConnectorsForCategory: () => dispatch(ACTIONS.getConnectorsForCategory()),
        generateTimeSeriesSchema: (topic, payload) => dispatch(ACTIONS.generateTimeSeriesSchema(topic, payload))
    };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "addOrEditEtlConfig", reducer });
const withSaga = injectSaga({ key: "addOrEditEtlConfig", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(AddOrEditEtlConfig);
