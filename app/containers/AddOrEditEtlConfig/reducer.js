/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { fromJS } from "immutable";
import {
  DEFAULT_ACTION,
  GET_ETL_INFO_SUCCESS, GET_ETL_INFO_FAILURE,
  SAVE_ETL_SUCCESS, SAVE_ETL_FAILURE,
  DATA_LIST_SUCCESS, DATA_LIST_FAILURE,
  GET_ALL_SPARKS_METHODS_SUCCESS, GET_ALL_SPARKS_METHODS_FAILURE,
  GET_SOURCE_FAILURE,GET_SOURCE_SUCCESS,
  GET_CATEGORY_SUCCESS,GET_CATEGORY_FAILURE,
  GET_TOPICS_SUCCESS,GET_TOPICS_FAILURE,
  GET_FUNCTIONS_SUCCESS,GET_FUNCTIONS_FAILURE,
  GET_CONNECTORS_SUCCESS,GET_CONNECTORS_FAILURE,
  DO_TRANSFORMATION_SUCCESS,DO_TRANSFORMATION_FAILURE,
  GENERATE_SCHEMA_SUCCESS,GENERATE_SCHEMA_FAILURE,
  GET_BUCKETS_SUCCESS,GET_BUCKETS_FAILURE, GET_ETL_INFO_LIST_SUCCESS, GET_ETL_INFO_LIST_SUCCESS_FAILURE,GET_DATA_CONNECTORS_SUCCESS,GET_DATA_CONNECTORS_FAILURE,
  GET_CATEGORY_CONNECTOR_SUCCESS, GET_CATEGORY_CONNECTOR_FAILURE, GENERATE_TIME_SERIES_SCHEMA_FAILURE, GENERATE_TIME_SERIES_SCHEMA_SUCCESS,
  GET_CONNECTORS_FOR_CATEGORY_SUCCESS, GET_CONNECTORS_FOR_CATEGORY_FAILURE
} from "./constants";

export const initialState = fromJS({});

function addOrEditEtlConfigReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case GET_ETL_INFO_SUCCESS:
      return Object.assign({}, state, {
        'etlInfo': action.response
      });
    case GET_ETL_INFO_FAILURE:
      return Object.assign({}, state, {
        'etlError': action.error
      });
    case SAVE_ETL_SUCCESS:
      return Object.assign({}, state, {
        'saveEtlSuccess': action.response
      });
    case SAVE_ETL_FAILURE:
      return Object.assign({}, state, {
        'saveEtlError': action.error
      });
    case DATA_LIST_SUCCESS:
      return Object.assign({}, state, {
        'dataListSuccess': action.response
      });
    case DATA_LIST_FAILURE:
      return Object.assign({}, state, {
        'dataListError': action.error
      });
    case GET_ALL_SPARKS_METHODS_SUCCESS:
      return Object.assign({}, state, {
        'allSparksMethodsSuccess': action.response
      });
    case GET_ALL_SPARKS_METHODS_FAILURE:
      return Object.assign({}, state, {
        'allSparksMethodsError': action.error
      });
    case GET_SOURCE_SUCCESS:
      return Object.assign({}, state, {
        'getSourceList': action.response
      });
    case GET_SOURCE_FAILURE:
      return Object.assign({}, state, {
        'getSourceListError': action.error
      });
    case GET_CATEGORY_SUCCESS:
      return Object.assign({}, state, {
        'fetchCategory': action.response
      });
    case GET_CATEGORY_FAILURE:
      return Object.assign({}, state, {
        'fetchCategoryError': action.error
      });
    case GET_TOPICS_SUCCESS:
      return Object.assign({}, state, {
        'fetchTopics': action.response
      });
    case GET_TOPICS_FAILURE:
      return Object.assign({}, state, {
        'fetchTopicsError': action.error
      });
    case GET_FUNCTIONS_SUCCESS:
      return Object.assign({}, state, {
        'fetchedFunction': action.response
      });
    case GET_FUNCTIONS_FAILURE:
      return Object.assign({}, state, {
        'fetchedFunctionError': action.error
      });
    case GET_CONNECTORS_SUCCESS:
      return Object.assign({}, state, {
        'fetchedConnectors': action.response
      });
    case GET_CONNECTORS_FAILURE:
      return Object.assign({}, state, {
        'fetchedConnectorsError': action.error
      });
    case GET_CONNECTORS_FOR_CATEGORY_SUCCESS:
      return Object.assign({}, state, {
        'fetchedConnectorsForCategory': action.response
      });
    case GET_CONNECTORS_FOR_CATEGORY_FAILURE:
      return Object.assign({}, state, {
        'fetchedConnectorsForCategoryError': action.error
      });
    case DO_TRANSFORMATION_SUCCESS:
        return Object.assign({}, state, {
          'transformation': action.response
        });
    case DO_TRANSFORMATION_FAILURE:
      return Object.assign({}, state, {
        'transformationError': action.error
      });
    case GET_BUCKETS_SUCCESS:
      return Object.assign({}, state, {
        'fetchBranchesSuccess': action.response
      });
    case GET_BUCKETS_FAILURE:
      return Object.assign({}, state, {
        'fetchBranchesError': action.error
      });
    case GENERATE_SCHEMA_SUCCESS:
      return Object.assign({}, state, {
        'schemaApi': action.response
      });
    case GENERATE_SCHEMA_FAILURE:
      return Object.assign({}, state, {
        'schemaApiError': action.error
      });
    
    case GENERATE_TIME_SERIES_SCHEMA_SUCCESS :
      return Object.assign({}, state, {
        'timeSeriesSchemaSuccess' : action.response
      })
    case GENERATE_TIME_SERIES_SCHEMA_FAILURE :
      return Object.assign({}, state, {
        'timeSeriesSchemaFailure' : action.error
      })  
    case GET_ETL_INFO_LIST_SUCCESS:
      return Object.assign({}, state, {
        'EtlListSuccess':action.response
      })
    case GET_ETL_INFO_LIST_SUCCESS_FAILURE:
      return Object.assign({}, state, {
        'EtlListFailure': action.error,
      })
    case GET_DATA_CONNECTORS_SUCCESS: 
      return Object.assign({},state,{
        'getConnectorsSuccess': action.response,
      })
    case GET_DATA_CONNECTORS_FAILURE: 
      return Object.assign({},state,{
        'getConnectorsFailure': action.error,
      })
    case GET_CATEGORY_CONNECTOR_SUCCESS:
      return Object.assign({}, state, {
        'getConnectorByCategorySuccess': action.response
      })
    case GET_CATEGORY_CONNECTOR_FAILURE:
      return Object.assign({}, state, {
        'getConnectorByCategoryFailure': action.error
      })
    default:
      return initialState;
  }
}

export default addOrEditEtlConfigReducer;
