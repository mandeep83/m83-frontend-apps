/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
 * Direct selector to the addOrEditEtlConfig state domain
 */

const selectAddOrEditEtlConfigDomain = state =>
  state.get("addOrEditEtlConfig", initialState);

export const etlInfo = () => createSelector(selectAddOrEditEtlConfigDomain, substate => substate.etlInfo);
export const etlError = () => createSelector(selectAddOrEditEtlConfigDomain, substate => substate.etlError);

export const saveEtlSuccess = () => createSelector(selectAddOrEditEtlConfigDomain, substate => substate.saveEtlSuccess);
export const saveEtlError = () => createSelector(selectAddOrEditEtlConfigDomain, substate => substate.saveEtlError);

export const getDataSourceSuccess = () => createSelector(selectAddOrEditEtlConfigDomain, substate => substate.dataListSuccess);
export const getDataSourceError = () => createSelector(selectAddOrEditEtlConfigDomain, substate => substate.dataListError);

export const getAllSparksMethodsSuccess = () => createSelector(selectAddOrEditEtlConfigDomain, substate => substate.allSparksMethodsSuccess);
export const getAllSparksMethodsError = () => createSelector(selectAddOrEditEtlConfigDomain, substate => substate.allSparksMethodsError);

export const fetchSourceList = () => createSelector(selectAddOrEditEtlConfigDomain, substate => substate.getSourceList);
export const fetchSourceListError = () => createSelector(selectAddOrEditEtlConfigDomain, substate => substate.getSourceListError);

export const fetchCategory = () => createSelector(selectAddOrEditEtlConfigDomain, substate => substate.fetchCategory);
export const fetchCategoryError = () => createSelector(selectAddOrEditEtlConfigDomain, substate => substate.fetchCategoryError);

export const fetchTopics = () => createSelector(selectAddOrEditEtlConfigDomain, substate => substate.fetchTopics);
export const fetchTopicsError = () => createSelector(selectAddOrEditEtlConfigDomain, substate => substate.fetchTopicsError);

export const fetchedFunction = () => createSelector(selectAddOrEditEtlConfigDomain, substate => substate.fetchedFunction);
export const fetchedFunctionError = () => createSelector(selectAddOrEditEtlConfigDomain, substate => substate.fetchedFunctionError);

export const fetchedConnectors = () => createSelector(selectAddOrEditEtlConfigDomain, substate => substate.fetchedConnectors);
export const fetchedConnectorsError = () => createSelector(selectAddOrEditEtlConfigDomain, substate => substate.fetchedConnectorsError);

export const fetchedConnectorsForCategory = () => createSelector(selectAddOrEditEtlConfigDomain, substate => substate.fetchedConnectorsForCategory);
export const fetchedConnectorsForCategoryError = () => createSelector(selectAddOrEditEtlConfigDomain, substate => substate.fetchedConnectorsForCategoryError);

export const transformationSuccess = () => createSelector(selectAddOrEditEtlConfigDomain, substate => substate.transformation);
export const transformationError = () => createSelector(selectAddOrEditEtlConfigDomain, substate => substate.transformationError);

export const fetchBranchesSuccess = () => createSelector(selectAddOrEditEtlConfigDomain, substate => substate.fetchBranchesSuccess);
export const fetchBranchesError = () => createSelector(selectAddOrEditEtlConfigDomain, substate => substate.fetchBranchesError);

export const schemaApi = () => createSelector(selectAddOrEditEtlConfigDomain, substate => substate.schemaApi);
export const schemaApiError = () => createSelector(selectAddOrEditEtlConfigDomain, substate => substate.schemaApiError);

export const EtlListSuccess = () => createSelector(selectAddOrEditEtlConfigDomain, substate => substate.EtlListSuccess);
export const EtlListFailure = () => createSelector(selectAddOrEditEtlConfigDomain, substate => substate.EtlListFailure);

export const getConnectorsSuccess = () => createSelector(selectAddOrEditEtlConfigDomain, substate => substate.getConnectorsSuccess);
export const getConnectorsFailure = () => createSelector(selectAddOrEditEtlConfigDomain, substate => substate.getConnectorsFailure);

export const getConnectorByCategorySuccess = () => createSelector(selectAddOrEditEtlConfigDomain, substate => substate.getConnectorByCategorySuccess);
export const getConnectorByCategoryFailure = () => createSelector(selectAddOrEditEtlConfigDomain, substate => substate.getConnectorByCategoryFailure);

export const timeSeriesSchemaSuccess = () => createSelector(selectAddOrEditEtlConfigDomain, substate => substate.timeSeriesSchemaSuccess);
export const timeSeriesSchemaFailure = () => createSelector(selectAddOrEditEtlConfigDomain, substate => substate.timeSeriesSchemaFailure);

export const isFetchingState = state => state.get('loader');

export const getIsFetching = () =>
  createSelector(isFetchingState, substate => substate.get('isFetching'));
