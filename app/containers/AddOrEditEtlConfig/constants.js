/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

export const DEFAULT_ACTION = "app/AddOrEditEtlConfig/DEFAULT_ACTION";

export const DATA_LIST_REQUEST = "app/AddOrEditEtlConfig/DATA_LIST_REQUEST";
export const DATA_LIST_SUCCESS = "app/AddOrEditEtlConfig/DATA_LIST_SUCCESS";
export const DATA_LIST_FAILURE = "app/AddOrEditEtlConfig/DATA_LIST_FAILURE";

export const GET_ETL_INFO_LIST = "app/AddOrEditEtlConfig/GET_ETL_INFO_LIST";
export const GET_ETL_INFO_LIST_SUCCESS = "app/AddOrEditEtlConfig/GET_ETL_INFO_LIST_SUCCESS";
export const GET_ETL_INFO_LIST_SUCCESS_FAILURE = "app/AddOrEditEtlConfig/GET_ETL_INFO_LIST_SUCCESS_FAILURE";

export const GET_ETL_INFO = "app/AddOrEditEtlConfig/GET_ETL_INFO";
export const GET_ETL_INFO_SUCCESS = "app/AddOrEditEtlConfig/GET_ETL_INFO_SUCCESS";
export const GET_ETL_INFO_FAILURE = "app/AddOrEditEtlConfig/GET_ETL_INFO_FAILURE";

export const SAVE_ETL = "app/AddOrEditEtlConfig/SAVE_ETL";
export const SAVE_ETL_SUCCESS = "app/AddOrEditEtlConfig/SAVE_ETL_SUCCESS";
export const SAVE_ETL_FAILURE = "app/AddOrEditEtlConfig/SAVE_ETL_FAILURE";

export const GET_ALL_SPARKS_METHODS = "app/AddOrEditEtlConfig/GET_ALL_SPARKS_METHODS";
export const GET_ALL_SPARKS_METHODS_SUCCESS = "app/AddOrEditEtlConfig/GET_ALL_SPARKS_METHODS_SUCCESS";
export const GET_ALL_SPARKS_METHODS_FAILURE = "app/AddOrEditEtlConfig/GET_ALL_SPARKS_METHODS_FAILURE";

export const GET_SOURCE = "app/AddOrEditEtlConfig/GET_SOURCE";
export const GET_SOURCE_SUCCESS = "app/AddOrEditEtlConfig/GET_SOURCE_SUCCESS";
export const GET_SOURCE_FAILURE = "app/AddOrEditEtlConfig/GET_SOURCE_FAILURE";

export const GET_CATEGORY = "app/AddOrEditEtlConfig/GET_CATEGORY";
export const GET_CATEGORY_SUCCESS = "app/AddOrEditEtlConfig/GET_CATEGORY_SUCCESS";
export const GET_CATEGORY_FAILURE = "app/AddOrEditEtlConfig/GET_CATEGORY_FAILURE";

export const GET_TOPICS = "app/AddOrEditEtlConfig/GET_TOPICS";
export const GET_TOPICS_SUCCESS = "app/AddOrEditEtlConfig/GET_TOPICS_SUCCESS";
export const GET_TOPICS_FAILURE = "app/AddOrEditEtlConfig/GET_TOPICS_FAILURE";

export const GET_FUNCTIONS = "app/AddOrEditEtlConfig/GET_FUNCTIONS";
export const GET_FUNCTIONS_SUCCESS = "app/AddOrEditEtlConfig/GET_FUNCTIONS_SUCCESS";
export const GET_FUNCTIONS_FAILURE = "app/AddOrEditEtlConfig/GET_FUNCTIONS_FAILURE";

export const GET_CONNECTORS = "app/AddOrEditEtlConfig/GET_CONNECTORS";
export const GET_CONNECTORS_SUCCESS = "app/AddOrEditEtlConfig/GET_CONNECTORS_SUCCESS";
export const GET_CONNECTORS_FAILURE = "app/AddOrEditEtlConfig/GET_CONNECTORS_FAILURE";

export const DO_TRANSFORMATION = "app/AddOrEditEtlConfig/DO_TRANSFORMATION";
export const DO_TRANSFORMATION_SUCCESS = "app/AddOrEditEtlConfig/DO_TRANSFORMATION_SUCCESS";
export const DO_TRANSFORMATION_FAILURE = "app/AddOrEditEtlConfig/DO_TRANSFORMATION_FAILURE";

export const GET_BUCKETS = "app/AddOrEditEtlConfig/GET_BUCKETS";
export const GET_BUCKETS_SUCCESS = "app/AddOrEditEtlConfig/GET_BUCKETS_SUCCESS";
export const GET_BUCKETS_FAILURE = "app/AddOrEditEtlConfig/GET_BUCKETS_FAILURE";

export const GENERATE_SCHEMA = "app/AddOrEditEtlConfig/GENERATE_SCHEMA";
export const GENERATE_SCHEMA_SUCCESS = "app/AddOrEditEtlConfig/GENERATE_SCHEMA_SUCCESS";
export const GENERATE_SCHEMA_FAILURE = "app/AddOrEditEtlConfig/GENERATE_SCHEMA_FAILURE";

export const GET_DATA_CONNECTORS = "app/AddOrEditEtlConfig/GET_DATA_CONNECTORS";
export const GET_DATA_CONNECTORS_SUCCESS = "app/AddOrEditEtlConfig/GET_DATA_CONNECTORS_SUCCESS";
export const GET_DATA_CONNECTORS_FAILURE = "app/AddOrEditEtlConfig/GET_DATA_CONNECTORS_FAILURE";

export const GET_CATEGORY_CONNECTOR = "app/AddOrEditEtlConfig/GET_CATEGORY_CONNECTOR";
export const GET_CATEGORY_CONNECTOR_SUCCESS = "app/AddOrEditEtlConfig/GET_CATEGORY_CONNECTOR_SUCCESS";
export const GET_CATEGORY_CONNECTOR_FAILURE = "app/AddOrEditEtlConfig/GET_CATEGORY_CONNECTOR_FAILURE";

export const GET_CONNECTORS_FOR_CATEGORY = "app/AddOrEditEtlConfig/GET_CONNECTORS_FOR_CATEGORY";
export const GET_CONNECTORS_FOR_CATEGORY_SUCCESS = "app/AddOrEditEtlConfig/GET_CONNECTORS_FOR_CATEGORY_SUCCESS";
export const GET_CONNECTORS_FOR_CATEGORY_FAILURE = "app/AddOrEditEtlConfig/GET_CONNECTORS_FOR_CATEGORY_FAILURE";

export const GENERATE_TIME_SERIES_SCHEMA = "app/AddOrEditEtlConfig/GENERATE_TIME_SERIES_SCHEMA";
export const GENERATE_TIME_SERIES_SCHEMA_SUCCESS = "app/AddOrEditEtlConfig/GENERATE_TIME_SERIES_SCHEMA_SUCCESS";
export const GENERATE_TIME_SERIES_SCHEMA_FAILURE = "app/AddOrEditEtlConfig/GENERATE_TIME_SERIES_SCHEMA _FAILURE";