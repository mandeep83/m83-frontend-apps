/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

export const DEFAULT_ACTION = 'app/ManageRoles/DEFAULT_ACTION';

export const DELETE_ROLE = 'app/ManageRoles/DELETE_ROLE';
export const DELETE_ROLE_SUCCESS = 'app/ManageRoles/DELETE_ROLE_SUCCESS';
export const DELETE_ROLE_ERROR = 'app/ManageRoles/DELETE_ROLE_ERROR';
export const DELETE_ROLE_INITIATED = 'app/ManageRoles/DELETE_ROLE_INITIATED';

export const GET_ROLES_LIST = 'app/ManageRoles/GET_ROLES_LIST';
export const GET_ROLES_LIST_SUCCESS = 'app/ManageRoles/GET_ROLES_LIST_SUCCESS';
export const GET_ROLES_LIST_ERROR = 'app/ManageRoles/GET_ROLES_LIST_ERROR';

export const SAVE_ROLE = 'app/ManageRoles/SAVE_ROLE';
export const SAVE_ROLE_SUCCESS = 'app/ManageRoles/SAVE_ROLE_SUCCESS';
export const SAVE_ROLE_ERROR = 'app/ManageRoles/SAVE_ROLE_ERROR';

export const UPDATE_ROLE = 'app/ManageRoles/UPDATE_ROLE';
export const UPDATE_ROLE_SUCCESS = 'app/ManageRoles/UPDATE_ROLE_SUCCESS';
export const UPDATE_ROLE_ERROR = 'app/ManageRoles/UPDATE_ROLE_ERROR';

export const GET_ROLE_BY_ID = 'app/ManageRoles/GET_ROLE_BY_ID';
export const GET_ROLE_BY_ID_SUCCESS = 'app/ManageRoles/GET_ROLE_BY_ID_SUCCESS';
export const GET_ROLE_BY_ID_ERROR = 'app/ManageRoles/GET_ROLE_BY_ID_ERROR';

export const GET_DEVELOPER_QUOTA = 'app/ManageRoles/GET_DEVELOPER_QUOTA';
export const GET_DEVELOPER_QUOTA_SUCCESS = 'app/ManageRoles/GET_DEVELOPER_QUOTA_SUCCESS';
export const GET_DEVELOPER_QUOTA_ERROR = 'app/ManageRoles/GET_DEVELOPER_QUOTA_ERROR';
