/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  GET_ROLES_LIST_SUCCESS,
  GET_ROLES_LIST_ERROR,
  DELETE_ROLE_INITIATED,
  DELETE_ROLE_SUCCESS,
  DELETE_ROLE_ERROR,
  SAVE_ROLE,
  SAVE_ROLE_SUCCESS,
  SAVE_ROLE_ERROR,
  UPDATE_ROLE,
  UPDATE_ROLE_SUCCESS,
  UPDATE_ROLE_ERROR,
  GET_ROLE_BY_ID_SUCCESS,
  GET_ROLE_BY_ID_ERROR,
  DELETE_ROLE,
  GET_DEVELOPER_QUOTA_SUCCESS,
  GET_DEVELOPER_QUOTA_ERROR
} from './constants';

export const initialState = fromJS({
  isFetching: false,
  isDeleteSuccess: false,
  error: {},
  list: [],
});

function manageRolesReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case GET_ROLES_LIST_SUCCESS:
      return Object.assign({}, state, {
        list: action.response,
        isFetching: false,
      });
    case GET_ROLES_LIST_ERROR:
      return Object.assign({}, state, {
        error: action.error,
      });
    // case DELETE_ROLE_INITIATED:
    //   return Object.assign({}, state, {
    //     isFetching: true,
    //   });
    case DELETE_ROLE:
      return Object.assign({}, state, {
        isDeleteSuccess: undefined,
        deleteRoleError: undefined,

      });
    case DELETE_ROLE_SUCCESS:
      let temp = Object.assign({}, state, {
        isDeleteSuccess: action.response,
        isFetching: false,
      })
      temp.list = temp.list.filter(function (listObject) {
        if (listObject.id != action.response.id)
          return listObject;
      })
      return temp;
    case DELETE_ROLE_ERROR:
      return Object.assign({}, state, {
        deleteRoleError: action.error,
      });
    case SAVE_ROLE:
      return Object.assign({}, state, {
        saveRoleSuccess: undefined,
        saveRoleError: undefined
      });
    case SAVE_ROLE_SUCCESS:
      return Object.assign({}, state, {
        saveRoleSuccess: action.response,
      });
    case SAVE_ROLE_ERROR:
      return Object.assign({}, state, {
        saveRoleError: action.error,
      });
    case UPDATE_ROLE:
      return Object.assign({}, state, {
        updateRoleSuccess: undefined,
        updateRoleError: undefined
      });
    case UPDATE_ROLE_SUCCESS:
      return Object.assign({}, state, {
        updateRoleSuccess: action.response,
      });
    case UPDATE_ROLE_ERROR:
      return Object.assign({}, state, {
        updateRoleError: action.error,
      });
    case GET_ROLE_BY_ID_SUCCESS:
      return Object.assign({}, state, {
        getRoleByIdSuccess: action.response,
      });
    case GET_ROLE_BY_ID_ERROR:
      return Object.assign({}, state, {
        getRoleByIdError: action.error,
      });
    case GET_DEVELOPER_QUOTA_SUCCESS:
      return Object.assign({}, state, {
        'developerQuotaSuccess': action.response,
      });
    case GET_DEVELOPER_QUOTA_ERROR:
      return Object.assign({}, state, {
        'developerQuotaFailure': action.error
      });
    default:
      return state;
  }
}

export default manageRolesReducer;
