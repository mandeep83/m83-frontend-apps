/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { takeEvery } from 'redux-saga';
import { put } from 'redux-saga/effects';
import {
  GET_ROLES_LIST, GET_ROLES_LIST_SUCCESS, GET_ROLES_LIST_ERROR,
  DELETE_ROLE, DELETE_ROLE_SUCCESS, DELETE_ROLE_INITIATED, DELETE_ROLE_ERROR, SAVE_ROLE, SAVE_ROLE_SUCCESS, SAVE_ROLE_ERROR, UPDATE_ROLE_SUCCESS,
  UPDATE_ROLE_ERROR, UPDATE_ROLE, GET_ROLE_BY_ID, GET_ROLE_BY_ID_SUCCESS,GET_ROLE_BY_ID_ERROR, 
  GET_DEVELOPER_QUOTA, GET_DEVELOPER_QUOTA_SUCCESS, GET_DEVELOPER_QUOTA_ERROR
} from './constants';
import { apiCallHandler } from '../../api';

export function* getListApiHandlerAsync(action) {
  yield [apiCallHandler(action, GET_ROLES_LIST_SUCCESS, GET_ROLES_LIST_ERROR, 'getRoleList')];
}

export function* apiGetDeveloperQuota(action) {
  yield [apiCallHandler(action, GET_DEVELOPER_QUOTA_SUCCESS, GET_DEVELOPER_QUOTA_ERROR, 'getDeveloperQuota')];
}

export function* deleteRoleHandlerAsync(action) {
  // yield put({ type: DELETE_ROLE_INITIATED });
  yield [apiCallHandler(action, DELETE_ROLE_SUCCESS, DELETE_ROLE_ERROR, 'deleteRoleById', false)];
}

export function* saveRoleHandlerAsync(action) {
  yield [apiCallHandler(action, SAVE_ROLE_SUCCESS, SAVE_ROLE_ERROR, 'saveRole')];
}

export function* updateRoleHandlerAsync(action) {
  yield [apiCallHandler(action, UPDATE_ROLE_SUCCESS, UPDATE_ROLE_ERROR, 'updateRole')];
}

export function* getRoleByIdHandlerAsync(action) {
  yield [apiCallHandler(action, GET_ROLE_BY_ID_SUCCESS, GET_ROLE_BY_ID_ERROR, 'getRoleById')];
}

export function* watcherGetListRequest() {
  yield takeEvery(GET_ROLES_LIST, getListApiHandlerAsync);
}
export function* watcherDeleteRoleRequest() {
  yield takeEvery(DELETE_ROLE, deleteRoleHandlerAsync);
}

export function* watcherSaveRole() {
  yield takeEvery(SAVE_ROLE, saveRoleHandlerAsync);
}

export function* watcherUpdateRole() {
  yield takeEvery(UPDATE_ROLE, updateRoleHandlerAsync);
}

export function* watcherGetRoleById() {
  yield takeEvery(GET_ROLE_BY_ID, getRoleByIdHandlerAsync);
}

export function* watcherGetDeveloperQuota() {
  yield takeEvery(GET_DEVELOPER_QUOTA, apiGetDeveloperQuota);
}

export default function* rootSaga() {
  yield [
    watcherGetListRequest(),
    watcherDeleteRoleRequest(),
    watcherSaveRole(),
    watcherGetRoleById(),
    watcherUpdateRole(),
    watcherGetDeveloperQuota(),
  ];
}
