/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/
import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectManageRolesDomain = state =>
  state.get('manageRoles', initialState);

// export const getIsFetching = () =>
//   createSelector(selectManageRolesDomain, substate => substate.isFetching);

export const isFetchingState = state => state.get('loader');

export const getIsFetching = () =>
  createSelector(isFetchingState, substate => substate.get('isFetching'));

export const getRoles = () =>
  createSelector(selectManageRolesDomain, substate => substate.list);

export const isDeleteSuccess = () =>
  createSelector(selectManageRolesDomain, substate => substate.isDeleteSuccess);

export const getDeleteRoleError = () =>
  createSelector(selectManageRolesDomain, substate => substate.deleteRoleError);

export const saveRoleSuccess = () =>
  createSelector(selectManageRolesDomain, substate => substate.saveRoleSuccess);

export const saveRoleError = () =>
  createSelector(selectManageRolesDomain, substate => substate.saveRoleError);

export const updateRoleSuccess = () =>
  createSelector(selectManageRolesDomain, substate => substate.updateRoleSuccess);

export const updateRoleError = () =>
  createSelector(selectManageRolesDomain, substate => substate.updateRoleError);

export const getRoleByIdSuccess = () =>
  createSelector(selectManageRolesDomain, substate => substate.getRoleByIdSuccess);

export const getRoleByIdError = () =>
  createSelector(selectManageRolesDomain, substate => substate.getRoleByIdError);

export const getRolesListRError = () =>
  createSelector(selectManageRolesDomain, substate => substate.error);
  
export const developerQuotaSuccess = () =>
createSelector(selectManageRolesDomain, subState => subState.developerQuotaSuccess);

export const developerQuotaFailure = () =>
createSelector(selectManageRolesDomain, subState => subState.developerQuotaFailure);

