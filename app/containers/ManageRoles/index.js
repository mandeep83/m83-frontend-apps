/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import * as SELECTORS from './selectors';
import * as ACTIONS from './actions';
import reducer from './reducer';
import saga from './saga';
import Loader from '../../components/Loader/Loadable'
import NotificationModal from '../../components/NotificationModal/Loadable'
import ReactTable from 'react-table';
import FontAwesomeDisplayList from "../../components/FontAwesomeDisplayList";
import ReactTooltip from "react-tooltip";
import SlidingPane from 'react-sliding-pane';
import Modal from 'react-modal';
import { convertTimestampToDate } from '../../commonUtils';
import ListingTable from "../../components/ListingTable/Loadable";
import NoDataFoundMessage from "../../components/NoDataFoundMessage/Loadable"
import AddNewButton from "../../components/AddNewButton/Loadable"
import ReactSelect from "react-select";
import { cloneDeep } from 'lodash';
import Skeleton from "react-loading-skeleton";

let READ_ONLY_MENUS = ["Uptime Info", "Event History", "Aggregates", "Alarm Config", "Groups", "Attributes"]
/* eslint-disable react/prefer-stateless-function */
export class ManageRoles extends React.Component {
    state = {
        selectedRoleId: null,
        isFetching: true,
        addOrEditIsFetching: false,
        toggleView:true,
        isOpen: false,
        payload: {
            description: '',
            name: '',
            icon: '',
            menus: [],
            accessType: 'PLATFORM'
        },
        isAddOrEditRole: false,
        searchTerm: '',
        list: [],
        filteredDataList: [],
        rolesQuota: {
            total: 0,
            remaining: 0,
            used: 0,
        }
    }

    createNavMenu = (accessType) => {
        let navMenu = JSON.parse(localStorage.getItem('sideNav')).filter(temp => temp.navName !== "Home").map(el => {
            if (el.navName === "IAM" && accessType != "PLATFORM") {
                el.disabled = true
                if (el.subMenus.length) {
                    el.subMenus.map(subMenu => {
                        if (subMenu.navName === "Roles")
                            subMenu.permission = "READ";
                    })
                }
                else el.permission = "READ"
            }
            return el
        })
        return navMenu;
    }

    componentDidMount() {
        this.props.getDeveloperQuota([{
            dependingProductId: null,
            productName: "role"
        }]);
        this.props.getManageRolesList();
        Modal.setAppElement('body');
    }

    confirmModalHandler = id => {
        this.setState({
            selectedRoleId: id,
        });
        $('#confirmModal').modal('show');
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.isDeleteSuccess && nextProps.isDeleteSuccess !== this.props.isDeleteSuccess) {
            let rolesQuota = cloneDeep(this.state.rolesQuota)
            rolesQuota.remaining++;
            rolesQuota.used--;
            this.setState({
                modalType: "success",
                isOpen: true,
                message2: nextProps.isDeleteSuccess.message,
                isFetching: false,
                rolesQuota,
            })
        }

        if (nextProps.list && nextProps.list !== this.props.list) {
            let list = nextProps.list,
                filteredDataList = cloneDeep(nextProps.list),
                searchTerm = this.state.searchTerm;

            filteredDataList = (!searchTerm) ? nextProps.list : list.filter((item) => item.name.toLowerCase().includes(searchTerm.toLowerCase()))
            this.setState({
                isFetching: false,
                filteredDataList,
                list,
            })
        }

        if (nextProps.deleteRoleError && nextProps.deleteRoleError !== this.props.deleteRoleError) {

            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.deleteRoleError,
                isFetching: false,
            })
        }

        if (nextProps.saveRoleSuccess && nextProps.saveRoleSuccess !== this.props.saveRoleSuccess) {
            this.setState({
                modalType: "success",
                isOpen: true,
                message2: nextProps.saveRoleSuccess.message,
                isAddOrEditRole: false,
                isFetching: true,
                addOrEditIsFetching: false,
                selectedRoleId: '',
            }, () => {
                this.props.getManageRolesList()
                this.props.getDeveloperQuota([{
                    dependingProductId: null,
                    productName: "role"
                }]);
            })
        }

        if (nextProps.saveRoleError && nextProps.saveRoleError !== this.props.saveRoleError) {
            this.setState({
                errorMessage: nextProps.saveRoleError,
                addOrEditIsFetching: false
            }, () => {
                setTimeout(() => {
                    this.setState({
                        errorMessage: null
                    })
                }, 3000)
            });
        }

        if (nextProps.updateRoleSuccess && nextProps.updateRoleSuccess !== this.props.updateRoleSuccess) {
            this.setState({
                modalType: "success",
                isOpen: true,
                message2: nextProps.updateRoleSuccess.message,
                isAddOrEditRole: false,
                isFetching: true,
                addOrEditIsFetching: false
            }, () => {
                this.props.getManageRolesList()
                this.props.getDeveloperQuota([{
                    dependingProductId: null,
                    productName: "role"
                }]);
            })
        }

        if (nextProps.updateRoleError && nextProps.updateRoleError !== this.props.updateRoleError) {
            this.setState({
                errorMessage: nextProps.updateRoleError,
                addOrEditIsFetching: false
            }, () => {
                setTimeout(() => {
                    this.setState({
                        errorMessage: null
                    })
                }, 3000)
            });
        }

        if (nextProps.getRoleByIdSuccess && nextProps.getRoleByIdSuccess !== this.props.getRoleByIdSuccess) {
            let navMenu = this.createNavMenu(nextProps.getRoleByIdSuccess.accessType)
            navMenu.map(nav => {
                if (nav.subMenus.length > 0) {
                    nav.subMenus.map(subMenu => {
                        subMenu.permission = "";
                        nextProps.getRoleByIdSuccess.menus.map(menu => {
                            if (menu.menuId === subMenu.id) {
                                subMenu.permission = menu.permission
                            }
                        })
                    })
                } else {
                    nav.permission = '';
                    nextProps.getRoleByIdSuccess.menus.map(menu => {
                        if (menu.menuId === nav.id) {
                            nav.permission = menu.permission
                        }
                    })
                }
                nav.disabled = this.handleDisabled(nextProps.getRoleByIdSuccess.accessType, nav.navName);
            })
            this.setState({
                addOrEditIsFetching: false,
                payload: nextProps.getRoleByIdSuccess,
                navMenu,
            })
        }

        if (nextProps.getRoleByIdError && nextProps.getRoleByIdError !== this.props.getRoleByIdError) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.getRoleByIdError
            })
        }

        if (nextProps.rolesListError && nextProps.rolesListError !== this.props.rolesListError) {
            this.setState({
                modalType: "error",
                isOpen: true,
                message2: nextProps.rolesListError,
                isFetching: false
            })
        }

        if (nextProps.developerQuotaSuccess && nextProps.developerQuotaSuccess !== this.props.developerQuotaSuccess) {
            this.setState({
                rolesQuota: nextProps.developerQuotaSuccess[0],
            })
        }

        if (nextProps.developerQuotaFailure && nextProps.developerQuotaFailure !== this.props.developerQuotaFailure) {
            this.setState({
                isOpenModal: true,
                message2: nextProps.developerQuotaFailure,
                type: 'error'
            });
        }
    }

    addNewRoleHandler = () => {
        let payload = {
            description: '',
            name: '',
            menus: [],
            accessType: 'PLATFORM',
            icon: ""
        };
        let navMenu = this.createNavMenu(payload.accessType)
        this.setState({
            isAddOrEditRole: true,
            payload,
            navMenu,
            errorMessage: null
        })
    }

    onCloseHandler = (index) => {

        this.setState({
            isOpen: false,
            message2: ''
        });
    }

    handlePermission = (accessType, navName, subMenu) => {
        if (navName === "IAM" && accessType != "PLATFORM") {
            if ((subMenu && subMenu === "Roles"))
                return "READ"
            return ""
        }
        if (accessType !== "PLATFORM") {
            if (navName === "Applications") {
                return accessType === "BOTH" ? "ALL" : "READ"
            }
            else if (navName === "Operations" || navName === "Templates") {
                return "ALL"
            }
            return ""
        }

    }

    handleDisabled = (accessType, navName, subMenu) => {
        if ((navName === "IAM" && accessType != "PLATFORM") || accessType === "APPLICATION") {
            if (navName === "IAM" && (subMenu && subMenu !== "Roles"))
                return false;
            return true
        }
        if (accessType !== "PLATFORM")
            return (navName === "Applications" || navName === "Operations" || navName === "IAM" || navName === "Templates")
    }

    changeHandler = (event) => {
        let payload = JSON.parse(JSON.stringify(this.state.payload))
        let navMenu = JSON.parse(JSON.stringify(this.state.navMenu))

        if (event.target.name.includes("permission")) {
            if (event.target.id.split('_')[2]) {
                let notDebug = navMenu[event.target.id.split('_')[1]].subMenus[event.target.id.split('_')[2]].navName !== "Debug"
                if (notDebug) {
                    navMenu[event.target.id.split('_')[1]].subMenus[event.target.id.split('_')[2]].permission = event.target.checked ? event.target.value : ""
                }
                else {
                    (navMenu[event.target.id.split('_')[1]].subMenus.find(subMenu => subMenu.navName === "Manage").permission === "") ?
                        navMenu[event.target.id.split('_')[1]].subMenus[event.target.id.split('_')[2]].permission = event.target.checked ? event.target.value : ""
                        :
                        navMenu[event.target.id.split('_')[1]].subMenus[event.target.id.split('_')[2]].permission = event.target.value
                }
                if (navMenu[event.target.id.split('_')[1]].subMenus.filter(sub => sub.permission == event.target.value).length === navMenu[event.target.id.split('_')[1]].subMenus.length)
                    navMenu[event.target.id.split('_')[1]].permission = event.target.value
                if (navMenu[event.target.id.split('_')[1]].subMenus[event.target.id.split('_')[2]].navName === "Manage") {
                    (navMenu[event.target.id.split('_')[1]].subMenus.find(subMenu => subMenu.navName === "Debug")).permission = event.target.value
                }
                else if (navMenu[event.target.id.split('_')[1]].permission)
                    navMenu[event.target.id.split('_')[1]].permission = ""
            } else {
                navMenu[event.target.id.split('_')[1]].permission = event.target.checked ? event.target.value : ""
                if (navMenu[event.target.id.split('_')[1]].subMenus) {
                    navMenu[event.target.id.split('_')[1]].subMenus.map(sub => {
                        sub.permission = event.target.checked ? READ_ONLY_MENUS.includes(sub.navName) ? "READ" : event.target.value : ""
                    })
                }
            }

        }
        else {
            if (event.target.id === "name") {
                if (/^[a-zA-Z]+(?:_[a-zA-Z]+)*$/.test(event.target.value) || /^$/.test(event.target.value) || /^[a-zA-Z_]*$/.test(event.target.value))
                    payload.name = (event.target.value).toUpperCase()
            } else if (event.target.name === 'accessLevel') {
                payload.accessType = event.target.value;
                navMenu.map(nav => {
                    if (nav.subMenus.length > 0) {
                        nav.subMenus.map(subMenu => {
                            subMenu.permission = this.handlePermission(payload.accessType, nav.navName, subMenu.navName);
                        })
                    }
                    else nav.permission = this.handlePermission(payload.accessType, nav.navName);
                    nav.disabled = this.handleDisabled(payload.accessType, nav.navName);
                })
            } else payload[event.target.id] = event.target.value
        }
        this.setState({
            payload,
            navMenu
        })
    }

    submitHandler = (event) => {
        event.preventDefault();
        let payload = JSON.parse(JSON.stringify(this.state.payload))
        let navMenu = JSON.parse(JSON.stringify(this.state.navMenu))
        payload.menus = navMenu.find(val => val.navName == "Help") ? [{
            "menuId": navMenu.find(val => val.navName == "Help").id,
            "permission": "ALL",
        }] : [];
        navMenu.map(nav => {
            if (nav.subMenus.length > 0) {
                nav.subMenus.map(subMenu => {
                    if (subMenu.permission) {
                        payload.menus.push({
                            "menuId": subMenu.id,
                            "permission": subMenu.permission,
                        })
                    }
                })
            } else {
                if (nav.permission && nav.navName != "Help") {
                    payload.menus.push({
                        "menuId": nav.id,
                        "permission": nav.permission
                    })
                }
            }
        })
        if (payload.id) {
            this.props.updateRole(payload)
        }
        else {
            this.props.saveRole(payload)
        }

        this.setState({
            addOrEditIsFetching: true,
            navMenu
        })
    }

    handleChange = (value) => {
        let payload = JSON.parse(JSON.stringify(this.state.payload));
        payload.icon = value;
        this.setState({ payload })
    }

    closeModalHandler = () => {
        this.setState({
            isAddOrEditRole: false,
            selectedRoleId: '',
            errorMessage: null
        })
    }

    onSearchHandler = (e) => {
        let filterList = JSON.parse(JSON.stringify(this.props.list))
        if (e.target.value.length > 0) {
            filterList = filterList.filter(item => item.name.toLowerCase().includes(e.target.value.toLowerCase()))
        }
        this.setState({
            filteredDataList: filterList,
            searchTerm: e.target.value
        })
    }

    emptySearchBox = () => {
        let filteredDataList = JSON.parse(JSON.stringify(this.props.list))
        this.setState({
            searchTerm: '',
            filteredDataList
        })
    }

    getColumns = () => {
        let column = [
            {
                Header: "Name",
                width: 25,
                cell: (row) => (
                    <React.Fragment>
                        <div className="list-view-icon"><i className={row.name == "ACCOUNT_ADMIN" ? "fad fa-user-shield" : row.icon ? row.icon : "fad fa-user-tie"}></i></div>
                        <div className="list-view-icon-box">
                            <h6 className="text-theme fw-600">{row.name}</h6>
                            <p>{`${row.description || "-"}`}</p>
                        </div>
                    </React.Fragment>
                )
            },
            {
                Header: "Type",
                width: 20,
                cell: (row) => (
                    <h6 className={row.type == "SYSTEM_ROLE" ? "text-pink" : "text-indigo"}>{row.type}</h6>
                )
            },
            {
                Header: "Associated Users",
                width: 15,
                cell: (row) => (
                    <span className="alert alert-primary list-view-badge">{row.userCount}</span>
                )
            },
            {
                Header: "Created",
                width: 15,
                accessor: "createdAt"
            },
            {
                Header: "Updated",
                width: 15,
                accessor: "updatedAt"
            },
            {
                Header: "Actions",
                width: 10,
                cell: (row) => (
                    <div className="button-group">
                        <button className="btn-transparent btn-transparent-blue" disabled={row.type === 'SYSTEM_ROLE'}
                            onClick={() => {
                                this.setState({ selectedRoleId: row.id, addOrEditIsFetching: true, isAddOrEditRole: true });
                                this.props.getRoleById(row.id)
                            }}
                            data-tooltip data-tooltip-text="Edit" data-tooltip-place="bottom">
                            <i className="far fa-pencil"></i>
                        </button>
                        <button className="btn-transparent btn-transparent-red" disabled={row.type === 'SYSTEM_ROLE'} onClick={() => { this.confirmModalHandler(row.id); }}
                            data-tooltip data-tooltip-text="Delete" data-tooltip-place="bottom">
                            <i className="far fa-trash-alt"></i>
                        </button>
                    </div>
                )
            }
        ];
        return column;
    }

    refreshRoles = () => {
        this.setState({
            isFetching: true
        }, () => {
            this.props.getManageRolesList()
        })
    }

    getAppNavs = () => {
        let appNavs = {}
        this.state.navMenu.forEach((nav, navIndex) => {
            if (nav.subMenus.length ? nav.subMenus[0].url.startsWith("dashboard/") : nav.url.startsWith("dashboard/")) {
                appNavs[nav.customMenuName] = appNavs[nav.customMenuName] || []
                appNavs[nav.customMenuName].push({ ...nav, navIndex })
            }
        })
        return appNavs
    }

    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>Roles</title>
                    <meta name="description" content="M83-Roles" />
                </Helmet>

                <header className="content-header d-flex">
                    <div className="flex-60">
                        <h6>Roles -
                            <span className="content-header-badge-group">
                                <span className="content-header-badge-item">Allocated <span className="badge badge-pill badge-primary">{this.state.rolesQuota.total}</span></span>
                                <span className="content-header-badge-item">Used <span className="badge badge-pill badge-success">{this.state.rolesQuota.used}</span></span>
                                <span className="content-header-badge-item">Remaining <span className="badge badge-pill badge-warning">{this.state.rolesQuota.remaining}</span></span>
                            </span>
                        </h6>
                    </div>
                    <div className="flex-40 text-right">
                        <div className="content-header-group">
                            <div className="search-box">
                                <span className="search-icon"><i className="far fa-search"></i></span>
                                <input type="text" className="form-control" placeholder="Search..." value={this.state.searchTerm} onChange={this.onSearchHandler} disabled={!this.props.list || this.props.list.length == 0 || this.state.isFetching} />
                                {this.state.searchTerm.length > 0 &&
                                    <button className="search-button"><i className="far fa-times" onClick={() => this.emptySearchBox()}></i></button>
                                }
                            </div>
                            <button className={this.state.toggleView == true ? "btn btn-light active" : "btn btn-light"} disabled={this.state.isFetching} data-tooltip="true" data-tooltip-text="List View" data-tooltip-place="bottom" onClick={() => this.setState({ toggleView: true })}><i className="fad fa-list-ul"></i></button>
                            <button className={this.state.toggleView == false ? "btn btn-light active" : "btn btn-light"} disabled={this.state.isFetching} data-tooltip="true" data-tooltip-text="Card View" data-tooltip-place="bottom" onClick={() => this.setState({ toggleView: false })}><i className="fad fa-table"></i></button>
                            <button className="btn btn-light" disabled={this.state.isFetching} data-tooltip data-tooltip-text="Refresh" data-tooltip-place="bottom" onClick={this.refreshRoles} ><i className="far fa-sync-alt"></i></button>
                            <button className="btn btn-primary" data-tooltip data-tooltip-text="Add Role" data-tooltip-place="bottom" disabled={this.state.rolesQuota.remaining === 0 || this.state.isFetching} onClick={this.addNewRoleHandler}><i className="far fa-plus"></i></button>
                        </div>
                    </div>
                </header>

                <div className="content-body">
                    {this.state.isFetching ?
                        <Skeleton count={9} className="skeleton-list-loader" /> :
                        <React.Fragment>
                            {this.state.list.length > 0 ?
                                this.state.toggleView ?
                                    <ListingTable
                                        columns={this.getColumns()}
                                        data={this.state.filteredDataList}
                                    />
                                    :
                                    <ul className="card-view-list">
                                        {this.state.filteredDataList.map((item, index) =>
                                            <li key={index}>
                                                <div className="card-view-box">
                                                    <div className="card-view-header">
                                                        <span className={`alert alert-secondary bg-white ${item.type == "SYSTEM_ROLE" ? "text-pink" : "text-indigo"}`}>{item.type}</span>
                                                        <div className="dropdown">
                                                            <button className="btn-transparent btn-transparent-gray" type="button" data-toggle="dropdown">
                                                                <i className="fas fa-ellipsis-v"></i>
                                                            </button>
                                                            <div className="dropdown-menu">
                                                                <button className="dropdown-item" disabled={item.type === 'SYSTEM_ROLE'}
                                                                    onClick={() => {
                                                                        this.setState({ selectedRoleId: item.id, addOrEditIsFetching: true, isAddOrEditRole: true });
                                                                        this.props.getRoleById(item.id)
                                                                    }}>
                                                                    <i className="far fa-pencil"></i>Edit
                                                                </button>
                                                                <button className="dropdown-item" disabled={item.type === 'SYSTEM_ROLE'} onClick={() => { this.confirmModalHandler(item.id); }}>
                                                                    <i className="far fa-trash-alt"></i>Delete
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="card-view-body">
                                                        <div className="card-view-icon">
                                                            <i className={item.name == "ACCOUNT_ADMIN" ? "fad fa-user-shield" : item.icon ? item.icon : "fad fa-user-tie"}></i>
                                                        </div>
                                                        <h6><i className="far fa-address-book"></i>{item.name ? item.name : "N/A"}</h6>
                                                        <p><i className="far fa-file-alt"></i>{item.description ? item.description : "N/A"}</p>
                                                        <p><span className="badge badge-pill alert-primary">Associated Users - {item.userCount}</span></p>
                                                    </div>
                                                    <div className="card-view-footer d-flex">
                                                        <div className="flex-50 p-3">
                                                            <h6>Created</h6>
                                                            <p><strong>By - </strong>{item.createdBy}</p>
                                                            <p><strong>At - </strong>{new Date(item.createdAt).toLocaleString('en-US', { createdAt: localStorage.createdAt })}</p>
                                                        </div>
                                                        <div className="flex-50 p-3">
                                                            <h6>Updated</h6>
                                                            <p><strong>By - </strong>{item.updatedBy}</p>
                                                            <p><strong>At - </strong>{new Date(item.updatedAt).toLocaleString('en-US', { createdAt: localStorage.createdAt })}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        )}
                                    </ul>
                                :
                                <AddNewButton
                                    text1="No Roles available"
                                    text2="You haven't created any role yet"
                                    imageIcon="addRole.png"
                                    addButtonEnable={true}
                                    createItemOnAddButtonClick={this.addNewRoleHandler}
                                />
                            }
                        </React.Fragment>
                    }
                </div>

                {/* add new role */}
                <SlidingPane
                    className=''
                    overlayClassName='sliding-form'
                    closeIcon={<div></div>}
                    isOpen={this.state.isAddOrEditRole || false}
                    from='right'
                    width='500px'
                >
                    <div className="modal-content">
                        <div className="modal-header">
                            <h6 className="modal-title">{this.state.payload.id ? "Update Role" : "Add New Role"}
                                <button type="button" className="btn btn-light close" onClick={this.closeModalHandler}><i className="fas fa-angle-right"></i></button>
                            </h6>
                        </div>

                        {this.state.addOrEditIsFetching ?
                            <div className="modal-loader">
                                <i className="fad fa-sync-alt fa-spin"></i>
                            </div> :
                            <form onSubmit={this.submitHandler}>
                                <div className="modal-body">
                                    <div className="alert alert-info note-text">
                                        <p>Role name must contain only capital letters and underscore.</p>
                                    </div>

                                    {this.state.errorMessage &&
                                        <React.Fragment>
                                            {Array.isArray(this.state.errorMessage) ?
                                                this.state.errorMessage.map((val, index) => (
                                                    <p className="modal-body-error" key={index}>{val}</p>
                                                )) :
                                                <p className="modal-body-error">{this.state.errorMessage}</p>
                                            }
                                        </React.Fragment>
                                    }

                                    <div className="form-group">
                                        <label className="form-group-label">Name :  <i className="fas fa-asterisk form-group-required"></i></label>
                                        <input type="text" id="name" name="roleName" className="form-control" value={this.state.payload.name}
                                            onChange={this.changeHandler} pattern="^[A-Za-z]+(?:_[A-Za-z]+)*$" required
                                        />
                                    </div>

                                    <div className="form-group">
                                        <label className="form-group-label">Role Icon :</label>
                                        <FontAwesomeDisplayList value={this.state.payload.icon} changeHandlerForSelect={(value) => this.handleChange(value)}></FontAwesomeDisplayList>
                                    </div>

                                    <div className="form-group">
                                        <label className="form-group-label">Description :</label>
                                        <textarea rows="3" type="text" name="desc" id="description" className="form-control"
                                            value={this.state.payload.description} onChange={this.changeHandler}
                                        />
                                    </div>

                                    {/*<div className="form-group">
                                        <label className="form-group-label">Access :</label>
                                        <div className="d-flex">
                                            <div className="flex-33 pd-r-10">
                                                <label className="radio-button">
                                                    <span className="radio-button-text">Platform</span>
                                                    <input type="radio" name="accessLevel" value="PLATFORM" checked={this.state.payload.accessType === 'PLATFORM'} onChange={this.changeHandler} />
                                                    <span className="radio-button-mark"></span>
                                                </label>
                                            </div>
                                            <div className="flex-33 pd-l-5 pd-r-5">
                                                <label className="radio-button">
                                                    <span className="radio-button-text">Application</span>
                                                    <input type="radio" name="accessLevel" value="APPLICATION" checked={this.state.payload.accessType === 'APPLICATION'} onChange={this.changeHandler} />
                                                    <span className="radio-button-mark"></span>
                                                </label>
                                            </div>
                                            <div className="flex-33 pd-l-10">
                                                <label className="radio-button">
                                                    <span className="radio-button-text">Both</span>
                                                    <input type="radio" name="accessLevel" value="BOTH" checked={this.state.payload.accessType === 'BOTH'} onChange={this.changeHandler} />
                                                    <span className="radio-button-mark"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>*/}

                                    <div className="form-group">
                                        <label className="form-group-label collapse-icon" data-toggle="collapse" data-target="#standardMenu">Standard Menu : <i className="fad fa-chevron-circle-up f-14 float-right"></i></label>
                                        <div className="collapse show" id="standardMenu">
                                            <div className="navigation-box">
                                                <div className="d-flex">
                                                    <h6 className="flex-60">Navigation</h6>
                                                    <h6 className="flex-20">Read</h6>
                                                    <h6 className="flex-20">Read/Write</h6>
                                                </div>
                                                <ul className="nav-list list-style-none">
                                                    {this.state.navMenu && this.state.navMenu.filter(sysNav => !sysNav.systemRoleNav && (sysNav.subMenus.length ? !sysNav.subMenus[0].url.startsWith("dashboard/") : !sysNav.url.startsWith("dashboard/"))).length ? this.state.navMenu.map((nav, navIndex) => {
                                                        if (!nav.systemRoleNav && nav.navName != "Help" && (nav.subMenus.length ? !nav.subMenus[0].url.startsWith("dashboard/") : !nav.url.startsWith("dashboard/"))) {
                                                            return <li key={nav.id}>
                                                                <div className="d-flex">
                                                                    <div className="flex-60"><p>{nav.navName}</p></div>
                                                                    <div className="flex-20">
                                                                        <label className={nav.disabled ? 'check-box check-box-disabled' : 'check-box'}>
                                                                            <input type="checkbox" disabled={nav.disabled} name={"permission" + navIndex} value="READ" id={"READ_" + navIndex} checked={nav.permission == "READ" || (nav.subMenus.length > 0 && nav.subMenus.every(per => per.permission == "READ")) ? true : false} onChange={this.changeHandler}></input>
                                                                            <span className="check-mark"></span>
                                                                        </label>
                                                                    </div>
                                                                    <div className="flex-20">
                                                                        <label className={nav.disabled ? 'check-box check-box-disabled' : 'check-box'}>
                                                                            <input type={"checkbox"} disabled={nav.disabled} name={"permission" + navIndex} value="ALL" id={"ALL_" + navIndex} checked={nav.permission == "ALL" || (nav.subMenus.length > 0 && nav.subMenus.every(per => per.permission == "ALL")) ? true : false} onChange={this.changeHandler}></input>
                                                                            <span className="check-mark"></span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <ul className="nav-sub-list list-style-none">
                                                                    {nav.subMenus.map((subMenu, subMenuIndex) => {
                                                                        if (!subMenu.systemRoleNav && subMenu.navName !== "Trends & Forecast") {

                                                                            return <li key={subMenu.id}>
                                                                                <div className="d-flex">
                                                                                    <div className="flex-60">
                                                                                        <p>{subMenu.navName}</p>
                                                                                    </div>
                                                                                    <div className="flex-20">
                                                                                        <label className={(nav.disabled && subMenu.navName !== "Groups" && subMenu.navName !== "Users") ? 'check-box check-box-disabled' : 'check-box'}>
                                                                                            <input type="checkbox" disabled={nav.disabled && subMenu.navName !== "Groups" && subMenu.navName !== "Users"} value="READ" name={"permission" + navIndex + "_" + subMenuIndex} id={"READ_" + navIndex + "_" + subMenuIndex} checked={subMenu.permission == "READ" ? true : false} onChange={this.changeHandler}></input>
                                                                                            <span className="check-mark"></span>
                                                                                        </label>
                                                                                    </div>
                                                                                    <div className="flex-20">
                                                                                        {READ_ONLY_MENUS.includes(subMenu.navName) ?
                                                                                            <span className="f-12 text-content"> N/A </span> :
                                                                                            <label className={(nav.disabled && subMenu.navName !== "Groups" && subMenu.navName !== "Users") ? 'check-box check-box-disabled' : 'check-box'}>
                                                                                                <input type="checkbox" disabled={(nav.disabled && subMenu.navName !== "Groups" && subMenu.navName !== "Users")} value="ALL" name={"permission" + navIndex + "_" + subMenuIndex} id={"ALL_" + navIndex + "_" + subMenuIndex} checked={subMenu.permission == "ALL" ? true : false} onChange={this.changeHandler}></input>
                                                                                                <span className="check-mark"></span>
                                                                                            </label>
                                                                                        }
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                        }
                                                                    })}
                                                                </ul>
                                                            </li>
                                                        }
                                                    }) :
                                                        <p className="text-content p-3 text-center">No Navigation(s) found !</p>
                                                    }
                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="form-group">
                                        <label className="form-group-label collapse-icon" data-toggle="collapse" data-target="#customMenu">Applications : <i className="fad fa-chevron-circle-up f-14 float-right"></i></label>
                                        <div className="collapse show" id="customMenu">
                                            <div className="navigation-box">
                                                <ul className="nav-list list-style-none">
                                                    {this.state.navMenu && this.state.navMenu.filter(nav => nav.subMenus.length ? nav.subMenus[0].url.startsWith("dashboard/") : nav.url.startsWith("dashboard/")).length ? Object.entries(this.getAppNavs()).map(([app, navs]) =>
                                                        <React.Fragment key={app}>
                                                            <div className="d-flex">
                                                                <h6 className="flex-70">{app}</h6>
                                                                <h6 className="flex-30">Access Enabled</h6>
                                                            </div>
                                                            {navs.map(nav => {
                                                                let navIndex = nav.navIndex
                                                                return <li key={nav.id}>
                                                                    <div className="d-flex">
                                                                        <div className="flex-70"><p>{nav.navName}</p></div>
                                                                        <div className="flex-30">
                                                                            <label className={nav.disabled ? 'check-box check-box-disabled' : 'check-box'}>
                                                                                <input type="checkbox" disabled={nav.disabled} name={"permission" + navIndex} value="READ" id={"READ_" + navIndex} checked={nav.permission == "READ" || (nav.subMenus.length > 0 && nav.subMenus.every(per => per.permission == "READ")) ? true : false} onChange={this.changeHandler}></input>
                                                                                <span className="check-mark"></span>
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                    <ul className="nav-sub-list list-style-none">
                                                                        {nav.subMenus.map((subMenu, subMenuIndex) => {
                                                                            if (!subMenu.systemRoleNav && subMenu.navName !== "Trends & Forecast") {
                                                                                return <li key={subMenu.id}>
                                                                                    <div className="d-flex">
                                                                                        <div className="flex-70">
                                                                                            <p>{subMenu.navName}</p>
                                                                                        </div>
                                                                                        <div className="flex-30">
                                                                                            <label className={(nav.disabled && subMenu.navName !== "Groups" && subMenu.navName !== "Users") ? 'check-box check-box-disabled' : 'check-box'}>
                                                                                                <input type="checkbox" disabled={nav.disabled && subMenu.navName !== "Groups" && subMenu.navName !== "Users"} value="READ" name={"permission" + navIndex + "_" + subMenuIndex} id={"READ_" + navIndex + "_" + subMenuIndex} checked={subMenu.permission == "READ" ? true : false} onChange={this.changeHandler}></input>
                                                                                                <span className="check-mark"></span>
                                                                                            </label>
                                                                                        </div>
                                                                                    </div>
                                                                                </li>
                                                                            }
                                                                        })}
                                                                    </ul>
                                                                </li>
                                                            })}
                                                        </React.Fragment>
                                                    ) : <ul className="nav-list list-style-none">
                                                            <p className="text-content p-3 text-center">No Application(s) found !</p>
                                                        </ul>}
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="modal-footer justify-content-start">
                                    <button className="btn btn-primary">{this.state.payload.id ? "Update" : "Save"}</button>
                                    <button type="button" className="btn btn-dark" onClick={this.closeModalHandler}>Cancel</button>
                                </div>
                            </form>
                        }
                    </div>
                </SlidingPane>
                {/* end add new role */}

                {/* delete role modal */}
                <div className="modal show animated slideInDown" id="confirmModal">
                    <div className="modal-dialog modal-dialog-centered">
                        <div className="modal-content">
                            <div className="modal-body">
                                <div className="delete-content">
                                    <div className="delete-icon">
                                        <i className="fad fa-trash-alt text-red"></i>
                                    </div>
                                    <h4>Delete!</h4>
                                    <h6>Are you sure you want to delete this role ?</h6>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-dark" data-dismiss="modal" onClick={() => { $('#confirmModal').modal('hide'); }}>No</button>
                                <button type="button" className="btn btn-success" data-dismiss="modal" onClick={() => {
                                    this.props.deleteSelectedRole(this.state.selectedRoleId)
                                    this.setState({ isFetching: true })
                                }}>Yes</button>
                            </div>
                        </div>
                    </div>
                </div>
                {/* end delete role modal */}

                {this.state.isOpen &&
                    <NotificationModal
                        type={this.state.modalType}
                        message2={this.state.message2}
                        onCloseHandler={this.onCloseHandler}
                    />
                }

            </React.Fragment>
        );
    }
}

ManageRoles.defaultProps = {
    isDeleteSuccess: false,
};

ManageRoles.propTypes = {
    isDeleteSuccess: PropTypes.bool,
};
const mapStateToProps = createStructuredSelector({
    list: SELECTORS.getRoles(),
    isDeleteSuccess: SELECTORS.isDeleteSuccess(),
    deleteRoleError: SELECTORS.getDeleteRoleError(),
    saveRoleSuccess: SELECTORS.saveRoleSuccess(),
    saveRoleError: SELECTORS.saveRoleError(),
    updateRoleSuccess: SELECTORS.updateRoleSuccess(),
    updateRoleError: SELECTORS.updateRoleError(),
    getRoleByIdSuccess: SELECTORS.getRoleByIdSuccess(),
    getRoleByIdError: SELECTORS.getRoleByIdError(),
    rolesListError: SELECTORS.getRolesListRError(),
    developerQuotaSuccess: SELECTORS.developerQuotaSuccess(),
    developerQuotaFailure: SELECTORS.developerQuotaFailure(),
});

function mapDispatchToProps(dispatch) {
    return {
        dispatch,
        getManageRolesList: () => dispatch(ACTIONS.getManageRolesList()),
        deleteSelectedRole: id => dispatch(ACTIONS.deleteSelectedRole(id)),
        saveRole: payload => dispatch(ACTIONS.saveRole(payload)),
        getRoleById: id => dispatch(ACTIONS.getRoleById(id)),
        updateRole: payload => dispatch(ACTIONS.updateRole(payload)),
        getDeveloperQuota: payload => dispatch(ACTIONS.getDeveloperQuota(payload)),
    };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'manageRoles', reducer });
const withSaga = injectSaga({ key: 'manageRoles', saga });

export default compose(
    withReducer,
    withSaga,
    withConnect,
)(ManageRoles);



