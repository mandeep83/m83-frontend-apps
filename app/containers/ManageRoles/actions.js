/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { DEFAULT_ACTION, GET_ROLES_LIST, DELETE_ROLE, SAVE_ROLE, UPDATE_ROLE, GET_ROLE_BY_ID, GET_DEVELOPER_QUOTA } from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}
export function getManageRolesList() {
  return {
    type: GET_ROLES_LIST,
  };
}

export function deleteSelectedRole(id) {
  return {
    type: DELETE_ROLE,
    id,
  };
}

export function saveRole(payload) {
  return {
    type: SAVE_ROLE,
    payload,
  };
}

export function updateRole(payload) {
  return {
    type: UPDATE_ROLE,
    payload,
  };
}

export function getRoleById(id) {
  return {
    type: GET_ROLE_BY_ID,
    id,
  };
}

export function getDeveloperQuota(payload) {
  return {
    type: GET_DEVELOPER_QUOTA,
    payload
  };
}