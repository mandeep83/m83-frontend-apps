/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/*
 *
 * AddOrEditEvent constants
 *
 */

export const RESET_TO_INITIAL_STATE = "app/AddOrEditEvent/RESET_TO_INITIAL_STATE";


export const GET_ALL_CONNECTORS_AND_GROUPS = {
	action: 'app/AddOrEditEvent/GET_ALL_CONNECTORS_AND_GROUPS',
	success: "app/AddOrEditEvent/GET_ALL_CONNECTORS_AND_GROUPS_SUCCESS",
	failure: "app/AddOrEditEvent/GET_ALL_CONNECTORS_AND_GROUPS_FAILURE",
	urlKey: "getDeviceTypeList",
	successKey: "getAllConnectorsSuccess",
	failureKey: "getAllConnectorsFailure",
	actionName: "getAllConnectors",
	actionArguments: []
}

export const GET_ALL_DEVICES_BY_CONNECTOR_ID = {
	action: 'app/AddOrEditEvent/GET_ALL_DEVICES_BY_CONNECTOR_ID',
	success: "app/AddOrEditEvent/GET_ALL_DEVICES_BY_CONNECTOR_ID_SUCCESS",
	failure: "app/AddOrEditEvent/GET_ALL_DEVICES_BY_CONNECTOR_ID_FAILURE",
	urlKey: "getDevicesFromGroupId",
	successKey: "getAllDevicesByConnectorIdSuccess",
	failureKey: "getAllDevicesByConnectorIdFailure",
	actionName: "getAllDevicesByConnectorId",
	actionArguments: ["payload"]
}

export const CREATE_EVENT = {
	action: 'app/AddOrEditEvent/CREATE_EVENT',
	success: "app/AddOrEditEvent/CREATE_EVENT_SUCCESS",
	failure: "app/AddOrEditEvent/CREATE_EVENT_FAILURE",
	urlKey: "createOrUpdateEvent",
	successKey: "createEventSuccess",
	failureKey: "createEventFailure",
	actionName: "createEvent",
	actionArguments: ["payload"]
}

export const GET_ATTRIBUTE_LIST = {
	action: 'app/AddOrEditEvent/GET_ATTRIBUTE_LIST_BY_ID',
	success: "app/AddOrEditEvent/GET_ATTRIBUTE_LIST_BY_ID_SUCCESS",
	failure: "app/AddOrEditEvent/GET_ATTRIBUTE_LIST_BY_ID_FAILURE",
	urlKey: "getAttributeList",
	successKey: "getAttributeListSuccess",
	failureKey: "getAttributeListFailure",
	actionName: "getAttributeList",
	actionArguments: ["id"]
}

export const GET_EVENT_BY_ID = {
	action: 'app/AddOrEditEvent/GET_EVENT_BY_ID',
	success: "app/AddOrEditEvent/GET_EVENT_BY_ID_SUCCESS",
	failure: "app/AddOrEditEvent/GET_EVENT_BY_ID_FAILURE",
	urlKey: "getEventById",
	successKey: "getEventByIdSuccess",
	failureKey: "getEventByIdFailure",
	actionName: "getEventById",
	actionArguments: ["id"]
}

export const UPDATE_EVENT = {
	action: 'app/AddOrEditEvent/UPDATE_EVENT',
	success: "app/AddOrEditEvent/UPDATE_EVENT_SUCCESS",
	failure: "app/AddOrEditEvent/UPDATE_EVENT_FAILURE",
	urlKey: "createOrUpdateEvent",
	successKey: "updateEventSuccess",
	failureKey: "updateEventFailure",
	actionName: "updateEvent",
	actionArguments: ["id", "payload"]
}

export const CREATE_ACTION = {
	action: 'app/AddOrEditEvent/CREATE_ACTION',
	success: "app/AddOrEditEvent/CREATE_ACTION_SUCCESS",
	failure: "app/AddOrEditEvent/CREATE_ACTION_FAILURE",
	urlKey: "createorEditAction",
	successKey: "createActionSuccess",
	failureKey: "createActionFailure",
	actionName: "createAction",
	actionArguments: ["payload"]
}

export const UPDATE_ACTION = {
	action: 'app/AddOrEditEvent/UPDATE_ACTION',
	success: "app/AddOrEditEvent/UPDATE_ACTION_SUCCESS",
	failure: "app/AddOrEditEvent/UPDATE_ACTION_FAILURE",
	urlKey: "createorEditAction",
	successKey: "updateActionSuccess",
	failureKey: "updateActionFailure",
	actionName: "updateAction",
	actionArguments: ["id", "payload"]
}

export const GET_ACTIONS = {
	action: 'app/AddOrEditEvent/GET_ACTIONS',
	success: "app/AddOrEditEvent/GET_ACTIONS_SUCCESS",
	failure: "app/AddOrEditEvent/GET_ACTIONS_FAILURE",
	urlKey: "getActionsList",
	successKey: "getActionsListSuccess",
	failureKey: "getActionsListFailure",
	actionName: "getActionsList",
	actionArguments: [""]
}

export const DELETE_ACTION = {
	action: 'app/AddOrEditEvent/DELETE_ACTION',
	success: "app/AddOrEditEvent/DELETE_ACTION_SUCCESS",
	failure: "app/AddOrEditEvent/DELETE_ACTION_FAILURE",
	urlKey: "deleteAction",
	successKey: "deleteActionSuccess",
	failureKey: "deleteActionFailure",
	actionName: "deleteAction",
	actionArguments: ["id"]
}

export const GET_TOPICS = {
	action: 'app/AddOrEditEvent/GET_TOPICS',
	success: "app/AddOrEditEvent/GET_TOPICS_SUCCESS",
	failure: "app/AddOrEditEvent/GET_TOPICS_FAILURE",
	urlKey: "getTopicsFromConnectorID",
	successKey: "getTopicsSuccess",
	failureKey: "getTopicsFailure",
	actionName: "getTopics",
	actionArguments: ["id"]
}

export const GET_ALL_EVENT_LIST = {
	action: 'app/AddOrEditEvent/GET_ALL_EVENT_LIST',
	success: "app/AddOrEditEvent/GET_ALL_EVENT_LIST_SUCCESS",
	failure: "app/AddOrEditEvent/GET_ALL_EVENT_LIST_FAILURE",
	urlKey: "getEventList",
	successKey: "getAllEventListSuccess",
	failureKey: "getAllEventListFailure",
	actionName: "getAllEventList",
	actionArguments: []
}

export const GET_ALL_RPC_LIST = {
	action: 'app/AddOrEditEvent/GET_ALL_RPC_LIST',
	success: "app/AddOrEditEvent/GET_ALL_RPC_LIST_SUCCESS",
	failure: "app/AddOrEditEvent/GET_ALL_RPC_LIST_FAILURE",
	urlKey: "getAllRpc",
	successKey: "getAllRpcSuccess",
	failureKey: "getAllRpcFailure",
	actionName: "getAllRpc",
	actionArguments: ["id"]
}

export const DEVELOPER_QUOTA = {
	action: 'app/AddOrEditEvent/GET_DEVELOPER_QUOTA',
	success: "app/AddOrEditEvent/GET_DEVELOPER_QUOTA_SUCCESS",
	failure: "app/AddOrEditEvent/GET_DEVELOPER_QUOTA_FAILURE",
	urlKey: "getDeveloperQuota",
	successKey: "developerQuotaSuccess",
	failureKey: "developerQuotaFailure",
	actionName: "getDeveloperQuota",
	actionArguments: ["payload"]
}

export const GET_RPC_ACTION = {
	action: 'app/AddOrEditEvent/GET_RPC_ACTION',
	success: "app/AddOrEditEvent/GET_RPC_ACTION_SUCCESS",
	failure: "app/AddOrEditEvent/GET_RPC_ACTION_FAILURE",
	urlKey: "getAllActions",
	successKey: "getRPCActionSuccess",
	failureKey: "getRPCActionFailure",
	actionName: "getRPCAction",
	actionArguments: ["payload"]
}

export const GET_ALL_DEVICES_BY_DEVICE_TYPES = {
	action: 'app/AddOrEditEvent/GET_ALL_DEVICES_BY_DEVICE_TYPES',
	success: "app/AddOrEditEvent/GET_ALL_DEVICES_BY_DEVICE_TYPES_SUCCESS",
	failure: "app/AddOrEditEvent/GET_ALL_DEVICES_BY_DEVICE_TYPES_FAILURE",
	urlKey: "getAllDevicesByDeviceTypes",
	successKey: "getAllDevicesByDeviceTypesSuccess",
	failureKey: "getAllDevicesByDeviceTypesFailure",
	actionName: "getAllDevicesByDeviceTypes",
	actionArguments: []
} 