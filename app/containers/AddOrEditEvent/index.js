/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * AddOrEditEvent
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { FormattedMessage } from "react-intl";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import { allSelectors as SELECTORS } from "./selectors";
import { allActions as ACTIONS } from './actions'

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import reducer from "./reducer";
import saga from "./saga";
import messages from "./messages";
import { cloneDeep } from "lodash";
import Select from 'react-select';
import NotificationModal from '../../components/NotificationModal/Loadable';
import Loader from "../../components/Loader/Loadable";
import TagsInput from 'react-tagsinput'
import PhoneInput from "react-phone-input-2";
import 'react-phone-input-2/lib/style.css';
import AceEditor from "react-ace";
import 'brace/mode/python';
import 'brace/mode/json';
import 'brace/theme/monokai';
import 'brace/theme/github';
import 'brace/ext/searchbox'
import 'brace/ext/language_tools'
import JSONInput from "react-json-editor-ajrm/dist";
import { isNavAssigned } from "../../commonUtils";


let conditionsTemp = {
    operand: "OR",
    conditionList: [
        {
            displayName: "",
            path: "",
            value: "",
            operation: "EQUALS_TO",
            attribute: "",
            unit: ""
        }
    ]
}

const activeAction = {
    sms: "Recipient",
    email: "Recipient",
    rpc: "Control",
    webhook: "Endpoint",
}

/* eslint-disable react/prefer-stateless-function */
export class AddOrEditEvent extends React.Component {
    state = {
        eventList: [],
        connectorList: [],
        selectedID: [],
        attributeList: [],
        payload: {
            description: "",
            deviceGroupId: [],
            deviceTypeId: "",
            duration: "",
            evaluateEvent: false,
            excludeIds: [],
            rpcConfigs: [],
            condition: {
                operand: "OR",
                conditionList: [{
                    operand: "OR",
                    conditionList: [
                        {
                            displayName: "",
                            path: "",
                            value: "",
                            operation: "EQUALS_TO",
                            attribute: "",
                            unit: ""
                        }
                    ]
                }]
            },
            name: "",
            occurrence: "",
            actions: []
        },
        triggerActionsPayload: {
            name: "",
            triggerType: "",
            metaData: {
                numbers: [{
                    number: "",
                    country: {
                        value: "",
                        label: ""
                    }
                }]
            }
        },
        isFetching: true,
        activeAction: "sms",
        getAllActions: [],
        getActionByType: [],
        deviceGroupByConnector: [],
        selectedGroup: [],
        actionToBeEdited: "",
        isCreateOrDeleteActionLoader: true,
        //  controlTopic: "",
        selectedConnector: {},
        isFetchingAttribute: true,
        allRpcList: [],
        isAddorEditActions: false,
        developerQuota: [],
        rulesQuota: {
            remaining: 0,
            total: 0,
            used: 0,
        },
        smsQuota: {
            remaining: 0,
            total: 0,
            used: 0,
        },
        emailQuota: {
            remaining: 0,
            total: 0,
            used: 0,
        },
        webhookQuota: {
            remaining: 0,
            total: 0,
            used: 0,
        },
        rpcQuota: {
            remaining: 0,
            total: 0,
            used: 0,
        },
        getDeviceIDPayload: {
            deviceGroupIds: [],
            deviceTypeId: ""
        },
        rpcList: [],
        selectedDeviceTypesRPCs: [],
        selectedRPCDeviceTypes: [],
        allDevices: []
    }

    componentDidMount() {
        this.props.getAllEventList()
        this.props.getAllConnectors()
        this.props.getActionsList();
        this.props.getAllDevicesByDeviceTypes();
        let payload = [
            {
                dependingProductId: null,
                productName: "rules"
            },
            {
                dependingProductId: null,
                productName: "sms"
            },
            {
                dependingProductId: null,
                productName: "email"
            },
            {
                dependingProductId: null,
                productName: "webhooks"
            },
            {
                dependingProductId: null,
                productName: "rpc"
            }
        ];
        this.props.getDeveloperQuota(payload);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.developerQuotaSuccess) {
            this.setState({
                developerQuota: nextProps.developerQuotaSuccess.response,
                rulesQuota: nextProps.developerQuotaSuccess.response.filter(quota => quota.productName === 'rules')[0],
                smsQuota: nextProps.developerQuotaSuccess.response.filter(quota => quota.productName === 'sms')[0],
                emailQuota: nextProps.developerQuotaSuccess.response.filter(quota => quota.productName === 'email')[0],
                webhookQuota: nextProps.developerQuotaSuccess.response.filter(quota => quota.productName === 'webhooks')[0],
                rpcQuota: nextProps.developerQuotaSuccess.response.filter(quota => quota.productName === 'rpc')[0],
            })
        }

        if (nextProps.developerQuotaFailure) {
            this.setState({
                isOpen: true,
                type: "error",
                message2: nextProps.developerQuotaFailure.error,

            })
        }

        if (nextProps.getAllEventListSuccess) {
            let eventList = nextProps.getAllEventListSuccess.response
            this.setState({ eventList })
        }

        if (nextProps.getAllEventListFailure) {
            this.setState({
                isOpen: true,
                type: "error",
                message2: nextProps.getAllEventListFailure.error,
                isFetching: false
            })
        }

        if (nextProps.getAllConnectorsSuccess && nextProps.getAllConnectorsSuccess !== this.props.getAllConnectorsSuccess) {
            let connectorList = nextProps.getAllConnectorsSuccess.response.map(connector => {
                connector.label = `${connector.name}${connector.assetType ? ` (${connector.assetType})` : ""}`
                connector.value = connector.deviceTypeId
                return connector
            }),
                payload = cloneDeep(this.state.payload),
                selectedConnector = cloneDeep(this.state.selectedConnector),
                deviceGroupByConnector = cloneDeep(this.state.deviceGroupByConnector)
            payload.deviceTypeId = this.props.match.params.id ? payload.deviceTypeId : this.props.history.location.state && this.props.history.location.state.deviceTypeId ? this.props.history.location.state.deviceTypeId : connectorList.length && connectorList[0].deviceTypeId;

            if (!this.props.match.params.id && payload.deviceTypeId) {
                selectedConnector = connectorList.find(el => el.deviceTypeId == payload.deviceTypeId)
                deviceGroupByConnector = connectorList.find(el => el.deviceTypeId == payload.deviceTypeId).deviceGroups.map(group => {
                    return {
                        label: group.name,
                        value: group.deviceGroupId
                    }
                })
            }
            !this.props.match.params.id && this.props.getRPCAction(["rpc"]);
            this.setState({
                connectorList,
                payload,
                selectedConnector,
                deviceGroupByConnector,
                isFetching: this.props.match.params.id ? true : false,
                isFetchingAttribute: connectorList.length && selectedConnector.attributesAvailable ? true : false
            }, () => {

                //payload.deviceTypeId ? this.props.getAllRpc(payload.deviceTypeId) : null;

                if (this.props.match.params.id) {
                    this.props.getEventById(this.props.match.params.id)
                }

                if (connectorList.length > 0) {
                    if (selectedConnector.attributesAvailable) {
                        this.props.getAttributeList(payload.deviceTypeId);
                    }
                }
            })
            return;
        }

        if (nextProps.getAllConnectorsFailure) {
            this.setState({
                isOpen: true,
                type: "error",
                message2: nextProps.getAllConnectorsFailure.error,
                isFetching: false
            })
        }

        if (nextProps.getAllDevicesByConnectorIdSuccess) {
            let listOfAllDevices = nextProps.getAllDevicesByConnectorIdSuccess.response.length ?
                nextProps.getAllDevicesByConnectorIdSuccess.response : [],
                selectedID = cloneDeep(this.state.selectedID),
                uniqueDeviceID = [...new Set(listOfAllDevices.map(el => el.deviceId))]

            selectedID = selectedID.filter(el => uniqueDeviceID.includes(el.value))

            selectedID = selectedID.map(temp => {
                let selectDeviceID = listOfAllDevices.find(device => device.deviceId == temp.value)
                return {
                    label: selectDeviceID.name ? selectDeviceID.name : selectDeviceID.deviceId.slice(0, 25) + (selectDeviceID.deviceId.length > 25 ? "..." : ""),
                    value: temp.value
                }
            })
            let getAllDevicesOptions = listOfAllDevices.map(device => {
                return {
                    label: device.name ? device.name : device.deviceId.slice(0, 25) + (device.deviceId.length > 25 ? "..." : ""),
                    value: device.deviceId
                }
            })


            this.setState({
                listOfAllDevices,
                getAllDevicesOptions,
                isFetchingDevices: false,
                selectedID,
                isFetching: false,
            })
            return;

        }

        if (nextProps.getAllDevicesByConnectorIdFailure) {
            this.setState({
                isOpen: true,
                type: "error",
                message2: nextProps.getAllDevicesByConnectorIdFailure.error,
                isFetching: false
            })
        }

        if (nextProps.getAttributeListSuccess) {
            let attributeList = nextProps.getAttributeListSuccess.response ? nextProps.getAttributeListSuccess.response : [];
            attributeList = attributeList.filter(e => e.attribute !== "timestamp")
            let payload = cloneDeep(this.state.payload)
            if (attributeList.length > 0) {
                conditionsTemp.conditionList[0].displayName = attributeList[0].displayName
                conditionsTemp.conditionList[0].attribute = attributeList[0].attribute
                conditionsTemp.conditionList[0].value = attributeList[0].value
                conditionsTemp.conditionList[0].unit = attributeList[0].unit

                if (!this.props.match.params.id) {
                    payload.condition.conditionList[0].conditionList[0].displayName = attributeList[0].displayName
                    payload.condition.conditionList[0].conditionList[0].attribute = attributeList[0].attribute
                    payload.condition.conditionList[0].conditionList[0].value = attributeList[0].value
                    payload.condition.conditionList[0].conditionList[0].unit = attributeList[0].unit
                }
            }

            this.setState({ attributeList, payload, isFetchingAttribute: false })
            return
        }

        if (nextProps.getAttributeListFailure) {
            this.setState({
                isFetchingAttribute: false,
                isOpen: true,
                type: "error",
                message2: nextProps.getAttributeListFailure.error,
            })
        }

        if (nextProps.createEventSuccess) {
            this.props.showNotification("success", nextProps.createEventSuccess.response.message)
            this.props.history.push('/events/' + this.state.selectedConnector.deviceTypeId)
        }

        if (nextProps.createEventFailure) {
            this.setState({
                isOpen: true,
                type: "error",
                message2: nextProps.createEventFailure.error,
                isFetching: false
            })
        }

        if (nextProps.getEventByIdSuccess) {
            let payload = cloneDeep(nextProps.getEventByIdSuccess.response),
                getAllActions = cloneDeep(this.state.getAllActions),
                getActionByType = cloneDeep(this.state.getActionByType),
                selectedConnector = cloneDeep(this.state.selectedConnector),
                deviceGroupByConnector = cloneDeep(this.state.deviceGroupByConnector),
                selectedGroup = cloneDeep(this.state.selectedGroup),
                getDeviceIDPayload = {
                    deviceTypeId: nextProps.getEventByIdSuccess.response.deviceTypeId,
                    deviceGroupIds: nextProps.getEventByIdSuccess.response.deviceGroupId
                }

            selectedConnector = this.state.connectorList.find(e => e.deviceTypeId == payload.deviceTypeId)
            getAllActions = getAllActions.map(el => {
                el.isActionChecked = payload.actions.includes(el.id);
                return el
            })

            getActionByType = getAllActions.filter(el => el.triggerType == this.state.activeAction);

            deviceGroupByConnector = selectedConnector.deviceGroups.map(group => {
                return {
                    label: group.name,
                    value: group.deviceGroupId
                }
            })

            selectedGroup = deviceGroupByConnector.map(group => {
                if (payload.deviceGroupId.includes(group.value)) {
                    return {
                        label: group.label,
                        value: group.value
                    }
                }
            })


            let selectedID = payload.excludeIds && payload.excludeIds.map((el) => {
                return {
                    label: el,
                    value: el
                }
            })

            this.props.getRPCAction(["rpc"])

            this.setState({
                payload,
                selectedID,
                getActionByType,
                getAllActions,
                selectedConnector,
                isFetchingAttribute: true,
                deviceGroupByConnector,
                selectedGroup
            }, () => {
                this.props.getAttributeList(payload.deviceTypeId)
                this.props.getAllDevicesByConnectorId(getDeviceIDPayload)
                //this.props.getAllRpc(payload.deviceTypeId)
            })
        }

        if (nextProps.getEventByIdFailure) {
            this.setState({
                isOpen: true,
                type: "error",
                message2: nextProps.getEventByIdFailure.error,
                isFetching: false
            })
        }

        if (nextProps.updateEventSuccess) {
            this.props.showNotification("success", nextProps.updateEventSuccess.response.message)
            this.props.history.push('/events/' + this.state.selectedConnector.deviceTypeId)
        }

        if (nextProps.updateEventFailure) {
            this.setState({
                isFetching: false,
                isOpen: true,
                type: "error",
                message2: nextProps.updateEventFailure.error,
            })

        }
        if (nextProps.getActionsListSuccess) {
            let getAllActions = nextProps.getActionsListSuccess.response
            let payload = cloneDeep(this.state.payload)
            getAllActions = getAllActions.map(e => {
                if (payload.actions.includes(e.id)) {
                    e.isActionChecked = true
                }
                return e
            }
            )

            let getActionByType = cloneDeep(this.state.getActionByType)
            getActionByType = getAllActions.filter(el => el.triggerType == this.state.activeAction);
            this.setState({ getAllActions, getActionByType, isCreateOrDeleteActionLoader: false })
        }

        if (nextProps.getActionsListFailure) {
            this.setState({
                isCreateOrDeleteActionLoader: false,
                isFetching: false,
                isOpen: true,
                type: "error",
                message2: nextProps.getActionsListFailure.error,
            })
        }

        if (nextProps.createActionSuccess) {
            this.setState({
                isFetching: false,
                isOpen: true,
                type: "success",
                message2: nextProps.createActionSuccess.response.message,
                isAddorEditActions: false,
            }, () => this.props.getActionsList())
        }
        if (nextProps.createActionFailure) {
            this.setState({
                isOpen: true,
                type: "error",
                message2: nextProps.createActionFailure.error,
                isCreateOrDeleteActionLoader: false
            })
        }

        if (nextProps.updateActionSuccess) {
            this.setState({
                isOpen: true,
                type: "success",
                message2: nextProps.updateActionSuccess.response.message,
                isAddorEditActions: false,
            }, () => this.props.getActionsList())
        }
        if (nextProps.updateActionFailure) {
            this.setState({
                isOpen: true,
                type: "error",
                message2: nextProps.updateActionFailure.error,
                isCreateOrDeleteActionLoader: false
            })
        }

        if (nextProps.deleteActionSuccess) {
            let getAllActions = cloneDeep(this.state.getAllActions),
                getActionByType = cloneDeep(this.state.getActionByType),
                payload = cloneDeep(this.state.payload)

            getAllActions = getAllActions.filter(el => el.id !== nextProps.deleteActionSuccess.response.id)
            payload.actions = payload.actions.filter(e => e !== nextProps.deleteActionSuccess.response.id)
            getActionByType = getAllActions.filter(el => el.triggerType == this.state.activeAction);

            this.setState({
                getAllActions,
                getActionByType,
                isOpen: true,
                message2: nextProps.deleteActionSuccess.response.message,
                isCreateOrDeleteActionLoader: false,
                type: "success",
                isEditActionMode: false,
                payload
            })
        }

        if (nextProps.deleteActionFailure) {
            this.setState({
                isOpen: true,
                type: "error",
                message2: nextProps.deleteActionFailure.error,
                isCreateOrDeleteActionLoader: false
            })
        }

        // if (nextProps.getTopicsSuccess) {
        //     this.setState({ controlTopic: nextProps.getTopicsSuccess.response.controlTopic })
        // }

        // if (nextProps.getTopicsFailure) {
        //     this.setState({
        //         isOpen: true,
        //         type: "error",
        //         message2: nextProps.getTopicsFailure.error,
        //     })
        // }

        if (nextProps.getAllRpcSuccess) {
            let allRpcList = cloneDeep(this.state.allRpcList),
                triggerActionsPayload = cloneDeep(this.state.triggerActionsPayload)
            allRpcList = nextProps.getAllRpcSuccess.response
            triggerActionsPayload.metaData.command = "",
                triggerActionsPayload.metaData.controlName = ""

            this.setState({ allRpcList, triggerActionsPayload })
        }
        if (nextProps.getRPCActionSuccess) {
            let rpcList = nextProps.getRPCActionSuccess.response.rpc
            let selectedDeviceTypesRPCs, selectedRPCDeviceTypes;
            if (this.props.match.params.id && this.state.payload.rpcConfigs.length) {
                selectedRPCDeviceTypes = this.state.connectorList.filter(connector => this.state.payload.rpcConfigs.some(config => config.toDeviceTypeId === connector.deviceTypeId))
                selectedDeviceTypesRPCs = rpcList.filter(rpc => this.state.payload.rpcConfigs.some(config => config.toDeviceTypeId === rpc.metaData.deviceTypeId))
            }
            else {
                selectedDeviceTypesRPCs = rpcList.filter(rpc => rpc.metaData.deviceTypeId === this.state.payload.deviceTypeId)
                selectedRPCDeviceTypes = [this.state.connectorList.find(connector => connector.deviceTypeId == this.state.payload.deviceTypeId)]
            }
            this.setState({ rpcList, selectedDeviceTypesRPCs, selectedRPCDeviceTypes })
        }
        if (nextProps.getAllDevicesByDeviceTypesSuccess) {
            let allDevices = {}
            nextProps.getAllDevicesByDeviceTypesSuccess.response.map(deviceType => {
                allDevices[deviceType.deviceTypeId] = deviceType.devices.map(device => {
                    device.label = device.name || device._uniqueDeviceId
                    device.value = device._uniqueDeviceId
                    return device
                })
            })
            this.setState({ allDevices })
        }
    }

    componentDidUpdate(prevProps, prevState) {
        let newProp = Object.keys(SELECTORS).find(prop => this.props[prop])
        if (newProp) {
            if (["getAllDevicesByDeviceTypesFailure", "getRPCActionFailure"].includes(newProp)) {
                this.props.showNotification("error", this.props[newProp].error)
            }
            this.props.resetToInitialState()
        }
    }

    eventChangeHandler = (event, actionIndex) => {
        let payload = cloneDeep(this.state.payload),
            getDeviceIDPayload = cloneDeep(this.state.getDeviceIDPayload),
            getAllActions = cloneDeep(this.state.getAllActions),
            getActionByType = cloneDeep(this.state.getActionByType),
            selectedConnector = cloneDeep(this.state.selectedConnector),
            attributeList = cloneDeep(this.state.attributeList),
            errorDurationMessage = this.state.errorDurationMessage,
            isFetchingAttribute = this.state.isFetchingAttribute,
            isFetchingDevices = false,
            selectedID = cloneDeep(this.state.selectedID),
            deviceGroupByConnector = cloneDeep(this.state.deviceGroupByConnector),
            selectedGroup = cloneDeep(this.state.selectedGroup),
            selectedRPCDeviceTypes = this.state.selectedRPCDeviceTypes,
            selectedDeviceTypesRPCs = this.state.selectedDeviceTypesRPCs

        if (event.target.id == "addActionToEvent") {
            let getActionIndex = getAllActions.findIndex(e => e.id == getActionByType[actionIndex].id)
            getAllActions[getActionIndex].isActionChecked = event.target.checked;
            getActionByType[actionIndex].isActionChecked = event.target.checked;
            let actionsToBeAdded = getAllActions.map(e => {
                if (e.isActionChecked) {
                    return e.id
                }
            })


            payload.actions = actionsToBeAdded.filter(e => !!e)

            getActionByType = getAllActions.filter(el => el.triggerType == this.state.activeAction);

        }
        else if (event.target.id == "name") {
            payload.name = event.target.value.trimStart()
        }
        else if (event.target.id == "duration" || event.target.id == "occurrence") {
            if (/[0-9]+/.test(event.target.value) && event.target.value > 0 && (parseInt(event.target.value) <= (event.target.id == "occurrence" ? 50 : 1500)))
                payload[event.target.id] = parseInt(event.target.value)
            else if (event.target.value == "") {
                payload[event.target.id] = ""
            }
            if (payload.duration < (((selectedConnector.reportInterval) / 60) * payload.occurrence)) {
                let durationMin = Math.ceil(((selectedConnector.reportInterval) / 60) * payload.occurrence)
                errorDurationMessage = "Value should be more than " + durationMin + " min(s)"
            } else {
                errorDurationMessage = ""
            }
        }
        else {
            payload[event.target.id] = event.target.value;
        }

        if (payload.deviceTypeId !== this.state.payload.deviceTypeId) {
            selectedConnector = this.state.connectorList.length > 0 && this.state.connectorList.find(e => e.deviceTypeId == payload.deviceTypeId)
            deviceGroupByConnector = selectedConnector.deviceGroups.map(group => {
                return {
                    label: group.name,
                    value: group.deviceGroupId
                }
            })
            isFetchingAttribute = selectedConnector.attributesAvailable;

            isFetchingDevices = true
            payload.deviceGroupId = [];
            payload.excludeIds = [];
            payload.actions = [];
            payload.rpcConfigs = [];
            selectedID = [];
            selectedGroup = [];
            attributeList = []
            payload.condition = {
                operand: "OR",
                conditionList: [{
                    operand: "OR",
                    conditionList: [
                        {
                            displayName: "",
                            path: "",
                            value: "",
                            operation: "EQUALS_TO",
                            attribute: "",
                            unit: ""
                        }
                    ]
                }]
            }

            getAllActions = getAllActions.map(el => {
                el.isActionChecked = false;
                return el
            })

            //this.props.getAllRpc(payload.deviceTypeId)

            if (selectedConnector.attributesAvailable) {
                this.props.getAttributeList(payload.deviceTypeId);
                let deviceQuotaPayload = [
                    {
                        dependingProductId: selectedConnector.deviceTypeId,
                        productName: "rules"
                    },
                    {
                        dependingProductId: null,
                        productName: "sms"
                    },
                    {
                        dependingProductId: null,
                        productName: "email"
                    },
                    {
                        dependingProductId: null,
                        productName: "webhooks"
                    },
                    {
                        dependingProductId: null,
                        productName: "rpc"
                    }
                ];
                this.props.getDeveloperQuota(deviceQuotaPayload);
            }
            selectedDeviceTypesRPCs = this.state.rpcList.filter(rpc => rpc.metaData.deviceTypeId === payload.deviceTypeId)
            selectedRPCDeviceTypes = [this.state.connectorList.find(connector => connector.deviceTypeId == payload.deviceTypeId)]
        }

        this.setState({
            payload,
            getAllActions,
            getActionByType,
            errorDurationMessage,
            selectedConnector,
            isFetchingAttribute,
            attributeList,
            isFetchingDevices,
            selectedID,
            deviceGroupByConnector,
            selectedGroup,
            selectedRPCDeviceTypes,
            selectedDeviceTypesRPCs
        })
    }

    rpcActionChangeHandler = ({ id, metaData }) => {
        let payload = cloneDeep(this.state.payload);
        let rpcConfigIndex = payload.rpcConfigs.findIndex(rpc => rpc.rpcId == id)
        if (rpcConfigIndex !== -1) {
            payload.rpcConfigs.splice(rpcConfigIndex, 1);
        } else {
            payload.rpcConfigs.push({
                toDeviceTypeId: metaData.deviceTypeId,
                rpcId: id,
                deviceIds: []
            })
        }
        this.setState({
            payload
        })
    }

    handleChange = selectedID => {
        let payload = cloneDeep(this.state.payload);
        payload.excludeIds = [...new Set(selectedID.map(e => e.value))]
        this.setState({ selectedID, payload });
    };

    addConditionHandler = () => {
        let payload = cloneDeep(this.state.payload);
        payload.condition.operand = payload.condition.conditionList.length == 1 ? "OR" : this.state.payload.condition.operand
        payload.condition.conditionList.push(conditionsTemp)
        this.setState({ payload })
    }

    addConditionListHandler = (index) => {
        let payload = cloneDeep(this.state.payload);
        payload.condition.conditionList[index].conditionList.push(conditionsTemp.conditionList[0])

        if (payload.condition.conditionList[index].conditionList.length == 1) {
            payload.condition.conditionList[index].operand = "OR"
        } else {
            payload.condition.conditionList[index].operand = this.state.payload.condition.conditionList[index].operand
        }
        this.setState({ payload })
    }

    deleteConditionHandler = (id, listIndex) => {
        let payload = cloneDeep(this.state.payload);
        if (typeof listIndex == "number") {
            payload.condition.conditionList[id].conditionList = payload.condition.conditionList[id].conditionList.filter((el, id) => id !== listIndex)
            payload.condition.conditionList[id].operand = payload.condition.conditionList[id].conditionList.length == 1 ? "OR" : this.state.payload.condition.conditionList[id].operand
        } else {
            payload.condition.conditionList = payload.condition.conditionList.filter((e, index) => index !== id)
            payload.condition.operand = payload.condition.conditionList.length == 1 ? "OR" : this.state.payload.condition.operand
        }

        this.setState({ payload })
    }

    conditionChangeHandler = (event, index, listIndex) => {
        let payload = cloneDeep(this.state.payload);

        if (typeof index == "number") {
            if (event.target.id == "operand") {
                if (payload.condition.conditionList[index].conditionList.length > 1) {
                    payload.condition.conditionList[index].operand = event.target.value;
                }
            } else {
                if (event.target.id == "attribute") {
                    let attributeObj = this.state.attributeList.find(el => el.attribute == event.target.value);
                    payload.condition.conditionList[index].conditionList[listIndex].attribute = event.target.value;
                    payload.condition.conditionList[index].conditionList[listIndex].value = typeof attributeObj.value == "number" ? parseFloat(attributeObj.value) : attributeObj.value
                    payload.condition.conditionList[index].conditionList[listIndex].unit = attributeObj.unit;
                    payload.condition.conditionList[index].conditionList[listIndex].displayName = attributeObj.displayName;
                } else {
                    if (event.target.id == "value") {
                        let attributeObj = this.state.attributeList.find(el => el.attribute == payload.condition.conditionList[index].conditionList[listIndex].attribute)
                        payload.condition.conditionList[index].conditionList[listIndex][event.target.id] = (typeof attributeObj.value == "number" && event.target.value) ? parseFloat(event.target.value) : typeof attributeObj.value === "boolean" ? Boolean(event.target.value) : event.target.value;
                    } else {
                        payload.condition.conditionList[index].conditionList[listIndex][event.target.id] = event.target.value;
                    }
                }
            }
        } else {
            if (payload.condition.conditionList.length > 1) {
                payload.condition.operand = event.target.value;
            }
        }
        this.setState({ payload })
    }

    onSaveEventHandler = () => {
        let payload = cloneDeep(this.state.payload)
        payload.duration = parseInt(this.state.payload.duration)
        payload.occurrence = parseInt(this.state.payload.occurrence)
        payload.name = this.state.payload.name.trim()
        this.setState({ isFetching: true })
        if (this.props.match.params.id) {
            this.props.updateEvent(this.props.match.params.id, payload)
        } else {
            this.props.createEvent(payload)
        }
    }

    onCloseHandler = () => {
        this.setState({
            isOpen: false,
            type: '',
            message2: ''
        })
    }

    handleChangeTags = (tags) => {
        let triggerActionsPayload = cloneDeep(this.state.triggerActionsPayload)
        triggerActionsPayload.metaData.emails = tags
        this.setState({ triggerActionsPayload })
    }

    actionsHandler = (event, index) => {
        let triggerActionsPayload = cloneDeep(this.state.triggerActionsPayload)
        triggerActionsPayload.triggerType = this.state.activeAction
        if (this.state.activeAction == "webhook" && event.target.id !== "name") {
            if (typeof index == "number") {
                if (event.target.id === "key" && /^[\w-_]+$/.test(event.target.value) || event.target.value === "" || event.target.id !== "key")
                    triggerActionsPayload.metaData.headers[index][event.target.id] = event.target.value
            } else {
                triggerActionsPayload.metaData[event.target.id] = event.target.value
            }
        }
        else if (this.state.activeAction == "rpc" && event.target.id !== "name") {
            triggerActionsPayload.metaData.controlName = event.target.value
            let rpcObj = this.state.allRpcList.find(el => el.controlName == event.target.value)
            triggerActionsPayload.metaData.command = rpcObj && rpcObj.controlCommand
        }

        else {
            triggerActionsPayload[event.target.id] = event.target.value
        }
        this.setState({ triggerActionsPayload })
    }

    createActionHandler = (event) => {
        event.preventDefault()
        let triggerActionsPayload = cloneDeep(this.state.triggerActionsPayload)
        triggerActionsPayload.name = this.state.triggerActionsPayload.name.trim()
        if (this.state.activeAction == "webhook" && triggerActionsPayload.metaData.headers) {
            let headersObj = Object.fromEntries(this.state.triggerActionsPayload.metaData.headers.map(item => [item.key, item.value]));
            triggerActionsPayload.metaData.headers = headersObj
        }
        else if (this.state.activeAction == "rpc") {
            triggerActionsPayload.metaData.command = triggerActionsPayload.metaData.command
        }

        if (this.state.actionToBeEdited) {
            this.setState({ isCreateOrDeleteActionLoader: true }, () => this.props.updateAction(this.state.actionToBeEdited, triggerActionsPayload))
        } else {
            this.setState({ isCreateOrDeleteActionLoader: true }, () => this.props.createAction(triggerActionsPayload))
        }
    }

    tabChangeHandler = (tab) => {
        let getActionByType = cloneDeep(this.state.getActionByType)
        let triggerActionsPayload = cloneDeep(this.state.triggerActionsPayload);
        getActionByType = this.state.getAllActions.filter(el => el.triggerType == tab);

        triggerActionsPayload.name = ""
        triggerActionsPayload.triggerType = ""
        triggerActionsPayload.metaData = {}

        if (tab == "email") {
            triggerActionsPayload.metaData.emails = []
        } else if (tab == "webhook") {
            triggerActionsPayload.metaData = {
                url: "",
                method: "",
                payload: "",
                headers: [],
                deviceTypeId: this.state.payload.deviceTypeId
            }
        } else if (tab == "sms") {
            triggerActionsPayload.metaData = {
                numbers: [{
                    number: "",
                    country: {
                        value: "",
                        label: ""
                    }
                }]
            }
        } else {
            triggerActionsPayload.metaData = {
                command: "",
            }
        }
        this.setState({ getActionByType, activeAction: tab, triggerActionsPayload, isAddorEditActions: false, isEditActionMode: false, actionToBeEdited: '' })
    }

    telChangeHandler = (value, country, e, formattedValue) => {
        let triggerActionsPayload = cloneDeep(this.state.triggerActionsPayload);
        triggerActionsPayload.metaData.numbers = [{
            number: value,
            country: {
                value: "+" + country.dialCode,
                label: country.name
            }
        }]
        this.setState({ triggerActionsPayload })
    }

    addHeadersHandler = () => {
        let triggerActionsPayload = cloneDeep(this.state.triggerActionsPayload);
        triggerActionsPayload.metaData.headers = triggerActionsPayload.metaData.headers ? triggerActionsPayload.metaData.headers : []
        triggerActionsPayload.metaData.headers.push({
            key: "",
            value: ""
        })
        this.setState({ triggerActionsPayload })
    }

    onChangeAceEditor = (newValue) => {
        let triggerActionsPayload = cloneDeep(this.state.triggerActionsPayload)
        triggerActionsPayload.metaData.payload = newValue
        this.setState({ triggerActionsPayload })
    }

    deleteActionHandler = (id) => {
        this.setState({ isCreateOrDeleteActionLoader: true }, () => this.props.deleteAction(id), this.tabChangeHandler(this.state.activeAction))
    }

    editActionHandler = (id, isForRPC) => {
        let editObj = this.state[isForRPC ? "allRpcList" : "getAllActions"].find(el => el.id == id);
        let url = window.location.origin + "/actions/" + (isForRPC ? "rpc" : editObj.triggerType) + "/" + id
        let win = window.open(url, '_blank');
        win.focus();
    }

    getActionButtonHtml = () => {
        return (
            <div className="form-content-footer pl-0 pr-0">
                <button className="btn btn-success mr-r-15" disabled={this.disableOrEnableActionSave() || this.props.location.state.isViewMode}>{this.state.isEditActionMode ? "Update" : "Save"}</button>
                <button type="button" className="btn btn-light" onClick={() => this.setState({ isAddorEditActions: false, isEditActionMode: false })}>Cancel</button>
            </div>
        )
    }

    disableOrEnableActionSave = () => {
        let isDisableSave = true
        let triggerActionsPayload = cloneDeep(this.state.triggerActionsPayload)
        switch (this.state.activeAction) {
            case "sms":
                isDisableSave = (triggerActionsPayload.name && triggerActionsPayload.metaData.numbers[0].number.length > 1) ? false : true
                break;
            case "email":
                isDisableSave = (triggerActionsPayload.name && triggerActionsPayload.metaData.emails.length) ? false : true
                break;
            case "rpc":
                isDisableSave = (triggerActionsPayload.name && triggerActionsPayload.metaData.command) ? false : true
                break;
            case "webhook":
                isDisableSave = (triggerActionsPayload.name && triggerActionsPayload.metaData.method && triggerActionsPayload.metaData.url) ? false : true
                break;
            default:
                break;
        }
        return isDisableSave
    }

    addNewActionHandler = () => {
        this.props.history.push({ pathname: "/actions/" + this.state.activeAction, state: { deviceTypeId: this.state.payload.deviceTypeId } })
    }


    enableOrDisableSaveButton = () => {

        let { isAddorEditActions } = this.state
        let { rpcConfigs, actions, deviceGroupId, duration, name, occurrence, deviceTypeId } = this.state.payload;
        return (!deviceGroupId.length || !name.length || typeof occurrence == "string" || typeof duration == "string" || !deviceTypeId.length || isAddorEditActions || (rpcConfigs.length ? rpcConfigs.some(config => config.toDeviceTypeId !== deviceTypeId && !config.deviceIds.length) : !actions.length))
    }

    deleteHeadersHandler = (index) => {
        let triggerActionsPayload = cloneDeep(this.state.triggerActionsPayload)
        triggerActionsPayload.metaData.headers = triggerActionsPayload.metaData.headers.filter((el, id) => id !== index)
        this.setState({ triggerActionsPayload })
    }

    checkInputType = (index, listIndex) => {
        let attributeObj = this.state.attributeList.find(el => el.attribute == this.state.payload.condition.conditionList[index].conditionList[listIndex].attribute)
        return attributeObj && typeof attributeObj.value == "number" ? "number" : "text"
    }

    disableConditionButton = () => {
        let payload = cloneDeep(this.state.payload),
            count = 0,
            isDisable = false,
            ruleQoutaObj = this.state.developerQuota.length && this.state.developerQuota.find(e => e.productName == "rules")

        for (let i = 0; i < payload.condition.conditionList.length; i++) {
            count = count + payload.condition.conditionList[i].conditionList.length
            if (count === ruleQoutaObj.total) {
                isDisable = true
            }
        }
        return isDisable
    }
    getRulesQuota = () => {
        let payload = cloneDeep(this.state.payload),
            rulesQuota = cloneDeep(this.state.rulesQuota),
            rules = {
                remaining: rulesQuota.remaining,
                total: rulesQuota.total,
                used: 0,
            }
        if (rules.total > 0 && this.state.selectedConnector.attributesAvailable) {
            payload.condition.conditionList.map(conditionObj => {
                conditionObj.conditionList.map(subCondition => {
                    rules.used = rules.used + 1;
                    rules.remaining = rules.remaining - 1;
                })
            })
        }
        return rules
    }

    handleGroupChange = (selectedGroup) => {
        let payload = cloneDeep(this.state.payload),
            getDeviceIDPayload = cloneDeep(this.state.getDeviceIDPayload)

        payload.deviceGroupId = [...new Set(selectedGroup.map(e => e.value))]
        getDeviceIDPayload.deviceTypeId = this.state.selectedConnector.deviceTypeId
        getDeviceIDPayload.deviceGroupIds = payload.deviceGroupId
        payload.excludeIds = []
        this.setState({ selectedGroup, payload, isFetchingDevices: true }, () => this.props.getAllDevicesByConnectorId(getDeviceIDPayload)
        )
    }

    getWarningLabel = () => {
        let label = '',
            count = 0;
        if (this.state.smsQuota.remaining === 0) {
            label += 'SMS';
            count++;
        }
        if (this.state.emailQuota.remaining === 0) {
            count === 0 ? label += 'Email' : label += ', Email';
            count++;
        }
        if (this.state.rpcQuota.remaining === 0) {
            count === 0 ? label += 'Controls' : label += ', Controls';
            count++;
        }
        if (this.state.webhookQuota.remaining === 0) {
            count === 0 ? label += 'Webhook' : label += ', Webhook';
            count++;
        }
        return ` You have utilised all of your quota for ${label}`;
    }

    showCommand = (selectedCommand) => {
        this.setState({
            selectedCommand,
            showCommand: true
        })
    }


    RPCDeviceTypeChangeHandler = (selectedRPCDeviceTypes) => {
        let selectedDeviceTypesRPCs = this.state.rpcList.filter(rpc => selectedRPCDeviceTypes.some(deviceType => deviceType.value === rpc.metaData.deviceTypeId))
        let payload = cloneDeep(this.state.payload)
        payload.rpcConfigs = payload.rpcConfigs.filter(({ rpcId }) => {
            let requiredRPC = this.state.selectedDeviceTypesRPCs.find(rpc => rpc.id === rpcId)
            let RPCDeviceType = requiredRPC.metaData.deviceTypeId
            return selectedRPCDeviceTypes.some(deviceType => deviceType.value === RPCDeviceType)
        })
        this.setState({ selectedRPCDeviceTypes, selectedDeviceTypesRPCs, payload })
    }

    getRPCDevicesValue = (allDevices = [], rpcId) => {
        let RPCIndex = this.state.payload.rpcConfigs.findIndex(config => config.rpcId === rpcId)
        if (RPCIndex !== -1) {
            return allDevices.filter(device => this.state.payload.rpcConfigs[RPCIndex].deviceIds.includes(device._uniqueDeviceId))
        }
        return null
    }


    RPCDeviceChangeHandler = (selectedDevices, rpcId) => {
        let payload = cloneDeep(this.state.payload)
        let RPCIndex = payload.rpcConfigs.findIndex(config => config.rpcId === rpcId)
        payload.rpcConfigs[RPCIndex].deviceIds = selectedDevices.map(device => device.value)
        this.setState({ payload })
    }

    handleDeviceTypeChange = (deviceType) => {
        let payload = cloneDeep(this.state.payload),
            getAllActions = cloneDeep(this.state.getAllActions),
            getActionByType = cloneDeep(this.state.getActionByType),
            selectedConnector = cloneDeep(this.state.selectedConnector),
            attributeList = cloneDeep(this.state.attributeList),
            errorDurationMessage = this.state.errorDurationMessage,
            isFetchingAttribute = this.state.isFetchingAttribute,
            isFetchingDevices = false,
            selectedID = cloneDeep(this.state.selectedID),
            deviceGroupByConnector = cloneDeep(this.state.deviceGroupByConnector),
            selectedGroup = cloneDeep(this.state.selectedGroup),
            selectedRPCDeviceTypes = this.state.selectedRPCDeviceTypes,
            selectedDeviceTypesRPCs = this.state.selectedDeviceTypesRPCs
        payload.deviceTypeId = deviceType.value;

        if (payload.deviceTypeId !== this.state.payload.deviceTypeId) {
            selectedConnector = this.state.connectorList.length > 0 && this.state.connectorList.find(e => e.deviceTypeId == payload.deviceTypeId)
            deviceGroupByConnector = selectedConnector.deviceGroups.map(group => {
                return {
                    label: group.name,
                    value: group.deviceGroupId
                }
            })
            isFetchingAttribute = selectedConnector.attributesAvailable;

            isFetchingDevices = true
            payload.deviceGroupId = [];
            payload.excludeIds = [];
            payload.actions = [];
            payload.rpcConfigs = [];
            selectedID = [];
            selectedGroup = [];
            attributeList = []
            payload.condition = {
                operand: "OR",
                conditionList: [{
                    operand: "OR",
                    conditionList: [
                        {
                            displayName: "",
                            path: "",
                            value: "",
                            operation: "EQUALS_TO",
                            attribute: "",
                            unit: ""
                        }
                    ]
                }]
            }

            getAllActions = getAllActions.map(el => {
                el.isActionChecked = false;
                return el
            })

            if (selectedConnector.attributesAvailable) {
                this.props.getAttributeList(payload.deviceTypeId);
                let deviceQuotaPayload = [
                    {
                        dependingProductId: selectedConnector.deviceTypeId,
                        productName: "rules"
                    },
                    {
                        dependingProductId: null,
                        productName: "sms"
                    },
                    {
                        dependingProductId: null,
                        productName: "email"
                    },
                    {
                        dependingProductId: null,
                        productName: "webhooks"
                    },
                    {
                        dependingProductId: null,
                        productName: "rpc"
                    }
                ];
                this.props.getDeveloperQuota(deviceQuotaPayload);
            }
            selectedDeviceTypesRPCs = this.state.rpcList.filter(rpc => rpc.metaData.deviceTypeId === payload.deviceTypeId)
            selectedRPCDeviceTypes = [this.state.connectorList.find(connector => connector.deviceTypeId == payload.deviceTypeId)]
        }

        this.setState({
            payload,
            getAllActions,
            getActionByType,
            errorDurationMessage,
            selectedConnector,
            isFetchingAttribute,
            attributeList,
            isFetchingDevices,
            selectedID,
            deviceGroupByConnector,
            selectedGroup,
            selectedRPCDeviceTypes,
            selectedDeviceTypesRPCs
        })
    }
    deviceTypeOptions = (connectorList) => {
        let option = [];
        connectorList.map(temp => {
            option.push({
                label: temp.name,
                value: temp.deviceTypeId
            })
        })
        return option;
    }

    getSelectedDeviceType = (deviceTypeId) => {
        if (deviceTypeId) {
            let connectorList = this.state.connectorList
            return {
                value: deviceTypeId,
                label: connectorList.find(item => item.deviceTypeId === deviceTypeId).name,
            }
        }
    }

    render() {
        let rulesQuota = this.getRulesQuota();
        let isViewMode = this.props.history.location.state && this.props.history.location.state.isViewMode
        return (
            <React.Fragment>
                <Helmet>
                    <title>AddOrEditEvent</title>
                    <meta name="description" content="Description of AddOrEditEvent" />
                </Helmet>

                <header className="content-header d-flex">
                    <div className="flex-60">
                        <div className="d-flex">
                            <h6 className="previous" onClick={() => this.props.history.push('/events')}>Events</h6>
                            <h6 className="active">{this.props.match.params.id ? this.state.payload.name : "Add New"}</h6>
                        </div>
                    </div>
                    <div className="flex-40 text-right">
                        <div className="content-header-group">
                            {this.state.developerQuota.length > 0 && (this.state.smsQuota.remaining === 0 || this.state.emailQuota.remaining === 0 || this.state.rpcQuota.remaining === 0 || this.state.webhookQuota.remaining === 0) &&
                                <span className="text-red f-12 mr-2"><i className="far fa-exclamation-triangle"></i>{this.getWarningLabel()}</span>
                            }
                            <button className="btn btn-light" data-tooltip data-tooltip-text="Back" data-tooltip-place="bottom" onClick={() => this.props.history.push('/events')}><i className="fas fa-angle-left"></i></button>
                        </div>
                    </div>
                </header>

                {this.state.isFetching ?
                    <Loader /> :
                    <div className="content-body">
                        <div className="form-content-wrapper form-content-wrapper-left">
                            <div className="form-content-box">
                                <div className="form-content-header">
                                    <p>Triggers / Conditions
                                        <span className="allocation-badge-group float-right">
                                            <span className="allocation-badge-item">Allocated <span className="badge badge-pill badge-primary">{rulesQuota.total}</span></span>
                                            <span className="allocation-badge-item">Used <span className="badge badge-pill badge-success">{rulesQuota.used}</span></span>
                                            <span className="allocation-badge-item">Remaining <span className="badge badge-pill badge-warning">{rulesQuota.remaining}</span></span>
                                        </span>
                                    </p>
                                </div>

                                {this.state.isFetchingAttribute ?
                                    <div className="form-content-body">
                                        <div className="inner-loader-wrapper" style={{ height: 321 }}>
                                            <div className="inner-loader-content">
                                                <i className="fad fa-sync-alt fa-spin f-20"></i>
                                            </div>
                                        </div>
                                    </div>
                                    :
                                    <div className="form-content-body">
                                        <div className="form-group condition-box">
                                            <div className="condition-select">
                                                <select className="form-control" value={this.state.payload.condition.operand} onChange={() => this.conditionChangeHandler(event)}>
                                                    <option value="AND">AND</option>
                                                    <option value="OR">OR</option>
                                                </select>
                                            </div>
                                            {this.state.payload.condition && this.state.payload.condition.conditionList.length > 0 && this.state.payload.condition.conditionList.map((condition, index) =>
                                                <div className="condition-box-list" key={index}>
                                                    <div className="condition-select">
                                                        <select className="form-control" id="operand"
                                                            value={this.state.payload.condition.conditionList[index].operand}
                                                            onChange={() => this.conditionChangeHandler(event, index)}>
                                                            <option value="AND">AND</option>
                                                            <option value="OR">OR</option>
                                                        </select>
                                                        <button className="btn btn-primary" data-tooltip data-tooltip-text="Add Condition" data-tooltip-place="bottom" onClick={() => this.addConditionListHandler(index)} disabled={((this.state.attributeList.length == 0 && !this.state.selectedConnector.attributesAvailable) || this.disableConditionButton() || isViewMode)}><i className="far fa-plus"></i></button>
                                                    </div>
                                                    <div className="d-flex pd-r-30">
                                                        <label className="form-group-label flex-40">Attribute :</label>
                                                        <label className="form-group-label flex-20">Operation :</label>
                                                        <label className="form-group-label flex-35">Value :</label>
                                                        <label className="form-group-label flex-5">Unit</label>
                                                    </div>
                                                    {condition.conditionList && condition.conditionList.map((list, listIndex) =>
                                                        <ul className="list-style-none d-flex" key={listIndex}>
                                                            <li className="flex-40">
                                                                <select className="form-control" id="attribute" value={list.attribute}
                                                                    onChange={() => this.conditionChangeHandler(event, index, listIndex)}
                                                                    disabled={(this.state.attributeList.length == 0 || !this.state.selectedConnector.attributesAvailable || isViewMode)}
                                                                >
                                                                    {(this.state.attributeList.length > 0 && this.state.selectedConnector.attributesAvailable) ? this.state.attributeList.map((attribute, index) =>
                                                                        <option key={index} value={attribute.attribute}>{attribute.displayName ? attribute.displayName : attribute.attribute}</option>
                                                                    )
                                                                        :
                                                                        <option value="">No Attribute Available</option>}
                                                                </select>
                                                            </li>
                                                            <li className="flex-20">
                                                                <select className="form-control" id="operation" value={list.operation}
                                                                    onChange={() => this.conditionChangeHandler(event, index, listIndex)} disabled={(this.state.attributeList.length == 0 || !this.state.selectedConnector.attributesAvailable || isViewMode)}>
                                                                    <option value="EQUALS_TO">{"="}</option>
                                                                    <option value="NOT_EQUALS_TO">{"!="}</option>
                                                                    {(typeof list.value == this.checkInputType(index, listIndex) && typeof list.value != "boolean") &&
                                                                        <React.Fragment>
                                                                            <option value="LESS_THAN">{"<"}</option>
                                                                            <option value="GREATER_THAN">{">"}</option>
                                                                            <option value="LESS_THAN_EQUALS_TO">{"<="}</option>
                                                                            <option value="GREATER_THAN_EQUALS_TO">{">="}</option>
                                                                        </React.Fragment>
                                                                    }
                                                                </select>
                                                            </li>
                                                            <li className="flex-40">
                                                                <div className="input-group">
                                                                    {typeof list.value === "boolean" ?
                                                                        <select
                                                                            className="form-control"
                                                                            id="value"
                                                                            value={list.value}
                                                                            onChange={() => this.conditionChangeHandler(event, index, listIndex)}
                                                                            disabled={(this.state.attributeList.length == 0 || !this.state.selectedConnector.attributesAvailable || isViewMode)}
                                                                        >
                                                                            <option value={true}>true</option>
                                                                            <option value={false}>false</option>
                                                                        </select> :
                                                                        <input type={this.checkInputType(index, listIndex)} id="value" className="form-control" value={list.value} onChange={() => this.conditionChangeHandler(event, index, listIndex)} disabled={(this.state.attributeList.length == 0 || !this.state.selectedConnector.attributesAvailable || isViewMode)} />
                                                                    }
                                                                    <div className="input-group-append">
                                                                        <span className="input-group-text">{list.unit ? list.unit : "N/A"}</span>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <div className="button-group">
                                                                <button className="btn-transparent btn-transparent-red" data-tooltip data-tooltip-text="Delete" data-tooltip-place="bottom" disabled={condition.conditionList.length == 1 || isViewMode}
                                                                    onClick={() => this.deleteConditionHandler(index, listIndex)}>
                                                                    <i className="far fa-trash-alt"></i>
                                                                </button>
                                                            </div>
                                                        </ul>
                                                    )}
                                                    <button className="btn btn-light box-top-right-button" data-tooltip data-tooltip-text="Remove" data-tooltip-place="bottom" disabled={this.state.payload.condition.conditionList.length == 1} onClick={() => this.deleteConditionHandler(index)}>
                                                        <i className="far fa-times"></i>
                                                    </button>
                                                </div>
                                            )}
                                            <div className="text-center">
                                                <button className="btn btn-link" onClick={() => this.addConditionHandler()} disabled={((this.state.attributeList.length == 0 && !this.state.selectedConnector.attributesAvailable) || this.disableConditionButton()) || isViewMode}><i className="far fa-plus"></i>Add New</button>
                                            </div>
                                        </div>

                                        <div className="d-flex">
                                            <div className="flex-40 pd-r-10 ">
                                                <div className="form-group">
                                                    <label className="form-group-label">Occurrence (Consecutive occurrence of condition(s)) :<i className="fas fa-asterisk form-group-required"></i></label>
                                                    <div className="input-group">
                                                        <input type="number" id="occurrence" className="form-control" value={this.state.payload.occurrence} disabled={isViewMode} onChange={() => this.eventChangeHandler(event)} />
                                                        <div className="input-group-append">
                                                            <span className="input-group-text">times</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="flex-60 pd-l-10">
                                                <div className="form-group">
                                                    <label className="form-group-label">Trigger Action Interval = <strong className="text-dark">Reporting Frequency * Occurrence :  <i className="fas fa-asterisk form-group-required"></i></strong></label>
                                                    <div className="input-group">
                                                        <input type="number" id="duration" className="form-control" value={this.state.payload.duration} disabled={isViewMode} onChange={() => this.eventChangeHandler(event)} />
                                                        <div className="input-group-append">
                                                            <span className="input-group-text">mins</span>
                                                        </div>
                                                    </div>
                                                    {this.state.errorDurationMessage && <span className="form-group-error">{this.state.errorDurationMessage}</span>}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                }
                            </div>

                            <div className="form-content-box">
                                <div className="form-content-header">
                                    <p>Actions
                                        <span className="allocation-badge-group float-right">
                                            <span className="allocation-badge-item">Allocated <span className="badge badge-pill badge-primary">{this.state[this.state.activeAction + "Quota"].total}</span></span>
                                            <span className="allocation-badge-item">Used <span className="badge badge-pill badge-success">{this.state[this.state.activeAction + "Quota"].used}</span></span>
                                            <span className="allocation-badge-item">Remaining <span className="badge badge-pill badge-warning">{this.state[this.state.activeAction + "Quota"].remaining}</span></span>
                                        </span>
                                    </p>
                                </div>

                                <div className="form-content-body">
                                    <div className="action-tabs">
                                        <ul className="nav nav-tabs list-style-none d-flex">
                                            <li className={`nav-item ${this.state.activeAction == "sms" ? "active" : null}`} href="#tab1" data-toggle="tab" onClick={() => this.tabChangeHandler("sms")}>
                                                <a className="nav-link"><i className="fad fa-comment-lines"></i>SMS</a>
                                            </li>
                                            <li className={`nav-item ${this.state.activeAction == "email" ? "active" : null}`} href="#tab2" data-toggle="tab" onClick={() => this.tabChangeHandler("email")}>
                                                <a className="nav-link"><i className="fad fa-envelope"></i>Email</a>
                                            </li>
                                            <li className={`nav-item ${this.state.activeAction == "rpc" ? "active" : null}`} href="#tab3" data-toggle="tab" onClick={() => this.tabChangeHandler("rpc")}>
                                                <a className="nav-link"><i className="fad fa-terminal"></i>Controls</a>
                                            </li>
                                            <li className={`nav-item ${this.state.activeAction == "webhook" ? "active" : null}`} href="#tab4" data-toggle="tab" onClick={() => this.tabChangeHandler("webhook")}>
                                                <a className="nav-link"><i className="fad fa-code-merge"></i>Webhook</a>
                                            </li>
                                            {isNavAssigned('actions') && <button className="btn btn-link" onClick={() => this.addNewActionHandler()}><i className="far fa-plus"></i>
                                                    Add {activeAction[this.state.activeAction]}
                                            </button>
                                            }
                                        </ul>
                                    </div>
                                    {this.state.isCreateOrDeleteActionLoader ?
                                        <div className="inner-loader-wrapper" style={{ height: 300 }}>
                                            <div className="inner-loader-content">
                                                <i className="fad fa-sync-alt fa-spin f-20"></i>
                                            </div>
                                        </div>
                                        :
                                        <div className="tab-content">
                                            {this.state.activeAction == "sms" ?
                                                <div className={`tab-pane ${this.state.activeAction == "sms" ? "active" : null}`}>
                                                    <div className="content-table action-tabs-table">
                                                        {this.state.getActionByType && this.state.getActionByType.length ?
                                                            <table className="table table-bordered m-0">
                                                                <thead>
                                                                    <tr>
                                                                        <th width="10%"></th>
                                                                        <th width="40%">Action Name</th>
                                                                        <th width="40%">Mobile No.</th>
                                                                        <th width="10%">Actions</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    {this.state.getActionByType.map((action, actionIndex) =>
                                                                        <tr key={actionIndex}>
                                                                            <td>
                                                                                <label className={isViewMode ? "check-box-disabled check-box" : "check-box"}>
                                                                                    <input
                                                                                        type="checkbox"
                                                                                        id="addActionToEvent"
                                                                                        disabled={isViewMode}
                                                                                        value={action.id}
                                                                                        checked={this.state.payload.actions.includes(action.id)}
                                                                                        onChange={() => this.eventChangeHandler(event, actionIndex)}
                                                                                    />
                                                                                    <span className="check-mark"></span>
                                                                                </label>
                                                                            </td>
                                                                            <td>{action.name}</td>
                                                                            <td>
                                                                                <div className="list-phone-input">
                                                                                    <PhoneInput
                                                                                        inputProps={{
                                                                                            name: 'phone',
                                                                                        }}
                                                                                        disabled={true}
                                                                                        disableDropdown={true}
                                                                                        value={action.metaData.numbers[0].number}
                                                                                        onChange={(value) => null}
                                                                                    />
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div className="button-group">
                                                                                    <button type="button" className="btn-transparent btn-transparent-blue" disabled={isViewMode} data-tooltip data-tooltip-text="Edit" data-tooltip-place="bottom" onClick={() => this.editActionHandler(action.id)}>
                                                                                        <i className="far fa-pencil"></i>
                                                                                    </button>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    )}
                                                                </tbody>
                                                            </table>
                                                            :
                                                            <p className="text-gray p-5 text-center">There is no data to display.</p>
                                                        }
                                                    </div>
                                                </div>
                                                :
                                                this.state.activeAction == "email" ?
                                                    <div className={`tab-pane ${this.state.activeAction == "email" ? "active" : null}`}>
                                                        <div className="content-table action-tabs-table">
                                                            {this.state.getActionByType && this.state.getActionByType.length ?
                                                                <table className="table table-bordered m-0">
                                                                    <thead>
                                                                        <tr>
                                                                            <th width="10%"></th>
                                                                            <th width="40%">Action Name</th>
                                                                            <th width="40%">Emails</th>
                                                                            <th width="10%">Actions</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        {this.state.getActionByType && this.state.getActionByType.map((action, actionIndex) =>
                                                                            <tr key={actionIndex}>
                                                                                <td>
                                                                                    <label className={isViewMode ? "check-box-disabled check-box" : "check-box"}>
                                                                                        <input
                                                                                            type="checkbox"
                                                                                            id="addActionToEvent"
                                                                                            value={action.id}
                                                                                            disabled={isViewMode}
                                                                                            checked={this.state.payload.actions.includes(action.id)}
                                                                                            onChange={() => this.eventChangeHandler(event, actionIndex)}
                                                                                        />
                                                                                        <span className="check-mark"></span>
                                                                                    </label>
                                                                                </td>
                                                                                <td>{action.name}</td>
                                                                                <td>
                                                                                    {action.metaData.emails && action.metaData.emails.map((email, index) => <a className="mr-2" href={`mailto:${email}`}>{email}</a>)}
                                                                                </td>
                                                                                <td>
                                                                                    <div className="button-group">
                                                                                        <button type="button" className="btn-transparent btn-transparent-blue" disabled={isViewMode} data-tooltip data-tooltip-text="Edit" data-tooltip-place="bottom" onClick={() => this.editActionHandler(action.id)}>
                                                                                            <i className="far fa-pencil"></i>
                                                                                        </button>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        )}
                                                                    </tbody>
                                                                </table>
                                                                :
                                                                <p className="text-content p-5 text-center">There is no data to display.</p>
                                                            }
                                                        </div>
                                                    </div>
                                                    :
                                                    this.state.activeAction == "rpc" ?
                                                        <div className={`tab-pane ${this.state.activeAction == "rpc" ? "active" : null}`}>
                                                            <div className="form-group">
                                                                <label className="form-group-label">Device Type(s) : <i className="fas fa-asterisk form-group-required"></i></label>
                                                                <Select
                                                                    className="form-control-multi-select"
                                                                    value={this.state.selectedRPCDeviceTypes}
                                                                    onChange={this.RPCDeviceTypeChangeHandler}
                                                                    options={this.state.connectorList}
                                                                    isDisabled={isViewMode}
                                                                    isMulti={true}
                                                                />
                                                            </div>
                                                            <div className="content-table action-tabs-table">
                                                                {this.state.selectedDeviceTypesRPCs && this.state.selectedDeviceTypesRPCs.length ?
                                                                    <table className="table table-bordered m-0">
                                                                        <thead>
                                                                            <tr>
                                                                                <th width="7.5%"></th>
                                                                                <th width="32.5%">Name</th>
                                                                                <th width="50%">Devices</th>
                                                                                <th width="10%">Actions</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            {this.state.selectedDeviceTypesRPCs.map((action, actionIndex) => {
                                                                                let isChecked = this.state.payload.rpcConfigs.some(config => config.rpcId === action.id)
                                                                                return (<tr key={actionIndex}>
                                                                                    <td>
                                                                                        <label className={isViewMode ? "check-box-disabled check-box" : "check-box"}>
                                                                                            <input
                                                                                                type="checkbox"
                                                                                                id="addActionToEvent"
                                                                                                value={action.id}
                                                                                                disabled={isViewMode}
                                                                                                checked={isChecked}
                                                                                                onChange={() => this.rpcActionChangeHandler(action)}
                                                                                            />
                                                                                            <span className="check-mark"></span>
                                                                                        </label>
                                                                                    </td>
                                                                                    <td className="position-relative pd-l-50">
                                                                                        <div className="content-table-image">
                                                                                            <img src={action.imageUrl || "https://content.iot83.com/m83/misc/device-type-fallback.png"}/>
                                                                                        </div>{action.name}
                                                                                    </td>
                                                                                    <td>
                                                                                        <Select
                                                                                            className="form-control-multi-select"
                                                                                            value={this.getRPCDevicesValue(this.state.allDevices[action.metaData.deviceTypeId], action.id)}
                                                                                            onChange={(val) => this.RPCDeviceChangeHandler(val, action.id)}
                                                                                            options={this.state.allDevices[action.metaData.deviceTypeId] || []}
                                                                                            isDisabled={isViewMode || !isChecked || (action.metaData.deviceTypeId === this.state.payload.deviceTypeId)}
                                                                                            isMulti={true}
                                                                                            placeholder={(action.metaData.deviceTypeId === this.state.payload.deviceTypeId) ? "All Devices except excluded ones." : "Select.."}
                                                                                        />
                                                                                        {/*<div className="d-flex">
                                                                                            <span className="flex-50  pd-r-30">{action.name}</span>
                                                                                            <div className="form-group flex-50">
                                                                                                <label className="form-group-label">Device(s) : <i className={"form-group-required " + (isChecked ? "fas fa-asterisk " : "")}></i></label>
                                                                                                
                                                                                            </div>
                                                                                        </div>*/}
                                                                                    </td>
                                                                                    <td>
                                                                                        <div className="button-group">
                                                                                            <button type="button" className="btn-transparent btn-transparent-blue" disabled={isViewMode} data-tooltip data-tooltip-text="Edit" data-tooltip-place="bottom" onClick={() => this.editActionHandler(action.id, true)}>
                                                                                                <i className="far fa-pencil"></i>
                                                                                            </button>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>)
                                                                            }
                                                                            )}
                                                                        </tbody>
                                                                    </table>
                                                                    :
                                                                    <p className="text-content p-5 text-center">There is no data to display.</p>
                                                                }
                                                            </div>
                                                        </div>
                                                        :
                                                        <div className="tab-pane active">
                                                            <div className="content-table action-tabs-table">
                                                                {this.state.getActionByType && this.state.getActionByType.length ?
                                                                    <table className="table table-bordered m-0">
                                                                        <thead>
                                                                            <tr>
                                                                                <th width="10%"></th>
                                                                                <th width="25%">Name</th>
                                                                                <th width="35%">URL</th>
                                                                                <th width="20%">Method</th>
                                                                                <th width="10%">Actions</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            {this.state.getActionByType && this.state.getActionByType.map((action, actionIndex) =>
                                                                                <tr key={actionIndex}>
                                                                                    <td>
                                                                                        <label
                                                                                            className={isViewMode ? "check-box-disabled check-box" : "check-box"}>
                                                                                            <input
                                                                                                type="checkbox"
                                                                                                id="addActionToEvent"
                                                                                                disabled={isViewMode}
                                                                                                value={action.id}
                                                                                                checked={this.state.payload.actions.includes(action.id)}
                                                                                                onChange={() => this.eventChangeHandler(event, actionIndex)}
                                                                                            />
                                                                                            <span className="check-mark"></span>
                                                                                        </label>
                                                                                    </td>
                                                                                    <td>{action.name}</td>
                                                                                    <td><a href="/actions" target="_blank">{action.metaData.url}</a></td>
                                                                                    <td className={action.metaData.method == 'GET' ? "text-primary" : action.metaData.method == 'POST' ? "text-green" : action.metaData.method == 'PUT' ? "text-yellow" : action.metaData.method == 'PATCH' ? "text-cyan" : "text-red"}>{action.metaData.method}</td>
                                                                                    <td>
                                                                                        <div className="button-group">
                                                                                            <button type="button" disabled={isViewMode} className="btn-transparent btn-transparent-blue" data-tooltip data-tooltip-text="Edit" data-tooltip-place="bottom" onClick={() => this.editActionHandler(action.id)}>
                                                                                                <i className="far fa-pencil"></i>
                                                                                            </button>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            )}
                                                                        </tbody>
                                                                    </table>
                                                                    :
                                                                    <p className="text-content p-5 text-center">There is no data to display.</p>
                                                                }
                                                            </div>
                                                        </div>
                                            }
                                        </div>
                                    }
                                </div>
                            </div>
                        </div>

                        <div className="form-info-wrapper form-info-wrapper-left">
                            <div className="form-info-header">
                                <h5>Basic Information</h5>
                            </div>
                            <div className="form-info-body form-info-body-adjust">
                                <div className="form-group">
                                    <label className="form-group-label">Device Type : <i className="fas fa-asterisk form-group-required"></i></label>
                                    <Select
                                        name="deviceType"
                                        isMulti={false}
                                        className="form-control-multi-select"
                                        options={this.deviceTypeOptions(this.state.connectorList)}
                                        onChange={this.handleDeviceTypeChange}
                                        value={this.getSelectedDeviceType(this.state.payload.deviceTypeId)}
                                        isDisabled={this.props.match.params.id || isViewMode}
                                    />
                                </div>
                                <div className="form-group">
                                    <label className="form-group-label">Asset Type :</label>
                                    <input type="text" id="assetType" className="form-control" disabled={true} value={this.state.connectorList.filter(deviceType => deviceType.deviceTypeId === this.state.payload.deviceTypeId)[0]['assetType'] ? this.state.connectorList.filter(deviceType => deviceType.deviceTypeId === this.state.payload.deviceTypeId)[0]['assetType'] : 'N/A'} />

                                </div>
                                <div className="form-group">
                                    <label className="form-group-label">Device Group(s) : <i className="fas fa-asterisk form-group-required"></i></label>
                                    <Select
                                        className="form-control-multi-select"
                                        value={this.state.selectedGroup}
                                        onChange={this.handleGroupChange}
                                        options={this.state.deviceGroupByConnector}
                                        isDisabled={isViewMode}
                                        isMulti={true}
                                    />
                                </div>
                                <div className="form-group">
                                    <label className="form-group-label">Name : <i className="fas fa-asterisk form-group-required"></i></label>
                                    <input type="text" id="name" className="form-control" disabled={isViewMode} value={this.state.payload.name} onChange={() => this.eventChangeHandler(event)} maxLength="30" />
                                </div>
                                <div className="form-group">
                                    <label className="form-group-label">Description :</label>
                                    <textarea rows="3" type="text" id="description" className="form-control" disabled={isViewMode} value={this.state.payload.description} onChange={() => this.eventChangeHandler(event)} />
                                </div>
                                <div className="form-group">
                                    <label className="form-group-label">Excluded Device(s) :</label>
                                    <Select
                                        className="form-control-multi-select"
                                        value={this.state.selectedID}
                                        onChange={this.handleChange}
                                        options={this.state.getAllDevicesOptions}
                                        isMulti={true}
                                        isDisabled={this.state.isFetchingDevices || isViewMode}
                                    />
                                </div>
                            </div>
                            <div className="form-info-footer">
                                <button className="btn btn-light" onClick={() => { this.props.history.push('/events') }}>Cancel</button>
                                <button className="btn btn-primary" onClick={() => this.onSaveEventHandler()}
                                    disabled={this.enableOrDisableSaveButton() || isViewMode || this.state.errorDurationMessage}>{this.props.match.params.id ? "Update" : "Save"}</button>
                            </div>
                        </div>
                    </div>
                }

                {this.state.showCommand &&
                    <div className="modal d-block animated slideInDown" id="commandModal" role="dialog">
                        <div className="modal-dialog modal-lg modal-dialog-centered">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h6 className="modal-title">Device Controls - <span>{this.state.selectedCommand.controlName}</span>
                                        <button type="button" className="close" onClick={() => this.setState({ showCommand: false })} data-dismiss="modal"><i className="far fa-times"></i></button>
                                    </h6>
                                </div>
                                <div className="modal-body">
                                    <div className="json-input-box" style={{ marginTop: "20px" }}>
                                        <JSONInput
                                            theme="light_mitsuketa_tribute"
                                            value={JSON.parse(this.state.selectedCommand.controlCommand)}
                                            viewOnly={true}
                                            placeholder={JSON.parse(this.state.selectedCommand.controlCommand)}
                                        />
                                    </div>
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-dark" onClick={() => this.setState({ showCommand: false })} data-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                }

                {this.state.isOpen &&
                    <NotificationModal
                        type={this.state.type}
                        message2={this.state.message2}
                        onCloseHandler={this.onCloseHandler}
                    />
                }
            </React.Fragment>
        );
    }
}

AddOrEditEvent.propTypes = {
    dispatch: PropTypes.func.isRequired
};

let allSelectors = {}
Object.entries(SELECTORS).map(([key, value]) => {
    allSelectors[key] = value()
})

const mapStateToProps = createStructuredSelector(allSelectors);


function mapDispatchToProps(dispatch) {
    let allActions = { dispatch }
    Object.entries(ACTIONS).map(([key, value]) => {
        allActions[key] = (...args) => dispatch(value(...args))
    })
    return allActions;
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({ key: "addOrEditEvent", reducer });
const withSaga = injectSaga({ key: "addOrEditEvent", saga });

export default compose(
    withReducer,
    withSaga,
    withConnect
)(AddOrEditEvent);
