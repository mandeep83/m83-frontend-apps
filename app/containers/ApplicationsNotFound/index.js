/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import React from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {Helmet} from "react-helmet";
import {createStructuredSelector} from "reselect";
import {compose} from "redux";

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import makeSelectApplicationNotFound from "./selectors";
import reducer from "./reducer";
import saga from "./saga";

/* eslint-disable react/prefer-stateless-function */
export class ApplicationNotFound extends React.Component {

    logoutHandler = () => {
        delete localStorage['isVerified'];
        delete localStorage['tenantName'];
        delete localStorage['role'];
        delete localStorage['tenant'];
        delete localStorage['sideNav'];
        delete localStorage['token'];
        delete localStorage['Username'];
        // delete localStorage['userId'];
        delete localStorage['oAuthProviders'];
        delete localStorage['isGitHubLogin'];
        delete localStorage['selectedApplicationId'];
        delete localStorage['applications'];
        delete localStorage['VMQ_Credentials'];
        this.props.history.push('/login');
    }

    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>No Applications Found</title>
                    <meta name="description" content="Description of ApplicationNotFound"/>
                </Helmet>
                
                <div className="content-body">
                    <div className="content-add-wrapper">
                        <div className="content-add-bg"><img src="https://content.iot83.com/m83/misc/m83-logo.png" /></div>
                        <div className="content-add-detail">
                            <div className="content-add-image-outer">
                                <div className="content-add-image">
                                    <img src="https://content.iot83.com/m83/misc/addApp.png" />
                                </div>
                            </div>
                            <h5>You don't have any applications assigned.</h5>
                            <h6>Please contact your system administrator</h6>
                            {localStorage.isPlatformLogin &&
                            <button className="btn btn-primary" type="button" onClick={() => logoutHandler()}>
                                <i className="far fa-sign-out-alt"></i>Log Out
                            </button>
                            }
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

ApplicationNotFound.propTypes = {
    dispatch: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
    applicationNotfound: makeSelectApplicationNotFound()
});

function mapDispatchToProps(dispatch) {
    return {
        dispatch
    };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

const withReducer = injectReducer({key: "applicationNotFound", reducer});
const withSaga = injectSaga({key: "applicationNotFound", saga});

export default compose(
    withReducer,
    withSaga,
    withConnect
)(ApplicationNotFound);
