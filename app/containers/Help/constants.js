/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/*
 *
 * HelpAndFaq constants
 *
 */
export const RESET_TO_INITIAL_STATE = "app/HelpAndFaq/RESET_TO_INITIAL_STATE";

export const GET_ALL_FAQ = "app/HelpAndFaq/GET_ALL_FAQ";
export const GET_ALL_FAQ_SUCCESS = "app/HelpAndFaq/GET_ALL_FAQ_SUCCESS";
export const GET_ALL_FAQ_FAILURE = "app/HelpAndFaq/GET_ALL_FAQ_FAILURE";

export const DELETE_FAQ_REQUEST = "app/HelpAndFaq/DELETE_FAQ_REQUEST";
export const DELETE_FAQ_REQUEST_SUCCESS = "app/HelpAndFaq/DELETE_FAQ_REQUEST_SUCCESS";
export const DELETE_FAQ_REQUEST_FAILURE = "app/HelpAndFaq/DELETE_FAQ_REQUEST_FAILURE";
