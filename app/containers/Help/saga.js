/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { apiCallHandler } from '../../api';
import { takeEvery } from 'redux-saga';
import * as CONSTANTS from "./constants";


export function* getAllFaqApiHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.GET_ALL_FAQ_SUCCESS, CONSTANTS.GET_ALL_FAQ_FAILURE, 'getAllFaq')];
}

export function* watcherGetAllFaqRequest() {
  yield takeEvery(CONSTANTS.GET_ALL_FAQ, getAllFaqApiHandlerAsync);
}

export function* deleteFaqApiHandlerAsync(action) {
  yield [apiCallHandler(action, CONSTANTS.DELETE_FAQ_REQUEST_SUCCESS, CONSTANTS.DELETE_FAQ_REQUEST_FAILURE, 'deleteFaq')];
}

export function* watcherDeleteFaqRequest() {
  yield takeEvery(CONSTANTS.DELETE_FAQ_REQUEST, deleteFaqApiHandlerAsync);
}

export default function* rootSaga() {
  yield [
    watcherGetAllFaqRequest(),
    watcherDeleteFaqRequest()
  ];
}
