/************************************************************************
 * 83incs CONFIDENTIAL
 * ***********************************************************************
 *
 *  [2017] - [2022] 83incs Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
 * Source Code Licenses (if any).  The intellectual and technical concepts contained
 * herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
 * Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from 83incs Ltd or IoT83 Ltd.
 ****************************************************************************
 */

/**
 *
 * HelpAndFaq
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { FormattedMessage } from "react-intl";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import reducer from "./reducer";
import saga from "./saga";
import messages from "./messages";
import cloneDeep from 'lodash/cloneDeep';
import * as SELECTORS from "./selectors";
import * as ACTIONS from "./actions";
import draftToHtml from 'draftjs-to-html';
import Loader from "../../components/Loader/Loadable"
import NotificationModal from '../../components/NotificationModal/Loadable';

/* eslint-disable react/prefer-stateless-function */
export class HelpAndFaq extends React.Component {

  state = {
    isFetching: true,
    category: [{
      name: 'connectors',
      displayName: 'Connectors',
      data: [],
      icon: "fal fa-chart-network"
    },
    {
      name: 'transformations',
      displayName: 'ETL/JS Functions',
      data: [],
      icon: "fal fa-exchange"
    },
    {
      name: 'codeEngine',
      displayName: 'FaaS/APIs',
      data: [],
      icon: "fal fa-lambda"
    },
    {
      name: 'dashboardStudio',
      displayName: 'Page/Widgets',
      data: [],
      icon: "fal fa-browser"
    },
    {
      name: 'mlStudio',
      displayName: 'ML Studio',
      data: [],
      icon: "fal fa-lightbulb"
    },
    {
      name: 'simulationStudio',
      displayName: 'Simulation Studio',
      data: [],
      icon: "fal fa-cube"
    },
    {
      name: 'operations',
      displayName: 'Operations',
      data: [],
      icon: "fal fa-tasks"
    },
    {
      name: 'devops',
      displayName: 'Devops',
      data: [],
      icon: "fal fa-wrench"
    }
    ],
    selectedCategory: "connectors",
    filteredList: [],
    searchTerm: ""
  }

  componentDidMount() {
    this.props.getAllFaq()
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.getAllFaqSuccess) {
      let category = cloneDeep(prevState.category)
      nextProps.getAllFaqSuccess.map(temp => {
        category.find(categoryDetail => categoryDetail.name == temp.category).data = temp.documents;
      })
      return {
        ...prevState,
        category,
        isFetching: false
      }
    }

    if (nextProps.deleteFaqSuccess) {
      let category = cloneDeep(prevState.category)
      let data = category.find(categoryDetail => categoryDetail.name == nextProps.deleteFaqSuccess.action.category).data;
      category.find(categoryDetail => categoryDetail.name == nextProps.deleteFaqSuccess.action.category).data = data.filter(temp => temp.id !== nextProps.deleteFaqSuccess.action.id)
      return {
        ...prevState,
        modalType: "success",
        isOpen: true,
        message2: nextProps.deleteFaqSuccess.message,
        category,
        isFetching: false,
      }
    }

    if (Object.keys(SELECTORS).filter(prop => prop.toLowerCase().includes("failure")).some(prop => nextProps[prop])) {
      let propName = Object.keys(SELECTORS).find(props => nextProps[props])
      return {
        ...prevState,
        isFetching: false,
        isOpen: true,
        message2: nextProps[propName],
        modalType: "error",
      }
    }
    return null
  }

  componentDidUpdate(prevProps, prevState) {
    // the below if statement should handle only step api success cases, add a check in case a new success case selector is added 
    if (Object.keys(SELECTORS).some(prop => this.props[prop])) {
      this.props.resetToInitialState()
    }

  }

  deleteFaq = (id, category) => {

    this.setState({
      isFetching: true
    }, () => {
      this.props.deleteFaqRequest(id, category);
    })
  }

  onCloseHandler = () => {
    this.setState({
      isOpen: false,
      message2: '',
      modalType: "",
    })
  }


  searchTerm = (event) => {
    this.setState({
      searchTerm: event.currentTarget.value
    })
  }

  render() {
    let list = this.state.category.find(temp => temp.name === this.state.selectedCategory).data
    let allQuestion = []

    if (this.state.searchTerm) {
      let category = cloneDeep(this.state.category);
      allQuestion = category.map(o => o.data).flat()
      list = allQuestion.filter((temp) => temp.question.toUpperCase().includes(this.state.searchTerm.toUpperCase()));
    }
    return (
      <React.Fragment>
        <Helmet>
          <title>Help</title>
          <meta name="description" content="Description of HelpAndFaq" />
        </Helmet>
        <div className="pageBreadcrumb">
          <div className="row">
            <div className="col-8">
              <p><span>Help</span></p>
            </div>
            <div className="col-4 text-right">
              {localStorage.role === "SYSTEM_ADMIN" && <div className="flex h-100 justify-content-end align-items-center">
                <button type="button" name="button" className="btn btn-primary" onClick={() => this.props.history.push(`/createFaq`)}>
                  <span className="btn-primary-icon"><i className="far fa-plus"></i></span>
                </button>
              </div>}
            </div>
          </div>
        </div>
        {
          this.state.isFetching ?
            <Loader />
            :
            <div className="outerBox helpOuterBox pd-l-0 pd-t-45 pb-0">
              <div className="helpInnerBox">
                <div className="helpHeader">
                  {/*<h1>Help</h1>*/}
                  <div className="input-group helpSearch">
                    <input type="text" id="search" value={this.state.searchTerm} onChange={this.searchTerm} placeholder="Search for help topic" className="form-control" />
                    {this.state.searchTerm &&<div className="input-group-append">
                      <span className="input-group-text bg-transparent border-0 cursor-pointer" onClick={() => this.setState({ searchTerm: '' })}>
                      <i className="far fa-times text-primary"></i>
                    </span>
                    </div>
                    }
                    <div className="icon" >
                      <i className="far fa-search"></i>
                    </div>

                  </div>
                </div>
                <ul className="helpList">
                  {this.state.category.map((temp, index) => (
                    <li className={(this.state.selectedCategory === temp.name && this.state.searchTerm == "") && "active"} key={index} onClick={() => this.setState({ selectedCategory: temp.name })}>
                      <a>
                        <p className="nav-link-icon"><i className={temp.icon}></i></p>
                        <h3>{temp.displayName}</h3>
                      </a>
                    </li>
                  ))}
                </ul>
                <div className="faqBox">
                  {list.length > 0 ?
                      <ul className="faqList listStyleNone">
                        {list.map((temp) => (<li key={temp.id}>
                              <p>
                                {localStorage.role === "SYSTEM_ADMIN" && <span className="btnGroup">
                                  <button onClick={() => this.props.history.push("/createFaq/" + temp.id)} type="button"
                                          className="btn btn-transparent btn-transparent-primary"><i className="far fa-pen"></i></button>
                                  <button onClick={() => this.deleteFaq(temp.id, this.state.selectedCategory)} type="button"
                                        className="btn btn-transparent btn-transparent-danger"><i
                                    className="far fa-trash-alt"></i></button>
                                </span>}
                                <span className="collapsed" data-toggle="collapse" data-target={"#demo" + temp.id}>{temp.question}</span>
                                <i className="far fa-chevron-down"></i>
                              </p>
                              <div className="answerBox collapse" id={"demo" + temp.id}>
                                {<div className="pd-b-10" dangerouslySetInnerHTML={{__html: draftToHtml(temp.answer)}}></div>}
                              </div>
                            </li>
                        ))
                        }
                      </ul>
                      :
                      <div className="contentAddBox">
                        <div className="contentAddBoxDetail">
                          <div className="contentAddBoxImage">
                            <img src="https://content.iot83.com/m83/icons/search.png" />
                          </div>
                          <h6>No data found.</h6>
                        </div>
                      </div>
                  }
                </div>
              </div>
            </div>
        }
        {
          this.state.isOpen &&
          <NotificationModal
            type={this.state.modalType}
            message2={this.state.message2}
            onCloseHandler={this.onCloseHandler}
          />
        }
      </React.Fragment >
    );
  }
}

HelpAndFaq.propTypes = {
  dispatch: PropTypes.func.isRequired
};


let allSelectors = {}
Object.entries(SELECTORS).map(([key, value]) => {
  allSelectors[key] = value()
})

const mapStateToProps = createStructuredSelector(allSelectors)


function mapDispatchToProps(dispatch) {
  let allActions = { dispatch }
  Object.entries(ACTIONS).map(([key, value]) => {
    allActions[key] = (...args) => dispatch(value(...args))
  })
  return allActions
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

const withReducer = injectReducer({ key: "helpAndFaq", reducer });
const withSaga = injectSaga({ key: "helpAndFaq", saga });

export default compose(
  withReducer,
  withSaga,
  withConnect
)(HelpAndFaq);
