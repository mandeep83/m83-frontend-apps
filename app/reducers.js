/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { combineReducers } from 'redux-immutable';
import { fromJS } from 'immutable';
import { LOCATION_CHANGE } from 'react-router-redux';
import * as CONSTANTS from "./containers/CreateOrEditPage/constants"
import * as PREVIEW_PAGE_CONSTANTS from "./containers/PagePreview/constants"
import languageProviderReducer from 'containers/LanguageProvider/reducer';

/*
 * routeReducer
 *
 * The reducer merges route location changes into our immutable state.
 * The change is necessitated by moving to react-router-redux@4
 *
 */

// Initial routing state
const routeInitialState = fromJS({
  location: null,
});

/**
 * Merge route into the global application state
 */
export function routeReducer(state = routeInitialState, action) {
  switch (action.type) {
    /* istanbul ignore next */
    case LOCATION_CHANGE:
      return state.merge({
        location: action.payload,
      });
    default:
      return state;
  }
}

const loaderInitialState = fromJS({
  isFetching: false,
});

/**
 * Merge route into the global application state
 */
export function loaderReducer(state = loaderInitialState, action) {
  switch (action.type) {
    /* istanbul ignore next */
    case 'show_loader':
      return state.set('isFetching', true);
    case 'hide_loader':
      return state.set('isFetching', false);
    default:
      return state;
  }
}
let initialState = fromJS({})
let allCases = Object.values(CONSTANTS).filter(constant => typeof (constant) == "object")

let getValueObj = (action, index, allCases) => {
  let isSuccessCase = action.type.includes("SUCCESS")
  let key = isSuccessCase ? allCases[index].successKey : allCases[index].failureKey
  return {
    [key]: action
  }
}

export function createOrEditPageReducer(state = initialState, action) {
  if (action.type === CONSTANTS.RESET_TO_INITIAL_STATE_PROPS) {
    return Object.assign({}, state, {
      [action.props]: undefined,
    })
  }
  for (let i = 0; i < allCases.length; i++) {
    if (action.type === allCases[i].success || action.type === allCases[i].failure) {
      return Object.assign({}, state, getValueObj(action, i, allCases));
    }
  }
  return state
}


let initialStatePreviewPage = fromJS({})
let allCasesPreviewPage = Object.values(PREVIEW_PAGE_CONSTANTS).filter(constant => typeof (constant) == "object")
export function previewPageReducer(state = initialStatePreviewPage, action) {
  if (action.type === PREVIEW_PAGE_CONSTANTS.RESET_TO_INITIAL_STATE_PROPS) {
    return Object.assign({}, state, {
      [action.props]: undefined,
    })
  }
  for (let i = 0; i < allCasesPreviewPage.length; i++) {
    if (action.type === allCasesPreviewPage[i].success || action.type === allCasesPreviewPage[i].failure) {
      return Object.assign({}, state, getValueObj(action, i, allCasesPreviewPage));
    }
  }
  return state
}

/**
 * Creates the main reducer with the dynamically injected ones
 */
export default function createReducer(injectedReducers) {
  return combineReducers({
    route: routeReducer,
    language: languageProviderReducer,
    loader: loaderReducer,
    createOrEditPageReducer,
    previewPageReducer,
    ...injectedReducers,
  });
}
