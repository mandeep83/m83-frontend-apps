/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

import { put } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import cloneDeep from 'lodash/cloneDeep';
import moment from 'moment';
const CHANGE_ETL_STATE_FAILURE = "app/EtlConfigList/CHANGE_ETL_STATE_FAILURE";
const LOGIN_FAILED = "M83/Login/LOGIN_FAILED";
import { GOOGLE_MAP_KEY } from './utils/constants';
import Geocode from "react-geocode";
import { HANDLE_402 } from "../app/containers/HomePage/constants"
Geocode.setApiKey(GOOGLE_MAP_KEY);
Geocode.enableDebug();
import React, { useEffect, useState } from "react";

export const useDebounce = (value, delay) => {
    const [debouncedValue, setDebouncedValue] = useState(value);

    useEffect(() => {
        const handler = setTimeout(() => {
            setDebouncedValue(value);
        }, delay);

        return () => {
            clearTimeout(handler);
        };
    }, [value, delay]);

    return debouncedValue;
};

export function* ErrorCheck(action, error, constant) {
    // if (error && !error.response) {
    //     yield put({ type: constant, error: new Date() });
    // } else {
    if (error.response) {
        if (error.response.status === 401) {
            if (constant === CHANGE_ETL_STATE_FAILURE) {
                yield put({ type: constant, error: error.message, addOns: action });
            }
            if (constant === LOGIN_FAILED) {
                yield put({ type: constant, error: error.response.data.message });
            } else {
                delete localStorage.token;
                delete localStorage.showThemePopUp;
                delete localStorage.selectedProjectId
                yield put(push('/login'));
            }
        }
        else if (error.response.status === 402) {
            if (localStorage.tenantType === 'FLEX') {
                yield put({ type: constant, error: error.response.data.message, addOns: action });
            } else if (localStorage.role === 'DEVELOPER') {
                yield put({ type: constant, error: error.response.data.message, addOns: action });
            } else {
                delete localStorage.showThemePopUp;
                delete localStorage.token;
                delete localStorage.selectedProjectId
                yield put({ type: HANDLE_402, error: error.response.data.message, addOns: action });
            }
        }
        else if (error.response.status === 400) {
            yield put({ type: constant, error: error.response.data.data || error.response.data.error, addOns: action });
        }
        else {
            yield put({ type: constant, error: error.response.data.message, addOns: action });
        }
    } else {
        yield put({ type: constant, error: error.message, addOns: action });
    }
}

export function GetHeaders(hasFormData) {
    let headers = {
        headers: {
            'Content-Type': hasFormData ? "multipart/form-data" : 'application/json',
        }
    };

    localStorage.token && (headers.headers["x-authorization"] = `Bearer ${localStorage.token}`);
    (localStorage.selectedProjectId && localStorage.role !== "SYSTEM_ADMIN") && (headers.headers["x-project-id"] = `${localStorage.selectedProjectId}`);
    return headers
}

export function GetHeadersWithoutContentType() {
    let headers = {
        headers: {}
    };

    if (localStorage.token) {
        headers.headers["x-authorization"] = `Bearer ${localStorage.token}`;
        headers.headers["x-project-id"] = `${localStorage.selectedProjectId}`;
    }
    return headers;

}

export function setCharAt(str, index, chr) {
    if (index > str.length - 1) return str;
    return str.substr(0, index) + chr + str.substr(index + 1);
}

export function stringAppend(data, length) {
    return data ? data.length > length ? data.substr(0, length) + "..." : data : 'N/A';
}

export function errorHandling(keys, error, _this) {
    let obj = {};
    keys.map((key, index) => {
        if (index === 0) {
            obj[key] = "error";
        } else if (index === 1) {
            obj[key] = true
        } else if (index === 2) {
            obj[key] = error
        } else {
            obj[key] = false
        }
    })
    return _this.setState(obj)
}

export function uIdGenerator() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

export function compare(a, b) {
    const nameA = a.name.toUpperCase();
    const nameB = b.name.toUpperCase();
    let comparison = 0;
    if (nameA > nameB) {
        comparison = 1;
    } else if (nameA < nameB) {
        comparison = -1;
    }
    return comparison;
}

export function getTimeDifference(timestamp) {
    if (!timestamp) {
        return ""
    }
    let currentTimestamp = Date.now(),
        timeDiffInMiliseconds = currentTimestamp - timestamp;
    if (timeDiffInMiliseconds < 0) {
        return "few seconds ago"
    }
    let totalSeconds = parseInt(Math.floor(timeDiffInMiliseconds / 1000)),
        totalMinutes = parseInt(Math.floor(totalSeconds / 60)),
        totalHours = parseInt(Math.floor(totalMinutes / 60)),
        days = parseInt(Math.floor(totalHours / 24));
    if (days) {
        return `${days} ${days > 1 ? "days" : "day"} ago`
    } else if (totalHours) {
        return `${totalHours} ${totalHours > 1 ? "hours" : "hour"} ago`
    } else if (totalMinutes) {
        return `${totalMinutes} ${totalMinutes > 1 ? "minutes" : "minute"} ago`
    } else {
        return `${totalSeconds} ${totalSeconds > 1 ? "seconds" : "second"} ago`
    }
}
export function getBillingTime(timestamp) {
    if (!timestamp) {
        return ""
    }
    let date = new Date(Date.parse(new Date(timestamp).toLocaleString('en-US', { timeZone: localStorage.timeZone }))),
        currentDate = new Date(Date.parse(new Date().toLocaleString('en-US', { timeZone: localStorage.timeZone })));
    if (currentDate.getDate() - date.getDate() === 0) {
        return `Today, ${date.getHours() < 10 ? `0${date.getHours()}` : date.getHours()}:${date.getMinutes() < 10 ? `0${date.getMinutes()}` : date.getMinutes()}`
    } else if (currentDate.getDate() - date.getDate() === 1) {
        return `Yesterday, ${date.getHours() < 10 ? `0${date.getHours()}` : date.getHours()}:${date.getMinutes() < 10 ? `0${date.getMinutes()}` : date.getMinutes()}`
    } else {
        return `${date.getMonth() + 1}/${date.getDate()}/${date.getFullYear()}, ${date.getHours() < 10 ? `0${date.getHours()}` : date.getHours()}:${date.getMinutes() < 10 ? `0${date.getMinutes()}` : date.getMinutes()}`
    }
}


export function getInitials(string) {
    return string ? string.split(' ').map(x => x.charAt(0)).join('').substr(0, 2).toUpperCase() : "N/A";
};

export function convertTimestampToDate(timestamp) {
    return timestamp ? new Date(timestamp).toLocaleString('en-US', { timeZone: localStorage.timeZone }) : "-";
};

export function toTitleCase(str) {
    return str.replace(
        /\w\S*/g,
        function (txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        }
    )
}

export function isUsedForEnabled(checkingFor) {
    let oAuthList = localStorage.oAuthProviders ? JSON.parse(localStorage.getItem("oAuthProviders")) : [];
    if (oAuthList.length === 0) {
        return false;
    } else {
        let gitHubOauth = oAuthList.find(provider => provider.name === 'gitHub')
        let usedForList = gitHubOauth ? gitHubOauth.usedFor.split(',') : [];
        if (usedForList.includes(checkingFor)) {
            return true;
        } else {
            return false;
        }
    }
}

export function getBucketSizeOptions(reportInterval) {
    let bucketOptions = [],
        optionsInSecs = [];
    if (reportInterval < 60) {
        optionsInSecs = [300, 900, 1800, 3600, 10800, 21600, 43200, 86400];
    } else if (reportInterval >= 60 && reportInterval < 1800) {
        optionsInSecs = [1800, 3600, 10800, 21600, 43200, 86400, 259200, 604800];
    } else {
        optionsInSecs = [3600, 10800, 21600, 43200, 86400, 259200, 604800, 1296000];
    }
    optionsInSecs.map(timeInSecs => {
        let label = '';
        if (timeInSecs < 3600) {
            label = `${timeInSecs/60} Mins`;
        } else if (timeInSecs < 86400) {
            label = `${timeInSecs/3600} Hour${timeInSecs/3600 > 1 ? 's' : ''}`;
        } else {
            label = `${timeInSecs/86400} Day${timeInSecs/86400 > 1 ? 's' : ''}`;
        }
        bucketOptions.push({
            label,
            value: timeInSecs,
        });
    });
    return bucketOptions;
}

export const getNotificationObj = (message2, modalType) => {
    return {
        isFetching: false,
        isOpen: true,
        message2,
        modalType,
    }
}

export function getVMQCredentials() {
    let vmqCredentials = JSON.parse(localStorage.VMQ_Credentials),
        userName = window.atob(vmqCredentials.username),
        password = window.atob(vmqCredentials.password);

    password = password.substring(8, password.length - 6);

    return {
        userName,
        password,
    }
}

export const getTopicName = (connector, deviceId, connectorsList) => {
    let topicId = connectorsList.find(temp => temp.id === connector).properties.topics[0].name;
    return `${topicId}/${deviceId}/report`
}


export function getFilteredItems(allItems, filters) {
    if (Object.values(filters).some(val => val)) {
        return allItems.filter(dashboard => {
            let activeFilters = []
            Object.entries(filters).forEach(([key, value]) => {
                if (value) activeFilters.push(key)
            })
            let filterStatus = {}
            Object.keys(filters).map(filterName => {
                filterStatus[filterName] = true
            })
            activeFilters.map(filter => {
                filterStatus[filter] = dashboard[filter].toLowerCase().includes(filters[filter].toLowerCase())
            })
            return Object.values(filterStatus).every(status => status)
        })
    }
    return allItems
}

export function getAddressFromLatLng(lat, lng, addressHandler, errorHandler) {
    Geocode.fromLatLng(lat, lng).then(
        response => {
            const address = response.results[0].formatted_address
            addressHandler(address, lat, lng)
        },
        error => {
            if (error.message.includes("ZERO_RESULTS")) {
                addressHandler("Unknown Location", lat, lng)
            }
            else {
                errorHandler && errorHandler(error.message)
            }
            console.error('Map Error: ', error);
        }
    );
}

export function isNavAssigned(navName) {
    let sideNav = JSON.parse(localStorage.getItem('sideNav')),
        isNavAssigned = false;
    sideNav.map(nav => {
        if (nav.subMenus.length > 0) {
            nav.subMenus.map(subNav => {
                if (subNav.url === navName) {
                    isNavAssigned = true;
                }
            })
        } else {
            if (nav.url === navName) {
                isNavAssigned = true;
            }
        }
    })
    return isNavAssigned;
}

export const allRegexPatterns = {
    alphanumeric_underscore_forwardSlash : /^[0-9a-zA-Z/_]+$/, // will return false for empty string, add a check accordingly
    alphabeticStart_aplhanumericEnd_aplhanumericAndUndercoreInMiddle : /^([a-zA-Z])(?!.*_$)([\w]*)?$/, // will return false for empty string, add a check accordingly
}

export const DURATION_OPTIONS = [
    {
        value: 60,
        label: "Last 1 hour"
    },
    {
        value: 180,
        label: "Last 3 hours"
    },
    {
        value: 360,
        label: "Last 6 hours"
    },
    {
        value: 720,
        label: "Last 12 hours"
    },
    {
        value: 1440,
        label: "Last 1 day"
    },
    {
        value: 4320,
        label: "Last 3 days"
    },
    {
        value: 10080,
        label: "Last 7 days"
    }
]

export const getMiniLoader = () => {
    return (
        <div className="inner-loader-wrapper h-100">
            <div className="inner-loader-content">
                <i className="fad fa-sync-alt fa-spin"></i>
            </div>
        </div>
    );
}