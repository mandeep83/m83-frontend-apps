/* ***********************************************************************
* 83incs CONFIDENTIAL
* ***********************************************************************
*
*  [2017] - [2022] 83incs Ltd.
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of 83incs Ltd, IoT83 Ltd, its suppliers (if any), its subsidiaries (if any) and
* Source Code Licenses (if any).  The intellectual and technical concepts contained
* herein are proprietary to 83incs Ltd, IoT83 Ltd, its subsidiaries (if any) and
* Source Code Licenses (if any) and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from 83incs Ltd or IoT83 Ltd.
****************************************************************************
*/

export const RESTART_ON_REMOUNT = '@@saga-injector/restart-on-remount';
export const DAEMON = '@@saga-injector/daemon';
export const ONCE_TILL_UNMOUNT = '@@saga-injector/once-till-unmount';

export const GOOGLE_MAP_KEY = 'AIzaSyDhe3XJyqFibPDk5tRdcAMeH09cv7Elc08';
export const GOOGLE_MAP_URL = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDhe3XJyqFibPDk5tRdcAMeH09cv7Elc08&libraries=visualization';
export const GOOGLE_MAP_URL_PLACE = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDhe3XJyqFibPDk5tRdcAMeH09cv7Elc08&libraries=places';

export const CDN_LIST = [
	{
		value: "https://unpkg.com/react@16/umd/react.development.js",
		isRemovable: false
	},
	{
		value: "https://unpkg.com/react-dom@16/umd/react-dom.development.js",
		isRemovable: false
	},
	{
		value: "https://unpkg.com/@babel/standalone/babel.min.js",
		isRemovable: false
	}
]
export const DAYS_IN_WEEK = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

