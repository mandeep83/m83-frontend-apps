FROM 83incs/node:12.13.1 as build
COPY . ./
RUN yarn install
RUN build_version=build_number deployment_env=Branch_Name yarn run build

FROM 83incs/m83-nginx:ui
COPY --chown=nginx:nginx --from=build ./build /build
ENTRYPOINT ["nginx", "-g", "daemon off;"]
